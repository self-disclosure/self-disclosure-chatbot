# provided by Zhou

import pandas as pd
import numpy as np
import statsmodels.formula.api as sm
import pprint

#import matplotlib.pyplot as plt

pd.options.display.max_colwidth = 1000

path = "./"
filename = "output"
df = pd.read_csv(path + filename + '.csv')
df['rating'] = df['rating'].apply(lambda x: float(x.replace("*", "")) if type(x) == str else x)
#a = df[df['rating']==2.0]
#f = open(filename+'_2Rating' +'.csv','w')
a = dict()
for ind in range(len(df['selected_modules'])):
        i = df['selected_modules'][ind]
        if i not in a.keys():
            a.update({i:[1,0]})
        else:
            a[i][0]+=1
        rating = df['rating'][ind]
        a[i][1] = (a[i][1]*(a[i][0]-1)+rating)/(a[i][0])

pprint.pprint(a)
