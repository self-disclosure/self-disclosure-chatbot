# Alexa Prize Framework

This is a stack of CloudFormation templates that deliver a framework designed to give teams a CI/CD (Continuous Integration / Continuous Deployment) environment with which to rapidly prototype, deploy, and iterate on socialbots for the Alexa Prize.

Launching the `master.yaml` AWS CloudFormation template provisions a stack that deploys a continuous deployment process that uses AWS CodePipeline to monitor a GitHub repository for new commits, AWS CodeBuild to create a new Docker container image and to push it into Amazon ECR, and AWS CloudFormation to deploy the new container image to production on Amazon ECS. The Flask application sits behind an Amazon Application Load Balancer (ALB) to provide the scalability and resiliency to handle traffic from socialbot customers.

![](architecture.png)

## Running the template

Specify the `master.yaml` template as a new stack in CloudFormation. The template requires the following parameters:

- GitHub configuration
  - Repo: The repo name of the sample service.
  - Branch: The branch of the repo to deploy continuously.
  - User: Your username on GitHub.
  - Personal Access Token: Token for the user specified above. (https://github.com/settings/tokens)

- The CloudFormation stack provides the following output:
  - ServiceUrl: The sample service that is being continuously deployed.
  - PipelineUrl: The continuous deployment pipeline in the AWS Management Console.

## Testing your Stack

After the CloudFormation stack is created, the latest commit to the GitHub repository is run through the pipeline and deployed to ECS. Open the PipelineUrl to watch the first revision run through the CodePipeline pipeline. After the deploy step turns green, open the URL from ServiceUrl which loads a page similar to this:

    { "response": "pong" }

Calls from your Lambda function to the root of your ALB URL will return the `pong` message. Add additional functionality through additional endpoints as necessary.

To test continuous deployment, make a change to `src/main.py` in your repository and push it to GitHub. CodePipeline detects the change, builds the new application, and deploys it to your cluster automatically. After the pipeline finishes deploying the revision, reload the page to see the changes made.

#### Credits

Based off of
[https://github.com/awslabs/ecs-refarch-continuous-deployment](https://github.com/awslabs/ecs-refarch-continuous-deployment).

## Updating the <aster Stack

When you update any of the CloudFormation templates, they must be copied to s3://alexaprize-infra so that everything stays in sync.

    zip -r templates.zip templates
    aws s3 cp templates.zip s3://alexaprize-infra/ --acl public-read
    aws s3 cp master.yaml s3://alexaprize-infra/ --acl public-read
    aws s3 sync templates/ s3://alexaprize-infra/templates/ --acl public-read
