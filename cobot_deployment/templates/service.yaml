Parameters:

    ProcessorType:
        Type: String
        Default: cpu
        
    TaskGPUUsage:
        Type: String
        Default: ''

    Tag:
        Type: String
        Default: latest

    Repository:
        Type: String
        Default: ''

    DesiredCount:
        Type: Number
        Default: 1

    MinTaskNumber:
        Description: Min. Number of Tasks the Service should run
        Type: String
        Default: 1

    MaxTaskNumber:
        Description: Max. Number of Tasks the Service should run
        Type: String
        Default: 20

    TaskScaleUpAdjustment:
        Description: The number by which the tasks should increment
        Type: String
        Default: 1

    TaskScaleDownAdjustment:
        Description: The number by which the tasks should decrease
        Type: String
        Default: 1

    TargetGroup:
        Type: String

    Cluster:
        Type: String

    ModuleName:
        Type: String

    ContainerPort:
        Type: Number
        Default: 80

    MemoryReservation:
        Type: Number
        Default: 4096

    ImageId:
        Type: String
        Default: ''

    EnvEngineAuthType:
        Type: String
        Default: '' 

    EnvEngineAuthData:
        Type: String
        Default: ''

    EntryPoint:
        Type: CommaDelimitedList
        Default: ""

    Stage:
        Type: String
        Default: PROD

    HealthCheckGracePeriodSeconds:
        Type: Number
        Default: 0

Conditions:
    EnvAuthTypeExists: !Not [ !Equals [!Ref EnvEngineAuthType, '']]
    EnvAuthDataExists: !Not [ !Equals [!Ref EnvEngineAuthData, '']]
    ImageIdExists: !Not [ !Equals [!Ref ImageId, '']]
    EntryPointExists: !Not [ !Equals [!Join ['', !Ref EntryPoint], '']]
    GPUForContainer: !Equals [!Ref ProcessorType,'gpu']

Resources:
    ECSServiceRole:
        Type: AWS::IAM::Role
        Properties:
            Path: /
            AssumeRolePolicyDocument: |
                {
                        "Statement": [{
                                "Effect": "Allow",
                                "Principal": { "Service": [ "ecs.amazonaws.com","application-autoscaling.amazonaws.com" ]},
                                "Action": [ "sts:AssumeRole" ]
                        }]
                }
            ManagedPolicyArns:
                - arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceRole
                - arn:aws:iam::aws:policy/AWSOpsWorksCloudWatchLogs
                - arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceAutoscaleRole

    TaskRole:
        Type: AWS::IAM::Role
        Properties:
            AssumeRolePolicyDocument:
                Statement:
                - Effect: Allow
                  Principal:
                      Service: [ecs-tasks.amazonaws.com]
                  Action: ['sts:AssumeRole']
            Path: /
            Policies:
            - PolicyDocument:
                Statement:
                - Action:
                  - logs:CreateLogGroup
                  - logs:CreateLogStream
                  - logs:PutLogEvents
                  - logs:DescribeLogStreams
                  - s3:*
                  - sns:*
                  - sqs:*
                  Effect: Allow
                  Resource: "*"
                Version: 2012-10-17
              PolicyName: !Sub ${AWS::Region}-${AWS::StackName}-taskrole

    ContainerLog:
        Type: "AWS::Logs::LogGroup"
        Properties:
            LogGroupName: !Sub ${AWS::StackName}-container-log
            RetentionInDays: 7

    ECSService:
        Type: AWS::ECS::Service
        Properties:
            Cluster: !Ref Cluster
            Role: !Ref ECSServiceRole
            DesiredCount: !Ref DesiredCount
            TaskDefinition: !Ref TaskDefinition
            HealthCheckGracePeriodSeconds: !Ref HealthCheckGracePeriodSeconds
            LoadBalancers:
                - 
                    ContainerName: !Ref ModuleName
                    ContainerPort: !Ref ContainerPort
                    TargetGroupArn: !Ref TargetGroup
    TaskDefinition:
        Type: AWS::ECS::TaskDefinition
        Properties:
            Family: !Sub ${AWS::StackName}
            PlacementConstraints:
                !If
                - GPUForContainer
                - 
                    -   Expression: !Sub attribute:processor == ${ProcessorType}
                        Type: memberOf
                - !Ref "AWS::NoValue"
            ContainerDefinitions:
                - 
                    Name: !Ref ModuleName
                    ResourceRequirements: 
                        !If
                        - GPUForContainer
                        -
                            -   Type: "GPU"
                                Value: !Ref TaskGPUUsage 
                        - !Ref "AWS::NoValue"
                    Image: 
                        'Fn::If':
                            - ImageIdExists
                            - !Ref ImageId
                            - !Sub "${AWS::AccountId}.dkr.ecr.${AWS::Region}.amazonaws.com/${Repository}:${Tag}"

                    EntryPoint: 
                        'Fn::If':
                            - EntryPointExists
                            - !Ref EntryPoint
                            - !Ref "AWS::NoValue"
                    Essential: true
                    MemoryReservation: !Ref MemoryReservation
                    MountPoints:
                        - 
                            SourceVolume: my-vol
                            ContainerPath: /var/www/my-vol
                    PortMappings:
                        - ContainerPort: !Ref ContainerPort
                    LogConfiguration:
                        LogDriver: 'awslogs'
                        Options:
                            awslogs-group: !Ref ContainerLog
                            awslogs-region: !Ref "AWS::Region"
                    Environment:
                        - 
                            Name: Tag
                            Value: !Ref Tag
                        - 'Fn::If':
                            - EnvAuthTypeExists
                            -
                                Name: !Select [0, !Split ["|||", !Ref EnvEngineAuthType]]
                                Value: !Select [1, !Split ["|||", !Ref EnvEngineAuthType]]
                            - !Ref "AWS::NoValue"

                        - 'Fn::If':
                            - EnvAuthDataExists
                            -
                                Name: !Select [0, !Split ["|||", !Ref EnvEngineAuthData]]
                                Value: !Select [1, !Split ["|||", !Ref EnvEngineAuthData]]
                            - !Ref "AWS::NoValue"
                        - 
                            Name: STAGE
                            Value: !Ref Stage
            Volumes:
                - Name: my-vol
            TaskRoleArn: !Ref TaskRole

# Task Level AutoScaling Rules Starts here
    AutoScalingService:
      Type: AWS::ApplicationAutoScaling::ScalableTarget
      Properties:
        MaxCapacity: !Ref MaxTaskNumber
        MinCapacity: !Ref MinTaskNumber
        ResourceId: !Join [/, [service, !Ref Cluster, !GetAtt [ECSService, Name]]]
        RoleARN: !GetAtt ECSServiceRole.Arn
        ScalableDimension: ecs:service:DesiredCount
        ServiceNamespace: ecs
    ScalingUpPolicyService:
      Type: AWS::ApplicationAutoScaling::ScalingPolicy
      Properties:
        PolicyName: !Sub ${AWS::StackName}-${ECSService}-ScalingUpPolicy
        PolicyType: StepScaling
        ScalingTargetId: !Ref AutoScalingService
        StepScalingPolicyConfiguration:
          AdjustmentType: ChangeInCapacity
          Cooldown: 60
          StepAdjustments:
          - ScalingAdjustment: !Ref TaskScaleUpAdjustment
            MetricIntervalLowerBound: 0
    ScalingDownPolicyService:
      Type: AWS::ApplicationAutoScaling::ScalingPolicy
      Properties:
        PolicyName: !Sub ${AWS::StackName}-${ECSService}-ScalingDownPolicy
        PolicyType: StepScaling
        ScalingTargetId: !Ref AutoScalingService
        StepScalingPolicyConfiguration:
          AdjustmentType: ChangeInCapacity
          Cooldown: 60
          StepAdjustments:
          - ScalingAdjustment: !Ref TaskScaleDownAdjustment
            MetricIntervalUpperBound: -20
          - ScalingAdjustment: -1
            MetricIntervalLowerBound: -20
            MetricIntervalUpperBound: -10
    HighCPUServiceAlarm:
      Type: AWS::CloudWatch::Alarm
      Properties:
        AlarmDescription: ECS Service CPUUtilization exceeding threshold 70% . Triggers scale up
        ActionsEnabled: true
        Namespace: AWS/ECS
        MetricName: CPUUtilization
        Unit: Percent
        Dimensions:
        - Name: ClusterName
          Value: !Ref Cluster
        - Name: ServiceName
          Value: !GetAtt [ECSService, Name]
        Statistic: Maximum
        Period: '60'
        EvaluationPeriods: '1'
        Threshold: '70'
        AlarmActions: [!Ref ScalingUpPolicyService]
        ComparisonOperator: GreaterThanThreshold
    LowCPUServiceAlarm:
      Type: AWS::CloudWatch::Alarm
      Properties:
        AlarmDescription: ECS Service CPUUtilization lowers threshold 20% . Triggers scale down
        ActionsEnabled: true
        Namespace: AWS/ECS
        MetricName: CPUUtilization
        Unit: Percent
        Dimensions:
        - Name: ClusterName
          Value: !Ref Cluster
        - Name: ServiceName
          Value: !GetAtt [ECSService, Name]
        Statistic: Average
        Period: '60'
        EvaluationPeriods: '1'
        Threshold: '20'
        AlarmActions: [!Ref ScalingDownPolicyService]
        ComparisonOperator: LessThanThreshold
    HighMemoryUtilizationServiceAlarm:
      Type: AWS::CloudWatch::Alarm
      Properties:
        AlarmDescription: ECS Service MemoryUtilization exceeding threshold 70% . Triggers scale up
        ActionsEnabled: true
        Namespace: AWS/ECS
        MetricName: MemoryUtilization
        Unit: Percent
        Dimensions:
        - Name: ClusterName
          Value: !Ref Cluster
        - Name: ServiceName
          Value: !GetAtt [ECSService, Name]
        Statistic: Maximum
        Period: '60'
        EvaluationPeriods: '1'
        Threshold: '70'
        AlarmActions: [!Ref ScalingUpPolicyService]
        ComparisonOperator: GreaterThanThreshold
    LowMemoryUtilizationServiceAlarm:
      Type: AWS::CloudWatch::Alarm
      Properties:
        AlarmDescription: ECS Service MemoryUtilization lowers threshold 20% . Triggers scale down
        ActionsEnabled: true
        Namespace: AWS/ECS
        MetricName: MemoryUtilization
        Unit: Percent
        Dimensions:
        - Name: ClusterName
          Value: !Ref Cluster
        - Name: ServiceName
          Value: !GetAtt [ECSService, Name]
        Statistic: Average
        Period: '60'
        EvaluationPeriods: '1'
        Threshold: '20'
        AlarmActions: [!Ref ScalingDownPolicyService]
        ComparisonOperator: LessThanThreshold
# Task Level AutoScaling Rules Ends here

