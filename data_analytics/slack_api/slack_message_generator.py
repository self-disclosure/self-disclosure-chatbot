def load_integration_test_link(result_integration, data, channels="#analytics"):

    attachments = []
    result_json = {"channel": channels, "text": "_Find your integration test cases here._  \n*Input* - input.txt for debugging.  \n*Output* - ASR/TTS result"}
    for conv_id in data:
        files = data[conv_id]
        input_loc = result_integration("inputs", files[0].split("/")[-1])
        output_loc = result_integration("outputs", files[1].split("/")[-1])
        fallback = "Integration Test Generation Disabled"
        input_button = {"type": "button", "text": "Input Files", "url": input_loc}
        output_button = {"type": "button", "text": "Output Files", "url": output_loc, "style": "primary"}

        attachments.append({"fallback": fallback, "text": "Conversation ID : {}...".format(conv_id[:10]), "actions":[input_button, output_button]})

    result_json["attachments"] = attachments
    return result_json