import requests, os
import json

from data_analytics.slack_api.slack_message_utils import read_from_results, \
    create_message_json, add_abtest, add_resp_type

from data_analytics.data_utils.utils import get_conversations_by_id, combine_df
from data_analytics.athena_metadata import today_string
from data_analytics.analytics_metadata import DAILY_ERROR, DAILY_RESP_TYPE, DAILY_COUNT, DAILY_AB_TEST, DAILY_DATA, get_raw_filepath

import slack

module_to_user = {"TECHSCIENCECHAT":"techscience", "FOODCHAT":"food_drink", "BOOKCHAT":"book", "SPORT":"sports","CONTROVERSIALOPION":"controversial", "SOCIAL":"social","TRAVELCHAT":"travel",\
                  "MUSICCHAT":"music", "LAUNCHGREETING":"launchgreeting", "ANIMALCHAT":"animal", "NEWS":"news", "MOVIECHAT":"movies", \
                      "FASHIONCHAT":"fashion", "GAMECHAT":"game", "RETRIEVAL":"retrieval"}

def send_sample_file(topic, df):
    filename = "daily_error_csv_file_for_{}_{}.csv".format(topic, today_string)
    if df.shape[0] < 1:
        return None
    df.to_csv(filename)
    if topic in module_to_user:
        channel = module_to_user[topic]
        client.files_upload(channels="#debug", initial_comment="Daily Error CSV for {}".format(topic), \
                                mimetype= "text/csv",\
                                filetype="csv",title="daily_error_csv_file_for_{}".format(topic), \
                                name=filename, file=open(filename, "r"))
        return None
    else:
        return filename
def send_error_csv(daily_error, output_joined_file, output_no_rating):
    other_files = []
    for topic in daily_error:
        keys = daily_error[topic]
        rated_df = get_conversations_by_id(output_joined_file, keys.get("conv_id_rating"), True)
        unrated_df = get_conversations_by_id(output_no_rating, keys.get("conv_id_no_rating"), False)
        combined_df = combine_df(rated_df + unrated_df)
        result = send_sample_file(topic, combined_df)
        if result is not None:
            client.files_upload(channels="#debug", initial_comment="Daily Error CSV for {}".format(topic), \
                                mimetype= "text/csv",\
                                filetype="csv",title="daily_error_csv_file_for_{}".format(topic), \
                                name=result, file=open(result, "r"))
    return


if __name__ == '__main__':

    webbook_url = 'https://hooks.slack.com/services/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'
    token = "xoxb-295775362084-818546181940-EA1hKU4anGOaHNrfBcopxSLD"
    client = slack.WebClient(token=token)

    daily_json = read_from_results(DAILY_DATA)
    daily_ab = read_from_results(DAILY_AB_TEST)
    daily_count = read_from_results(DAILY_COUNT)
    daily_resptype = read_from_results(DAILY_RESP_TYPE)
    daily_error = read_from_results(DAILY_ERROR)
    ab_test_string = add_abtest(daily_ab)
    resp_type_string = add_resp_type(daily_resptype)
    json_dict = create_message_json(daily_json, daily_count, daily_error, [ab_test_string, resp_type_string], "#debug")

    try:
        # Get the Integration Test Results
        response = client.api_call('chat.postMessage', json=json_dict)
        output_joined_file = get_raw_filepath(include_tail=True)
        output_no_rating = get_raw_filepath(include_tail=True, include_no_rating=True)
        # Upload daily statistics file
        if os.path.exists(output_joined_file) and os.path.exists(output_no_rating):
            client.files_upload(channels="#debug", initial_comment="Daily Statistics CSV", \
                                mimetype= "text/csv",\
                                url_private_download= "https://.../output.csv",
                                filetype="csv",title="daily_statistics_{}.csv".format(today_string), name="daily_statistics_{}.csv".format(today_string), file=open(output_joined_file, "r"))
        #     client.files_upload(channels="#debug", initial_comment="Daily Conversation No Rating CSV", \
        #                         mimetype= "text/csv",\
        #                         url_private_download= "https://.../output.csv",
        #                         filetype="csv",title="daily_statistics_no_rating_{}.csv".format(today_string), name="daily_conversation_{}_no_rating.csv".format(today_string), file=open(output_no_rating, "r"))

        send_error_csv(daily_error, output_joined_file, output_no_rating)

    except Exception as e:
        print("Message not sent")
        print(e)

    