import os
from flask import Flask, request, Response, send_file
from flask import Flask, redirect, url_for, render_template, request

from data_analytics.athena_metadata import ANALYTICS_ROOT
from datetime import datetime as dt
import pandas as pd
import datetime
import slack
import subprocess
from data_analytics.slack_api.slack_bot.slack_utils import process_inputs
app = Flask(__name__)

SLACK_WEBHOOK_SECRET = "zWW96YZRUbNnT8mCTSND0GkD"
url_name = "https://ec2-34-229-107-43.compute-1.amazonaws.com"
token = "xoxb-295775362084-818546181940-EA1hKU4anGOaHNrfBcopxSLD"
client = slack.WebClient(token=token)

@app.route('/downloads/<path:filepath>', methods=['GET', 'POST'])
def download(filepath):
    form, path = filepath.split("/")
    date = path.split("_")[-2]
    filepath = os.path.join(ANALYTICS_ROOT, "output_files", "custom_statistics", "integration_log", form, date, path)
    return send_file(filepath, as_attachment=True)

@app.route('/slack', methods=['POST'])
def hello_world():
    print(request.form)
    return Response(), 200

@app.route('/get_2018_conversations', methods=['POST'])
def get_conversations_2018():
    if request.form.get('token') == SLACK_WEBHOOK_SECRET:
        error_message = "Get conversation failed: "
        try:
            inputs = process_inputs(request.form.get("text"), keys=["date", "module", "rating"], year=2018)
            initial_comment = "Result for user - {}, get_conversations 2018 :  query - {}".format(request.form.get("user_name"), request.form.get("text"))
            e = []
            for k in inputs:
                msg = inputs[k] # If msg is not None, send message
                if msg is not None:
                    e.append(msg)
            if len(e) > 0:
                error_message += "\n \t".join(e)
                raise Exception(error_message)

            parameters = list(inputs.keys())
            proc = subprocess.Popen(["python3.6", "process_get_past_conversation.py",\
                 "--initial_comment={}".format(initial_comment),\
                "--channels={}".format(request.form.get("user_id")), *parameters],\
                    shell=False, stdin=None, stdout=None, stderr=None, close_fds=True)
            return Response(), 200
        except Exception as e:
            return Response(error_message), 200
    return Response(), 200


@app.route('/get_2019_conversations', methods=['POST'])
def get_conversations():
    """Get the last few utterances for specific module
    """
    if request.form.get('token') == SLACK_WEBHOOK_SECRET:
        error_message = "Get conversation failed: "
        try:
            inputs = process_inputs(request.form.get("text"), keys=["date", "module", "rating", "force", "conv_id", "data_table", "updated", "ratings_only", "user_id"], year=2019)
            initial_comment = "Result for user - {}, get_conversations 2019 query - {}".format(request.form.get("user_name"), request.form.get("text"))
            e = []
            for k in inputs:
                msg = inputs[k] # If msg is not None, send message
                if msg is not None:
                    e.append(msg)
            if len(e) > 0:
                error_message += "\n \t".join(e)
                raise Exception(error_message)

            parameters = list(inputs.keys())
            proc = subprocess.Popen(["python3.6", "process_get_conversations.py", "--initial_comment={}".format(initial_comment), "--channels={}".format(request.form.get("user_id")), *parameters],shell=False, stdin=None, stdout=None, stderr=None, close_fds=True)
            return Response(), 200
        except Exception as e:
            print(e)
            return Response(error_message), 200
    return Response(), 200

@app.route('/get_error_logs', methods=['POST'])
def syscrash_log():
    """Get chatlog with system crash
        - module
    """
    if request.form.get('token') == SLACK_WEBHOOK_SECRET:
        error_message = "Get conversation failed: "
        try:
            inputs = process_inputs(request.form.get("text"), keys=["module", "date"], year=2019)
            initial_comment = "Result for user - {}, query - {}".format(request.form.get("user_name"), request.form.get("text"))
            e = []
            for k in inputs:
                msg = inputs[k] # If msg is not None, send message
                if msg is not None:
                    e.append(msg)
            if len(e) > 0:
                error_message += "\n \t".join(e)
                raise Exception(error_message)

            parameters = list(inputs.keys())
            proc = subprocess.Popen(["python3.6", "process_get_error.py", "--initial_comment={}".format(initial_comment), "--channels={}".format(request.form.get("user_id")), *parameters],shell=False, stdin=None, stdout=None, stderr=None, close_fds=True)
            return Response(), 200
        except Exception as e:
            return Response(error_message), 200
    return Response(), 200

@app.route('/refresh', methods=['POST'])
def refresh():
    if request.form.get('token') == SLACK_WEBHOOK_SECRET:
        error_message = "Refresh Data Failed. "
        try:
            inputs = process_inputs(request.form.get("text"), keys=["force"], year=2019)
            print(inputs)
            e = []
            for k in inputs:
                msg = inputs[k] # If msg is not None, send message
                if msg is not None:
                    e.append(msg)
            if len(e) > 0:
                error_message += "\n \t".join(e)
                raise Exception(error_message)

            parameters = list(inputs.keys())
            proc = subprocess.Popen(["python3.6", "process_refresh_visualization.py",  *parameters],shell=False, stdin=None, stdout=None, stderr=None, close_fds=True)
            return Response(), 200
        except Exception as e:
            return Response(error_message), 200
    return Response(), 200

@app.route('/get_unittests', methods=['POST'])
def get_unittests():
    """Get chatlog with system crash
        - module
    """
    if request.form.get('token') == SLACK_WEBHOOK_SECRET:
        error_message = "Get conversation failed: "
        try:
            inputs = process_inputs(request.form.get("text"), keys=["module", "conv_id", "date"], year=2019)
            print(inputs)
            initial_comment = "Result for user - {}, query - {}".format(request.form.get("user_name"), request.form.get("text"))
            e = []
            for k in inputs:
                msg = inputs[k] # If msg is not None, send message
                if msg is not None:
                    e.append(msg)
            if len(e) > 0:
                error_message += "\n \t".join(e)
                raise Exception(error_message)

            parameters = list(inputs.keys())
            proc = subprocess.Popen(["python3.6", "process_get_unittest.py", "--initial_comment={}".format(initial_comment), "--channels={}".format(request.form.get("user_id")), *parameters],shell=False, stdin=None, stdout=None, stderr=None, close_fds=True)
            return Response(), 200
        except Exception as e:
            return Response(error_message), 200
    return Response(), 200

@app.route('/get_user_prev_conv', methods=['POST'])
def get_user_prev_conv():
    """Get chatlog with system crash
        - module
    """
    if request.form.get('token') == SLACK_WEBHOOK_SECRET:
        error_message = "Get conversation failed: "
        try:
            inputs = process_inputs(request.form.get("text"), keys=["conv_id", "date", "user_id", "force"], year=2019)
            initial_comment = "Result for user - {}, query - {}".format(request.form.get("user_name"), request.form.get("text"))
            e = []
            for k in inputs:
                msg = inputs[k] # If msg is not None, send message
                if msg is not None:
                    e.append(msg)
            if len(e) > 0:
                error_message += "\n \t".join(e)
                raise Exception(error_message)

            parameters = list(inputs.keys())
            proc = subprocess.Popen(["python3.6", "process_getprevconv.py", "--initial_comment={}".format(initial_comment), "--channels={}".format(request.form.get("user_id")), *parameters],shell=False, stdin=None, stdout=None, stderr=None, close_fds=True)
            return Response(), 200
        except Exception as e:
            return Response(error_message), 200
    return Response(), 200

@app.route('/get_integration_test', methods=['POST'])
def get_integration_test():
    """Get chatlog with system crash
        - module
    """
    if request.form.get('token') == SLACK_WEBHOOK_SECRET:
        error_message = "Get conversation failed: "
        try:
            inputs = process_inputs(request.form.get("text"), keys=["conv_id", "force"], year=2019)
            initial_comment = "Result for user - {}, query - {}".format(request.form.get("user_name"), request.form.get("text"))
            e = []
            for k in inputs:
                msg = inputs[k] # If msg is not None, send message
                if msg is not None:
                    e.append(msg)
            if len(e) > 0:
                error_message += "\n \t".join(e)
                raise Exception(error_message)

            parameters = list(inputs.keys())
            proc = subprocess.Popen(["python3.6", "process_get_integration_test.py", "--initial_comment={}".format(initial_comment), "--channels={}".format(request.form.get("user_id")), *parameters],shell=False, stdin=None, stdout=None, stderr=None, close_fds=True)
            return Response(), 200
        except Exception as e:
            return Response(error_message), 200
    return Response(), 200

@app.route('/', methods=['GET', 'POST'])
def test():
    print(request.form)
    return Response('It works!')

@app.route('/daily_rating', methods=['GET', 'POST'])
def visualization():
    return render_template('daily_topic_rating.html')

@app.route('/chitchat_rating', methods=['GET', 'POST'])
def chitchat_rating():
    return render_template('daily_chitchat_rating.html')

@app.route('/chitchat_keywords', methods=['GET', 'POST'])
def chitchat_keywords():
    return render_template('daily_chitchat_keywords.html')

@app.after_request
def apply_caching(response):
    response.headers['Cache-Control'] = 'no-cache, no-store, must-revalidate'
    return response

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5002)
