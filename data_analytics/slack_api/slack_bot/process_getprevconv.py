"""Give a search range and conversation id, get user's previous conversation
"""
import argparse
from data_analytics.athena_metadata import get_today
from data_analytics.data_utils.daily_conversation_generator import main as daily_conv_generator
import pandas as pd
from data_analytics.slack_api.slack_bot.slack_utils import send_csv, send_prelim_slack_message
from data_analytics.data_utils.utils import get_conversations_by_id, combine_df
import os

def process_get_prev_utterance(args):
    send_prelim_slack_message("Command {} Received, processing csv data..".format(args.initial_comment), args.channels)

    # If conv_id is not None:  Then we want to get the user_id with conv_id

    # Then, we will get the conversation with user_id

    message, success_joins, failed_joins = daily_conv_generator(args)
    user_id = args.user_id
    if args.conv_id is not None and user_id is None:
        conversation_id = args.conv_id
        for files in success_joins:
            rated_file, unrated_file = files
            rated_result = get_conversations_by_id(rated_file, [conversation_id], rated=True)
            if len(rated_result) > 0:
                user_id = rated_result[0]["user_id"].iloc[0]
                send_prelim_slack_message("Found User ID {0} for conversation_id {1}".format(user_id, conversation_id), args.channels)
                break
            unrated_result = get_conversations_by_id(unrated_file, [conversation_id], rated=False)
            if len(unrated_result) > 0:
                user_id = unrated_result[0]["user_id"].iloc[0]
                send_prelim_slack_message("Found User ID {0} for conversation_id {1}".format(user_id, conversation_id),  args.channels)
                break

        if user_id is None:
            send_prelim_slack_message("User ID for conversation_id {0} not found".format(conversation_id),  args.channels)
            return
    
    
    if user_id is None:
        send_prelim_slack_message("User ID is None, No Query Performed")
    else:
        args.user_id = user_id
        args.conv_id = None
        message, success_joins, failed_joins = daily_conv_generator(args)
        send_prelim_slack_message("File Retrieval Complete, {0} files found".format(len(success_joins)), args.channels)
        new_filename = "conv_by_user_id-{0}_date-{1}-{2}.csv".format(user_id[:6], args.start, args.end)
        result = []
        for files in success_joins:
            rating_files, no_rating_files = files
            ratings_df = pd.read_csv(rating_files)
            if ratings_df.shape[0] > 0:
                result.append(ratings_df)
            no_ratings_df = pd.read_csv(no_rating_files)
            if no_ratings_df.shape[0] > 0:
                result.append(no_ratings_df)
            if os.path.exists(rating_files):
                os.remove(rating_files)
            if os.path.exists(no_rating_files):
                os.remove(no_rating_files)
        if len(result) == 0:
            send_prelim_slack_message("No conversation found for user_id - {}".format(user_id), args.channels)
        else:
            total_df = combine_df(result)
            total_df.to_csv(new_filename)
            send_prelim_slack_message("Sending file - {}".format(new_filename), args.channels)

        send_csv(new_filename, args.initial_comment, args.channels)
        
    return

def add_args(parser):
    today = get_today(True)
    parser.add_argument('--start', '-s', default=today,
                        help='timerange from (YYYY-mm-dd)')
    parser.add_argument('--end', '-e', default=today,
                        help='timerange to (YYYY-mm-dd)')
    parser.add_argument('--data_table', '-d', default="state_table_prod", help="data_table")
    parser.add_argument('--ratings', '-r', default=None, help="Filter by Ratings, default to 5.0")
    parser.add_argument('--module', '-m', default=None, help="Filter by Modules")
    parser.add_argument('--forced', '-f', default=False, help="Force build joined table")
    parser.add_argument('--conv_id', '-n', default=None, help="Conversation ID")
    parser.add_argument('--updated', '-u', default=False, help="most_updated_conversations")
    parser.add_argument('--user_id', "-i", default=None, help="user_id")
    parser.add_argument('--ratings_only', '-v', default=False),

    parser.add_argument('--initial_comment', '-c', default="",
                        help='initial comment to send')

    parser.add_argument('--channels', '-l', default="#debug",
                        help='channel to send the message')

    return



if __name__ == "__main__":
    """ This is executed when run from the command line """
    # debug_mode()
    parser = argparse.ArgumentParser()

    add_args(parser)

    args = parser.parse_args()
    process_get_prev_utterance(args)