"""Given a get module command.  Get all the conversation into a csv with the module.
"""
import datetime
from datetime import datetime as dt
import os
import argparse
import slack
from data_analytics.data_utils.past_data_generator import main
from data_analytics.slack_api.slack_bot.slack_utils import send_csv, send_prelim_slack_message


token = "xoxb-295775362084-818546181940-EA1hKU4anGOaHNrfBcopxSLD"
client = slack.WebClient(token=token)

def process_get_module(args):
    send_prelim_slack_message("Generating Data for 2018 Conversations...", args.channels)
    module = args.module
    total_conversation, output_file = main(args)
    # Send a message on total conversation for 2018
    if output_file is None:
        return send_prelim_slack_message("No data for {} generated".format(module), args.channels)
    send_prelim_slack_message("{0} conversations generated for {1} generated".format(str(total_conversation), module), args.channels)
    send_csv(output_file, args.initial_comment, args.channels)


def add_args(parser):
    parser.add_argument("--module", '-m', default=None, help="MODULE NAME")
    parser.add_argument("--ratings", '-r', default=None, help="Ratings")

    parser.add_argument('--initial_comment', '-c', default="",
                        help='initial comment to send')

    parser.add_argument('--channels', '-l', default="#debug",
                        help='channel to send the message')
    parser.add_argument('--refresh', '-g', default="False",
                        help='reset data if true')
    parser.add_argument('--start', '-s', default="2019-01-01",
                        help='timerange from (yyyy-mm-dd)')
    parser.add_argument('--end', '-e', default="2019-01-01",
                        help='timerange to (yyyy-mm-dd)')
    parser.add_argument('--forced', '-f', default=False, help="Force build joined table")
    parser.add_argument('--conv_id', '-n', default=None, help="Conversation ID")

    return



if __name__ == "__main__":
    """ This is executed when run from the command line """
    # debug_mode()
    parser = argparse.ArgumentParser()

    add_args(parser)

    args = parser.parse_args()
    process_get_module(args)