"""Common Utils for Slack Bots
"""
import os
import slack
import pandas as pd
from datetime import datetime as dt
from data_analytics.athena_metadata import get_today, state_tables
from data_analytics.analytics_metadata import _2019_START_DATE, _2018_START_DATE,\
    _2018_END_DATE, MODULE_2018, MODULE_2019, get_raw_filepath


SLACK_WEBHOOK_SECRET = "zWW96YZRUbNnT8mCTSND0GkD"
token = "xoxb-295775362084-818546181940-EA1hKU4anGOaHNrfBcopxSLD"
client = slack.WebClient(token=token)
SPLIT_LIMIT = 10000


def split_csv(csv_file, channels):
    df = pd.read_csv(csv_file)
    cnt = 1
    while df.shape[0] > SPLIT_LIMIT:
        send_prelim_slack_message("Splitting File..., sending {0} part".format(cnt), channels)
        send_df = df.iloc[:SPLIT_LIMIT, :]
        df = df.iloc[SPLIT_LIMIT:, :].reset_index(drop=True)
        new_file_path = csv_file.replace(".csv", "--part{}.csv".format(cnt))
        send_df.to_csv(new_file_path)
        cnt += 1
        yield new_file_path
    
    if cnt > 1:
        new_file_path = csv_file.replace(".csv", "--part{}--Final.csv".format(cnt))
    else:
        new_file_path = csv_file

    df.to_csv(new_file_path)
    yield new_file_path

def send_csv(csv_output, initial_comment, channels):
    """Send CSV File
    """
    try:
        if os.path.exists(csv_output):
            final_generator = split_csv(csv_output, channels)
            for result in final_generator:
                client.files_upload(channels=channels,\
                        initial_comment=initial_comment, \
                        title=result.split("/")[-1].replace(".csv", ""), file=open(result, "r"))
                os.remove(result)
            os.remove(csv_output)   
    except Exception as e:
        return 

def send_file(file_output, initial_comment, channels):
    if os.path.exists(file_output):
        if pd.read_csv(file_output).shape[0] == 0:
            send_prelim_slack_message("No conversations found for {}".format(file_output),channels=channels)
        else:
            client.files_upload(channels=channels,\
                                initial_comment=initial_comment, \
                                title=file_output.split("/")[-1], file=open(file_output, "r"))
        os.remove(file_output)

def process_inputs(inputs, keys, year):
    """Given raw inputs from slackbot, process each parts.
    """
    list_of_inputs = inputs.split(" ")
    result = {}

    for i in list_of_inputs:
        op = None
        if "=" not in i:
            if "<" in i:
                kv = i.split("<")
                op = "<"
            elif ">" in i:
                kv = i.split(">")
                op = ">"
            else:
                continue
        else:
            if "<=" in i:
                kv = i.split("<=")
                op = "<="
            elif ">=" in i:
                kv = i.split(">=")
                op = ">="
            else:
                kv = i.split("=")
                op = "="
        if len(kv) != 2:
            continue
        key, value = kv
        if key in keys:
            # Process value by key
            if key == "date":
                result.update(process_date(value, year))
            if key == "module":
                result.update(process_module(value, year))
            if key == "rating":
                result.update(process_rating(value, year, op))
            if key == "ratings_only":
                result.update({"--ratings_only={0}".format(value):None})
            if key == "force":
                result.update({"--forced={0}".format(value):None})
            if key == "conv_id":
                result.update({"--conv_id={0}".format(value):None})
            if key == "data_table":
                result.update(process_data_table(value, year))
            if key == "updated":
                result.update({"--updated={0}".format(value):None})
            if key == "user_id":
                result.update({"--user_id={0}".format(value):None})
        if key not in keys:
            result.update({"general":"Unrecognized Command " + keys})
            
            
    return result

def process_date(value, year):
    """Make sure the value is within the year.  Or provide a
       Error Message
    """

    try:
        if "_" in value:
            time_tuple = value.split("_")
            if len(time_tuple) == 2:
                start_time, end_time = time_tuple
        else:
            start_time, end_time = value, value

        start_value = get_today(date=start_time)
        end_value = get_today(date=end_time)
    except Exception as e:
        print(e)
        return {"date": "Invalid date format.  {} should follow YYYY-MM-DD.".format(value)}

    if year == 2018:
        start_key = "--start={0}".format(start_time)
        end_key = "--end={0}".format(end_time)
        if end_value > _2018_START_DATE and start_value < _2018_END_DATE:
            return {start_key: None, end_key: None}
        else:
            return {start_key: "Invalid date range.  Date should be ranging from {0} to {1}".format(_2018_START_DATE.strftime("%Y-%m-%d"), _2018_END_DATE.strftime("%Y-%m-%d"))}

    elif year == 2019:
        start_key = "--start={0}".format(start_time)
        end_key = "--end={0}".format(end_time)
        if start_value > _2019_START_DATE and end_value < get_today():
            return {start_key: None, end_key: None}
        else:
            return {start_key: "Invalid date range.  Date should be ranging from {0} to {1}".format(_2019_START_DATE, get_today().strftime('%Y-%m-%d'))}

    error_message = "Year parameter only can be in 2018 or 2019"
    return {"date": error_message}

def process_module(value, year):
    """Make sure the module is correct within the available modules
        TODO
    """
    if year == 2018:
        if value in MODULE_2018:
            return {"--module={0}".format(value): None} # TODO Precheck existing modules
        else:
            return {"module": "Module not correct.  Available Modules:  {}".format(MODULE_2018)}
    if year == 2019:
        if value in MODULE_2019:
            return {"--module={0}".format(value): None}
        else:
            return {"module": "Module not correct.  Available Modules:  {}".format(MODULE_2019)}



def process_rating(value, year, op):
    try:
        value = float(value)
        return {"--ratings={0}_{1}".format(op, value): None}
    except:
        return {"rating": "Rating value {0} is not valid".format(value)}

def process_data_table(value, year):
    if value not in state_tables:
        return {"data_table": "Tablename doesn't exist, pick from \n {}".format(state_tables.keys())}

    return {"--data_table={0}".format(value): None}

def send_prelim_slack_message(msg, channels):
    data = {
        'text': msg,
        'channel': channels,            
    }

    response = client.api_call('chat.postMessage', json=data)
    