"""Process the slack command, get_conversations for 2019 data
"""

import os
import argparse
import slack
from data_analytics.athena_metadata import get_today
from data_analytics.data_utils.utils import get_conversations_by_id, combine_df
from data_analytics.data_utils.daily_conversation_generator import main as daily_conv_generator
from data_analytics.slack_api.slack_bot.slack_utils import send_csv, send_prelim_slack_message
from data_analytics.analytics_metadata import RESULT_DISPLAY_DIR, _2019_START_DATE
from data_analytics.libs.utils import read_raw_csv
from data_analytics.libs.error_analytics import main as error_log_generator
import pandas as pd

def combine_common_csv(csv_file, reference_common):
    df = read_raw_csv(csv_file)
    new_df = pd.DataFrame()
    if df is None:
        return new_df
    return df[df["conversation_id"].isin(reference_common)]

def process_get_error_logs(args):
    send_prelim_slack_message("Retrieving 2019 Data For Error Log Checking...", args.channels)
    message, success_joins, failed_joins = daily_conv_generator(args)
    send_prelim_slack_message(message, args.channels)
    total_dfs = []
    module = args.module if args.module is not None else "ALL Modules"
    total_errors = 0
    for files in success_joins:
        rating_files, no_rating_files = files
        send_prelim_slack_message("Sending file - {}".format(rating_files), args.channels)
        result_json = error_log_generator({"rating":rating_files, "no_rating":no_rating_files}, None)
       
        if args.module is None:
            conv_id_ratings = [conv_id for topic in result_json for conv_id in result_json[topic].get("conv_id_rating", [])]
            conv_id_noratings = [conv_id for topic in result_json for conv_id in result_json[topic].get("conv_id_no_rating", [])]
            total_errors += sum([result_json[topic].get("num_turns_rating", 0) for topic in result_json])
            total_errors += sum([result_json[topic].get("num_turns_no_rating", 0) for topic in result_json])
        else:
            if args.module in result_json:
                conv_id_ratings = [conv_id for conv_id in result_json[args.module].get("conv_id_rating", [])]
                conv_id_noratings = [conv_id for conv_id in result_json[args.module].get("conv_id_no_rating", [])]
                total_errors += result_json[args.module].get("num_turns_rating", 0)
                total_errors += result_json[args.module].get("num_turns_no_rating", 0)
            else:
                conv_id_ratings = []
                conv_id_noratings = []
        if len(conv_id_ratings) > 0:
            rated_dfs = get_conversations_by_id(rating_files, conv_id_ratings, True)
            total_dfs.extend(rated_dfs)
        if len(conv_id_noratings) > 0:
            unrated_dfs = get_conversations_by_id(no_rating_files, conv_id_noratings, False)
            total_dfs.extend(unrated_dfs)
    combined_df = combine_df(total_dfs)
    if combined_df.shape[0] == 0:
        send_prelim_slack_message("No data found for module {0}".format(module), args.channels)
    else:
        send_prelim_slack_message("Total {1} response errors found for {0}\n\n".format(module, total_errors), args.channels)

    result_file = "error_csv_file_for_{0}_{1}-{2}.csv".format(module, args.start, args.end)
    combined_df.to_csv(result_file)
    send_csv(result_file, args.initial_comment, args.channels)




def add_args(parser):
    today = get_today(True)
    parser.add_argument('--start', '-s', default=today,
                        help='timerange from (YYYY-mm-dd)')
    parser.add_argument('--end', '-e', default=today,
                        help='timerange to (YYYY-mm-dd)')
    parser.add_argument('--data_table', '-d', default="state_table_prod", help="data_table")
    parser.add_argument('--ratings', '-r', default=None, help="Filter by Ratings, default to 5.0")
    parser.add_argument('--module', '-m', default=None, help="Filter by Modules")
    parser.add_argument('--forced', '-f', default=False, help="Force build joined table")
    parser.add_argument('--conv_id', '-n', default=None, help="Conversation ID")
    parser.add_argument('--updated', '-u', default=False, help="Conversation ID")
    parser.add_argument('--ratings_only', '-v', default=True),
    parser.add_argument('--user_id', '-i', default=None),
    parser.add_argument('--initial_comment', '-c', default="",
                        help='initial comment to send')

    parser.add_argument('--channels', '-l', default="#debug",
                        help='channel to send the message')

    return



if __name__ == "__main__":
    """ This is executed when run from the command line """
    # debug_mode()
    parser = argparse.ArgumentParser()

    add_args(parser)

    args = parser.parse_args()
    if args.conv_id is not None:
        args.start = _2019_START_DATE
    process_get_error_logs(args)