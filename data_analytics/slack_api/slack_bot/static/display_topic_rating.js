var dom = document.getElementById("container");
var myChart = echarts.init(dom);
var app = {};
option = null;
myChart.showLoading();
$.get('../static/result_display_data/display_rating.json', function (data) {
    myChart.hideLoading();
    // var ratings = data.series.map(function (item) {
    //     return item.daily_rating;
    // });
    var selected = {};
    for (var v in data.modules) {
        selected[data.modules[v]] = false;
    }
    console.log(selected);
    myChart.setOption(option = {
        title: {
            text: 'Daily Rating per Modules',
            bottom: '0%',
            left: '50%',
            padding: [15, 5, 5, 5]
        },
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: data.modules,
            selected: selected,
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        toolbox: {
            feature: {
                saveAsImage: {}
            }
        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: data.date
        },
        yAxis: {
            type: 'value'
        },
        series: data.series.map(function(item) {
            return {
                name: item.name,
                type: 'line',
                data: item.daily_rating,
                selected: false,
                smooth: true,
                hoverAnimation: false,
                symbolSize: 6,
                showSymbol: false
            }
        })
    });

});
if (option && typeof option === "object") {
    myChart.setOption(option, true);
}