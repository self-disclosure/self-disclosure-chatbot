import os
from datetime import datetime as dt

from data_analytics.athena_metadata import ANALYTICS_ROOT, today_string, OUTPUT_ROOT, get_today

#############################################################################################
################################### Output Files ############################################
#############################################################################################

# OUTPUT_FEEDBACK_FILE =os.path.join(ANALYTICS_ROOT, "output_files", "daily_statistics", "daily_feedbacks-" + today_string + ".csv")

OUTPUT_JOINED_FILE =os.path.join(ANALYTICS_ROOT, "output_files", "daily_statistics", "daily_statistics-" + today_string + ".csv")
OUTPUT_JOINED_FILE_NO =os.path.join(ANALYTICS_ROOT, "output_files", "daily_statistics", "daily_statistics-" + today_string + "-no-rating.csv")

CACHED_DIR = os.path.join(OUTPUT_ROOT, "daily_data_cache")

UNIT_TEST_LOCATION = os.path.join(OUTPUT_ROOT, "unittest_raw")
if not os.path.exists(UNIT_TEST_LOCATION):
    os.mkdir(UNIT_TEST_LOCATION)

if not os.path.exists(CACHED_DIR):
    os.mkdir(CACHED_DIR)


def get_cached_state_tables(table_name):
    cached_table_dir = os.path.join(CACHED_DIR, table_name)
    if not os.path.exists(cached_table_dir):
        os.mkdir(cached_table_dir)
    return cached_table_dir

def get_daily_joined_folder():
    returns_paths = os.path.join(CACHED_DIR, "daily_conversations")
    if not os.path.exists(returns_paths):
        os.mkdir(returns_paths)
    return returns_paths

def get_raw_filepath(date=today_string, include_tail=False, include_no_rating=False):
    """Get the filename of the statistics file for one day"""
    joined_folder = get_daily_joined_folder()
    format_string = "daily_statistics_{}_.csv"
    if include_tail:
        format_string = format_string.replace("_.csv", "_conversations_.csv")
    if include_no_rating:
        format_string = format_string.replace("_.csv", "_norating_.csv")
    return os.path.join(joined_folder, format_string.format(date))

HISTORY_DATA_CACHE = os.path.join(OUTPUT_ROOT, "history_data_cache")
if not os.path.exists(HISTORY_DATA_CACHE):
    os.mkdir(HISTORY_DATA_CACHE)
#############################################################################################
################################### Analytics Path ##########################################
#############################################################################################
RESULT_DISPLAY_DIR = os.path.join(ANALYTICS_ROOT, "result_display_data")

if not os.path.exists(RESULT_DISPLAY_DIR):
    os.mkdir(RESULT_DISPLAY_DIR)
HISTORY_DATA = os.path.join(RESULT_DISPLAY_DIR, "display_topic_rating.json")
DAILY_DATA = os.path.join(RESULT_DISPLAY_DIR, "display_daily_topic_rating.json")

HISTORY_AB_TEST = os.path.join(RESULT_DISPLAY_DIR, "display_abtest_rating.json")
DAILY_AB_TEST = os.path.join(RESULT_DISPLAY_DIR, "display_daily_abtest_rating.json")

DAILY_COUNT = os.path.join(RESULT_DISPLAY_DIR, "display_daily_count.json")
DAILY_ERROR = os.path.join(RESULT_DISPLAY_DIR, "display_daily_error.json")
DAILY_RESP_TYPE = os.path.join(RESULT_DISPLAY_DIR, "display_daily_resp_type.json")

#############################################################################################
################################### Metadata Path ##########################################
#############################################################################################

TOPIC_TO_DATE_2018_FILE = os.path.join(ANALYTICS_ROOT, "topic_to_date.json")

#############################################################################################
################################### Default Data ############################################
#############################################################################################

_2018_DATA_KEY = ['index', 'session_id', 'creation_date_time', 'candidate_response',
       'conversation_id', 'user_id', 'a_b_test', 'text', 'response',
       'selected_modules', 'last_custom_intents', 'topic_class', 'sentiment',
       'intent_classify', 'npkknowledge', 'knowledge', 'noun_phrase', 'topic',
       'ner', 'slots', 'request_type', 'intent', 'asr', 'rating', 'day']

_2019_START_DATE = get_today(date="2019-10-29")
_2018_START_DATE = get_today(date="2018-06-07")
_2018_END_DATE = get_today(date="2019-03-19")

MODULE_2018 = ['LAUNCHGREETING', 'MOVIECHAT', 'TRAVELCHAT', 'ANIMALCHAT', 'FOODCHAT', 'SOCIAL', 'TERMINATE', 'SPORT', 'RETRIEVAL', 'BOOKCHAT', 'PLAY_GAME', 'CLARIFICATION', 'NEWS', 'MUSICCHAT', 'GAMECHAT', 'SAY_FUNNY', 'HOLIDAYCHAT', 'TECHSCIENCECHAT', 'WEATHER', 'FASHIONCHAT', 'PLAY_MUSIC', 'REQ_TASK', 'ADJUSTSPEAK', 'ELECTIONCHAT', 'REQ_TIME', 'STORYFUNFACT', 'PHATIC', 'PSYPHICHAT', 'SAY_COMFORT', 'RECOMMENDATIONTCHAT', 'SAY_THANKS', 'WHATNAME', 'EVI', 'TRANSITION', 'CHANGETOPIC', 'REQ_TOPIC', 'SAY_BIO', 'BACKSTORY', 'REQ_EASTEREGG', 'WHEREFROM', 'CONTROVERSIALOPION', 'REQ_MORE', 'HEALTH', 'TODAYNEWSFACTS']

MODULE_2019 =  ["FOODCHAT","MOVIECHAT", "BOOKCHAT", "FASHIONCHAT", "TECHSCIENCECHAT", "TRAVELCHAT", "GAMECHAT", "SPORT", "MUSICCHAT", "HOLIDAYCHAT", "NEWS", "ANIMALCHAT"]
