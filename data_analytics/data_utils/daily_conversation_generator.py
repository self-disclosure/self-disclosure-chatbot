"""New Version of Daily Rating Generator.  Used to generate daily average.

Supports:
1. Get rated conversation for a particular date [date-range] or [day]
2. Get unrated conversation for a particular date
3. Get statistics from beta table
    parser = argparse.ArgumentParser()

    add_args(parser)

    args = parser.parse_args()
    process_get_rating(args)
"""
import json
from data_analytics.get_from_athena import get_2019_data, get_ratings
from data_analytics.athena_metadata import OUTPUT_ROOT, RATING_PATH, state_tables, get_today
from data_analytics.data_utils.utils import get_daily_range, reformat_unjoined_tables
from data_analytics.analytics_metadata import get_cached_state_tables, CACHED_DIR, get_daily_joined_folder, get_raw_filepath
import datetime
from datetime import datetime as dt
import os
import argparse
import pandas as pd
from data_analytics.data_utils.utils import move_column_to_front, join_daily_rating_table, get_list_of_filters, filter_by_date


def retrieve_state_table(actual_date, forced=False, table_name="state_table_prod"):
    """Retrieve the Data from State Table

    Parameters:
      - cached_dir: location of the cache file for state table data
      - start_date:  datetime object of the starting date
      - end_date: datetime object of the ending date

    Return:
      - file_names: A list of filenames downloaded successfully
      - unable_to_retrieve:  A list of filenames unable to retrieve
    """

    # Create the file name
    date = actual_date - datetime.timedelta(days=1) 
    curr_date = actual_date + datetime.timedelta(days=1)
    cached_dir = get_cached_state_tables(table_name)
    file_name = "raw_data_{0}.csv".format(actual_date.strftime("%Y-%m-%d"))
    file_path = os.path.join(cached_dir, file_name)
    if os.path.exists(file_path) and not forced:
        return file_path
    else:
        start_string = date.strftime("%Y-%m-%d")
        curr_string = curr_date.strftime("%Y-%m-%d")
        success = get_2019_data(start_string, curr_string, table_name=table_name, output_file=file_path)
        if success:
            return file_path
        else:
            return None

def process_retrieved_data(start_date, end_date, raw_filters, conv_id_search=False, user_id=None, forced=False, table_name="state_table_prod", updated=False, clean_fields=True):
    if not os.path.exists(RATING_PATH):
        result = get_ratings()
    returns_paths = get_daily_joined_folder()

    success_joins = []
    failed_joins = []
    # Return a list of daily-conversation-csv-files
    total_rated = 0
    total_not_rated = 0
    curr_date = end_date
    joined_folder = get_daily_joined_folder()
    while curr_date >= start_date:
        filter_string = [l[:6] for l in raw_filters if l != "date"]
        _f = get_raw_filepath(date=curr_date.strftime("%Y-%m-%d"))
        if len(filter_string) > 0:
            _f = _f.replace("_.csv", "_"+"_".join(filter_string) + "_.csv")
        raw_filters["date"] = lambda df: filter_by_date(df, curr_date)
    
        if table_name != "state_table_prod":
            _f = _f.replace("_.csv", "_{}.csv".format(table_name))
        raw_table_file = retrieve_state_table(curr_date, forced=forced, table_name=table_name)
        conversation_path = _f.replace("_.csv", "_conversations_.csv")
        no_rating_path = _f.replace("_.csv", "_conversations_norating_.csv")
        output_files = [conversation_path, no_rating_path]
        if raw_table_file is None:
            failed_joins.append(output_files)
        else:
            if table_name == "state_table_prod":
                total_j, total_n_j = join_daily_rating_table(curr_date, raw_table_file, RATING_PATH, output_files, raw_filters, forced, updated=updated, clean_fields=clean_fields)

            else:
                if user_id is not None:
                    output_files[1] = no_rating_path.replace(".csv", "_{}.csv".format(user_id[:6]))
                total_j, total_n_j = reformat_unjoined_tables(raw_table_file, output_files, forced, user_id, updated=updated)

            total_rated += total_j
            total_not_rated += total_n_j
            if conv_id_search and total_j > 0:
                return [output_files], [], total_j, total_n_j
            if total_j + total_n_j > 0:
                success_joins.append(output_files)
            else:
                failed_joins.append(output_files)

        curr_date -= datetime.timedelta(days=1)
    if conv_id_search:
        return [], [], 0, 0
    return success_joins, failed_joins, total_rated, total_not_rated

def main(args, clean_fields=True):
    _, start_date = get_daily_range(args.start)
    _, end_date = get_daily_range(args.end, updated=args.updated)
    table_name = args.data_table

    
    if table_name not in state_tables:
        message = "State table file retrieval failed for {0}, {1}-{2}".format(table_name, start_date, end_date)
        success_joins, failed_joins = [], []
    else:
        conv_id_search = args.conv_id is not None
        module = args.module if args.module is not None else " ALL Modules "
        ratings = args.ratings if args.ratings is not None else " All Ratings "
        raw_filters = get_list_of_filters(args)

        success_joins, failed_joins, total_rated, total_not_rated = process_retrieved_data(start_date, end_date, raw_filters, conv_id_search, args.user_id, args.forced, table_name, args.updated, clean_fields=clean_fields)
        if total_not_rated > 0 or total_rated > 0:
            msg1 = "Total {} rated conversations ".format(total_rated)
            msg2 = "{} unrated conversations ".format(total_not_rated)
            message = "{0} {1} newly generated for {2} {3} {4} {5}".format(msg1, msg2, table_name, args.start, module, ratings)
        else:
            message = "No conversations found.  Please use force=True to recreate the state table"
    return message, success_joins, failed_joins

def add_args(parser):
    today = get_today(True)
    parser.add_argument('--start', '-s', default=today,
                        help='timerange from (YYYY-mm-dd)')
    parser.add_argument('--end', '-e', default=today,
                        help='timerange to (YYYY-mm-dd)')
    parser.add_argument('--data_table', '-d', default="state_table_prod", help="data_table")
    parser.add_argument('--ratings', '-r', default=None, help="Filter by Ratings, default to =_5.0")
    parser.add_argument('--module', '-m', default=None, help="Filter by Modules")
    parser.add_argument('--forced', '-f', default=False, help="Force build joined table")
    parser.add_argument('--conv_id', '-n', default=None, help="Conversation ID")
    parser.add_argument('--updated', '-u', default=False, help="Most updated conversations")
    parser.add_argument('--user_id', '-i', default=None, help="User ID Filter, beta table only")

    return

def imported_run():
    parser = argparse.ArgumentParser()
    add_args(parser)

    args = parser.parse_args()
    args.forced=True
    msg, _, _ = main(args)

if __name__ == "__main__":
    """ This is executed when run from the command line """
    # debug_mode()
    imported_run()