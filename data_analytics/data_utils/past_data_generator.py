"""Generate Statistics for custom defined date, dateranges, or ratings
"""

import argparse
import os
import shutil

from data_analytics.analytics_metadata import ANALYTICS_ROOT, TOPIC_TO_DATE_2018_FILE, \
     _2018_DATA_KEY, HISTORY_DATA_CACHE
from data_analytics.athena_metadata import RATING_PATH, get_today
from datetime import datetime as dt
import pandas as pd
import datetime
import json
import boto3
from data_analytics.data_utils.utils import filter_by_ratings, move_column_to_front

s3 = boto3.resource('s3', region_name='us-east-1')

def add_args(parser):
    parser.add_argument("--module", '-m', default=None, help="MODULE NAME")
    parser.add_argument("--ratings", '-r', default=None, help="MODULE NAME")

    parser.add_argument('--start', '-s', default="2019-01-01",
                        help='timerange from (mm-dd)')
    parser.add_argument('--end', '-e', default="2019-03-30",
                        help='timerange to (mm-dd)')

    parser.add_argument('--forced', '-f', default=False, help="Force build joined table")
    parser.add_argument('--conv_id', '-n', default=None, help="Conversation ID")
    return

def compare(f_date, y_date):
    """ compare two dates"""

    if f_date.month > 3:
        f_date = f_date.replace(year=2018)
    else:
        f_date = f_date.replace(year=2019)
    f = f_date.strftime("%Y-%m-%d")
    if f < y_date:
        return -1
    elif f > y_date:
        return 1
    return 0

def get_2018_data(args, module, metadata, custom_dir, forced=False):
    start_date = args.start
    end_date =args.end
    dates = metadata["day"]
    
    conversation_ids = metadata["conversation_id"]
    if args.conv_id is not None:
        conversation_ids = [c for c in conversation_ids if c == args.conv_id]
    ratings = args.ratings if args.ratings is not None else "ALL"
    num_conversation = 0
    if start_date == end_date:
        output_statistcs = os.path.join(custom_dir, "{module}-{start_date}-{ratings}.csv".format(module=module, start_date=start_date, ratings=ratings))
    else:
        output_statistcs = os.path.join(custom_dir, "{module}-{start_date}_to_{end_date}-{ratings}.csv".format(module=module, start_date=start_date, end_date=end_date, ratings=ratings))
    if os.path.exists(output_statistcs) and not forced:
        return num_conversation, output_statistcs
    master_pd = []

    for f in dates:
        f_date = dt.strptime(f, "%m-%d")
        if compare(f_date, start_date) >= 0 and compare(f_date, end_date) <= 0:
            output_csv = os.path.join(custom_dir, f)
            try:
                if not os.path.exists(output_csv):
                    ret = s3.meta.client.download_file(
                                "gunrock-2018-dialog", os.path.join('full_data', f+".csv"), output_csv)
                df = pd.read_csv(output_csv, index_col=False, names=_2018_DATA_KEY)
                    # For each dialog, if it is in conversation_id then retrieve
                filtered_pd = df[df["conversation_id"].isin(conversation_ids)]
                master_pd.append(filtered_pd)
                
            except:
                print("Not downloaded " + f)

    df  = pd.concat(master_pd)
    if args.ratings is not None:
        cf = filter_by_ratings(df, args.ratings, True)
        df = df[df.conversation_id.isin(cf)]
    

    result_pd = df.sort_values(["conversation_id", "creation_date_time"])



    ordered_list = ["conversation_id", "session_id", "user_id", "creation_date_time", "rating","text", "response", "selected_modules"]
    result_pd = move_column_to_front(result_pd, ordered_list)
    num_conversation = len(result_pd)
    result_pd.to_csv(output_statistcs)

    print("Find the final joined file for module {0} at {1}".format(module, output_statistcs))
    
    return num_conversation, output_statistcs

def get_list_of_dates(module_name):
    with open(TOPIC_TO_DATE_2018_FILE, "r") as m:
        mapping = json.loads(m.read())
    result = {"day": [], "conversation_id": []}
    for module in mapping:
        if module_name is None:
            result["day"].extend(mapping[module]["day"])
            result["conversation_id"].extend(mapping[module]["conversation_id"])
        elif module_name.strip() in module:
            return module, mapping[module]
    result["day"] = list(set(result["day"]))
    result["conversation_id"] = list(set(result["conversation_id"]))
    return "ALL", result

def main(args):
    module, metadata = get_list_of_dates(args.module)
    if len(metadata) > 0:
        num_conversation, output_file = get_2018_data(args, module, metadata, HISTORY_DATA_CACHE, args.forced)
    else:
        num_conversation = 0
        output_file = None

    return num_conversation, output_file


if __name__ == "__main__":
    """ This is executed when run from the command line """
    # debug_mode()
    parser = argparse.ArgumentParser()

    add_args(parser)

    args = parser.parse_args()
    print(main(args))