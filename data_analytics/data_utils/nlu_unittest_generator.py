"""Generate Raw CSV File for Unittest raw labeling
"""
import argparse
from datetime import datetime as dt
import os, json
import pandas as pd
from data_analytics.athena_metadata import get_today
from data_analytics.analytics_metadata import UNIT_TEST_LOCATION
from data_analytics.data_utils.daily_conversation_generator import main as conversation_generator

def filter_my_module(row, module_name):
    i = row['selected_modules']
    if type(i) != str:
        i = '[{"S": "None"}]'
    s = json.loads(i)[0]['S']
    if s == module_name:
        return True
    return False

RETURNNLP_FIELDS = []

def construct_returnnlp(df):
    """Fields in returnnlp
    - text
    - dialog_act
    - dialog_act_label
    - intent
    - sentiment
    - googlekg
    - noun_phrase
    - topic
    - amazon_topic
    - concept
    - dependency_parsing
    - pos_tagging
    """
    returnnlp_keys = {"text":"text", "dialog_act":"segmented_dialog_act (dialog_act)", \
    "topic_class": "amazon_topic_class (topic_class)",
    "amazon_topic": "amazon_topic",
    "intent":"segmented_custom_intents (intent_classify2)", \
    "sentiment":"segmented_sentiment (sentiment2)", "googlekg":"segmented_googlekg", \
    "noun_phrase":"segmented_nounphrase", "topic":"segmented_topic (topic2)", "dependency_parsing":"dependency_parsing", \
    "concept":"segmented_concept (concept)",\
    "pos_tagging":"pos_tagging", }
    results = []
    for i, row in df.iterrows():
        result_dict = {keys:row[returnnlp_keys[keys]] if not isinstance(row[returnnlp_keys[keys]], float) else "" for keys in returnnlp_keys}
        results.append(json.dumps(result_dict))
    return results

def unittest_generator(filepath, result_folder, module):
    df = pd.read_csv(filepath)
    filename = filepath.split("/")[-1]
    result_file = os.path.join(result_folder, filename.replace(".csv", "unittest.csv"))
    df = pd.read_csv(filepath)
    last_module = filter_selected_modules(df, module)
    df["last_module"] = last_module

    df = df[df["selected_modules"] == module].reset_index(drop=True)

    result = df[["conversation_id", "text", "selected_modules", "central_elem", "a_b_test", "last_module"]].reset_index(drop=True)
    result["returnnlp"] = construct_returnnlp(df)
    result.to_csv(result_file)
    return result_file

def filter_selected_modules(df, module):
    last_module_field = [""]
    for i in range(len(df.selected_modules)-1):
        last_module = df.selected_modules[i]
        new_module = df.selected_modules[i+1]

        if new_module == module:
            last_module_field.append(last_module)
        else:
            last_module_field.append("")
    return last_module_field

def add_args(parser):
    today = get_today(True)
    parser.add_argument('--start', '-s', default=today,
                        help='timerange from (YYYY-mm-dd)')
    parser.add_argument('--end', '-e', default=today,
                        help='timerange to (YYYY-mm-dd)')
    parser.add_argument('--data_table', '-d', default="state_table_prod", help="data_table")
    parser.add_argument('--ratings', '-r', default=None, help="Filter by Ratings, default to =_5.0")
    parser.add_argument('--module', '-m', default=None, help="Filter by Modules")
    parser.add_argument('--forced', '-f', default=False, help="Force build joined table")
    parser.add_argument('--conv_id', '-n', default=None, help="Conversation ID")
    parser.add_argument('--updated', '-u', default=False, help="Updated to most current")

def main(args):
    if not os.path.exists(UNIT_TEST_LOCATION):
        os.mkdir(UNIT_TEST_LOCATION)
    _, success_joins, failed_joins = conversation_generator(args, clean_fields=False)
    result_files = []
    for rating_file, no_rating_file in success_joins:
        r = unittest_generator(rating_file, UNIT_TEST_LOCATION, args.module)
        result_files.append(r)
    return "Completed Unit Test Retrieval", result_files

if __name__ == "__main__":
    """ This is executed when run from the command line """
    # debug_mode()
    parser = argparse.ArgumentParser()

    add_args(parser)

    args = parser.parse_args()
    msg, _, = main(args)