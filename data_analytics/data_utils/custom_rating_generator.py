"""Generate Statistics for custom defined date, dateranges, or ratings
"""

import argparse
import os
import shutil

from data_analytics.analytics_metadata import ANALYTICS_ROOT
from data_analytics.athena_metadata import RATING_PATH
from data_analytics.get_from_athena import get_2019_data, get_ratings
from data_analytics.data_utils.integration_test_generator import generate_integration_tests
from datetime import datetime as dt
import pandas as pd
import datetime

from data_analytics.data_utils.daily_rating_generator import move_column_to_front

def get_between_ratings(rating, r1, r2):
    try:
        if type(rating) != float:
            rating = float(rating)
    except:
        print("Rating not proper format.  Please provide rating float value between 1.0 - 5.0")
    r1 = min(r1, r2)
    r2 = max(r1, r2)
    if r1 < rating and r2 > rating:
        return True
    return False

def get_rating(rating, r):
    try:
        if type(rating) != float:
            rating = float(rating)
    except:
        print("Rating not proper format.  Please provide rating float value between 1.0 - 5.0")

    if abs(r - rating) <= 1e-7:
        return True
    return False


def get_conversation_ids_by_rating(rating_filter, state_table_file, rating_file, output_file):
    """ Provide rating, get the conversation with that rating.

    Write the conversation_statistics file.csv

    Write the integration test case.

    Note: Clear custom statistics file everytime before use.
    """
    # Join two data tables
    state_table = pd.read_csv(state_table_file)
    rating_table = pd.read_csv(RATING_PATH)

    # Creating a dictionary to faciliate rating query
    rating_table_dict = rating_table.set_index("Conversation ID").transpose().to_dict()
    filtered_rating = {r:rating_table_dict[r] for r in rating_table_dict if rating_filter(rating_table_dict[r]["Rating"])}
    filtered_df = state_table[state_table.conversation_id.isin(filtered_rating)]

    ratings = [rating_table_dict[row["conversation_id"]]["Rating"] for i, row in filtered_df.iterrows()]
    
    filtered_df["ratings"] = ratings

    filtered_df = filtered_df.sort_values(["conversation_id", "creation_date_time"])

    ordered_list = ["conversation_id", "session_id", "user_id", "creation_date_time", "ratings","text", "response", "selected_modules"]
    filtered_df = move_column_to_front(filtered_df, ordered_list)
    filtered_df.to_csv(output_file)

    print("Find the final joined file at {0}".format(output_file))

    return filtered_df

def get_custom_datatables(start_time, end_time, output_state_table_csv):
    """Provided the start_time and end_time, get the custom csv datatable file
    """
    get_2019_data(start_time, end_time, output_file=output_state_table_csv)

def add_args(parser):
    today = dt.today() + datetime.timedelta(days=1)
    beginning_date_string = "2019-10-28"

    parser.add_argument("--rating", '-r', default="5.0", help="Rating to retrieve.  Either --rating=5.0 or --rating=1.0-5.0")

    parser.add_argument('--start', '-s', default=beginning_date_string,
                        help='timerange from (YYYY-mm-dd)')
    parser.add_argument('--end', '-e', default=today.strftime("%Y-%m-%d"),
                        help='timerange to (YYYY-mm-dd)')

    parser.add_argument('--refresh', '-f', default="False",
                        help='Reset Data.  Default True for integration Test, default to False for local testing')
    return

def parse_rating(rating_input):
    try:
        if "-" in rating_input:
            ratings = rating_input.split("-")
            r1, r2 = ratings
            return float(r1), float(r2)
        else:
            return float(rating_input), float(rating_input)
    except:
        print("Format is wrong for rating: {}".format(rating_input))
        return None, None

def main(args):
    output_dir = os.path.join(ANALYTICS_ROOT, "output_files")
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    custom_dir =  os.path.join(output_dir, "custom_statistics")
    if args.refresh == "True":
        shutil.rmtree(custom_dir)
    if not os.path.exists(custom_dir):
        os.mkdir(custom_dir)

    
    s_rating = "p".join(args.rating)
    output_state_table_csv = os.path.join(custom_dir, "{}_{}-statetableprod.csv".format(args.start, args.end))
    output_file = os.path.join(custom_dir, "custom_statistics_for_{}-{}_{}.csv".format(args.start, args.end, s_rating))
    if not os.path.exists(output_state_table_csv):
        get_2019_data(args.start, args.end, output_file=output_state_table_csv)

    if not os.path.exists(RATING_PATH):
        get_ratings()
    
    r1, r2 = parse_rating(args.rating)

    if r1 == r2:
        rating_filter = lambda rating: get_rating(rating, r1)
    else:
        rating_filter = lambda rating: get_between_ratings(rating, r1, r2)

    df = get_conversation_ids_by_rating(rating_filter, output_state_table_csv, RATING_PATH, output_file)

    # Integration Test Log
    log_path = os.path.join(custom_dir, "integration_log")
    if not os.path.exists(log_path):
        os.mkdir(log_path)
    locations = generate_integration_tests(df, log_path)

    return output_file, locations, log_path


if __name__ == "__main__":
    """ This is executed when run from the command line """
    # debug_mode()
    parser = argparse.ArgumentParser()

    add_args(parser)

    args = parser.parse_args()
    main(args)