import pandas as pd
import json
import numpy
import os
import datetime

from data_analytics.athena_metadata import state_table_prod, RATING_PATH, OUTPUT_JOINED_FILE, today, OUTPUT_ROOT, ANALYTICS_ROOT, OUTPUT_JOINED_FILE_NO
from data_analytics.data_utils.utils import move_column_to_front

PROD3_FILE = state_table_prod["output_csv"]

def retrieve_new_conversation_ids():
    yesterday = today - datetime.timedelta(days=1)
    yesterday = yesterday.replace(hour=9, minute=0, second=0)
    yesterday_string = yesterday.strftime("%Y-%m-%d %H:%M:%S")
    df = pd.read_csv(RATING_PATH)
    return df[df["Approximate Start Time"] > yesterday_string]["Conversation ID"]


def join_daily_rating_table():
    if not os.path.exists(PROD3_FILE) or not os.path.exists(RATING_PATH):
        print("Join Operation Not Run, Please pull the most recent datasets")
    else:
        state_table = pd.read_csv(PROD3_FILE)
        rating_table = pd.read_csv(RATING_PATH)

        # Creating a dictionary to faciliate rating query
        rating_table_dict = rating_table.set_index("Conversation ID").transpose().to_dict()
    
        filtered_df = state_table[state_table.conversation_id.isin(rating_table_dict)]
        filtered_df = filtered_df[filtered_df.conversation_id.isin(retrieve_new_conversation_ids())]

        no_rating_df = state_table[~state_table.conversation_id.isin(rating_table_dict)]
        ratings = [rating_table_dict[row["conversation_id"]]["Rating"] for i, row in filtered_df.iterrows()]
        no_rating_df["ratings"] = ""
        filtered_df["ratings"] = ratings

        filtered_df = filtered_df.sort_values(["conversation_id", "creation_date_time"])
        no_rating_df = no_rating_df.sort_values(["conversation_id", "creation_date_time"])
        # Theoretically, we'll cut off everyday at 1pm
        yesterday = today - datetime.timedelta(days=1)
        yesterday = yesterday.replace(hour=13, minute=0, second=0)
        yesterday_string = yesterday.strftime("%Y-%m-%d %H:%M:%S")
        no_rating_df = no_rating_df[no_rating_df["creation_date_time"] > yesterday_string]
        no_rating_df = no_rating_df[no_rating_df["conversation_id"].notna()]
        ordered_list = ["conversation_id", "session_id", "user_id", "creation_date_time", "ratings","text", "response", "selected_modules", \
            "a_b_test", "resp_type", "template_keys", "topic_class", "topic2", "intent_classify2", \
                "returnnlp", "central_element", "dialog_act", "googlekg", "concept"]
        filtered_df = move_column_to_front(filtered_df, ordered_list)
        no_rating_df = move_column_to_front(no_rating_df, ordered_list)
        filtered_df.to_csv(OUTPUT_JOINED_FILE)
        no_rating_df.to_csv(OUTPUT_JOINED_FILE_NO)
        print("Find the final joined file at {0}".format(OUTPUT_JOINED_FILE))
        print("Find the final joined file with no rating at {0}".format(OUTPUT_JOINED_FILE_NO))
    return

if __name__ == "__main__":
    """ This is executed when run from the command line """
    # debug_mode()
    daily_statics = os.path.join(os.getenv("COBOT_HOME"), "data_analytics", "output_files", "daily_statistics")
    if not os.path.exists(daily_statics):
        os.mkdir(daily_statics)
    join_daily_rating_table()
