# """Generate Integration Test Cases Per Module

# Read from output_files/daily_statistics and create a folder for integration test cases for each module
# """

import pandas as pd
# import numpy as np

# import json
import os

def generate_integration_test(filename):
    """Given a Dataframe only containing the conv_id, generate the integration test files
    """
    if not os.path.exists(filename):
        return None
    df = pd.read_csv(filename)
    sub_df = df.sort_values("creation_date_time")
    input_log = []
    result_log = []
    conv_id = list(sub_df["conversation_id"])
    if len(conv_id) == 0:
        return None
    conv_id = conv_id[0]
    for i, row in sub_df.iterrows():
        t = row["text"]
        if type(t) == float:
            t = ""
        input_log.append(t)
        result_log.append("ASR: " + t)
        r = row["response"]
        if type(r) == float:
            r = ""
        result_log.append("TTS: " + r)
    if len(input_log) == 0:
        return None
    input_filename = "integration_test_input_for_{}.txt".format(conv_id)
    output_filename = "integration_test_output_for_{}.txt".format(conv_id)

    create_file(input_filename, input_log)
    create_file(output_filename, result_log)
    return input_filename, output_filename

def create_file(filename, l):
    """Given a filename and a list, print list into the file like integration test
    """
    result_string = "\n".join(l)
    with open(filename, "w") as w:
        w.write(result_string)

# if __name__ == "__main__":
#     """ This is executed when run from the command line """
#     # debug_mode()
#     if not os.path.exists(GENERATED_INTEGRATION_ROOT):
#         os.mkdir(GENERATED_INTEGRATION_ROOT)

#     cmd_lin = sys.argv
#     if len(cmd_lin) == 2:
#         print("Generating custom test case for ".format(cmd_lin[1]))
#         file_name = os.path.join(OUTPUT_ROOT, "daily_statistics", "daily_statistics-{0}.csv".format(cmd_lin[1]))
#         if not os.path.exists(file_name):
#             print("File not found! " + file_name)
#         else:
#             generate_integration_tests(get_daily_statistics(file_name))
#     elif len(cmd_lin) == 1:
#         daily_df = get_daily_statistics()
#         daily_no_rating = get_daily_statistics(OUTPUT_JOINED_FILE_NO)
#         generate_integration_tests(daily_df)
#         generate_integration_tests(daily_no_rating)
#     else:
#         print("No Test Run")

