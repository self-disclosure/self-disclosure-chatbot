"""Common util functions for data mangling
"""

import pandas as pd
import os
import json
from datetime import datetime as dt
import datetime
from data_analytics.athena_metadata import get_today
from data_analytics.libs.utils import extract_template_keys, extract_errors

ORDERED_LIST = ["conversation_id", "session_id", "user_id", "creation_date_time", "ratings", "resp_type", "text", "response", "selected_modules", \
        "dialog_act", "error_template", "a_b_test", "template_keys", "sentiment","sentiment2", "sentiment_allennlp", "topic_class", "amazon_topic", "topic", "topic2", "system_acknowledgement", "backstory",\
         "intent_classify", "intent_classify2", \
         "nounphrase2", "googlekg", "concept", "dependency_parsing", "pos_tagging", "converttext", "profanity_check", "central_elem"]


FIELD_NAME_MAP = {"resp_type": "System Response Type (resp_type)", 
                  "topic_class": "amazon_topic_class (topic_class)",
                  "dialog_act": "segmented_dialog_act (dialog_act)", 
                  "sentiment_allennlp": "segmented_sentiment_allennlp (sentiment_allennlp)", 
                  "sentiment2": "segmented_sentiment (sentiment2)", 
                  "topic2":"segmented_topic (topic2)",
                  "intent_classify":"custom_intents (intent_classify)",
                  "intent_classify2": "segmented_custom_intents (intent_classify2)", 
                  "googlekg": "segmented_googlekg", 
                  "concept":"segmented_concept (concept)", 
                  "nounphrase2": "segmented_nounphrase"}

def remove_extra_field(d):
    """Remove M, L, S, BOOL, N from dictionary value
    """
    if isinstance(d, dict):
        if "M" in d:
            return remove_extra_field(d["M"])
        elif "L" in d:
            return [remove_extra_field(l) for l in d["L"]]
        elif "S" in d:
            return str(d["S"])
        elif "BOOL" in d:
            return bool(d["BOOL"])
        elif "N" in d:
            return float(d["N"])
        else:
            return {k: remove_extra_field(d[k]) for k in d}
    if isinstance(d, list):
        return [remove_extra_field(i) for i in d]
    return d

def pretty_process(series, remove_extra_list=False):
    def process(json_string):
        if isinstance(json_string, str):
            try:
                d = json.loads(json_string)
                processed_fields = remove_extra_field(d)
                if len(processed_fields) == 1 and isinstance(processed_fields, list) and remove_extra_list:
                    return processed_fields[0]
                return processed_fields
            except Exception as e:
                return json_string
        return json_string
    return series.apply(lambda x: process(x))

def move_column_to_front(df, ordered_list, clean_fields=True):
    cols = list(df.columns.values)
    for item in ordered_list[:]:
        if item not in cols:
            ordered_list.remove(item)
        else:
            if clean_fields:
                df[item] = pretty_process(df[item], item=="selected_modules")
            

    return df[ordered_list].reset_index(drop=True)

def reformat_unjoined_tables(state_table_path, output_files,  forced, user_id=None, updated=False):
    rating_file = output_files[0]
    no_ratings_file = output_files[1]
    if not os.path.exists(state_table_path):
        print("Please retrieve {} first".format(state_table_path))
        return 0, 0
    elif os.path.exists(no_ratings_file) and not forced:
        return 0, pd.read_csv(no_ratings_file).shape[0]
    else:
        state_table = pd.read_csv(state_table_path)
        # Filter by User ID
        if user_id is not None:
            full_user_ids = get_full_user_id(user_id, state_table)

            state_table = state_table[state_table.user_id.isin(full_user_ids)]
        state_table = process_joined_table(state_table)
        state_table.to_csv(no_ratings_file)
        print("Find the final joined file for beta table {0}".format(no_ratings_file))
        return 0, state_table.shape[0]
    return 0, 0

def combine_df(list_of_df):
    return pd.concat(list_of_df, axis=1)

def extract_row_error(row):
    template_keys = extract_template_keys(row.get("template_keys", ""))
    errors = extract_errors(template_keys)
    if len(errors) > 0:
        return " ".join(errors)
    return " "

def get_conversations_by_id(fname, conv_ids, rated=True):
    result = []
    if conv_ids is not None and os.path.exists(fname):
        df = pd.read_csv(fname)
        if df.shape[0] == 0:
            return result
        filtered_df = df[df.conversation_id.isin(conv_ids)]
        filtered_df["rated"] = rated
        result = filtered_df.apply(lambda row: extract_row_error(row), axis=1)
        try:
            filtered_df["error_template"] = result
        except:
            filtered_df["error_template"] = ""

        return [process_joined_table(filtered_df, ["rated", "error_template"])]
    return result

def get_full_user_id(incomplete_user_id, state_table):
    full_user_ids = set(state_table.user_id)
    return [u for u in full_user_ids if u.split(".")[-1].startswith(incomplete_user_id)]

def join_daily_rating_table(curr_date, state_table_path, rating_path, output_files, conversation_filter={}, forced=False, updated=False, clean_fields=True):
    start, end = get_daily_range(curr_date, updated=updated)
    start = start.strftime("%Y-%m-%d %H:%M:%S")
    end = end.strftime("%Y-%m-%d %H:%M:%S")
    rating_file = output_files[0]
    no_ratings_file = output_files[1]
    if not os.path.exists(state_table_path) or not os.path.exists(rating_path):
        print("Join Operation Not Run")
        return 0, 0
    elif os.path.exists(rating_file) and os.path.exists(no_ratings_file) and not forced:
        return pd.read_csv(rating_file).size, pd.read_csv(no_ratings_file).size
    else:
        state_table = pd.read_csv(state_table_path)
        rating_table = pd.read_csv(rating_path)
        # Theoretically, we'll cut off everyday at 1pm
        rating_table = rating_table[rating_table["Approximate Start Time"] <= end]
        rating_table = rating_table[rating_table["Approximate Start Time"] >= start]
        # Creating a dictionary to faciliate rating query
        rating_table_dict = rating_table.set_index("Conversation ID").transpose().to_dict()
        filtered_df = state_table[state_table.conversation_id.isin(rating_table_dict)]
        no_rating_df = state_table[~state_table.conversation_id.isin(rating_table_dict)]
        ratings = [float(rating_table_dict[row["conversation_id"]]["Rating"]) for i, row in filtered_df.iterrows()]
        no_rating_df["ratings"] = 0.0
        filtered_df["ratings"] = ratings
        no_rating_df = no_rating_df[no_rating_df["conversation_id"].notna()]
        if len(conversation_filter) > 0:
            for f in conversation_filter:
                c_f = conversation_filter[f](filtered_df)
                filtered_df = filtered_df[filtered_df.conversation_id.isin(c_f)]
        
        if len(conversation_filter) > 0:
            for f in conversation_filter:
                c_f = conversation_filter[f](no_rating_df)
                no_rating_df = no_rating_df[no_rating_df.conversation_id.isin(c_f)]

        result = filtered_df.apply(lambda row: extract_row_error(row), axis=1)
        if len(result) !=  0:
            filtered_df["error_template"] = result
        else:
            filtered_df["error_template"] = ""
        filtered_df = process_joined_table(filtered_df, clean_fields=clean_fields)
        no_rating_df = process_joined_table(no_rating_df, clean_fields=clean_fields)


        
        filtered_df.to_csv(rating_file)
        no_rating_df.to_csv(no_ratings_file)
        print("Find the final joined file at {0}".format(rating_file))
        print("Find the final joined file with no rating at {0}".format(no_ratings_file))
    return filtered_df.shape[0], no_rating_df.shape[0]

def process_joined_table(df, custom_fields=[], clean_fields=True):
    df = df.sort_values(["conversation_id", "creation_date_time"])
    ordered_list = custom_fields + ORDERED_LIST
    df = move_column_to_front(df, ordered_list, clean_fields=clean_fields)
    if clean_fields:
        df = df.rename(columns=FIELD_NAME_MAP)
    return df

def get_list_of_filters(args):
    """Create a list of filters for the args
        Note the args are processed
    """
    filters = {}
    if "module" in args and args.module != None:
        filters[args.module] = lambda df: filter_by_module(df, args.module)

    if "ratings" in args and args.ratings != None:
        filters[args.ratings] = lambda df: filter_by_ratings(df, args.ratings)

    if "conv_id" in args and args.conv_id != None:
        filters[args.conv_id] = lambda df: filter_by_conversation_id(df, args.conv_id)

    if "user_id" in args and args.user_id != None:
        filters[args.user_id] = lambda df: filter_by_user_id(df, args.user_id)

    return filters

def filter_by_date(df, date):
    """Return a list of conversations ids with a particular date"""
    start_date, end_date = get_daily_range(date)
    start_date = start_date.strftime("%Y-%m-%d %H:%M:%S")
    end_date = end_date.strftime("%Y-%m-%d %H:%M:%S")
    df = df[df["creation_date_time"] >= start_date]
    df = df[df["creation_date_time"] <= end_date]
    return set(df["conversation_id"])

def filter_by_module(df, module):
    conv_list = []
    for j, row in df.iterrows():
        s = row["selected_modules"]
        if isinstance(s, str): 
            if s == module:
                conv_list.append(row["conversation_id"])
    return conv_list

def filter_by_ratings(df, ratings_op, past=False):
    if past:
        rating_keys = "rating"
    else:
        rating_keys = "ratings"
    if df[rating_keys].iloc[0] == "":
        return df
    op, ratings = ratings_op.split("_")
    if op == "=":
        return set(df[df[rating_keys].astype("float") == float(ratings)]["conversation_id"])
    elif op == ">=":
        return set(df[df[rating_keys].astype("float") >= float(ratings)]["conversation_id"])
    elif op == "<=":
        return set(df[df[rating_keys].astype("float") <= float(ratings)]["conversation_id"])
    elif op == ">":
        return set(df[df[rating_keys].astype("float") > float(ratings)]["conversation_id"])
    elif op == "<":
        return set(df[df[rating_keys].astype("float") < float(ratings)]["conversation_id"])
    return set(df[df[rating_keys] == float(ratings)]["conversation_id"])

def filter_by_conversation_id(df, conversation_id):
    return set(df[df["conversation_id"] == conversation_id]["conversation_id"])

def filter_by_user_id(df, user_id):
    full_ids = get_full_user_id(user_id, df)
    return set(df[df["user_id"].isin(full_ids)]["conversation_id"])
"""
Utils to Handle Filenames
"""

def get_daily_range(date, is_string=False, updated=False):
    """Given the date string, convert to UTC, and provide a date range.
    

    Parameters:
      date - Date in format YYYY-MM-DD

    Return:
      start_time -
      end_time - 
    """
    date = get_today(date=date, utc=True)
    yesterday = date - datetime.timedelta(days=1)
    yesterday = yesterday.replace(hour=9, minute=45, second=0)
    if not updated:
        date_obj = date.replace(hour=9, minute=45, second=0)
    else:
        yesterday = date.replace(hour=9, minute=45, second=0)
        date_obj = get_today(utc=True) + datetime.timedelta(days=1)
    # Every day will have data of yesterday's 5:00AM to today's 5:00AM
    
    start_time = get_today(is_string, True, yesterday)
    end_time = get_today(is_string, True, date_obj)
    return start_time, end_time

def combine_df(list_of_df):
    return pd.concat(list_of_df, axis=1)

def extract_row_error(row):
    template_keys = extract_template_keys(row.get("template_keys"))
    errors = extract_errors(template_keys)
    if len(errors) > 0:
        return " ".join(errors)
    return ""

def get_conversations_by_id(fname, conv_ids, rated=True):
    result = []
    if conv_ids is not None and os.path.exists(fname):
        df = pd.read_csv(fname)
        if df.shape[0] == 0:
            return result
        filtered_df = df[df.conversation_id.isin(conv_ids)]
        filtered_df["rated"] = rated
        result = filtered_df.apply(lambda row: extract_row_error(row), axis=1)
        try:
            filtered_df["error_template"] = result
        except:
            filtered_df["error_template"] = ""

        return [process_joined_table(filtered_df, ["rated", "error_template"])]
    return result