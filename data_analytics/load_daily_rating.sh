#!/usr/bin/env bash

cd $COBOT_HOME
# python3.6 data_analytics/get_from_athena.py 2019
cd data_analytics/libs
python3.6 daily_rating_metrics.py
python3.6 error_analytics.py

# python3.6 feedback_generator.py
# python3.6 integration_test_generator.py

# cd $COBOT_HOME
# cd data_analytics/libs
# python3.6 rating_metrics.py 
