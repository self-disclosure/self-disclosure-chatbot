"""
Data Retrieval from athena for 2018,2019 cloudwatch logs

"""

import argparse
import csv
import os
import time
import re

import boto3
import botocore

from datetime import datetime as dt
import datetime

import logging

from datetime import datetime
from data_analytics.athena_metadata import CLOUDWATCH_LOGS, today_string, today
from data_analytics.analytics_metadata import OUTPUT_JOINED_FILE

from retrying import retry
import pprint

import time

LOG_GROUP = "/aws/lambda/cobot-lambda"
cloud_watchclient = boto3.client('logs')


def get_logs_from_s(s):
    time.sleep(1) # Add timer to prevent throttling
    response = cloud_watchclient.get_log_events(logGroupName=LOG_GROUP, logStreamName=s)
    forward_token = response["nextForwardToken"]
    backward_token = response["nextBackwardToken"]

    # Get forward token
    curr_token = ""
    result_dict = {event["timestamp"]:event["message"] for event in response["events"]}
    while curr_token != forward_token:
        response = cloud_watchclient.get_log_events(logGroupName=LOG_GROUP, logStreamName=s, startFromHead=True, nextToken=forward_token)
        curr_token = forward_token
        forward_token = response["nextForwardToken"]
        events = {event["timestamp"]:event["message"] for event in response["events"]}
        result_dict = {**events, **result_dict}

    curr_token = ""
    while curr_token != backward_token:
        response = cloud_watchclient.get_log_events(logGroupName=LOG_GROUP, logStreamName=s, startFromHead=True, nextToken=backward_token)
        curr_token = backward_token
        backward_token = response["nextBackwardToken"]
        events = {event["timestamp"]:event["message"] for event in response["events"]}
        result_dict = {**events, **result_dict}
    return result_dict

def get_cloudwatch_logs(conv_id, day_input, day_string=None):
    print("Getting the logs for {conv_id}, {date}...".format(conv_id=conv_id, date=day_input))
    paginator = cloud_watchclient.get_paginator('filter_log_events')

def get_cloudwatch_logs(query, starttime, endtime):
    result = cloud_watchclient.start_query(
        logGroupName=LOG_GROUP,
        logStreamNamePrefix=day_input,
        filterPattern=conv_id)
    stream_names = set()
    for response in response_iterator:
        r = set([stream["logStreamName"] for stream in response["searchedLogStreams"]])
        stream_names = stream_names.union(r)
    if day_string is None:
        day_string = "custom"
    result_stream_to_message = {}
    for s in stream_names:
        events = get_logs_from_s(s)

        # sort event by timestamp
        timestamps = list(events.keys())
        timestamps.sort()
        result_string = "\n".join([events[t] for t in timestamps])
        result_stream_to_message[s] = result_string
    
    final_output = "\n\n".join(["{s}\n{m}".format(s=r, m=result_stream_to_message[r]) for r in result_stream_to_message])
    result_folder = os.path.join(CLOUDWATCH_LOGS, day_string)
    if not os.path.exists(result_folder):
        os.mkdir(result_folder)
    result_file = os.path.join(CLOUDWATCH_LOGS, day_string, "{conv_id}.txt".format(conv_id=conv_id))
    with open(result_file, "a+") as log:
        log.write(final_output)

# def get_logs_for_conversation_id(conversation_id):
#     """Get logs for conversation_id

#     Parameters:
#       conversation_id
#     """

    """
    if not os.path.exists(OUTPUT_JOINED_FILE):
        print("Logs not generated!  Please run ./load_daily_rating.sh to get the latest statistics")
        return False
    joined_table = pd.read_csv(OUTPUT_JOINED_FILE)
    for i, row in joined_table.iterrows():
        conv_id = row["conversation_id"]
        cdt = row["creation_date_time"]
        date = cdt.split(" ")[0]
        date = "/".join(date.split("-"))
        get_cloudwatch_logs(conv_id, date, today_string)
    

    return True

# def add_args(parser):
#     today = dt.today() + datetime.timedelta(days=1)
#     # parser = argparse.ArgumentParser(description='Search range.')
#     yesterday = dt.today() - datetime.timedelta(days=1)
#     yesterday = yesterday.replace(hour=9, minute=0, second=0)

#     parser.add_argument("--ratings", '-r', default=True, help="Retrieve daily ratings")

#     parser.add_argument('--start', '-s', default=yesterday.strftime("%Y-%m-%d"),
#                         help='timerange from (YYYY-mm-dd)')
#     parser.add_argument('--end', '-e', default=today.strftime("%Y-%m-%d"),
#                         help='timerange to (YYYY-mm-dd)')
#     parser.add_argument('--output', '-o', default='output.csv',
#                         help='output file')

#     # parser.add_argument('--update_feedback', default=True, help="Pull directly from amazon feedback")
#     return parser

# def deprecated_main(): # Remove Later
#     """ Main entry point of the app """
#     parser = add_args()
#     args = parser.parse_args()
        
#     print("Start: {}".format(args.start))
#     print("End: {}".format(args.end))

#     if int(args.data_source) == 2018:
#         get_2018_data()

#     if int(args.data_source) == 2019:
#         get_2019_data()

def main():
    start_time = 1572400200
    end_time = 1572400380
    conv_id = "950fc89dac3fa6af6d8623fe080af8df4cc36af7b4f0cd24bc3514fdb3d0ca3b"
    query = QUERY.format(conversation_id=conv_id)
    result = get_cloudwatch_logs(query, start_time, end_time)
    # parser = argparse.ArgumentParser()
    # subparsers =  parser.add_subparsers(dest="version")

    # _2018_parser = subparsers.add_parser("2018")
    # _2019_parser = subparsers.add_parser("2019")

    # add_args(_2018_parser)
    # add_args(_2019_parser)

    # args = parser.parse_args()

    # if not os.path.exists(os.path.join(ANALYTICS_ROOT, "output_files")):
    #     os.mkdir(os.path.join(ANALYTICS_ROOT, "output_files"))
    
    # if args.version == '2018':
    #     # run 2018 parser
    #     subparser_args = _2018_parser.parse_known_args()
    #     get_2018_data()
    # else:
    #     # run 2019 parser
    #     subparser_args, _ = _2019_parser.parse_known_args()
    #     get_2019_data(subparser_args.start, subparser_args.end)
    #     # if bool(subparser_args.ratings):
    #     get_ratings()




if __name__ == "__main__":
    """ This is executed when run from the command line """
    # debug_mode()
    main()