"""Data Analytics 2.0
- Get Historical Data if not exist
- Generate Running Average of All Known Modules
"""
import pandas as pd
import numpy as np

import json
import os
from datetime import datetime as dt
import datetime
import pprint

from data_analytics.athena_metadata import get_today
from data_analytics.analytics_metadata import HISTORY_DATA, HISTORY_AB_TEST, MODULE_2019, _2019_START_DATE
from data_analytics.data_utils.daily_conversation_generator import process_retrieved_data
from data_analytics.libs.utils import generate_module_average, write_to_json, read_from_json

def get_csv_table(forced=False):
    """ If the table exists, then retrieve the csv data frame
        for one day
    """
    if forced:
        history_dict = None
    else:    
        history_dict = get_history_data()
    if history_dict is None:
        metadata = {m:{"daily_rating":[], "counts":[]} for m in MODULE_2019}
        last_date = _2019_START_DATE
    else:
        metadata = history_dict
        last_date = history_dict["last_date"]

    success_joins, failed_joins, total_rated, total_not_rated = process_retrieved_data(last_date, get_today(), {})
    dates = []
    
    for f, _ in success_joins:
        date = f.split("/")[-1].split("_")[0]
        dates.append(date)
        get_data_for_one_day(f, metadata)
    
    # reformat metadata
    series = [{"name": m, "daily_rating":metadata[m]["daily_rating"], "counts":metadata[m]["counts"] }for m in metadata]
    result = {"series": series, "date":dates, "modules":MODULE_2019, "last_date":date}
    write_to_json(result, HISTORY_DATA)

def get_data_for_one_day(filename, metadata):
    df = pd.read_csv(filename)
    for module in MODULE_2019:
        module_metadata = metadata[module]
        daily_rating = module_metadata["daily_rating"]
        daily_count = module_metadata["counts"]
        last_rating = daily_rating[-1] if len(daily_rating) > 0 else 0
        last_count = daily_count[-1] if len(daily_count) > 0 else 0
        m = generate_module_average(df, module)
        if m is not None:
            count, rating = m[module]
        else:
            count, rating = last_count, last_rating

        daily_rating.append("{:2f}".format(rating))
        daily_count.append(count)
    return

def get_history_data():
    if os.path.exists(HISTORY_DATA):
        result = read_from_json(HISTORY_DATA)
        if "last_date" in result:
            return result
    return None

def main():
    get_csv_table()

if __name__ == "__main__":
    """ This is executed when run from the command line """
    main()
