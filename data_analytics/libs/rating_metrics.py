"""
   Calculate the running average of the rating
   # provided by Zhou

   2019 Cobot Update:
   Load the result_display_data.json for last summary
   Load daily_rating.csv from get_from_athena script

   Calculate the current running average, along with running max and running
   min, and conversation length

   Update result_display_data.json

   Pprint result json to slack

   Generally it will read from joined output path
"""
import pandas as pd
import numpy as np

import json
import os
from datetime import datetime as dt
import datetime
import pprint

from data_analytics.athena_metadata import OUTPUT_JOINED_FILE, today_string
from data_analytics.analytics_metadata import HISTORY_DATA, DAILY_DATA, DAILY_AB_TEST, HISTORY_AB_TEST, get_raw_filename

#import matplotlib.pyplot as plt

pd.options.display.max_colwidth = 1000

def read_raw_csv():
    output_file = get_raw_filename()
    if os.path.exists(output_file):
        df = pd.read_csv(output_file)
        return df
    else:
        print("Analysis not Run.  Output File not generated")
        return None

def read_from_old(f):
    with open(f, "r") as json_file:
        file_json = json.load(json_file)
    return file_json

def write_to_json(json_obj, data_path=HISTORY_DATA):
    with open(data_path, 'w') as outfile:
        json.dump(json_obj, outfile)

def clean_df(df):
    df['ratings'] = df['ratings'].apply(lambda x: float(x.replace("*", "")) if type(x) == str else x)
    return df

def load_previous_summary(json_obj):
    last_statistic = json_obj["last_statistic"]
    return last_statistic

def generate_max_min(df, json_obj):
    """
    For each selected modules, calculate the max and min ratings
    """
    return

def generate_ab_rating(df, json_obj):
    """
    For each ab_test cases, calculate the daily average
    """
    daily = dict()

    for ind in range(len(df["a_b_test"])):
        s = df['a_b_test'][ind]
        if type(s) != str:
            s = 'None'

        if s not in daily.keys():
            daily.update({s:[1,0]})
        else:
            daily[s][0]+=1

        rating = df['ratings'][ind]

        daily[s][1] = (daily[s][1]*(daily[s][0]-1)+rating)/(daily[s][0])
    # Write to json file
    write_to_json(daily, DAILY_AB_TEST)
    

def generate_running_average(df, json_obj):
    """
    For each selected modules, calculate the running average
    """
    # Load from old json_object
    b = dict()
    b.update({"ALL":[0,0]})
    for ind in range(len(df['selected_modules'])):
        i = df['selected_modules'][ind]

        if type(i) != str:
            i = '[{"S": "None"}]'
        s = json.loads(i)[0]['S']

        if s not in b.keys():
            b.update({s:[1,0]})
            b["ALL"][0] += 1
        else:
            b[s][0]+=1
            b["ALL"][0] += 1

        rating = df['ratings'][ind]

        b[s][1] = (b[s][1]*(b[s][0]-1)+rating)/(b[s][0])
        b["ALL"][1] = (b["ALL"][1]*(b["ALL"][0]-1)+rating)/(b["ALL"][0])
    # Write to json file
    write_to_json(b, DAILY_DATA)
    return b, b

def main():
    df = read_raw_csv()
    if df is not None:
        cleaned_df = clean_df(df)
        # topic_json = read_from_old(HISTORY_DATA)
        # abtest_json = read_from_old(HISTORY_AB_TEST)
        r = generate_running_average(cleaned_df, None)
        ab_test = generate_ab_rating(cleaned_df, None)

if __name__ == "__main__":
    """ This is executed when run from the command line """
    main()
