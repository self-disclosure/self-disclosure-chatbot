"""Metrics Utilities Functions
"""
import os
import pandas as pd
import json

def read_raw_csv(filename):
    if os.path.exists(filename):
        df = pd.read_csv(filename)
        return df
    else:
        print("Analysis not Run.  Output File not generated")
        return None

def clean_df(df):
    df['ratings'] = df['ratings'].apply(lambda x: float(x.replace("*", "")) if type(x) == str else x)
    return df

def write_to_json(json_obj, data_path):
    with open(data_path, 'w') as outfile:
        json.dump(json_obj, outfile)

def read_from_json(data_path):
    with open(data_path, "r") as json_file:
        file_json = json.load(json_file)
    return file_json

def process_selected_modules(raw_txt):
    if not isinstance(raw_txt, str):
        return "No Selected Modules"
    return raw_txt

def generate_module_average(df, module=None):
    """
    For each selected modules, calculate the running average
    """
    # Load from old json_object
    b = dict()
    b.update({"ALL":[0,0]})
    df = df.astype({"ratings": "float64"})
    raw_avg = df.groupby("conversation_id").agg("ratings").mean()
    b.update({"CONV_AVG":[int(raw_avg.count()), float(raw_avg.mean())]})
    for ind in range(len(df['selected_modules'])):
        i = df['selected_modules'][ind]
        s = process_selected_modules(i)

        if s not in b.keys():
            b.update({s:[1,0]})
            b["ALL"][0] += 1
        else:
            b[s][0]+=1
            b["ALL"][0] += 1

        rating = df['ratings'][ind]
        b[s][1] = (b[s][1]*(b[s][0]-1)+rating)/(b[s][0])
        b["ALL"][1] = (b["ALL"][1]*(b["ALL"][0]-1)+rating)/(b["ALL"][0])
    # Write to json file
    if module == None:
        return b
    elif module not in b:
        return None
    else:
        return {module: b[module]}

def get_resp_type_json(df):
    """
    For each ab_test cases, calculate the daily average
    """
    daily = dict()

    for ind in range(len(df["System Response Type (resp_type)"])):
        s = df['System Response Type (resp_type)'][ind]
        if type(s) != str:
            continue

        if s not in daily.keys():
            daily.update({s:[1,0]})
        else:
            daily[s][0]+=1

        rating = df['ratings'][ind]

        daily[s][1] = (daily[s][1]*(daily[s][0]-1)+rating)/(daily[s][0])
    # Write to json file
    return daily 

def generate_module_ratings_count(df):
    """
    For each selected modules, calculate the running average
    """
    b = dict()
    b.update({"ALL": {}})
    def extract_modules(df):
        return {"rating": df["ratings"].iloc[0],
                "modules": set(df["selected_modules"].apply(process_selected_modules))}
    # Load from old json_object
    result_dict = df.groupby("conversation_id").apply(extract_modules)
    for values in result_dict:
        rating = values["rating"]
        modules = values["modules"]
        for m in modules:
            if m is None:
                m = "None"
            if m not in b:
                b[m] = {}
            if rating not in b[m]:
                b[m][rating] = 0
            if rating not in b["ALL"]:
                b["ALL"][rating] = 0
            b[m][rating] += 1
            b["ALL"][rating] += 1
    b["CONV_AVG"] = {**b["ALL"]}

    b = {m:[[k, b[m][k]] for k in b[m]] for m in b}
    for m in b:
        b[m].sort(key=lambda x: x[0])
    return b

def map_module_to_count(group, result, modules):
    b = {"ALL": 0}
    
    for i, r in group.iterrows():
        i = r['selected_modules']
        s = process_selected_modules(i)
        if s not in b.keys():
            b.update({s:0})
        if s not in modules:
            modules.append(s)
        b[s] += 1
        b["ALL"] += 1
    result[r["conversation_id"]] = b

def get_module_count(df, result={}):
    r = {}
    modules = ["ALL"]
    df.groupby('conversation_id').apply(lambda x: map_module_to_count(x, r, modules))
    for module in modules:
        l = [r[i][mod] for i in r for mod in r[i] if module == mod]
        result[module] = float(sum(l))/len(l)
    
    return result

def generate_ab_rating(df):
    """
    For each ab_test cases, calculate the daily average
    """
    daily = dict()

    for ind in range(len(df["a_b_test"])):
        s = df['a_b_test'][ind]
        if type(s) != str:
            s = 'None'

        if s not in daily.keys():
            daily.update({s:[1,0]})
        else:
            daily[s][0]+=1

        rating = df['ratings'][ind]

        daily[s][1] = (daily[s][1]*(daily[s][0]-1)+rating)/(daily[s][0])
    # Write to json file
    return daily

def get_error_texts():
    from gunrock_cobot.response_templates.template_data import data
    error_data = data["error"]
    result = {}
    for error_type in error_data:
        keys = [error_type]
        process_error_d(error_data[error_type], keys, result)
        
    return result
        
def process_error_d(d, keys, result):
    if isinstance(d, dict):
        for v in d:
            new_keys = keys[:]
            new_keys.append(v)
            process_error_d(d[v], new_keys , result)
    if isinstance(d, list):
        result["_".join(keys)] = [l["entry"] for l in d]
    
def extract_template_keys(raw_txt):
    result = []
    if type(raw_txt) != str:
        return result
    return raw_txt.split(" ")

def extract_errors(template_keys):
    return [keys for keys in template_keys if "error" in keys and keys not in errors_to_ignore]

errors_to_ignore = ["error::asr_processor/asr_error", "error::profanity_req", "error::topic_modules/default"]
