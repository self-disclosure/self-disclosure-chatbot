import redis
import praw
import random

class RedisEndpoint:
    """
    An endpoint to store the API responses.
    """
    def __init__(self):
        self.r = redis.StrictRedis(host='107.23.189.246', port=16517, db=0, password="alexaprize", charset='utf-8',decode_responses=True)
        self.reddit = praw.Reddit(client_id='fYexX1VJpx_B4A',
             client_secret='hs4VxupetIdh8fct04u8ZJYGO9g',
             user_agent='extractor',
             timeout=2,
             username='gunrock_cobot',
             password='ucdavis123')

    def find(self, module, keyword):
        search_term = 'module:'+module+':'+keyword
        if self.r.exists(search_term):
            response = self.r.hgetall(search_term)
            return response
        else:
            return None

    def store_from_reddit(self, module, keyword, subreddits, number):
        store_term = 'module:'+module+':'+keyword
        responses = []
        for subreddit in subreddits:
            query = self.reddit.subreddit(subreddit)
            search_results = list(self.query.search(keyword))
            max_num = number if len(search_results) > number else len(search_results)
            for i in range(max_num):
                responses.append(search_results[i])
        samples = number if len(responses) > number else len(responses)
        final_responses = random.choices(responses, samples)
        self.r.hmset(store_term, final_responses)

    def store(self, module, keyword, data):
        store_term = 'module:'+module+':'+keyword
        self.r.hmset(store_term, data)
