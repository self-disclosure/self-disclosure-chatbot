'''
The script is used to get Washingoton post from API and write to redis

    Example: python3 write_news.py

TODO List:
    1. setup a cron job to update news
    2. write a handy client to get news data from redis

Author: Arbit Chen
'''

import requests
import redis
from datetime import datetime, timedelta
import hashlib
import logging

# https://docs.washpost.com/docs\?stdate\=2018/06/04\&enddate\=2018/06/05\&offset\=101\&count\=150\&key\=zcBuiIfOgKRN7778vfLM

def get_hash(s):
    return hashlib.md5(s.encode('utf-8')).hexdigest()

now = datetime.now()
today = now.strftime("%Y/%m/%d")
yesterday = (now - timedelta(1)).strftime("%Y/%m/%d")
cur_time = int(now.timestamp())

def gen_prim_key(content_type):
    return "gunrock:news:prim:{}".format(content_type)

def gen_subcata_key(subcata_key, sec_type, subcata_data):
    return "gunrock:news:{key}:{sec}:{data}".format(
        key=subcata_key, sec=sec_type, data=subcata_data)

def gen_subcata_cnt_key(subcata_key, sec_type):
    return "gunrock:news:{key}:{sec}:cnt".format(
        key=subcata_key, sec=sec_type)

def gen_scheme_key():
    return "gunrock:news:scheme"

def gen_hashurl_key(hashurl):
    return "gunrock:news:hashurl:{}".format(hashurl)

def write_prim(r, news):
    primary_key = gen_prim_key(news["contenttype"].lower())
    if "primarysection" in news:
        r.zadd(primary_key, cur_time, news["primarysection"].lower())
    else:
        r.zadd(primary_key, cur_time, news["headline"].lower())

def write_scheme(r, news):
    scheme_key = gen_scheme_key()
    if "primarysection" in news:
        r.hset(scheme_key, news["primarysection"].lower(), True)

def write_sub_cata(r, urlhash, news, subcata, subcata_key):
    if subcata in news:
        for n in news[subcata]:
            if "primarysection" in news:
                # EXAMPLE: Person:Sports:Stephen Curry
                org_key = gen_subcata_key(
                    subcata_key, news["primarysection"].lower(), n.lower())
                cnt_key = gen_subcata_cnt_key(
                    subcata_key, news["primarysection"].lower())
            else:
                # NOTE: contenttype key is not section but a ctype, becareful!!
                org_key = gen_subcata_key(
                    subcata_key, news["contenttype"], n.lower())
                cnt_key = gen_subcata_cnt_key(
                    subcata_key, news["contenttype"].lower())
            r.zadd(org_key, cur_time, urlhash)
            r.zincrby(cnt_key, n.lower(), 1)

def write_redis(r, data):
    for news in data["docs"]:
        urlhash = get_hash(news["contenturl"])
        # primary to find all the category
        write_prim(r, news)
        write_scheme(r, news)
        # subcategory to write url id key
        write_sub_cata(r, urlhash, news, "organization", "org")
        write_sub_cata(r, urlhash, news, "location", "loc")
        write_sub_cata(r, urlhash, news, "person", "person")
        write_sub_cata(r, urlhash, news, "theme", "theme")
        # write content
        r.hmset(gen_hashurl_key(urlhash), news)



if __name__ == "__main__":
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    for i in range(0, 10):
        # 0 - 99, 100 -199,
        offset = 0 + i * 100
        print(offset)
        req = requests.get(
            "https://docs.washpost.com/docs?stdate={stdate}&enddate={enddate}&offset={offset}&count=100&key=zcBuiIfOgKRN7778vfLM".format(stdate=yesterday, enddate=today, offset=offset))
        if req.status_code > 299:
            logging.error("request washington post error, req: {}", req)
            break
        data = req.json()
        # check if a url hash already exist in Redis
        if (len(data["docs"]) > 0 and
            r.exists(gen_hashurl_key(get_hash(data["docs"][0]["contenturl"]))) is not True):
            logging.info("key not found with key: {}".format(gen_hashurl_key(get_hash(data["docs"][0]["contenturl"]))))
            write_redis(r, data)
        else:
            logging.warning("request washington post with news already exist")
            break
