Cobot Toolkit
=============

Cobot (Conversational Bot) Toolkit is a set of tools, libraries and components designed to make developing, training and deploying open-domain or multi-domain conversational experiences for the Alexa Skills Kit.

------------
Key Features
------------

The Cobot Toolkit consists of several components that provide critical services for building conversational bots.

**Cobot Deployment**

  * Manage full deployments of ASK skill components, Lambda endpoints and Docker modules that can host large, persistent machine learning models and data sources
  * Provides a continuous integration pipeline where integrating changes to language models, dialog management techniques, and response generation strategies can be integrated and tested by a single "git push" command
  * Enables seamless scaling from development and test to production-level workloads on AWS
  * Enforces separation of test and production environments for high reliability

**Cobot Core Python SDK**

  * Manages state and handles strategies for mapping from state and intent information to dialog actions
  * Supports modular response generation components and overall dialog management strategies
  * Focused on multi-component bots, with a transparent mix of local and remote response generation modules
  * Integrated A/B testing support, for efficient, simultaneous testing of alternate bot configurations
  * Built-in Cloudwatch logging and alert mechanisms for easy debugging and operational management of bots

**Cobot Common Library**

  * Online support for Cobot Core with modules providing NLP and sentiment analysis via third party libraries
  * Offline data cleansing and preparation tools for conversational data sets, easy retrieval and usage of common open data sources
  * Topic annotation and offensive speech classification model support in both online and offline use cases

Cobot can be used to build and deploy bots based on sequence to sequence or other generative models, retrieval-based systems, scripted and modeled dialog, and ensemble approaches of all of these mechanisms.

The goal of Cobot is support experimentation and innovation in conversational bots at scale, while also being an effective platform for developing production-grade bots.

---------------------
What Cobot Doesn't Do
---------------------

Cobot is not:

* A single technique for modeling dialog
* Opinionated about the mechanism used to model dialog, understand language, or generate responses
* Strongly coupled to a particular dialog policy mechanism
* Directly dependent on the ASK NLU modeling system, but rather supports ASK NLU as well as custom NLU implementations

Tutorial
--------

.. toctree::
   :maxdepth: 1

   getting_started
   cheat_sheet
   faq
   building_a_sample_socialbot
   expanding_your_socialbot
   non_trivial_bot_example
   baseline_socialbot
   baseline_socialbot_terminology
   baseline_socialbot_nlp_pipeline
   baseline_socialbot_response_generators
   baseline_socialbot_build
   baseline_socialbot_extension
   dialog_management_and_sagemaker
   bot_configuration
   intent_model
   accessing_toolkit_service_client
   alexa_information_knowledge_graph
   model_release_material
   experimentation_and_ab_testing
   handling_launch_requests
   state_management_and_information_flow
   overriding_cobot_components
   handling_launch_requests
   nlp_pipeline
   adding_and_removing_modules
   testing_cobot_components
   debugging_and_log_access
   beta_stage_for_cobot
   workflow_recommendation
   cleansing_data_and_using_annotator
   using_included_big_data
   workflow_recommendation
   data_analytics_visualization
   change_log

Modules
_______

.. autosummary::
    :toctree:

    .. Add/replace module names you want documented here
    cobot_common
    cobot_core
    cobot_python_sdk


Indices and tables
__________________

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
