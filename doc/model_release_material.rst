.. _model_release_material:

**************************************************************************************
Improving SocialBot Quality Using Dialog Act, Topic, and Conversation Evaluator Models
**************************************************************************************

To improve the quality of SocialBots during the Alexa Prize 2018 finals, we are sharing Dialog Act and Conversation Evaluator Models. These models have been trained using hundreds of thousands of annotated Alexa Prize conversations. One of our findings from last year's competition is that several Alexa Prize teams used Dialog Act models to better identify the intent of users. Furthermore, some teams built their own heuristic conversation evaluators within the ranking strategy to obtain the best response. To improve the performance of all of the socialbots, we built a learned model from aggregated data at scale.


Dialog Act/Topic Model
======================
------
Models
------

We trained a joint model for dialog act and topic classification, which uses context (past utterances and past responses in the same session) and current utterance to predict both user's intent and dialog topic in the current interaction. Currently, the model predicts a dialog act and a topic labels from the following classes:

* Dialog Act Classes
  
  1. ClarificationIntent

  2. General_ChatIntent

  3. Information_RequestIntent

  4. Information_DeliveryIntent

  5. Opinion_RequestIntent

  6. Opinion_ExpressionIntent

  7. Topic_SwitchIntent

  8. User_InstructionIntent

  9. InteractiveIntent

  10. Multiple_GoalsIntent

  11. OtherIntent

* Topic Classes
  
  1. Entertainment_Movies
   
  2. Entertainment_Music
   
  3. Entertainment_Books

  4. Entertainment_General

  5. Sports

  6. Politics

  7. Science_and_Technology

  8. Phatic

  9. Interactive

  10. Inappropriate_Content

  11. Other

------------
Applications
------------
Following are some suggested applications of the Alexa Prize dialog act model.

1. To supplement an intent model with additional “conversational goal” meta data.
2. To enable better response generation and ranking strategies by leveraging context. E.g. It would be more meaningful to follow up with a response identified as “Information_DeliveryIntent” for an input of “Information_RequestIntent”.
3. To analyze data and identify strengths to build on and areas for improvement for the socialbot.
4. To evaluate conversation quality. Cervone et. al. [1] have identified Dialog Acts as an important factor to measure coherence in dialogs. Dialog Act can be used as an offline metric to measure the quality of the socialbot conversations.
5. For improved and personalized response styles by identification of the pattern of intents within a conversation or at the customer level.


----------------
How to use it?
----------------
The model requires a list of Conversations as input, where each conversation contains:

1. CurrentUtterance: String
2. PastUtterances: List of String (Optional, optimal performance comes from passing up to the last 5 turns' context from the oldest to newest)
3. PastResponses: List of String (Optional, optimal performance comes from passing up to the last 5 turns' context from the oldest to newest)

Offline Setting
^^^^^^^^^^^^^^^
See demo_client.py_ for detail, below is a code snippet.

.. code-block:: python

    client = get_client(api_key='<API_KEY>')
    r = client.batch_get_dialog_act_intents(conversations=[
        {
        "currentUtterance": "let's talk about the review",
        "pastUtterances": ["i'm fine let's talk about movies",
                          "i want to talk about wonder woman"],
        "pastResponses": ["'here are some movies i would like to recommend, i dream in another language, wonder woman, valerian and the city of a thousand planets, despicable me 3, wolf warrior ii   i still got some most recent movie information from rotten tomatoes, would you like to talk about that",
                           "i have some information about the movie wonder woman, plot, rating, author, review, actor, director  which one would you like to know"]
        }
    ])
    print(json.dumps(r, indent=1))

Online Setting (Socialbot's Runtime System)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
1. **If you are using CoBot**: You can create a DialogActIntentClassifier in the NLP pipeline. DialogActIntentClassifier can complement/replace the default ASK intent classifier.

For example, if you uncomment Line 125-131, comment out Line 133 in **sample_cobot/non_trivial_bot.py** (see the code snippet below as well), and add **sample_cobot/dialog_act_intent_classifier.py**, your sample cobot will use DialogActIntentClassifier to get the user intent.

.. code-block:: python

    INTENT = {
        'name': "intent",
         'class': DialogActIntentClassifier,
         'context_manager_keys': ['text']
     }
    cobot.upsert_module(INTENT)
    nlp_def = [["ner", "sentiment", "topic"], ["intent"]]

The code registers a new intent classifier (DialogActIntentClassifier) and upserts (update and insert) it to the NLP pipeline. In Line 132, intent classifier is running after topic module, since it needs to get current utterance's topic from topic module.

Note: If your custom selecting strategy selects different response generators based on intent or a combination of intent and other attributes, you need to change your selecting strategy, to accommodate the new DialogActIntent.

2. **If you are not using CoBot**, you can still get Dialog Act intents in the same way as in the offline setting.


Conversation Evaluator Model
============================

------
Models
------

We trained conversation evaluators which estimate the probability of the current utterance falling into each of 5 classes: IsResponseOnTopic, IsResponseErroneous, ResponseEngagesUser, IsResponseComprehensible, IsResponseInteresting.

These models output a probability in the scale of [0, 1] for these classes:

1. IsResponseOnTopic
2. IsResponseComprehensible
3. IsResponseInteresting
4. ResponseEngagesUser
5. IsResponseErroneous

------------
Applications
------------
Following are some suggested applications of the Alexa Prize conversation evaluator models.

1. Real-time generation of more coherent and engaging responses: This can also help in avoiding irrelevant responses, driving longer and better conversations.
2. Turn-level evaluation: These models provide scores at turn-level as opposed to conversation evaluation, which is obtained through ratings, helping identify impact of a turn on the final rating.
3. Proxy to rating: The output of these models can be used as features to predict/estimate customer experience as indicated by the final rating, maximizing which is one of the final objectives of the Alexa Prize competition.
4. Identification of strong and weak domains: These models can be used to identify the topical areas where the socialbot is good and the topical areas where it needs improvement.
5. Offline Evaluation: A/B Testing is expensive and time-consuming. It is not possible to evaluate every feature using A/B testing. These metrics can help minimize A/B tests by evaluating the component offline.


----------------
How to use it?
----------------

The model requires a list of Conversations, where each conversation contains:

1. CurrentUtterance: String
2. CurrentUtteranceTopic: String (Optional, if absent or mismatches AlexaPrizeTopicService topic classes in topic_label.txt_, it will call AlexaPrizeTopicService to get current utterance's topic)
3. CurrentResponse: String
4. CurrentResponseTopic: String (Optional, if absent or mismatches AlexaPrizeTopicService topic classes in topic_label.txt_, it will call AlexaPrizeTopicService to get current response's topic)
5. PastUtterances: List of String (Optional)
6. PastResponses: List of String (Optional)

Offline Setting
^^^^^^^^^^^^^^^
See demo_client.py_ for detail, below is a code snippet.

.. code-block:: python

    client = get_client(api_key='<API_KEY>')
    r = client.batch_get_conversation_scores(conversations=[
        {
        "currentUtterance": "no thank you",
        "currentUtteranceTopic": "Phatic",
        "currentResponse": "I see anyway I'll be glad to tell you some more news later if you want but Now I have a question for you Patrick would you help me solve a great mystery",
        "currentResponseTopic": "News",
        "pastUtterances": ["Alexa let's chat",
                           "my name is Patrick"],
        "pastResponses": ["hello this is an Alexa Prize socialbot I don't think we have met what's your name my friend",
                          "hey Patrick it's a pleasure to meet you well how is it going"]
        }
    ])
    print(json.dumps(r, indent=1))

Online Setting
^^^^^^^^^^^^^^

1. **If you are using CoBot**, you can create a CustomRankingStrategy which uses ConversationEvaluatorModel to rank the best response.

You can bind CustomRankingStrategy to RankingStrategy by uncommenting Line 79 in **sample_cobot/non_trivial_bot.py** and add sample_cobot/custom_ranking_strategy_using_conversation_evaluator.py.
For all the candidate responses from selected response generators, it uses one metric (IsResponseErroneous) to select the best response.

You can experiment with other metrics or any combination of the 5 metrics returned from ConversationEvaluatorModel.

2. **If you are not using CoBot**, you can still get Conversation Evaluator model in the same way as in the offline setting.


Best Practices
==============
1. For both Dialog Act model and Conversation Evaluator model, we recommend providing the topic from Alexa Prize Topic Model.
2. For the Dialog Act model, we recommend providing the last 2 turns' context from the oldest to newest for pastUtterances and pastResponses.

References
==========
[1] Cervone et. al., Coherence Models for Dialog, 2018,  https://arxiv.org/abs/1806.08044


.. _demo_client.py: demo_client_link.html
.. _topic_label.txt: topic_label_link.html
