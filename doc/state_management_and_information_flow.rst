.. _state_management_and_information_flow:

State Management and Information Flow
=====================================


--------
Overview
--------

State Manager is the core class that provides fast, transparent state access. Generally, state information is stored in-memory, loaded at the beginning of each turn of interaction, and stored at the end of the turn for persistence.
The persistent store that underlies the state management system is DynamoDB, a very fast key-value datastore provided in AWS. This model of state management ensures low latency, consistent and reliable data, and is flexible, allowing you to add additional key/value pairs to the State information from your own custom modules.

----------
Life Cycle
----------
The life cycle of State Manager is as below and can be further illustrated by the diagram,

 #. initialize current turn's state from ASK request and save it to current_state
 #. fetch session history from DynamoDB table (default table name is StateTable for Prod and StateTableBeta for Beta) for the same session id and save it to session_history
 #. fetch last turn from DynamoDB table and save it to last_state
 #. fetch user attributes from ASK request and DynamoDB User table (default table name is UserTable for Prod and UserTableBeta for Beta) and save it to user_attributes
 #. default NLP pipeline updates topic, ner, sentiment attributes in current_state
 #. default Dialog Manager updates candidate_responses attribute in current_state
 #. all Local CoBot classes on the Lambda Function can access and update State Manager's information through referencing State Manager class
 #. all Remote CoBot Docker modules can access and update State Manager's information through passing data between Lambda Function and Docker Modules
 #. at the end of each turn, State Manager persists current_state to DynamoDB State table and persists user attributes to User table

.. image:: images/state_manager_life_cycle_diagram.png

--------------------------
State Manager's Attributes
--------------------------
CoBot's State Manager stores the following properties:

1. current_state: State, see state.py_ for all the default attributes. These attributes will be added in the Cobot NLP Pipeline and Dialog Manager: input_offensive, topic, ner, sentiment, candidate_responses.
2. session_history: List<Dict>, session history for the same session id
3. last_state: Dict, last turn's state information
4. user_attributes: UserAttributes, refer to user_attributes.py_ for all the attributes

The current_state, last_state, and session history all reference to information related to the current turn, last turn, and history turns in this user interaction, respectively.

The user_attributes value can be used to store cross-turn and cross-interaction information about a user, their likes, wants or preferences.

.. _state.py: cobot_core.html#module-cobot_core.state
.. _user_attributes.py: cobot_python_sdk.html#module-cobot_python_sdk.user_attributes

-------------
Standard Keys
-------------

The standard keys most commonly referenced throughout the documentation and example code are "intent", "slots", "ner", "sentiment", "asr", "text".

These key values are set by FeatureExtractor and the NLP pipeline. "text" is a special value - this is extracted from the n-best ASR values ("asr") if they are available. If not, it will check the current slots to see if any match an AMAZON.RAW_TEXT slot type in the current intent model definition. If so, that will be used as the "text" value. In case you are using those slots in another way and you are not whitelisted for n-best ASR access, the "text" key may not contain what you expect. If none of those values are present, then "text" will be empty.


----------------------------------------------------------------------------------------------
Getting a Reference to State Manager and read/write APIs between State Manager and CoBot class
----------------------------------------------------------------------------------------------

When your class is not a subclass of ServiceModule
--------------------------------------------------

These include RankingStrategy, SelectingStrategy, FeatureExtractor, ASRProcessor, GlobalIntentHandler.

These classes will have state manager in the constructor by inheriting from parent class.

Read API
^^^^^^^^
You can access a specific state information by referencing it using current_state.<state_key>, if <state_key> exists in current_state parameter, otherwise, use `built-in getattr function  <https://docs.python.org/3/library/functions.html#getattr>`_ and return a default value.
getattr(x, 'foobar', None) is equivalent to x.foobar if 'foobar' exists, otherwise it returns the default value of None.

Similarly, you can access a specific user attribute by referencing it using user_attributes.<user_attribute_key>, if <user_attribute_key> exists in user_attributes parameter, otherwise, it return None.

See example code in a Local CustomRankingStrategy class:

.. code-block:: python

   class CustomRankingStrategy(Cobot.RankingStrategy):

       def rank(self, responses):
           # Read APIs to State Manager

           # Get current state's attribute: i.e.
           intent = self.state_manager.current_state.intent
           # Get session history's attributes: i.e.
           past_topics = [turn.get('topic', None) for turn in self.state_manager.session_history]
           # Get last state's attributes: i.e.
           last_response = self.state_manager.last_state.get('response', None) if self.state_manager.last_state is not None else None

           # Get user attributes: i.e.
           user_topic_preference = self.state_manager.user_attributes.favorite_topic
           print(intent, past_topics, last_response, user_topic_preference)
           return responses[0]

Write API
^^^^^^^^^
You can update a specific state information by `built-in setattr function <https://docs.python.org/3/library/functions.html#setattr>`_  function.

Similarly, you can update a specific user attribute by `built-in setattr function <https://docs.python.org/3/library/functions.html#setattr>`_ or simply reassign a new value to a new key.

See example code in a Local CustomRankingStrategy class:

.. code-block:: python

   class CustomRankingStrategy(Cobot.RankingStrategy):

       def rank(self, responses):
           # Write APIs to State Manager
           # Update current state's intent attribute: i.e.
           setattr(self.state_manager.current_state, intent, 'new_intent')
           # Update last state's attributes: since last_state is a dict, you can use dict update operations i.e.
           self.state_manager.last_state['new_key'] =  'new_value'

           # Update user attributes: i.e.
           self.state_manager.user_attributes.favorite_topic = 'Movie'
           return responses[0]


Caveat on Remote Docker Module
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
If your class is a Remote Docker Module, but not a subclass of RemoteServiceModule,
you need to extract state information from State Manager and save response from Remote Module to State Manager yourself.

See example below for a custom_remote_ranking_strategy:

.. code-block:: python

   import cobot_core as Cobot
   import json
   import requests
   from injector import inject
   from cobot_core import StateManager
   from cobot_core.log.logger import LoggerFactory
   from cobot_core.service_url_loader import ServiceURLLoader

   class CustomRemoteRankingStrategy(Cobot.RankingStrategy):
       @inject
       def __init__(self, state_manager: StateManager):
           self.state_manager = state_manager
           self.url = ServiceURLLoader.get_url_for_module("RANKER")
           self.timeout_in_millis = 1000   # default timeout
           self.logger = LoggerFactory.setup(self)

       def rank(self, responses):
            ######################################################
            # 1. Extract state information from State Manager
            # Get current state's attribute: i.e.
           intent = self.state_manager.current_state.intent
           # Get session history's attributes: i.e.
           past_topics = [turn.get('topic', None) for turn in self.state_manager.session_history]
           # Get last state's attributes: i.e.
           last_response = self.state_manager.last_state.get('response', None) if self.state_manager.last_state is not None else None

           # Get user attributes: i.e.
           user_topic_preference = self.state_manager.user_attributes.favorite_topic

           # package all the selected state information to input_data
           self.input_data = {
                'intent': intent,
                'past_topics': past_topics,
                'last_response': last_response,
                'user_topic_preference': user_topic_preference
           }

           # Finished Extract state information from State Manager
           #######################################################
           # 2. use requests library to send a HTTP request to remote service

           response = requests.post(self.url,
                                 data=json.dumps(self.input_data),
                                 headers={'content-type': 'application/json'},
                                 timeout=self.timeout_in_millis / 1000.0)


           result = response.json()

           ########################################################
           # 3. Save responses from Remote Module to State Manager
           # Update modified attributes in state manager
           modified_user_attributes = result.get('user_attributes', None)
           if modified_user_attributes:
                for k, v in modified_user_attributes.items():
                    setattr(self.state_manager.user_attributes, k, v)

           modified_state_info = result.get('context_manager', None)
           if modified_state_info:
                for k,v in modified_state_info.items():
                    setattr(self.state_manager.current_state, k, v)

           return result.get('response', None)

.. code-block:: python

   # in your cobot/docker/ranker/response_generator.py, implement handle_message
   def handle_message(msg):
       # TODO: add your ranker model inference code
       # If you also want to update state and user attributes info:
       ret = {
            'response': 'top_1_response_generator_name',
            'user_attributes':
                {
                    'expertise': 'Build software'
                },
            'context_manager':
                {
                    'mode': 'dialog_continue'
                }
       }
       return ret


.. _state_manager_access_on_local_service_module:

When your class is a subclass of LocalServiceModule
---------------------------------------------------
Local Response generators and Local NLP modules inherit from Local Service Module.

These classes will have state manager in the constructor by inheriting from parent class. The way to reference State Manager, Read APIs, and Write APIs are the same as `When your class is not a subclass of ServiceModule`_

.. _state_manager_access_on_remote_service_module:

When your class is a subclass of RemoteServiceModule
----------------------------------------------------
Remote response generators and NLP modules inherit from Remote Service Module.

Remote modules **DO NOT** currently have direct access to State Manager, since State Manager lives in-memory on the Lambda Function. Rather, they receive a subset of the state information from state manager in their remote calls.
They can send response, updated state information, and updated user attributes to State Manager on the Lambda Function in their remote calls, as well.
CoBot will process responses from Remote modules and eventually persist state information and user attributes to DynamoDB tables.

Read API
^^^^^^^^
You can access a subset of state information by specifying the remote service module definition. See all the definition config keys in service_module_config.py_.

CoBot reads and processes remote service module definition, extracts a subset of "context_manager_keys" and "input_user_attributes_keys" from State Manager and saves it to input_data, then sends a http POST request to remote service module's URL.

.. _service_module_config.py: cobot_core.html#module-cobot_core.service_module_config

.. code-block:: python

    RemoteGreeterBot = {
        'name': "GREETER",
        'class': RemoteServiceModule,
        'url': ServiceURLLoader.get_url_for_module("GREETER"),
        'context_manager_keys': ['intent', 'slots', 'text'],
        'input_user_attributes_keys': ['last_conversation_date','favorite_topic']
    }

context_manager_keys: a list of attributes in the State will be included in input_data, if absent, **all the attributes** will be included

history_turns: the number of historical turns' state information (excluding current turn) for the same session will be included in input_data. If absent, 0 history turn will be included.

If history_turns = 0, it means only the current state will be saved to input_data and its type is Dict<String, String>, where key is each specified context_manager_key, value is the state value.

If history_turns = N, it means current state and N history turns' states will be saved to input data and its type is Dict<String, List<String>>, where key is each context_manager_key, value is the List of state values, sorted by time in DESC order.

For example, if the context_manager_keys provided are "intent", "slots", and "text", history_turns is not set, and the input_user_attributes_keys provided are last_conversation_date and favorite_topic, the input sent to the remote service module via POST will look like (see the **args** variable in args = request.get_json(force=True) in app.py):

.. code-block:: python

   {
      "text":"what do you think about roger federer"
      "intent":"retrieval",
      "slots":{
         "text":{
            "name":"text",
            "value":"what do you think about roger federer"
         }
      },
      "last_conversation_date": "2018-05-05T02:26:10+00:00",
      "favorite_topic": "Movies_TV"
   }

Write API
^^^^^^^^^
Remote modules do not currently have direct access to State Manager. Rather, they run main logic in their remote calls and send back response to CoBot.
CoBot can save response, additional state information change, and user attributes change to State Manager, and eventually persist to DynamoDB tables.

If a remote module only returns the response (plain text or a list of string) to CoBot, it will simply save the response to State Manager's current state.

If a remote module not only returns the response, but also changes user attributes, and additional state information, the **expected response structure** is as below:

.. code-block:: python

   {
      "response": "<response>",
      "user_attributes":
         {
            "<user_attributes_key>": "<user_attributes_value>"
         },
      "context_manager":
         {
            "<context_manager_key>": "<context_manager_value>"
         }
   }

The State Manager will save the response, additional context_manager key-value pairs to current state, and save user attributes key-value pairs to UserAttributes object.

For example, if one remote service module returns the below response,

.. code-block:: python

   # in response_generator.py
   def handle_message(msg):
        ret = {
            'response': 'AI is the new electricity',
            'user_attributes':
                {
                    'favorite_topic': 'AI'
                },
            'context_manager':
                {
                    'intent': 'new_intent'
                }
        }
       return ret

.. code-block:: python

   # in app.py
   @staticmethod
    def __get_response(text):
        response = agent.handle_message(text)[0]

        if isinstance(response, dict):
            ret = response
        else:
            ret = {
                'response': response
            }

        app.logger.info("result: %s", ret)
        return ret

CoBot will get a response in JSON, save response to current state, update the intent to "new_intent", and update user_attributes's "favorite_topic" to "AI".
At the end of current turn, State Manager will persist current state and user attributes to DynamoDB tables.

**Special Note**:To use Write API, save_mode must set to True, the default NLP Pipeline and ResponseGeneratorRunner's save_mode is True. Avoid using CustomResponseGeneratorRunner class in your Cobot overrides function  whose save_mode is False.
