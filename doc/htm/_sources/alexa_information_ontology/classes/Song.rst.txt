
Direct super classes:
        * aio:TrackWork

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * aio:isTheNationalAnthemOf
        * aio:isTheNationalSongOf

Used in the *range* of the following properties:
        * aio:isASingerOf
        * aio:wroteTheLyricsTo
        * aio:hasTheNationalAnthem
