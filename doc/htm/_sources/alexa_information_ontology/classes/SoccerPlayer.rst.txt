
Direct super classes:
        * aio:Athlete

Direct sub classes:
        * aio:SoccerStriker
        * aio:SoccerMidfielder
        * aio:SoccerGoalkeeper
        * aio:SoccerDefender
        * aio:PremiershipFootballer

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
