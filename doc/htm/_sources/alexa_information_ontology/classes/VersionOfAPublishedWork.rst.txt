
Direct super classes:
        * aio:PublishedThing

Direct sub classes:
        * aio:AudioPublicationVersion

Used in the *domain* of the following properties:
        * aio:isAVersionOfThePublishedWork

Used in the *range* of the following properties:
        * *None*
