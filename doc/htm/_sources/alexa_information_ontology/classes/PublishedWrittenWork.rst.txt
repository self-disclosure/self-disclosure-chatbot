
Direct super classes:
        * aio:PublishedWork
        * aio:CreativeWork

Direct sub classes:
        * aio:Book

Used in the *domain* of the following properties:
        * aio:isABookInTheSeries

Used in the *range* of the following properties:
        * aio:publishedTheFirstEditionOf
