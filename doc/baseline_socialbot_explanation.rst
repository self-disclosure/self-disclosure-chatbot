.. _baseline_socialbot_explanation:

Explaining The Baseline Socialbot
=================================

------------------------
Response Generator Types
------------------------

.. image:: images/handler_dispatch_system.png

Handler Dispatch Response Generator
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

There are four components to this response generator template: the response generator itself
(HandlerDispatchResponseGenerator), the factory which creates HandlerSelectionFeatures which is used 
to determine which handlers will be called (HandlerSelectionFeaturesFactory), the dispatcher which
acts as the flow director between different handlers (HandlerDispatcher), and all the different handlers (Handler).

The HandlerDispatchResponseGenerator will first call the HandlerSelectionFeaturesFactory to create the HandlerSelectionFeatures.
A caveat to this is that we save state manager in handler_selection_features, and we may save extra state manager variables throughout our handlers to it.
Then, it will call HandlerDispatcher's dispatch method, which will go through each Handler we have specified for the specific
response generator and call can_handle.  The main variables checked in can_handle are action_name, which describes what kind of 
action will be taken, and nlg_content, which has the information needed to craft responses.  Each combination of action_name and nlg_content
applies to one handler only.  

There are two types of handlers: information retrieval handlers and NLG handlers.  We have set up this template so that
we can dispatch multiple times to information retrieval handlers until we have gotten satisfactory information, and at that point we will
populate the nlg_content variable and send it off to one NLG handler class, each of which serve as an endpoint for various paths.  
These NLG handlers then return the final response for the response generator.

Response Generator Model
^^^^^^^^^^^^^^^^^^^^^^^^

This is the neural response generator.

Team's Custom Response Generator
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You may wish to have response generators with other structures, so feel free to do so!  The handler dispatch response generator
merely serves as a template for those who wish to implement response generators in the same style.