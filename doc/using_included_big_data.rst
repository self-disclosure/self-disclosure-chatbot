.. _using_included_big_data:

Using Included Big Data Module
==============================

This section explains how to use hosted big data modules (DBPedia, Wikipedia) and a YodaQAList Knowledge Graph search module.

.. __: cleansing_data_and_using_annotator.html


---------------------------------------------
Data Modules (DBPedia, Wikipedia, YodaQALite)
---------------------------------------------

The data modules provide example packagings of important databases for retrieval-based and generative question answering and conversational response generator modules. These modules are configured Docker units that allow for online querying and usage from within a ResponseGenerator system.

These modules are not explicitly supported by the Alexa Prize team, but rather are provided as a convenience to teams for reference.

You can refer to sample_cobot/modules.yaml to see how to use these data modules (uncomment the data_modules section in modules.yaml if you want to deploy sample_cobot with these modules). 

.. code-block:: bash

   cobot_modules:
     - greeter

   infrastructure_modules:
     - nlp

   data_modules:
     - dbpedia
     - enwiki
     - apyoda

**Using data modules**

Note that in order to use apyoda, you need to enable dbpedia and enwiki as well under the data_modules section.
It is also highly recommended that you query dbpedia and enwiki through apyoda. 

.. code-block:: bash

    parameters (make sure they are uriencoded):
      - query: The subject you want to query
      - numResults: the number of results you want from enwiki. (dbpedia will always return all results)

    Example: curl -X POST -d 'query=Obama&numResults=4' {apyoda_loadbalancer_url}/getData

    Output:

    {
        "dbpedia": [
            "Natural Person",
            "Office Holder",
            "Writer",
            "Lawyer",
            "Academician",
            "President",
            .
            .
            .
            "Hawaii",
            "Illinois",
            "Presidents Of The United Nations Security Council",
            "Punahou School Alumni",
            "United States Senators From Illinois",
            "Illinois"
        ],
        "enwiki": {
            "numFound": 18302,
            "start": 0,
            "docs": [
                {
                    "id": 1238839,
                    "text": "\nObama of America\nClinton of Obama\n\n",
                    "titleText": "List of state leaders in 1098",
                    "url": "https://en.wikipedia.org/wiki?curid=1238839",
                    "_version_": 1582889171896238091,
                    "score": 4.709094
                },
                {
                    "id": 26460273,
                    "text": "\nObama is a surname.\n\n",
                    "titleText": "Obama (surname)",
                    "url": "https://en.wikipedia.org/wiki?curid=26460273",
                    "_version_": 1582889970507448326,
                    "score": 4.1622906
                }
            ]
        }
    }

