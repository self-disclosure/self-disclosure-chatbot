from pprint import pprint
from cobot_common.service_client import get_client

# Client object to call AlexaPrizeToolkitService
client = get_client(api_key='<API_KEY>', timeout_in_millis=2000)

# Set the natural language text referring to the unresolved entity
mention = {"text": "harry potter"}

# Constrain the search to books
classConstraints = [
    {
        "dataType": "aio:Entity",
        "value": "aio:Book"
    }
]

# Send the request to the entity resolution service
response = client.get_knowledge_entity_resolution(mention=mention, classConstraints=classConstraints)

# Dump the content of the response to standard output
pprint(response)
