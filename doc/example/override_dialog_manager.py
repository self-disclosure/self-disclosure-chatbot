from injector import singleton

import cobot_core as Cobot
# from sample_cobot.sample_events import new_session_event


@singleton
class CustomDialogManager(Cobot.DialogManager):
    def __init__(self):
        pass

    def select_response(self, features):
        return "custom dialog manager"


def overrides(binder):
    binder.bind(Cobot.DialogManager, to=CustomDialogManager)


def lambda_handler(event, context):
    cobot = Cobot.handler(event,
                              context,
                              app_id=None,
                              user_table_name='UserTable',
                              save_before_response=True,
                              overrides=overrides,
                              api_key=None)

    return cobot.execute()

# if __name__ == '__main__':
#     lambda_handler(event=new_session_event, context={})
