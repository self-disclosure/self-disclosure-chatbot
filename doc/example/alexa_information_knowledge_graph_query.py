from pprint import pprint
from cobot_common.service_client import get_client

# Client object to call AlexaPrizeToolkitService
client = get_client(api_key='<API_KEY>', timeout_in_millis=2000)

# Define a query to find who directed the movie Pulp Fiction
query = {"text": "query d|d <aio:directed> <aie:pulp_fiction_movie>"}

# Execute the query against the knowledge graph
response = client.get_knowledge_query_answer(query=query)

# Dump the content of the response to standard output
pprint(response)
