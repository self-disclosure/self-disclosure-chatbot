
Direct super classes:
        * aio:AudioWork

Direct sub classes:
        * aio:Song

Used in the *domain* of the following properties:
        * aio:isAnAlbumTrackOn
        * aio:isATrackWorkBy
        * aio:isASongPerformedBy

Used in the *range* of the following properties:
        * aio:isAnAudioActFeaturedOnTheWork
        * aio:isATrackVersionOf
        * aio:isTheMusicbrainzTrackWorkKeyFor
