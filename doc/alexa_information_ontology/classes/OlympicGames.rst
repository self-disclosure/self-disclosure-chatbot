
Direct super classes:
        * aio:SportingEvent

Direct sub classes:
        * aio:WinterOlympicGames

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
