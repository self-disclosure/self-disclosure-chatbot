
Direct super classes:
        * aio:MusicalAct
        * aio:HumanBeing

Direct sub classes:
        * aio:Vocalist
        * aio:RockMusician
        * aio:MusicGroupMember
        * aio:MusicArranger
        * aio:JazzMusician
        * aio:Instrumentalist
        * aio:Composer

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
