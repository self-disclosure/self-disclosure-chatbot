
Direct super classes:
        * aio:BaseballPlayer

Direct sub classes:
        * aio:RightFielder
        * aio:LeftFielder
        * aio:CenterFielder

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
