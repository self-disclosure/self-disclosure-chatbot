
Direct super classes:
        * aio:Thing

Direct sub classes:
        * aio:Integer

Used in the *domain* of the following properties:
        * aio:isTheImdbScoreFor
        * aio:isTheIqOnAStandardDeviation15TestOf

Used in the *range* of the following properties:
        * *None*
