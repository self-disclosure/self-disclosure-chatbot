
Direct super classes:
        * aio:Organisation

Direct sub classes:
        * aio:SoccerTeam
        * aio:ProfessionalSportsTeam
        * aio:HockeyTeam
        * aio:BasketballTeam
        * aio:BaseballTeam
        * aio:AmericanFootballTeam

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * aio:isTheSportOfTheTeam
        * aio:isTheQuarterbackOf
