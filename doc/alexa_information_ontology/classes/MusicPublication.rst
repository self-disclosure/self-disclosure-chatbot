
Direct super classes:
        * aio:PieceOfMusic
        * aio:AudioPublication

Direct sub classes:
        * aio:SingleTrackMusicPublication
        * aio:ExtendedPlayMusicPublication

Used in the *domain* of the following properties:
        * aio:isAMusicPublicationBy
        * aio:isAnAlbumBy

Used in the *range* of the following properties:
        * aio:isTheRecordLabelThatProduced
