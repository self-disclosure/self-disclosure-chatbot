
Direct super classes:
        * aio:Thing

Direct sub classes:
        * aio:AvmGenre

Used in the *domain* of the following properties:
        * aio:isTheGenreOf

Used in the *range* of the following properties:
        * *None*
