
Direct super classes:
        * aio:VersionOfAPublishedWork

Direct sub classes:
        * aio:TrackVersion
        * aio:SingleVersion
        * aio:AlbumVersion

Used in the *domain* of the following properties:
        * aio:isAnAudioPublicationVersionBy
        * aio:isAnInstrumentalVersionOf

Used in the *range* of the following properties:
        * *None*
