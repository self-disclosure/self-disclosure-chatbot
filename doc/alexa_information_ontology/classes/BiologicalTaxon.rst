
Direct super classes:
        * aio:Thing

Direct sub classes:
        * aio:BiologicalKingdom
        * aio:BiologicalSpecies
        * aio:BiologicalGenus
        * aio:BiologicalFamily
        * aio:BiologicalOrder
        * aio:BiologicalPhylum
        * aio:BiologicalClass

Used in the *domain* of the following properties:
        * aio:isTheStateBirdOf
        * aio:isTheStateFishOf
        * aio:isATaxonUnder
        * aio:isTheStateTreeOf

Used in the *range* of the following properties:
        * aio:isATaxonUnder
