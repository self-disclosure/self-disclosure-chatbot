
Direct super classes:
        * aio:PopulatedPlace

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * aio:isTheStateCapitalOf
        * aio:isTheCapitalOf

Used in the *range* of the following properties:
        * *None*
