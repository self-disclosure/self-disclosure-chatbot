
Direct super classes:
        * aio:AnimatedVisualMedium

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * aio:isAnEpisodeOf

Used in the *range* of the following properties:
        * *None*
