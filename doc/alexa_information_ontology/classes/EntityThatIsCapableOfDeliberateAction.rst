
Direct super classes:
        * aio:Thing

Direct sub classes:
        * aio:MilitaryCombatant

Used in the *domain* of the following properties:
        * aio:built
        * aio:created
        * aio:isACreatorOfTheTvSeries
        * aio:wasMajorlyInvolvedIn
        * aio:wasMajorlyCreativelyInvolvedIn
        * aio:isSomeoneWhoKilledThePerson
        * aio:isBasedIn
        * aio:uses
        * aio:isSomethingThatInvaded

Used in the *range* of the following properties:
        * aio:isTheIqOnAStandardDeviation15TestOf
