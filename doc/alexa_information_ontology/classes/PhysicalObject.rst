
Direct super classes:
        * aio:Thing

Direct sub classes:
        * aio:AstronomicalObject
        * aio:ObjectWithAMass

Used in the *domain* of the following properties:
        * aio:isAComponentOf

Used in the *range* of the following properties:
        * aio:isTheDepthOf
        * aio:isTheTopSpeedOf
        * aio:isTheDensityOfTheObject
        * aio:isTheAverageTemperatureOf
        * aio:isTheVolumeOf
        * aio:shot
        * aio:isAComponentOf
        * aio:wasTheFirstToLandOn
