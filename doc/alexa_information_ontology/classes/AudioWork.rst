
Direct super classes:
        * aio:CreativeWork

Direct sub classes:
        * aio:TrackWork
        * aio:PieceOfMusic
        * aio:AudioPublication

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * aio:isAnInstrumentalVersionOf
