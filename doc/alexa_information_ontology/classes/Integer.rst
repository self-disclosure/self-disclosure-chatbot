
Direct super classes:
        * aio:Number

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * aio:isThePopulationOf

Used in the *range* of the following properties:
        * *None*
