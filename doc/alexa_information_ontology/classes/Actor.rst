
Direct super classes:
        * aio:HumanBeing

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * aio:isAStarIn

Used in the *range* of the following properties:
        * *None*
