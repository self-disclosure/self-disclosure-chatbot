
Direct super classes:
        * aio:Thing

Direct sub classes:
        * aio:CurrencyZone
        * aio:WorldHeritageSite
        * aio:SportsTeamLocation
        * aio:OfficialResidence
        * aio:Stadium
        * aio:State
        * aio:AreaWithinANation
        * aio:PopulatedPlace
        * aio:TimeZoneArea
        * aio:SportsVenue

Used in the *domain* of the following properties:
        * aio:isTheBirthplaceOf
        * aio:isThePlaceOfDeathOf
        * aio:isTheHighestPointIn
        * aio:isATownIn
        * aio:isTheHeadquartersOf
        * aio:isAPlaceSharingABorderWith
        * aio:isGeographicallyLocatedWithinOrIsEqualTo
        * aio:isThePlaceOfOriginOfTheClassOfObjects

Used in the *range* of the following properties:
        * aio:isIllegalIn
        * aio:isThe2LetterIsoCountryCodeFor
        * aio:isAClassOfThingsFrequentlyFoundIn
        * aio:isFilmedIn
        * aio:isTheAverageAnnualRainfallOf
        * aio:built
        * aio:isLegalIn
        * aio:isATownIn
        * aio:isTheMonarchOf
        * aio:hasABranchIn
        * aio:isAPlaceSharingABorderWith
        * aio:isAPoliticalPartyIn
        * aio:represents
        * aio:isAMovieFrom
        * aio:isTheHighestPointIn
        * aio:isThePlaceNameAbbreviationFor
        * aio:isSignificantlySpokenIn
        * aio:isGeographicallyLocatedWithinOrIsEqualTo
        * aio:isBasedIn
        * aio:isTheOfficialCurrencyUsedWithin
        * aio:isSomethingThatInvaded
        * aio:isGeographicallyLocatedWithin
        * aio:isTheLandAreaOf
        * aio:isLivingIn
        * aio:isThePredominantLanguageOf
        * aio:isTheSystemOfGovernmentUsedIn
        * aio:grewUpIn
        * aio:isAWrittenLanguageSignificantlyUsedIn
        * aio:isTheElevationOf
        * aio:isBuriedIn
        * aio:wasFoundedIn
        * aio:isSomethingThatOriginatedInThePlace
        * aio:isAnOfficialLanguageOf
        * aio:wentTo
