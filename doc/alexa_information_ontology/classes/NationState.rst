
Direct super classes:
        * aio:SovereignState

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * aio:isACountryWherePeopleSpeak
        * aio:isANationStateSharingABorderWith

Used in the *range* of the following properties:
        * aio:isTheLeaderOfTheOppositionInLokSabhaIn
        * aio:isANationStateSharingABorderWith
        * aio:isTheLeaderOfTheOppositionInRajyaSabhaIn
        * aio:isTheLeaderOfTheOppositionIn
