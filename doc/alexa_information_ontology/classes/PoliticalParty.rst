
Direct super classes:
        * aio:PoliticalOrganisation
        * aio:MembershipOrganisation

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * aio:isAPoliticalPartyIn

Used in the *range* of the following properties:
        * *None*
