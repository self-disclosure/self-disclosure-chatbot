
Direct super classes:
        * aio:Vocalist

Direct sub classes:
        * aio:TenorSinger
        * aio:SopranoSinger
        * aio:SingerSongwriter
        * aio:Contralto
        * aio:Baritone

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
