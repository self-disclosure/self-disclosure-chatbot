
Direct super classes:
        * aio:Instrumentalist

Direct sub classes:
        * aio:Saxophonist
        * aio:Flautist
        * aio:Clarinetist

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
