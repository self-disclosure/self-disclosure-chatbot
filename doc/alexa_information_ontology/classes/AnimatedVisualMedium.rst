
Direct super classes:
        * aio:PlausiblyActedObject
        * aio:CreativeWork

Direct sub classes:
        * aio:Movie
        * aio:EpisodicTvBroadcast
        * aio:EpisodeOfATvSeries

Used in the *domain* of the following properties:
        * aio:isFilmedIn

Used in the *range* of the following properties:
        * aio:isTheImdbScoreFor
        * aio:isASongFrom
