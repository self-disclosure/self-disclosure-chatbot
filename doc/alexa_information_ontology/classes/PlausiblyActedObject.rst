
Direct super classes:
        * aio:Thing

Direct sub classes:
        * aio:AnimatedVisualMedium

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * aio:isInTheCastOfTheScreenedProduction
        * aio:isAStarIn
        * aio:isStarringIn
        * aio:directed
        * aio:appearedIn
        * aio:actedIn
