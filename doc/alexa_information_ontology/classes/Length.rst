
Direct super classes:
        * aio:Quantity

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * aio:isTheDepthOf
        * aio:isTheHeightOf
        * aio:isTheElevationOf
        * aio:isTheAstronomicalDistanceTo
        * aio:isTheDistanceFromOurSunTo
        * aio:isTheAverageAnnualRainfallOf

Used in the *range* of the following properties:
        * *None*
