
Direct super classes:
        * aio:Organisation

Direct sub classes:
        * aio:SportingGoodsManufacturer
        * aio:ArtsEntertainmentOrRecreationBusiness

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * aio:isTheCfoOf
        * aio:isTheCeoOf
