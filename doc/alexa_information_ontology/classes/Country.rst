
Direct super classes:
        * aio:GeographicalAreaOfSignificantSize

Direct sub classes:
        * aio:SovereignState

Used in the *domain* of the following properties:
        * aio:isTheCountryOfOriginOf
        * aio:hasTheNationalAnthem
        * aio:isTheCountryOfNationalityOf

Used in the *range* of the following properties:
        * aio:isTheNationalAnthemOf
        * aio:isAnIso3166CodeForAllOrPartOf
        * aio:isTheIso3166-1Alpha-2CodeFor
        * aio:isTheVicePresidentOfTheCountry
        * aio:isTheInternationalVehicleRegistrationCodeFor
        * aio:isAEurovisionContestantRepresenting
        * aio:isAnAmbassadorOf
        * aio:isTheQueenOf
        * aio:isACitizenOf
        * aio:isTheKingOf
        * aio:isTheNationalSongOf
        * aio:wasTheFirstPrimeMinisterOf
        * aio:isAFormerPresidentOf
        * aio:isAGovernmentMinisterOf
