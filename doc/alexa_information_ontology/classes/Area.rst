
Direct super classes:
        * aio:Quantity

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * aio:isTheLandAreaOf

Used in the *range* of the following properties:
        * *None*
