
Direct super classes:
        * aio:HumanBeing

Direct sub classes:
        * aio:VicePresident
        * aio:UsPresidentialCandidate
        * aio:UsCongressperson
        * aio:President
        * aio:Mayor
        * aio:Legislator
        * aio:HeadOfGovernment

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
