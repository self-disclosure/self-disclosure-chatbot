
Direct super classes:
        * aio:Thing

Direct sub classes:
        * aio:Pressure
        * aio:Density
        * aio:Temperature
        * aio:Force
        * aio:Speed
        * aio:Frequency
        * aio:SurfaceDensity
        * aio:LuminousIntensity
        * aio:ElectricalResistance
        * aio:PixelCount
        * aio:Hardness
        * aio:Mass
        * aio:Pungency
        * aio:Area
        * aio:MolecularWeight
        * aio:Brightness
        * aio:CalorificValuePerWeightOrVolume
        * aio:Power
        * aio:EnergyPerUnitOfTime
        * aio:MomentOfForce
        * aio:Angle
        * aio:Illumination
        * aio:Voltage
        * aio:Energy
        * aio:DataCapacity
        * aio:Length
        * aio:Concentration
        * aio:VolumeDistanceRatio
        * aio:Volume
        * aio:AmountOfMoney

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * aio:isTheUnitUsedIn
