
Direct super classes:
        * aio:Thing

Direct sub classes:
        * aio:EntityCapableOfDeliberateAction

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * aio:isTheDateOfCreationOf
        * aio:created
        * aio:wasMajorlyCreativelyInvolvedIn
        * aio:isTheOrganisationOperating
        * aio:isTheSloganFor
