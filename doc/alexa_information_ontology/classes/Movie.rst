
Direct super classes:
        * aio:CreativeWork
        * aio:AnimatedVisualMedium

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * aio:isAMovieFrom

Used in the *range* of the following properties:
        * aio:isTheRuntimeOfTheMovie
        * aio:hasAMajorRoleInTheMovie
        * aio:isTheMusicalComposerForTheMovie
