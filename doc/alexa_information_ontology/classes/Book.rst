
Direct super classes:
        * aio:PublishedWrittenWork

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * aio:isABookBy

Used in the *range* of the following properties:
        * *None*
