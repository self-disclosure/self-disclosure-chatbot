
Direct super classes:
        * aio:BiologicalTaxon

Direct sub classes:
        * *None*

Used in the *domain* of the following properties:
        * aio:isAClassInThePhylum

Used in the *range* of the following properties:
        * aio:isAnOrderInTheClass
