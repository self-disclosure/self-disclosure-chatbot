
Direct super classes:
        * aio:AgreementMakingEntity

Direct sub classes:
        * aio:Business
        * aio:PoliticalOrganisation
        * aio:Government
        * aio:GovernmentalOrganisation
        * aio:IntergovernmentalOrganisation
        * aio:MembershipOrganisation
        * aio:SportsTeam
        * aio:SportsGoverningBody
        * aio:SportsLeague

Used in the *domain* of the following properties:
        * aio:hasABranchIn
        * aio:isLikelyToSellMembersOf
        * aio:isTheOrganisationOperating
        * aio:wasFoundedIn

Used in the *range* of the following properties:
        * aio:isAMemberOfTheSeniorManagementTeamOf
        * aio:isTheHeadquartersOf
        * aio:joined
        * aio:isAFounderOf
        * aio:isTheDateOfTheFoundingOf
