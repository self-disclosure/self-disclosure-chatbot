
Direct super classes:
        * aio:HumanBeing

Direct sub classes:
        * aio:TennisPlayer
        * aio:BaseballPlayer
        * aio:CompetingCyclist
        * aio:BasketballPlayer
        * aio:Gymnast
        * aio:BeachVolleyballPlayer
        * aio:Boxer
        * aio:ProfessionalAthlete
        * aio:PoloPlayer
        * aio:Cricketer
        * aio:RugbyPlayer
        * aio:IceHockeyPlayer
        * aio:Goalkeeper
        * aio:SoccerPlayer
        * aio:Footballer
        * aio:OlympicAthlete
        * aio:NationalCollegiateAthleticAssociationAthlete
        * aio:FigureSkater
        * aio:Golfer
        * aio:Wrestler
        * aio:Kickboxer
        * aio:AmericanFootballer
        * aio:TrackAthlete
        * aio:Swimmer
        * aio:AustralianFootballPlayer
        * aio:Skier
        * aio:NascarDriver
        * aio:Jockey
        * aio:HockeyPlayer

Used in the *domain* of the following properties:
        * *None*

Used in the *range* of the following properties:
        * *None*
