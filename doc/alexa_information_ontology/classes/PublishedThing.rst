
Direct super classes:
        * aio:CreativeWork

Direct sub classes:
        * aio:WrittenPublication
        * aio:VersionOfAPublishedWork
        * aio:PublishedWorkOrVersion
        * aio:CollectionOfBooks

Used in the *domain* of the following properties:
        * aio:wasPublishedAtTimepoint
        * aio:isASequelTo
        * aio:isASeriesFirstPublishedAtTimepoint
        * aio:isAvailableInTheFormat

Used in the *range* of the following properties:
        * aio:isASequelTo
