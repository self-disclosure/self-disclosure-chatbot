.. _adding_and_removing_modules:

Adding And Removing Modules
===========================


-------------------
Adding a New Module
-------------------

Let's create a new module in our sample_cobot directory, using the greeter module as our pattern.

You can define your own domain and set of dialog interactions for this new Response Generator module:

.. code-block:: bash

   cd docker
   cp -R greeter/ topic_responses
   cd topic_responses/app
   emacs domain.yml
   emacs models/stories.md
   make train-core
   cd ../../..
   emacs modules.yaml

and change your modules.yaml file to look as follows:

.. code-block:: bash

   cobot_modules:
       - greeter
       - topic_responses

   infrastructure_modules:
       - nlp

Now you've defined a new Docker module in your pipeline, with its own Dockerfile, Flask handler class, and underlying models.

-----------------------------------------
Updating the Stack to Reflect New Modules
-----------------------------------------

You can test this module locally to ensure everything is working properly, but before your new module will start working, you'll need to update your deployment pipelines.

.. code-block:: bash

   cobot update sample_cobot

This will regenerate your Cobot's CloudFormation templates, and update them in your AWS account. The result will be new load balances, clusters in ECS, and a modified CodePipeline to include the new Docker module build and deployment steps.

Any time you add or remove a Cobot Module or Infrastructure Module, you'll need to perform this manual "cobot update" step.