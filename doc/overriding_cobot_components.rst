.. _overriding_cobot_components:

Overriding Cobot Components
===========================


------------------
Modular Philosophy
------------------

Cobot is designed to allow a developer to replace any of the core system components.
Each component has a default implementation, but you can choose to "override" the default implementation with your own custom logic.
These custom classes can extend existing Cobot classes or be written from scratch, but should never require modifying the underlying Cobot code.

Cobot components are modular: you can override individual components independently from the rest of Cobot, and you can mix-and-match default components with custom components.
This modularity is driven by our philosophy of providing a "non-opinionated" toolkit that enables many different dialog and response strategies to be easily implemented, and our goal of advancing the state of science of Conversational AI.

The diagram below illustrates the Cobot logical architecture.
Each large or small box can be overridden, but the overall flow of information is defined by this pipeline.
The following sections provide quick examples of how to override each modular component.

.. image:: images/logical_architecture.jpg

..  _override_global_intent_handler:

--------------------------------
Overriding Global Intent Handler
--------------------------------

The Global Intent Handler runs at the beginning of the Cobot pipeline and generates the first part of a response.
The default Global Intent Handler returns a welcome prompt for Launch Requests and a goodbye prompt for Stop and Cancel intents.
Returning should_end_session = False will run the rest of the Cobot pipeline and append output to the second part of the response.
Returning should_end_session = True will stop the Cobot pipeline and return the Global Intent Handler output as the entire response.
See `Handling Launch Requests <handling_launch_requests.html>`_ for further detail.

1. Create a child class of GlobalIntentHandler.
2. Override the execute method, which takes a JSON event from ASK as input and returns output (string or None) and should_end_session (bool).
3. Bind the Cobot GlobalIntentHandler to your custom class in your_bot.py.

.. code-block:: python

      from cobot_core.global_intent_handler import GlobalIntentHandler
      from cobot_core.prompt_constants import Prompt

      class CustomGlobalIntentHandler(GlobalIntentHandler):
          def execute(self, event):
              request_type = self.state_manager.current_state.request_type
              intent = self.state_manager.current_state.intent
              output = None
              should_end_session = False

              # add or modify global intent handler logic here
              if request_type == 'LaunchRequest':
                  output = Prompt.welcome_prompt 
                            + ' What is your name?' # add custom welcome prompt here
                  should_end_session = False

              if intent in ['AMAZON.StopIntent', 'AMAZON.CancelIntent']:
                  output = Prompt.goodbye_prompt
                  should_end_session = True

              return output, should_end_session

      def overrides(binder):
           binder.bind(GlobalIntentHandler, to=CustomGlobalIntentHandler)

..  _override_asr_processor:

------------------------
Overriding ASR Processor
------------------------

The ASR Processor runs before the NLP feature extractor and can generate a response based on the ASR text.
The default ASR Processor returns a topic redirection prompt if the user utterance contains offensive language.
Returning a string will stop the Cobot pipeline and append the string to the response, and returning None will run the rest of the Cobot pipeline.

1. Create a child class of ASRProcessor.
2. Override the process method, which returns a string or None.
3. Bind the Cobot ASRProcessor to your custom class in your_bot.py.

.. code-block:: python

      from cobot_core.asr_processor import ASRProcessor

      class CustomASRProcessor(ASRProcessor):
          def process(self, input=None):
              text = self.state_manager.current_state.text
              # add your own asr processor logic here, return a string or None
              return None

      def overrides(binder):
          binder.bind(ASRProcessor, to=CustomASRProcessor)

..  _override_offensive_speech_classifier:

--------------------------------------
Overriding Offensive Speech Classifier
--------------------------------------

In the default Cobot, the Offensive Speech Classifier is used in the ASR Processor and the Dialog Manager.
The default Offensive Speech Classifier takes a list of text as input and returns a list of numbers, where each number specifies whether the text at the same index is offensive.
Returning 0 indicates the text is not offensive, and returning 1 indicates the text is offensive.

1. Create a child class of OffensiveSpeechClassifier.
2. Override the classify method, which returns a list of numbers.
3. Bind the Cobot OffensiveSpeechClassifier to your custom class.

.. code-block:: python

      from cobot_core.offensive_speech_classifier import OffensiveSpeechClassifier

      class CustomOffensive(OffensiveSpeechClassifier):
          def classify(self, input_data_list):
              # add your own offensive speech classifier logic here, return a list of numbers
              return [0] * len(input_data_list)

          def overrides(binder):
              binder.bind(OffensiveSpeechClassifier, to=CustomOffensive)

..  _override_feature_extractor:

----------------------------
Overriding Feature Extractor
----------------------------

The Feature Extractor runs after the ASR Processor and extracts relevant attributes to pass to the Dialog Manager.
The default Feature Extractor runs an NLP Pipeline that extracts NLP features and saves them to the state manager.
Return a dictionary of {feature_key: feature_value} to pass those features to the Dialog Manager,
and call setattr(self.state_manager.current_state, feature_key, feature_value) to save a feature to the state manager.

1. Create a child class of FeatureExtractor (which extends Analyzer interface).
2. Override the analyze method, which returns a dict of {str: Any}.
3. Bind the Cobot Analyzer to your custom class.

.. code-block:: python

      from cobot_core.feature_extractor import FeatureExtractor
      from cobot_python_sdk.analyzer import Analyzer

      class CustomFeatureExtractor(FeatureExtractor):
          def analyze(self) -> Optional[Dict]:
              # add your own feature extractor logic here, return a dict of {str: Any}
              return {}

          def overrides(binder):
              binder.bind(Analyzer, to=CustomFeatureExtractor)

..  _override_nlp_modules:

----------------------
Overriding NLP Modules
----------------------

By default, the Feature Extractor runs an NLP Pipeline that contains four independent modules:
sentiment, NER, topic, and intent. Each existing module can be overridden with a custom module of the same name,
and additional modules can be added to the pipeline by specifying a new module name.
The modules can be local or remote, and they can be run in parallel or sequentially in any combination.
See `NLP Pipeline <nlp_pipeline.html>`_ for further detail.

**Local NLP Module**

1. Create a child class of LocalServiceModule.
2. Override the execute function, which returns a string, list, or dict.
3. Define the NLP module config in the lambda handler as a dictionary with required keys name and class. See more optional keys described in `cobot_core.service_module_config <cobot_core.html#module-cobot_core.service_module_config>`_.
4. Run upsert_module to update an existing module with the same name or register a new module to the pipeline.
5. Run create_nlp_pipeline with the name of each module and the module running order.

.. code-block:: python

      from cobot_core.service_module import LocalServiceModule
      class CustomSentiment(LocalServiceModule):
          def execute(self):
              # add your own Sentiment logic here, return a string, list, or dict
              return 'Positive'

      def lambda_handler(event, context):
          cobot = Cobot.handler(event,
                                context,
                                app_id=<APP_ID>,
                                user_table_name='UserTable',
                                save_before_response=True,
                                state_table_name='StateTable',
                                overrides=overrides,
                                api_key=<API_KEY>)
          SENTIMENT = {
              'name': "sentiment",
              'class': CustomSentiment,
              'url': 'local',
              'context_manager_keys': ['text']
          }

          cobot.upsert_module(SENTIMENT)
          cobot.create_nlp_pipeline([["intent", "ner", "sentiment", "topic"]])


**Remote NLP Module**

1. Add your custom NLP module logic to a Docker module in the docker directory of your Cobot. We recommend creating a template using bin/add-response-generator and modifying the get_required_context and handle_message methods in response_generator.py.
2. Define the NLP module config in the lambda handler as a dictionary with required keys name, class, and url. See more optional keys described in `cobot_core.service_module_config <cobot_core.html#module-cobot_core.service_module_config>`_.
3. Run upsert_module to update an existing module with the same name or register a new module to the pipeline.
4. Run create_nlp_pipeline with the name of each module and the module running order.
5. Add the new module name to the cobot_modules section of modules.yaml in your Cobot directory. This name should match the name of your module docker directory.
6. Git push all new module code. You can continue to Step 7 while this push is deploying.
7. Run cobot update to add the new remote module to your Cobot stack.

[Optional]: A remote NLP module can optionally implement a child class of RemoteServiceModule if you want to override RemoteServiceModule's logic.

..  _override_dialog_manager:

-------------------------
Overriding Dialog Manager
-------------------------

The Dialog Manager runs after the Feature Extractor and is responsible for generating the optional second part of a response.
The default Dialog Manager includes a Selecting Strategy, Response Generator Runner, Offensive Filter, and Ranking Strategy.
The resulting response is appended to the GlobalIntentHandler response and passed to the Response Builder.

1. Create a child class of DialogManager.
2. Override the select_response method, which takes a dictionary of features as input and returns response (string) and should_end_session (bool).
3. Bind the Cobot DialogManager to your custom class.

.. code-block:: python

      from cobot_core.dialog_manager import DialogManager

      class CustomDialogManager(DialogManager):
          def select_response(self, features):
              response = ''
              should_end_session = False
              # add your own dialog manager logic here, return (string, bool)
              return response, should_end_session

          def overrides(binder):
              binder.bind(DialogManager, to=CustomDialogManager)

..  _override_selecting_strategy:

-----------------------------
Overriding Selecting Strategy
-----------------------------

The Selecting Strategy runs at the beginning of the Dialog Manager and
chooses which response generators will be run by the Response Generator Runner.
The default Selecting Strategy returns a list of all registered response generators.
You can see two sample selecting strategies: `cobot_core.mapping_selecting_strategy <cobot_core.html#module-cobot_core.mapping_selecting_strategy>`_
and sample_cobot/learned_selecting_strategy.

1. Create a child class of SelectingStrategy.
2. Override the select_response_mode method, which takes a dictionary of features as input and returns a list of strings.
3. Bind the Cobot SelectingStrategy to your custom class.

.. code-block:: python

      from cobot_core.selecting_strategy import SelectingStrategy

      class CustomSelectingStrategy(SelectingStrategy):
          def select_response_mode(self, features:Dict[str,Any])->List[str]:
              # add your own selecting strategy logic here, return a list of strings
              return []

          def overrides(binder):
              binder.bind(SelectingStrategy, to=CustomSelectingStrategy)

..  _override_response_generators_runner:

-------------------------------------
Overriding Response Generators Runner
-------------------------------------

The Response Generators Runner takes the list of response generators from the Selecting Strategy,
runs them, and sends their output to the Offensive Filter. Offensive responses are discarded, and
non-offensive responses are passed to the Ranking Strategy. The default Response Generators Runner
runs all the selected response generators in parallel and saves all the candidate responses,
additional state information, and user attributes to the state manager.
You can see an example CustomResponseGeneratorRunner in sample_cobot/non_trivial_bot.py
that demonstrates how to disable saving candidate responses to the state manager.

1. Create a child class of ResponseGeneratorsRunner.
2. Override the run method, which takes a list of response generator names as input and returns a dictionary of {response_generator_name: response}.
3. Bind the Cobot ResponseGeneratorsRunner to your custom class.

.. code-block:: python

      from cobot_core.nlp.pipeline import ResponseGeneratorsRunner

      class CustomResponseGeneratorsRunner(ResponseGeneratorsRunner):
          def run(self, module_names=None):
              # add your own response generators runner logic here, return a dictionary of responses
              return {}

          def overrides(binder):
              binder.bind(ResponseGeneratorsRunner, to=CustomResponseGeneratorsRunner)

..  _add_response_generators:

--------------------------
Adding Response Generators
--------------------------

Each response generator provides a candidate response that could be appended to the initial response generated by the GlobalIntentHandler.
There are no default response generators; you must add at least one new response generator to your Cobot.
Each response generator can be local or remote. See `Writing A Bot <writing_a_bot.html>`_ and `Expanding Your Bot <expanding_your_bot.html>`_ for several detailed examples.

**Local Response Generator**

1. Create a child class of LocalServiceModule.
2. Override the execute function, which returns a string, list, or dict.
3. Define the response generator config in the lambda handler as a dictionary with required keys name and class. See more optional keys described in `cobot_core.service_module_config <cobot_core.html#module-cobot_core.service_module_config>`_.
4. Run add_response_generators with the new response generator config.

.. code-block:: python

      from cobot_core.service_module import LocalServiceModule
      class EviResponseGenerator(LocalServiceModule):
          def execute(self):
              # add your own Evi response generator logic here, return a string, list, or dict
              return {}


      def lambda_handler(event, context):
          cobot = Cobot.handler(event,
                                context,
                                app_id=<APP_ID>,
                                user_table_name='UserTable',
                                save_before_response=True,
                                state_table_name='StateTable',
                                overrides=overrides,
                                api_key=<API_KEY>)
          EviBot = {
              'name': "EVI",
              'class': EviResponseGenerator,
              'url': 'local',
              'context_manager_keys': ['intent', 'slots']
          }

          cobot.add_response_generators([[EviBot]])


**Remote Response Generator**

1. Add your custom response generator logic to a Docker module in the docker directory of your Cobot. We recommend creating a template using bin/add-response-generator and modifying the get_required_context and handle_message methods in response_generator.py.
2. Define the response generator config in the lambda handler as a dictionary with required keys name, class, and url. See more optional keys described in `cobot_core.service_module_config <cobot_core.html#module-cobot_core.service_module_config>`_.
3. Run add_response_generators with the new response generator config.
4. Add the new response generator name to the cobot_modules section of modules.yaml in your Cobot directory. This name should match the name of your response generator docker directory.
5. Git push all new response generator code. You can continue to Step 6 while this push is deploying.
6. Run cobot update to add the new remote response generator to your Cobot stack.

[Optional]: A remote response generator can optionally implement a child class of RemoteServiceModule if you want to override RemoteServiceModule's logic.

..  _override_ranking_strategy:

---------------------------
Overriding Ranking Strategy
---------------------------

The Ranking Strategy takes the non-offensive responses from the response generators and chooses the single best response.
The default Ranking Strategy just returns the first response in the list.
See sample_cobot/custom_remote_ranking_strategy.py for a detailed example of how to override the Ranking Strategy to a remote module
and how to change some state information and user attributes in a remote ranking strategy.

1. Create a child class of RankingStrategy.
2. Override the rank method, which takes a list of strings as input and returns a string.
3. Bind the Cobot RankingStrategy to your custom class.

.. code-block:: python

      from cobot_core.ranking_strategy import RankingStrategy

      class CustomRankingStrategy(RankingStrategy):
          def rank(self, output_responses):
              # add your own ranking strategy logic here, return a string
              return output_responses[0]

          def overrides(binder):
              binder.bind(RankingStrategy, to=CustomRankingStrategy)

..  _override_response_builder:

---------------------------
Overriding Response Builder
---------------------------

The Response Builder is the last step in the Cobot pipeline and is responsible for packaging and delivery to the Alexa Skills Kit API.
The default Response Builder converts the chosen response to a TTS response for ASK and generates the re-prompt speech.
The class with the most flexibility to customize is the RepromptBuilder, which generates the speech output used to re-prompt the user.
The default RepromptBuilder returns re-prompt speech output that is the same as the response speech output.

1. Create a child class of RepromptBuilder.
2. Override the build method, which takes the response as input and returns a custom re-prompt string.
3. Bind the Cobot RepromptBuilder to your custom class.

.. code-block:: python

      from cobot_core.response_builder import RepromptBuilder

      class CustomRepromptBuilder(RepromptBuilder):
          def build(self, speech_output):
              # add your own re-prompt builder logic here, return a string
              return speech_output

          def overrides(binder):
              binder.bind(RepromptBuilder, to=CustomRepromptBuilder)

..  _customize_state_manager:

-------------------------
Customizing State Manager
-------------------------

The state manager cannot be overridden, but you can use it to store custom context manager keys and user attributes keys.
Refer to the Read API and Write API sections of `State Management And Information Flow <state_management_and_information_flow.html>`_
for examples of how to use custom state manager keys from different types of classes.

