.. _expanding_your_socialbot:

Expanding Your SocialBot
========================

Now that we have a basic working bot deployed and understand the key structure and elements of the Cobot Core library, we will step through a few simple examples of adding capabilties to your cobot.

---------------------------------
Adding a Local Response Generator
---------------------------------

Let's add basic question-answering capabilities to our Greeter bot. We will take advantage of the Evi system to provide answers, and will use a local class, EviResponseGenerator, to retrieve those responses. However, we first need to add a question answering intent ("QAIntent") to our language model.

In cobot-skill/models/en-US.json, you will need to change the "types" value to create a custom slot type, then add a new intent as shown below:

.. code-block:: bash

      "types": [
        {
          "name": "QUESTION_WORD",
          "values": [
                  {
                    "id": "WHO",
                    "name": {
                      "value": "who",
                      "synonyms": []
                     }
                  },
                  {
                    "id": "WHAT",
                    "name": {
                      "value": "what",
                      "synonyms": []
                     }
                  },
                  {
                    "id": "WHEN",
                    "name": {
                      "value": "when",
                      "synonyms": []
                     }
                  },
                  {
                    "id": "WHERE",
                    "name": {
                      "value": "where",
                      "synonyms": []
                     }
                  },
                  {
                    "id": "WHY",
                    "name": {
                      "value": "why",
                      "synonyms": []
                     }
                  }
          ]
        }

        ... [and at the very end of the file]

        ,{
          "name": "QAIntent",
          "slots": [
            {
              "name": "question_word",
              "type": "QUESTION_WORD"
            },
            {
              "name": "question_text",
              "type": "AMAZON.RAW_TEXT"
            }
          ],
          "samples": [
            "{question_word} {question_text}"
          ]
        }

Now, we'll need to edit greeter_bot.py in the sample_cobot directory to add the new response generator class:

.. code-block:: python

    class EviResponseGenerator(LocalServiceModule):
        def execute(self):
	    isWhitelisted = True
            question_text = ''
            if not isWhitelisted:
                intent = self.input_data['intent']
               	slots = self.input_data['slots']
               	question_word = slots['question_word']['value']
               	question_text = question_word+' '+slots['question_text']['value']
            else:
                question_text = self.input_data['text']
                question_text = question_text.replace(' ','_')
            intent = self.input_data['intent']
            slots = self.input_data['slots']
            question_word = slots['question_word']['value']
	    question_text = question_word+' '+slots['question_text']['value']
       	    question_text = question_text.replace(' ','_')
            url = "http://www.evi.com/q/"+question_text
            response = requests.get(url)
            soup = BeautifulSoup(response.text, 'html.parser')
            result = soup.find('div', attrs={'class': 'tk_common'})
            print('result:', result.string.strip())
       	    return result.string.strip()


First, we recommend writing a test for your new response generator:

.. code-block:: python

   #insert unit test here

Make sure your new response generator is working properly by running your test:

.. code-block:: bash

   ../bin/bot-test evi_response_generator_test

Now you've tested your new class, so it's time to modify the add_response_generator call to add a new response generator definition, so your Cobot knows about the new EviResponseGenerator.

.. code-block:: python

    EviBot = {
            'name': "EVI",
            'class': EviResponseGenerator,
            'url': 'local',
            'context_manager_keys': ['intent', 'slots']
    }

    cobot.add_response_generators([RemoteGreeterBot, EviBot])

Now we can push these changes to CodeCommit to test them end-to-end:

.. code-block:: bash

   git add greeter_bot.py cobot-skill/models/en-US.json
   git commit
   git push https://git-codecommit.us-east-1.amazonaws.com/v1/repos/cobot

This will trigger a rebuild of your pipeline and a deployment of your new bot changes.

----------------------------------
Adding a Remote Response Generator
----------------------------------

From the sample_cobot directory, run the following script:

.. code-block:: bash

   ../bin/add-response-generator <rgname> sample_cobot

This script will configure a new response generator Docker module in the docker directory. You'll still need to change the add_response_generators call in your greeter_bot.py, and add a line to cobot_modules in modules.yaml (see `Adding and Removing Modules`__).

.. __: adding_and_removing_modules.html

Your new response generator's code is located in docker/<rgname>/app. In all likelihood, the only code you will need to edit is response_generator.py:

.. code-block:: python

   required_context = ['text']

   def get_required_context():
       return required_context

   def handle_message(msg):
       #your response generator model should operate on the text or other context information here
       input_text = msg['text']
       return input_text

You can add your model setup and code into this response_generator.py file in the handle_message() method. The required_context lists the series of required context items that this remote Response Generator needs to be passed.

Once implemented, the local response generator can be started simply by running:

.. code-block:: bash

   python app.py

Testing this response generator can then be performed from another terminal window by a simple curl command containing a set of required context items for this response generator, such as the following:

.. code-block:: bash

   curl http://0.0.0.0:5001/ --header 'content-type: application/json' --data '{"text": "what is your opinion on Donald Trump", "intent": "opinion_request", "slots": {"role": {"name": "role"}, "topic": {"name": "topic", "value": "donald trump"}}}' -X POST



------------------------
Extending Dialog Manager
------------------------

The DialogManager in the Cobot Toolkit is responsible for mapping intents and state information to ResponseGenerator modules (SelectingStrategy), then ranking the candidate responses to choose a best response (RankingStrategy). Other DialogManager logic flows are possible, however, in practice the behavior can be modified almost entirely through overriding the SelectingStrategy and RankingStrategy classes.

Now that we've added a new ResponseGenerator, we need to make sure it is receiving the QAIntent requests that it can handle well.

We'll use MappingSelectingStrategy, an extension of the base SelectingStrategy interface, to define our mappings from Intents to ResponseGenerators. All you need to
do is change the overrides method as follows and the default DialogManager will use the MappingSelectingStrategy class with the intent_map you define below passed
in to its __init__ method.

.. code-block:: python

   def overrides(binder):
       intent_map = { 'QAIntent':'EVI', 'greet':'GREETER', 'yes_intent':'GREETER', 'topic_request':'GREETER', 'more_info_request':'GREETER',
   		      'no_intent':'GREETER', 'mood_great':'GREETER', 'mood_unhappy':'GREETER', 'love':'GREETER', 'hate':'GREETER', 'opinion_request':'GREETER',
		      'frustration':'GREETER', 'error':'GREETER' }
       binder.bind(IntentMap, to=intent_map)
       binder.bind(Cobot.SelectingStrategy, to=Cobot.MappingSelectingStrategy)

Hopefully the power of this approach is evident.

For example, we could easily create and bind a StickyMappingSelectingStrategy, where conversations tend to stick within particular ResponseGenerators, unless an intent match with another response generator is sufficiently high probability to overcome that stickiness. This would give current response generators "first dibs" on responding to a user utterance, depending on how sticky each configured ResponseGenerator is - and these could be hard-coded or learned values.

We will go into a more involved example later, using AWS Sagemaker and MxNet to learn a dialog management policy, i.e. to infer mappings from intent and state information to response generators.
