.. _handling_launch_requests:

Handling Launch Requests
========================

When a user initiates a conversation, your skill receives a Launch Request event.
Cobot contains special logic to handle these launch requests gracefully.

-----------------------------
Checking for a Launch Request
-----------------------------
Your skill receives one of `three standard request types <https://developer.amazon.com/docs/custom-skills/request-types-reference.html>`_:

* LaunchRequest: Sent when the user invokes your skill.
* IntentRequest: Sent when the user makes a request that corresponds to one of the intents defined in your intent schema.
* SessionEndedRequest: Sent when the current skill session ends for any reason other than your code closing the session.

You can check the request type to determine if your bot received a Launch Request. There are two ways to check the request type.

1. You can retrieve the request type directly from the event:

.. code-block:: python

    request_type = event.get('request.type')
    if request_type == 'LaunchRequest':
        # Handle Launch Request

2. You can retrieve the request type from Cobot's state manager:

.. code-block:: python

    request_type = self.state_manager.current_state.request_type
    if request_type == 'LaunchRequest':
        # Handle Launch Request

Another way to check for a Launch Request is by checking the state manager's intent value. Cobot contains a special intent called LaunchRequestIntent. Whenever Cobot receives a Launch Request, it sets the state manager's intent to LaunchRequestIntent.

.. code-block:: python

    intent = self.state_manager.current_state.intent
    if intent == 'LaunchRequestIntent':
        # Handle Launch Request

----------------------------------------------------
Creating the First Part of a Launch Request Response
----------------------------------------------------
Cobot contains a class called GlobalIntentHandler to handle mandatory global events, including Launch Requests. The Global Intent Handler is the first piece of the Cobot pipeline: it runs before any other components and generates the first part of a response.
The Global Intent Handler can check for a Launch Request using one of the methods above and generate the conversation's opening phrase.
This opening phrase could be the same each time, or it could vary based on attributes in the event or state manager.
The built-in Global Intent Handler returns a default welcome prompt, but you can override the Global Intent Handler by following the example in `Overriding Cobot Components <overriding_cobot_components.html#how-to-override-global-intent-handler>`_.

-----------------------------------------------------
Creating the Second Part of a Launch Request Response
-----------------------------------------------------
After the Global Intent Handler generates the first part of the response, the rest of the Cobot pipeline completes the second part of the response.
You can use one of the checks mentioned above to check for a Launch Request in your Selecting Strategy, Response Generators, and other pipeline components.
There are many possible strategies for handling Launch Requests in the Cobot pipeline...it's up to you to decide how to kick off the conversation!

The Cobot pipeline is a powerful tool, and sometimes things can go wrong. If you are using the default Dialog Manager and encounter an exception during the Cobot pipeline, the second part of the Launch Request response will default to "What would you like to chat about?"

----------------------
Example Implementation
----------------------
While Cobot enables countless possible implementations, here is one simple idea for generating the second part of a Launch Request response:
Add the LaunchRequestIntent to the MappingSelectingStrategy intent map and create a custom LaunchRequestResponseGenerator to randomly select from a list of responses.

.. code-block:: python

    import random
    import cobot_core as Cobot
    from cobot_core.service_module import LocalServiceModule, RemoteServiceModule
    from cobot_core.service_url_loader import ServiceURLLoader


    class LaunchRequestResponseGenerator(LocalServiceModule):
        def execute(self):
            responses = ["How\'s it going?",
                         "What do you wanna chat about?",
                         "It\'s great to meet you. What should we talk about?"]
            return random.choice(responses)


    def overrides(binder):
        intent_map = {'LaunchRequestIntent': 'LAUNCH', 'retrieval': 'RETRIEVAL'}
        binder.bind(Cobot.IntentMap, to=intent_map)
        binder.bind(Cobot.SelectingStrategy, to=Cobot.MappingSelectingStrategy)


    def lambda_handler(event, context):
        cobot = Cobot.handler(event,
                              context,
                              app_id=None,
                              user_table_name='UserTable',
                              save_before_response=True,
                              state_table_name='StateTable',
                              overrides=overrides,
                              api_key=None)

        cobot.create_nlp_pipeline()

        RetrievalBot = {
            'name': "RETRIEVAL",
            'class': RemoteServiceModule,
            'url': ServiceURLLoader.get_url_for_module("RETRIEVAL"),
            'context_manager_keys': ['intent', 'slots', 'text']
        }

        LaunchRequestBot = {
            'name': "LAUNCH",
            'class': LaunchRequestResponseGenerator,
            'url': 'local'
        }
        cobot.add_response_generators([RetrievalBot, LaunchRequestBot])
        return cobot.execute()

----------------
A Note about ASR
----------------
When you invoke a skill directly, the Launch Request does not contain ASR. Similarly, if you use the ASK developer console and use text to interact with your socialbot, your requests will not contain ASR. This can cause errors in the Cobot pipeline if you do not handle the no-ASR case.
For Alexa Prize competitors with skills that are whitelisted for n-best ASR, the Launch Request will contain ASR when you invoke the skill through the "Let's chat" experience (aka Alexa Prize Speechlet).
Your skill will get connected to the Alexa Prize Speechlet during the beta launch, starting mid-April 2018. After launch, invoking the skill directly will still result in Launch Requests with no ASR.
The only way to access ASR for a Launch Request is by going through the Alexa Prize Speechlet.
Once your skill is connected to the speechlet, you will be able to handle user requests for specific conversation topics in the Launch Request, for example: "Let's chat about tennis."
