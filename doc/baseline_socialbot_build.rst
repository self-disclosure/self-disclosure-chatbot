.. _baseline_socialbot_build:

Building Up The Baseline Socialbot
=====================================

This is an in depth guide on building the baseline socialbot for the first time, starting from the sample_cobot example code.  
[your_cobot_directory] will refer to the name you gave to your copy of the sample_cobot directory.
This guide is for those who would like to understand how all the components of the baseline socialbot come together and launch a functioning bot.

-------------
Initial Setup
-------------

1. Make sure your environment is set up and your initial Cobot source has been pulled following the instructions in `Getting Started`__.

.. __: getting_started.html

2. We will also want to make sure you have all the requirements installed.  There are three requirements.txt files you should look at for this step:
sample_cobot/docker/asrcorr/app/requirements.txt
sample_cobot/docker/coref/app/requirements.txt
sample_cobot/docker/nounphrases/app/requirements.txt

You have two options; you can create a conda environment or install everything locally on your computer.

Conda
^^^^^

.. code-block:: bash

    conda create -n cobot-environment python=3.7
    pip install -r [your_cobot_directory]/docker/asrcorr/app/requirements.txt
    pip install -r [your_cobot_directory]/docker/coref/app/requirements.txt
    pip install -r [your_cobot_directory]/docker/nounphrases/app/requirements.txt

Note: If you choose to use a Conda environment, be sure to use pip and not pip3 for installation.  pip3 will install the dependencies to /usr/bin/python 
instead of /anaconda3/bin/python like pip does, which is what we want.

Local
^^^^^

.. code-block:: bash

    pip3 install -r [your_cobot_directory]/docker/asrcorr/app/requirements.txt
    pip3 install -r [your_cobot_directory]/docker/coref/app/requirements.txt
    pip3 install -r [your_cobot_directory]/docker/nounphrases/app/requirements.txt

3. Following the instructions from `the intent model section`__, we can see an example of a custom intent from [your_cobot_directory]/sample_cobot/ask_intent_model_config/baseline_intent_model.json:

.. __: intent_model.html

.. code-block:: bash

    {
      "interactionModel": {
        "languageModel": {
        "invocationName": "baseline socialbot",
        "intents": [
          {
            "name": "InformTopicIntent",
            "slots": [
              {
                "name": "continuer",
                "type": "LIST_OF_CONTINUER"
              },
              {
                "name": "topic",
                "type": "LIST_OF_TOPIC"
              },
              {
                "name": "request_action",
                "type": "LIST_OF_REQUEST_ACTION"
              }
            ],
            "samples": [
              "{request_action} {topic}",
              "{continuer} {request_action} {topic}",
              "{request_action} something about {topic}",
              "{continuer} {request_action} something about {topic}",
              "{request_action} something else about {topic}",
              "{continuer} {request_action} something else about {topic}",
              "{request_action} anything else about {topic}",
              "{continuer} {request_action} anything else about {topic}",
              "{request_action} more about {topic}",
              "{continuer} {request_action} more about {topic}",
              "what can you tell me about {topic}",
              "{continuer} what can you tell me about {topic}",
              "what can you show me about {topic}",
              "{continuer} what can you show me about {topic}",
              "what can you say about {topic}",
              "{continuer} what can you say about {topic}",
              "what do you know about {topic}",
              "{continuer} what do you know about {topic}",
              "what would you say about {topic}",
              "{continuer} what would you say about {topic}",
              "what would you like to say about {topic}",
              "{continuer} what would you like to say about {topic}",
              "what are you going to tell me about {topic}",
              "{continuer} what are you going to tell me about {topic}",
              "what's on your mind about {topic}",
              "{continuer} what's on your mind about {topic}",
              "what do you have to say about {topic}",
              "{continuer} what do you have to say about {topic}",
              "what is the deal with {topic}",
              "{continuer} what is the deal with {topic}",
              "what's the deal with {topic}",
              "{continuer} what's the deal with {topic}"
            ]
          }
        }
      }
    }

4. Ensure that your [your_cobot_directory]/modules.yaml contains the following:

.. code-block:: bash

   cobot_modules:

   infrastructure_modules:
       - nlp

5. There should only be one response generator added, the launch response generator, in baseline_bot.py.

.. code-block:: python

    cobot.add_response_generators([Launch])

6. We will also want to reset the NLP pipeline.

.. code-block:: python

    nlp_def = []

-----------------------------
Adding A New Local NLP Module
-----------------------------

We will be adding the UserResponseModule below.

1. Define your class and inherit from LocalServiceModule as seen in miniskills/custom_nlp/user_response_module/user_response_module.py.  
What you return here will be populated as the value for your module's key under the "features" key in the state manager.

.. code-block:: python

    class UserResponseModule(LocalServiceModule):
        def execute(self):
            return UserResponseInformation()

2. Define your new NLP module in your lambda_handler in [your_cobot_directory]/baseline_bot.py

.. code-block:: python

    USER_RESPONSE = {
        'name': 'user_response_information',
        'class': UserResponseModule,
        'url': 'local',
        'context_manager_keys': ['text']
    }

This module does not depend on any other pipeline output, so we will just use the text from the state manager, the user utterance processed through n-best ASR, under context_manager_keys.
You can access your module's output with self.state_manager.current_state['features']['user_response_information'].

3. Upsert your module and add it to your NLP pipeline

.. code-block:: python

    cobot.upsert_module(USER_RESPONSE)
    nlp_def = [["user_response_information"]]

Since this will be the first module you create, you will want to create the NLP pipeline as well.  This only needs to be done once.
As we add to the NLP pipeline, we will merely update nlp_def.

.. code-block:: python

    cobot.create_nlp_pipeline(nlp_def)

4. We can now test this.  In the ASK console, we can type "I agree" after triggering the skill.  Even as the skill fails, we can see that
user_side: AGREE comes up in the current state, which is the expected behavior.  We will be able to see the use case of this module later in the tutorial.

---------------------------
Overriding The Intent Model
---------------------------

Overriding the intent model is very similar to adding a local NLP module.

1. Define your class and inherit from IntentClassifier as seen in miniskills/custom_nlp/corrected_ask_intent_classifier.
We will be populating a new field in the state table, 'corrected_ask_intent', which will be the intent used in our selecting strategy.

.. code-block:: python

    class CorrectedAskIntentClassifier(IntentClassifier):
        def execute(self):
            self.state_manager.current_state.set_new_field('corrected_ask_intent', None)
            self.state_manager.current_state.corrected_ask_intent = self.state_manager.current_state.intent

2. Define the Intent module in your lambda_handler in [your_cobot_directory]/baseline_bot.py

.. code-block:: python

    INTENT = {
        'name': 'intent',
        'class': CorrectedAskIntentClassifier,
        'url': 'local',
        'context_manager_keys': ['text', 'user_response_information']
    }

3. Add the Intent module

.. code-block:: python

    cobot.upsert_module(INTENT)

4. Update nlp_def

.. code-block:: python

    nlp_def = [["user_response_information"], ["intent"]]

5. We can now test this.  In the ASK console, we can type "What is your opinion on Taylor Swift" after triggering the skill.  
The skill will continue to fail since we do not have a selecting strategy, but we can see in the json input on the right side of the console that the utterance
has indeed been classified as RequestOpinionIntent.

---------------------------------
Overriding The Selecting Strategy
---------------------------------

We will now override the default selecting strategy with one of our own.

1. Define your class and inherit from SelectingStrategy as seen in miniskills/custom_nlp/custom_selecting_strategy.
We overwrite select_response_mode with our own selecting strategy.

.. code-block:: python

    class CustomSelectingStrategy(SelectingStrategy):
        def select_response_mode(self, features:Dict[str,Any])->List[str]:
            return None

2. Add the following under the overrides method in [your_cobot_directory]/baseline_bot.py:

.. code-block:: python

    def overrides(binder):
        binder.bind(Cobot.SelectingStrategy, to=CustomSelectingStrategy)

This replaces the default selecting strategy with our own.

---------------------------------
Adding Global Response Generators
---------------------------------

At this point, we can add in our other global response generators.

1. Add the response generators in [your_cobot_directory]/baseline_bot.py

.. code-block:: python

    cobot.add_response_generators([Launch, Greeting, Goodbye, Invalid])

2. In the ASK console, try typing "good how about you?" after the bot is triggered.
The expected response will be something along the lines of "Glad to hear it!  I'm doing well.  I can share facts or opinions!  What would you like to talk about?"
This is the greeting module that deals with the user's first turn if they choose to respond to the bot in this way.

-------------------------------
Adding The Evi Knowledge Module
-------------------------------

This knowledge module pulls information from Evi, the question/answer service.
We have provided it here as a module in the NLP pipeline so all miniskills may access the information.
The knowledge module is treated the same as a local NLP module in terms of integration.

1. Define your class and inherit from ToolkitServiceModule as seen in miniskills/custom_nlp/evi_module/evi_module.py.

.. code-block:: python

    class EviModule(ToolkitServiceModule):
        def execute(self):
            return None

2. Define your new knowledge module in your lambda_handler in [your_cobot_directory]/baseline_bot.py.

.. code-block:: python

    EVI = {
        'name': 'evi',
        'class': EviModule,
        'url': 'local',
        'context_manager_keys': ['text', 'intent']
    }

3. Upsert your module and add it to your NLP pipeline

.. code-block:: python

    cobot.upsert_module(EVI)
    nlp_def = [["user_response_information"], ["intent"], ["evi"]]

4. We can test this by typing "who is Taylor Swift" in the ASK console.  Even though the bot will throw an error, 
looking in CloudWatch logs, we can see that under the "features" key in state manager, the "evi" field has been populated with the response fetched from Evi.
We have several different ways of pulling knowledge from Evi.  This module returns a dictionary with three keys, 'asr_text', 'current_topic_from_slots', and 'old_topic'.

'asr_text' will have the information pulled by trying to query Evi with the entire text given by ASR.  

'current_topic_from_slots' will have the information pulled by trying to query Evi with a given question word and the topic from slots, or with the topic from slots assumed to be a person and adding 'who is' to the Evi query.

'old_topic' will have the information pulled by taking last state's topic and querying as if it were a person.  This field is mostly used in the Movies miniskill.

---------------------------------
Adding A Local Response Generator
---------------------------------

Below, we will be adding the ShareFacts response generator, which obtains its information from r/todayilearned.

1. Define your class and inherit from LocalServiceModule.  In this case, we have a generic response generator called HandlerDispatchResponseGenerator, 
found in miniskills/handler_dispatch_generic_response_generator/handler_dispatch_response_generator.py.  This inherits directly from LocalServiceModule, 
and is the basis for all miniskills wishing to use the handler dispatch system.

.. code-block:: python

    class HandlerDispatchResponseGenerator(LocalServiceModule):
        def execute(self):
            return None

ShareFactsResponseGenerator, found in miniskills/share_facts/share_facts_response_generator.py, inherits from this HandlerDispatchResponseGenerator class.

.. code-block:: python

    class ShareFactsResponseGenerator(HandlerDispatchResponseGenerator):
        def execute(self):
            return None

2. Define your response generator in your lambda_handler in [your_cobot_directory]/baseline_bot.py

.. code-block:: python

    ShareFacts = {
        'name': 'SHAREFACTS',
        'class': ShareFactsResponseGenerator,
        'url': 'local'
    }

3. Finally, add the bot to cobot.

.. code-block:: python

    cobot.add_response_generators([Launch, Greeting, Goodbye, Invalid, ShareFacts])

4. We can now test this in the ASK console by typing "give me a fact about Taylor Swift".  The expected response
will be a fact about Taylor Swift.  

We can also now test user response information.  If we say "tell me more" after 
the previous utterance, the expected response will be another fact about Taylor Swift, since we have expressed a "positive user satisfaction"
according to our rules for user response information.

----------------------
Adding A Remote Module
----------------------

We will be adding NounPhrases as the module of choice.

1. The first step to add a remote module is to set up the docker directory where your remote module will live. 
In this tutorial we already have our remote module directory set up. 
For reference, the following command was used to set up the nounphrases docker directory, but for this tutorial you do not need to run this command again.

.. code-block:: bash

    add-response-generator nounphrases [your_cobot_directory]

You will see a few files in the directory.

app.py runs your module.

quick-test is used in cobot quick-test to test your module locally.

requirements.txt has all the dependencies needed.  You can add additional dependencies here (we have added spacy).

response_generator.py has the bulk of your logic.  handle_message(msg) is run when your module is called.

test_script has an example state passed in that is utilized during local testing.

wsgi.py runs app.py.

2. Define your remote module in your lambda_handler in [your_cobot_directory]/baseline_bot.py.
We want to access the last state's user utterance and bot response too, so we will set history_turns to 1.  
We will receive both 'text', and 'response' as lists with the first index being the most recent turn.
The first index of 'response' will always be empty, as the bot has not created a response for the current turn.

.. code-block:: python

    NOUN_PHRASES = {
        'name': 'nounphrases',
        'class': NounPhraseExtractionModule,
        'url': ServiceURLLoader.get_url_for_module("nounphrases"),
        'context_manager_keys': ['text', 'response'],
        'history_turns': 1
    }

3. Add the module into the NLP pipeline

.. code-block:: python

    cobot.upsert_module(NOUN_PHRASES)
    nlp_def = [["nounphrases"], ["user_response_information"], ["intent"], ["evi"]]

4. Add the module into [your_cobot_directory]/modules.yaml

.. code-block:: bash

   cobot_modules:
       - nounphrases

   infrastructure_modules:
       - nlp

5. We will then want to run two commands, git push and cobot update.  We will be pushing to the codecommit repository, which when pushed to, 
will trigger the CodePipeline.  Whenever a change is made in the remote module, be sure to push to codecommit or else your changes will not be propagated to the bot.
You cannot use the update_lambda script.  Instead, to test quickly, use cobot quick-test.  You should run cobot update only once to create the stack.

.. code-block:: bash

    cobot update [your_cobot_directory]

6. We can test this by typing "give me a fact about Taylor Swift" in the ASK console.  You will see under the "features" key 
in the state manager that the noun phrases have been filled in under the "nounphrases" key.

-------------------------------
Adding The Remainder Of The Bot
-------------------------------

Coreference Module
^^^^^^^^^^^^^^^^^^

1. Coreference Module is a remote module like nounphrases, and will be added the same way.  We will define the module in the lambda_handler in [your_cobot_directory]/baseline_bot.py.
Similar to nounphrases, we will want the last state's user utterance and response, so we will set history_turns to 1.

.. code-block:: python

    COREF = {
        'name': "coref",
        'class': CoreferenceExtractionModule,
        'url': ServiceURLLoader.get_url_for_module("coref"),
        'context_manager_keys': ['text', 'response'],
        'history_turns': 1
    }

2. Add the module into the NLP pipeline

.. code-block:: python

    cobot.upsert_module(COREF)
    nlp_def = [["nounphrases"], ["coref"], ["user_response_information"], ["intent"], ["evi"]]

3. Add the module into [your_cobot_directory]/modules.yaml

.. code-block:: python 

    cobot_modules:
        - nounphrases
        - coref

    infrastructure_modules:
        - nlp

4. Again, we will run git push and cobot update.

.. code-block:: bash

    cobot update [your_cobot_directory]

5. We can test this with cobot quick-test with the example script provided without having to wait for the pipeline:

.. code-block:: bash

    cobot quick-test [your_cobot_directory] coref

This should give a result of [["taylor swift", "It", "Taylor Swift"]].
We can access this module's output in our code through the "coref" key under the "features" key in the state manager.

ASR Correction Module
^^^^^^^^^^^^^^^^^^^^^

1. The ASR Correction Module is also a remote module, but has slightly different inputs from the others.  We will be using another of our remote modules,
nounphrases, as input to this module, and making use of the numbers given in the ASR parsing.  We will once again define the module in the lambda_handler in [your_cobot_directory]/baseline_bot.py.

.. code-block:: python

    ASR_CORRECTION = {
        'name': "asrcorr",
        'class': AsrCorrectionModule,
        'url': ServiceURLLoader.get_url_for_module("asrcorr"),
        'context_manager_keys': ['text', 'nounphrases', 'asr']
    }

2. Add the module into the NLP pipeline

.. code-block:: python

    cobot.upsert_module(ASR_CORRECTION)
    nlp_def = [["nounphrases"], ["coref", "asrcorr"], ["user_response_information"], ["intent"], ["evi"]]

3. Add the module into [your_cobot_directory]/modules.yaml

.. code-block:: python

    cobot_modules:
        - nounphrases
        - coref
        - asrcorr

    infrastructure_modules:
        - nlp

4. Again, we will run git push and cobot update

.. code-block:: bash

    cobot update [your_cobot_directory]

5. We can test this with cobot quick-test with the example script provided without having to wait for the pipeline:

.. code-block:: bash

    cobot quick-test [your_cobot_directory] asrcorr

This should give a result of ['crazy rich asian': ['crazy rich asians']], which means that we have mapped "crazy rich asian" as the movie "crazy rich asians".
We can access this module's output in our code through the "asrcorr" key under the "features" key in the state manager.

Knowledge Module
^^^^^^^^^^^^^^^^

This is the generic knowledge module, which currently pulls information from r/todayilearned based on the topic given.  We can treat this in a similar way to the Evi knowledge module
except this knowledge module inherits from LocalServiceModule.

1. Define your class and inherit from LocalServiceModule as seen in miniskills/custom_nlp/knowledge_module/knowledge_module.py

.. code-block:: python

    class KnowledgeModule(LocalServiceModule):
        def execute(self):
            return None

2. Define your new knowledge module in your lambda_handler in [your_cobot_directory]/baseline_bot.py

.. code-block:: python

    KNOWLEDGE = {
        'name': "knowledge",
        'class': KnowledgeModule,
        'url': 'local',
        'context_manager_keys': ['text', 'user_response_information']
    }

3. Upsert your module and add it to your NLP pipeline

.. code-block:: bash

    cobot.upsert_module(KNOWLEDGE)
    nlp_def = [["nounphrases"], ["coref", "asrcorr"], ["user_response_information"], ["intent"], ["evi", "knowledge"]]

4. We can test this by typing "give me an opinion on Taylor Swift" in the ASK console.  Even though this will route to the miniskill that will pull from r/showerthoughts,
we can see that under the "knowledge" key in the "features" key under state manager that knowledge from r/todayilearned has been pulled.

All Other Response Generators
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Adding the other response generators are very simple, just add them to the response generator list.

.. code-block:: python

    cobot.add_response_generators([ShareFacts, Launch, Invalid, SpecialUserUtterance, ShowerThoughts, News, Greeting, Goodbye, Movies])