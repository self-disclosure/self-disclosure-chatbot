��"\      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �target���)��}�(h� .. _overriding_cobot_components:�h]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��refid��overriding-cobot-components�u�tagname�h	�line�K�parent�hhh�source��=/Users/raeferg/cobot_base/doc/overriding_cobot_components.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Overriding Cobot Components�h]�h �Text����Overriding Cobot Components�����}�(hh+hh)hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh$hhh h!hKubh#)��}�(hhh]�(h()��}�(h�Modular Philosophy�h]�h.�Modular Philosophy�����}�(hh>hh<hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh9hhh h!hK	ubh �	paragraph���)��}�(hX9  Cobot is designed to allow a developer to replace any of the core system components, in addition to adding response generators and dialog management functions. For a walkthrough of adding response generator logic, as well as overriding SelectingStrategy and RankingStrategy components, see `Expanding Your Bot`__.�h]�(h.X"  Cobot is designed to allow a developer to replace any of the core system components, in addition to adding response generators and dialog management functions. For a walkthrough of adding response generator logic, as well as overriding SelectingStrategy and RankingStrategy components, see �����}�(hX"  Cobot is designed to allow a developer to replace any of the core system components, in addition to adding response generators and dialog management functions. For a walkthrough of adding response generator logic, as well as overriding SelectingStrategy and RankingStrategy components, see �hhLhhh NhNubh �	reference���)��}�(h�`Expanding Your Bot`__�h]�h.�Expanding Your Bot�����}�(hhhhWubah}�(h]�h]�h]�h]�h]��name��Expanding Your Bot��	anonymous�K�refuri��expanding_your_bot.html�uhhUhhL�resolved�Kubh.�.�����}�(h�.�hhLhhh NhNubeh}�(h]�h]�h]�h]�h]�uhhJh h!hKhh9hhubhK)��}�(h��This modularity is driven by our philosophy of providing a "non-opinionated" toolkit that enables many different dialog and response strategies to be easily implemented, and our goal of advancing the state of science of Conversational AI.�h]�h.��This modularity is driven by our philosophy of providing a “non-opinionated” toolkit that enables many different dialog and response strategies to be easily implemented, and our goal of advancing the state of science of Conversational AI.�����}�(hhxhhvhhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKhh9hhubh
)��}�(h�.. __: expanding_your_bot.html�h]�h}�(h]��id2�ah]�h]�h]�h]�hhhihgKuhh	hKhh9hhh h!�
referenced�Kubeh}�(h]��modular-philosophy�ah]�h]��modular philosophy�ah]�h]�uhh"hh$hhh h!hK	ubh#)��}�(hhh]�(h()��}�(h�/How to Override the Offensive Speech Classifier�h]�h.�/How to Override the Offensive Speech Classifier�����}�(hh�hh�hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh�hhh h!hKubhK)��}�(h�SIn the default Cobot handler class, OffensiveSpeechClassifier is used in 2 classes.�h]�h.�SIn the default Cobot handler class, OffensiveSpeechClassifier is used in 2 classes.�����}�(hh�hh�hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKhh�hhubh �enumerated_list���)��}�(hhh]�(h �	list_item���)��}�(h�UASR Processor. If the ASK request contains offensive speech, it will redirect topics.�h]�hK)��}�(hh�h]�h.�UASR Processor. If the ASK request contains offensive speech, it will redirect topics.�����}�(hh�hh�ubah}�(h]�h]�h]�h]�h]�uhhJh h!hKhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhh h!hNubh�)��}�(h�lDialog Manager. It will filter out offensive candidate responses after running all the response generators.
�h]�hK)��}�(h�kDialog Manager. It will filter out offensive candidate responses after running all the response generators.�h]�h.�kDialog Manager. It will filter out offensive candidate responses after running all the response generators.�����}�(hh�hh�ubah}�(h]�h]�h]�h]�h]�uhhJh h!hKhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhh h!hNubeh}�(h]�h]�h]�h]�h]��enumtype��arabic��prefix�h�suffix�houhh�hh�hhh h!hKubh �literal_block���)��}�(hX�  class CustomOffensive(OffensiveSpeechClassifier):
    def classify(self, input_data_list):
        """
        classify whether the texts in the input_data_list are offensive, 0: not-offensive, 1: offensive. This method is used in default ASRProcessor and DialogManager.
        :return: List of Number
        """
        return [0] * len(input_data_list)

def overrides(binder):
    binder.bind(OffensiveSpeechClassifier, to=CustomOffensive)�h]�h.X�  class CustomOffensive(OffensiveSpeechClassifier):
    def classify(self, input_data_list):
        """
        classify whether the texts in the input_data_list are offensive, 0: not-offensive, 1: offensive. This method is used in default ASRProcessor and DialogManager.
        :return: List of Number
        """
        return [0] * len(input_data_list)

def overrides(binder):
    binder.bind(OffensiveSpeechClassifier, to=CustomOffensive)�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]��	xml:space��preserve��language��python��linenos���highlight_args�}�uhh�h h!hKhh�hhubeh}�(h]��/how-to-override-the-offensive-speech-classifier�ah]�h]��/how to override the offensive speech classifier�ah]�h]�uhh"hh$hhh h!hKubh#)��}�(hhh]�(h()��}�(h�&How to Override and Add NLP Components�h]�h.�&How to Override and Add NLP Components�����}�(hj  hj  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hj  hhh h!hK*ubhK)��}�(h�VBy default, NLP Pipeline has 4 independent modules: sentiment, NER, topic, and intent.�h]�h.�VBy default, NLP Pipeline has 4 independent modules: sentiment, NER, topic, and intent.�����}�(hj)  hj'  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hK,hj  hhubhK)��}�(h�To override an existing module,�h]�h.�To override an existing module,�����}�(hj7  hj5  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hK.hj  hhubhK)��}�(h�eNote: the 'name' value must be one of ["sentiment", “ner”, “topic”, “intent”], lowercase.�h]�h.�mNote: the ‘name’ value must be one of [“sentiment”, “ner”, “topic”, “intent”], lowercase.�����}�(hjE  hjC  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hK0hj  hhubh�)��}�(hXb  class CustomSentiment(Sentiment):
    def execute(self):
        return 'Positive'

def lambda_handler(event, context):
    # app_id: replace with your ASK skill id to validate ask request. None means skipping ASK request validation.
    # user_table_name: replace with a DynamoDB table name to store user preference data. We will auto create the
    #                  DynamoDB table if the table name doesn’t exist.
    #                  None means user preference data won’t be persisted in DynamoDB.
    # save_before_response: If it is true, skill persists user preference data at the end of each turn.
    #                       Otherwise, only at the last turn of whole session.
    # state_table_name: replace with a DynamoDB table name to store session state data. We will auto create the
    #                   DynamoDB table if the table name doesn’t exist.
    #                   None means session state data won’t be persisted in DynamoDB.
    # overrides: provide custom override for dialog manager components.
    # api_key: replace with your api key
    cobot = Cobot.handler(event,
                          context,
                          app_id=<APP_ID>,
                          user_table_name='UserTable',
                          save_before_response=True,
                          state_table_name='StateTable',
                          overrides=overrides,
                          api_key=<API_KEY>)
    SENTIMENT = {
        'name': "sentiment",
        'class': CustomSentiment,
        'url': 'local',
        'context_manager_keys': ['text']
    }

    cobot.upsert_module(SENTIMENT)�h]�h.Xb  class CustomSentiment(Sentiment):
    def execute(self):
        return 'Positive'

def lambda_handler(event, context):
    # app_id: replace with your ASK skill id to validate ask request. None means skipping ASK request validation.
    # user_table_name: replace with a DynamoDB table name to store user preference data. We will auto create the
    #                  DynamoDB table if the table name doesn’t exist.
    #                  None means user preference data won’t be persisted in DynamoDB.
    # save_before_response: If it is true, skill persists user preference data at the end of each turn.
    #                       Otherwise, only at the last turn of whole session.
    # state_table_name: replace with a DynamoDB table name to store session state data. We will auto create the
    #                   DynamoDB table if the table name doesn’t exist.
    #                   None means session state data won’t be persisted in DynamoDB.
    # overrides: provide custom override for dialog manager components.
    # api_key: replace with your api key
    cobot = Cobot.handler(event,
                          context,
                          app_id=<APP_ID>,
                          user_table_name='UserTable',
                          save_before_response=True,
                          state_table_name='StateTable',
                          overrides=overrides,
                          api_key=<API_KEY>)
    SENTIMENT = {
        'name': "sentiment",
        'class': CustomSentiment,
        'url': 'local',
        'context_manager_keys': ['text']
    }

    cobot.upsert_module(SENTIMENT)�����}�(hhhjQ  ubah}�(h]�h]�h]�h]�h]�j  j  j	  �python�j  �j  }�uhh�h h!hK2hj  hhubeh}�(h]��&how-to-override-and-add-nlp-components�ah]�h]��&how to override and add nlp components�ah]�h]�uhh"hh$hhh h!hK*ubh#)��}�(hhh]�(h()��}�(h�'Adding a New Module to the NLP Pipeline�h]�h.�'Adding a New Module to the NLP Pipeline�����}�(hjn  hjl  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hji  hhh h!hKXubhK)��}�(h�bIt is also possible to add a completely new module to the Cobot NLP Pipeline, with minimal effort.�h]�h.�bIt is also possible to add a completely new module to the Cobot NLP Pipeline, with minimal effort.�����}�(hj|  hjz  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKZhji  hhubhK)��}�(h�uNote: the 'name' value in the COREFERENCE descriptor must be the same string passed into cobot.create_nlp_pipeline().�h]�h.�yNote: the ‘name’ value in the COREFERENCE descriptor must be the same string passed into cobot.create_nlp_pipeline().�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hK\hji  hhubh�)��}�(hXd  class Coreference(ServiceModule):
    def execute(self):
        # add your own coreference resolution logic here, using input_data['text'][0..2]
        # this could also easily be added to the NLP remote module
        pass

def lambda_handler(event, context):
    # app_id: replace with your ASK skill id to validate ask request. None means skipping ASK request validation.
    # user_table_name: replace with a DynamoDB table name to store user preference data. We will auto create the
    #                  DynamoDB table if the table name doesn’t exist.
    #                  None means user preference data won’t be persisted in DynamoDB.
    # save_before_response: If it is true, skill persists user preference data at the end of each turn.
    #                       Otherwise, only at the last turn of whole session.
    # state_table_name: replace with a DynamoDB table name to store session state data. We will auto create the
    #                   DynamoDB table if the table name doesn’t exist.
    #                   None means session state data won’t be persisted in DynamoDB.
    # overrides: provide custom override for dialog manager components.
    # api_key: replace with your api key
    cobot = Cobot.handler(event,
                          context,
                          app_id=<APP_ID>,
                          user_table_name='UserTable',
                          save_before_response=True,
                          state_table_name='StateTable',
                          overrides=overrides,
                          api_key=<API_KEY>)
    COREFERENCE = {
        'name': "coreference",
        'class': Coreference,
        'url': 'local',
        'context_manager_keys': ['text'],
        'history_turns': 2
    }
    cobot.upsert_module(COREFERENCE)
    cobot.create_nlp_pipeline(["intent", "ner", "sentiment", "topic", "coreference"])�h]�h.Xd  class Coreference(ServiceModule):
    def execute(self):
        # add your own coreference resolution logic here, using input_data['text'][0..2]
        # this could also easily be added to the NLP remote module
        pass

def lambda_handler(event, context):
    # app_id: replace with your ASK skill id to validate ask request. None means skipping ASK request validation.
    # user_table_name: replace with a DynamoDB table name to store user preference data. We will auto create the
    #                  DynamoDB table if the table name doesn’t exist.
    #                  None means user preference data won’t be persisted in DynamoDB.
    # save_before_response: If it is true, skill persists user preference data at the end of each turn.
    #                       Otherwise, only at the last turn of whole session.
    # state_table_name: replace with a DynamoDB table name to store session state data. We will auto create the
    #                   DynamoDB table if the table name doesn’t exist.
    #                   None means session state data won’t be persisted in DynamoDB.
    # overrides: provide custom override for dialog manager components.
    # api_key: replace with your api key
    cobot = Cobot.handler(event,
                          context,
                          app_id=<APP_ID>,
                          user_table_name='UserTable',
                          save_before_response=True,
                          state_table_name='StateTable',
                          overrides=overrides,
                          api_key=<API_KEY>)
    COREFERENCE = {
        'name': "coreference",
        'class': Coreference,
        'url': 'local',
        'context_manager_keys': ['text'],
        'history_turns': 2
    }
    cobot.upsert_module(COREFERENCE)
    cobot.create_nlp_pipeline(["intent", "ner", "sentiment", "topic", "coreference"])�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j  j  j	  �python�j  �j  }�uhh�h h!hK^hji  hhubhK)��}�(h��If, instead, you want to extend the remote NLP module to use a model for coreference resolution, this can be accomplished as follows:�h]�h.��If, instead, you want to extend the remote NLP module to use a model for coreference resolution, this can be accomplished as follows:�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hK�hji  hhubh�)��}�(hX  class Coreference(RemoteServiceModule):
   """
   Coreference resolution base class
   """


def lambda_handler(self, event, context):
     ...

    COREFERENCE = {
             'name': "coreference",
             'class': Coreference,
             'url': ServiceURLLoader.get_url_for_module("nlp") + "coreference",
             'context_manager_keys': ['text'],
             'history_turns': 2
    }

    cobot.upsert_module(COREFERENCE)
    cobot.create_nlp_pipeline(["intent", "ner", "sentiment", "topic", "coreference"])�h]�h.X  class Coreference(RemoteServiceModule):
   """
   Coreference resolution base class
   """


def lambda_handler(self, event, context):
     ...

    COREFERENCE = {
             'name': "coreference",
             'class': Coreference,
             'url': ServiceURLLoader.get_url_for_module("nlp") + "coreference",
             'context_manager_keys': ['text'],
             'history_turns': 2
    }

    cobot.upsert_module(COREFERENCE)
    cobot.create_nlp_pipeline(["intent", "ner", "sentiment", "topic", "coreference"])�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j  j  j	  �python�j  �j  }�uhh�h h!hK�hji  hhubhK)��}�(h��This would then be followed by adding a method into the nlp module in cobot_common (located in cobot_common/docker/nlp/app/app.py):�h]�h.��This would then be followed by adding a method into the nlp module in cobot_common (located in cobot_common/docker/nlp/app/app.py):�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hK�hji  hhubh�)��}�(hX     class Coreference(Default):

       def post(self):
           return super().post()

       def analyze(self, text):
           ret = {'result': {}}
           ret['result'] = ...
           return ret
...

     api.add_resource(Coreference, '/coreference')�h]�h.X     class Coreference(Default):

       def post(self):
           return super().post()

       def analyze(self, text):
           ret = {'result': {}}
           ret['result'] = ...
           return ret
...

     api.add_resource(Coreference, '/coreference')�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j  j  j	  �python�j  �j  }�uhh�h h!hK�hji  hhubeh}�(h]��'adding-a-new-module-to-the-nlp-pipeline�ah]�h]��'adding a new module to the nlp pipeline�ah]�h]�uhh"hh$hhh h!hKXubeh}�(h]�(h�id1�eh]�h]�(�overriding cobot components��overriding_cobot_components�eh]�h]�uhh"hhhhh h!hK�expect_referenced_by_name�}�j�  hs�expect_referenced_by_id�}�hhsubeh}�(h]�h]�h]�h]�h]��source�h!uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h'N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h!�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�N�character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}�h]�has�nameids�}�(j�  hj�  j�  h�h�j  j  jf  jc  j�  j�  u�	nametypes�}�(j�  �j�  Nh�Nj  Njf  Nj�  Nuh}�(hh$j�  h$h�h9h�h�j  h�jc  j  j�  ji  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�(h �system_message���)��}�(hhh]�(hK)��}�(h�Title underline too short.�h]�h.�Title underline too short.�����}�(hhhjw  ubah}�(h]�h]�h]�h]�h]�uhhJhjt  ubh�)��}�(h�)Overriding Cobot Components
=============�h]�h.�)Overriding Cobot Components
=============�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j  j  uhh�hjt  ubeh}�(h]�h]�h]�h]�h]��level�K�type��WARNING��line�K�source�h!uhjr  hh$hhh h!hKubjs  )��}�(hhh]�(hK)��}�(h�Title overline too short.�h]�h.�Title overline too short.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhJhj�  ubh�)��}�(h�6-----------------
Modular Philosophy
-----------------�h]�h.�6-----------------
Modular Philosophy
-----------------�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j  j  uhh�hj�  ubeh}�(h]�h]�h]�h]�h]��level�K�type�j�  �line�K�source�h!uhjr  hh9hhh h!hK	ubjs  )��}�(hhh]�(hK)��}�(hhh]�h.�Title overline too short.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhJhj�  ubh�)��}�(h�Y--------------------
How to Override the Offensive Speech Classifier
--------------------�h]�h.�Y--------------------
How to Override the Offensive Speech Classifier
--------------------�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j  j  uhh�hj�  ubeh}�(h]�h]�h]�h]�h]��level�K�type�j�  �line�K�source�h!uhjr  ubjs  )��}�(hhh]�(hK)��}�(h�Title overline too short.�h]�h.�Title overline too short.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhJhj�  ubh�)��}�(h�Y--------------------
How to Override the Offensive Speech Classifier
--------------------�h]�h.�Y--------------------
How to Override the Offensive Speech Classifier
--------------------�����}�(hhhj   ubah}�(h]�h]�h]�h]�h]�j  j  uhh�hj�  ubeh}�(h]�h]�h]�h]�h]��level�K�type�j�  �line�K�source�h!uhjr  hh�hhh h!hKubjs  )��}�(hhh]�(hK)��}�(hhh]�h.�Title overline too short.�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhhJhj  ubh�)��}�(h�P--------------------
How to Override and Add NLP Components
--------------------�h]�h.�P--------------------
How to Override and Add NLP Components
--------------------�����}�(hhhj(  ubah}�(h]�h]�h]�h]�h]�j  j  uhh�hj  ubeh}�(h]�h]�h]�h]�h]��level�K�type�j�  �line�K(�source�h!uhjr  ubjs  )��}�(hhh]�(hK)��}�(h�Title overline too short.�h]�h.�Title overline too short.�����}�(hhhjC  ubah}�(h]�h]�h]�h]�h]�uhhJhj@  ubh�)��}�(h�P--------------------
How to Override and Add NLP Components
--------------------�h]�h.�P--------------------
How to Override and Add NLP Components
--------------------�����}�(hhhjQ  ubah}�(h]�h]�h]�h]�h]�j  j  uhh�hj@  ubeh}�(h]�h]�h]�h]�h]��level�K�type�j�  �line�K(�source�h!uhjr  hj  hhh h!hK*ubjs  )��}�(hhh]�(hK)��}�(hhh]�h.�Title overline too short.�����}�(hhhjl  ubah}�(h]�h]�h]�h]�h]�uhhJhji  ubh�)��}�(h�Y------------------------
Adding a New Module to the NLP Pipeline
------------------------�h]�h.�Y------------------------
Adding a New Module to the NLP Pipeline
------------------------�����}�(hhhjy  ubah}�(h]�h]�h]�h]�h]�j  j  uhh�hji  ubeh}�(h]�h]�h]�h]�h]��level�K�type�j�  �line�KV�source�h!uhjr  ubjs  )��}�(hhh]�(hK)��}�(h�Title overline too short.�h]�h.�Title overline too short.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhJhj�  ubh�)��}�(h�Y------------------------
Adding a New Module to the NLP Pipeline
------------------------�h]�h.�Y------------------------
Adding a New Module to the NLP Pipeline
------------------------�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j  j  uhh�hj�  ubeh}�(h]�h]�h]�h]�h]��level�K�type�j�  �line�KV�source�h!uhjr  hji  hhh h!hKXube�transform_messages�]�js  )��}�(hhh]�hK)��}�(hhh]�h.�AHyperlink target "overriding-cobot-components" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhJhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h!�line�Kuhjr  uba�transformer�N�
decoration�Nhhub.