.. _workflow_recommendation:


Workflow Recommendation
=======================
------------------------------------------------------------------------------------
If you only need to change your Lambda function code or remote service modules' code
------------------------------------------------------------------------------------

1. [Optional & Recommended] Create a new feature branch from mainline. Refer to https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging.
2. Change the component and test locally by running ``cobot quick-test`` CLI to test docker modules, ``cobot interactive-mode`` CLI to do end-to-end integration test. An alternative is ``cobot local-test`` CLI.

**Important Note:Because each full build and deployment process takes at least 20min, it's recommended to batch your changes and local test all the changes before "git push", due to a limitation in CodePipeline.**
If you git push small changes without fully testing it locally, you will waste 20min waiting for a full deployment and in the end find there is a small bug in your Lambda function or docker modules.

3. Merge the feature branch to mainline and push to remote mainline branch with a **git push**. Refer to https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging.
4. You will receive an email notification for the approval actions. In the notification email, you can find a URL pointing to the approval action which needs to be clicked (step 6.)

    .. image:: images/approval_email.png

5. Do an end-to-end integration test in your Beta environment by talking to your echo device or through the Test console in the Alexa Skills Kit Developer console.
6. If tests pass, approve the revision to deploy in production environment by clicking **Review** button, enter comments, and click **Approve** button. Otherwise, you can go to AWS console/CodePipeline to find the approval actions.

    Approval Action in CodePipeline

    .. image:: images/approval_action.png

    Popup Window with Comments Textarea and **Approve** Button

    .. image:: images/approval_message_window.png

7. If tests fail or you want to deploy several git commits from Beta to Prod stages, click **Reject** button in above image to reject the revision.
8. Wait until it successfully deploys to Prod stage. If it fails, check the CloudWatch logs for detail. If the error is transient, click “Retry”.

--------------------------------------------------------------------------------------
If you only need to change remote service modules' config: modules.yaml or config.yaml
--------------------------------------------------------------------------------------

1. [Optional & Recommended] Create a new feature branch from mainline. . Refer to https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging.
2. Change the component and test locally by running ``cobot quick-test`` CLI to test docker modules, ``cobot interactive-mode`` CLI to do end-to-end integration test. An alternative is ``cobot local-test`` CLI.
3. Merge the feature branch to mainline and git push to the remote mainline branch, then run the ``cobot update``CLI. . Refer to https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging.
4. Do an end-to-end integration test in your Beta environment by talking to your echo device or through the test console in the Alexa Skills Kit Developer console.
5. If tests pass, approve the revision to deploy in production environment by clicking **Review** button.
6. If tests fail, reject the revision
7. Wait until it successfully deploys to Prod stage. If it fails, check the CloudWatch logs for detail. If the error is transient, click “Retry”.

---------------------------------
If tests fail in beta environment
---------------------------------

1. Reject the **approval action**
2. Revert the failure commit from mainline
3. Fix bugs in feature branch and merge to mainline again

--------------------------------------
To submit your skill for certification
--------------------------------------

1. In developer.amazon.com website, change the Lambda endpoint of your development skill in the Configuration page.
2. Append **:Prod** to AWS Lambda ARN. It will connect ASK skill's certified version (and also live version once it's certified) to Lambda function's Prod Alias.
3. Click "Next" and "Submit for review".
4. Remember to delete the **:Prod** from your development skill's endpoint configuration after your skill is certificated. Click "Save Endpoints" in the top left.

    .. image:: images/submit_skill.png

