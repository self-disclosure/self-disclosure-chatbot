new_session_event= {
  "session": {
    "sessionId": "SessionId.8b4bb5a8-6498-4cdd-b846-71e9849418b6",
    "application": {
      "applicationId": "amzn1.ask.skill.5017db5b-d522-4743-aefe-72dfa50c14fc"
    },
    "attributes": {
      "conversationId": "2e860e8f7f9ca0ad6bcc4952cec93af5bec968b0a01b197abf7cee37d1ef1745"
    },
    "user": {
      "userId": "amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY"
    },
    "new": True
  },
  "request": {
    "type": "IntentRequest",
    "requestId": "EdwRequestId.a1253d59-419b-4d6f-ba74-779d9775a31a",
    "locale": "en-US",
    "timestamp": "2017-06-08T20:44:15Z",
    "speechRecognition": {
      "hypotheses": [
        {
          "tokens": [
            {
              "confidence": 0.976,
              "value": "five"
            }
          ]
        }
      ]
    },
    "intent": {
      "name": "RatingIntent",
      "slots": {
        "rating_fraction_number": {
          "name": "rating_fraction_number"
        },
        "rating": {
          "name": "rating"
        },
        "rating_fraction": {
          "name": "rating_fraction",
          "value": "a half"
        }
      }
    }
  },
  "version": "1.0"
}

existing_session_event={
  "session": {
    "new": False,
    "sessionId": "SessionId.8b4bb5a8-6498-4cdd-b846-71e9849418b6",
    "application": {
      "applicationId": "amzn1.ask.skill.e1cfea85-891e-4099-b7d1-72c5e56183d5"
    },
    "attributes": {
      "conversationId": "2e860e8f7f9ca0ad6bcc4952cec93af5bec968b0a01b197abf7cee37d1ef1745",
      "lastState": "{\"user_id\": \"amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY\", \"conversation_id\": \"2e860e8f7f9ca0ad6bcc4952cec93af5bec968b0a01b197abf7cee37d1ef1745\", \"session_id\": \"SessionId.8b4bb5a8-6498-4cdd-b846-71e9849418b6\", \"intent\": \"RatingIntent\", \"slots\": {\"rating_fraction_number\": {\"name\": \"rating_fraction_number\"}, \"rating\": {\"name\": \"rating\"}, \"rating_fraction\": {\"name\": \"rating_fraction\", \"value\": \"a half\"}}, \"creation_date_time\": \"2017-12-21T20:13:16.451449\", \"nlp\": {}, \"topic\": \"BASELINE Topic\", \"asr\": [{\"confidence\": 0.976, \"value\": \"five\"}], \"response\": \"Local Baseline Response Generator\", \"mode\": null}"
    },
    "user": {
      "userId": "amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY"
    }
  },
  "request": {
    "type": "IntentRequest",
    "requestId": "EdwRequestId.eb3ecee7-b35f-4df1-9a37-e38e93e5f394",
    "intent": {
      "name": "qa",
      "slots": {
        "question":
          {
            "name": "question",
            "value": "what's the capital of france?"
          }
      }
    },
    "locale": "en-US",
    "timestamp": "2017-12-10T01:46:55Z"
  },
  "context": {
    "AudioPlayer": {
      "playerActivity": "IDLE"
    },
    "System": {
      "application": {
        "applicationId": "amzn1.ask.skill.e1cfea85-891e-4099-b7d1-72c5e56183d5"
      },
      "user": {
        "userId": "amzn1.ask.account.AENDI33E2R76DCR2HDAENMHYRZEBMACLKDU4SZSHXISGNPZN3BTBOKOLCSVYTDUKUD774INSHWL64AID5HUXCGLN726B5DRGZNYSJMCNYV5QJDRNJGOECVSEH3TCKHJ63VQHGM45OTGTRMUM72IYIY2YVPOFS2HWUMW35U6CHBJ44B3OQ3WBOAAPIEP5M4JL6V4HCZZXT3BQFQY"
      },
      "device": {
        "supportedInterfaces": {}
      }
    }
  },
  "version": "1.0"
}