# AlexaPrizeCobotToolkit

This is a Python3 toolkit for building conversational bot on the Alexa Skills Kit.

## Documentation

See doc/htm/index.html for detailed documentation on installing and using the Cobot Toolkit.