from miniskills.constants.intent_names import IntentNames
from miniskills.constants.intent_to_miniskill_map import IntentToMiniskillMap
from miniskills.constants.plug_in_config import PlugInConfig
from miniskills.constants.responses import Responses
from miniskills.constants.user_response_information_names import UserResponseInformationNames
from miniskills.custom_nlp.miniskill_chooser import MiniskillChooser
from miniskills.custom_nlp.set_corrected_ask_intent_for_specific_ask_intent import SetCorrectedAskIntentForSpecificAskIntent
from miniskills.custom_nlp.set_corrected_ask_intent_for_user_response_to_launch import SetCorrectedAskIntentForUserResponseToLaunch
import re

class GenerateCorrectedAskIntent(object):
    """
    Generate corrected_ask_intent
    """
    FIRST_TURN = '0'

    def _generate_corrected_ask_intent_for_last_state_is_invalid(self, miniskill_to_turn_used, last_state_saved_intent):
        last_miniskill = None
        most_recent = 0
        for miniskill in miniskill_to_turn_used:
            if int(miniskill_to_turn_used[miniskill]) > most_recent and miniskill != 'turn_number':
                most_recent = int(miniskill_to_turn_used[miniskill])
                last_miniskill = miniskill
        for intent, miniskill in IntentToMiniskillMap.intent_map.items():
            if last_miniskill == miniskill:
                return intent
        return IntentNames.greeting_intent
    
    def execute(self, user_response_information, current_intent, last_state_corrected_ask_intent, last_state_intent, miniskill_to_turn_used, last_state_saved_intent, text, corrected_topic, last_state_old_topic, last_state_response, last_state_new_topic, last_turn, visited_threads):
        temp_corrected_ask_intent = None

        # user replies to launch_request_intent
        if last_state_intent == IntentNames.launch_request_intent:
            temp_corrected_ask_intent = SetCorrectedAskIntentForUserResponseToLaunch().execute(last_state_response, text, corrected_topic)
        # user wants to end the conversation
        elif current_intent == IntentNames.amazon_stop_intent or current_intent == IntentNames.ask_question_intent:
            temp_corrected_ask_intent = current_intent

        if temp_corrected_ask_intent is None:
            # set corrected_ask_intent to this state's intent if it corresponds to a miniskill
            if current_intent in IntentToMiniskillMap.intent_map.keys():
                temp_corrected_ask_intent = current_intent   
            elif last_state_corrected_ask_intent == IntentNames.invalid_intent or current_intent == IntentNames.ignored_and_deflected_intent:
                # continue previous miniskill after invalid intent
                temp_corrected_ask_intent = self._generate_corrected_ask_intent_for_last_state_is_invalid(miniskill_to_turn_used, last_state_saved_intent)
            # intents with miniskill diversity
            elif current_intent == IntentNames.inform_topic_intent or last_turn == '5':
                # choose miniskill to handle InformTopicIntent because it can go to more than one
                temp_corrected_ask_intent = MiniskillChooser().choose_one_miniskill(corrected_topic, last_state_corrected_ask_intent, visited_threads)
            # plugins for miniskill diversity
            elif PlugInConfig.continue_miniskill_diversity_plug_in and current_intent == IntentNames.continue_intent:
                # goes to different miniskill for "tell me more"
                temp_corrected_ask_intent = MiniskillChooser().choose_one_miniskill(last_state_old_topic, last_state_corrected_ask_intent, visited_threads)
            else:
                if last_state_saved_intent == IntentNames.request_news_intent or last_state_corrected_ask_intent == IntentNames.request_news_intent:
                    # stay on request_news_intent and pull another relevant news thread
                    if current_intent == IntentNames.amazon_no_intent:
                        temp_corrected_ask_intent = IntentNames.request_news_intent
                    if last_state_saved_intent is not None:
                        temp_corrected_ask_intent = last_state_saved_intent
                    else:
                        temp_corrected_ask_intent = MiniskillChooser().choose_one_miniskill(last_state_new_topic, last_state_saved_intent, visited_threads)
                    # choose a new informational miniskill if we want more information about a topic
                    for response in Responses.ARE_YOU_FAMILIAR_WITH:
                        response = response.strip()
                        if last_state_response.find(response) >= 0:
                            # want to trigger a new skill when news hits this response
                            if current_intent == IntentNames.amazon_no_intent or current_intent == IntentNames.navigation_intent:
                                temp_corrected_ask_intent = MiniskillChooser().choose_one_miniskill(last_state_new_topic, last_state_saved_intent, visited_threads)
                elif last_state_saved_intent == IntentNames.request_movie_intent or last_state_intent == IntentNames.request_movie_intent or last_state_intent == IntentNames.navigation_intent:
                    # choose a new informational miniskill if we want more information about a topic
                    for response in Responses.ARE_YOU_FAMILIAR_WITH:
                        response = response.strip()
                        if last_state_response.find(response) >= 0:
                            # want to trigger a new skill when movies hits this response because it is the end of the 4 set turns
                            if current_intent == IntentNames.amazon_no_intent or current_intent == IntentNames.navigation_intent or (current_intent == IntentNames.amazon_yes_intent and (last_state_saved_intent == IntentNames.request_movie_intent or last_state_intent == IntentNames.request_movie_intent)):
                                temp_corrected_ask_intent = MiniskillChooser().choose_one_miniskill(last_state_new_topic, last_state_saved_intent, visited_threads)
                    # keep on the movie skill
                    for response in Responses.INFORM_MOVIE_NAME:
                        response = response.strip()
                        if last_state_response.find(response) >= 0:
                            if current_intent == IntentNames.amazon_no_intent or current_intent == IntentNames.navigation_intent or current_intent == IntentNames.amazon_yes_intent:
                                temp_corrected_ask_intent = IntentNames.request_movie_intent
                    for response in (Responses.CONFIRM_INFORM_MOVIE_PLOT + Responses.CONFIRM_INFORM_MOVIE_REVIEW):
                        response = response.strip()
                        if last_state_response.find(response) >= 0:
                            # always route back to movies
                            temp_corrected_ask_intent = IntentNames.request_movie_intent
                elif PlugInConfig.positive_user_information_miniskill_diversity_plug_in and (user_response_information.user_satisfaction == UserResponseInformationNames.POSITIVE or user_response_information.user_side == UserResponseInformationNames.AGREE or user_response_information.user_side == UserResponseInformationNames.UNSURE_SIDE or (user_response_information.user_knowledge == UserResponseInformationNames.NOT_KNOWN and current_intent != IntentNames.amazon_yes_intent)):
                    temp_corrected_ask_intent = MiniskillChooser().choose_one_miniskill(last_state_old_topic, last_state_corrected_ask_intent, visited_threads)
                if temp_corrected_ask_intent is None:
                    temp_corrected_ask_intent = SetCorrectedAskIntentForSpecificAskIntent().execute(user_response_information, current_intent, last_state_corrected_ask_intent, last_state_intent, text, last_state_response)
                    # handle intent being the same as corrected_ask_intent due to direct miniskill invocation
                    if temp_corrected_ask_intent is None:
                        temp_corrected_ask_intent = current_intent

        return temp_corrected_ask_intent