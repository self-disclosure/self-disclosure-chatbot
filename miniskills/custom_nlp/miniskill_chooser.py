from miniskills.constants.dynamo_db_table_names import DynamoDbTableNames
from miniskills.constants.intent_names import IntentNames
from miniskills.constants.intent_to_dynamo_db_table_map import IntentToDynamoDbTableMap
from miniskills.constants.intent_to_miniskill_map import IntentToMiniskillMap
from miniskills.constants.miniskills import Miniskills
from miniskills.constants.miniskill_list import MiniskillList
from miniskills.miniskill_dynamo_db_manager import MiniskillDynamoDbManager
import random
from collections import Counter

class MiniskillChooser(object):

    def no_topic_switch_miniskill(self, corrected_topic, current_corrected_ask_intent, miniskill_to_turn_used, last_state_visited_threads, dont_include_current_miniskill, last_state_new_topic, last_state_corrected_ask_intent):
        """
        Switches to another miniskill if the topic is not found in the dynamodb table, else go to the same miniskill (which will map to topic_exhausted)
        """
        if current_corrected_ask_intent is not None and current_corrected_ask_intent != IntentNames.launch_request_intent or (last_state_new_topic and last_state_corrected_ask_intent == IntentNames.request_news_intent):
            miniskill_dynamo_db_manager = MiniskillDynamoDbManager()
            possible_miniskill_to_dynamo_db_tables = {}
            for miniskill in MiniskillList.skill_list_config:
                intent = [key for key, value in IntentToMiniskillMap.intent_map.items() if value == miniskill][0]
                if dont_include_current_miniskill and current_corrected_ask_intent != intent or dont_include_current_miniskill is False:
                    if intent in IntentToDynamoDbTableMap.intent_map.keys():
                        possible_miniskill_to_dynamo_db_tables[miniskill] = IntentToDynamoDbTableMap.intent_map[intent]
            if corrected_topic is not None or (last_state_new_topic and last_state_corrected_ask_intent == IntentNames.request_news_intent):
                if not corrected_topic:
                    corrected_topic = last_state_new_topic
                else:
                    corrected_topic = corrected_topic.lower()
                current_miniskill = IntentToMiniskillMap.intent_map[current_corrected_ask_intent]
                if dont_include_current_miniskill is False:
                    if current_miniskill in possible_miniskill_to_dynamo_db_tables.keys():
                        for table in possible_miniskill_to_dynamo_db_tables[current_miniskill]:
                            if miniskill_dynamo_db_manager.check_topic_exists(table, corrected_topic, last_state_visited_threads) is True:
                                return current_corrected_ask_intent
                closest_miniskill = None
                closest_turns_to_current_miniskill = float('inf')
                current_turn = miniskill_to_turn_used[current_miniskill]
                for miniskill, table_list in possible_miniskill_to_dynamo_db_tables.items():
                    for table in table_list:
                        if miniskill_dynamo_db_manager.check_topic_exists(table, corrected_topic, last_state_visited_threads) is True:
                            thread_list = miniskill_dynamo_db_manager.pull_threads_for_topic(table, corrected_topic)['thread_list']
                            intersection = None
                            if last_state_visited_threads is not None:
                                intersection = set(last_state_visited_threads).intersection(thread_list)
                            if intersection != thread_list:
                                turns_away_from_current_miniskill = int(current_turn) - int(miniskill_to_turn_used[miniskill])
                                if turns_away_from_current_miniskill < closest_turns_to_current_miniskill:
                                    closest_miniskill = miniskill
                                    closest_turns_to_current_miniskill = turns_away_from_current_miniskill
                if closest_miniskill is not None:
                    return list(IntentToMiniskillMap.intent_map.keys())[list(IntentToMiniskillMap.intent_map.values()).index(closest_miniskill)]
                else:
                    return current_corrected_ask_intent

    def _populate_list_of_miniskills_to_choose_from(self, last_state_saved_intent):
        list_of_miniskills_to_choose_from = MiniskillList.skill_list_config
        if last_state_saved_intent == IntentNames.request_news_intent or last_state_saved_intent == IntentNames.request_movie_intent:
            list_of_miniskills_to_choose_from = MiniskillList.informative_skill_list
        return list_of_miniskills_to_choose_from

    def choose_one_miniskill(self, corrected_topic, last_state_saved_intent, visited_threads):
        """
        Chooses one miniskill based on content availability
        """
        possible_miniskills = []
        miniskill_dynamo_db_manager = MiniskillDynamoDbManager()
        list_of_miniskills_to_choose_from = self._populate_list_of_miniskills_to_choose_from(last_state_saved_intent)
        for miniskill in list_of_miniskills_to_choose_from:
            intent = [key for key, value in IntentToMiniskillMap.intent_map.items() if value == miniskill][0]
            try:
                for table in IntentToDynamoDbTableMap.intent_map[intent]:
                    if miniskill_dynamo_db_manager.check_topic_exists(table, corrected_topic, visited_threads) is True:
                        possible_miniskills.append(miniskill)
            except KeyError:
                possible_miniskills.append(miniskill)

        # artificial deflation of news
        miniskill_counts = Counter(possible_miniskills)
        if miniskill_counts[Miniskills.NEWS] >= 1:
            possible_miniskills[:] = (miniskill for miniskill in possible_miniskills if miniskill != Miniskills.NEWS)
            possible_miniskills.append(Miniskills.NEWS)
        # articial inflation of movies if movies can be called
        if miniskill_counts[Miniskills.MOVIES] == 1:
            possible_miniskills.append(Miniskills.MOVIES)

        if possible_miniskills:
            miniskill_chosen = random.choice(possible_miniskills)
            return list(IntentToMiniskillMap.intent_map.keys())[list(IntentToMiniskillMap.intent_map.values()).index(miniskill_chosen)]
        else:
            # stays on own for topic exhaustion
            return last_state_saved_intent