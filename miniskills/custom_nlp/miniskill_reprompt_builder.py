from cobot_core.response_builder import RepromptBuilder
from miniskills.constants.intent_names import IntentNames

class MiniskillRepromptBuilder(RepromptBuilder):
    """
    Create custom reprompts for miniskill actions
    """

    def build(self, speech_output):
        return self.state_manager.current_state.reprompt