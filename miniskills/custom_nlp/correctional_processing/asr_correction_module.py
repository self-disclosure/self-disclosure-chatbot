from cobot_core.service_module import RemoteServiceModule
import json
import requests
from decimal import Decimal

class AsrCorrectionModule(RemoteServiceModule):
    def execute(self):
        for i in range(len(self.input_data['nounphrases'])):
            try:
                for key in self.input_data['nounphrases'][i].keys():
                    if isinstance(self.input_data['nounphrases'][i][key], list) and isinstance(self.input_data['nounphrases'][i][key][0], Decimal):
                        self.input_data['nounphrases'][i][key][0] = float(self.input_data['nounphrases'][i][key][0])
            except:
                pass

        response = requests.post(self.url, data=json.dumps(self.input_data), headers={'content-type': 'application/json'})

        return response.json()