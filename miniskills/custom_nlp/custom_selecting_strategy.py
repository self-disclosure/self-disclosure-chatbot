from cobot_core import SelectingStrategy
from miniskills.constants.miniskill_list import MiniskillList
from typing import Dict, List, Any

class CustomSelectingStrategy(SelectingStrategy):
    """
    Selecting strategy method usable for custom intent classifier's corrected_ask_intent
    """
    def select_response_mode(self, features:Dict[str,Any])->List[str]:
        """
        Return a list of possible miniskills for the given input.
        """
        if self.state_manager.current_state.corrected_ask_intent:
            rgs = MiniskillList.intent_map.get(self.state_manager.current_state.corrected_ask_intent, [])
            if isinstance(rgs, str):
                rgs = [rgs]
            keys = self.service_module_manager.response_generator_names
            return list(set(keys).intersection(rgs))
        elif self.state_manager.current_state.intent == 'LaunchRequestIntent':
            return ['LAUNCH']
        else:
            return None