from miniskills.constants.categorical_topic_names import CategoricalTopicNames
from miniskills.constants.intent_names import IntentNames
from miniskills.constants.intent_to_dynamo_db_table_map import IntentToDynamoDbTableMap
from miniskills.miniskill_dynamo_db_manager import MiniskillDynamoDbManager
import random
import re

class SetCorrectedTopic(object):
    """
    Set the corrected topic based on slots, categorical topics or neither
    """
    EMPTY = ''

    def _pull_specific_topic_for_categorical_topic(self, regex, category, slots, intent, text, visited_threads):
        """
        Pull specific topic for a categorical topic
        """
        topic = None
        current_intent_topic_table_list = IntentToDynamoDbTableMap.intent_map.get(intent)
        if current_intent_topic_table_list is not None:
            for table in current_intent_topic_table_list:
                miniskill_dynamo_db_manager = MiniskillDynamoDbManager()
                if re.match("|".join(regex), text) is not None:
                    topic = random.choice(category)
                    number_of_different_topics_pulled = 0
                    while not miniskill_dynamo_db_manager.check_topic_exists(table, slots['topic']['value'], visited_threads):
                        topic = random.choice(category)
                        number_of_different_topics_pulled += 1
                        if number_of_different_topics_pulled > self.MAX_NUMBER_OF_TOPICS:
                            return None
        return topic

    def _update_topic_for_categorical_topic(self, slots, intent, text, visited_threads):
        """
        Check for categorical intent and chooses a specific topic
        """
        if text != '':
            categorical_topics = [self._pull_specific_topic_for_categorical_topic([r'\bholiday\b',
                                                                                   r'\bholidays\b'],
                                                                                   CategoricalTopicNames.HOLIDAYS, slots, intent, text, visited_threads),
                                  self._pull_specific_topic_for_categorical_topic([r'\bshow\b',
                                                                                   r'\bshows\b',
                                                                                   r'\baward shows\b',
                                                                                   r'\baward show\b'
                                                                                   r'\bupcoming award show\b',
                                                                                   r'\bupcoming award shows\b',
                                                                                   r'\bsome upcoming award show\b',
                                                                                   r'\bsome upcoming award shows\b'],
                                                                                   CategoricalTopicNames.AWARD_SHOWS, slots, intent, text, visited_threads),
                                  self._pull_specific_topic_for_categorical_topic([r'\bpet\b',
                                                                                   r'\bpets\b'],
                                                                                   CategoricalTopicNames.PETS, slots, intent, text, visited_threads),
                                  self._pull_specific_topic_for_categorical_topic([r'\bmusic\b',
                                                                                   r'\bmusics\b'],
                                                                                   CategoricalTopicNames.MUSIC, slots, intent, text, visited_threads),
                                  self._pull_specific_topic_for_categorical_topic([r'\bfood\b',
                                                                                   r'\bfoods\b'],
                                                                                   CategoricalTopicNames.FOOD, slots, intent, text, visited_threads),
                                  self._pull_specific_topic_for_categorical_topic([r'\bfashion\b'],
                                                                                   CategoricalTopicNames.FASHION, slots, intent, text, visited_threads),
                                  self._pull_specific_topic_for_categorical_topic([r'\bcar\b',
                                                                                   r'\bcars\b',
                                                                                   r'\bautomotive\b',
                                                                                   r'\bvehicle\b',
                                                                                   r'\bvehicles\b'],
                                                                                   CategoricalTopicNames.CARS, slots, intent, text, visited_threads),
                                  self._pull_specific_topic_for_categorical_topic([r'\bcelebrity\b',
                                                                                   r'\bcelebrities\b'],
                                                                                   CategoricalTopicNames.CELEBRITIES, slots, intent, text, visited_threads),
                                  self._pull_specific_topic_for_categorical_topic([r'\bgame\b',
                                                                                   r'\bgames\b',
                                                                                   r'\bvideo game\b',
                                                                                   r'\bvideo games\b'],
                                                                                   CategoricalTopicNames.VIDEO_GAMES, slots, intent, text, visited_threads)]
            for each_topic in categorical_topics:
                if each_topic is not None:
                    return each_topic
            return None

    def _update_topic_for_request_movie_intent(self, intent, last_state_topic, last_state_intent, turn_number):
        if last_state_intent == IntentNames.request_movie_intent and turn_number:
            return last_state_topic
    
    def execute(self, slots, intent, text, last_state_topic, last_state_intent, turn_number, visited_threads):
        text = str(text)
        topic = None
        list_of_topic_setters = [self._update_topic_for_categorical_topic(slots, intent, text, visited_threads), self._update_topic_for_request_movie_intent(intent, last_state_topic, last_state_intent, turn_number)]
        if slots is not None:
            if slots.get('topic') is not None:
                if slots['topic'].get('value') is not None:
                    topic = self._update_topic_for_categorical_topic(slots, intent, text, visited_threads)
            elif slots.get('navigation') is not None:
                if slots['navigation'].get('value') is not None:
                    topic = self._update_topic_for_request_movie_intent(intent, last_state_topic, last_state_intent, turn_number)
            
            if topic is None and slots.get('topic') is not None:
                if slots['topic'].get('value'):
                    return slots['topic']['value']
                else:
                    return None

        return topic