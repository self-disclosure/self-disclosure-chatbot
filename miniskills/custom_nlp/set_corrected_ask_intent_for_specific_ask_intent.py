from miniskills.constants.dynamo_db_table_names import DynamoDbTableNames
from miniskills.constants.intent_names import IntentNames
from miniskills.constants.miniskills import Miniskills
from miniskills.constants.miniskill_list import MiniskillList
from miniskills.constants.responses import Responses
from miniskills.constants.user_response_information_names import UserResponseInformationNames
import re

class SetCorrectedAskIntentForSpecificAskIntent(object):
    """
    Set corrected ask intent for specific ask intents
    """

    def _set_corrected_ask_intent(self, last_state_corrected_ask_intent, last_state_intent):
        """
        Set corrected ask intent
        """
        if last_state_corrected_ask_intent is not None and last_state_corrected_ask_intent != IntentNames.invalid_intent:
            return last_state_corrected_ask_intent
        else:
            last_state_intent

    def _set_corrected_ask_intent_for_navigational_intent(self, current_intent, last_state_corrected_ask_intent, last_state_intent):
        """
        Check navigational intents
        """
        if current_intent == IntentNames.repeat_intent or current_intent == IntentNames.continue_intent or current_intent == IntentNames.cancel_intent:
            if last_state_corrected_ask_intent:
                return last_state_corrected_ask_intent
            else:
                return IntentNames.launch_request_intent

    def _set_corrected_ask_intent_with_miniskill_intent(self, regex, miniskill_intent, text):
        """
        Check miniskill intent
        """
        if text is not None:
            if re.match("|".join(regex), text) is not None:
                return miniskill_intent

    def _set_corrected_ask_intent_with_request_fact_intent(self, text):
        """
        Manually correct intent for RequestFactIntent
        """
        self._set_corrected_ask_intent_with_miniskill_intent([r'^(fact|facts|trivia)$',
                                                              r'^(interesting|funny|fun) (fact|facts|trivia)$'], IntentNames.request_fact_intent, text)

    def _set_corrected_ask_intent_with_request_opinion_intent(self, text):
        """
        Manually correct intent for RequestOpinionIntent
        """
        self._set_corrected_ask_intent_with_miniskill_intent([r'^(opinion|opinions|opinion piece|thoughts)$'], IntentNames.request_opinion_intent, text)

    def _set_corrected_ask_intent_with_request_news_intent(self, text):
        """
        Manually correct intent for RequestNewsIntent
        """
        self._set_corrected_ask_intent_with_miniskill_intent([r'^(technology|tech)$',
                                                              r'\b(technology|tech) (news|new)$',
                                                              r'\b(bout|about) (technology|tech)\b',
                                                              r'^(futurology|futuro logy|future ology)$',
                                                              r'\b(futurology|future) (news|new)$',
                                                              r'\b(bout|about) (futurology|future|future ology)\b',
                                                              r'^(science)$',
                                                              r'\b(science) (news|new)$',
                                                              r'\b(bout|about) (science)\b',
                                                              r'^(sport|sports)$',
                                                              r'\b(sport|sports) (news|new)$',
                                                              r'\b(bout|about) (sport|sports)\b',
                                                              r'\b(talk|chat)\b.*\b(sport|sports)\b',
                                                              r'^(politics)$',
                                                              r'\b(politics|political) (news|new)$',
                                                              r'\b(bout|about) (politics)\b',
                                                              r'\b(international|global|world) news$',
                                                              r'^news$',
                                                              r'\b(what|what\'s|whats)\b.*\bnews\b',
                                                              r'\b(give|tell) me\b.*\bnews$'], IntentNames.request_news_intent, text)
    
    def _set_corrected_ask_intent_for_special_user_utterance(self, current_intent, text):
        """
        Match special user utterance
        """
        if current_intent == IntentNames.amazon_help_intent:
            return IntentNames.special_user_utterance_response_intent

        if text is not None:
            # checks for special user utterances
            if re.match("|".join([r'^(sorry|I\'m sorry)\b',
                                  r'^(thank|thanks)\b',
                                  r'^(no) (thank|thanks)\b',
                                  r'\bthat sounds too good to be true\b',
                                  r'^really$',
                                  r'\bwhat\'s that$',
                                  r'\bwhat is that$',
                                  r'\bwhat does that mean\b',
                                  r'\bwhat is that mean\b',
                                  r'\bwhat is it called\b',
                                  r'\bwhat is the reason for that\b',
                                  r'\bwhat is it\b',
                                  r'\bwhat was the name of it\b',
                                  r'\bwhere is it\b',
                                  r'\bwho were they\b',
                                  r'\bhow do you know that\b',
                                  r'\bhow did they do that\b',
                                  r'\bhow is that possible\b',
                                  r'\bhow does that work\b',
                                  r'\bwhy is that\b',
                                  r'\bwhy does that matter\b',
                                  r'\bwhat happened then\b',
                                  r'\bwhy did they do that\b',
                                  r'\bwhy$',
                                  r'\bI have a headache\b']), 
                                  text) is not None:
                return IntentNames.special_user_utterance_response_intent

    def execute(self, user_response_information, current_intent, last_state_corrected_ask_intent, last_state_intent, text, last_state_response):
        text = str(text)

        # set corrected_ask_intent for navigational intents
        set_corrected_ask_intents = [self._set_corrected_ask_intent_for_navigational_intent(current_intent, last_state_corrected_ask_intent, last_state_intent), self._set_corrected_ask_intent_for_special_user_utterance(current_intent, text)]

        if (user_response_information.user_decision is not None or user_response_information.user_knowledge is not None or user_response_information.user_satisfaction is not None or user_response_information.user_side is not None) and user_response_information.user_satisfaction != UserResponseInformationNames.NEGATIVE:
            return self._set_corrected_ask_intent(last_state_corrected_ask_intent, last_state_intent)
        else:
            # manually check miniskill invocations from ASR
            if Miniskills.SHAREFACTS in MiniskillList.skill_list_config:
                set_corrected_ask_intents.append(self._set_corrected_ask_intent_with_request_fact_intent(text))

            if Miniskills.SHOWERTHOUGHTS in MiniskillList.skill_list_config:
                set_corrected_ask_intents.append(self._set_corrected_ask_intent_with_request_opinion_intent(text))

            if Miniskills.NEWS in MiniskillList.skill_list_config:
                set_corrected_ask_intents.append(self._set_corrected_ask_intent_with_request_news_intent(text))
            
            for resulting_corrected_ask_intent in set_corrected_ask_intents:
                if resulting_corrected_ask_intent:
                    return resulting_corrected_ask_intent