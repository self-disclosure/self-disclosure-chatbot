from miniskills.constants.intent_names import IntentNames
from miniskills.constants.responses import Responses
import re

class SetCorrectedAskIntentForUserResponseToLaunch(object):
    """
    Set corrected ask intent if user responses to launch
    """
    user_feeling = [r'^(.*?)fine',
                    r'^(.*?)good',
                    r'^(.*?)amazing',
                    r'^(.*?)well',
                    r'^(.*?)great',
                    r'^(.*?)bad',
                    r'^(.*?)terrible',
                    r'^(.*?)horrible',
                    r'^(.*?)shitty',
                    r'^(.*?)sleepy',
                    r'^(.*?)tired',
                    r'^(.*?)angry',
                    r'^(.*?)annoyed']

    def execute(self, last_state_response, text, corrected_topic):
        """
        Match greeting small talk
        """
        for response in Responses.GREETING:
            response = response.strip()
            if last_state_response.find(response) >= 0:
                text = str(text)
                corrected_topic = str(corrected_topic)
                # checks for user feeling
                if re.match("|".join(self.user_feeling), text) is not None or re.match("|".join(self.user_feeling), corrected_topic) is not None:
                    return IntentNames.greeting_intent