from cobot_core.nlp.nlp_modules import IntentClassifier
from cobot_core.state_manager import StateManager
from miniskills.constants.intent_names import IntentNames
from miniskills.constants.intent_to_miniskill_map import IntentToMiniskillMap
from miniskills.constants.miniskill_list import MiniskillList
from miniskills.custom_nlp.generate_corrected_ask_intent import GenerateCorrectedAskIntent
from miniskills.custom_nlp.miniskill_chooser import MiniskillChooser
from miniskills.custom_nlp.modify_ask_intent import ModifyAskIntent
from miniskills.custom_nlp.set_corrected_topic import SetCorrectedTopic
import re
import random

class CorrectedAskIntentClassifier(IntentClassifier):
    """
    Create and fill corrected_ask_intent based on ASR.
    """
    FIRST_TURN = '0'
    TURNS_BEFORE_SWITCHING = 3

    def _get_text_from_navigation(self):
        """
        Get text from navigation slot
        """
        if self.state_manager.current_state.slots is not None:
            if self.state_manager.current_state.slots.get('navigation') is not None:
                if self.state_manager.current_state.slots['navigation'].get('value') is not None:
                    text = self.state_manager.current_state.slots['navigation']['value']
                    if "open baseline socialbot and " in text:
                        return text[28:]
                    return text

    def _update_miniskill_to_turn_used(self):
        """
        Update miniskill to turn used
        """
        if not (self.state_manager.current_state.corrected_ask_intent == IntentNames.navigation_intent or self.state_manager.current_state.corrected_ask_intent == IntentNames.invalid_intent or self.state_manager.current_state.corrected_ask_intent == IntentNames.launch_request_intent or self.state_manager.current_state.corrected_ask_intent == IntentNames.special_user_utterance_response_intent) and self.state_manager.current_state.corrected_ask_intent:
            miniskill = IntentToMiniskillMap.intent_map[self.state_manager.current_state.corrected_ask_intent]
            self.state_manager.current_state.miniskill_to_turn_used['turn_number'] = str(int(self.state_manager.current_state.miniskill_to_turn_used['turn_number']) + 1)
            self.state_manager.current_state.miniskill_to_turn_used[miniskill] = self.state_manager.current_state.miniskill_to_turn_used['turn_number']
        elif self.state_manager.current_state.corrected_ask_intent:
            self.state_manager.current_state.corrected_ask_intent = IntentNames.invalid_intent

    def _initialize_miniskill_to_turn_used(self):
        """
        Initialize miniskill to turn used
        """
        if self.state_manager.current_state.intent == IntentNames.launch_request_intent:
            miniskill_to_turn_used = {}
            for miniskill in MiniskillList.skill_list_config:
                miniskill_to_turn_used[miniskill] = self.FIRST_TURN
            miniskill_to_turn_used['turn_number'] = self.FIRST_TURN
            self.state_manager.current_state.miniskill_to_turn_used = miniskill_to_turn_used
        else:
            self.state_manager.current_state.miniskill_to_turn_used = self.state_manager.last_state.get('miniskill_to_turn_used')

    def _set_corrected_topic(self):
        """
        Set corrected topic
        """
        slots = self.state_manager.current_state.slots
        intent = self.state_manager.current_state.intent
        text = self.state_manager.current_state.text
        last_state_topic = self.state_manager.last_state.get('old_topic')
        last_state_intent = self.state_manager.last_state.get('corrected_ask_intent')
        turn_number = self.state_manager.last_state.get('turn_number')
        visited_threads = self.state_manager.last_state.get('visited_threads')
        self.state_manager.current_state.corrected_topic = SetCorrectedTopic().execute(slots, intent, text, last_state_topic, last_state_intent, turn_number, visited_threads)

    def _modify_ask_intent(self):
        """
        Modify ASK Intent
        """
        last_state_require_confirmation = self.state_manager.last_state.get('require_confirmation')
        last_state_corrected_ask_intent = self.state_manager.last_state.get('corrected_ask_intent')
        current_intent = self.state_manager.current_state.intent
        topic = self.state_manager.current_state.corrected_topic
        saved_intent = self.state_manager.current_state.saved_intent
        text_from_navigation = self._get_text_from_navigation()
        text = self.state_manager.current_state.text
        intent = ModifyAskIntent().execute(last_state_require_confirmation, last_state_corrected_ask_intent, current_intent, topic, saved_intent, text_from_navigation, text)
        if intent is not None:
            self.state_manager.current_state.intent = intent

    def _generate_corrected_ask_intent(self):
        """
        Generate corrected_ask_intent
        """
        user_response_information = self.state_manager.current_state.user_response_information
        current_intent = self.state_manager.current_state.intent
        last_state_corrected_ask_intent = self.state_manager.last_state.get('corrected_ask_intent')
        last_state_intent = self.state_manager.last_state.get('intent')
        miniskill_to_turn_used = self.state_manager.current_state.miniskill_to_turn_used
        last_state_saved_intent = self.state_manager.last_state.get('saved_intent')
        text = self._get_text_from_navigation()
        corrected_topic = self.state_manager.current_state.corrected_topic
        last_state_old_topic = self.state_manager.last_state.get('old_topic')
        last_state_response = self.state_manager.last_state.get('response')
        last_state_new_topic = self.state_manager.last_state.get('new_topic')
        last_turn = self.state_manager.last_state.get('turn_number')
        visited_threads = self.state_manager.last_state.get('visited_threads')
        self.state_manager.current_state.corrected_ask_intent = GenerateCorrectedAskIntent().execute(user_response_information, current_intent, last_state_corrected_ask_intent, last_state_intent, miniskill_to_turn_used, last_state_saved_intent, text, corrected_topic, last_state_old_topic, last_state_response, last_state_new_topic, last_turn, visited_threads)
        if self.state_manager.last_state.get('intent') == IntentNames.launch_request_intent and self.state_manager.current_state.corrected_ask_intent == IntentNames.special_user_utterance_response_intent:
            self.state_manager.current_state.corrected_ask_intent = IntentNames.launch_request_intent

    def _check_topic_exists(self):
        """
        Find other possible miniskills for the topic if topic is not found in the original topic table (happens after you choose a miniskill at first), also switch miniskill if the same miniskill has been used 4 times
        """
        corrected_topic = self.state_manager.current_state.corrected_topic
        current_corrected_ask_intent = self.state_manager.current_state.corrected_ask_intent
        miniskill_to_turn_used = self.state_manager.current_state.miniskill_to_turn_used
        last_state_visited_threads = self.state_manager.last_state.get('visited_threads')
        last_state_new_topic = self.state_manager.last_state.get('new_topic')
        last_state_corrected_ask_intent = self.state_manager.last_state.get('corrected_ask_intent')
        dont_include_current_miniskill = False
        if int(self.state_manager.last_state.get('number_of_turns_on_same_miniskill', self.FIRST_TURN)) >= self.TURNS_BEFORE_SWITCHING:
            dont_include_current_miniskill = True
            corrected_topic = self.state_manager.last_state.get('old_topic')
        temp_corrected_ask_intent = MiniskillChooser().no_topic_switch_miniskill(corrected_topic, current_corrected_ask_intent, miniskill_to_turn_used, last_state_visited_threads, dont_include_current_miniskill, last_state_new_topic, last_state_corrected_ask_intent)
        if current_corrected_ask_intent == IntentNames.request_movie_intent:
            return None
        if current_corrected_ask_intent == IntentNames.ask_question_intent:
            return None
        # we cannot decouple saved_intent and temp_corrected_ask_intent here at this time
        if temp_corrected_ask_intent is not None and temp_corrected_ask_intent != current_corrected_ask_intent:
            self.state_manager.current_state.saved_intent = current_corrected_ask_intent
            self.state_manager.current_state.corrected_ask_intent = temp_corrected_ask_intent
            if dont_include_current_miniskill:
                # we cannot decouple setting corrected_topic from changing miniskills after a certain number of turns at this time
                self.state_manager.current_state.corrected_topic = self.state_manager.last_state.get('old_topic')

    def _update_number_of_turns_on_same_miniskill(self):
        """
        Update number of turns on the same miniskill
        """
        if self.state_manager.current_state.corrected_ask_intent == self.state_manager.last_state.get('corrected_ask_intent') and self.state_manager.current_state.intent != self.state_manager.current_state.corrected_ask_intent:
            self.state_manager.current_state.number_of_turns_on_same_miniskill = str(int(self.state_manager.last_state.get('number_of_turns_on_same_miniskill', self.FIRST_TURN)) + 1)
        else:
            self.state_manager.current_state.number_of_turns_on_same_miniskill = self.FIRST_TURN

    def execute(self):
        """
        Set corrected_ask_intent based on the regex expressions
        """
        # corrected_ask_intent is used to assign the task to a miniskill
        self.state_manager.current_state.set_new_field('corrected_ask_intent', None)
        # require_confirmation prioritizes yes/no utterances over others if set to true
        self.state_manager.current_state.set_new_field('require_confirmation', False)
        # keeps track of which miniskills were used in each turn
        self.state_manager.current_state.set_new_field('miniskill_to_turn_used', None)
        # keeps track of what the final topic sent to the miniskill is
        self.state_manager.current_state.set_new_field('corrected_topic', None)
        # keeps track of the saved intent (ex. navigational intent, previous used miniskill's intent)
        self.state_manager.current_state.set_new_field('saved_intent', None)
        # keep track of reprompt
        self.state_manager.current_state.set_new_field('reprompt', None)
        # keep track of how many turns the bot has been on the same miniskill and switch to another miniskill if it has been more than 4 turns
        self.state_manager.current_state.set_new_field('number_of_turns_on_same_miniskill', self.FIRST_TURN)
        
        # step 1: initialize and updates this turn's miniskill_to_turn_used from last turn
        self._initialize_miniskill_to_turn_used()
        # if the first state is launch_request_intent, we don't want to run any more steps
        if self.state_manager.current_state.intent == IntentNames.launch_request_intent:
            return None

        # step 2: overwrite ask intent
        # do not need to process any other information if inappropriate topic
        self._modify_ask_intent()

        # step 3: set the corrected_topic
        self._set_corrected_topic()

        # step 4: generate corrected ask intent
        # update navigational intents and manually check miniskill invocations from ASR, save intent if needed to saved_intent
        self._generate_corrected_ask_intent()
        # if the user is responding to the launch_request_intent, we don't want to run any more steps
        if self.state_manager.current_state.corrected_ask_intent == IntentNames.greeting_intent or self.state_manager.current_state.corrected_ask_intent == IntentNames.launch_request_intent:
            return None

        # step 5: check if the corrected ask intent has a topic, if not, save corrected_ask_intent into saved_intent and choose a new miniskill
        self._check_topic_exists()

        # step 6: updates the miniskill_to_turn_used dictionary or goes to invalid intent
        self._update_miniskill_to_turn_used()

        # step 7: update how many turns the miniskill has been used
        self._update_number_of_turns_on_same_miniskill()