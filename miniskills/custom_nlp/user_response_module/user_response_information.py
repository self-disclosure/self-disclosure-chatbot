import json
from cobot_core.state import serialize_to_json

class UserResponseInformation(object):
    """
    Object with four attributes for use response information, user decision, user knowledge, user satisfaction and user side
    """

    def __init__(self, user_decision: str = None, user_knowledge: str = None, user_satisfaction: str = None, user_side: str = None):
        # when the user says yes/no or related words to Alexa's prompts
        self.user_decision = user_decision

        # user knows/doesn't know the information Alexa has given
        self.user_knowledge = user_knowledge

        # how satisfied the user is with the information Alexa has given
        self.user_satisfaction = user_satisfaction

        # if the user agrees with what Alexa has said
        self.user_side = user_side

    def to_json(self):
        return json.dumps(self, default=serialize_to_json)

@serialize_to_json.register(UserResponseInformation)
def serialize_to_json(val):
    return {
        'user_decision': val.user_decision,
        'user_knowledge': val.user_knowledge,
        'user_satisfaction': val.user_satisfaction,
        'user_side': val.user_side
    }