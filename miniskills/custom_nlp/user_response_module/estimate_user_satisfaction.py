from miniskills.constants.intent_names import IntentNames
from miniskills.constants.user_response_information_names import UserResponseInformationNames
import re

class EstimateUserSatisfaction(object):
    POSITIVE = UserResponseInformationNames.POSITIVE
    NEGATIVE = UserResponseInformationNames.NEGATIVE
    STRONG_NEGATIVE = UserResponseInformationNames.STRONG_NEGATIVE

    def estimate(self, last_state_require_confirmation, last_state_response, current_intent, text, last_state_corrected_ask_intent):
        """
        Check for user satisfaction and type
        """
        # yes/no intent has priority over manual regexes
        if text is None:
            if current_intent == IntentNames.amazon_yes_intent and last_state_require_confirmation is False:
                if last_state_response.find('You can say "yes", to continue') < 0:
                    return self.POSITIVE

        # manual regexes
        if re.search("|".join([r'\S (inappropriate)\b',
                               r'\bnot\b.*\b(funny|informative|appropriate)\b',
                               r'\bwho cares\b',
                               r'\b(I|we) (don\'t|do not)\b.*(care|want|wanna)\b',
                               r'\bnot\b.*\b(interested)\b',
                               r'\blet\'s not\b',
                               r'\bstop\b',
                               r'\b(angry|upset|sad|mad|stupid|dumb|confused)\b',
                               r'\b(fuck|hell)\b',
                               r'\byou are\b.*\b(mean|broken)\b',
                               r'\b(I|we) (wanna|want to) (.*)$']), # other
                               text) is not None:
            return self.STRONG_NEGATIVE
        elif re.search("|".join([r'\b(that\'s|that|this|it\'s|it|very|they\'re|they)\b(.*)\b(cool|informative|interesting|funny|amusing|surprising|fascinating|awesome|exciting|hilarious)\b',
                                 r'\b(.*)\b(nice|great|good|perfect|glad|happy|surprise|I like|I love)\b',
                                 r'\b(ha|ha )+\b',
                                 r'\b(lol)\b',
                                 r'^(cool|awesome|nice|great|good|perfect|wow|interesting)\b']),
                                 text) is not None:
             # check negation
            if re.search(r'\b(not|no|don\'t|doesn\'t|didn\'t)\b', text) is not None:
                if re.search(r'\b(informative|funny)\b', text) is not None:
                    return self.STRONG_NEGATIVE
                else:
                    return self.NEGATIVE
            else:
                return self.POSITIVE
        elif re.search("|".join([r'\b(.*)\b(confusing|awful|disgusting|horrible|terrible|scary|upset|sorry|bad|sad|insane|boring|garbage)\b',
                                 r'\bthat can\'t be true\b',
                                 r'\bthat (doesn\'t|does not) sound right\b',
                                 r'\b(that\'s|that is)\b.*\b(incorrect|fake|wrong|a lie|lie)\b',
                                 r'\bI doubt it\b',
                                 r'^(really|what)$',
                                 r'\bis that (real|true)\b',
                                 r'\bare you sure\b',
                                 r'\bI\b.*\b(told|said|tell) you\b',
                                 r'\bI didn\'t get it\b',
                                 r'\bI don\'t understand\b',
                                 r'\byou puzzle me\b',
                                 r'\bnot\b(.*)\b(nice|great|good|interesting)\b',
                                 r'\bI hate\b',
                                 r'\b(that\'s|that|this|it\'s|it)\b(.*)\b(mean)\b',
                                 r'\bI (don\'t|do not)\b.*(like)',
                                 r'\bI\'m not a fan\b']),
                                 text) is not None:
            return self.NEGATIVE