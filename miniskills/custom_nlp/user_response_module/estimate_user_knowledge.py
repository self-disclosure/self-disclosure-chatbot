from miniskills.constants.intent_names import IntentNames
from miniskills.constants.responses import Responses
from miniskills.constants.user_response_information_names import UserResponseInformationNames
import re

class EstimateUserKnowledge(object):
    KNOWN = UserResponseInformationNames.KNOWN
    NOT_KNOWN = UserResponseInformationNames.NOT_KNOWN

    def estimate(self, last_state_require_confirmation, last_state_response, current_intent, text, last_state_corrected_ask_intent):
        """
        Estimate user knowledge
        """
        # yes/no intent has priority over manual regex patterns
        if last_state_require_confirmation is not None:
            for response in (Responses.ARE_YOU_FAMILIAR_WITH + Responses.INFORM_NEWS_DATE_SUFFIX + Responses.INFORM_NEWS_DATE_CONFIRM):
                response = response.strip()
                if last_state_response.find(response) >= 0:
                    if current_intent == IntentNames.amazon_yes_intent:
                        return self.KNOWN
                    elif current_intent == IntentNames.amazon_no_intent:
                        return None
        elif last_state_response is not None:
            if last_state_response.find('Did you know that') >= 0:
                if current_intent == IntentNames.amazon_yes_intent:
                    return self.KNOWN
                elif current_intent == IntentNames.amazon_no_intent:
                    return self.NOT_KNOWN

        if re.search("|".join([r'\b(I have not|I haven\'t|I did not|I didn\'t|I don\'t|I do not)\b',
                               r'\b(nobody|never)\b']),
                               text) is not None:
            return self.NOT_KNOWN
        elif re.search("|".join([r'\b(I have|I\'ve|I|already|I did|I do)\b(.*)\b(knew|know|heard|seen|watched|read)\b',
                                 r'\bI (did|do|have)\b(.*)\b']),
                                 text) is not None:
            return self.KNOWN