from cobot_core.state_manager import StateManager
from cobot_core.service_module import LocalServiceModule
from miniskills.constants.intent_names import IntentNames
from miniskills.custom_nlp.user_response_module.register_user_response_information import RegisterUserResponseInformation
from miniskills.custom_nlp.user_response_module.user_response_information import UserResponseInformation

class UserResponseModule(LocalServiceModule):
    def _get_text_from_navigation(self):
        """
        Get text from navigation slot
        """
        if self.state_manager.current_state.slots is not None:
            if self.state_manager.current_state.slots.get('navigation') is not None:
                if self.state_manager.current_state.slots['navigation'].get('value') is not None:
                    text = self.state_manager.current_state.slots['navigation']['value']
                    return text

    def execute(self):
        """
        Register User Response Information
        """
        if self.state_manager.last_state is not None:
            last_state_require_confirmation = self.state_manager.last_state.get('require_confirmation')
            last_state_response = self.state_manager.last_state['response']
            current_intent = self.state_manager.current_state.intent
            text_from_navigation = self._get_text_from_navigation()
            last_state_corrected_ask_intent = self.state_manager.last_state.get('corrected_ask_intent')
            return RegisterUserResponseInformation().execute(last_state_require_confirmation, last_state_response, current_intent, text_from_navigation, last_state_corrected_ask_intent)
        else:
            return UserResponseInformation()
