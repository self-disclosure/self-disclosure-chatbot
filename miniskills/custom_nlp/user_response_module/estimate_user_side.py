from miniskills.constants.intent_names import IntentNames
from miniskills.constants.user_response_information_names import UserResponseInformationNames
import re

class EstimateUserSide(object):
    AGREE = UserResponseInformationNames.AGREE
    DISAGREE = UserResponseInformationNames.DISAGREE
    UNSURE = UserResponseInformationNames.UNSURE_SIDE

    def estimate(self, last_state_require_confirmation, last_state_response, current_intent, text, last_state_corrected_ask_intent):
        """
        Check for user side
        """
        # manual regexes
        if re.search("|".join([r'(makes sense|true|right|correct|kind of|kinda|probably)\b',
                               r'\bI (totally |completely ){0,1}(agree|think so)\b',
                               r'\b(that|this|it) makes sense\b',
                               r'\b(good|great|excellent|nice) point\b',
                               r'\bthat\'s (reasonable|correct|right|true)\b']),
                               text) is not None:
            return self.AGREE
        elif re.search("|".join([r'^not really\b',
                                 r'\bI\b.*\bdisagree\b',
                                 r'\bI\b.*\b(don\'t|do not) (agree|think so)\b',
                                 r'\bit (doesn\'t|does not) make\b.*\bsense\b',
                                 r'\b(that|this|it) (make|makes) no sense\b',
                                 r'\bthat\'s\b.*\b(wrong|bullshit)\b',
                                 r'\bthat\'s not the case\b']),
                                 text) is not None:
            return self.DISAGREE
        elif re.search("|".join([r'^maybe\b',
                                 r'\bI (don\'t|do not) know$',
                                 r'\bI\'m not sure$',
                                 r'\bI (don\'t|do not) have any opinion\b',
                                 r'\bI\'m running out of (opinion|opinions)\b',
                                 r'\bI (really ){0,1}have no (opinion|idea)\b']),
                                 text) is not None:
            return self.UNSURE