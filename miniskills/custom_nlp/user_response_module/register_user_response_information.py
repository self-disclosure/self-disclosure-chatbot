from miniskills.constants.intent_names import IntentNames
from miniskills.constants.user_response_information_names import UserResponseInformationNames
from miniskills.custom_nlp.user_response_module.estimate_user_decision import EstimateUserDecision
from miniskills.custom_nlp.user_response_module.estimate_user_knowledge import EstimateUserKnowledge
from miniskills.custom_nlp.user_response_module.estimate_user_satisfaction import EstimateUserSatisfaction
from miniskills.custom_nlp.user_response_module.estimate_user_side import EstimateUserSide
from miniskills.custom_nlp.user_response_module.user_response_information import UserResponseInformation

class RegisterUserResponseInformation(object):
    """
    Parse user information and save it into an UserResponseInformation object
    """

    def execute(self, last_state_require_confirmation, last_state_response, current_intent, text, last_state_corrected_ask_intent):
        """
        Create and update UserResponseInformation object
        """

        user_decision = None
        user_knowledge = None
        user_satisfaction = None
        user_side = None

        text = str(text)

        estimate_user_response_information = [EstimateUserKnowledge(), EstimateUserSatisfaction(), EstimateUserSide()]

        specific_user_response_information = None
        if last_state_require_confirmation is True:
            specific_user_response_information = EstimateUserDecision().estimate(last_state_require_confirmation, last_state_response, current_intent, text, last_state_corrected_ask_intent)

        if specific_user_response_information is None:
            for estimator in estimate_user_response_information:
                specific_user_response_information = estimator.estimate(last_state_require_confirmation, last_state_response, current_intent, text, last_state_corrected_ask_intent)
                if specific_user_response_information is not None:
                    if type(estimator) == EstimateUserKnowledge:
                        user_knowledge = specific_user_response_information
                    elif type(estimator) == EstimateUserSatisfaction:
                        user_satisfaction = specific_user_response_information
                    else:
                        user_side = specific_user_response_information
                    break
        else:
            user_decision = specific_user_response_information

        return UserResponseInformation(user_decision, user_knowledge, user_satisfaction, user_side)