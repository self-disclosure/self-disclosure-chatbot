from miniskills.constants.intent_names import IntentNames
from miniskills.constants.responses import Responses
from miniskills.constants.user_response_information_names import UserResponseInformationNames
import re

class EstimateUserDecision(object):
    ACCEPT = UserResponseInformationNames.ACCEPT
    REJECT = UserResponseInformationNames.REJECT
    UNSURE = UserResponseInformationNames.UNSURE

    def estimate(self, last_state_require_confirmation, last_state_response, current_intent, text, last_state_corrected_ask_intent):
        """
        Estimate user decision
        """
        # yes/no intent has priority over manual regex patterns
        if last_state_require_confirmation is True:
            for response in (Responses.ARE_YOU_FAMILIAR_WITH + Responses.INFORM_NEWS_DATE_SUFFIX + Responses.INFORM_NEWS_DATE_CONFIRM):
                response = response.strip()
                if last_state_response.find(response) >= 0:
                    return None
            if current_intent == IntentNames.amazon_yes_intent:
                return self.ACCEPT
            elif current_intent == IntentNames.amazon_no_intent:
                return self.REJECT
        else:
            if text is None:
                if current_intent == IntentNames.amazon_yes_intent:
                    if last_state_response.find('You can say "yes", to continue') >= 0:
                        return self.ACCEPT

        # manual regex patterns
        if re.search("|".join([r'/^please$',
                               r'^(sure|okay|ok|certainly|of course|fantastic|absolutely)\b',
                               r'\b(go ahead|why not|sounds good|sounds great|good idea)$',
                               r'^(go ahead|why not|sounds good|sounds great|good idea)\b',
                               r'\bgreat\b',
                               r'\bof course\b',
                               r'^(cough|oh cough|across)$',
                               r'\ball the time\b',
                               r'\bI like\b']),
                               text) is not None:
            return self.ACCEPT
        elif re.search("|".join([r'\bI\'m good( now| for now){0,1}$',
                                 r'\bthat\'s alright\b',
                                 r'^not really\b',
                                 r'^nothing\b',
                                 r'^never\b',
                                 r'^please don\'t\b',
                                 r'^I (don\'t|do not) (want|like|wanna)\b',
                                 r'\blet\'s not\b',
                                 r'^I\'m not interested\b',
                                 r'\bboring$',
                                 r'\bnot right now\b',
                                 r'^(sorry|I\'m sorry)\b']),
                                 text) is not None:
            return self.REJECT
        elif re.search("|".join([r'\bI\'m (really ){0,1}not sure\b',
                                 r'\bI (really ){0,1}(don\'t|do not) know\b',
                                 r'\bI (really ){0,1}(have no|haven\'t)\b',
                                 r'\bI (really ){0,1}(don\'t|do not) have\b',
                                 r'\bI\'m (really ){0,1}(don\'t|do not) understand\b']),
                                 text) is not None:
            return self.UNSURE

        # last state was invalid intent
        if last_state_corrected_ask_intent == IntentNames.invalid_intent:
            if last_state_response.find('continue') >= 0:
                if current_intent == IntentNames.amazon_yes_intent:
                    return self.ACCEPT
                elif current_intent == IntentNames.amazon_no_intent:
                    return self.REJECT