from cobot_core.service_module import ToolkitServiceModule
from multiprocessing.pool import ThreadPool

class EviModule(ToolkitServiceModule):
    def evi_query_fix(self, topic, wh_word='who'):
        if topic and wh_word:
            return wh_word + ' is ' + topic

    def evi_call(self, question_text):
        if question_text:
            self.logger.info('Evi question_text: %s', question_text)
            r = self.toolkit_service_client.get_answer(question=question_text)
            result = ''
            if 'response' in r:
                result = r['response']
            elif 'message' in r:
                result = r['message']
            self.logger.info('Evi response: %s', result)
            return result
        return None

    def execute(self):
        pool = ThreadPool(processes=4)
        evi_module_output = {'asr_text': None, 'current_topic_from_slots': None, 'old_topic': None}
        async_evi_module_asr_text = None
        async_evi_module_old_topic = None
        async_evi_module_current_topic_from_slots = None
        try:
            async_evi_module_asr_text = pool.apply_async(self.evi_call, ([self.state_manager.current_state.text]))
        except:
            pass

        # mostly used to give more information about directors and actors
        try:
            evi_query_fix = self.evi_query_fix(self.state_manager.last_state.get('old_topic'))
            async_evi_module_old_topic = pool.apply_async(self.evi_call, ([evi_query_fix]))

        except:
            pass

        try:
            # try to get the wh_word from the slots
            if 'wh_word' in self.state_manager.current_state.slots:
                evi_query_fix = self.evi_query_fix(self.state_manager.current_state.slots['topic']['value'], self.state_manager.current_state.slots['wh_word']['value'])
            else:
                evi_query_fix = self.evi_query_fix(self.state_manager.current_state.slots['topic']['value'])
            async_evi_module_current_topic_from_slots = pool.apply_async(self.evi_call, [evi_query_fix])
        except:
            pass
        try:
            evi_module_output['asr_text'] = async_evi_module_asr_text.get()
        except:
            pass
        try:
            evi_module_output['old_topic'] = async_evi_module_old_topic.get()
        except:
            pass
        try:
            evi_module_output['current_topic_from_slots'] = async_evi_module_current_topic_from_slots.get()
        except:
            pass
        return evi_module_output

