from cobot_core.state_manager import StateManager
from cobot_core.service_module import LocalServiceModule
from miniskills.miniskill_dynamo_db_manager import MiniskillDynamoDbManager
import random

class KnowledgeModule(LocalServiceModule):
    def execute(self):
        # generic knowledge pull for news (pull a thread which has not been visited)
        # can replace these with any tables for any miniskill, the end goal is to have knowledge modules in parallel for each Reddit table
        topic_table_names = ['News_Topics_r1.0.0']
        thread_table_names = ['News_Threads_r1.0.0']
        self.state_manager.current_state.set_new_field('visited_threads', None)

        thread_data = []
        
        miniskill_dynamo_db_manager = MiniskillDynamoDbManager()
        topic = self.state_manager.current_state.corrected_topic
        if not topic:
            try:
                topic = self.state_manager.current_state.slots['topic']['value']
            except:
                pass

        if topic:
            topic = topic.lower()
            for i in range(len(topic_table_names)):
                try:
                    # pull thread id and list of threads for topic
                    thread_information = miniskill_dynamo_db_manager.pull_threads_for_topic(topic_table_names[i], topic)
                    thread_id = thread_information['thread_id']
                    thread_list = thread_information['thread_list']

                    # thread_id is always the last element in thread_list
                    thread_list.pop()

                    # update visited threads
                    if self.state_manager.current_state.visited_threads is None:
                        self.state_manager.current_state.visited_threads = [thread_id]
                    else:
                        while thread_id in self.state_manager.current_state.visited_threads:
                            thread_id = random.choice(thread_list)
                            thread_list.remove(thread_id)
                        self.state_manager.current_state.visited_threads.append(thread_id)

                    self.state_manager.current_state.current_topic_threads = thread_list
                    thread_data.append(miniskill_dynamo_db_manager.pull_thread_title(thread_table_names[i], thread_id))
                except IndexError:
                    # catches when the topic has no threads
                    pass

        returned_thread_data = None
        if thread_data:
            # choose random thread from possible threads from tables
            returned_thread_data = random.choice(thread_data)
            self.state_manager.current_state.require_confirmation = False
        return returned_thread_data