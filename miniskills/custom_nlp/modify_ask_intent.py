from miniskills.constants.intent_names import IntentNames
from miniskills.constants.intent_to_miniskill_map import IntentToMiniskillMap
import re

class ModifyAskIntent(object):    
    """
    Modify ASK Intent based on manual corrections
    """
    # maximum number of possible topics in categorical topics
    MAX_NUMBER_OF_TOPICS = 130

    def _regex_yes_or_no_intent(self, text):
        """
        Capture additional ways to say yes or no
        """
        intent = None
        if text is not None:
            if re.match("|".join([r'\byes\b', r'\byep\b', r'\byeah\b', r'\byah']), text) is not None:
                intent = IntentNames.amazon_yes_intent
            elif re.match("|".join([r'\bno\b', r'\bnope\b', r'\bnah\b']), text) is not None:
                intent = IntentNames.amazon_no_intent
        return intent

    def _update_intent_for_ignored_and_deflected_topics(self, text, current_intent):
        """
        Catch ignored and deflected topics
        """
        intent = None
        if text != '' and current_intent != IntentNames.navigation_intent:
            if re.match("|".join([r'\b(topic|topics|subject|subjects|fact|facts|trivia|opinion|opinions|piece|joke|jokes|personality|quiz|news|test|tip|tips|suggestion|suggestions|advice|advices|question|questions|answer|answers)\b',
                                  r'\b(talk|chat)\b',
                                  r'^(hi|hey|hello)$',
                                  r'^(yes|yep|yeah|yah|no|nope|hum|huh|wow|cool|great|good|right)$',
                                  r'^(thing|things|nothing|anything|something|everything)$',
                                  r'^(everyone)$',
                                  r'^(else|another|other)$',
                                  r'^(this|that|these|those)$',
                                  r'^(you|your|yourself|i|my|myself|ours|ourselves|he|his|him|himself|she|her|hers|herself|it|its|itself|they|their|them|themselves)$',
                                  r'^(your mom|your dad|your mother|your father|your mommy|your daddy)$',
                                  r'^(person|people|nobody|somebody|someone|someone else)$',
                                  r'^(cause|pause)$',
                                  r'^(what|whats)$',
                                  r'\b(thank|thanks|thanx)\b',
                                  r'^(idea|ideas|good idea|good ideas)$',
                                  r'\b(hundred|hundreds|thousand|thousands|million|millions|billion|billions)\b',
                                  r'^(day|days|someday|morning|afternoon|evening|nigt|today|yesterday|tomorrow)$',
                                  r'^(time|hour|hours|minute|minutes|week|weeks|year|years|decade|decades)$',
                                  r'\b(past|previous|next)\b',
                                  r'^(a|an|some|one|1|the|and|and the)$',
                                  r'^(name|weather|start|play|can|volume)$',
                                  r'^(whole)$',
                                  r'^(about|bout|way|before|bit|little|a little|a little bit|little bit|many|guess|i guess)$',
                                  r'^(lot|lot of|a lot|a lot of|a lot of|a lot of|lot of)( \w+){0,1}$',
                                  r'^(fine|help)$',
                                  r'^(lol)$',
                                  r'^(harder)$',
                                  r'^(god|my god|godness|my godness|goodness|my goodness)$',
                                  r'\b(sex|fuck|blow job|blowjob|stripper|strippers)\b',
                                  r'\b(jerk|jerks|jerkoff|jerkoffs|fap|faps|fapped|fapping|fapper|fappers|fappetite|fappetites|fappeninng)\b',
                                  r'\b(kill|kills|killed|suicide|suicides|suicided|poison|die|dead|kidnap)\b',
                                  r'\b(abortion|abortions|racism|racist|racists|terror|terroism)\b',
                                  r'\b(god|godness|goodness|nazi|nazis|muslim|muslims|islam|islamic|islamist)\b',
                                  r'\b(stock|stocks|investment|invest|buy|purchase)\b']), text) is not None:
                intent = IntentNames.ignored_and_deflected_intent
        return intent

    def _update_intent_for_navigational_intent(self, text_from_navigation):
        """
        Update intent with navigational intent
        """
        # repeat intent
        if re.match("|".join([r'\brepeat$', 
                              r'\brepeat please\b', 
                              r'\brepeat the question\b', 
                              r'\bcan you repeat that\b', 
                              r'\bcan you say that again\b', 
                              r'\bsay again\b', 
                              r'\bI didn\'t follow\b', 
                              r'\bI didn\'t hear you\b',
                              r'\b(who|what) did you say$',
                              r'\bwhat did you say$',
                              r'^what$']), text_from_navigation) is not None:
            return IntentNames.repeat_intent
        elif re.match("|".join([r'\bwhat else$',
                                r'\bcontinue$',
                                r'^(ok|okay)$',
                                r'\btell (me|mean) (more){0,1}(about|about) (the ){0,1}(it|movie|movies|film|films|news|new)\b',
                                r'\btell (me|mean) (some more|more)$',
                                r'\btell (me|mean) (some more|more) please$',
                                r'\btell (me|mean)$',
                                r'\btell (me|mean) please$',
                                r'\bwhy not$']), text_from_navigation) is not None:
            return IntentNames.continue_intent
        elif re.match("|".join([r'\bcancel\b',
                                r'\bthat\'s enough\b',
                                r'\benough\b.*\b(jokes|joke|news|question|questions)\b',
                                r'\bI (do not|don\'t|dont)\b.*\b(talk|chat|discuss)\b',
                                r'\b(not interested)\b.*\b(talking|chatting|discussing)\b']), text_from_navigation) is not None:
            return IntentNames.cancel_intent

    # manual correction for stop intent
    def _update_intent_for_stop_intent(self, text_from_navigation, text):
        stop_intent_phrases = [r'\bgoodbye\b',
                               r'\bbye\b',
                               r'\bbye bye\b',
                               r'\blet\'s stop\b',
                               r'\bno stop\b',
                               r'\bno please stop\b',
                               r'\bI\'m done\b',
                               r'\bI\'m done with the conversation\b',
                               r'\bend the conversation\b',
                               r'\bstop the conversation\b',
                               r'\blet\'s end the conversation\b',
                               r'\blet\'s stop the conversation\b',
                               r'\b(stop|end|quit)\b.*\b(talking|talk|chatting|chat|conversation|now)$',
                               r'\b(stop|end|quit)\b.*\b(boring|bored)$',
                               r'\b(stop|end|quit|exit)$'
                               r'\b(stop|end|quit) it$',
                               r'^and',
                               r'\b(shut|turn) (down|off)$',
                               r'^(shut|turn) (down|off)\b',
                               r'\bshut you mouth\b',
                               r'\bbe quiet\b',
                               r'\bgoodbye$',
                               r'\b(go|going) to (sleep|bed)\b',
                               r'\bgood night\b',
                               r'\btake a nap\b',
                               r'\bI (don\'t|do not) want to (talk|speak) to you\b',
                               r'\bI (don\'t|do not) want to (talk|chat|speak) anymore\b',
                               r'\b(I\'m|we are|we\'re) (done|finished)\b.*\b(talking|chatting|conversation|now|today)\b',
                               r'\b(I\'m|we are|we\'re) (done|finished)$',
                               r'\b(I\'m|we are|we\'re) (done|finished) thank you$',
                               r'\b(enough|that\'s it) for (now|today)\b',
                               r'\bI\'m\b.*\btired\b',
                               r'\btalk to you (later|next)\b',
                               r'\bI (have|need) to (go|leave)\b',
                               r'\bI\'m gonna call it a day\b',
                               r'\bgo away\b']
        if re.match("|".join(stop_intent_phrases), text_from_navigation) is not None or re.match("|".join(stop_intent_phrases), text) is not None:
            return IntentNames.amazon_stop_intent

    def _update_intent_for_request_movies_intent(self, text_from_navigation, text):
        if re.search(r'\b(bout|about){0,1}(S+ ){0,1}(movies|movie|films|film)(.*)$', text_from_navigation) is not None or re.search(r'\b(bout|about){0,1}(S+ ){0,1}(movies|movie|films|film)(.*)$', text) is not None:
            return IntentNames.request_movie_intent

    def execute(self, last_state_require_confirmation, last_state_corrected_ask_intent, current_intent, topic, saved_intent, text_from_navigation, text):
        # for an unknown reason, this is not passed as a string
        text_from_navigation = str(text_from_navigation)
        text = str(text)

        # next is cancel in confirmation but continue for everything else
        if current_intent == IntentNames.amazon_next_intent:
            if last_state_require_confirmation and last_state_corrected_ask_intent != IntentNames.invalid_intent:
                return IntentNames.cancel_intent

        intent_choices = [self._update_intent_for_request_movies_intent(text_from_navigation, text), self._regex_yes_or_no_intent(text_from_navigation), self._update_intent_for_stop_intent(text_from_navigation, text), self._update_intent_for_navigational_intent(text_from_navigation), self._update_intent_for_ignored_and_deflected_topics(text, current_intent)]
        for intent in intent_choices:
            if intent is not None:
                return intent

        for intent, miniskill in IntentToMiniskillMap.intent_map.items():
            if current_intent == intent:
                return current_intent