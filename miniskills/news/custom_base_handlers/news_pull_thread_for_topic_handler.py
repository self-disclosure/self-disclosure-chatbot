from miniskills.base_handlers.pull_thread_for_topic_handler import PullThreadForTopicHandler
from miniskills.miniskill_dynamo_db_manager import MiniskillDynamoDbManager
from datetime import datetime
import pytz
import random

class NewsPullThreadForTopicHandler(PullThreadForTopicHandler):
    """
    Basic template for news to handle pulling threads for the same topic
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Need created_utc to create response, so run return_nlg_content
        """
        self.return_nlg_content(handler_selection_features)
        return handler_dispatcher.dispatch(handler_dispatcher, handler_selection_features)

    def _set_require_confirmation(self, handler_selection_features):
        handler_selection_features.state_manager.current_state.require_confirmation = True

    def return_nlg_content(self, handler_selection_features):
        """
        Handle different miniskills need different fields in the dynamodb table
        """
        self.dynamo_db_pull(handler_selection_features)
        created_utc = int(MiniskillDynamoDbManager().pull_created_utc(handler_selection_features.thread_table_name, handler_selection_features.nlg_content['thread_id']))
        # calculate how far away from the current day this article was written
        current_timezone = pytz.timezone('America/Los_Angeles')

        # current times in UTC
        current_time = datetime.now()
        created_utc_datetime_object = datetime.utcfromtimestamp(created_utc)
        # times in PST
        current_time = pytz.utc.localize(current_time, is_dst=None).astimezone(current_timezone)
        created_utc_datetime_object = pytz.utc.localize(created_utc_datetime_object, is_dst=None).astimezone(current_timezone)

        utc_time = created_utc_datetime_object.strftime('%B %d %Y')

        if current_time.date() == created_utc_datetime_object.date():
            utc_time = ' today '
        elif (current_time - created_utc_datetime_object).days == 0:
            utc_time = ' yesterday '
        elif current_time.isocalendar()[1] - created_utc_datetime_object.isocalendar()[1] == 1 or (current_time.isocalendar()[1] == 1 and created_utc_datetime_object.isocalendar()[1] == 52):
            utc_time = ' last week '

        handler_selection_features.nlg_content['created_utc'] = utc_time
        self._set_require_confirmation(handler_selection_features)