from miniskills.base_handlers.switch_topic_handler import SwitchTopicHandler
from miniskills.constants.action_names import ActionNames
from miniskills.handler_dispatch_generic_response_generator.handler import Handler
from miniskills.miniskill_dynamo_db_manager import MiniskillDynamoDbManager
import random
import re

class NewsSwitchTopicHandler(SwitchTopicHandler):
    """
    Basic template to handle switching topic dynamodb pulls
    """
    def _pull_thread_topic(self, handler_selection_features, thread_id):
        miniskill_dynamo_db_manager = MiniskillDynamoDbManager()
        handler_selection_features.state_manager.current_state.topics_within_a_thread = miniskill_dynamo_db_manager.pull_topics_within_a_thread(handler_selection_features.thread_table_name, thread_id)
        thread_topic = random.choice(handler_selection_features.state_manager.current_state.topics_within_a_thread)
        handler_selection_features.state_manager.current_state.new_topic = thread_topic
        handler_selection_features.nlg_content = thread_topic