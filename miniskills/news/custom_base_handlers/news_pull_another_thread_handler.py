from miniskills.base_handlers.pull_another_thread_handler import PullAnotherThreadHandler
from miniskills.news.custom_base_handlers.news_pull_thread_for_topic_handler import NewsPullThreadForTopicHandler
from miniskills.news.custom_base_handlers.news_pull_different_topic_in_thread_handler import NewsPullDifferentTopicInThreadHandler
from miniskills.constants.action_names import ActionNames
from miniskills.constants.plug_in_config import PlugInConfig
from miniskills.constants.responses import Responses
from miniskills.constants.speech_action_names import SpeechActionNames
from miniskills.constants.user_response_information_names import UserResponseInformationNames
import re

class NewsPullAnotherThreadHandler(PullAnotherThreadHandler):
    """
    Basic template to handle pulling another thread for the same topic dynamodb pulls
    """
    def dynamo_db_pull(self, handler_selection_features):
        is_continue = False
        for response in (Responses.CONFIRM_INFORM_NEWS + Responses.INFORM_NEWS_DATE_SUFFIX + Responses.INFORM_NEWS_DATE_CONFIRM):
            response = response.strip()
            if handler_selection_features.state_manager.last_state.get('response').find(response) >= 0:
                handler_selection_features.action_name = ActionNames.PULL_THREAD_FOR_TOPIC
                handler_selection_features.speech_action_name = SpeechActionNames.INFORM_TOPIC
                is_continue = True
        if not is_continue:
            if handler_selection_features.state_manager.current_state.user_response_information.user_knowledge == UserResponseInformationNames.NOT_KNOWN or handler_selection_features.state_manager.current_state.user_response_information.user_satisfaction == UserResponseInformationNames.POSITIVE and PlugInConfig.positive_user_information_miniskill_diversity_plug_in:
                handler_selection_features.action_name = ActionNames.PULL_THREAD_FOR_TOPIC
                handler_selection_features.speech_action_name = SpeechActionNames.INFORM_TOPIC
            else:
                handler_selection_features.action_name = ActionNames.PULL_ANOTHER_THREAD