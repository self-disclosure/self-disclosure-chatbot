from miniskills.constants.action_names import ActionNames
from miniskills.constants.dynamo_db_table_names import DynamoDbTableNames
from miniskills.constants.intent_names import IntentNames
from miniskills.constants.speech_action_names import SpeechActionNames
from miniskills.base_handlers.pull_different_topic_in_thread_handler import PullDifferentTopicInThreadHandler
from miniskills.miniskill_dynamo_db_manager import MiniskillDynamoDbManager
import random

class NewsPullDifferentTopicInThreadHandler(PullDifferentTopicInThreadHandler):
    """
    Basic template to handle pulling a different topic in the same thread
    """
    NEWS_EMPTY = 'news_empty'

    def _remove_current_topic_from_available_topics(self, handler_selection_features):
        if handler_selection_features.state_manager.current_state.topics_within_a_thread is None or len(handler_selection_features.state_manager.current_state.topics_within_a_thread) > 0:
            handler_selection_features.state_manager.current_state.topics_within_a_thread.remove(handler_selection_features.state_manager.last_state.get('corrected_topic'))
        else:
            handler_selection_features.speech_action_name = SpeechActionNames.NO_MORE_KNOWN
            handler_selection_features.action_name = ActionNames.SWITCH_TOPIC
            return self.NEWS_EMPTY

    def _topic_exists(self, handler_selection_features, table_name, topic, thread_list):
        miniskill_dynamo_db_manager = MiniskillDynamoDbManager()
        share_facts_exists = miniskill_dynamo_db_manager.check_topic_exists(DynamoDbTableNames.today_i_learned_topics, topic, handler_selection_features.state_manager.last_state.get('visited_threads'))
        shower_thoughts_exists = miniskill_dynamo_db_manager.check_topic_exists(DynamoDbTableNames.shower_thoughts_topics, topic, handler_selection_features.state_manager.last_state.get('visited_threads'))
        if share_facts_exists:
            handler_selection_features.state_manager.current_state.saved_intent = IntentNames.request_fact_intent
            return True
        elif shower_thoughts_exists:
            handler_selection_features.state_manager.current_state.saved_intent = IntentNames.request_opinion_intent
            return True
        return False

    def _update_handler_selection_features(self, handler_selection_features, topic):
        handler_selection_features.state_manager.current_state.require_confirmation = True
        handler_selection_features.state_manager.current_state.new_topic = topic
        handler_selection_features.state_manager.current_state.saved_intent = IntentNames.request_news_intent
        handler_selection_features.state_manager.current_state.topic_threads = None
        handler_selection_features.nlg_content = topic

    def _pull_thread_list(self, handler_selection_features):
        return self.NEWS_EMPTY