from miniskills.base_handlers.no_dynamo_db_pull_handler import NoDynamoDbPullHandler
from miniskills.news.custom_base_handlers.news_pull_another_thread_handler import NewsPullAnotherThreadHandler
from miniskills.news.custom_base_handlers.news_pull_different_topic_in_thread_handler import NewsPullDifferentTopicInThreadHandler
from miniskills.news.custom_base_handlers.news_pull_thread_for_topic_handler import NewsPullThreadForTopicHandler
from miniskills.base_handlers.switch_miniskill_handler import SwitchMiniskillHandler
from miniskills.news.custom_base_handlers.news_switch_topic_handler import NewsSwitchTopicHandler
from miniskills.global_handlers.global_navigational_handler import GlobalNavigationalHandler
from miniskills.handler_dispatch_generic_response_generator.handler_dispatch_response_generator import HandlerDispatchResponseGenerator
from miniskills.news.news_nlg_handlers import NewsNoDynamoDbPullNlgHandler, NewsSwitchMiniskillNlgHandler, NewsPullThreadForTopicNlgHandler, NewsSwitchTopicNlgHandler, NewsPullAnotherThreadNlgHandler, NewsPullDifferentTopicInThreadNlgHandler

class NewsResponseGenerator(HandlerDispatchResponseGenerator):
    """
    Initialize list of possible handlers, run dispatch, return a response for News
    """

    list_of_handlers = [NoDynamoDbPullHandler(), NewsPullAnotherThreadHandler(), NewsPullDifferentTopicInThreadHandler(), NewsPullThreadForTopicHandler(), SwitchMiniskillHandler(), NewsSwitchTopicHandler(), NewsNoDynamoDbPullNlgHandler(), NewsSwitchMiniskillNlgHandler(), NewsPullThreadForTopicNlgHandler(), NewsSwitchTopicNlgHandler(), NewsPullAnotherThreadNlgHandler(), NewsPullDifferentTopicInThreadNlgHandler(), GlobalNavigationalHandler()]

    def __init__(self, state_manager, module_name, service_module_config, handler_dispatcher = None, list_of_handlers = None):
        super(NewsResponseGenerator, self).__init__(state_manager, module_name, service_module_config, handler_dispatcher, self.list_of_handlers)