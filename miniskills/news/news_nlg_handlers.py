from miniskills.constants.action_names import ActionNames
from miniskills.constants.responses import Responses
from miniskills.base_nlg_handlers.no_dynamo_db_pull_nlg_handler import NoDynamoDbPullNlgHandler
from miniskills.base_nlg_handlers.pull_another_thread_nlg_handler import PullAnotherThreadNlgHandler
from miniskills.base_nlg_handlers.pull_different_topic_in_thread_nlg_handler import PullDifferentTopicInThreadNlgHandler
from miniskills.base_nlg_handlers.pull_thread_for_topic_nlg_handler import PullThreadForTopicNlgHandler
from miniskills.base_nlg_handlers.switch_miniskill_nlg_handler import SwitchMiniskillNlgHandler
from miniskills.base_nlg_handlers.switch_topic_nlg_handler import SwitchTopicNlgHandler
import random

class NewsNoDynamoDbPullNlgHandler(NoDynamoDbPullNlgHandler):
    """
    Handle no dynamodb pulls for News
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        confirm_resume_topic = random.choice(Responses.CONTINUE) + random.choice(Responses.CONVERSATION) + random.choice(Responses.ON) + handler_selection_features.state_manager.current_state.old_topic + '?'
        return self.nlg_template(handler_selection_features, confirm_resume_topic)

class NewsPullAnotherThreadNlgHandler(PullAnotherThreadNlgHandler):
    """
    Handle pulling another thread for News
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        continue_inform_topic = random.choice(Responses.ARE_YOU_FAMILIAR_WITH) + handler_selection_features.nlg_content
        inform_followup = continue_inform_topic
        did_you_know_response = continue_inform_topic
        confirm_resume_topic = random.choice(Responses.CONTINUE) + random.choice(Responses.CONVERSATION) + random.choice(Responses.ON) + handler_selection_features.state_manager.current_state.old_topic + '?'
        return self.nlg_template(handler_selection_features, continue_inform_topic, inform_followup, did_you_know_response, confirm_resume_topic)

class NewsPullDifferentTopicInThreadNlgHandler(PullDifferentTopicInThreadNlgHandler):
    """
    Handle pulling topic in the same thread for News
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        previously_mentioned = random.choice(Responses.PREVIOUSLY_MENTIONED) + handler_selection_features.nlg_content + '.'
        return self.nlg_template(handler_selection_features, previously_mentioned)

class NewsPullThreadForTopicNlgHandler(PullThreadForTopicNlgHandler):
    """
    Handle pulling thread for topic for News
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        random_number = random.randint(0, 1)
        if random_number == 0:
            inform_topic = random.choice(Responses.INFORM_NEWS_DATE_CONFIRM) + handler_selection_features.nlg_content['created_utc'] + '?' + random.choice(Responses.INFORM_NEWS) + handler_selection_features.nlg_content['thread_data']
        else:
            inform_topic = random.choice(Responses.INFORM_NEWS_DATE) + handler_selection_features.nlg_content['created_utc'] + '.' + random.choice(Responses.INFORM_NEWS) + handler_selection_features.nlg_content['thread_data'] + '.' + random.choice(Responses.INFORM_NEWS_DATE_SUFFIX)
        continue_inform = inform_topic
        inform_no_topic = inform_topic
        followup = random.choice(Responses.INFORM_NEWS_FOLLOWUP)
        return self.nlg_template(handler_selection_features, inform_topic, continue_inform, inform_no_topic, followup)

class NewsSwitchMiniskillNlgHandler(SwitchMiniskillNlgHandler):
    """
    Handle switching miniskill dynamodb pulls for switching to News miniskill
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        topic_exhausted_response = self.last_miniskill_topic_exhausted_response(handler_selection_features)
        topic = handler_selection_features.state_manager.current_state.corrected_topic
        handler_selection_features.state_manager.current_state.old_topic = topic
        confirm_inform_response = random.choice(Responses.CONFIRM_INFORM_NEWS) + topic
        return self.nlg_template(topic_exhausted_response, topic, confirm_inform_response)

class NewsSwitchTopicNlgHandler(SwitchTopicNlgHandler):
    """
    Handle switching topic dynamodb pulls for switching to News miniskill
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        topic_exhausted_response = random.choice(Responses.NEWS_TOPIC_EXHAUSTED)
        confirm_inform_response = random.choice(Responses.CONFIRM_INFORM_NEWS) + handler_selection_features.nlg_content
        return self.nlg_template(handler_selection_features, topic_exhausted_response, confirm_inform_response)