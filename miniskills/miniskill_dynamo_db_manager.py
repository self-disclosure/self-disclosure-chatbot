from boto3.dynamodb.conditions import Key
from cobot_python_sdk.dynamodb_manager import DynamoDbManager
import ast
import random
import re

class MiniskillDynamoDbManager(DynamoDbManager):
    """
    Interact with the Dynamodbmanager to pull data
    """
    def delete_article(self, topic):
        """
        Deletes articles from topic
        """
        # sometimes topics can be pulled when the article is removed and we don't want the topic to seem exhausted when it isn't
        if topic.startswith('a ') or topic.startswith('an ') or topic.startswith('the '):
            topic = topic.split(' ', 1)[1]
        return topic

    def check_topic_exists(self, topic_table_name, topic, visited_threads):
        """
        Check if the topic has threads in the table
        """
        if topic is None:
            return False

        # remove articles
        has_article = False
        if self.delete_article(topic) != topic:
            has_article = True

        topic = topic.lower()
        thread_list = self.query(topic_table_name, Key('topic').eq(topic))
        if not thread_list and not has_article:
            return False
        elif not thread_list and has_article:
            topic = self.delete_article(topic)
            thread_list = self.query(topic_table_name, Key('topic').eq(topic))
            if not thread_list:
                return False
        elif visited_threads:
            all_exhausted = True
            for thread in thread_list:
                # check if threadId is just a number
                try:
                    thread_id = int(thread['threadId'])
                except ValueError:
                    if thread['threadId'] not in visited_threads:
                        all_exhausted = False
            if all_exhausted:
                return False
            else:
                return True
        else:
            del thread_list[0]
            if not thread_list:
                return False
        return True

    def check_thread_exists_in_given_table(self, thread_table_name, thread_id):
        """
        Check if the thread exists in the given table; used when miniskill switches after a positive user response
        """
        if not self.query(thread_table_name, Key('threadId').eq(thread_id)):
            return False
        return True

    def is_primary_topic(self, thread_table_name, topic, thread_list):
        """
        Check if the topic is a primary topic
        """
        if not thread_list:
            return False
        for thread in thread_list:
            if self.query(thread_table_name, Key('threadId').eq(thread))[0].get('primaryTopic') == topic or self.query(thread_table_name, Key('threadId').eq(thread))[0].get('primaryTopic') == self.delete_article(topic):
                return True
        return False
    
    def pull_created_utc(self, thread_table_name, thread_id):
        """
        Pull created_utc with thread id given
        """
        created_utc = self.query(thread_table_name, Key('threadId').eq(thread_id))[0].get('created_utc')
        return created_utc

    def pull_thread_primary_topic(self, thread_table_name, thread_id):
        """
        Pull thread primary topic with thread id given
        """
        thread_primary_topic = self.query(thread_table_name, Key('threadId').eq(thread_id))[0].get('primaryTopic')
        return thread_primary_topic

    def pull_thread_title(self, thread_table_name, thread_id):
        """
        Pull thread title with thread id given
        """
        thread_data = self.query(thread_table_name, Key('threadId').eq(thread_id))[0]['title']
        return thread_data

    def pull_topics_within_a_thread(self, thread_table_name, thread_id):
        """
        Pull list of topics from thread
        """
        list_of_topics = self.query(thread_table_name, Key('threadId').eq(thread_id))[0]['topics']
        list_of_topics = ast.literal_eval(list_of_topics)
        return list_of_topics

    def pull_threads_for_topic(self, topic_table_name, topic):
        """
        Query for all possible threads
        """
        thread_information_list = self.query(topic_table_name, Key('topic').eq(topic))

        if not thread_information_list:
            topic = self.delete_article(topic)
            thread_information_list = self.query(topic_table_name, Key('topic').eq(topic))

        # removes the blank topic at the beginning of the list
        del thread_information_list[0]

        # parse thread_list into a list of just threads instead of a list of dictionaries with extra information
        thread_list = []
        for thread in thread_information_list:
            thread_list.append(thread['threadId'])

        thread_id = random.choice(thread_list)
        thread_information = {'thread_id': thread_id, 'thread_list': thread_list}
        return thread_information

    def pull_all_information(self, thread_table_name, thread_id):
        """
        Query for all information about the thread
        """
        movie_information = self.query(thread_table_name, Key('threadId').eq(thread_id))[0]
        return movie_information