from miniskills.constants.action_names import ActionNames
from miniskills.constants.responses import Responses
from miniskills.constants.speech_action_names import SpeechActionNames
from miniskills.constants.user_response_information_names import UserResponseInformationNames
from miniskills.handler_dispatch_generic_response_generator.handler import Handler
import random
import re

class NoDynamoDbPullHandler(Handler):
    """
    Basic template to handle no dynamodb pull instances
    """
    def can_handle(self, handler_selection_features):
        return handler_selection_features.action_name == ActionNames.NO_DYNAMO_DB_PULL and handler_selection_features.nlg_content is None

    def handle(self, handler_dispatcher, handler_selection_features):
        self.dynamo_db_pull(handler_selection_features)
        return handler_dispatcher.dispatch(handler_dispatcher, handler_selection_features)
    
    def dynamo_db_pull(self, handler_selection_features):
        self._copy_previous_state_new_attributes_to_current_state(handler_selection_features.state_manager)
        handler_selection_features.state_manager.current_state.require_confirmation = True
        handler_selection_features.nlg_content = self.EMPTY