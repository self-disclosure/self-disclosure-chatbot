from miniskills.constants.action_names import ActionNames
from miniskills.constants.intent_names import IntentNames
from miniskills.handler_dispatch_generic_response_generator.handler import Handler

class SwitchMiniskillHandler(Handler):
    """
    Basic template to handle switching miniskill dynamodb pulls (none)
    """
    def can_handle(self, handler_selection_features):
        return handler_selection_features.action_name == ActionNames.SWITCH_MINISKILL and handler_selection_features.nlg_content is None

    def handle(self, handler_dispatcher, handler_selection_features):
        handler_selection_features.nlg_content = self.EMPTY
        return handler_dispatcher.dispatch(handler_dispatcher, handler_selection_features)