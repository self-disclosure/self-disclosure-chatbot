from miniskills.constants.action_names import ActionNames
from miniskills.constants.intent_names import IntentNames
from miniskills.constants.plug_in_config import PlugInConfig
from miniskills.constants.speech_action_names import SpeechActionNames
from miniskills.constants.user_response_information_names import UserResponseInformationNames
from miniskills.base_handlers.pull_thread_for_topic_handler import PullThreadForTopicHandler
from miniskills.handler_dispatch_generic_response_generator.handler import Handler
from miniskills.miniskill_dynamo_db_manager import MiniskillDynamoDbManager
import random
import re

class PullAnotherThreadHandler(Handler):
    """
    Basic template to handle pulling another thread for the same topic dynamodb pulls
    """
    def can_handle(self, handler_selection_features):
        """
        Check if this handler can handle the request based on input of HandlerSelectionFeatures and output a boolean
        """
        return handler_selection_features.action_name == ActionNames.PULL_ANOTHER_THREAD and handler_selection_features.nlg_content is None

    def handle(self, handler_dispatcher, handler_selection_features):
        self.dynamo_db_pull(handler_selection_features)
        return handler_dispatcher.dispatch(handler_dispatcher, handler_selection_features)

    def _update_handler_selection_features(self, handler_selection_features, topic):
        handler_selection_features.state_manager.current_state.require_confirmation = False
        handler_selection_features.old_topic = topic
        handler_selection_features.new_topic = topic

    def dynamo_db_pull(self, handler_selection_features):
        self._copy_previous_state_new_attributes_to_current_state(handler_selection_features.state_manager)
        miniskill_dynamo_db_manager = MiniskillDynamoDbManager()

        # TOPIC DIVERSITY FOR "TELL ME MORE"
        if PlugInConfig.continue_topic_diversity_plug_in:
            handler_selection_features.state_manager.current_state.intent = IntentNames.continue_intent
            pull_thread_for_topic_handler = PullThreadForTopicHandler().dynamo_db_pull(handler_selection_features)
            thread_data = handler_selection_features.nlg_content['thread_data']
        else:
            if handler_selection_features.state_manager.current_state.user_response_information.user_decision == UserResponseInformationNames.ACCEPT:
                # if a new topic was already selected, pull a thread for it
                if handler_selection_features.state_manager.current_state.new_topic is not None:
                    handler_selection_features.action_name = ActionNames.PULL_THREAD_FOR_TOPIC
                    return None

            # pull new thread from current_topic_threads
            thread_list = handler_selection_features.state_manager.current_state.current_topic_threads
            if handler_selection_features.state_manager.last_state.get('corrected_ask_intent') != handler_selection_features.state_manager.current_state.corrected_ask_intent:
                handler_selection_features.state_manager.current_state.intent = IntentNames.continue_intent
                handler_selection_features.speech_action_name = SpeechActionNames.INFORM_TOPIC
                handler_selection_features.action_name = ActionNames.PULL_THREAD_FOR_TOPIC
                return None
            if thread_list is None or len(thread_list) == 0:
                handler_selection_features.speech_action_name = SpeechActionNames.TOPIC_EXHAUSTED
                handler_selection_features.action_name = ActionNames.SWITCH_TOPIC
                return None
            thread_id = random.choice(thread_list)
            thread_list.remove(thread_id)

            # check if thread is in the table
            if not miniskill_dynamo_db_manager.check_thread_exists_in_given_table(handler_selection_features.thread_table_name, thread_id):
                handler_selection_features.action_name = ActionNames.PULL_THREAD_FOR_TOPIC
                handler_selection_features.speech_action_name = SpeechActionNames.INFORM_TOPIC
                if handler_selection_features.state_manager.current_state.corrected_topic is None:
                    handler_selection_features.state_manager.current_state.corrected_topic = handler_selection_features.state_manager.last_state.get('new_topic')
                else:
                    # allows for the responses after switching miniskill to flow more smoothly
                    handler_selection_features.state_manager.current_state.intent = IntentNames.continue_intent
                return None

            # TOPIC DIVERSITY FOR POSITIVE USER INFORMATION
            if PlugInConfig.positive_user_information_topic_diversity_plug_in:
                if not miniskill_dynamo_db_manager.check_thread_exists_in_given_table(handler_selection_features.thread_table_name, thread_id):
                    handler_selection_features.state_manager.current_state.intent = IntentNames.continue_intent
                    handler_selection_features.action_name = ActionNames.PULL_THREAD_FOR_TOPIC
                    return None

            # if the thread_list is empty, we need to switch a topic
            if len(thread_list) == 0:
                handler_selection_features.speech_action_name = SpeechActionNames.TOPIC_EXHAUSTED
                handler_selection_features.action_name = ActionNames.SWITCH_TOPIC
                return None

            # makes sure the thread hasn't been already visited
            while thread_id in handler_selection_features.state_manager.current_state.visited_threads:
                # if the topic is exhausted and no other miniskills can handle it, switch to a new topic
                if len(thread_list) == 0:
                    handler_selection_features.speech_action_name = SpeechActionNames.TOPIC_EXHAUSTED
                    handler_selection_features.action_name = ActionNames.SWITCH_TOPIC
                    return None
                thread_id = random.choice(thread_list)
                thread_list.remove(thread_id)
            handler_selection_features.state_manager.current_state.visited_threads.append(thread_id)
            handler_selection_features.state_manager.current_state.topics_within_a_thread = miniskill_dynamo_db_manager.pull_topics_within_a_thread(handler_selection_features.thread_table_name, thread_id)

            # pull thread data from thread id
            thread_data = miniskill_dynamo_db_manager.pull_thread_title(handler_selection_features.thread_table_name, thread_id)
        
        self._update_handler_selection_features(handler_selection_features, handler_selection_features.state_manager.last_state.get('old_topic'))
        handler_selection_features.nlg_content = thread_data