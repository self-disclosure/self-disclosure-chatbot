from miniskills.constants.action_names import ActionNames
from miniskills.handler_dispatch_generic_response_generator.handler import Handler
from miniskills.miniskill_dynamo_db_manager import MiniskillDynamoDbManager
import re

class SwitchTopicHandler(Handler):
    """
    Basic template to handle switching topic dynamodb pulls
    """
    def can_handle(self, handler_selection_features):
        return handler_selection_features.action_name == ActionNames.SWITCH_TOPIC and handler_selection_features.nlg_content is None
    
    def handle(self, handler_dispatcher, handler_selection_features):
        self.dynamo_db_pull(handler_selection_features)
        return handler_dispatcher.dispatch(handler_dispatcher, handler_selection_features)

    def dynamo_db_pull(self, handler_selection_features):
        self._copy_previous_state_new_attributes_to_current_state(handler_selection_features.state_manager)
        handler_selection_features.state_manager.current_state.require_confirmation = True
        miniskill_dynamo_db_manager = MiniskillDynamoDbManager()
        thread_information = miniskill_dynamo_db_manager.pull_threads_for_topic(handler_selection_features.topic_table_name, ':frontpage')
        thread_id = thread_information['thread_id']
        self._pull_thread_topic(handler_selection_features, thread_id)

    def _pull_thread_topic(self, handler_selection_features, thread_id):
        miniskill_dynamo_db_manager = MiniskillDynamoDbManager()
        thread_primary_topic = miniskill_dynamo_db_manager.pull_thread_primary_topic(handler_selection_features.thread_table_name, thread_id)
        handler_selection_features.state_manager.current_state.new_topic = thread_primary_topic
        handler_selection_features.nlg_content = thread_primary_topic