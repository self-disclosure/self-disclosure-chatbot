from miniskills.constants.action_names import ActionNames
from miniskills.constants.intent_names import IntentNames
from miniskills.constants.intent_to_miniskill_map import IntentToMiniskillMap
from miniskills.constants.plug_in_config import PlugInConfig
from miniskills.constants.speech_action_names import SpeechActionNames
from miniskills.handler_dispatch_generic_response_generator.handler import Handler
from miniskills.miniskill_dynamo_db_manager import MiniskillDynamoDbManager
import random

class PullThreadForTopicHandler(Handler):
    """
    Basic template to handle pulling threads for the same topic
    """
    def can_handle(self, handler_selection_features):
        return handler_selection_features.action_name == ActionNames.PULL_THREAD_FOR_TOPIC and handler_selection_features.nlg_content is None

    def handle(self, handler_dispatcher, handler_selection_features):
        self.dynamo_db_pull(handler_selection_features)
        return handler_dispatcher.dispatch(handler_dispatcher, handler_selection_features)

    def _pull_data(self, handler_selection_features, thread_id, topic):
        miniskill_dynamo_db_manager = MiniskillDynamoDbManager()
        thread_data = miniskill_dynamo_db_manager.pull_thread_title(handler_selection_features.thread_table_name, thread_id)
        data = {
            'thread_data': thread_data,
            'topic': topic,
            'thread_id': thread_id
        }
        return data

    def _set_require_confirmation(self, handler_selection_features):
        handler_selection_features.state_manager.current_state.require_confirmation = False

    def _set_topics_within_a_thread(self, handler_selection_features, thread_id):
        miniskill_dynamo_db_manager = MiniskillDynamoDbManager()
        handler_selection_features.state_manager.current_state.topics_within_a_thread = miniskill_dynamo_db_manager.pull_topics_within_a_thread(handler_selection_features.thread_table_name, thread_id)

    def dynamo_db_pull(self, handler_selection_features):
        self._copy_previous_state_new_attributes_to_current_state(handler_selection_features.state_manager)
        miniskill_dynamo_db_manager = MiniskillDynamoDbManager()

        # TOPIC DIVERSITY for positive user information responses
        if PlugInConfig.positive_user_information_topic_diversity_plug_in:
            if handler_selection_features.state_manager.current_state.intent == IntentNames.continue_intent:
                if handler_selection_features.action_name != ActionNames.PULL_ANOTHER_THREAD:
                    handler_selection_features.speech_action_name = SpeechActionNames.INFORM_TOPIC
                else:
                    handler_selection_features.speech_action_name = SpeechActionNames.TOPIC_DOESNT_EXIST

        # NLU parses first letter of proper nouns to capital letter but database is all lowercase
        if handler_selection_features.state_manager.current_state.corrected_topic is not None:
            topic = handler_selection_features.state_manager.current_state.corrected_topic.lower()
        elif handler_selection_features.state_manager.current_state.new_topic is not None:
            topic = handler_selection_features.state_manager.current_state.new_topic.lower()
        elif handler_selection_features.state_manager.current_state.old_topic:
            topic = handler_selection_features.state_manager.current_state.old_topic.lower()
        else:
            topic = ':frontpage'

        # check if topic has threads
        if miniskill_dynamo_db_manager.check_topic_exists(handler_selection_features.topic_table_name, topic, handler_selection_features.state_manager.last_state.get('visited_threads')) is False:
            handler_selection_features.speech_action_name = SpeechActionNames.TOPIC_EXHAUSTED
            handler_selection_features.action_name = ActionNames.SWITCH_TOPIC
            return None

        # pull thread id and list of threads for topic
        thread_information = miniskill_dynamo_db_manager.pull_threads_for_topic(handler_selection_features.topic_table_name, topic)
        thread_id = thread_information['thread_id']

        # pull thread list with primaryTopic of thread if no topic given
        if handler_selection_features.speech_action_name == SpeechActionNames.TOPIC_DOESNT_EXIST:
            if PlugInConfig.continue_topic_diversity_plug_in or PlugInConfig.positive_user_information_topic_diversity_plug_in:
                thread_primary_topic = random.choice(handler_selection_features.state_manager.current_state.topics_within_a_thread)
            else:
                thread_primary_topic = miniskill_dynamo_db_manager.pull_thread_primary_topic(handler_selection_features.thread_table_name, thread_id)
            if thread_primary_topic:
                thread_information = miniskill_dynamo_db_manager.pull_threads_for_topic(handler_selection_features.topic_table_name, thread_primary_topic)
                topic = thread_primary_topic
            else:
                thread_information = None
                topic = None
            handler_selection_features.speech_action_name = SpeechActionNames.INFORM_NO_TOPIC
        thread_list = None
        if thread_information:
            thread_list = thread_information['thread_list']

        # thread_id is always the last element in thread_list
        if thread_list:
            thread_list.pop()

        # update visited threads
        if handler_selection_features.state_manager.current_state.visited_threads is None:
            handler_selection_features.state_manager.current_state.visited_threads = [thread_id]
        else:
            while thread_id in handler_selection_features.state_manager.current_state.visited_threads:
                # if the topic is exhausted and no other miniskills can handle it, switch to a new topic
                if len(thread_list) == 0:
                    handler_selection_features.speech_action_name = SpeechActionNames.TOPIC_EXHAUSTED
                    handler_selection_features.action_name = ActionNames.SWITCH_TOPIC
                    return None
                thread_id = random.choice(thread_list)
                thread_list.remove(thread_id)
            handler_selection_features.state_manager.current_state.visited_threads.append(thread_id)

        handler_selection_features.state_manager.current_state.current_topic_threads = thread_list

        if thread_list is None:
            handler_selection_features.state_manager.current_state.current_topic_threads = None

        handler_selection_features.state_manager.current_state.old_topic = topic
        handler_selection_features.state_manager.current_state.new_topic = topic

        self._set_topics_within_a_thread(handler_selection_features, thread_id)
        self._set_require_confirmation(handler_selection_features)

        handler_selection_features.nlg_content = self._pull_data(handler_selection_features, thread_id, topic)