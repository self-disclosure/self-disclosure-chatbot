from miniskills.constants.action_names import ActionNames
from miniskills.constants.speech_action_names import SpeechActionNames
from miniskills.handler_dispatch_generic_response_generator.handler import Handler
from miniskills.miniskill_dynamo_db_manager import MiniskillDynamoDbManager
import random

class PullDifferentTopicInThreadHandler(Handler):
    """
    Basic template to handle pulling a different topic in the same thread
    """
    EMPTY = 'empty'

    def can_handle(self, handler_selection_features):
        return handler_selection_features.action_name == ActionNames.PULL_DIFFERENT_TOPIC_IN_THREAD and handler_selection_features.nlg_content is None

    def handle(self, handler_dispatcher, handler_selection_features):
        self.dynamo_db_pull(handler_selection_features)
        return handler_dispatcher.dispatch(handler_dispatcher, handler_selection_features)

    def dynamo_db_pull(self, handler_selection_features):
        self._copy_previous_state_new_attributes_to_current_state(handler_selection_features.state_manager)
        miniskill_dynamo_db_manager = MiniskillDynamoDbManager()
        try:
            self._remove_current_topic_from_available_topics(handler_selection_features)
        except ValueError:
            pass
        topic = None
        thread_list = None
        while thread_list is None:
            while not self._topic_exists(handler_selection_features, handler_selection_features.thread_table_name, topic, thread_list):
                if len(handler_selection_features.state_manager.current_state.topics_within_a_thread) == 0:
                    handler_selection_features.speech_action_name = SpeechActionNames.NO_MORE_KNOWN
                    handler_selection_features.action_name = ActionNames.SWITCH_TOPIC
                    return None
                topic = random.choice(handler_selection_features.state_manager.current_state.topics_within_a_thread)
                handler_selection_features.state_manager.current_state.topics_within_a_thread.remove(topic)
                while not self._topic_exists(handler_selection_features, handler_selection_features.topic_table_name, topic, thread_list):
                    if len(handler_selection_features.state_manager.current_state.topics_within_a_thread) == 0:
                        handler_selection_features.speech_action_name = SpeechActionNames.NO_MORE_KNOWN
                        handler_selection_features.action_name = ActionNames.SWITCH_TOPIC
                        return None
                    topic = random.choice(handler_selection_features.state_manager.current_state.topics_within_a_thread)
                    handler_selection_features.state_manager.current_state.topics_within_a_thread.remove(topic)
                thread_list = self._pull_thread_list(handler_selection_features)
                # used when we need to switch a topic
                if thread_list == self.EMPTY:
                    return None
        self._update_handler_selection_features(handler_selection_features, topic)

    def _remove_current_topic_from_available_topics(self, handler_selection_features):
        handler_selection_features.state_manager.current_state.topics_within_a_thread.remove(handler_selection_features.state_manager.last_state.get('corrected_topic'))

    def _topic_exists(self, handler_selection_features, table_name, topic, thread_list):
        miniskill_dynamo_db_manager = MiniskillDynamoDbManager()
        return miniskill_dynamo_db_manager.is_primary_topic(table_name, topic, thread_list)

    def _update_handler_selection_features(self, handler_selection_features, topic):
        handler_selection_features.state_manager.current_state.require_confirmation = True
        handler_selection_features.nlg_content = topic
        handler_selection_features.old_topic = topic
        handler_selection_features.new_topic = topic

    def _pull_thread_list(self, handler_selection_features):
        miniskill_dynamo_db_manager = MiniskillDynamoDbManager()
        thread_list = miniskill_dynamo_db_manager.pull_threads_for_topic(handler_selection_features.topic_table_name, topic)['thread_list']
        intersection = set(handler_selection_features.state_manager.current_state.visited_threads).intersection(thread_list)
        if intersection == thread_list:
            thread_list = None
            if len(handler_selection_features.state_manager.current_state.topics_within_a_thread) == 0:
                handler_selection_features.speech_action_name = SpeechActionNames.NO_MORE_KNOWN
                handler_selection_features.action_name = ActionNames.SWITCH_TOPIC
                return self.EMPTY
            topic = random.choice(state_manager.current_state.topics_within_a_thread)
            state_manager.current_state.topics_within_a_thread.remove(topic)
        else:
            thread_list = [thread for thread in thread_list if thread not in handler_selection_features.state_manager.current_state.visited_threads]
            handler_selection_features.state_manager.current_state.topic_threads = thread_list
        return thread_list