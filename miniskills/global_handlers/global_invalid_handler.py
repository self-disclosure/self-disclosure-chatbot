from miniskills.constants.intent_names import IntentNames
from miniskills.constants.responses import Responses
from miniskills.constants.speech_action_names import SpeechActionNames
from miniskills.handler_dispatch_generic_response_generator.handler import Handler
import random

class GlobalInvalidHandler(Handler):
    """
    Call when no valid handlers are selected in dispatch
    """

    def nlg_template(self, handler_selection_features):
        if handler_selection_features.speech_action_name == SpeechActionNames.CONTINUE:
            response = None
            if handler_selection_features.state_manager.last_state.get('old_topic') is not None:
                response = random.choice(Responses.CONFIRM_RESUME) + handler_selection_features.state_manager.last_state.get('old_topic') + random.choice(Responses.QUESTION_MARK)
            else:
                response = random.choice(Responses.MINISKILL_OPTIONS) + random.choice(Responses.LAUNCH)
            reprompt = response
        elif handler_selection_features.state_manager.last_state.get('intent') == IntentNames.launch_request_intent:
            response = random.choice(Responses.MINISKILL_OPTIONS) + random.choice(Responses.LAUNCH)
            reprompt = response
        else:
            response = random.choice(Responses.DID_NOT_UNDERSTAND) + random.choice(Responses.INSTRUCT_YES)
            reprompt = random.choice(Responses.INSTRUCT_YES)
        response_and_reprompt = {
            'response': response,
            'reprompt': reprompt
        }
        return response_and_reprompt

    def handle(self, handler_selection_features):
        self._copy_previous_state_new_attributes_to_current_state(handler_selection_features.state_manager)
        if handler_selection_features.state_manager.last_state.get('require_confirmation', False) is True:
            handler_selection_features.state_manager.current_state.require_confirmation = False
            handler_selection_features.state_manager.current_state.saved_intent = handler_selection_features.state_manager.last_state.get('saved_intent')
        else:
            handler_selection_features.state_manager.current_state.require_confirmation = True
            handler_selection_features.state_manager.current_state.saved_intent = handler_selection_features.state_manager.last_state.get('corrected_ask_intent')
        return self.nlg_template(handler_selection_features)