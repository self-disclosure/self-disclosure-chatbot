from miniskills.constants.action_names import ActionNames
from miniskills.constants.dynamo_db_table_names import DynamoDbTableNames
from miniskills.constants.intent_names import IntentNames
from miniskills.constants.responses import Responses
from miniskills.custom_nlp.miniskill_chooser import MiniskillChooser
from miniskills.handler_dispatch_generic_response_generator.handler import Handler
from miniskills.miniskill_dynamo_db_manager import MiniskillDynamoDbManager
import random

class GlobalNavigationalHandler(Handler):
    """
    Handler for navigational intent
    """
    FRONTPAGE_TOPIC = ':frontpage'

    def can_handle(self, handler_selection_features):
        if handler_selection_features.action_name == ActionNames.REPEAT or handler_selection_features.action_name == ActionNames.CANCEL:
            return True
        return False

    def _new_miniskill_trigger(self, state_manager, topic_table_name, thread_table_name):
        miniskill_dynamo_db_manager = MiniskillDynamoDbManager()
        thread_information = miniskill_dynamo_db_manager.pull_threads_for_topic(topic_table_name, self.FRONTPAGE_TOPIC)
        thread_primary_topic = miniskill_dynamo_db_manager.pull_thread_primary_topic(thread_table_name, thread_information['thread_id'])
        # carry over last state's information
        state_manager.current_state.new_topic = thread_primary_topic
        state_manager.current_state.require_confirmation = True
        state_manager.current_state.visited_threads = state_manager.last_state.get('visited_threads', None)
        state_manager.current_state.current_topic_threads = state_manager.last_state.get('current_topic_threads', None)
        state_manager.current_state.new_topic = state_manager.last_state.get('new_topic', None)
        state_manager.current_state.old_topic = state_manager.last_state.get('old_topic', None)
        state_manager.current_state.topics_within_a_thread = state_manager.last_state.get('topics_within_a_thread', None)
        state_manager.current_state.require_confirmation = state_manager.last_state.get('require_confirmation', False)
        return thread_primary_topic

    def nlg_template(self, handler_selection_features):
        response = None
        reprompt = None
        if handler_selection_features.action_name == ActionNames.REPEAT:
            response = random.choice(Responses.I_SAID) + handler_selection_features.state_manager.last_state.get('response')
            reprompt = handler_selection_features.state_manager.last_state.get('reprompt', None)
        elif handler_selection_features.action_name == ActionNames.CANCEL:
            response = random.choice(Responses.SORRY) + '. '
            miniskill_chooser = MiniskillChooser()
            miniskill_chooser.choose_one_miniskill(self.FRONTPAGE_TOPIC, handler_selection_features.state_manager.last_state.get('corrected_ask_intent'), handler_selection_features.state_manager.last_state.get('visited_threads'))
            if handler_selection_features.state_manager.current_state.corrected_ask_intent == IntentNames.request_fact_intent:
                topic = self._new_miniskill_trigger(handler_selection_features.state_manager, DynamoDbTableNames.today_i_learned_topics, DynamoDbTableNames.today_i_learned_threads)
                reprompt = random.choice(Responses.DO_YOU_WANT) + random.choice(Responses.HEAR) + random.choice(Responses.INTERESTING_FACT) + topic + '?'
            elif handler_selection_features.state_manager.current_state.corrected_ask_intent == IntentNames.request_opinion_intent:
                topic = self._new_miniskill_trigger(handler_selection_features.state_manager, DynamoDbTableNames.shower_thoughts_topics, DynamoDbTableNames.shower_thoughts_threads)
                reprompt = random.choice(Responses.CONFIRM_INFORM_SHOWER_THOUGHTS) + topic
            handler_selection_features.state_manager.current_state.new_topic = topic
            response += reprompt
            handler_selection_features.state_manager.current_state.require_confirmation = True
        response_and_reprompt = {
            'response': response,
            'reprompt': reprompt
        }
        return response_and_reprompt

    def handle(self, handler_dispatcher, handler_selection_features):
        return self.nlg_template(handler_selection_features)