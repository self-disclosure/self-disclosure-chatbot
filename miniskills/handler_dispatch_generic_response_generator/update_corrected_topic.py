from miniskills.constants.action_names import ActionNames
from miniskills.constants.intent_names import IntentNames
from miniskills.constants.intent_to_miniskill_map import IntentToMiniskillMap
from miniskills.constants.user_response_information_names import UserResponseInformationNames
import json

class UpdateCorrectedTopic(object):
    """
    Update corrected topic
    """

    def execute(self, user_response_information, last_state_saved_intent, current_corrected_ask_intent, last_state_corrected_ask_intent, last_state_intent, last_state_user_response_information, last_state_new_topic, last_state_old_topic, action_name, current_intent, current_corrected_topic):
        corrected_topic = None
        has_previous_miniskill = False
        if action_name == ActionNames.PULL_THREAD_FOR_TOPIC or current_intent == IntentNames.continue_intent:
            if not current_corrected_topic or current_corrected_topic == 'movies':
                if last_state_saved_intent == IntentNames.request_news_intent:
                    corrected_topic = last_state_new_topic
                elif has_previous_miniskill or current_intent == IntentNames.continue_intent:
                    corrected_topic = last_state_old_topic
                elif last_state_corrected_ask_intent != IntentNames.request_news_intent or current_corrected_ask_intent == IntentNames.request_movie_intent and current_corrected_topic == 'movies':
                    corrected_topic = ':frontpage'
        elif user_response_information is not None:
            if user_response_information.user_decision == UserResponseInformationNames.ACCEPT:
                for intent in IntentToMiniskillMap.intent_map.keys():
                    if last_state_saved_intent == intent:
                        has_previous_miniskill = True
                if has_previous_miniskill or current_corrected_ask_intent == IntentNames.request_news_intent or last_state_corrected_ask_intent == IntentNames.invalid_intent or last_state_intent == IntentNames.cancel_intent or json.loads(last_state_user_response_information).get('user_satisfaction') == UserResponseInformationNames.STRONG_NEGATIVE:
                    corrected_topic = last_state_new_topic
            elif user_response_information.user_knowledge == UserResponseInformationNames.KNOWN:
                if current_corrected_ask_intent == IntentNames.request_news_intent:
                    corrected_topic = last_state_old_topic

        return corrected_topic