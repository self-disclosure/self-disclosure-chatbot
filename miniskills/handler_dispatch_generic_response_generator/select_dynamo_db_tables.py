from miniskills.constants.intent_names import IntentNames
from miniskills.constants.intent_to_topic_and_thread_dynamo_db_tables_map import IntentToTopicAndThreadDynamoDbTablesMap
from miniskills.miniskill_dynamo_db_manager import MiniskillDynamoDbManager
import random

class SelectDynamoDbTables(object):
    """
    Select dynamo db tables
    """

    def execute(self, current_corrected_ask_intent, current_corrected_topic, last_state_old_topic, visited_threads):
        """
        Select dynamo db tables based on current_corrected_ask_intent and current_corrected_topic and last_state_old_topic
        """
        selected_tables = []
        tables = IntentToTopicAndThreadDynamoDbTablesMap.intent_map.get(current_corrected_ask_intent)
        if tables:
            if current_corrected_ask_intent == IntentNames.request_news_intent and (current_corrected_topic is not None or last_state_old_topic is not None):
                miniskill_dynamo_db_manager = MiniskillDynamoDbManager()
                possible_tables = []
                for index, table in enumerate(tables):
                    if index % 2 == 0:
                        if miniskill_dynamo_db_manager.check_topic_exists(table, current_corrected_topic, visited_threads) or miniskill_dynamo_db_manager.check_topic_exists(table, last_state_old_topic, visited_threads):
                            possible_tables.append(table)
                if possible_tables:
                    if not selected_tables:
                        selected_tables.append(random.choice(possible_tables))
                        selected_tables.append(tables[tables.index(selected_tables[0]) + 1])
                    else:
                        selected_tables[0] = random.choice(possible_tables)
                        selected_tables[1] = tables[tables.index(selected_tables[0]) + 1]
            else:
                selected_tables.append(tables[0])
                selected_tables.append(tables[1])
        return selected_tables