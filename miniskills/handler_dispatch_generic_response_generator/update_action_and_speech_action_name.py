from miniskills.constants.action_names import ActionNames
from miniskills.constants.intent_names import IntentNames
from miniskills.constants.intent_to_miniskill_map import IntentToMiniskillMap
from miniskills.constants.plug_in_config import PlugInConfig
from miniskills.constants.responses import Responses
from miniskills.constants.speech_action_names import SpeechActionNames
from miniskills.constants.user_and_intent_information_to_action_map import UserAndIntentInformationToActionMap
from miniskills.constants.user_response_information_names import UserResponseInformationNames
import json

class UpdateActionAndSpeechActionName(object):
    """
    Map user response information to action and speech action name for use by HandlerSelectionFeaturesCreator, also do corrections
    """
    
    # You can rewrite this method if you want to override any of the mappings in user_response_information_to_action_map
    def _update_action_and_speech_name_for_user_response_information(self, user_response_information, last_state_require_confirmation, last_state_saved_intent, current_corrected_ask_intent, last_state_corrected_ask_intent, last_state_intent, last_state_user_response_information, last_state_response):
        """
        Update action and speech name for user response information
        """
        updated_action_and_speech_action_names = []
        if user_response_information is not None:
            if user_response_information.user_decision == UserResponseInformationNames.ACCEPT:
                has_previous_miniskill = False
                for intent in IntentToMiniskillMap.intent_map.keys():
                    if last_state_saved_intent == intent:
                        has_previous_miniskill = True
                if has_previous_miniskill or current_corrected_ask_intent == IntentNames.request_news_intent or current_corrected_ask_intent == IntentNames.request_movie_intent or last_state_corrected_ask_intent == IntentNames.invalid_intent or last_state_intent == IntentNames.cancel_intent or json.loads(last_state_user_response_information).get('user_satisfaction') == UserResponseInformationNames.STRONG_NEGATIVE:
                    updated_action_and_speech_action_names.append(ActionNames.PULL_THREAD_FOR_TOPIC)
                    updated_action_and_speech_action_names.append(SpeechActionNames.INFORM_TOPIC)
                else:
                    updated_action_and_speech_action_names.append(ActionNames.PULL_ANOTHER_THREAD)
                    updated_action_and_speech_action_names.append(SpeechActionNames.CONTINUE)
            elif (user_response_information.user_knowledge == UserResponseInformationNames.KNOWN or user_response_information.user_decision == UserResponseInformationNames.REJECT or user_response_information.user_decision == UserResponseInformationNames.UNSURE) and last_state_corrected_ask_intent == IntentNames.request_movie_intent:
                updated_action_and_speech_action_names.append(ActionNames.PULL_THREAD_FOR_TOPIC)
                updated_action_and_speech_action_names.append(SpeechActionNames.INFORM_TOPIC)
            elif user_response_information.user_knowledge == UserResponseInformationNames.KNOWN:
                if current_corrected_ask_intent == IntentNames.request_news_intent:
                    known = False
                    article_known = False
                    for response in Responses.PREVIOUSLY_MENTIONED:
                        response = response.strip()
                        if last_state_response.find(response) >= 0:
                            known = True
                    for response in (Responses.INFORM_NEWS_DATE_CONFIRM + Responses.INFORM_NEWS_DATE_SUFFIX):
                        response = response.strip()
                        if last_state_response.find(response) >= 0:
                            article_known = True
                    if not known and not article_known:
                        updated_action_and_speech_action_names.append(ActionNames.PULL_THREAD_FOR_TOPIC)
                        updated_action_and_speech_action_names.append(SpeechActionNames.INFORM_TOPIC)
                    elif article_known:
                        updated_action_and_speech_action_names.append(ActionNames.PULL_ANOTHER_THREAD)
                        updated_action_and_speech_action_names.append(SpeechActionNames.CONTINUE)
        elif current_corrected_ask_intent == IntentNames.invalid_intent:
            if last_state_require_confirmation is True:
                updated_action_and_speech_action_names.append(ActionNames.PULL_THREAD_FOR_TOPIC)
                updated_action_and_speech_action_names.append(SpeechActionNames.CONTINUE)

        return updated_action_and_speech_action_names

    # You can rewrite this method if you want to override any of the mappings in intent_to_action_map
    def _update_action_and_speech_name_for_current_intent(self, current_intent, last_state_corrected_ask_intent, last_state_intent, topic_table_name):
        """
        Update action and speech name for current intent
        """
        updated_action_and_speech_action_names = []
        if current_intent in UserAndIntentInformationToActionMap.intent_to_action_map:
            updated_action_and_speech_action_names = UserAndIntentInformationToActionMap.intent_to_action_map[current_intent]
        elif last_state_corrected_ask_intent == IntentNames.invalid_intent and last_state_intent != IntentNames.launch_request_intent and topic_table_name is None:
            updated_action_and_speech_action_names.append(ActionNames.PULL_ANOTHER_THREAD)
            updated_action_and_speech_action_names.append(None)

        return updated_action_and_speech_action_names

    def _update_action_and_speech_action_name_for_specific_cases(self, user_response_information, action_name, current_intent, current_corrected_topic, last_state_saved_intent, last_state_corrected_ask_intent, last_state_response, current_corrected_ask_intent):
        """
        Update action and speech name for pull_thread_for_topic and continue_intent
        """
        updated_action_and_speech_action_names = []
        if action_name == ActionNames.PULL_THREAD_FOR_TOPIC or current_intent == IntentNames.continue_intent:
            topic_exists = False
            if user_response_information is not None and user_response_information.user_decision == UserResponseInformationNames.ACCEPT or current_corrected_topic:
                topic_exists = True
                updated_action_and_speech_action_names.append(action_name)
                updated_action_and_speech_action_names.append(SpeechActionNames.INFORM_TOPIC)
            else:
                for intent in IntentToMiniskillMap.intent_map.keys():
                    if last_state_saved_intent == intent:
                        topic_exists = True
                if last_state_saved_intent == IntentNames.request_news_intent:
                    updated_action_and_speech_action_names.append(action_name)
                    updated_action_and_speech_action_names.append(SpeechActionNames.INFORM_TOPIC)
                    topic_exists = True
                elif last_state_corrected_ask_intent == IntentNames.request_news_intent:
                    for response in (Responses.CONFIRM_INFORM_NEWS + Responses.INFORM_NEWS_DATE_SUFFIX + Responses.INFORM_NEWS_DATE_CONFIRM):
                        response = response.strip()
                        if last_state_response.find(response) >= 0:
                            updated_action_and_speech_action_names.append(ActionNames.PULL_DIFFERENT_TOPIC_IN_THREAD)
                            topic_exists = True
                elif last_state_corrected_ask_intent == IntentNames.request_movie_intent:
                    topic_exists = True
                elif topic_exists or current_intent == IntentNames.continue_intent:
                    updated_action_and_speech_action_names.append(action_name)
                    updated_action_and_speech_action_names.append(SpeechActionNames.INFORM_TOPIC)
                    if not PlugInConfig.continue_topic_diversity_plug_in:
                        topic_exists = True
                else:
                    updated_action_and_speech_action_names.append(action_name)
                    updated_action_and_speech_action_names.append(SpeechActionNames.INFORM_NO_TOPIC)
            if not topic_exists:
                if not updated_action_and_speech_action_names:
                    updated_action_and_speech_action_names.append(action_name)
                    updated_action_and_speech_action_names.append(SpeechActionNames.TOPIC_DOESNT_EXIST)
                else:
                    updated_action_and_speech_action_names[0] = action_name
                    updated_action_and_speech_action_names[1] = SpeechActionNames.TOPIC_DOESNT_EXIST

        for intent in IntentToMiniskillMap.intent_map.keys():
            if current_intent == intent and current_corrected_ask_intent != intent:
                updated_action_and_speech_action_names.append(ActionNames.SWITCH_MINISKILL)
                updated_action_and_speech_action_names.append(None)

        return updated_action_and_speech_action_names

    def execute(self, user_response_information, topic_table_name, last_state_require_confirmation, last_state_saved_intent, current_corrected_ask_intent, last_state_corrected_ask_intent, last_state_intent, last_state_user_response_information, current_intent, last_state_response, current_corrected_topic):
        action_and_speech_action_names = []

        # step 1: initialize action and speech action names
        # possibility 1: set action name if topic_table_name is defined
        if topic_table_name:
            action_and_speech_action_names.append(ActionNames.PULL_THREAD_FOR_TOPIC)
            action_and_speech_action_names.append(None)

        # possibility 2: map the user response information to the default action and speech action names, which could override possibility 1
        if user_response_information is not None and (user_response_information.user_decision is not None or user_response_information.user_knowledge is not None or user_response_information.user_satisfaction is not None or user_response_information.user_side is not None):
            if user_response_information.user_decision in UserAndIntentInformationToActionMap.user_response_information_to_action_map:
                action_and_speech_action_names = UserAndIntentInformationToActionMap.user_response_information_to_action_map[user_response_information.user_decision]
            elif user_response_information.user_knowledge in UserAndIntentInformationToActionMap.user_response_information_to_action_map:
                action_and_speech_action_names = UserAndIntentInformationToActionMap.user_response_information_to_action_map[user_response_information.user_knowledge]
            elif user_response_information.user_satisfaction in UserAndIntentInformationToActionMap.user_response_information_to_action_map:
                action_and_speech_action_names = UserAndIntentInformationToActionMap.user_response_information_to_action_map[user_response_information.user_satisfaction]
            elif user_response_information.user_side in UserAndIntentInformationToActionMap.user_response_information_to_action_map:
                action_and_speech_action_names = UserAndIntentInformationToActionMap.user_response_information_to_action_map[user_response_information.user_side]

            # possibility 2 sub-condition: make any corrections to the mapping based on current state and last state, ACCEPT and KNOWN
            updated_action_and_speech_names = self._update_action_and_speech_name_for_user_response_information(user_response_information, last_state_require_confirmation, last_state_saved_intent, current_corrected_ask_intent, last_state_corrected_ask_intent, last_state_intent, last_state_user_response_information, last_state_response)
            if updated_action_and_speech_names:
                action_and_speech_action_names = updated_action_and_speech_names
        # possibility 3: map the current state's intent to an action name, which could override possibility 1
        else:
            updated_action_and_speech_names = self._update_action_and_speech_name_for_current_intent(current_intent, last_state_corrected_ask_intent, last_state_intent, topic_table_name)
            if updated_action_and_speech_names:
                action_and_speech_action_names = updated_action_and_speech_names

        # step 2: action and speech action correction for action name pull_thread_for_topic or intent continue_intent
        topic_table_name = None
        if len(action_and_speech_action_names) != 0:
            topic_table_name = action_and_speech_action_names[0]
        updated_action_and_speech_names = self._update_action_and_speech_action_name_for_specific_cases(user_response_information, topic_table_name, current_intent, current_corrected_topic, last_state_saved_intent, last_state_corrected_ask_intent, last_state_response, current_corrected_ask_intent)
        if len(updated_action_and_speech_names) > 0:
            action_and_speech_action_names = updated_action_and_speech_names

        # For any final overrides on action and speech action name, one can add the override statement here.
        # current_corrected_ask_intent is the current intent that user utterance got mapped to after running through our intent classifier.
        if current_corrected_ask_intent == IntentNames.ask_question_intent:
            action_name = ActionNames.NO_DYNAMO_DB_PULL
            speech_action_name = SpeechActionNames.CONTINUE
            action_and_speech_action_names.append(action_name)
            action_and_speech_action_names.append(speech_action_name)

        return action_and_speech_action_names