from miniskills.constants.action_names import ActionNames
import random

class Handler(object):
    """
    Dispatch to another handler or dispatch to Nlg, determine whether or not the handler can be used with can_handle
    """
    EMPTY = 'empty'

    def execute(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        pass
        
    def can_handle(self, handler_selection_features):
        """
        Check if this handler can handle the request based on input of HandlerSelectionFeatures and output a boolean
        """
        return False

    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Dispatch or output response based on input of HandlerSelectionFeatures
        """
        pass

    # used to get text to parse for type of user satisfaction in pull_another_thread, switch_topic, and no_dynamo_db_pull handler
    def _get_text_from_navigation(self, state_manager):
        """
        Get text from navigation slot
        """
        if state_manager.current_state.slots is not None:
            if state_manager.current_state.slots.get('navigation'):
                return state_manager.current_state.slots['navigation']['value']
            else:
                return state_manager.current_state.text
        return None
    
    def nlg_template(self):
        """
        NLG template for each handler
        """
        pass

    def dynamo_db_pull(self, handler_selection_features):
        """
        Pull information from dynamodb for handler
        """
        pass

    def _copy_previous_state_new_attributes_to_current_state(self, state_manager):
        """
        Copy previous state's new attributes into the current state so they can be accessed in future states
        """
        state_manager.current_state.visited_threads = state_manager.last_state.get('visited_threads', None)
        state_manager.current_state.current_topic_threads = state_manager.last_state.get('current_topic_threads', None)
        state_manager.current_state.new_topic = state_manager.last_state.get('new_topic', None)
        state_manager.current_state.old_topic = state_manager.last_state.get('old_topic', None)
        state_manager.current_state.topics_within_a_thread = state_manager.last_state.get('topics_within_a_thread', None)
        state_manager.current_state.require_confirmation = state_manager.last_state.get('require_confirmation', False)