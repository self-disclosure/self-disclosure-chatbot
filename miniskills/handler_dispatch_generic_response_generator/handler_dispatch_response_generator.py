from cobot_core.service_module import LocalServiceModule
from cobot_core.state_manager import StateManager
from miniskills.handler_dispatch_generic_response_generator.handler_dispatcher import HandlerDispatcher
from miniskills.handler_dispatch_generic_response_generator.handler_selection_features_creator import HandlerSelectionFeaturesCreator

class HandlerDispatchResponseGenerator(LocalServiceModule):
    """
    Initialize list of possible handlers, run dispatch, return a response
    """

    list_of_handlers = []

    def __init__(self, state_manager, module_name, service_module_config, handler_dispatcher = None, list_of_handlers = None):
        super(HandlerDispatchResponseGenerator, self).__init__(state_manager, module_name, service_module_config)
        self.handler_dispatcher = HandlerDispatcher(list_of_handlers)
        # keep track of threads already visited
        self.state_manager.current_state.set_new_field('visited_threads', None)
        # keep track of possible threads for current topic
        self.state_manager.current_state.set_new_field('current_topic_threads', None)
        # keeps track of new topic before confirmation
        self.state_manager.current_state.set_new_field('new_topic', None)
        # keeps track of old topic before confirmation or topic exhaustion
        self.state_manager.current_state.set_new_field('old_topic', None)
        # keeps track of possible topics within a thread
        self.state_manager.current_state.set_new_field('topics_within_a_thread', None)
        # turn number for movies
        self.state_manager.current_state.set_new_field('turn_number', None)
        # set next turn number for movies
        self.state_manager.current_state.set_new_field('next_turn_number', None)

    def create_handler_selection_features(self):
        """
        Create and return HandlerSelectionFeatures object
        """
        handler_selection_features_creator = HandlerSelectionFeaturesCreator()
        return handler_selection_features_creator.create(self.state_manager)

    def execute(self):
        """
        Update current_state's fields and return response dictionary
        """
        handler_selection_features = self.create_handler_selection_features()
        response_and_reprompt = self.handler_dispatcher.dispatch(self.handler_dispatcher, handler_selection_features)
        self.state_manager.current_state.reprompt = response_and_reprompt['reprompt']
        response_dict = {
            'response': response_and_reprompt['response']
        }
        return response_dict