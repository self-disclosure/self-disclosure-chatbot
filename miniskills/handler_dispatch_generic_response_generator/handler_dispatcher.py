from miniskills.global_handlers.global_invalid_handler import GlobalInvalidHandler
from miniskills.handler_dispatch_generic_response_generator.handler import Handler
from typing import List

class HandlerDispatcher(object):
    """
    Dispatch to handlers based on handler_selection_features, has mechanism to stop infinite loops
    """

    handlers = []

    def __init__(self, handlers):
        self.handlers = handlers

    def dispatch(self, handler_dispatcher, handler_selection_features):
        """
        Dispatch to a handler based on input HandlerSelectionFeatures object and return response str
        """
        for handler in self.handlers:
            if handler.can_handle(handler_selection_features):
                return handler.handle(handler_dispatcher, handler_selection_features)
        handler = GlobalInvalidHandler()
        return handler.handle(handler_selection_features)