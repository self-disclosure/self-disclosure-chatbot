from typing import *
from cobot_core.state_manager import StateManager

class HandlerSelectionFeatures(object):
    """
    Object with four attributes, used to determiner handler usage
    """
    
    def __init__(self, action_name: str = None, state_manager: StateManager = None, topic_table_name: str = None, thread_table_name: str = None, speech_action_name: str = None, nlg_content: str = None):
        # action determines which handler is run
        self.action_name = action_name

        # StateManager
        self.state_manager = state_manager

        # topic table is the table with topics corresponding to thread_ids in dynamodb
        self.topic_table_name = topic_table_name

        # thread table is the table with thread_ids corresponding to the information about the threads
        self.thread_table_name = thread_table_name

        # determines which nlg response is run
        self.speech_action_name = speech_action_name

        # can either be the content in the thread or the topic of the thread
        self.nlg_content = nlg_content