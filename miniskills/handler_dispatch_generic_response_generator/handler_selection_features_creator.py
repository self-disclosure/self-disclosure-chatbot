from miniskills.handler_dispatch_generic_response_generator.update_action_and_speech_action_name import UpdateActionAndSpeechActionName
from miniskills.handler_dispatch_generic_response_generator.select_dynamo_db_tables import SelectDynamoDbTables
from miniskills.handler_dispatch_generic_response_generator.update_corrected_topic import UpdateCorrectedTopic
from miniskills.handler_dispatch_generic_response_generator.handler_selection_features import HandlerSelectionFeatures

class HandlerSelectionFeaturesCreator(object):
    """
    Create an instance of HandlerSelectionFeatures with default values
    """
    def _select_dynamo_db_tables(self, handler_selection_features):
        """
        Select dynamo db tables
        """
        current_corrected_ask_intent = handler_selection_features.state_manager.current_state.corrected_ask_intent
        current_corrected_topic = handler_selection_features.state_manager.current_state.corrected_topic
        last_state_old_topic = handler_selection_features.state_manager.last_state.get('old_topic')
        visited_threads = handler_selection_features.state_manager.last_state.get('visited_threads')
        selected_tables = SelectDynamoDbTables().execute(current_corrected_ask_intent, current_corrected_topic, last_state_old_topic, visited_threads)
        if selected_tables:
            handler_selection_features.topic_table_name = selected_tables[0]
            handler_selection_features.thread_table_name = selected_tables[1]

    def _update_action_name_and_speech_action_name(self, handler_selection_features):
        """
        Update action and speech action name
        """
        user_response_information = handler_selection_features.state_manager.current_state.user_response_information
        topic_table_name = handler_selection_features.topic_table_name
        last_state_require_confirmation = handler_selection_features.state_manager.last_state.get('require_confirmation', False)
        last_state_saved_intent = handler_selection_features.state_manager.last_state.get('saved_intent')
        current_corrected_ask_intent = handler_selection_features.state_manager.current_state.corrected_ask_intent
        last_state_corrected_ask_intent = handler_selection_features.state_manager.last_state.get('corrected_ask_intent')
        last_state_intent = handler_selection_features.state_manager.last_state.get('intent')
        last_state_user_response_information = handler_selection_features.state_manager.last_state.get('user_response_information')
        current_intent = handler_selection_features.state_manager.current_state.intent
        last_state_response = handler_selection_features.state_manager.last_state.get('response')
        current_corrected_topic = handler_selection_features.state_manager.current_state.corrected_topic
        action_and_speech_action_names = UpdateActionAndSpeechActionName().execute(user_response_information, topic_table_name, last_state_require_confirmation, last_state_saved_intent, current_corrected_ask_intent, last_state_corrected_ask_intent, last_state_intent, last_state_user_response_information, current_intent, last_state_response, current_corrected_topic)
        if action_and_speech_action_names:
            handler_selection_features.action_name = action_and_speech_action_names[0]
            if len(action_and_speech_action_names) == 2:
                handler_selection_features.speech_action_name = action_and_speech_action_names[1]

    def _update_corrected_topic(self, state_manager, action_name):
        """
        Update corrected topic
        """
        user_response_information = state_manager.current_state.user_response_information
        last_state_saved_intent = state_manager.last_state.get('saved_intent')
        current_corrected_ask_intent = state_manager.current_state.corrected_ask_intent
        last_state_corrected_ask_intent = state_manager.last_state.get('corrected_ask_intent')
        last_state_intent = state_manager.last_state.get('intent')
        last_state_user_response_information = state_manager.last_state.get('user_response_information')
        last_state_new_topic = state_manager.last_state.get('new_topic')
        last_state_old_topic = state_manager.last_state.get('old_topic')
        current_intent = state_manager.current_state.intent
        current_corrected_topic = state_manager.current_state.corrected_topic
        corrected_topic = UpdateCorrectedTopic().execute(user_response_information, last_state_saved_intent, current_corrected_ask_intent, last_state_corrected_ask_intent, last_state_intent, last_state_user_response_information, last_state_new_topic, last_state_old_topic, action_name, current_intent, current_corrected_topic)
        if corrected_topic:
            state_manager.current_state.corrected_topic = corrected_topic

    def create(self, state_manager):
        """
        Create and return HandlerSelectionFeatures object with StateManager object input
        """
        handler_selection_features = HandlerSelectionFeatures(None, state_manager, None, None, None)

        # step 1: pull thread for topic handler, set topic and thread tables
        self._select_dynamo_db_tables(handler_selection_features)
        
        # step 2: update action and speech action names
        self._update_action_name_and_speech_action_name(handler_selection_features)

        # step 3: update corrected topic
        self._update_corrected_topic(state_manager, handler_selection_features.action_name)

        return handler_selection_features