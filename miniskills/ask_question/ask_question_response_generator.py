from miniskills.handler_dispatch_generic_response_generator.handler_dispatch_response_generator import HandlerDispatchResponseGenerator
from miniskills.ask_question.ask_question_nlg_handlers import AskQuestionNoDynamoDbPullNlgHandler
from miniskills.ask_question.ask_question_no_dynamo_db_pull_handler import  AskQuestionNoDynamoDbPullHandler

class AskQuestionResponseGenerator(HandlerDispatchResponseGenerator):
    """
    Initialize list of possible handlers, run dispatch, return just the EVI Question Answer
    """

    list_of_handlers = [AskQuestionNoDynamoDbPullHandler(), AskQuestionNoDynamoDbPullNlgHandler()]

    def __init__(self, state_manager, module_name, service_module_config, handler_dispatcher = None, list_of_handlers = None):
        super(AskQuestionResponseGenerator, self).__init__(state_manager, module_name, service_module_config, handler_dispatcher, self.list_of_handlers)