from miniskills.base_handlers.no_dynamo_db_pull_handler import NoDynamoDbPullHandler
from miniskills.constants.responses import Responses
import random

class AskQuestionNoDynamoDbPullHandler(NoDynamoDbPullHandler):
    """
    Basic template to handle no dynamodb pull instances
    """

    def handle(self, handler_dispatcher, handler_selection_features):
        self.dynamo_db_pull(handler_selection_features)
        return handler_dispatcher.dispatch(handler_dispatcher, handler_selection_features)


    def dynamo_db_pull(self, handler_selection_features):
        self._copy_previous_state_new_attributes_to_current_state(handler_selection_features.state_manager)
        handler_selection_features.state_manager.current_state.require_confirmation = False
        if handler_selection_features.state_manager.current_state.features['evi']['asr_text']:
            handler_selection_features.nlg_content =  handler_selection_features.state_manager.current_state.features['evi']['asr_text']
        elif handler_selection_features.state_manager.current_state.features['evi']['current_topic_from_slots']:
            handler_selection_features.nlg_content = handler_selection_features.state_manager.current_state.features['evi']['current_topic_from_slots']
        else:
            handler_selection_features.nlg_content = random.choice(Responses.NO_EVI_RESPONSE_APOLOGY)