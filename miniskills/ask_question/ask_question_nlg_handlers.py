from miniskills.base_nlg_handlers.no_dynamo_db_pull_nlg_handler import NoDynamoDbPullNlgHandler
from miniskills.constants.responses import Responses

class AskQuestionNoDynamoDbPullNlgHandler(NoDynamoDbPullNlgHandler):
    """
    Handle no dynamodb pulls for EVIQuestion Answer
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        return self.nlg_template(handler_selection_features)

    def nlg_template(self, handler_selection_features):
        reprompt = Responses.EVI_REPROMPT
        response_and_reprompt = {
            'response': handler_selection_features.nlg_content,
            'reprompt': reprompt
        }
        return response_and_reprompt