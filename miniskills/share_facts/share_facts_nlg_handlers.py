from miniskills.constants.action_names import ActionNames
from miniskills.constants.responses import Responses
from miniskills.constants.share_facts_combined_responses import ShareFactsCombinedResponses
from miniskills.base_nlg_handlers.no_dynamo_db_pull_nlg_handler import NoDynamoDbPullNlgHandler
from miniskills.base_nlg_handlers.pull_another_thread_nlg_handler import PullAnotherThreadNlgHandler
from miniskills.base_nlg_handlers.pull_different_topic_in_thread_nlg_handler import PullDifferentTopicInThreadNlgHandler
from miniskills.base_nlg_handlers.pull_thread_for_topic_nlg_handler import PullThreadForTopicNlgHandler
from miniskills.base_nlg_handlers.switch_miniskill_nlg_handler import SwitchMiniskillNlgHandler
from miniskills.base_nlg_handlers.switch_topic_nlg_handler import SwitchTopicNlgHandler
import random

class ShareFactsNoDynamoDbPullNlgHandler(NoDynamoDbPullNlgHandler):
    """
    Handle no dynamodb pulls for ShareFacts
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        confirm_resume_topic = random.choice(Responses.CONTINUE) + random.choice(Responses.CONVERSATION) + random.choice(Responses.ON) + handler_selection_features.state_manager.current_state.old_topic + '?'
        return self.nlg_template(handler_selection_features, confirm_resume_topic)

class ShareFactsPullAnotherThreadNlgHandler(PullAnotherThreadNlgHandler):
    """
    Handle pulling another thread for ShareFacts
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        combined_responses = ShareFactsCombinedResponses()
        continue_inform_topic = combined_responses.continue_inform_topic(handler_selection_features.nlg_content)
        inform_followup = random.choice(Responses.INFORM_TIL_TITLE_FOLLOWUP)
        did_you_know_response = combined_responses.did_you_know_selector('', handler_selection_features.nlg_content)
        confirm_resume_topic = random.choice(Responses.CONTINUE) + random.choice(Responses.CONVERSATION) + random.choice(Responses.ON) + handler_selection_features.state_manager.current_state.old_topic + '?'
        return self.nlg_template(handler_selection_features, continue_inform_topic, inform_followup, did_you_know_response, confirm_resume_topic)

class ShareFactsPullDifferentTopicInThreadNlgHandler(PullDifferentTopicInThreadNlgHandler):
    """
    Handle pulling topic in the same thread for ShareFacts
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        return self.nlg_template(handler_selection_features, '')

class ShareFactsPullThreadForTopicNlgHandler(PullThreadForTopicNlgHandler):
    """
    Handle pulling thread for topic for ShareFacts
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        combined_responses = ShareFactsCombinedResponses()
        inform_topic = None
        continue_inform = combined_responses.did_you_know_selector('', handler_selection_features.nlg_content['thread_data'])
        inform_no_topic = combined_responses.inform_no_topic(handler_selection_features.nlg_content['topic'], handler_selection_features.nlg_content['thread_data'])
        if handler_selection_features.nlg_content['topic'] != ':frontpage':
            inform_topic = combined_responses.inform_topic(handler_selection_features.nlg_content['topic'], handler_selection_features.nlg_content['thread_data'])
        else:
            inform_topic = continue_inform
        followup = random.choice(Responses.INFORM_TIL_TITLE_FOLLOWUP)
        return self.nlg_template(handler_selection_features, inform_topic, continue_inform, inform_no_topic, followup)

class ShareFactsSwitchMiniskillNlgHandler(SwitchMiniskillNlgHandler):
    """
    Handle switching miniskill dynamodb pulls for switching to ShareFacts miniskill
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        combined_responses = ShareFactsCombinedResponses()
        topic_exhausted_response = self.last_miniskill_topic_exhausted_response(handler_selection_features)
        topic = None
        if handler_selection_features.state_manager.current_state.corrected_topic != ':frontpage':
            topic = handler_selection_features.state_manager.current_state.corrected_topic
        handler_selection_features.state_manager.current_state.old_topic = topic
        confirm_inform_response = combined_responses.confirm_inform_fact(topic)
        return self.nlg_template(topic_exhausted_response, topic, confirm_inform_response)

class ShareFactsSwitchTopicNlgHandler(SwitchTopicNlgHandler):
    """
    Handle switching topic dynamodb pulls for switching to ShareFacts miniskill
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        combined_responses = ShareFactsCombinedResponses()
        topic_exhausted_response = random.choice(Responses.TOPIC_EXHAUSTED)
        confirm_inform_response = combined_responses.confirm_inform_fact(handler_selection_features.nlg_content)
        return self.nlg_template(handler_selection_features, topic_exhausted_response, confirm_inform_response)