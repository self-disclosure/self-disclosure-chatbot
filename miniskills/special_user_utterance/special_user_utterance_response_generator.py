from miniskills.base_handlers.no_dynamo_db_pull_handler import NoDynamoDbPullHandler
from miniskills.handler_dispatch_generic_response_generator.handler_dispatch_response_generator import HandlerDispatchResponseGenerator
from miniskills.special_user_utterance.special_user_utterance_handler_selection_features_creator import SpecialUserUtteranceHandlerSelectionFeaturesCreator
from miniskills.special_user_utterance.special_user_utterance_no_dynamo_db_pull_nlg_handler import SpecialUserUtteranceNoDynamoDbPullNlgHandler
from miniskills.constants.intent_names import IntentNames

class SpecialUserUtteranceResponseGenerator(HandlerDispatchResponseGenerator):
    """
    Initialize list of possible handlers, run dispatch, return a response for SpecialUserUtterance
    """

    list_of_handlers = [NoDynamoDbPullHandler(), SpecialUserUtteranceNoDynamoDbPullNlgHandler()]

    def __init__(self, state_manager, module_name, service_module_config, handler_dispatcher = None, list_of_handlers = None):
        super(SpecialUserUtteranceResponseGenerator, self).__init__(state_manager, module_name, service_module_config, handler_dispatcher, self.list_of_handlers)
    
    def create_handler_selection_features(self):
        handler_selection_features_creator = SpecialUserUtteranceHandlerSelectionFeaturesCreator()
        self.state_manager.current_state.corrected_ask_intent = self.state_manager.last_state.get('corrected_ask_intent', None)
        return handler_selection_features_creator.create(self.state_manager)