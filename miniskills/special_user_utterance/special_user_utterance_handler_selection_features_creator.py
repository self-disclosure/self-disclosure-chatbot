from miniskills.handler_dispatch_generic_response_generator.handler_selection_features_creator import HandlerSelectionFeaturesCreator
from miniskills.handler_dispatch_generic_response_generator.handler_selection_features import HandlerSelectionFeatures
from miniskills.constants.action_names import ActionNames
from miniskills.constants.intent_names import IntentNames
from miniskills.constants.speech_action_names import SpeechActionNames
import re

class SpecialUserUtteranceHandlerSelectionFeaturesCreator(HandlerSelectionFeaturesCreator):
    """
    Set HandlerSelectionFeatures for SpecialUserUtterance
    """
    def create(self, state_manager):
        speech_action_name = None
        if re.match("|".join([r'^(sorry|I\'m sorry)\b']), 
                              state_manager.current_state.text) is not None:
            speech_action_name = SpeechActionNames.USER_APOLOGY
        elif re.match("|".join([r'^(thank|thanks)\b',
                                r'^(no) (thank|thanks)\b']), 
                                state_manager.current_state.text) is not None:
            speech_action_name = SpeechActionNames.USER_THANKS
        elif re.match("|".join([r'\bthat sounds too good to be true\b',
                                r'^really$']), 
                                state_manager.current_state.text) is not None:
            speech_action_name = SpeechActionNames.USER_SKEPTICAL
        elif re.match("|".join([r'\bwhat\'s that$',
                                r'\bwhat is that$',
                                r'\bwhat does that mean\b',
                                r'\bwhat is that mean\b',
                                r'\bwhat is it called\b',
                                r'\bwhat is the reason for that\b',
                                r'\bwhat is it\b',
                                r'\bwhat was the name of it\b',
                                r'\bwhere is it\b',
                                r'\bwho were they\b',
                                r'\bhow do you know that\b',
                                r'\bhow did they do that\b',
                                r'\bhow is that possible\b',
                                r'\bhow does that work\b',
                                r'\bwhy is that\b',
                                r'\bwhy does that matter\b',
                                r'\bwhat happened then\b',
                                r'\bwhy did they do that\b',
                                r'\bwhy$']), 
                                state_manager.current_state.text) is not None:
            speech_action_name = SpeechActionNames.USER_BACKWARD_LOOKING_DETAIL
        elif re.match("|".join([r'\bI have a headache\b']), 
                                state_manager.current_state.text) is not None:
            speech_action_name = SpeechActionNames.USER_BAD_MOOD
        elif state_manager.current_state.intent == IntentNames.amazon_help_intent:
            speech_action_name = SpeechActionNames.HELP

        action_name = ActionNames.NO_DYNAMO_DB_PULL
        handler_selection_features = HandlerSelectionFeatures(action_name, state_manager, None, None, speech_action_name)

        return handler_selection_features