from miniskills.constants.action_names import ActionNames
from miniskills.constants.responses import Responses
from miniskills.base_nlg_handlers.no_dynamo_db_pull_nlg_handler import NoDynamoDbPullNlgHandler
import random

class SpecialUserUtteranceNoDynamoDbPullNlgHandler(NoDynamoDbPullNlgHandler):
    """
    Handle no dynamodb pulls for SpecialUserUtterance
    """
    def can_handle(self, handler_selection_features):
        return handler_selection_features.action_name == ActionNames.NO_DYNAMO_DB_PULL and handler_selection_features.nlg_content is not None
        
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        confirm_resume_topic = None
        if handler_selection_features.state_manager.current_state.old_topic:
            confirm_resume_topic = random.choice(Responses.CONTINUE) + random.choice(Responses.CONVERSATION) + random.choice(Responses.ON) + handler_selection_features.state_manager.current_state.old_topic + '?'
        else:
            confirm_resume_topic = random.choice(Responses.CONTINUE) + random.choice(Responses.CONVERSATION) + '?'
        return self.nlg_template(handler_selection_features, confirm_resume_topic)