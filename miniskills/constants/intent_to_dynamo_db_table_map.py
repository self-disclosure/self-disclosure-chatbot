from miniskills.constants.intent_names import IntentNames
from miniskills.constants.dynamo_db_table_names import DynamoDbTableNames

class IntentToDynamoDbTableMap(object):
    """
    Map intent to topic dynamodbtable name
    """
    intent_map = {IntentNames.request_fact_intent: [DynamoDbTableNames.today_i_learned_topics], 
                  IntentNames.request_opinion_intent: [DynamoDbTableNames.shower_thoughts_topics], 
                  IntentNames.request_news_intent: [DynamoDbTableNames.news_topics,
                                                        DynamoDbTableNames.futurology_topics,
                                                        DynamoDbTableNames.politics_topics,
                                                        DynamoDbTableNames.science_topics,
                                                        DynamoDbTableNames.sports_topics,
                                                        DynamoDbTableNames.technology_topics,
                                                        DynamoDbTableNames.world_news_topics],
                  IntentNames.request_movie_intent: [DynamoDbTableNames.movie_topics]}