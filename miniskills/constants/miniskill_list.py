from miniskills.constants.miniskills import Miniskills 

class MiniskillList(object):
    """
    Dictionary of intent mapped to RG.
    """

    skill_list_config = [Miniskills.NEWS,  Miniskills.ASKQUESTION]
    # list of informative skills used
    informative_skill_list = []

    # maps intent to the miniskill RG
    intent_map = {'LaunchRequestIntent': 'LAUNCH',
                  'SpecialUserUtteranceResponseIntent': 'SPECIALUSERUTTERANCE',
                  'InvalidIntent': 'INVALID',
                  'GreetingIntent': 'GREETING',
                  'AMAZON.StopIntent': 'GOODBYE'}

    for skill in skill_list_config:
        if skill == Miniskills.SHAREFACTS:
            intent_map['RequestFactIntent'] = 'SHAREFACTS'
            informative_skill_list.append(skill)
        elif skill == Miniskills.SHOWERTHOUGHTS:
            intent_map['RequestOpinionIntent'] = 'SHOWERTHOUGHTS'
            informative_skill_list.append(skill)
        elif skill == Miniskills.NEWS:
            intent_map['RequestNewsIntent'] = 'NEWS'
        elif skill == Miniskills.MOVIES:
            intent_map['RequestMovieIntent'] = 'MOVIES'
        elif skill == Miniskills.ASKQUESTION:
            intent_map['AskQuestionIntent'] = 'ASKQUESTION'