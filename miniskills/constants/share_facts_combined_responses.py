from miniskills.constants.responses import Responses
import random
import re

class ShareFactsCombinedResponses(object):
    """
    Combine commonly used responses for ShareFacts into one callable response function that can be reused
    """

    def confirm_inform_fact(self, topic):
        """
        Create response for confirming informing a fact of input string topic
        """
        return random.choice(Responses.DO_YOU_WANT) + random.choice(Responses.HEAR) + random.choice(Responses.INTERESTING_FACT) + topic

    def inform_topic(self, topic, data):
        """
        Create response for informing with an input string topic and string data about the topic
        """
        response_number = random.randint(0, 1)
        if response_number == 0:
            return self.did_you_know_selector(random.choice(Responses.YOU_WANTED_TO) + random.choice(Responses.TALK_ABOUT) + topic, data)
        else:
            return self.did_you_know_selector(topic + random.choice(Responses.QUESTION_MARK), data)

    def inform_no_topic(self, topic, data):
        """
        Create response for informing with a new input string topic and string data about the topic
        """
        suggest_topic = random.choice(Responses.SUGGEST_TOPIC)
        if re.match(r'\bwe can\b', suggest_topic) is not None:
            return suggest_topic + topic + '. ' + data
        else:
            return suggest_topic + topic + '? ' + data

    def continue_inform_topic(self, data):
        """
        Create response for continuing to inform about the same topic with input string data
        """
        return self.did_you_know_selector(random.choice(Responses.SURE), data)

    def did_you_know_selector(self, response, data):
        """
        Determine punctuation and create response for responses including DID_YOU_KNOW
        """
        did_you_know = random.choice(Responses.DID_YOU_KNOW)
        if did_you_know == ' Did you know that ':
            if response:
                return response + '.' + did_you_know + data + '?'
            else:
                return did_you_know + data + '?'
        else:
            if response:
                return response + '.' + did_you_know + data + '.'
            else:
                return did_you_know + data + '.'