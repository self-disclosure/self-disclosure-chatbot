from miniskills.constants.action_names import ActionNames
from miniskills.constants.intent_names import IntentNames
from miniskills.constants.speech_action_names import SpeechActionNames
from miniskills.constants.user_response_information_names import UserResponseInformationNames

class UserAndIntentInformationToActionMap(object):
    """
    Map user response information to action and speech action; map intent to action and speech action
    """

    user_response_information_to_action_map = {UserResponseInformationNames.ACCEPT: [ActionNames.PULL_ANOTHER_THREAD, SpeechActionNames.CONTINUE],
                                               UserResponseInformationNames.UNSURE: [ActionNames.SWITCH_TOPIC, SpeechActionNames.UNSURE],
                                               UserResponseInformationNames.REJECT: [ActionNames.SWITCH_TOPIC, SpeechActionNames.REJECT],
                                               UserResponseInformationNames.KNOWN: [ActionNames.PULL_DIFFERENT_TOPIC_IN_THREAD, None],
                                               UserResponseInformationNames.NOT_KNOWN: [ActionNames.PULL_ANOTHER_THREAD, SpeechActionNames.NOT_KNOWN],
                                               UserResponseInformationNames.POSITIVE: [ActionNames.PULL_ANOTHER_THREAD, SpeechActionNames.POSITIVE_USER_SATISFACTION],
                                               UserResponseInformationNames.NEGATIVE: [ActionNames.NO_DYNAMO_DB_PULL, SpeechActionNames.NEGATIVE_USER_SATISFACTION],
                                               UserResponseInformationNames.STRONG_NEGATIVE: [ActionNames.SWITCH_TOPIC, SpeechActionNames.STRONG_NEGATIVE_USER_SATISFACTION],
                                               UserResponseInformationNames.AGREE: [ActionNames.PULL_ANOTHER_THREAD, SpeechActionNames.AGREE],
                                               UserResponseInformationNames.UNSURE_SIDE: [ActionNames.PULL_ANOTHER_THREAD, SpeechActionNames.UNSURE_SIDE],
                                               UserResponseInformationNames.DISAGREE: [ActionNames.SWITCH_TOPIC, SpeechActionNames.DISAGREE]
                                              }

    intent_to_action_map = {IntentNames.ignored_and_deflected_intent: [ActionNames.PULL_ANOTHER_THREAD, SpeechActionNames.CONTINUE],
                            IntentNames.amazon_next_intent: [ActionNames.PULL_ANOTHER_THREAD, SpeechActionNames.CONTINUE],
                            IntentNames.repeat_intent: [ActionNames.REPEAT, None],
                            IntentNames.cancel_intent: [ActionNames.CANCEL, None]
                           }