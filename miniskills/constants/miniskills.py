class Miniskills(object):
    """
    Names of miniskills.
    """

    SHAREFACTS = 'ShareFacts'
    SHOWERTHOUGHTS = 'ShowerThoughts'
    NEWS = 'News'
    MOVIES = 'Movies'
    ASKQUESTION = 'AskQuestion'