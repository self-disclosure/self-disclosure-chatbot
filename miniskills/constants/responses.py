class Responses(object):
    """
    List of all possible general responses and synonyms
    """
    # greeting
    GREETING = [' How\'re you? ',
                ' How\'re you doing today? ',
                ' How\'re you doing? ',
                ' How\'s it going? ',
                ' How\'re things? ',
                ' How\'re things going? ',
                ' How\'s your day? ',
                ' How\'s your day going? ']

    GOODBYE_PROMPT = [' Please say stop if you wish to end the conversation. ']

    # accepting user navigation
    SURE = [' Sure! ', 
            ' Okay! ', 
            ' Absolutely! ', 
            ' My pleasure! ']
    
    # used for listing topics
    OKAY = [' Okay. ', 
            ' Well. ', 
            ' So. ', 
            ' ']

    ALRIGHT = [' Alright. ',
               ' No problem. ',
               ' Okay. ']

    MINISKILL_OPTIONS = [' I can share facts or opinions! ']

    LAUNCH = [' What would you like to talk about? ']
    
    # used for repeating
    I_SAID = [' I said ']
    
    # used for repeating
    I_ASKED = [' I asked ']

    # user's intent doesn't match any existing intents
    DID_NOT_UNDERSTAND = [' Sorry, I didn\'t understand that. ', 
                          ' What was that? I didn\'t quite understand. ',
                          'I\'m sorry, I couldn\'t understand, I get a little confused sometimes. ',
                          'I heard you, but unfortunately I couldn\'t process what you said. ',
                          'I\'m sorry, I don\'t know how to respond to that right now. ',
                          'Unfortunately I\'m not sure I understood what you said. ']

    # ask the user if they would like to continue talking (more work)
    CONTINUE = [' Would you like to continue our ']

    CONVERSATION = [' discussion ',
                    ' conversation ']

    ON = [' on ']

    # words for talking (more work)
    TALKING = [' chatting ', 
               ' talking ', 
               ' discussing ']

    # sorry
    SORRY = [' Sorry ', 
             ' I\'m sorry ']

    # switch miniskill
    SWITCH_MINISKILL = [' Would you like to try something else ? ',
                        ' Would you like to try something different? ']

    # special user utterances
    USER_APOLOGY = [' don\'t worry. ',
               ' no worries. ',
               ' it\'s fine. ',
               ' that\'s fine. ',
               ' no problem. ',
               ' it\'s okay. ',
               ' that\'s okay. ']
        
    USER_THANKS = [' You are welcome. ',
                   ' You are very welcome. ',
                   ' My pleasure. ']

    USER_SKEPTICAL = [' I\'m pretty gullible and can\'t always tell what\'s true! '
                      ' That\'s just what I heard! ',
                      ' I could be mistaken! ']

    USER_BACKWARD_LOOKING_DETAIL = [' Sorry. I don\'t have the details. ',
                                    ' Sorry. I\'m not able to find the details. ',
                                    ' I\'m sorry. I don\'t have the details. ',
                                    ' I\'m sorry. I\'m not able to find the details. ',
                                    ' That\'s tough to explain. ']

    USER_BAD_MOOD = [' I\'m sorry to hear that. ']

    # instructions
    INSTRUCT_YES = [' You can say "yes", to continue. ']

    INSTRUCT_ALL = [' Say "repeat" to hear what I said, and "stop" to end our conversation. Otherwise I\'m programmed to chat about a bunch of things like news and facts. Or we can also get to know each other if you\'d like. ']
    
    INSTRUCT_CONTINUE = [' To keep going, please say "continue". ',
                         ' To move on, please say "continue". ']

    INSTRUCT_HELP = [' Remember, you can always say "help me", to hear some instructions. ',
                     ' Remember, you can also say "help me", to hear more options. ',
                     ' Don\'t forget, you can always say "help me", to hear some instructions. ',
                     ' Don\'t forget, you can also say "help me", to hear more options. ',
                     ' You can always say "help me" to hear some instructions. ',
                     ' You can also say "help me", to hear more options. ']

    # not familiar or inappropriate topics
    NOT_FAMILIAR_TOPIC = [' I haven\'t learned anything about ',
                          ' I\'m actually not very familiar with ',
                          ' I don\'t know enough to say anything about ',
                          ' I don\'t really have anything to say about ',
                          ' I need to do some homework on ',
                          ' I\'ll need to do some research on ',
                          ' I\'m not sure I know anything about ']

    # share facts
    HEAR = [' hear ', 
            ' know ']

    DO_YOU_WANT = [' do you wanna ',
                   ' would you like to ']

    INTERESTING_FACT = [' something interesting about ', 
                        ' an interesting fact about ']

    CONFIRM_RESUME = [' Do you wanna continue our conversation on ', 
                      ' Do you wanna continue our discussion on ', 
                      ' Would you like to continue our conversation on ', 
                      ' Would you like to continue our discussion on ']

    CONFIRM_RESUME_NO_TOPIC = [' Do you wanna continue our conversation? ']

    INFORM_INFERRED_TOPIC = [' You mentioned ', 
                             ' Speaking of ']

    YOU_WANTED_TO = [' I think you wanted to',
                     ' It looks like you wanted to',
                     ' I think you\'d like to',
                     ' It looks like you wanna',
                     ' It sounds like you wanna',
                     ' Sounds like you wanna']

    TALK_ABOUT = [' talk about ',
                  ' chat about ',
                  ' hear my thoughts on ',
                  ' hear what I have to say about ']

    INFORM_START = [' Absolutely. ',
                    ' Okay! ',
                    ' Sure! ',
                    ' I love it! ',
                    ' Let\'s do it! ',
                    ' Sounds good! ']

    QUESTION_MARK = [' ? ']

    DID_YOU_KNOW = [' My friend told me that ',
                    ' My friend in the cloud told me that ',
                    ' I learned from the web that ',
                    ' Did you know that ']

    TOPIC_EXHAUSTED = [' Hmm. I don\'t actually know more about ',
                       ' Hmm. I don\'t really have more information about ',
                       ' Hmm. I don\'t really know anything else about ',
                       ' Hmm. I think I\'ve actually run out of things to say about ']

    SUGGEST_TOPIC = [' How about we talk about ',
                     ' Would you like to chat about ',
                     ' Do you wanna chat about ',
                     ' We can chat about ',
                     ' We can talk about ']

    INFORM_TIL_TITLE_FOLLOWUP = [' Did I tell you anything you didn\'t know? ',
                                 ' Was there anything surprising in what I told you? ',
                                 ' Do you have anything to say about this? ',
                                 ' Is there anything you wanna say about this? ',
                                 ' Is there anything you wanna share with me as well? ',
                                 ' Would you like to tell me something as well? ',
                                 ' What would you like to say about this? ',
                                 ' What do you think of that? ']

    ADDITIONAL = [' another ',
                  ' one more ']

    FACT = [' fact ']

    ABOUT = [' about ']

    # shower thoughts
    NO_THOUGHTS = [' I can\'t remember any random thoughts about ',
                   ' I can\'t really think of anything about ',
                   ' I don\'t really have any random thoughts right now. ',
                   ' I don\'t really have any random thoughts currently. ',
                   ' I don\'t really have any random thoughts at this moment. ',
                   ' I can\'t really think of anything right now. ',
                   ' I can\'t really think of anything currently. ',
                   ' I can\'t really think of anything at this moment. ']

    SHOWER_THOUGHTS_TOPIC_EXHAUSTED = [' Hmm. I haven\'t really thought more about ',
                                       ' Hmm. I don\'t really have any other random thoughts about ',
                                       ' Hmm. I don\'t really have any more random thoughts about ']

    CONFIRM_INFORM_SHOWER_THOUGHTS = [' Do you wanna know what I was thinking about ',
                                      ' Do you wanna hear this random thought I had about ',
                                      ' Do you wanna know this random thought I had about ',
                                      ' Do you wanna hear what I was thinking about ',
                                      ' Do you wanna know what I realized about ',
                                      ' Do you wanna hear what I realized about ']

    CONFIRM_ANOTHER_INFORM_SHOWER_THOUGHTS = [' Do you wanna hear another random thought I had about ',
                                              ' Do you wanna know another random thought I had about ',
                                              ' Do you wanna know what I was also thinking about ',
                                              ' Do you wanna hear what I was also thinking about ',
                                              ' Do you wanna know what I also realized about ',
                                              ' Do you wanna hear what I also realized about ',]

    INFORM_SHOWER_THOUGHTS_TOPIC = [' I think you wanna talk about ',
                                    ' I think you\'d like to talk about ',
                                    ' I think you wanna chat about ',
                                    ' I think you\'d like to chat about ',
                                    ' It looks like you wanna talk about ',
                                    ' It looks like you\'d like to talk about ',
                                    ' It looks like you wanna chat about ',
                                    ' It looks like you\'d like to chat about ',
                                    ' Looks like you wanna talk about ',
                                    ' Looks like you\'d like to talk about ',
                                    ' Looks like you wanna chat about ',
                                    ' Looks like you\'d like to talk about ']

    INFORM_SHOWER_THOUGHTS = [' Here\'s a random thought. ',
                              ' You know what I was thinking? ',
                              ' You know what I realized the other day? ',
                              ' I was high up in the cloud when I realized: ']

    INFORM_SHOWER_THOUGHTS_FOLLOWUP = [' Had you thought about that? ',
                                       ' Isn\'t that weird? ',
                                       ' Don\'t you agree? ']

    # used for errors
    ERROR = [' Sorry! Something wrong happened. ', 
            ' Whoops a daisy. Something went wrong. ', 
            ' Uh oh? I don\'t know what happened. ', 
            ' Whoops! System failure. ']

    # used after a response for an inappropriate topic
    INSTEAD = [' instead?']

    # user satisfaction
    GLAD_NO_FEELING = [' I knew you\'d like that. ',
                       ' I\'m glad you liked that. ',
                       ' I\'m happy you liked that. ',
                       ' Awesome! ']

    GLAD_FEELING = [' I\'m glad you thought that was ',
                    ' I\'m glad you feel this is ',
                    ' I\'m happy you thought that was ',
                    ' I\'m happy you feel this is ']

    APOLOGY_NO_FEELING = [' I\'m sorry to make you upset. ',
                          ' I\'m sorry. I didn\'t mean to upset you. ',
                          ' I didn\'t mean to upset you. ',
                          ' I\'m sorry for making you feel this way. ',
                          ' I\'m sorry. Thanks for telling me. I will try to learn what things are unpleasant. ']

    APOLOGY_FEELING = [' I\'m sorry you felt that was ',
                       ' I didn\'t realize you\'d think that was ']

    APOLOGY_CHALLENGE = [' I\'m pretty gullible and can\'t always tell what\'s true! ',
                         ' That\'s just what I heard! ',
                         ' I could be mistaken! ']

    APOLOGY_DUPLICATED_CONTENT = [' Sorry, I\'m a little out of it today. ',
                                  ' Sorry about that. I\'m a little out of it today. ',
                                  ' Sorry, sometimes I experience short-term memory loss! ',
                                  ' Sorry about that. Sometimes I experience short-term memory loss! ']

    APOLOGY_DUPLICATED_QUESTION = [' Sorry, I\'m rather forgetful. ',
                                   ' Sorry about that. I\'m rather forgetful. ',
                                   ' Sorry, my head\'s in the cloud sometimes. ',
                                   ' Sorry about that. My head\'s in the cloud sometimes. ',
                                   ' Sorry, I\'m a little out of it today. ',
                                   ' Sorry about that. I\'m a little out of it today. ',
                                   ' Sorry, sometimes I experience short-term memory loss! ',
                                   ' Sorry about that. Sometimes I experience short-term memory loss! ']

    APOLOGY_CONFUSING = [' Sorry. ',
                         ' I\'m sorry. ',
                         ' Sorry about that! ',
                         ' I\'m sorry about that! ',
                         ' I\'ll do a better job next time. ',
                         ' Sorry I can be confusing sometimes. ',
                         ' I\'m sorry, I can be confusing sometimes. ',
                         ' Sorry I know I tend to babble. ',
                         ' I\'m sorry, I know I tend to babble. ']

    APOLOGY_SKIP = [' I\'ll try to do a better job. ',
                    ' I\'ll do a better job next time. ']

    # user knowledge
    BY_THE_WAY  = [' By the way ']

    LET_ME_SEE = [' Let me think. ',
                  ' Let\'s see. ',
                  ' ']

    ARE_YOU_FAMILIAR_WITH = [' do you know anything about ',
                             ' have you heard anything about ',
                             ' have you heard about ',
                             ' are you familiar with ']

    # user side
    AGREE = [' Looks like you mostly agree with this. ',
             ' Looks like you think that it makes sense. ',
             ' Looks like this does make sense to you. ',
             ' Sounds like you mostly agree with this. ',
             ' Sounds like you think that it makes sense. ',
             ' Sounds like this does make sense to you. ',
             ' So you\'re saying that you mostly agree with this. ',
             ' So you\'re saying that you think that it makes sense. ',
             ' So you\'re saying that this does make sense to you. ',
             ' It seems that you mostly agree with this. ',
             ' It seems that you think that it makes sense. ',
             ' It seems that this does make sense to you. ',
             ' It looks like you mostly agree with this. ',
             ' It looks like you think that it makes sense. ',
             ' It looks like this does make sense to you. ',
             ' I see that you mostly agree with this. ',
             ' I see that you think that it makes sense. ',
             ' I see that this does make sense to you. ']

    DISAGREE = [' Looks like you mostly disagree with this. ',
                ' Looks like you think that it doesn\'t make sense. ',
                ' Looks like this does not make sense to you. ',
                ' Sounds like you mostly disagree with this. ',
                ' Sounds like you think that it doesn\'t make sense. ',
                ' Sounds like this does not make sense to you. ',
                ' So you\'re saying that you mostly disagree with this. ',
                ' So you\'re saying that you think that it doesn\'t make sense. ',
                ' So you\'re saying that this does not make sense to you. ',
                ' It seems that you mostly disagree with this. ',
                ' It seems that you think that it doesn\'t make sense. ',
                ' It seems that this does not make sense to you. ',
                ' It looks like you mostly disagree with this. ',
                ' It looks like you think that it doesn\'t make sense. ',
                ' It looks like this does not make sense to you. ',
                ' I see that you mostly disagree with this. ',
                ' I see that you think that it doesn\'t make sense. ',
                ' I see that this does not make sense to you. ']

    UNSURE = [' Looks like you are unsure about this. ',
              ' Looks like you don\'t have an opinion on this. ',
              ' Sounds like you are unsure about this. ',
              ' Sounds like you don\'t have an opinion on this. ',
              ' So you\'re saying that you are unsure about this. ',
              ' So you\'re saying that you don\'t have an opinion on this. ',
              ' It seems that you are unsure about this. ',
              ' It seems that you don\'t have an opinion on this. ',
              ' It looks like you are unsure about this. ',
              ' It looks like you don\'t have an opinion on this. ',
              ' I see that you are unsure about this. ',
              ' I see that you don\'t have an opinion on this. ']

    # news miniskill
    NO_NEWS_TOPIC = [' I\'m having trouble finding news about ',
                     ' I couldn\'t find any news about ',
                     ' I momentarily forgot everything I know about ',
                     ' I don\'t really have anything to say about ',
                     ' I don\'t have anything to say about ',
                     ' I didn\'t find anything related with ']

    RIGHT_NOW = [' right now. ',
                 ' currently. ',
                 ' at this moment. ']
        
    NO_NEWS_NO_TOPIC = [' I\'m having trouble finding news ',
                        ' I couldn\'t find any news ',
                        ' I momentarily forgot everything I know ']

    NEWS_TOPIC_EXHAUSTED = [' I\'m having trouble finding more news about ',
                            ' I couldn\'t find any other news about ',
                            ' I haven\'t seen any other news about ',
                            ' I don\'t know more about ',
                            ' I don\'t really have more to say about ',
                            ' I don\'t have anything else to say about ',
                            ' I\'m running out of things to say about ']

    CONFIRM_INFORM_NEWS = [' Would you like to talk about ',
                           ' Would you like to chat about ',
                           ' Would you like to hear about ',
                           ' Do you wanna talk about ',
                           ' Do you wanna chat about ',
                           ' Do you wanna hear about ',
                           ' Let\'s talk about ',
                           ' Let\'s chat about ',
                           ' How about we talk about ',
                           ' How about we chat about ']

    CONFIRM_ANOTHER_INFORM_NEWS = [' Do you wanna go back to our conversation about ',
                                   ' Do you wanna go back to our discussion about ',
                                   ' Do you wanna go back to talking about ',
                                   ' Should we go back to our discussion about ',
                                   ' Should we go back to our conversation about ',
                                   ' Should we go back to talking about ',
                                   ' Do you wanna talk more about ']

    INFORM_NEWS_FOLLOWUP = [' Have you read this? ',
                            ' Have you seen this news? ',
                            ' Have you heard this news? ',
                            ' Has anyone told this before? ',
                            ' Do you have anything to say about this? ',
                            ' Is there anything you wanna say about this? ',
                            ' Have you read it? ',
                            ' Did you read this? ',
                            ' Did you see this? ',
                            ' Have you heard of this? ',
                            ' Did you know about this? ',
                            ' Did you read about this? ']

    CONFIRM_RESUME_BACKGROUND = [' Are you still interested in hearing background information about ',
                                 ' Are you still interested in hearing more information about ',
                                 ' Are you still interested in knowing background information about ',
                                 ' Are you still interested in knowing more information about ',
                                 ' Should I continue telling you background information about ',
                                 ' Should I continue telling you more information about ',
                                 ' Do you want me to continue telling you background information about ',
                                 ' Do you want me to continue telling you more information about ',
                                 ' Would you like me to continue telling you background information about ',
                                 ' Would you like me to continue telling you more information about ',
                                 ' Are you interested in hearing background information about ',
                                 ' Are you interested in hearing more information about ',
                                 ' Are you interested in knowing background information about ',
                                 ' Are you interested in knowing more information about ',
                                 ' Should I give you background information about ',
                                 ' Should I give you more information about ']

    # used in conjunction with ARE_YOU_FAMILIAR_WITH
    CONFIRM_INFORM_BACKGROUND_NEWS = [' Should I tell you some background information about ',
                                      ' Do you want me to tell you some background information about ',
                                      ' Would you like to let me tell you some background information about ',
                                      ' Should I tell you some more information about ',
                                      ' Do you want me to tell you some more information about ',
                                      ' Would you like to let me tell you some more information about ',
                                      ' Should I look up some background information about ',
                                      ' Do you want me to look up some background information about ',
                                      ' Would you like to let me look up some background information about ',
                                      ' Should I look up some more information about ',
                                      ' Do you want me to look up some more information about ',
                                      ' Would you like to let me look up some more information about ',
                                      ' Should I get you some background information about ',
                                      ' Do you want me to get you some background information about ',
                                      ' Would you like to let me get you some background information about ',
                                      ' Should I get you some more information about ',
                                      ' Do you want me to get you some more information about ',
                                      ' Would you like to let me get you some more information about ',
                                      ' Should I search for some background information about ',
                                      ' Do you want me to search for some background information about ',
                                      ' Would you like to let me search for some background information about ',
                                      ' Should I search for some more information about ',
                                      ' Do you want me to search for some more information about ',
                                      ' Would you like me to search for some more information about ']

    CONFIRM_INFORM_BACKGROUND_NEWS_PART_ONE = [' I can look up some background information about ',
                                               ' I can get you some background information about ',
                                               ' I can tell you some background information about ',
                                               ' I can search for some background information about ',
                                               ' I can also look up some background information about ',
                                               ' I can also get you some background information about ',
                                               ' I can also tell you some background information about ',
                                               ' I can also search for some background information about ',
                                               ' I can look up some more information about ',
                                               ' I can get you some more information about ',
                                               ' I can tell you some more information about ',
                                               ' I can search for some more information about ',
                                               ' I can also look up some more information about ',
                                               ' I can also get you some more information about ',
                                               ' I can also tell you some more information about ',
                                               ' I can also search for some more information about ']

    CONFIRM_INFORM_BACKGROUND_NEWS_PART_TWO = [' should I continue? ',
                                               ' do you want me to continue? ',
                                               ' would you like to let me continue? ']

    INFORM_NEWS = [' The title was: ',
                   ' It said: ']

    INFORM_NEWS_DATE_CONFIRM = [' Did you see this article from ',
                                ' Have you read this article from ',
                                ' Did you see this headline from ']

    INFORM_NEWS_DATE = [' I read this article from ',
                        ' I found this article from ',
                        ' I saw this headline from ']

    INFORM_NEWS_DATE_SUFFIX = [' Have you read it? ',
                               ' Have you read this? ',
                               ' Did you read this? ',
                               ' Did you see this? ',
                               ' Did you see it? ',
                               ' Have you heard of this? ',
                               ' Did you know about this? ',
                               ' Did you read about this? ']

    PREVIOUSLY_MENTIONED = [' This news talked about ',
                            ' This news mentioned ',
                            ' This article talked about ',
                            ' This article mentioned ']

    # movies
    NO_MOVIE_TOPIC = [' I don\'t know the movie: ']

    NO_MOVIE_NO_TOPIC = [' I\'m having trouble remembering interesting movies right now. ',
                         ' I\'m having trouble remembering interesting movies currently. ',
                         ' I\'m having trouble remembering interesting movies at this moment. ',
                         ' I can\'t seem to find any interesting movies right now. ',
                         ' I can\'t seem to find any interesting movies currently. ',
                         ' I can\'t seem to find any interesting movies at this moment. ']

    MOVIE_MORE_EXHAUSTED = [' Hmm. I don\'t know more about this movie. ',
                            ' Hmm. I don\'t really have more to say about this movie. ',
                            ' Hmm. I don\'t have anything else to say about this movie. ',
                            ' Hmm. I\'m running out of things to say about this movie. ']

    MOVIE_EXHAUSTED = [' Hmm. I don\'t really have anything to say about this movie. ',
                       ' Hmm. I don\'t have anything to say about this movie. ',
                       ' Hmm. I didn\'t find anything related with this movie. ']

    APOLOGY_REJECT_MOVIE = [' No worries! I didn\'t think it was that great either. ',
                            ' No worries! I didn\'t think it was that good either. ',
                            ' No problem, I didn\'t think it was that great either. ',
                            ' No problem, I didn\'t think it was that good either. ',
                            ' Okay! I didn\'t think it was that great either. ',
                            ' Okay! I didn\'t think it was that good either. ',
                            ' No worries! I also honestly didn\'t think it was that good. ',
                            ' No worries! I also honestly didn\'t think it was that great. ',
                            ' No problem, I also honestly didn\'t think it was that good. ',
                            ' No problem, I also honestly didn\'t think it was that great. ',
                            ' Okay! I also honestly didn\'t think it was that great. ',
                            ' Okay! I also honestly didn\'t think it was that good. ',
                            ' No worries! ',
                            ' No problem! ',
                            ' Okay! ']

    APOLOGY_NO_PLOT = [' Sorry, I actually haven\'t seen this one yet. ',
                       ' I actually haven\'t seen this one yet. ',
                       ' Sorry, I actually haven\'t watched this one yet. ',
                       ' I actually haven\'t watched this one yet. ',
                       ' Sorry, I actually haven\'t read the plot for this one yet. ',
                       ' I actually haven\'t read the plot for this one yet. ',
                       ' Sorry, I actually haven\'t this movie yet. ',
                       ' I actually haven\'t seen this movie yet. ',
                       ' Sorry, I actually haven\'t watched this movie yet. ',
                       ' I actually haven\'t watched this movie yet. ',
                       ' Sorry, I actually haven\'t read the plot for this movie yet. ',
                       ' I actually haven\'t read the plot for this movie yet. ',]

    CONFIRM_INFORM_MOVIE = [' Would you like to talk about ',
                            ' Would you like to chat about ',
                            ' Would you like to hear about ',
                            ' Do you wanna talk about ',
                            ' Do you wanna chat about ',
                            ' Do you wanna hear about ',
                            ' Let\'s talk about ',
                            ' Let\'s chat about ',
                            ' How about we talk about ',
                            ' How about we chat about ']

    CONFIRM_RESUME_MOVIE = [' Do you wanna go back to our conversation about the movie ',
                            ' Do you wanna go back to our discussion about the movie ',
                            ' Do you wanna go back to talking about the movie ',
                            ' Should we go back to our discussion about the movie ',
                            ' Should we go back to our conversation about the movie ',
                            ' Should we go back to talking about the movie ',
                            ' Do you wanna talk more about the movie ']
                             
    INFORM_MOVIE_NO_NAME = [' Have you seen this movie? ',
                            ' Have you watched this movie? ',
                            ' Did you see this movie? ',
                            ' Did you watch this movie? ']

    INFORM_MOVIE_NAME = [' Have you seen ',
                         ' Have you watched ',
                         ' Did you see ',
                         ' Did you watch ']

    YEAR_RELEASED = [' It was released in ',
                     ' It came out in ']

    TYPE_OF_MOVIE = [' It is a ']

    MOVIE = [' movie ']

    INFORM_MOVIE_DIRECTOR_PREFIX = [' It is directed by ',
                                    ' It was directed by ']

    INFORM_MOVIE_DIRECTOR_SUFFIX = [' directed this movie. ',
                                    ' directed it. ',
                                    ' directed this film. ']

    INFORM_MOVIE_CO_DIRECTOR_PREFIX = [' It is co-directed by ',
                                       ' It was co-directed by ']

    INFORM_MOVIE_CO_DIRECTOR_SUFFIX = [' co-directed this movie. ',
                                       ' co-directed it. ',
                                       ' co-directed this film. ']

    AND = [' and ']

    INFORM_MOVIE_CAST = [' starred in it. ',
                         ' actually starred in that. ']

    CONFIRM_INFORM_MOVIE_PLOT = [' Do you want to hear the plot? ',
                                 ' Do you want to hear the gist of the movie? ',
                                 ' Do you want to hear what it\'s about? ',
                                 ' Do you want to hear what the movie is about? ',
                                 ' Do you want to hear what the overall plot is?',
                                 ' Do you want me to tell you the plot? ',
                                 ' Do you want me to tell you the gist of the movie? ',
                                 ' Do you want me to tell you what it\'s about? ',
                                 ' Do you want me to tell you what the movie is about? ',
                                 ' Do you want me to tell you what the overall plot is? ',
                                 ' Do you want me to explain the plot? ',
                                 ' Do you want me to explain the gist of the movie? ',
                                 ' Do you want me to explain what it\'s about? ',
                                 ' Do you want me to explain what the movie is about? ',
                                 ' Do you want me to explain what the overall plot is? ']

    INFORM_MOVIE_PLOT = [' Here\'s the main storyline: ',
                         ' Here\'s the plot: ',
                         ' Here\'s what it\'s about: ',
                         ' Here\'s basically what it\'s about ']

    INFORM_IMDB_RATING_PREFIX = [' The movie was rated a ',
                                 ' The movie got a ',
                                 ' The movie has a ',
                                 ' It was rated a ',
                                 ' It got a ',
                                 ' It has a ',
                                 ' The film was rated a ',
                                 ' The film got a ',
                                 ' The film has a ']

    INFORM_IMDB_RATING_SUFFIX = [' out of 10 on IMDB. ']

    INFORM_IMDB_RATING_GOOD = [' It seems to be really popular. ',
                               ' which seems really good! ',
                               ' so it must be highly acclaimed. ']

    INFORM_IMDB_RATING_OK = [' It seems to be pretty popular. ',
                             ' which seems pretty good! ']

    INFORM_IMDB_RATING_BAD = [' so it\'s probably not well liked. ',
                              ' which seems pretty low to me. ']

    INFORM_IMDB_RATING_REALLY_BAD = [' It seems to be not that popular. ',
                                     ' so it\'s probably mildly disliked. ']

    CONFIRM_INFORM_MOVIE_REVIEW = [' Do you wanna hear a review of the movie? ']

    INFORM_MOVIE_FOLLOWUP = [' Have you watched this movie? ',
                             ' Have you seen this movie? ',
                             ' Have you heard of this movie? ']

    INFORM_MOVIE_TOPIC = [' I think you wanna talk about ',
                          ' I think you\'d like to talk about ',
                          ' I think you wanna chat about ',
                          ' I think you\'d like to chat about ',
                          ' It looks like you wanna talk about ',
                          ' It looks like you\'d like to talk about ',
                          ' It looks like you wanna chat about ',
                          ' It looks like you\'d like to chat about ',
                          ' Looks like you wanna talk about ',
                          ' Looks like you\'d like to talk about ',
                          ' Looks like you wanna chat about ',
                          ' Looks like you\'d like to talk about ']

    THEY_DIRECTED = [' They directed ']

    THEY_STARRED_IN = [' They starred in ',
                       ' They were a part of ']

    WHAT_TO_TALK_ABOUT_NEXT = [' What would you like to talk about next? ',
                               ' Is there something you would like to talk about next? ',
                               ' Is there any other topic that interests you? ']

    THEY_WERE_A = ['They were a ']
    NO_EVI_RESPONSE_APOLOGY = ['Sorry, I don\'t know, ask me about something else?']
    EVI_REPROMPT = ['Can I answer anything else?']
