class PlugInConfig(object):
    """
    Determine which plugins are used
    """

    # topic diversity for continue/next
    continue_topic_diversity_plug_in = False

    # topic diversity for positive user information
    positive_user_information_topic_diversity_plug_in = False

    # miniskill diversity for continue/next
    continue_miniskill_diversity_plug_in = True

    # miniskill diversity for positive user information
    positive_user_information_miniskill_diversity_plug_in = True