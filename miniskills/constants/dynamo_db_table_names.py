class DynamoDbTableNames(object):
    """
    Names of DynamoDbTables
    """

    # share facts
    today_i_learned_threads = 'TodayILearned_Threads_r1.2.0'
    today_i_learned_topics = 'TodayILearned_Topics_r1.2.0'

    # shower thoughts
    shower_thoughts_threads = 'ShowerThoughts_Threads_r1.1.0'
    shower_thoughts_topics = 'ShowerThoughts_Topics_r1.1.0'

    # news
    news_threads = 'News_Threads_r1.0.0'
    news_topics = 'News_Topics_r1.0.0'

    futurology_threads = 'Futurology_Threads_r1.0.0'
    futurology_topics = 'Futurology_Topics_r1.0.0'

    politics_threads = 'Politics_Threads_r1.0.0'
    politics_topics = 'Politics_Topics_r1.0.0'

    science_threads = 'Science_Threads_r1.0.0'
    science_topics = 'Science_Topics_r1.0.0'

    sports_threads = 'Sports_Threads_r1.0.0'
    sports_topics = 'Sports_Topics_r1.0.0'

    technology_threads = 'Technology_Threads_r1.0.0'
    technology_topics = 'Technology_Topics_r1.0.0'

    world_news_threads = 'WorldNews_Threads_r1.0.0'
    world_news_topics = 'WorldNews_Topics_r1.0.0'

    # movies
    movie_threads = 'MovieGuide_Threads_r1.4.0'
    movie_topics = 'MovieGuide_Topics_r1.4.0'