class GreetingResponses(object):
    """
    All responses possible for greeting
    """
    RESPONSE_GOOD = [' Glad to hear it! ',
                     ' That\'s great! ']

    RESPONSE_BAD = [' I\'m sorry to hear that. ',
                    ' Sorry to hear that. ']

    RECIPROCATE_MILD = [' I\'m good! ',
                        ' I\'m doing good! ',
                        ' I\'m well! ',
                        ' I\'m doing well! ']

    RECIPROCATE_HAPPY = [' I\'m good. ',
                         ' I\'m doing good. ',
                         ' I\'m amazing. ',
                         ' I\'m doing amazing. ',
                         ' I\'m well. ',
                         ' I\'m doing well. ',
                         ' I\'m great. ',
                         ' I\'m doing great. ']