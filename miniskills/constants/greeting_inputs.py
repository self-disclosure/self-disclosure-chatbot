class GreetingInputs(object):
    """
    All inputs possible for greeting
    """
    # user input
    GOOD = ['fine',
            'good',
            'amazing',
            'well',
            'great']

    BAD = ['bad',
           'terrible',
           'horrible',
           'shitty',
           'sleepy',
           'tired',
           'angry',
           'annoyed']