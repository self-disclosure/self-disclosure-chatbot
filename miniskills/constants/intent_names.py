class IntentNames(object):
    """
    Names of intents.
    """

    repeat_intent = 'AMAZON.RepeatIntent'
    continue_intent = 'ContinueIntent'
    cancel_intent = 'CancelIntent'
    greeting_intent = 'GreetingIntent'

    # initial ASK intents
    # correct_ask_intent
    special_user_utterance_response_intent = 'SpecialUserUtteranceResponseIntent'
    request_fact_intent = 'RequestFactIntent'
    navigation_intent = 'NavigationIntent'
    launch_request_intent = 'LaunchRequestIntent'
    invalid_intent = 'InvalidIntent'
    request_opinion_intent = 'RequestOpinionIntent'
    request_news_intent = 'RequestNewsIntent'
    request_movie_intent = 'RequestMovieIntent'
    ask_question_intent = 'AskQuestionIntent'
    # navigation for ignored and deflected content/topic from the user
    ignored_and_deflected_intent = 'IgnoreAndDeflectedIntent'

    # AMAZON intents
    amazon_yes_intent = 'AMAZON.YesIntent'
    amazon_no_intent = 'AMAZON.NoIntent'
    amazon_help_intent = 'AMAZON.HelpIntent'
    amazon_next_intent = 'AMAZON.NextIntent'
    amazon_stop_intent = 'AMAZON.StopIntent'

    # intents that map to more than 1 miniskill
    inform_topic_intent = 'InformTopicIntent'
