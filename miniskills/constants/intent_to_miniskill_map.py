from miniskills.constants.intent_names import IntentNames
from miniskills.constants.miniskills import Miniskills

class IntentToMiniskillMap(object):
    """
    Map intent to miniskill
    """
    intent_map = {IntentNames.request_fact_intent: Miniskills.SHAREFACTS, 
                  IntentNames.request_opinion_intent: Miniskills.SHOWERTHOUGHTS, 
                  IntentNames.request_news_intent: Miniskills.NEWS,
                  IntentNames.request_movie_intent: Miniskills.MOVIES,
                  IntentNames.ask_question_intent: Miniskills.ASKQUESTION}