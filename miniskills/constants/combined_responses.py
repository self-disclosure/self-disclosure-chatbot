from miniskills.constants.responses import Responses
import random
import re

class CombinedResponses(object):
    """
    Combine commonly used responses into one callable response function that can be reused
    """

    def topic_exhausted(self, topic_exhausted_response, topic, confirm_inform_response):
        """
        Create response for exhausting an input string topic and informing about a new string data
        """
        return topic_exhausted_response + topic + '. ' + confirm_inform_response

    def not_familiar_topic(self, topic):
        """
        Create response for an unfamiliar input string topic
        """
        not_familiar_topic = random.choice(Responses.NOT_FAMILIAR_TOPIC)
        if topic is None:
            not_familiar_topic += 'that'
        else:
            not_familiar_topic += topic
        if re.match(r'\bI haven\'t\b', not_familiar_topic) is not None:
            return not_familiar_topic + ' yet.'
        else:
            return not_familiar_topic + '.'

    def confirm_resume_topic(self, topic):
        """
        Create response for asking user to continue conversation on topic
        """
        return random.choice(Responses.CONTINUE) + random.choice(Responses.CONVERSATION) + random.choice(Responses.ON) + topic + '?'

    def apology_feeling_selector(self, feeling, topic):
        """
        Create response for apologies with feelings
        """
        response_number = random.randint(0, 1)
        if response_number == 0:
            return random.choice(Responses.APOLOGY_FEELING) + feeling + '. ' + self.confirm_resume_topic(topic)
        else:
            return random.choice(Responses.APOLOGY_NO_FEELING) + '. ' + self.confirm_resume_topic(topic)