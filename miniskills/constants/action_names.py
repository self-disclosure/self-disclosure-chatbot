class ActionNames(object):
    """
    Action names used to determine response.
    """

    # used in global and invalid handlers
    REPEAT = 'REPEAT'
    CANCEL = 'CANCEL'
    HELP = 'HELP'

    # handler actions
    SWITCH_MINISKILL = 'SWITCH_MINISKILL'
    PULL_THREAD_FOR_TOPIC = 'PULL_THREAD_FOR_TOPIC'
    SWITCH_TOPIC = 'SWITCH_TOPIC'
    PULL_ANOTHER_THREAD = 'PULL_ANOTHER_THREAD'
    PULL_DIFFERENT_TOPIC_IN_THREAD = 'PULL_DIFFERENT_TOPIC_IN_THREAD'
    NO_DYNAMO_DB_PULL = 'NO_DYNAMO_DB_PULL'