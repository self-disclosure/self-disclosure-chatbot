from miniskills.constants.dynamo_db_table_names import DynamoDbTableNames
from miniskills.constants.intent_names import IntentNames

class IntentToTopicAndThreadDynamoDbTablesMap(object):
    """
    Map intent to topic and thread dynamo db tables
    """
    intent_map = {IntentNames.request_fact_intent: [DynamoDbTableNames.today_i_learned_topics, DynamoDbTableNames.today_i_learned_threads],
                  IntentNames.request_opinion_intent: [DynamoDbTableNames.shower_thoughts_topics, DynamoDbTableNames.shower_thoughts_threads],
                  IntentNames.request_news_intent: [DynamoDbTableNames.news_topics, DynamoDbTableNames.news_threads,
                                                    DynamoDbTableNames.futurology_topics, DynamoDbTableNames.futurology_threads,
                                                    DynamoDbTableNames.politics_topics, DynamoDbTableNames.politics_threads,
                                                    DynamoDbTableNames.science_topics, DynamoDbTableNames.science_threads,
                                                    DynamoDbTableNames.sports_topics, DynamoDbTableNames.sports_threads,
                                                    DynamoDbTableNames.technology_topics, DynamoDbTableNames.technology_threads,
                                                    DynamoDbTableNames.world_news_topics, DynamoDbTableNames.world_news_threads],
                  IntentNames.request_movie_intent: [DynamoDbTableNames.movie_topics, DynamoDbTableNames.movie_threads]}