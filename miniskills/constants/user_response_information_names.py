class UserResponseInformationNames(object):
    """
    User Response Information Names
    """

    # User Satisfaction
    POSITIVE = 'POSITIVE'
    NEGATIVE = 'NEGATIVE'
    STRONG_NEGATIVE = 'STRONG_NEGATIVE'

    # Type of User Satisfaction
    FEELING = 'FEELING'
    CHALLENGE = 'CHALLENGE'
    DUPLICATED_CONTENT = 'DUPLICATED_CONTENT'
    DUPLICATED_QUESTION = 'DUPLICATED_QUESTION'
    CONFUSING = 'CONFUSING'
    OTHER_FEELING = 'OTHER_FEELING'
    OTHER = 'OTHER'
    SKIP = 'SKIP'
    GLAD_FEELING = 'GLAD_FEELING'
    GLAD_NO_FEELING = 'GLAD_NO_FEELING'

    # User Knowledge
    NOT_KNOWN = 'NOT_KNOWN'
    KNOWN = 'KNOWN'

    # User Decision
    ACCEPT = 'ACCEPT'
    REJECT = 'REJECT'
    UNSURE = 'UNSURE'

    # User Side
    AGREE = 'AGREE'
    DISAGREE = 'DISAGREE'
    UNSURE_SIDE = 'UNSURE_SIDE'