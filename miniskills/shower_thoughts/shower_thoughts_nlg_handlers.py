from miniskills.constants.action_names import ActionNames
from miniskills.constants.responses import Responses
from miniskills.base_nlg_handlers.no_dynamo_db_pull_nlg_handler import NoDynamoDbPullNlgHandler
from miniskills.base_nlg_handlers.pull_another_thread_nlg_handler import PullAnotherThreadNlgHandler
from miniskills.base_nlg_handlers.pull_different_topic_in_thread_nlg_handler import PullDifferentTopicInThreadNlgHandler
from miniskills.base_nlg_handlers.pull_thread_for_topic_nlg_handler import PullThreadForTopicNlgHandler
from miniskills.base_nlg_handlers.switch_miniskill_nlg_handler import SwitchMiniskillNlgHandler
from miniskills.base_nlg_handlers.switch_topic_nlg_handler import SwitchTopicNlgHandler
import random

class ShowerThoughtsNoDynamoDbPullNlgHandler(NoDynamoDbPullNlgHandler):
    """
    Handle no dynamodb pulls for ShowerThoughts
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        confirm_resume_topic = random.choice(Responses.CONTINUE) + random.choice(Responses.CONVERSATION) + random.choice(Responses.ON) + handler_selection_features.state_manager.current_state.old_topic + '?'
        return self.nlg_template(handler_selection_features, confirm_resume_topic)

class ShowerThoughtsPullAnotherThreadNlgHandler(PullAnotherThreadNlgHandler):
    """
    Handle pulling another thread for ShowerThoughts
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        continue_inform_topic = random.choice(Responses.SURE) + random.choice(Responses.INFORM_SHOWER_THOUGHTS) + handler_selection_features.nlg_content
        inform_followup = random.choice(Responses.INFORM_SHOWER_THOUGHTS_FOLLOWUP)
        did_you_know_response = random.choice(Responses.INFORM_SHOWER_THOUGHTS) + handler_selection_features.nlg_content
        confirm_resume_topic = random.choice(Responses.CONTINUE) + random.choice(Responses.CONVERSATION) + random.choice(Responses.ON) + handler_selection_features.state_manager.current_state.old_topic + '?'
        return self.nlg_template(handler_selection_features, continue_inform_topic, inform_followup, did_you_know_response, confirm_resume_topic)

class ShowerThoughtsPullDifferentTopicInThreadNlgHandler(PullDifferentTopicInThreadNlgHandler):
    """
    Handle pulling topic in the same thread for ShowerThoughts
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        return self.nlg_template(handler_selection_features, '')

class ShowerThoughtsPullThreadForTopicNlgHandler(PullThreadForTopicNlgHandler):
    """
    Handle pulling thread for topic for ShowerThoughts
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        inform_topic = None
        random_number = random.randint(0, 1)
        if handler_selection_features.state_manager.current_state.old_topic != ':frontpage':
            if random_number == 0:
                inform_topic = random.choice(Responses.INFORM_SHOWER_THOUGHTS_TOPIC) + handler_selection_features.state_manager.current_state.old_topic
            else:
                inform_topic = handler_selection_features.state_manager.current_state.old_topic + random.choice(Responses.QUESTION_MARK) + random.choice(Responses.SURE)
        continue_inform = random.choice(Responses.INFORM_SHOWER_THOUGHTS) + handler_selection_features.nlg_content['thread_data']
        if inform_topic:
            inform_topic = inform_topic + '. ' + continue_inform
        else:
            inform_topic = continue_inform
        inform_no_topic = random.choice(Responses.INFORM_SHOWER_THOUGHTS) + handler_selection_features.nlg_content['thread_data']
        followup = random.choice(Responses.INFORM_SHOWER_THOUGHTS_FOLLOWUP)
        return self.nlg_template(handler_selection_features, inform_topic, continue_inform, inform_no_topic, followup)

class ShowerThoughtsSwitchMiniskillNlgHandler(SwitchMiniskillNlgHandler):
    """
    Handle switching miniskill dynamodb pulls for switching to ShowerThoughts miniskill
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        topic_exhausted_response = self.last_miniskill_topic_exhausted_response(handler_selection_features)
        topic = handler_selection_features.state_manager.current_state.corrected_topic
        handler_selection_features.state_manager.current_state.old_topic = topic
        confirm_inform_response = random.choice(Responses.CONFIRM_INFORM_SHOWER_THOUGHTS) + topic
        return self.nlg_template(topic_exhausted_response, topic, confirm_inform_response)

class ShowerThoughtsSwitchTopicNlgHandler(SwitchTopicNlgHandler):
    """
    Handle switching topic dynamodb pulls for switching to ShowerThoughts miniskill
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        topic_exhausted_response = random.choice(Responses.SHOWER_THOUGHTS_TOPIC_EXHAUSTED)
        confirm_inform_response = random.choice(Responses.CONFIRM_INFORM_SHOWER_THOUGHTS) + handler_selection_features.nlg_content
        return self.nlg_template(handler_selection_features, topic_exhausted_response, confirm_inform_response)