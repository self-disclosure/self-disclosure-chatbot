from miniskills.base_handlers.no_dynamo_db_pull_handler import NoDynamoDbPullHandler
from miniskills.base_handlers.pull_another_thread_handler import PullAnotherThreadHandler
from miniskills.base_handlers.pull_different_topic_in_thread_handler import PullDifferentTopicInThreadHandler
from miniskills.base_handlers.pull_thread_for_topic_handler import PullThreadForTopicHandler
from miniskills.base_handlers.switch_miniskill_handler import SwitchMiniskillHandler
from miniskills.base_handlers.switch_topic_handler import SwitchTopicHandler
from miniskills.global_handlers.global_navigational_handler import GlobalNavigationalHandler
from miniskills.handler_dispatch_generic_response_generator.handler_dispatch_response_generator import HandlerDispatchResponseGenerator
from miniskills.shower_thoughts.shower_thoughts_nlg_handlers import ShowerThoughtsNoDynamoDbPullNlgHandler, ShowerThoughtsSwitchMiniskillNlgHandler, ShowerThoughtsPullThreadForTopicNlgHandler, ShowerThoughtsSwitchTopicNlgHandler, ShowerThoughtsPullAnotherThreadNlgHandler, ShowerThoughtsPullDifferentTopicInThreadNlgHandler

class ShowerThoughtsResponseGenerator(HandlerDispatchResponseGenerator):
    """
    Initialize list of possible handlers, run dispatch, return a response for ShowerThoughts
    """

    list_of_handlers = [NoDynamoDbPullHandler(), PullAnotherThreadHandler(), PullDifferentTopicInThreadHandler(), PullThreadForTopicHandler(), SwitchMiniskillHandler(), SwitchTopicHandler(), ShowerThoughtsNoDynamoDbPullNlgHandler(), ShowerThoughtsSwitchMiniskillNlgHandler(), ShowerThoughtsPullThreadForTopicNlgHandler(), ShowerThoughtsSwitchTopicNlgHandler(), ShowerThoughtsPullAnotherThreadNlgHandler(), ShowerThoughtsPullDifferentTopicInThreadNlgHandler(), GlobalNavigationalHandler()]

    def __init__(self, state_manager, module_name, service_module_config, handler_dispatcher = None, list_of_handlers = None):
        super(ShowerThoughtsResponseGenerator, self).__init__(state_manager, module_name, service_module_config, handler_dispatcher, self.list_of_handlers)