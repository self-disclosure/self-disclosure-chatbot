from miniskills.constants.action_names import ActionNames
from miniskills.constants.intent_names import IntentNames
from miniskills.constants.speech_action_names import SpeechActionNames
from miniskills.handler_dispatch_generic_response_generator.handler import Handler

class PullThreadForTopicNlgHandler(Handler):
    """
    NLG template for pull thread for topic
    """
    def can_handle(self, handler_selection_features):
        return handler_selection_features.action_name == ActionNames.PULL_THREAD_FOR_TOPIC and handler_selection_features.nlg_content is not None

    def nlg_template(self, handler_selection_features, inform_topic, continue_inform, inform_no_topic, followup):
        response = None
        reprompt = None
        if handler_selection_features.speech_action_name == SpeechActionNames.INFORM_TOPIC or handler_selection_features.speech_action_name == SpeechActionNames.CONTINUE:
            if handler_selection_features.state_manager.current_state.intent == IntentNames.continue_intent or handler_selection_features.state_manager.last_state.get('intent') == IntentNames.navigation_intent:
                response = continue_inform
            else:
                response = inform_topic
            reprompt = followup
        elif handler_selection_features.speech_action_name == SpeechActionNames.INFORM_NO_TOPIC:
            response = inform_no_topic
            reprompt = followup
        response_and_reprompt = {
            'response': response,
            'reprompt': reprompt
        }
        return response_and_reprompt