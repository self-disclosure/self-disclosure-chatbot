from miniskills.constants.action_names import ActionNames
from miniskills.constants.intent_names import IntentNames
from miniskills.constants.responses import Responses
from miniskills.constants.speech_action_names import SpeechActionNames
from miniskills.constants.user_response_information_names import UserResponseInformationNames
from miniskills.handler_dispatch_generic_response_generator.handler import Handler
import random
import re

class NoDynamoDbPullNlgHandler(Handler):
    def can_handle(self, handler_selection_features):
        return handler_selection_features.action_name == ActionNames.NO_DYNAMO_DB_PULL and handler_selection_features.nlg_content is not None

    def nlg_template(self, handler_selection_features, confirm_resume_topic):
        response = None
        reprompt = None
        if handler_selection_features.state_manager.current_state.corrected_ask_intent == IntentNames.special_user_utterance_response_intent:
            if handler_selection_features.speech_action_name == SpeechActionNames.USER_APOLOGY:
                response = random.choice(Responses.USER_APOLOGY)
            elif handler_selection_features.speech_action_name == SpeechActionNames.USER_THANKS:
                response = random.choice(Responses.USER_THANKS)
            elif handler_selection_features.speech_action_name == SpeechActionNames.USER_SKEPTICAL:
                response = random.choice(Responses.USER_SKEPTICAL)
            elif handler_selection_features.speech_action_name == SpeechActionNames.USER_BACKWARD_LOOKING_DETAIL:
                response = random.choice(Responses.USER_BACKWARD_LOOKING_DETAIL)
            elif handler_selection_features.speech_action_name == SpeechActionNames.USER_BAD_MOOD:
                response = random.choice(Responses.USER_BAD_MOOD)
            elif handler_selection_features.speech_action_name == SpeechActionNames.HELP:
                response = random.choice(Responses.INSTRUCT_CONTINUE) + random.choice(Responses.INSTRUCT_ALL)
                reprompt = response
            else:
                response = random.choice(Responses.ERROR)

            if handler_selection_features.state_manager.current_state.old_topic is None:
                response += random.choice(Responses.MINISKILL_OPTIONS)
            else:
                response += confirm_resume_topic
            reprompt = random.choice(Responses.INFORM_TIL_TITLE_FOLLOWUP)
        elif handler_selection_features.speech_action_name == SpeechActionNames.NEGATIVE_USER_SATISFACTION:
            text = self._get_text_from_navigation(handler_selection_features.state_manager)
            feeling = ''
            try:
                feeling = re.findall(r'\b(confusing|awful|disgusting|horrible|terrible|scary|upset|sorry|bad|sad|insane|boring|garbage)\b', text)[0]
                response_number = random.randint(0, 1)
                if response_number == 0:
                    response = random.choice(Responses.APOLOGY_FEELING) + feeling + '. ' + confirm_resume_topic
                else:
                    response = random.choice(Responses.APOLOGY_NO_FEELING) + '. ' + confirm_resume_topic
            except IndexError:
                if re.match("|".join([r'\bnot\b(.*)\b(nice|great|good|interesting|cool|interesting|amusing|surprising|fascinating|awesome|exciting|hilarious|perfect|glad|happy|surprise)\b',
                                        r'\bI hate\b',
                                        r'\b(that\'s|that|this|it\'s|it)\b(.*)\b(mean)\b',
                                        r'\bI (don\'t|do not)\b.*(like)',
                                        r'\bI\'m not a fan\b']),
                                        text) is not None:
                    response = random.choice(Responses.APOLOGY_NO_FEELING) + confirm_resume_topic
                elif re.match("|".join([r'\bthat can\'t be true\b',
                                        r'\bthat (doesn\'t|does not) sound right\b',
                                        r'\b(that\'s|that is)\b.*\b(incorrect|fake|wrong|a lie|lie)\b',
                                        r'\bI doubt it\b',
                                        r'^(really|what)$',
                                        r'\bis that (real|true)\b',
                                        r'\bare you sure\b']),
                                        text) is not None:
                    response = random.choice(Responses.APOLOGY_CHALLENGE) + confirm_resume_topic
                elif re.match(r'\b(you|you\'ve)\b.*\b(told|said|asked) (me|that|this)\b', 
                              text) is not None:
                    response = random.choice(Responses.APOLOGY_DUPLICATED_CONTENT) + confirm_resume_topic
                elif re.match(r'\bI\b.*\b(told|said|tell) you\b',
                              text) is not None:
                    response = random.choice(Responses.APOLOGY_DUPLICATED_QUESTION) + confirm_resume_topic
                elif re.match("|".join([r'\bI didn\'t get it\b',
                                        r'\bI don\'t understand\b',
                                        r'\byou puzzle me\b']),
                                        text) is not None:
                    response = random.choice(Responses.APOLOGY_CONFUSING) + confirm_resume_topic
            reprompt = confirm_resume_topic
        response_and_reprompt = {
            'response': response,
            'reprompt': reprompt
        }
        return response_and_reprompt