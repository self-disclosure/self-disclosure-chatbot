from miniskills.constants.action_names import ActionNames
from miniskills.constants.intent_names import IntentNames
from miniskills.constants.responses import Responses
from miniskills.handler_dispatch_generic_response_generator.handler import Handler
import random

class PullDifferentTopicInThreadNlgHandler(Handler):
    """
    NLG template for pull different topic in thread
    """
    def can_handle(self, handler_selection_features):
        return handler_selection_features.action_name == ActionNames.PULL_DIFFERENT_TOPIC_IN_THREAD and handler_selection_features.nlg_content is not None

    def nlg_template(self, handler_selection_features, previously_mentioned):
        prompt = random.choice(Responses.ARE_YOU_FAMILIAR_WITH) + handler_selection_features.nlg_content
        if handler_selection_features.state_manager.current_state.corrected_ask_intent == IntentNames.request_news_intent:
            if handler_selection_features.state_manager.current_state.saved_intent == IntentNames.request_fact_intent:
                prompt = random.choice(Responses.DO_YOU_WANT) + random.choice(Responses.HEAR) + random.choice(Responses.INTERESTING_FACT) + handler_selection_features.nlg_content
            elif handler_selection_features.state_manager.current_state.saved_intent == IntentNames.request_opinion_intent:
                prompt = random.choice(Responses.CONFIRM_INFORM_SHOWER_THOUGHTS) + handler_selection_features.nlg_content
        response_and_reprompt = {
            'response': random.choice(Responses.LET_ME_SEE) + previously_mentioned + prompt,
            'reprompt': prompt
        }
        return response_and_reprompt