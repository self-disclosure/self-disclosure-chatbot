from miniskills.constants.action_names import ActionNames
from miniskills.constants.combined_responses import CombinedResponses
from miniskills.constants.responses import Responses
from miniskills.constants.speech_action_names import SpeechActionNames
from miniskills.handler_dispatch_generic_response_generator.handler import Handler
import random

class SwitchTopicNlgHandler(Handler):
    """
    NLG template for switching topic
    """
    def can_handle(self, handler_selection_features):
        return handler_selection_features.action_name == ActionNames.SWITCH_TOPIC and handler_selection_features.nlg_content is not None

    def nlg_template(self, handler_selection_features, topic_exhausted_response, confirm_inform_response):
        combined_responses = CombinedResponses()
        response = None
        reprompt = None
        if handler_selection_features.speech_action_name == SpeechActionNames.TOPIC_EXHAUSTED:
            topic = handler_selection_features.state_manager.current_state.corrected_topic
            if topic is None:
                topic = handler_selection_features.state_manager.last_state.get('old_topic')
            response = combined_responses.topic_exhausted(topic_exhausted_response, topic, confirm_inform_response) + random.choice(Responses.INSTEAD)
            reprompt = random.choice(Responses.INSTRUCT_YES)
        elif handler_selection_features.speech_action_name == SpeechActionNames.INAPPROPRIATE_TOPIC:
            response = combined_responses.not_familiar_topic(None) + confirm_inform_response + random.choice(Responses.INSTEAD)
            reprompt = random.choice(Responses.INSTRUCT_YES)
        elif handler_selection_features.speech_action_name == SpeechActionNames.NO_MORE_KNOWN:
            response = topic_exhausted_response + ' that. ' + confirm_inform_response
            reprompt = confirm_inform_response
        elif handler_selection_features.speech_action_name == SpeechActionNames.STRONG_NEGATIVE_USER_SATISFACTION:
            text = self._get_text_from_navigation(handler_selection_features.state_manager)
            response = random.choice(Responses.APOLOGY_SKIP) + confirm_inform_response
            reprompt = confirm_inform_response
            # TODO: add "OTHER"
        elif handler_selection_features.speech_action_name == SpeechActionNames.REJECT or handler_selection_features.speech_action_name == SpeechActionNames.UNSURE:
            response = random.choice(Responses.ALRIGHT) + confirm_inform_response + random.choice(Responses.INSTEAD)
            reprompt = confirm_inform_response
        elif handler_selection_features.speech_action_name == SpeechActionNames.DISAGREE:
            response = random.choice(Responses.DISAGREE) + confirm_inform_response
            reprompt = confirm_inform_reprompt
            
        response_and_reprompt = {
            'response': response,
            'reprompt': reprompt
        }
        return response_and_reprompt