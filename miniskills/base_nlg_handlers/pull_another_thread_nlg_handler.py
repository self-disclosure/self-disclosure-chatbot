from miniskills.constants.action_names import ActionNames
from miniskills.constants.responses import Responses
from miniskills.constants.speech_action_names import SpeechActionNames
from miniskills.handler_dispatch_generic_response_generator.handler import Handler
import random
import re

class PullAnotherThreadNlgHandler(Handler):
    """
    NLG template for pull another thread
    """
    def can_handle(self, handler_selection_features):
        return handler_selection_features.action_name == ActionNames.PULL_ANOTHER_THREAD and handler_selection_features.nlg_content is not None

    def nlg_template(self, handler_selection_features, continue_inform_topic, inform_followup, did_you_know_response, confirm_resume_topic):
        response = None
        reprompt = None
        if handler_selection_features.speech_action_name == SpeechActionNames.AGREE:
            response = random.choice(Responses.AGREE) + '. ' + did_you_know_response
            reprompt = inform_followup
        elif handler_selection_features.speech_action_name == SpeechActionNames.CONTINUE:
            response = continue_inform_topic
            reprompt = inform_followup
        elif handler_selection_features.speech_action_name == SpeechActionNames.POSITIVE_USER_SATISFACTION:
            text = self._get_text_from_navigation(handler_selection_features.state_manager)
            feeling = ''
            try:
                feeling = re.findall(r'(cool|informative|interesting|funny|amusing|surprising|fascinating|awesome|exciting|hilarious)', text)[0]
                response = random.choice(Responses.GLAD_FEELING) + feeling + '. ' + did_you_know_response
                reprompt = random.choice(Responses.INSTRUCT_YES)
            except IndexError:
                response = random.choice(Responses.GLAD_NO_FEELING) + '. ' + did_you_know_response
                reprompt = random.choice(Responses.INSTRUCT_YES)
        elif handler_selection_features.speech_action_name == SpeechActionNames.NOT_KNOWN:
            response = random.choice(Responses.BY_THE_WAY) + did_you_know_response
            reprompt = inform_followup
        elif handler_selection_features.speech_action_name == SpeechActionNames.UNSURE_SIDE:
            response = random.choice(Responses.UNSURE) + '. ' + confirm_resume_topic
            reprompt = confirm_resume_topic
        else:
            response = did_you_know_response
            reprompt = inform_followup
        response_and_reprompt = {
            'response': response,
            'reprompt': reprompt
        }
        return response_and_reprompt