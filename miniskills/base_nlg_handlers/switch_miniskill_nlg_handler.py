from miniskills.constants.action_names import ActionNames
from miniskills.constants.combined_responses import CombinedResponses
from miniskills.constants.intent_names import IntentNames
from miniskills.constants.responses import Responses
from miniskills.handler_dispatch_generic_response_generator.handler import Handler
import random

class SwitchMiniskillNlgHandler(Handler):
    """
    NLG template for switch miniskill
    """
    def can_handle(self, handler_selection_features):
        return handler_selection_features.action_name == ActionNames.SWITCH_MINISKILL and handler_selection_features.nlg_content is not None

    def nlg_template(self, topic_exhausted_response, topic, confirm_inform_response):
        combined_responses = CombinedResponses()
        response_and_reprompt = {
            'response': combined_responses.topic_exhausted(topic_exhausted_response, topic, confirm_inform_response),
            'reprompt': confirm_inform_response
        }
        return response_and_reprompt

    def last_miniskill_topic_exhausted_response(self, handler_selection_features):
        self._copy_previous_state_new_attributes_to_current_state(handler_selection_features.state_manager)
        handler_selection_features.state_manager.current_state.require_confirmation = True
        response = ''
        if handler_selection_features.state_manager.current_state.intent == IntentNames.request_fact_intent:
            response = random.choice(Responses.TOPIC_EXHAUSTED)
        elif handler_selection_features.state_manager.current_state.intent == IntentNames.request_opinion_intent:
            response = random.choice(Responses.SHOWER_THOUGHTS_TOPIC_EXHAUSTED)
        elif handler_selection_features.state_manager.current_state.intent == IntentNames.request_news_intent:
            response = random.choice(Responses.NEWS_TOPIC_EXHAUSTED)
        elif handler_selection_features.state_manager.current_state.intent == IntentNames.request_movie_intent:
            response = random.choice(Responses.MOVIE_EXHAUSTED)
        return response