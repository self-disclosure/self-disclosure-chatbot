from miniskills.global_handlers.global_invalid_handler import GlobalInvalidHandler
from miniskills.handler_dispatch_generic_response_generator.handler_dispatch_response_generator import HandlerDispatchResponseGenerator

class GlobalInvalidResponseGenerator(HandlerDispatchResponseGenerator):
    """
    Deal with invalid responses
    """

    list_of_handlers = [GlobalInvalidHandler()]

    def __init__(self, state_manager, module_name, service_module_config, handler_dispatcher = None, list_of_handlers = None):
        super(GlobalInvalidResponseGenerator, self).__init__(state_manager, module_name, service_module_config, handler_dispatcher, self.list_of_handlers)