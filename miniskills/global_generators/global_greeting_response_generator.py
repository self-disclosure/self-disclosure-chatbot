from cobot_core.service_module import LocalServiceModule
from miniskills.constants.greeting_inputs import GreetingInputs
from miniskills.constants.greeting_responses import GreetingResponses
from miniskills.constants.responses import Responses
import random
import re

class GlobalGreetingResponseGenerator(LocalServiceModule):
    """
    Deal with greeting intent
    """
    NOT = 'not'
    
    def execute(self):
        user_feeling = 0
        text = None
        if self.state_manager.current_state.slots is not None:
            if self.state_manager.current_state.slots.get('navigation') is not None:
                if self.state_manager.current_state.slots['navigation'].get('value') is not None:
                    text = self.state_manager.current_state.slots['navigation']['value']
            else:
                text = self.state_manager.current_state.corrected_topic
        response = ''
        text = str(text)

        # check if user asks for reciprocation
        needs_reciprocation = False
        if re.search("|".join([r'(?:how|what)\s+(?:are|a?bout)\s+you',
                              r'(?:what|how)\s*(?:\'s|is|s|are)?\s+(?:it|\w*thin\w*)?\s*(?:up|new|good|goi\w+)?']),
                              text) is not None:
            needs_reciprocation = True

        has_not = False
        for word in text.split():
            # if the utterance has not, it negates the feeling
            if word == self.NOT:
                has_not = True

            # check what feeling the word is
            if word in GreetingInputs.GOOD:
                if has_not:
                    user_feeling -= 1
                else:
                    user_feeling += 1
            elif word in GreetingInputs.BAD:
                if has_not:
                    user_feeling += 1
                else:
                    user_feeling -= 1
        
        if user_feeling > 0:
            response = random.choice(GreetingResponses.RESPONSE_GOOD)
            if needs_reciprocation:
                response += random.choice(GreetingResponses.RECIPROCATE_HAPPY)
        elif user_feeling < 0:
            response = random.choice(GreetingResponses.RESPONSE_BAD)
            if needs_reciprocation:
                response += random.choice(GreetingResponses.RECIPROCATE_MILD)
        response = response + random.choice(Responses.MINISKILL_OPTIONS) + random.choice(Responses.LAUNCH)

        self.state_manager.current_state.reprompt = Responses.LAUNCH[0]
        response_dict = {
            'response': response
        }
        
        return response_dict