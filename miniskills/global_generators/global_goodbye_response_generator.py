from cobot_core.service_module import LocalServiceModule
from miniskills.constants.responses import Responses

class GlobalGoodbyeResponseGenerator(LocalServiceModule):
    """
    Deal with goodbye intent
    """
    
    def execute(self):
        response = Responses.GOODBYE_PROMPT[0]
        self.state_manager.current_state.reprompt = response
        response_dict = {
            'response': response,
        }
        return response_dict