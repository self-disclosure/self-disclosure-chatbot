from cobot_core.service_module import LocalServiceModule
from miniskills.constants.intent_names import IntentNames
from miniskills.constants.responses import Responses
import random

class GlobalLaunchResponseGenerator(LocalServiceModule):
    """
    Deal with launch intent
    """
    
    def execute(self):
        response = None
        if hasattr(self.state_manager.current_state, 'intent'):
            if self.state_manager.current_state.intent == IntentNames.repeat_intent:
                response = self.state_manager.last_state['response']
            else:
                response = random.choice(Responses.GREETING)
        else:
            response = Responses.LAUNCH[0]
        self.state_manager.current_state.reprompt = response
        response_dict = {
            'response': response,
        }
        return response_dict