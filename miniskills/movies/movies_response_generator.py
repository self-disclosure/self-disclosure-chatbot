from miniskills.base_handlers.no_dynamo_db_pull_handler import NoDynamoDbPullHandler
from miniskills.movies.custom_base_handlers.movies_pull_another_thread_handler import MoviesPullAnotherThreadHandler
from miniskills.base_handlers.pull_different_topic_in_thread_handler import PullDifferentTopicInThreadHandler
from miniskills.movies.custom_base_handlers.movies_pull_thread_for_topic_handler import MoviesPullThreadForTopicHandler
from miniskills.base_handlers.switch_miniskill_handler import SwitchMiniskillHandler
from miniskills.base_handlers.switch_topic_handler import SwitchTopicHandler
from miniskills.global_handlers.global_navigational_handler import GlobalNavigationalHandler
from miniskills.handler_dispatch_generic_response_generator.handler_dispatch_response_generator import HandlerDispatchResponseGenerator
from miniskills.movies.movies_nlg_handlers import MoviesNoDynamoDbPullNlgHandler, MoviesSwitchMiniskillNlgHandler, MoviesPullThreadForTopicNlgHandler, MoviesSwitchTopicNlgHandler, MoviesPullAnotherThreadNlgHandler, MoviesPullDifferentTopicInThreadNlgHandler

class MoviesResponseGenerator(HandlerDispatchResponseGenerator):
    """
    Initialize list of possible handlers, run dispatch, return a response for Movies
    """

    list_of_handlers = [NoDynamoDbPullHandler(), MoviesPullAnotherThreadHandler(), PullDifferentTopicInThreadHandler(), MoviesPullThreadForTopicHandler(), SwitchMiniskillHandler(), SwitchTopicHandler(), MoviesNoDynamoDbPullNlgHandler(), MoviesSwitchMiniskillNlgHandler(), MoviesPullThreadForTopicNlgHandler(), MoviesSwitchTopicNlgHandler(), MoviesPullAnotherThreadNlgHandler(), MoviesPullDifferentTopicInThreadNlgHandler(), GlobalNavigationalHandler()]

    def __init__(self, state_manager, module_name, service_module_config, handler_dispatcher = None, list_of_handlers = None):
        super(MoviesResponseGenerator, self).__init__(state_manager, module_name, service_module_config, handler_dispatcher, self.list_of_handlers)