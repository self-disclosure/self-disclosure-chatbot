from miniskills.base_handlers.pull_another_thread_handler import PullAnotherThreadHandler
from miniskills.constants.action_names import ActionNames
from miniskills.constants.speech_action_names import SpeechActionNames
from miniskills.constants.user_response_information_names import UserResponseInformationNames

class MoviesPullAnotherThreadHandler(PullAnotherThreadHandler):
    """
    Basic template to handle pulling another thread for the same topic dynamodb pulls, will reroute to PullThreadForTopicHandler
    """
    def dynamo_db_pull(self, handler_selection_features):
        # reroute through this handler
        handler_selection_features.action_name = ActionNames.PULL_THREAD_FOR_TOPIC
        handler_selection_features.speech_action_name = None