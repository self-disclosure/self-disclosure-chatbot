from miniskills.base_handlers.pull_thread_for_topic_handler import PullThreadForTopicHandler
from miniskills.constants.action_names import ActionNames
from miniskills.constants.intent_names import IntentNames
from miniskills.constants.user_response_information_names import UserResponseInformationNames
from miniskills.constants.responses import Responses
from miniskills.constants.speech_action_names import SpeechActionNames
from miniskills.miniskill_dynamo_db_manager import MiniskillDynamoDbManager
import ast
import random
import json

class MoviesPullThreadForTopicHandler(PullThreadForTopicHandler):
    """
    Movie template to pull thread for a topic
    """
    BLANK = '_'

    def _matches_response(self, last_state_response, possible_response_list):
        for response in possible_response_list:
            response = response.strip()
            if last_state_response.find(response) >= 0:
                return True
        return False

    # turn 1: year released + genre
    def _year_and_genre(self, handler_selection_features, movie_information):
        year_released = movie_information['year']
        genre = json.loads(movie_information['genres'])[0]
        return [year_released, genre]

    # functions to determine which turn it is followed by function to pull the data
    # turn 2: director + imdb rating
    def _is_director_and_imdb_rating(self, last_state_response):
        return self._matches_response(last_state_response, Responses.INFORM_MOVIE_NAME)

    def _director_and_imdb_rating(self, handler_selection_features, movie_information):
        if handler_selection_features.state_manager.current_state.intent == IntentNames.amazon_yes_intent:
            return None
        else:
            # pull director + imdb rating
            corrected_director_list = []
            director_list = json.loads(movie_information['directors'])
            for director in director_list:
                corrected_director_list.append(director['directorName'])
            imdb_rating = movie_information['imdbRating']
            return [corrected_director_list, imdb_rating]

    # turn 3: plot
    def _is_plot(self, last_state_response):
        return self._matches_response(last_state_response, Responses.CONFIRM_INFORM_MOVIE_PLOT)

    def _plot(self, handler_selection_features, movie_information):
        if handler_selection_features.state_manager.current_state.intent == IntentNames.amazon_yes_intent:
            plot = movie_information['plot']
            if plot == BLANK:
                return None
            else:
                return plot
        else:
            return None

    # turn 4: critical reception
    def _is_critical_reception(self, last_state_response):
        return self._matches_response(last_state_response, Responses.CONFIRM_INFORM_MOVIE_REVIEW)

    def _critical_reception(self, handler_selection_features, movie_information):
        if handler_selection_features.state_manager.current_state.intent == IntentNames.amazon_yes_intent:
            critical_reception = movie_information['criticalReception']
            if critical_reception == self.BLANK:
                return None
            else:
                return critical_reception
        else:
            return None

    # ask if the user wants to talk about the director or one of the cast
    def _director_or_cast(self, handler_selection_features, movie_information):
        possible_directors_and_cast = []
        for director_data in json.loads(movie_information['directors']):
            director_name_and_movie = {}
            director_name_and_movie['name'] = director_data['directorName']
            director_name_and_movie['movie'] = None
            if director_data['movieList']:
                director_name_and_movie['movie'] = random.choice(director_data['movieList'])
            director_name_and_movie['type'] = 'director'
            possible_directors_and_cast.append(director_name_and_movie)
        for cast_data in json.loads(movie_information['cast']):
            cast_name_and_movie = {}
            cast_name_and_movie['name'] = cast_data['actorName']
            cast_name_and_movie['movie'] = None
            if cast_data['movieList']:
                cast_name_and_movie['movie'] = random.choice(cast_data['movieList'])
            cast_name_and_movie['type'] = 'cast'
            possible_directors_and_cast.append(cast_name_and_movie)
        return random.choice(possible_directors_and_cast)

    def _pull_data(self, handler_selection_features, thread_id, topic):
        if handler_selection_features.state_manager.current_state.turn_number == 5:
            data = {
                'turn_number': handler_selection_features.state_manager.current_state.turn_number
            }
            return data
        else:
            miniskill_dynamo_db_manager = MiniskillDynamoDbManager()
            turn_number = int(handler_selection_features.state_manager.last_state.get('next_turn_number')) if handler_selection_features.state_manager.last_state.get('next_turn_number') is not None else None
            
            movie_information = miniskill_dynamo_db_manager.pull_all_information(handler_selection_features.thread_table_name, thread_id)
            if turn_number:
                movie_information = miniskill_dynamo_db_manager.pull_all_information(handler_selection_features.thread_table_name, handler_selection_features.state_manager.last_state.get('visited_threads')[0])

            last_state_response = handler_selection_features.state_manager.last_state.get('response')
            thread_data = None
            next_turn_number = turn_number
            additional_information = None
            
            # there are four turns in this miniskill, turns 3 and 4 can be skipped
            # turn 2: director + imdb rating
            # turn 3: plot (could be empty)
            # turn 4: review (could be empty)
            miniskill_turns_two_to_four = [self._is_director_and_imdb_rating(last_state_response),
                                        self._is_plot(last_state_response),
                                        self._is_critical_reception(last_state_response)]
            for index, is_turn in enumerate(miniskill_turns_two_to_four, start=2):
                if is_turn:
                    if next_turn_number == 2:
                        thread_data = self._director_and_imdb_rating(handler_selection_features, movie_information)
                        if thread_data is None:
                            additional_information = self._director_or_cast(handler_selection_features, movie_information)
                            turn_number = 3
                        else:
                            if self._plot(handler_selection_features, movie_information) is None:
                                additional_information = self._director_or_cast(handler_selection_features, movie_information)
                                if self._critical_reception(handler_selection_features, movie_information) is None:
                                    next_turn_number = 5
                                else:
                                    next_turn_number = 4
                            else:
                                next_turn_number = 3
                    elif next_turn_number == 3:
                        thread_data = self._plot(handler_selection_features, movie_information)
                        if thread_data is None:
                            next_turn_number = 4
                        if self._critical_reception(handler_selection_features, movie_information) is None:
                            additional_information = self._director_or_cast(handler_selection_features, movie_information)
                        else:
                            next_turn_number = 4
                    else:
                        thread_data = self._critical_reception(handler_selection_features, movie_information)
                        if thread_data is None:
                            turn_number = 5
                        else:
                            additional_information = self._director_or_cast(handler_selection_features, movie_information)

            # turn 1: year released + genre; anything that doesn't fall under turns 2-4 is turn 1
            if next_turn_number is None:
                turn_number = 1
                thread_data = self._year_and_genre(handler_selection_features, movie_information)
                next_turn_number = 2
                if topic == ":frontpage":
                    topic = None
                handler_selection_features.state_manager.current_state.old_topic = movie_information["primaryTopic"]

            handler_selection_features.state_manager.current_state.turn_number = str(turn_number)
            handler_selection_features.state_manager.current_state.next_turn_number = str(next_turn_number)
            handler_selection_features.state_manager.current_state.saved_intent = IntentNames.request_movie_intent
            try:
                handler_selection_features.state_manager.current_state.old_topic = additional_information['name']
            except (KeyError, TypeError):
                pass

            data = {
                'thread_data': thread_data,
                'topic': topic,
                'thread_id': thread_id,
                'turn_number': turn_number,
                'next_turn_number': next_turn_number,
                'additional_information': additional_information
            }
            return data

    def _set_require_confirmation(self, handler_selection_features):
        handler_selection_features.state_manager.current_state.require_confirmation = True

    def _set_topics_within_a_thread(self, handler_selection_features, thread_id):
        pass

    def dynamo_db_pull(self, handler_selection_features):
        self._copy_previous_state_new_attributes_to_current_state(handler_selection_features.state_manager)
        miniskill_dynamo_db_manager = MiniskillDynamoDbManager()

        topic = None

        # NLU parses first letter of proper nouns to capital letter but database is all lowercase
        if handler_selection_features.state_manager.current_state.corrected_topic is not None:
            topic = handler_selection_features.state_manager.current_state.corrected_topic.lower()
        elif handler_selection_features.state_manager.current_state.new_topic is not None:
            topic = handler_selection_features.state_manager.current_state.new_topic.lower()
        else:
            topic = handler_selection_features.state_manager.current_state.old_topic.lower()

        # check if topic has threads
        if len(miniskill_dynamo_db_manager.pull_threads_for_topic(handler_selection_features.topic_table_name, topic)) < 1 and not handler_selection_features.state_manager.current_state.turn_number:
            handler_selection_features.speech_action_name = SpeechActionNames.TOPIC_EXHAUSTED
            handler_selection_features.action_name = ActionNames.SWITCH_TOPIC
            return None

        thread_id = handler_selection_features.state_manager.last_state.get('visited_threads')[0] if handler_selection_features.state_manager.last_state.get('visited_threads') is not None else None
        turn_number = handler_selection_features.state_manager.last_state.get('turn_number')

        if not turn_number:
            if not thread_id and not topic:
                topic = ':frontpage'

            # pull thread id and list of threads for topic
            thread_information = miniskill_dynamo_db_manager.pull_threads_for_topic(handler_selection_features.topic_table_name, topic)
            thread_id = thread_information['thread_id']

            if not handler_selection_features.state_manager.current_state.visited_threads:
                handler_selection_features.state_manager.current_state.visited_threads = [thread_id]
            else:
                handler_selection_features.state_manager.current_state.visited_threads.append(thread_id)

            handler_selection_features.nlg_content = self._pull_data(handler_selection_features, thread_id, topic)
        else:
            handler_selection_features.nlg_content = self._pull_data(handler_selection_features, thread_id, topic)