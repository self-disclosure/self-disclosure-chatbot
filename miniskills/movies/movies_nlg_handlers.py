from miniskills.constants.action_names import ActionNames
from miniskills.constants.intent_names import IntentNames
from miniskills.constants.responses import Responses
from miniskills.base_nlg_handlers.no_dynamo_db_pull_nlg_handler import NoDynamoDbPullNlgHandler
from miniskills.base_nlg_handlers.pull_another_thread_nlg_handler import PullAnotherThreadNlgHandler
from miniskills.base_nlg_handlers.pull_different_topic_in_thread_nlg_handler import PullDifferentTopicInThreadNlgHandler
from miniskills.base_nlg_handlers.pull_thread_for_topic_nlg_handler import PullThreadForTopicNlgHandler
from miniskills.base_nlg_handlers.switch_miniskill_nlg_handler import SwitchMiniskillNlgHandler
from miniskills.base_nlg_handlers.switch_topic_nlg_handler import SwitchTopicNlgHandler
import random

class MoviesNoDynamoDbPullNlgHandler(NoDynamoDbPullNlgHandler):
    """
    Handle no dynamodb pulls for Movies
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        confirm_resume_topic = random.choice(Responses.CONFIRM_RESUME_MOVIE) + handler_selection_features.state_manager.current_state.old_topic + '?'
        return self.nlg_template(handler_selection_features, confirm_resume_topic)

class MoviesPullAnotherThreadNlgHandler(PullAnotherThreadNlgHandler):
    """
    Handle pulling another thread for Movies
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        continue_inform_topic = random.choice(Responses.SURE) + random.choice(Responses.INFORM_MOVIE_NAME) + handler_selection_features.nlg_content
        inform_followup = random.choice(Responses.INFORM_MOVIE_FOLLOWUP)
        did_you_know_response = random.choice(Responses.INFORM_MOVIE_NAME) + handler_selection_features.nlg_content
        confirm_resume_topic = random.choice(Responses.CONFIRM_RESUME_MOVIE) + handler_selection_features.state_manager.current_state.old_topic + '?'
        return self.nlg_template(handler_selection_features, continue_inform_topic, inform_followup, did_you_know_response, confirm_resume_topic)

class MoviesPullDifferentTopicInThreadNlgHandler(PullDifferentTopicInThreadNlgHandler):
    """
    Handle pulling topic in the same thread for Movies
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        return self.nlg_template(handler_selection_features, '')

class MoviesPullThreadForTopicNlgHandler(PullThreadForTopicNlgHandler):
    """
    Handle pulling thread for topic for Movies
    """
    def additional_information(self, handler_selection_features, response):
        new_response = response + random.choice(Responses.LET_ME_SEE) + random.choice(Responses.ARE_YOU_FAMILIAR_WITH) + handler_selection_features.nlg_content['additional_information']['name'] + '?'
        if handler_selection_features.nlg_content['additional_information']['movie']:
            additional_information = handler_selection_features.nlg_content['additional_information']['movie']
            if isinstance(additional_information, dict):
                additional_information = additional_information['movieName']
            if handler_selection_features.nlg_content['additional_information']['type'] == 'director':
                new_response = new_response + random.choice(Responses.THEY_DIRECTED) + additional_information
            else:
                new_response = new_response + random.choice(Responses.THEY_STARRED_IN) + additional_information
        else:
            new_response = new_response + random.choice(Responses.THEY_WERE_A) + handler_selection_features.nlg_content['additional_information']['type']
        reprompt = random.choice(Responses.ARE_YOU_FAMILIAR_WITH) + 'them? '
        return new_response, reprompt

    def evi_information(self, handler_selection_features):
        response = None
        reprompt = random.choice(Responses.WHAT_TO_TALK_ABOUT_NEXT)
        handler_selection_features.state_manager.current_state.turn_number = '5'
        evi_info = handler_selection_features.state_manager.current_state.features['evi']['old_topic']
        if evi_info == handler_selection_features.state_manager.last_state.get('response') or handler_selection_features.state_manager.current_state.intent == IntentNames.amazon_yes_intent:
            response = random.choice(Responses.TOPIC_EXHAUSTED) + handler_selection_features.state_manager.current_state.old_topic + random.choice(Responses.WHAT_TO_TALK_ABOUT_NEXT)
        elif evi_info and handler_selection_features.state_manager.current_state.intent == IntentNames.amazon_no_intent:
            response = evi_info
        else:
            response = random.choice(Responses.NOT_FAMILIAR_TOPIC) + handler_selection_features.state_manager.current_state.old_topic + random.choice(Responses.WHAT_TO_TALK_ABOUT_NEXT)
        return response, reprompt

    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string for all 5 steps; doesn't use nlg_template
        """
        try:
            response = None
            reprompt = None
            if handler_selection_features.nlg_content['turn_number'] == 1:
                if handler_selection_features.nlg_content['topic']:
                    random_number = random.randint(0, 1)
                    if random_number == 0:
                        response = random.choice(Responses.INFORM_MOVIE_TOPIC) + handler_selection_features.state_manager.current_state.old_topic + '.'
                    else:
                        response = handler_selection_features.state_manager.current_state.old_topic + random.choice(Responses.QUESTION_MARK) + random.choice(Responses.SURE)
                    response += random.choice(Responses.INFORM_MOVIE_NO_NAME)
                else:
                    response = random.choice(Responses.SURE) + random.choice(Responses.INFORM_MOVIE_NAME) + handler_selection_features.state_manager.current_state.old_topic + '?'
                response = response + random.choice(Responses.YEAR_RELEASED) + handler_selection_features.nlg_content['thread_data'][0] + '. ' + random.choice(Responses.TYPE_OF_MOVIE) + handler_selection_features.nlg_content['thread_data'][1] + random.choice(Responses.MOVIE) + '.'
                reprompt = random.choice(Responses.INFORM_MOVIE_FOLLOWUP)
            elif handler_selection_features.nlg_content['turn_number'] == 2:
                directors = handler_selection_features.nlg_content['thread_data'][0]
                random_number = random.randint(0, 1)
                if len(directors) > 1:
                    if random_number == 0:
                        response = random.choice(Responses.INFORM_MOVIE_CO_DIRECTOR_PREFIX) + directors[0] + ' and ' + directors[1] + '.'
                    else:
                        response = directors[0] + ' and ' + directors[1] + random.choice(Responses.INFORM_MOVIE_CO_DIRECTOR_SUFFIX)
                else:
                    if random_number == 0:
                        response = random.choice(Responses.INFORM_MOVIE_DIRECTOR_PREFIX) + directors[0] + '.'
                    else:
                        response = directors[0] + random.choice(Responses.INFORM_MOVIE_DIRECTOR_SUFFIX)
                response = response + random.choice(Responses.INFORM_IMDB_RATING_PREFIX) + handler_selection_features.nlg_content['thread_data'][1] + random.choice(Responses.INFORM_IMDB_RATING_SUFFIX)
                rating = float(handler_selection_features.nlg_content['thread_data'][1])
                if rating >= 8:
                    response += random.choice(Responses.INFORM_IMDB_RATING_GOOD)
                elif rating >= 6:
                    response += random.choice(Responses.INFORM_IMDB_RATING_OK)
                elif rating >= 3:
                    response += random.choice(Responses.INFORM_IMDB_RATING_BAD)
                else:
                    response += random.choice(Responses.INFORM_IMDB_RATING_REALLY_BAD)

                if handler_selection_features.nlg_content['next_turn_number'] == 3:
                    response += random.choice(Responses.CONFIRM_INFORM_MOVIE_PLOT)
                    reprompt = random.choice(Responses.CONFIRM_INFORM_MOVIE_PLOT)
                elif handler_selection_features.nlg_content['next_turn_number'] == 4:
                    response += random.choice(Responses.CONFIRM_INFORM_MOVIE_REVIEW)
                    reprompt = random.choice(Responses.CONFIRM_INFORM_MOVIE_REVIEW)
                else:
                    response, reprompt = self.additional_information(handler_selection_features, response)
            elif handler_selection_features.nlg_content['turn_number'] == 3:
                response = ""
                if handler_selection_features.nlg_content['thread_data']:
                    response = random.choice(Responses.INFORM_MOVIE_PLOT) + handler_selection_features.nlg_content['thread_data']
                if handler_selection_features.nlg_content['next_turn_number'] == 4:
                    response += random.choice(Responses.CONFIRM_INFORM_MOVIE_REVIEW)
                    reprompt = random.choice(Responses.CONFIRM_INFORM_MOVIE_REVIEW)
                else:
                    response, reprompt = self.additional_information(handler_selection_features, response)
            elif handler_selection_features.nlg_content['turn_number'] == 4:
                response = handler_selection_features.nlg_content['thread_data'] + random.choice(Responses.WHAT_TO_TALK_ABOUT_NEXT)
                reprompt = random.choice(Responses.WHAT_TO_TALK_ABOUT_NEXT)
            elif handler_selection_features.nlg_content['turn_number'] == 5:
                response, reprompt = self.evi_information(handler_selection_features)
            else:
                response, reprompt = self.additional_information(handler_selection_features, response)
            handler_selection_features.state_manager.current_state.reprompt = reprompt
            response_dict = {
                'response': response,
                'reprompt': reprompt
            }
            return response_dict
        except TypeError:
            response, reprompt = self.evi_information(handler_selection_features)
            response_dict = {
                'response': response,
                'reprompt': reprompt
            }
            return response_dict

class MoviesSwitchMiniskillNlgHandler(SwitchMiniskillNlgHandler):
    """
    Handle switching miniskill dynamodb pulls for switching to Movies miniskill
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        topic_exhausted_response = self.last_miniskill_topic_exhausted_response(handler_selection_features)
        topic = handler_selection_features.state_manager.current_state.corrected_topic
        handler_selection_features.state_manager.current_state.old_topic = topic
        confirm_inform_response = random.choice(Responses.CONFIRM_INFORM_MOVIE) + topic
        return self.nlg_template(topic_exhausted_response, topic, confirm_inform_response)

class MoviesSwitchTopicNlgHandler(SwitchTopicNlgHandler):
    """
    Handle switching topic dynamodb pulls for switching to Movies miniskill
    """
    def handle(self, handler_dispatcher, handler_selection_features):
        """
        Return a completed response, input HandlerSelectionFeatures object and output response string
        """
        topic_exhausted_response = random.choice(Responses.MOVIE_EXHAUSTED)
        confirm_inform_response = random.choice(Responses.CONFIRM_INFORM_MOVIE) + handler_selection_features.nlg_content
        return self.nlg_template(handler_selection_features, topic_exhausted_response, confirm_inform_response)