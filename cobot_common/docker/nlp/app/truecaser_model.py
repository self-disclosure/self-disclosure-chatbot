from Truecaser import *
import _pickle as cPickle
import nltk
import string


class truecaser_model:

    do_log = False

    def load_model(self):
        f = open('english_distributions.obj', 'rb')
        self.uniDist = cPickle.load(f)
        self.backwardBiDist = cPickle.load(f)
        self.forwardBiDist = cPickle.load(f)
        self.trigramDist = cPickle.load(f)
        self.wordCasingLookup = cPickle.load(f)
        f.close()

    def run_model(self,inputs):
        wordCasingLookup=self.wordCasingLookup
        uniDist=self.uniDist
        backwardBiDist=self.backwardBiDist
        forwardBiDist=self.forwardBiDist 
        trigramDist=self.trigramDist

        correctTokens = 0
        totalTokens = 0
        results = []

        for sentence in inputs:
            tokensCorrect = nltk.word_tokenize(sentence)
            tokens = [token.lower() for token in tokensCorrect]
            tokensTrueCase = getTrueCase(tokens, 'title', wordCasingLookup, uniDist, backwardBiDist, forwardBiDist, trigramDist)
            result = ' '.join(tokensTrueCase)
            results.append(result)

            if self.do_log:
                perfectMatch = True

                for idx in range(len(tokensCorrect)):
                    totalTokens += 1
                    if tokensCorrect[idx] == tokensTrueCase[idx]:
                        correctTokens += 1
                    else:
                        perfectMatch = False
        
                if not perfectMatch:
                    print(tokensCorrect)
                    print(tokensTrueCase)
                    
                    print("-------------------")
    

        if self.do_log:
            print("Accuracy: %.2f%%" % (correctTokens / float(totalTokens)*100))
        return results
    
if __name__ == "__main__":       
    model = truecaser_model()
    model.load_model()
    results = model.run_model([ "alexa do you want to talk about donald trump" ])
    print(results[0])
