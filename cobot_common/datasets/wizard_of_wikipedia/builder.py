import os
import json

from cobot_common.datasets.builder import DatasetBuilder


class WowBuilder(DatasetBuilder):

    def __init__(self):
        self._name = 'wizard_of_wikipedia'

        super().__init__(self._name)

    def all(self):
        self._build_from_file(
            os.path.join(self.raw_data_dir(), 'data.json'),
            os.path.join(self.output_data_dir(), 'all'))

    def train(self):
        self._build_from_file(
            os.path.join(self.raw_data_dir(), 'train.json'),
            os.path.join(self.output_data_dir(), 'train'))

    def test_random(self):
        self._build_from_file(
            os.path.join(self.raw_data_dir(), 'test_random_split.json'),
            os.path.join(self.output_data_dir(), 'test_random'))

    def test_topic(self):
        self._build_from_file(
            os.path.join(self.raw_data_dir(), 'test_topic_split.json'),
            os.path.join(self.output_data_dir(), 'test_topic'))

    def valid_random(self):
        self._build_from_file(
            os.path.join(self.raw_data_dir(), 'valid_random_split.json'),
            os.path.join(self.output_data_dir(), 'valid_random'))

    def valid_topic(self):
        self._build_from_file(
            os.path.join(self.raw_data_dir(), 'valid_topic_split.json'),
            os.path.join(self.output_data_dir(), 'valid_topic'))

    def _build_from_file(self, file_path, output_dir):
        with open(file_path, 'r') as f:
            data = json.load(f)

        self.build_split(data, output_dir)

    def convert(self, dialog):
        turns = []
        for i in range(len(dialog.get('dialog'))):
            if i < len(dialog.get('dialog')) - 1:
                turns.append(self._convert_turn(dialog.get('dialog')[i], dialog.get('dialog')[i + 1]))
            else:
                turns.append(self._convert_turn(dialog.get('dialog')[i]))

        converted_dialog = {
            'dataset': self._name,
            'version': "1.0",
            'dialog_id': '',
            'annotation': {
                'wizard_eval': dialog.get('wizard_eval')
            },
            'metadata': {
                'persona': dialog.get('persona'),
                'chosen_topic': dialog.get('chosen_topic'),
                'chosen_topic_passage': dialog.get('chosen_topic_passage')
            },
            'turns': turns
        }
        return converted_dialog

    @staticmethod
    def _convert_turn(turn, next_turn=None):
        converted_turn = {
            'input': turn.get('text'),
            'response': next_turn.get('text') if next_turn is not None else '',
            'annotation': {},
            'metadata': {
                'speaker': turn.get('speaker'),
                'retrieved_passages': turn.get('retrieved_passages'),
                'retrieved_topics': turn.get('retrieved_topics'),
                'checked_sentence': turn.get('checked_sentence', {}),
                'checked_passage': turn.get('checked_passage', {})
            }
        }
        return converted_turn
