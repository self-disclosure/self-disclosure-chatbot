from cobot_common.datasets.wizard_of_wikipedia.builder import WowBuilder
from cobot_common.datasets.topical_chat.builder import TopicalChatBuilder
from cobot_common.datasets.reader import Dataset

"""
The tool consists of dataset builder and reader. The builder downloads a dataset from the given URL and 
pre-processes it with schema conversion and sharding. The reader can access the pre-processed dataset.

Here is an example of one dialog in the converted schema,

{
    'dataset': 'wizard_of_wikipedia',
    'dialog_id': ''
    'turns': 
        [
            {
                'input': 'I have been studying Veterinary Medicine for 10 years.',
                'response': 'Oh good for you, veterinary medicine is needed a lot for our pets especially dogs.',
                'annotation': {},
                'metadata': {}
            }     
        ],
    'annotation': {},
    'metadata': {}
}

The pre-processed datasets are stored in files with JSON format and each file contains a list of the above dicts.
In the converted schema, dialog_id, annotation and metadata fields are optional. Annotation and metadata fields have 
dataset-specific schema.

"""


def build_wow_dataset():
    builder = WowBuilder()
    builder.build()


def build_topical_chat_dataset():
    builder = TopicalChatBuilder()
    builder.build()


def read_dataset(name):
    dataset = Dataset(name)

    print('================Read all data of {} dataset================='.format(name))
    print('size: {}'.format(dataset.size()))
    iterator = dataset.iterator()
    count = 0
    for item in iterator:
        count += 1
    print('Iteration times with batch size equals to {}: {}'.format(1, count))
    iterator = dataset.iterator(batch_size=20)
    count = 0
    for item in iterator:
        count += 1
    print('Iteration times with batch size equals to {}: {}'.format(20, count))


def read_dataset_split(name, split):
    dataset = Dataset(name)
    print('================Read a split of {} dataset================='.format(name))
    print('size: {}'.format(dataset.size(split=split)))
    iterator = dataset.iterator(split=split)
    count = 0
    for item in iterator:
        count += 1
    print('Iteration times with batch size equals to {}: {}'.format(1, count))


if __name__ == '__main__':
    build_wow_dataset()
    read_dataset('wizard_of_wikipedia')
    read_dataset_split('wizard_of_wikipedia', 'test_random')

    build_topical_chat_dataset()
    read_dataset('topical_chat')
    read_dataset_split('topical_chat', 'test_seen')


