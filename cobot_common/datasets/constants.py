SHARD_SIZE = 1000  # maximum size of the dict stored in a single file
RAW_DATA_FOLDER = 'raw_data'  # folder where the downloaded dataset stored
OUTPUT_DATA_FOLDER = 'data'  # folder where the pre-processed dataset stored

AWS_PROFILE = ''  # AWS credential profile name used to download datasets from S3. The default cobot
                  # AWS profile is used if not set.
