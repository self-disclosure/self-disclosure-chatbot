import json
import os
import tarfile
import sys
import shutil
import boto3

from abc import ABC, abstractmethod
from cobot_common.datasets import constants, dataset_config


class DatasetBuilder(ABC):
    """
    Base dataset builder class. It handles downloading a dataset from a configured HTTP endpoint.
    Dataset builder classes should extend this class.
    """

    def __init__(self, dataset_name):
        self._name = dataset_name
        self._config = self._get_config()
        self._s3_helper = S3Helper(constants.AWS_PROFILE)

        self._validate_build_methods()

    def download(self):
        """
        Download the zip file from the url of the dataset.
        """
        s3_url = self._config.get('s3_url')
        bucket, key = s3_url.split('/', 2)[-1].split('/', 2)[-2:]
        file_name = key.split('/')[1]
        file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), self._name, file_name)
        if os.path.exists(file_path):
            return file_path

        print('Downloading {} datakeyset to {}...'.format(self._name, file_path))
        # TODO: add a progress bar
        # with requests.get(url, stream=True) as response:
        #     with open(file_path, 'wb') as f:
        #         for chunk in response.iter_content(chunk_size=constants.CHUNK_SIZE):
        #             if chunk:
        #                 f.write(chunk)
        self._s3_helper.download_file(bucket, key, file_path)
        return file_path

    def extract(self, file_path):
        """
        Extract all files from the given zip file and store into the raw data folder.
        """
        with tarfile.open(file_path) as tar:
            tar.extractall(self.raw_data_dir())

    def raw_data_dir(self):
        """
        Return the absolute path to the raw data folder.
        """
        return os.path.join(os.path.dirname(os.path.realpath(__file__)), self._name, constants.RAW_DATA_FOLDER)

    def output_data_dir(self):
        """
        Return the absolute path to the output data folder.
        """
        return os.path.join(os.path.dirname(os.path.realpath(__file__)), self._name, constants.OUTPUT_DATA_FOLDER)

    def build(self):
        """
        Build the dataset with all the splits.
        """
        print('Building {} dataset...'.format(self._name))
        file_path = self.download()
        self.extract(file_path)
        for split in self._config.get('splits'):
            getattr(self, split)()

    def build_split(self, iterator, output_dir, **kwargs):
        """
        Convert data provided by a iterator and store on disks. Need to call abstract method _convert to get the
        converted data.
        """
        if not os.path.isdir(os.path.dirname(output_dir)):
            os.mkdir(os.path.dirname(output_dir))

        # Recreate output dir
        if os.path.isdir(output_dir):
            shutil.rmtree(output_dir)
        os.mkdir(output_dir)

        start_idx = 0
        data_list = []
        for data in iterator:
            data_list.append(self.convert(data, **kwargs))
            if sys.getsizeof(data_list) >= constants.SHARD_SIZE:
                with open(os.path.join(
                        output_dir, '{}-{}.json'.format(start_idx, start_idx + len(data_list) - 1)), 'w') as f:
                    json.dump(data_list, f)
                start_idx += len(data_list)
                data_list = []
        if len(data_list) > 0:
            with open(os.path.join(
                    output_dir, '{}-{}.json'.format(start_idx, start_idx + len(data_list) - 1)), 'w') as f:
                json.dump(data_list, f)

    @abstractmethod
    def convert(self, item, **kwargs):
        """
        Convert an item to the the schema to be stored.
        """
        raise NotImplemented("convert method is not implemented")

    def _validate_build_methods(self):
        for split in self._config.get('splits'):
            if split is None:
                raise ValueError('No build method defined for split {}.'.format(split))
            if getattr(self, split) is None:
                raise ValueError('Build method {} is not defined.'.format(split))

    def _get_config(self):
        if not hasattr(dataset_config, self._name):
            raise ValueError('Dataset {} is not defined in config file.'.format(self._name))
        return getattr(dataset_config, self._name)


class S3Helper(object):

    def __init__(self, profile=None):
        if profile is None or profile == '':
            profile = os.getenv('AWS_PROFILE')
        self._profile = profile

    def download_file(self, bucket, key, local_path):
        session = boto3.Session(profile_name=self._profile)
        s3 = session.resource('s3')
        s3.Bucket(bucket).download_file(key, local_path)