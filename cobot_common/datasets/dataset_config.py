wizard_of_wikipedia = {
    "s3_url": "https://s3.amazonaws.com/alexa-prize-cobot-toolkit/datasets/wizard_of_wikipedia.tar.gz",
    "splits": ["all", "train", "test_random", "test_topic", "valid_random", "valid_topic"],
    "description": """
        An open domain knowledge based dialog datset. 
        Details can be found at https://parl.ai/projects/wizard_of_wikipedia.
    """
}

topical_chat = {
    "s3_url": "https://s3.amazonaws.com/alexa-prize-cobot-toolkit/datasets/topical_chat.tar.gz",
    "splits": ["all", "train", "test_seen", "test_unseen", "valid_seen", "valid_unseen"],
    "description": """
        A dialog dataset consisting of chats data based topical content.
    """
}