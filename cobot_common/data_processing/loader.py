class Loader(object):

    def __init__(self, file_path):
        self._file_path = file_path
        self._text_list = []

    def read(self):
        with open(self._file_path, 'r') as text_file:
            for line in text_file:
                self._text_list.append(line.strip(' \n'))
        return self

    def text_list(self):
        return self._text_list