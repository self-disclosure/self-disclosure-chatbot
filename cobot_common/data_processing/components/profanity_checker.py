from textblob import TextBlob
import nltk
import ssl
import codecs
import os

# Special characters to strip from text before scanning for profanity. Most of string.punctuation, without a few important characters such as * and '
SPECIAL_CHARS = "!\"#$%&()+,./:;<=>?[\\]^_`{|}~"


def _get_resource_file_path(file_name):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    resource_file_path = os.path.join(dir_path, 'resources', file_name)
    return resource_file_path


def _install_nltk_corpuses():
    try:
        _create_unverified_https_context = ssl._create_unverified_context
    except AttributeError:
        pass
    else:
        ssl._create_default_https_context = _create_unverified_https_context
    # Download popular corpuses
    nltk.download(info_or_id="popular")


def _asciiify(text):
    # text = text.encode('utf-8').decode('ascii', 'ignore')
    # return text.decode('unicode-escape').encode('ascii', 'ignore')
    return text


def _text_contains_exact_word_fast(text, word):
 	# Check if text is exactly the given word
 	if len(word) >= len(text):
 		if word == text:
 			return True
 	# Check if word is in the middle of text, surrounded by whitespace
 	if ' ' + word + ' ' in text:
 		return True
 	# Check if word is at the start of text, followed by whitespace
 	if text[:len(word) + 1] == word + ' ':
 		return True
 	# Check if word is at the end of text, preceded by whitespace
 	if text[len(text)-len(word)-1:] == ' ' + word:
 		return True


class BlacklistLoader(object):

    @classmethod
    def _open(cls, filename, permission):
        return codecs.open(_get_resource_file_path(filename), permission, 'utf-8')

    @classmethod
    def _get_not_pluralized_words(cls):
         word_lst = []
 
         with cls._open('not_pluralized_word_list.txt', 'r') as f:
             for row in f:
                 cleaned_string = _asciiify(row.replace('\n', '').lower())
                 word_lst.append(cleaned_string)
         return word_lst

    @classmethod
    def load(cls, file_path):
        blacklist = []
        # _install_nltk_corpuses()
        not_pluralized_words = cls._get_not_pluralized_words()

        with open(file_path, 'r') as blacklist_file:
            for row in blacklist_file:
                # Ignore non-ascii characters in order to utilize TextBlob library
                cleaned_string = _asciiify(row.replace('\n', '').lower())

                if cleaned_string in not_pluralized_words:
                    blacklist.append(cleaned_string)
                    continue
                banned_phrase = TextBlob(cleaned_string)
                blacklist.append(str(banned_phrase))
                # Add pluralizations as well for words not containing symbols
                if str(banned_phrase).isalnum():
                    # Only pluralize the final word in the banned phrase
                    pluralized_word_list = banned_phrase.words[:-1] + [banned_phrase.words[-1].pluralize()]
                    pluralized_banned_phrase = ' '.join(pluralized_word_list)
                    blacklist.append(str(pluralized_banned_phrase))
        return blacklist


class ProfanityChecker(object):

    name = 'profanity'

    def __init__(self, blacklist_file_path=None):
        if blacklist_file_path == None:
            blacklist_file_path = _get_resource_file_path('profanity_blacklist.txt')
        
        self.blacklist = BlacklistLoader.load(blacklist_file_path)

    def _check_banned_pharase(self, text):
        text = text.lower()
        # Remove special characters
        for p in SPECIAL_CHARS:
            text = text.replace(p, '')
        for banned_phrase in self.blacklist:
            if _text_contains_exact_word_fast(text, banned_phrase):
                return True
        return False
    
    def check(self, text):
        return self._check_banned_pharase(text)

    @staticmethod
    def get(cls, doc):
        return doc._.get(cls,)

    