import spacy
import sys

try:
    from cobot_common.data_processing.utterance import UtteranceUtil
except ModuleNotFoundError:
    # add cobot_common module to module searching path
    base_path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    sys.path.append(base_path)
    from cobot_common.data_processing.utterance import UtteranceUtil


class NerChecker(object):
    """
    Use spacy to do NER
    """

    def __init__(self):
        self._nlp = spacy.load('en')

    def detech_ner(self, utterances):

        def detect(utterance):
            doc = self._nlp(utterance.text)
            ner_obj = [
                {
                    'text': ent.text,
                    'start': ent.start_char,
                    'end': ent.end_char,
                    'label': ent.label_
                }
                for ent in doc.ents
            ]
            utterance.ner = ner_obj
            return utterance

        utterances = map(detect, utterances)
        return list(utterances)

    