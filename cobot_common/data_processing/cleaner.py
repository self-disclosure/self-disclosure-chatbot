import re
import string

from .components import ProfanityChecker
from .loader import Loader

class Cleaner(object):

    DATA_TYPE_CONVERSATION = "DATA_TYPE_CONVERSATION"
    DATA_TYPE_FLAT_TEXT = "DATA_TYPE_FLAT_TEXT"

    def __init__(self, text_list=None, file_path=None):
        if text_list is not None:        
            self._data_set = text_list
        elif file_path is not None:
            self._data_set = Loader(file_path).read().text_list()
        else:
            self._data_set = []

    @classmethod
    def _clean_text(cls, text):
        cleaned_text = re.sub(r'<[^<]+?>', '', text)
        cleaned_text = re.sub(r'[a-z]*[:.]+\S+', '', cleaned_text)
        cleaned_text = " ".join(re.sub("[^A-Za-z0-9, ']+", ' ', cleaned_text).split())
        return cleaned_text

    def clean_text(self):
        result = []
        for text in self._data_set:
            result.append(text.replace(string.punctuation, ''))
        self._data_set = result
        self._data_type = self.DATA_TYPE_FLAT_TEXT
        return self

    def scrub_with_blacklist(self, blacklist_file_path=None):
        checker = ProfanityChecker(blacklist_file_path)
        result = []
        for text in self._data_set:
            result.append({
                "text": text,
                "profanity": checker.check(text)
            })
        self._data_set = result
        return self

    def _filter(self):
        return [
            item['text']
            for item in self._data_set
            if not item['profanity']
        ]
    
    def output(self, output_file_path):
        cleaned_text_list = self._filter()
        with open(output_file_path, 'w') as text_file:
            for line in cleaned_text_list:
                text_file.write(line + '\n')

    def result(self):
        return self._filter()