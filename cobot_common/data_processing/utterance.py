class Utterance(object):
    """
    Utterance instances go through each pipeline component and attach annotation data on predefined attributes.
    """

    def __init__(self, text):

        self.text = text
        self.ner = None
        self.topic = None
        self.profanity_class = None

    def json(self, output_attr):
        json_obj = {
            'text': self.text,
        }
        for attr in output_attr:
            json_obj[attr] = getattr(self, attr)
        return json_obj
    

class UtteranceUtil(object):

    @classmethod
    def retrieve_text(cls, utterances):
        text_list = [
            utterance.text
            for utterance in utterances
        ]
        return text_list