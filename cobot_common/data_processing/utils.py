def batch(l, size):
    for i in range(0, len(l), size):
        yield l[i:i + size]


def chunk(l, count):
    for i in range(0, len(l), count):
        yield l[i:i + count]