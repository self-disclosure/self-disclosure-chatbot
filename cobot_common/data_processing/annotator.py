import re
import os
import concurrent.futures
import traceback
import json

from .utterance import Utterance
from .components import RemoteChecker, NerChecker
from .utils import batch, chunk
from .loader import Loader


class SingleThreadAnnotator(object):

    BATCH_SIZE = 10 # Todo: Current toolkit service limitation

    def __init__(self, data_set):

        self._data_set = data_set
        self._result = []
        self._pipeline = []

    def add_components(self, components):
        self._pipeline.extend(components)
        return self

    @classmethod
    def _clean_text(cls, text):
        cleaned_text = re.sub(r'<[^<]+?>', '', text)
        cleaned_text = re.sub(r'[a-z]*[:.]+\S+', '', cleaned_text)
        cleaned_text = " ".join(re.sub("[^A-Za-z0-9, ']+", ' ', cleaned_text).split())
        return cleaned_text

    def _convert_to_utterance(self):
        if len(self._data_set) == 0:
            return
        if type(self._data_set[0]) == Utterance:
            return
        self._data_set = [
            Utterance(self._clean_text(text))
            for text in self._data_set
        ]

    def result(self):
        self._convert_to_utterance()

        utterance_chunk_list = batch(self._data_set, self.BATCH_SIZE)

        temp_result = []
        for utterance_chunk in utterance_chunk_list:
            for func in self._pipeline:
                func(utterance_chunk)

            temp_result.extend(utterance_chunk)

        return temp_result
        

class Annotator(object):
    """
    Split entire data set in a list of chunks. Use one `SingleThreadAnnotator` instance in each thread to process a chunk and then merge the results.
    """

    # Used to split input into a list of chunks
    MAX_WORKERS = os.cpu_count() * 5 

    def __init__(self, text_list=None, api_key=None, file_path=None):
        if text_list is not None:        
            self._data_set = text_list
        elif file_path is not None:
            self._data_set = Loader(file_path).read().text_list()
        else:
            self._data_set = []

        self._remote_checker = RemoteChecker(api_key=api_key)
        self._pipeline = []
        self._output_attr = []

    def detect_ner(self, customer_handler=None):
        if customer_handler is not None:
            self._pipeline.append(customer_handler)
        else:
            ner_checker = NerChecker()
            self._pipeline.append(ner_checker.detech_ner)
        
        self._output_attr.append('ner')
        return self

    def detect_topic(self, customer_handler=None):
        if customer_handler is not None:
            self._pipeline.append(customer_handler)
        else:
            self._pipeline.append(self._remote_checker.detect_topic)

        self._output_attr.append('topic')
        return self

    def detect_profanity(self, customer_handler=None):
        if customer_handler is not None:
            self._pipeline.append(customer_handler)
        else:
            self._pipeline.append(self._remote_checker.detect_profanity)

        self._output_attr.append('profanity_class')
        return self

    def detect_sentiment(self, customer_handler=None):
        if customer_handler is not None:
            self._pipeline.append(customer_handler)
        else:
            self._pipeline.append(self._remote_checker.detect_sentiment)

        self._output_attr.append('sentiment_class')
        return self

    def _execute(self):
        utterance_chunks = chunk(self._data_set, self.MAX_WORKERS)

        def annotate(data_set):
            result = SingleThreadAnnotator(data_set) \
                        .add_components(self._pipeline) \
                        .result()
            return result

        annotated_result = []
        with concurrent.futures.ThreadPoolExecutor() as executor:
            jobs = {executor.submit(annotate, utterances): utterances for utterances in utterance_chunks}
            for future in concurrent.futures.as_completed(jobs):
                try:
                    data = future.result()
                except Exception as exc:
                    print('generated an exception: %s' % (exc))
                    traceback.print_exc()
                else:
                    annotated_result.extend(data)

        return [
            utterance.json(self._output_attr)
            for utterance in annotated_result
        ]

    def result(self):
        return self._execute()

    def output(self, output_file_path):
        json_obj = self._execute()
        with open(output_file_path, 'w') as json_file:
            json.dump(json_obj, json_file)