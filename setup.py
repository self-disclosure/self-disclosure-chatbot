import re
from setuptools import setup, find_packages
from glob import glob


with open('README.md', encoding='utf-8') as f:
    long_description = f.read()

# declare your scripts:
# scripts in bin/ with a shebang containing python will be
# recognized automatically
scripts = []
for fname in glob('bin/*'):
    with open(fname, 'r') as fh:
        if re.search(r'^#!.*python', fh.readline()):
            scripts.append(fname)


setup(
    name="alexa_prize_cobot_toolkit",
    version="2.0.0",
    description="Cobot toolkit vended for Alexa Prize participants.",
    long_description=long_description,

    # declare your packages
    packages=find_packages(exclude=(
        'bin', 'doc', 'example', 'integ_test', 'sample_cobot', 'test')),

    # declare your scripts
    scripts=scripts,
    package_data={'': ['*.json']},
    include_package_data=True,
)