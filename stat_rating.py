import csv
import re
import datetime

# Conversation ID,Approximate Start Time,Rating

user_map = {}
module_map = {}
time_map = {}
ab_map = {}
cid_to_ab_map = {}

count = 0
summ = 0
line = 0
count_sm = 0
csvfile = None
try:
    csvfile = open('/home/ubuntu/cobot_base/output.csv', 'r')
except FileNotFoundError:
    pass

if not csvfile:
    try:
        csvfile = open('/home/ec2-user/cobot_base/output.csv', 'r')
    except FileNotFoundError as e:
        print(e)
else:
    print("ERROR: cannot open csvfile")
    exit(1)




reader = csv.DictReader(csvfile)
for row in reader:
    time = datetime.datetime.strptime(
        row['creation_date_time'], '%Y-%m-%d %H:%M:%S.%f')
    line += 1

    if row['a_b_test'] not in ab_map:
        ab_map[row['a_b_test']] = {}
        ab_map[row['a_b_test']]["count"] = 0
        ab_map[row['a_b_test']]["sum"] = 0
        ab_map[row['a_b_test']]["duration"] = 0
        ab_map[row['a_b_test']]["duration_count"] = 0
        module_map[row['selected_modules']] = float(
            re.sub('\*$', '', row['rating']))

    ab_map[row['a_b_test']]["count"] += 1

    if row['selected_modules'] not in module_map:
        module_map[row['selected_modules']] = float(
            re.sub('\*$', '', row['rating']))
    module_map[row['selected_modules']]
    if row['conversation_id'] not in user_map:
        ab_map[row['a_b_test']
               ]["sum"] += float(re.sub('\*$', '', row['rating']))
        cid_to_ab_map[row['conversation_id']] = row['a_b_test']
        time_map[row['conversation_id']] = {'low': time, 'high': time}
        user_map[row['conversation_id']] = float(
            re.sub('\*$', '', row['rating']))

        summ += float(re.sub('\*$', '', row['rating']))
        count += 1

    if time > time_map[row['conversation_id']]['high']:
        time_map[row['conversation_id']]['high'] = time
    if time < time_map[row['conversation_id']]['low']:
        time_map[row['conversation_id']]['low'] = time

tsum = 0
duration = []
for user, time_range in time_map.items():
    tsum += (time_range['high'] - time_range['low']).total_seconds()
    duration.append((time_range['high'] - time_range['low']).total_seconds())
    ab_map[cid_to_ab_map[user]
           ]['duration'] += (time_range['high'] - time_range['low']).total_seconds()
    ab_map[cid_to_ab_map[user]]['duration_count'] += 1

duration.sort()

# print(count_sm)
print("Total Conversation Time: " + str(datetime.timedelta(seconds=tsum)))
print("Average Conversation Time: " +
      str(datetime.timedelta(seconds=float(tsum) / float(count))))
print("Median Conversation Time: " +
      str(datetime.timedelta(seconds=duration[int(len(duration) / 2)])))
print("90 Percentile Conversation Time: " +
      str(datetime.timedelta(seconds=duration[int(len(duration) / 10 * 9)])))
print("Number of conversations: " + str(count))
print("Number of turns: " + str(line))
print("Average of turns: " + str(line / count))
print("Average of ratings: " + str(summ / count))

for case, stats in ab_map.items():
    print("Test case: {}, avg score: {} with {} of cases; Total turns: {}, avg turns: {}, avg duration: {}".format(
        case, float(stats["sum"]) / float(stats["duration_count"]), stats["duration_count"], stats["count"], float(stats["count"]) / float(stats["duration_count"]),  str(datetime.timedelta(seconds=float(stats["duration"]) / float(stats["duration_count"])))))
