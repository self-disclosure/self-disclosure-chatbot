from injector import singleton
from typing import Optional, Dict, Any, Union

from .path_dict import PathDict


@singleton
class Event(PathDict):
    """
    Use `PathDict` to wrap event parameters from lambda handler function.
    Provide shortcut functions to fetch some specific data from the event.
    """

    @property
    def app_id(self) -> str:
        app_id = self.get('context.System.application.applicationId')
        if app_id is None:
            app_id = self.get('session.application.applicationId')
        if app_id is None:
            app_id = ''
        if not isinstance(app_id, str):
            raise TypeError("app_id should be a str")
        return app_id

    @property
    def user_id(self) -> str:
        user_id = self.get('context.System.user.userId')
        if user_id is None:
            user_id = self.get('session.user.userId')
        if user_id is None:
            user_id = ''
        if not isinstance(user_id, str):
            raise TypeError("user_id should be a str")
        return user_id

    @property
    def conversation_id(self) -> Union[str, None]:
        conversation_id = self.get('request.payload.conversationId')
        if conversation_id is None:
            conversation_id = self.get('session.attributes.conversationId')
        if conversation_id is not None:
            if not isinstance(conversation_id, str):
                raise TypeError("conversation_id should be a str or None")
        return conversation_id

    @property
    def attributes(self) -> dict:
        attributes_ =  self.get('session.attributes', {})
        if not isinstance(attributes_, dict):
            raise TypeError("attributes should be a dict")
        return attributes_

    @property
    def speech_recognition(self) -> list:
        speech_recognition = self.get('request.payload.speechRecognition.hypotheses')
        if speech_recognition is None:
            speech_recognition = self.get('request.speechRecognition.hypotheses')
        if speech_recognition is None:
            speech_recognition = []
        if not isinstance(speech_recognition, list):
            raise TypeError("speech_recognition should be a list")
        return speech_recognition
