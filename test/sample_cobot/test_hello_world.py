from unittest import TestCase
from sample_cobot.sample_events import new_session_event
from doc.example.hello_world import lambda_handler

class TestHelloWorld(TestCase):
    # def setUp(self):
    # 	self.cobot = Cobot.handler(event=new_session_event,
    # 	                           context={},
    # 	                           app_id=None,
    # 	                           user_table_name=None,
    # 	                           save_before_response=True,
    # 	                           state_table_name=None)

    def test_execute(self):
        result = lambda_handler(event=new_session_event,
                                context={})
        print (result)
        self.assertEqual(result.get('response').get('outputSpeech').get('ssml'),
                         '<speak>Local Baseline Response Generator</speak>')

