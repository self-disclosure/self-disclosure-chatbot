import sys
#add package AlexaPrizeCobotToolkit to sys path
sys.path.append("./../..")
print("sys path {}".format(sys.path))

import unittest
from cobot_common.docker.sentiment.app import app
import json
import sys

class TestFlaskApi(unittest.TestCase):
    """
    Test can be run by python3 -m unittest test_sentiment.py after you are at test/cobot_common/
    Precondition: vaderSentiment must exist in pythonpath
    1. run python3 -m pip install vaderSentiment to install vaderSentiment
    """
    def setUp(self):
        app.app.testing = True
        self.app = app.app.test_client()

    def test_hello_world(self):
        response = self.app.get('/')
        self.assertEqual(response.get_data().decode(sys.getdefaultencoding()), 'Welcome')

        response = self.app.post('/sentiment', data=json.dumps({'text': 'good'}))
        self.assertEqual(json.loads(response.get_data().decode(sys.getdefaultencoding())).get('sentiment').get('pos'), 1.0)


if __name__ == "__main__":
    unittest.main()
