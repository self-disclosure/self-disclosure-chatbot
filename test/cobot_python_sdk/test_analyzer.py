from unittest import TestCase
from unittest.mock import Mock, call
from cobot_python_sdk.analyzer import Analyzer

class TestAnalyzer(TestCase):

    def setUp(self):

        self.event = Mock()
        self.attributes = Mock()
        self.user_attributes_manager = Mock()

        self.analyzer = Analyzer(
            event=self.event, 
            attributes=self.attributes, 
            user_attributes_manager=self.user_attributes_manager
        )
    
    def test_merge_attributes_from_db(self):

        # persistence is not enabled
        self.event.get = Mock(side_effect=['session_id', True])
        self.user_attributes_manager.persistence_enabled = False
        self.user_attributes_manager.retrieve_user_attributes = Mock()

        self.analyzer._merge_attributes_from_db()
        self.user_attributes_manager.retrieve_user_attributes.assert_not_called()

        # persistence is enabled
        self.user_attributes_manager.persistence_enabled = True
        
        self.analyzer._merge_attributes_from_db()
        self.user_attributes_manager.retrieve_user_attributes.assert_called()
        self.event.get.assert_called_once()

        # session.sessionId is None
        self.event.get = Mock(side_effect=[None, True])
        self.user_attributes_manager.persistence_enabled = True
        
        self.analyzer._merge_attributes_from_db()
        self.user_attributes_manager.retrieve_user_attributes.assert_called()
        self.event.get.assert_has_calls([call('session.sessionId'), call('session.new')])

        # No persistant attributes
        self.user_attributes_manager.persistence_enabled = True
        self.event.get = Mock(side_effect=['session_id', True])
        self.user_attributes_manager.retrieve_user_attributes = Mock(return_value=None)
        self.attributes.merge = Mock()

        self.analyzer._merge_attributes_from_db()
        self.attributes.merge.assert_not_called()

        # Has persistant attributes
        self.user_attributes_manager.persistence_enabled = True
        self.event.get = Mock(side_effect=['session_id', True])
        self.user_attributes_manager.retrieve_user_attributes = Mock(return_value='')
        self.attributes.merge = Mock()

        self.analyzer._merge_attributes_from_db()
        self.attributes.merge.assert_called()

    def test_extract_feature(self):
        
        # If there is no mode field in attributes, mode should be empty string.
        self.attributes.mode = None
        
        feature = self.analyzer._extract_feature()
        self.assertEqual(feature['mode'], '')

        # There is a mode field in attributes
        self.attributes.mode = 'test_mode'

        feature = self.analyzer._extract_feature()
        self.assertEqual(feature['mode'], 'test_mode')

    def test_analyze(self):
        self.analyzer._merge_attributes_from_db = Mock()
        self.analyzer._extract_feature = Mock()
        
        self.analyzer.analyze()
        self.analyzer._merge_attributes_from_db.assert_called_once()
        self.analyzer._extract_feature.assert_called_once()