from unittest import TestCase
from unittest.mock import Mock

from cobot_python_sdk.alexa_response_builder import AlexaResponseBuilder

class TestAlexaResponseBuilder(TestCase):

    def setUp(self):
        self.attributes = attributes = Mock()

        self.map_attributes = attributes.map_attributes = {}

        self.builder = AlexaResponseBuilder(attributes)

    def test_speak(self):

        self.builder.speak('test_speak')

        speech = self.builder._response['response']['outputSpeech']

        self.assertIsNotNone(speech)

    def test_listen(self):
        self.builder.listen('test_listen')

        speech = self.builder._response['response']['reprompt']

        self.assertIsNotNone(speech)

        shouldEndSession = self.builder._response['response']['shouldEndSession']

        self.assertFalse(shouldEndSession)

    def test_build(self):

        response = self.builder.build()

        self.assertEqual(response['sessionAttributes'], self.map_attributes)

    def test_response_syntax(self):

        response = self.builder.speak('speak content').listen('listen content').build()

        self.assertTrue('version' in response)
        self.assertTrue('sessionAttributes' in response)
        self.assertTrue('response' in response)
        self.assertTrue('shouldEndSession' in response['response'])
        self.assertTrue('outputSpeech' in response['response'])
        self.assertTrue('reprompt' in response['response'])