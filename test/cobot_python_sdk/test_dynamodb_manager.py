"""Test suites for cobot_python_sdk.dynamodb_manager,
copied from https://code.amazon.com/packages/AlexaMoviebot/blobs/mainline/--/test/unit/test_dynamodb_manager.py"""

from cobot_python_sdk.configuration.config_constants import ConfigConstants
from cobot_python_sdk.dynamodb_manager import DynamoDbManager

from decimal import Decimal
import mock
from nose.tools import assert_equal, assert_is_none, assert_false, assert_is_instance
from boto3.dynamodb.conditions import Key
import unittest


class TestDynamoDbManager(unittest.TestCase):
    @mock.patch('cobot_python_sdk.dynamodb_manager.DynamoDbManager._get_table')
    def test_get(self, mock_get_table):
        """
        Test the get function. Validate it is called with an expected key condition and that it handles missing item
        values.
        """
        mock_table = mock.Mock()
        mock_table.get_item.return_value = {'SomeKey': 'SomeValue', 'Item': 'Success, we found it!'}
        mock_get_table.return_value = mock_table

        mock_table_name = 'imdb_cache'
        mock_key_name = 'api_relative_uri'
        mock_key_value = '/title/tt0026752/'

        found_item = DynamoDbManager.get_item(mock_table_name, mock_key_name, mock_key_value)

        # Check that we only called for the table once
        mock_get_table.assert_called_once()
        mock_get_table.assert_called_with(mock_table_name)

        # Check that the mock table had its get_item method called once with the appropriate Key
        mock_table.get_item.assert_called_once()
        mock_table.get_item.assert_called_with(Key={mock_key_name: mock_key_value})

        # Verify that the Item was returned
        assert_equal(found_item, 'Success, we found it!')

        # Reset our mocks
        mock_get_table.reset_mock()
        mock_table.get_item.reset_mock()

        mock_table.get_item.return_value = {'SomeKey': 'SomeValue'}  # No item!
        found_item = DynamoDbManager.get_item(mock_table_name, mock_key_name, mock_key_value)

        # Check that we only called for the table once
        mock_get_table.assert_called_once()
        mock_get_table.assert_called_with(mock_table_name)

        # Check that the mock table had its get_item method called once with the appropriate Key
        mock_table.get_item.assert_called_once()
        mock_table.get_item.assert_called_with(Key={mock_key_name: mock_key_value})

        # Verify that no Item was returned.
        assert_is_none(found_item)

    @mock.patch('cobot_python_sdk.dynamodb_manager.DynamoDbManager._get_table')
    def test_put(self, mock_get_table):
        """
        Test the put function. Validate it is called with an appropriate Item
        """
        mock_table = mock.Mock()
        mock_table.put_item.return_value = {'SomeKey': 'SomeValue'}
        mock_get_table.return_value = mock_table

        mock_item_dict = {'key': 'key_value', 'col1': 'col1_value'}
        mock_table_name = 'dynamo_table'
        DynamoDbManager.put_item(mock_table_name, mock_item_dict)

        mock_get_table.assert_called_once_with(mock_table_name)

        mock_table.put_item.assert_called_once_with(Item=mock_item_dict)

    def test_fix_types(self):
        """
        Test the _fix_types method. The only thing this function does is turn floats into decimal numbers.
        Here we validate that the float values are fixed and others are unchanged.

        Note that this does not work for nested dictionaries/lists of floats etc. This may be something to address
        in the future.
        """
        # Test no floats
        test_dict = {
            'key1': 'not a float',
            'key2': 1
        }
        fixed_dict = DynamoDbManager._fix_types(test_dict)

        # Confirm they are different instances
        assert_false(test_dict is fixed_dict)
        # Confirm no changes were made
        assert_equal(test_dict, fixed_dict)

        # Now test with a float value
        test_dict['key2'] = 1.39
        fixed_dict = DynamoDbManager._fix_types(test_dict)

        # The dictionaries are still equal as they are equivalent. However, the float has been converted to a Decimal
        # assert_equal(test_dict, fixed_dict)
        print(fixed_dict)
        assert_is_instance(fixed_dict['key2'], Decimal)

        # Now test with a float value in a list
        test_dict['key2'] = {"n_best_asr_result": [{"confidence": 0.976, "value": "five"}]}
        fixed_dict = DynamoDbManager._fix_types(test_dict)

        # The dictionaries are still equal as they are equivalent. However, the float has been converted to a Decimal
        assert_equal(test_dict, fixed_dict)
        assert_is_instance(fixed_dict['key2']['n_best_asr_result'][0]['confidence'], Decimal)

    @mock.patch('cobot_python_sdk.dynamodb_manager.DynamoDbManager._get_table')
    def test_query(self, mock_get_table):
        """
        Test the query method. Validate that we build an argument dictionary appropriately and that we loop through
        queries until they are complete.
        """
        mock_table = mock.Mock()
        mock_get_table.return_value = mock_table

        # First, check the argument construction
        test_table_name = 'dynamo_table'
        test_user_id = 'a.user.1234'

        # Create a key condition
        test_key_condition = ComparableKey(ConfigConstants.USER_ID_FIELD_NAME).eq(test_user_id)

        test_query_response = {'Items': [{'Key': 'key1', 'Col1': 'val1'}]}
        mock_table.query.return_value = test_query_response

        # Basic call with table name and key condition
        query_result = DynamoDbManager.query(test_table_name, test_key_condition)

        expected_call_args = {
            'KeyConditionExpression': test_key_condition
        }

        # Verify the get_table call
        mock_get_table.assert_called_once()
        mock_get_table.assert_called_with(test_table_name)

        # Verify the query call
        mock_table.query.assert_called_once()
        mock_table.query.assert_called_with(**expected_call_args)

        assert_equal(query_result, test_query_response.get('Items'))

        # Reset the mocks
        mock_get_table.reset_mock()
        mock_table.reset_mock()

        # Now add an index name and expression_attribute_values_dict
        test_index_name = 'second_index'
        test_expression_attribute_values_dict = {'Attr1': 3, 'Attr2': 'Scholesy'}

        query_result = DynamoDbManager.query(test_table_name, test_key_condition, test_index_name,
                                             test_expression_attribute_values_dict)

        expected_call_args['IndexName'] = test_index_name
        expected_call_args['ExpressionAttributeValues'] = test_expression_attribute_values_dict

        # Verify the get_table call
        mock_get_table.assert_called_once()
        mock_get_table.assert_called_with(test_table_name)

        # Verify the query call
        mock_table.query.assert_called_once()
        mock_table.query.assert_called_with(**expected_call_args)

        assert_equal(query_result, test_query_response.get('Items'))

        # Reset the mocks
        mock_get_table.reset_mock()
        mock_table.reset_mock()

        # Now verify the loop logic by creating a list of return values, the first one has the 'LastEvaluatedKey' in it
        test_query_response_with_extras = test_query_response.copy()
        test_query_response_with_extras['LastEvaluatedKey'] = 'key1'
        test_query_response['Items'][0]['Key'] = 'key2'

        response_list = [test_query_response_with_extras, test_query_response]
        mock_table.query.side_effect = response_list

        query_result = DynamoDbManager.query(test_table_name, test_key_condition, test_index_name,
                                             test_expression_attribute_values_dict)

        # Verify the get_table call
        mock_get_table.assert_called_once()
        mock_get_table.assert_called_with(test_table_name)

        expected_call_args_second_call = expected_call_args.copy()
        expected_call_args_second_call['ExclusiveStartKey'] = 'key1'

        # Verify the query call
        assert_equal([mock.call(**expected_call_args), mock.call(**expected_call_args_second_call)],
                     mock_table.query.call_args_list)

        # Query result is one list combining the two lists, so manually take the single item from each list to verify
        assert_equal(query_result,
                     [test_query_response_with_extras.get('Items')[0], test_query_response.get('Items')[0]])

    @mock.patch('cobot_python_sdk.dynamodb_manager.DynamoDbManager._get_table')
    def test_update(self, mock_get_table):
        """
        Test the update method. No complicated logic, simply check that values are passed to the update_item call.
        """
        mock_table = mock.Mock()
        mock_get_table.return_value = mock_table
        mock_table.update_item.return_value = {'Attributes': ['Cantona']}

        test_table_name = 'dynamo_table'
        test_key_dict = {'Key': 'giggsy', 'Key': 'scholesy'}
        test_update_expression = 'UPDATE ALL'  # This does not need to be a 'real' update expression
        test_expression_attribute_values_dict = {'Attr1': 3, 'Attr2': 'Scholesy'}
        test_return_values = 'ALL_UPDATED'

        DynamoDbManager.update_item(test_table_name, test_key_dict, test_update_expression,
                                    test_expression_attribute_values_dict, test_return_values)

        mock_get_table.assert_called_once()
        mock_get_table.assert_called_with(test_table_name)

        mock_table.update_item.assert_called_once()
        mock_table.update_item.assert_called_with(
            Key=test_key_dict,
            UpdateExpression=test_update_expression,
            ExpressionAttributeValues=test_expression_attribute_values_dict,
            ReturnValues=test_return_values
        )

    def test_get_table(self):
        """
        Test the get_table method. No logic here, just test we call the resource.Table function with the right table
        name.
        """
        # Save the actual resource
        original_resource = DynamoDbManager._resource
        mock_resource = mock.Mock()
        DynamoDbManager._resource = mock_resource

        test_table_name = 'dynamo_table'
        DynamoDbManager._get_table(test_table_name)

        mock_resource.Table.assert_called_once()
        mock_resource.Table.assert_called_with(test_table_name)

        # Reset the resource
        DynamoDbManager._resource = original_resource


class ComparableKey(Key):
    """
    This is an extension of the DynamoDb Key class so that it implements an equality function.
    Otherwise, comparison of key conditions falls back to object comparison, which is not what we want.

    This is used to test that an appropriate key condition is passed to DynamoDb.

    Implementation of Key is available here:
    http://boto3.readthedocs.io/en/latest/_modules/boto3/dynamodb/conditions.html
    """

    def __eq__(self, other):
        """
        The only configurable attribute of a Key (which extends AttributeBase) is a name.
        """
        if isinstance(other, Key):
            if self.name == other.name:
                return True

        return False

if __name__ == '__main__':
    unittest.main()