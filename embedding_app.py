import pickle
import numpy as np
import pandas as pd
import tensorflow as tf
import tensorflow_hub as hub
import redis
from flask import Flask, request, jsonify
import json
import ast
import time
import hashlib
import time
app = Flask(__name__)

class embedder:
        module_names =  ["LAUNCHGREETING", "ADJUSTSPEAK", "TERMINATE", "CHANGETOPIC", "CLARIFICATION", "SAY_COMFORT",
                 "SAY_THANKS", "SAY_FUNNY", "SAY_BIO", "REQ_MORE", "REQ_EASTEREGG", "PLAY_GAME", "PLAY_MUSIC", "REQ_TASK",
                 "REQ_LOC", "REQ_TOPIC", "TRANSITION", "PHATIC", "CONTROVERSIALOPION", "BACKSTORY","RETRIEVAL","EVI",
                 "STORYFUNFACT", "MOVIECHAT", "BOOKCHAT", "TECHSCIENCECHAT", "TRAVELCHAT", "GAMECHAT", "SPORT",
                 "MUSICCHAT", "HOLIDAYCHAT", "NEWS", "ANIMALCHAT", "WEATHER", "HEALTH"]
        def __init__(self):
                self.embed = hub.Module("https://tfhub.dev/google/universal-sentence-encoder/2")
                self.r = redis.StrictRedis(host='localhost', port=9376, db=0, password="alexaprize", decode_responses=True)

                with open("model_7_16.pkl", "rb") as rlist:
                        self.model = pickle.load(rlist)

                self.ss = tf.Session()
                self.similarity_input_placeholder = tf.placeholder(tf.string, shape=(None))
                self.similarity_message_encodings = self.embed(self.similarity_input_placeholder)
                self.ss.run(tf.tables_initializer())
                self.ss.run(tf.global_variables_initializer())

        def embed_module(self, module):
                code = [0 for _ in range(len(self.module_names))]
                if module not in self.module_names:
                        return code
                else:
                    code[self.module_names.index(module)] = 1
                return code

        def select_module(self, text, last_module):
                embeddings = self.ss.run(
                      self.similarity_message_encodings, feed_dict={self.similarity_input_placeholder: [text]})
                # embeddings = self.ss.run(query_embeddings)
                feats = np.concatenate((embeddings[0],self.embed_module(last_module))).reshape(1,-1)
                pred = self.model.predict(feats)[0]
                pred =int(pred)
                if pred == len(self.module_names):
                        return "RETRIEVAL"
                else:
                        return self.module_names[pred]

        def generate_module(self, query):
                return self.select_module(query['text'], query['last_module'])


ed = embedder()

@app.route("/module", methods=['POST'])
def module():
        content = request.get_json(silent=True)
        ed_result = ed.generate_module(content)
        return jsonify(ed_result)

# @app.route("/embedding", methods=['POST'])
# def embedding():
#         ed_result = ed.generate_comments_response(request.data)
#         return jsonify(ed_result)


if __name__ == "__main__":
        app.run(host='0.0.0.0', port=8085)
