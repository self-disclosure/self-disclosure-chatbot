import logging
import json
import re
from typing import List

from selecting_strategy.topic_mapping import custom_intent_topic_map, custom_intent_sys_map
from selecting_strategy.module_selection import ModuleSelector, topics_available_for_proposal, GlobalState
from selecting_strategy.topic_module_proposal import reset_propose_continue, module_propose_stop, \
    check_propose_continue, last_module_propose_unclear, get_propose_topic
from utils import get_working_environment
from utils import set_logger_level
from utils import check_intersect
from utils import unique
from nlu.util_nlu import get_sentiment_key
from nlu.dataclass.central_element import CentralElementFeatures
from nlu.constants import TopicModule
from nlu.question_detector import QuestionDetector
from user_profiler.user_profile import UserProfile

import template_manager
from nlu.dataclass.returnnlp import ReturnNLP, ReturnNLPSegment
from nlu.constants import DialogAct as DialogActEnum
from nlu.friendship_acitvities_classifier import FriendshipActivityClassifier, CommonActivity

YES_NO_OPEN_QUESTION = r"\b(is|are|was) there (something|anything|any)\b|\bcould you tell me\b|\b(something|anything) else you\b|\b(?<!what )have you\b|\bdo you (have|want)\b|\bare you up to\b|^anything\b|did you come back"

TEMPORARY_BLOCK_MODULES = []

REGEX_NEG_ANSWER = r"^na$|^nope$|^nah$|^no$|^no (thank you|thanks|i'm good|i am good)$|^not really$|^(i'm|i am) not (interested|a)\b|^i (don't|do not)|i .*stay away from|^not much|i (don't|do not) (wanna|want) talk|^not video games|^i (cannot|can't|don't) travel"
REGEX_POS_ANSWER = r"^yes$|^sort of$|^sure$|^why not|\btell me more\b"


amazon_intent_map = {
    # intents to pass certification test
    # TODO: remove it after verification
    'LaunchRequestIntent': 'LAUNCHGREETING'
}


dialogflow_modules = [
    "MOVIECHAT",
    "BOOKCHAT",
    "TECHSCIENCECHAT",
    "TRAVELCHAT",
    "GAMECHAT",
    "SPORT",
    "MUSICCHAT",
    "HOLIDAYCHAT",
    "NEWS",
    "ANIMALCHAT",
    "WEATHER",
    "SAY_COMFORT",
    "SOCIAL",
    "FOODCHAT",
    "FASHIONCHAT",
    "DAILYLIFE",
    "OUTDOORCHAT",
    "SELFDISCLOSURE",
]

other_key = "others"

set_logger_level(get_working_environment())


def getCustomModules(curr_utt, global_senti, return_nlp, central_elem, amazon_intents, state_manager):
    logging.info('[Selecting Strategy] get custom modules...')

    user_attributes = state_manager.user_attributes
    last_state = state_manager.last_state
    last_response = last_state.get('response', None) if last_state else ""
    last_module = user_attributes.last_module

    propose_module = get_propose_topic(user_attributes)
    module_selector = ModuleSelector(user_attributes)
    user_profile = UserProfile(user_attributes)
    if propose_module:
        module_selector.add_used_topic_module(propose_module)

    central_features = get_features_from_central_elements(central_elem)

    nlu = read_returnnlp(return_nlp)
    #module detection list with amazon 2019 API
    list_module_detection_with_amazon = extract_module_list_with_amazon_detection(return_nlp, central_features)

    #build question detector
    question_detector = QuestionDetector(curr_utt, return_nlp)

    # added pos and neg dialog act detection in return nlp
    pos_ans_in_nlu, neg_ans_in_nlu = detect_pos_and_neg_ans_in_any_segments(return_nlp)

    #added minor element to detect answer for open question
    minor_elem = search_question_after_answer(last_module, nlu, return_nlp, central_features)
    minor_features = None
    if minor_elem != None:
        minor_features = get_features_from_minor_elements(minor_elem)
    print_inputs_for_local_env(central_elem, global_senti, last_module, propose_module, return_nlp)
    # check whether there is default Amazon intents
    module_candidates = [amazon_intent_map[intent]
                    for intent in amazon_intent_map.keys() if intent in amazon_intents]
    logging.info(
        '[Selecting Strategy] get amazon module: {}'.format(module_candidates))

    logging.info(
        '[Selecting Strategy] get keyword module: {}'.format(central_features.topic_modules))


    # proceed if no default Amazon intents
    if len(module_candidates) == 0:
        logging.info("[Selecting Strategy][Rule] no amazon module name found")
        #profanity topic detection
        if check_if_profanity_topic(central_features):
            logging.info("[Selecting Strategy][Rule]: sensitive words")
            append_module_candidate_and_reset_propose_continue(module_candidates, custom_intent_topic_map["topic_controversial"], user_attributes,
                                                               True)
        # DISABLE BEFORE THE FINAL
        elif check_if_terminate_chat(central_features):
            logging.info("[Selecting Strategy][Rule]: terminate chat")
            append_module_candidate_and_reset_propose_continue(module_candidates, custom_intent_sys_map["terminate"], user_attributes,
                                                               False)
        #special cases match
        elif special_match(central_features, last_module, propose_module):
            logging.info("[Selecting Strategy][Rule]: special logic for edge cases")
            special_match_module = special_match(central_features, last_module, propose_module)
            append_module_candidate_and_reset_propose_continue(module_candidates, special_match_module, user_attributes,
                                                               False)
        # jump intent
        elif check_if_change_topic(central_features) or check_if_get_off_topic(central_features):
            logging.info("[Selecting Strategy][Rule]: change topic")
            if (last_module == module_selector.get_propose_continue_module()) and (last_module in ["NEWS"]):
                append_module_candidate_and_reset_propose_continue(module_candidates, 'NEWS', user_attributes,
                                                                   False)
            elif (len(central_features.topic_modules) == 0 or check_if_get_off_topic(central_features)):
                append_module_candidate_and_reset_propose_continue(module_candidates, "SOCIAL", user_attributes,
                                                                   False)
            else:
                bool_should_reset = (central_features.topic_modules[0] != last_module)
                append_module_candidate_and_reset_propose_continue(module_candidates, central_features.topic_modules[0], user_attributes,
                                                                   bool_should_reset)

        elif check_if_select_wrong(central_features):
            logging.info("[Selecting Strategy][Rule]: select wrong modules")
            append_module_candidate_and_reset_propose_continue(module_candidates, "SOCIAL", user_attributes,
                                                               False)
        # <--- Special Opening Logic ---> #
        elif check_if_opening(central_features):
            logging.info("[Selecting Strategy][Rule]: name correction, or answer users' names")
            append_module_candidate_and_reset_propose_continue(module_candidates, "LAUNCHGREETING", user_attributes,
                                                               False)

        # one turn service
        elif predict_custom_sys_intent(central_features, user_attributes, return_nlp, last_state, last_module):
            custom_sys_intent = predict_custom_sys_intent(central_features, user_attributes, return_nlp, last_state, last_module)
            system_topic_module = mapping_from_sys_intent_to_module(custom_sys_intent, last_module)
            logging.info("[Selecting Strategy][Rule]: match system topic module: {}".format(system_topic_module))
            append_module_candidate_and_reset_propose_continue(
                module_candidates, system_topic_module, user_attributes, should_reset_propose_continue(custom_sys_intent))
        #controversial topic
        elif check_if_controversial_topic(central_features):
            logging.info("[Selecting Strategy][Rule]: controversial topic intent")
            append_module_candidate_and_reset_propose_continue(module_candidates, custom_intent_topic_map['topic_controversial'],
                                                               user_attributes,
                                                               True)
        #finance topic
        elif check_if_finance_topic(central_features):
            logging.info("[Selecting Strategy][Rule]: finance topic intent")
            append_module_candidate_and_reset_propose_continue(module_candidates, custom_intent_topic_map['topic_finance'],
                                                               user_attributes,
                                                               True)
        # QUICK FIX FOR QUESTION
        # and last_module in ['STORYFUNFACT', 'SAY_FUNNY']
        # jump to specific topics
        elif check_if_jump_topic_command(central_features, last_module, propose_module, neg_ans_in_nlu):
            logging.info("[Selecting Strategy][Rule]: jump to specific topics")
            append_req_jump_topic(propose_module, central_features, module_candidates, last_module, user_attributes)

        # jump to specific topic by a single word
        elif check_if_jump_topic_by_single_word(central_features, return_nlp, last_module):
            logging.info(
                "[Selecting Strategy][Rule]: jump to specific topics by single word requests")
            if central_features.text.split()[-1] == 'news':
                append_module_candidate_and_reset_propose_continue(module_candidates, 'NEWS', user_attributes,
                                                                   False)
            else:
                append_req_jump_topic(propose_module, central_features, module_candidates, last_module,
                                      user_attributes)

        # check if append last module
        elif check_if_append_last_module(central_features, last_module, user_attributes, last_response, propose_module):
            append_module_candidate_and_reset_propose_continue(module_candidates, last_module, user_attributes,
                                                               False)

        #check if append back story
        elif check_if_append_backstory(central_features):
            logging.info(
                "[Selecting Strategy][Rule]: utt with high backstory conf and module tag")
            append_module_candidate_and_reset_propose_continue(module_candidates, central_features.backstory['module'], user_attributes,
                                                               False)
        #check if jump to weather module
        elif check_if_jump_to_weather(central_features):
            logging.info("[Selecting Strategy][Rule]: ask weather jump out")
            append_module_candidate_and_reset_propose_continue(module_candidates, "WEATHER", user_attributes,
                                                               False)
        #command dialog act but noun phrase instead of specific topic, use retrieval
        elif check_if_command_without_topic(central_features, last_module):
            if 'topic_dailylife' in central_features.custom_intents['topic'] and not last_module in ['DAILYLIFE']:
                    logging.info(
                        "[Selecting Strategy][Rule]: system cannot handle the topic but daily life can do the transition")
                    append_module_candidate_and_reset_propose_continue(module_candidates, "DAILYLIFE",
                                                                       user_attributes,
                                                                       True)
            else:
                logging.info("[Selecting Strategy][Rule]: jumpout request but system cannot handle the topic")
                append_module_candidate_and_reset_propose_continue(module_candidates, "RETRIEVAL", user_attributes,
                                                                True)
        # <--- Special Last Module Logic Rules ---> #
        elif check_if_special_last_modules_cases(user_attributes, last_module):
            logging.info("[Selecting Strategy][Rule]: grounding for specific jumpout")
            user_attributes.socialchat['curr_state'] = 's_grounding_topic'
            append_module_candidate_and_reset_propose_continue(module_candidates, "SOCIAL", user_attributes,
                                                               False)

        #<---bad response to how are you question in launchgreeting, go to comfort chat--->
        elif check_if_need_saycomfort_in_opening(central_features, last_module, nlu):
            logging.info("[Selecting Strategy][Rule]: bad response to how are you question in launchgreeting, go to comfort chat")
            append_module_candidate_and_reset_propose_continue(module_candidates,
                                                               custom_intent_topic_map['topic_saycomfort'],
                                                               user_attributes,
                                                               True)

        #selected module continues
        elif module_selector.get_propose_continue_module():
            logging.info("[Selecting Strategy][Rule]: select modules propose to continue")
            append_module_candidate_and_reset_propose_continue(module_candidates, module_selector.get_propose_continue_module(), user_attributes,
                                                               False)
        ##### <--- Topic Level Rule Base ---> #####
        #if specific module propose topic and is unclear
        elif check_if_last_topic_module_unclear_and_propose_module(user_attributes, last_module, propose_module, pos_ans_in_nlu, neg_ans_in_nlu):
            if neg_ans_in_nlu:
                logging.info("[Selecting Strategy][Rule]: neg ans to the propose topic in unclear module, enter in the last module")
                append_module_candidate_and_reset_propose_continue(module_candidates, last_module, user_attributes,
                                                                   False)
            elif pos_ans_in_nlu:
                logging.info("[Selecting Strategy][Rule]: pos ans to the propose topic in unclear module, enter in the propose module")
                append_module_candidate_and_reset_propose_continue(module_candidates, propose_module, user_attributes,
                                                                   False)

        # enter the proposed module if system propose subtopic directly
        elif is_proposing_subtopic(user_attributes, propose_module, neg_ans_in_nlu):
            logging.info("[Selecting Strategy][Rule]: proposing subtopic")
            append_module_candidate_and_reset_propose_continue(module_candidates, propose_module,
                                                               user_attributes,
                                                               False)

        elif check_if_pos_ans_but_prefer_others(propose_module, pos_ans_in_nlu, central_features):
            logging.info("[Selecting Strategy][Rule]: but prefer another topic")
            append_req_jump_topic(propose_module, central_features, module_candidates, last_module, user_attributes)

        #pos_ans following the propose topic
        elif check_if_pos_ans_following_propose_topic(propose_module, pos_ans_in_nlu):
            logging.info("[Selecting Strategy][Rule]: pos ans to the propose topic, enter in the propose topic")
            append_module_candidate_and_reset_propose_continue(module_candidates, propose_module,
                                                               user_attributes,
                                                               False)
        #pos_ans following the open question "Have you done anything fun lately?"
        elif check_if_pos_ans_following_open_question(user_attributes, last_module, last_response, central_features, pos_ans_in_nlu):
            logging.info("[Selecting Strategy][Rule]: pos ans to the open question, go back to launch greeting")
            user_attributes.open_question_has_pos_ans = True
            if user_attributes.social_open_question_proposing == True:
                append_module_candidate_and_reset_propose_continue(module_candidates, "SOCIAL",
                                                                   user_attributes,
                                                                   False)
            else:
                append_module_candidate_and_reset_propose_continue(module_candidates, "LAUNCHGREETING",
                                                               user_attributes,
                                                               False)
        
        # elif check_if_daily_life_in_friendship(curr_utt, user_attributes, module_selector):
        #     logging.info("[Selecting Strategy][Rule]: daily life friendship proposes open question and detect daily life topic, go back to dailylife")
        #     append_module_candidate_and_reset_propose_continue(module_candidates, "DAILYLIFE", user_attributes, False)

        elif check_if_not_negative_sentiment_to_topic(return_nlp, minor_features, central_features, propose_module, neg_ans_in_nlu, list_module_detection_with_amazon, user_attributes):
            #search topics in all segments when last module is launch greeting and ask open question
            preferred_module_list = []
            if last_module in ['LAUNCHGREETING'] and user_attributes.social_open_question_proposing == True:
                preferred_module_list = get_detected_modules_from_all_segments(return_nlp)
                preferred_entity_topics_list = select_preferred_entity_topics_from_all_segments(return_nlp)
                logging.info("[Selecting Strategy][Rule]: user answer hobbies and interests, detect topic in central elem but search all detected topics: {}".format(preferred_module_list))
                #set this list to user profile
                user_profile.add_preferred_entity_topics(preferred_entity_topics_list)
                logging.info(f"[Detected entities from open question] user utterance: {curr_utt},"
                             f"preferred_entity_topics_list: {preferred_entity_topics_list}")
            if user_attributes.social_open_question_proposing == True:
                if minor_features != None:
                    list_module_detection_with_amazon_in_minor = extract_module_list_with_amazon_detection(return_nlp, minor_features)
                    logging.info("[Selecting Strategy][Rule]: open_question: choose a module on topic level: minor element")
                    append_req_jump_topic_with_amazon(propose_module, minor_features, module_candidates, last_module,
                                                    user_attributes, list_module_detection_with_amazon_in_minor, preferred_topics=preferred_module_list)
                else:
                    logging.info("[Selecting Strategy][Rule]: open_question: choose a module on topic level: central element")
                    append_req_jump_topic_with_amazon(propose_module, central_features, module_candidates, last_module,
                                    user_attributes, list_module_detection_with_amazon, preferred_topics=preferred_module_list)
            else:
                if minor_features != None:
                    logging.info("[Selecting Strategy][Rule]: choose a module on topic level: minor element")
                    append_req_jump_topic(propose_module, minor_features, module_candidates, last_module, user_attributes)
                else:
                    logging.info("[Selecting Strategy][Rule]: choose a module on topic level: central element")
                    append_req_jump_topic(propose_module, central_features, module_candidates, last_module, user_attributes)
        elif check_if_in_phatic_topic(central_features):
            logging.info("[Selecting Strategy][Rule]: phatic topic")
            append_module_candidate_and_reset_propose_continue(module_candidates,
                                                               custom_intent_topic_map['topic_phatic'],
                                                               user_attributes,
                                                               True)
        elif check_if_in_saycomfort_topic(central_features):
            logging.info("[Selecting Strategy][Rule]: say_comfort without sentiment")
            append_module_candidate_and_reset_propose_continue(module_candidates,
                                                               custom_intent_topic_map['topic_saycomfort'],
                                                               user_attributes,
                                                               False)
        #backstory confidence is high but does not get backstory module
        elif check_if_append_backstory_and_set_state(central_features):
            logging.info("[Selecting Strategy][Rule]: high conf goes to backstory")
            append_module_candidate_and_reset_propose_continue(module_candidates, "SOCIAL",
                                                               user_attributes,
                                                               False)

        elif check_if_has_question(central_features, question_detector):
            logging.info("[Selecting Strategy][Rule]: questions to social at the end")
            append_module_candidate_and_reset_propose_continue(module_candidates, "SOCIAL",
                                                               user_attributes,
                                                               False)
        # user shows positive sentiment to the proposed module
        elif check_if_pos_sent_to_propose_module(propose_module, global_senti, neg_ans_in_nlu):
            logging.info("[Selecting Strategy][Rule]: select the proposed modules")
            append_module_candidate_and_reset_propose_continue(module_candidates, propose_module,
                                                               user_attributes,
                                                               False)
        #user request for a topic
        elif check_if_req_for_a_topic(central_features):
            logging.info("[Selecting Strategy][Rule]: provide a topic for the user")
            append_module_candidate_and_reset_propose_continue(module_candidates, "SOCIAL",
                                                               user_attributes,
                                                               False)

        elif check_if_unclear_proposed_module(last_module, central_features, user_attributes, pos_ans_in_nlu):
            logging.info(
                "[Selecting Strategy][Rule]: module propose unclear to last module: {}".format(last_module))
            append_module_candidate_and_reset_propose_continue(module_candidates, last_module,
                                                               user_attributes,
                                                               False)
        # user shows positive sentiment to the unhandled topic, go to retrieval
        elif check_if_pos_sent_to_unhandled_topic(central_features, propose_module):
            if user_attributes.social_open_question_proposing == True:
                logging.info("[Selecting Strategy][Rule]: open question, unhandled topic")
                open_question_handling(central_features, last_module, module_candidates, module_selector, return_nlp,
                                       user_attributes, user_profile, curr_utt)
            else:
                logging.info("[Selecting Strategy][Rule]: topics cannot be handled go to retrieval")
                append_module_candidate_and_reset_propose_continue(module_candidates, "RETRIEVAL",
                                                                   user_attributes,
                                                                   True)
        # topic proposed by social lower than topic classifier
        elif check_if_pos_sent_to_proposed_module(propose_module, central_features, neg_ans_in_nlu):
            logging.info("[Selecting Strategy][Rule]: second time select the proposed modules")
            append_module_candidate_and_reset_propose_continue(module_candidates, propose_module,
                                                               user_attributes,
                                                               False)
        # user shows negative sentiment to last module
        elif user_show_neg_senti_to_last_module(last_module, central_features, propose_module, neg_ans_in_nlu):
            #check when neg_answer to the propose question, if we detect any topic in other segments, eg: no, i like music
            detected_topic = get_topic_in_other_segments(nlu, propose_module)
            if detected_topic and detected_topic not in TEMPORARY_BLOCK_MODULES:
                logging.info(
                    "[Selecting Strategy][Rule]: negative feedback for propose and detect loved topic, go to topic")
                append_module_candidate_and_reset_propose_continue(module_candidates, detected_topic,
                                                               user_attributes,
                                                               False)
            else:
                logging.info(
                    "[Selecting Strategy][Rule]: negative feedback for propose, go back to Social")
                module_selector.add_disliked_topic_module(propose_module)
                append_module_candidate_and_reset_propose_continue(module_candidates, "SOCIAL",
                                                                user_attributes,
                                                                False)
        # topic module unclear, not pos ans, and no neg senti in user utterance, go back to last module
        elif check_if_unclear_last_module(last_module, user_attributes):
            logging.info(
                "[Selecting Strategy][Rule]: 2nd module propose unclear to last module: {}".format(last_module))
            append_module_candidate_and_reset_propose_continue(module_candidates, "SOCIAL",
                                                               user_attributes,
                                                               False)
        # other cases
        else:
            #when last turn in social and user say unmeaningful words, don't go to retrieval
            if user_attributes.social_open_question_proposing == True:
                logging.info("[Selecting Strategy][Rule]: open question, non meaningful words")
                open_question_handling(central_features, last_module, module_candidates, module_selector, return_nlp,
                                       user_attributes, user_profile, curr_utt)
            elif retrieval_in_generation_mode(state_manager) and last_module == TopicModule.RETRIEVAL.value:
                logging.info("[Selecting Strategy][Rule]: last turn in retrieval, non meaningful words, go to retrieval again")
                append_module_candidate_and_reset_propose_continue(module_candidates, "RETRIEVAL",
                                                                   user_attributes,
                                                                   False)
            else:
                logging.info("[Selecting Strategy][Rule]: non meaningful words, go to social")
                append_module_candidate_and_reset_propose_continue(module_candidates, "SOCIAL",
                                                                   user_attributes,
                                                                   False)
    if not module_candidates:
        module_candidates.append('RETRIEVAL')

    # Some necessary post processing
    module_candidates = unique(module_candidates)
    check_propose_continue(user_attributes, module_candidates[0])

    module_selector.add_used_topic_module(module_candidates[0])

    # UPDATE MODULE RANK
    module_selector.update_module_rank(module_candidates, return_nlp)

    return module_candidates

def retrieval_in_generation_mode(state_manager):
    a_b_test = getattr(state_manager.current_state, "a_b_test", "")
    return a_b_test == 'C'

def open_question_handling(central_features, last_module, module_candidates, module_selector, return_nlp,
                           user_attributes, user_profile, user_utterance=""):
    preferred_module_list = []
    if last_module in ['LAUNCHGREETING']:
        preferred_module_list = get_detected_modules_from_all_segments(return_nlp)
        preferred_entity_topics_list = select_preferred_entity_topics_from_all_segments(return_nlp)
        logging.info(
            "[Selecting Strategy][Rule]: user answer hobbies and interests, no noun phrase in cetral elem, search all detected topics: {}".format(
                preferred_module_list))
        # set this list to user profile
        user_profile.add_preferred_entity_topics(preferred_entity_topics_list)
        logging.info(f"[Detected entities from open question] user utterance: {user_utterance},"
                     f"preferred_entity_topics_list: {preferred_entity_topics_list}")

    if preferred_module_list:
        logging.info("[Selecting Strategy][Rule]: no noun phrase in central elem, detect topic in other segment")
        append_module_candidate_and_reset_propose_continue(module_candidates, preferred_module_list[0],
                                                           user_attributes,
                                                           True)
    elif 'topic_dailylife' in central_features.custom_intents['topic'] or module_selector.global_state == GlobalState.FRIENDSHIP_OPEN_QUESTION:
        logging.info("[Selecting Strategy][Rule]: daily life answers to the open question, go to daily life bot")
        append_module_candidate_and_reset_propose_continue(module_candidates, "DAILYLIFE",
                                                           user_attributes,
                                                           False)
    else:
        logging.info("[Selecting Strategy][Rule]: unhandled answers to the open question, go to social")
        append_module_candidate_and_reset_propose_continue(module_candidates, "SOCIAL",
                                                           user_attributes,
                                                           False)


def append_module_candidate_and_reset_propose_continue(module_candidates, module, user_attributes, should_reset):
    module_candidates.append(module)
    if should_reset:
        reset_propose_continue(user_attributes)


def get_features_from_central_elements(central_elem) -> CentralElementFeatures:
    dialog_act = get_filtered_dialog_act(central_elem)

    return CentralElementFeatures(central_elem['text'],
                                  dialog_act,
                                  central_elem['regex'],
                                  central_elem['senti'],
                                  central_elem['np'],
                                  central_elem['key_phrase'],
                                  central_elem['module'],
                                  central_elem['backstory'])


def get_features_from_minor_elements(minor_elem) -> CentralElementFeatures:
    dialog_act = get_filtered_dialog_act(minor_elem)

    return CentralElementFeatures(minor_elem['text'],
                                  dialog_act,
                                  minor_elem['regex'],
                                  minor_elem['senti'],
                                  minor_elem['np'],
                                  minor_elem['key_phrase'],
                                  minor_elem['module'],
                                  {})


def get_filtered_dialog_act(central_elem):
    if central_elem['DA_score'] is None:
        DA = None
    elif float(central_elem['DA_score']) < 0.45:
        DA = None
    elif float(central_elem['DA2_score']) > 0.45 and central_elem['DA2'] == "commands":
        DA = central_elem['DA2']
    else:
        DA = central_elem['DA']
    return DA


def print_inputs_for_local_env(central_elem, global_senti, last_module, propose_module, return_nlp):
    if get_working_environment() == "local":
        from pprint import pprint
        print("[DEBUG] last module: {}".format(last_module))
        print("[DEBUG] propose module: {}".format(propose_module))
        print("[DEBUG] global senti: {}".format(global_senti))
        print("[DEBUG] return nlp:")
        pprint(return_nlp, width=120)
        print("[DEBUG] central elem:")
        pprint(central_elem, width=120)
        # setattr(user_attributes, "usr_name", None)


def read_returnnlp(returnnlp):
    logging.info('[UTILS][NLP] return nlp: {}'.format(returnnlp))
    if not returnnlp:
        logging.info('[UTILS][NLP] return nlp is none')
        return [{'senti': None,
                 'regex': {'sys': [],
                           'topic': [],
                           'lexical': []},
                 'DA': None,
                 'DA_score': None,
                 'DA2': None,
                 'DA2_score': None,
                 'module': [],
                 'np': [],
                 'key_phrase': [],
                 'text': ''
                 }]
    nlu = []

    for segment in returnnlp:

        conf = 0
        sentiment = other_key
        if 'sentiment' in segment and segment['sentiment'] is not None and type(segment['sentiment']) is dict:
            for tag in segment['sentiment']:
                if float(segment['sentiment'][tag]) > conf:
                    conf = float(segment['sentiment'][tag])
                    sentiment = tag
        senti = get_sentiment_key(sentiment, segment['intent']['lexical'])

        returnnlp_segment = ReturnNLPSegment.from_dict(segment)
        elem = {
            'senti': senti,
            'regex': segment.get('intent'),
            'DA': segment.get('dialog_act')[0] if segment.get('dialog_act') else None,
            'DA_score': segment.get('dialog_act')[1] if segment.get('dialog_act') else None,
            'DA2': segment.get('dialog_act2')[0] if segment.get('dialog_act2') else None,
            'DA2_score': segment.get('dialog_act2')[1] if segment.get('dialog_act2') else None,
            'module': returnnlp_segment.detect_topic_module_from_system_level(consider_entertainment_ner=True),
            'np': segment.get('noun_phrase'),
            'key_phrase': segment.get('keyphrase'),
            'text': segment.get('text')
        }
        nlu.append(elem)
    logging.info('[UTILS][NLP] get nlu: {}'.format(nlu))
    return nlu


def is_short_utterance(text):
    return len(text.split()) < 8


def is_clarification_request(sys_intents):
    return "clarify" in sys_intents \
        or "clarify_misunderstood" in sys_intents


def predict_custom_sys_intent(central_features, user_attributes, returnnlp, last_state, last_module=None):
    _dialog_act = central_features.dialog_act
    _sys_intents = central_features.custom_intents['sys']
    not_open_question = not last_state.get("bot_ask_open_question", False)
    regex_instrument = r"\binstrument\b"
    regex_artist = r"\bcelebrity\b|\bstar\b|\bartist\b|\banalyst\b"
    if "req_play_game" in central_features.custom_intents['sys'] and \
            is_short_utterance(central_features.text):
        return "req_playgame"
    elif "req_play_music" in central_features.custom_intents['sys'] \
            and ('MUSICCHAT' in central_features.topic_modules or search_results_in_concept(returnnlp, regex_artist)) \
            and is_short_utterance(central_features.text) and not search_results_in_concept(returnnlp, regex_instrument):
        return "req_playmusic"
    # elif "req_task" in custom_intents['sys'] and not topic_module_list and len(text.split()) < 10:
    elif (not_open_question and _dialog_act == "dev_command" and "req_play_music" not in _sys_intents) \
    or ("req_task" in _sys_intents) \
    or (not_open_question and _dialog_act == "commands" and "req_task_special" in _sys_intents):
        return "req_task"

    elif "adjust_speak" in central_features.custom_intents['sys'] and \
            not central_features.topic_modules:
        return "adjust_speak"
    elif is_clarification_request(_sys_intents) and \
            not central_features.topic_modules and has_no_consecutive_clarifications(user_attributes):
        return "clarify"
    elif "say_bio" in central_features.custom_intents['sys'] and \
            central_features.sentiment != "neg":
        return "say_bio"
    elif "req_time" in central_features.custom_intents['sys']:
        return "req_time"

    return None


def should_reset_propose_continue(custom_sys_intent_map_key):
    reset_list = ["req_playgame", "req_task", "adjust_speak", "req_time"]
    return custom_sys_intent_map_key in reset_list

def read_googlekg(knowledge_list):
    knowledges = []
    for k in knowledge_list:
        if k and len(k) == 4:
            confidence = float(k[2])
            #increase movie threshold, googlekg returns too much movie name results.
            if k[3] in ['movie']:
                if confidence > 1000:
                    knowledges.append((confidence, k[3]))
            elif k[3] in ['music', 'animals', 'travel']:
                if confidence > 600:
                    knowledges.append((confidence, k[3]))
            else:
                if confidence > 500:
                    knowledges.append((confidence, k[3]))
    knowledges.sort(reverse=True)
    return [k[1] for k in knowledges]


def special_match(central_features, last_module, propose_module):
    text = central_features.text
    module_list = central_features.topic_modules
    if re.search(r"\bcurry\b", text) and 'SPORT' in [propose_module, last_module]:
        return 'SPORT'
    elif module_list and module_list[0] == 'WEATHER' and 'NEWS' in [propose_module, last_module]:
        return 'WEATHER'
    elif 'SPORT' in module_list and re.search(r"\b(game|games)\b", text) and not re.search(r"video (game|games)", text) and 'GAMECHAT' not in [last_module]:
        return 'SPORT'
    elif 'SAY_FUNNY' == last_module and re.search(r"\bha ha\b", text):
        return 'SAY_FUNNY'
    elif 'FOODCHAT' in module_list and propose_module != 'FOODCHAT' and 'ANIMALCHAT' in [last_module, propose_module] \
            and not 'topic_food' in central_features.custom_intents['topic']:
        return 'ANIMALCHAT'
    elif 'ANIMALCHAT' in module_list and propose_module != 'ANIMALCHAT' and 'FOODCHAT' in [last_module, propose_module] \
            and not 'topic_animal' in central_features.custom_intents['topic']:
        return 'FOODCHAT'
    return False


def check_if_mentioned(np, last_response):
    if last_response is None:
        return False
    for phrase in np:
        if phrase in last_response:
            return True
    return False

def append_req_jump_topic(propose_module, central_features, module_candidates, last_module, user_attributes):
    if "besides_topic" in central_features.custom_intents['topic']:
        if central_features.topic_modules:
            module_selector = ModuleSelector(user_attributes)
            module_selector.add_used_topic_module(central_features.topic_modules[0])
        append_module_candidate_and_reset_propose_continue(module_candidates, "SOCIAL", user_attributes,
                                                           False)
    elif propose_module in central_features.topic_modules:
        append_module_candidate_and_reset_propose_continue(module_candidates, propose_module, user_attributes,
                                                           False)
    elif last_module in central_features.topic_modules:
        append_module_candidate_and_reset_propose_continue(module_candidates, last_module, user_attributes,
                                                           False)
    else:
        append_module_candidate_and_reset_propose_continue(module_candidates, central_features.topic_modules[0], user_attributes,
                                                           True)

def pronoun_in_text(central_features):
    return re.search(
                r"\b(it|that|this|those|one|them|him|his|her|their|he|she|our|another)\b", central_features.text)

def user_show_neg_senti_to_last_module(last_module, central_features, propose_module, neg_ans_in_nlu):
    return  propose_module and neg_ans_in_nlu

#<-----------------All condition checking methods---------------->#
#detect profanity topic
def check_if_profanity_topic(central_features):
    return "topic_profanity" in central_features.custom_intents['topic']

#terminate chat
def check_if_terminate_chat(central_features):
    return "terminate" in central_features.custom_intents['sys']

#user wants to change topic, jump intent
def check_if_change_topic(central_features):
    return "change_topic" in central_features.custom_intents['sys']

def check_if_get_off_topic(central_features):
    return "get_off_topic" in central_features.custom_intents['sys']

#selected wrong modules
def check_if_select_wrong(central_features):
    return "select_wrong" in central_features.custom_intents['sys']

#opening logic
def check_if_opening(central_features):
    return "info_name" in central_features.custom_intents['lexical'] or "ask_user_name" in central_features.custom_intents['lexical']


#controversial topics
def check_if_controversial_topic(central_features):
    return ('topic_controversial' in central_features.custom_intents['topic']) and (check_intersect(
        ['ask_opinion', 'ask_preference',
         'ask_yesno', 'ask_advice', 'ask_ability', 'ask_loc'],
        central_features.custom_intents['lexical']) or "req_topic_jump" in central_features.custom_intents[
        'sys'])

def check_if_finance_topic(central_features):
    return ('topic_finance' in central_features.custom_intents['topic']) and (check_intersect(['ask_opinion', 'ask_preference',
                                                                                       'ask_yesno', 'ask_advice', 'ask_ability', 'ask_loc'],
                                                                                      central_features.custom_intents['lexical'])
                                                                 or "req_topic_jump" in central_features.custom_intents['sys'])

#user wants to jump to specific topic
def check_if_jump_topic_command(central_features, last_module, propose_module, neg_ans_in_nlu):
    #add special case for topic entertainment in news
    if (("req_topic_jump_single" not in central_features.custom_intents['sys'] and
         central_features.dialog_act in ["commands"]) or "req_topic_jump" in central_features.custom_intents[
        'sys'] or "question_topic_jump" in central_features.custom_intents[
        'sys']) and central_features.topic_modules and not ("topic_entertainment" in central_features.custom_intents[
        'topic'] and last_module in ["NEWS"]) and "req_not_jump" not in central_features.custom_intents[
        'sys'] and "ans_dislike" not in central_features.custom_intents['lexical']:
        return True
    else:
        return False

#user wants to jump to module by a single word
def check_if_jump_topic_by_single_word(central_features, return_nlp, last_module):
    return "req_topic_jump_single" in central_features.custom_intents['sys'] and len(
                return_nlp) == 1 and central_features.topic_modules and last_module not in ["NEWS", "TRAVELCHAT"]

#append last module
def check_if_append_last_module(central_features, last_module, user_attributes, last_response, propose_module):
    module_selector = ModuleSelector(user_attributes)
    if "req_more" in central_features.custom_intents['sys'] and not propose_module:
        logging.info(
            "[Selecting Strategy][Rule]: select last module when the user requests more")
        return True
    elif check_if_mentioned(central_features.key_phrase, last_response) and \
            last_module in [module_selector.get_propose_continue_module(), "RETRIEVAL"] and \
            last_module not in ["LAUNCHGREETING"] and \
            user_attributes.retrieval_propose is not True and \
            not propose_module:
        logging.info(
            "[Selecting Strategy][Rule]: check if np is mentioned and propose continue")
        return True
    else:
        return False

#back story confidence is high and backstory module is not none
def check_if_append_backstory(central_features):
    return central_features.backstory['confidence'] > 0.82 and central_features.backstory.get('module') is not None and central_features.backstory.get('module') in dialogflow_modules

#jump to weather module
def check_if_jump_to_weather(central_features):
    return (central_features.dialog_act in ['open_question', 'open_question_opinion',
                    'open_question_factual'] or "ask_fact" in central_features.custom_intents['lexical']) and "WEATHER" in central_features.topic_modules and len(central_features.topic_modules) == 1

#command dialog act but noun phrase instead of specific topic, use retrieval
def check_if_command_without_topic(central_features, last_module):
    return "req_topic_jump" in central_features.custom_intents['sys'] and \
           not central_features.topic_modules and not pronoun_in_text(central_features)\
                and len(central_features.key_phrase) > 0 and last_module not in ["NEWS"]

#special last module cases
def check_if_special_last_modules_cases(user_attributes, last_module):
    return user_attributes.previous_modules is not None and isinstance(user_attributes.previous_modules, list) and \
            user_attributes.previous_modules[1] in dialogflow_modules and \
            last_module in ['STORYFUNFACT', 'SAY_FUNNY', custom_intent_topic_map['topic_controversial']]

#topic level module selection
def check_if_last_topic_module_unclear_and_propose_module(user_attributes, last_module, propose_module, pos_ans_in_nlu, neg_ans_in_nlu):
    if propose_module and last_module_propose_unclear(user_attributes, last_module) and (pos_ans_in_nlu or neg_ans_in_nlu):
        return True
    else:
        return False

def is_proposing_subtopic(user_attributes, propose_module, neg_ans_in_nlu):
    module_selector = ModuleSelector(user_attributes)
    all_modules_discussed = getattr(user_attributes, "all_modules_discussed", None)
    return  module_selector.global_state != GlobalState.NA and propose_module and not (all_modules_discussed and neg_ans_in_nlu)

#pos_ans to the proposed topic
def check_if_pos_ans_following_propose_topic(propose_module, pos_ans_in_nlu):
    if propose_module and pos_ans_in_nlu:
        return True
    else:
        return False


#pos_ans to the open_question
def check_if_pos_ans_following_open_question(user_attributes, last_module, last_response, central_features, pos_ans_in_nlu):
    if user_attributes.social_open_question_proposing == True and last_response and re.search(YES_NO_OPEN_QUESTION, last_response.lower()) and \
        pos_ans_in_nlu and not central_features.topic_modules and 'topic_dailylife' not in central_features.custom_intents['topic']:
        return True
    else:
        return False

def check_if_not_negative_sentiment_to_topic(return_nlp, minor_features, central_features, propose_module, neg_ans_in_nlu, list_module_detection_with_amazon, user_attributes):
    if user_attributes.social_open_question_proposing == True:
        if minor_features != None:
            list_module_detection_with_amazon_in_minor = extract_module_list_with_amazon_detection(return_nlp, minor_features)
            return len(list_module_detection_with_amazon_in_minor) > 0 and not (propose_module and neg_ans_in_nlu)
        else:
            #special case for "hangout with friends, googlekg returns movie"
            return len(list_module_detection_with_amazon) > 0 and not (propose_module and neg_ans_in_nlu)
    else:
        if minor_features != None:
            return len(minor_features.topic_modules) > 0 and not (propose_module and neg_ans_in_nlu)
        else:
            return len(central_features.topic_modules) > 0 and not (propose_module and neg_ans_in_nlu)

#phatic topic
def check_if_in_phatic_topic(central_features):
    return "topic_phatic" in central_features.custom_intents['topic']

#saycomfort topic
def check_if_in_saycomfort_topic(central_features):
    return "topic_saycomfort" in central_features.custom_intents['topic']

#in back story
def check_if_append_backstory_and_set_state(central_features):
    return central_features.backstory['confidence'] > 0.82

#check if a question in the utterance, go to question handler in social
def check_if_has_question(central_features, question_detector):
    #has a question
    return question_detector.has_question()

#check if pos sent to the proposed module
def check_if_pos_sent_to_propose_module(propose_module, global_senti, neg_ans_in_nlu):
    if propose_module and not neg_ans_in_nlu and global_senti != 'neg':
        return True
    else:
        return False

#check if request for a topic
def check_if_req_for_a_topic(central_features):
    return "req_topic" in central_features.custom_intents['topic'] and not central_features.topic_modules

#module propose unclear
def check_if_unclear_proposed_module(last_module, central_features, user_attributes, pos_ans_in_nlu):
    return last_module is not None and last_module in dialogflow_modules and pos_ans_in_nlu and not module_propose_stop(
                user_attributes, last_module)

#neg sent to the topic
def check_if_pos_sent_to_unhandled_topic(central_features, propose_module):
    if central_features.key_phrase and not propose_module:
        return True
    else:
        return False

#pos sent to the proposed module
def check_if_pos_sent_to_proposed_module(propose_module, central_features, neg_ans_in_nlu):
    if propose_module and central_features.sentiment != 'neg' and not neg_ans_in_nlu:
        return True
    else:
        return False

#unclear last module
def check_if_unclear_last_module(last_module, user_attributes):
    return last_module is not None and last_module in dialogflow_modules and not module_propose_stop(user_attributes,
                                                                                                     last_module)

def detect_pos_and_neg_ans_in_any_segments(return_nlp):
    pos_ans = False
    neg_ans = False
    ret_nlp_ins = ReturnNLP.from_list(return_nlp)
    pos_ans = ret_nlp_ins.has_dialog_act('pos_answer')
    neg_ans = ret_nlp_ins.has_dialog_act('neg_answer')
    #if no neg_ans, search if match ans_dislike regex
    if not neg_ans:
        neg_ans = ret_nlp_ins.has_intent('ans_dislike') or (ret_nlp_ins.has_intent('ans_negative') and not ret_nlp_ins.has_intent('ans_like'))
    if not pos_ans:
        pos_ans = ret_nlp_ins.has_intent('ans_positive')
    for segment_text in ret_nlp_ins.text:
        # special regex to detect negative answer
        if re.search(REGEX_NEG_ANSWER, segment_text) and not ret_nlp_ins.has_intent('ans_like'):
            neg_ans = True
        # special regex to detect positive answer
        if re.search(REGEX_POS_ANSWER, segment_text):
            pos_ans = True
    #when we have neg ans, we neglect the pos ans
    if neg_ans == True:
        pos_ans = False
    return pos_ans, neg_ans

def has_no_consecutive_clarifications(user_attributes):
    if user_attributes.previous_modules is not None and isinstance(user_attributes.previous_modules, list) and len(user_attributes.previous_modules) >= 3:
        for prev_module in user_attributes.previous_modules[0:3]:
            if prev_module != 'CLARIFICATION':
                return True
        return False
    else:
        return False

#extract module list with amazon 2019 topic detection
def extract_module_list_with_amazon_detection(returnnlp, central_features):
    logging.info('[UTILS][NLP] extract module list with amazon topic detection')
    if not returnnlp:
        logging.info('[UTILS][NLP] return nlp is none, return, void')
        return []
    module_list_with_amazon = []
    for segment in returnnlp:
        returnnlp_segment = ReturnNLPSegment.from_dict(segment)

        if segment.get('text') == central_features.text:
            module_list_with_amazon = returnnlp_segment.detect_topic_module_from_system_level(consider_entertainment_ner=True, consider_amazon_topic_2019=True)
    logging.info('[UTILS][NLP] get module list with amazon: {}'.format(module_list_with_amazon))
    return module_list_with_amazon

def append_req_jump_topic_with_amazon(propose_module, central_features, module_candidates, last_module, user_attributes, list_module_detection_with_amazon, preferred_topics = None):
    if "besides_topic" in central_features.custom_intents['topic']:
        if list_module_detection_with_amazon:
            module_selector = ModuleSelector(user_attributes)
            module_selector.add_used_topic_module(list_module_detection_with_amazon[0])
        append_module_candidate_and_reset_propose_continue(module_candidates, "SOCIAL", user_attributes,
                                                           False)
    elif propose_module in list_module_detection_with_amazon:
        append_module_candidate_and_reset_propose_continue(module_candidates, propose_module, user_attributes,
                                                           False)
    elif last_module in list_module_detection_with_amazon:
        append_module_candidate_and_reset_propose_continue(module_candidates, last_module, user_attributes,
                                                           False)
    elif list_module_detection_with_amazon[0] in TEMPORARY_BLOCK_MODULES:
        user_attributes.socialchat['propose_block_module'] = list_module_detection_with_amazon[0]
        append_module_candidate_and_reset_propose_continue(module_candidates, "SOCIAL", user_attributes,
                                                           False)
    elif list_module_detection_with_amazon[0] == "RETRIEVAL" and preferred_topics:
        append_module_candidate_and_reset_propose_continue(module_candidates, preferred_topics[0], user_attributes,
                                                            True)
    else:
        append_module_candidate_and_reset_propose_continue(module_candidates, list_module_detection_with_amazon[0], user_attributes,
                                                            True)

def search_results_in_concept(returnnlp, search_regex):
    # search if instrument in return nlp knowledge graph (microsoft concept)
    ret = False
    if not returnnlp:
        logging.info('[UTILS][NLP] return nlp is none')
        return ret

    for sentence in returnnlp:
        concept = sentence.get('concept')
        if not concept:
            continue
        else:
            for noun_data in concept:
                if noun_data and 'data' in noun_data.keys():
                    for category in noun_data['data'].keys():
                        if re.search(search_regex, category):
                            return True
    return ret

def search_question_after_answer(last_module, nlu, returnnlp, central_features):
    # search if the user utterance is answer + question, it would be used in detect topic for the open question response
    #input: last_module, return_nlp, central_features
    #output: CentralElementFeatures
    if returnnlp != None and len(returnnlp) == 2 and last_module in ['LAUNCHGREETING'] and central_features.dialog_act in ["open_question_factual", "open_question_opinion"]:
        ret_nlp_ins = ReturnNLP.from_list(returnnlp)
        if ret_nlp_ins[0].has_dialog_act({DialogActEnum.OPINION, DialogActEnum.STATEMENT}, threshold=(0.45,0.2)) and ret_nlp_ins[1].has_dialog_act({DialogActEnum.OPEN_QUESTION_FACTUAL, DialogActEnum.OPEN_QUESTION_OPINION}, threshold=(0.45,0.2)):
            return nlu[0]
    return None

def check_if_need_saycomfort_in_opening(central_features, last_module, nlu):
    if last_module in ['LAUNCHGREETING', 'SOCIAL']:
        for segment in nlu:
            if "need_comfort" in segment['regex']['sys']:
                return True
    return False

def mapping_from_sys_intent_to_module(system_intent, last_module):
    return custom_intent_sys_map[system_intent]

def get_topic_in_other_segments(nlu, propose_module):
    # when we detect a neg answer after propose a topic, we search if the user claims they like other topic
    #input: nlu, propose_module
    #output: the detected topic
    for segment in nlu:
        if "ans_like" in segment['regex']['lexical'] and segment['module']:
            #eg: i like music more than movie, need to exclude movie
            if re.search(r"\bmore\b", segment['text']):
                filted_list = list(filter(lambda a: a != propose_module, segment['module']))
                if filted_list:
                    return filted_list[0]
            else:
                return segment['module'][0]
    return None

def get_detected_modules_from_all_segments(return_nlp):
    list_modules_in_all_segments = []
    for segment in return_nlp:
        returnnlp_segment = ReturnNLPSegment.from_dict(segment)
        module_detection_on_system_level = returnnlp_segment.detect_topic_module_from_system_level()
        module_detection_by_regex = returnnlp_segment.detect_topics_by_regex()
        #add all the module detected by regex
        for module_by_regex in module_detection_by_regex:
            if module_by_regex in topics_available_for_proposal:
                list_modules_in_all_segments.append(module_by_regex)
        #add the first module in each segment
        if module_detection_on_system_level and module_detection_on_system_level[0] in topics_available_for_proposal:
            list_modules_in_all_segments.append(module_detection_on_system_level[0])
    #unique the list
    list_modules_in_all_segments = unique(list_modules_in_all_segments)
    return list_modules_in_all_segments

def check_if_pos_ans_but_prefer_others(propose_module, pos_ans_in_nlu, central_features):
    if propose_module and pos_ans_in_nlu and "ans_prefer" in central_features.custom_intents['lexical'] and len(central_features.topic_modules) > 0:
        return True
    else:
        return False


def select_preferred_entity_topics_from_all_segments(return_nlp) -> List[UserProfile.PreferrdEntityTopic]:
    preferred_entity_topics_list_in_all_segments = []
    for segment in return_nlp:
        returnnlp_segment = ReturnNLPSegment.from_dict(segment)
        preferred_entity_topics_list_in_all_segments.extend(
            select_preferred_entity_topics_from_single_segment(returnnlp_segment))

    logging.info(f"[select_preferred_entity_topics_from_all_segments] preferred_entity_topics_list_in_all_segments={preferred_entity_topics_list_in_all_segments}")
    return preferred_entity_topics_list_in_all_segments


def select_preferred_entity_topics_from_single_segment(returnnlp_segment: ReturnNLPSegment) \
        -> List[UserProfile.PreferrdEntityTopic]:
    # use ner, do not use amazon 2019 api
    detected_entity_topic_list = returnnlp_segment.detect_entities(
        consider_entertainment_ner=True, consider_amazon_topic_2019=False)

    # get all entity topics detected by regex (most confident modules)
    entity_topics_from_regex_list = returnnlp_segment.get_entity_topic_list_from_regex()
    if entity_topics_from_regex_list:
        # if there is entity topics from regex, add all the entity topics that have same module
        entity_topics_from_regex_modules_list = [elem.module.value for elem in entity_topics_from_regex_list]
        # get all the other entity topics when its module is also in regex
        entity_topics_same_module_list = [
            elem for elem in detected_entity_topic_list
            if elem.source != "regex" and elem.module.value in entity_topics_from_regex_modules_list
        ]
        preferred_entity_topics_list = adapt_preferred_entity_topics(
            entity_topics_from_regex_list + entity_topics_same_module_list)
    else:
        # no regex, get all entity topics detected by ner and return
        entity_topics_from_ner_list = returnnlp_segment.get_entity_topic_list_from_ner()
        if entity_topics_from_ner_list:
            preferred_entity_topics_list = adapt_preferred_entity_topics(entity_topics_from_ner_list)

        else:
            # no regex, no ner, check google kg and concept, if more than 2 prediction of a same module ,return that module.
            entity_topics_from_concept = returnnlp_segment.get_entity_topic_list_from_concept_kg()

            # google's entity type length is always 1
            entity_topics_from_google = returnnlp_segment.get_entity_topic_list_from_google_kg(True)

            # concept's entity type sometimes is also 1
            entity_topics_from_concept_with_single_entity_type = [
                elem for elem in entity_topics_from_concept if len(elem.entity_type) == 1
            ]

            entity_topics_from_google_and_concept = entity_topics_from_concept_with_single_entity_type + \
                                                    entity_topics_from_google

            # from concept, if entity type list is more than 1, add it to preferred list,
            # otherwise check if same module detected in google
            preferred_entity_topics_from_concept = [
                elem for elem in entity_topics_from_concept if len(elem.entity_type) > 1
            ]

            preferred_entity_topics_from_google_and_concept = []
            preferred_entity_topics_from_google_and_concept.extend(preferred_entity_topics_from_concept)

            for index, entity1 in enumerate(entity_topics_from_google_and_concept):
                if entity1 in preferred_entity_topics_from_google_and_concept:
                    continue
                for entity2 in entity_topics_from_google_and_concept[index+1:]:
                    if entity1.module == entity2.module:
                        if entity1 not in preferred_entity_topics_from_google_and_concept:
                            preferred_entity_topics_from_google_and_concept.append(entity1)
                        if entity2 not in preferred_entity_topics_from_google_and_concept:
                            preferred_entity_topics_from_google_and_concept.append(entity2)

            preferred_entity_topics_list = adapt_preferred_entity_topics(
                preferred_entity_topics_from_google_and_concept)

    return preferred_entity_topics_list


def adapt_preferred_entity_topics(entity_topics_list) -> List[UserProfile.PreferrdEntityTopic]:
    preferred_entity_topics_list = []
    preferred_entity_topics_from_kgs = []
    for elem in entity_topics_list:
        if elem.source == "regex":
            preferred_entity_topics_list.append(
                UserProfile.PreferrdEntityTopic.from_dict({"module": elem.module}))
        elif elem.source == "entertainment_ner":
            preferred_entity_topics_list.append(
                UserProfile.PreferrdEntityTopic.from_dict({
                    "entity_in_utterance": elem.entity_in_utterance,
                    "entity_detected": elem.entity_detected,
                    "entity_type": elem.entity_type,
                    "module": elem.module}))
        else:
            preferred_entity_topics_from_kgs.append(elem)

    # adapt google kg and concept
    module_list = [elem.module.value for elem in preferred_entity_topics_from_kgs]

    # unique
    module_list = list(set(module_list))
    for module in module_list:
        entity_topic = {
            "entity_in_utterance": "",
            "entity_detected": "",
            "entity_type": [],
            "module": module
        }

        for elem in preferred_entity_topics_from_kgs:
            if module == elem.module.value:
                if entity_inclusion_relation(entity_topic["entity_in_utterance"], elem.entity_in_utterance):
                    entity_topic["entity_in_utterance"] = \
                        entity_inclusion_relation(entity_topic["entity_in_utterance"], elem.entity_in_utterance)

                    if elem.source == "concept" or entity_topic["entity_detected"] == "":
                        entity_topic["entity_detected"] = elem.entity_detected
                    entity_topic["entity_type"] = entity_topic["entity_type"] + elem.entity_type

        preferred_entity_topics_list.append(UserProfile.PreferrdEntityTopic.from_dict(entity_topic))

    return preferred_entity_topics_list

def entity_inclusion_relation(entity_a, entity_b):
    if entity_a in entity_b:
        return entity_b
    elif entity_b in entity_a:
        return entity_a
    else:
        return None

def check_if_daily_life_in_friendship(curr_utt, user_attributes, module_selector):
    return user_attributes.social_open_question_proposing == True and \
        module_selector.global_state == GlobalState.FRIENDSHIP_OPEN_QUESTION and \
            FriendshipActivityClassifier.classify(curr_utt) != CommonActivity.unknown
