import sys
import os

import nlu.util_nlu
import selecting_strategy.topic_module_proposal

file_dir = os.path.dirname(__file__)
sys.path.append(file_dir)

import re
from typing import Dict, Any, List
from injector import inject

import utils
from cobot_core import SelectingStrategy, StateManager
from non_trivial_bot import logger
from selecting_strategy.util_selecting_strategy import getCustomModules

utils.set_logger_level(utils.get_working_environment())

class CustomSelectingStrategy(SelectingStrategy):
    @inject
    def __init__(self, state_manager: StateManager):
        self.state_manager = state_manager
        self.removewords = ['alexa', 'Alexa', 'echo', 'dot']

    def select_response_mode(self, features: Dict[str, Any]) -> List[str]:
        logger.info('[SS] start selecting strategy with feature: {}', features)

        amazon_intents = features['intent']
        returnnlp = features['returnnlp']
        central_elem = features['central_elem']
        custom_intents = features['intent_classify']
        curr_utt = features['text'].lower()

        global_senti = nlu.util_nlu.get_sentiment(
            self.state_manager.current_state, custom_intents["lexical"])
        last_state = self.state_manager.last_state
        last_response = last_state.get('response', None)

        selected_modules = getCustomModules(
            curr_utt, global_senti, returnnlp, central_elem, amazon_intents, self.state_manager)

        logger.info('[Selecting Strategy] Selected module: {}'.format(selected_modules))

        return selected_modules
