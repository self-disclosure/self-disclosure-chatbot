#title           :regex_intent_handler.py
#description     :template code for mapping user utterance to response using regex.
#                :can generate dynamic responses from specified word chocies
#authors         :Terry and Ashwin
#=======================================================================================

import re
import random


class RegexIntentHandler:
    def __init__(self, input):
        self.text = input["text"].lower() # user utterance
        self.input = input # input can contain other information such as nlp results
        self.regexTemplate = self.defineTemplate() # see below
        self.regexOrder = self.defineOrder() # see below
        self.defaultResponse = self.default_response # see below

#======================================================================================
# Map regex patterns to its corresponding response function
# You have to define the response function which returns the output
    def defineTemplate(self):
        template = {
                        "group1": {
                            "regex": r"pattern1",
                            "responseFunction": self.group1_response,
                        },
                        "group2": {
                            "regex": r"pattern2",
                            "responseFunction": self.group2_response,
                        },
                        "group3": {
                            "regex": r"pattern3",
                            "responseFunction": self.group3_response,
                        }
        }
        return template

#=======================================================================================
# Define the order of evaluation for each group defined above
# When multiple regex patterns can match the user utterance, this order defines which
# reponse function is used to generate response
    def defineOrder(self):
        order = ["group1", "group2", "group3"]
        return order

#=======================================================================================
# Default response generator when none of the regex patterns match
# You define the logic
    def default_response(self):
        return "Sorry, I don't know how to answer that. Can you clarify?"

#=======================================================================================
# You define the logic for the response functions

# E.g.: can be simply returning a random element from list of responses
    def group1_response(self):
        responses = [
            "example1",
            "example2",
            "example3",
        ]
        return random.choice(responses)

# E.g.: can use specific regex matching to return specific responses
    def group2_response(self):
        if re.search(r"some regex", self.text):
            return "specific response"
        elif re.search(r"another regex", self.text):
            return "another specific response"
        else:
            return "general response"

# E.g.: can be some sort of hybrid system. consider also using nlp features for crafting
# specific responses for user
    def group3_response(self):
        if re.search(r"some regex", self.text):
            return "specific response"
        elif re.search(r"another regex", self.text):
            return "another specific response"

        responses = [
            "example1",
            "example2",
            "example3",
        ]
        return random.choice(responses)

#=======================================================================================
# Using the specified order in defineOrder(), when a regex group matches,
# run its corresponding response function to get response
    def generateResponse(self, debug=False):
        # check for regex matches in the specified order
        for key in self.regexOrder:
            if re.search(self.regexTemplate[key]["regex"], self.text):
                response = self.regexTemplate[key]["responseFunction"]()
                if debug == True:
                    response += " " + str(self.regexTemplate[key]["responseFunction"])
                return constructDynamicResponse([response])

        # if no matches, then output default
        response = self.defaultResponse()
        return constructDynamicResponse([response])

#=======================================================================================
# Pass in list containing responses with patterns like (word1|word2|word3) and
# only one word will be used when constructing the response. "|" is like "or".
def constructDynamicResponse(candidateResponses):
    selectedResponse = random.choice(candidateResponses)
    finalString = ""
    choicesBuffer = ""
    flag = False
    for char in selectedResponse:
        if char == "(":
            flag = True
        elif char == ")":
            flag = False
            choices = choicesBuffer.split("|")
            finalString += random.choice(choices)
            choicesBuffer = ""
        if flag == True and char != "(" and char != ")":
            choicesBuffer += char
        elif flag == False and char != "(" and char != ")":
            finalString += char
    finalString = " ".join(finalString.split())
    return finalString


