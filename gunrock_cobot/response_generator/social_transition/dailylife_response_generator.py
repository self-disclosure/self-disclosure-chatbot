import re
import logging
from typing import Dict, Pattern
from nlu.dataclass import CentralElement
from cobot_core.service_module import LocalServiceModule

import template_manager

from template_manager import Template, TemplateManager
from selecting_strategy.module_selection import ModuleSelector

logger = logging.getLogger(__name__)
template_topics = Template.topics
template_social = Template.social
template_transition = Template.transition


class Dailylife_ResponseGenerator(LocalServiceModule):
    """daily life responses: """
    @property
    def curr_utt(self):
        return self.input_data['text'].lower()

    @property
    def central_element(self) -> CentralElement:
        try:
            ce = self.input_data['central_elem']
            return CentralElement.from_dict(ce)
        except KeyError:
            logger.warn(f"[FSM] FSM did not receive a valid central_element. ce: {ce}")
            return CentralElement()

    @property
    def dailylife(self):
        if not self.state_manager.user_attributes.dailylife:
            self.state_manager.user_attributes.dailylife = {}
        return self.state_manager.user_attributes.dailylife

    def __init__(self, *args, **kwargs):
        LocalServiceModule.__init__(self, *args, **kwargs)
        self.tm_social = TemplateManager(template_social, self.state_manager.user_attributes)
        self.tm_transition = TemplateManager(template_transition, self.state_manager.user_attributes)
        self.regexes: Dict[str, Pattern] = {k: re.compile(v, re.I) for k, v in {
            "topic_school": r"\bschool\b",
            "topic_sleep": r"\bsleep\b|\bsleeping\b|\bnaps\b",
            "topic_hangout": r"\b(hang|hanging|hang out|hangout) with (my )?friend(s)?\b",
            "topic_friends": r"\bfriend\b|\bfriends\b|^play$|^playing$",
            "topic_work": r"(\bwork\b|\bworking\b)(?! (on|with))",
            "topic_draw": r"\bdraw\b|\bdrawing\b|\bpaint\b|\bpainting\b|\bart\b|\bsketch\b|\bsketching\b|\bphotography\b",
            "topic_dance": r"\bdance(s)?\b|\bdancing\b",
            "topic_chores":r"\b(clean|cleaning) (up )?(my room|(the |my )(house|yard))\b|\bdo cleaning\b|^cleaning",
            "topic_family_kids":r"\bwith my kids\b",
            "topic_family_parent":r"\b(play with|\btalk to) my (father|dad|mother|mom|daddy|mummy)\b",
            "topic_family_general":r"\bfamily\b",
            "topic_youtube":r"\byoutube\b",
            "topic_tablet_phone":r"\bphone\b|\btablet\b",
            "topic_toy":r"\btoy(s)?\b",
            "topic_my_birthday":r"\bmy birthday\b",
            "topic_party":r"\bparty\b",
            "topic_talk_to_bot":r"^(talk|talking) (to|with) (you|chatbot)\b",
            "topic_relationships":r"\bgirlfriend\b|\bboyfriend\b|^i (like|love) a (girl|boy)|\brelationship",
            "topic_stuckhome":r"\b(stuck|stucking|stay|staying) (at |in )?(my |the )?(home|house)\b|\bquarantine\b",
            "topic_gardening":r"\bgardening\b|\bgarden\b",
            "topic_climate":r"\bclimate change\b",
        }.items()}

    def execute(self):
        utt = ""
        append_t = ""
        topic_school = self.regexes['topic_school'].search(self.central_element.text)
        topic_sleep = self.regexes['topic_sleep'].search(self.central_element.text)
        topic_hangout = self.regexes['topic_hangout'].search(self.central_element.text)
        topic_friends = self.regexes['topic_friends'].search(self.central_element.text)
        topic_work = self.regexes['topic_work'].search(self.central_element.text)
        topic_draw = self.regexes['topic_draw'].search(self.central_element.text)
        topic_dance = self.regexes['topic_dance'].search(self.central_element.text)
        topic_chores = self.regexes['topic_chores'].search(self.central_element.text)
        topic_family_kids = self.regexes['topic_family_kids'].search(self.central_element.text)
        topic_family_parent = self.regexes['topic_family_parent'].search(self.central_element.text)
        topic_family_general = self.regexes['topic_family_general'].search(self.central_element.text)
        topic_youtube = self.regexes['topic_youtube'].search(self.central_element.text)
        topic_tablet_phone = self.regexes['topic_tablet_phone'].search(self.central_element.text)
        topic_toy = self.regexes['topic_toy'].search(self.central_element.text)
        topic_my_birthday = self.regexes['topic_my_birthday'].search(self.central_element.text)
        topic_party = self.regexes['topic_party'].search(self.central_element.text)
        topic_talk_to_bot = self.regexes['topic_talk_to_bot'].search(self.central_element.text)
        topic_relationships = self.regexes['topic_relationships'].search(self.central_element.text)
        topic_stuckhome = self.regexes['topic_stuckhome'].search(self.central_element.text)
        topic_gardening = self.regexes['topic_gardening'].search(self.central_element.text)
        topic_climate = self.regexes['topic_climate'].search(self.central_element.text)

        module_selector = ModuleSelector(self.state_manager.user_attributes)

        if topic_school:
            utt = self.tm_social.speak("one_turn_service_acknowledge/school", {})
            self.convert_tm_return_value(utt)

            args = {}
            propose_module = None
            append_t = self.tm_social.speak("one_turn_service_propose/school", {}, embedded_info=args)
            self.convert_tm_return_value(append_t, args)

            if args and 'module' in args.keys():
                propose_module = args['module']
                if propose_module in module_selector.used_topic_modules:
                    propose_module, append_t = self.get_propose_module_and_utt(propose_module)
            setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
            self.dailylife['propose_continue'] = 'STOP'

        elif topic_sleep:
            utt = self.tm_social.speak("one_turn_service_acknowledge/sleep", {})
            self.convert_tm_return_value(utt)

            args = {}
            propose_module = None
            append_t = self.tm_social.speak("one_turn_service_propose/sleep", {}, embedded_info=args)
            self.convert_tm_return_value(append_t, args)

            if args and 'module' in args.keys():
                propose_module = args['module']
                if propose_module in module_selector.used_topic_modules:
                    propose_module, append_t = self.get_propose_module_and_utt(propose_module)
            setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
            self.dailylife['propose_continue'] = 'STOP'

        elif topic_hangout:
            utt = self.tm_social.speak("one_turn_service_acknowledge/hangout", {})
            self.convert_tm_return_value(utt)

            args = {}
            propose_module = None
            append_t = self.tm_social.speak("one_turn_service_propose/hangout", {}, embedded_info=args)
            self.convert_tm_return_value(append_t, args)

            if args and 'module' in args.keys():
                propose_module = args['module']
                if propose_module in module_selector.used_topic_modules:
                    propose_module, append_t = self.get_propose_module_and_utt(propose_module)
            setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
            self.dailylife['propose_continue'] = 'STOP'
        
        elif topic_friends:
            utt = self.tm_social.speak("one_turn_service_acknowledge/play_with_friends", {})
            self.convert_tm_return_value(utt)

            args = {}
            propose_module = None
            append_t = self.tm_social.speak("one_turn_service_propose/play_with_friends", {}, embedded_info=args)
            self.convert_tm_return_value(append_t, args)

            if args and 'module' in args.keys():
                propose_module = args['module']
                if propose_module in module_selector.used_topic_modules:
                    propose_module, append_t = self.get_propose_module_and_utt(propose_module)
            setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
            self.dailylife['propose_continue'] = 'STOP'
        
        elif topic_work:
            utt = self.tm_social.speak("one_turn_service_acknowledge/work", {})
            self.convert_tm_return_value(utt)

            args = {}
            propose_module = None
            append_t = self.tm_social.speak("one_turn_service_propose/work", {}, embedded_info=args)
            self.convert_tm_return_value(append_t, args)

            if args and 'module' in args.keys():
                propose_module = args['module']
                if propose_module in module_selector.used_topic_modules:
                    propose_module, append_t = self.get_propose_module_and_utt(propose_module)
            setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
            self.dailylife['propose_continue'] = 'STOP'
        
        elif topic_draw:
            utt = self.tm_social.speak("one_turn_service_acknowledge/draw", {})
            self.convert_tm_return_value(utt)

            args = {}
            propose_module = None
            append_t = self.tm_social.speak("one_turn_service_propose/draw", {}, embedded_info=args)
            self.convert_tm_return_value(append_t, args)

            if args and 'module' in args.keys():
                propose_module = args['module']
                if propose_module in module_selector.used_topic_modules:
                    propose_module, append_t = self.get_propose_module_and_utt(propose_module)
            setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
            self.dailylife['propose_continue'] = 'STOP'
        
        elif topic_dance:
            utt = self.tm_social.speak("one_turn_service_acknowledge/dance", {})
            self.convert_tm_return_value(utt)

            args = {}
            propose_module = None
            append_t = self.tm_social.speak("one_turn_service_propose/dance", {}, embedded_info=args)
            self.convert_tm_return_value(append_t, args)

            if args and 'module' in args.keys():
                propose_module = args['module']
                if propose_module in module_selector.used_topic_modules:
                    propose_module, append_t = self.get_propose_module_and_utt(propose_module)
            setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
            self.dailylife['propose_continue'] = 'STOP'

        elif topic_chores:
            utt = self.tm_social.speak("one_turn_service_acknowledge/chores-clean", {})
            self.convert_tm_return_value(utt)

            args = {}
            propose_module = None
            append_t = self.tm_social.speak("one_turn_service_propose/chores-clean", {}, embedded_info=args)
            self.convert_tm_return_value(append_t, args)

            if args and 'module' in args.keys():
                propose_module = args['module']
                if propose_module in module_selector.used_topic_modules:
                    propose_module, append_t = self.get_propose_module_and_utt(propose_module)
            setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
            self.dailylife['propose_continue'] = 'STOP'

        elif topic_family_kids:
            utt = self.tm_social.speak("one_turn_service_acknowledge/family/kids", {})
            self.convert_tm_return_value(utt)

            args = {}
            propose_module = None
            append_t = self.tm_social.speak("one_turn_service_propose/family/kids", {}, embedded_info=args)
            self.convert_tm_return_value(append_t, args)

            if args and 'module' in args.keys():
                propose_module = args['module']
                if propose_module in module_selector.used_topic_modules:
                    propose_module, append_t = self.get_propose_module_and_utt(propose_module)
            setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
            self.dailylife['propose_continue'] = 'STOP'

        elif topic_family_parent:
            utt = self.tm_social.speak("one_turn_service_acknowledge/family/parent", {})
            self.convert_tm_return_value(utt)

            args = {}
            propose_module = None
            append_t = self.tm_social.speak("one_turn_service_propose/family/parent", {}, embedded_info=args)
            self.convert_tm_return_value(append_t, args)

            if args and 'module' in args.keys():
                propose_module = args['module']
                if propose_module in module_selector.used_topic_modules:
                    propose_module, append_t = self.get_propose_module_and_utt(propose_module)
            setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
            self.dailylife['propose_continue'] = 'STOP'

        elif topic_family_parent:
            utt = self.tm_social.speak("one_turn_service_acknowledge/family/parent", {})
            self.convert_tm_return_value(utt)

            args = {}
            propose_module = None
            append_t = self.tm_social.speak("one_turn_service_propose/family/parent", {}, embedded_info=args)
            self.convert_tm_return_value(append_t, args)

            if args and 'module' in args.keys():
                propose_module = args['module']
                if propose_module in module_selector.used_topic_modules:
                    propose_module, append_t = self.get_propose_module_and_utt(propose_module)
            setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
            self.dailylife['propose_continue'] = 'STOP'

        elif topic_family_general:
            utt = self.tm_social.speak("one_turn_service_acknowledge/family/family_general", {})
            self.convert_tm_return_value(utt)

            args = {}
            propose_module = None
            append_t = self.tm_social.speak("one_turn_service_propose/family/family_general", {}, embedded_info=args)
            self.convert_tm_return_value(append_t, args)

            if args and 'module' in args.keys():
                propose_module = args['module']
                if propose_module in module_selector.used_topic_modules:
                    propose_module, append_t = self.get_propose_module_and_utt(propose_module)
            setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
            self.dailylife['propose_continue'] = 'STOP'
        
        elif topic_youtube:
            utt = self.tm_social.speak("one_turn_service_acknowledge/youtube", {})
            self.convert_tm_return_value(utt)

            args = {}
            propose_module = None
            append_t = self.tm_social.speak("one_turn_service_propose/tablet_phone", {}, embedded_info=args)
            self.convert_tm_return_value(append_t, args)

            if args and 'module' in args.keys():
                propose_module = args['module']
                if propose_module in module_selector.used_topic_modules:
                    propose_module, append_t = self.get_propose_module_and_utt(propose_module)
            setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
            self.dailylife['propose_continue'] = 'STOP'

        elif topic_tablet_phone:
            utt = self.tm_social.speak("one_turn_service_acknowledge/tablet_phone", {})
            self.convert_tm_return_value(utt)

            args = {}
            propose_module = None
            append_t = self.tm_social.speak("one_turn_service_propose/tablet_phone", {}, embedded_info=args)
            self.convert_tm_return_value(append_t, args)

            if args and 'module' in args.keys():
                propose_module = args['module']
                if propose_module in module_selector.used_topic_modules:
                    propose_module, append_t = self.get_propose_module_and_utt(propose_module)
            setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
            self.dailylife['propose_continue'] = 'STOP'
        
        elif topic_toy:
            utt = self.tm_social.speak("one_turn_service_acknowledge/toys", {})
            self.convert_tm_return_value(utt)

            args = {}
            propose_module = None
            append_t = self.tm_social.speak("one_turn_service_propose/toys", {}, embedded_info=args)
            self.convert_tm_return_value(append_t, args)

            if args and 'module' in args.keys():
                propose_module = args['module']
                if propose_module in module_selector.used_topic_modules:
                    propose_module, append_t = self.get_propose_module_and_utt(propose_module)
            setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
            self.dailylife['propose_continue'] = 'STOP'

        elif topic_my_birthday:
            utt = self.tm_social.speak("one_turn_service_acknowledge/my_birthday", {})
            self.convert_tm_return_value(utt)

            args = {}
            propose_module = None
            append_t = self.tm_social.speak("one_turn_service_propose/my_birthday", {}, embedded_info=args)
            self.convert_tm_return_value(append_t, args)

            if args and 'module' in args.keys():
                propose_module = args['module']
                if propose_module in module_selector.used_topic_modules:
                    propose_module, append_t = self.get_propose_module_and_utt(propose_module)
            setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
            self.dailylife['propose_continue'] = 'STOP'
        
        elif topic_party:
            utt = self.tm_social.speak("one_turn_service_acknowledge/my_birthday", {})
            self.convert_tm_return_value(utt)

            args = {}
            propose_module = None
            append_t = self.tm_social.speak("one_turn_service_propose/my_birthday", {}, embedded_info=args)
            self.convert_tm_return_value(append_t, args)

            if args and 'module' in args.keys():
                propose_module = args['module']
                if propose_module in module_selector.used_topic_modules:
                    propose_module, append_t = self.get_propose_module_and_utt(propose_module)
            setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
            self.dailylife['propose_continue'] = 'STOP'

        elif topic_talk_to_bot:
            utt = self.tm_social.speak("one_turn_service_acknowledge/talk_to_bot", {})
            self.convert_tm_return_value(utt)

            args = {}
            propose_module = None
            append_t = self.tm_social.speak("one_turn_service_propose/talk_to_bot", {}, embedded_info=args)
            self.convert_tm_return_value(append_t, args)

            if args and 'module' in args.keys():
                propose_module = args['module']
                if propose_module in module_selector.used_topic_modules:
                    propose_module, append_t = self.get_propose_module_and_utt(propose_module)
            setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
            self.dailylife['propose_continue'] = 'STOP'
        
        elif topic_relationships:
            utt = self.tm_social.speak("one_turn_service_acknowledge/relationship", {})
            self.convert_tm_return_value(utt)

            args = {}
            propose_module = None
            append_t = self.tm_social.speak("one_turn_service_propose/relationship", {}, embedded_info=args)
            self.convert_tm_return_value(append_t, args)

            if args and 'module' in args.keys():
                propose_module = args['module']
                if propose_module in module_selector.used_topic_modules:
                    propose_module, append_t = self.get_propose_module_and_utt(propose_module)
            setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
            self.dailylife['propose_continue'] = 'STOP'
        
        elif topic_stuckhome:
            utt = self.tm_social.speak("one_turn_service_acknowledge/stuck_home", {})
            self.convert_tm_return_value(utt)

            args = {}
            propose_module = None
            append_t = self.tm_social.speak("one_turn_service_propose/stuck_home", {}, embedded_info=args)
            self.convert_tm_return_value(append_t, args)

            if args and 'module' in args.keys():
                propose_module = args['module']
                if propose_module in module_selector.used_topic_modules:
                    propose_module, append_t = self.get_propose_module_and_utt(propose_module)
            setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
            self.dailylife['propose_continue'] = 'STOP'
        elif topic_gardening:
            utt = self.tm_social.speak("one_turn_service_acknowledge/gardening", {})
            self.convert_tm_return_value(utt)

            args = {}
            propose_module = None
            append_t = self.tm_social.speak("one_turn_service_propose/gardening", {}, embedded_info=args)
            self.convert_tm_return_value(append_t, args)

            if args and 'module' in args.keys():
                propose_module = args['module']
                if propose_module in module_selector.used_topic_modules:
                    propose_module, append_t = self.get_propose_module_and_utt(propose_module)
            setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
            self.dailylife['propose_continue'] = 'STOP'
        elif topic_climate:
            utt = self.tm_social.speak("one_turn_service_acknowledge/climate_change", {})
            self.convert_tm_return_value(utt)

            args = {}
            propose_module = None
            append_t = self.tm_social.speak("one_turn_service_propose/climate_change", {}, embedded_info=args)
            self.convert_tm_return_value(append_t, args)

            if args and 'module' in args.keys():
                propose_module = args['module']
                if propose_module in module_selector.used_topic_modules:
                    propose_module, append_t = self.get_propose_module_and_utt(propose_module)
            setattr(self.state_manager.user_attributes, "propose_topic", propose_module)
            self.dailylife['propose_continue'] = 'STOP'

        else:
            logger.warning('[DAILYLIFE] mistakenly enter daily life bot')

        logger.info('[DAILYLIFE]: utt = {}'.format(utt))
        logger.info('[DAILYLIFE]: append_t = {}'.format(append_t))


        utt = utt + append_t
        return utt

    def convert_tm_return_value(self, tm_return_value, args=None):
        if type(tm_return_value) is dict:
            if args is not None:
                args = tm_return_value.get('args', {})
            tm_return_value = tm_return_value.get("utt", "")
    
    def get_propose_module_and_utt(self, propose_module):
        append_t = ""
        module_selector = ModuleSelector(self.state_manager.user_attributes)
        propose_module, _ = module_selector.get_next_modules()[0]
        module_selector.add_used_topic_module(propose_module)

        if propose_module is not None:
            append_t = self.tm_social.speak('open_question_transition/{}'.format(propose_module), {})
        else:
            append_t = self.tm_transition.speak('propose_topic_short/SOCIAL', {})
        return propose_module, append_t
