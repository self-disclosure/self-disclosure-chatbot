from typing import Optional

from cobot_core.service_module import LocalServiceModule

from response_generator.fsm2 import FSMAttributeAdaptor, FSMModule
from template_manager import Template
from user_initializer import UserInitializer

from .states import LGState


class LaunchGreetingModule(FSMModule):

    def __post_init__(
        self,
        state_manager=None
    ):
        if state_manager:
            setattr(self, 'state_manager_ref', state_manager)

    @property
    def user_initializer(self) -> Optional[UserInitializer]:
        state_manager = getattr(self, 'state_manager_ref', None)
        if state_manager:
            return UserInitializer(state_manager)


class LaunchGreeting_ResponseGenerator(LocalServiceModule):

    def execute(self):
        fsm = LaunchGreetingModule(
            FSMAttributeAdaptor('launchgreeting', self.state_manager.user_attributes),
            self.input_data,
            Template.greeting,
            LGState,
            first_state='initial',
            state_manager_last_state=self.state_manager.last_state
        )
        fsm.__post_init__(self.state_manager)

        return fsm.generate_response()
