import logging
from typing import Any, Dict

from transitions import Machine

from nlu.constants import TopicModule
from nlu.dataclass import CentralElement, ReturnNLP
from nlu.command_detector import CommandDetector
from nlu.question_detector import QuestionDetector

from selecting_strategy.module_selection import ModuleSelector
from template_manager import Template, TemplateManager
from user_profiler.user_profile import UserProfile
from question_handling.quetion_handler import QuestionHandler

from .user_attributes import LaunchGreetingUserAttributes
from .utils.constants import regexes
from .states import states
from .transitions import transitions


class LoggerAdapter(logging.LoggerAdapter):
    def __init__(self, prefix: str, state_name: str, logger=None):
        if not logger:
            logger = logging.getLogger(f"{prefix}.{state_name}")
        super(LoggerAdapter, self).__init__(logger, {})
        self.prefix = prefix
        self.state_name = state_name

    def process(self, msg, kwargs):
        return f"[{self.prefix}] <{self.state_name}>: {msg}", kwargs


BACKSTORY_THRESHOLD = 0.82


class LaunchGreetingFSM(Machine):

    def __init__(
        self,
        user_attribute_adapter: LaunchGreetingUserAttributes,
        input_data: Dict[str, Any],
        state_manager,
    ):

        self.fsm_logger = LoggerAdapter('launchgreeting', '__fsm__')

        self.ua = user_attribute_adapter
        self.state_manager = state_manager
        self.input_data = input_data
        self._returnnlp = None
        self._central_element = None

        self.module_selector = ModuleSelector(self.ua.ua_ref)
        self.user_model = UserProfile(self.ua.ua_ref)

        self.tm_greet = TemplateManager(Template.greeting, user_attribute_adapter.ua_ref)
        self.tm_transition = TemplateManager(Template.transition, user_attribute_adapter.ua_ref)

        self.command_detector = CommandDetector(self.input_text, self.input_data['returnnlp'])

        self.question_detector = QuestionDetector(
            self.input_text, self.input_data['returnnlp']
        )
        self.question_handler = QuestionHandler(
            self.input_text, self.input_data['returnnlp'],
            backstory_threshold=BACKSTORY_THRESHOLD,
            system_ack=self.input_data['system_acknowledgement'],
            user_attributes_ref=self.ua.ua_ref
        )

        self._responses = []
        self._propose_continue = 'CONTINUE'
        self._propose_topic = None

        self.last_fsm_state = self.ua.states.get('state', 'initial')

        # FSM
        super().__init__(
            self,
            states=states.get_states(self),
            transitions=transitions.get_transitions(self),
            initial=self.ua.states.get('state', 'initial'),
            prepare_event=[self.check_input_for_name],
            after_state_change=[self.machine_clean_up]
        )

        self.pytest_logger = LoggerAdapter('launchgreeting', '__pytest__', logging.getLogger('pytest.input'))
        self.pytest_logger.info("<state = {}>\n{}".format(self.state, dict(
            inputs=dict(
                input_data=self.input_data,
                ua_ref=self.ua.ua_ref.__dict__
            )
        )))

        self.logger = LoggerAdapter('launchgreeting', self.state)

    # MARK: - System

    def machine_clean_up(self):
        self.fsm_logger.debug("cleaning up machine")
        self.fsm_logger.debug(f"state: {self.state}")
        self.ua.states.update({'state': self.state})

    def execute(self):
        """
        main function called by cobot
        """
        self.fsm_logger.debug("executing transition")
        self.next_state()
        self.fsm_logger.debug(f"tracker: {self.ua.persistent}, state={self.state}")
        if not self._responses:
            self.ua.persistent['is_first_module'] = False
            self.ua['propose_continue'] = 'STOP'
            self.ua.clear()
            self.machine_clean_up()
            raise ValueError((
                f"response not set. "
                f"state: {self.state}, resp: {self._responses}, "
                f"locals: {self.ua.volatile}, {self.ua.persistent}"
            ))

        self.ua['propose_continue'] = self._propose_continue
        if self._propose_topic:
            self.ua.propose_topic = self._propose_topic.value
        return " ".join(self._responses)

    # MARK: - Info

    @property
    def input_text(self):
        # text = self.input_data['text'].lower()  # type: str
        # if text.startswith('open gunrock and '):
        #     text = re.sub('open gunrock and ', '', text)
        # return text
        return self.input_data['text']

    @property
    def last_session_id(self):
        return getattr(self.ua.ua_ref, 'last_session_id')
        # return self.state_manager.user_attributes.last_session_id

    @last_session_id.setter
    def last_session_id(self, _id):
        setattr(self.ua.ua_ref, 'last_session_id', _id)
        # self.state_manager.user_attributes.last_session_id = _id

    @property
    def last_state(self):
        return getattr(self.ua.ua_ref, 'last_module')
        # return self.state_manager.user_attributes.last_module

    @property
    def open_question_pos_ans(self):
        return getattr(self.ua.ua_ref, 'open_question_has_pos_ans', None)
        # return self.state_manager.user_attributes.open_question_has_pos_ans

    @open_question_pos_ans.setter
    def open_question_pos_ans(self, pos_ans):
        setattr(self.ua.ua_ref, 'open_question_has_pos_ans', pos_ans)
        # self.state_manager.user_attributes.open_question_has_pos_ans = pos_ans

    @property
    def segmented_text(self):
        return self.returnnlp.text
        # st = self.state_manager.current_state.features
        # if st is None:
        #     return [self.input_text]
        # st = st.get('segmentation', {}).get('segmented_text_raw')
        # if st is None:
        #     return [self.input_text]
        # return st

    @property
    def lexicals(self):
        ret = self.input_data['features']['central_elem']['senti']
        self.fsm_logger.debug(f"LEXICALS: {ret}")
        return ret

    @property
    def central_element(self) -> CentralElement:
        try:
            if not self._central_element:
                ce = self.input_data['central_elem']
                self._central_element = CentralElement.from_dict(ce)
            return self._central_element
        except KeyError:
            self.fsm_logger.warn(f"did not receive a valid central_element. ce: {ce}")
            return CentralElement()

    @property
    def returnnlp(self) -> ReturnNLP:
        try:
            if not self._returnnlp:
                rnlp = self.input_data['returnnlp']
                self._returnnlp = ReturnNLP(rnlp)
            return self._returnnlp
        except (KeyError, TypeError):
            self.fsm_logger.warn(f"did not receive a valid returnnlp. rnlp: {rnlp}")
            return ReturnNLP([])

    @property
    def regexes(self):
        return regexes

    # MARK: - Dispatcher

    def respond(self, text: str):
        self._responses.append(text)

    def respond_template(self, selector: str, slots: Dict[str, str], embedded_info: dict = None):
        try:
            utt = self.tm_greet.speak(selector, slots, embedded_info)
            self.logger.debug(f"respond_template: selector={selector}, utt={utt}")
            self.respond(utt)
        except (KeyError, ValueError) as e:
            self.fsm_logger.warn(f"respond_template encountered error: {e}")

    def respond_transition_template(self, selector: str, slots: Dict[str, str], embedded_info: dict = None):
        try:
            utt = self.tm_transition.speak(selector, slots, embedded_info)
            self.respond(utt)
        except (KeyError, ValueError) as e:
            self.fsm_logger.warn(f"respond_template encountered error: {e}")

    def propose_continue(self, state: str, topic: TopicModule = None):
        if state not in {'CONTINUE', 'STOP', 'UNCLEAR'}:
            raise ValueError(f"received invalid propose_continue ({state})")
        self._propose_continue = state
        self._propose_topic = topic

    # MARK: - Utils

    from .utils.utils import (
        my_name,
        is_new_user,
        is_intent_correcting_name,
        _search_common_name,
        check_input_for_name,
        holiday_greet,
        how_are_you_sentiment,
        get_social_template,
        proposing_topics,
        has_command_or_question,
    )

    # MARK: - Transitions
    # from .states.states import (
    #     on_enter_greet_new_user,
    #     on_enter_greet_returning_user_with_username,
    #     on_enter_greet_returning_user_without_username,
    #     on_enter_returning_user_same_user,
    #     on_enter_returning_user_different_user,

    #     on_enter_got_name_common_name,
    #     on_enter_got_name_uncommon_name,
    #     on_enter_got_name_no_name,
    #     on_enter_got_name_no_name_again,
    #     on_enter_got_name_decline,

    #     on_enter_how_are_you,
    #     on_enter_how_are_you_followup,

    #     on_enter_open_question_followup,
    #     on_enter_social_template_exit,

    #     on_enter_not_my_name,
    #     on_enter_not_my_name_with_correction,
    #     on_enter_whats_my_name,
    #     on_enter_correction_default_pos,
    #     on_enter_correction_default_neg,
    #     on_enter_correction_no_name,
    #     on_enter_got_name_uncommon_verify_pos,

    #     on_enter_question_handler,

    # )
