from typing import TYPE_CHECKING

from nlu.constants import Positivity

from . import conditions

if TYPE_CHECKING:
    from ..fsm import LaunchGreetingFSM


def get_transitions(fsm: 'LaunchGreetingFSM'):
    return [
        # coronavirus
        dict(
            trigger='next_state',
            source='*',
            dest='coronavirus',
            conditions=lambda: conditions.got_topic_coronavirus(fsm)
        ),

        # init
        dict(
            trigger='next_state', source='initial', dest='greet_new_user',
            conditions=[
                fsm.is_new_user,
            ]
        ),
        dict(
            trigger='next_state', source='initial', dest='greet_returning_user_with_username',
            conditions=[
                lambda: bool(fsm.user_model.username)],
            unless=[
                fsm.is_new_user,
            ]
        ),
        dict(
            trigger='next_state', source='initial', dest='greet_returning_user_without_username',
            unless=[
                lambda: bool(fsm.user_model.username),
                fsm.is_new_user,
            ]
        ),
        dict(
            trigger='next_state', source='greet_returning_user_with_username', dest='returning_user_same_user',
            conditions=[
                lambda: fsm.returnnlp.answer_positivity is Positivity.pos,
            ],
            unless=[
                fsm.has_command_or_question,
            ]
        ),
        dict(
            trigger='next_state', source='greet_returning_user_with_username', dest='returning_user_different_user',
            conditions=[
                lambda: fsm.returnnlp.answer_positivity is not Positivity.pos,
            ],
            unless=[
                fsm.has_command_or_question,
            ]
        ),
        dict(
            trigger='next_state', source='returning_user_same_user', dest='how_are_you',
            unless=[
                fsm.has_command_or_question,
            ]
        ),

        # asking for name
        dict(
            trigger='next_state',
            source=['greet_new_user', 'not_my_name', 'got_name_no_name', 'correction_default_neg',
                    'correction_no_name', 'returning_user_different_user', 'greet_returning_user_without_username'],
            dest='got_name_common_name',
            prepare='check_input_for_name',
            conditions=lambda: conditions.got_name_common_name(fsm)
        ),
        dict(
            trigger='next_state', source='got_name_uncommon_name', dest='got_name_no_name_again',
            unless=[fsm.has_command_or_question, ]
        ),
        dict(
            trigger='next_state',
            source=['greet_new_user', 'not_my_name', 'got_name_no_name', 'correction_default_neg',
                    'correction_no_name', 'returning_user_different_user', 'greet_returning_user_without_username'],
            dest='got_name_uncommon_name',
            prepare='check_input_for_name',
            conditions=lambda: conditions.got_name_uncommon_name(fsm)
        ),
        dict(
            trigger='next_state',
            source=['greet_new_user', 'not_my_name', 'correction_default_neg', 'correction_no_name',
                    'greet_returning_user_without_username'],
            dest='got_name_no_name',
            prepare=[
                'check_input_for_name',
            ],
            conditions=lambda: conditions.got_name_no_name(fsm)
        ),
        dict(
            trigger='next_state',
            source=['returning_user_different_user'],
            dest='got_name_no_name_again',
            prepare=[
                'check_input_for_name',
            ],
            conditions=lambda: conditions.got_name_no_name(fsm)
        ),
        dict(
            trigger='next_state',
            source=['greet_new_user', 'not_my_name', 'correction_default_neg', 'correction_no_name',
                    'returning_user_different_user', 'greet_returning_user_without_username'],
            dest='got_name_decline',
            prepare=[
                'check_input_for_name',
            ],
            conditions=lambda: conditions.got_name_decline(fsm)
        ),
        # dict(
        #     trigger='next_state',
        #     source=['returning_user_different_user'],
        #     dest='got_name_no_name',
        #     prepare='check_input_for_name',
        #     conditions=[
        #         lambda: fsm.ua.volatile.get('check_input_for_name') is None,
        #     ],
        #     unless=[
        #         # lambda: fsm.central_elem.backstory.confidence >= BACKSTORY_THRESHOLD,
        #         fsm.has_command_or_question,
        #     ]
        # ),
        # dict(
        #     trigger='next_state',
        #     source=['greet_returning_user_without_username'],
        #     dest='got_name_no_name_again',
        #     prepare='check_input_for_name',
        #     conditions=[
        #         lambda: fsm.ua.volatile.get('check_input_for_name') is None,
        #     ],
        #     unless=[
        #         # lambda: fsm.central_elem.backstory.confidence >= BACKSTORY_THRESHOLD,
        #         fsm.has_command_or_question,
        #     ]
        # ),
        dict(
            trigger='next_state',
            source='got_name_no_name',
            dest='got_name_no_name_again',
            prepare='check_input_for_name',
            conditions=[
                lambda: fsm.ua.volatile.get('check_input_for_name') is None
            ],
            unless=[
                # lambda: fsm.central_elem.backstory.confidence >= BACKSTORY_THRESHOLD,
                fsm.has_command_or_question,
            ]
        ),
        # dict(
        #     trigger='next_state',
        #     source='got_name_uncommon_name',
        #     dest='got_name_uncommon_verify_pos',
        #     conditions=[
        #         lambda: fsm.returnnlp.answer_positivity == Positivity.pos
        #     ],
        #     unless=[
        #         # lambda: fsm.central_elem.backstory.confidence >= BACKSTORY_THRESHOLD,
        #         fsm.has_command_or_question,
        #     ]
        # ),
        dict(
            trigger='next_state',
            source='got_name_uncommon_name',
            dest='got_name_no_name_again',
            conditions=[
                lambda: fsm.returnnlp.answer_positivity != Positivity.pos
            ],
            unless=[
                # lambda: fsm.central_elem.backstory.confidence >= BACKSTORY_THRESHOLD,
                fsm.has_command_or_question,
            ]
        ),
        # dict(
        #     trigger='next_state',
        #     source='got_name_uncommon_verify_pos',
        #     dest='got_name_common_name',
        #     unless=[
        #         # lambda: fsm.central_elem.backstory.confidence >= BACKSTORY_THRESHOLD,
        #         fsm.has_command_or_question,
        #     ]
        # ),

        # chitchat
        dict(
            trigger='next_state',
            source=['got_name_common_name', 'got_name_decline', 'got_name_no_name_again'],
            dest='social_template_exit',
            conditions=[
                lambda: fsm.ua.persistent.get('not_my_name_flag') is True,
                lambda: fsm.ua.persistent.get('is_first_module', True) is False
            ],
            unless=[
                # lambda: fsm.central_elem.backstory.confidence >= BACKSTORY_THRESHOLD,
                fsm.has_command_or_question,
            ]
        ),
        dict(
            trigger='next_state',
            source=['got_name_common_name', 'got_name_decline', 'got_name_no_name_again'],
            dest='how_are_you',
            conditions=[
                # lambda: (fsm.ua.persistent.get('not_my_name_flag') is not True or
                #          fsm.ua.persistent.get('is_first_module') is True)
            ],
            unless=[
                # lambda: fsm.central_elem.backstory.confidence >= BACKSTORY_THRESHOLD,
                fsm.has_command_or_question,
                # lambda: fsm.ua.persistent.get('not_my_name_flag') is True,
            ]
        ),
        dict(
            trigger='next_state',
            source='how_are_you',
            dest='how_are_you_followup',
            conditions=[
                lambda: fsm.regexes['how_are_you'].search(fsm.input_text) is not None,
            ],
            unless=[
                # lambda: fsm.central_elem.backstory.confidence >= BACKSTORY_THRESHOLD,
                lambda: fsm.regexes['not_my_name'].search(fsm.input_text) is not None,
                lambda: bool(fsm.is_intent_correcting_name())
            ]
        ),
        dict(
            trigger='next_state',
            source='how_are_you',
            dest='how_are_you_followup',
            unless=[
                lambda: fsm.regexes['how_are_you'].search(fsm.input_text) is not None,
                # lambda: fsm.central_elem.backstory.confidence >= BACKSTORY_THRESHOLD,
                fsm.has_command_or_question,
                lambda: fsm.regexes['not_my_name'].search(fsm.input_text) is not None,
                lambda: bool(fsm.is_intent_correcting_name())
            ]
        ),
        dict(
            trigger='next_state',
            source='how_are_you_followup',
            dest='open_question_followup',
            conditions=[
                lambda: fsm.open_question_pos_ans is True,
            ],
            unless=[
                # lambda: fsm.central_elem.backstory.confidence >= BACKSTORY_THRESHOLD,
                fsm.has_command_or_question,
                lambda: fsm.regexes['not_my_name'].search(fsm.input_text) is not None,
            ]
        ),
        dict(
            trigger='next_state',
            source='open_question_followup',
            dest='social_template_exit',
            unless=[
                # lambda: fsm.central_elem.backstory.confidence >= BACKSTORY_THRESHOLD,
                fsm.has_command_or_question,
                lambda: fsm.regexes['not_my_name'].search(fsm.input_text) is not None,
            ]
        ),
        dict(
            trigger='next_state',
            source='how_are_you_followup',
            dest='social_template_exit',
            unless=[
                lambda: fsm.open_question_pos_ans is not True,
                # lambda: fsm.central_elem.backstory.confidence >= BACKSTORY_THRESHOLD,
                fsm.has_command_or_question,
                lambda: fsm.regexes['not_my_name'].search(fsm.input_text) is not None,
            ]
        ),

        # not my name
        dict(
            trigger='next_state',
            source='*',
            dest='not_my_name_with_correction',
            prepare=[
                'check_input_for_name',
                fsm.is_intent_correcting_name,
                lambda: fsm.logger.debug(
                    f"not_my_name_with_correction: {fsm.is_intent_correcting_name()}")
            ],
            conditions=[
                lambda: fsm.last_fsm_state not in {'initial'},
                lambda: fsm.is_intent_correcting_name() == 'with_correction',
            ],
            unless=[
                # lambda: fsm.central_elem.backstory.confidence >= BACKSTORY_THRESHOLD,
                fsm.has_command_or_question,
                # lambda: fsm.regexes['not_my_name_with_correction'].search(fsm.input_text) is not None,
            ]
        ),
        dict(
            trigger='next_state',
            source='*',
            dest='not_my_name',
            prepare=[
                'check_input_for_name',
                lambda: fsm.logger.debug(f"not_my_name: {fsm.regexes['not_my_name'].search(fsm.input_text)}")
            ],
            conditions=[
                lambda: fsm.last_fsm_state not in {'initial'},
                lambda: fsm.state != 'not_my_name',
                lambda: fsm.regexes['not_my_name'].search(fsm.input_text) is not None
            ],
            unless=[
                # lambda: fsm.central_elem.backstory.confidence >= BACKSTORY_THRESHOLD,
                fsm.has_command_or_question,
                # lambda: fsm.regexes['not_my_name_with_correction'].search(fsm.input_text) is not None,
                # fsm.is_intent_correcting_name,
            ]
        ),
        dict(
            trigger='next_state',
            source='*',
            dest='whats_my_name',
            prepare=[
                'check_input_for_name'
            ],
            conditions=[
                lambda: fsm.regexes['whats_my_name'].search(fsm.input_text) is not None
            ],
            unless=[
                # lambda: fsm.central_elem.backstory.confidence >= BACKSTORY_THRESHOLD,
            ]
        ),
        dict(
            trigger='next_state',
            source='whats_my_name',
            dest='correction_default_pos',
            conditions=[
                lambda: fsm.ua.persistent['whats_my_name_key'] == 'default',
                lambda: fsm.returnnlp.answer_positivity != Positivity.neg
            ],
            unless=[
                # lambda: fsm.central_elem.backstory.confidence >= BACKSTORY_THRESHOLD,
                fsm.has_command_or_question,
            ]
        ),
        dict(
            trigger='next_state',
            source='whats_my_name',
            dest='correction_default_neg',
            conditions=[
                lambda: fsm.ua.persistent['whats_my_name_key'] == 'default',
                lambda: fsm.returnnlp.answer_positivity == Positivity.neg
            ],
            unless=[
                # lambda: fsm.central_elem.backstory.confidence >= BACKSTORY_THRESHOLD,
                fsm.has_command_or_question,
            ]
        ),
        dict(
            trigger='next_state',
            source='whats_my_name',
            dest='correction_no_name',
            prepare='check_input_for_name',
            conditions=[
                lambda: fsm.ua.persistent['whats_my_name_key'] != 'default'
            ],
            unless=[
                # lambda: fsm.central_elem.backstory.confidence >= BACKSTORY_THRESHOLD,
                fsm.has_command_or_question,
            ]
        ),

        # question handler
        dict(
            trigger='next_state',
            source='*',
            dest='question_handler',
            conditions=[
                fsm.has_command_or_question,
            ]
        ),

        # fallthrough
        dict(
            trigger='next_state',
            source='*',
            dest='fallthrough',
        ),
    ]
