import re

from nlu.constants import Positivity
from response_generator.fsm2 import FSMLogger, Tracker

from response_generator.social_transition.launchgreeting.utils import constants, utils

logger = FSMLogger('LG', 'conditions', 'launchgreeting.conditions')


def got_name_common_name(tracker: Tracker):
    utils.check_input_for_name(tracker)
    return (
        tracker.volatile_store.get('check_input_for_name') and
        tracker.volatile_store.get('is_name_common') is True
    )


def got_name_uncommon_name(tracker: Tracker):
    utils.check_input_for_name(tracker)
    return (
        tracker.volatile_store.get('check_input_for_name') and
        tracker.volatile_store.get('is_name_common') is False
        # not fsm.regexes['yes_only'].search(fsm.input_text)
    )


def got_name_no_name(tracker: Tracker):
    utils.check_input_for_name(tracker)
    return (
        not tracker.volatile_store.get('check_input_for_name') and
        tracker.returnnlp.answer_positivity is not Positivity.neg
    )


def got_name_decline(tracker: Tracker):
    utils.check_input_for_name(tracker)
    return (
        not tracker.volatile_store.get('check_input_for_name') and
        tracker.returnnlp.answer_positivity is Positivity.neg
    )


def got_topic_coronavirus(tracker: Tracker):
    pattern = constants.regexes['coronavirus'].pattern
    return (
        re.search(pattern, tracker.input_text) and
        not re.sub(pattern, "", tracker.input_text).strip() == ""
    )
