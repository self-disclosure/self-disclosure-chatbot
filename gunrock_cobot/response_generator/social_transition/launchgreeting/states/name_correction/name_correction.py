# from nlu.constants import Positivity
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.social_transition.launchgreeting import states
from response_generator.social_transition.launchgreeting.states.base import LGState
from response_generator.social_transition.launchgreeting.utils import constants, utils


class NotMyNameRespond(LGState):

    name = 'not_my_name_respond'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker):
        if states.DetectNameRespond.name in self.module.state_tracker.curr_turn():
            return
        elif utils.is_intent_correcting_name(
            tracker, self.module.state_tracker, self.user_model
        ):
            return self.name

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        name_correction_intent = utils.is_intent_correcting_name(
            tracker, self.module.state_tracker, self.user_model)

        # TODO: migrate detection method, avoid just string matching
        if name_correction_intent == 'no_correction':

            self.logger.debug(f"going no_correction")

            dispatcher.respond_template('ask_name/correction', {})
            tracker.persistent_store['not_my_name_flag'] = True

            return [NextState(states.DetectNameRespond.name)]

        elif name_correction_intent == 'with_correction':
            utils.check_input_for_name(tracker)

            self.logger.debug(f"going with_correction ")

            # first, identify if a new name is uttered
            matches = constants.regexes['not_my_name_with_correction'].search(tracker.input_text)
            matches = [v for k, v in matches.groupdict().items() if k.startswith('nn') and v] if matches else []

            if (
                matches and self.user_model.username in matches and
                any(
                    i in {states.GreetReturningUserAreYouNameRespond.name}
                    for i in self.module.state_tracker.curr_turn())
            ):
                # if user say "yes my name is anna" for returning users with username
                states.GreetReturningUserAreYouNameRespond.name_correction_override(tracker, 'yes')
                return [NextState(states.GreetReturningUserAreYouNameRespond.name, jump=True)]

            if matches:
                self.logger.debug(f"check new name matches = {matches}")
                # get match group from regex
                new_name = matches[0]

            elif (
                tracker.volatile_store.get('check_input_for_name') and
                tracker.volatile_store.get('is_name_common', False) and
                tracker.volatile_store.get('check_input_for_name') != self.user_model.username
            ):
                new_name = tracker.volatile_store.get('check_input_for_name', '')
                self.logger.debug(
                    f"not_my_name_with_correction (check_input): "
                    f"{tracker.volatile_store.get('check_input_for_name')}")

            else:
                self.logger.warning(f"with correction cannot set new_name")
                new_name = ""

            # set the new name into user_model
            if new_name:
                self.user_model._username = new_name

            # generate response to the new name
            if tracker.persistent_store.get('is_first_module', False):
                dispatcher.respond_template('correction/lets_start_over', {'new_name': new_name})
                # self.ua.volatile['utt'] = utt
                # self.to_how_are_you()
                return [NextState(states.HowAreYouPropose.name, jump=True)]
            else:
                social_module, social_utt = utils.get_social_template(dispatcher, tracker)
                # utt += social_utt
                dispatcher.respond(social_utt)
                dispatcher.propose_continue('STOP')
                # self.response = utt, 'STOP'
                tracker.user_attributes._set_raw('propose_topic', social_module)

                return [NextState(states.Initial.name)]
