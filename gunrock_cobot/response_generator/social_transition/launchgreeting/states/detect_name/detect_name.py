from response_generator.fsm2 import Dispatcher, Tracker, NextState

from user_profiler.predictor import GenderPredictor
from question_handling.quetion_handler import QuestionResponse

from response_generator.social_transition.launchgreeting import states
from response_generator.social_transition.launchgreeting.states.base import LGState
from response_generator.social_transition.launchgreeting.utils import utils
from response_generator.social_transition.launchgreeting.transitions import conditions


class DetectNameRespond(LGState):
    """
    aka. got_name_<any>

    detect the user's input for a name
    """

    name = "detect_name_respond"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if conditions.got_name_common_name(tracker):
            self.logger.debug("common_name")
            return [NextState(states.DetectNameRespondCommonName.name, jump=True)]
        elif conditions.got_name_uncommon_name(tracker):
            self.logger.debug("uncommon_name")
            return [NextState(states.DetectNameRespondUncommonName.name, jump=True)]
        elif conditions.got_name_no_name(tracker):
            self.logger.debug("no_name")
            return [NextState(states.DetectNameRespondNoName.name, jump=True)]
        elif conditions.got_name_decline(tracker):
            self.logger.debug("decline name")
            return [NextState(states.DetectNameRespondDeclineName.name, jump=True)]
        else:
            self.logger.debug(f"fallback")
            dispatcher.respond("Okay.")
            return [NextState(states.HowAreYouPropose.name, jump=True)]


class DetectNameRespondCommonName(LGState):
    """
    aka. got_name_<any>

    detect the user's input for a name
    """

    name = "detect_name_respond_common_name"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        username = tracker.volatile_store['check_input_for_name']
        self.user_model._username = username

        predicted_gender = GenderPredictor().get_gender_prediction(username)
        self.user_model.gender = predicted_gender

        # patch in such that if returning user but without username, we'll count as returning user based on their answer
        if tracker.persistent_store.get('returning_user_without_username_flag', False):
            del tracker.persistent_store['returning_user_without_username_flag']
            tracker.persistent_store['is_returning_user'] = True

        dispatcher.respond_template('ask_name/got_name', {'name': username})

        """
        question handling
        """
        if self.handler.has_question(tracker):

            def ask_back_handler(qr: QuestionResponse):
                dispatcher.respond_template('exception/whats_your_name', {})

            events = self.handler.handle_question(
                dispatcher, tracker,
                ask_back_handler=ask_back_handler,
                follow_up_handler=lambda qr: None,
            )
            if events:
                return events

        # go to either social_template_exit or how_are_you
        return [NextState(states.HowAreYouPropose.name, jump=True)]


class DetectNameRespondUncommonName(LGState):
    """
    aka. got_name_<any>

    detect the user's input for a name
    """

    name = "detect_name_respond_uncommon_name"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        self.user_model._username = '<no_name>'
        self.user_model._username_uncommon = tracker.volatile_store['check_input_for_name']
        return [NextState(states.DetectNameRespondNoNameAgain.name, jump=True)]


class DetectNameRespondNoName(LGState):
    """
    aka. got_name_<any>

    detect the user's input for a name
    """

    name = "detect_name_respond_no_name"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        counter = tracker.persistent_store.get('detect_name_respond_no_name_counter') or 0
        if counter < 1:
            """
            question handling
            """
            if self.handler.has_question(tracker):

                def ask_back_handler(qr: QuestionResponse):
                    dispatcher.respond_template('exception/whats_your_name', {})

                events = self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=ask_back_handler,
                    follow_up_handler=lambda qr: None,
                )
                if events:
                    return events

            tracker.persistent_store['detect_name_respond_no_name_counter'] = counter + 1
            dispatcher.respond_template("ask_name/yes_only", {})
            return [NextState(states.DetectNameRespond.name)]
        else:
            return [NextState(states.DetectNameRespondNoNameAgain.name, jump=True)]


class DetectNameRespondDeclineName(LGState):
    """
    aka. got_name_<any>

    detect the user's input for a name
    """

    name = "detect_name_respond_decline_name"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        self.user_model._username = '<no_name>'
        dispatcher.respond(f"That's ok.")
        if utils.is_new_user(tracker):
            dispatcher.respond_template('exit/no_name', dict(my_name=utils.my_name))

        """
        question handling
        """
        if self.handler.has_question(tracker):

            def ask_back_handler(qr: QuestionResponse):
                dispatcher.respond_template('exception/whats_your_name', {})

            events = self.handler.handle_question(
                dispatcher, tracker,
                ask_back_handler=ask_back_handler,
                follow_up_handler=lambda qr: None,
            )
            if events:
                return events

        # go to either social_template_exit or how_are_you
        return [NextState(states.HowAreYouPropose.name, jump=True)]


class DetectNameRespondNoNameAgain(LGState):
    """
    aka. got_name_<any>

    detect the user's input for a name
    """

    name = "detect_name_respond_no_name_again"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        prev_state = self.module.state_tracker.curr_turn().prev_state()
        self.logger.debug(f"prev_state = {prev_state}")
        if (
            isinstance(prev_state, str) and
            prev_state not in {
                states.GreetReturningUser.name, states.GreetReturningUserAreYouNameRespond.name,
            }
        ):
            # only if new user, otherwise how_are_you is going to have an ack already
            dispatcher.respond_template('exit/no_name', dict(my_name=utils.my_name))

        """
        question handling
        """
        if self.handler.has_question(tracker):

            def ask_back_handler(qr: QuestionResponse):
                dispatcher.respond_template('exception/whats_your_name', {})

            events = self.handler.handle_question(
                dispatcher, tracker,
                ask_back_handler=ask_back_handler,
                follow_up_handler=lambda qr: None,
            )
            if events:
                return events

        # go to either social_template_exit or how_are_you
        return [NextState(states.HowAreYouPropose.name, jump=True)]
