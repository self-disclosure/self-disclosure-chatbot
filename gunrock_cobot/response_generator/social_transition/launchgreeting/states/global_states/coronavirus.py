from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.social_transition.launchgreeting import states
from response_generator.social_transition.launchgreeting.states.base import LGState
from response_generator.social_transition.launchgreeting.utils import utils
from response_generator.social_transition.launchgreeting.transitions import conditions


class CoronavirusRespond(LGState):

    name = "coronavirus_respond"
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker):
        if conditions.got_topic_coronavirus(tracker):
            return self.name

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template("special/coronavirus/ack/neu", {})
        dispatcher.respond_template('open_questions/general', {})

        tracker.persistent_store['is_first_module'] = False
        dispatcher.propose_continue('STOP')

        return [NextState(states.Initial.name)]
