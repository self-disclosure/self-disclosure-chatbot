import re
from typing import TYPE_CHECKING
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.social_transition.launchgreeting import states
from response_generator.social_transition.launchgreeting.states.base import LGState
from response_generator.social_transition.launchgreeting.utils import utils
from selecting_strategy.topic_module_proposal import EntityProposer

if TYPE_CHECKING:
    from response_generator.social_transition.launchgreeting.response_generator import LaunchGreetingModule


class Initial(LGState):
    """
    Entry point. Check if user is returning user.
    """

    name = "initial"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if utils.is_new_user(tracker):
            return [NextState(states.GreetNewUser.name, jump=True)]
        else:
            return [NextState(states.GreetReturningUser.name, jump=True)]


class GreetNewUser(LGState):
    """
    Greet user as if user is a new user.
    """

    name = "greet_new_user"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if tracker.persistent_store.get('is_first_module', True):
            tracker.persistent_store['is_first_module'] = True

        holiday_greet, _ = utils.holiday_greet()
        dispatcher.respond_template('init/new', dict(holiday_greet=holiday_greet))
        self.user_model._username = '<no_name>'
        return [NextState(states.DetectNameRespond.name)]


class GreetReturningUser(LGState):
    """
    Greet user as if user is a returnning user. Ask to confirm who they are.
    i.e. not confirming their returning user status
    """

    name = "greet_returning_user"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if tracker.persistent_store.get('is_first_module', True):
            tracker.persistent_store['is_first_module'] = True
        holiday_greet, _ = utils.holiday_greet()
        tracker.persistent_store['is_returning_user'] = False

        self.logger.info(f"username = {self.user_model.username}")

        if self.user_model.username:
            """
            with username, say "are you <username>?", listen for response
            """
            dispatcher.respond_template(
                "init/returning_with_check/with_name",
                dict(name=self.user_model.username, holiday_greet=holiday_greet)
            )
            return [NextState(states.GreetReturningUserAreYouNameRespond.name)]

        else:
            """
            without username, say "can you remind me of your name?", go to detect_name
            """
            tracker.persistent_store['returning_user_without_username_flag'] = True
            dispatcher.respond_template(
                "init/returning_with_check/without_name", dict(holiday_greet=holiday_greet)
            )
            return [NextState(states.DetectNameRespond.name)]


class GreetReturningUserAreYouNameRespond(LGState):
    """
    aka. returning_user_<any>_user
    respond to "can you remind me of your name?"
    if the same user, treat as true returning user.
    if different name, or "no", go to detect_name, ask for name.
    """

    name = "greet_returning_user_are_you_<name>_respond"

    @classmethod
    def name_correction_override(cls, tracker: Tracker, override: str):
        tracker.volatile_store['name_correction_override'] = override

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if tracker.persistent_store.get('is_first_module', True):
            tracker.persistent_store['is_first_module'] = True

        if (
            tracker.volatile_store.get('name_correction_override') == 'yes' or
            tracker.returnnlp.answer_positivity == 'pos' or
            re.search(r"^(yes )?i am ?((don't )?you(r|'re)? remember( me)?)?$", tracker.input_text)
        ):
            """
            if user say they're the same person
            """
            tracker.persistent_store['is_returning_user'] = True
            dispatcher.respond_template("ask_name/got_name", dict(name=self.user_model.username))

            dispatcher.respond_template("chitchat/returning_ack", dict())
            # todo. mention that we rememeber the discussed topic in the last conversation and propose an open question directly.
            propose = EntityProposer(tracker.user_attributes.ua_ref)
            result = propose.propose_entity_in_topic_profile()
            self.logger.info("propose entity results: {}".format(result))

            if result and len(result.strip()) > 0:
                dispatcher.respond(result)
                dispatcher.respond("I enjoyed chatting with you about that last time! ")
                dispatcher.respond("By the way, I'm curious, ")
            else:
                dispatcher.respond("I'm curious, ")
            dispatcher.respond_template('open_questions/return_user', {})

            tracker.persistent_store['is_first_module'] = False
            dispatcher.propose_continue('STOP')
            return [NextState(states.Initial.name)]

        else:
            """
            if user say neutral or negative positivity stuff, i.e.
            if user is different person
            """
            module: LaunchGreetingModule = self.module
            user_initializer = module.user_initializer
            if user_initializer:
                self.logger.info(
                    f"user_attribute being cleared for returning user: {getattr(self, 'state_manager_ref', None)}")
                user_initializer.init_for_returning_user_different_name()
                self.logger.info(
                    f"user_attribute cleared for returning user: {getattr(self, 'state_manager_ref', None)}")
            else:
                self.logger.warning(f"missing user_initializer. Check launch_greeting fsm custom init")

            dispatcher.respond_template('init/returning_different_user', {})

            return [NextState(states.DetectNameRespond.name)]
