import re
from .base import DailyLifeState
from response_generator.fsm2 import Dispatcher, Tracker, NextState
from selecting_strategy.module_selection import ModuleSelector, GlobalState
from nlu.constants import TopicModule
from .ack import regexes
from user_profiler.user_profile import UserProfile
from template_manager import Template

import logging
logger = logging.getLogger("dailylife.states")

# You can check social_template.yml and look for "DAILY LIFE PROPOASE NEXT MODULE"
# You can also check out ack.py for dailylife module
mapping = {
    "birth" : {
        "general":[TopicModule.MUSIC]
    },
    "social_media" : {
        "general":[TopicModule.MUSIC]
    },
    "pod_cast" : {
        "general":[TopicModule.BOOK]
    },
    "school" : {
        "general": [TopicModule.MOVIE],
    },
    "draw" : {
        "male":[TopicModule.TECHSCI],
        "general":[TopicModule.ANIMAL, TopicModule.MUSIC, TopicModule.TECHSCI],
    },
    "sleep" : {
        "general": [TopicModule.MOVIE, TopicModule.ANIMAL, TopicModule.MUSIC]
    },
    "family_kids" : {
        "male":[TopicModule.GAME],
        "general":[TopicModule.ANIMAL, TopicModule.MOVIE, TopicModule.GAME]
    },
    "family_parent" : {
        "general" : [TopicModule.MOVIE, TopicModule.ANIMAL, TopicModule.GAME],
        "male" : [TopicModule.GAME]
    },
    "family_general" : {
        "general":[TopicModule.MOVIE, TopicModule.ANIMAL, TopicModule.GAME],
        "male":[TopicModule.GAME]
    },
    "tablet_phone" : {
        "general":[TopicModule.MOVIE, TopicModule.ANIMAL, TopicModule.GAME],
    },
    "toys" : {
        "general":[TopicModule.ANIMAL, TopicModule.GAME],
        "male":[TopicModule.GAME]
    },
    "talk_to_bot" : {
        "general":[TopicModule.MOVIE, TopicModule.ANIMAL]
    },
    "relationship" : {
        "general":[TopicModule.ANIMAL]
    },
    "hangout" : {
        "general":[TopicModule.MOVIE, TopicModule.SPORTS, TopicModule.GAME],
        "male":[TopicModule.SPORTS, TopicModule.GAME],
        "female":[TopicModule.FASHION]
    },
    "play_with_friends" : {
        "general":[TopicModule.MOVIE, TopicModule.GAME]
    },
    "work" : {
        "general":[TopicModule.MUSIC]
    },
    "dance" : {
        "general":[TopicModule.MUSIC]
    },
    "chores_clean" : {
        "general":[TopicModule.MUSIC]
    },
    "my_birthday" : {
        "general":[TopicModule.MOVIE]
    },
    "youtube": {
        "general":[TopicModule.ANIMAL, TopicModule.GAME],
        "male":[TopicModule.GAME]
    },
    "stuck_home" : {
        "general":[TopicModule.MOVIE, TopicModule.GAME],
        "male":[TopicModule.GAME]
    },
    "gardening" : {
        "general":[TopicModule.MUSIC]
    },
    "climate_change" : {
        "general":[TopicModule.NEWS]
    }
}

class Propose(DailyLifeState):

    name = 'propose'

    def propose_next_topic(self, dispatcher, tracker):

        user_attributes_ref = tracker.user_attributes.ua_ref
        module_selector = ModuleSelector(user_attributes_ref)

        def get_topic(next_topic, dispatcher):
            if next_topic == "fashion":
                module_selector.global_state = GlobalState.ASK_CLOTHES
                dispatcher.respond("Going shopping can be a fun thing to do with you friends.")
                dispatcher.respond("Do you enjoy shopping for clothes with your friends?")
                return TopicModule.FASHION
            if next_topic == "fashion_style":
                module_selector.global_state = GlobalState.ASK_COMFORT_VS_PRACTICAL
                dispatcher.respond("When you go shopping, do you prefer to buy comfortable, practical clothes, or whatever looks the best?")
                return TopicModule.FASHION
            if next_topic == "movie_recent":
                dispatcher.respond("Watching movies with your friends can be a lot of fun.")
                dispatcher.respond("What's a recent movie you've seen?")
                module_selector.global_state = GlobalState.ASK_RECENT_MOVIE
                return TopicModule.MOVIE
            if next_topic == "movie":
                dispatcher.respond("Do you like to watch movies with your friends?")
                return TopicModule.MOVIE
            if next_topic == "sport":
                module_selector.global_state = GlobalState.ASK_FAV_SPORT
                dispatcher.respond("What's your favourite sport to play with your friends?")
                return TopicModule.SPORTS
            if next_topic == "game":
                module_selector.global_state = GlobalState.ASK_RECENT_PLAYED_GAME
                dispatcher.respond("What's a game you've played recently?")
                return TopicModule.GAME

        def propose(dispatcher, topic):
            # Temporary work around for movie
            if TopicModule.MOVIE == topic:
                dispatcher.propose_continue('UNCLEAR', topic)
                return [NextState('initial', jump=False)]

            dispatcher.propose_continue('STOP', topic)
            return [NextState('initial', jump=False)]

        next_topic = tracker.persistent_store.get("propose_topic", "default")
        topic = get_topic(next_topic, dispatcher)
        if topic: return propose(dispatcher, topic)

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        # Get a reference to user attributes
        user_attributes_ref = tracker.user_attributes.ua_ref

        # Initalize the module selector
        module_selector = ModuleSelector(user_attributes_ref)

        # Get a list of used topics
        used_topics = module_selector.used_topic_modules

        # Get the topic key
        mapping_key = ""
        for topic, regex in regexes.items():
            if re.search(regex, tracker.input_text):
                mapping_key = topic.replace("topic_", "")
                break

        next_topic = self.propose_next_topic(dispatcher, tracker)
        if next_topic:
            return next_topic

        # Get the user gender
        ua_ref = tracker.user_attributes.ua_ref
        user_profile = UserProfile(ua_ref)
        gender = user_profile.gender

        # Try first for the user gender, if that fails, try the general
        for key in [gender, 'general']:
            try:
                potential_topics_general = [x for x in mapping[mapping_key][key] if x.value not in used_topics]
            except Exception as error:
                potential_topics_general = []

                logger.info(potential_topics_general)

            for topic in potential_topics_general:
                try:
                    dispatcher.respond_template(f"daily_life_propose/{mapping_key}/{topic.value}", {})
                    dispatcher.propose_continue('STOP', topic)
                    module_selector.add_used_topic_module(topic.value)

                    # if topic.value == TopicModule.FASHION.value and (re.search(r"\b()\b"), tracker.input_text.lower()):
                    #     user_attributes_ref = tracker.module.response_generator_ref.state_manager.user_attributes
                    #     module_selector = ModuleSelector(user_attributes_ref)
                    #     module_selector.global_state = GlobalState.ASK_CLOTHES

                    return [NextState('initial', jump=False)]
                except Exception as error:
                    pass

        #when potential_topics_general is always null
        propose_module, _ = module_selector.get_next_modules()[0]
        module_selector.add_used_topic_module(propose_module)
        if propose_module:
            dispatcher.respond_template(f"open_question_transition/{propose_module}", {})
        else:
            dispatcher.respond_template(f"propose_topic_short/SOCIAL", {}, template = Template.transition)

        dispatcher.propose_continue("STOP")
        return [NextState('initial', jump=False)]