import logging
import re
from response_generator.fsm.utils import Dispatcher, Tracker
from nlu.constants import *
from user_profiler.user_profile import UserProfile

logger = logging.getLogger("fashion.states")

def gender(tracker):
    ua_ref = tracker.user_attributes.ua_ref
    user_profile = UserProfile(ua_ref)
    gender = user_profile.gender
    return gender

def simple_ack(dispatcher, tracker):
    if is_unsure(tracker):
        dispatcher.respond("That's okay if you're unsure.")
    elif is_maybe(tracker):
        dispatcher.respond("Maybe, you say?")
    elif is_pos(tracker):
        dispatcher.respond_template("ack/pos", {})
        # dispatcher.respond("That's great!")
    elif is_neg(tracker):
        dispatcher.respond_template("ack/neg", {})
        # dispatcher.respond("No worries.")
    elif is_neu(tracker):
        dispatcher.respond_template("ack/neu", {})
        # dispatcher.respond("Nice!")

def is_pos(tracker: Tracker):
    if tracker.returnnlp.answer_positivity is Positivity.pos: return True
    return False

def is_neu(tracker: Tracker):
    if tracker.returnnlp.answer_positivity is Positivity.neu: return True
    if re.search(r"(.*)(it is fine|its fine|it's fine|it is okay|its okay|it's okay)(.*)", tracker.input_text): return True
    return False

def is_neg(tracker: Tracker):
    if tracker.returnnlp.answer_positivity is Positivity.neg: return True
    return False

def is_maybe(tracker: Tracker):
    if re.search(r"(.*)(maybe|perhaps|sometimes|occasionally)(.*)", tracker.input_text) and num_concept(tracker) == 0: return True
    return False

def is_unsure(tracker: Tracker):
    if re.search(r"(.*)(i'm not sure|im not sure|i am not sure|i dont know|i don't know|i do not know)(.*)", tracker.input_text.lower()):
        return True
    return False