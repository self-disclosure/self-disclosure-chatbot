import re

from cobot_core.service_module import LocalServiceModule
import template_manager
from selecting_strategy.module_selection import ModuleSelector

template_topics = template_manager.Templates.topics


class Phatic_ResponseGenerator(LocalServiceModule):
    """topic_phatic: """

    def execute(self):
        curr_utt = self.input_data['text'].lower()
        # lexical_intents = utils.get_feature_by_key(self.state_manager.current_state, 'intent_classify')['lexical']
        username = self.state_manager.user_attributes.usr_name

        module_selector = ModuleSelector(self.state_manager.user_attributes)
        module, utt = module_selector.get_next_modules()[0]
        module_selector.add_used_topic_module(module)

        selector = 'phatic/'
        if re.search(r'morning|got up', curr_utt):
            selector += 'morning/'
            setattr(self.state_manager.user_attributes,
                    "propose_topic", module)

        elif re.search(r"afternoon", curr_utt):
            selector += 'afternoon/'
            setattr(self.state_manager.user_attributes,
                    "propose_topic", module)

        elif re.search(r"evening", curr_utt):
            selector += 'evening/'
            setattr(self.state_manager.user_attributes,
                    "propose_topic", module)

        elif re.search(r"night", curr_utt):
            selector += 'night/'

        else:
            selector += 'default/'
            setattr(self.state_manager.user_attributes,
                    "propose_topic", module)

        username = username if username != '' and username != '<no_name>' else ''

        return "{prefix} {suffix}".format(
            prefix=template_topics.utterance(selector=selector + 'prefix',
                                             slots={'name': username},
                                             user_attributes_ref=self.state_manager.user_attributes),
            suffix=template_topics.utterance(selector=selector + 'suffix',
                                             slots={'topic': utt},
                                             user_attributes_ref=self.state_manager.user_attributes)
        )