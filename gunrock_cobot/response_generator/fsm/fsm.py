import itertools
import logging
from collections import UserList
from typing import Iterable, List, Optional, Set, Type, TYPE_CHECKING, Union

from template_manager import Template, TemplateManager

from . import exceptions
from .events import NextState, PreviousState
from .state import State
from .utils.dispatcher import Dispatcher
from .utils.tracker import Tracker

if TYPE_CHECKING:
    from cobot_core.service_module import LocalServiceModule


logger = logging.getLogger('fsm.fsm')


class StateTracker:

    class Turn(UserList):

        TURN_THRESHOLD = 64

        def __add__(self, other: Iterable[str]) -> str:
            other = list(other)
            if len(self.data) + len(other) > self.TURN_THRESHOLD:
                raise exceptions.FSMStateTrackerTurnOverflow(len(self.data) + len(other), self)
            super().__add__(other)

        def append(self, item: str):
            if len(self.data) + 1 > self.TURN_THRESHOLD:
                raise exceptions.FSMStateTrackerTurnOverflow(len(self.data) + 1, self)
            super().append(item)

        def extend(self, other: Iterable[str]):
            other = list(other)
            if len(self.data) + len(other) > self.TURN_THRESHOLD:
                raise exceptions.FSMStateTrackerTurnOverflow(len(self.data) + len(other), self)
            super().extend(other)

        def curr_state_idx(self, offset=0) -> str:
            if not (0 <= offset < len(self.data)):
                raise exceptions.FSMStateTrackerIndexError(offset, None, (0, len(self.data)))
            return len(self.data) - 1 + offset

        def curr_state(self, offset=0) -> str:
            return self.data[self.curr_state_idx(offset)]

        def prev_state(self, offset=0) -> str:
            try:
                return self.curr_state(offset=(-1 - offset))
            except exceptions.FSMStateTrackerIndexError:
                return None

        def is_from_jump(self) -> bool:
            return len(self.data) > 1

    def __init__(self, state_list: List[List[str]]):
        self.data = [StateTracker.Turn(turn) for turn in state_list]
        self.transition()
        logger.debug(f"[FSM] state_tracker init: {self.data}")

    def json(self, *, flatten=False) -> Union[List[List[str]], List[str]]:
        data = (turn.data for turn in self.data)
        return list(itertools.chain.from_iterable(data) if flatten else data)

    def curr_turn_idx(self, offset=0) -> int:
        """
        Get the index of the current turn.
        params: offset (int) = 0: a negative integer that defines an offset
        """
        logger.debug(
            f"[FSM] curr_turn_idx: {len(self.data)}, {offset}, {0 <= offset < len(self.data)}")
        if not (0 <= offset < len(self.data)):
            logger.debug(f"[FSM] curr_turn_idx raise error", stack_info=True)
            raise exceptions.FSMStateTrackerIndexError(offset, None, (0, len(self.data)))
        return self._curr_turn_idx + offset

    def curr_turn(self, offset=0) -> 'Turn':
        return self.data[self.curr_turn_idx(offset)]

    def prev_turn(self, offset=0) -> 'Turn':
        try:
            return self.curr_turn(offset=(-1 - offset))
        except exceptions.FSMStateTrackerIndexError:
            return None

    def next_turn(self) -> 'Turn':
        offset = len(self.data) - self._curr_turn_idx
        logger.debug(f"[FSM] next_turn() offset={offset}")
        if offset == 1:
            self.data.append(self.Turn())
        logger.debug(f"[FSM] next_turn() data={self.data}")
        return self.curr_turn(offset=offset)

    def transition(self):
        self._curr_turn_idx = len(self.data) - bool(self.data)

    def is_from_jump(self) -> Optional[str]:
        return len(self.curr_turn()) > 1


class FSMModule:

    def __init__(self,
                 response_generator_ref: 'LocalServiceModule',
                 template: 'Template',
                 user_attributes_class: Type['FSMAttributeAdaptor'],
                 states_class: Type['State'],
                 *,
                 first_state: str = 'init'):
        self.response_generator_ref = response_generator_ref
        self.tm = TemplateManager(template, response_generator_ref.state_manager.user_attributes)
        self.ua = user_attributes_class(response_generator_ref.state_manager.user_attributes)
        self.state_class = states_class  # type: Type[State]

        self.state_tracker = StateTracker(self.ua.state_history or [[first_state]])

    def generate_response(self):
        dispatcher = Dispatcher(self.tm, self.ua)
        tracker = Tracker(self, self.ua)

        logger.debug(
            f"[FSM] transition begin. Current Turn: {self.state_tracker.curr_turn()}, {self.state_tracker.data}")

        # first check global states
        self._check_global_states_and_run(dispatcher, tracker, special_type={'global_state'})

        # running recursive state transitions
        self._transition(dispatcher, tracker)

        # commiting tracker
        tracker.commit()
        self.commit()

        # finalize
        logger.debug(f"[FSM] Cycle complete.")
        dispatcher._commit_propose_continue()
        return " ".join(dispatcher.responses)

    def commit(self):
        self.ua.state_history = self.state_tracker.json()

    def _proposed_states_from_enter_condition(self,
                                              dispatcher: Dispatcher, tracker: Tracker,
                                              special_type: Set[str]) -> Optional['State']:

        global_states = self.state_class.subclasses(special_type=special_type)  # type: List[State]
        for global_state in global_states:
            global_state = global_state(self.tm)  # type: State
            proposed_state = global_state.enter_condition(dispatcher, tracker)
            logger.debug(f"[FSM] global state '{global_state.name}' proposed proposed_state: {proposed_state}")

            if proposed_state:
                yield proposed_state

    def _check_global_states_and_run(self,
                                     dispatcher: Dispatcher, tracker: Tracker, special_type: Set[str]):
        logger.debug(f"[FSM] transition checking special states.")
        proposed_state = next(self._proposed_states_from_enter_condition(dispatcher, tracker, special_type), None)
        if proposed_state:
            logger.debug(f"[FSM] global state selected: '{proposed_state}'")
            self.state_tracker.curr_turn().append(proposed_state)

    def _transition(self, dispatcher: Dispatcher, tracker: Tracker):

        # begin state running
        curr_state = self.state_class.from_name(self.state_tracker.curr_turn().curr_state())(self.tm)

        logger.debug(f"[FSM] Running state... \n"
                     f"curr_state:\t{curr_state.name}\n"
                     "tracker:\t{}".format(dict(
                         volatile=tracker.volatile_store, last_utt=tracker.last_utt_store,
                         persistent=tracker.persistent_store)))

        events = curr_state.run(dispatcher, tracker)

        logger.debug(f"[FSM] State run complete.\n"
                     f"State:\t{curr_state.name}\n"
                     f"Response:\t{dispatcher.responses}\n"
                     f"Returned events: {events}\n"
                     "Tracker:\t{}".format(dict(
                         volatile=tracker.volatile_store, one_turn=tracker.one_turn_store,
                         last_utt=tracker.last_utt_store, persistent=tracker.persistent_store)))

        # obtaining next state
        next_state: Union[NextState, PreviousState] = next(
            (event for event in events if isinstance(event, (NextState, PreviousState))), None)
        if not next_state:
            raise exceptions.FSMTransitionEventError(curr_state.name, events)
        logger.debug(f"[FSM] next_state: {next_state}")

        # recursively call next state or save to next turn
        if isinstance(next_state, NextState) and next_state.jump is True:
            logger.debug(f"[FSM] Jumping to next state: {next_state.state}")
            self.state_tracker.curr_turn().append(next_state.state)
            self._transition(dispatcher, tracker)

        elif isinstance(next_state, PreviousState):
            prev_state = self.state_tracker.curr_turn().prev_state()
            logger.debug(f"[FSM] Jumping to previous state: {prev_state}")
            self.state_tracker.curr_turn().append(prev_state)
            if next_state.jump is True:
                self._transition(dispatcher, tracker)

        else:
            self.state_tracker.next_turn().append(next_state.state)
