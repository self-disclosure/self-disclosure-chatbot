
from typing import List, Tuple, Type, TYPE_CHECKING

if TYPE_CHECKING:
    from .state import State  # noqa: F401
    from .utils import StateTracker  # noqa: F401


class FSMError(Exception):

    def __init__(self, message: str = None):
        message = f"[FSM] {message}"
        super().__init__(message)


class FSMTransitionError(FSMError):
    pass


class FSMTransitionEventError(FSMError):

    def __init__(self, curr_state: str, events: List['Event'], message: str = None):
        message = message or (
            f"Events must contain at least a NextState() Event. "
            f"(curr_state={curr_state}, events={events})"
        )
        super().__init__(message)
        self.curr_state = curr_state
        self.events = events


class FSMStateTrackerError(FSMError):
    pass


class FSMStateTrackerIndexError(FSMStateTrackerError):

    def __init__(self, offset: int, new_offset: int, bounds: Tuple[int, int], message: str = None):
        message = message or (
            f"StateTracker index offset exceeds bounds. "
            f"(offset={offset}, new_offset={new_offset}, bounds={bounds})"
        )
        super().__init__(message)
        self.offset = offset
        self.new_offset = new_offset
        self.bounds = bounds


class FSMStateTrackerTurnOverflow(FSMStateTrackerError):

    def __init__(self, length: int, turn: 'StateTracker.Turn', message: str = None):
        message = message or (
            f"StateTracker.Turn exceeds maximum length ({turn.TURN_THRESHOLD}). "
            f"(len(turn)={length}, turn={turn})"
        )
        super().__init__(message)
        self.length = length
        self.turn = turn


class FSMNewTurnError(FSMStateTrackerError):

    def __init__(self, state: str, state_tracker: 'StateTracker', message: str = None):
        message = message or (
            f"Cannot append new turn. "
            f"(Turn={state}, "
            f"existing turns: {state_tracker})"
        )
        super().__init__(message)
        self.state_name = state


class FSMStateError(FSMError):
    pass


class FSMStateInitError(FSMStateError):

    def __init__(self, name: str, state_class: Type['State'], message: str = None):
        message = message or (
            f"State transition has no valid next_state. "
            f"(search name={name}, "
            f"subclasses={state_class.__subclasses__()})."
        )
        super().__init__(message)
        self.state_name = name
