import logging
from typing import Iterator, List, Optional, Set, Type, TYPE_CHECKING

if TYPE_CHECKING:
    from template_manager import TemplateManager
    from .utils import Dispatcher  # noqa: F401


class LoggerAdapter(logging.LoggerAdapter):
    def __init__(self, prefix: str, state_name: str, logger):
        super(LoggerAdapter, self).__init__(logger, {})
        self.prefix = prefix
        self.state_name = state_name

    def process(self, msg, kwargs):
        return f"[{self.prefix}] <{self.state_name}>: {msg}", kwargs


class State:
    @classmethod
    def subclass(cls, name: str) -> 'State':
        return cls.from_name(name)

    @classmethod
    def from_name(cls, name: str) -> Type['State']:
        """
        Get a State class from its name.
        Raise FSMStateInitError if name is invalid.
        """

        state = next(cls.subclasses(name=name), None)
        return state

    @classmethod
    def subclasses(cls, *, name: str = None, special_type: Set[str] = None) -> Iterator[Type['State']]:
        states = (subclass for subclass in cls.__subclasses__())
        if name:
            states = filter(lambda s: s.name == name, states)
        elif special_type and all(isinstance(i, str) for i in special_type):

            def filter_condition(state: Type['State']):
                lhs = state.special_type
                if isinstance(lhs, str):
                    lhs = {lhs}
                elif isinstance(lhs, list):
                    lhs = set(lhs)
                else:
                    return False
                return lhs & special_type

            states = filter(filter_condition, states)

        return states

    @classmethod
    def has_subclass(cls, name: str) -> bool:
        return cls.from_name(name) is not None

    def __init__(self, tm: 'TemplateManager'):
        self.tm = tm
        self.logger = LoggerAdapter(
            tm.template.name.upper(),
            self.name,
            logging.getLogger(self.__class__.__name__)
        )

    name: str = NotImplementedError
    special_type: Set[str] = None

    # MARK: - Main driver methods

    def enter_condition(self, dispatcher: Type['Dispatcher'], tracker: Type['Tracker']) -> Optional[str]:
        return

    def run(self, dispatcher: Type['Dispatcher'], tracker: Type['Tracker']) -> List[Type['Event']]:
        return NotImplementedError
