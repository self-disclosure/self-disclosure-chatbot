import dataclasses
import json
import logging
import pydoc
import re
import warnings
from collections import defaultdict
from enum import Enum
from typing import List, Optional, TYPE_CHECKING, Union

from cobot_python_sdk.user_attributes import UserAttributes

from nlu.dataclass import CentralElement, ReturnNLP

from .attribute_adaptor import FSMAttributeAdaptor

if TYPE_CHECKING:
    from ..fsm import FSMModule, StateTracker


logger = logging.getLogger('fsm.utils.tracker')


class TrackerBase:

    def __init__(self, ua: Union['FSMAttributeAdaptor', UserAttributes]):
        if isinstance(ua, UserAttributes):
            ua = FSMAttributeAdaptor(ua)

        self.user_attributes: FSMAttributeAdaptor = ua
        self._central_element: CentralElement = None
        self._returnnlp: ReturnNLP = None

        self.volatile_store = defaultdict(str)
        self.one_turn_store = TrackerStore()
        self.last_utt_store = TrackerStore(**self.user_attributes._storage.get('last_utt') or {})
        self.persistent_store = TrackerStore(**self.user_attributes._storage.get('persistent') or {})

        def __repr__(self):
            return "{}<module: {}>({})".format(
                self.__class__.__name__,
                self.module.__class__.__name__,
                repr({
                    'volatile_store': self.volatile_store,
                    'one_turn_store': self.one_turn_store,
                    'last_utt_store': self.last_utt_store,
                    'persistent_store': self.persistent_store,
                }))

    def commit(self):
        self.user_attributes._storage['last_utt'] = self.one_turn_store
        self.user_attributes._storage['persistent'] = self.persistent_store

    # MARK: - Input Data

    @property
    def input_text(self) -> str:
        return self.module.response_generator_ref.input_data['text']

    def user_response(self, *,
                      central_element=False,
                      coference=True,
                      ) -> str:
        return self.module.response_generator_ref.input_data['text']

    def last_bot_response(self, *, ssml=True) -> Optional[str]:
        last_state = self.module.response_generator_ref.state_manager.last_state
        if last_state and isinstance(last_state, dict):
            resp = last_state.get('response')
            if not ssml and isinstance(resp, str):
                resp = re.sub(r"<[^>]+>", "", resp)
            return resp

    @property
    def central_element(self) -> CentralElement:
        try:
            if not self._central_element:
                ce = self.module.response_generator_ref.input_data['central_elem']
                self._central_element = CentralElement.from_dict(ce)
            return self._central_element
        except KeyError:
            logger.warn(f"[FSM] FSM did not receive a valid central_element. ce: {ce}")
            return CentralElement()

    @property
    def returnnlp(self) -> ReturnNLP:
        try:
            if not self._returnnlp:
                rnlp = self.module.response_generator_ref.input_data['returnnlp']
                self._returnnlp = ReturnNLP(rnlp)
            return self._returnnlp
        except (KeyError, TypeError):
            logger.warn(f"[FSM] FSM did not receive a valid returnnlp. rnlp: {rnlp}")
            return ReturnNLP([])

    def conversation_count(self) -> int:
        return self.user_attributes._get_raw('visit') or 1


class Tracker(TrackerBase):

    def __init__(self, module: 'FSMModule', ua: 'FSMAttributeAdaptor'):
        super().__init__(ua)
        self._module = module

    def __repr__(self):
        return super().__repr__()

    # MARK: - State Transitions

    @property
    def state_tracker(self) -> 'StateTracker':
        return self.module.state_tracker

    @property
    def previous_states(self) -> List[str]:
        warnings.warn("User tracker.state_tracker instead.", DeprecationWarning, stacklevel=2)
        return self.state_tracker.json(flatten=True)

    @property
    def curr_state_idx(self) -> int:
        warnings.warn("User tracker.state_tracker instead.", DeprecationWarning, stacklevel=2)
        return self.state_tracker.curr_turn_idx()

    @curr_state_idx.setter
    def curr_state_idx(self, value: int):
        warnings.warn("User tracker.state_tracker instead. This has no effect.", DeprecationWarning, stacklevel=2)

    @property
    def module(self) -> 'FSMModule':
        return self._module

    # MARK: - State Storage

    @property
    def is_last_state_open_question(self) -> bool:
        return self.module.response_generator_ref.state_manager.last_state.get("bot_ask_open_question", False)

    @property
    def is_from_jump(self) -> Optional[str]:
        if 'is_from_jump' not in self.volatile_store:
            self.volatile_store['is_from_jump'] = False

        return self.previous_states[-1] if self.volatile_store['is_from_jump'] else None

    @is_from_jump.setter
    def is_from_jump(self, value: bool):
        if not isinstance(value, bool):
            logger.warn(f"is_from_jump is being set with invalid value: {value}")
            return
        self.volatile_store['is_from_jump'] = value


class TrackerStore(dict):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __getitem__(self, key):
        logger.debug(f"[TRACKER] getting item: {key}")

        def replace_dataclass(item):
            class_type = pydoc.locate(item[0][len("__dataclass__"):])
            logger.debug(f"[TRACKER] replace_dataclass class_type: {class_type}")
            try:
                if not class_type:
                    raise ValueError(f"replace_dataclass cannot find valid class_type: {class_type}")

                item = json.loads(item[1])
                if not isinstance(item, dict):
                    raise ValueError(f"replace_dataclass item is not dict: {item}")

                return class_type(**item)
            except Exception as e:
                logger.warn(f"[TRACKER] replacing dataclass failed: {item}, {e}")
                return

        def replace_class(item, classtype: str):
            class_type = pydoc.locate(item[0][len("__{classtype}__"):])
            logger.debug(f"[TRACKER] replace_class class_type: {class_type}")
            try:
                if not class_type:
                    raise ValueError(
                        f"replace_class cannot find valid class_type: {class_type}, classtype str: {classtype}")

                item = json.loads(item[1])
                return class_type[item]
            except Exception as e:
                logger.warn(f"[TRACKER] replacing class failed: {item}, classtype str {classtype}, {e}")
                return

        value = super().__getitem__(key)

        def crawl_check(value_):
            if value_ and isinstance(value_, list) and len(value_) == 2 and isinstance(value_[0], str):
                try:
                    if value_[0].startswith("__dataclass__"):
                        value_ = replace_dataclass(value_)
                    elif value_[0].startswith("__enum__"):
                        value_ = replace_class(value_, class_type='enum')
                except Exception:
                    pass
            elif value_ and isinstance(value_, list):
                for idx, v in enumerate(value_):
                    value_[idx] = crawl_check(v)
            elif value_ and isinstance(value_, dict):
                for k, v in value_.items():
                    value_[k] = crawl_check(v)
            return value_

        return crawl_check(value)

    def __setitem__(self, key, value):
        def replace_dataclass(item):
            item: dataclasses.dataclass
            return [f"__dataclass__{_fullname(item)}", json.dumps(item.__dict__)]

        def replace_class(item, classtype: str):
            return [f"__{classtype}__{_fullname(item)}", json.dumps(item.name)]

        def crawl_check(value_):
            if dataclasses.is_dataclass(value_):
                value_ = replace_dataclass(value_)
            elif isinstance(value_, Enum):
                value_ = replace_class(value_, classtype='enum')
            elif isinstance(value_, list):
                for idx, v in enumerate(value_):
                    value_[idx] = crawl_check(v)
            elif isinstance(value_, dict):
                for k, v in value_.items():
                    value_[k] = crawl_check(v)
            return value_

        value = crawl_check(value)
        super().__setitem__(key, value)

    def get(self, key, default=None):
        try:
            return self.__getitem__(key)
        except KeyError:
            return default


def _fullname(o):
    # o.__module__ + "." + o.__class__.__qualname__ is an example in
    # this context of H.L. Mencken's "neat, plausible, and wrong."
    # Python makes no guarantees as to whether the __module__ special
    # attribute is defined, so we take a more circumspect approach.
    # Alas, the module name is explicitly excluded from __qualname__
    # in Python 3.

    module = o.__class__.__module__
    if module is None or module == str.__class__.__module__:
        return o.__class__.__name__  # Avoid reporting __builtin__
    else:
        return module + '.' + o.__class__.__name__
