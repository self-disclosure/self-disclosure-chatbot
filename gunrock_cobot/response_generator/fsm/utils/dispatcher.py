import logging
from typing import Dict, Optional

from nlu.constants import TopicModule
from template_manager import Template, TemplateManager

from .attribute_adaptor import FSMAttributeAdaptor


logger = logging.getLogger('fsm.utils.dispatcher')


class Dispatcher:

    def __init__(self, tm: TemplateManager, ua: FSMAttributeAdaptor):
        self.tm = tm
        self.ua = ua

        self.responses = []
        self._propose_continue = 'CONTINUE'
        self._propose_topic: Optional[TopicModule] = None

    def respond(self, text: str):
        self.responses.append(text)

    def respond_template(self, selector: str, slots: Dict[str, str], embedded_info: dict = None):
        try:
            utt = self.tm.speak(selector, slots, embedded_info)
            self.respond(utt)
        except (KeyError, ValueError) as e:
            logger.warn(f"Dispatcher::respond_template encountered error: {e}")

    def respond_with_alt_template(
            self, template: Template, selector: str, slots: Dict[str, str], embedded_info: dict = None):
        try:
            tm = TemplateManager(template, self.tm.user_attributes.ua_ref)
            utt = tm.speak(selector, slots, embedded_info)
            self.respond(utt)
        except (KeyError, ValueError) as e:
            logger.warn(f"Dispatcher::respond_template encountered error: {e}")

    def respond_error(self, selector: str, slots: Dict[str, str] = {}, embedded_info: dict = None):
        self.respond_template(f"error/{selector}", slots, embedded_info)

    def respond_remove(self, last: int):
        if len(self.responses) >= last:
            del self.responses[-last:]

    def propose_continue(self, state: str, topic: TopicModule = None):
        if state not in {'CONTINUE', 'STOP', 'UNCLEAR'}:
            raise ValueError(f"Dispatcher received invalid propose_continue ({state})")
        self._propose_continue = state
        self._propose_topic = topic

    def _commit_propose_continue(self):
        self.ua.propose_continue = self._propose_continue
        if self._propose_topic:
            self.ua.propose_topic = self._propose_topic.value
