from dataclasses import dataclass, field


class Event:
    pass


@dataclass
class NextState(Event):
    state: str
    jump: bool = field(default=False)


class PreviousState(Event):
    jump: bool = field(default=False)


@dataclass
class Reset(Event):
    level: str
