import itertools
import json
import logging
import numbers
import re
from dataclasses import dataclass, field
from functools import reduce
from typing import Callable, Dict, Iterable, Iterator, List, Match, Optional, Set, Type

from response_generator.fsm.utils import Tracker
from nlu.dataclass import ReturnNLP, ReturnNLPSegment


logger = logging.getLogger(__name__)


@dataclass(frozen=True)
class EntityItem:
    noun: str
    description: str
    confidence: float
    canonical: Optional[str] = field(default=None)
    positivity: bool = field(default=True)

    def __hash__(self):
        return hash(self.noun)

    def __eq__(self, other):
        # logger.debug(f"EQUAL: other = {other} {type(other)}, self = {self}")
        if isinstance(other, EntityItem):
            return (
                self.noun.lower() == other.noun.lower() and
                self.positivity == other.positivity
            )
        elif isinstance(other, str):
            return self.positivity and self.noun.lower() == other.lower()
        else:
            return False

    def __gt__(self, other):
        if isinstance(other, EntityItem):
            return self.confidence > other.confidence
        elif isinstance(other, numbers.Number):
            return self.confidence > other
        else:
            return False

    def __ge__(self, other):
        if isinstance(other, EntityItem):
            return self.confidence >= other.confidence
        elif isinstance(other, numbers.Number):
            return self.confidence >= other
        else:
            return False

    def __lt__(self, other):
        if isinstance(other, EntityItem):
            return self.confidence < other.confidence
        elif isinstance(other, numbers.Number):
            return self.confidence < other
        else:
            return False

    def __le__(self, other):
        if isinstance(other, EntityItem):
            return self.confidence <= other.confidence
        elif isinstance(other, numbers.Number):
            return self.confidence <= other
        else:
            return False

    @classmethod
    def canonical_map(cls) -> Dict[str, str]:
        return {}

    @classmethod
    def default_description(cls) -> str:
        return f"__{cls.__name__}__"

    @classmethod
    def whiteblacklist(cls) -> Dict[str, Dict[str, str]]:
        return dict(whitelist=None, blacklist=None)

    @classmethod
    def detection_mode(cls) -> str:
        return 'both'

    @classmethod
    def detect(
        cls, tracker: Tracker, *,
        include_negative_positivity=False,
        rnlp_slice: Optional[slice] = None,
    ) -> List[Type['EntityItem']]:
        return Detector(cls)(
            tracker,
            include_negative_positivity=include_negative_positivity,
            rnlp_slice=rnlp_slice
        )

    @classmethod
    def from_regex_match(cls, noun: str, canonical: str, positivity=True):
        return cls(
            positivity=positivity, noun=noun.lower(), canonical=canonical,
            description=cls.default_description(), confidence=1000000.0)

    @classmethod
    def from_conceptdata(cls, noun: str, concept_data: ReturnNLPSegment.Concept.ConceptData, positivity=True):
        return cls(
            positivity=positivity, noun=noun.lower(), canonical=None, description=concept_data.description.lower(),
            confidence=concept_data.confidence)

    @classmethod
    def from_googlekg(cls, kg: ReturnNLPSegment.GoogleKG, positivity=True):
        return cls(
            positivity=positivity, noun=kg.name.lower(), canonical=None,
            description=kg.description.lower(), confidence=kg.result_score)

    def __post_init__(self):
        if not self.canonical:
            for entity, regex in self.canonical_map().items():
                if re.search(regex, self.noun):
                    object.__setattr__(self, 'canonical', entity)
                    return
            object.__setattr__(self, 'canonical', None)

    def __ua_encode__(self) -> str:
        return json.dumps(self.__dict__)

    @classmethod
    def __ua_decode__(cls, value: str):
        try:
            value = json.loads(value)
            return cls(**value)
        except (json.JSONDecodeError, TypeError) as e:
            logger.warning(e, exc_info=True)
            return cls(noun="", description="", confidence=0.0)


@dataclass
class Detector:

    itemtype: Type[EntityItem]
    mode: str = field(init=False)
    mode_map: Set[str] = field(init=False)

    regex_validator: Optional[Callable[[Match], Iterable[Type[EntityItem]]]] = None
    concept_validator: Optional[Callable[[ReturnNLP], Iterable[Type[EntityItem]]]] = None
    googlekg_validator: Optional[Callable[[ReturnNLP], Iterable[Type[EntityItem]]]] = None

    return_filter: Optional[Callable[[Type[EntityItem]], bool]] = None
    extra_entities: Optional[Callable[[], Iterator[Type[EntityItem]]]] = None

    def __post_init__(self):
        modes = {
            'local': (1, 0, 0),
            'remote': (0, 1, 1),
            'concept': (0, 1, 0),
            'googlekg': (0, 0, 1),
            'both': (1, 1, 1)}
        self.mode = self.itemtype.detection_mode() if self.itemtype.detection_mode() in modes else 'both'
        self.mode_map = set(itertools.compress(('regex', 'concept', 'googlekg'), modes[self.mode]))
        logger.debug(f"[DETECTOR] mode: {self.itemtype.detection_mode()}, {self.mode}, {self.mode_map}")

    def __call__(
        self, tracker: Tracker, *,
        include_negative_positivity=False,
        rnlp_slice: Optional[slice] = None,
    ) -> List[Type[EntityItem]]:

        if not rnlp_slice:
            rnlp_slice = slice(len(tracker.returnnlp))

        def regex_entities(canonical_map: Dict[str, str]) -> Iterator[Type[EntityItem]]:
            for entity_canon, regex in canonical_map.items():
                for segment in tracker.returnnlp[rnlp_slice]:
                    segment: ReturnNLPSegment
                    positivity = not segment.has_intent({'ans_dislike'})
                    if not positivity and not include_negative_positivity:
                        continue
                    match = re.search(regex, segment.text, re.IGNORECASE)
                    if match:
                        yield self.itemtype.from_regex_match(match.group(), entity_canon, positivity=positivity)

        def concept_entities(rnlp: ReturnNLP) -> Iterator[Type[EntityItem]]:
            for segment in rnlp:
                segment: ReturnNLPSegment
                positivity = not segment.has_intent({'ans_dislike'})
                if not positivity and not include_negative_positivity:
                    continue
                for concept in segment.concept:
                    if self._is_valid_noun(concept.noun):
                        filtered_cds = (
                            cd for cd in concept.data if self._is_valid_description(cd.description))
                        try:
                            concept_data = reduce(
                                lambda prev, cd: cd if cd.confidence > prev.confidence else prev,
                                filtered_cds)
                            yield self.itemtype.from_conceptdata(concept.noun, concept_data, positivity=positivity)
                        except TypeError:
                            pass

        def googlekg_entities(rnlp: ReturnNLPSegment) -> Iterator[Type[EntityItem]]:
            for segment in rnlp:
                segment: ReturnNLPSegment
                positivity = not segment.has_intent({'ans_dislike'})
                if not positivity and not include_negative_positivity:
                    continue
                for kg in segment.googlekg:
                    if self._is_valid_description(kg.description) and self._is_valid_noun(kg.name):
                        yield self.itemtype.from_googlekg(kg, positivity=positivity)

        logger.debug(f"[DETECTOR] ({self}) begin detection: \n"
                     f"concept: {tracker.returnnlp[rnlp_slice].concept}\n"
                     f"googlgkg: {tracker.returnnlp[rnlp_slice].googlekg}")

        entities_iters: Iterable[Type[EntityItem]] = []

        if self.extra_entities is not None:
            entities_iters.append(self.extra_entities())

        if 'regex' in self.mode_map:
            entities_iters.append(regex_entities(self.itemtype.canonical_map()))

        if 'concept' in self.mode_map:
            if self.concept_validator is not None:
                entities_iters.append(self.concept_validator(tracker.returnnlp[rnlp_slice]))
            else:
                entities_iters.append(concept_entities(tracker.returnnlp[rnlp_slice]))

        if 'googlekg' in self.mode_map:
            if self.googlekg_validator is not None:
                entities_iters.append(self.googlekg_validator(tracker.returnnlp[rnlp_slice]))
            else:
                entities_iters.append(googlekg_entities(tracker.returnnlp[rnlp_slice]))

        entities_iters = itertools.chain.from_iterable(entities_iters)
        entities = self._filter_entities(entities_iters)
        entities.sort(key=lambda entity: entity.confidence, reverse=True)

        if self.return_filter is not None:
            entities = [e for e in entities if self.return_filter(e)]

        logger.debug(f"[DETECTOR] detected {self.itemtype.__name__} entities: \n{entities}")
        return entities

    def _filter_entities(self, entities: Iterable[Type[EntityItem]]) -> List['Type[EntityItem]']:
        hash_table = {}
        for e in entities:
            prev = hash_table.get(e)
            if not prev or e > prev:
                hash_table[e] = e
        return list(hash_table)

    def _is_valid_description(self, description: str) -> bool:
        """
        Return if the input knowledge description describes a valid entity NER.
        """
        blacklist = self.itemtype.whiteblacklist().get('description', {}).get('blacklist')
        whitelist = self.itemtype.whiteblacklist().get('description', {}).get('whitelist')

        description = description.strip().lower()
        return bool(
            not (re.search(blacklist, description, re.I) is not None if blacklist else False) and
            (re.search(whitelist, description, re.I) is not None if whitelist else True)
        )

    def _is_valid_noun(self, noun: str) -> bool:
        blacklist = self.itemtype.whiteblacklist().get('noun', {}).get('blacklist')
        return bool(
            not (re.search(blacklist, noun, re.I) is not None if blacklist else False)
        )
