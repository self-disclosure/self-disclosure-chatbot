import itertools
from collections import UserList
from typing import Iterable, List, Union

from .. import exceptions
from .logger import FSMLogger


logger = FSMLogger('FSM2', 'FSMModule', 'fsm.state_tracker')


class StateTracker:

    class Turn(UserList):

        TURN_THRESHOLD = 64

        def __add__(self, other: Iterable[str]) -> str:
            other = list(other)
            if len(self.data) + len(other) > self.TURN_THRESHOLD:
                raise exceptions.FSMStateTrackerTurnOverflow(len(self.data) + len(other), self)
            super().__add__(other)

        def append(self, item: str):
            if len(self.data) + 1 > self.TURN_THRESHOLD:
                raise exceptions.FSMStateTrackerTurnOverflow(len(self.data) + 1, self)
            super().append(item)

        def extend(self, other: Iterable[str]):
            other = list(other)
            if len(self.data) + len(other) > self.TURN_THRESHOLD:
                raise exceptions.FSMStateTrackerTurnOverflow(len(self.data) + len(other), self)
            super().extend(other)

        def curr_state_idx(self, offset=0) -> str:
            if not (0 <= -offset < len(self.data)):
                raise exceptions.FSMStateTrackerIndexError(offset, None, (0, len(self.data)))
            return len(self.data) - 1 + offset

        def curr_state(self, offset=0) -> str:
            return self.data[self.curr_state_idx(offset)]

        def prev_state(self, offset=0) -> str:
            try:
                return self.curr_state(offset=(-1 - offset))
            except exceptions.FSMStateTrackerIndexError:
                return None

        def is_from_jump(self) -> bool:
            return len(self.data) > 1

    def __init__(self, state_list: List[List[str]]):
        self.data = [StateTracker.Turn(turn) for turn in state_list]
        self.transition()
        logger.debug(f"state_tracker init: {self.data}")

    def __repr__(self):
        return f"{self.json()}"

    def json(self, *, flatten=False) -> Union[List[List[str]], List[str]]:
        data = (turn.data for turn in self.data)
        return list(itertools.chain.from_iterable(data) if flatten else data)

    def curr_turn_idx(self, offset=0) -> int:
        """
        Get the index of the current turn.
        params: offset (int) = 0: a negative integer that defines an offset
        """
        index = self._curr_turn_idx + offset
        logger.debug(
            f"curr_turn_idx: turn = {len(self.data)}, "
            f"offset = {offset}, index = {index}")
        if not 0 <= index < len(self.data):
            logger.debug(f"curr_turn_idx raise error", exc_info=True)
            raise exceptions.FSMStateTrackerIndexError(offset, None, (0, len(self.data)))
        return index

    def curr_turn(self, offset=0) -> 'Turn':
        return self.data[self.curr_turn_idx(offset)]

    def prev_turn(self, offset=0) -> 'Turn':
        try:
            return self.curr_turn(offset=(offset - 1))
        except exceptions.FSMStateTrackerIndexError:
            return None

    def next_turn(self) -> 'Turn':
        self.data.append(self.Turn())
        return self.curr_turn(offset=(1))
        # offset = len(self.data) - self._curr_turn_idx
        # logger.debug(f"next_turn() offset={offset}")
        # if offset == 1:
        #     self.data.append(self.Turn())
        # logger.debug(f"next_turn() data={self.data}")
        # return self.curr_turn(offset=offset)

    def transition(self):
        self._curr_turn_idx = len(self.data) - bool(self.data)
