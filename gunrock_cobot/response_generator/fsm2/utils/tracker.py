import json
import pydoc
import re
from collections import defaultdict
from enum import Enum
from typing import Any, Dict, List, Optional, Tuple

from nlu.dataclass import CentralElement, ReturnNLP

from .logger import FSMLogger
from .attribute_adaptor import FSMAttributeAdaptor


logger = FSMLogger('FSM2', 'TRACKER', 'fsm2.tracker')


class Tracker:

    def __init__(
        self,
        input_data: Dict[str, Any],
        ua_adaptor: FSMAttributeAdaptor,
        last_state: Optional[Dict[str, Any]] = None,
    ):
        self.input_data = input_data
        self.user_attributes = ua_adaptor
        self._central_element: CentralElement = None
        self._returnnlp: ReturnNLP = None

        self.last_state = last_state

        self.volatile_store = defaultdict(str)
        self.one_turn_store = TrackerStore()
        self.last_utt_store = TrackerStore(**self.user_attributes.get('last_utt') or {})
        self.persistent_store = TrackerStore(**self.user_attributes.get('persistent') or {})

        def __repr__(self):
            return "{}<module: {}>({})".format(
                self.__class__.__name__,
                self.module.__class__.__name__,
                repr({
                    'volatile_store': self.volatile_store,
                    'one_turn_store': self.one_turn_store,
                    'last_utt_store': self.last_utt_store,
                    'persistent_store': self.persistent_store,
                }))

    def commit(self):
        self.user_attributes['last_utt'] = self.one_turn_store
        self.user_attributes['persistent'] = self.persistent_store

    # MARK: - Input Data

    @property
    def input_text(self) -> str:
        try:
            return self.input_data['text']
        except KeyError:
            logger.warning(f"")
            return ""

    @property
    def system_acknowledgment(self) -> Tuple[str, str]:
        try:
            ack = self.input_data['system_acknowledgement']
            return ack['output_tag'], ack['ack']
        except KeyError:
            return "", ""

    def sys_ack(self, exclude_tag: List[str] = None) -> Tuple[Optional[str], Optional[str], Optional[str]]:
        """return utt, tag, source"""
        try:
            ack = self.input_data['system_acknowledgement']
            utt, tag, source = tuple(ack[k] for k in ['ack', 'output_tag', 'source'])
        except KeyError:
            utt, tag, source = (None, ) * 3

        if tag not in {'ack_question_idk', *(exclude_tag or [])}:
            return utt, tag, source
        else:
            return (None, ) * 3

    def last_bot_response(self, *, ssml=True) -> Optional[str]:
        if self.last_state and isinstance(self.last_state, dict):
            resp = self.last_state.get('response')
            if not ssml and isinstance(resp, str):
                resp = re.sub(r"<[^>]+>", "", resp)
            return resp

    @property
    def central_element(self) -> CentralElement:
        try:
            if not self._central_element:
                ce = self.input_data['central_elem']
                self._central_element = CentralElement.from_dict(ce)
            return self._central_element
        except KeyError:
            logger.warn(f"did not receive a valid central_element.")
            return CentralElement.from_dict({})

    @property
    def returnnlp(self) -> ReturnNLP:
        try:
            if not self._returnnlp:
                rnlp = self.input_data['returnnlp']
                self._returnnlp = ReturnNLP(rnlp)
            return self._returnnlp
        except (KeyError, TypeError):
            logger.warn(f"did not receive a valid returnnlp.")
            return ReturnNLP.from_list([])

    def conversation_count(self) -> int:
        return self.user_attributes._get_raw('visit') or 1

    @property
    def is_last_state_open_question(self) -> bool:
        return self.last_state.get("bot_ask_open_question", False)


class TrackerStore(dict):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __getitem__(self, key):

        def crawl(_value):

            if (
                _value and isinstance(_value, list) and len(_value) == 2 and isinstance(_value[0], str) and
                _value[0].startswith('__enum__.')
            ):
                class_type = pydoc.locate(_value[0][len("__enum__."):])
                if not class_type:
                    raise ValueError(f"replace_dataclass cannot find valid class_type: {class_type}")

                item = json.loads(_value[1])
                return class_type[item]

            elif (
                _value and isinstance(_value, list) and len(_value) == 2 and isinstance(_value[0], str) and
                _value[0].startswith('__custom__.')
            ):
                class_type = pydoc.locate(_value[0][len("__custom__."):])
                if not class_type:
                    raise ValueError(f"replace_dataclass cannot find valid class_type: {class_type}")

                return class_type.__ua_decode__(_value[1])

            elif isinstance(_value, list):
                ret = []
                for vi in _value:
                    try:
                        ret.append(crawl(vi))
                    except (AttributeError, NameError, ValueError, TypeError) as e:
                        logger.warning(e, exc_info=True)
                return ret

            elif isinstance(_value, dict):
                ret = {}
                for (k, v) in _value.items():
                    try:
                        ret[k] = crawl(v)
                    except (AttributeError, NameError, ValueError, TypeError) as e:
                        logger.warning(e, exc_info=True)
                return ret

            elif isinstance(_value, (int, float, str, bool)):
                return _value

        value = super().__getitem__(key)
        try:
            return crawl(value)
        except (AttributeError, NameError, ValueError, TypeError) as e:
            logger.warning(e, exc_info=True)

    def __setitem__(self, key, value):

        def crawl(_value):
            """bfs search to sanitize input value, raise NameError if value's leaf has no __encode__"""

            if isinstance(_value, (list, set, tuple)):
                ret = []
                for vi in list(_value):
                    try:
                        ret.append(crawl(vi))
                    except AttributeError as e:
                        logger.warning(e, exc_info=True)
                return ret

            elif isinstance(_value, dict):
                ret = {}
                for (k, v) in _value.items():
                    try:
                        ret[k] = crawl(v)
                    except AttributeError as e:
                        logger.warning(e, exc_info=True)
                return ret

            elif isinstance(_value, (int, float, str, bool)) or _value is None:
                return _value

            elif isinstance(_value, Enum):
                return [f"__enum__.{_fullname(_value)}", json.dumps(_value.name)]

            else:
                return [f"__custom__.{_fullname(_value)}", _value.__ua_encode__()]

        try:
            super().__setitem__(key, crawl(value))
        except AttributeError as e:
            logger.warning(e, exc_info=True)

    def get(self, key, default=None):
        try:
            return self.__getitem__(key)
        except KeyError:
            return default


def _fullname(o):
    # o.__module__ + "." + o.__class__.__qualname__ is an example in
    # this context of H.L. Mencken's "neat, plausible, and wrong."
    # Python makes no guarantees as to whether the __module__ special
    # attribute is defined, so we take a more circumspect approach.
    # Alas, the module name is explicitly excluded from __qualname__
    # in Python 3.

    module = o.__class__.__module__
    if module is None or module == str.__class__.__module__:
        return o.__class__.__name__  # Avoid reporting __builtin__
    else:
        return module + '.' + o.__class__.__name__
