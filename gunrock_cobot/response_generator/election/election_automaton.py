import random
from response_generator.common.evi import EVI_bot
# import pytransitions
from time import time
import template_manager
from response_generator import automaton2 as automaton
from response_generator.automaton2 import *
from response_generator.election.debates import *

logger = logging.getLogger(__name__)
template_election = template_manager.Templates.election
template_social = template_manager.Templates.social
template_shared = template_manager.Templates.shared
automaton.set_logger(logger)

BACKSTORY_CONFIDENCE = .85
CHITCHAT_BACKSTORY_CONFIDENCE = .9

regex_map = {
    'have_you_played': r"(have|do) you (play(|ed)|like)",
    'you_tell_me': r"you tell me",
    'i_dont_like_games': r"I (do(n't| not).*(like|play)|hate) (|playing )(|video |computer )game(|s)"
}


class Election(Keyword):

    # def in_hardcode_knowledge(self) -> bool:
    #     if self.from_knowledge and self._input_name in self.game_map_inv:
    #         return True
    #     elif not self.from_knowledge:
    #         return True
    #     else:
    #         return False
    #
    # @property
    # def name(self):
    #     if self.in_hardcode_knowledge():
    #         return random.choice(list(self.game_map[self.hardcode_name]))
    #     else:
    #         return self._input_name
    #
    # @property
    # def hardcode_name(self):
    #     if self._input_name in self.game_map_inv:
    #         return self.game_map_inv[self._input_name]
    #     if not self.from_knowledge:
    #         return self._input_name
    #     else:
    #         return None
    #
    @classmethod
    def random_game(cls):
        key = random.choice(list(cls.game_map))
        utt = random.choice(list(cls.game_map[key]))
        return cls.fromhardcode(key), utt


class State(automaton.State):
    """
    Defines all state that is possible within ElectionAutomaton.
    """

    @classmethod
    def default(cls):
        return cls.intro

    @classmethod
    def interruption(cls, automaton: 'ElectionAutomaton') -> Optional['State']:
        if re.search(regex_map['i_dont_like_games'], automaton.input_text):
            return State.exit
        else:
            return None

    intro, vote1, vote2, vote3 = auto(), auto(), auto(), auto()
    today, over, evi, backstory, backstoryReason = auto(), auto(), auto(), auto(), auto()
    done, simpleYN, simpleYNReason, exit, simpleOp = auto(), auto(), auto(), auto(), auto()
    more, startchit, chitmore = auto(), auto(), auto()

    introduce, change, why, opinion, chit1, chit2, chit3, chit4, chit5, chit6, chit7 = auto(
    ), auto(), auto(), auto(), auto(), auto(), auto(), auto(), auto(), auto(), auto()
    verify = auto()


class ContextManager(automaton.ContextManager):

    def __init__(self, session_id, user_attributes_ref, current_state_ref, state_class):
        # logger.warning("HIHIHI")
        # logger.warning(current_state_ref)
        super(ContextManager, self).__init__(
            session_id, user_attributes_ref, current_state_ref, State)

    @property
    def modulechat(self):
        if not self.user_attributes.electionchat:
            self.user_attributes.electionchat = {}
        return self.user_attributes.electionchat

    # MARK: - Getter/Setter - user_attributes stores

    @property
    def topics(self) -> List[str]:
        if self.modulechat.get('topics') is None:
            self.modulechat['topics'] = []

        return self.modulechat['topics']

    @property
    def topicsfull(self) -> List[str]:
        if self.modulechat.get('topicsfull') is None:
            self.modulechat['topicsfull'] = []

        return self.modulechat['topicsfull']

    # MARK: - APIs

    def flush(self):
        self.curr_state = State.intro
        self.prev_state = None
        self.prev_state_params.clear()
        self.curr_game = None


class ElectionAutomaton(Automaton):

    @classmethod
    def from_rg(cls,
                response_generator_ref,
                context_manager_class: Type[ContextManager] = ContextManager,
                state_class: Type[State] = State):
        return super(ElectionAutomaton, cls).from_rg(response_generator_ref, context_manager_class, state_class)

    def __init__(self, input_data, context_manager: ContextManager):
        super(ElectionAutomaton, self).__init__(input_data, context_manager)
        self.cm = context_manager

    # type alias
    decorator = Automaton._Decorators

    # topics = ['Donald Trump', 'universal healthcare', 'the senate race in texas', 'supreme court', 'economy', 'tariffs', 'womens rights', 'immigration', 'impeachement']

    # topic_map = {'Donald Trump': 'trump', 'universal healthcare': 'healthcare', 'the senate race in texas': 'beto', 'the supreme court': 'supremecourt',
    #              'the economy': 'economy', 'the tariffs on China': 'tariffs', "women's rights": 'womensrights', 'immigration and DACA': 'immigration', 'possible impeachement of the president': 'impeachement', }

    topic_map = {'universal healthcare': 'healthcare', 'the senate race in texas': 'beto', 'the supreme court': 'supremecourt',
                 'the economy': 'economy', 'the tariffs on China': 'tariffs', "women's rights": 'womensrights',
                 'immigration and DACA': 'immigration', 'what people are saying about impeachment': 'impeachment',
                 'trump': 'trump', 'the refugee caravan': 'caravan', 'fake news': 'media', 'the mail bombings': 'bombing',
                 'the election': 'election', 'the house of representatives': 'house'}

    chitchat_patterns = {
        # "change": r"change topics|another topic",
        "healthcare": r"healthcare|(universal|public|free) healthcare|single-payer",
        "beto": r"ted cruz|beto|o('|\s)?rourke|texas senate|texas election|senate",
        "supremecourt": r"supreme court|senate judicial|(brett )?(k|c)avanaugh|christine (blasey )?ford",
        "economy": r"homelessness|jobs|stock|economy|nafta",
        "tariffs": r"tariff|trade|trade war",
        "womensrights": r"abortion|roe .* wade|women|women vote",
        "immigration": r"immigration|emigration|daca|DACA|border|wall|mexico|illegals|immigrants",
        "impeachment": r"impeach|impeachment|removal from office",
        "trump": r"stormy|trump|the president|POTUS",  # TODO: more trump
        "media": r"fake news|media|bias|left|liberal(s)?|news|propaganda|enemy of the people|new york times|cnn|fox|jamal",
        "caravan": r"migrants|refugee(s)?|guatemala|south america|mexico|caravan|migration",
        "bombing": r"mail|bomb|soros|usps",
        "election": r"won|democrats|republicans|mcconnell|results|election|ballot|governor|republican|democrat|midterms",
        "house": r"house|senate"
    }

    class CompiledPattern:
        def __init__(self, pat):
            self.chitchat_patterns = {}
            # pre-compile system level patterns
            for key, val in pat.items():
                self.chitchat_patterns[key] = re.compile(val)

        def get_chitchat_regex(self, key):
            return self.chitchat_patterns[key]

    CompiledPtn = CompiledPattern(chitchat_patterns)

    @property
    def chitchat_regex(self):
        hits = []
        if self.cm.topics:
            last = self.cm.topics[-1]
        for intent in self.chitchat_patterns.keys():
            # prevent talking about something we already have
            if self.CompiledPtn.get_chitchat_regex(intent).search(self.input_text) and intent not in self.cm.topics:
                hits.append(intent)
        # add to make sure it is not the same s last
        self.cm.topics.extend(hits)
        return True if hits else False

    @property  # add debate simple
    def yesno(self):
        if self.backstory['confidence'] >= BACKSTORY_CONFIDENCE:
            self.cm.backstory = self.backstory
            return self.backstory['text']

        debate = get_opinion(self.input_text)
        if not debate:
            utt = template_election.utterance(selector='debate/unsure', slots={},
                                              user_attributes_ref=self.cm.user_attributes)
            return utt, State.done, ProposeContinue.CONTINUE, ProposeTopic.NONE
        db = debate_lookup(debate['text'])

        # self.cm.debates.append((db['url'], str(debate['confidence'])))
        if debate['confidence'] >= .9:  # answer question directly
            if db['majority'] != 'tie':
                n = db[db['majority']]
                if n >= 60:
                    op = db[db['majority'] + '_ops']
                    if op:
                        # utt = ' {0}. {1} .'.format(db['majority'], op[0][0])
                        utt = template_election.utterance(selector='debate/opinion', slots={'ans': db['majority'],
                                                                                            'opinion': op[0][0]},
                                                          user_attributes_ref=self.cm.user_attributes)
                    else:
                        utt = template_election.utterance(selector='debate/majority', slots={'maj': db['majority'],
                                                                                             'p1': str(n)},
                                                          user_attributes_ref=self.cm.user_attributes)
                        # utt = ' The majority of people have told me {0}. If your curious, {1} percent of people I have spoken with. What side do you fall on?'.format(
                        #     db['majority'], str(n))
                else:
                    utt = template_election.utterance(selector='debate/contro', slots={'p1': db['yes'],
                                                                                       'p2': db['no']},
                                                      user_attributes_ref=self.cm.user_attributes)
                    # utt = " This is a very controversial topic and I have not made up my mind. {0} percent say yes and {1} percent say no. Tell me what you think so I can make up my mind! ".format(
                    #     db['yes'], db['no'])
            else:
                utt = template_election.utterance(selector='debate/fifty', slots={},
                                                  user_attributes_ref=self.cm.user_attributes)
                # utt = " I'm fifty fifty on this one. All people I've asked have either said yes or no. Your opinion will make the difference! "
            return utt, State.done, ProposeContinue.CONTINUE, ProposeTopic.NONE

        return None, None, None, None

    # State - intro

    @decorator.transition
    def t_intro(self):
        if 'ask_leave' in self.lexicals or (self.dialog_act and 'closing' in self.dialog_act):
            return State.done
        elif self.backstory['confidence'] > BACKSTORY_CONFIDENCE:
            return State.backstory
        elif self.chitchat_regex:
            return State.chit1
        elif 'topic_qa' in self.topic:
            return State.evi
        elif 'ask_yesno' in self.lexicals:
            return State.simpleYN
        elif "ans_factopinion" in self.lexicals or 'ask_advice' in self.lexicals or 'ask_opinion' in self.lexicals or 'ask_reason' in self.lexicals or 'ask_person' in self.lexicals:
            return State.simpleOp
        elif 'req_more' in self.sys:
            return State.more
        elif self.dialog_act:
            if 'yes_no_question' in self.dialog_act:
                return State.simpleYN
            elif 'open_question_factual' in self.dialog_act:
                return State.evi
        return

    @decorator.state
    def s_intro(self, params: dict):
        # date logicvote
        utt = ''
        epoch_time = int(time())
        # epoch_time = 1541584800

        if epoch_time >= 1541404800 and epoch_time < 1541491200:  # Nov 5
            utt = template_election.utterance(
                selector='today/tmrw',
                slots={},
                user_attributes_ref=self.cm.user_attributes
            )
        elif epoch_time >= 1541491200 and epoch_time < 1541548800:  # Nov 6
            utt = template_election.utterance(
                selector='today/ge',
                slots={},
                user_attributes_ref=self.cm.user_attributes
            )
        elif epoch_time < 1541491200:  # before nov 6
            utt = template_election.utterance(
                selector='today/up',
                slots={},
                user_attributes_ref=self.cm.user_attributes
            )
        elif epoch_time >= 1541548800:  # Nov 7 and ond
            utt = template_election.utterance(
                selector='today/over',
                slots={},
                user_attributes_ref=self.cm.user_attributes
            )
            return utt, State.exit, ProposeContinue.STOP, ProposeTopic.NONE

            # return utt, State.exit, ProposeContinue.CONTINUE, ProposeTopic.NONE # limiting states for finals

        utt += template_election.utterance(
            selector='vote/this',
            slots={},
            user_attributes_ref=self.cm.user_attributes
        )
        return utt, State.startchit, ProposeContinue.CONTINUE, ProposeTopic.NONE
        # return utt, State.done, ProposeContinue.CONTINUE, ProposeTopic.NONE  # limiting states for finals

    @decorator.transition
    def t_vote1(self):
        if 'ask_leave' in self.lexicals or 'req_topic_jump' in self.sys or (self.dialog_act and 'closing' in self.dialog_act):
            return State.done
        elif self.backstory['confidence'] > BACKSTORY_CONFIDENCE:
            return State.backstory
        elif self.chitchat_regex:
            return State.chit1
        elif 'topic_qa' in self.topic:
            return State.evi
        elif 'ask_yesno' in self.lexicals:
            return State.simpleYN
        elif "ans_factopinion" in self.lexicals or 'ask_advice' in self.lexicals or 'ask_opinion' in self.lexicals or 'ask_reason' in self.lexicals or 'ask_person' in self.lexicals:
            return State.simpleOp
        elif 'req_more' in self.sys:
            return State.more
        elif self.dialog_act:
            if 'yes_no_question' in self.dialog_act:
                return State.simpleYN
            elif 'open_question_factual' in self.dialog_act:
                return State.evi
        return

    @decorator.state
    def s_vote1(self, params: dict):
        utt = ''
        if 'ans_pos' in self.lexicals:
            utt += template_election.utterance(
                selector='vote/good',
                slots={},
                user_attributes_ref=self.cm.user_attributes
            )
        elif 'ans_neg' in self.lexicals:
            utt += template_election.utterance(
                selector='vote/bad',
                slots={},
                user_attributes_ref=self.cm.user_attributes
            )

        utt += template_election.utterance(
            selector='exit/chitchat',
            slots={},
            user_attributes_ref=self.cm.user_attributes
        )
        return utt, State.startchit, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @decorator.transition
    def t_backstory(self):
        if 'ask_leave' in self.lexicals or 'req_topic_jump' in self.sys or (self.dialog_act and 'closing' in self.dialog_act):
            return State.done
        elif self.backstory['confidence'] > BACKSTORY_CONFIDENCE:
            return State.backstory
        elif self.chitchat_regex:
            return State.chit1
        elif 'topic_qa' in self.topic:
            return State.evi
        elif 'ask_yesno' in self.lexicals:
            return State.simpleYN
        elif "ans_factopinion" in self.lexicals or 'ask_advice' in self.lexicals or 'ask_opinion' in self.lexicals or 'ask_reason' in self.lexicals or 'ask_person' in self.lexicals:
            return State.simpleOp
        elif 'req_more' in self.sys:
            return State.more
        elif self.dialog_act:
            if 'yes_no_question' in self.dialog_act:
                return State.simpleYN
            elif 'open_question_factual' in self.dialog_act:
                return State.evi

    @decorator.state
    def s_backstory(self, params: dict):
        # if self.cm.prev_state == State.exit:
        #     return self.backstory['text'], State.exit, ProposeContinue.STOP, ProposeTopic.NONE
        return self.backstory['text'], State.backstoryReason, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @decorator.transition
    def t_backstoryReason(self):
        if 'ask_leave' in self.lexicals or 'req_topic_jump' in self.sys or (self.dialog_act and 'closing' in self.dialog_act):
            return State.done
        elif self.backstory['confidence'] > BACKSTORY_CONFIDENCE:
            return State.backstory
        elif self.chitchat_regex:
            return State.chit1
        elif 'topic_qa' in self.topic:
            return State.evi
        elif 'ask_yesno' in self.lexicals:
            return State.simpleYN
        elif "ans_factopinion" in self.lexicals or 'ask_advice' in self.lexicals or 'ask_opinion' in self.lexicals or 'ask_reason' in self.lexicals or 'ask_person' in self.lexicals:
            return State.simpleOp
        elif 'req_more' in self.sys:
            return State.more
        elif self.dialog_act:
            if 'yes_no_question' in self.dialog_act:
                return State.simpleYN
            elif 'open_question_factual' in self.dialog_act:
                return State.evi

        return State.startchit

    @decorator.state
    def s_backstoryReason(self, params: dict):
        utt = self.cm.backstory['reason']
        if not utt:
            utt = template_election.utterance(selector='backstory/noreason', slots={},
                                              user_attributes_ref=self.cm.user_attributes)
        return utt, State.startchit, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @decorator.transition
    def t_simpleYN(self):
        if 'ask_leave' in self.lexicals or 'req_topic_jump' in self.sys or (self.dialog_act and 'closing' in self.dialog_act):
            return State.done
        elif self.backstory['confidence'] > BACKSTORY_CONFIDENCE:
            return State.backstory
        elif self.chitchat_regex:
            return State.chit1
        elif 'topic_qa' in self.topic:
            return State.evi
        elif 'ask_yesno' in self.lexicals:
            return State.simpleYN
        elif "ans_factopinion" in self.lexicals or 'ask_advice' in self.lexicals or 'ask_opinion' in self.lexicals or 'ask_reason' in self.lexicals or 'ask_person' in self.lexicals:
            return State.simpleOp
        elif 'req_more' in self.sys:
            return State.more
        elif self.dialog_act:
            if 'yes_no_question' in self.dialog_act:
                return State.simpleYN
            elif 'open_question_factual' in self.dialog_act:
                return State.evi
        return State.done

    @decorator.state
    def s_simpleYN(self, params: dict):
        utt, state, cont, topic = self.yesno
        if utt:
            return utt, state, cont, topic
        utt = template_shared.utterance(selector='yes_no_question/general', slots={},
                                        user_attributes_ref=self.cm.user_attributes)
        return utt, State.simpleYNReason, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @decorator.transition
    def t_simpleYNReason(self):
        if 'ask_leave' in self.lexicals or 'req_topic_jump' in self.sys or (self.dialog_act and 'closing' in self.dialog_act):
            return State.done
        elif self.backstory['confidence'] > BACKSTORY_CONFIDENCE:
            return State.backstory
        elif self.chitchat_regex:
            return State.chit1
        elif 'topic_qa' in self.topic:
            return State.evi
        elif 'ask_yesno' in self.lexicals:
            return State.simpleYN
        elif "ans_factopinion" in self.lexicals or 'ask_advice' in self.lexicals or 'ask_opinion' in self.lexicals or 'ask_reason' in self.lexicals or 'ask_person' in self.lexicals:
            return State.simpleOp
        elif 'req_more' in self.sys:
            return State.more
        elif self.dialog_act:
            if 'yes_no_question' in self.dialog_act:
                return State.simpleYN
            elif 'open_question_factual' in self.dialog_act:
                return State.evi
        return State.done

    @decorator.state
    def s_simpleYNReason(self, params: dict):
        utt = template_shared.utterance(selector='yes_no_question/follow_up', slots={},
                                        user_attributes_ref=self.cm.user_attributes)
        return utt, State.startchit, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @decorator.transition
    def t_evi(self):
        if 'ask_leave' in self.lexicals or 'req_topic_jump' in self.sys or (self.dialog_act and 'closing' in self.dialog_act):
            return State.done
        elif self.backstory['confidence'] > BACKSTORY_CONFIDENCE:
            return State.backstory
        elif self.chitchat_regex:
            return State.chit1
        elif 'topic_qa' in self.topic:
            return State.evi
        elif 'ask_yesno' in self.lexicals:
            return State.simpleYN
        elif "ans_factopinion" in self.lexicals or 'ask_advice' in self.lexicals or 'ask_opinion' in self.lexicals or 'ask_reason' in self.lexicals or 'ask_person' in self.lexicals:
            return State.simpleOp
        elif 'req_more' in self.sys:
            return State.more
        elif self.dialog_act:
            if 'yes_no_question' in self.dialog_act:
                return State.simpleYN
            elif 'open_question_factual' in self.dialog_act:
                return State.evi
        return

    @decorator.state
    def s_evi(self, params: dict):
        try:
            evi_response = EVI_bot(self.input_text)
            if evi_response:
                utt = template_election.utterance(selector='knowledge/evi',
                                                  slots={'evi': evi_response},
                                                  user_attributes_ref=self.cm.user_attributes)
                return utt, State.startchit, ProposeContinue.CONTINUE, ProposeTopic.NONE
        except IndexError:
            pass
        utt = "I am not sure that I know the answer to your question."
        return utt, State.startchit, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @decorator.transition
    def t_simpleOp(self):
        if 'ask_leave' in self.lexicals or 'req_topic_jump' in self.sys or (self.dialog_act and 'closing' in self.dialog_act):
            return State.done
        elif self.backstory['confidence'] > BACKSTORY_CONFIDENCE:
            return State.backstory
        elif self.chitchat_regex:
            return State.chit1
        elif 'topic_qa' in self.topic:
            return State.evi
        elif 'ask_yesno' in self.lexicals:
            return State.simpleYN
        elif "ans_factopinion" in self.lexicals or 'ask_advice' in self.lexicals or 'ask_opinion' in self.lexicals or 'ask_reason' in self.lexicals or 'ask_person' in self.lexicals:
            return State.simpleOp
        elif 'req_more' in self.sys:
            return State.more
        elif self.dialog_act:
            if 'yes_no_question' in self.dialog_act:
                return State.simpleYN
            elif 'open_question_factual' in self.dialog_act:
                return State.evi
        return

    @decorator.state
    def s_simpleOp(self, params: dict):
        utt = ''
        if 'ask_reason' in self.lexicals:
            utt = template_election.utterance(selector='person/reason', slots={},
                                              user_attributes_ref=self.cm.user_attributes)
        elif 'ask_opinion' in self.lexicals or 'ans_factopinion' in self.lexicals:
            utt = template_election.utterance(selector='person/opinion', slots={},
                                              user_attributes_ref=self.cm.user_attributes)
        elif 'ask_person' in self.lexicals:
            utt = template_election.utterance(selector='person/askperson', slots={},
                                              user_attributes_ref=self.cm.user_attributes)
        elif 'ask_advice' in self.lexicals:
            utt = template_election.utterance(selector='person/advice', slots={},
                                              user_attributes_ref=self.cm.user_attributes)

        return utt, State.startchit, ProposeContinue.CONTINUE, ProposeTopic.NONE

    # @decorator.transition
    # def t_advice(self):
    #     if 'ask_leave' in self.lexicals:
    #         return State.done
    #     return
    #
    # @decorator.state
    # def s_advice(self, params: dict):
    #     utt = template_news.utterance(selector='giveOpinion/advice', slots={},
    #                                   user_attributes_ref=self.cm.user_attributes)
    #     utt += template_news.utterance(selector='person/othernews', slots={},
    #                                    user_attributes_ref=self.cm.user_attributes)
    #     return utt, States.intro, ProposeContinue.CONTINUE, ProposeTopic.NONE
    #

    # State - exit

    @decorator.transition
    def t_done(self):
        if 'ask_leave' in self.lexicals or 'req_topic_jump' in self.sys or (self.dialog_act and 'closing' in self.dialog_act):
            return State.done
        elif self.backstory['confidence'] > BACKSTORY_CONFIDENCE:
            return State.backstory
        elif self.chitchat_regex:
            return State.chit1
        elif 'topic_qa' in self.topic:
            return State.evi
        elif 'ask_yesno' in self.lexicals:
            return State.simpleYN
        elif "ans_factopinion" in self.lexicals or 'ask_advice' in self.lexicals or 'ask_opinion' in self.lexicals or 'ask_reason' in self.lexicals or 'ask_person' in self.lexicals:
            return State.simpleOp
        elif 'req_more' in self.sys:
            return State.more
        elif self.dialog_act:
            if 'yes_no_question' in self.dialog_act:
                return State.simpleYN
            elif 'open_question_factual' in self.dialog_act:
                return State.evi
            # elif 'ans_positive' in self.dialog_act:
            #     return State.startchit
            elif 'ans_negative' in self.dialog_act:
                return State.exit
        # elif "ans_pos" in self.lexicals:
        #     return State.startchit
        elif "ans_neg" in self.lexicals:
            return State.exit
        return

    @decorator.state
    def s_done(self, params: dict):

        # utt = template_election.utterance(selector='vote/votenow',
        #                                   slots={},
        #                                   user_attributes_ref=self.cm.user_attributes)
        utt = template_election.utterance(
            selector='vote/vote',
            slots={},
            user_attributes_ref=self.cm.user_attributes
        )

        # finals

        return utt, State.exit, ProposeContinue.STOP, ProposeTopic.NONE
        ########################################################

        # return utt, State.exit, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @decorator.transition
    def t_exit(self):
        # return
        if 'ask_leave' in self.lexicals or 'req_topic_jump' in self.sys or (self.dialog_act and 'closing' in self.dialog_act):
            return State.exit
        elif self.backstory['confidence'] > BACKSTORY_CONFIDENCE:
            return State.backstory
        elif self.chitchat_regex:
            return State.chit1
        elif 'topic_qa' in self.topic:
            return State.evi
        elif 'ask_yesno' in self.lexicals:
            return State.simpleYN
        elif "ans_factopinion" in self.lexicals or 'ask_advice' in self.lexicals or 'ask_opinion' in self.lexicals or 'ask_reason' in self.lexicals or 'ask_person' in self.lexicals:
            return State.simpleOp
        elif 'req_more' in self.sys:
            return State.more
        elif self.dialog_act:
            if 'yes_no_question' in self.dialog_act:
                return State.simpleYN
            elif 'open_question_factual' in self.dialog_act:
                return State.evi
        return

    @decorator.state
    def s_exit(self, params: dict):

        utt = template_election.utterance(selector='exit/motive',
                                          slots={},
                                          user_attributes_ref=self.cm.user_attributes)

        self.cm.flush()
        return utt, State.intro, ProposeContinue.STOP, ProposeTopic.NONE

    @decorator.transition
    def t_more(self):
        if 'ask_leave' in self.lexicals or 'req_topic_jump' in self.sys or (self.dialog_act and 'closing' in self.dialog_act):
            return State.done
        elif self.backstory['confidence'] > BACKSTORY_CONFIDENCE:
            return State.backstory
        elif self.chitchat_regex:
            return State.chit1
        elif 'topic_qa' in self.topic:
            return State.evi
        elif 'ask_yesno' in self.lexicals:
            return State.simpleYN
        elif "ans_factopinion" in self.lexicals or 'ask_advice' in self.lexicals or 'ask_opinion' in self.lexicals or 'ask_reason' in self.lexicals or 'ask_person' in self.lexicals:
            return State.simpleOp
        elif 'req_more' in self.sys:
            return State.more
        elif self.dialog_act:
            if 'yes_no_question' in self.dialog_act:
                return State.simpleYN
            elif 'open_question_factual' in self.dialog_act:
                return State.evi
        return

    @decorator.state
    def s_more(self, params: dict):

        utt = template_election.utterance(selector='knowledge/more',
                                          slots={},
                                          user_attributes_ref=self.cm.user_attributes)
        return utt, State.startchit, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @decorator.transition
    def t_startchit(self):
        if 'ask_leave' in self.lexicals or 'req_topic_jump' in self.sys or (self.dialog_act and 'closing' in self.dialog_act):
            return State.done
        elif self.backstory['confidence'] > BACKSTORY_CONFIDENCE:
            return State.backstory
        elif self.chitchat_regex:
            return State.chit1
        elif 'topic_qa' in self.topic:
            return State.evi
        # elif 'ask_yesno' in self.lexicals and self.cm.prev_state ==State.intro:
        #     return State.simpleYN
        elif 'ask_yesno' in self.lexicals:
            return State.simpleYN
        elif "ans_factopinion" in self.lexicals or 'ask_opinion' in self.lexicals:
            return State.simpleOp
        elif 'ask_reason' in self.lexicals:
            return State.why
        elif 'ask_advice' in self.lexicals or 'ask_person' in self.lexicals:
            return State.simpleOp
        elif 'req_more' in self.sys:
            return State.more
        elif self.cm.prev_state == State.verify and "ans_neg" in self.lexicals:
            return State.done
        elif (self.cm.prev_state != State.intro) and len(self.cm.topics) and not(len(self.cm.topics) % 2) and self.cm.prev_state != State.verify:
            return State.verify
        elif self.dialog_act:
            if 'yes_no_question' in self.dialog_act:
                return State.simpleYN
            elif 'open_question_factual' in self.dialog_act:
                return State.evi
            # elif 'ans_positive' in self.dialog_act and self.cm.prev_state:
            #     return State.startchit
            elif self.cm.prev_state == State.verify and 'ans_negative' in self.dialog_act:
                return State.done
        # elif "ans_pos" in self.lexicals:
        #     return State.startchit
        # elif "ans_neg" in self.lexicals:
        #     return State.exit
        return

    @decorator.state
    def s_startchit(self, params: dict):
        # if 'trump' not in self.cm.topics:
        #     top = 'trump'
        #     trump = 'Donald Trump'
        #     self.cm.topics.append(top)
        #     utt = template_election.utterance(selector='chitchat/introduce',
        #                                       slots={'topic': trump},
        #                                       user_attributes_ref=self.cm.user_attributes)
        #     # go to trump chit chat1
        # else:
        # if (self.cm.prev_state != State.intro) and len(self.cm.topics) and not(len(self.cm.topics) % 2) and self.cm.prev_state != State.verify:
        #     utt = template_election.utterance(selector='chitchat/verify',
        #                                       slots={},
        #                                       user_attributes_ref=self.cm.user_attributes)
        #     return utt, State.startchit, ProposeContinue.CONTINUE, ProposeTopic.NONE

        selection = list(set(self.topic_map.keys()) - set(self.cm.topicsfull))
        if not selection:
            return "Oops I think we covered everything I know of.", State.done, ProposeContinue.CONTINUE, ProposeTopic.NONE

        utt = ''
        if self.cm.prev_state == State.intro:

            if 'ans_pos' in self.lexicals or 'voted' in self.input_text:
                utt += template_election.utterance(
                    selector='vote/good',
                    slots={},
                    user_attributes_ref=self.cm.user_attributes
                )
            elif 'ans_neg' in self.lexicals:
                utt += template_election.utterance(
                    selector='vote/bad',
                    slots={},
                    user_attributes_ref=self.cm.user_attributes
                )

        #########################################################
            # TEMP for finals

            # utt += template_election.utterance(
            #     selector='exit/chitchat',
            #     slots={},
            #     user_attributes_ref=self.cm.user_attributes
            # )

        utt += template_election.utterance(
            selector='vote/vote',
            slots={},
            user_attributes_ref=self.cm.user_attributes
        )
        return utt, State.exit, ProposeContinue.STOP, ProposeTopic.NONE

        # for final just reduce coverage and dont chit chat
        ##########################################################

        # elif self.cm.prev_state == State.chit4 or self.cm.prev_state == State.chit3:
        #     utt = template_election.utterance(selector='chitchat/chitmore',
        #                                       slots={},
        #                                       user_attributes_ref=self.cm.user_attributes)
        # elif self.cm.prev_state == State.startchit:
        #     pass
        #     # utt = template_election.utterance(selector='chitchat/nochit',
        #     #                                   slots={},
        #     #                                   user_attributes_ref=self.cm.user_attributes)
        # elif self.cm.prev_state == State.opinion or self.cm.prev_state == State.why or self.cm.prev_state == State.evi or self.cm.prev_state == State.backstory:
        #     utt = template_election.utterance(selector='chitchat/resume',
        #                                       slots={},
        #                                       user_attributes_ref=self.cm.user_attributes)
        #
        # t = random.choice(selection)
        #
        # self.cm.topicsfull.append(t)
        # self.cm.topics.append(self.topic_map.get(t))
        #
        # utt += template_election.utterance(selector='chitchat/introduce',
        #                                    slots={'topic': t},
        #                                    user_attributes_ref=self.cm.user_attributes)
        # return utt, State.chit1, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @decorator.transition
    def t_verify(self):
        if 'ask_leave' in self.lexicals or 'req_topic_jump' in self.sys or (self.dialog_act and 'closing' in self.dialog_act):
            return State.done
        elif self.backstory['confidence'] > BACKSTORY_CONFIDENCE:
            return State.backstory
        elif self.chitchat_regex:
            return State.chit1
        elif 'topic_qa' in self.topic:
            return State.evi
        # elif 'ask_yesno' in self.lexicals and self.cm.prev_state ==State.intro:
        #     return State.simpleYN
        elif 'ask_yesno' in self.lexicals:
            return State.simpleYN
        elif "ans_factopinion" in self.lexicals or 'ask_opinion' in self.lexicals:
            return State.simpleOp
        elif 'ask_reason' in self.lexicals:
            return State.why
        elif 'ask_advice' in self.lexicals or 'ask_person' in self.lexicals:
            return State.simpleOp
        elif 'req_more' in self.sys:
            return State.more
        elif self.dialog_act:
            if 'yes_no_question' in self.dialog_act:
                return State.simpleYN
            elif 'open_question_factual' in self.dialog_act:
                return State.evi
            # elif 'ans_positive' in self.dialog_act and self.cm.prev_state:
            #     return State.startchit
            # elif 'ans_negative' in self.dialog_act:
            #     return State.exit
        # elif "ans_pos" in self.lexicals:
        #     return State.startchit
        # elif "ans_neg" in self.lexicals:
        #     return State.exit
        return

    @decorator.state
    def s_verify(self, params: dict):
        utt = template_election.utterance(selector='chitchat/verify',
                                          slots={},
                                          user_attributes_ref=self.cm.user_attributes)
        return utt, State.startchit, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @decorator.transition
    def t_introduce(self):
        if 'ask_leave' in self.lexicals or 'req_topic_jump' in self.sys or (self.dialog_act and 'closing' in self.dialog_act):
            return State.done
        elif self.backstory['confidence'] > BACKSTORY_CONFIDENCE:
            return State.backstory
        elif self.chitchat_regex:
            return State.chit1
        elif 'topic_qa' in self.topic:
            return State.evi
        elif 'ask_yesno' in self.lexicals:
            return State.simpleYN
        elif "ans_factopinion" in self.lexicals or 'ask_advice' in self.lexicals or 'ask_opinion' in self.lexicals or 'ask_reason' in self.lexicals or 'ask_person' in self.lexicals:
            return State.simpleOp
        elif 'req_more' in self.sys:
            return State.more
        elif self.dialog_act:
            if 'yes_no_question' in self.dialog_act:
                return State.simpleYN
            elif 'open_question_factual' in self.dialog_act:
                return State.evi
        return

    @decorator.state
    def s_introduce(self, params: dict):
        # INSERT DOMAIN CHIT CHAT TOPIC IDENTIFICATION

        utt = template_election.utterance(selector='knowledge/more',
                                          slots={},
                                          user_attributes_ref=self.cm.user_attributes)
        return utt, State.exit, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @decorator.transition
    def t_change(self):
        if 'ask_leave' in self.lexicals or 'req_topic_jump' in self.sys or (self.dialog_act and 'closing' in self.dialog_act):
            return State.done
        elif self.backstory['confidence'] > BACKSTORY_CONFIDENCE:
            return State.backstory
        elif self.chitchat_regex:
            return State.chit1
        elif 'topic_qa' in self.topic:
            return State.evi
        elif 'ask_yesno' in self.lexicals:
            return State.simpleYN
        elif 'ask_opinion' in self.lexicals:
            return State.opinion
        elif 'ask_reason' in self.lexicals:
            return State.why
        elif 'ask_advice' in self.lexicals or 'ask_person' in self.lexicals:
            return State.simpleOp
        elif 'req_more' in self.sys:
            return State.more
        elif self.dialog_act:
            if 'yes_no_question' in self.dialog_act:
                return State.simpleYN
            elif 'open_question_factual' in self.dialog_act:
                return State.evi
        return

    @decorator.state
    def s_change(self, params: dict):
        top = self.cm.topics[-1]
        utt = template_election.utterance(selector='chitchat/resume',
                                          slots={},
                                          user_attributes_ref=self.cm.user_attributes)
        return utt, State.exit, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @decorator.transition
    def t_why(self):
        if 'ask_leave' in self.lexicals or 'req_topic_jump' in self.sys or (self.dialog_act and 'closing' in self.dialog_act):
            return State.done
        elif self.backstory['confidence'] > CHITCHAT_BACKSTORY_CONFIDENCE:
            return State.backstory
        elif self.chitchat_regex:
            return State.chit1
        elif 'topic_qa' in self.topic:
            return State.evi
        elif 'ask_yesno' in self.lexicals:
            return State.simpleYN
        # elif self.backstory['confidence'] > BACKSTORY_CONFIDENCE:
        #     return State.backstory
        elif 'ask_opinion' in self.lexicals:
            return State.opinion
        elif 'ask_reason' in self.lexicals:
            return State.why
        elif 'ask_advice' in self.lexicals or 'ask_person' in self.lexicals:
            return State.simpleOp
        elif 'req_more' in self.sys:
            return State.more
        elif self.dialog_act:
            if 'yes_no_question' in self.dialog_act:
                return State.simpleYN
            elif 'open_question_factual' in self.dialog_act:
                return State.evi
        return

    @decorator.state
    def s_why(self, params: dict):
        top = self.cm.topics[-1]
        utt = template_election.utterance(selector='chitchat/' + top + '/why',
                                          slots={},
                                          user_attributes_ref=self.cm.user_attributes)
        # utt += template_election.utterance(selector='chitchat/prevq',
        #                                    slots={},
        #                                    user_attributes_ref=self.cm.user_attributes)
        # return utt, self.cm.prev_state, ProposeContinue.CONTINUE, ProposeTopic.NONE
        return utt, State.startchit, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @decorator.transition
    def t_opinion(self):
        if 'ask_leave' in self.lexicals or 'req_topic_jump' in self.sys or (self.dialog_act and 'closing' in self.dialog_act):
            return State.done
        elif self.backstory['confidence'] > CHITCHAT_BACKSTORY_CONFIDENCE:
            return State.backstory
        elif self.chitchat_regex:
            return State.chit1
        elif 'topic_qa' in self.topic:
            return State.evi
        elif 'ask_yesno' in self.lexicals:
            return State.simpleYN
        # elif "ans_factopinion" in self.lexicals or 'ask_advice' in self.lexicals or 'ask_opinion' in self.lexicals or 'ask_reason' in self.lexicals or 'ask_person' in self.lexicals:
        elif 'ask_opinion' in self.lexicals:
            return State.opinion
        elif 'ask_reason' in self.lexicals:
            return State.why
        elif 'ask_advice' in self.lexicals or 'ask_person' in self.lexicals:
            return State.simpleOp
        elif 'req_more' in self.sys:
            return State.more
        elif self.dialog_act:
            if 'yes_no_question' in self.dialog_act:
                return State.simpleYN
            elif 'open_question_factual' in self.dialog_act:
                return State.evi
        return

    @decorator.state
    def s_opinion(self, params: dict):

        top = self.cm.topics[-1]
        utt = template_election.utterance(selector='chitchat/' + top + '/opinion',
                                          slots={},
                                          user_attributes_ref=self.cm.user_attributes)
        # utt += template_election.utterance(selector='chitchat/prevq',
        #                                    slots={},
        #                                    user_attributes_ref=self.cm.user_attributes)
        # return utt, self.cm.prev_state, ProposeContinue.CONTINUE, ProposeTopic.NONE

        return utt, State.startchit, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @decorator.transition
    def t_chit1(self):
        if 'ask_leave' in self.lexicals or 'req_topic_jump' in self.sys or (self.dialog_act and 'closing' in self.dialog_act):
            return State.done
        elif self.backstory['confidence'] > CHITCHAT_BACKSTORY_CONFIDENCE:
            return State.backstory
        elif self.chitchat_regex:
            return State.chit1
        elif 'topic_qa' in self.topic:
            return State.evi
        elif self.cm.prev_state == State.why or self.cm.prev_state == State.opinion:
            return State.chit2
        elif 'ask_yesno' in self.lexicals:
            return State.opinion
        elif 'ask_opinion' in self.lexicals:
            return State.opinion
        elif self.backstory['confidence'] > BACKSTORY_CONFIDENCE:
            return State.backstory
        elif 'ask_reason' in self.lexicals:
            return State.why
        elif 'ask_advice' in self.lexicals or 'ask_person' in self.lexicals:
            return State.simpleOp
        elif 'req_more' in self.sys:
            return State.more
        elif 'ans_neg' in self.lexicals:
            return State.startchit  # go back to chit chat
        elif self.dialog_act:
            if 'yes_no_question' in self.dialog_act:
                return State.simpleYN
            elif 'open_question_factual' in self.dialog_act:
                return State.evi

        return

    @decorator.state
    def s_chit1(self, params: dict):
        top = self.cm.topics[-1]
        ack = self.central_elem_text
        utt = ''
        # t = random.randint(1, 4)
        # if ack and t < 3:
        #     utt = template_election.utterance(selector='chitchat/ack',
        #                                       slots={'ack': ack},
        #                                       user_attributes_ref=self.cm.user_attributes)
        utt += template_election.utterance(selector='chitchat/' + top + '/one',
                                           slots={},
                                           user_attributes_ref=self.cm.user_attributes)
        return utt, State.chit2, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @decorator.transition
    def t_chit2(self):
        if 'ask_leave' in self.lexicals or 'req_topic_jump' in self.sys or (self.dialog_act and 'closing' in self.dialog_act):
            return State.done
        elif self.backstory['confidence'] > CHITCHAT_BACKSTORY_CONFIDENCE:
            return State.backstory
        elif self.chitchat_regex:
            return State.chit1
        elif 'topic_qa' in self.topic:
            return State.evi
        elif self.cm.prev_state == State.why or self.cm.prev_state == State.opinion:
            return State.chit3
        elif 'ask_yesno' in self.lexicals:
            return State.opinion
        elif 'ask_opinion' in self.lexicals:
            return State.opinion
        elif self.backstory['confidence'] > BACKSTORY_CONFIDENCE:
            return State.backstory
        elif 'ask_reason' in self.lexicals:
            return State.why
        elif 'ask_advice' in self.lexicals or 'ask_person' in self.lexicals:
            return State.simpleOp
        elif 'req_more' in self.sys:
            return State.more
        elif self.dialog_act:
            if 'yes_no_question' in self.dialog_act:
                return State.simpleYN
            elif 'open_question_factual' in self.dialog_act:
                return State.evi
        return

    @decorator.state
    def s_chit2(self, params: dict):

        top = self.cm.topics[-1]
        ack = self.central_elem_text
        utt = ''
        # t = random.randint(1, 4)
        # if ack and t < 3:
        #     utt = template_election.utterance(selector='chitchat/ack',
        #                                       slots={'ack': ack},
        #                                       user_attributes_ref=self.cm.user_attributes)
        utt += template_election.utterance(selector='chitchat/' + top + '/two',
                                           slots={},
                                           user_attributes_ref=self.cm.user_attributes)
        return utt, State.chit3, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @decorator.transition
    def t_chit3(self):
        top = self.cm.topics[-1]
        if 'ask_leave' in self.lexicals or 'req_topic_jump' in self.sys or (self.dialog_act and 'closing' in self.dialog_act):
            return State.done
        elif self.backstory['confidence'] > CHITCHAT_BACKSTORY_CONFIDENCE:
            return State.backstory
        elif self.chitchat_regex:
            return State.chit1
        elif 'topic_qa' in self.topic:
            return State.evi
        elif self.cm.prev_state == State.why or self.cm.prev_state == State.opinion and template_election.has_selector(selector='chitchat/' + top + '/four'):
            return State.chit4
        elif 'ask_yesno' in self.lexicals:
            return State.opinion
        elif 'ask_opinion' in self.lexicals:
            return State.opinion
        elif self.backstory['confidence'] > BACKSTORY_CONFIDENCE:
            return State.backstory
        elif 'ask_reason' in self.lexicals:
            return State.why
        elif 'ask_advice' in self.lexicals or 'ask_person' in self.lexicals:
            return State.simpleOp
        elif 'req_more' in self.sys:
            return State.more
        elif self.dialog_act:
            if 'yes_no_question' in self.dialog_act:
                return State.simpleYN
            elif 'open_question_factual' in self.dialog_act:
                return State.evi
        return

    @decorator.state
    def s_chit3(self, params: dict):

        top = self.cm.topics[-1]
        ack = self.central_elem_text
        utt = ''
        # t = random.randint(1, 4)
        # if ack and t < 3:
        #     utt = template_election.utterance(selector='chitchat/ack',
        #                                       slots={'ack': ack},
        #                                       user_attributes_ref=self.cm.user_attributes)
        utt += template_election.utterance(selector='chitchat/' + top + '/three',
                                           slots={},
                                           user_attributes_ref=self.cm.user_attributes)

        if template_election.has_selector(selector='chitchat/' + top + '/four'):
            return utt, State.chit4, ProposeContinue.CONTINUE, ProposeTopic.NONE
        else:
            return utt, State.startchit, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @decorator.transition
    def t_chit4(self):
        top = self.cm.topics[-1]
        if 'ask_leave' in self.lexicals or 'req_topic_jump' in self.sys or (self.dialog_act and 'closing' in self.dialog_act):
            return State.done
        elif self.backstory['confidence'] > CHITCHAT_BACKSTORY_CONFIDENCE:
            return State.backstory
        elif self.chitchat_regex:
            return State.chit1
        elif 'topic_qa' in self.topic:
            return State.evi
        elif 'ask_yesno' in self.lexicals:
            return State.opinion
        elif 'ask_opinion' in self.lexicals:
            return State.opinion

        elif self.cm.prev_state == State.why or self.cm.prev_state == State.opinion and template_election.has_selector(selector='chitchat/' + top + '/five'):
            return State.chit5

        elif self.backstory['confidence'] > BACKSTORY_CONFIDENCE:
            return State.backstory
        elif 'ask_reason' in self.lexicals:
            return State.why
        elif 'ask_advice' in self.lexicals or 'ask_person' in self.lexicals:
            return State.simpleOp
        elif 'req_more' in self.sys:
            return State.more
        elif self.dialog_act:
            if 'yes_no_question' in self.dialog_act:
                return State.simpleYN
            elif 'open_question_factual' in self.dialog_act:
                return State.evi
        return

    @decorator.state
    def s_chit4(self, params: dict):

        top = self.cm.topics[-1]
        ack = self.central_elem_text
        utt = ''
        # t = random.randint(1, 4)
        # if ack and t < 3:
        #     utt += template_election.utterance(selector='chitchat/ack',
        #                                        slots={'ack': ack},
        #                                        user_attributes_ref=self.cm.user_attributes)
        utt = template_election.utterance(selector='chitchat/' + top + '/four',
                                          slots={},
                                          user_attributes_ref=self.cm.user_attributes)

        if template_election.has_selector(selector='chitchat/' + top + '/five'):
            return utt, State.chit5, ProposeContinue.CONTINUE, ProposeTopic.NONE
        else:
            return utt, State.startchit, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @decorator.transition
    def t_chit5(self):
        top = self.cm.topics[-1]
        if 'ask_leave' in self.lexicals or 'req_topic_jump' in self.sys or (self.dialog_act and 'closing' in self.dialog_act):
            return State.done
        elif self.backstory['confidence'] > CHITCHAT_BACKSTORY_CONFIDENCE:
            return State.backstory
        elif self.chitchat_regex:
            return State.chit1
        elif 'topic_qa' in self.topic:
            return State.evi
        elif 'ask_yesno' in self.lexicals:
            return State.opinion
        elif 'ask_opinion' in self.lexicals:
            return State.opinion
        elif self.cm.prev_state == State.why or self.cm.prev_state == State.opinion and template_election.has_selector(selector='chitchat/' + top + '/six'):
            return State.chit6
        elif self.backstory['confidence'] > BACKSTORY_CONFIDENCE:
            return State.backstory
        elif 'ask_reason' in self.lexicals:
            return State.why
        elif 'ask_advice' in self.lexicals or 'ask_person' in self.lexicals:
            return State.simpleOp
        elif 'req_more' in self.sys:
            return State.more
        elif self.dialog_act:
            if 'yes_no_question' in self.dialog_act:
                return State.simpleYN
            elif 'open_question_factual' in self.dialog_act:
                return State.evi
        return

    @decorator.state
    def s_chit5(self, params: dict):
        top = self.cm.topics[-1]
        ack = self.central_elem_text
        utt = ''
        # t = random.randint(1, 4)
        # if ack and t < 3:
        #     utt += template_election.utterance(selector='chitchat/ack',
        #                                        slots={'ack': ack},
        #                                        user_attributes_ref=self.cm.user_attributes)
        utt = template_election.utterance(selector='chitchat/' + top + '/five',
                                          slots={},
                                          user_attributes_ref=self.cm.user_attributes)

        if template_election.has_selector(selector='chitchat/' + top + '/six'):
            return utt, State.chit6, ProposeContinue.CONTINUE, ProposeTopic.NONE
        else:
            return utt, State.startchit, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @decorator.transition
    def t_chit6(self):
        top = self.cm.topics[-1]
        if 'ask_leave' in self.lexicals or 'req_topic_jump' in self.sys or (self.dialog_act and 'closing' in self.dialog_act):
            return State.done
        elif self.backstory['confidence'] > CHITCHAT_BACKSTORY_CONFIDENCE:
            return State.backstory
        elif self.chitchat_regex:
            return State.chit1
        elif 'topic_qa' in self.topic:
            return State.evi
        elif 'ask_yesno' in self.lexicals:
            return State.opinion
        elif 'ask_opinion' in self.lexicals:
            return State.opinion
        elif self.cm.prev_state == State.why or self.cm.prev_state == State.opinion and template_election.has_selector(selector='chitchat/' + top + '/seven'):
            return State.chit7
        elif self.backstory['confidence'] > BACKSTORY_CONFIDENCE:
            return State.backstory
        elif 'ask_reason' in self.lexicals:
            return State.why
        elif 'ask_advice' in self.lexicals or 'ask_person' in self.lexicals:
            return State.simpleOp
        elif 'req_more' in self.sys:
            return State.more
        elif self.dialog_act:
            if 'yes_no_question' in self.dialog_act:
                return State.simpleYN
            elif 'open_question_factual' in self.dialog_act:
                return State.evi
        return

    @decorator.state
    def s_chit6(self, params: dict):

        top = self.cm.topics[-1]
        ack = self.central_elem_text
        utt = ''
        # t = random.randint(1, 4)
        # if ack and t < 3:
        #     utt += template_election.utterance(selector='chitchat/ack',
        #                                        slots={'ack': ack},
        #                                        user_attributes_ref=self.cm.user_attributes)
        utt = template_election.utterance(selector='chitchat/' + top + '/six',
                                          slots={},
                                          user_attributes_ref=self.cm.user_attributes)

        if template_election.has_selector(selector='chitchat/' + top + '/seven'):
            return utt, State.chit7, ProposeContinue.CONTINUE, ProposeTopic.NONE
        else:
            return utt, State.startchit, ProposeContinue.CONTINUE, ProposeTopic.NONE


# Mark: - Testing

class UserAttributes:
    def __init__(self):
        self.pchat = {}
        self.prev_hash = {}
        self.last_module = ""


if __name__ == "__main__":

    from pprint import pprint

    def _input_data(
            text: str,
            keywords: List[Tuple[float, str]],
            knowledge: Tuple[str, str, float, str],
            sentiment: str
    ):
        from nlu.intent_classifier import IntentClassifier
        intents = IntentClassifier(text).getIntents()

        return {
            'text': text,
            'features': {
                'ner': None, 'sentiment': sentiment,
                'intent': 'general',
                'topic': [
                    {
                        'topicClass': 'Games', 'confidence': 999.0,
                        'text': text,
                        'topicKeywords': [{'confidence': k[0], 'keyword': k[1]} for k in keywords]
                    }
                ],
                'intent_classify': intents,
                # {
                #     'sys': ['req_playgame'], 'topic': ['topic_game'], 'lexical': lexical  # ['ask_yesno']
                # },
                'topic_module': 'GAMECHAT',
                'topic_keywords': [{'confidence': k[0], 'keyword': k[1]} for k in keywords],
                # [('minecraft', 'Video game', 123.718781, 'game')], 80 for now
                'knowledge': knowledge
                # 'noun_phrase': ["", "", "minecraft", "your favorite game", "creative"]
            }
        }

    test_cases_db = [

    ]

    def get_test_case(index: List[int]):
        return [test_cases_db[i] for i in index]

    test_cases = [
        # get_test_case([6, 24, 25]),
        get_test_case([27])
    ]

    for idx, test_case in enumerate(test_cases):

        _u_a = UserAttributes()
        session_id = 123

        # NOTE: don't print anything in production code
        print('Test Case: {}\n'.format(idx))
        for i, t in enumerate(test_case):
            # print('round {}'.format(i))
            input_data = _input_data(*t)
            print('usr:\t', t, input_data['features']['intent_classify'])

            _ctx = ContextManager(session_id=session_id,
                                  user_attributes_ref=_u_a, state_class=State)
            _rg = ElectionAutomaton(
                input_data=input_data, context_manager=_ctx)
            _utt = _rg.response()

            # NOTE: don't print anything in production code
            print('utt:\t', _utt, _rg.cm.prev_state, _rg.cm.curr_state)
            # print(_rg.curr_state)
            # print(_u_a.modulechat)

            _u_a.last_module = "GAMECHAT"

        _u_a.last_module = ""

        pprint(_u_a.__dict__)
