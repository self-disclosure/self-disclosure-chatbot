from .states import FashionState

from typing import TYPE_CHECKING
from template_manager import Template
from response_generator.fsm.utils import FSMAttributeAdaptor, FSMModule
from response_generator.fsm.state import State

if TYPE_CHECKING:
    from cobot_core.service_module import LocalServiceModule

import logging
logger = logging.getLogger(__name__)

class FashionUserAttributes(FSMAttributeAdaptor):

    @property
    def module_name(self) -> str:
        return "fashionchat"

class FashionModule(FSMModule):

    def __init__(self, response_generator_ref: 'LocalServiceModule'):
        super().__init__(response_generator_ref,
                         Template.fashion,
                         FashionUserAttributes,
                         FashionState)
