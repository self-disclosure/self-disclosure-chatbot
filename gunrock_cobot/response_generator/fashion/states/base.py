from response_generator.fsm.state import State
from response_generator.fsm.events import NextState, Event
from response_generator.fsm.utils import Dispatcher, Tracker
from typing import Optional, List

# Logging
import logging
logger = logging.getLogger("fashion.states")

class FashionState(State):
    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        pass

def is_most_recent_topic(tracker, topic):
    try:
        if tracker.persistent_store["most_recent_recommendation_topic"] == topic: return True
    except:
        pass
    return False

def stop_bot(dispatcher: Dispatcher, tracker: Tracker):
    dispatcher.propose_continue("STOP")
    return [NextState("propose_stop", jump=False)]

def get_next_topic(dispatcher: Dispatcher, tracker: Tracker):

    try:
        next_state = tracker.persistent_store["fashion_sub_topics"].pop()
    except:
        return stop_bot(dispatcher, tracker)

    if next_state == "style":
        if is_most_recent_topic(tracker, "makeup"):
            dispatcher.respond("Speaking of makeup and fashion, I was wondering if you also enjoy shopping online for clothes?")
            tracker.persistent_store["most_recent_recommendation_topic"] = "style"
            return [NextState('style_a1', jump=False)]
        tracker.persistent_store["most_recent_recommendation_topic"] = "style"
        return [NextState('style_q1', jump=True)]

    if next_state == "makeup":
        if is_most_recent_topic(tracker, "style"):
            dispatcher.respond("Speaking of clothes, I think makeup can be a perfect touch to any outfit. Are you interested in make-up?")
            tracker.persistent_store["most_recent_recommendation_topic"] = "makeup"
            return [NextState("makeup_routine_q1", jump=False)]
        else:
            dispatcher.respond("So, recently, I've been following makeup tips from Kim Kardashian. Are you interested in make-up?")
            tracker.persistent_store["most_recent_recommendation_topic"] = "makeup"
            return [NextState("makeup_routine_q1", jump=False)]

    if next_state == "fragrance":
        if is_most_recent_topic(tracker, "style"):
            fragrance = "fragrance"
            if tracker.persistent_store['gender'] == "female": fragrance = 'perfume'
            elif tracker.persistent_store['gender'] == "male": fragrance = 'cologne'
            dispatcher.respond(f"By the way, the perfect complement to any outfit is the right {fragrance}.")
            dispatcher.respond(f"Do you want me to use my A.I. technology to match you with the perfect {fragrance}?")
            tracker.persistent_store["most_recent_recommendation_topic"] = "fragrance"

        elif is_most_recent_topic(tracker, "makeup"):
            dispatcher.respond("Speaking of makeup, I think the right perfume can be the perfect complement to any look. Would you like me to recommend you one?")
            tracker.persistent_store["most_recent_recommendation_topic"] = "fragrance"

        else:
            utt = "Do you want me to use my A.I. technology to match you with the perfect scent?"
            if tracker.persistent_store['gender'] == "female":
                dispatcher.respond(f"So, on the topic of fashion, I personally love perfume! {utt}")
            elif tracker.persistent_store['gender'] == "male":
                dispatcher.respond(f"So, on the topic of fashion, I really like cologne. {utt}")
            else:
                dispatcher.respond(f"So, on the topic of fashion, I really like fragrances. {utt}")
            tracker.persistent_store["most_recent_recommendation_topic"] = "fragrance"

        return [NextState("does_user_want_a_recommendation", jump=False)]

    return stop_bot(dispatcher, tracker)