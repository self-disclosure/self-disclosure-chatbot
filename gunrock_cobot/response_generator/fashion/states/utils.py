from .base import *
import json
import logging
import re
from dataclasses import dataclass, field, InitVar
from typing import Callable, Dict, List, Optional, TYPE_CHECKING, Type
from nlu.intentmap_scheme import lexical_re_patterns
from nlu.intentmap_scheme import sys_re_patterns
from response_generator.fsm.events import Event, NextState, PreviousState
from response_generator.fsm.utils import Dispatcher, Tracker
from response_generator.fsm.state import State
from user_profiler.user_profile import UserProfile
from nlu.constants import *

import logging
logger = logging.getLogger("fashion.states")

"""
A default fashionstate necessary for the FSM to work
"""

def should_jump(tracker:Tracker):
    if re.search(sys_re_patterns["terminate"], tracker.input_text): return True
    if re.search(sys_re_patterns["change_topic"], tracker.input_text): return True
    if re.search(sys_re_patterns["get_off_topic"], tracker.input_text): return True
    if re.search(r"(.*)(please stop|exit|power off|turn off|stop the conversation|go away|stop talking|stop chat|leave me alone)(.*)", tracker.input_text): return True
    return False

def stop_bot(dispatcher : Dispatcher, tracker : Tracker):
    """
    Automatically stops the bot and returns the next state.
    :param dispatcher:
    :return: [NextState]
    """
    # dispatcher.respond_template("exit", {})
    dispatcher.propose_continue("STOP")
    return [NextState("propose_stop", jump=False)]

def ack(dispatcher, tracker):
    if "did_ack" in tracker.one_turn_store.keys(): return
    if is_unsure(tracker): dispatcher.respond_template("ack/uncertain", {})
    elif is_maybe(tracker): dispatcher.respond("Maybe, you say?")
    elif is_pos(tracker):
        dispatcher.respond_template("ack/pos", {})
    elif is_neg(tracker): dispatcher.respond_template("ack/neg", {})
    elif is_neu(tracker): dispatcher.respond_template("ack/pos", {})

# Question detection
def is_question(tracker: Tracker):
    # if tracker.central_element.dialog_act.name in ['open_question', 'open_question_opinion', 'open_question_factual']: return False

    if tracker.returnnlp.has_dialog_act([
        DialogAct.OPEN_QUESTION,
        DialogAct.OPEN_QUESTION_FACTUAL,
        DialogAct.OPEN_QUESTION_OPINION,
        DialogAct.YES_NO_QUESTION
    ]): return True

    # for key in lexical_re_patterns.keys():
    #     if "ask_" in key and re.search(lexical_re_patterns[key], tracker.input_text):
    #         return True

    if re.search(r"\b(would you like)\b", tracker.input_text.lower()):
        return True

    return False

# Sentiment
def is_pos(tracker: Tracker):
    if tracker.returnnlp.answer_positivity is Positivity.pos: return True
    return False
    # if tracker.central_element.dialog_act.name in ["pos_answer"] or tracker.central_element.sentiment == 'pos': return True
    # return False
def is_neu(tracker: Tracker):
    if tracker.returnnlp.answer_positivity is Positivity.neu: return True
    if re.search(r"(.*)(it is fine|its fine|it's fine|it is okay|its okay|it's okay)(.*)", tracker.input_text): return True
    return False
    # if tracker.central_element.sentiment == 'neu':
    #     return True
    # return False
def is_neg(tracker: Tracker):
    if tracker.returnnlp.answer_positivity is Positivity.neg: return True
    # if tracker.central_element.dialog_act.name in ["neg_answer"] or tracker.central_element.sentiment == 'neg' or 'ans_neg' in tracker.central_element.custom_intents.lexical: return True
    return False

# Unsure
def is_maybe(tracker: Tracker):
    if re.search(r"(.*)(maybe|perhaps|sometimes|occasionally)(.*)", tracker.input_text) and num_concept(tracker) == 0: return True
    return False

def is_unsure(tracker: Tracker):
    # re.search(lexical_re_patterns["ans_unknown"], tracker.input_text) or
    if re.search(r"(.*)(i'm not sure|im not sure|i am not sure|i dont know|i don't know|i do not know)(.*)", tracker.input_text.lower()):
        return True
    return False

# Nouns
def noun_phrase(tracker:Tracker):
    possessive_pronouns = ['their', 'him', 'her', 'our', 'your', 'it', 'mine', 'a', "i", 'like', 'to', 'love', 'wear', 'too', 'any']

    # edge case
    if "louis" in tracker.input_text or "vuitton" in tracker.input_text:
        return "louis vuitton"

    try:
        logger.info("MY NOUN PHRASE")
        logger.info(tracker.returnnlp.noun_phrase)

        noun_phrase = tracker.returnnlp.noun_phrase[0][0]

        noun_phrase = noun_phrase.strip()

        for pro_noun in possessive_pronouns:
            if len(re.compile('\b'+pro_noun+'\b').findall(noun_phrase)) > 0:
                noun_phrase = noun_phrase.replace(pro_noun, "")
            if len(re.compile('\b'+pro_noun+'s\b').findall(noun_phrase)) > 0:
                noun_phrase = noun_phrase.replace(pro_noun, "")
            if len(re.compile('\b'+pro_noun+"'s\b").findall(noun_phrase)) > 0:
                noun_phrase = noun_phrase.replace(pro_noun, "")

        if noun_phrase != "":
            if noun_phrase.split(" ")[0].lower() == "the": noun_phrase = noun_phrase.replace('the ', '', 1)

        return noun_phrase

    except Exception as error:
        logger.info("JOSHERROR")
        logger.info(error)
        return ""

def noun_str(tracker:Tracker):
    nouns = get_nouns(tracker)
    if len(nouns) == 0: return ""
    if len(nouns) == 1: return nouns[0]
    if len(nouns) > 1:
        result = nouns.pop()
        while len(nouns) > 0:
            if len(nouns) == 1:
                result += ' and ' + nouns.pop()
            else:
                result += ', ' + nouns.pop()
        return result
def num_concept(tracker: Tracker):
    return len([item for sublist in [x for x in tracker.returnnlp.concept] for item in sublist])
def get_nouns(tracker: Tracker):
    result = []
    blacklist = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "zero", 'i', "I", 'I\'d']
    for x in [x for x in tracker.returnnlp.googlekg]:
        try:
            result += x
        except:
            continue
    # logger.info("josh test")
    # logger.info(result)
    return [x.name.lower() for x in result if len(x.name.split(" ")) == 1 and str(x.name.lower()) not in blacklist]
def contains_noun(tracker: Tracker):
    if len(tracker.returnnlp.noun_phrase) > 0: return True
    return False
def concept(tracker: Tracker, data: List=[]):
    # if not contains_noun(tracker): return False
    result = []
    for x in [item for sublist in [x for x in tracker.returnnlp.concept] for item in sublist]:
        try:
            result += [y.description for y in x.data]
        except:
            continue
    logging.info("detected concept:")
    logging.info(result)
    for item in result:
        for topic in data:
            if topic in item: return True
    return False

def gender(tracker: Tracker):
    ua_ref = tracker.user_attributes.ua_ref
    user_profile = UserProfile(ua_ref)
    gender = user_profile.gender
    return gender