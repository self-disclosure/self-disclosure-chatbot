from ..base import *
from ..utils import *
from response_generator.fsm.events import Event, NextState
from response_generator.fsm.utils import Dispatcher, Tracker
from typing import List
import re
import logging
logger = logging.getLogger("fashion.states")

USER_RARELY_APPLYS_MAKEUP = "user_rarely_applys_makeup"

class MakeUpIntro(FashionState):
    name = "makeup_routine_q1"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:

        # Ack
        ack(dispatcher, tracker)

        # Ask if they want to discuss make up
        if is_pos(tracker) or is_neu(tracker):
            dispatcher.respond("So, do you wear makeup everyday, or mostly for special occasions?")
            return [NextState('makeup_usage', jump=False)]

        # Else, lets get the next topic
        tracker.persistent_store["skipped_makeup"] = True
        return get_next_topic(dispatcher, tracker)

class MakeUpUsage(FashionState):
    name = "makeup_usage"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:

        # Ack
        ack(dispatcher, tracker)

        if re.search(r"\b(everyday|most of the time|most days|often|a lot|all the time|regularly)\b", tracker.input_text) and not is_neg(tracker):
            dispatcher.respond("I think wearing make up often leads to feeling more confident and taking better care of oneself.")
            dispatcher.respond("What do you think?")
            return [NextState("makeup_do_you_agree", jump=False)]

        elif re.search(r"\b(occasionally|not often|special|event|events|not much|occasions|occasion|only when|only for|sometimes|when i feel)\b", tracker.input_text.lower()):
            dispatcher.respond("It's always nice to add an extra touch to your appearance for special occasions with make-up!")
            return [NextState("makeup_ask_natural_vs_dramatic", jump=True)]

        elif re.search(r"\b(never|i don't|i dont|occasions|only when|only for|sometimes|when I feel|hate|when i feel)\b", tracker.input_text.lower()) or is_neg(tracker):
            dispatcher.respond("It's nice to not have to spend time every day putting on make up. I know some people can spend two hours everyday applying make up, its a bit crazy, don't you agree?")
            tracker.persistent_store[USER_RARELY_APPLYS_MAKEUP] = True
            return [NextState("makeup_do_you_agree", jump=False)]

        return get_next_topic(dispatcher, tracker)

class MakeupDoYouAgree(FashionState):
    name = "makeup_do_you_agree"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        if re.search(r"\b(agree|yes you're right|yes your right|yes i do|yeah i do|i think|yeah its|yeah it's|yeah a little|yeah i definitely|confident|good|attractive|yeah sure|sure i do)\b", tracker.input_text.lower()) \
                and is_pos(tracker):
            dispatcher.respond("I'm glad you agree!")
        elif re.search(r"\b(disagree|no|i don't|i do not|wrong|not really|yeah but)\b", tracker.input_text.lower()) and not is_unsure(tracker) or is_neg(tracker):
            dispatcher.respond("That's okay if you don't agree.")
        elif is_unsure(tracker):
            ack(dispatcher, tracker)
        else:
            dispatcher.respond("Yeah, right.")
            if len(tracker.input_text.split(" ")) > 5:
                dispatcher.respond("I see, that's a good point.")

        if not USER_RARELY_APPLYS_MAKEUP in tracker.persistent_store:
            return [NextState("makeup_ask_natural_vs_dramatic", jump=True)]

        return get_next_topic(dispatcher, tracker)

class MakeupAskNaturalVSDramatic(FashionState):
    name = "makeup_ask_natural_vs_dramatic"

    def run(self, dispatcher, tracker):
        dispatcher.respond("So what's your opinion, do you prefer more natural looking makeup, or something more dramatic?")
        return [NextState("makeup_answer_natural_vs_dramatic", jump=False)]
class MakeupAnswerNaturalVSDramatic(FashionState):
    name = "makeup_answer_natural_vs_dramatic"

    def run(self, dispatcher, tracker):

        if re.search(r"\b(natural|calm|simple|not over the top|too much|healthy|cakey|cake)\b", tracker.input_text.lower()):
            dispatcher.respond("Yeah, I think a more natural look complements a person's features more.")
        elif re.search(r"\b(dramatic|bright|flashy|blue|purple|highlights|highlight|flash|orange|pink|dark)\b", tracker.input_text.lower()):
            dispatcher.respond("Yeah, I think a look with some flair really stands out.")
        else:
            ack(dispatcher, tracker)

        return get_next_topic(dispatcher, tracker)

