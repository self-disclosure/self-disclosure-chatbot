from ..base import *
from ..utils import *
from response_generator.fsm.events import Event, NextState
from response_generator.fsm.utils import Dispatcher, Tracker
from typing import List
import re
import logging

logger = logging.getLogger("fashion.states")

"""
This is the state we transition to when we propose stop, so we can decide what to do after the user exits our module.
In the fashion module, to stop the bot, always call and return the following:

dispatcher.respond_template("exit", {})
dispatcher.propose_continue("STOP")
return [NextState("propose_stop", jump=False)]
"""
class Terminate(FashionState):
    name = 'propose_stop'

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        return [NextState('fashion_resume_mode_global', jump=True)]