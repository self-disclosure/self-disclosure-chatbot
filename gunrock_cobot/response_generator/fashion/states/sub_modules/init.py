from ..base import *
from response_generator.fsm.events import Event, NextState, PreviousState
from response_generator.fsm.utils import Dispatcher, Tracker
from typing import List
from user_profiler.user_profile import UserProfile

# Logging
import logging
logger = logging.getLogger("fashion.states")

class Init(FashionState):

    name = 'init'

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        return get_next_topic(dispatcher, tracker)