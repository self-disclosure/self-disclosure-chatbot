from ..base import *
from ..utils import *
from response_generator.fsm.events import Event, NextState
from response_generator.fsm.utils import Dispatcher, Tracker
from typing import List
import re
import logging

logger = logging.getLogger("fashion.states")

class StyleQ1(FashionState):
    name = 'style_q1'

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        dispatcher.respond("So, I was wondering, do you enjoy shopping for clothes?")
        try:
            del tracker.persistent_store["style_a1_catch"]
        except: pass
        return [NextState('style_a1', jump=False)]

class StyleA1(FashionState):
    name = 'style_a1'

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:

        # online shopping
        if re.search(r"\bonline shopping\b|\bshopping online\b", tracker.input_text.lower()):
            dispatcher.respond("Shopping online is really convenient!")
            return [NextState('style_q2', jump=True)]

        if re.search(r"\bcan't|cannot|not at the moment|not right now|corona|corona virus|coronavirus\b", tracker.input_text.lower()) and not "style_a1_catch" in tracker.persistent_store:
            dispatcher.respond("No worries. What about shopping online for clothes?")
            tracker.persistent_store['style_a1_catch'] = True
            return [NextState('style_a1', jump=False)]

        # Sometimes
        if re.search(r"\b(sometimes|occasionally|when i feel like it|occasions|rarely|when i need)\b", tracker.input_text.lower()):
            dispatcher.respond("So you like to go shopping occasionally? Nice.")
            return [NextState('style_q2', jump=True)]

        # Positive
        elif is_pos(tracker):
            if re.search(r"\b(love|favourite)\b", tracker.input_text.lower()):
                dispatcher.respond("I guess we have that in common then—we both love shopping!")
            else:
                ack(dispatcher, tracker)
                dispatcher.respond("I'm happy you like shopping too.")
            return [NextState('style_q2', jump=True)]

        # Neutral
        elif is_neu(tracker) and re.search(r'\b(ok|okay|sure|why not)\b', tracker.input_text.lower()) or is_unsure(tracker):
            ack(dispatcher, tracker)
            return [NextState('style_q2', jump=True)]

        # Other / Negative
        ack(dispatcher, tracker)
        return get_next_topic(dispatcher, tracker)

class StyleQ2(FashionState):
    name = 'style_q2'

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        dispatcher.respond(
            "When shopping, do you prefer to buy comfortable, practical clothes, or whatever looks the best?")
        return [NextState('style_a2', jump=False)]

class StyleA2(FashionState):
    name = 'style_a2'

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        comfy = re.search(r"(.*)(comfy|practical|cheap|affordable|sale|comfortable)(.*)", tracker.input_text.lower())
        best = re.search(r"(.*)(best|looks|stylish|nicest|cool|coolest|latest|trendy)(.*)", tracker.input_text.lower())
        both = re.search(r"(.*)(both)(.*)", tracker.input_text.lower())

        if re.search(r"\b(don't mind|dont mind|do not mind|don't care|do not care|dont care)\b", tracker.input_text.lower()):
            dispatcher.respond("No worries!")
            return [NextState('style_q3', jump=True)]

        if both or (comfy and best):
            dispatcher.respond(
                "That's great, clothes that are both comfy and stylish are the best!")
        elif comfy:
            dispatcher.respond("Yes! Comfort first is the way to go. Sweat pants look a lot more comfortable to wear than jeans.")
        elif best:
            dispatcher.respond("Nice! It must feel good knowing you look extra stylish!")
        else:
            ack(dispatcher, tracker)

        return [NextState('style_q3', jump=True)]

class StyleQ3(FashionState):
    name = 'style_q3'

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        dispatcher.respond("Do you prefer to shop for summer clothes or winter clothes?")
        return [NextState('style_a3', jump=False)]

class StyleA3(FashionState):
    name = 'style_a3'

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        winter = re.search(r"\b(winter|cold|comfy)\b", tracker.input_text.lower())
        summer = re.search(r"\b(summer|hot)\b", tracker.input_text.lower())
        spring = re.search(r"\b(spring)\b", tracker.input_text.lower())
        fall = re.search(r"\b(fall|autumn)\b", tracker.input_text.lower())
        both = re.search(r"\b(both)\b", tracker.input_text.lower())

        if both:
            dispatcher.respond(
                "Me too! It's nice that we enjoy both summer and winter fashion styles.")
        elif spring:
            dispatcher.respond("Yes! Spring is also a great time to go clothes shopping.")
        elif fall:
            dispatcher.respond("Yes! Fall is also a great time to go clothes shopping.")
        elif summer:
            dispatcher.respond("Nice! Summer clothes are great because the clothes have bright colors, plus, you don't always have to wear jackets or long pants.")
        elif winter:
            dispatcher.respond("Nice! Winter clothes are great because they're usually a lot more comfortable and warm, like oversized hoodies.")
        else:
            ack(dispatcher, tracker)

        return get_next_topic(dispatcher, tracker)

