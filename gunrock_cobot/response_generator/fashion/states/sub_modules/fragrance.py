from ..base import *
from ..utils import *
from typing import Optional, List
from response_generator.fsm.events import Event, NextState
from response_generator.fsm.utils import Dispatcher, Tracker

import logging
logger = logging.getLogger("fashion.states")

class DetectWantToBuy(FashionState):
    name = 'detect_want_to_buy'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:
        if re.search(r"\b(buy|purchase)\b", tracker.input_text):
            if tracker.persistent_store["most_recent_recommendation_topic"] == "fragrance":
                return DetectWantToBuy.name
        return None

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        dispatcher.respond("Hmm. I'm not sure where you can buy it sorry, but you can definitely check online!")
        return get_next_topic(dispatcher, tracker)

class FragranceGlobalCatch(FashionState):
    name = 'fragrance_global_catch'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:
        if re.search(r"(.*)(lets|can we|i want to|)(.*)(chat|talk|discuss|learn|hear|find out)(.*)(cologne|fragrance|perfume|smell|scent)(.*)", tracker.input_text):
            return FragranceGlobalCatch.name
        return None

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        dispatcher.respond("Sounds good!")
        tracker.persistent_store["fashion_sub_topics"].remove('fragrance')
        tracker.persistent_store["fashion_sub_topics"].insert(0, "fragrance")
        try:
            del tracker.persistent_store["most_recent_recommendation_topic"]
        except:
            pass
        return get_next_topic(dispatcher, tracker)

"""
A basic class for each recommendation item.
self.matching_array is an array of strings that we can see how many overlap
self.rec_text is the text we will "print" / "respond" with to the user
"""
class FashionRecommendationItem:
    def __init__(self, tags, rec):
        self.matching_array = tags
        self.rec_text = rec

"""
A database full of different fragrance recommendations alongside their properities
"""
class FashionFragranceRecommendationDatabase:
    """
    Masculine or feminine
    - man, mens type of fragrances
    - woman, womens type of fragrance
    - unisex, both men and women

    Strong or Light
    - strong, a high level of projection
    - light, not overwhelming, causal-ware

    Fragrance type:
    - ocean : fresh fragrances
    - fire, woody, musky type of fragrances
    - candy, sweet fragrances
    - flower, floral type fragrances
    """
    def __init__(self):
        self.items = []

        # Below are combinations for every type

        # CK One
        self.items += [FashionRecommendationItem(["unisex", "light", "ocean"], "CK One by Calvin Klein is a unisex, citrus aromatic fragrance. Its top notes include pineapple, papaya and cardamom. It has moderate longevity and soft to moderate <lang xml:lang='fr-FR'>sillage</lang>, perfect for casual everyday ware.")]

        # CK One Summer
        self.items += [FashionRecommendationItem(["unisex", "light", "fire"], "CK One Summer by Calvin Klein is a unisex fragrance with a woody, musky, floral aroma. Its top notes include kiwi, mandarin orange and peach. It has moderate longevity and soft to moderate <lang xml:lang='fr-FR'>sillage</lang>, a perfect, light scent for the summer season.")]

        # Eau de Rhubarbe Ecarlate
        self.items += [FashionRecommendationItem(["unisex", "light", "candy"], "<lang xml:lang='fr-FR'>Eau de Rhubarbe Ecarlate</lang> by <lang xml:lang='fr-FR'>Hermès</lang> is a unisex fragrance composed of notes of rhubarb and red berries. It has a moderate longevity and a soft projection. A light, fruity, sweet aroma, perfect for the spring and summer.")]

        # Un Jardin sur le Nil
        self.items += [FashionRecommendationItem(["unisex", "light", "flower"], "<lang xml:lang='fr-FR'>Un Jardin sur le Nil</lang> by <lang xml:lang='fr-FR'>Hermès</lang> is an iconic fragrance with citrus, green and floral accords. Mango, grapefruit and bulrush are the main notes, and it has a moderate longevity and sillage, a light scent inspired by the garden islands of the Nile at <lang xml:lang='fr-FR'>Assouan</lang>.")]

        # Virgin Island Water
        # Temporarily disable this to avoid triggering warning with "virgin"
        # self.items += [FashionRecommendationItem(["unisex", "strong", "ocean"], "Virgin Island Water by Creed is a fragrance inspired by a sailing trip in the Carribbean islands. It celebrates the tropical and exotic scents carried by the trade winds of the Sir Francis Drake channel. A high quality scent, perfect for any occasion.")]

        # Oud Wood
        self.items += [FashionRecommendationItem(["unisex", "strong", "fire"], "Oud Wood by Tom Ford is a unisex fragrance with a woody, warm and spicy aroma. Comprised of notes of agarwood, Brazillian rosewood and sandalwood, Oud Wood is a mature smell that is both mysterious and alluring.")]

        # Tobacco Vanille
        self.items += [FashionRecommendationItem(["unisex", "strong", "candy"], "<lang xml:lang='fr-FR'>Tobacco Vanille</lang> by Tom Ford is a fragrance for both men and women. Its main accord is sweet, comprised of notes of tobacco, vanilla and tonka bean. A long lasting and strong scent with a heavy sillage that is noticable as soon as you enter a room.")]

        # Carnal flower
        self.items += [FashionRecommendationItem(["unisex", "strong", "flower"], "<lang xml:lang='fr-FR'>Carnal flower</lang> by <lang xml:lang='fr-FR'>Frederick Malle</lang> is a scent inspired by the forbidden flower, tuberose. Passionate and intoxicating, the floral scent is completed with fruity nuances of melon and coconut that makes for a scent that can be worn all year round.")]

        # Acqua di Gio by Gorgia Armani
        self.items += [FashionRecommendationItem(["man", "light", "ocean"], "<lang xml:lang='fr-FR'>Acqua di Gio</lang> by <lang xml:lang='fr-FR'>Gorgia Armani</lang> is a scent of freedom, full of wind and water. Comprised from the sweet and salty notes of the ocean, followed by the scorching notes of the mediterranean sea. A perfect scent for the warmer seasons.")]

        # Bleu de Chanel
        self.items += [FashionRecommendationItem(["man", "light", "fire"], "<lang xml:lang='fr-FR'>Bleu de Chanel</lang> is a sophisticated and contemporary fragrance with a woody aromatic composition. Grapefruit, incense and amber represent the main notes, and this fragrance can be worn anytime of the year.")]

        # Allure home sport
        self.items += [FashionRecommendationItem(["man", "light", "candy"], "<lang xml:lang='fr-FR'>Allure Homme Sport by Chanel</lang> is a masculine fragrance with a citric and aromatic aroma. Notes of orange, aldehyde and the ocean make for a sweet scent with sharp flavours. A great choice for the summer.")]

        # Probably won't find too many scents like this
        #FashionRecommendationItem(["man", "light", "flower"], "")

        # Green Irish Tweed
        self.items += [FashionRecommendationItem(["man", "strong", "ocean"], "Green Irish Tweed by Creed is an iconic fougere fragrance, this fresh, citrus, sporty scent is favourite for many celebrities. A natural, high quality fragrance that can be worn for many occassions.")]

        # YSL La nuit De l'homme
        self.items += [FashionRecommendationItem(["man", "strong", "fire"], "<lang xml:lang='fr-FR'>La Nuit De L'homme by yves saint laurent</lang> is a masculine fragrance full of contrasts and tensions. Mysterious, explosive and aromatic with notes of cardamom, lavender and cedar, it is an iconic scent of our time.")]

        # Boss bottled Intense by Hugo Boss
        self.items += [FashionRecommendationItem(["man", "strong", "candy"], "Boss Bottled Intense by Hugo Boss is a strong masculine scent, wrapped in notes of green apple, vanilla and cinnamon. It is a warm and cozy scent that opens with sweet top notes. It is a quality scent that can be worn anytime of the day, but responds better during colder seasons.")]

        # Probably doesn't exist
        #FashionRecommendationItem(["man", "strong", "flower"], "")

        # Probably doesn't exist
        # FashionRecommendationItem(["woman", "light", "ocean"], "")

        # Nomade Chloe for women
        self.items += [FashionRecommendationItem(["woman", "light", "fire"], "<lang xml:lang='fr-FR'>Nomade Chloe</lang> for women by <lang xml:lang='fr-FR'>Chloe</lang> is a new pillar fragrance for its collection. <lang xml:lang='fr-FR'>Nomade</lang> evokes the bold and adventurous side of women, with an aromatic floral chypre composition that includes notes of mirabelle plum, freesia and oak moss.")]

        # Black Opium Intense
        self.items += [FashionRecommendationItem(["woman", "light", "candy"], "Black Opium Intense by <lang xml:lang='fr-FR'>Yves Saint Laurent</lang> is a fragrance that represents a magical, bold and independent woman. The fragrance opens with notes of absinthe and boysenberry, with coffee and vanilla notes representing the heart and base notes. The fragrance is smooth and refined, perfect for a night out.")]

        # Aura Mugler Eau de Parfum
        self.items += [FashionRecommendationItem(["woman", "light", "flower"], "<lang xml:lang='fr-FR'>Aura Mugler Eau de Parfum Sensuelle by Mugler</lang> is an oriental vanilla fragrance for women with white floral and green accords. A simple composition of gardenia, green notes and cinnamon leaf make for a light, long lasting floral fragrance.")]

        # Probably doesn't exist
        # FashionRecommendationItem(["woman", "strong", "ocean"], "")

        # Gucci Guilty Absolute
        self.items += [FashionRecommendationItem(["woman", "strong", "fire"], "Gucci Guilty Absolute Pour Femme by Gucci is a mysterious fragrance composed of woody and fruity accords. The main notes of this perfume include blackberry and cypress. A non-traditional chypre fruity fragrance for a contemporary woman.")]

        # Coco mademoiselle Intenense by Chanel
        self.items += [FashionRecommendationItem(["woman", "strong", "candy"], "<lang xml:lang='fr-FR'>Coco mademoiselle intense by Chanel</lang> is a powerful and deep composition featuring notes of patchouli, vanilla and tonka beans. The aroma can be described as warm, soft and feminine. Ultimately, it's a sweet and balsamic fragrance perfect for any occasion.")]

        # Chance Eau Tendre Eau de Parfum
        self.items += [FashionRecommendationItem(["woman", "strong", "flower"], "<lang xml:lang='fr-FR'>Chance Eau Tendre Eau de Parfum by Chanel</lang> is aromatic blend of citrus, rose and fruity accords. The fragrance radiants confidence and femininity, while its floral heart brings a feeling of absolute tenderness.")]

"""
We handle whether or not the user wants a recommendation. If they respond with anything other than a positive or neutral answer, we do NOT continue. This includes a question.
We ask the user the first question in order to recommend them a fragrance.
"""
class DoesUserWantARecommendation(FashionState):

    name = "does_user_want_a_recommendation"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:

        ack(dispatcher, tracker)

        if re.search(r"\b(i'm|i am|im) (fine)\b", tracker.input_text):
            return get_next_topic(dispatcher, tracker)

        if re.search(r"\b(i'm|i am|im) (fine|okay|good) (for now|right now|at the moment)\b", tracker.input_text):
            return get_next_topic(dispatcher, tracker)

        if is_unsure(tracker):
            return get_next_topic(dispatcher, tracker)

        # Here are some regexes for some potential things users can say that are not handled
        if re.search(r"(.*)(maybe|you decide|up to you)(.*)", tracker.input_text) and not re.search(r"\b(no thanks|no thank you|no)\b", tracker.input_text):
            dispatcher.respond("How about we give it a try anyway?")
            return self.get_recommendation_topic_and_ask_question(tracker, dispatcher)

        # The user wants a recommendation
        if is_pos(tracker) or is_neu(tracker): return self.get_recommendation_topic_and_ask_question(tracker, dispatcher)

        tracker.persistent_store["skipped_fragrance"] = True
        return get_next_topic(dispatcher, tracker)

    def get_recommendation_topic_and_ask_question(self, tracker, dispatcher):
        dispatcher.respond_template("fragrance/ask/q2", {})
        tracker.persistent_store["fragrance_q1"] = "unisex"
        if gender(tracker) == "female": tracker.persistent_store["fragrance_q1"] = "female"
        if gender(tracker) == "male": tracker.persistent_store["fragrance_q1"] = "male"
        tracker.persistent_store["most_recent_recommendation_topic"] = "fragrance"
        return [NextState("fragrance_q2", jump=False)]

"""
If we continue, we analyze the users answer to the first question. We try to assign a value of either masculine, feminine, or unisex for the gender of the fragrance.
If we detect a negative answer, we stop our bot.
We then ask them the second question about strong or light fragrance. This is kind of difficult to detect, so maybe we need to improve this. 
"""
class ProcessQ1Fragrance(FashionState):

    name = "fragrance_q1"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        if "disable_ack" in tracker.one_turn_store.keys() and tracker.one_turn_store["disable_ack"] == True: return self.continue_to_next_question(tracker, dispatcher)

        tracker.persistent_store["fragrance_q1"] = ''

        if re.search(r"(.*)(i don't mind|i do not mind|i dont mind|either is)(.*)", tracker.input_text):
            tracker.persistent_store["fragrance_q1"] = "unisex"
            return self.continue_to_next_question(tracker, dispatcher)

        if is_question(tracker): tracker.volatile_store["ack_question"] = True

        # Handle the user being unsure
        if is_unsure(tracker):
            tracker.persistent_store["fragrance_q1"] = "uncertain"
            return self.continue_to_next_question(tracker, dispatcher)

        # If the user said a negative response
        if is_neg(tracker):
            tracker.persistent_store["fragrance_q1"] = "unisex"
            return self.continue_to_next_question(tracker, dispatcher)

        # I added spaces for "man" and for "men" to ensure its not woman and women

        # If both men and girl type scent detected, this is confusing for us so lets stick a unisex scent
        if re.search(r"(.*)( man|masculine| men)(.*)", tracker.input_text) and re.search(r"(.*)(girl|feminine|women|lady|woman|eminem)(.*)", tracker.input_text):
            tracker.persistent_store["fragrance_q1"] = "unisex"
            tracker.volatile_store["simple_positive_ack"] = True
            return self.continue_to_next_question(tracker, dispatcher)

        # If neither men or girl type scent detected, this is confusing for us so lets stick a unisex scent
        if not re.search(r"(.*)( man|masculine| men|guy)(.*)", tracker.input_text) and not re.search(r"(.*)(girl|feminine|women|lady|gal|woman|eminem)(.*)", tracker.input_text):
            tracker.persistent_store["fragrance_q1"] = "unisex"
            tracker.volatile_store["simple_positive_ack"] = True
            return self.continue_to_next_question(tracker, dispatcher)

        # If  men and not girl
        if re.search(r"(.*)( man|masculine| men|guy)(.*)", tracker.input_text) and not re.search(r"(.*)(girl|feminine|women|lady|gal|woman|eminem)(.*)", tracker.input_text):
            tracker.persistent_store["fragrance_q1"] = "man"
            return self.continue_to_next_question(tracker, dispatcher)

        # If  girl and not man
        if not re.search(r"(.*)( man|masculine| men|guy)(.*)", tracker.input_text) and re.search(r"(.*)(girl|feminine|women|lady|gal|woman|eminem)(.*)", tracker.input_text):
            tracker.persistent_store["fragrance_q1"] = "woman"
            return self.continue_to_next_question(tracker, dispatcher)

        return get_next_topic(dispatcher, tracker)

    def stop_bot(self, dispatcher):
        """
        Automatically stops the bot and returns the next state.
        :param dispatcher:
        :return: [NextState]
        """
        dispatcher.respond_template("exit", {})
        dispatcher.propose_continue("STOP")
        return [NextState("propose_stop", jump=False)]

    def continue_to_next_question(self, tracker, dispatcher):
        # Acknowledge their choice:
        if "disable_ack" in tracker.one_turn_store.keys() and tracker.one_turn_store["disable_ack"] == True:
            pass
        elif tracker.volatile_store["ack_question"] == True:
            dispatcher.respond_template('ack/unknown_question', {})
        elif tracker.volatile_store["simple_positive_ack"] == True:
            dispatcher.respond("Got it!")
        elif tracker.persistent_store["fragrance_q1"] == "man":
            dispatcher.respond_template('fragrance/ack/q1/man', {})
        elif tracker.persistent_store["fragrance_q1"] == "woman":
            dispatcher.respond_template('fragrance/ack/q1/woman', {})
        elif tracker.persistent_store["fragrance_q1"] == "uncertain":
            dispatcher.respond_template("ack/either", {}) # I feel like uncertain is similar to either
        elif tracker.persistent_store["fragrance_q1"] == "unisex":
            dispatcher.respond("Okay, we will go with something flexible then.")

        dispatcher.respond_template("fragrance/ask/q2", {})
        return [NextState("fragrance_q2", jump=False)]

"""
Similar to Previous State, we analyze the answer. It primarily uses a regex to analyze the answer, thus, this must be improved.
If a negative answer, or if we don't catch any of the handlers we, simply stop the bot where we are.
"""
class ProcessQ2Fragrance(FashionState):

    name = "fragrance_q2"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:

        if "disable_ack" in tracker.one_turn_store.keys() and tracker.one_turn_store["disable_ack"] == True: return self.continue_to_next_question(tracker, dispatcher)

        tracker.persistent_store["fragrance_q2"] = "neither"

        if is_question(tracker): tracker.volatile_store["ack_question"] = True

        if re.search(r"(.*)(both)(.*)", tracker.input_text):
            tracker.persistent_store["fragrance_q2"] = "both"
            return self.continue_to_next_question(tracker, dispatcher)

        if re.search(r"(.*)(i don't mind|i dont mind|i do not mind)(.*)", tracker.input_text):
            tracker.persistent_store["fragrance_q2"] = "either"
            return self.continue_to_next_question(tracker, dispatcher)

        # Handle the user being unsure
        if is_unsure(tracker):
            tracker.persistent_store["fragrance_q2"] = "uncertain"
            return self.continue_to_next_question(tracker, dispatcher)

        strong_regex = r"(.*)(strong|intense|powerful|potent|stands out|stand out|noticeable|apparent|sharp|aromatic|spicy|notice|spicy|easily smell)(.*)"
        light_regex = r"(.*)(mild|bland|lighter|light|fresh|not to strong|nothing too|nothing to|overwhelming|liar|later)(.*)"

        # A strong scent or light scent
        # TODO: Make this more accurate detection
        if re.search(strong_regex, tracker.input_text):
            tracker.persistent_store["fragrance_q2"] = "strong"
            return self.continue_to_next_question(tracker, dispatcher)
        elif re.search(light_regex, tracker.input_text):
            tracker.persistent_store["fragrance_q2"] = "light"
            return self.continue_to_next_question(tracker, dispatcher)

        # If we are uncertain what the user said
        if is_pos(tracker) or is_neu(tracker):
            return self.continue_to_next_question(tracker, dispatcher)

        # If the user said a negative response, lets stop this madness.
        if is_neg(tracker):
            # dispatcher.respond_template("ack/neg", {})
            tracker.persistent_store["fragrance_q2"] = "either" # for now we propose either scent and continue forward as opposed to stoping
            return self.continue_to_next_question(tracker, dispatcher)

        # dispatcher.respond_template("error", {})
        return get_next_topic(dispatcher, tracker)

    def stop_bot(self, dispatcher):
        """
        Automatically stops the bot and returns the next state.
        :param dispatcher:
        :return: [NextState]
        """
        dispatcher.respond_template("exit", {})
        dispatcher.propose_continue("STOP")
        return [NextState("fragrance_q3", jump=False)]

    def continue_to_next_question(self, tracker, dispatcher):

        # Acknowledge their choice:
        if "disable_ack" in tracker.one_turn_store.keys() and tracker.one_turn_store["disable_ack"] == True:
            pass
        elif tracker.volatile_store["ack_question"] == True:
            dispatcher.respond_template('ack/unknown_question', {})
        elif tracker.persistent_store["fragrance_q2"] == "strong":
            dispatcher.respond_template('fragrance/ack/q2/strong', {})
        elif tracker.persistent_store["fragrance_q2"] == "light":
            dispatcher.respond_template('fragrance/ack/q2/light', {})
        elif tracker.persistent_store["fragrance_q2"] == "uncertain":
            dispatcher.respond_template("ack/uncertain", {})
        elif tracker.persistent_store["fragrance_q2"] == "either":
            dispatcher.respond_template("ack/either", {})
        elif tracker.persistent_store["fragrance_q2"] == "both":
            dispatcher.respond("Hmm okay. I'll try find something in between!")
        else:
            dispatcher.respond_template('ack/pos', {})

        dispatcher.respond_template("fragrance/ask/q3", {})
        tracker.persistent_store["ignore_question"] = True
        return [NextState("fragrance_q3", jump=False)]

"""
The last question analysis state. We analyze whether or not the user chose what type of scent.
We then no matter what offer a recommendation and ask how we did.
"""
class ProcessQ3Fragrance(FashionState):

    name = "fragrance_q3"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        tracker.persistent_store["fragrance_q3"] = ""

        # Unsure or we regex detect a "surprise me"
        surprise_regex = r"(.*)(surprise)(.*)"
        if re.search(surprise_regex, tracker.input_text): tracker.persistent_store["fragrance_q3"] = "surprise"

        # We ignore questions for this turn
        # if is_question(tracker): tracker.volatile_store["ack_question"] = True

        if is_unsure(tracker): tracker.persistent_store["fragrance_q3"] = "uncertain"

        # try to extract keywords
        ocean_regex = r"(.*)(ocean|sea)(.*)"
        fire_regex = r"(.*)(fire|fireplace|fire place|wood)(.*)"
        candy_regex = r"(.*)(candy|sweet|lollies|chocolate|candy shop)(.*)"
        flower_regex = r"(.*)(garden|flower|bed|jungle|forest|leaves|green)(.*)"

        neither_regex = r"(.*)(neither|don't like|don't have a preference|don't mind|do not mind|any of them|don't really like|do not like)(.*)"
        if re.search(neither_regex, tracker.input_text) or is_neg(tracker): tracker.persistent_store["fragrance_q3"] = "neither"

        if re.search(ocean_regex, tracker.input_text): tracker.persistent_store["fragrance_q3"] = "ocean"
        if re.search(fire_regex, tracker.input_text): tracker.persistent_store["fragrance_q3"] = "fire"
        if re.search(candy_regex, tracker.input_text): tracker.persistent_store["fragrance_q3"] = "candy"
        if re.search(flower_regex, tracker.input_text): tracker.persistent_store["fragrance_q3"] = "flower"

        # Lets just assume everything is okay at this point and make the recommendation
        return self.continue_to_next_question(tracker, dispatcher)

    def stop_bot(self, dispatcher):
        """
        Automatically stops the bot and returns the next state.
        :param dispatcher:
        :return: [NextState]
        """
        dispatcher.respond_template("exit", {})
        dispatcher.propose_continue("STOP")
        return [NextState("propose_stop", jump=False)]

    def continue_to_next_question(self, tracker, dispatcher):
        # Acknowledge their choice:
        if tracker.volatile_store["ack_question"] == True:
            dispatcher.respond_template('ack/unknown_question', {})
        elif tracker.persistent_store["fragrance_q3"] == "surprise" and not "acknowledged_question" in tracker.one_turn_store.keys():
            dispatcher.respond("Ooh a surprise.")
        elif tracker.persistent_store["fragrance_q3"] == "uncertain" and not "acknowledged_question" in tracker.one_turn_store.keys():
            dispatcher.respond_template("ack/uncertain", {})

        # Acknowledge their selection
        if tracker.persistent_store["fragrance_q3"] == "ocean": dispatcher.respond_template("fragrance/ack/q3/ocean", {})
        if tracker.persistent_store["fragrance_q3"] == "fire": dispatcher.respond_template("fragrance/ack/q3/fire", {})
        if tracker.persistent_store["fragrance_q3"] == "candy": dispatcher.respond_template("fragrance/ack/q3/candy", {})
        if tracker.persistent_store["fragrance_q3"] == "flower": dispatcher.respond_template("fragrance/ack/q3/flower", {})
        if tracker.persistent_store["fragrance_q3"] == "neither": dispatcher.respond_template("fragrance/ack/q3/neither", {})

        # Final option for acknowledgement
        if tracker.persistent_store["fragrance_q3"] == "" and tracker.volatile_store["ack_question"] == False: dispatcher.respond_template('ack/pos', {})

        # Update the uncertain category to unisex
        if tracker.persistent_store["fragrance_q1"] == "uncertain": tracker.persistent_store["fragrance_q1"] = "unisex"

        # Make the recommendation
        rec = FashionFragranceRecommendationDatabase().items[0].rec_text
        tags = [tracker.persistent_store["fragrance_q1"], tracker.persistent_store["fragrance_q2"], tracker.persistent_store["fragrance_q3"]]
        current_len = 0
        for item in FashionFragranceRecommendationDatabase().items:
            result = len([x for x in item.matching_array if x in tags])
            if result >= current_len and tracker.persistent_store["fragrance_q1"] in item.matching_array:
                rec = item.rec_text
                current_len = result
        dispatcher.respond_template('fragrance/rec', {'rec':rec})

        tracker.persistent_store["recommended_fragrance_name"] = rec.split("is a")[0]

        # Next state
        return [NextState("fragrance_ask_opinion", jump=True)]

class AskOpinionFragrance(FashionState):

    name = "fragrance_ask_opinion"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        dispatcher.respond_template('fragrance/ask_user_opinion', {})
        tracker.persistent_store['ignore_question'] = True
        return self.continue_to_next_question(tracker, dispatcher)

    def stop_bot(self, dispatcher):
        """
        Automatically stops the bot and returns the next state.
        :param dispatcher:
        :return: [NextState]
        """
        dispatcher.respond_template("exit", {})
        dispatcher.propose_continue("STOP")
        return [NextState("propose_stop", jump=False)]

    def continue_to_next_question(self, tracker, dispatcher):
        tracker.persistent_store['ignore_question'] = True
        return [NextState("fragrance_end", jump=False)]

"""
Here, we simply provide an acknowledgement to whether or not the user appreciated our recommendation, and then we finish the fashion module. 
"""
class EndFragranceRecommendation(FashionState):

    name = "fragrance_end"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        # User wants to repeat
        if re.search(r"(.*)(like)(.*)(better)", tracker.input_text):
            dispatcher.respond("That's also a great choice.")
        elif re.search(r"(.*)(not exactly sure if i like|not sure if i exactly like)(.*)", tracker.input_text):
            dispatcher.respond("That's okay if you're not sure if you'll exactly like it or not, but sometimes, you have to be bold and make a decision.")

        elif re.search(r"(.*)(how much)(.*)", tracker.input_text):
            name = tracker.persistent_store["recommended_fragrance_name"].lower()
            dispatcher.respond("Most fragrances are around one hundred dollars.")
            if "tom ford" not in name or "creed" not in name:
                dispatcher.respond(f'I would guess {name} would be around the same price.')
            dispatcher.respond("Some colognes like Aventus Creed or Tom Ford can be as much as five hundred dollars!")
            dispatcher.respond("What do you think of that?")
            return [NextState("fragrance_follow_up_on_price", jump=False)]

        elif re.search(r"(.*)(what was it again|what was the name again|can you repeat the name|say the name again|the name again)(.*)", tracker.input_text):
            dispatcher.respond("Sure, let me repeat that for you. The fragrance is " + tracker.persistent_store["recommended_fragrance_name"])
        elif re.search(r"(.*)(who sells|where (.*) buy)(.*)", tracker.input_text):
            dispatcher.respond("Sorry, I'm not exactly sure who sells " + tracker.persistent_store["recommended_fragrance_name"] + ", but you could try looking it up online.")
        elif is_question(tracker):
            dispatcher.respond_template('ack/unknown_question', {})
        elif re.search(r"(.*)(fine|not bad)(.*)", tracker.input_text):
            dispatcher.respond_template("fragrance/ack/finish/pos", {})
        elif is_neg(tracker) and not is_neu(tracker) or "terrible" in tracker.input_text.lower() or "not very good" in tracker.input_text.lower():
            dispatcher.respond_template("fragrance/ack/finish/neg", {})
        elif is_neu(tracker) or is_pos(tracker):
            dispatcher.respond_template("fragrance/ack/finish/pos", {})
        elif is_unsure(tracker):
            dispatcher.respond_template("ack/uncertain", {})

        # dispatcher.respond_template("fragrance/ack/finish/transition", {})

        return get_next_topic(dispatcher, tracker)
        # return self.stop_bot(dispatcher)

    def stop_bot(self, dispatcher):
        """
        Automatically stops the bot and returns the next state.
        :param dispatcher:
        :return: [NextState]
        """
        # dispatcher.respond_template("exit", {})
        dispatcher.propose_continue("STOP")
        return [NextState("propose_stop", jump=False)]

class FragranceFollowUpOnPrice(FashionState):

    name = "fragrance_follow_up_on_price"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        if re.search(r"\b(pricey|expensive|costs a lot|way too much)\b", tracker.input_text.lower()):
            dispatcher.respond("I know right!")
        else:
            ack(dispatcher, tracker)
        return get_next_topic(dispatcher, tracker)