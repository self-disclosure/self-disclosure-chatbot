from ..base import *
from ..utils import *
from typing import Optional, List
from response_generator.fsm.events import Event, NextState, PreviousState
from response_generator.fsm.utils import Dispatcher, Tracker
from template_manager.manager import Template, TemplateManager
from nlu.constants import TopicModule
from nlu.question_detector import QuestionDetector
from question_handling.quetion_handler import QuestionHandler, QuestionResponse, QuestionResponseTag
from nlu.constants import TopicModule
from selecting_strategy.module_selection import ModuleSelector, GlobalState
from time import time

# Logging
import logging
logger = logging.getLogger("fashion.states")

def is_user_female(tracker):
    is_female = False
    ua_ref = tracker.user_attributes.ua_ref
    user_profile = UserProfile(ua_ref)
    gender = user_profile.gender
    tracker.persistent_store['gender'] = gender
    if gender == "female": is_female = True
    return is_female

"""
We try to enter the fashion resume mode first.

We check if fashion is not the last topic and if its in the used topics.
"""
class FashionResumeMode(FashionState):
    name = 'fashion_resume_mode_global'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:
        user_attributes_ref = tracker.module.response_generator_ref.state_manager.user_attributes
        module_selector = ModuleSelector(user_attributes_ref)
        last_topic = module_selector.last_topic_module
        used_topics = module_selector.used_topic_modules

        if (TopicModule.FASHION.value != last_topic and "fashion_sub_topics" in tracker.persistent_store):
            try:
                del tracker.persistent_store["resume"]
            except:
                return FashionResumeMode.name

        return None

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        dispatcher.respond("I'm excited to talk about fashion again.")
        if len(tracker.persistent_store["fashion_sub_topics"]) > 0:
            return get_next_topic(dispatcher, tracker)
        return [NextState('trivia_q', jump=True)]

# Important initialization state
"""
This will only be called at the start once if the resume mode is not called.
"""
class InitGlobal(FashionState):
    name = 'global_init'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:
        user_attributes_ref = tracker.module.response_generator_ref.state_manager.user_attributes
        module_selector = ModuleSelector(user_attributes_ref)
        last_topic = module_selector.last_topic_module

        if TopicModule.FASHION.value != last_topic:

            tracker.persistent_store["fashion_sub_topics"] = []
            tracker.persistent_store["most_recent_recommendation_topic"] = "intro"

            # Gender detection / user modelling
            if is_user_female(tracker):
                tracker.persistent_store["fashion_sub_topics"] += ["fragrance"]
                tracker.persistent_store["fashion_sub_topics"] += ["makeup"]
                tracker.persistent_store["fashion_sub_topics"] += ["style"]
            else:
                tracker.persistent_store["fashion_sub_topics"] += ["fragrance"]
                tracker.persistent_store["fashion_sub_topics"] += ["style"]

        return None

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        pass

# If the user wants to discuss clothes/shopping/style
class DetectStyleSubModule(FashionState):
    name = 'detect_style_submodule'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:
        if re.search(r"(.*)(lets|let us|let's|can we|want|can you)(.*)(talk|discuss|chat|tell)(.*)(shop|cloth|style)(.*)", tracker.input_text):
            return DetectStyleSubModule.name
        return None

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        dispatcher.respond("Sure, lets talk about clothes and shopping!")
        if "style" in tracker.persistent_store["fashion_sub_topics"]:
            del tracker.persistent_store["fashion_sub_topics"][tracker.persistent_store["fashion_sub_topics"].index("style")]
        return [NextState('style_q2', jump=True)]

class DetectShoppingQuestion(FashionState):
    name = 'detect_shopping_question'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:
        if re.search(r"(.*)(do|you)(.*)(like)(.*)(shop|cloth|buy)(.*)", tracker.input_text):
            return DetectShoppingQuestion.name
        return None

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:

        dispatcher.respond("Yes, I love shopping and buying new clothes.")

        if "style" in tracker.persistent_store["fashion_sub_topics"]:
            del tracker.persistent_store["fashion_sub_topics"][tracker.persistent_store["fashion_sub_topics"].index("style")]
            dispatcher.respond("Do you enjoy shopping  and buying clothes too?")
            return [NextState('style_a1', jump=False)]

        return get_next_topic(dispatcher, tracker)

class DetectAnotherRecommendation(FashionState):
    name = 'detect_another_recommendation'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:
        if re.search(r"(.*)(recommend)(.*)(me|another)(.*)(one|fragrance|cologne|perfume|scent|smell)(.*)", tracker.input_text) and (not is_neg(tracker)):
            return DetectAnotherRecommendation.name

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:

        dispatcher.respond("I'd love to recommend you another fragrance.")

        try:
            tracker.persistent_store["fashion_sub_topics"].remove("fragrance")
        except Exception as error:
            logger.info(error)

        dispatcher.respond_template("fragrance/ask/q2", {})
        tracker.persistent_store["fragrance_q1"] = "unisex"
        if gender(tracker) == "female": tracker.persistent_store["fragrance_q1"] = "female"
        if gender(tracker) == "male": tracker.persistent_store["fragrance_q1"] = "male"
        tracker.persistent_store["most_recent_recommendation_topic"] = "fragrance"
        return [NextState("fragrance_q2", jump=False)]

# If the user wants to discuss makeup
class DetectMakeupSubModule(FashionState):
    name = 'detect_makeup_submodule'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:
        if re.search(r"(.*)(lets|let us|let's|can we|want|can you|do you)(.*)(talk|discuss|chat|tell)(.*)(makeup|make up|make-up)(.*)", tracker.input_text):
            return DetectMakeupSubModule.name
        return None

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        dispatcher.respond("Sure, lets talk about makeup!")
        dispatcher.respond("So, I'm wondering, do you like to wear makeup often?")
        try:
            tracker.persistent_store["fashion_sub_topics"].remove("makeup")
        except Exception as error:
            logger.info(error)
        return [NextState('makeup_routine_q2', jump=False)]

"""
If we don't resume, we enter the normal fashion module for the first time.
"""
class FashionIntro(FashionState):
    name = 'fashion_intro_global'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:

        user_attributes_ref = tracker.module.response_generator_ref.state_manager.user_attributes
        module_selector = ModuleSelector(user_attributes_ref)
        last_topic = module_selector.last_topic_module
        global_state = module_selector.global_state

        # Check global state
        if global_state == GlobalState.ASK_CLOTHES and TopicModule.FASHION.value != last_topic:
            try:
                tracker.persistent_store["fashion_sub_topics"].remove("style")
            except:
                pass
            tracker.persistent_store["most_recent_recommendation_topic"] = "style"
            return 'style_a1'
        if global_state == GlobalState.ASK_COMFORT_VS_PRACTICAL and TopicModule.FASHION.value != last_topic:
            try:
                tracker.persistent_store["fashion_sub_topics"].remove("style")
            except:
                pass
            tracker.persistent_store["most_recent_recommendation_topic"] = "style"
            return 'style_a2'

        if re.search(r"\bmakeup\b", tracker.input_text.lower()) and TopicModule.FASHION.value != last_topic and not is_neg(tracker) and \
                not re.search(r"(.*)(don't|do not|dont)(.*)(like)(.*)(makeup)(.*)", tracker.input_text.lower()):
            try:
                tracker.persistent_store["fashion_sub_topics"].remove("makeup")
            except:
                pass
            tracker.persistent_store["most_recent_recommendation_topic"] = "makeup"
            return 'makeup_routine_q1'

        if re.search(r"\b(clothes|clothe|shop|shopping)\b", tracker.input_text.lower()) and TopicModule.FASHION.value != last_topic and not is_neg(tracker) and \
                not re.search(r"(.*)(don't|do not|dont)(.*)(like)(.*)(cloth|clothes|shop|shopping)(.*)", tracker.input_text.lower()):
            try:
                tracker.persistent_store["fashion_sub_topics"].remove("style")
            except:
                pass
            tracker.persistent_store["most_recent_recommendation_topic"] = "style"
            return 'style_q1'

        # Check Fashion state
        if TopicModule.FASHION.value != last_topic: return FashionIntro.name

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        # Edge case
        if re.search(r"(.*)(im|i'm|i am)(.*)(boy|man|guy|male)(.*)", tracker.input_text.lower()):
            dispatcher.respond("No worries.")
            return stop_bot(dispatcher, tracker)

        dispatcher.respond("I'm excited to talk about fashion!")
        return get_next_topic(dispatcher, tracker)

# Important question Handler
class QuestionHandlerGlobalState(FashionState):
    name = 'handle_questions'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:
        # whatever looks the best
        if "look" in tracker.input_text.lower() and "best" in tracker.input_text.lower(): return
        if re.search(r"(.*)(what is it|tell me about it)(.*)", tracker.input_text.lower()): return
        if "ignore_question" in tracker.persistent_store:
            del tracker.persistent_store['ignore_question']
            return

        return_nlp_input = tracker.module.response_generator_ref.input_data['returnnlp']

        if QuestionDetector(tracker.input_text, return_nlp_input).has_question():
            return QuestionHandlerGlobalState.name
        return None

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:

        # Get input data
        return_nlp_input = tracker.module.response_generator_ref.input_data['returnnlp']
        system_ack = tracker.module.response_generator_ref.input_data['system_acknowledgement']
        user_attributes_ref = tracker.module.response_generator_ref.state_manager.user_attributes

        # Load the handler
        handler = QuestionHandler(complete_user_utterance=tracker.input_text,
                                  returnnlp=return_nlp_input,
                                  backstory_threshold=0.85, system_ack=system_ack,
                                  user_attributes_ref=user_attributes_ref)

        # If we cannot handle the question
        detector = QuestionDetector(tracker.input_text, return_nlp_input)
        do_not_answer = detector.is_ask_back_question() or detector.is_follow_up_question()
        if do_not_answer:
            answer = handler.generate_i_dont_know_response()
            dispatcher.respond(answer)
            tracker.one_turn_store["did_ack"] = True
            return get_next_topic(dispatcher, tracker)
            # prev = PreviousState()
            # prev.jump = True
            # return [prev]

        # General question handler
        answer = handler.handle_question()
        dispatcher.respond(answer.response)
        tracker.one_turn_store["did_ack"] = True

        if answer and answer.bot_propose_module and \
            answer.bot_propose_module != TopicModule.FASHION:
            tm = TemplateManager(Template.transition, user_attributes_ref)
            result_string = tm.speak(f"propose_topic_short/{answer.bot_propose_module.value}", {})
            dispatcher.respond(result_string)
            dispatcher.propose_continue('UNCLEAR', answer.bot_propose_module)
            return get_next_topic(dispatcher, tracker)
            #prev = PreviousState()
            #prev.jump = False
            #return [prev]

        return get_next_topic(dispatcher, tracker)
        # prev = PreviousState()
        # prev.jump = True
        # return [prev]