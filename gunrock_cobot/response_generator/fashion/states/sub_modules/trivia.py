from ..base import *
from ..utils import *
from response_generator.fsm.events import Event, NextState
from response_generator.fsm.utils import Dispatcher, Tracker
from typing import List
import re
import logging

logger = logging.getLogger("fashion.states")

class TriviaQ(FashionState):
    name = 'trivia_q'

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:

        if tracker.persistent_store["most_recent_recommendation_topic"] == "makeup":
            dispatcher.respond("We were just talking about makeup.")
            dispatcher.respond("I learned about a new spring 2020 makeup trend where models are wearing bright, fluorescent eyeliner. Did you know about that?")

        elif tracker.persistent_store["most_recent_recommendation_topic"] == "fragrance":

            dispatcher.respond("We were just talking about fragrances.")

            if tracker.persistent_store['gender'] == "male":
                dispatcher.respond("I learned the most popular cologne this year is Dior Sauvage. Did you know that?")
            elif tracker.persistent_store['gender'] == "female":
                dispatcher.respond("I learned the most popular perfume this year is Chanel Coco Mademoiselle. Did you know that?")
            else:
                dispatcher.respond("I learned the most popular cologne this year is Dior Sauvage. Did you know that?")

        elif tracker.persistent_store["most_recent_recommendation_topic"] == "style":
            dispatcher.respond("We were just talking about shopping for clothes.")

            if tracker.persistent_store['gender'] == "female":
                dispatcher.respond("I learned about a new fashion trend this spring where models are wearing green, neon dresses. Did you know about that?")
            else:
                dispatcher.respond("I learned that the purple Nike Air Jordan's are the most in demand sneakers at the moment. Did you know that?")

        else:
            return stop_bot(dispatcher, tracker) # stop bot if we don't have a most recent recommendation topic

        return [NextState('trivia_a', jump=False)]

class TriviaA(FashionState):
    name = 'trivia_a'

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:

        if is_neg(tracker):
            dispatcher.respond("That's okay! I thought it was interesting myself.")
        elif is_pos(tracker):
            dispatcher.respond("That's great.")
        else:
            ack(dispatcher, tracker)

        return stop_bot(dispatcher, tracker)