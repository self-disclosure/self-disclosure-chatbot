from cobot_core.service_module import LocalServiceModule
import response_generator.fashion.fashionchat as fashionchat
import logging

logger = logging.getLogger(__name__)

class FashionResponseGeneratorLocal(LocalServiceModule):
    def execute(self):
        module = fashionchat.FashionModule(self)
        response = module.generate_response()
        return response


