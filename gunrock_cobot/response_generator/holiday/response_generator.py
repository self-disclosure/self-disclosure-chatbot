import random
import re
import logging

import nlu.util_nlu
import utils
from cobot_core.service_module import LocalServiceModule
import template_manager
from cobot_common.service_client import get_client

template_topics = template_manager.Templates.topics

class HolidayFunfacts_ResponseGenerator(LocalServiceModule):
    """ Holiday topic module"""
    # list of specific major holidays
    major_holidays = ["patrick", "chinese new year", "chinese holiday", "indian holiday", "gift",
                      "jewish holiday", "halloween", "thanksgiving", "new year", "christmas",
                      "july fourth", "santa claus", "columbus day", "independence day", "diwali", "dussehra", "trick or treating"]

    holiday_topic_module_map = {
        'Food': 'FOODCHAT',
        'Pets': 'ANIMALCHAT',
        'Sports': 'SPORT',
        'Technology': 'TECHSCIENCECHAT',
        'Career': 'TECHSCIENCECHAT',
        'Fun': 'MOVIECHAT',
        'Politics': 'NEWS'
    }

    module_postfix = {
        'FOODCHAT': "<prosody rate = \"x-slow\">um, <break time = \"300ms\"></break> </prosody> This reminds me. are you a foodie?",
        'ANIMALCHAT': "<break time = \"300ms\"></break>, talking about pets holiday, sometimes I wonder, where we would be without the fluffy friends that show us an endless amount of love? ",
        'SPORT': "<break time = \"300ms\"></break> speaking of Sports holiday, what's the biggest day for you to celebrate? For me, it would be Super Bowl Sunday.",
        'TECHSCIENCECHAT': "<break time = \"300ms\"></break> By the way, do you think we spend too much time on technology and social media surrounding us?",
        'MOVIECHAT': '<break time = \"300ms\"></break> Ouu, Did I mention watching movies is my favorite thing to do?'
    }

    def EVI_bot(self, utterance):
        try:
            client = get_client(
                api_key='xgTjk23SRM9Q9VFrUEFOjav0Bn4ot21W8uFLEieT')
            r = client.get_answer(question=utterance, timeout_in_millis=1000)
            if r["response"] == "" or re.search(r"skill://|didn't get that", r["response"]):
                return None
            if re.search(r"(an opinion on that)|(usually defined as)|Sorry", r["response"]):
                return "<prosody rate = \"x-slow\">um, <break time = \"350ms\"></break> </prosody> <prosody rate = \"x-slow\">so,</prosody>, how do you usually spend your holidays?"
            return r["response"]
        except Exception as e:
            logging.info("[HOLIDAY_MODULE] EVI_bot error : {}".format(e))
            return None

    def execute(self):
        curr_utt = self.input_data['text'].lower()
        holidaychat = self.state_manager.user_attributes.holidaychat
        if holidaychat is None or 'holiday_state' not in holidaychat:
            holidaychat = {}

        # extract NLP attributes
        # senti = utils.get_feature_by_key(
        #     self.state_manager.current_state, 'sentiment')
        central_elem = nlu.util_nlu.get_feature_by_key(
            self.state_manager.current_state, 'central_elem')
        # intent_classify = utils.get_feature_by_key(self.state_manager.current_state, 'intent_classify')
        intent_classify = central_elem["regex"]
        lexical_intent = intent_classify['lexical']
        topic_intent = intent_classify['topic']
        sys_intent = intent_classify['sys']
        # senti = utils.get_sentiment_key(senti, lexical_intent)
        senti = central_elem['senti']
        backstory_dict = central_elem['backstory']
        dialog_act = central_elem['DA']
        dialog_act_score = float(central_elem['DA_score'])
        # noun_phrases = utils.get_feature_by_key(
        #     self.state_manager.current_state, 'noun_phrase')

        try:
            topic_keyword = nlu.util_nlu.get_feature_by_key(
                self.state_manager.current_state, "noun_phrase")[-1].replace("about|you|your|my", "")
            # topic_keyword = self.state_manager.current_state.features["topic_keywords"][0]['keyword']
        except Exception:
            topic_keyword = ''

        holidays_pattern = "(?P<major_holiday>" + \
            "|".join(self.major_holidays) + ")"
        holiday_match = re.search(holidays_pattern, curr_utt)

        # if 'topic_backstory' in topic_intent or 'ask_opinion' in lexical:
        # First logic to handle strong one turn, command and miscelaneous
        # intents
        if backstory_dict['confidence'] > 0.85:
            holidaychat['propose_continue'] = "STOP"
            # holidaychat['holiday_state'] = "transition"
            self.state_manager.user_attributes.holidaychat = holidaychat
            resp = backstory_dict['text']
            if backstory_dict.get('followup', None) is not None:
                resp = resp + "<break time=\"200ms\"></break> <say-as interpret-as=\"interjection\">Ooh! </say-as> " + \
                    backstory_dict['followup']
            return resp
        elif holiday_match is not None:
            holidaychat['propose_continue'] = "CONTINUE"
            holidaychat['holiday_state'] = "special_holiday"
            self.state_manager.user_attributes.holidaychat = holidaychat
            picked_holiday = holiday_match.group("major_holiday")
            holidaychat['majorholiday'] = picked_holiday

            # posts_resps = utils.get_interesting_reddit_posts(picked_holiday, 6)
            # posts_resps = list(filter(lambda x: re.search(picked_holiday, x.lower()), posts_resps))
            # postfixes = [" I'm curious, what's your favorite tradition on ", " So, do you like "]
            try:
                if picked_holiday in ['gift', 'halloween', 'christmas', 'santa claus', 'thanksgiving', 'new year', 'trick or treating']:
                    picked_holiday = "_".join(picked_holiday.split(' ')) if re.search(
                        r" ", picked_holiday) else picked_holiday
                    if picked_holiday == "gift":
                        resp = template_topics.utterance(selector='holiday_chat/gift/open',
                                                         slots={},
                                                         user_attributes_ref=self.state_manager.user_attributes)
                    else:
                        resp = template_topics.utterance(selector='holiday_chat/%s' % picked_holiday,
                                                         slots={},
                                                         user_attributes_ref=self.state_manager.user_attributes)
                else:
                    posts_resps = utils.get_interesting_reddit_posts(
                        picked_holiday, 6)
                    posts_resps = list(filter(lambda x: re.search(
                        picked_holiday, x.lower()), posts_resps))
                    resp = random.choice(posts_resps)
                # if picked_holiday not in ['santa_claus', "trick_or_treating"]:
                #         resp = resp + random.choice(postfixes) + \
                #             picked_holiday + "? "
                if picked_holiday in ["christmas", "thanksgiving", "new year"]:
                    postfixes = ["<break time=\"200ms\"/> So, What's your favorite thing to do on %s ?" % picked_holiday,
                                 "<break time=\"200ms\"/> Any plans on %s?" % picked_holiday,
                                 "<break time=\"200ms\"/> Do you have any lovely memory on %s to share? " % picked_holiday]
                    holidaychat['propose_continue'] = "CONTINUE"
                    resp = resp + random.choice(postfixes)
            except Exception:
                resp = "<say-as interpret-as=\"interjection\">aha</say-as>, What's your favorite tradition or memory on " + picked_holiday + "? "
            return resp
        elif 'ask_opinion' in lexical_intent or (dialog_act == "open_question_opinion" and dialog_act_score > 0.85):
            try:
                posts_resps = utils.get_interesting_reddit_posts(
                    topic_keyword, 6)
                posts_resps = list(filter(lambda x: re.search(
                    r"\b%s\b" % topic_keyword, x.lower()), posts_resps))
                prefix = random.choice(
                    ["Sometimes, I think ", "Well, just a random thought, ", "Hmm, I just have this random thought, "])
                resp = prefix + \
                    random.choice(posts_resps).lower().replace("today", "")
                # print(posts_resps)
            except Exception:
                resp = "<say-as interpret-as=\"interjection\">aha</say-as>, you wanted my thoughts on it. I swear." +\
                    " I haven't thought about that before. Wanna contiue our chat on holiday?"
                if topic_keyword != '':
                    resp = resp.replace('it', topic_keyword)
            holidaychat['propose_continue'] = "STOP"
            # holidaychat['holiday_state'] = "self_disclosure"
            return resp
        elif re.search(r"national holiday|famous holiday", curr_utt):
            holidaychat['propose_continue'] = "STOP"
            self.state_manager.user_attributes.holidaychat = holidaychat
            return template_topics.utterance(selector='holiday_chat/national_holiday', slots={},
                                             user_attributes_ref=self.state_manager.user_attributes)
        elif re.search(r"tomorrow", curr_utt):
            holiday_content = utils.get_future_holiday(limit=3)
            selected_holiday = holiday_content[-1]
            self.state_manager.user_attributes.holidaychat = holidaychat
            holidaychat['holiday_state'] = "chitchat"
            holidaychat['propose_continue'] = "STOP"
            return template_topics.utterance(selector='holiday_chat/tomorrow_holiday', slots={'holiday': selected_holiday['Holiday']},
                                             user_attributes_ref=self.state_manager.user_attributes)
        elif re.search(r"you don't know|not good|not great|not interesting|hate|boring|damn", curr_utt) or (dialog_act == "complaint" and dialog_act_score > 0.95):
            holidaychat['propose_continue'] = "STOP"
            self.state_manager.user_attributes.holidaychat = holidaychat
            return template_topics.utterance(selector='holiday_chat/complaint', slots={},
                                             user_attributes_ref=self.state_manager.user_attributes)
        elif holidaychat != {} and not re.search('obscure|scare', curr_utt) and (re.search(r"holiday \bin\b", curr_utt) or 'topic_qa' in topic_intent or (dialog_act == "open_question_factual" and dialog_act_score > 0.85)):
            holidaychat['propose_continue'] = "STOP"
            holidaychat['holiday_state'] = "self_disclosure"
            self.state_manager.user_attributes.holidaychat = holidaychat
            resp = self.EVI_bot(curr_utt.replace(r"yes|no", ""))
            if resp is None or resp == "" or re.search("don't know|response", resp):
                if topic_keyword != "":
                    error_prefix = "Hmm, you asked a one million dollar question about %s" % topic_keyword.replace(r"\byou\b", "I").replace(r"\bme\b", "you") + \
                        ". I will need to do more study on that. "
                    resp = error_prefix + template_topics.utterance(selector='holiday_chat/chitchat_err',
                                                                    slots={},
                                                                    user_attributes_ref=self.state_manager.user_attributes)
                    return resp
            else:
                return resp
        elif ('change_topic' in sys_intent or 'req_topic_jump' in sys_intent) and 'topic_holiday' not in topic_intent:
            holidaychat['propose_continue'] = "STOP"
            holidaychat['holiday_state'] = "self_disclosure"
            # check if there exists specific topic intent
            self.state_manager.user_attributes.holidaychat = holidaychat
            keyword = topic_keyword if topic_keyword != '' else 'something different'
            resp = template_topics.utterance(selector='holiday_chat/changetopic', slots={'keyword': keyword},
                                             user_attributes_ref=self.state_manager.user_attributes)
            return resp
        elif dialog_act == "device_command" and dialog_act_score > 0.8 or 'req_play_game' in sys_intent or 'req_play_music' in sys_intent or "req_task" in sys_intent:
            holidaychat['propose_continue'] = "STOP"
            return "I'm <emphasis level=\"strong\"> really </emphasis> sorry! I'm not able to do that in the social mode. <break time = \"200ms\"></break> what else would you like to chat about?"
        elif 'terminate' in sys_intent or (dialog_act == "closing" and dialog_act_score > 0.95):
            holidaychat['propose_continue'] = "STOP"
            holidaychat['holiday_state'] = "self_disclosure"
            setattr(self.state_manager.user_attributes,
                    "propose_topic", "TERMINATE")
            self.state_manager.user_attributes.holidaychat = holidaychat
            return template_topics.utterance(selector='holiday_chat/terminate',
                                             slots={},
                                             user_attributes_ref=self.state_manager.user_attributes)

        # Dialog Flow Logic - State Machine Transition
        holiday_content = utils.get_future_holiday(limit=3)
        # if holidaychat == {}:
        #     # store a list of dicts
        #     holidaychat['holiday_state'] = "init"
        #     todayholiday = holiday_content[0]
        #     holidaychat['holiday_idx'] = 0
        #     # holidaychat['holiday_content_idx'] = 0
        #     holidaychat['propose_continue'] = "CONTINUE"
        #     resp = template_topics.utterance(selector='holiday_chat/start',
        #                                      slots={
        #                                          'holiday': todayholiday['Holiday']},
        #                                      user_attributes_ref=self.state_manager.user_attributes)
        # if 'holiday_state' in holidaychat and holidaychat['holiday_state'] ==
        # "init" and senti != "neg":
        if holidaychat == {}:
            todayholiday = holiday_content[0]
            holidaychat['holiday_idx'] = 0
            holidaychat['holiday_category'] = todayholiday['Category'].split(', ')[0]
            # holiday_des = todayholiday['content'].split('. ')
            holiday_des = re.split(
                r'(?<=[^A-Z].[.?]) +(?=[A-Z])', todayholiday['content'])
            # determine state
            if 'opening' in todayholiday and todayholiday['opening'] != '':
                resp = todayholiday['opening']
                holidaychat['holiday_state'] = "todayholiday_chitchat"
            else:
                if len(holiday_des) > 1:
                    resp = template_topics.utterance(selector='holiday_chat/init',
                                                     slots={'holiday': ' '.join(
                                                         holiday_des[:-1]), 'holidayname': todayholiday['Holiday']},
                                                     user_attributes_ref=self.state_manager.user_attributes)
                    holidaychat['holiday_state'] = "reqmore"
                else:
                    resp = holiday_des[0]
                    holidaychat['holiday_state'] = "transition"
            holidaychat['propose_continue'] = "CONTINUE"
        elif 'holiday_state' in holidaychat and holidaychat['holiday_state'] == "todayholiday_chitchat":
            try:
                holiday_idx = holidaychat['holiday_idx']
            except Exception:
                holiday_idx = 0
            selected_holiday = holiday_content[holiday_idx]

            if 'chitchat' in selected_holiday and selected_holiday['chitchat'] != '':
                resp = selected_holiday['chitchat']
                holidaychat['holiday_state'] = "todayholiday_exit"
            else:
                resp = "Interesting! I'm also wondering wherther you have a favorite holiday."
                holidaychat['holiday_state'] = "self_disclosure"

            holidaychat['propose_continue'] = "CONTINUE"
        elif 'holiday_state' in holidaychat and holidaychat['holiday_state'] == "todayholiday_exit":
            try:
                holiday_idx = holidaychat['holiday_idx']
            except Exception:
                holiday_idx = 0
            selected_holiday = holiday_content[holiday_idx]

            if 'end' in selected_holiday and selected_holiday['end'] != '':
                resp = selected_holiday['end']
                holidaychat['holiday_state'] = "self_disclosure"
            else:
                resp = "Interesting! I'm also wondering wherther you have a favorite holiday."
                holidaychat['holiday_state'] = "self_disclosure"

            # propose stop after one obscure holiday
            try:
                propose_module = self.holiday_topic_module_map[
                    holidaychat['holiday_category']]
                setattr(self.state_manager.user_attributes,
                        "propose_topic", propose_module)
                postfix = self.module_postfix[propose_module]
                resp = resp + postfix
                holidaychat['propose_continue'] = "STOP"
            except Exception:
                holidaychat['propose_continue'] = "STOP"
                postfix = self.module_postfix["MOVIECHAT"]
                resp = resp + postfix
                setattr(self.state_manager.user_attributes,
                        "propose_topic", "MOVIECHAT")
        elif 'holiday_state' in holidaychat and holidaychat['holiday_state'] == "reqmore":
            try:
                holiday_idx = holidaychat['holiday_idx']
            except Exception:
                holiday_idx = 0
            selected_holiday = holiday_content[holiday_idx]
            # content_idx = holidaychat['holiday_content_idx']
            rest_content_ls = re.split(
                r'(?<=[^A-Z].[.?]) +(?=[A-Z])', selected_holiday['content'])[-1:]
            resp = ' '.join(rest_content_ls)  # [content_idx]
            # resp += '. Would you like to know more?'
            resp = template_topics.utterance(selector='holiday_chat/rest_content',
                                             slots={'rest_content': resp},
                                             user_attributes_ref=self.state_manager.user_attributes)
            holidaychat['holiday_state'] = "self_disclosure"
            # holidaychat['propose_continue'] = "CONTINUE"

            # propose stop after one obscure holiday
            try:
                propose_module = self.holiday_topic_module_map[
                    holidaychat['holiday_category']]
                setattr(self.state_manager.user_attributes,
                        "propose_topic", propose_module)
                postfix = self.module_postfix[propose_module]
                resp = resp + postfix
                holidaychat['propose_continue'] = "STOP"
            except Exception:
                holidaychat['propose_continue'] = "STOP"
                postfix = self.module_postfix["MOVIECHAT"]
                resp = resp + postfix
                setattr(self.state_manager.user_attributes,
                        "propose_topic", "MOVIECHAT")
        elif 'holiday_state' in holidaychat and holidaychat['holiday_state'] == "special_holiday" and holidaychat['majorholiday'] is not None:
            try:
                picked_holiday = "_".join(holidaychat['majorholiday']) if re.search(
                    r" ", holidaychat['majorholiday']) else holidaychat['majorholiday']
                if picked_holiday == "gift":
                    person_ls = ['mom', 'dad', 'wife', 'husband', 'son', 'daughter']
                    person_pattern = "(?P<person>" + \
                                        "|".join(person_ls) + ")"
                    person_match = re.search(person_pattern, curr_utt)
                    if person_match is not None:
                        person_gift = person_match.group("person")
                        resp = template_topics.utterance(selector='holiday_chat/gift/%s' % person_gift,
                                                     slots={},
                                                     user_attributes_ref=self.state_manager.user_attributes)
                    else:
                        resp = template_topics.utterance(selector='holiday_chat/gift/general',
                                                     slots={},
                                                     user_attributes_ref=self.state_manager.user_attributes)
                else:
                    resp = template_topics.utterance(selector='holiday_chat/%s' % picked_holiday,
                                                     slots={},
                                                     user_attributes_ref=self.state_manager.user_attributes)
            except Exception:
                resp = 'Nice! ' + template_topics.utterance(selector='holiday_chat/chitchat_err',
                                                            slots={}, user_attributes_ref=self.state_manager.user_attributes)
            prefix = ""
            if senti == "pos":
                prefix = random.choice(["Oh, nice! ", "Ouu, sounds fun! "])
            elif senti == "neu":
                prefix = random.choice(["Okay. ", "Cool. "])

            resp = prefix + resp
            holidaychat['holiday_state'] = "self_disclosure"
            holidaychat['propose_continue'] = "UNCLEAR"
        elif 'holiday_state' in holidaychat and holidaychat['holiday_state'] == "self_disclosure":
            resp = template_topics.utterance(selector='holiday_chat/self_disclosure',
                                             slots={},
                                             user_attributes_ref=self.state_manager.user_attributes)
            try:
                if len(holiday_content) > 1:
                    holidaychat['holiday_idx'] = holidaychat['holiday_idx'] + 1
                    # holidaychat['holiday_content_idx'] = 0
                    holidaychat['holiday_state'] = "newholiday"
                else:
                    holidaychat['holiday_state'] = "transition"
            except Exception:
                holidaychat['holiday_idx'] = 0
                holidaychat['holiday_state'] = "newholiday"

            # propose stop after one obscure holiday
            try:
                propose_module = self.holiday_topic_module_map[
                    holidaychat['holiday_category']]
                setattr(self.state_manager.user_attributes,
                        "propose_topic", propose_module)
                postfix = self.module_postfix[propose_module]
                resp = resp + postfix
                holidaychat['propose_continue'] = "STOP"
            except Exception:
                holidaychat['propose_continue'] = "STOP"
                postfix = self.module_postfix["MOVIECHAT"]
                resp = resp + postfix
                setattr(self.state_manager.user_attributes,
                        "propose_topic", "MOVIECHAT")
        elif 'holiday_state' in holidaychat and holidaychat['holiday_state'] == "newholiday" and senti != "neg":
            try:
                holiday_idx = holidaychat['holiday_idx']
                new_holidaycontent = holiday_content[holiday_idx]
                resp = template_topics.utterance(selector='holiday_chat/newholiday/normal',
                                                 slots={
                                                     'holiday': new_holidaycontent['Holiday']},
                                                 user_attributes_ref=self.state_manager.user_attributes)
                # holidaychat['holiday_idx'] = holidaychat['holiday_idx']  + 1
                # holidaychat['holiday_content_idx'] = 0
                holidaychat['holiday_state'] = "saynew"
                holidaychat['holiday_category'] = new_holidaycontent['Category'].split(', ')[
                    0]
                holidaychat['propose_continue'] = "CONTINUE"
            except Exception:
                resp = template_topics.utterance(selector='holiday_chat/newholiday/except',
                                                 slots={},
                                                 user_attributes_ref=self.state_manager.user_attributes)
                holidaychat['holiday_state'] = "transition"
                holidaychat['propose_continue'] = "UNCLEAR"
        elif 'holiday_state' in holidaychat and holidaychat['holiday_state'] == "saynew" and senti != "neg":
            try:
                holiday_idx = holidaychat['holiday_idx']
            except Exception:
                holiday_idx = 0
            selected_holiday = holiday_content[holiday_idx]
            # content_idx = holidaychat['holiday_content_idx']
            rest_content_ls = re.split(
                r'(?<=[^A-Z].[.?]) +(?=[A-Z])', selected_holiday['content'])[:-1]
            holiday_des = ' '.join(rest_content_ls)
            resp = template_topics.utterance(selector='holiday_chat/saynew',
                                             slots={'holiday': holiday_des},
                                             user_attributes_ref=self.state_manager.user_attributes)
            holidaychat['holiday_state'] = "finishnew"
            # holidaychat['holiday_content_idx'] = holidaychat['holiday_content_idx'] + 1
            holidaychat['propose_continue'] = "CONTINUE"

        elif 'holiday_state' in holidaychat and holidaychat['holiday_state'] == "finishnew":
            try:
                holiday_idx = holidaychat['holiday_idx']
            except Exception:
                holiday_idx = 0
            selected_holiday = holiday_content[holiday_idx]
            # content_idx = holidaychat['holiday_content_idx']
            rest_content_ls = re.split(
                r'(?<=[^A-Z].[.?]) +(?=[A-Z])', selected_holiday['content'])[-1:]
            resp = '. '.join(rest_content_ls)  # [content_idx]
            if len(holiday_content) >= 2 and holiday_idx == 1:
                holidaychat['holiday_idx'] = holidaychat['holiday_idx'] + 1
                holidaychat['holiday_content_idx'] = 0
                holidaychat['holiday_state'] = "newholiday"
                resp = template_topics.utterance(selector='holiday_chat/finishnew',
                                                 slots={'holiday': resp},
                                                 user_attributes_ref=self.state_manager.user_attributes)
            else:
                holidaychat['holiday_state'] = "transition"
            holidaychat['propose_continue'] = "CONTINUE"

        elif'holiday_state' in holidaychat and holidaychat['holiday_state'] == "transition" and senti != "neg":
            holidays_cats = ['Food', 'Environmental', 'Sports', 'Pets', 'Historical',
                             'Family', 'Education']
            selected_cats = random.sample(holidays_cats, 1)
            holidaychat['holiday_themes_proprosed'] = selected_cats
            selected_cat_str = "the Environment" if selected_cats[
                0] == "Environmental" else selected_cats[0]
            selected_cat_str = "History" if selected_cats[
                0] == "Historical" else selected_cat_str
            if re.search(r"family|friend|\bmom\b|\bdad\b|father|mother", curr_utt):
                acknowledgement_str = "ahh, it'a always nice to spend time with your love ones!"
            else:
                acknowledgement_str = "Oh, " + \
                    topic_keyword.replace("your|my", "") + "? "
            resp = template_topics.utterance(selector='holiday_chat/transition',
                                             slots={
                                                 'acknowledgement': acknowledgement_str, 'cat1': selected_cat_str},
                                             user_attributes_ref=self.state_manager.user_attributes)
            holidaychat['holiday_state'] = "recommend"
            holidaychat['propose_continue'] = "CONTINUE"
        elif 'holiday_state' in holidaychat and holidaychat['holiday_state'] == "recommend" and senti != "neg":
            selected_theme = random.choice(
                self.state_manager.user_attributes.holidaychat['holiday_themes_proprosed']).capitalize()
            future_holiday = utils.get_all_holiday(
                category=selected_theme, limit=300)
            if 'holiday_themes_proprosed' in holidaychat and senti != "neg" and len(future_holiday) != 0:
                random_day = random.choice(range(len(future_holiday)))
                holiday_name_proprosed = future_holiday[random_day]['Holiday']
                holidaychat['holiday_name_proprosed'] = holiday_name_proprosed
                holiday_date = future_holiday[random_day]['Date']
                resp = template_topics.utterance(selector='holiday_chat/recommend',
                                                 slots={
                                                     'holidayname': holiday_name_proprosed, 'date': holiday_date},
                                                 user_attributes_ref=self.state_manager.user_attributes)
                holidaychat['holiday_state'] = "explain_recommend"
                holidaychat['propose_continue'] = "CONTINUE"
            else:
                resp = "Oh, do you know I'm also a time machine. Do you like to know what happened in history today?"
                holidaychat['holiday_state'] = "chitchat"
                holidaychat['propose_continue'] = "CONTINUE"
        elif 'holiday_state' in holidaychat and holidaychat['holiday_state'] == "explain_recommend":
            proposed_holiday = utils.scan_related_holiday(
                holidaychat['holiday_name_proprosed'], 70)
            resp = '. '.join(
                re.split(r'(?<=[^A-Z].[.?]) +(?=[A-Z])', proposed_holiday['content'])[:3])
            holidaychat['holiday_state'] = "ask_todayhistoryfact"
            holidaychat['propose_continue'] = "CONTINUE"
        elif 'holiday_state' in holidaychat and holidaychat['holiday_state'] == "ask_todayhistoryfact":
            resp = "Let me tell you a secret. I'm also a time machine. Do you want to know what happened in history today?"
            holidaychat['holiday_state'] = "chitchat"
            holidaychat['propose_continue'] = "CONTINUE"
        elif 'holiday_state' in holidaychat and holidaychat['holiday_state'] == "chitchat":
            if re.search(r"today", curr_utt) or senti != "neg":
                resp = "<say-as interpret-as=\"interjection\">Ooh! </say-as> Did you known " + \
                    self.EVI_bot("what happened in history today") + "?"
            else:
                resp = self.EVI_bot(curr_utt)

            if resp is None or resp == "" or re.search("don't know|response", resp):
                resp = template_topics.utterance(selector='holiday_chat/chitchat_err',
                                                 slots={},
                                                 user_attributes_ref=self.state_manager.user_attributes)
            holidaychat['holiday_state'] = "end"
            holidaychat['propose_continue'] = "UNCLEAR"
        else:
            if re.search(r"\bmore\b|\bkeep\b|another|i want to", curr_utt):
                holidaychat['holiday_state'] = "transition"
                resp = template_topics.utterance(selector='holiday_chat/reqmore',
                                                 slots={},
                                                 user_attributes_ref=self.state_manager.user_attributes)
                holidaychat['propose_continue'] = "CONTINUE"

            else:
                holidaychat['holiday_state'] = "exit"
                resp = template_topics.utterance(selector='holiday_chat/exit',
                                                 slots={},
                                                 user_attributes_ref=self.state_manager.user_attributes)
                setattr(self.state_manager.user_attributes,
                        "propose_topic", "TRAVELCHAT")
                holidaychat['propose_continue'] = "STOP"

        self.state_manager.user_attributes.holidaychat = holidaychat
        return resp
