import logging

from cobot_core.service_module import LocalServiceModule
from response_generator.special.special_response_generator import template_sys


class ReqPlayGame_ResponseGenerator(LocalServiceModule):
    """req_playgame: a user asks to play a game"""

    def execute(self):
        logging.info("[REQGAME] enter play game")
        setattr(self.state_manager.user_attributes,
                "propose_topic", "GAMECHAT")
        setattr(self.state_manager.current_state, "resp_type", "req_task")
        return template_sys.utterance(selector='play_game', slots={},
                                      user_attributes_ref=self.state_manager.user_attributes)