import re

import nlu.util_nlu
import utils
from cobot_core.service_module import LocalServiceModule
from response_generator.special.tellstory_funfact.tell_storyfunfact_handler import tellStoryFactMemeIntentHandler
from backstory_client import get_backstory_response_text

class TellStoryFunfact_ResponseGenerator(LocalServiceModule):
    """say_funny: a user wants to make fun of the chatbot - use terry script"""

    def execute(self):
        # curr_utt = self.input_data['text']
        # last_custom_intent = self.state_manager.last_state.get('last_custom_intents', None) \
        #     if self.state_manager.last_state is not None else None
        # if last_custom_intent is not None:
        #     last_sys_intent = last_custom_intent['sys']
        # else:
        #     last_sys_intent = None

        # if not re.search(r"fun fact[s] about", curr_utt) and last_sys_intent is not None
        #     and 'req_topic' not in last_sys_intent:
        utt = self.input_data['text'].lower()

        backstory_response_text = get_backstory_response_text(utt, self.input_data['a_b_test'], 0.85)
        if backstory_response_text:
            return backstory_response_text

        if re.search(r"story about ", utt):
            resp = "Oh, I think you want a story about " + re.sub(r".*story about ", "", utt) +\
                   ". <break time='600ms'/>Sorry, but I don't know any. Can I tell you a different story or joke?"
            return resp

        sys_intents = nlu.util_nlu.get_feature_by_key(
            self.state_manager.current_state, 'intent_classify')['sys']
        last_content_key = self.state_manager.last_state.get(
            'funfact_content_key', None)
        tellfunfactHandler = tellStoryFactMemeIntentHandler(
            self.input_data, last_content_key, sys_intents)
        response, content_key = tellfunfactHandler.generateResponse()
        self.state_manager.current_state.funfact_content_key = content_key
        return response