import re

from cobot_core.service_module import LocalServiceModule
from response_generator.special.special_response_generator import logger, template_sys


class AdjustSpeak_ResponseGenerator(LocalServiceModule):
    """ Special Intents to pass Amazon's test """

    def execute(self):
        usr_utterance = self.input_data['text'].lower()
        logger.info(
            '[AdjustSpeakRG] execute adjust speak with utterance {}'.format(usr_utterance))
        user_utterance = self.input_data['text']
        key = ['adjust_speak']
        if re.search(
                r"talk.* too much|talk too.* much|leave me alone|too much information|too many question|"
                r"you always say|you.* too loud|you keep saying|cut me off|you.* noisy|zip.* mouth",
                user_utterance
        ):
            key.append('too_much')
        elif re.search(
                r'hold on|pause|take a break|be right back|wait.* minute|wait.* second|hold.* second|pause.* second',
                user_utterance
        ):
            key.append('pause')
        elif re.search(
                r"you.* \bfast\b|speak slower|talk slow|slower|slowly|talk.* soft|speak.* soft",
                user_utterance
        ):
            key.append('too_fast')
        elif re.search(
                r"talk.* loud|louder",
                user_utterance
        ):
            key.append('louder')
        elif re.search(
                r"bad grammar|grammar sucks|poor grammar",
                user_utterance
        ):
            key.append('bad_grammar')
        else:
            key.append('default')
        setattr(self.state_manager.current_state, "resp_type", "adjust_speak")
        utt = template_sys.utterance(selector=key, slots={},
                                     user_attributes_ref=self.state_manager.user_attributes)
        return utt