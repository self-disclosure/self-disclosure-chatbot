import re

from cobot_core.service_module import LocalServiceModule
from response_generator.special.special_response_generator import template_sys
from nlu.util_nlu import get_feature_by_key

class ReqTask_ResponseGenerator(LocalServiceModule):
    """req_task: a user asks to play music"""

    def execute(self):
        setattr(self.state_manager.current_state, "resp_type", "req_task")
        usr_utterance = self.input_data['text'].lower()
        central_elem = get_feature_by_key(self.state_manager.current_state, 'central_elem')
        sys_intent = get_feature_by_key(
            self.state_manager.current_state, 'intent_classify')['sys']
        key = ['request_task']

        last_state = self.state_manager.last_state
        if last_state and last_state.get('resp_type', '') == 'req_task':
            key.append('suggest_exit')
            return template_sys.utterance(selector=key, slots={},
                                          user_attributes_ref=self.state_manager.user_attributes)

        if (central_elem['DA'] == "closing" and float(central_elem['DA_score']) > 0.9) or 'terminate' in sys_intent:
            return "Okay, Let me know if anything else you'd like to chat about! You can simply say Stop to exit social mode!"

        if re.search(r"^order|ordr \bme\b|shopping", usr_utterance):
            key.append('order')
            return template_sys.utterance(selector=key, slots={},
                                          user_attributes_ref=self.state_manager.user_attributes)
        elif re.search(r"pause", usr_utterance):
            return "Sure. I will be waiting for you!"
        elif re.search(r"turn yourself off|i don't want to talk|turn off social", usr_utterance):
            return "Got you, Let me know if anything else you'd like to chat about! You say stop to exit the social mode!"
        elif re.search(
                r"^pair (?!of)|bluetooth|turn.* on|turn.* off|turn.* up|connect|message|switch off|volume",
                usr_utterance
        ):
            key.append('device_control')
            return template_sys.utterance(selector=key, slots={},
                                          user_attributes_ref=self.state_manager.user_attributes)
        elif re.search(r"timer", usr_utterance):
            key.append('timer')
            return template_sys.utterance(selector=key, slots={},
                                          user_attributes_ref=self.state_manager.user_attributes)
        elif re.search(
                r"alarm|notification|to do list|reminder|text|email|my calendar|shopping list|grocery list",
                usr_utterance
        ):
            key.append('assistant')
            return template_sys.utterance(selector=key, slots={},
                                          user_attributes_ref=self.state_manager.user_attributes)
        # TODO: add room temperature
        elif re.search(
            r"room temperature|thermostat",
            usr_utterance
        ):
            key.append('homekit')
            return template_sys.utterance(selector=key, slots={},
                                          user_attributes_ref=self.state_manager.user_attributes)
        else:
            key.append('default')
            return template_sys.utterance(selector=key, slots={},
                                          user_attributes_ref=self.state_manager.user_attributes)