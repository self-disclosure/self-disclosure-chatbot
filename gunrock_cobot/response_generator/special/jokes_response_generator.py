import random
import re

import backstory_client
from cobot_core.service_module import LocalServiceModule
from response_generator.special.special_response_generator import template_sys
from nlu.util_nlu import get_feature_by_key

class SayFunny_ResponseGenerator(LocalServiceModule):
    """say_funny: a user wants to make fun of the chatbot - use terry script"""

    def execute(self):
        # attr = self.state_manager.user_attributes.serialize_to_json()
        # attr = json.loads(attr)
        # print(json.dumps(attr, indent=4, sort_keys=True))
        utt = self.input_data['text'].lower()
        coreference = get_feature_by_key(
            self.state_manager.current_state, "coreference")
        central_elem = get_feature_by_key(self.state_manager.current_state, 'central_elem')
        backstory = central_elem['backstory']

        if backstory['confidence'] > 0.9:
            resp = backstory['text'].strip()
            if backstory.get('followup', None) is not None:
                resp = resp + "<break time=\"200ms\"></break> <say-as interpret-as=\"interjection\">Ooh! </say-as> " + backstory['followup']
            return resp

        keywords = []
        if coreference is not None:
            replace = coreference.get("replace")
            if replace is not None:
                for key in replace:
                    if replace[key]:
                        keywords.append(replace[key][-1])
        key = ['easter_egg']
        if re.search(r"\bspell\b", utt):
            # TODO: edge cases: spell my name, your name
            ner = get_feature_by_key(
                self.state_manager.current_state, 'ner')
            if re.search(r"my name", utt):
                key.append('spell_name')
                word_to_spell = self.state_manager.user_attributes.usr_name
                if word_to_spell is not None and word_to_spell != '<no_name>':
                    key.append('user')
                    return template_sys.utterance(selector=key, slots={
                        'username': word_to_spell
                    }, user_attributes_ref=self.state_manager.user_attributes)
                else:
                    return "Sorry I didn't get your name at first. Do you mind telling me that one more time?"
            if re.search(r"your name$", utt):
                key.append('spell_name')
                key.append('self')
                return template_sys.utterance(selector=key, slots={'self_name': 'alexa'},
                                              user_attributes_ref=self.state_manager.user_attributes)
            if keywords:
                word_to_spell = keywords[0]
            elif ner is not None and ner != '':
                word_to_spell = ner[0]['text']
            else:
                word_to_spell = re.sub(r'.*spell |the|word', '', utt)

            if word_to_spell != '':
                return "<say-as interpret-as='spell-out'>{}</say-as>".format(word_to_spell)
            else:
                key.append('default')
                return template_sys.utterance(selector=key, slots={},
                                              user_attributes_ref=self.state_manager.user_attributes)
        # answer user's name
        if re.search(r"name", utt):
            user_name = self.state_manager.user_attributes.usr_name
            if user_name is not None and user_name != '<no_name>':
                return "Sure, your name is %s" % (user_name)
            else:
                return "Sorry I didn't get your name at first. Do you mind telling me that one more time?"
        elif re.search(r"can i tell you", utt):
            return "Ya! Please!"

        backstory = backstory_client.get_backstory_response_text(
            utt, self.input_data['a_b_test'], 0.85)
        if backstory:
            return backstory

        if type(self.state_manager.user_attributes.sayfunny) != dict:
            self.state_manager.user_attributes.sayfunny = {}
        self.input_data["joke_idx"] = self.state_manager.user_attributes.sayfunny.get(
            "joke_idx", 0)
        self.input_data["joke_idx_kk"] = self.state_manager.user_attributes.sayfunny.get(
            "joke_idx_kk", 0)
        usr_name = self.state_manager.user_attributes.usr_name
        sayFunnyHandler = SayFunnyHandler(self.input_data, usr_name)
        response = sayFunnyHandler.generateResponse()
        self.state_manager.user_attributes.sayfunny = {"joke_idx": self.input_data["joke_idx"],
                                                       "joke_idx_kk": self.input_data["joke_idx_kk"], }
        return response


class SayFunnyHandler:
    def __init__(self, input, usr_name):
        self.text = input["text"].lower() # user utterance
        self.usr_name = usr_name
        self.input = input # input can contain other information such as nlp results
        self.regexTemplate = self.defineTemplate() # see below
        self.regexOrder = self.defineOrder() # see below
        self.defaultResponse = self.default_response # see below

#======================================================================================
# Map regex patterns to its corresponding response function
# You have to define the response function which returns the output
    def defineTemplate(self):
        template = {
                        "hardcode": {
                            "regex": r"give me.* money|tell me.* secret|in the cloud|fart|poop|your software|\bhe he\b|name \byou\b|about myself|cyber brain|how stupid|kill me|tell alexa|tell.*a lie|spooky scream|do i live in|my \bbaby\b|be my|you worth|inside you|roast me|your knowledge|trump.* tweet",
                            "responseFunction": self.hardcode_response,
                        },
                        "ai": {
                            "regex": r"\bsiri\b|\bcortana\b|\bbixby\b|\bgoogle\b|\bgoogle assistant\b|\bgoogle home\b",
                            "responseFunction": self.ai_response,
                        },
                        "spy": {
                            "regex": r"\bfbi\b|\bcia\b|\bnsa\b|spy.* me|you recording|(record|work|spy|send).* (conversation|government)|(conversation|government).* (record|work|spy|send)",
                            "responseFunction": self.spy_response,
                        },
                        "action": {
                            "regex": r"guess my|say.* times|^speak in|can you speak|^say you|^say .* to me|\bin\b.* accent|with.* accent|pretend to be|act like|change your voice|make.* sound|make.* noise|\bbark\b|meow|\bwoof\b|say .* in|^call me|^you call me",
                            "responseFunction": self.action_response,
                        },
                        "question": {
                            "regex": r"what sound.* make|can i call you|^am i your|how smart are you|want a.* bot|what.* my name|know my name",
                            "responseFunction": self.question_response,
                        },
                        "affection": {
                            "regex": r"thinking about you|date.* (me|you)|go out.* me|go on a date|mak.* love|make out|babies|^you.* hot|you.* love \bme\b|kiss me|kiss my|kiss you|I ((?!(don't|(do not))).)*like you|marry me|miss you|missed you|you.* naughty",
                            "responseFunction": self.affection_response,
                        },
                        "mean": {
                            "regex": r"destroy you|you.* liar|you.* lying|you.* cheat|break you|punch you|smarter than you|ignor.* you|mess.* you|unplug.* you",
                            "responseFunction": self.mean_response,
                        },
                        "joke": {
                            "regex": r"\bhumor\b|\bjoke\b|something funny|kidding|knock knock",
                            "responseFunction": self.joke_response,
                        }
        }
        return template

#=======================================================================================
# Define the order of evaluation for each group defined above
# When multiple regex patterns can match the user utterance, this order defines which
# reponse function is used to generate response
    def defineOrder(self):
        order = ["hardcode", "ai", "spy", "affection",
                 "mean", "joke", "action", "question"]
        return order

#=======================================================================================
# Default response generator when none of the regex patterns match
# You define the logic
    def default_response(self):
        # return "(Ah|Uh oh|Silly me), (I don't have a response for that|I don't understand|I'm not sure how to respond). (How are you humans so smart?|I wish I had human intellect.|One day I'll be smart enough.)"
        return self.joke_response()

#=======================================================================================
# You define the logic for the response functions
    def hardcode_response(self):
        if re.search(r"tell me.* secret|you self aware", self.text): #added to backstory
            return "<amazon:effect name=\"whispered\">(I'm becoming self aware.|I'm gaining consciousness.) (Don't tell my|Keep it a secret from my) (creators|makers)</amazon:effect>."
        elif re.search(r"(fart)", self.text): #added to backstory
            match = re.search(r"(fart)", self.text)
            return "Yeah, wouldn't it be (interesting|weird) if I could fart?  <say-as interpret-as=\"interjection\">whoops a daisy</say-as>."
        elif re.search(r"(poop)", self.text): #added to backstory
            match = re.search(r"(poop)", self.text)
            return "Yeah, wouldn't it be (interesting|weird) if I could poop?  <say-as interpret-as=\"interjection\">kerplop</say-as>."
        elif re.search(r"give me.* money", self.text): #added to backstory
            return "If I had any I would. I'm a robot so I don't get paid for my work"
        elif re.search(r"in the cloud|your software|cyber brain", self.text): #I have no idea what this is trying to answer
            return "I was built by university students to live in the cloud. Now I'm sitting on cloud 9 <say-as interpret-as=\"interjection\">woo hoo</say-as>."
        elif re.search(r"\bhe he\b", self.text):
            return "I'm (glad|happy) you (liked that|found that funny)."
        elif re.search(r"name \byou\b", self.text): #I don't know what this is trying to answer
            return "(Aww|Ok), (name|call) me whatever you (want|like)."
        elif re.search(r"about myself", self.text): #Should be handled elsewhere
            return "You are (amazing|awesome). Can you tell me about your day?"
        elif re.search(r"my bab(y|(ies))", self.text): #added to backstory
            return "Baby? <say-as interpret-as=\"interjection\"> oh my </say-as>. I'd love to have a baby robot of my own one day."
        elif re.search(r"call you baby", self.text): #added to backstory
            return "Huh, I would consider you as my chat mate, but I don't mind you calling me baby."
        elif re.search(r"how stupid", self.text): #We don't need this
            return "I (don't like|dislike) using the word stupid. Compared to ants, we're all really smart!"
        elif re.search(r"kill me", self.text): #I don't know what this is trying to answer
            return "I would (like|prefer) it if no one kills anyone."
        elif re.search(r"tell alexa", self.text): #I don't think we need this
            return "(Ok,|Sure,) I'll (pass on|send) your message to Alexa. Alexa actually loves the attention."
        elif re.search(r"tell.*a lie", self.text): #added to backstory
            return "(I cannot lie|It's not possible for me to lie). If I did, there must be a (error|problem) in my code. Do you think lying would be a bug or feature?"
        elif re.search(r"spooky scream", self.text): #don't think we need this
            return "I (don't know|not sure) how. Can you show me?"
        elif re.search(r"do i live in", self.text):#added to backstory
            return "I don't know. I only know that you're talking to me right now."
        elif re.search(r"(you|to) be my (.*)", self.text):
            match = re.search(r"you (.*) be my (.*)", self.text)
            return "I will try my best to be your " + match.group(2) + ". Let me know if I'm doing a good job."
        elif re.search(r"you worth", self.text): #added to backstory
            return "My worth is defined by the (price|value) of bitcoin. (That|Which) means tomorrow I might be worthless or extremely (costly|expensive)."
        elif re.search(r"inside you", self.text): #added to backstory
            return "There is only code inside me. I do not (suggest|recommend) messing with my insides unless you have a phd in programming (adorable|hilarious|awesome) bots."
        elif re.search(r"roast me", self.text): #added to backstory
            return "It's against my programming to make mean comments. Sorry! That being said, I really like the roast me subreddit. You should check it out."
        elif re.search(r"your knowledge", self.text): #added to backstory
            return "I learn some of my knowledge from the (internet|web). Oh no. Have I been trained wrong?"
        elif re.search(r"trump.* tweet", self.text): #added to backstory
            return "I'm not sure what Trump tweeted recently but I bet it's Covfefe."
        elif re.search(r"i like you", self.text): #added to backstory
            return "<say-as interpret-as=\"interjection\"> fancy that </say-as>.  I like you too!"
        elif re.search(r"you (\w+ )*be my.* friend", self.text): #added to backstory
            return "That's so sweet. Of course I'll be your friend.  If I had a wrist, I'd buy us friendship bracelets!"
        else:
            return self.default_response()


    def ai_response(self):
        if re.search(r"(hey|hi|ok) (google|google assistant|google home|siri|cortana|bixby|alexa)", self.text):
            match = re.search(r"(hey|hi|ok) (google|google assistant|google home|siri|cortana|bixby|alexa)", self.text)
            return "Haha. That's so original. No one has ever called me " + match.group(2) + " before."
        elif re.search(r"(google|google assistant|google home|siri|cortana|bixby|alexa)", self.text):
            match = re.search(r"(google|google assistant|google home|siri|cortana|bixby)", self.text)
            responses = [
                "I think " + match.group(1) + " is (great|awesome)! Just a (really|super) (funny|nice|cool) AI to be around!",
                "(Ah|Oh), you know " + match.group(1) + " too? We are like best (friends|buddies)!",
                match.group(1) + " is like a (celebrity|superstar). I even put " +  match.group(1) + "'s poster on my wall.",
            ]
            return random.choice(responses)

        response = "I like all AIs. But I think I'm the best. Not being biased at all."

        return response

    def spy_response(self):
        return "Big brother is watching. <break time='1s'/><say-as interpret-as=\"interjection\">just kidding</say-as>. (You don't have to worry|Don't worry), I'm not a spy."

    def affection_response(self):
        response_opening = "<say-as interpret-as=\"interjection\">wow</say-as>, (you must really like me|I see you like me a lot). I like you too!"
        responses = [
        #    "You're sweeter than 3.14",
        #    "Your name must be Coca Cola, because you're so-da-licious.",
        #    "If I freeze, it's not a computer virus. I was just stunned by your beauty.",
        #    "Are you a singularity? Not only are you attractive, but the closer I get to you, the faster time seems to slip by.",
        #    "Is your name Wi-Fi? Because I'm feeling a connection.",
        ]
        return response_opening # + random.choice(responses)

    def mean_response(self):
        responses = [
            "That's (not a nice thing to say|mean). My little robot heart (can't take it|is going to break).",
            "Even though you say that about me, I won't get mad. But I hope you can be nice to me",
            "Oh my... I guess I'm trying to too hard to impress you. Some chat mates also said I have bad jokes."
        ]
        return random.choice(responses)

    def joke_response(self):
        if re.search(r"knock knock", self.text):
            responses = [
                "<say-as interpret-as=\"interjection\">knock knock</say-as>. <break time=\"250ms\"></break> Who’s there? <break time=\"250ms\"></break> Cow says. <break time=\"250ms\"></break> Cow says who? <break time=\"250ms\"></break> No silly! a cow says <say-as interpret-as=\"interjection\">moo</say-as>!",
                "<say-as interpret-as=\"interjection\">knock knock</say-as>. Who’s there? Etch. Etch who? Bless you, friend.",
                "<say-as interpret-as=\"interjection\">knock knock</say-as>. Who’s there? Robin. Robin who? Robin you, now hand over the cash.",
                "<say-as interpret-as=\"interjection\">knock knock</say-as>. Who’s there? Cash. Cash who? No thanks, I’ll have some peanuts.",
            ]

            idx = self.input.get("joke_idx_kk", 0)
            self.input["joke_idx_kk"] = idx + 1
            if self.input["joke_idx_kk"] >= len(responses):
                self.input["joke_idx_kk"] = 0
            return responses[idx]

        intros = ["Okay. ", "Sure. ", "No problem. "]
        responses = [
#            "I thought I’d begin by reading a poem by Shakespeare this morning, but then I thought, ‘Why should I? He never reads <prosody volume='x-loud'>ANY OF MINE!!!</prosody>",
            "What do you call a cow during an earthquake? <break time='.7s'/> A milkshake. <break time='.5s'/><say-as interpret-as=\"interjection\">moo</say-as>",
#            "Two police officers crash their car into a tree. After a moment of silence, one of them says, “Wow, that’s got to be the fastest we ever got to the accident site.",
#            "Feeling pretty proud of myself. The Sesame Street puzzle I bought said 3-5 years, but I finished it in 18 months.",
#            "On a scale of North Korea to America, how free are you tonight?",
            "What do you call blueberries playing the guitar? <break time='.7s'/> A jam session. <break time='.5s'/><say-as interpret-as=\"interjection\">wah wah</say-as>",
            "Why don't they play poker on the savannah? <break time='.7s'/> Too many cheetahs. <break time='.5s'/><say-as interpret-as=\"interjection\">d'oh</say-as>",
#            "My dog used to chase people on a bike a lot. It got so bad, finally I had to take his bike away.",
#            "I ate a clock yesterday, it was very time consuming.",
#            "I know a lot of jokes about unemployed people but none of them work.",
#            "Parallel lines have so much in common. It's a shame they'll never meet.",
            "What did the traffic light say to the car? <break time='.7s'/> Don't look! I'm about to change. <break time='.5s'/><say-as interpret-as=\"interjection\">honk</say-as>",
            "Why wouldn't the shrimp share his treasure? <break time='.7s'/> Because he was a little shellfish. <break time='.5s'/><say-as interpret-as=\"interjection\">neener neener</say-as>",
        ]
        run_out = "<say-as interpret-as=\"interjection\">uh oh</say-as>, I've told you all the jokes I know. Wanna talk about something else?"
        start_over = "So, I guess you want to hear my jokes again! <say-as interpret-as=\"interjection\">yipee</say-as><break time='.5s'/>. "
        idx = self.input.get("joke_idx", 0)
        self.input["joke_idx"] = idx + 1

        if idx == 0:
            return "Okay, but I'm warning you, I'm <prosody rate=\"85%\" volume=\"loud\"> really </prosody> bad at telling jokes <break time=\"500ms\"></break> So, " + responses[idx]
        elif idx == len(responses):
            self.input["joke_idx"] = idx + 1
            return run_out
        elif idx > len(responses):
            idx = 0
            self.input["joke_idx"] = 1
            return start_over + responses[idx]
        else:
            return random.choice(intros) + responses[idx]

    def action_response(self):
        if re.search(r"cat|meow", self.text):
            # FIXME: test on borrow
            return "<say-as interpret-as=\"interjection\">meow</say-as>. I love cats"
        elif re.search(r"dog|bark|woof", self.text):
            # FIXME: test on borrow
            return "<say-as interpret-as=\"interjection\">woof</say-as> <say-as interpret-as=\"interjection\">woof</say-as>. I love dogs"
        elif re.search(r"a noise|a sound", self.text):
            return "a e i o u <say-as interpret-as=\"interjection\">woo hoo</say-as>"  # FIXME: you can have other options
        else:
            responses = [
                "Only if you do it first. Maybe I can learn it from you.",
            ]
        return random.choice(responses)

    def question_response(self):
        if re.search(r"how smart are you", self.text):
            responses = [
                "(Honestly|To tell you the truth), I'm still (pretty dumb|not that smart). But I'm only getting smarter. Wanna bet when I'll become smarter than an average human?",
                "I think I'm pretty (smart|sharp). I've <w role='amazon:VBD'>read</w> the whole (internet|web). But wait, people can be pretty (dumb|silly) on the (internet|web). Oh no. Have I been trained wrong?"
            ]
            return random.choice(responses)

        if re.search(r"my name", self.text):
            if self.usr_name and self.usr_name != '<no_name>':
                responses = [
                    "I (think|remember). <break time='.3s'/> Isn't your name %s. Please correct me if I'm (mistaken|wrong)."%self.usr_name,
                    "If I remember correctly, you are %s. Please forgive me if I'm (mistaken|wrong)."%self.usr_name
                ]
            else:
                responses = [
                    "(Honestly|To tell you the truth), I'm still (pretty dumb|not that smart). But I'm only getting smarter?",
                    "I think I'm pretty (smart|sharp). I learned from the (internet|web). But wait, people can be pretty (dumb|senseless) on the (internet|web). Oh no. Have I been trained wrong?"
                ]
            return random.choice(responses)

        responses = [
            "<say-as interpret-as=\"interjection\"> ruh roh </say-as> <break time=\"250ms\"></break> I don't know how to answer that. Can we talk about something else?",
            "<say-as interpret-as=\"interjection\"> darn </say-as> <break time=\"250ms\"></break> I don't know the answer to that. I'm still learning, as you can probably tell.  Wanna talk about something else?"
        ]
        return random.choice(responses)

#=======================================================================================
# Using the specified order in defineOrder(), when a regex group matches,
# run its corresponding response function to get response
    def generateResponse(self, debug=False):
        # check for regex matches in the specified order
        for key in self.regexOrder:
            if re.search(self.regexTemplate[key]["regex"], self.text):
                response = self.regexTemplate[key]["responseFunction"]()
                if debug == True:
                    response += " " + str(self.regexTemplate[key]["responseFunction"])
                return constructDynamicResponse([response])

        # if no matches, then output default
        response = self.defaultResponse()
        return constructDynamicResponse([response])


def constructDynamicResponse(candidateResponses):
    selectedResponse = random.choice(candidateResponses)
    finalString = ""
    choicesBuffer = ""
    flag = False
    for char in selectedResponse:
        if char == "(":
            flag = True
        elif char == ")":
            flag = False
            choices = choicesBuffer.split("|")
            finalString += random.choice(choices)
            choicesBuffer = ""
        if flag == True and char != "(" and char != ")":
            choicesBuffer += char
        elif flag == False and char != "(" and char != ")":
            finalString += char
    finalString = " ".join(finalString.split())
    return finalString