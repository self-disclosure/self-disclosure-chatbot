import utils
from cobot_core.service_module import LocalServiceModule
from response_generator.special.controversial_opinion.controversial_opinion_handler import ControversialOpinionHandler
from backstory_client import get_backstory_response_text

class ControversialOpinion_ResponseGenerator(LocalServiceModule):
    """topic_controversial: a user wants to ask opinions on controversial topics"""

    def execute(self):
        utt = self.input_data['text'].lower()

        backstory_text = get_backstory_response_text(utt, self.input_data['a_b_test'], 0.85)

        if backstory_text:
            return backstory_text

        sayControOpionionHandler = ControversialOpinionHandler(
            self.input_data, self.state_manager)
        response = sayControOpionionHandler.generateResponse()
        return response