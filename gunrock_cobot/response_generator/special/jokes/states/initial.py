from response_generator.fsm2 import Dispatcher, Tracker, NextState
from .base import JokesState

import re
import logging
from nlu.constants import DialogAct as DialogActEnum
from nlu.intent_classifier import IntentClassifier
#"regex": r"\bhumor\b|\bjoke\b|something funny|kidding|knock knock" enter condition

logger = logging.getLogger("jokes.initial")

class Initial(JokesState):

    name = 'initial'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        knock_knock_regex = r"knock knock"
        knock_knock_check = re.compile(knock_knock_regex)

        logger.debug("[JOKES] entered initial")

        #TODO: figure out states for knock knock jokes
        if knock_knock_check.search(tracker.input_text) is not None:
            logger.debug("[JOKES] entered knock knock")
            dispatcher.respond_template('jokes/knock_knock', {})
            dispatcher.propose_continue('STOP')
            return [NextState('initial')]
        #regular joke
        else:
            dispatcher.respond_template('jokes/start', {})
            dispatcher.respond_template('jokes/general', {})
            tracker.one_turn_store['joke_limit'] = 1
            dispatcher.propose_continue('CONTINUE')
            return [NextState('want_joke')]
