from cobot_core.service_module import LocalServiceModule

from response_generator.fsm2 import FSMModule, FSMAttributeAdaptor
from template_manager import Template

from .states import JokesState


class JokesResponseGenerator(LocalServiceModule):

    def execute(self):
        module = FSMModule(
            FSMAttributeAdaptor('sayfunny', self.state_manager.user_attributes),
            self.input_data,
            Template.jokes,
            JokesState,
            first_state='initial',
            state_manager_last_state=self.state_manager.last_state
        )
        return module.generate_response()
