import re
import random
import signal
import logging

import nlu.util_nlu
from cobot_core.service_module import LocalServiceModule
from cobot_common.service_client import get_client

import utils
import template_manager

logger = logging.getLogger(__name__)

template_sys = template_manager.Templates.sys_rg
template_social = template_manager.Templates.social


class SayThanks_ResponseGenerator(LocalServiceModule):
    """ Special Intents to pass Amazon's test """

    def execute(self):
        user_utterance = self.input_data['text'].lower()
        key = ['thanks']
        if re.search(r"thank|appreciate|good job|great job", user_utterance):
            key.append('good_job')
        elif re.search(r"that.* funny|impressive|^you.* intelligent|make.*me laugh", user_utterance):
            key.append('impressive')
        elif re.search(r"blow.* my mind|proud of you", user_utterance):
            key.append('proud')
        else:
            key.append('default')
        return template_sys.utterance(selector=key, slots={},
                                      user_attributes_ref=self.state_manager.user_attributes)


class SayBio_ResponseGenerator(LocalServiceModule):
    """say_bio: a user wants to know the basic functionalities of the chatbot for opener topics."""

    def execute(self):
        custom_intents = nlu.util_nlu.get_feature_by_key(
            self.state_manager.current_state, "intent_classify")
        senti = nlu.util_nlu.get_sentiment(
            self.state_manager.current_state, custom_intents['lexical'])

        key = ['bio']
        first_resp = ''

        if senti in ["pos", "neu", "unknown"]:
            key.append('positive')
            return template_sys.utterance(selector=key, slots={'clause': first_resp},
                                          user_attributes_ref=self.state_manager.user_attributes)

        elif senti == "neg":
            key.append('negative')
        else:
            key.append('default')
        return template_sys.utterance(selector=key, slots={'clause': first_resp},
                                      user_attributes_ref=self.state_manager.user_attributes)


class ReqTime_ResponseGenerator(LocalServiceModule):
    """req_time: a user asks for time"""

    def EVI_bot(self, utterance):  # TODO: get EVI response
        try:
            client = get_client(
                api_key='xgTjk23SRM9Q9VFrUEFOjav0Bn4ot21W8uFLEieT')
            r = client.get_answer(question=utterance, timeout_in_millis=1000)
            if r["response"] == "" or re.search(r"skill://", r["response"]):
                return None
            if re.search(r"an opinion on that|tough to explain|Sorry|usually defined as|didn't catch", r["response"]):
                return "Speaking of time, how do you spend your free time?"
            return r["response"]
        except Exception as e:
            logging.info("[REQTIME_MODULE] EVI_bot error : {}".format(e))
            return None

    def execute(self):
        setattr(self.state_manager.current_state, "resp_type", "req_task")
        location = self.state_manager.user_attributes.location
        curr_utt = self.input_data['text'].lower()
        central_elem = nlu.util_nlu.get_feature_by_key(self.state_manager.current_state, 'central_elem')
        intent_classify = nlu.util_nlu.get_feature_by_key(self.state_manager.current_state, 'intent_classify')
        nlpkg = nlu.util_nlu.get_feature_by_key(self.state_manager.current_state, 'knowledge')
        dialog_act = central_elem['DA']
        backstory_dict = central_elem['backstory']
        dialog_act_score = float(central_elem['DA_score'])
        timechat = self.state_manager.user_attributes.timechat
        if timechat is None or 'time_state' not in timechat:
            timechat = {}

        if backstory_dict['confidence'] > 0.85:
            timechat['propose_continue'] = "STOP"
            self.state_manager.user_attributes.timechat = timechat
            resp = backstory_dict['text']
            if backstory_dict.get('followup', None) is not None:
                resp = resp + "<break time=\"200ms\"></break> <say-as interpret-as=\"interjection\">Ooh! </say-as> " + backstory_dict['followup']
            return resp
        elif (dialog_act == "complaint" and dialog_act_score > 0.9) or "complaint" in intent_classify['sys']:
            timechat['propose_continue'] = "STOP"
            self.state_manager.user_attributes.timechat = timechat
            resp = "Sometime the connection is not stable on my side. You can repeat the request again if you will!"
            return resp
        elif dialog_act == "back-channeling" and dialog_act_score > 0.9:
            timechat['propose_continue'] = "STOP"
            self.state_manager.user_attributes.timechat = timechat
            resp = "All righty, do you have any other questions for me?"
            return resp

        if timechat == {}:
            if re.search(r"time", curr_utt) and not re.search(r"\bin\b", curr_utt) and (location is None or location == ''):
                ask_loc_templates = ["I hear you are asking for local time. Could you tell me where you are? So I can search for you.",
                                     "Do you mind telling me where you are? So I can search for you based on your time zone."]
                timechat['time_state'] = "ask_loc"
                timechat['propose_continue'] = "CONTINUE"
                response = random.choice(ask_loc_templates)
            elif re.search(r"time", curr_utt) and not re.search(r"\bin\b", curr_utt) and (location is not None and location != ''):
                question_text = curr_utt + ' in ' + location
                response = self.EVI_bot(question_text)
                timechat['time_state'] = "ans_time"
                timechat['propose_continue'] = "STOP"
            elif re.search(r"what day", curr_utt):
                response = self.EVI_bot("what day is it today")
                timechat['time_state'] = "ans_time"
                timechat['propose_continue'] = "STOP"
            else:
                timechat['time_state'] = "ans_time"
                timechat['propose_continue'] = "STOP"
                response = self.EVI_bot(curr_utt)
        elif "time_state" in timechat and timechat['time_state'] == "ask_loc":
            ner = nlu.util_nlu.get_feature_by_key(
                self.state_manager.current_state, 'ner')
            location_temp = ''
            for kg_ls in nlpkg:
                if re.search("travel", kg_ls[3]) or re.search("city|county|island", kg_ls[1].lower()):
                    location_temp = kg_ls[0]
                    continue

            if location_temp == '' and ner is not None:
                for ent in ner:
                    if 'label' in ent and ent['label'] == 'LOC':
                        location_temp = ent['text']
                        self.state_manager.user_attributes.location = location_temp

            if location_temp != '':
                question_text = "what time is it in " + location_temp
                response = self.EVI_bot(question_text)
                timechat['time_state'] = "ans_time"
                timechat['propose_continue'] = "STOP"
            elif re.search(r"what day", curr_utt):
                response = self.EVI_bot("what day is it today")
                timechat['time_state'] = "ans_time"
                timechat['propose_continue'] = "STOP"
            else:
                question_text = "what time is it in " + \
                    re.sub(r"\bin\b", "", curr_utt)
                response = self.EVI_bot(question_text)
                timechat['time_state'] = "ans_time"
                timechat['propose_continue'] = "STOP"
        else:
            if re.search(r"what day", curr_utt):
                response = self.EVI_bot("what day is it today")
            else:
                response = self.EVI_bot(curr_utt)
                if response is None:
                    timequote_templates = ["Speaking of time, I hope you have a productive day today!",
                                           "Speaking of time, What do you like to do in your free time?",
                                           "Speaking of time, do you have any weekend plans?"]
                    response = random.choice(timequote_templates)
            timechat['time_state'] = "exit"
            timechat['propose_continue'] = "STOP"

        self.state_manager.user_attributes.timechat = timechat
        if response is not None:
            return response + "<break time = \"1s\"></break>"
        else:
            timechat['propose_continue'] = "STOP"
            timechat['time_state'] = "exit"
            error_templates = ["I guess I got lost in your time zone. <break time = \"1s\"></break>",
                               "Time is an illusion for me. <break time = \"1s\"></break>",
                               "My clock function is off but my chatting function is one hundred percent on. <break time = \"1s\"></break>"]
            return random.choice(error_templates)


class ReqEasterEgg_ResponseGenerator(LocalServiceModule):
    """req_task: a user asks to play music"""

    def execute(self):
        setattr(self.state_manager.current_state, "resp_type", "req_task")
        usr_utterance = self.input_data['text'].lower()

        key = ['easter_egg']

        if re.search(r"sing", usr_utterance):
            key.append('sing')
            return template_sys.utterance(selector=key, slots={},
                                          user_attributes_ref=self.state_manager.user_attributes)
        elif re.search(r"what are you doing", usr_utterance):
            key.append('ask_doing')
            return template_sys.utterance(selector=key, slots={},
                                          user_attributes_ref=self.state_manager.user_attributes)

        elif re.search(r"birthday", usr_utterance):
            key.append('happy_birthday')
            return template_sys.utterance(selector=key, slots={},
                                          user_attributes_ref=self.state_manager.user_attributes)
        elif re.search(r"about \byou\b", usr_utterance):
            key.append('talk_about_yourself')
            return template_sys.utterance(selector=key, slots={},
                                          user_attributes_ref=self.state_manager.user_attributes)
        # elif re.search(r"say hi", usr_utterance):
        #     # TODO: Why are we repeating their response?
        #     resp = re.sub(r"say |again|this|\bme\b|\byou\b|\bto\b", "", usr_utterance)
        #     postfixes = random.choice([" Ok I would only say once.", " Well, I can only say once"])
        #     responses = [resp + postfixes]
        elif re.search(r"fox say", usr_utterance):
            key.append('fox_say')
            return template_sys.utterance(selector=key, slots={},
                                          user_attributes_ref=self.state_manager.user_attributes)
        elif re.search(r"who invented you|your inventor|you.* robot|creator", usr_utterance):
            key.append('inventor')
            return template_sys.utterance(selector=key, slots={},
                                          user_attributes_ref=self.state_manager.user_attributes)
        elif re.search(r"\bspell\b", usr_utterance):
            # TODO: edge cases: spell my name, your name
            word_to_spell = re.sub(r'.*spell |the|word', '', usr_utterance)
            ner = nlu.util_nlu.get_feature_by_key(
                self.state_manager.current_state, 'ner')
            word_to_spell = ner[0]['text'] if ner is not None and ner != '' else ''

            key.append('spell_name')
            if re.search(r"your name|you spell$", usr_utterance):
                key.append('self')
                return template_sys.utterance(selector=key, slots={'self_name': 'alexa'},
                                              user_attributes_ref=self.state_manager.user_attributes)
            elif word_to_spell != '':
                responses = [
                    "<say-as interpret-as='spell-out'>{}</say-as>".format(word_to_spell)]
            elif re.search(r"my name|me", usr_utterance):
                word_to_spell = self.state_manager.user_attributes.usr_name
                if word_to_spell is not None and word_to_spell != '':
                    key.append('user')
                    return template_sys.utterance(selector=key, slots={
                        'username': word_to_spell
                    }, user_attributes_ref=self.state_manager.user_attributes)
                else:
                    key.append('no_name')
                    return template_sys.utterance(selector=key, slots={},
                                                  user_attributes_ref=self.state_manager.user_attributes)
            else:
                key.append('default')
                return template_sys.utterance(selector=key, slots={},
                                              user_attributes_ref=self.state_manager.user_attributes)
        elif re.search(r"alexa prize|your friend|turing test", usr_utterance):
            key.append('alexa_prize')
            return template_sys.utterance(selector=key, slots={},
                                          user_attributes_ref=self.state_manager.user_attributes)
        else:
            key.append('default')
            return template_sys.utterance(selector=key, slots={},
                                          user_attributes_ref=self.state_manager.user_attributes)


class ReqLoc_ResponseGenerator(LocalServiceModule):
    """req_loc: a user asks for a location"""

    def execute(self):
        setattr(self.state_manager.current_state, "resp_type", "location")
        return template_sys.utterance(selector='request_location', slots={},
                                      user_attributes_ref=self.state_manager.user_attributes)


class ReqMore_ResponseGenerator(LocalServiceModule):
    """req_more: a user asks for more"""

    def EVI_bot(self, utterance):  # TODO: get EVI response
        start_templates = ["Oh do you know that ", "Here is a fun fact "]
        try:
            client = get_client(
                api_key='xgTjk23SRM9Q9VFrUEFOjav0Bn4ot21W8uFLEieT')
            r = client.get_answer(question=utterance)
            if r["response"] == "" or re.search(r"skill://", r["response"]):
                return None
            if re.search(r"an opinion on that", r["response"]):
                return "Oh, I haven't thought about that before. What do you think?"
            return random.choice(start_templates) + r["response"]
        except Exception as e:
            logging.info("[REQMORE_MODULE] EVI_bot error : {}".format(e))
            return None

    def execute(self):
        # keyword = self.state_manager.last_state.get('ner', '') if self.state_manager.last_state is not None else ''
        setattr(self.state_manager.current_state, "resp_type", "req_more")
        random_thought = ''
        random_evi_funfact = ''
        key = ['request_more']

        try:
            keyword = self.state_manager.current_state.features["topic_keywords"][0]['keyword']
        except Exception:
            keyword = self.state_manager.user_attributes.suggest_keywords
            keyword = re.sub(r"\byes\b|\bno\b|okay|cool|alright|what|why|where|nice",
                             "", keyword) if keyword is not None else ''
        logging.info("[REQMORE_MODULE] Key word: %s" % keyword)

        if isinstance(keyword, str) and keyword != '':
            # there are key words
            random_thought = utils.get_randomthoughts_by_key(keyword)
            if random_thought is not None:
                key.append('random_thought')
                return template_sys.utterance(selector=key, slots={
                    'keyword': keyword, 'random_thought': random_thought
                }, user_attributes_ref=self.state_manager.user_attributes)

            if random_thought is None or random_thought == '':
                random_evi_funfact = self.EVI_bot(
                    utterance="fun fact about %s" % keyword)
                if random_evi_funfact is not None and random_evi_funfact != '':
                    return random_evi_funfact
                else:
                    key.append('default')
                    return template_sys.utterance(selector=key, slots={'keyword': keyword},
                                                  user_attributes_ref=self.state_manager.user_attributes)
        else:
            key.append('no_keyword')
            return template_sys.utterance(selector=key, slots={},
                                          user_attributes_ref=self.state_manager.user_attributes)


class Timeout:
    """Timeout class using ALARM signal."""
    class Timeout(Exception):
        pass

    def __init__(self, sec):
        self.sec = sec

    def __enter__(self):
        signal.signal(signal.SIGALRM, self.raise_timeout)
        signal.alarm(self.sec)

    def __exit__(self, *args):
        signal.alarm(0)    # disable alarm

    def raise_timeout(self, *args):
        raise Timeout.Timeout()
