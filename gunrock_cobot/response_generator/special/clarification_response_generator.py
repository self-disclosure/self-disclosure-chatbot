import re

from cobot_core.service_module import LocalServiceModule
from response_generator.special.special_response_generator import logger, template_sys
from nlu.util_nlu import get_feature_by_key

class Clarification_ResponseGenerator(LocalServiceModule):
    """req_clarify: A user asks to repeat/clarify what the chatbot said before or in the other way"""

    def execute(self):
        setattr(self.state_manager.current_state, "resp_type", "clarify")
        user_utterance = self.input_data['text'].lower()
        last_response = self.state_manager.last_state.get('response',
                                                          None) if self.state_manager.last_state is not None else None
        central_elem = get_feature_by_key(self.state_manager.current_state, 'central_elem')
        backstory = central_elem['backstory']
        sys_intents = central_elem['regex']['sys']

        if backstory['confidence'] > 0.9:
            resp = backstory['text']
            if backstory.get('followup', None) is not None:
                resp = resp + "<break time=\"400ms\"></break> <say-as interpret-as=\"interjection\">Ohh! </say-as> " + backstory['followup']
            return resp

        if last_response is None:
            # TODO: Migrate to template
            return "I think you want some clarification. Can you ask me in a different way?"
        key_word = self.state_manager.user_attributes.suggest_keywords
        logger.info(
            '[ClarificationRG] execute clarification with utterance: {}, last respnose: {}'.format(
                user_utterance, last_response
            )
        )

        key = ['clarification']
        if re.search(r"what|that|it", user_utterance) and isinstance(key_word, str) and key_word != '':
            key.append('what')
            return template_sys.utterance(selector=key, slots={'keyword': key_word},
                                          user_attributes_ref=self.state_manager.user_attributes)

        elif 'clarify' in sys_intents \
        or re.search(
            r"repeat|say.*again|rephrase|one again|what was the question|that again|you clarify|"
            r"try again|last thing you said|pardon|can't hear|once again|"
            r"what (did|do).* say|what you said|what did you say|what$|what.* just said|"
            r"what was that|what are you talking about",
            user_utterance
        ):
            if re.search(r"^(<.*>)?Sure! I said:", last_response):
                return last_response
            key.append('repeat')
            return template_sys.utterance(selector=key, slots={'utt': re.sub(r"(I said: )|Hmm|Ah|Great! |Alright\. |Okay! |Sure\. ", "", last_response)},
                                          user_attributes_ref=self.state_manager.user_attributes)
        elif re.search(r"what.* point|get.* point", user_utterance):
            key.append('what_point')
            return template_sys.utterance(selector=key, slots={'utt': re.sub(r"(My point is that)|Hmm|Ah ", "", last_response)},
                                          user_attributes_ref=self.state_manager.user_attributes)
        elif re.search(r"what.* you mean|what does it mean$", user_utterance):
            key.append('what_you_mean')
            return template_sys.utterance(selector=key,
                                          slots={'utt': re.sub(r"(I meant)|Hmm|Ah", "", last_response)},
                                          user_attributes_ref=self.state_manager.user_attributes)
        elif 'clarify_misunderstood' in sys_intents:
            key.append('dont_understand')
            return template_sys.utterance(selector=key,
                                          slots={},
                                          user_attributes_ref=self.state_manager.user_attributes)
        elif re.search(r"give.* example", user_utterance):
            key.append('give_example')
            return template_sys.utterance(selector=key,
                                          slots={'utt': re.sub(r"(I don't know any example but I can definitely repeat what I said. I meant)|Hmm|Ah", "", last_response)},
                                          user_attributes_ref=self.state_manager.user_attributes)

        key.append('other')
        return template_sys.utterance(selector=key,
                                      slots={},
                                      user_attributes_ref=self.state_manager.user_attributes)
