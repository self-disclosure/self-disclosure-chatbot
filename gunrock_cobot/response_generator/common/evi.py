import logging
import re
import warnings
from cobot_common.service_client import get_client
from constants_api_keys import COBOT_API_KEY

from template_manager import Template, TemplateManager

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


evi_response_filter = (
    r"an opinion on that|tough to explain|I didn’t get that.|Sorry, I didn’t catch that|"
    r"Alexa Original|'favorite' is usually defined|is a plural form of|Hi there|Hi|"
    r"Sorry, I can’t find the answer to the question I heard.|I don't have an opinion on that.|"
    r"You could ask me about music or geography.|You can ask me anything you like.|"
    r"I can answer questions about people, places and more.|^As a|I didn't get that|"
    r"'Artist' is usually defined|The Place I Belong, the movie released|"
    r"'Talking' is usually defined|usually defined as plural of what|sound is a vibration|"
    r"something that would happen|Say is a town in southwest Niger|An album is a collection of|"
    r"I love dancing|I pronounce that 'would'|I pronounce that 'do'|'Was' is related to a word|"
    r"'Next' is usually defined|Dreams aren't well understood|\"Me Too\" or \"#MeToo\"|"
    r"Yours, the chain of women's clothing outlets.|the computer application developed by Apricorn|"
    r"The movie \"Instrument\"|A Little Bit is by Jessica Simpson|'S is by Jim Bianco"
)


def EVI_bot(utterance, handle_empty_response=False, user_attributes_ref=None):
    warnings.warn("Use query", DeprecationWarning, stacklevel=2)
    return query(utterance,
                 handle_empty_response=handle_empty_response,
                 user_attributes_ref=user_attributes_ref)


def query(utterance: str, *,
          handle_empty_response=False, user_attributes_ref=None,
          response_filter: str = None):
    def get_tm():
        return TemplateManager(Template.error, user_attributes_ref)

    try:
        client = get_client(api_key=COBOT_API_KEY, timeout_in_millis=350)
        r = client.get_answer(question=utterance, timeout_in_millis=350)

        if not r["response"] or r["response"].startswith('skill://'):
            if handle_empty_response:
                return get_tm().speak('evi/no_response', {})
            return None

        if re.search(response_filter or r"an opinion on that", r["response"]):
            return get_tm().speak('evi/evi_response_filtered_out', {}) + " "
        return r["response"] + " "
    except Exception as e:
        logger.warning('[evi.py] call evi error: {}'.format(e))
        if handle_empty_response:
            return get_tm().speak('evi/no_response', {})
        return None

if __name__ == '__main__':
    print(query("abdefgt", handle_empty_response=True))