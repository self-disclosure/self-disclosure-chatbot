import nlu.util_nlu
import utils
from cobot_core.service_module import LocalServiceModule
from response_generator.food.food_chatbot import food_response_generator
import logging
from nlu.dataclass import CentralElement, ReturnNLP, ReturnNLPSegment
from nlu.constants import DialogAct as DialogActEnum

logger = logging.getLogger(__name__)


class FoodResponseGeneratorLocal(LocalServiceModule):

    def execute(self):
        # returnnlp = ReturnNLP(self.input_data['returnnlp'])
        # logging.info("[FOODMODULE] check dialog act of yes_no_question using returnnlp util: {}".format(returnnlp.has_dialog_act(DialogActEnum.YES_NO_QUESTION)))

        user_utterance = self.input_data["text"].lower()
        # logger.info("[FOODMODULE] Entering Module with input: {}".format(
        #     self.input_data['text']))
        # logger.info("[FOODMODULE] input_data keys: {}".format(
        #     list(self.input_data.keys())))

        # initialize sys_info
        sys_info = {}
        # Get features from System
        sys_info['features'] = {}
        sys_info['features']['returnnlp'] = nlu.util_nlu.get_feature_by_key(
            self.state_manager.current_state, 'returnnlp')
        sys_info['features']['input_data_returnnlp'] = self.input_data["returnnlp"]
        # returnnlp = ReturnNLP(sys_info['features']['returnnlp'])
        #logging.debug("[FOODMODULE] check dsystem acknowledgement: {}".format(self.input_data['system_acknowledgement']))
        sys_info['features']['system_acknowledgement'] = self.input_data['system_acknowledgement']
        sys_info['features']['sys_DA'] = nlu.util_nlu.get_feature_by_key(
            self.state_manager.current_state, 'dialog_act')
        sys_info['features']['sys_modules'] = nlu.util_nlu.get_feature_by_key(
            self.state_manager.current_state, 'topic_module')
        # Get the central elem
        sys_info['features']['central_elem'] = nlu.util_nlu.get_feature_by_key(
            self.state_manager.current_state, 'central_elem')
        # Get the session_id
        sys_info['session_id'] = self.state_manager.current_state.session_id
        # Get the food_context content
        sys_info['food_context'] = self.input_data.get("foodchat", {})
        # Get the last_module
        sys_info['last_module'] = self.input_data['previous_modules'][0]
        # Get previous modules
        sys_info['previous_modules'] = self.input_data['previous_modules']
        sys_info['a_b_test'] = self.input_data['a_b_test']

        logger.info("[FOODMODULE] Last_Module is: {}".format(
            sys_info['last_module']))
        # logger.info("[FOODMODULE] Foodcontext: {}".format(
        #      sys_info['food_context']['context']))
        logger.info("[FOODMODULE] Import Sys_Info successful")

        # return
        food_response = food_response_generator(
            user_utterance, sys_info, self.state_manager.user_attributes)

        logger.info("[FOODMODULE] Generate food_module response")

        # Update the foodchat context
        self.state_manager.user_attributes.foodchat = food_response['foodchat']
        # self.state_manager,user_attributes.prev_hash = food_response['prev_hash']

        response_text = food_response["response"]
        return response_text