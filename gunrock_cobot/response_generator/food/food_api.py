import requests
import ast
import json


base_url = "https://developers.zomato.com/api/v2.1/"
ZOMATO_API_KEY = ["b535154760d09aab961c44ca277ff5c4",
                  "97d83405c47e0376e286a71a18b0d63c"]


def initialize_app(config):
    return Zomato(config)


class Zomato:

    def __init__(self, config):
        self.user_key = config["user_key"]

    def get_categories(self):
        """
        Takes no input.
        Returns a dictionary of IDs and their respective category names.
        """
        headers = {'Accept': 'application/json', 'user-key': self.user_key}
        r = (requests.get(base_url + "categories",
                          headers=headers).content).decode("utf-8")
        a = ast.literal_eval(r)

        self.is_key_invalid(a)
        self.is_rate_exceeded(a)

        categories = {}
        for category in a['categories']:
            categories.update(
                {category['categories']['id']: category['categories']['name']})

        return categories

    def get_city_search_result(self, city_name, count=1):
        """
        Takes City Name as input.
        Returns the ID for the city given as input.
        """
        # if city_name.isalpha() == False:
        #     raise ValueError('InvalidCityName')
        city_name = city_name.split(' ')
        city_name = '%20'.join(city_name)
        headers = {'Accept': 'application/json', 'user-key': self.user_key}
        # r = (requests.get(base_url + "cities?q=" + city_name + '&count=' + str(count),
        #                   headers=headers).content).decode("utf-8")
        # a = ast.literal_eval(r)
        a = requests.get(base_url + "cities?q=" + city_name +
                         '&count=' + str(count), headers=headers).json()

        self.is_key_invalid(a)
        self.is_rate_exceeded(a)

        result = []

        original_city_name = city_name.replace('%20', ' ')

        if len(a['location_suggestions']) > 0:
            for location in a['location_suggestions']:
                if 'name' in location:
                    if str(location['name']).lower().split(',')[0] == str(original_city_name).lower():
                        result.append(location)
        return result

    def get_city_ID(self, city_name):
        """
        Takes City Name as input.
        Returns the ID for the city given as input.
        """
        if city_name.isalpha() == False:
            raise ValueError('InvalidCityName')
        city_name = city_name.split(' ')
        city_name = '%20'.join(city_name)
        headers = {'Accept': 'application/json', 'user-key': self.user_key}
        r = (requests.get(base_url + "cities?q=" + city_name,
                          headers=headers).content).decode("utf-8")
        a = ast.literal_eval(r)

        self.is_key_invalid(a)
        self.is_rate_exceeded(a)

        if len(a['location_suggestions']) == 0:
            raise Exception('invalid_city_name')
        elif 'name' in a['location_suggestions'][0]:
            # print(a['location_suggestions'][0])
            city_name = city_name.replace('%20', ' ')
            if str(a['location_suggestions'][0]['name']).lower().split(',')[0] == str(city_name).lower():
                return a['location_suggestions'][0]['id']
            else:
                raise ValueError('InvalidCityName')
    # Write by Mingyang

    def get_city_location(self, city_name):
        """
        Takes City Name as input.
        Returns the the Location Details for the city given as input.
        """
        if city_name.isalpha() == False:
            raise ValueError("InvalidCityName")
        city_name = city_name.split(' ')
        city_name = '%20'.join(city_name)
        headers = {'Accept': 'application/json', 'user-key': self.user_key}
        r = (requests.get(base_url + "locations?query=" + city_name,
                          headers=headers).content).decode("utf-8")

        a = ast.literal_eval(r)

        self.is_key_invalid(a)
        self.is_rate_exceeded(a)

        if len(a['location_suggestions']) == 0:
            raise Exception('invalid_city_name')
        elif 'latitude' in a['location_suggestions'][0]:
            city_name = city_name.replace('%20', ' ')
            if str(a['location_suggestions'][0]['city_name']).lower().split(',')[0] == str(city_name).lower():
                return a['location_suggestions'][0]
            else:
                raise ValueError('InvalidCityName')

    def get_city_name(self, city_ID):
        """
        Takes City ID as input.
        Returns the name of the city ID given as input.
        """
        self.is_valid_city_id(city_ID)

        headers = {'Accept': 'application/json', 'user-key': self.user_key}
        r = (requests.get(base_url + "cities?city_ids=" +
                          str(city_ID), headers=headers).content).decode("utf-8")
        a = ast.literal_eval(r)

        self.is_key_invalid(a)
        self.is_rate_exceeded(a)

        if a['location_suggestions'][0]['country_name'] == "":
            raise ValueError('InvalidCityId')
        else:
            temp_city_ID = a['location_suggestions'][0]['id']
            if temp_city_ID == str(city_ID):
                return a['location_suggestions'][0]['name']

    def get_collections(self, city_ID, limit=None):
        """
        Takes City ID as input. limit parameter is optional.
        Returns dictionary of Zomato restaurant collections in a city and their respective URLs.
        """
        self.is_valid_city_id(city_ID)

        headers = {'Accept': 'application/json', 'user-key': self.user_key}
        if limit == None:
            r = (requests.get(base_url + "collections?city_id=" +
                              str(city_ID), headers=headers).content).decode("utf-8")
        else:
            if str(limit).isalpha() == True:
                raise ValueError('LimitNotInteger')
            else:
                r = (requests.get(base_url + "collections?city_id=" + str(city_ID) +
                                  "&count=" + str(limit), headers=headers).content).decode("utf-8")
        a = ast.literal_eval(r)

        self.is_key_invalid(a)
        self.is_rate_exceeded(a)

        collections = {}
        for collection in a['collections']:
            collections.update(
                {collection['collection']['title']: collection['collection']['url']})

        return collections

    def get_cuisines(self, city_ID):
        """
        Takes City ID as input.
        Returns a sorted dictionary of all cuisine IDs and their respective cuisine names.
        """
        self.is_valid_city_id(city_ID)

        headers = {'Accept': 'application/json', 'user-key': self.user_key}
        r = (requests.get(base_url + "cuisines?city_id=" +
                          str(city_ID), headers=headers).content).decode("utf-8")
        a = ast.literal_eval(r)

        self.is_key_invalid(a)
        self.is_rate_exceeded(a)

        if len(a['cuisines']) == 0:
            raise ValueError('InvalidCityId')
        temp_cuisines = {}
        cuisines = {}
        for cuisine in a['cuisines']:
            temp_cuisines.update(
                {cuisine['cuisine']['cuisine_id']: cuisine['cuisine']['cuisine_name']})

        for cuisine in sorted(temp_cuisines):
            cuisines.update({cuisine: temp_cuisines[cuisine]})

        return cuisines

    def get_establishment_types(self, city_ID):
        """
        Takes City ID as input.
        Returns a sorted dictionary of all establishment type IDs and their respective establishment type names.
        """
        self.is_valid_city_id(city_ID)

        headers = {'Accept': 'application/json', 'user-key': self.user_key}
        r = (requests.get(base_url + "establishments?city_id=" +
                          str(city_ID), headers=headers).content).decode("utf-8")
        a = ast.literal_eval(r)

        self.is_key_invalid(a)
        self.is_rate_exceeded(a)

        temp_establishment_types = {}
        establishment_types = {}
        if 'establishments' in a:
            for establishment_type in a['establishments']:
                temp_establishment_types.update({establishment_type['establishment'][
                                                'id']: establishment_type['establishment']['name']})

            for establishment_type in sorted(temp_establishment_types):
                establishment_types.update(
                    {establishment_type: temp_establishment_types[establishment_type]})

            return establishment_types
        else:
            raise ValueError('InvalidCityId')

    def get_nearby_restaurants(self, latitude, longitude):
        """
        Takes the latitude and longitude as inputs.
        Returns a dictionary of Restaurant IDs and their corresponding Zomato URLs.
        """
        try:
            float(latitude)
            float(longitude)
        except ValueError:
            raise ValueError('InvalidLatitudeOrLongitude')

        headers = {'Accept': 'application/json', 'user-key': self.user_key}
        r = (requests.get(base_url + "geocode?lat=" + str(latitude) +
                          "&lon=" + str(longitude), headers=headers).content).decode("utf-8")
        a = ast.literal_eval(r)

        nearby_restaurants = {}
        for nearby_restaurant in a['nearby_restaurants']:
            nearby_restaurants.update({nearby_restaurant['restaurant'][
                                      'id']: nearby_restaurant['restaurant']['url']})

        return nearby_restaurants

    def get_restaurant(self, restaurant_ID):
        """
        Takes Restaurant ID as input.
        Returns a dictionary of restaurant details.
        """
        self.is_valid_restaurant_id(restaurant_ID)

        headers = {'Accept': 'application/json', 'user-key': self.user_key}
        r = (requests.get(base_url + "restaurant?res_id=" +
                          str(restaurant_ID), headers=headers).content).decode("utf-8")
        a = ast.literal_eval(r)

        if 'code' in a:
            if a['code'] == 404:
                raise('InvalidRestaurantId')

        restaurant_details = {}
        restaurant_details.update({"name": a['name']})
        restaurant_details.update({"url": a['url']})
        restaurant_details.update({"location": a['location']['address']})
        restaurant_details.update({"city": a['location']['city']})
        restaurant_details.update({"city_ID": a['location']['city_id']})
        restaurant_details.update(
            {"user_rating": a['user_rating']['aggregate_rating']})

        restaurant_details = DotDict(restaurant_details)
        return restaurant_details

    def restaurant_search(self, query="", latitude="", longitude="", cuisines="", limit=10):
        """
        Takes either query, latitude and longitude or cuisine as input.
        Returns a list of Restaurant IDs.
        """
        cuisines = "%2C".join(cuisines.split(","))
        if str(limit).isalpha() == True:
            raise ValueError('LimitNotInteger')
        headers = {'Accept': 'application/json', 'user-key': self.user_key}
        # r = (requests.get(base_url + "search?q=" + str(query) + "&count=" + str(limit) + "&lat=" + str(latitude) +
        #                   "&lon=" + str(longitude) + "&cuisines=" + str(cuisines), headers=headers).content).decode("utf-8")
        r = requests.get(base_url + "search?q=" + str(query) + "&count=" + str(limit) + "&lat=" + str(latitude) +
                         "&lon=" + str(longitude) + "&cuisines=" + str(cuisines), headers=headers)
        a = json.loads(r.content)
        #a = ast.literal_eval(r)

        restaurants = []

        if a['results_found'] == 0:
            return []
        else:
            for restaurant in a['restaurants']:
                restaurants.append(restaurant['restaurant']['id'])

        return restaurants

    def restaurant_search_full(self, city_id, cusine_id=None, category_id=None, limit=2):
        """
        Takes city_id, cusine_id or category_id
        Returns a list of Restaurant IDs.
        """
        headers = {'Accept': 'application/json', 'user-key': self.user_key}
        # r = (requests.get(base_url + "search?q=" + str(query) + "&count=" + str(limit) + "&lat=" + str(latitude) +
        #                   "&lon=" + str(longitude) + "&cuisines=" + str(cuisines), headers=headers).content).decode("utf-8")
        if not cusine_id and not category_id:
            r = requests.get(base_url + "search?entity_id=" + str(city_id) + "&entity_type=city" +
                             "&count=" + str(limit) + "&sort=rating&order=desc", headers=headers)
        elif cusine_id:
            r = requests.get(base_url + "search?entity_id=" + str(city_id) + "&entity_type=city" + "&count=" +
                             str(limit) + "&cuisines=" + str(cusine_id) + "&sort=rating&order=desc", headers=headers)
        else:
            r = requests.get(base_url + "search?entity_id=" + str(city_id) + "&entity_type=city" + "&count=" +
                             str(limit) + "&category=" + str(category_id) + "&sort=rating&order=desc", headers=headers)

        a = json.loads(r.content)
        #a = ast.literal_eval(r)

        restaurants = []

        if a['results_found'] == 0:
            return []
        else:
            for restaurant in a['restaurants']:
                restaurants.append(restaurant['restaurant'])

        return restaurants

    def is_valid_restaurant_id(self, restaurant_ID):
        """
        Checks if the Restaurant ID is valid or invalid.
        If invalid, throws a InvalidRestaurantId Exception.
        """
        restaurant_ID = str(restaurant_ID)
        if restaurant_ID.isnumeric() == False:
            raise ValueError('InvalidRestaurantId')

    def is_valid_city_id(self, city_ID):
        """
        Checks if the City ID is valid or invalid.
        If invalid, throws a InvalidCityId Exception.
        """
        city_ID = str(city_ID)
        if city_ID.isnumeric() == False:
            raise ValueError('InvalidCityId')

    def is_key_invalid(self, a):
        """
        Checks if the API key provided is valid or invalid.
        If invalid, throws a InvalidKey Exception.
        """
        if 'code' in a:
            if a['code'] == 403:
                raise ValueError('InvalidKey')

    def is_rate_exceeded(self, a):
        """
        Checks if the request limit for the API key is exceeded or not.
        If exceeded, throws a ApiLimitExceeded Exception.
        """
        if 'code' in a:
            if a['code'] == 440:
                raise Exception('ApiLimitExceeded')


class DotDict(dict):
    """
    Dot notation access to dictionary attributes
    """

    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

# if __name__ == "__main__":
#     config = {"user_key": ZOMATO_API_KEY}
#     zomato = initialize_app(config)

    # # define the category_dictionary
    # category_dictionary = zomato.get_categories()
    # print(category_dictionary)

    # # Get ID for city
    # city_ID = int(zomato.get_city_ID("lansing"))
    # print(city_ID)

    # collections_dictionary = zomato.get_collections(city_ID)
    # print(collections_dictionary)

    # # Get cusines in a city
    # cuisine_dictionary = zomato.get_cuisines(city_ID)
    # print("cusines: ")
    # print(cuisine_dictionary)

    # # Get establishment in a city
    # establishment_dictionary = zomato.get_cuisines(city_ID)
    # print("establishment: ")
    # print(establishment_dictionary)

    # Get the location details of a city
    # location_dictionary = zomato.get_city_location("seattle")
    # print("location: ")
    # print(location_dictionary)

    # latitude = location_dictionary['latitude']
    # longitude = location_dictionary['longitude']

    # # Get the nearby restaurants
    # nearby_restaurants = zomato.restaurant_search(
    #     latitude=latitude, longitude=longitude)
    # print("restaurants: ")
    # print(nearby_restaurants)
