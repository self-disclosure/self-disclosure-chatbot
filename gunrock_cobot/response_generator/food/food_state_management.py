import logging
logging.basicConfig(
    format='[%(levelname)s] %(asctime)s [%(filename)s:%(lineno)d] %(message)s',
)
from response_generator.food.food_utils import *
from selecting_strategy.topic_module_proposal import disable_propose
import random
from backstory_client import get_backstory_response_text
from random import sample
from response_generator.food.food_question_handeler import FoodQuestionHandeler
from nlu.dataclass import CentralElement, ReturnNLP, ReturnNLPSegment

template_food = template_manager.Templates.food

TOTAL_NUM_Q_GENERAL = template_food.count_utterances(
    selector="qa_templates/qa_general/majority") + template_food.count_utterances(
    selector="qa_templates/qa_general/minority") + template_food.count_utterances(
    selector="qa_templates/qa_general/least_minority")
TOTAL_NUM_Q_GENERAL_MAJORITY = template_food.count_utterances(
    selector="qa_templates/qa_general/majority")
TOTAL_NUM_Q_GENERAL_MINORITY = template_food.count_utterances(
    selector="qa_templates/qa_general/minority")
TOTAL_NUM_Q_GENERAL_LEAST_MINORITY = template_food.count_utterances(
    selector="qa_templates/qa_general/least_minority")

TOTAL_NUM_Q_FOOD = template_food.count_utterances(
    selector="qa_templates/qa_food")
TOTAL_NUM_Q_CUISINE = template_food.count_utterances(
    selector="qa_templates/qa_cuisine")
# logging.info("[FOODMODULE] Total number of general questions: {}".format(
#     TOTAL_NUM_Q_GENERAL))
# logging.debug(
#     "[FOODMODULE] Total number of cuisine questions: {}".format(TOTAL_NUM_Q_CUISINE))


class Food_Acknowledgement(object):

    def __init__(self, user_utterance, user_intent, context, user_attributes, sys_info={}):
        self.user_utterance = user_utterance
        self.user_intent = user_intent
        self.context = context
        self.user_attributes = user_attributes
        self.sys_info = sys_info
        self.returnnlp_util = ReturnNLP(self.sys_info['features']['returnnlp'])

        # Bind the Acknowledge Function
        self.__bind_acknowledge_key_map()
        self.__bind_acknowledge_intent_map()

    def __bind_acknowledge_key_map(self):
        self.acknowledge_key_func = {
            "ask_interest_food": self.a_ask_interest_food,
            "ask_personal_experience": self.a_ask_personal_experience,
            "ask_yes_no_cook": self.a_ask_yes_no_cook,
            "ask_frequency_week": self.a_ask_frequency_week,
            "ask_where_best_food": self.a_ask_where_best_food,
            "ask_where_best_dish": self.a_ask_where_best_dish,
            "ask_yes_no_childfood": self.a_ask_yes_no_childfood,
            "ask_yes_no_eat_healthy": self.a_ask_yes_no_eat_healthy,
            "ask_personal_suggestion": self.a_ask_personal_suggestion,
            "ask_favorite_country_food": self.a_ask_favorite_country_food,
            "ask_favorite_dish": self.a_ask_favorite_dish,
            "ask_personal_opinion": self.a_ask_personal_opinion,
            "ask_whether_grow_own_food": self.a_ask_whether_grow_own_food,
            "ask_what_grow_own_food": self.a_ask_what_grow_own_food,
            "ask_strange_food": self.a_ask_strange_food,
            "ask_whether_adventurous_eater": self.a_ask_whether_adventurous_eater,
            "ask_best_beginner_sushi": self.a_ask_best_beginner_sushi,
            "ask_whether_pizza_topping": self.a_ask_whether_pizza_topping,
            "ask_favorite_pizza_topping": self.a_ask_favorite_pizza_topping,
            "ask_better_nystrip_ribeye": self.a_ask_better_nystrip_ribeye,
            "ask_how_to_eat_peanut_butter": self.a_ask_how_to_eat_peanut_butter,
            "ask_whether_instant_traditional_ramen": self.a_ask_whether_instant_traditional_ramen,
            "ask_interest_in_ppa_question": self.a_ask_interest_in_ppa_question,
            "ask_cuisine_favorite_dish": self.a_ask_cuisine_favorite_dish,
            "ask_cuisine_favorite_restaurant": self.a_ask_cuisine_favorite_restaurant,
            "ask_yes_no_cook_cuisine": self.a_ask_yes_no_cook_cuisine,
            "ask_yes_no_cook_dish": self.a_ask_yes_no_cook_dish,
            "ask_yes_no_learn_recipe": self.a_ask_yes_no_learn_recipe,
            "ask_followup_recipe": self.a_ask_followup_recipe,
            "ask_foodstock_quarantine": self.a_ask_foodstock_quarantine,
            "ask_preference_cook_restaurant": self.a_ask_preference_cook_restaurant,
            "ask_how_to_find_restaurant": self.a_ask_how_to_find_restaurant,
            "ask_eat_often_dish": self.a_ask_eat_often_dish,
            "ask_yes_no_secret_food_recipe": self.a_ask_yes_no_secret_food_recipe,
            "ask_how_raw_food_cook": self.a_ask_how_raw_food_cook,
            "ask_raw_food_dish": self.a_ask_raw_food_dish,
            "ask_yes_no_drink_partner": self.a_ask_yes_no_drink_partner,
            "ask_what_best_drink_partner": self.a_ask_what_best_drink_partner,
        }

    def __bind_acknowledge_intent_map(self):
        self.acknowledge_intent_func = {
            "food_i_suggestfood": self.a_suggestfood,
            "food_i_suggestfoodcuisine": self.a_suggestfoodcuisine,
            "food_i_askquestion": self.a_askquestion,
            #"food_i_notinterest": self.a_notinterest,
            "food_i_answeruncertain": self.a_answeruncertain,
        }

    # def get_current_state(self, current_state):
    #     self.current_state = current_state

    def acknowledge_paraphrase(self):
        # Replacement_Rules
        replacement_rules = [(r"\bare you\b", "am i"), (r"\bi was\b", "you were"), (r"\bi am\b", "you are"), (r"\bwere you\b", "was i"),
                             (r"\bi\b", "you"), (r"\bmy\b",
                                                 "your"), (r"\bmyself\b", "yourself"),
                             (r"\byou\b", "i"), (r"\byour\b", "my"),
                             (r"\bme\b", "you")]

        paraphrase_response = self.user_utterance
        # Replace a couple of things
        for rule in replacement_rules:
            paraphrase_response = re.sub(rule[0], rule[1], paraphrase_response)
        return paraphrase_response

    def get_acknowledge(self):
        # acknowledge the intent first
        system_acknowledgement = self.sys_info["features"]["system_acknowledgement"]
        if system_acknowledgement.get("output_tag", "") and (system_acknowledgement.get("output_tag", "") not in ["ack_question_idk", "ack_hobbies", "ack_general"]):
            acknowledge_response = system_acknowledgement["ack"]
            return acknowledge_response

        acknowledge_response = self.get_intent_acknowledge()
        if not self.user_intent['food_i_askquestion']:
            acknowledge_response = " ".join(
                [acknowledge_response, self.get_key_acknowledge()])

        return acknowledge_response

    def get_intent_acknowledge(self):
        r"""
        acknowledge for different intents and special instances
        """
        response = ""
        if self.user_intent['food_i_requestrestaurant']:
            pass
        elif self.user_intent['food_i_requestrecipe']:
            pass
        elif self.user_intent['food_i_askquestion']:
            response = self.acknowledge_intent_func['food_i_askquestion']()
        elif self.user_intent['food_i_suggestfood']:
            response = self.acknowledge_intent_func['food_i_suggestfood']()
        elif self.user_intent['food_i_answeruncertain']:
            response = self.acknowledge_intent_func['food_i_answeruncertain']()
        elif self.user_intent['food_i_suggestfoodcuisine']:
            response = self.acknowledge_intent_func[
                'food_i_suggestfoodcuisine']()
        #add another case for opening acknowledgement
        elif self.context["conversation_context"]["previous_state"] == "food_none" and re.search("cook", self.user_utterance) is None and re.search("eat", self.user_utterance) is None:
            #indicates reentering food
            response = self.acknowledge_food_entering()
        return response

    def get_key_acknowledge(self):
        response = ""
        if self.context["conversation_context"]["selected_question"]:
            acknowledge_key = self.context["conversation_context"]["selected_question"].get("acknowledge", None)
            if acknowledge_key:
                response = self.acknowledge_key_func[acknowledge_key]()
        return response

#################special instance acknowledge##################
    def acknowledge_food_entering(self):
        r"""
        Build Opening response for normal entering and resume mode
        """
        user_conversation_context = self.context["conversation_context"]
        user_context = self.context["user_context"]
        if user_conversation_context["resume_mode"]:
            #check favorite food, talked food, talked cuisine
            if any([x["food"] in user_context["favorite_food"] for x in user_conversation_context["detected_food"]]):
                for x in user_conversation_context["detected_food"]:
                    if x["food"] in list(user_context["favorite_food"]):
                        favorite_food = x["food"]
                        break
                response = template_food.utterance(selector='general_templates/resume_fav_food', slots={"food": favorite_food}, user_attributes_ref=self.user_attributes)
            elif any([x in user_context["favorite_cuisine"] for x in user_conversation_context["detected_cuisine"]]):
                for x in user_conversation_context["detected_cuisine"]:
                    if x in user_context["favorite_cuisine"]:
                        favorite_cuisine = x
                        break
                response = template_food.utterance(selector='general_templates/resume_fav_cuisine', slots={"cuisine": favorite_cuisine}, user_attributes_ref=self.user_attributes)

            else:
                response = template_food.utterance(selector='general_templates/resume_start_food', slots={}, user_attributes_ref=self.user_attributes)
                self.context["conversation_context"]["start_food_general"] = True
        else:
            response = template_food.utterance(selector='general_templates/start_food', slots={}, user_attributes_ref=self.user_attributes)
            self.context["conversation_context"]["start_food_general"] = True

        return response
#################intent_acknowledge############################

    def a_suggestfood(self):
        current_food = self.context["conversation_context"]["current_food"]['food']
        if current_food and current_food.lower() != "onion" and current_food.lower() != "onions":
            response = template_food.utterance(selector='general_templates/acknowledge_suggest_food', slots={'food': current_food},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_suggestfoodcuisine(self):
        current_cuisine = self.context[
            "conversation_context"]["current_cuisine"]

        if current_cuisine:
            response = template_food.utterance(selector='general_templates/acknowledge_suggest_cuisine', slots={'cuisine': current_cuisine},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_askquestion(self):
        response = template_food.utterance(selector='general_templates/acknowledge_question', slots={},
                                           user_attributes_ref=self.user_attributes)
        return response

    def a_answeruncertain(self):
        response = template_food.utterance(selector='general_templates/acknowledge_uncertain_answer', slots={},
                                           user_attributes_ref=self.user_attributes)
        return response
#################key_acknowledge###############################

    def a_ask_interest_food(self):
        r"""
        acknowledge the question for interest food.
        (1) Detect whether it is a positive question or negative question. (favorite vs least favorite)
        (2) Detect whether there is food detected
        (3) Otherwise just acknowledge in general
        """
        # current_question = self.context[
        #     "conversation_context"]["selected_question"]['q'][0]['text']
        current_question = self.context[
            "conversation_context"]["selected_question"]['q'][0]
        # detect whether it is a positive interest or negative interest
        positive_interest = True
        if re.search(r"least", current_question):
            positive_interest = False

        if positive_interest and self.context["conversation_context"]["detected_food"]:
            selected_food = self.context[
                "conversation_context"]["detected_food"][0]['food']
            response = template_food.utterance(selector='general_templates/acknowledge_ask_interest_positive', slots={'food': selected_food},
                                               user_attributes_ref=self.user_attributes)
        elif not positive_interest and self.context["conversation_context"]["detected_food"]:
            selected_food = self.context[
                "conversation_context"]["detected_food"][0]['food']
            response = template_food.utterance(selector='general_templates/acknowledge_ask_interest_negative', slots={'food': selected_food},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_personal_experience(self):
        r"""
        acknowledge the question where we ask about people's personal experience. For example,
        do you like cooking food for yourself or you like eating in the restaurant.
        (1) if the answer is more than three words, we appreciate for users to share their experience.
        (2) if the answer is less than three words, respond with general acknowledgement.
        """
        if len(self.user_utterance.split()) > 3:
            response = template_food.utterance(selector='general_templates/appreciate_share_experience', slots={},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_yes_no_cook(self):
        r"""
        acknowledge yes_no cook question.
        (1)The User answer yes.
        (2)The User answer no.
        (3)The User answer something else.
        """
        if self.user_intent['food_i_answeryes']:
            response = template_food.utterance(selector='general_templates/acknowledge_yes_cook', slots={},
                                               user_attributes_ref=self.user_attributes)
        elif self.user_intent['food_i_answerno']:
            response = template_food.utterance(selector='general_templates/acknowledge_no_cook', slots={},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_frequency_week(self):
        r"""
        acknowledge the question where we ask user how often do they cook in a week?
        (1)Check if a number is detected, and based on the number give different acknowledgement
        (2) If not detected, just do general_templates
        """
        # TODO: Detect the number of times or just repeat the users response.
        if detect_sentence_digit(self.user_utterance)[0]:
            frequency = detect_sentence_digit(self.user_utterance)[1]
            acknowledge_key = week_frequency_acknowledge(frequency)
            response = template_food.utterance(selector='general_templates/{}'.format(acknowledge_key), slots={"frequency": frequency},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_where_best_food(self):
        r"""
        acknowledge the question where we ask user about where he has the best food before.
        """
        # TODO: Optimize the templates for this acknowledgement
        if len(self.user_utterance.split()) >= 2:
            response = template_food.utterance(selector='general_templates/acknowledge_where_best_food', slots={},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_where_best_dish(self):
        r"""
        acknowledge the question where we ask user about where he has the best food before.
        """
        # TODO: Optimize the templates for this acknowledgement
        if len(self.user_utterance.split()) >= 2:
            response = template_food.utterance(selector='general_templates/acknowledge_where_best_food', slots={},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_yes_no_childfood(self):
        r"""
        acknowledge the question where we ask if they have a food that they
        (1) answer yes and share the food or a longer response: Appreciate for their sharing.
        (2) Other cases: general acknowledge
        """
        if self.user_intent['food_i_answeryes'] and len(self.user_utterance.split()) < 3 and not self.context["conversation_context"]["detected_food"]:
            response = template_food.utterance(selector='general_templates/acknowledge_yes_childfood', slots={},
                                               user_attributes_ref=self.user_attributes)
        elif len(self.user_utterance.split()) < 5 and self.context["conversation_context"]["detected_food"]:
            response = template_food.utterance(selector='general_templates/acknowledge_yes_childfood_food_detected', slots={
                                               "food": self.context["conversation_context"]["detected_food"][0]['food']}, user_attributes_ref=self.user_attributes)
        elif len(self.user_utterance.split()) > 5:
            response = template_food.utterance(selector='general_templates/appreciate_share_experience', slots={},
                                               user_attributes_ref=self.user_attributes)
            if self.context["conversation_context"]["detected_food"]:
                response = self.context["conversation_context"][
                    "detected_food"][0]['food'] + "? " + response
        elif self.user_intent['food_i_answerno']:
            response = template_food.utterance(selector='general_templates/acknowledge_no_childfood',
                                               slots={}, user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)

        return response

    def a_ask_yes_no_eat_healthy(self):
        r"""
        acknowledge the question where we ask if they will concern about how to eat healthy.
        (1) answer_yes: congratulation them on being a healthy person.
        (2) answer_no: encourage them to eat more healthy.
        (3) others: general acknowledge
        """
        if self.user_intent['food_i_answeryes']:
            response = template_food.utterance(selector='general_templates/acknowledge_yes_eat_healthy', slots={},
                                               user_attributes_ref=self.user_attributes)
        elif self.user_intent['food_i_answerno']:
            response = template_food.utterance(selector='general_templates/acknowledge_no_eat_healthy', slots={},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_personal_suggestion(self):
        r"""
        Ask if people would like to share personal suggestion
        (1) People give some longer response
        (2) People give short response which is either yes or I don't know
        """
        if len(self.user_utterance.split()) > 6 or self.context["conversation_context"]["detected_food"]:
            response = template_food.utterance(selector='general_templates/acknowledge_give_suggestion', slots={},
                                               user_attributes_ref=self.user_attributes)
            if self.context["conversation_context"]["detected_food"] and len(self.user_utterance.split()) < 3:
                paraphrase_response = self.acknowledge_paraphrase() + "?"
                response = " ".join([paraphrase_response, response])
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_favorite_country_food(self):
        r"""
        Ask people's favorite country food
        (1) If people mention a country food name, then address it.
        (2) If people didn't say anything, then just general acknowledge
        """
        if self.context["conversation_context"]["current_food_country"]:
            country_food = FOOD_COUNTRY_LIST[self.context[
                "conversation_context"]["current_food_country"]]

            response = template_food.utterance(selector='general_templates/acknowledge_favorite_country_food', slots={"country": country_food},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_favorite_dish(self):
        r"""
        Ask people's favorite dish in a specific cuisine
        (1) If we detect food or the response is less than two words, paraphrase it.
        (2) Otherwise, general acknowledge
        """
        if self.context["conversation_context"]["detected_food"] and len(self.user_utterance.split()) < 6:
            paraphrase_response = self.acknowledge_paraphrase()

            response = template_food.utterance(selector='general_templates/paraphrase_favorite_dish', slots={"paraphrase": paraphrase_response},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_personal_opinion(self):
        r"""
        acknowledge the question where we ask about people's personal experience. For example,
        do you like cooking food for yourself or you like eating in the restaurant.
        (1) if the answer is more than three words, we appreciate for users to share their experience.
        (2) if the answer is less than three words, respond with general acknowledgement.
        """
        if len(self.user_utterance.split()) > 3:
            response = template_food.utterance(selector='general_templates/appreciate_share_opinion', slots={},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_whether_grow_own_food(self):
        r"""
        acknowledge the question whether they grow their own food
        (1) if answer_yes, then express your interest to learn about their experience.
        (2) if answer_no, then indicate maybe one day they will want to do it.
        (3) General
        """
        if self.user_intent['food_i_answeryes']:
            response = template_food.utterance(selector='general_templates/acknowledge_yes_grow_own_food', slots={},
                                               user_attributes_ref=self.user_attributes)
        elif self.user_intent['food_i_answerno']:
            response = template_food.utterance(selector='general_templates/acknowledge_no_grow_own_food', slots={},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_what_grow_own_food(self):
        r"""
        Ask what food have grown before
        (1) If we detect food or the response is less than three words, paraphrase it.
        (2) Otherwise, general acknowledge
        """
        if self.context["conversation_context"]["detected_food"] and len(self.user_utterance.split()) < 6:
            paraphrase_response = self.acknowledge_paraphrase()

            response = template_food.utterance(selector='general_templates/paraphrase_grow_food', slots={"paraphrase": paraphrase_response},
                                               user_attributes_ref=self.user_attributes)
        elif self.context["conversation_context"]["detected_food"]:
            response = template_food.utterance(selector='general_templates/acknowledge_what_grow_food', slots={"food": self.context["conversation_context"]["detected_food"][0]['food']},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_strange_food(self):
        r"""
        Ask what food have grown before
        (1) If we detect food or the response is less than three words, paraphrase it.
        (2) Otherwise, general acknowledge
        """
        detected_noun_phrase = self.sys_info['features'].get('noun_phrase', [])
        detected_noun_phrase.sort(key=lambda s: len(s.split()))
        detected_noun_phrase.reverse()
        detected_food = self.context["conversation_context"]["detected_food"]

        if detected_food:
            response = template_food.utterance(selector='general_templates/acknowledge_unusual_food', slots={"food": detected_food[0]['food']}, user_attributes_ref=self.user_attributes)
        elif detected_noun_phrase and len(self.user_utterance.split()) < 6:
            paraphrase_response = self.acknowledge_paraphrase()

            response = template_food.utterance(selector='general_templates/paraphrase_strange_food', slots={"paraphrase": paraphrase_response},
                                               user_attributes_ref=self.user_attributes)
        elif detected_noun_phrase:
            response = template_food.utterance(selector='general_templates/acknowledge_strange_food', slots={"food": detected_noun_phrase[0]},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_whether_adventurous_eater(self):
        r"""
        Ask whether the user is an adventurous eater
        (1) If yes, appreciate their bravity
        (2) If no, then acknowledge that you where the same
        """
        if self.user_intent['food_i_answeryes']:
            response = template_food.utterance(selector='general_templates/acknowledge_yes_adventurous_eater', slots={},
                                               user_attributes_ref=self.user_attributes)
        elif self.user_intent['food_i_answerno']:
            response = template_food.utterance(selector='general_templates/acknowledge_no_adventurous_eater', slots={},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_best_beginner_sushi(self):
        r"""
        Ask best beginner_sushi
        (1) If user says I don't know or answer uncertain, just say that is okay. You will checkout with someone else
        (2) If user answers a detected_noun_phrase and the len(self.user_utterance.split()) < 6, paraphrase the answer and thank the user.
        """
        detected_noun_phrase = self.sys_info['features'].get('noun_phrase', [])
        detected_noun_phrase.sort(key=lambda s: len(s.split()))
        detected_noun_phrase.reverse()

        if self.user_intent['food_i_answeruncertain']:
            response = template_food.utterance(
                selector='general_templates/acknowledge_best_begineer_sushi_uncertain', slots={}, user_attributes_ref=self.user_attributes)
        elif detected_noun_phrase:
            response = template_food.utterance(selector='general_templates/acknowledge_best_beginner_sushi', slots={"suggestion": detected_noun_phrase[0]},
                                               user_attributes_ref=self.user_attributes)
        elif len(self.user_utterance.split()) < 6:
            paraphrase_response = self.acknowledge_paraphrase()

            response = template_food.utterance(selector='general_templates/acknowledge_best_beginner_sushi', slots={"suggestion": paraphrase_response},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)

        return response

    def a_ask_whether_pizza_topping(self):
        r"""
        acknowledge the question do you like plain pizza over pizza with toppings
        (1) if answer_yes. Acknowledge the opinion that the user likes plain pizza.
        (2) if answer_no, Mention that you feel the same~
        (3) General
        """
        if self.user_intent['food_i_answeryes']:
            response = template_food.utterance(selector='general_templates/acknowledge_yes_pizza_topping', slots={},
                                               user_attributes_ref=self.user_attributes)
        elif self.user_intent['food_i_answerno']:
            response = template_food.utterance(selector='general_templates/acknowledge_no_pizza_topping', slots={},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_favorite_pizza_topping(self):
        r"""
        acknowledge the question what is your favorite pizza topping?
        (1) if detected food or match any of the topping list. Paraphrase the topping or the detected food and address that you would try it.
        (2) if nothing is detected. Just general acknowledge it.
        (3) General
        """
        detected_food = self.context[
            "conversation_context"]["detected_food"].copy()
        topping_list = ["pepperoni", "mushrooms", "onions", "sausage", "bacon",
                        "extra cheese", "black olives", "green peppers", "pineapple", "spinach"]
        detected_topping = [x['food'] for x in detected_food] + \
            [x for x in topping_list if re.search(x, self.user_utterance)]

        if "pineapple" in detected_topping or "suasage" in detected_topping:
            if "pineapple" in detected_topping:
                topping = "pineapple"
            else:
                topping = "suasage"
            response = template_food.utterance(selector='general_templates/acknowledge_favorite_pizza_topping_same', slots={"topping": topping},
                                               user_attributes_ref=self.user_attributes)
        elif detected_topping:
            topping = random.choice(detected_topping)
            response = template_food.utterance(selector='general_templates/acknowledge_favorite_pizza_topping_different', slots={"topping": topping},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

        # if self.user_intent['food_i_answeryes']:
        #     response = template_food.utterance(selector='general_templates/acknowledge_yes_pizza_topping', slots={},
        #                                        user_attributes_ref=self.user_attributes)
        # elif self.user_intent['food_i_answerno']:
        #     response = template_food.utterance(selector='general_templates/acknowledge_no_pizza_topping', slots={},
        #                                        user_attributes_ref=self.user_attributes)
        # else:
        #     response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
        #                                        user_attributes_ref=self.user_attributes)
        return response

    def a_ask_better_nystrip_ribeye(self):
        r"""
        acknowledge the question nystrip or ribeye? which one is better.
        (1) if detected nystrip, acknowledge the advantage of nystrip and appreciate the share of experience.
        (2) if detected ribeye, acknowledge the advantage of ribeye and appreciate the share of experience.
        (3) If nothing is detected and the length of the comments is larger than 3 words, appreciate the sharing
        (4) If nothing is detected and the length of the comments is less than 3 words, general acknowledge
        """
        ny_strip_keywords = [r"\bnew york\b", r"\bstrip\b", r"\bny\b"]
        ribeye_keywords = [r"\bribeye\b"]

        if any([re.search(x, self.user_utterance) for x in
                ny_strip_keywords]):
            response = template_food.utterance(selector='general_templates/acknowledge_nystrip_better', slots={},
                                               user_attributes_ref=self.user_attributes)
        elif any([re.search(x, self.user_utterance) for x in ribeye_keywords]):
            response = template_food.utterance(selector='general_templates/acknowledge_ribeye_better', slots={},
                                               user_attributes_ref=self.user_attributes)
        elif len(self.user_utterance.split()) > 3:
            response = template_food.utterance(selector='general_templates/appreciate_share_opinion', slots={},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_how_to_eat_peanut_butter(self):
        r"""
        acknowledge the question how to eat peanut butter.
        (1) if detected food, paraphrase the detected food? if sanwitch is detected, address the same way.
        (2) Sentence > 3 words, appreciate for share opinion
        (3) Other case would be general acknowledgement
        """
        detected_food = self.context[
            "conversation_context"]["detected_food"].copy()

        if re.search(r"sandwitch", self.user_utterance):
            response = template_food.utterance(selector='general_templates/acknowledge_shared_answer', slots={},
                                               user_attributes_ref=self.user_attributes)
        elif detected_food and any([x['food'] != "peanut butter" for x in detected_food]):
            for x in detected_food:
                if x['food'] != "peanut_butter":
                    paraphrased_food = x
                    break
            response = template_food.utterance(selector='general_templates/acknowledge_eatting_peanut_butter_different', slots={"food": paraphrased_food},
                                               user_attributes_ref=self.user_attributes)
        elif len(self.user_utterance.split()) > 3:
            response = template_food.utterance(selector='general_templates/appreciate_share_opinion', slots={},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_whether_instant_traditional_ramen(self):
        r"""
        acknowledge the question are you a traditional ramen lover or instant ramen lover.
        (1) if traditional is detected, appreciate traditional ramen.
        (2) elif instan is detected, appreciate instant  ramen.
        (3) else appreciate for sharing thoughts
        """

        if re.search(r"traditional", self.user_utterance):
            response = template_food.utterance(selector='general_templates/acknowledge_traditional_ramen', slots={},
                                               user_attributes_ref=self.user_attributes)
        elif re.search(r"instant", self.user_utterance):

            response = template_food.utterance(selector='general_templates/acknowledge_instant_ramen', slots={},
                                               user_attributes_ref=self.user_attributes)
        elif len(self.user_utterance.split()) > 3:
            response = template_food.utterance(selector='general_templates/appreciate_share_opinion', slots={},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_interest_in_ppa_question(self):
        r"""
        acknowledge whether people know the answer toward a ppa question.
        TODO:
        1. If user provide a longer answer
        1. Add acknowledge if people do know the answer
        2. Add acknowledge if people don't know the answer
        3. Add ackonwlegde if people says i don't know

        """
        if len(self.user_utterance.split()) > 3:
            response = template_food.utterance(selector='general_templates/appreciate_share_opinion', slots={},
                                               user_attributes_ref=self.user_attributes)
        elif self.user_intent["food_i_answeryes"]:
            response = template_food.utterance(selector='general_templates/acknowledge_know_ppa_answer', slots={},
                                               user_attributes_ref=self.user_attributes)
        elif self.user_intent["food_i_answerno"]:
            response = template_food.utterance(selector='general_templates/acknowledge_not_know_ppa_answer', slots={},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                                          user_attributes_ref=self.user_attributes)

        return response

    def a_ask_cuisine_favorite_dish(self):
        """
        acknowledge people's favorite dish in a cuisine
        """
        detected_food = self.context[
            "conversation_context"]["detected_food"].copy()
        current_cuisine = self.context[
            "conversation_context"]["current_cuisine"]
        if detected_food:
            # acknowledge the detected food
            sampled_food = random.choice(detected_food)
            # update the current food
            self.context["conversation_context"]["current_food"] = sampled_food
            response = template_food.utterance(selector='general_templates/acknowledge_favorite_cuisine_dish', slots={"food": sampled_food['food'], "cuisine": current_cuisine},
                                               user_attributes_ref=self.user_attributes)
        elif len(self.user_utterance.split()) > 4:
            response = template_food.utterance(selector='general_templates/appreciate_share_experience', slots={},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                                          user_attributes_ref=self.user_attributes)

        return response

    def a_ask_cuisine_favorite_restaurant(self):
        """
        acknowledge people's favorite cuisine restaurant
        """
        logging.debug("[FOODMODULE] googlekg from returnnlp_util: {}".format(self.returnnlp_util.googlekg))
        #logging.debug("[FOODMODULE] detected_entities from returnnlp_util: {}".format(self.returnnlp_util.detect_entities))
        detected_restaurants = detect_food_restaurant(
            self.returnnlp_util.googlekg)
        current_cuisine = self.context[
            "conversation_context"]["current_cuisine"]

        if detected_restaurants:
            sampled_restaurant = random.choice(detected_restaurants)
            response = template_food.utterance(selector='general_templates/acknowledge_favorite_cuisine_restaurant', slots={"restaurant": sampled_restaurant, "cuisine": current_cuisine},
                                               user_attributes_ref=self.user_attributes)
        elif len(self.user_utterance.split()) >= 2:
            response = template_food.utterance(selector='general_templates/acknowledge_where_best_food', slots={},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_yes_no_cook_cuisine(self):
        r"""
        acknowledge yes_no cook question.
        (1)The User answer yes.
        (2)The User answer no.
        (3)The User answer something else.
        """
        current_cuisine = self.context[
            "conversation_context"]["current_cuisine"]

        if self.user_intent['food_i_answeryes']:
            response = template_food.utterance(selector='general_templates/acknowledge_yes_cook_cuisine', slots={"cuisine": current_cuisine},
                                               user_attributes_ref=self.user_attributes)
        elif self.user_intent['food_i_answerno']:
            response = template_food.utterance(selector='general_templates/acknowledge_no_cook_cuisine', slots={"cuisine": current_cuisine},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_yes_no_cook_dish(self):
        r"""
        acknowledge yes_no cook question.
        (1)The User answer yes.
        (2)The User answer no.
        (3)The User answer something else.
        """
        current_food = self.context[
            "conversation_context"]["current_food"]["food"]

        if self.user_intent['food_i_answeryes']:
            response = template_food.utterance(selector='general_templates/acknowledge_yes_cook_dish', slots={"food": current_food},
                                               user_attributes_ref=self.user_attributes)
        elif self.user_intent['food_i_answerno']:
            response = template_food.utterance(selector='general_templates/acknowledge_no_cook_dish', slots={"food": current_food},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response


    def a_ask_yes_no_learn_recipe(self):
        r"""
        acknowledge yes_no cook question on learning a new recipe for corona virus.
        (1)The User answer yes.
        (2)The User answer no.
        (3)The User answer something else.
        """
        current_cuisine = self.context[
            "conversation_context"]["current_cuisine"]

        if self.user_intent['food_i_answeryes']:
            response = template_food.utterance(selector='general_templates/acknowledge_yes_learn_recipe', slots={},
                                               user_attributes_ref=self.user_attributes)
        elif self.user_intent['food_i_answerno']:
            response = template_food.utterance(selector='general_templates/acknowledge_no_learn_recipe', slots={},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_followup_recipe(self):
        r"""
        acknowledge ask whether to followup_recipe
        """
        if self.user_intent['food_i_answeryes']:
            response = ""
        elif self.user_intent['food_i_answerno'] or re.search(r"(can you skip .* recipe)", self.user_utterance):
            response = template_food.utterance(selector='general_templates/acknowledge_no_learn_recipe', slots={},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_foodstock_quarantine(self):
        r"""
        acknowledge ask what food to buy for quarantine
        (1) If food is detected, give positive acknowledgement to that food. 
        (2) if the word "food" is detected and short, paraphrase and acknowledge people's choice. 
        (3) Other, try with the new idea of acknowledge sentence that is hard to be understood.
        """
        detected_food = self.context[
            "conversation_context"]["detected_food"].copy()
        if detected_food:
            sample_food = random.choice(detected_food)['food']
            response = template_food.utterance(selector='general_templates/acknowledge_quarantine_food', slots={"food": sample_food},
                                               user_attributes_ref=self.user_attributes)
        elif re.search("\bfood\b", self.user_utterance):
            paraphrased_response = self.acknowledge_paraphrase()
            response = paraphrase_response + ' ' + template_food.utterance(selector='general_templates/acknowledge_quarantine_food_general', slots={"paraphrased_response": paraphrased_response},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)
        return response

    def a_ask_preference_cook_restaurant(self):
        r"""
        acknowledge ask does user prefer cook or restaurant
        (1) If restaurant is detected, acknowledge on advantage of restaurant over cooking 
        (2) If cook is detected, acknowledge on advantage of cooking. ask_preference_cook_restaurant
        (3) if he is half & half, say something different.
        (4) long utterance, appreciate for shared experience
        (5) others: general acknowledgement. 
        """
        #TODO: Improve the detection on preference on restaurant than cook
        if re.search("restaurant", self.user_utterance):
            response = template_food.utterance(selector='general_templates/acknowledge_preference_restaurant', slots={},
                                               user_attributes_ref=self.user_attributes)
        elif re.search("cook", self.user_utterance):
            response = template_food.utterance(selector='general_templates/acknowledge_preference_cook', slots={},
                                               user_attributes_ref=self.user_attributes)
        elif re.search(r"half|like both|both .* good", self.user_utterance):
            response = template_food.utterance(selector='general_templates/acknowledge_preference_restaurant_and_cook', slots={}, user_attributes_ref=self.user_attributes)
        elif len(self.user_utterance.split()) > 5:
            response = template_food.utterance(selector='general_templates/appreciate_share_experience', slots={},
                                               user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={},
                                               user_attributes_ref=self.user_attributes)

        return response
    
    def a_ask_how_to_find_restaurant(self):
        r"""
        acknowledge ask does user prefer cook or restaurant
        (1) If website or related answer detected, acknowledge the website and recommend some other websites.
        (2) If they say something related to people, acknowledge people are great resources but also recommend them to checkout yelp
        (3) general_acknowledgement and comment on both the advantage of website and friends. 
        """
        WEB_INTRO = {
            "yelp": "The best thing about yelp is that it has abundant options to filter out the best restaurant for you, including food types, price range, distance from you and so on.",
            "zomato": "One thing i really like about zomato is that it really focus on social aspect where a lot of their users share unique restaurant experience. So that, you can easily find something tasty and trendy at the same time.",
            "zagat": "Zagat differentiates itself from the other web resources by only having ratings and reviews that are curated by Zagat editors rather than anyone. If you really want to get the best of the best, i would highly recommend zagat to you. ",
            "opentable": "The best thing i like about opentable is that you can easily make a reversation after you find a good restaurant on it. If you are the type of person who only find restaurants when you want to go there soon, then open table is a must.",
            }
        RESTAURANT_DATABASE = ["yelp", "zomato", "zagat", "foursquare", "zagat", "opentable", "happycow"]

        if re.search(r"yelp|zomato|zagat|foursquare|zagat|opentable|happycow|uber eats|grubhub", self.user_utterance):
            detected_websites = [x for x in RESTAURANT_DATABASE if re.search(x, self.user_utterance)]
            recommend_web  = "yelp"
            for x in WEB_INTRO.keys():
                if x not in detected_websites:
                    recommend_web = x
                    break
            sampled_web = random.choice(detected_websites)
            #generate the response
            response = template_food.utterance(selector='general_templates/acknowledge_detected_restaurant_web', slots={"user_website":sampled_web, "recommend_website": recommend_web, "recommend_reason": WEB_INTRO[recommend_web]}, user_attributes_ref=self.user_attributes)
        elif re.search(r"website|internet|google|\bweb", self.user_utterance) and not self.user_intent["food_i_notinterest"]:
            recommend_web = random.choice(list(WEB_INTRO.keys()))
            response  = template_food.utterance(selector='general_templates/acknowledge_nondetected_restaurant_web', slots={"recommend_website": recommend_web, "recommend_reason": WEB_INTRO[recommend_web]}, user_attributes_ref=self.user_attributes)
        elif re.search(r"friend|roommate|people|family|parents|dad|mom", self.user_utterance):
            recommend_web = random.choice(list(WEB_INTRO.keys()))
            response = template_food.utterance(selector = 'general_templates/acknowledge_find_restaurant_person', slots={"recommend_website": recommend_web, "recommend_reason": WEB_INTRO[recommend_web]}, user_attributes_ref=self.user_attributes)
        else:
            #general acknowledgement
            response = template_food.utterance(selector='general_templates/acknowledge_restaurant_web_general', slots={}, user_attributes_ref=self.user_attributes)

        return response

    def a_ask_eat_often_dish(self):
        r"""
        acknowledge ask do they eat dish often or just save it for occasional moment
        """
        current_food = self.context["conversation_context"]["current_food"]["food"]
        if not self.user_intent['food_i_answerno'] and re.search(r"often|a lot|everyday", self.user_utterance):
            response = template_food.utterance(selector='general_templates/acknowledge_eat_dish_often', slots={"food": current_food}, user_attributes_ref=self.user_attributes)
        elif (self.user_intent['food_i_answerno'] and re.search(r"often|a lot|everyday", self.user_utterance)) or re.search(r"occasinally|special", self.user_utterance):
            response = template_food.utterance(selector='general_templates/acknowledge_eat_dish_notoften', slots={}, user_attributes_ref=self.user_attributes)
        else:
            #general acknowledgement
            response = template_food.utterance(selector='general_templates/acknowledge_restaurant_web_general', slots={}, user_attributes_ref=self.user_attributes)

        return response

    def a_ask_yes_no_secret_food_recipe(self):
        r"""
        acknowledge ask whether they have a recipe to share about a dish
        1. answer_yes_short
        2. answer_yes_long
        3. answer_no_short
        4. long_response
        5. other 
        """
        current_food = self.context["conversation_context"]["current_food"]["food"]
        if self.user_intent['food_i_answeryes'] and len(self.user_utterance.split()) < 3:
            response = template_food.utterance(selector='general_templates/acknowledge_yes_share_recipe', slots={}, user_attributes_ref=self.user_attributes)
        elif self.user_intent['food_i_answerno'] and len(self.user_utterance.split()) < 3:
            response = template_food.utterance(selector='general_templates/acknowledge_no_share_recipe', slots={}, user_attributes_ref=self.user_attributes)
        elif len(self.user_utterance.split()) > 5:
            response = template_food.utterance(selector='general_templates/acknowledge_appreciate_share_recipe', slots={"food": current_food}, user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={}, user_attributes_ref=self.user_attributes)

        return response

    def a_ask_how_raw_food_cook(self):
        r"""
        acknowledge ask how to cook raw food
        1. If key words is captured, paraphrase keywords + food and apprecaite the cooking mehtod
        2. Generally mention that any way to cook sounds delicious
        """
        current_food = self.context["conversation_context"]["current_food"]["food"]
        cooking_method = {"fry": "fried", "grill": "grilled", "roast": "roasted", "steam": "steamed", "poach": "poached", "simmer": "simmered", "broil": "broiled", "blanch": "blanched", "braise": "braised", "stew": "stewed"}
        detected_method = None
        for key in cooking_method.keys():
            if re.search(key, self.user_utterance):
                detected_method = cooking_method[key]

        if detected_method and not self.user_intent['food_i_answerno']:
            response  = template_food.utterance(selector='general_templates/acknowledge_cook_raw_food', slots={"cooking_method": detected_method, "food": current_food}, user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_cook_raw_food_general', slots={"food": current_food}, user_attributes_ref=self.user_attributes)

        return response

    def a_ask_raw_food_dish(self):
        r"""
        acknowledge ask how to cook raw food
        1. If we detect dish, acknowlegde the dish is reall tasty!
        2. IF we detect something that containing the original food, then paraphrase and acknowledge that we want to try that. 
        3. Otherwise, we will just do general acknowledgement 
        """
        current_food = self.context["conversation_context"]["current_food"]["food"]
        detected_food = self.context["conversation_context"]["detected_food"]
        detected_dish = None
        for food in detected_food:
            if food["category"] == "dish":
                detected_dish = food["food"]

        if detected_dish:
            response = template_food.utterance(selector='general_templates/acknowledge_raw_food_dish', slots={"dish": detected_dish}, user_attributes_ref=self.user_attributes)
        elif re.search(current_food, self.user_utterance) and len(self.user_utterance.split()) < 5:
            paraphrased_response = self.acknowledge_paraphrase()
            response = template_food.utterance(selector='general_templates/acknowledge_raw_food_dish_notdetected', slots={"paraphrased_response": paraphrased_response}, user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={}, user_attributes_ref=self.user_attributes)

        return response

    def a_ask_yes_no_drink_partner(self):
        r"""
        acknowledge ask yes_no_draink partner
        1. detected food acknowledge food must be really great
        2. answer yes, acknowledge yes answer
        3. answer no, acknowledge no answer
        4. general
        """
        current_food = self.context["conversation_context"]["current_food"]["food"]
        detected_food = self.context["conversation_context"]["detected_food"]
        sampled_food = None
        for food in detected_food:
            if food["food"] != "beer":
                sampled_food = food["food"]

        if sampled_food:
            #sampled_food = detected_food[0]['food']
            response = template_food.utterance(selector='general_templates/acknowledge_drink_partner_detected', slots={"food": sampled_food, "drink": current_food}, user_attributes_ref=self.user_attributes)
        elif self.user_intent['food_i_answeryes']:
            response = template_food.utterance(selector='general_templates/acknowledge_drink_partner_yes', slots={"drink": current_food},
                                               user_attributes_ref=self.user_attributes)
        elif self.user_intent['food_i_answerno']:
            response = template_food.utterance(selector='general_templates/acknowledge_drink_partner_no', slots={"drink": current_food},
                                               user_attributes_ref=self.user_attributes)
        elif len(self.user_utterance.split()) > 5:
            response = template_food.utterance(selector='general_templates/appreciate_share_experience', slots={}, user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={}, user_attributes_ref=self.user_attributes)

        return response

    def a_ask_what_best_drink_partner(self):
        r"""
        acknowledge ask what best drink partner
        1. detected food, acknowledge food must be really great
        2. Long response, appreciate sharing!
        3. Other, general response
        """
        current_food = self.context["conversation_context"]["current_food"]["food"]
        detected_food = self.context["conversation_context"]["detected_food"]
        sampled_food = None
        for food in detected_food:
            if food["food"] != "beer":
                sampled_food = food["food"]

        if sampled_food:
            response = template_food.utterance(selector='general_templates/acknowledge_drink_partner_detected', slots={"food": sampled_food, "drink": current_food}, user_attributes_ref=self.user_attributes)
        elif len(self.user_utterance.split()) > 5:
            response = template_food.utterance(selector='general_templates/appreciate_share_experience', slots={}, user_attributes_ref=self.user_attributes)
        else:
            response = template_food.utterance(selector='general_templates/acknowledge_general', slots={}, user_attributes_ref=self.user_attributes)

        return response


class Food_State_Management(object):
    r"""
    The Food Dialog State Management
    """

    def __init__(self, user_utterance, user_intent, context, sys_info, user_attributes):
        # syncronize the input information
        self.user_utterance = user_utterance
        self.user_intent = user_intent
        # This will be modified, let's check if it will also modify the context
        self.context = context
        self.sys_info = sys_info
        self.user_attributes = user_attributes

        # Initialize previous_state
        if self.context["conversation_context"]["previous_state"] == "food_exit" or self.context["conversation_context"]["previous_state"] is None:
            self.context["conversation_context"][
                "previous_state"] = "food_none"
        self.previous_state = self.context[
            "conversation_context"]["previous_state"]

        # Initialize Acknowledgement_Status
        self.food_acknowledgement = Food_Acknowledgement(
            user_utterance, user_intent, context, user_attributes, sys_info)

        # Initialize the transition and response states
        self.__bind_states()

        # 6. TODO: Initialize the Acknowledge System
        # logging.info(
        #     "[FOODMODULE] Finish initializing Food_State_Management Object")

    def __bind_states(self):
        self.state_mapping = {
            "food_none": {"transition": self.t_food_none, "response": None},
            "food_exit": {"transition": None, "response": self.s_food_exit},
            "food_chitchat_ask": {"transition": self.t_food_chitchat_ask, "response": self.s_food_chitchat_ask},
            "food_chitchat_answer": {"transition": self.t_food_chitchat_answer, "response": self.s_food_chitchat_answer},
            "food_answer_question": {"transition": self.t_food_answer_question, "response": self.s_food_answer_question},
            "food_provide_recipe": {"transition": self.t_food_provide_recipe, "response": self.s_food_provide_recipe},
            "food_recommend_restaurant_location": {"transition": self.t_food_recommend_restaurant_location, "response": self.s_food_recommend_restaurant_location},
            "food_recommend_restaurant_cuisine": {"transition": self.t_food_recommend_restaurant_cuisine, "response": self.s_food_recommend_restaurant_cuisine},
            "food_recommend_restaurant": {"transition": self.t_food_recommend_restaurant, "response": self.s_food_recommend_restaurant},
        }

    def get_response(self):
        # Get the Current State
        self.state_mapping[self.previous_state]['transition']()

        # Get the response
        response = self.state_mapping[self.current_state]['response']()

        if self.context["conversation_context"]["fail_question_answer"]:
            self.previous_state = "food_answer_question"
            self.current_state = "food_chitchat_ask"
            response = " ".join([response, self.state_mapping[
                self.current_state]['response']()])

        # Update the Context Previous State
        self.context["conversation_context"][
            "previous_state"] = self.current_state

        return response

    ###########################Transition States Functions####################
    def t_food_none(self):
        if self.user_intent['food_i_notinterestfoodchat'] or self.context["conversation_context"]["unclear"]:
            r"""
            Transit to exit state
            """
            self.current_state = "food_exit"

        elif self.user_intent['food_i_requestrestaurant'] and self.context["conversation_context"]["restaurant_info"].get("city_id", None) is None:
            r"""
            Transit to recommend_restaurant
            """
            self.current_state = "food_recommend_restaurant_location"

        elif self.user_intent['food_i_requestrestaurant'] and self.context["conversation_context"]["restaurant_info"].get("cuisine_id", None) is None and self.context["conversation_context"]["restaurant_info"].get("category_id", None) is None:

            self.current_state = "food_recommend_restaurant_cuisine"

        elif self.user_intent['food_i_requestrestaurant']:
            r"""
            Transit to recommend_restaurant
            """
            self.current_state = "food_recommend_restaurant"

        elif self.user_intent['food_i_requestrecipe']:
            r"""
            Transit to request_recipe
            """
            self.current_state = "food_provide_recipe"

        elif self.user_intent['food_i_suggestfoodchat']:
            r"""
            Transit to general question answering
            """
            self.current_state = "food_chitchat_ask"

        elif self.user_intent['food_i_askquestion']:
            r"""
            Transit to answer question state
            """
            self.current_state = "food_answer_question"

        elif self.user_intent['food_i_suggestfood'] and self.context["conversation_context"]["previous_state"] != "food_recommend_restaurant_cuisine":
            r"""
            Transit to food_ask_question
            """
            self.current_state = "food_chitchat_ask"
        elif self.user_intent['food_i_suggestfoodcuisine'] and self.context["conversation_context"]["previous_state"] != "food_recommend_restaurant_cuisine":
            r"""
            Transit to food_ask_question
            """
            self.current_state = "food_chitchat_ask"
        else:
            r"""
            Transit to food_ask_question with chitchat_type equal to "general"
            """
            self.current_state = "food_chitchat_ask"

    def t_food_chitchat_ask(self):
        if self.user_intent['food_i_notinterestfoodchat'] or self.context["conversation_context"]["unclear"]:
            r"""
            Transit to exit state
            """
            self.current_state = "food_exit"

        elif self.user_intent['food_i_requestrestaurant'] and self.context["conversation_context"]["restaurant_info"].get("city_id", None) is None:
            r"""
            Transit to recommend_restaurant
            """
            self.current_state = "food_recommend_restaurant_location"

        elif self.user_intent['food_i_requestrestaurant'] and self.context["conversation_context"]["restaurant_info"].get("cuisine_id", None) is None and self.context["conversation_context"]["restaurant_info"].get("category_id", None) is None:
            self.current_state = "food_recommend_restaurant_cuisine"

        elif self.user_intent['food_i_requestrestaurant']:
            r"""
            Transit to recommend_restaurant
            """
            self.current_state = "food_recommend_restaurant"

        elif self.user_intent['food_i_requestrecipe']:
            r"""
            Transit to request_recipe
            """
            self.current_state = "food_provide_recipe"
        elif self.user_intent['food_i_suggestfoodchat']:
            r"""
            Transit to general question answering
            """
            self.current_state = "food_chitchat_ask"

        elif self.user_intent['food_i_askquestion']:
            r"""
            Transit to answer question state
            """
            self.current_state = "food_answer_question"

        elif self.user_intent['food_i_suggestfood'] and self.context["conversation_context"]["previous_state"] != "food_recommend_restaurant_cuisine":
            r"""
            Transit to food_ask_question
            """
            self.current_state = "food_chitchat_ask"
        # elif self.user_intent['food_i_notinterest']:
        #     r"""
        #     Transit to chitchat_ask if people express not interest, and chitchat_type is equalt to "general"
        #     """
        #     self.current_state = "food_chitchat_ask
        elif self.user_intent['food_i_suggestfoodcuisine'] and self.context["conversation_context"]["previous_state"] != "food_recommend_restaurant_cuisine":
            r"""
            Transit to food_ask_question
            """
            self.current_state = "food_chitchat_ask"
        elif self.user_intent['food_i_answerabandoned']:
            r"""
            Stay with the previous_state
            """
            self.current_state = self.context[
                'conversation_context']['previous_state']

        elif self.user_intent['food_i_answeruncertain']:
            r"""
            Transit to food_ask_question without self disclosure
            """
            self.current_state = "food_chitchat_ask"
        elif self.context["conversation_context"]["expect_next_state"]:
            self.current_state = self.context[
                "conversation_context"]["expect_next_state"]
        else:
            self.current_state = "food_chitchat_answer"

    def t_food_chitchat_answer(self):
        if self.user_intent['food_i_notinterestfoodchat'] or self.context["conversation_context"]["unclear"]:
            r"""
            Transit to exit state
            """
            self.current_state = "food_exit"
        elif self.user_intent['food_i_requestrestaurant'] and self.context["conversation_context"]["restaurant_info"].get("city_id", None) is None:
            r"""
            Transit to recommend_restaurant
            """
            self.current_state = "food_recommend_restaurant_location"

        elif self.user_intent['food_i_requestrestaurant'] and self.context["conversation_context"]["restaurant_info"].get("cuisine_id", None) is None and self.context["conversation_context"]["restaurant_info"].get("category_id", None) is None:
            self.current_state = "food_recommend_restaurant_cuisine"

        elif self.user_intent['food_i_requestrestaurant']:
            r"""
            Transit to recommend_restaurant
            """
            self.current_state = "food_recommend_restaurant"

        elif self.user_intent['food_i_requestrecipe']:
            r"""
            Transit to request_recipe
            """
            self.current_state = "food_provide_recipe"

        elif self.user_intent['food_i_suggestfoodchat']:
            r"""
            Transit to general question answering
            """
            self.current_state = "food_chitchat_ask"

        elif self.user_intent['food_i_askquestion']:
            r"""
            Transit to answer question state
            """
            self.current_state = "food_answer_question"

        elif self.user_intent['food_i_suggestfood'] and self.context["conversation_context"]["previous_state"] != "food_recommend_restaurant_cuisine":
            r"""
            Transit to food_ask_question
            """
            # self.context["conversation_context"]["chitchat_type"] = "food"
            self.current_state = "food_chitchat_ask"
        # elif self.user_intent['food_i_notinterest']:
        #     r"""
        #     Transit to chitchat_ask if people express not interest, and chitchat_type is equalt to "general"
        #     """
        #     # self.context["conversation_context"]["chitchat_type"] = "general"
        #     self.current_state = "food_chitchat_ask"
        elif self.user_intent['food_i_suggestfoodcuisine'] and self.context["conversation_context"]["previous_state"] != "food_recommend_restaurant_cuisine":
            r"""
            Transit to food_ask_question
            """
            self.current_state = "food_chitchat_ask"
        else:
            self.current_state = self.context[
                "conversation_context"]["expect_next_state"]

    def t_food_answer_question(self):
        if self.user_intent['food_i_notinterestfoodchat'] or self.context["conversation_context"]["unclear"]:
            r"""
            Transit to exit state
            """
            self.current_state = "food_exit"

        elif self.user_intent['food_i_requestrestaurant'] and self.context["conversation_context"]["restaurant_info"].get("city_id", None) is None:
            r"""
            Transit to recommend_restaurant
            """
            self.current_state = "food_recommend_restaurant_location"

        elif self.user_intent['food_i_requestrestaurant'] and self.context["conversation_context"]["restaurant_info"].get("cuisine_id", None) is None and self.context["conversation_context"]["restaurant_info"].get("category_id", None) is None:
            self.current_state = "food_recommend_restaurant_cuisine"

        elif self.user_intent['food_i_requestrestaurant']:
            r"""
            Transit to recommend_restaurant
            """
            self.current_state = "food_recommend_restaurant"

        elif self.user_intent['food_i_requestrecipe']:
            r"""
            Transit to request_recipe
            """
            self.current_state = "food_provide_recipe"

        elif self.user_intent['food_i_suggestfoodchat']:
            r"""
            Transit to general question answering
            """
            self.current_state = "food_chitchat_ask"

        elif self.user_intent['food_i_askquestion']:
            r"""
            Transit to answer question state
            """
            self.current_state = "food_answer_question"

        elif self.user_intent['food_i_suggestfood'] and self.context["conversation_context"]["previous_state"] != "food_recommend_restaurant_cuisine":
            r"""
            Transit to food_ask_question
            """
            # self.context["conversation_context"]["chitchat_type"] = "food"
            self.current_state = "food_chitchat_ask"
        # elif self.user_intent['food_i_notinterest']:
        #     r"""
        #     Transit to chitchat_ask if people express not interest, and chitchat_type is equalt to "general"
        #     """
        #     # self.context["conversation_context"]["chitchat_type"] = "general"
        #     self.current_state = "food_chitchat_ask"
        elif self.user_intent['food_i_suggestfoodcuisine'] and self.context["conversation_context"]["previous_state"] != "food_recommend_restaurant_cuisine":
            r"""
            Transit to food_ask_question
            """
            self.current_state = "food_chitchat_ask"
        elif self.context["conversation_context"]["expect_next_state"]:
            # TODO: When more submodules built this should be changed
            if self.user_intent['food_i_answerno']:
                self.current_state = "food_chitchat_ask"
            else:
                self.current_state = self.context[
                    "conversation_context"]["expect_next_state"]
        else:
            self.current_state = "food_chitchat_ask"

    def t_food_provide_recipe(self):
        if self.user_intent['food_i_notinterestfoodchat'] or self.context["conversation_context"]["unclear"]:
            r"""
            Transit to exit state
            """
            self.current_state = "food_exit"

        elif self.user_intent['food_i_requestrestaurant'] and self.context["conversation_context"]["restaurant_info"].get("city_id", None) is None:
            r"""
            Transit to recommend_restaurant
            """
            self.current_state = "food_recommend_restaurant_location"

        elif self.user_intent['food_i_requestrestaurant'] and self.context["conversation_context"]["restaurant_info"].get("cuisine_id", None) is None and self.context["conversation_context"]["restaurant_info"].get("category_id", None) is None:
            self.current_state = "food_recommend_restaurant_cuisine"

        elif self.user_intent['food_i_requestrestaurant']:
            r"""
            Transit to recommend_restaurant
            """
            self.current_state = "food_recommend_restaurant"

        elif self.user_intent['food_i_requestrecipe']:
            r"""
            Transit to request_recipe
            """
            self.current_state = "food_provide_recipe"

        elif self.user_intent['food_i_suggestfoodchat']:
            r"""
            Transit to general question answering
            """
            self.current_state = "food_chitchat_ask"

        elif self.user_intent['food_i_askquestion']:
            r"""
            Transit to answer question state
            """
            self.current_state = "food_answer_question"

        elif self.user_intent['food_i_suggestfood'] and self.context["conversation_context"]["previous_state"] != "food_recommend_restaurant_cuisine":
            r"""
            Transit to food_ask_question
            """
            # self.context["conversation_context"]["chitchat_type"] = "food"
            self.current_state = "food_chitchat_ask"

        elif self.user_intent['food_i_suggestfoodcuisine'] and self.context["conversation_context"]["previous_state"] != "food_recommend_restaurant_cuisine":
            r"""
            Transit to food_ask_question
            """
            self.current_state = "food_chitchat_ask"

        elif self.context["conversation_context"]["expect_next_state"]:
            self.current_state = self.context[
                "conversation_context"]["expect_next_state"]
        else:
            self.current_state = "food_chitchat_ask"

    def t_food_recommend_restaurant_location(self):
        if self.user_intent['food_i_notinterestfoodchat'] or self.context["conversation_context"]["unclear"]:
            r"""
            Transit to exit state
            """
            self.current_state = "food_exit"
        elif self.user_intent['food_i_requestrestaurant'] and self.context["conversation_context"]["restaurant_info"].get("city_id", None) is None:
            r"""
            Transit to recommend_restaurant
            """
            self.current_state = "food_recommend_restaurant_location"

        elif self.user_intent['food_i_requestrestaurant'] and self.context["conversation_context"]["restaurant_info"].get("cuisine_id", None) is None and self.context["conversation_context"]["restaurant_info"].get("category_id", None) is None:
            r"""
            Transit to recommend_restaurant_cuisine
            """
            self.current_state = "food_recommend_restaurant_cuisine"

        elif self.user_intent['food_i_requestrestaurant']:
            r"""
            Transit to recommend_restaurant
            """
            self.current_state = "food_recommend_restaurant"
        elif self.user_intent['food_i_requestrecipe']:
            r"""
            Transit to request_recipe
            """
            self.current_state = "food_provide_recipe"

        elif self.user_intent['food_i_suggestfoodchat']:
            r"""
            Transit to general question answering
            """
            self.current_state = "food_chitchat_ask"

        elif self.user_intent['food_i_askquestion']:
            r"""
            Transit to answer question state
            """
            self.current_state = "food_answer_question"

        elif self.user_intent['food_i_suggestfood'] and self.context["conversation_context"]["previous_state"] != "food_recommend_restaurant_cuisine":
            r"""
            Transit to food_ask_question
            """
            # self.context["conversation_context"]["chitchat_type"] = "food"
            self.current_state = "food_chitchat_ask"
        elif self.user_intent['food_i_suggestfoodcuisine'] and self.context["conversation_context"]["previous_state"] != "food_recommend_restaurant_cuisine":
            r"""
            Transit to food_ask_question
            """
            self.current_state = "food_chitchat_ask"

        elif self.context["conversation_context"]["expect_next_state"]:
            if self.context["conversation_context"]["detected_city"] and self.context["conversation_context"]["expect_next_state"] == "food_chitchat_ask":
                self.current_state = "food_recommend_restaurant_location"
            else:
                self.current_state = self.context[
                    "conversation_context"]["expect_next_state"]
        else:
            self.current_state = "food_chitchat_ask"

    def t_food_recommend_restaurant_cuisine(self):
        if self.user_intent['food_i_notinterestfoodchat'] or self.context["conversation_context"]["unclear"]:
            r"""
            Transit to exit state
            """
            self.current_state = "food_exit"
        elif self.user_intent['food_i_requestrestaurant'] and self.context["conversation_context"]["restaurant_info"].get("city_id", None) is None:
            r"""
            Transit to recommend_restaurant
            """
            self.current_state = "food_recommend_restaurant_location"

        elif self.user_intent['food_i_requestrestaurant'] and self.context["conversation_context"]["restaurant_info"].get("cuisine_id", None) is None and self.context["conversation_context"]["restaurant_info"].get("category_id", None) is None:
            r"""
            Transit to recommend_restaurant_cusine
            """
            self.current_state = "food_recommend_restaurant_cuisine"

        elif self.user_intent['food_i_requestrestaurant']:
            r"""
            Transit to recommend_restaurant
            """
            self.current_state = "food_recommend_restaurant"
        elif self.user_intent['food_i_requestrecipe']:
            r"""
            Transit to request_recipe
            """
            self.current_state = "food_provide_recipe"

        elif self.user_intent['food_i_suggestfoodchat']:
            r"""
            Transit to general question answering
            """
            self.current_state = "food_chitchat_ask"

        elif self.user_intent['food_i_askquestion']:
            r"""
            Transit to answer question state
            """
            self.current_state = "food_answer_question"

        elif self.user_intent['food_i_suggestfood'] and self.context["conversation_context"]["previous_state"] != "food_recommend_restaurant_cuisine":
            r"""
            Transit to food_ask_question
            """
            # self.context["conversation_context"]["chitchat_type"] = "food"
            self.current_state = "food_chitchat_ask"

        elif self.user_intent['food_i_suggestfoodcuisine'] and self.context["conversation_context"]["previous_state"] != "food_recommend_restaurant_cuisine":
            r"""
            Transit to food_ask_question
            """
            self.current_state = "food_chitchat_ask"

        elif self.context["conversation_context"]["expect_next_state"]:
            if (self.context["conversation_context"]["detected_cuisine"] or self.context["conversation_context"]["detected_cuisine"]) and self.context["conversation_context"]["expect_next_state"] == "food_chitchat_ask":
                self.current_state = "food_recommend_restaurant_cuisine"
            else:
                self.current_state = self.context[
                    "conversation_context"]["expect_next_state"]
        else:
            self.current_state = "food_chitchat_ask"

    def t_food_recommend_restaurant(self):
        if self.user_intent['food_i_notinterestfoodchat'] or self.context["conversation_context"]["unclear"]:
            r"""
            Transit to exit state
            """
            self.current_state = "food_exit"
        elif self.user_intent['food_i_requestrestaurant'] and self.context["conversation_context"]["restaurant_info"].get("city_id", None) is None:
            r"""
            Transit to recommend_restaurant
            """
            self.current_state = "food_recommend_restaurant_location"

        elif self.user_intent['food_i_requestrestaurant'] and self.context["conversation_context"]["restaurant_info"].get("cuisine_id", None) is None and self.context["conversation_context"]["restaurant_info"].get("category_id", None) is None:
            r"""
            Transit to recommend_restaurant_cusine
            """
            self.current_state = "food_recommend_restaurant_cuisine"

        elif self.user_intent['food_i_requestrestaurant']:
            r"""
            Transit to recommend_restaurant
            """
            self.current_state = "food_recommend_restaurant"
        elif self.user_intent['food_i_requestrecipe']:
            r"""
            Transit to request_recipe
            """
            self.current_state = "food_provide_recipe"

        elif self.user_intent['food_i_suggestfoodchat']:
            r"""
            Transit to general question answering
            """
            self.current_state = "food_chitchat_ask"

        elif self.user_intent['food_i_askquestion']:
            r"""
            Transit to answer question state
            """
            self.current_state = "food_answer_question"

        elif self.user_intent['food_i_suggestfood'] and self.context["conversation_context"]["previous_state"] != "food_recommend_restaurant_cuisine":
            r"""
            Transit to food_ask_question
            """
            # self.context["conversation_context"]["chitchat_type"] = "food"
            self.current_state = "food_chitchat_ask"

        elif self.user_intent['food_i_suggestfoodcuisine'] and self.context["conversation_context"]["previous_state"] != "food_recommend_restaurant_cuisine":
            r"""
            Transit to food_ask_question
            """
            self.current_state = "food_chitchat_ask"
        else:
            self.current_state = "food_chitchat_ask"
    ##########################################################################

    ###########################Response States Functions######################
    def s_food_exit(self):
        # Initialize Terms for Every State
        user_intents = self.user_intent
        user_context = self.context
        propose_continue = user_context["propose_continue"]
        user_utterance = self.user_utterance

        # Initialize the Start and Main Body Response
        start_response = ""
        main_body_response = ""

        # Reinitialize the expec_next_state
        self.context["conversation_context"]["expect_next_state"] = None

        # update the missed_times context
        self.context["conversation_context"]["missed_times"] = 0

        if user_context["conversation_context"]["unclear"]:
            detected_other_topic = MODULE_KEYWORD_MAP.get(
                user_context["conversation_context"]["propose_topic"], None)
            if detected_other_topic:
                main_body_response = template_food.utterance(selector='general_templates/exit_food_module_topic', slots={'other_topic': detected_other_topic},
                                                             user_attributes_ref=self.user_attributes)
            else:
                main_body_response = template_food.utterance(selector='general_templates/exit_food_module_no_topic', slots={},
                                                             user_attributes_ref=self.user_attributes)
        else:
            main_body_response = template_food.utterance(selector='general_templates/exit_food_module_sure', slots={},
                                                         user_attributes_ref=self.user_attributes)

        # Construct the Final Response
        propose_continue = 'STOP'
        # Update the propose_continue status
        self.context["propose_continue"] = propose_continue
        response = ' '.join([start_response, main_body_response])

        return response

    def s_food_chitchat_ask(self):
        r"""
        Generate Chitchat Ask Response
        """

        # Initialize Terms for Every State
        user_intents = self.user_intent
        user_context = self.context
        user_utterance = self.user_utterance
        propose_continue = user_context["propose_continue"]

        # Initialize Chitchat_Type fot chitchat_ask Mode
        chitchat_type = user_context["conversation_context"]["chitchat_type"]

        # Initialize Start_Response and Main_Body_Response
        start_response = ""
        main_body_response = ""

        if user_context["conversation_context"]["previous_state"] != "food_chitchat_ask":
            selected_question = {}
        else:
            selected_question = user_context[
                "conversation_context"]["selected_question"]

        # Reinitialize the expec_next_state
        self.context["conversation_context"]["expect_next_state"] = None

        # Define the Opening of the Food_Chat
        start_response = self.food_acknowledgement.get_acknowledge()
        #logging.debug("[FOODMODULE] start_response is: {}".format(start_response))

        # Initialize the followup_questions
        followup_questions = user_context[
            "conversation_context"]["followup_questions"].copy()


        def select_general_chitchat_q(self):
            """
            Design the logic to sample general chitchat questions
            """
            selected_q = None
            #update the chitchat_type
            self.context["conversation_context"]["chitchat_type"] = "general"
            #check majority group
            #logging.debug("[FOODMODULE] num_maority_chitchat is : {}".format(self.context["user_context"]["num_majority_chitchat"]))
            if self.context["user_context"]["num_majority_chitchat"] < TOTAL_NUM_Q_GENERAL_MAJORITY:
                selected_q = template_food.utterance(selector='qa_templates/qa_general/majority', slots={},user_attributes_ref=self.user_attributes)
                self.context["user_context"]["num_majority_chitchat"] += 1
                #logging.debug("[FOODMODULE] num_maority_chitchat after update is : {}".format(self.context["user_context"]["num_majority_chitchat"]))
                while selected_q.get("subtopic", None) is not None and selected_q.get("subtopic", None) in self.context["user_context"]["talked_subtopic"] and self.context["user_context"]["num_majority_chitchat"] <= TOTAL_NUM_Q_GENERAL_MAJORITY:
                    selected_q = template_food.utterance(selector='qa_templates/qa_general/majority', slots={},user_attributes_ref=self.user_attributes)
                    self.context["user_context"]["num_majority_chitchat"] += 1
                #return selected_q
                if self.context["user_context"]["num_majority_chitchat"] <= TOTAL_NUM_Q_GENERAL_MAJORITY:
                    return selected_q
            
            #check minority group
            if self.context["user_context"]["num_minority_chitchat"] < TOTAL_NUM_Q_GENERAL_MINORITY:
                selected_q = template_food.utterance(selector='qa_templates/qa_general/minority', slots={},user_attributes_ref=self.user_attributes)
                self.context["user_context"]["num_minority_chitchat"] += 1

                while selected_q.get("subtopic", None) is not None and selected_q.get("subtopic", None) in self.context["user_context"]["talked_subtopic"] and self.context["user_context"]["num_minority_chitchat"] <= TOTAL_NUM_Q_GENERAL_MINORITY:
                    selected_q = template_food.utterance(selector='qa_templates/qa_general/minority', slots={},user_attributes_ref=self.user_attributes)
                    self.context["user_context"]["num_minority_chitchat"] += 1
                
                #return selected_q
                if self.context["user_context"]["num_minority_chitchat"] <= TOTAL_NUM_Q_GENERAL_MINORITY:
                    return selected_q
            
            #check least_minority group
            if self.context["user_context"]["num_least_minority_chitchat"] < TOTAL_NUM_Q_GENERAL_LEAST_MINORITY:
                selected_q = template_food.utterance(selector='qa_templates/qa_general/least_minority', slots={},user_attributes_ref=self.user_attributes)
                self.context["user_context"]["num_least_minority_chitchat"] += 1

                while selected_q.get("subtopic", None) is not None and selected_q.get("subtopic", None) in self.context["user_context"]["talked_subtopic"] and self.context["user_context"]["num_least_minority_chitchat"] <= TOTAL_NUM_Q_GENERAL_LEAST_MINORITY:
                    selected_q = template_food.utterance(selector='qa_templates/qa_general/least_minority', slots={},user_attributes_ref=self.user_attributes)
                    self.context["user_context"]["num_least_minority_chitchat"] += 1
                
                #return selected_q
                if self.context["user_context"]["num_least_minority_chitchat"] <= TOTAL_NUM_Q_GENERAL_LEAST_MINORITY:
                    return selected_q

            return selected_q

        def select_food_chitchat_q(self, current_food, current_food_category='other'):
            selected_q = None
            talked_questions = self.context["user_context"]["talked_food"].get(current_food, [])
            current_food_key = ''.join(current_food.split())

            #PPA food vs general food
            if current_food_key in PPA_FOOD_LIST:
                #try chitchat quesiton
                if len([x for x in talked_questions if re.search(r'chitchat', x)]) < template_food.count_utterances(selector="qa_templates/qa_{}/chitchat".format(current_food_key)):
                    selected_q = template_food.utterance(selector='qa_templates/qa_{}/chitchat'.format(current_food_key), slots={}, user_attributes_ref=self.user_attributes)
                elif len([x for x in talked_questions if re.search(r'ppa', x)]) < template_food.count_utterances(selector="qa_templates/qa_{}/ppa".format(current_food_key)):
                    selected_q = template_food.utterance(selector='qa_templates/qa_{}/ppa'.format(current_food_key), slots={}, user_attributes_ref=self.user_attributes)
                elif len([x for x in talked_questions if re.search(current_food_category, x)]) < template_food.count_utterances(selector="qa_templates/qa_food/{}".format(current_food_category)):
                    selected_q = template_food.utterance(selector='qa_templates/qa_food/{}'.format(current_food_category), slots={}, user_attributes_ref=self.user_attributes)
            else:
                if len([x for x in talked_questions if re.search(current_food_category, x)]) < template_food.count_utterances(selector="qa_templates/qa_food/{}".format(current_food_category)):
                    selected_q = template_food.utterance(selector='qa_templates/qa_food/{}'.format(current_food_category), slots={}, user_attributes_ref=self.user_attributes)
            #update the talked food
            if selected_q is not None:
                self.context["user_context"]["talked_food"][current_food] = self.context["user_context"]["talked_food"].get(current_food, [])+[selected_q['q_id']]

            return selected_q
            

        # Reinitialize selected_question
        selected_question = {}

        # update the missed_times context
        self.context["conversation_context"]["missed_times"] = 0

        if followup_questions:
            if chitchat_type == "food":
                current_food = user_context[
                    "conversation_context"]["current_food"]["food"]
                talked_food = user_context[
                    "user_context"]["talked_food"]
                #define first_ppa_question
                first_ppa_question = False
            elif chitchat_type == "cuisine":
                current_cuisine = user_context[
                    "conversation_context"]["current_cuisine"]
                talked_cuisine = user_context["user_context"]["talked_cuisine"]
                # current_food =
                # user_context["conversation_context"]["current_food"]

            selected_question = followup_questions[-1]
            # Update the context by removing the selected questions
            # del self.context["conversation_context"]["followup_questions"][0]
            self.context["conversation_context"][
                "followup_questions"].pop()  # Get rid of the last question
        elif chitchat_type == "food":
            current_food = user_context["conversation_context"]["current_food"]['food']
            current_food_category = user_context["conversation_context"]["current_food"]['category']
            talked_questions = user_context["user_context"]["talked_food"].get(current_food, [])
            first_ppa_question = True
            # len(talked_quesitons) makes the length of the talked questions, 2
            # is the current limit set for special quesion talked about a
            # certain food type
            if len(talked_questions) < 2:
                if any([re.search('ppa', q_id) for q_id in talked_questions]):
                    first_ppa_question = False
                selected_question = select_food_chitchat_q(self, current_food, current_food_category)
            
            #logging.debug("[FOODMODULE] talked_questions: {}".format(self.context['user_context']["talked_food"]))
            # verify if selected_question
            if not selected_question:
                selected_question = select_general_chitchat_q(self)
                

        elif chitchat_type == "cuisine":
            current_cuisine = user_context[
                "conversation_context"]["current_cuisine"]
            talked_cuisine = user_context[
                "user_context"]["talked_cuisine"]
            talked_questions = talked_cuisine.get(current_cuisine, [])

            # Select the question from qa_cuisine
            # logging.debug("[FOODMODULE] used cuisine utterances:
            # {}".format(template_food.count_used_utterances(selector="qa_templates/qa_cuisine",
            # user_attributes_ref=self.user_attributes)))
            if len(talked_questions) < TOTAL_NUM_Q_CUISINE:
                selected_question = template_food.utterance(
                    selector='qa_templates/qa_cuisine', slots={}, user_attributes_ref=self.user_attributes)

                # Update the Context
                if not talked_questions:
                    # initialize the talked_questions for talked_food
                    self.context["user_context"]["talked_cuisine"][
                        current_cuisine] = [selected_question['q_id']]
                else:
                    # append the question id to existing list
                    self.context["user_context"]["talked_cuisine"][
                        current_cuisine].append(selected_question['q_id'])

                # Disable the current food
                self.context["conversation_context"]["current_food"] = {}
            else:
                selected_question = select_general_chitchat_q(self)

        else:  # When chitchat_type is general and no follow_up
            selected_question = select_general_chitchat_q(self)

        #logging.debug("[FOODMODULE] the used utterances in qa_general/majority is: {}".format(template_food.count_used_utterances(selector="qa_templates/qa_general/majority", user_attributes_ref=self.user_attributes)))
        # Now Define the Main Body Response
        if selected_question:

            # logging.info(
            #     "[FOODMOUDLE] selected_question is: {}".format(selected_question))

            # Use the question as the main_body_response
            if self.context["conversation_context"]["chitchat_type"] == "general":
                # Modify the start_response based on the previous_state
                # if user_context["conversation_context"]["previous_state"] == "food_none" and re.search("cook", self.user_utterance) is None and re.search("eat", self.user_utterance) is None:
                #     start_response = " ".join([start_response, template_food.utterance(selector='general_templates/start_food', slots={},
                #                                                                        user_attributes_ref=self.user_attributes)])
                if not followup_questions and random.uniform(0, 1) > 0.2 and not self.context["conversation_context"]["start_food_general"]:
                    start_response = " ".join([start_response, template_food.utterance(selector='general_templates/propose_another_food_general_question', slots={},
                                                                                       user_attributes_ref=self.user_attributes)])
                # Check if a certain format_key is in the text
                # Determine the Main_Body_Response
                # main_body_response = random.choice(
                #     selected_question['q'])['text']
                main_body_response = random.choice(
                    selected_question['q'])
                # Check if there is a special key in the response
                if re.search(r'\{country\}', main_body_response):
                    food_country = FOOD_COUNTRY_LIST[self.context[
                        "conversation_context"]["current_food_country"]]
                    main_body_response = main_body_response.format(
                        country=food_country)

            elif self.context["conversation_context"]["chitchat_type"] == "food":
                # main_body_response = random.choice(selected_question['q'])[
                #     'text'].format(food=current_food)
                # Get the tempaltes for question_grounding
                # selected_q_grounding =
                # template_food.utterance(selector='general_templates/')
                if not first_ppa_question:
                    selected_q_grounding_key = 'general_templates/ppa_question_grounding_continue'
                else:
                    selected_q_grounding_key = 'general_templates/ppa_question_grounding_intro'

                selected_q_grounding = template_food.utterance(selector=selected_q_grounding_key, slots={'food': current_food},
                                                               user_attributes_ref=self.user_attributes)
                main_body_response = random.choice(
                    selected_question['q']).format(food=current_food, question_grounding=selected_q_grounding)
            else:
                # The chitchat typr is "cuisine"
                # if len(talked_questions) == 1:
                # selected_q_grounding_key =
                # 'general_templates/ppa_question_grounding_intro'

                assert current_cuisine is not None, 'current_cuisine should not be None'
                if self.context["conversation_context"]["current_food"]:
                    main_body_response = random.choice(
                        selected_question['q']).format(cuisine=current_cuisine, food=self.context["conversation_context"]["current_food"]['food'])
                else:
                    main_body_response = random.choice(
                        selected_question['q']).format(cuisine=current_cuisine)

            # Update the context for selected question
            self.context["conversation_context"][
                "selected_question"] = selected_question

            # Update the context for followup_question
            if selected_question.get("follow_up", {}):
                '''
                self.context["conversation_context"][
                    "followup_questions"] += selected_question['follow_up'].get("general_followup", [])
                '''
                general_followup = selected_question[
                    'follow_up'].get("general_followup", [])
                if general_followup:
                    for x in general_followup:
                        # self.context["conversation_context"][
                        #     "followup_questions"].append(x['text'])
                        self.context["conversation_context"][
                            "followup_questions"].append(x)
        else:
            start_response = ""
            # Append the acknowledgement to the question
            start_response = self.food_acknowledgement.get_key_acknowledge()
            main_body_response = template_food.utterance(selector='general_templates/propose_quit_food_modules', slots={},
                                                         user_attributes_ref=self.user_attributes)
            # diable food from proposed topic
            disable_propose(self.user_attributes, "FOODCHAT")

            # Set the run_out_of_food to true
            self.context["user_context"]["run_out_of_food"] = True

            propose_continue = "STOP"
            self.context["propose_continue"] = propose_continue
            response = " ".join([start_response, main_body_response])
            return response

        # Now Define the Expect Next State Based on Selected_question
        if selected_question.get("a", None):
            self.context["conversation_context"][
                "expect_next_state"] = "food_chitchat_answer"
        else:
            self.context["conversation_context"][
                "expect_next_state"] = "food_chitchat_ask"

        propose_continue = 'CONTINUE'
        self.context["propose_continue"] = propose_continue

        # logging.info("[FOODMODULE] start_response is: {}".format(start_response))
        # logging.info("[FOODMODULE] main_body_response is:
        # {}".format(main_body_response))
        response = " ".join([start_response, main_body_response])
        # response = "I would like to ask you a food question"
        return response

    def s_food_chitchat_answer(self):
        r"""
        Generate Chitchat Answer Response
        """
        # 8.TODO: Include the logics to answer chitchat question in the
        # template

        # Initialize Terms for Every State
        user_intents = self.user_intent
        user_context = self.context
        user_utterance = self.user_utterance
        propose_continue = user_context["propose_continue"]
        selected_question = user_context[
            "conversation_context"]["selected_question"]

        # Initialize Start_Response and Main_Body_Response
        start_response = ""
        main_body_response = ""

        # Reinitialize the expec_next_state
        self.context["conversation_context"]["expect_next_state"] = None

        # update the missed_times context
        self.context["conversation_context"]["missed_times"] = 0

        # Get the Acknowledgement
        start_response = self.food_acknowledgement.get_acknowledge()

        # Get the Main Body Response
        selected_answer = selected_question['a']
        if type(selected_answer) is list:
            # main_body_response = random.choice(selected_answer)['text']
            main_body_response = random.choice(selected_answer)
        else:
            main_body_response = selected_answer

        if self.context["conversation_context"]["chitchat_type"] == "cuisine":
            main_body_response = main_body_response.format(
                cuisine=self.context["conversation_context"]["current_cuisine"])
        # propose_continue = 'CONTINUE'
        self.context["propose_continue"] = propose_continue
        self.context["conversation_context"][
            "expect_next_state"] = 'food_chitchat_ask'

        # response = "here is my answer to my own question"
        response = " ".join([start_response, main_body_response])

        return response

    def s_food_answer_question(self):
        r"""
        Generate Question Answer Response
        """
        # Initialize Terms for Every State
        user_intents = self.user_intent
        user_context = self.context
        user_utterance = self.user_utterance
        propose_continue = user_context["propose_continue"]
        detected_food = self.context["conversation_context"]["detected_food"]

        # Flip the chitchat_type when detect the food

        # TODO: Implement better logic to transit to food chitchat
        # if detected_food:
        #     # Change the chatmode to food

        #     self.context["conversation_context"]["chitchat_type"] = "food"

        #     self.context["conversation_context"][
        #         "expect_next_state"] = "food_chitchat_ask"
        #     # clear all the followup questions
        #     self.context["conversation_context"]["followup_questions"] = []

        #     # clear the selected_question
        #     self.context["conversation_context"]["selected_question"] = {}

        
        # Initilaize start_response and main_body_response
        start_response = ""
        main_body_response = ""

        # Reinitialize the expec_next_state
        self.context["conversation_context"]["expect_next_state"] = None

        # update the missed_times context
        self.context["conversation_context"]["missed_times"] = 0

        #Initialize the food question handler
        food_question_handeler = FoodQuestionHandeler(self.user_utterance, self.sys_info, self.user_attributes)
        # test_question_handeler_response = food_question_handeler.handle_question()
        # logging.debug("[FOODMODULE] the response from question handeler is: {}".format(test_question_handeler_response.response))
        # logging.debug("[FOODMODULE] the tag of the response is: {}".format(test_question_handeler_response.tag))

        # Get the Acknowledgement, let's do 50%
        start_response = self.food_acknowledgement.get_acknowledge()

        if self.context["conversation_context"]["fail_question_answer"]:
            start_response = template_food.utterance(selector='general_templates/propose_talk_more_food', slots={},
                                                     user_attributes_ref=self.user_attributes)

        # First Check if it is a follow_up_question (TO BE IMPLEMENTED IN QUESTION HANDLER)
        if user_context["conversation_context"]["previous_state"] == "food_answer_question" or user_context["conversation_context"]["previous_state"] == "food_provide_recipe":
            followup_response = food_follow_up_question_answer(user_utterance, user_context)
            if followup_response:
                main_body_response = followup_response
                propose_continue = "CONTINUE"
                self.context["propose_continue"] = propose_continue
                response = ' '.join([start_response, main_body_response])
                self.context["conversation_context"]["backstory_reason"] = None
                return response

        # Then Check if it is abandoned (CAN BE IGNORED with the system detect abandoned)
        if user_intents['food_i_answerabandoned']:
            start_response = ""
            if user_intents['food_i_answerabandoned_not_question']:
                main_body_response = template_food.utterance(selector='general_templates/acknowledge_abandoned_answer', slots={},
                                                             user_attributes_ref=self.user_attributes)
                # Modity the expect_next_state as itself
                self.context["conversation_context"][
                    "expect_next_state"] = "food_chitchat_ask"
            else:
                main_body_response = template_food.utterance(selector='general_templates/acknowledge_abandoned_question', slots={},
                                                             user_attributes_ref=self.user_attributes)
                # Modity the expect_next_state as itself
                self.context["conversation_context"][
                    "expect_next_state"] = "food_answer_question"

            propose_continue = "CONTINUE"
            self.context["propose_continue"] = propose_continue
            response = ' '.join([start_response, main_body_response])
            self.context["conversation_context"]["backstory_reason"] = None
            return response

        # Get Backstory Response if the question is about opinion
        food_question_handeler_response = food_question_handeler.handle_question()
        if food_question_handeler_response and food_question_handeler_response.response and food_question_handeler_response.tag != "ack_question_idk":
            main_body_response = food_question_handeler_response.response
            #logging.debug("[FOODMODULE] bot propose module is: {}".format(food_question_handeler_response.bot_propose_module.value))
            if food_question_handeler_response.bot_propose_module:
                if food_question_handeler_response.bot_propose_module.value != "FOODCHAT":
                    self.context["conversation_context"]["propose_topic"] = food_question_handeler_response.bot_propose_module.value
                    propose_continue = "STOP"
                else:
                    propose_continue = "CONTINUE"
            else:
                propose_continue = "CONTINUE"

            self.context["propose_continue"] = propose_continue
            #Keep the reason if there is one
            if food_question_handeler_response.backstory_reason:
                self.context["conversation_context"]["backstory_reason"] = food_question_handeler_response.backstory_reason
            else:
                self.context["conversation_context"]["backstory_reason"] = None

            response = ' '.join([start_response, main_body_response])
            # if food_question_handeler_response.tag == "ack_question_idk":
            #     # Change the context to "fail_answer_question"
            #     self.context["conversation_context"]["fail_question_answer"] = True

            return response
        elif detected_food:
            # Provide Funfact with Today_I_Learnt
            selected_food = detected_food[0]['food']
            food_facts = get_food_reddit_posts(selected_food, limit=1)
            if food_facts:
                selected_food_fact = food_facts[0]
                selected_food_fact_hash = hash(selected_food_fact)
                if selected_food_fact_hash not in user_context['user_context']['provided_facts']:
                    main_body_response = template_food.utterance(selector='general_templates/unable_handle_question_with_funfact', slots={"food": selected_food, "fun_fact": selected_food_fact},
                                                                 user_attributes_ref=self.user_attributes)
                # Update the talked_facts
                self.context["user_context"][
                    "provided_facts"].append(selected_food_fact_hash)
                # Return the results
                propose_continue = "CONTINUE"
                self.context["propose_continue"] = propose_continue
                response = ' '.join(
                    [start_response, main_body_response])
                self.context["conversation_context"]["backstory_reason"] = None # update the backstory_reason
                return response
        elif food_question_handeler_response.tag == "ack_question_idk":
             #logging.debug("[FOODMODULE] bot propose module is: {}".format(food_question_handeler_response.bot_propose_module.value))
            main_body_response = food_question_handeler_response.response
            if food_question_handeler_response.bot_propose_module:
                if food_question_handeler_response.bot_propose_module.value != "FOODCHAT":
                    self.context["conversation_context"]["propose_topic"] = food_question_handeler_response.bot_propose_module.value
                    propose_continue = "STOP"
                else:
                    propose_continue = "CONTINUE"
            else:
                propose_continue = "CONTINUE"

            self.context["propose_continue"] = propose_continue
            #Keep the reason if there is one
            if food_question_handeler_response.backstory_reason:
                self.context["conversation_context"]["backstory_reason"] = food_question_handeler_response.backstory_reason
            else:
                self.context["conversation_context"]["backstory_reason"] = None

            response = ' '.join([start_response, main_body_response])
            # if food_question_handeler_response.tag == "ack_question_idk":
            #     # Change the context to "fail_answer_question"
            self.context["conversation_context"]["fail_question_answer"] = True

            return response
                
        main_body_response = template_food.utterance(selector='general_templates/unable_handle_question_general', slots={},
                                                     user_attributes_ref=self.user_attributes)
        main_body_response = unable_answer_question_error_handle(
            user_utterance, user_context, self.sys_info, self.user_attributes)
        propose_continue = "CONTINUE"
        # Change the context to "fail_answer_question"
        self.context["conversation_context"]["fail_question_answer"] = True

        # clear the question_answer intent
        self.user_intent["food_i_askquestion"] = False

        # Clear the selected_question
        self.context["conversation_context"]["selected_question"] = {}
        
        self.context["conversation_context"]["backstory_reason"] = None # update the backstory_reason
        # Update the propose_continue status
        self.context["propose_continue"] = propose_continue
        response = ' '.join([start_response, main_body_response])

        return response

        

    def s_food_provide_recipe(self):
        r"""
        Generate a Recipe based on User's Request. Let's make it a one turn reponse
        """
        # Initialize Terms for Every State
        user_intents = self.user_intent
        user_context = self.context
        user_utterance = self.user_utterance
        propose_continue = user_context["propose_continue"]
        detected_food = self.context["conversation_context"]["detected_food"]

        # Initialize Start_Response and Main_Body_Response
        start_response = ""
        main_body_response = ""

        # Reinitialize the expec_next_state
        self.context["conversation_context"]["expect_next_state"] = None

        # update the missed_times context
        self.context["conversation_context"]["missed_times"] = 0

        # Get the Acknowledgement
        start_response = self.food_acknowledgement.get_acknowledge()

        # Generate Response
        if user_context["conversation_context"]["detected_food"]:
            current_food = user_context["conversation_context"]["current_food"]['food']
            # Provide a recipe for that food.
            starter_for_food_recipe = template_food.utterance(selector='general_templates/start_food_recipe', slots={"food": current_food},
                                                              user_attributes_ref=self.user_attributes)
            start_response = ' '.join(
                [start_response, starter_for_food_recipe])

            # Get the recipe for that food
            request_recipe_utterance = "how to make {}".format(current_food)

            provided_recipe = get_food_evi_response(request_recipe_utterance)
        else:
            # Randomly Provide a recipe of a popular item
            provided_recipe = None
            # Randomize the order of Popular food list
            proposed_food_list = POPULAR_FOOD_LIST
            random.shuffle(proposed_food_list)

            for food in proposed_food_list:
                request_recipe_utterance = "how to make {}".format(food)
                if get_food_evi_response(request_recipe_utterance):
                    starter_for_food_recipe = template_food.utterance(selector='general_templates/start_random_food_recipe', slots={"food": food},
                                                                      user_attributes_ref=self.user_attributes)
                    start_response = ' '.join(
                        [start_response, starter_for_food_recipe])
                    provided_recipe = get_food_evi_response(
                        request_recipe_utterance)
                    break

        # generate the main body response based on the retrieval result
        if provided_recipe:
            main_body_response = postprocessing_evi_response(provided_recipe)
        else:
            start_response = ''
            if user_context["conversation_context"]["detected_food"]:
                main_body_response = template_food.utterance(selector='general_templates/unable_find_food_recipe', slots={"food": user_context["conversation_context"]["current_food"]['food']},
                                                             user_attributes_ref=self.user_attributes)
            else:
                main_body_response = template_food.utterance(selector='general_templates/unable_find_recipe_general', slots={},
                                                             user_attributes_ref=self.user_attributes)

        self.context["propose_continue"] = propose_continue
        response = ' '.join([start_response, main_body_response])
        # response = "Here is your recipe."

        return response

    def s_food_recommend_restaurant_location(self):
        # Initialize Terms for Every State
        user_intents = self.user_intent
        user_conversation_context = self.context["conversation_context"]
        user_utterance = self.user_utterance
        propose_continue = self.context["propose_continue"]
        # detected_food = self.context["conversation_context"]["detected_food"]

        # Initialize Start_Response and Main_Body_Response
        start_response = ""
        main_body_response = ""

        # Reinitialize the expec_next_state
        self.context["conversation_context"]["expect_next_state"] = None

        # update the missed_times context
        # self.context["conversation_context"]["missed_times"] = 0

        # Get the Acknowledgement
        # start_response = self.food_acknowledgement.get_acknowledge()

        current_city = user_conversation_context[
            'restaurant_info'].get('current_city_name', None)
        current_US_state = user_conversation_context[
            'restaurant_info'].get('current_state_name', None)

        # update the missed time
        if user_conversation_context['previous_state'] == 'food_recommend_restaurant_location':
            self.context["conversation_context"]["missed_times"] += 1
            # user_conversation_context['missed_times'] += 1

        if user_conversation_context["restaurant_info"].get("cuisine_id", None) or user_conversation_context["restaurant_info"].get("category_id", None):
            expect_next_state = "food_recommend_restaurnat"
        else:
            expect_next_state = "food_recommend_restaurant_cuisine"

        # Havn't confirm the city_id
        if user_conversation_context["missed_times"] == 0:
            # The first time ask about location
            if not current_city:
                main_body_response = template_food.utterance(selector='general_templates/ask_restaurant_location', slots={},
                                                             user_attributes_ref=self.user_attributes)
            elif not current_US_state:
                # #the user already state the city name but not he detected_state name
                # if user_intents["food_i_requestrestaurant"]:
                main_body_response = template_food.utterance(selector='general_templates/ask_city_state', slots={"city": current_city},
                                                             user_attributes_ref=self.user_attributes)
            else:
                main_body_response = template_food.utterance(selector='general_templates/appologize_for_no_match_position', slots={"city": current_city, "state": current_US_state},
                                                             user_attributes_ref=self.user_attributes)
                expect_next_state = "food_chitchat_ask"
                # reset the missed times
                self.context["conversation_context"]["missed_times"] = 0
                # reset the restaurant info
                self.context["conversation_context"]["restaurant_info"] = {}

        elif user_conversation_context["missed_times"] < 2:
            if not current_city:
                main_body_response = template_food.utterance(selector='general_templates/miss_restaurant_location', slots={},
                                                             user_attributes_ref=self.user_attributes)
            elif not current_US_state:
                # #the user already state the city name but not he detected_state name
                # if user_intents["food_i_requestrestaurant"]:
                main_body_response = template_food.utterance(selector='general_templates/ask_city_state', slots={"city": current_city},
                                                             user_attributes_ref=self.user_attributes)
            else:
                main_body_response = template_food.utterance(selector='general_templates/appologize_for_no_match_position', slots={"city": current_city, "state": current_US_state},
                                                             user_attributes_ref=self.user_attributes)
                expect_next_state = "food_chitchat_ask"

                # reset the missed times
                self.context["conversation_context"]["missed_times"] = 0

                # reset the restaurant info
                self.context["conversation_context"][
                    "restaurant_info"] = {}
        else:
            main_body_response = template_food.utterance(selector='general_templates/miss_restaurant_location_match_general', slots={},
                                                         user_attributes_ref=self.user_attributes)

            expect_next_state = "food_chitchat_ask"

            # reset the missed times
            self.context["conversation_context"]["missed_times"] = 0

            # reset the restaurant info
            self.context["conversation_context"]["restaurant_info"] = {}

        # Update the expect_next_state
        self.context["conversation_context"][
            "expect_next_state"] = expect_next_state
        self.context["propose_continue"] = propose_continue
        logging.info(
            "[FOODMODULE] proposed continue: {}".format(propose_continue))

        response = ' '.join([start_response, main_body_response])
        # response = "what is the location where you look for food?"
        return response

    def s_food_recommend_restaurant_cuisine(self):
        # Initialize Terms for Every State
        user_intents = self.user_intent
        user_context = self.context
        user_conversation_context = self.context["conversation_context"]
        user_utterance = self.user_utterance
        propose_continue = user_context["propose_continue"]
        # detected_food = self.context["conversation_context"]["detected_food"]

        # Initialize Start_Response and Main_Body_Response
        start_response = ""
        main_body_response = ""

        # Reinitialize the expec_next_state
        self.context["conversation_context"]["expect_next_state"] = None

        # update the missed_times context
        # self.context["conversation_context"]["missed_times"] = 0

        # Get the Acknowledgement
        start_response = self.food_acknowledgement.get_acknowledge()

        # update the missed time
        if user_conversation_context['previous_state'] == 'food_recommend_restaurant_cuisine':
            self.context["conversation_context"]["missed_times"] += 1
        elif user_conversation_context['previous_state'] == 'food_recommend_restaurant_location':
            self.context["conversation_context"]["missed_times"] = 0

        expect_next_state = "food_recommend_restaurant"

        # Havn't confirm the city_id
        if user_conversation_context["missed_times"] == 0:
            # The first time ask about cuisine
            main_body_response = template_food.utterance(selector='general_templates/ask_preference_for_cuisine', slots={},
                                                         user_attributes_ref=self.user_attributes)

        elif user_conversation_context["missed_times"] < 2:
            # Clarify with the user for the cusine type
            main_body_response = template_food.utterance(selector='general_templates/miss_cuisine_type', slots={},
                                                         user_attributes_ref=self.user_attributes)
        else:
            main_body_response = template_food.utterance(selector='general_templates/miss_cuisine_match_general', slots={},
                                                         user_attributes_ref=self.user_attributes)

            expect_next_state = "food_chitchat_ask"

            # reset the missed times
            self.context["conversation_context"]["missed_times"] = 0

            # reset the restaurant info
            # self.context["conversation_context"]["restaurant_info"] = {}

        self.context["conversation_context"][
            "expect_next_state"] = expect_next_state

        self.context["propose_continue"] = propose_continue

        response = ' '.join([start_response, main_body_response])
        # response = "what is the cusine of the restaurant you are looking for"
        return response

    def s_food_recommend_restaurant(self):
        # Initialize Terms for Every State
        user_intents = self.user_intent
        user_context = self.context
        user_conversation_context = self.context["conversation_context"]
        user_utterance = self.user_utterance
        propose_continue = user_context["propose_continue"]
        restaurant_info = user_conversation_context["restaurant_info"]
        # detected_food = self.context["conversation_context"]["detected_food"]

        # initialize city_id, current_city_name, cusine_id, category_id,
        # cusine_name, category_name
        city_id = restaurant_info['city_id']
        city_name = restaurant_info['current_city_name']
        cuisine_id = restaurant_info.get('cuisine_id', None)
        cuisine_name = restaurant_info.get('current_cuisine_name', None)
        category_id = restaurant_info.get('category_id', None)
        category_name = restaurant_info.get('current_category_name', None)
        logging.info(
            "[FOODMODULE] restaurant recommendation cuisine_name: {}".format(cuisine_name))
        logging.info(
            "[FOODMODULE] restaurant recommendation cuisine_id: {}".format(cuisine_id))

        # Initialize Start_Response and Main_Body_Response
        start_response = ""
        main_body_response = ""

        # Reinitialize the expec_next_state
        self.context["conversation_context"]["expect_next_state"] = None

        # update the missed_times context
        self.context["conversation_context"]["missed_times"] = 0

        # Get the Acknowledgement
        start_response = self.food_acknowledgement.get_acknowledge()

        # Get the resstaurants result!
        restaurant_results = search_zomato_restaurant(
            city_id, cuisine_id, category_id)

        if restaurant_results:
            selected_restaurant_id, main_body_response = format_recommendation_restaurant_response(
                city_name, cuisine_name, category_name, restaurant_results, self.user_attributes)
            # Record the selected_restaurant_id
            self.context["conversation_context"]["restaurant_info"][
                "selected_restaurant_id"] = selected_restaurant_id
        else:
            if not cuisine_id and not category_id:
                main_body_response = template_food.utterance(selector='general_templates/miss_restaurant_match_general', slots={"city": city_name},
                                                             user_attributes_ref=self.user_attributes)
            elif cuisine_id:
                main_body_response = template_food.utterance(selector='general_templates/miss_restaurant_match_cuisine', slots={"city": city_name, "cuisine": cuisine_name},
                                                             user_attributes_ref=self.user_attributes)
            else:
                main_body_response = template_food.utterance(selector='general_templates/miss_restaurant_match_category', slots={"city": city_name, "category": category_name},
                                                             user_attributes_ref=self.user_attributes)

        self.context["propose_continue"] = propose_continue

        response = ' '.join([start_response, main_body_response])
        # response = "here is your restaurant"
        return response

        #######################################################################
