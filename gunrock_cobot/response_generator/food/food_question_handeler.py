from nlu.question_detector import QuestionDetector
from question_handling.quetion_handler import QuestionHandler, QuestionResponse, QuestionResponseTag
from typing import Optional
backstory_threshold = 0.9 # todo: change threshold for you module

class FoodQuestionHandeler:
    def __init__(self, user_utterance, sys_info, user_attribute_ref):
        self.question_handler = QuestionHandler(
            user_utterance, 
            sys_info['features']["input_data_returnnlp"], 
            backstory_threshold,
            sys_info['features']["system_acknowledgement"], 
            user_attribute_ref # the same thing you pass to template maanger
        ) 
        self.question_detector = QuestionDetector(user_utterance, sys_info['features']["input_data_returnnlp"])
    def handle_question(self) -> Optional[QuestionResponse]:
        # # Step 1: check if it has question
        # if not self.question_detector.has_question():
        #     return None
        
        # # Step 2: Check if it's unanswerable 
        # if self.is_unanswerable_question():
        #     return QuestionResponse(response=self.question_handler.generate_i_dont_know_response())
        
        # # step 3: 
        # response = self.handle_topic_specific_question()
        # if response:
        #     return response
        
        # Step 1
        response = self.handle_general_question()
        # if response and response.tag != "ack_question_idk":
        #     return response
        if response:
            return response
        return None
        
    def handle_general_question(self) -> Optional[QuestionResponse]:
        answer = self.question_handler.handle_question()
        return answer