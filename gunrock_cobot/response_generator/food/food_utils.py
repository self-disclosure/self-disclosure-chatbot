import re
from cobot_common.service_client import get_client
import praw
import random
import logging
from constants_api_keys import COBOT_API_KEY
from nlu.constants import DialogAct as DialogActEnum
from nlu.command_detector import CommandDetector

logging.basicConfig(
    format='[%(levelname)s] %(asctime)s [%(filename)s:%(lineno)d] %(message)s',
)
from response_generator.food.food_api import initialize_app, ZOMATO_API_KEY
import template_manager


template_food = template_manager.Templates.food

# Create reddit keys to retrieve fun fact
reddit_keys = [{"REDDIT_CLIENT_SECRET": 'smytIRjC5L9lcjAFQkJjYV8asDY',
                "REDDIT_CLIENT_ID": 'aEE9fRbUYM3wWg'},
               {"REDDIT_CLIENT_SECRET": 'IEHzPvutOt4XkaPA9ZklmpwnwQw',
                "REDDIT_CLIENT_ID": '6-5cxgGJ1yF3XA'},
               {"REDDIT_CLIENT_SECRET": 'Fi6V_pCzKqE-4TRVqcmpMJdET2s',
                "REDDIT_CLIENT_ID": 'lKmLeMmSSLns6A'}]
key = random.choice(reddit_keys)
REDDIT_CLIENT_SECRET = key["REDDIT_CLIENT_SECRET"]
REDDIT_CLIENT_ID = key["REDDIT_CLIENT_ID"]

# Define a SENSITIVE_WORDS_LIST to filter out content
SENSITIVE_WORDS_LIST = [
    "gun",
    "revolver",
    "rifle",
    "weapon",
    "\)\)\<\>\(\(",
    "skewer",
    "blood",
    "cocaine",
    "heroin",
    "lsd",
    "meth",
    "stab",
    "shrooms",
    "ecstacy",
    "disaster",
    "injury",
    "fear",
    "ass",
    "asshole",
    "anal",
    "anus",
    "arsehole",
    "arse",
    "bitch",
    "bangbros",
    "bastards",
    "bitch",
    "tits",
    "butt",
    "blow job",
    "boob",
    "bra",
    "cock",
    "cum",
    "cunt",
    "dick",
    "shit",
    "sex",
    "erotic",
    "fuck",
    "fxxk",
    "f\*\*k",
    "f\*\*c",
    "f\*ck",
    "fcuk",
    "gang bang",
    "gangbang",
    "genital",
    "damn",
    "horny",
    "jerk off",
    "jerkoff",
    "kill",
    "loser",
    "masterbate",
    "naked",
    "nasty",
    "negro",
    "nigger",
    "nigga",
    "orgy",
    "pussy",
    "penis",
    "perve",
    "rape",
    "fucked",
    "fucking",
    "trump",
    "racist",
    "sexy",
    "strip club",
    "vagina",
    "faggot",
    "fag",
    "drug",
    "weed",
    "kill",
    "murder",
    "incel",
    "prostitute",
    "slut",
    "whore",
    "cuck",
    "cuckold",
    "shooting",
    "nazis",
    "nazism",
    "nazi",
    "sgw",
    "balls",
    "thot",
    "virgin",
    "cumshot",
    "cumming",
    "dildo",
    "vibrator",
    "sexual",
    "douche",
    "douche",
    "bukkake",
    "douchebag",
    "porn",
    "porngraph",
    "porngraphy",
    "die",
    "killed",
    "killing",
    "murdered",
    "murdering",
    "kills",
    "murders",
    "clit",
    "clitoris",
    "mushroom tatoo",
    "cheat",
    "cheated",
    "cheating",
    "dammit",
    "toxicity",
    "toxic",
    "wtf",
    "breasts",
    "tits",
    "boobies",
    "dead",
    "death",
    "bestial"]

# Define a list of hardcoded food that system cannot detect
MISDETECTED_FOOD_LIST = {"candy":'other', "orange chicken":'dish', "chicken foot":'raw', "hamburger":'dish', "alcohol": 'other', "cricket brownie": 'other'}

# Define the concept keys related to food
FOOD_CONCEPT_KEYS = ["food", "dish", "snack",
                     "beverage", "alcoholic beverage",
                     "traditional dish", "meat", "vegetable", "fruit",
                     "crop", "roll", "type of sushi", "cold water fish", "dairy product", "topping"]

FOOD_CONCEPT_MAP_CATEGORY = {"dish": "dish",
                             "traditional dish": "dish",
                             "meat": "raw",
                             "vegetable": "raw",
                             "fruit": "other", #To have new defined category
                             "roll": "dish",
                             "type of sushi": "other",
                             "food": "other",
                             "snack": "other",
                             "alcoholic beverage": "drink",
                             "beverage": "drink",
                             "food": "other",
                             "crop": "other",
                             "cold water fish": "raw",
                             "dairy product": "raw",
                             "topping": "raw"
                            }
FOOD_TAG_FIX = {'pizza': 'dish',
                'noodles': 'dish'}

WRONG_FOOD = ["saturated fat", "fat", "james", 'dad', 'drink alcohol']
WRONG_JUMPOUT = ["jennings", "internet", "tottenham", "soul", "whale", "often", "united state", "cricket brownie", "overload", "guatemala"]

# Define the list of keys of the topics not related to food
SYS_OTHER_TOPIC_LIST = ['topic_movietv', 'topic_health', 'topic_weather', 'topic_music', 'topic_book', 'topic_holiday',
                        'topic_techscience', 'topic_finance', 'topic_history', 'topic_game', 'topic_newspolitics', 'topic_saycomfort']  # 'topic_game', 'topic_others', topic travel disabled for now

SYS_OTHER_MODULE_LIST = ['WEATHER', 'BOOKCHAT',
                         'TECHSCIENCECHAT', 'GAMECHAT', 'SPORT', 'MOVIECHAT', 'FASHIONCHAT', 'HOLIDAYCHAT']

# Define the key to keywords mapping for reference, when we exit the module
TOPIC_KEYWORD_MAP = {'topic_movietv': 'movies',
                     'topic_sport': 'sports',
                     'topic_weather': 'weather',
                     'topic_music': 'music',
                     'topic_book': 'books',
                     'topic_game': 'games',
                     'topic_techscience': 'technology and science',
                     #'topic_travel':'travel',
                     'topic_animal': 'animals',
                     'topic_newspolitics': 'news',
                     'topic_holiday': 'holiday',
                     }

# Define the module word to key word mapping for reference, when we exit
# the module
MODULE_KEYWORD_MAP = {
    'MOVIECHAT': 'movies',
    'WEATHER': 'weather',
    'MUSICCHAT': 'music',
    'BOOKCHAT': 'books',
    'GAMECHAT': 'games',
    'TECHSCIENCECHAT': 'technology and science',
    'ANIMALCHAT': 'animals',
    'NEWS': 'news',
    'SPORT': 'sports',
    'FASIONCHAT': 'fashion',
    'HOLIDAYCHAT': 'holiday',
}

KEYWORD_MODULE_MAP = {
    'movies': 'MOVIECHAT',
    'weather': 'WEATHER',
    'music': 'MUSICCHAT',
    'books': 'BOOKCHAT',
    'technology and science': 'TECHSCIENCECHAT',  # disable it until version ready
    #'topic_travel': 'TRAVELCHAT',
    'animals': 'ANIMALCHAT',
    'games': 'GAMECHAT',
    'news': 'NEWS',
    'sports': 'SPORT',
    'fashion': 'FASIONCHAT',
    'holiday': 'HOLIDAYCHAT'
}

# Define the exception word list
EXCEPTION_KEY_WORDS = ["stomach", "mexican",
                       "all the time", "panda express", "davis", "things now", "indians", "meditation"]

# Define the PATTERN MANAGEMENT dict for regex
PATTERN_MANAGEMENT = {
    "ANSWER_UNCERTAIN": [
        r"(\bi\b|\bi'm\b).* (not.* sure|unsure|uncertain|(don't|do not).* know|(don't|do not).* have\s*(idea|an idea|answer|an answer))",
        r"\bnot.* sure|unsure|uncertain|(don't|do not).* know|(don't|do not).* have\s*(idea|an idea|answer|an answer)",
        r"(\bi\b).* (have|had).*(no idea)",
    ],
    "ANSWER_YES": [
        r"no problem|yes|yeah|^sure|^for sure|\bi\b(\bdo\b|\bam\b|\bwant to\b)|^i\b(\bdo\b|\bam\b|\bwant to\b|\blike\b|\blove\b)|^love it$"
    ],
    "ANSWER_NO": [
        r"\bnot|\bno\b|\bnope\b|(don't|do not) (like|enjoy|want)"
    ],
    "QUESTION_PATTERNS": [
        r"(^who\b|^what\b|^when\b|^where\b|^why\b|^how\b|^which\b|^do\b|^does\b|^did\b|^was\b|^is\b|^are\b|^were\b|^will\b|^would\b)\s*(was|is|were|are|will|would|did|do|does|can|could|you|he|she|him|her|they|we|i)",
        r"(^do|^don't|^did|^didn't)\s*(you)\s*(know|understand|hear|think|like)",
        r"(^who\b|^what\b|^when\b|^where\b|^why\b|^how\b|^which\b|^should\b|^will\b|^would\b)",
        r"(^can\b|^may\b|^should\b|^could\b)",
    ],
    "QUESTION_OPINION": [
        r"(who\b|what\b|when\b|where\b|why\b|which\b|how\b|can|know|curious|interest|hear|tell|give|let|do).* you.* (think|feel|like|love|thought|support|hate|believe|opinion|stance|view|say|favorite)"
    ],
    "SUGGEST_PATTERN": [
        r"(\blet's\b|\blet us\b)\s*(talk|chat)",
        r"(\bi\b)\s*(want to|expect to|would like to|wanna|really wanna|really want|used to play|enjoy|really enjoy)\s*(talk|chat|eat)",
        r"(\bhow about we\b|\bcan we\b|\bcould we\b)\s*(talk|chat)",
        r"(\bi)\s*(only like|like|love|eat|want|need)",
        r"(my favorite)\s*(food|dish|meat|vegetable|drink|meal)\s*(is|was)",
        r"tell me about"
    ],
    "NO_INTEREST_PATTERN": [
        r"(\bi\b).*(don't|do not|never).*(interested|like|love|talk about|want|eat|into|having|have)",
        r"(not|never).*(interested|like|love|talk about|want|into|having|have)",
        r"(\bother|\botherthings|\banything else|\bsomething else).*(you).*(tell|talk|chat)",
        r"\b(talk|chat).*(other|other things|something else|some other things|something)",
        r"(\bi)\s*(hate|dislike)"
    ],
    "QUIT_MODULE_PATTERN": [
        r"^finished$|^cancel$|^stop$",
        r"(^stop conversation$|^stop talk|^switch topic$|^change topic$)"
    ],

    "REQUEST_INFO_PATTERN": [
        r"tell me|give me",
        r"teach me"
    ],

    "REQUEST_INFO_QUESTION_PATTERNS": [
        r"(do you|can you)\s*(tell me|teach me|have|know about|give me|recommend|know .* recipe)",
        r"(can we)\s*(talk|chat)\s*(about)",
    ],

    "PLACES_FOR_FOOD_PATTERNS": [
        r"(place|restaurant|where).*(food|dinner|breakfast|lunch|eat)",
        r"(restaurant)",
        r"(food|dinner|breakfast|lunch|eat).*(place|restaurant)"
    ],
    "ASK_BACK_PATTERN": [
        r"how about you|what about you|^what do you think$|^do you$|^you$",
    ]
}

POPULAR_FOOD_LIST = ["cobb salad", "fajitas",
                     "banana split", "jambalaya", "biscuits gravy",
                     "chicken fried steak", "salmon", "california roll",
                     "meatloaf", "crab cakes", "cioppino", "peanut butter sandwich",
                     "baked beans", "clam chowder", "lobster rolls", "buffalo wings",
                     "barbecue ribs", "blueberry cobbler", "chicago-style pizza", "nachos",
                     "philly cheese steak", "hot dogs", "cheeseburger"]

FOOD_COUNTRY_LIST = {"vietnam": "vietnamese",
                     "greece": "greece",
                     "italy": "italian",
                     "pakistan": "pakistanis",
                     "philippines": "filipino",
                     "thailand": "thai",
                     "japan": "japanese",
                     "ukraine": "ukrainian",
                     "china": "chinese",
                     "india": "indian",
                     "spain": "spanish",
                     "france": "french",
                     "mexico": "mexican",
                     "switzerland": "switzerland",
                     "portugal": "portugese",
                     "korea": "korean",
                     "sweden": "swedish",
                     "montenegro": "montenegro",
                     "australia": "australian",
                     "america": "american",
                     "united states": "american",
                     "guatemala": "guatemala",
                     "africa": "african",
                     "asia": "asian",
                     "mongolia": "mongolian",
                     "indonesia": "indonesian",
                     "russia": "russian",
                     }

# FOOD_CUISINE_LIST = {"afghani": 6, "african": 152, "american": 1, "argentine": 151, "armenian": 175, "asian": 3,
#                      "australian": 131, "BBQ": 193, "bagels": 955, "bakery": 5, "bar food": 227, "beverages": 270, "brazilian": 159, "breakfast": 182,
#                      "british": 133, "bubble tea": 247, "burger": 168, "cafe": 30, "cajun": 491,
#                      "california": 956, "cambodian": 111, "cantonese": 121, "caribbean": 158, "chilean": 229, "chinese": 25,
#                      "coffee and tea": 161, "colombian": 287, "creole": 928, "crepes": 881, "cuban": 153, "deli": 192,
#                      "desserts": 100, "dim sum": 411, "diner": 541, "dominican": 958, "donuts": 959, "drinks only": 268, "east european": 651,
#                      "european": 38, "fast food": 40, "filipino": 112, "french": 45, "frozen yogurt": 501, "fusion": 274,
#                      "german": 134, "greek": 156, "grill": 181, "hawaiian": 521, "healthy food": 143, "ice cream": 233, "indian": 148, "indonesian": 114,
#                      "international": 154, "iranian": 140, "irish": 135, "italian": 55, "jamaican": 207, "japanese": 60, "jewish": 265, "juices": 164,
#                      "kebab": 178, "korean": 67, "latin american": 136, "malaysian": 69, "mediterranean": 70, "mexican": 73, "mongolian": 74,
#                      "moroccan": 147, "new american": 996, "pacific": 321, "pakistani": 139,
#                      "pizza": 82, "polish": 219, "portuguese": 87, "pub food": 983, "ramen": 320, "russian": 84,
#                      "salad": 998, "sandwich": 304, "scottish": 210, "seafood": 83, "sichuan": 128, "soul food": 461, "south american": 972,
#                      "spanish": 89, "sri lankan": 86, "steak": 141, "sushi": 177, "taco": 997, "taiwanese": 190,
#                      "tea": 163, "teriyaki": 964, "thai": 95, "turkish": 142, "vegetarian": 308, "vietnamese": 99}

FOOD_CUISINE_LIST = {"afghani": 6, "african": 152, "american": 1, "argentine": 151, "armenian": 175, "asian": 3,
                     "australian": 131,"brazilian": 159, "british": 133, "california": 956, "cambodian": 111, "cantonese": 121, "caribbean": 158, "chilean": 229, "chinese": 25,
                     "coffee and tea": 161, "colombian": 287, "creole": 928, "crepes": 881, "cuban": 153, "deli": 192, "dominican": 958, "donuts": 959, "drinks only": 268, "east european": 651,
                     "european": 38, "filipino": 112, "french": 45,"german": 134, "greek": 156, "hawaiian": 521, "indian": 148, "indonesian": 114,"international": 154, "iranian": 140, "irish": 135, "italian": 55, "jamaican": 207, "japanese": 60, "jewish": 265, "juices": 164,
                     "korean": 67, "latin american": 136, "malaysian": 69, "mediterranean": 70, "mexican": 73, "mongolian": 74, "moroccan": 147, "new american": 996, "pacific": 321, "pakistani": 139, "polish": 219, "portuguese": 87, 
                     "russian": 84,"scottish": 210, "seafood": 83, "sichuan": 128, "soul food": 461, "south american": 972,"spanish": 89, "sri lankan": 86, "taiwanese": 190,"thai": 95, "turkish": 142, "vietnamese": 99}

# FOOD_CUISINE_COUNTRY_LIST = ["afghani", "african", "american", "argentine", "armenian", "asian", "australian", "cambodian", "cantonese", "caribbean", "chilean", "chinese", "california", "colombian", "cuban", "dominican", "east european", 
#                      "european", "filipino", "french", "german", "greek", "hawaiian", "indian", "indonesian", "iranian", "irish", "italian", "jamaican", "japanese", "jewish", "korean", "latin american", "malaysian", "mediterranean",
#                      "mexican", "mogolian", "moroccan", "new american", "pakistani". "scottish", "sichuan", "spanish", "sri lankan", "taiwanese", "thai", "turkish", "vietnamese", "south american"
#                      ]

FOOD_CATEGORY_LIST = {"delivery": 1, "dine-out": 2, "night life": 3, "catching-up": 4, "takeaway": 5, "cafes": 6, "daily menus": 7, "breakfast": 8, "lunch": 9, "dinner": 10,
                      "pubs & bars": 11, "pocket friendly delivery": 13, "club & lounges": 14}

US_STATES_LIST = states = {
    'AK': 'Alaska',
    'AL': 'Alabama',
    'AR': 'Arkansas',
    'AS': 'American Samoa',
    'AZ': 'Arizona',
    'CA': 'California',
    'CO': 'Colorado',
    'CT': 'Connecticut',
    'DC': 'District of Columbia',
    'DE': 'Delaware',
    'FL': 'Florida',
    'GA': 'Georgia',
    'GU': 'Guam',
    'HI': 'Hawaii',
    'IA': 'Iowa',
    'ID': 'Idaho',
    'IL': 'Illinois',
    'IN': 'Indiana',
    'KS': 'Kansas',
    'KY': 'Kentucky',
    'LA': 'Louisiana',
    'MA': 'Massachusetts',
    'MD': 'Maryland',
    'ME': 'Maine',
    'MI': 'Michigan',
    'MN': 'Minnesota',
    'MO': 'Missouri',
    'MP': 'Northern Mariana Islands',
    'MS': 'Mississippi',
    'MT': 'Montana',
    'NA': 'National',
    'NC': 'North Carolina',
    'ND': 'North Dakota',
    'NE': 'Nebraska',
    'NH': 'New Hampshire',
    'NJ': 'New Jersey',
    'NM': 'New Mexico',
    'NV': 'Nevada',
    'NY': 'New York',
    'OH': 'Ohio',
    'OK': 'Oklahoma',
    'OR': 'Oregon',
    'PA': 'Pennsylvania',
    'PR': 'Puerto Rico',
    'RI': 'Rhode Island',
    'SC': 'South Carolina',
    'SD': 'South Dakota',
    'TN': 'Tennessee',
    'TX': 'Texas',
    'UT': 'Utah',
    'VA': 'Virginia',
    'VI': 'Virgin Islands',
    'VT': 'Vermont',
    'WA': 'Washington',
    'WI': 'Wisconsin',
    'WV': 'West Virginia',
    'WY': 'Wyoming'
}

# Create a list of food that we can use PPA
PPA_FOOD_LIST = ["sushi", "pizza", "steak", "peanutbutter", "ramen"]
##########################NLU Functions############################


def detect_sentence_digit(sentence):
    r"""
    Detect whether there is a digit in sentence, if yes return True and that digit
    """
    for word in sentence.split():
        if word.isdigit():
            return (True, int(word))
    return (False, None)


def detect_question_DA(dialog_act):
    r"""
    dialog_act is the dialog_act from Dian, which is a dictionary that contains the text for the sentence segment and the associated dialog act tag
    """
    if (dialog_act[0] == "yes_no_question" or dialog_act[0] == "open_question_factual" or dialog_act[0] == "open_question_opinion" or dialog_act[0] == "open_question") and float(dialog_act[1]) > 0.9:
        return True

    return False


def detect_question_DA_update(returnnlp_util, segment_index):
    r"""
    dialog_act is the dialog_act from Dian, which is a dictionary that contains the text for the sentence segment and the associated dialog act tag
    """
    # if (dialog_act[0] == "yes_no_question" or dialog_act[0] == "open_question_factual" or dialog_act[0] == "open_question_opinion" or dialog_act[0] == "open_question") and float(dialog_act[1]) > 0.9:
    #     return True
    #logging.debug("[FOODMODULE] segment_index for now is: {}".format(segment_index))
    #ogging.debug("[FOODMODULE] current returnnlp_util detect question is: {}".format(returnnlp_util[segment_index].has_dialog_act(DialogActEnum.OPEN_QUESTION_OPINION)))
    if returnnlp_util.has_dialog_act([DialogActEnum.YES_NO_QUESTION, DialogActEnum.OPEN_QUESTION, DialogActEnum.OPEN_QUESTION_FACTUAL, DialogActEnum.OPEN_QUESTION_OPINION],  index=segment_index):
        return True
    return False

##########################################Modification for detected food#################################################
def detect_food_concept(concept, sentence_segment):
    """
    Detect the food using concept net and categorize them into differnt kinds for corresponding chitchat
    and update the sentence_segment
    """
    concept_tags = list(concept['data'].keys())
    detected_food_category = None

    if FOOD_TAG_FIX.get(concept['noun'], None) is not None:
        detected_food_category = FOOD_TAG_FIX[concept['noun']]
    for tag in concept_tags:
        if FOOD_CONCEPT_MAP_CATEGORY.get(tag, None) is not None:
            if detected_food_category is None:
                detected_food_category = FOOD_CONCEPT_MAP_CATEGORY[tag]
            elif detected_food_category == "other" and FOOD_CONCEPT_MAP_CATEGORY[tag]:
                detected_food_category = FOOD_CONCEPT_MAP_CATEGORY[tag]
    if detected_food_category is not None:
        sentence_segment['detected_food'].append({'food':concept['noun'], 'category': detected_food_category})

def detect_PPA_food(detected_food):
    """
    Check whether food from PPA list has shown up in detected_food
    """
    for x in PPA_FOOD_LIST:
        for y in detected_food:
            if y['food'] == x:
                #find a match
                return True
    return False
##############################################################################################################################
def get_sys_other_topic(sys_info, context):
    r"""
    Return the other topic detected by system while no sport intent is detected
    """
    detect_food_topic = False
    wrong_othertopic_detection = False
    all_sys_detected_topic = []

    # Add the topic detected by regex 
    for x in sys_info['returnnlp']:
        for y in x['intent'].get('topic', []):
            if y in SYS_OTHER_TOPIC_LIST:
                all_sys_detected_topic.append(y)
            elif y == "topic_others":
                # Special Transit Out
                special_topic_key = {'car': 'topic_techscience'}
                for key in special_topic_key.keys():
                    if re.search(key, x['text']):
                        all_sys_detected_topic.append(special_topic_key[key])
        for w in WRONG_JUMPOUT:
            if re.search(w, x['text']):
                wrong_othertopic_detection = True

        # Added the topic detected by amazon classifier
        if x.get('topic', None):
            if x['topic'].get('topicClass', None):
                all_sys_detected_topic.append(x['topic']['topicClass'])

    # Add the topic detected by topic_list
    for z in sys_info['central_elem']['module']:
        all_sys_detected_topic.append(z)

    # logging.info("[FOODMODULE] all system detecetd_topics are: {}".format(
    #     all_sys_detected_topic))

    # If food topic is detected, it is expected that we still stay with
    # topic_food
    if 'topic_food' in all_sys_detected_topic or 'Food_Drink' in all_sys_detected_topic or "FOODCHAT" in all_sys_detected_topic or context['conversation_context']['detected_food'] or context["conversation_context"]["detected_cuisine"]:
        detect_food_topic = True

    if not detect_food_topic and not wrong_othertopic_detection:
        # check if the sys_intent list contains other topics
        for topic in all_sys_detected_topic:
            if topic in SYS_OTHER_TOPIC_LIST:
                # Check if one of the topic we can talk is detected here
                if TOPIC_KEYWORD_MAP.get(topic, None):
                    return TOPIC_KEYWORD_MAP[topic]
                else:
                    return True

        # check if the sys_topic topic module contains other topics
        for module in SYS_OTHER_MODULE_LIST:
            if module in all_sys_detected_topic:
                return MODULE_KEYWORD_MAP.get(module, True)

        # 5. TODO: If necessary include Google Knowledge to help determine
        # other topics
    return None


def detect_food_country(utterance):
    r"""
    Detect whether a country appears in the utterance
    """
    result = []
    for country, country_dish in FOOD_COUNTRY_LIST.items():
        if re.search(country, utterance) or re.search(country_dish, utterance):
            result.append(country)
    return result


def detect_food_city(sys_info_returnnlp):
    r"""
    detect city using concept_kg and google_kg
    """
    detected_city = []
    concept_kg = sys_info_returnnlp.get('concept', [])
    if concept_kg:
        # check if data exists
        for concept in concept_kg:
            if 'city' in list(concept['data'].keys()):
                detected_city.append(concept['noun'])
    return detected_city


def detect_food_state(utterance):
    r"""
    detect the state using regex search
    """
    detected_states = []
    for state_abrev, state in US_STATES_LIST.items():
        if re.search(state.lower(), utterance.lower()):
            detected_states.append(state_abrev)
    return detected_states


# def detect_food_country_cuisine(utterance):
#     r"""
#     Detect the list of COUNTRY CUISINES in the utterance
#     """
#     detected_country_cuisines = []
#     for cuisine in FOOD_CUISINE_COUNTRY_LIST:
#         if re.search(cuisine, utterance):
#             detected_country_cuisines.append(cuisine)
#     return detected_country_cuisines

def detect_food_cuisine(utterance, selected_question=None):
    r"""
    Detect the list of CUISINES in the utterance
    """
    detected_cuisines = []
    for cuisine in FOOD_CUISINE_LIST:
        if re.search(cuisine, utterance):
            detected_cuisines.append(cuisine)
    
    extra_cuisine= ["guatemala"]
    if selected_question:
        if selected_question.get("acknowledge") == "ask_favorite_country_food":
            for cuisine in  extra_cuisine:
                if re.search(cuisine, utterance):
                    detected_cuisines.append(cuisine)


    return detected_cuisines


def detect_food_categories(utterance):
    r"""
    Detect the list of CUISINES in the tuterance
    """
    detected_categories = []
    for category in FOOD_CATEGORY_LIST:
        if re.search(category, utterance):
            detected_categories.append(category)
    return detected_categories

    # print(detect_food_state("can you recommend me a restaurant in lasning, iowa"))


def detect_food_restaurant(returnnlp_util_googlekg):
    """
    Using Google KG and Concept Net to detect restaurant
    """
    RESTAURANT_GOOGLKG_KEYS = ["food company", "food restaurant"]
    restaurant_name = []
    # logging.debug("[FOODMODULE] google kg results: {}".format(sys_info_returnnlp[0]["googlekg"]))
    # logging.debug("[FOODMODULE] concept results: {}".format(sys_info_returnnlp[0]["concept"]))

    #detect restaurant through googlekg
    for kg_segment in returnnlp_util_googlekg:
        for kg in kg_segment:
            if any(re.search(x, kg.description.lower()) and float(kg.result_score) > 100 for x in RESTAURANT_GOOGLKG_KEYS):
                restaurant_name.append(kg.name.lower())
            elif "Restaurant" in kg.entity_type:
                restaurant_name.append(kg.name.lower())

    return restaurant_name


def detect_zomato_city_id(current_city, current_US_state):
    r"""
    Return a zomato_city_id if we can find a match
    """
    # initialize the city_id
    city_id = None

    # initilaize a zomato object
    zomato_config = {"user_key": random.choice(ZOMATO_API_KEY)}
    zomato_client = initialize_app(zomato_config)

    if current_city:
        # Get all the searching results in zomato
        city_results = zomato_client.get_city_search_result(
            current_city, count=5)

        if city_results:
            # logging.info(
            #     "[FOODMODULE] Detected City Results from Zomato API: {}".format(city_results))
            if len(city_results) == 1:
                city_id = city_results[0]['id']
                return city_id
            # TODO: If state detected match the state
            if current_US_state:
                for result in city_results:
                    # logging.info("[FOODMODULE] the result name is: {}".format(result['name'].lower().split(',')[1]))

                    # The state name match
                    if result['name'].lower().split(',')[1].strip() == current_US_state.lower():
                        city_id = result['id']
                        return city_id

    return city_id


def search_zomato_restaurant(city_id, cuisine_id=None, category_id=None):

    # initilaize a zomato object
    zomato_config = {"user_key": random.choice(ZOMATO_API_KEY)}
    zomato_client = initialize_app(zomato_config)

    restaurants = zomato_client.restaurant_search_full(
        city_id, cuisine_id, category_id)

    return restaurants

###############################################################

##########################Intent Detection functions###########


def detect_answeruncertain(utterance, lexical):
    #disable  system ans_unkown for now
    #if any(re.search(x, utterance) for x in PATTERN_MANAGEMENT['ANSWER_UNCERTAIN']) or "ans_unknown" in lexical:
    if any(re.search(x, utterance) for x in PATTERN_MANAGEMENT['ANSWER_UNCERTAIN']):
        return True
    return False


def detect_answeryes(utterance, lexical):
    logging.debug("[FOODMODULE] lexical is: {}".format(lexical))
    if any(re.search(x, utterance) for x in PATTERN_MANAGEMENT['ANSWER_YES']) or "ans_pos" in lexical:
        return True
    return False


def detect_answerno(utterance):
    if any(re.search(x, utterance) for x in PATTERN_MANAGEMENT['ANSWER_NO']):
        return True
    return False


def detect_ask_question(utterance, dialog_act, topic, context):
    # check if topic_backstory in topic
    # if "topic_backstory" in topic:
    #     return True

    if (any(re.search(x, utterance) for x in PATTERN_MANAGEMENT['QUESTION_PATTERNS']) and len(utterance.split()) > 1) or detect_question_DA(dialog_act):
        return True
    return False


def detect_ask_question_update(utterance, returnnlp_util, segment_index, topic, context):
    # check if topic_backstory in topic
    # if "topic_backstory" in topic:
    #     return True

    #if (any(re.search(x, utterance) for x in PATTERN_MANAGEMENT['QUESTION_PATTERNS']) and len(utterance.split()) > 1) or detect_question_DA_update(returnnlp_util,  segment_index):
    if detect_question_DA_update(returnnlp_util,  segment_index):
        return True
    return False


def detect_ask_question_special(utterance):
    # Update the user_intent to food_i_askquyestion in a special case
    if any(re.search(x, utterance) for x in PATTERN_MANAGEMENT['REQUEST_INFO_PATTERN']):
        after_match = ""
        for x in PATTERN_MANAGEMENT['REQUEST_INFO_PATTERN']:
            if re.search(x, utterance):
                after_match_pattern = r"(?<=\b({})\s)(.+)".format(str(x))
                # logging.info(
                #     "[FOODMODULE] the after match in request info_patter is: {}".format(
                #         after_match_pattern))
                after_match = re.search(after_match_pattern, utterance)
                if after_match:
                    after_match = after_match.group()
                    logging.info("[FOODMODULE] the after match is: {}".format(
                        after_match))
                    if any(re.search(x, after_match) for x in PATTERN_MANAGEMENT['QUESTION_PATTERNS']):
                        logging.info(
                            "[FOODMODULE] The special match of question is successful.")
                        # self.user_food_intents['food_i_askquestion'] = True
                        # sentence_segment['user_food_intents'][
                        #     'food_i_askquestion'] = True
                        # sentence_segment['text'] = after_match
                        return (True, after_match)
    return (False, None)


def detect_ask_opinion_question(utterance, dialog_act):
    if any(re.search(x, utterance) for x in PATTERN_MANAGEMENT['QUESTION_OPINION']) or (dialog_act[0] == 'open_question_opinion' and float(dialog_act[1]) > 0.8):
        return True
    return False


def detect_ask_opinion_question_update(utterance, returnnlp_util, segment_index):
    if any(re.search(x, utterance) for x in PATTERN_MANAGEMENT['QUESTION_OPINION']) or returnnlp_util.has_dialog_act(DialogActEnum.OPEN_QUESTION_OPINION, index=segment_index):
        return True
    return False


def detect_ask_back(utterance):
    if any(re.search(x, utterance) for x in PATTERN_MANAGEMENT['ASK_BACK_PATTERN']):
        return True

    return False


def detect_suggest_pattern(utterance):
    if any(re.search(x, utterance) for x in PATTERN_MANAGEMENT['SUGGEST_PATTERN']):
        if not re.search(r"\btoo|\bas well", utterance):
            return True
    return False


def detect_nointerest_pattern(utterance):
    if any(re.search(x, utterance) for x in PATTERN_MANAGEMENT['NO_INTEREST_PATTERN']):
        return True

    return False


def detect_quit_module_pattern(utterance, input_returnnlp):
    command_detector = CommandDetector(utterance, input_returnnlp)
    if any(re.search(x, utterance) for x in PATTERN_MANAGEMENT['QUIT_MODULE_PATTERN']):
        return True
    elif command_detector.is_request_jumpout():
        return True

    return False


def detect_request_info_pattern(utterance):
    if any(re.search(x, utterance) for x in PATTERN_MANAGEMENT['REQUEST_INFO_QUESTION_PATTERNS']):
        return True
    elif any(re.search(x, utterance) for x in PATTERN_MANAGEMENT['REQUEST_INFO_PATTERN']):
        return True

    return False

def detect_command(input_text, input_returnnlp):
    """
    Detect whether a command (not a device command) is given from the user
    """
    command_detector = CommandDetector(input_text, input_returnnlp)
    # if command_detector.is_request_tellmore():
    #     logging.debug("[FOODMODULE] the require more command is detected.")
    # else:
    #     logging.debug("[FOODMODULE] the require more command is not detected")
   
    return command_detector.has_command_complete() or re.search(r"can you skip .* recipe", input_text)

def detect_request_restaurant_pattern(utterance, dialog_act):
    r"""
    Decide whether users want a recommendation of restaurant
    """
    if any(re.search(y, utterance) for y in PATTERN_MANAGEMENT['REQUEST_INFO_QUESTION_PATTERNS']) or any(re.search(y, utterance) for y in PATTERN_MANAGEMENT['REQUEST_INFO_PATTERN']):
        # if (dialog_act[0] == "commands" and float(dialog_act[1]) > 0.9) or any(re.search(y, utterance) for y in PATTERN_MANAGEMENT['REQUEST_INFO_QUESTION_PATTERNS']):
        # check if key words are in the dialog_act
        if any(re.search(x, utterance) for x in PATTERN_MANAGEMENT["PLACES_FOR_FOOD_PATTERNS"]):
            return True
    return False


def detect_answer_abandoned_pattern(dialog_act):
    r"""
    Decide whether users have abandoned dialog_act, which means the sentence is incomplete such as "can we"
    """
    if dialog_act[0] == "abandoned" and float(dialog_act[1]) > 0.9:
        return True
    return False


def detect_answer_abandoned_pattern_update(returnnlp_util, sentence_segment):
    r"""
    Decide whether users have abandoned dialog_act, which means the sentence is incomplete such as "can we"
    """
    if returnnlp_util.has_dialog_act(DialogActEnum.ABANDONED, index=sentence_segment):
        return True
    return False
    #######################################################################

    ####################Response Generation Function#######################


def week_frequency_acknowledge(frequency):
    r"""
    Based on the frequency, return the appropirate key to acknowledge
    """
    if frequency < 2 and frequency > 0:
        acknowledge_key = "acknowledge_food_week_frequency_not_often"
    elif frequency < 5:
        acknowledge_key = "acknowledge_food_week_frequency_normal"
    else:
        acknowledge_key = "acknowledge_food_week_frequency_frequently"
    return acknowledge_key


def format_recommendation_restaurant_response(city_name, cuisine_name, category_name, restaurant_results, user_attributes):
    # selected_restaurant
    selected_restaurant = random.choice(restaurant_results)

    # Extract Some Necessary Information
    restaurant_name = selected_restaurant["name"]
    locality = selected_restaurant["location"]["locality"]
    restaurant_cuisines = selected_restaurant["cuisines"].split(',')
    rating = selected_restaurant["user_rating"]["aggregate_rating"]
    rating_text = selected_restaurant["user_rating"]["rating_text"]
    selected_restaurant_id = selected_restaurant['id']

    response = ""

    if not cuisine_name and not category_name:
        response = template_food.utterance(selector='general_templates/restaurant_recommendation_general', slots={"city": city_name, "restaurant_name": restaurant_name, "locality": locality, "cuisine": restaurant_cuisines[0], "rating": rating},
                                           user_attributes_ref=user_attributes)
    elif cuisine_name:
        response = template_food.utterance(selector='general_templates/restaurant_recommendation_cuisine', slots={"city": city_name, "restaurant_name": restaurant_name, "locality": locality, "cuisine": cuisine_name, "rating_text": rating_text, "rating": rating},
                                           user_attributes_ref=user_attributes)
    else:
        response = template_food.utterance(selector='general_templates/restaurant_recommendation_category', slots={"city": city_name, "restauran_name": restaurant_name, "locality": locality, "category": category_name, "rating": rating},
                                           user_attributes_ref=user_attributes)
    return selected_restaurant_id, response


def food_follow_up_question_answer(utterance, context):
    r"""
    Check if matches with one of the follow up question in sport type
    """
    response = None

    if re.search(r"(what|how)\s*(can|should)\s*(i).*(teach).*(you)", utterance):
        # hit the what can i teach you utterance
        response = "Please tell me how to answer the question you just asked."
    elif re.search(r"what else", utterance):
        response = "sorry that I don't have follow up content at this moment."
    elif re.search(r"^why\b", utterance) and context["conversation_context"]["backstory_reason"]:
        response = context["conversation_context"]["backstory_reason"]

    return response


def retrieve_reddit(utterance, subreddits, limit):
    # cuurently using gt's client info
    try:
        reddit = praw.Reddit(client_id=REDDIT_CLIENT_ID,
                             client_secret=REDDIT_CLIENT_SECRET,
                             user_agent='extractor',
                             timeout=2
                             # username='gunrock_cobot', # not necessary for read-only
                             # password='ucdavis123'
                             )
        subreddits = '+'.join(subreddits)
        subreddit = reddit.subreddit(subreddits)
        # higher limit is slower; delete limit arg for top 100 results
        submissions = list(subreddit.search(utterance, limit=limit))
        if len(submissions) > 0:
            return submissions
        return None
    except Exception as e:
        logging.error("Fail to retrieve reddit. error msg: {}. user utterance: {}".format(e, utterance))
        return None


def get_food_reddit_posts(text, limit):
    try:
        submissions = retrieve_reddit(text, ['todayilearned'], limit)
        if submissions != None:
            results = [submissions[i].title.replace(
                "TIL", "").lstrip() for i in range(len(submissions))]
            results = [results[i] + '.' if results[i][-1] != '.' and results[i][-1] !=
                       '!' and results[i][-1] != '?' else results[i] for i in range(len(results))]
            # Sensitive Words Pattern
            final_results = []
            for result in results:
                if not any(re.search(x, result.lower()) for x in SENSITIVE_WORDS_LIST):
                    final_results.append(result)
            return final_results
        return None
    except Exception as e:
        logging.error(
            "[FOODMODULE][ERROR] Food Module does not return rsubreddit appropriately. err: {}".format(e), exc_info=True)
        return None
# print(get_food_reddit_posts("pizza", limit=2))


def get_food_evi_response(utterance):
    try:
        client = get_client(api_key=COBOT_API_KEY)
        r = client.get_answer(question=utterance, timeout_in_millis=1000)
        if r["response"] == "" or r["response"].startswith('skill://') or re.search(r"an opinion on that", r["response"]):
            return None

        result = postprocessing_evi_response(r["response"])
        return result
    except Exception as e:
        logging.error(
            "[FOOD_MODULE][ERROR] EVI does properly return a response.err: {}".format(e), exc_info=True)
        return None

#print(get_food_evi_response('good basketball player'))


def postprocessing_evi_response(evi_response):
    r"""
    Some hard reformat of evi_response
    """

    # 1. Remove the "for more information response"
    evi_response = evi_response.lower()

    if re.search("wikihow", evi_response):
        evi_response = re.sub(r"for more details.*", "", evi_response)
        evi_response = re.sub(
            r"here's something i found on wikihow", "", evi_response)

    filter_list = ["alexa", "'that' is usually defined as"]

    # get rid of the response that include alexa
    for x in filter_list:
        if re.search(x, evi_response):
            return None

    # get rid of extremely short response from alexa
    if len(evi_response.split()) < 3 or len(evi_response.split()) > 50:
        return None

    return evi_response


def user_utterance_paraphrase(user_utterance):
        # Replacement_Rules
    replacement_rules = [(r"\bare you\b", "am i"), (r"\bi was\b", "you were"), (r"\bi am\b", "you are"), (r"\bwere you\b", "was i"),
                         (r"\bi\b", "you"), (r"\bmy\b",
                                             "your"), (r"\bmyself\b", "yourself"),
                         (r"\byou\b", "i"), (r"\byour\b", "my"),
                         (r"\bme\b", "you")]

    paraphrase_response = user_utterance
    # Replace a couple of things
    for rule in replacement_rules:
        paraphrase_response = re.sub(rule[0], rule[1], paraphrase_response)
    return paraphrase_response


def unable_answer_question_error_handle(user_utterance, user_context, sys_info, user_attributes):
    r"""
    Generate different acknowledgement of questions
    (1) If the length of the question is less than 10 words, paraphrase; otherwise paraphrase the longest noun_phrase
    (2) If the question is a backstory question, strategy, keep it as a secret for now.
    (3) If the question is an yes_or_no question
    (4) elif the question is asking opinion, then say that you have never thought about it before. ask his thought.
    (5) if the question is a fact question, then admit you don't know the answer and tell user that you will get back to him later.
    """
    # initialize error_handle_response
    error_handle_response = ""
    detected_noun_phrase = sys_info['features'].get('noun_phrase', [])
    detected_noun_phrase.sort(key=lambda s: len(s.split()))
    detected_noun_phrase.reverse()
    # initilaize returnnlp
    # returnnlp = sys_info['features']['returnnlp']
    # initialize the question type
    detected_question_types = user_context[
        "conversation_context"]["detected_question_types"]

    # paraphrase the question when the length of the question is less than 10
    # elif there is noun_phrase, paraphrse that noun_phrase
    if len(user_utterance.split()) < 10:
        error_handle_response = user_utterance_paraphrase(user_utterance) + '?'
    elif detected_noun_phrase:
        error_handle_respoinse = detected_noun_phrase[0] + '?'

    backstory_tag_list = ["topic_backstory", "ask_hobby", "ask_preference"]

    # Check if the question is a backstory question
    if any(x in detected_question_types for x in backstory_tag_list):
        unable_handle_backstory_question_response = template_food.utterance(selector='general_templates/unable_handle_backstory_question', slots={},
                                                                            user_attributes_ref=user_attributes)
        error_handle_response = ' '.join(
            [error_handle_response, unable_handle_backstory_question_response])

    elif "yes_no_question" in detected_question_types and re.search(r"\byou\b", user_utterance):
        # Try to just say no I am not
        if re.search(r"^do", user_utterance):
            unable_handle_yes_or_no_question_response = "no, i don't."
        elif re.search(r"^can", user_utterance):
            unable_handle_yes_or_no_question_response = "sorry, i can't."
        elif re.search(r"^would", user_utterance):
            unable_handle_yes_or_no_question_response = "no, i wouldn't sorry."
        elif re.search(r"^did", user_utterance):
            unable_handle_yes_or_no_question_response = "no, i did't."
        elif re.search(r"^could", user_utterance):
            unable_handle_yes_or_no_question_response = "sorry, i couldn't."
        else:
            unable_handle_yes_or_no_question_response = "sorry, but no."

        error_handle_response = ' '.join(
            [error_handle_response, unable_handle_yes_or_no_question_response])
    elif "open_question_opinion" in detected_question_types:
        unable_handle_opinion_question_response = template_food.utterance(selector='general_templates/unable_handle_opinion_question', slots={},
                                                                          user_attributes_ref=user_attributes)
        error_handle_response = ' '.join(
            [error_handle_response, unable_handle_opinion_question_response])
    else:
        unable_handle_question_general_response = template_food.utterance(selector='general_templates/unable_handle_question_general', slots={},
                                                                          user_attributes_ref=user_attributes)
        error_handle_response = ' '.join(
            [error_handle_response, unable_handle_question_general_response])

    # Add a break in the end
    error_handle_response = ' '.join(
        [error_handle_response, "<break time='600ms'/>"])

    return error_handle_response
    ##########################################################################
