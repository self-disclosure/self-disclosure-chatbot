from response_generator.food.food_utils import *
from response_generator.food.food_state_management import Food_State_Management
import random
from nlu.dataclass import CentralElement, ReturnNLP, ReturnNLPSegment
from selecting_strategy.module_selection import ModuleSelector, GlobalState
from user_profiler.user_profile import UserProfile
from nlu.question_detector import QuestionDetector

logging.basicConfig(
    format='[%(levelname)s] %(asctime)s [%(filename)s:%(lineno)d] %(message)s',
)
template_food = template_manager.Templates.food

class Food_Chatbot(object):

    def __init__(self, Utterance, Sys_Info, User_Attributes):
        # Get the user_utterance
        self.user_utterance = re.sub("open gunrock and", "", Utterance)
        self.sys_info = Sys_Info
        self.user_attributes = User_Attributes
        self.returnnlp_util = ReturnNLP(self.sys_info['features']['returnnlp'])
        self.module_selection = ModuleSelector(self.user_attributes)
        self.user_profile =  UserProfile(self.user_attributes)
        self.question_detector = QuestionDetector(self.user_utterance,self.sys_info['features']['returnnlp'])
        #logging.info("[FOODMODUILE] previous propose continue: {}".format(self.sys_info['propose_continue']))

        # self.sys_info[
        #     'food_context']['context'].get('previous_state', None)
        # Initilaize the food intents
        self.__bind_user_food_intents()
        logging.info("[FOODMODULE] Initialize the intents in food chat")
        # Initialize the food context information and user context information
        self.__bind_user_food_context()
        logging.info("[FOODMODULE] Initialize and sync up the context")

        # Update the Intents and Contact Information
        self.__update__()

        logging.debug("[FOODMODULE] expect_next_state: {}".format(
            self.context['conversation_context']['expect_next_state']))
        # if the acknowledge is ask_interest_food and least favorite food
        # logging.info("[FOODMODULE] selected_question is: {}".format(
        #     self.context['conversation_context']['selected_question']))
        # logging.info(
        #     "[FOODMODULE] Finish update the detected_intents, detected_food, and context")
        logging.debug("[FOODMODULE] previous_state is: {}".format(
            self.context['conversation_context']['previous_state']))
        logging.debug("[FOODMODULE] current food is: {}".format(
            self.context['conversation_context']['current_food']))
        logging.debug("[FOODMODULE] current food cuisine is: {}".format(
            self.context['conversation_context']['current_cuisine']))

        logging.debug("[FOODMOULE] current food chatmode is: {}".format(
            self.context["conversation_context"]["chitchat_type"]))

        # Initialize the state management object
        self.food_state_management = Food_State_Management(
            self.user_utterance, self.user_food_intents, self.context, self.sys_info, self.user_attributes)

    def __bind_user_food_intents(self):
        self.user_food_intents = {
            "food_i_followupentity": False,
            "food_i_notinterest": False,
            "food_i_notinterestfood": False,  # Not itnerest to talk about specific food
            "food_i_notinterestfoodchat": False,  # Not interest to talk about food topic
            "food_i_suggestfood": False,
            "food_i_suggestfoodcuisine": False,
            "food_i_suggestfoodchat": False,
            "food_i_requestrecipe": False,
            "food_i_askquestion": False,
            "food_i_askopinion": False,
            "food_i_requestrestaurant": False,
            # #################Lexiical Level Intent#################
            "food_i_askback": False,
            "food_i_answerno": False,
            "food_i_answeryes": False,
            "food_i_answeruncertain": False,
            "food_i_answerabandoned": False,
            "food_i_answerabandoned_not_question": False,
        }

    def __init_food_context(self):
        self.context['conversation_context'] = {
            'previous_state': None,
            'previous_system_response': None,
            'current_food': {},
            'current_cuisine': None,  # Adcded by Mingyang to track the current cuisine
            'expect_next_state': None,
            'conversation_id': None,  # Initialize Converstaion ID
            'chitchat_type': "general",
            'followup_questions': [],  # Added by Mingyang to store the followup questions
            'selected_question': {},  # Added by Mingyang to store the selected_questions
            'restaurant_info': {},
            'missed_times': 0,
            # Added by Mingyang Zhou to count the number of times the chitchat
            # in majority key is used in general food flow
            # "num_majority_chitchat": 0,
            # Added by Mingyang Zhou to count the number of times the chitchat
            # in minority key is used in general food flow
            # "num_minority_chitchat": 0,
            # "num_least_minority_chitchat":0,
            # 'recipe_tags': {'any_recipe': False},  # A flag for recipe list
            'current_food_country': None,
            'backstory_reason': None, #Added by Mingyang Zhou to store backstory_reason
            # ###Context Useful for Current Utterance#####
            'detected_food': [],
            'detected_city': [],
            'detected_state': [],
            'detected_cuisine': [],
            'detected_category': [],
            'detected_country': [],
            'detected_question_types': [],
            'unclear': False,
            'deeptalk_food': False,
            'propose_topic': None,
            'any_recipe': False,
            'food_module_run_out': False,
            'fail_question_answer': False,
            'resume_mode': False,
            'start_food_general': False,
        }

        self.context['user_context'] = {
            # 'user_id':None,
            'talked_food': {},
            'talked_cuisine': {},
            'talked_subtopic': [], #Added by Mingyang Zhou
            'least_favorite_food': [],
            'favorite_food': [],
            'favorite_cuisine': [],
            'not_interested_food': [],
            'provided_facts': [],
            'run_out_of_food': False,
            "num_majority_chitchat": 0,
            "num_minority_chitchat": 0,
            "num_least_minority_chitchat":0,

        }
        self.context['propose_continue'] = 'CONTINUE'

    def sync_conversation_context(self):
        r"""
        Sync the User Context from System_Information
        """
        self.context['conversation_context']['previous_state'] = self.sys_info[
            'food_context']['context'].get('previous_state', None)

        self.context['conversation_context']['previous_system_response'] = self.sys_info[
            'food_context']['context'].get('previous_system_response', None)

        self.context['conversation_context']['current_food'] = self.sys_info[
            'food_context']['context'].get('current_food', {})

        self.context['conversation_context']['current_cuisine'] = self.sys_info[
            'food_context']['context'].get('current_cuisine', None)

        self.context['conversation_context']['expect_next_state'] = self.sys_info[
            'food_context']['context'].get('expect_next_state', None)

        self.context['conversation_context']['conversation_id'] = self.sys_info[
            'food_context']['context'].get('conversation_id', None)

        self.context['conversation_context']['chitchat_type'] = self.sys_info[
            'food_context']['context'].get('chitchat_type', "general")

        self.context['conversation_context']['followup_questions'] = self.sys_info[
            'food_context']['context'].get('followup_questions', None)

        self.context['conversation_context']['selected_question'] = self.sys_info[
            'food_context']['context'].get('selected_question', {})

        self.context['conversation_context']['restaurant_info'] = self.sys_info[
            'food_context']['context'].get('restaurant_info', {})

        self.context['conversation_context']['missed_times'] = int(
            self.sys_info['food_context']['context'].get('missed_times', 0))

        # self.context['conversation_context']['num_majority_chitchat'] = int(
        #     self.sys_info['food_context']['context'].get('num_majority_chitchat', 0))

        # self.context['conversation_context']['num_minority_chitchat'] = int(
        #     self.sys_info['food_context']['context'].get('num_minority_chitchat', 0))

        # self.context['conversation_context']['num_least_minority_chitchat'] = int(
        #     self.sys_info['food_context']['context'].get('num_least_minority_chitchat', 0))

        # self.context['conversation_context']['run_out_of_food'] = self.sys_info[
        #     'food_context']['context'].get('run_out_of_food', False)

        self.context['conversation_context']['current_food_country'] = self.sys_info[
            'food_context']['context'].get('current_food_country', None)
        
        self.context['conversation_context']['backstory_reason'] = self.sys_info[
            'food_context']['context'].get('backstory_reason', None)
        self.context['conversation_context']['resume_mode'] = self.sys_info[
            'food_context']['context'].get('resume_mode', False)

        # self.context['propose_continue'] = self.sys_info[
        #     'food_context'].get('propose_continue', 'CONTINUE')

    def sync_user_context(self):
        r"""
        Sync the User Context from System_Information
        """
        self.context['user_context']['talked_food'] = self.sys_info[
            'food_context']['user'].get('talked_food', {})

        self.context['user_context']['talked_cuisine'] = self.sys_info[
            'food_context']['user'].get('talked_cuisine', {})
        
        self.context['user_context']['talked_subtopic'] = self.sys_info[
            'food_context']['user'].get('talked_subtopic', [])

        self.context['user_context']['least_favorite_food'] = self.sys_info[
            'food_context']['user'].get('least_favorite_food', [])

        self.context['user_context']['favorite_food'] = self.sys_info[
            'food_context']['user'].get('favorite_food', [])

        self.context['user_context']['not_interested_food'] = self.sys_info[
            'food_context']['user'].get('not_interested_food', [])

        self.context['user_context']['provided_facts'] = self.sys_info[
            'food_context']['user'].get('provided_facts', [])
        self.context['user_context']['favorite_cuisine'] = self.sys_info[
            'food_context']['user'].get('favorite_cuisine', [])

        self.context['user_context']['num_majority_chitchat'] = int(
            self.sys_info['food_context']['user'].get('num_majority_chitchat', 0))

        self.context['user_context']['num_minority_chitchat'] = int(
            self.sys_info['food_context']['user'].get('num_minority_chitchat', 0))

        self.context['user_context']['num_least_minority_chitchat'] = int(
            self.sys_info['food_context']['user'].get('num_least_minority_chitchat', 0))


    def __bind_user_food_context(self):
        r"""
        Update the Context for users
        """
        # initialize context
        self.context = {}
        self.__init_food_context()

        # Inherit the appropriate information based on whether previous users have
        # hit the utterance.
        try:
            if self.sys_info['food_context'].get('context', None):
                if self.sys_info['food_context']['context'].get('conversation_id', None) != self.sys_info['session_id']:
                    #logging.info("[FOODCHAT] Only Keep the User Context")
                    # get the user_context from the previous talk
                    self.sync_user_context()
                else:
                    # get both conversation_context and user_context from previous
                    # talk
                    #logging.info("[FOODCHAT] Inherit both User Context and Conversation Context")
                    self.sync_conversation_context()
                    self.sync_user_context()
        except Exception as e:
            logging.error(
                "[FOODCHAT] bind user food contact error, err: {}".format(e), exc_info=True)
            pass
        
        #Update resume mode
        if self.module_selection.last_topic_module != "FOODCHAT":
            if self.context["conversation_context"]["conversation_id"] == self.sys_info["session_id"]:
                self.context["conversation_context"]["resume_mode"] = True
                

        # Update the current session_id
        self.context['conversation_context'][
            'conversation_id'] = self.sys_info['session_id']

        # Update the Initial State if food is interuppted by other modules
        if self.context.get('propose_continue') == 'STOP' or self.context.get('propose_continue') == 'UNCLEAR' or self.sys_info['last_module'] != 'FOODCHAT' or self.context['user_context']['run_out_of_food']:
            self.context['conversation_context']['previous_state'] = None
            self.context['conversation_context']['selected_question'] = {}
            self.context['conversation_context']['followup_questions'] = []
            self.context['conversation_context']['expect_next_state'] = None
            self.context["conversation_context"]["chitchat_type"] = "general"

    def __update__(self):
        r"""
        update the intents and context information based on each sentence segmented.
        """
        # try:
        # Use the New NLU Pipeline
        for i, sentence_segment in enumerate(self.sys_info['features'].get('returnnlp', [])):
            # initialize the sentence_segment intent
            sentence_segment[
                'user_food_intents'] = self.user_food_intents.copy()

            # reinitialize the sentence_segment
            for key in list(sentence_segment['user_food_intents'].keys()):
                sentence_segment['user_food_intents'][key] = False

            self.__update_segment_new__(
                sentence_segment, sentence_segment_index=i)
        # except:
        #     # Fall Back to use the old NLU Pipeline
        #     for sentence_segment in self.sys_info['features']['sys_DA'][:-1]:
        #         sentence_segment[
        #             'user_food_intents'] = self.user_food_intents.copy()
        #         self.__update_segment_old__(sentence_segment)

        # Update Sentence_Level Context
        self.update_sentence_context()

    def __update_segment_new__(self, sentence_segment, sentence_segment_index=0):
        r"""
        Use the New NLU Pipeline to detect food, intents and update the appropriate context
        """
        # Detect the food in the current sentence_segment
        self.detect_food_new(sentence_segment)

        # Define the function to detect the intent
        self.detect_intent_new(sentence_segment, sentence_segment_index)

        detected_intents = [x for x in list(sentence_segment['user_food_intents'].keys(
        )) if sentence_segment['user_food_intents'][x]]
        
        logging.debug(
            "[FOODMODULE] The detected intents at current segments are: {}".format(detected_intents))
        logging.debug("[FOODMODULE] the detected food at current segment are: {}".format(sentence_segment['detected_food']))

        # Define the context updating process
        self.update_sentence_segment_context(sentence_segment)

    def __update_segment_old__(self, sentence_segment):
        r"""
        Use the Old NLU Pipeline to detect food, intents and update the appropriate context
        """
        pass

    def detect_food_new(self, sentence_segment):
        r"""
        detect any food in the sentence_segment
        """
        # Initialize the detected food list
        sentence_segment['detected_food'] = []
        # Extract the concept of the sentence
        segment_concept = sentence_segment.get('concept', [])

        # Deal with the case when sgment_concept is Empty
        if segment_concept is None:
            segment_concept = []

        # Iterate through the segment_concepts
        for concept in segment_concept:
            if concept['noun'] not in WRONG_FOOD:
                #update the sentence_segment
                detect_food_concept(concept, sentence_segment)


        # Iterate through the MISSDETECTED FOOD LIST
        for food in MISDETECTED_FOOD_LIST.keys():
            if re.search(food, sentence_segment['text']):
                sentence_segment['detected_food'].append({'food':food, 'category': MISDETECTED_FOOD_LIST[food]})

        # Detect the cuisine and categories
        sentence_segment['detected_cuisine'] = []
        sentence_segment['detected_cuisine'] = detect_food_cuisine(sentence_segment['text'], self.context["conversation_context"]["selected_question"])


        sentence_segment['detected_category'] = []
        sentence_segment['detected_category'] = detect_food_cuisine(sentence_segment['text'])

        # Detect the country mentioned in the name
        sentence_segment['detected_country'] = []
        sentence_segment['detected_country'] = detect_food_country(sentence_segment[
                                                                   'text'])

    def detect_intent_new(self, sentence_segment, sentence_segment_index=0):
        r"""
        detect the intents within food module
        """
        if detect_quit_module_pattern(sentence_segment['text'], self.sys_info['features']['input_data_returnnlp']):
            self.user_food_intents["food_i_notinterestfoodchat"] = True
            sentence_segment['user_food_intents'][
                "food_i_notinterestfoodchat"] = True

        if detect_nointerest_pattern(sentence_segment['text']):
            self.user_food_intents["food_i_notinterest"] = True
            sentence_segment['user_food_intents'][
                "food_i_notinterest"] = True

            # Detect the sport type
            if sentence_segment["detected_food"]:
                self.user_food_intents["food_i_notinterestfood"] = True
                sentence_segment['user_food_intents'][
                    "food_i_notinterestfood"] = True

            if re.search(r'(food|meal|dish)', sentence_segment['text']):
                wrong_jumpout_words = ["food truck"]
                if not any([re.search(x, sentence_segment['text']) for x in wrong_jumpout_words]):
                    self.user_food_intents["food_i_notinterestfoodchat"] = True
                    sentence_segment['user_food_intents'][
                        "food_i_notinterestfoodchat"] = True
        elif sentence_segment["detected_cuisine"] and ((len(sentence_segment['text'].split()) < 3 and (self.context["conversation_context"]["previous_state"] != "food_chitchat_ask")) or self.context["conversation_context"]["previous_state"] is None):
            for x in sentence_segment["detected_cuisine"]:
                if x and x != self.context["conversation_context"]["current_cuisine"] and x not in self.context["user_context"]["talked_cuisine"]:
                    sentence_segment['user_food_intents']['food_i_suggestfoodcuisine'] = True
                    self.user_food_intents['food_i_suggestfoodcuisine'] = True

        elif self.context["conversation_context"]["selected_question"].get("ask_interest", None) == "food" and detect_PPA_food(sentence_segment["detected_food"]):
            for x in sentence_segment["detected_food"]:
                if x and x['food'] != self.context["conversation_context"]["current_food"].get('food', None) and x['food'] not in self.context["user_context"]["talked_food"]:
                    self.user_food_intents["food_i_suggestfood"] = True
                    sentence_segment['user_food_intents']["food_i_suggestfood"] = True
                    break
        
        elif sentence_segment["detected_food"] and len(sentence_segment['text'].split()) < 3 and self.context["conversation_context"]["previous_state"] != "food_chitchat_ask":
            for x in sentence_segment["detected_food"]:
                if x and x['food'] != self.context["conversation_context"]["current_food"].get('food', None) and x['food'] not in self.context["user_context"]["talked_food"]:
                    self.user_food_intents["food_i_suggestfood"] = True
                    sentence_segment['user_food_intents'][
                        "food_i_suggestfood"] = True
                    break
        

        
        # detect suggestfood intent
        if detect_suggest_pattern(sentence_segment['text']):
            # Suggest Food
            logging.debug("[FOODMODULE] talked_food: {}".format(list(self.context["user_context"]["talked_food"].keys())))
            logging.debug("[FOODMODULE] detected_food: {}".format(sentence_segment["detected_food"]))
            if sentence_segment["detected_food"] and any(x['food'] not in list(self.context["user_context"]["talked_food"].keys()) for x in sentence_segment["detected_food"]) and self.context["conversation_context"]["previous_state"] != "food_chitchat_ask":
                self.user_food_intents["food_i_suggestfood"] = True
                sentence_segment['user_food_intents']["food_i_suggestfood"] = True

            # Suggest change to other food
            if re.search(r"(other food|other dishes|another food)", sentence_segment['text']):
                self.user_food_intents["food_i_notinterest"] = True
                sentence_segment['user_food_intents']['food_i_notinterest'] = True

            # # General Suggest food topic
            # if re.search(r"food", sentence_segment['text']):
            #     self.user_food_intents["food_i_suggestfoodchat"] = True
            #     sentence_segment['user_food_intents'][
            #         'food_i_suggestfoodchat'] = True

            # Request a recipe
            if re.search(r"recipe|recipes", sentence_segment['text']):
                self.user_food_intents["food_i_requestrecipe"] = True
                sentence_segment['user_food_intents'][
                    'food_i_requestrecipe'] = True

            # Suggest food cusine
            if sentence_segment['detected_cuisine'] and any(x not in self.context["user_context"]["talked_cuisine"] for x in sentence_segment["detected_cuisine"]):
                # detected food cusine
                sentence_segment['user_food_intents'][
                    'food_i_suggestfoodcuisine'] = True
                self.user_food_intents['food_i_suggestfoodcuisine'] = True

                # detect ask question intent
                # if detect_ask_question(sentence_segment['text'],
                # sentence_segment['dialog_act'],
                # sentence_segment['intent'].get('topic', []), self.context):
        # if detect_ask_question_update(sentence_segment['text'], self.returnnlp_util, sentence_segment_index, sentence_segment['intent'].get('topic', []), self.context) and not (detect_command(self.user_utterance, self.sys_info['features']['input_data_returnnlp'])):
        #     self.user_food_intents["food_i_askquestion"] = True
        #     sentence_segment['user_food_intents']["food_i_askquestion"] = True

        #     # Detect whether it ask the opinion
        #     # if detect_ask_opinion_question(sentence_segment['text'],
        #     # sentence_segment['dialog_act']):
        #     if detect_ask_opinion_question_update(sentence_segment['text'], self.returnnlp_util, sentence_segment_index):
        #         self.user_food_intents['food_i_askopinion'] = True
        #         sentence_segment['user_food_intents'][
        #             'food_i_askopinion'] = True

        #     # Detect Ask Back
        #     if detect_ask_back(sentence_segment['text']):
        #         self.user_food_intents["food_i_askback"] = True
        #         sentence_segment['user_food_intents'][
        #             'food_i_askback'] = True

        #         self.user_food_intents["food_i_askquestion"] = False
        #         sentence_segment['user_food_intents'][
        #             "food_i_askquestion"] = False
        if self.question_detector.has_question_seg(sentence_segment_index):
            self.user_food_intents["food_i_askquestion"] = True
            sentence_segment['user_food_intents']["food_i_askquestion"] = True
            #     # Detect whether it ask the opinion
            # if detect_ask_opinion_question(sentence_segment['text'],
            # sentence_segment['dialog_act']):
            if self.question_detector.is_opinion_question(sentence_segment_index):
                self.user_food_intents['food_i_askopinion'] = True
                sentence_segment['user_food_intents'][
                    'food_i_askopinion'] = True

            # Detect Ask Back
            if detect_ask_back(sentence_segment['text']):
                self.user_food_intents["food_i_askback"] = True
                sentence_segment['user_food_intents'][
                    'food_i_askback'] = True

                self.user_food_intents["food_i_askquestion"] = False
                sentence_segment['user_food_intents'][
                    "food_i_askquestion"] = False


        # detect request food recipe
        #if detect_request_info_pattern(sentence_segment['text']):
        if detect_command(self.user_utterance, self.sys_info['features']['input_data_returnnlp']) or detect_request_info_pattern(sentence_segment['text']):
            # Request a recipe
            if re.search(r"recipe|recipes", sentence_segment['text']):
                self.user_food_intents["food_i_requestrecipe"] = True
                sentence_segment['user_food_intents'][
                    'food_i_requestrecipe'] = True
                self.user_food_intents["food_i_askquestion"] = False
                sentence_segment['user_food_intents'][
                    "food_i_askquestion"] = False

            # Request to talk about food through questions
            if re.search(r"food", sentence_segment['text']):
                self.user_food_intents["food_i_suggestfoodchat"] = True
                sentence_segment['user_food_intents'][
                    'food_i_suggestfoodchat'] = True
                self.user_food_intents["food_i_askquestion"] = False
                sentence_segment['user_food_intents'][
                    "food_i_askquestion"] = False

            # Request to talk about specific food
            if sentence_segment["detected_food"]:
                self.user_food_intents["food_i_suggestfood"] = True
                sentence_segment['user_food_intents'][
                    "food_i_suggestfood"] = True
                self.user_food_intents["food_i_askquestion"] = False
                sentence_segment['user_food_intents'][
                    "food_i_askquestion"] = False
           
            # Request to talk about specific food cuisine
            if sentence_segment['detected_cuisine'] and any(x not in self.context["user_context"]["talked_cuisine"] for x in sentence_segment["detected_cuisine"]):
                # detected food cusine
                sentence_segment['user_food_intents'][
                    'food_i_suggestfoodcuisine'] = True
                self.user_food_intents['food_i_suggestfoodcuisine'] = True

        # detect request restaurant information
        if detect_request_restaurant_pattern(sentence_segment['text'], sentence_segment['dialog_act']):
            self.user_food_intents["food_i_requestrestaurant"] = True
            sentence_segment['user_food_intents'][
                'food_i_requestrestaurant'] = True
            self.user_food_intents["food_i_askquestion"] = False
            sentence_segment['user_food_intents']["food_i_askquestion"] = False

        # detect answer_no intent
        if detect_answerno(sentence_segment['text']):
            self.user_food_intents['food_i_answerno'] = True
            sentence_segment['user_food_intents']['food_i_answerno'] = True

        # detect answer_yes intent
        if detect_answeryes(sentence_segment['text'],sentence_segment['intent'].get('lexical', [])):
            self.user_food_intents['food_i_answeryes'] = True
            sentence_segment['user_food_intents']['food_i_answeryes'] = True

        # detect answer_uncertain intent
        if detect_answeruncertain(sentence_segment['text'], sentence_segment['intent'].get('lexical', [])):
            self.user_food_intents['food_i_answeruncertain'] = True
            sentence_segment['user_food_intents'][
                'food_i_answeruncertain'] = True

        # detect answer_abandoned intent
        # if food_i_answerabandoned
        # if detect_answer_abandoned_pattern(sentence_segment['dialog_act']):
        if detect_answer_abandoned_pattern_update(self.returnnlp_util, sentence_segment_index):
            self.user_food_intents['food_i_answerabandoned'] = True
            sentence_segment['user_food_intents'][
                'food_i_answerabandoned'] = True
            if self.context["conversation_context"]["previous_state"] != "food_chitchat_ask":
                if not self.user_food_intents['food_i_askquestion']:
                    # Add this flag to use appropriate response
                    self.user_food_intents[
                        'food_i_answerabandoned_not_question'] = True

                    self.user_food_intents['food_i_askquestion'] = True
                    sentence_segment['user_food_intents'][
                        'food_i_askquestion'] = True

    def update_sentence_segment_context(self, sentence_segment):
        r"""
        Update the context of the conversation based on the detected food and detected intents
        """
        # Check it we want to followup with a entity that user is interested to
        # talk about
        user_food_context = self.context
        # selected_chitchat_key = user_sport_context['selected_chitchat_key']
        # selected_chitchat_id = user_sport_context['selected_chitchat_id']

        # Use User Sport Intents
        user_food_intents = sentence_segment['user_food_intents']

        # Update the not interested food list
        if user_food_intents['food_i_notinterestfood']:
            # Update the user profile with the not interested food
            for food in sentence_segment['detected_food']:
                if food['food'] not in self.context['user_context']['not_interested_food']:
                    self.context['user_context'][
                        'not_interested_food'].append(food['food'])

        # Check whether a special conditiona of question is matched
        special_question_intent = detect_ask_question_special(
            self.user_utterance)

        if special_question_intent[0]:
            self.user_food_intents['food_i_askquestion'] = True
            user_food_intents['food_i_askquestion'] = True
            sentence_segment['text'] = special_question_intent[1]

        # Udpate the user_utterance if user_detect_question
        if user_food_intents['food_i_askquestion']:
            # 4. TODO: Include Coreference into questions

            # if self.sys_info['sys_coreference']:
            #     sentence_segment['text'] = self.sys_info[
            #         'sys_coreference']['text']

            # Udpate the User Utterance with the question
            self.user_utterance = sentence_segment['text']

            # If detected_food and the detected_food is not in talked_food
            if sentence_segment['detected_food']:
                for food in sentence_segment['detected_food']:
                    if food['food'] not in self.context["user_context"]["talked_food"]:
                        self.context["conversation_context"][
                            "current_food"] = food
                        break

            # update the question_type,
            self.context["conversation_context"][
                "detected_question_types"] = []

            self.context["conversation_context"]["detected_question_types"].append(
                sentence_segment['dialog_act'][0])

            for topic in sentence_segment["intent"].get("topic", []):
                self.context["conversation_context"][
                    "detected_question_types"].append(topic)

            for lexical in sentence_segment.get("lexical", []):
                self.context["conversation_context"][
                    "detected_question_types"].append(lexical)

        elif user_food_intents['food_i_suggestfood']:
            self.context['conversation_context']['current_food'] = random.choice(sentence_segment['detected_food'])  # Randomly Picked a Food
        
        elif user_food_intents['food_i_suggestfoodcuisine']:
            self.context['conversation_context']['current_cuisine'] = random.choice(sentence_segment['detected_cuisine'])

        # detect whether there is a city mentioned in the utterance
        if user_food_intents['food_i_requestrestaurant'] or (self.context["conversation_context"]["previous_state"] == "food_recommend_restaurant_location" and self.context["conversation_context"]["restaurant_info"].get("city_id", None) is None):
            # detect the city
            self.context["conversation_context"][
                "detected_city"] = self.context["conversation_context"][
                "detected_city"] + detect_food_city(sentence_segment)

            # Determine whether added the detected entities to
            # context['detected_entities']
        if not user_food_intents['food_i_notinterestfood'] and not user_food_intents['food_i_notinterest']:
            if sentence_segment['detected_food']:
                self.context['conversation_context'][
                    'detected_food'] += sentence_segment['detected_food']

            if sentence_segment['detected_cuisine']:
                self.context['conversation_context'][
                    'detected_cuisine'] += sentence_segment['detected_cuisine']

            if sentence_segment['detected_category']:
                self.context['conversation_context'][
                    'detected_category'] += sentence_segment['detected_category']

            if sentence_segment['detected_country']:
                self.context['conversation_context'][
                    'detected_country'] += sentence_segment['detected_country']

    def update_sentence_context(self):
        r"""
        After update with each sentence segment, we update the context in a sentence level
        """
        #Do we apply_update_followup_question
        apply_update_followup_question = True

        if self.user_food_intents["food_i_requestrestaurant"]:
            # re-initialize the restaurant_info
            self.context["conversation_context"]["restaurant_info"] = {}

            # re-initialize the missed times
            self.context["conversation_context"]["missed_times"] = 0

            # detect the state information
            self.context["conversation_context"][
                "detected_state"] = self.context["conversation_context"][
                "detected_state"] + detect_food_state(self.user_utterance)

            # try to detect city_id
            if self.context["conversation_context"]["detected_city"]:
                self.context["conversation_context"]["restaurant_info"][
                    "current_city_name"] = self.context["conversation_context"]["detected_city"][0]

            if self.context["conversation_context"]["detected_state"]:
                self.context["conversation_context"]["restaurant_info"][
                    "current_state_name"] = self.context["conversation_context"]["detected_state"][0]

              # detect city_id
            self.context['conversation_context'][
                'restaurant_info']['city_id'] = detect_zomato_city_id(self.context["conversation_context"]["restaurant_info"].get('current_city_name', None), self.context["conversation_context"]["restaurant_info"].get('current_state_name', None))

            # update the cuisine information and
            if self.context["conversation_context"]["detected_cuisine"]:
                self.context["conversation_context"]["restaurant_info"][
                    "current_cuisine_name"] = self.context["conversation_context"]["detected_cuisine"][0]
                self.context["conversation_context"]["restaurant_info"]["cuisine_id"] = FOOD_CUISINE_LIST[self.context["conversation_context"]["restaurant_info"][
                    "current_cuisine_name"]]

            if self.context["conversation_context"]["detected_category"]:
                self.context["conversation_context"]["restaurant_info"][
                    "current_category_name"] = self.context["conversation_context"]["detected_category"][0]
                self.context["conversation_context"]["restaurant_info"]["category_id"] = FOOD_CUISINE_LIST[self.context["conversation_context"]["restaurant_info"][
                    "current_category_name"]]

        elif self.user_food_intents["food_i_requestrecipe"]:
            if self.context["conversation_context"]["detected_food"]:
                self.context["conversation_context"]["current_food"] = random.choice(self.context[
                    'conversation_context']['detected_food'])

        elif self.user_food_intents["food_i_askquestion"]:
            pass

        elif self.user_food_intents['food_i_suggestfood']:
            # self.context['conversation_context']['current_food'] = random.choice(self.context[
            #     'conversation_context']['detected_food'])  # Randomly Picked a Food

            # Update the chitchat_type
            self.context['conversation_context']['chitchat_type'] = "food"

            # Clear the selected question and followup questions
            self.context["conversation_context"]["selected_question"] = {}
            self.context["conversation_context"]["followup_questions"] = []
            #update favorite food
            if self.context['conversation_context']['current_food']['food'] not in self.context["user_context"]["favorite_food"]:
                self.context["user_context"]["favorite_food"].append(self.context['conversation_context']['current_food']['food'])
            
            apply_update_followup_question = False

        elif self.user_food_intents['food_i_suggestfoodcuisine']:
            self.context['conversation_context']['chitchat_type'] = "cuisine"

            # Clear the selected question and followup questions
            self.context['conversation_context']['selected_question'] = {}
            self.context['conversation_context']['followup_questions'] = []
            #update favorite cuisine
            if self.context['conversation_context']['current_cuisine'] not in self.context["user_context"]["favorite_cuisine"]:
                self.context["user_context"]["favorite_cuisine"].append(self.context['conversation_context']['current_cuisine'])
            apply_update_followup_question = False

        elif self.user_food_intents['food_i_answeruncertain']:
            # Clear the selected_question
            self.context["conversation_context"]["selected_question"] = {}

            # Update the selected questions based on the previous_state
        if self.context["conversation_context"]["previous_state"] != "food_chitchat_ask" and self.context["conversation_context"]["previous_state"] != "food_answer_question":
            # Clear the selected_question
            self.context["conversation_context"]["selected_question"] = {}
        else:
            if self.context["conversation_context"]["selected_question"].get("acknowledge", None) == 'ask_interest_food':
                self.user_food_intents['food_i_notinterestfoodchat'] = False

        # detect the state and detect city_id
        if self.context["conversation_context"]["previous_state"] == "food_recommend_restaurant_location" and self.context["conversation_context"]["restaurant_info"].get("city_id", None) is None:
            self.context["conversation_context"][
                "detected_state"] = self.context["conversation_context"][
                "detected_state"] + detect_food_state(self.user_utterance)

            # try to detect city_id
            if self.context["conversation_context"]["detected_city"] and (not self.context["conversation_context"]["restaurant_info"].get("current_city_name", None)):
                self.context["conversation_context"]["restaurant_info"][
                    "current_city_name"] = self.context["conversation_context"]["detected_city"][0]
                logging.info("[FOODMODULE] current_city is: {}".format(self.context["conversation_context"]["restaurant_info"][
                    "current_city_name"]))

            if self.context["conversation_context"]["detected_state"] and (not self.context["conversation_context"]["restaurant_info"].get("current_state_name", None)):
                self.context["conversation_context"]["restaurant_info"][
                    "current_state_name"] = self.context["conversation_context"]["detected_state"][0]
                logging.info("[FOODMODULE] current_state is: {}".format(self.context["conversation_context"]["restaurant_info"][
                    "current_state_name"]))

            # detect city_id
            self.context['conversation_context'][
                'restaurant_info']['city_id'] = detect_zomato_city_id(self.context["conversation_context"]["restaurant_info"].get('current_city_name', None), self.context["conversation_context"]["restaurant_info"].get('current_state_name', None))

            # If city_id is not captured
            if self.context['conversation_context']['restaurant_info']['city_id'] is None and self.context['conversation_context']['expect_next_state'] != 'food_chitchat_ask':
                self.context['conversation_context'][
                    'expect_next_state'] = 'food_recommend_restaurant_location'

        # detect the cuisine id and category_id
        if self.context["conversation_context"]["previous_state"] == "food_recommend_restaurant_cuisine" and self.context["conversation_context"]["restaurant_info"].get("cuisine_id", None) is None and self.context["conversation_context"]["restaurant_info"].get("cuisine_id", None) is None:
            # update the cuisine information and
            if self.context["conversation_context"]["detected_cuisine"]:
                self.context["conversation_context"]["restaurant_info"][
                    "current_cuisine_name"] = self.context["conversation_context"]["detected_cuisine"][0]
                self.context["conversation_context"]["restaurant_info"]["cuisine_id"] = FOOD_CUISINE_LIST[self.context["conversation_context"]["restaurant_info"][
                    "current_cuisine_name"]]

            if self.context["conversation_context"]["detected_category"]:
                self.context["conversation_context"]["restaurant_info"][
                    "current_category_name"] = self.context["conversation_context"]["detected_category"][0]
                self.context["conversation_context"]["restaurant_info"]["category_id"] = FOOD_CUISINE_LIST[self.context["conversation_context"]["restaurant_info"][
                    "current_category_name"]]

            # If cuisine id and category id is not captured
            if self.context['conversation_context']['restaurant_info'].get("cuisine_id", None) is None and self.context['conversation_context']['restaurant_info'].get("category_id", None) is None and self.context['conversation_context']['expect_next_state'] != 'food_chitchat_ask' and not self.user_food_intents["food_i_answerno"]:
                self.context['conversation_context'][
                    'expect_next_state'] = 'food_recommend_restaurant_cuisine'
        
        if apply_update_followup_question:
            # Update the followup questions based on users_answer
            self.update_followup_question_context()

        # Update the Unclear Status
        detected_other_topic = get_sys_other_topic(
            self.sys_info['features'], self.context)

        if detected_other_topic:
            self.context['conversation_context']['unclear'] = True
            # update the proposed topic if the return result from
            # get_sys_other_topic is a string
            if type(detected_other_topic) == str:
                self.context['conversation_context']['propose_topic'] = KEYWORD_MODULE_MAP[detected_other_topic]

            # If any exception happens then don't break out
            for exception_word in EXCEPTION_KEY_WORDS:
                # logging.info(
                #     "[FOODMODULE] exception_word is: {}".format(exception_word))
                # It is better to do user_utterance
                if re.search(exception_word, self.user_utterance):
                    self.context['conversation_context']['unclear'] = False
                    break

    def update_followup_question_context(self):
        r"""
        Determine whether we should include the specific followup in the context_list
        """
        # The special followup key:['follow up reasoning', 'answer_yes']
        #try:

        #Four special cases for followup
        
        if self.context["conversation_context"]["previous_state"] is None and self.module_selection.global_state.value == "ask_fav_cuisine":
            #include the subtopic
            self.context["user_context"]["talked_subtopic"].append("eat_cuisine")
        elif self.context["conversation_context"]["previous_state"] is None and self.module_selection.global_state.value == "ask_fav_food":
            #include the subtopic
            self.context["user_context"]["talked_subtopic"].append("eat")

        elif self.context["conversation_context"]["previous_state"] is None and re.search("eat", self.user_utterance) and not self.user_food_intents["food_i_suggestfood"]:
            #special topic of eating
            s_follow_concept = template_food.utterance(selector='qa_templates/qa_eat', slots={},
                                                            user_attributes_ref=self.user_attributes).copy()
            self.context["conversation_context"][
                        "followup_questions"].append(s_follow_concept)
            #Use one of the majority question
            #self.context["conversation_context"]["num_majority_chitchat"] += 1

            #update the talked_subtopic
            self.context["user_context"]["talked_subtopic"].append("eat")

        elif self.context["conversation_context"]["previous_state"] is None and re.search("cook", self.user_utterance):
            selected_q = template_food.utterance(selector='qa_templates/qa_cook', slots={}, user_attributes_ref=self.user_attributes).copy()
            special_follow_up_questions = selected_q["follow_up"]["answer_yes"]
            special_follow_up_questions.reverse()
            # Append the question to the follow up question
            for s_follow in special_follow_up_questions:
                # self.context["conversation_context"][
                #     "followup_questions"].append(s_follow['text'])
                self.context["conversation_context"][
                    "followup_questions"].append(s_follow)
            #Use one of the majority quesiton
            #self.context["conversation_context"]["num_majority_chitchat"] += 1

            #update the talked_subtopic
            self.context["user_context"]["talked_subtopic"].append("cook")



        # check the follow_up keys
        if ("selected_question" not in self.context["conversation_context"]
                or "follow_up" not in self.context["conversation_context"]["selected_question"]):
            return
        for x in list(self.context["conversation_context"]["selected_question"]["follow_up"].keys()):
            # initialize special follow up questions
            special_follow_up_questions = []
            if x == "follow_up_reasoning" and self.context["conversation_context"]["detected_food"]:
                special_follow_up_questions = self.context[
                    "conversation_context"]["selected_question"]["follow_up"][x]
                special_follow_up_questions.reverse()

                # Append the question to the follow up question
                for s_follow in special_follow_up_questions:
                    s_follow_concept = s_follow.copy()
                    for i, q in enumerate(s_follow['q']):
                        s_follow_concept['q'][i] = q.format(
                            food=self.context["conversation_context"]["detected_food"][0]['food'])

                    self.context["conversation_context"][
                        "followup_questions"].append(s_follow_concept)

                # Change the expect_next_state to chit_chat_ask
                self.context["conversation_context"][
                    "expect_next_state"] = "food_chitchat_ask"

                logging.info(
                    "[FOODMODULE] Special Followup, follow_up_reqsoning,  is captured")

            if x == "follow_up_reasoning_dish" and any([x["category"] == "dish" for x in self.context["conversation_context"]["detected_food"]]):
                detected_dish = None
                for food in self.context["conversation_context"]["detected_food"]:
                    if food["category"] == "dish":
                        detected_dish = food["food"]

                special_follow_up_questions = self.context[
                    "conversation_context"]["selected_question"]["follow_up"][x]
                special_follow_up_questions.reverse()

                # Append the question to the follow up question
                for s_follow in special_follow_up_questions:
                    s_follow_concept = s_follow.copy()
                    for i, q in enumerate(s_follow['q']):
                        s_follow_concept['q'][i] = q.format(
                            food=detected_dish)

                    self.context["conversation_context"][
                        "followup_questions"].append(s_follow_concept)

                # Change the expect_next_state to chit_chat_ask
                self.context["conversation_context"][
                    "expect_next_state"] = "food_chitchat_ask"

                logging.info(
                    "[FOODMODULE] Special Followup, follow_up_reqsoning,  is captured")

            if x == "answer_yes" and self.user_food_intents["food_i_answeryes"]:
                special_follow_up_questions = self.context[
                    "conversation_context"]["selected_question"]["follow_up"][x]
                special_follow_up_questions.reverse()

                # Append the question to the follow up question
                for s_follow in special_follow_up_questions:
                    # self.context["conversation_context"][
                    #     "followup_questions"].append(s_follow['text'])
                    self.context["conversation_context"][
                        "followup_questions"].append(s_follow)

                logging.info(
                    "[FOODMODULE] Special Followup, answer_yes,  is captured")

            if x == "answer_no" and self.user_food_intents["food_i_answerno"]:
                special_follow_up_questions = self.context[
                    "conversation_context"]["selected_question"]["follow_up"][x]
                special_follow_up_questions.reverse()

                for s_follow in special_follow_up_questions:
                    # self.context["conversation_context"][
                    #     "followup_questions"].append(s_follow['text'])
                    self.context["conversation_context"][
                        "followup_questions"].append(s_follow)

                logging.info(
                    "[FOODMODULE] Special Followup, answer_no,  is captured")

            if x == "answer_yes_short" and self.user_food_intents["food_i_answeryes"] and len(self.user_utterance.split()) < 4:
                special_follow_up_questions = self.context[
                    "conversation_context"]["selected_question"]["follow_up"][x]
                special_follow_up_questions.reverse()

                # Append the question to the follow up question
                for s_follow in special_follow_up_questions:
                    # self.context["conversation_context"][
                    #     "followup_questions"].append(s_follow['text'])
                    self.context["conversation_context"][
                        "followup_questions"].append(s_follow)

                logging.info(
                    "[FOODMODULE] Special Followup, answer_yes_short,  is captured")

            if x == "answer_yes_no_food" and self.user_food_intents["food_i_answeryes"] and not self.context["conversation_context"]["detected_food"]:
                special_follow_up_questions = self.context[
                    "conversation_context"]["selected_question"]["follow_up"][x]
                special_follow_up_questions.reverse()

                # Append the question to the follow up question
                for s_follow in special_follow_up_questions:
                    # self.context["conversation_context"][
                    #     "followup_questions"].append(s_follow['text'])
                    self.context["conversation_context"][
                        "followup_questions"].append(s_follow)

                # Change the expect_next_state to chit_chat_ask
                self.context["conversation_context"][
                    "expect_next_state"] = "food_chitchat_ask"

                logging.info(
                    "[FOODMODULE] Special Followup, answer_yes_no_food,  is captured")

            if x == "answer_country" and self.context["conversation_context"]["detected_country"] and not self.user_food_intents["food_i_answerno"]:

                special_follow_up_questions = self.context[
                    "conversation_context"]["selected_question"]["follow_up"][x]
                special_follow_up_questions.reverse()
                # Append the question to the follow up question
                for s_follow in special_follow_up_questions:
                    # self.context["conversation_context"][
                    #     "followup_questions"].append(s_follow['text'])
                    self.context["conversation_context"][
                        "followup_questions"].append(s_follow)

                # Change the expect_next_state to chit_chat_ask
                # self.context["conversation_context"][
                #     "expect_next_state"] = "food_chitchat_ask"

                # Update the context
                self.context["conversation_context"]["current_food_country"] = self.context[
                    "conversation_context"]["detected_country"][0]

                logging.debug(
                    "[FOODMODULE] Special Followup, answer_country  is captured")

            if x == "answer_food_short" and self.context["conversation_context"]["detected_food"] and len(self.user_utterance.split()) < 5:
                special_follow_up_questions = self.context[
                    "conversation_context"]["selected_question"]["follow_up"][x]
                special_follow_up_questions.reverse()

                # Append the question to the follow up question
                for s_follow in special_follow_up_questions:
                    self.context["conversation_context"][
                        "followup_questions"].append(s_follow)

                logging.debug(
                    "[FOODMODULE] Special Followup, answer_food_short  is captured")


        # except Exception as e:
        #     logging.error(
        #         "[FOODMODULE] No Followup is captured. err: {}".format(e), exc_info=True)
        #     pass

    def generate_response(self):
        # Add a logic to directly give backstory response
        
        response = self.food_state_management.get_response()
        # if propose_topic is not None, change the user attributes
        if self.context["conversation_context"]["propose_topic"]:
            # self.user_attributes.propose_topic = self.context[
            #     'conversation_context']['propose_topic']
            self.module_selection.propose_topic = self.context["conversation_context"]["propose_topic"]
        #change the backstory_reason to default, if previous state is not reason
        if self.context["conversation_context"]["previous_state"] != "food_answer_question":
            self.context["conversation_context"]["backstory_reason"] = None
        
        #update the profile of user
        if self.context["user_context"]["favorite_cuisine"]:
            self.user_profile.topic_module_profiles.food._storage['fav_cuisine'] = self.context["user_context"]["favorite_cuisine"][0]
        if self.context["user_context"]["talked_cuisine"]:
            self.user_profile.topic_module_profiles.food._storage['talked_cuisine']  = list(self.context["user_context"]["talked_cuisine"].keys())
        if self.context["user_context"]["favorite_food"]:
            self.user_profile.topic_module_profiles.food._storage['fav_food'] = self.context["user_context"]["favorite_food"][0]
        if self.context["user_context"]["talked_food"]:
            self.user_profile.topic_module_profiles.food._storage['talked_food']  = list(self.context["user_context"]["talked_food"].keys())

        #update the status of food module
        if self.context["user_context"]["run_out_of_food"]:
            self.module_selection.add_out_of_template_topic_module("FOODCHAT")
        return response


def food_response_generator(user_utterance, sys_info, user_attributes):
    # Initalize a Food Chatbot

    # Initialize the food_chatbot
    food_chatbot = Food_Chatbot(user_utterance, sys_info, user_attributes)

    # Generate the response
    food_response = food_chatbot.generate_response()

    food_response = {
        "response": food_response,
        "foodchat": {
            "context": food_chatbot.context['conversation_context'],
            "user": food_chatbot.context['user_context'],
            "propose_continue": food_chatbot.context['propose_continue']
        }
    }

    return food_response
