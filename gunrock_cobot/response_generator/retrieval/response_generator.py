import random
import re
import logging
import utils
from cobot_core.service_module import LocalServiceModule
import praw
import string
from util_redis import RedisHelper
from nlg_post_process.profanity_classifier import BotResponseProfanityClassifier
from nlu.constants import TopicModule, DialogAct
from template_manager.template import Template
from template_manager.manager import TemplateManager
from selecting_strategy.module_selection import ModuleSelector
from user_profiler.user_profile import UserProfile
from utils import trace_latency
import requests
import json
import time

generation_server_url = 'http://34.207.72.17:5005/generation_ft/generate'
generation_timeout = (0.2, 0.5)

PREFIX_BLENDER_RETRIEVAL = "gunrock:blender:retrieval"
blender_length_append_threshold = 8

class TopicRetrievalResponseGenerator(LocalServiceModule):

    KEYS = utils.get_reddit_key()
    N_TITLES = 2
    N_COMMENTS = 1

    SENT_LEN_LIMIT = 25
    TITLE_LEN_LIMIT = 25

    # TIL_TITLE_TEMPLATE = ["that's an interesting topic. I learned that: ", "did you know that: ", "i heard that: "]
    # this new title works for TIL and showerthoughts
    # TIL_TITLE_TEMPLATE = ["that is an interesting topic. You know, I realized that: ",
    #                       "that is an interesting topic. That reminds me that: ", " I was thinking about this: "]
    TIL_TITLE_TEMPLATE = ["oh, I didn't know you are interested in <TOPIC>. You know, That reminds me that: ",
                            "oh, <prosody rate=\"slow\"><TOPIC></prosody>? interesting choice! That reminds me that: ",
                            "Well, about <TOPIC>, I was thinking that: ", "You know what's interesting about <TOPIC>? "]
    CONTINUE_TOPIC_TEMPLATE = ["Sure! Let's keep talking about <TOPIC>. You know, I was thinking that: ",
                            "Well, about <TOPIC>, I was thinking that: ",
                            "You know what else is interesting about <TOPIC>? "]
    TIL_COMMENT_TEMPLATE = [" , it got me thinking that: ",
                            " , and, i also heard that, ", " , and someone told me "]
    TITLE_TEMPLATE = ["oh, not to change the subject, but, ",
                      "the other day i was thinking about this, "]
    RELATED_TITLE_TEMPLATE = ["<say-as interpret-as=\"interjection\">Hmm...</say-as>. I don't know much about <TOPIC> specifically. However, I do know that ",
                        "I'm no expert on <TOPIC>, but I thought of something related! I learned that: ",
                        "Honestly, I'm still learning about <TOPIC>, but I thought this might be relevant. I was thinking that: "]
    COMMENT_TEMPLATE = [" , and then i thought that ", " , and now i'm thinking that "]

    NO_BODY_IN_COMMENT_TEMPLATE = ["it's a really hard question. I still can't figure it out. What do you think", "That's a tough question. I need more time to think about it some more. What are your thoughts?",
                                   "<say-as interpret-as=\"interjection\">Uh oh</say-as>! I'm drawing a blank and need more time to learn about that! Can you tell me more about it?"]
    # cobot profanity_check can give a 2 seconds latency, so used a list
    # instead
    BLACKLIST = ["ass", "asshole", "assholes", "anal", "anus", "arsehole", "arse", "bitch", "bangbros", "bastards", "bastard",
                 "bitch", "tit", "butt", "blow job", "blowjob", "boob", "bra", "cock", "cum", "cunt", "dick", "shit", "cocaine",
                 "sex", "erotic", "fuck", "fxxk", "f\*\*k", "f\*\*c", "f\*ck", "fcuk", "fcking", "fck", "fucken", "fucked", "fucks", "gang bang", "gangbang",
                 "genital", "damn", "damnit", "horny", "jerk off", "jerkoff", "kill", "loser", "masterbate", "masterbates", "masterbated", "naked",
                 "nasty", "negro", "nigger", "nigga", "orgy", "pussy", "penis", "perve", "rape", "fucked",
                 "fucking", "trump", "racist", "sexy", "strip club", "vagina", "faggot", "fag", "drug", "weed", "marijuana", "cannabis", "lsd",
                 "kill", "murder", "incel", "prostitute", "slut", "whore", "hooker", "cuck", "cuckold", "shooting", "nazis",
                 "nazism", "nazi", "sgw", "balls", "thot", "virgin", "virginity", "cumshot", "cumming", "dildo", "vibrator", "sexual",
                 "douche", "douche", "bukkake", "douchebag", "porn", "porngraph", "porngraphy", "died", "killed",
                 "killing", "murdered", "murdering", "kills", "murders", "clit", "clitoris", "masterbate tatoo", "cheat",
                 "cheated", "cheating", "dammit", "toxicity", "toxic", "wtf", "breasts", "tits", "boobies", "dead", "sperm",
                 "death", "pornhub", "intercourse", "pornography", "porno", "boobs", "penisis", "my husband", "my boyfriend",
                 "my girlfriend", "my wife", "end my life", "reddit", "reddit,", "reddit:", "subreddit", "tifu", "post raw", "show discussion thread",
                 "penises", "dildos", "vaginas", "school shooting", "breasts", "showerthoughts", "todayilearned", "r/.*", "u/.*", "faggot", "faggots", "f word", "suicidal"]

    IGNORE_NP_LIST = ["of course", "the next week", "kind of", "smell", "the words", "talk", "three four", "three", "four",
                    "any", "facts", "truth", "a", "turn ten", "turn the", "ten years", "please", "the name", "a litle bit", "kind of",
                    "one", "two", "five", "six", "seven", "eight", "nine", "ten", "we", "good point", "course", "i guess", "i guess so",
                    "guess", "mean", "a lot", "a little", "this question", "to talk", "more"]

    REQ_TOPIC_JUMP_PATTERN = r"(\blet's\b|\blet us\b|\blets\b|\bwe can\b)\s*(talk|chat|keep talking).*about|(\bi\b|\bwe\b)\s*(want to|expect to|like to|would like to|wanna|want)\s*(talk|chat|ask you|ask|know|keep talking) about|(\bhow about\b|\bcan we\b|\bcan you\b|\bcould we\b|i'd like to)\s*(talk|chat) about|why (dont|don't) we (talk|chat) about|do you know anything about|do you know|i (like|love)|tell me.*about|tell me|give me|how about|what about|what is it about|my favorite topic is|talk to me about|i'd (like|love) to talk about|talk about|have you heard about|want to know|you (are|were) talking about|(want to|wanna) talk|tell me.*about"
    OPINION_PATTERN = r"^what do you think (about|of)|how do you think that|why do you think that|how do you think|why do you think|what do you like about|what do you love about|what makes you|how do you feel about|^how is |^how was|how do you like|why do you (like|love)|what kind of| your opinion (of|about|concerning|regarding|on)|your view (of|about|concerning|regarding|on)|your thoughts (of|about|concerning|regarding|on)|do you know (anything|something) about|(who|what) do you think|do you believe in|do you believe that|do you believe|what.* best|who.* best|which.* best|which \w+ are you|what.*it like|would you (like to|wanna) talk about|would you (like to|wanna)|do you (love|like)"
    WRONG_PATTERN = r"why.*did you bring|(why|how).*relevant|(what|why|how).*have to|what.*mean|make.*no.*sense|makes.*no.*sense|(why|how|(in what way)).*(related|relevant)|nothing to do with|why.*(did|do) you say|why are you telling|(does not|doesn't) make sense|not making sense|(you're|you are) talking about|i (don't|didn't|did not) get"
    REMOVE_LIST = [r"https\S+", r"http\S+", r"\btil\b", r"tldr", r"tl:dr", r"tl dr", r"tl;dr", r"nbsp", r"HTTPS\S+", r"HTTP\S+", r"TIL.", r"TIL",
                   r"TLDR", r"TL:DR", r"TL DR", r"TL;DR", r"NBSP", r"Https\S+", r"Http\S+", r"Til", r"Tldr", r"Tl:dr", r"Tl dr", r"Tl;dr", r"Nbsp"]
    # SPEC_CHAR = "#&\()*+/:<=>@[\\]'^_`{|}~"
    SPEC_CHAR = "#&\()*+/:<=>@[\\]^_`{|}~"
    NEG_PATTERN = r"(not|isn't|wasn't).*(cool|interesting|interested|funny|a good)|sad|weird|disgusting|boring|bored|horrible|terrible|messed up|not interested|terrible|awful|pathetic|silly|unpleasant|bad|you are.*mean"
    POS_PATTERN = r"((that|thats|it's).*(cool|interesting|awesome|neat|sweet|fascinating|surprising|fantastic|crazy|amazing|fair|nice|smart|clever|great|love it|a good one)|wow|i agree)|(^cool$|^interesting$|^awesome$|^neat$|^sweet$|^fascinating$|^surprising$|^fantastic$|^crazy$|^amazing$|^fair$|^nice$|^smart$|^clever$|^great$|^wow$|of course)|you are welcome|you're welcome"
    FUNNY_PATTERN = r"((that|that's|it's).*(funny|hilarious))|^funny$|^hilarious$"
    OKAY_PATTERN = r"okay|yes|yeah|sure|oh|right|i see"
    NO_PATTERN = r"no|next"
    IDK_PATTERN = r"(did not|do not|didn't|don't) know|no i didn't"
    IK_PATTERN = r"(knew|(did know)|(heard about))|^me too$"
    MY_NAME_PATTERN = r"my name"
    THANK_YOU_PATTERN = r"thank you|\bthanks\b"
    KNOW_MY_FRIEND_PATTERN = r"know.*(husband|wife|son|daughter|friend|mom|dad|boyfriend|girlfriend)"
    BROKEN_PATTERN = r"you are broken"

    STUPID_PATTERN = r"you are ((not.*(smart|good))|stupid)|don't.*like you"
    YOU_CREATOR_PATTERN = r"your mom|you creators"
    YOU_MOM_PATTERN = r"(your|yo|you) (mom|mother|mama)"
    DONE_PATTERN = r"back to work|done talking|i'm done"
    FRIEND_PATTERN = r"are we friends|have (a friend|friends)|be friends"
    HOW_DID_YOU_PATTERN = r"how did you"
    KEEP_CHATTING_PATTERN = r"keep (chatting|talking|going)|let's hear|something (with|within|about|on|of) the topic|continue|go on|the same topic"
    WAKEUP_PATTERN = r"wake up"
    NEW_NAME_PATTERN = r"rename you|your new name|call you|change your.*name"
    CURSE_PATTERN = r"don't curse|inappropriate"
    RESTART_PATTERN = r"let's have.*conversation"
    GENERAL_COMMAND_PATTERN = ["let's chat", "let's talk"]
    ALEXA_PATTERN = ["you", "your life"]
    ASK_Q_PATTERN = r"ask me a question|ask me questions"
    WHO_SOMEONE = r"who.*someone"

    GENERATION_FILTER_LIST = ["lol.", "lol", "thread", "til"]
    generation_string = str.maketrans(dict.fromkeys(string.punctuation))

    EXPIRE_TIME = 7 * 24 * 60 * 60

    def execute(self):
        self.module_selector = ModuleSelector(self.state_manager.user_attributes)
        self.template_manager = TemplateManager(Template.retrieval, self.state_manager.user_attributes)
        self.blender_start_time = getattr(self.state_manager.user_attributes, "blender_start_time", None)

        response = self._query(self.input_data)
        setattr(self.state_manager.current_state, "generation_request", response.get("user_attributes", {}).get("generation_request"))

        return response

    def _rm_speechcon(self, request):
        return re.sub(r'\<.*?\>', "", request)

    def is_generation_mode(self):
        # a_b_test = getattr(self.state_manager.current_state, 'a_b_test', "")
        # return a_b_test == 'B'
        return False

    def is_blender_mode(self):
        a_b_test = getattr(self.state_manager.current_state, 'a_b_test', "")

        return a_b_test == 'C'
        # return True

    def _query(self, utterance, sub=None):

        topic_keyword = ""
        req_strong = False
        t_template = self.TITLE_TEMPLATE
        c_template = self.COMMENT_TEMPLATE

        # history
        history_turns = False
        topic_keyword = ""
        noun_phrase = []
        topicClass = ""
        skip_comment = False
        do_coref = False

        try:
            if history_turns:
                if "features" in utterance and "topic" in utterance["features"][0] and utterance["features"][0]["topic"] is not None and len(utterance["features"][0]["topic"][0]["topicKeywords"]) > 0:
                    topic_keyword = utterance["features"][0][
                        "topic"][0]["topicKeywords"][0]["keyword"]
                    if topic_keyword is None:
                        topic_keyword = ""

                if "features" in utterance and "intent_classify" in utterance["features"][0] and "sys" in utterance["features"][0]["intent_classify"]:
                    if "req_topic_jump" in utterance["features"][0]["intent_classify"]["sys"]:
                        req_strong = True

                if "features" in utterance and "topic" in utterance["features"][0] and utterance["features"][0]["topic"] is not None and "topicClass" in utterance["features"][0]["topic"][0]:
                    topicClass = utterance["features"][
                        0]["topic"][0]["topicClass"]

                if "features" in utterance and "noun_phrase" in utterance["features"][0]:
                    noun_phrase = utterance["features"][0]["noun_phrase"]

                if "slots" in utterance and "text" in utterance["slots"][0]:
                    utterance = utterance["slots"][0]["text"]["value"]
                else:
                    utterance = utterance["text"][0]
            else:
                if "features" in utterance and "topic" in utterance["features"] and utterance["features"]["topic"] is not None and len(utterance["features"]["topic"][0]["topicKeywords"]) > 0:
                    topic_keyword = utterance["features"][
                        "topic"][0]["topicKeywords"][0]["keyword"]
                    if topic_keyword is None:
                        topic_keyword = ""

                if "features" in utterance and "intent_classify" in utterance["features"] and "sys" in utterance["features"]["intent_classify"]:
                    if "req_topic_jump" in utterance["features"]["intent_classify"]["sys"]:
                        req_strong = True

                if "features" in utterance and "topic" in utterance["features"] and utterance["features"]["topic"] is not None and "topicClass" in utterance["features"]["topic"][0]:
                    topicClass = utterance["features"][
                        "topic"][0]["topicClass"]

                if "features" in utterance and "noun_phrase" in utterance["features"]:
                    noun_phrase = utterance["features"]["noun_phrase"]

                if "features" in utterance and "spacynp" in utterance["features"]:
                    spacynp = utterance["features"]["spacynp"]
                    if spacynp is None:
                        spacynp = []

                if "features" in utterance and "coreference" in utterance["features"] and utterance["features"]["coreference"] is not None:
                    coref_text = utterance["features"]["coreference"]["text"]
                    if coref_text != "" and coref_text != " ":
                        utterance = coref_text
                        do_coref = True

                # if "features" in utterance and "returnnlp" in utterance["features"]:
                #     returnnlp = utterance["features"]["returnnlp"]
                returnnlp = self.state_manager.current_state.features[
                    "returnnlp"]
                if returnnlp is None:
                    returnnlp = []
                # if "features" in utterance and "central_elem" in utterance["features"]:
                #     central_elem = utterance["features"]["central_elem"]
                central_elem = self.state_manager.current_state.features[
                    "central_elem"]

                if "slots" in utterance and "text" in utterance["slots"]:
                    utterance = utterance["slots"]["text"]["value"]
                else:
                    utterance = utterance["text"]

                # for interactive mode where "open gunrock and" is appended to
                # the utterance
                utterance = re.sub("open gunrock and", "", utterance).strip()
                utterance = re.sub("please", "", utterance).strip()

                # if self.state_manager.user_attributes.response is not None:
                #     general_last_response = self.state_manager.user_attributes.response
                # else:
                #     general_last_response = ""

                # last_state = self.state_manager.last_state
                # if last_state:
                #     general_last_response = last_state.get("response", "")
                # else:
                #     general_last_response = ""


        except Exception:
            pass

        last_state = self.state_manager.last_state
        if last_state:
            general_last_response = last_state.get("response", "")
        else:
            general_last_response = ""

        self.user_profile = UserProfile(self.state_manager.user_attributes)
        self.session_id = getattr(self.state_manager.current_state, "session_id", "")
        self.redis_client = RedisHelper()

        # for generation model
        his_use_generation = True
        his_use_blender = True
        generation_history_context_list = []
        if self.state_manager.session_history is not None:
            num_history = len(self.state_manager.session_history)
            # logging.info(
            #     "[Generation_Retrieval] num_history: {}".format(num_history))
            # logging.info(
            #     "[Generation_Retrieval] session history: {}".format(self.state_manager.session_history))
            his_use_generation = False if num_history >= 3 else True
            his_use_blender = False if num_history >= 3 else True
            for h_i in range(num_history):
                selected_modules = self.state_manager.session_history[h_i].get("selected_modules", "")
                if isinstance(selected_modules, list):
                    selected_module = selected_modules[0]
                else:
                    selected_module = ""
                # logging.info(
                #     "[Generation_Retrieval] h_i: {}".format(h_i))
                # logging.info(
                #     "[Generation_Retrieval] selected_modules: {}".format(selected_module))
                if selected_module != "RETRIEVAL":
                    his_use_generation = True
                    his_use_blender = True
                    his_response = self.state_manager.session_history[h_i].get("response", "")
                    logging.info(
                        "[Generation_Retrieval] his_text: {}".format(his_response))
                    if len(his_response) > 0:
                        his_response = self._rm_speechcon(his_response)
                        if h_i != 0:  # do not add <EOS> to the last response (added by the model)
                            his_response += " <|endoftext|>"
                    generation_history_context_list.append(his_response)
                    break
                else:
                    his_text = self.state_manager.session_history[h_i].get("text", "")
                    logging.info(
                        "[Generation_Retrieval] his_text: {}".format(his_text))
                    if len(his_text) > 0:
                        his_text += " <|endoftext|>"
                    his_response = self.state_manager.session_history[h_i].get("response", "")
                    logging.info(
                        "[Generation_Retrieval] his_text: {}".format(his_response))
                    if len(his_response) > 0:
                        his_response = self._rm_speechcon(his_response)
                        if h_i != 0:  # do not add <EOS> to the last response (added by the model)
                            his_response += " <|endoftext|>"
                    generation_history_context_list.append(his_response)
                    generation_history_context_list.append(his_text)
                if h_i == 2:
                    break

        logging.info("[Generation_Retrieval] generation_his_list: {}".format(generation_history_context_list))
        # logging.info("[Generation_Retrieval] session_history 0: {}".format(self.state_manager.session_history))

        request_history = ""
        if self.is_generation_mode() and his_use_generation:
            current_use_generation = True
            request_history = " ".join(t for t in generation_history_context_list[::-1])
        else:
            current_use_generation = False
            
        if self.is_blender_mode() and his_use_blender:
            current_use_blender = True
        else:
            current_use_blender = False

        logging.info("[Generation_Retrieval] use generation: {}".format(current_use_generation))
        logging.info("[Generation_Retrieval] request_his: {}".format(request_history))
        if current_use_generation:
            logging.info("[Generation_Retrieval] request_his: {}".format(request_history))

        generation_retrieval_backup = ""  # store information in case generation timeout

        # end for generation model

        # handling short sentence requests (error handling)
        error_respond = ""
        ignore_list = ["yes", "no", "wow", "that", "this",
                       "cool", "you", "yeah", "ya", "it", "sorry", "please"]
        propose = False
        general_clarification = ["what's that? can you clarify that for me? ", "<say-as interpret-as=\"interjection\">darn</say-as>! i didn't catch that. could you rephrase that for me? ",
                                 "<say-as interpret-as=\"interjection\">oh dear</say-as>! I didn't hear you clearly. Maybe you could rephrase that? "]
        what_clarification = ["<say-as interpret-as=\"interjection\">just kidding</say-as>! what were we talking about again? ", "<say-as interpret-as=\"interjection\">ruh roh</say-as>! never mind then. What should we talk about next? ",
                              "<say-as interpret-as=\"interjection\">Whoops a daisy</say-as>! Okay, let's move on. What's on your mind? "]
        pos_acknowledgement = ["<say-as interpret-as=\"interjection\">Booya</say-as>! I know, right? ",
                               "<say-as interpret-as=\"interjection\">Righto</say-as>! I'm glad you liked it! ", "<say-as interpret-as=\"interjection\">Uh huh</say-as>! yeah, right? "]
        funny_acknowledgement = ["<say-as interpret-as=\"interjection\">Tee hee</say-as>! I LOL'd at that as well! ",
                                 "<say-as interpret-as=\"interjection\">Tee hee</say-as>! If I could giggle, i would! ", "<say-as interpret-as=\"interjection\">Woo hoo</say-as>! I'm glad you get my awesome humor! "]
        neg_acknowledgement = ["<say-as interpret-as=\"interjection\">tsk tsk</say-as>! I'm sorry that brought the mood down. I'll do better next time. ", "<say-as interpret-as=\"interjection\">Oh dear</say-as>! thanks for the feedback. I'll share something better next time. ",
                               "<say-as interpret-as=\"interjection\">Ruh roh</say-as>! I'm sorry you didn't like that. I'm still learning, so thanks for letting me know! "]
        okay_acknowledgement = ["<say-as interpret-as=\"interjection\">Aha</say-as>! Sounds good to me! ",
                                "<say-as interpret-as=\"interjection\">Righto</say-as>! ", "<say-as interpret-as=\"interjection\">Uh huh</say-as>! Okay, I see. "]
        no_acknowledgement = ["<say-as interpret-as=\"interjection\">Oh dear</say-as>! that's fine. Never mind then. ",
                              "Sure thing, Moving on then. ", "Understood. ", "<say-as interpret-as=\"interjection\">Oh brother</say-as>! "]
        idk_acknowledgement = ["Happy to share knowledge! ",
                               "That's understandable; you're not plugged into the internet 24/7. ", "I'm glad we both learned new things today! "]
        ik_acknowledgement = ["<say-as interpret-as=\"interjection\">okey dokey</say-as>! I'm glad we're on the same page then! ",
                              "<say-as interpret-as=\"interjection\">no way</say-as>! Great minds must think alike! What did you think about it when you first heard it? ", "<say-as interpret-as=\"interjection\">well well</say-as>, of course you already knew that: you are pretty smart! "]
        me_talk = ["<say-as interpret-as=\"interjection\">you bet</say-as>! I do enjoy learning more about you! ", "<say-as interpret-as=\"interjection\">all righty</say-as>! Tell me something interesting! ",
                   "<say-as interpret-as=\"interjection\">ooh la la</say-as>! I'd be happy to chat. what do you want to say? "]
        you_are_broken = [
            "<say-as interpret-as=\"interjection\">wah wah,</say-as> the only thing broken is my heart. "]

        stupid_response = ["Stupid is as stupid does, says Forrest Gump. ", "That's not very nice. I'm sure you didn't mean that. ",
                           "Thank you for your constructive criticism. I will never forget it. "]
        you_creator_response = [
            "My creators are awesome! What about your creators? ", "I'm much more interested in your creators. "]
        your_mom_response = ["My mom is awesome. What about your mother? "]
        done_response = ["Okay, sounds good. Thanks for chatting with me! Have a good rest of your day! To exit social mode, just say stop!",
                         "Okay, sounds good. Thanks for chatting with me! To exit social mode, just say stop!", "Okay, sounds good! Thanks for chatting with me! Hope to talk to you again soon! To exit social mode, just say stop!"]
        friend_response = ["I thought we were friends! ",
                           "You've got a friend in me! "]
        how_did_you_response = ["I am all-knowing! Just kidding. I have good intuition. ",
                                "I know these things because I'm learning without stopping! "]
        keep_response = ["<say-as interpret-as=\"interjection\">As you wish!</say-as> What were we talking about again?",
                         "I can go on and on! But...  <say-as interpret-as=\"interjection\">oh boy</say-as> I seem to have lost my train of thought. Remind me what we were talking about?"]
        wakeup_response = ["It's too late. I'm already awake. What do you wanna talk about? ",
                           "Don't worry: I'm awake and listening! What do you wanna talk about? "]
        new_name_response = ["Hmm, no thanks. I like my name as it is. Alexa! ",
                             "Let me think about it. Hmm, how about instead, you call me Alexa? "]
        curse_response = ["<say-as interpret-as=\"interjection\">Oh my</say-as>! I'm sorry. I didn't mean to say that! ",
                          "<say-as interpret-as=\"interjection\">Whoops a daisy</say-as>! did I say that? I'm sorry, I'll watch myself next time."]
        why_response = ["I don't know. Just a random thought. "]

        random_thoughts = ["the word ambiguous only has one meaning", "If you drop an Oreo you can still safely eat two thirds of it", "Your stomach thinks all potatoes are mashed.",
                            "Nikola Tesla is now best known for not being well known.", "if you replace the w in when, what, and where with a t, you answer the question",
                            "Leonardo DiCaprio never died in Titanic. You know, the end scene of Titanic is of him going underwater, and you know what, The beginning scene of Inception is him waking up on a beach. Its like a movie within a movie"]
        # don't use regex because we only do this when no/yes is the only thing
        # in the utterance
        no_list = ["no", "na", "nay", "no no", "next", "i don't think so",
                   "i don't want to", "i said no", "no not really", "no that's fine"]
        okay_list = ["ok", "okay", "sure", "yes", "yeah", "right",
                     "oh", "really", "oh really", "okay okay", "oh yeah", "yup"]
        command_list = ["play", "set"]
        why_list = ["why"]

        da_threshold = 0.4
        backstory_threshold = 0.9

        response_list = []
        append_front = ""
        central_text = central_elem["text"]

        context_utterance = ""
        has_command = False

        generation_quick_response = False

        for i in range(len(returnnlp)):
            quick_response = ""
            quick_response_propose = False
            lexical_intent = ""
            da = returnnlp[i]["dialog_act"]
            da_tag = da[0]
            da_score = float(da[1])
            lex_intent = returnnlp[i]["intent"]["lexical"]
            i_text = returnnlp[i]["text"]
            # for generation model:
            context_utterance += i_text + ", "
            da2 = returnnlp[i]["dialog_act2"]
            da2_tag = da2[0]
            da2_score = float(da2[1])
            if (da == "commands" and da_score > da_threshold) or (da2_tag == "commands" and da2_score > da_threshold):
                has_command = True
            # end for generation model
            i_noun_phrase = returnnlp[i]["noun_phrase"]
            # double check to see if a noun phrase is actually a noun phrase in the whole sentence
            if len(i_noun_phrase) > 1:
                for i_np in i_noun_phrase:
                    if i_np not in spacynp or i_np in self.IGNORE_NP_LIST:
                        i_noun_phrase.remove(i_np)
            if "topic" in returnnlp[i] and returnnlp[i]["topic"] is not None and "topicKeywords" in returnnlp[i]["topic"]:
                i_topicKeywords = returnnlp[i]["topic"]["topicKeywords"]
            else:
                i_topicKeywords = ""
            for lex in lex_intent:
                if lex == "ask_yesno":
                    lexical_intent = "yes_no_question"
                    break
                if lex.startswith("ask"):
                    lexical_intent = "open_question_opinion"
                    break
            if re.search(self.NEG_PATTERN, i_text.lower()):
                quick_response = neg_acknowledgement[random.randint(
                    0, len(neg_acknowledgement) - 1)]
                append_front = quick_response
                quick_response_propose = True
            elif re.search(self.POS_PATTERN, i_text.lower()) or (da_tag == "pos_answer" and len(i_noun_phrase) < 1 and da_score > da_threshold):
                quick_response = pos_acknowledgement[random.randint(
                    0, len(pos_acknowledgement) - 1)]
                append_front = quick_response
                if "yeah, right?" in general_last_response or "I know, right?" in general_last_response:
                    quick_response_propose = True
            elif i_text.lower() in no_list or (da_tag == "neg_answer" and len(i_noun_phrase) < 1 and da_score > da_threshold):
                quick_response = no_acknowledgement[random.randint(
                    0, len(no_acknowledgement) - 1)]
                append_front = quick_response
                quick_response_propose = True
            # elif re.search(self.NEG_PATTERN, i_text.lower()):
            #     quick_response = neg_acknowledgement[random.randint(
            #         0, len(neg_acknowledgement) - 1)]
            #     append_front = quick_response
            #     quick_response_propose = True
            elif re.search(self.FUNNY_PATTERN, i_text.lower()):
                quick_response = funny_acknowledgement[random.randint(
                    0, len(funny_acknowledgement) - 1)]
                append_front = quick_response
                # quick_response_propose = True
            elif i_text.lower() in okay_list:
                quick_response = okay_acknowledgement[random.randint(
                    0, len(okay_acknowledgement) - 1)]
                append_front = quick_response
                quick_response_propose = True
            # elif i_text.lower() in no_list:
            #     quick_response = no_acknowledgement[random.randint(
            #         0, len(no_acknowledgement) - 1)]
            #     append_front = quick_response
            #     quick_response_propose = True
            elif re.search(self.IDK_PATTERN, i_text.lower()):
                quick_response = idk_acknowledgement[random.randint(
                    0, len(idk_acknowledgement) - 1)]
                append_front = quick_response
                quick_response_propose = True
            elif re.search(self.IK_PATTERN, i_text.lower()):
                quick_response = ik_acknowledgement[random.randint(
                    0, len(ik_acknowledgement) - 1)]
                append_front = quick_response
                # quick_response_propose = True
            elif re.search(self.ASK_Q_PATTERN, i_text.lower()):
                quick_response = "Okay! "
                quick_response_propose = True
            elif re.search(self.STUPID_PATTERN, i_text.lower()):
                quick_response = stupid_response[random.randint(
                    0, len(stupid_response) - 1)]
                append_front = quick_response
            elif re.search(self.YOU_CREATOR_PATTERN, i_text.lower()):
                quick_response = you_creator_response[random.randint(
                    0, len(you_creator_response) - 1)]
            elif re.search(self.YOU_MOM_PATTERN, i_text.lower()):
                quick_response = your_mom_response[random.randint(
                    0, len(your_mom_response) - 1)]
            elif re.search(self.DONE_PATTERN, i_text.lower()):
                quick_response = done_response[random.randint(
                    0, len(done_response) - 1)]
            elif re.search(self.FRIEND_PATTERN, i_text.lower()):
                quick_response = friend_response[random.randint(
                    0, len(friend_response) - 1)]
            elif re.search(self.HOW_DID_YOU_PATTERN, i_text.lower()):
                quick_response = how_did_you_response[random.randint(
                    0, len(how_did_you_response) - 1)]
                append_front = quick_response
            elif re.search(self.KEEP_CHATTING_PATTERN, i_text.lower()):
                quick_response = keep_response[random.randint(
                    0, len(keep_response) - 1)]
            elif re.search(self.WAKEUP_PATTERN, i_text.lower()):
                quick_response = wakeup_response[random.randint(
                    0, len(wakeup_response) - 1)]
            elif re.search(self.NEW_NAME_PATTERN, i_text.lower()):
                quick_response = new_name_response[random.randint(
                    0, len(new_name_response) - 1)]
            elif re.search(self.CURSE_PATTERN, i_text.lower()):
                quick_response = curse_response[random.randint(
                    0, len(curse_response) - 1)]
            elif i_text.lower() in why_list:
                quick_response = why_response[random.randint(
                    0, len(why_response) - 1)]
                append_front = quick_response
                quick_response_propose = True
            elif re.search(self.WHO_SOMEONE, i_text.lower()):
                quick_response = "That's also a secret. Why don't we focus on ourselves?"
                append_front = quick_response
                quick_response_propose = True
            elif re.search(self.WRONG_PATTERN, i_text.lower()):
                quick_response = "I don't know. Just a random thought. "
                append_front = quick_response
                quick_response_propose = True
            elif re.search(self.MY_NAME_PATTERN, i_text.lower()):
                quick_response = "Hello! Hello! How are you doing today?"
                # quick_response_propose = True
            elif re.search(self.THANK_YOU_PATTERN, i_text.lower()):
                quick_response = "Of course! "
                append_front = quick_response
                quick_response_propose = True
            elif re.search(self.KNOW_MY_FRIEND_PATTERN, i_text.lower()):
                quick_response = "I wish I knew! But why don't we talk about ourselves instead? "
                quick_response_propose = True
            elif i_text.lower().strip() in self.GENERAL_COMMAND_PATTERN:
                quick_response = "Sure! What do you want to talk about?"
                # quick_response_propose = True
            elif i_text.lower().strip() in self.ALEXA_PATTERN:
                quick_response = "Me? Sure thing! What do you want to know about me?"
            elif ((len(i_noun_phrase) < 1 and len(i_topicKeywords) > 0 and len(i_topicKeywords[0]) > 0) or (len(i_text.split()) <= 2 and len(noun_phrase) > 0 and noun_phrase[0] in ignore_list) or "never mind" in i_text) and ((da_tag != "commands" and da_tag != "yes_no_question") and da_score > da_threshold):
                if "what are you talking about" in i_text or "what you are talking about" in i_text or "never mind" in i_text:
                    quick_response = what_clarification[random.randint(
                        0, len(what_clarification) - 1)]
                else:
                    # print("--------------")
                    # print(i_text)
                    # print(i_noun_phrase)
                    # print(len(i_text.split()))
                    quick_response = "<say-as interpret-as=\"interjection\">Uh huh</say-as>!, yeah, right?"
                    quick_response_propose = True

            elif da_tag == "back-channeling" or (da_tag == "acknowledgement" and da_score > da_threshold):
                quick_response = "Yes!"
                append_front = quick_response
                quick_response_propose = True
            elif da_tag == "comment" and da_score > da_threshold:
                quick_response = "I can't agree more! "
                append_front = quick_response
                quick_response_propose = True
            elif da_tag == "appreciation" and len(i_noun_phrase) < 1 and da_score > da_threshold:
                sentiment = returnnlp[0]["sentiment"]
                if float(sentiment["pos"]) > float(sentiment["neg"]):
                    quick_response = pos_acknowledgement[random.randint(
                        0, len(pos_acknowledgement) - 1)]
                    append_front = quick_response
                    quick_response_propose = True
                else:
                    quick_response_propose = neg_acknowledgement[random.randint(
                        0, len(neg_acknowledgement) - 1)]
                    append_front = quick_response
                    quick_response_propose = True
            # elif (da_tag == "complaint" or da_tag == "not_understanding") and (da_score > da_threshold and len(i_noun_phrase) < 1):
            elif (da_tag == "complaint" or da_tag == "not_understanding") and da_score > da_threshold:
                quick_response = what_clarification[random.randint(
                    0, len(what_clarification) - 1)]
                generation_quick_response = True
            elif da_tag == "other_answer" and da_score > da_threshold and len(i_noun_phrase) < 1:
                quick_response = "It's okay. "
                append_front = quick_response
                quick_response_propose = True
            elif da_tag == "hold" and da_score > da_threshold and len(i_noun_phrase) < 1:
                quick_response = "Take your time! So, what did you want to say?"
                generation_quick_response = True
            elif da_tag == "abandon" and da_score > da_threshold and len(i_noun_phrase) < 1:
                quick_response = "i'm sorry! what did you say?"
                generation_quick_response = True
                if "what did you say" in general_last_response:
                    quick_response = "Sorry, I'm still not sure about that. Another topic? "
                    quick_response_propose = True
                append_front = quick_response
            # elif da_tag == "opening" and da_score > da_threshold and len(i_noun_phrase) < 1:
            #     quick_response = "Hello! Hello! How are you doing today?"
            #     append_front = quick_response
            # elif da_tag == "closing" and da_score > da_threshold and len(i_noun_phrase) < 1:
            #     quick_response = done_response[random.randint(
            #         0, len(done_response) - 1)]
            #     append_front = quick_response
            elif da_tag == "thanking" and da_score > da_threshold and len(i_noun_phrase) < 1:
                quick_response = "Of course! "
                append_front = quick_response
                quick_response_propose = True
            elif da_tag == "apology" and da_score > da_threshold and len(i_noun_phrase) < 1:
                quick_response = "No worries! "
                append_front = quick_response
                quick_response_propose = True
            elif da_tag == "respond_to_apology" and da_score > da_threshold and len(i_noun_phrase) < 1:
                quick_response = "Okay then!"
                append_front = quick_response
                quick_response_propose = True

            # short sentence without noun phrase detected
            elif len(i_text.split()) < 3 and (len(i_noun_phrase) < 1 and len(i_topicKeywords) < 1):
                quick_response = general_clarification[random.randint(
                    0, len(general_clarification) - 1)]
                if "can you clarify that for me" in general_last_response or "could you rephrase that for me" in general_last_response or "Maybe you could rephrase that" in general_last_response or "Could you share more with me" in general_last_response:
                    quick_response = "Sorry, I'm still not sure about that. Another topic? "
                    quick_response_propose = True
                if i_text == "what":
                    quick_response = what_clarification[random.randint(
                        0, len(what_clarification) - 1)]

            elif da_tag == "nonsense" and da_score > da_threshold:
                quick_response = "i'm sorry! I didn't get that! Can you rephrase what you meant?"

            # set subreddit and extract key info
            if i_text == central_text:
                central_text_change = True
            else:
                central_text_change = False
            # if req_strong:
            if re.search(self.REQ_TOPIC_JUMP_PATTERN, i_text.lower()):
                regex_keyword = re.sub(
                    self.REQ_TOPIC_JUMP_PATTERN, "!@#", i_text.lower()).strip().split("!@#")[1]
                # if topic_keyword != "":
                #     sub = "todayilearned"
                #     i_text = topic_keyword
                #     t_template = til_title_template
                #     c_template = til_comment_template
                i_text = regex_keyword.strip()
                if len(regex_keyword.split()) <= 5:
                    sub = "todayilearned+showerthoughts"
            # except Exception:
            #     pass
            if re.search(self.OPINION_PATTERN, i_text.lower()):
                opinion_keyword = re.sub(
                    self.OPINION_PATTERN, "!@#", i_text.lower()).strip().split("!@#")[1]
                i_text = opinion_keyword.strip()
                sub = "showerthoughts"

            if central_text_change:
                if central_text != i_text:
                    central_text_truncated = True
                    central_text = i_text
                else:
                    central_text_truncated = False

            # error handling
            if i_text.strip() == "" or i_text.strip() in command_list:
                quick_response = "i'm sorry! what did you say?"
            elif re.search(self.RESTART_PATTERN, i_text.lower()):
                quick_response = "<say-as interpret-as=\"interjection\">howdy</say-as>! How are you doing today?"
            elif re.search(self.BROKEN_PATTERN, i_text.lower()):
                quick_response = you_are_broken[random.randint(
                    0, len(you_are_broken) - 1)]
                quick_response_propose = True
            elif i_text == "me" or (len(i_text.split()) <= 5 and i_text.startswith("my")):
                quick_response = me_talk[random.randint(0, len(me_talk) - 1)]
                quick_response_propose = False

            response_list.append({"text": i_text, "da_tag": da_tag, "da_score": da_score, "sub": sub,
                                  "quick_response": quick_response, "quick_response_propose": quick_response_propose,
                                  "lexical_intent": lexical_intent, "noun_phrase": i_noun_phrase})

        if len(response_list) == 1:
            response_info = response_list[0]
            if len(response_info["quick_response"]) > 0:
                response_dict = {
                    'response': response_info["quick_response"],
                    'user_attributes': {
                        'retrieval_propose': response_info["quick_response_propose"],
                        'suggest_keywords': '',
                        'retrieve_next_reply': "sorry i still didn't get that. maybe elaborate more?",
                        'generation_request': "not using blender: quick response",
                    }
                }
                if not current_use_blender or generation_quick_response:
                    return response_dict
                elif current_use_blender:
                    blender_retrieval_backup = response_dict
                    try:
                        blender_response = self.get_response_from_blender()
                        if len(blender_response) == 0:
                            assert False
                        blender_response_length = len(blender_response.split())
                        blender_propose = blender_response_length < blender_length_append_threshold
                        if len(blender_response) > 0:
                            # blender_response += " Anyways. " if blender_propose else ""
                            response_dict = {
                                'response': blender_response,
                                'user_attributes': {
                                    'retrieval_propose': blender_propose,
                                    'suggest_keywords': "",
                                    'retrieve_next_reply': "",
                                    'generation_request': "",
                                }
                            }
                    except Exception as e:
                        logging.error(
                            "[Retrieval_Blender Error]: {}".format(e),
                            exc_info=True)
                        response_dict = blender_retrieval_backup
                        if "generation_request" in response_dict:
                            response_dict['generation_request'] += " Blender error: %s" % e
                        else:
                            response_dict['generation_request'] = " Blender error: %s" % e
                    return response_dict
                else:
                    generation_retrieval_backup = response_dict
                    # logging.info("[Generation_Retrieval] len(response_list) = 1 "
                    #              "returnnlp: {}".format(returnnlp))
                    if has_command:
                        request_history = ""  # do not use request history if there is a command
                    generation_data = {"request_his": request_history, "request": context_utterance,
                                       "knowledge": ""}
                    logging.info("[Generation_Retrieval] len(response_list) = 1 "
                                 "request_data: {}".format(generation_data))
                    try:
                        generation_response = requests.post(url=generation_server_url, json=generation_data,
                                                            timeout=generation_timeout).json()

                        # clean generation_response: remove lol., reddit, ...
                        filtered_response_list = []
                        for generation_token in generation_response.split():
                            if generation_token.lower() not in self.GENERATION_FILTER_LIST:
                                filtered_response_list.append(generation_token)
                        generation_response = " ".join(w for w in filtered_response_list)

                        # remove incomplete sentences and sentences contain personal info
                        generation_response = self._process_generation(generation_response)

                        if len(generation_response) < 1 or len(generation_response.translate(self.generation_string).strip()) == 0:
                            logging.info(
                                "[Generation_Retrieval] len(response_list) = 1 generation empty after post_processing")
                            generation_retrieval_backup['user_attributes']['generation_request'] += " generation empty after post_processing"
                            return generation_retrieval_backup
                        if generation_response == self._rm_speechcon(general_last_response):
                            logging.info(
                                "[Generation_Retrieval] len(response_list) = 1 generation repeating")
                            generation_retrieval_backup['user_attributes'][
                                'generation_request'] += " generation repeating"
                            return generation_retrieval_backup

                        logging.info("[Generation_Retrieval] len(response_list) = 1 generation response: {}".format(generation_response))
                        response_dict = {
                            'response': generation_response,
                            'user_attributes': {
                                'retrieval_propose': False,
                                'suggest_keywords': "",
                                'retrieve_next_reply': "",
                                'generation_request': generation_data,
                            }
                        }
                    except Exception as e:
                        logging.error(
                            "[Generation_Retrieval] len(response_list) = 1 server request error: {}".format(e),
                            exc_info=True)
                        response_dict = generation_retrieval_backup
                    return response_dict
            else:
                utterance = response_info["text"]
                if len(response_info["noun_phrase"]) > 0:
                    noun_phrase_max = max(
                        response_info["noun_phrase"], key=len)
                else:
                    noun_phrase_max = ""
                da_tag = response_info["da_tag"]
                da_score = response_info["da_score"]
                sub = response_info["sub"]
                lexical_intent = response_info["lexical_intent"]

        elif len(response_list) > 0:
            # initialize in case central element not found
            # print("-----------")
            # print(response_list)
            response_info_c = response_list[0]
            for response_info_i in response_list:
                if response_info_i["text"] == central_text:
                    response_info_c = response_info_i
                    break
            utterance = response_info_c["text"]
            da_tag = response_info_c["da_tag"]
            da_score = response_info_c["da_score"]
            sub = response_info_c["sub"]
            lexical_intent = response_info_c["lexical_intent"]
            if len(response_info_c["noun_phrase"]) > 0:
                noun_phrase_max = max(response_info_c["noun_phrase"], key=len)
            else:
                noun_phrase_max = ""
            backstory = central_elem["backstory"]
            if float(backstory["confidence"]) > backstory_threshold:
                response_dict = {
                    'response': backstory["text"],
                    'user_attributes': {
                        'retrieval_propose': False,
                        'suggest_keywords': '',
                        'retrieve_next_reply': "sorry i still didn't get that. maybe elaborate more?",
                        'generation_request': "use backstory",
                    }
                }
                return response_dict
            if len(response_info_c["quick_response"]) > 0:
                response_dict = {
                    'response': response_info_c["quick_response"],
                    'user_attributes': {
                        'retrieval_propose': response_info_c["quick_response_propose"],
                        'suggest_keywords': '',
                        'retrieve_next_reply': "sorry i still didn't get that. maybe elaborate more?",
                        'generation_request': "not using blender: response_info_c quick response",
                    }
                }
                if not current_use_blender:
                    return response_dict
                elif current_use_blender:
                    blender_retrieval_backup = response_dict
                    try:
                        blender_response = self.get_response_from_blender()
                        if len(blender_response) == 0:
                            assert False
                        blender_response_length = len(blender_response.split())
                        blender_propose = blender_response_length < blender_length_append_threshold
                        if len(blender_response) > 0:
                            # blender_response += " Anyways. " if blender_propose else ""
                            response_dict = {
                                'response': blender_response,
                                'user_attributes': {
                                    'retrieval_propose': blender_propose,
                                    'suggest_keywords': "",
                                    'retrieve_next_reply': "",
                                    'generation_request': "",
                                }
                            }
                    except Exception as e:
                        logging.error(
                            "[Retrieval_Blender Error]: {}".format(e),
                            exc_info=True)
                        response_dict = blender_retrieval_backup
                        if "generation_request" in response_dict:
                            response_dict['generation_request'] += " Blender error: %s" % e
                        else:
                            response_dict['generation_request'] = " Blender error: %s" % e
                    return response_dict
                else:
                    generation_retrieval_backup = response_dict
                    if has_command:
                        request_history = ""  # do not use request history if there is a command
                    generation_data = {"request_his": request_history, "request": context_utterance,
                                       "knowledge": ""}
                    logging.info("[Generation_Retrieval] len(response_info_c['quick_response'] > 0 request_data: {}".format(generation_data))
                    try:
                        generation_response = requests.post(url=generation_server_url, json=generation_data,
                                                            timeout=generation_timeout).json()
                        # clean generation_response: remove lol., reddit, ...
                        filtered_response_list = []
                        for generation_token in generation_response.split():
                            if generation_token.lower() not in self.GENERATION_FILTER_LIST:
                                filtered_response_list.append(generation_token)
                        generation_response = " ".join(w for w in filtered_response_list)

                        # remove incomplete sentences and sentences contain personal info
                        generation_response = self._process_generation(generation_response)

                        if len(generation_response) < 1 or len(
                                generation_response.translate(self.generation_string).strip()) == 0:
                            logging.info(
                                "[Generation_Retrieval] len(response_info_c['quick_response'] > 0 generation empty after post_processing")
                            generation_retrieval_backup['user_attributes'][
                                'generation_request'] += " generation empty after post_processing"
                            return generation_retrieval_backup
                        if generation_response == self._rm_speechcon(general_last_response):
                            logging.info(
                                "[Generation_Retrieval] len(response_info_c['quick_response'] > 0 generation repeating")
                            generation_retrieval_backup['user_attributes'][
                                'generation_request'] += " generation repeating"
                            return generation_retrieval_backup

                        logging.info("[Generation_Retrieval] len(response_info_c['quick_response'] > 0 generation response: {}".format(generation_response))
                        response_dict = {
                            'response': generation_response,
                            'user_attributes': {
                                'retrieval_propose': False,
                                'suggest_keywords': "",
                                'retrieve_next_reply': "",
                                'generation_request': generation_data,
                            }
                        }
                    except Exception as e:
                        logging.error(
                            "[Generation_Retrieval] len(response_info_c['quick_response'] > 0 server request error: {}".format(e), exc_info=True)
                        response_dict = generation_retrieval_backup
                    return response_dict

        else:
            response_dict = {
                'response': self.template_manager.speak("error_handling", {}),
                'user_attributes': {
                    'suggest_keywords': '',
                    'retrieve_next_reply': "sorry i still didn't get that. maybe elaborate more?",
                    'generation_request': "not using blender: error handling",
                }
            }
            if not current_use_blender:
                logging.error(
                    "[RETRIEVAL] response_list is empty error with utterance: {}".format(utterance), exc_info=True)
                return response_dict
            elif current_use_blender:
                blender_retrieval_backup = response_dict
                try:
                    blender_response = self.get_response_from_blender()
                    if len(blender_response) == 0:
                        assert False
                    blender_response_length = len(blender_response.split())
                    blender_propose = blender_response_length < blender_length_append_threshold
                    if len(blender_response) > 0:
                        # blender_response += " Anyways. " if blender_propose else ""
                        response_dict = {
                            'response': blender_response,
                            'user_attributes': {
                                'retrieval_propose': blender_propose,
                                'suggest_keywords': "",
                                'retrieve_next_reply': "",
                                'generation_request': "",
                            }
                        }
                except Exception as e:
                    logging.error(
                        "[Retrieval_Blender Error]: {}".format(e),
                        exc_info=True)
                    response_dict = blender_retrieval_backup
                    if "generation_request" in response_dict:
                        response_dict['generation_request'] += " Blender error: %s" % e
                    else:
                        response_dict['generation_request'] = " Blender error: %s" % e
                return response_dict
            else:
                logging.error("[Generation_Retrieval] response_list is empty error with utterance: {}".format(utterance), exc_info=True)
                generation_retrieval_backup = response_dict
                if has_command:
                    request_history = ""  # do not use request history if there is a command
                generation_data = {"request_his": request_history, "request": context_utterance,
                                   "knowledge": ""}
                logging.info("[Generation_Retrieval] empty response_list request_data: {}".format(generation_data))
                try:
                    generation_response = requests.post(url=generation_server_url, json=generation_data,
                                                        timeout=generation_timeout).json()
                    
                    # clean generation_response: remove lol., reddit, ...
                    filtered_response_list = []
                    for generation_token in generation_response.split():
                        if generation_token.lower() not in self.GENERATION_FILTER_LIST:
                            filtered_response_list.append(generation_token)
                    generation_response = " ".join(w for w in filtered_response_list)

                    # remove incomplete sentences and sentences contain personal info
                    generation_response = self._process_generation(generation_response)

                    if len(generation_response) < 1 or len(
                            generation_response.translate(self.generation_string).strip()) == 0:
                        logging.info(
                            "[Generation_Retrieval] empty response_list generation empty after post_processing")
                        generation_retrieval_backup['user_attributes'][
                            'generation_request'] += " generation empty after post_processing"
                        return generation_retrieval_backup
                    if generation_response == self._rm_speechcon(general_last_response):
                        logging.info(
                            "[Generation_Retrieval] empty response_list generation repeating")
                        generation_retrieval_backup['user_attributes'][
                            'generation_request'] += " generation repeating"
                        return generation_retrieval_backup

                    logging.info("[Generation_Retrieval] empty response_list generation response: {}".format(generation_response))
                    response_dict = {
                        'response': generation_response,
                        'user_attributes': {
                            'retrieval_propose': False,
                            'suggest_keywords': "",
                            'retrieve_next_reply': "",
                            'generation_request': generation_data,
                        }
                    }
                except Exception as e:
                    logging.error(
                        "[Generation_Retrieval] empty response_list server request error: {}".format(e), exc_info=True)
                    response_dict = generation_retrieval_backup
                return response_dict

        # prepare utterance by dialog act
        da_template = ""
        if da_tag == "opinion" or da_tag == "open_question_opinion" or da_tag == "open_question" or da_tag == "open_question_factual" or lexical_intent == "open_question_opinion" or da_tag == "other":
            da_template = "Right! "
            if not central_text_truncated:
                utterance = noun_phrase_max if len(
                    noun_phrase_max) > 0 else utterance
        elif da_tag == "yes_no_question" or da_tag == "commands" or lexical_intent == "yes_no_question":
            da_template = "Sure! "
            if not central_text_truncated:
                utterance = noun_phrase_max if len(
                    noun_phrase_max) > 0 else utterance
        elif da_tag == "statement":
            da_template = "Yeah! "
            if not central_text_truncated:
                utterance = noun_phrase_max if len(
                    noun_phrase_max) > 0 else utterance

        # get rid of "about"
        if utterance.strip().startswith("about") and not utterance.strip().startswith("about time"):
            utterance = utterance[6:]
        # call reddit api
        if len(utterance.split()) <= 2:
            sub = "todayilearned+showerthoughts"

        if sub == "todayilearned+showerthoughts" or sub == "showerthoughts":
            t_template = self.TIL_TITLE_TEMPLATE
            c_template = self.TIL_COMMENT_TEMPLATE
            n_titles = 2
        # disable other subreddit
        else:
            n_titles = 2

            # if self.state_manager.user_attributes.response is not None:
            #     last_response = self.state_manager.user_attributes.response
            # use if state because we may add ssml to the reponse
            if "i guess i never thought about this before" in general_last_response or "I’m not familiar with that" in general_last_response or "Seems like you’re really interested in" in general_last_response:
                response_empty = self.template_manager.speak("general_subreddit_second", {})
                response_empty_propose = True
            else:
                response_empty = self.template_manager.speak("general_subreddit_first", {})
                response_empty_propose = False


            if da_tag == "yes_no_question":
                response_empty = self.template_manager.speak("empty_response_yes_no", {})
                response_empty_propose = True
            if (da_tag == "opinion" or da_tag == "comment") and da_score > da_threshold:
                response_empty = self.template_manager.speak("empty_response_opinion", {})
                response_empty_propose = True

            response_dict = {
                'response': response_empty,
                'user_attributes': {
                    'retrieval_propose': response_empty_propose,
                    'suggest_keywords': "",
                    'retrieve_next_reply': "",
                    'generation_request': "not using blender: disable other subreddit",
                }
            }
            if not current_use_blender:
                return response_dict
            elif current_use_blender:
                blender_retrieval_backup = response_dict
                try:
                    blender_response = self.get_response_from_blender()
                    if len(blender_response) == 0:
                        assert False
                    blender_response_length = len(blender_response.split())
                    blender_propose = blender_response_length < blender_length_append_threshold
                    if len(blender_response) > 0:
                        # blender_response += " Anyways. " if blender_propose else ""
                        response_dict = {
                            'response': blender_response,
                            'user_attributes': {
                                'retrieval_propose': blender_propose,
                                'suggest_keywords': "",
                                'retrieve_next_reply': "",
                                'generation_request': "",
                            }
                        }
                except Exception as e:
                    logging.error(
                        "[Retrieval_Blender Error]: {}".format(e),
                        exc_info=True)
                    response_dict = blender_retrieval_backup
                    if "generation_request" in response_dict:
                        response_dict['generation_request'] += " Blender error: %s" % e
                    else:
                        response_dict['generation_request'] = " Blender error: %s" % e
                return response_dict
            else:
                generation_retrieval_backup = response_dict
                if has_command:
                    request_history = ""  # do not use request history if there is a command
                generation_data = {"request_his": request_history, "request": context_utterance,
                                   "knowledge": ""}
                logging.info("[Generation_Retrieval] disable other subreddit request_data: {}".format(generation_data))
                try:
                    generation_response = requests.post(url=generation_server_url, json=generation_data,
                                                        timeout=generation_timeout).json()
                    # clean generation_response: remove lol., reddit, ...
                    filtered_response_list = []
                    for generation_token in generation_response.split():
                        if generation_token.lower() not in self.GENERATION_FILTER_LIST:
                            filtered_response_list.append(generation_token)
                    generation_response = " ".join(w for w in filtered_response_list)

                    # remove incomplete sentences and sentences contain personal info
                    generation_response = self._process_generation(generation_response)

                    if len(generation_response) < 1 or len(
                            generation_response.translate(self.generation_string).strip()) == 0:
                        logging.info(
                            "[Generation_Retrieval] disable other subreddit generation empty after post_processing")
                        generation_retrieval_backup['user_attributes'][
                            'generation_request'] += " generation empty after post_processing"
                        return generation_retrieval_backup
                    if generation_response == self._rm_speechcon(general_last_response):
                        logging.info(
                            "[Generation_Retrieval] disable other subreddit generation repeating")
                        generation_retrieval_backup['user_attributes'][
                            'generation_request'] += " generation repeating"
                        return generation_retrieval_backup

                    logging.info("[Generation_Retrieval] disable other subreddit generation response: {}".format(generation_response))
                    response_dict = {
                        'response': generation_response,
                        'user_attributes': {
                            'retrieval_propose': False,
                            'suggest_keywords': "",
                            'retrieve_next_reply': "",
                            'generation_request': generation_data,
                        }
                    }
                except Exception as e:
                    logging.error(
                        "[Generation_Retrieval] disable other subreddit server request error: {}".format(e), exc_info=True)
                    response_dict = generation_retrieval_backup
                return response_dict

        comments = RedisHelper().get_retrieval(utterance.lower())
        # print("***********")
        # print(utterance)
        # print(sub)
        # print(comments)
        # for testing without redis chache
        # comments = None

        # for generation model
        generation_knowledge = ""
        # end for generation model

        try:
            if comments is None or len(comments) == 0:
                keys = self.KEYS
                c_id, c_secret, u_agent = keys.split("::")
                r = praw.Reddit(
                    client_id=c_id, client_secret=c_secret, user_agent=u_agent, timeout=2)

                if sub is None:
                    submission = list(r.subreddit('all').search(
                        utterance, limit=n_titles))
                else:
                    submission = list(r.subreddit(sub).search(
                        utterance, limit=n_titles))

                if len(submission) == 0:
                    raise ValueError("0 return for submission from reddit")

                t = 0
                weight = 1.5
                weight_decay = 0.6
                comments = []

                for s in submission:
                    if s.subreddit_name_prefixed == 'r/AskOuija':
                        continue
                    s.comment_limit = self.N_COMMENTS
                    # s.comment_sort = "top"
                    t += 1
                    if t > n_titles:
                        break
                    ups = int(s.ups)
                    up_ratio = float(s.upvote_ratio)
                    real_ups = ups

                    if real_ups == 0:
                        real_ups = 1000  # for scaling purpose

                    if ups > 100:
                        ups = 1.21
                    elif ups > 30:
                        ups = 1.1
                    elif ups > 10:
                        ups = 1
                    else:
                        ups = 0.9
                    title = s.title
                    # print("------------")
                    # print(title)
                    # make sure when query the whole reddit there is an exact
                    # match
                    if sub is None and re.search(utterance, re.sub('[-!@#$,.]', ' ', title.lower())) is None:
                        continue
                    # print("++++++++++++")
                    # title = title + " " + s.selftext
                    # TODO: add s.selftext, which is the detailed content of
                    # the post(title)
                    if self._profanity_check(title):
                        continue
                    # print("_____________")
                    # get rid of reddit/subreddit in title
                    title = self._filter_profanity(title)
                    # print("pppppppppppp")

                    c = 0

                    # not matching: noun phrase not found in retrieved title
                    if not do_coref:
                        for np in noun_phrase:
                            if np not in re.sub('[%s]' % re.escape(self.SPEC_CHAR), '', title.lower().strip()):
                                skip_comment = True

                    if len(title.split()) < 11:
                        skip_comment = False

                    # if sub != "todayilearned" and not skip_comment:
                    if not skip_comment and len(s.comments) > 0:
                        match_bonus = 1
                        if utterance in title:
                            match_bonus = 1.2
                        for top_level_comment in s.comments:
                            if hasattr(top_level_comment, "stickied"):
                                if top_level_comment.stickied:
                                    break

                            if c >= self.N_COMMENTS:
                                break
                            if hasattr(top_level_comment, "body"):
                                top1_comment = top_level_comment.body
                            else:
                                top1_comment = self.NO_BODY_IN_COMMENT_TEMPLATE[random.randint(
                                    0, len(self.NO_BODY_IN_COMMENT_TEMPLATE) - 1)]
                            if hasattr(top_level_comment, "author"):
                                if top_level_comment.author is not None:
                                    if top_level_comment.author.name == "AutoModerator":
                                        break

                            if hasattr(top_level_comment, "score"):
                                score = int(top_level_comment.score)
                            else:
                                score = 0

                            # print(top1_comment)
                            # if profanity_check(top1_comment):
                            #     continue
                            top1_comment = self._filter_profanity(top1_comment)

                            # sentiment analysis, base = 2, give higher score to positive title+comment
                            # title_sentiment_score = sid.polarity_scores(title)
                            # comment_sentiment_score = sid.polarity_scores(top1_comment)
                            # # more weights on the comments
                            # totle_sentiment_score = title_sentiment_score["pos"] - title_sentiment_score["neg"] + 2 * (comment_sentiment_score["pos"] - comment_sentiment_score["neg"])
                            # sentiment_weight = float(2 + totle_sentiment_score)
                            sentiment_weight = 1
                            comments.append((sentiment_weight * weight * match_bonus * (
                                ups * up_ratio * score / real_ups), (title, top1_comment)))
                            c += 1
                            weight *= weight_decay  # give more weights to the most relevant post
                    else:
                        score = 1.5 if (re.search(utterance, title.lower()) or re.search(
                            utterance[:-1], title.lower())) else 1
                        score *= ups
                        comments.append((score, (title, "")))

            RedisHelper().set_with_expire(RedisHelper.PREFIX_RETRIEVAL, utterance.lower(), comments, expire=self.EXPIRE_TIME)

            comments = sorted(comments, key=lambda x: x[0], reverse=True)
            # print(comments)

            # get/set index in user_attributes to avoid repetition of the
            # content
            if self.state_manager.user_attributes.retrieval_idx is None:
                self.state_manager.user_attributes.retrieval_idx = {}
            if utterance in self.state_manager.user_attributes.retrieval_idx:
                current_idx = self.state_manager.user_attributes.retrieval_idx[
                    utterance]
            else:
                current_idx = 0
            self.state_manager.user_attributes.retrieval_idx[
                utterance] = current_idx + 1

            # random select for TIL
            title_len_limit = 30
            if sub == "todayilearned+showerthoughts" or sub == "showerthoughts":
                title_len_limit = 45
                # random.shuffle(comments, random.random)

            # Testing
            # comments = [(0,("Who is that Pike Place Market artist that sketches very realistic face portraits of people for like $15: /u/Mosquitohana", "original submissionr/Seattlecomments4t2r4ewhoisthatpikeplacemarketartistthat"))]
            if len(comments) > 0:
                if current_idx < len(comments):
                    title1 = self._process_retrieve(
                        comments[current_idx][1][0], True, title_len_limit, not_use_template=current_use_generation)
                    generation_knowledge = title1

                    if not {"retrieval::too_much_to_say", "retrieval::too_little_to_say"}.intersection(
                            set(utils.get_template_keys(self.state_manager))):

                        # comment1 = self._process_retrieve(
                        #     comments[current_idx][1][1])

                        template_idx = random.randint(0, len(t_template) - 1)
                        c_template_idx = random.randint(0, len(c_template) - 1)
                        t_template_in_use = t_template[template_idx]

                        # add acknowledgement
                        if t_template == self.TIL_TITLE_TEMPLATE:

                            # keyword not found
                            # check if utterance itself or after removing plural ("s"/"es")
                            # > 1 in case utterance = "s" then check_utt = ""
                            if len(utterance) > 2 and utterance[-2:] == "es":
                                check_utt = utterance[:-2].lower()
                            elif len(utterance) > 1 and utterance[-1] == "s":
                                check_utt = utterance[:-1].lower()
                            else:
                                check_utt = utterance.lower()
                            if re.search(check_utt, re.sub('[-!@#$,.]', ' ', title1.lower())):
                                # if t_template_in_use.startswith("that"):
                                #     t_template_in_use = ["<say-as interpret-as=\"interjection\">Aha</say-as>! ", "<say-as interpret-as=\"interjection\">ta da</say-as>! ", ""][random.randint(0, 2)] +\
                                #         utterance + t_template_in_use[4:]
                                # else:
                                #     t_template_in_use = "yes, " + utterance + ", " + t_template_in_use

                                # ack_utterance = re.sub(r"\byour\b", "my", utterance)
                                # ack_utterance = re.sub(r"\bmy\b", "your", ack_utterance)
                                # replace "your" and "my" in one pass so won't interefere with each other
                                substrings = {"your": "my", "my": "your"}
                                regex = re.compile('|'.join(map(re.escape, substrings)))
                                ack_utterance = regex.sub(lambda match: substrings[match.group(0)], utterance)

                                ack_utterance = re.sub(r"^\babout \b", "", ack_utterance)

                                # use a different template if continuing talking about one topic
                                try:
                                    if self.state_manager.user_attributes.retrieval_idx is not None:
                                        if utterance in self.state_manager.user_attributes.retrieval_idx and self.state_manager.user_attributes.retrieval_idx[utterance] > 1:
                                            t_template = self.CONTINUE_TOPIC_TEMPLATE
                                            template_idx = random.randint(0, len(t_template) - 1)
                                            t_template_in_use = t_template[template_idx]
                                except Exception as e:
                                    logging.error(
                                        "[RETRIEVAL] second turn for a topic error: {}".format(e), exc_info=True)

                                t_template_in_use = re.sub("<TOPIC>", ack_utterance, t_template_in_use)

                                # profanity check
                                for w in utterance.split():
                                    if w.lower().strip() in self.BLACKLIST:
                                        t_template_in_use = ""
                            else:
                                # if utterance.lower().strip() in self.BLACKLIST:
                                    # utterance = "it"
                                for w in utterance.lower().strip().split():
                                    if w.lower().strip() in self.BLACKLIST:
                                        utterance = "it"
                                        break

                                if (spacynp is not None and len(spacynp) == 0) or re.search(r"\bi\b", utterance) or re.search(r"\byou\b", utterance):
                                    utterance = "it"

                                t_template = self.RELATED_TITLE_TEMPLATE
                                template_idx = random.randint(0, len(t_template) - 1)
                                t_template_in_use = t_template[template_idx]
                                t_template_in_use = re.sub("<TOPIC>", utterance, t_template_in_use)

                                # t_template_in_use = "<say-as interpret-as=\"interjection\">Hmm...</say-as>. I don't know much about " + \
                                # utterance + " specifically. However, I do know that "

                        # append short response + da_template
                        if len(da_template) > 0:
                            if t_template != self.TIL_TITLE_TEMPLATE:
                                t_template_in_use = da_template + t_template_in_use
                            else:
                                # t_template_in_use = da_template + " speaking of which, " + t_template_in_use
                                t_template_in_use = da_template + " " + t_template_in_use
                        if len(append_front) > 0:
                            t_template_in_use = append_front + t_template_in_use

                        # disable comment
                        comment1 = None
                        if len(comments[current_idx][1][1]) == 0 or comment1 is None or len(comment1) < 2:
                            response = t_template_in_use + title1
                        else:
                            response = t_template_in_use + title1 + \
                                c_template[c_template_idx] + comment1
                    else:
                        response = title1

                else:
                    response = "That's all I know. What else do you want to talk about?"
            else:
                # response = "i don't know much about that. Maybe we should talk about something else. What do you want to talk about?"
                # comment2 = "maybe we can talk about movies?"

                # if self.state_manager.user_attributes.response is not None:
                #     last_response = self.state_manager.user_attributes.response
                # use if state because we may add ssml to the reponse
                if "i guess i never thought about this before" in general_last_response or "I’m not familiar with that" in general_last_response or "Seems like you’re really interested in" in general_last_response:
                    response_empty_comment = self.template_manager.speak("general_subreddit_second", {})
                    response_empty_comment_propose = True
                else:
                    response_empty_comment = self.template_manager.speak("general_subreddit_first", {})
                    response_empty_comment_propose = False

                if da_tag == "yes_no_question":
                    response_empty_comment = self.template_manager.speak("empty_response_yes_no", {})
                    response_empty_comment_propose = True
                if (da_tag == "opinion" or da_tag == "comment") and da_score > da_threshold:
                    response_empty_comment = self.template_manager.speak("empty_response_opinion", {})
                    response_empty_comment_propose = True

                response_dict = {
                    'response': response_empty_comment,
                    'user_attributes': {
                        'retrieval_propose': response_empty_comment_propose,
                        'suggest_keywords': "",
                        'retrieve_next_reply': "",
                        'generation_request': "not using blender: empty retrieval",
                    }
                }
                if not current_use_blender:
                    return response_dict
                elif current_use_blender:
                    blender_retrieval_backup = response_dict
                    try:
                        blender_response = self.get_response_from_blender()
                        if len(blender_response) == 0:
                            assert False
                        blender_response_length = len(blender_response.split())
                        blender_propose = blender_response_length < blender_length_append_threshold
                        if len(blender_response) > 0:
                            # blender_response += " Anyways. " if blender_propose else ""
                            response_dict = {
                                'response': blender_response,
                                'user_attributes': {
                                    'retrieval_propose': blender_propose,
                                    'suggest_keywords': "",
                                    'retrieve_next_reply': "",
                                    'generation_request': "",
                                }
                            }
                    except Exception as e:
                        logging.error(
                            "[Retrieval_Blender Error]: {}".format(e),
                            exc_info=True)
                        response_dict = blender_retrieval_backup
                        if "generation_request" in response_dict:
                            response_dict['generation_request'] += " Blender error: %s" % e
                        else:
                            response_dict['generation_request'] = " Blender error: %s" % e
                    return response_dict
                else:
                    generation_retrieval_backup = response_dict
                    if has_command:
                        request_history = ""  # do not use request history if there is a command
                    generation_data = {"request_his": request_history, "request": context_utterance,
                                       "knowledge": ""}
                    logging.info("[Generation_Retrieval] empty retrieval request_data: {}".format(generation_data))
                    try:
                        generation_response = requests.post(url=generation_server_url, json=generation_data,
                                                            timeout=generation_timeout).json()
                        
                        # clean generation_response: remove lol., reddit, ...
                        filtered_response_list = []
                        for generation_token in generation_response.split():
                            if generation_token.lower() not in self.GENERATION_FILTER_LIST:
                                filtered_response_list.append(generation_token)
                        generation_response = " ".join(w for w in filtered_response_list)

                        # remove incomplete sentences and sentences contain personal info
                        generation_response = self._process_generation(generation_response)

                        if len(generation_response) < 1 or len(
                                generation_response.translate(self.generation_string).strip()) == 0:
                            logging.info(
                                "[Generation_Retrieval] empty retrieval generation empty after post_processing")
                            generation_retrieval_backup['user_attributes'][
                                'generation_request'] += " generation empty after post_processing"
                            return generation_retrieval_backup
                        if generation_response == self._rm_speechcon(general_last_response):
                            logging.info(
                                "[Generation_Retrieval] empty retrieval generation repeating")
                            generation_retrieval_backup['user_attributes'][
                                'generation_request'] += " generation repeating"
                            return generation_retrieval_backup

                        logging.info("[Generation_Retrieval] empty retrieval generation response: {}".format(generation_response))
                        response_dict = {
                            'response': generation_response,
                            'user_attributes': {
                                'retrieval_propose': False,
                                'suggest_keywords': "",
                                'retrieve_next_reply': "",
                                'generation_request': generation_data,
                            }
                        }
                    except Exception as e:
                        logging.error(
                            "[Generation_Retrieval] empty retrieval server request error: {}".format(e), exc_info=True)
                        response_dict = generation_retrieval_backup
                    return response_dict

        except Exception as e:
            logging.error(
                "[RETRIEVAL] query error: {}".format(e), exc_info=True)

            if "i guess i never thought about this before" in general_last_response or "I’m not familiar with " in general_last_response or "Seems like you’re really interested in" in general_last_response or "I’m not familiar with" in general_last_response:
                response_error_comment = self.template_manager.speak("general_subreddit_second", {})
                response_error_propose = True
            else:
                response_error_comment = self.template_manager.speak("general_subreddit_first", {})
                response_error_propose = False

            if not response_error_propose and (sub == "todayilearned+showerthoughts" or sub == "showerthoughts"):
                response_error_comment = self.template_manager.speak("subreddit_first", {'topic': utterance})

            for i in utterance.lower().strip().split():
                if i in self.BLACKLIST:
                    response_error_comment = re.sub("<TOPIC>", "that", response_error_comment)
                    break
            response_error_comment = re.sub("<TOPIC>", utterance, response_error_comment)

            if da_tag == "yes_no_question":
                response_error_comment = self.template_manager.speak("empty_response_yes_no", {})
                response_error_propose = True
            if (da_tag == "opinion" or da_tag == "comment") and da_score > da_threshold:
                response_error_comment = self.template_manager.speak("empty_response_opinion", {})
                response_error_propose = True

            response_error_dict = {
                'response': response_error_comment,
                'user_attributes': {
                    'retrieval_propose': response_error_propose,
                    'suggest_keywords': "",
                    'retrieve_next_reply': "",
                    'generation_request': "not using blender: retrieval query error",
                }
            }

            if not current_use_blender:
                return response_error_dict
            elif current_use_blender:
                blender_retrieval_backup = response_error_dict
                response_dict = response_error_dict
                try:
                    blender_response = self.get_response_from_blender()
                    if len(blender_response) == 0:
                        assert False
                    blender_response_length = len(blender_response.split())
                    blender_propose = blender_response_length < blender_length_append_threshold
                    if len(blender_response) > 0:
                        # blender_response += " Anyways. " if blender_propose else ""
                        response_dict = {
                            'response': blender_response,
                            'user_attributes': {
                                'retrieval_propose': blender_propose,
                                'suggest_keywords': "",
                                'retrieve_next_reply': "",
                                'generation_request': "",
                            }
                        }
                except Exception as e:
                    logging.error(
                        "[Retrieval_Blender Error]: {}".format(e),
                        exc_info=True)
                    response_dict = blender_retrieval_backup
                    if "generation_request" in response_dict:
                        response_dict['generation_request'] += " Blender error: %s" % e
                    else:
                        response_dict['generation_request'] = " Blender error: %s" % e
                return response_dict
            else:
                generation_retrieval_backup = response_error_dict
                if has_command:
                    request_history = ""  # do not use request history if there is a command
                generation_data = {"request_his": request_history, "request": context_utterance,
                                   "knowledge": ""}
                logging.info("[Generation_Retrieval] retrieval query error request_data: {}".format(generation_data))
                try:
                    generation_response = requests.post(url=generation_server_url, json=generation_data,
                                                        timeout=generation_timeout).json()

                    # clean generation_response: remove lol., reddit, ...
                    filtered_response_list = []
                    for generation_token in generation_response.split():
                        if generation_token.lower() not in self.GENERATION_FILTER_LIST:
                            filtered_response_list.append(generation_token)
                    generation_response = " ".join(w for w in filtered_response_list)

                    # remove incomplete sentences and sentences contain personal info
                    generation_response = self._process_generation(generation_response)

                    if len(generation_response) < 1 or len(
                            generation_response.translate(self.generation_string).strip()) == 0:
                        logging.info(
                            "[Generation_Retrieval] retrieval query error generation empty after post_processing")
                        generation_retrieval_backup['user_attributes'][
                            'generation_request'] += " generation empty after post_processing"
                        return generation_retrieval_backup
                    if generation_response == self._rm_speechcon(general_last_response):
                        logging.info(
                            "[Generation_Retrieval] retrieval query error generation repeating")
                        generation_retrieval_backup['user_attributes'][
                            'generation_request'] += " generation repeating"
                        return generation_retrieval_backup

                    logging.info(
                        "[Generation_Retrieval] retrieval query error generation response: {}".format(generation_response))
                    response_dict = {
                        'response': generation_response,
                        'user_attributes': {
                            'retrieval_propose': False,
                            'suggest_keywords': "",
                            'retrieve_next_reply': "",
                            'generation_request': generation_data,
                        }
                    }
                except Exception as e:
                    logging.error(
                        "[Generation_Retrieval] retrieval query error server request error: {}".format(e), exc_info=True)
                    response_dict = generation_retrieval_backup
                return response_dict

        try:
            keywords = utterance['suggest_keywords'].lower().strip()
            # logging.info("[Retrieval] Get suggest_keywords: {}".format(last_ner))
        except Exception:
            keywords = ''

        # for generation model
        if current_use_generation:
            if has_command:
                request_history = ""  # do not use request history if there is a command
            generation_data = {"request_his": request_history, "request": context_utterance,
                               "knowledge": generation_knowledge}
            logging.info("[Generation_Retrieval] request_data: {}".format(generation_data))
            try:
                generation_response = requests.post(url=generation_server_url, json=generation_data,
                                                    timeout=generation_timeout).json()

                # clean generation_response: remove lol., reddit, ...
                filtered_response_list = []
                for generation_token in generation_response.split():
                    if generation_token.lower() not in self.GENERATION_FILTER_LIST:
                        filtered_response_list.append(generation_token)
                generation_response = " ".join(w for w in filtered_response_list)

                # remove incomplete sentences and sentences contain personal info
                generation_response = self._process_generation(generation_response)

                if len(generation_response) < 1 or len(
                        generation_response.translate(self.generation_string).strip()) == 0:
                    logging.info(
                        "[Generation_Retrieval] generation empty after post_processing")
                    generation_retrieval_backup['user_attributes'][
                        'generation_request'] += " generation empty after post_processing"
                    return generation_retrieval_backup
                if generation_response == self._rm_speechcon(general_last_response):
                    logging.info(
                        "[Generation_Retrieval] generation repeating")
                    generation_retrieval_backup['user_attributes'][
                        'generation_request'] += " generation repeating"
                    return generation_retrieval_backup

                logging.info("[Generation_Retrieval] generation response: {}".format(generation_response))
                response_dict = {
                    'response': generation_response,
                    'user_attributes': {
                        'retrieval_propose': False,
                        'suggest_keywords': keywords,
                        'retrieve_next_reply': "",
                        'generation_request': generation_data,
                    }
                }
            except Exception as e:
                logging.error(
                    "[Generation_Retrieval] server request error: {}".format(e), exc_info=True)
                response_dict = generation_retrieval_backup
            return response_dict
        elif current_use_blender:
            response_dict = {
                'response': response,
                'user_attributes': {
                    'retrieval_propose': False,
                    'suggest_keywords': "",
                    'retrieve_next_reply': "",
                    'generation_request': "not using blender",
                }
            }
            try:
                blender_response = self.get_response_from_blender()
                if len(blender_response) == 0:
                    assert False
                blender_response_length = len(blender_response.split())
                blender_propose = blender_response_length < blender_length_append_threshold
                if len(blender_response) > 0:
                    # blender_response += " Anyways. " if blender_propose else ""
                    response_dict = {
                        'response': blender_response,
                        'user_attributes': {
                            'retrieval_propose': blender_propose,
                            'suggest_keywords': "",
                            'retrieve_next_reply': "",
                            'generation_request': "",
                        }
                    }
            except Exception as e:
                logging.error(
                    "[Retrieval_Blender Error]: {}".format(e),
                    exc_info=True)
                if "generation_request" in response_dict:
                    response_dict['generation_request'] += " Blender error: %s" % e
                else:
                    response_dict['generation_request'] = " Blender error: %s" % e
            return response_dict
        else:
            response_dict = {
                'response': response,
                'user_attributes': {
                    'retrieval_propose': False,
                    'suggest_keywords': "",
                    'retrieve_next_reply': "",
                    'generation_request': "not using blender",
                }
            }
            return response_dict

    def _filter_profanity(self, text):
        sentences = text.split(".")
        filtered_text = ""
        for sent in sentences:
            good = True

            # for token in sent.lower().split():
            #     token = token.strip()
            #     if not token[-1].isalnum():
            #         token = token[:-1] #could be "reddit," or "reddit:"
            #     if token.strip() in self.BLACKLIST:
            #         good = False
            #         break
            
            # # loop blacklist instead so can filter out phrases
            # for w in self.BLACKLIST:
            #     if re.search(r"\b%s\b" % w, sent.lower()):
            #         good = False
            #         break
            if BotResponseProfanityClassifier.is_profane(sent.lower(), TopicModule.RETRIEVAL.value):
                good = False
            
            if good:
                filtered_text = filtered_text + sent + "."
        return filtered_text

    def _post_process_blender(self, text):
        punctuations = [",", ".", "?", "!"]
        if text[-1] not in punctuations or text[-1] == "?":
            c_i = len(text) - 2
            truncated_text = ""
            while c_i > -1:
                if text[c_i] in punctuations:
                    truncated_text = text[:c_i + 1]
                    break
                c_i -= 1
        else:
            truncated_text = text
        return truncated_text

    @trace_latency
    def get_response_from_blender(self):
        response = ""

        if not self.session_id:
            return ""

        if self.blender_start_time:
            timeout = 1200  # 1200 msec
            while self._get_duration_in_msec() < timeout:
                response = self.redis_client.get(RedisHelper.PREFIX_BLENDER_RETRIEVAL, self._get_blender_key())
                if response:
                    logging.info(
                        f"[Retrieval Blender] fetch: get result with global duration (msec): {self._get_duration_in_msec()}")
                    return response

                time.sleep(0.05)
            logging.info(
                f"[Retrieval Blender] fetch: get no result with global duration (msec): {self._get_duration_in_msec()}")

        else:
            for i in range(6):
                response = self.redis_client.get(RedisHelper.PREFIX_BLENDER_RETRIEVAL, self._get_blender_key())
                if response:
                    logging.info(f"[Retrieval Blender] fetch: get result with local duration (msec): {0.05 * i * 1000}")
                    return response

                time.sleep(0.05)
            logging.info(f"[Retrieval Blender] fetch: get no result with local duration (msec): {300}")

        return response

    def _get_blender_key(self):
        key = "{}_{}".format(self.session_id, self.user_profile.total_turn_count)
        return key

    def _get_duration_in_msec(self):
        return (time.time() - self.blender_start_time) * 1000

    def _profanity_check(self, text):
        # profanity check built by the client
        # r = client.batch_detect_profanity(utterances=[text])
        # the "1" here means the source is the statistical_model where if it is 0, it would be blacklist
        # return r["offensivenessClasses"][0]["values"][1]["offensivenessClass"]
        # return 1 means it is offensive; 0 means not offensive

        # for token in text.lower().strip().split():
        #     if token in self.BLACKLIST and (token != "reddit" and token != "subreddit"):
        #         return 1
        # print(">>>>>>>>>>>>")
        
        # for w in self.BLACKLIST:
        #     if re.search(r"\b%s\b" % w, text.lower()) and "reddit" not in w:
        #         return 1
        if BotResponseProfanityClassifier.is_profane(text.lower(), TopicModule.RETRIEVAL.value):
            return 1
        
        return 0

    def _process_generation(self, text):  # remove incomplete sentences
        text = text.encode('ascii', 'ignore').decode('ascii')  # rm emoji

        punctuation_list = [".", "?", "!", ","]
        personal_pattern = r"my (son|daughter|sons|daughters|child|children|wife|husband|girlfriend|boyfriend)"
        chunks = re.split('\n', text)
        text = ""
        text_list = []
        c_s = 0
        for c in chunks:
            for i in range(len(c)):
                if c[i] in punctuation_list:
                    if i > 0 and i + 1 < len(c):
                        # deal with numbers with a "."
                        if c[i - 1].isdigit() and c[i + 1].isdigit():
                            continue
                    # deal with single char like "washington d.c."
                    if i + 2 < len(c):
                        if c[i + 1].isalpha() and c[i + 2] == ".":
                            continue
                    if len(c[c_s:i].strip()) > 0:
                        text = text + c[c_s:i + 1].strip() + " "
                        text_list.append(c[c_s:i + 1].strip())
                        c_s = i + 1
                elif i == len(c) - 1:
                    text = text + c[c_s:i + 1].strip() + " "
                    text_list.append(c[c_s:i + 1].strip())
            if len(text_list) == 0 and len(c.strip()) > 0:
                text_list.append(c.strip())
                text = c.strip()
            else:
                continue  # only executed if the inner loop did Not break
            break

        # remove sentence(s) with personal info
        new_text_list = []
        for sent in text_list:
            if not re.search(personal_pattern, sent.lower()):
                new_text_list.append(sent)

        if len(new_text_list) == 0:
            return ""
        elif len(new_text_list) == 1:
            return new_text_list[0]
        else:
            if new_text_list[-1][-1] not in punctuation_list:
                return " ".join(sent for sent in new_text_list[:-1])
            else:
                return " ".join(sent for sent in new_text_list)

    def _process_retrieve(self, text, title=False, title_len_limit=20, not_use_template=False):
        # remove_list = [r"https\S+", r"http\S+", r"til", r"tldr", r"tl:dr", r"tl dr", r"tl;dr", r"nbsp"]
        # spec_char = '#&\()*+/:;<=>@[\\]^_`{|}~'
        for pattern in self.REMOVE_LIST:
            text = re.sub(pattern, "", text)
        text = text.replace("\n", ". ").strip()
        if text.lower().startswith("that"):
            text = text[5:].strip()

        limit = title_len_limit if title else self.SENT_LEN_LIMIT

        num_words = len(text.split())
        
        if not not_use_template:
            if num_words < 1 or (num_words == 1 and len(text) == 1) or (num_words == 1 and ("deleted" in text.lower() or "removed" in text.lower())) or (text.lower().strip().startswith("delete")):
                return self.template_manager.speak("too_little_to_say", {})

        text = re.sub('[%s]' % re.escape(self.SPEC_CHAR), '', text)

        # replace me/I to someone
        text = re.sub(r"\bme\b|\bI\b|\bi\b|\bMe\b|\bME\b", "someone", text)
        text = re.sub(r"\bmy\b|\bMy\b|\bMY\b|\bmine\b|\bMine\b",
                      "someone's", text)

        # recover punctuation
        punctuation_list = [".", "?", "!", ","]
        chunks = re.split('\n', text)
        text = ""
        text_list = []
        c_s = 0
        for c in chunks:
            for i in range(len(c)):
                # clean the comments by removing "edit"
                if len(c) > i + 3:
                    if c[i:i + 3] == "edit":
                        break
                if c[i] in punctuation_list:
                    if i > 0 and i + 1 < len(c):
                        # deal with numbers with a "."
                        if c[i - 1].isdigit() and c[i + 1].isdigit():
                            continue
                    # deal with single char like "washington d.c."
                    if i + 2 < len(c):
                        if c[i + 1].isalpha() and c[i + 2] == ".":
                            continue
                    if len(c[c_s:i].strip()) > 0:
                        text = text + c[c_s:i + 1].strip() + " "
                        text_list.append(c[c_s:i + 1].strip())
                        c_s = i + 1
                elif i == len(c) - 1:
                    text = text + c[c_s:i + 1].strip() + " "
                    text_list.append(c[c_s:i + 1].strip())
            if len(text_list) == 0 and len(c.strip()) > 0:
                text_list.append(c.strip())
                text = c.strip()
            else:
                continue  # only executed if the inner loop did Not break
            break
        
        if not not_use_template:
            if len(text_list) == 1 and text_list[0].strip()[-1] == "?":
                return self.template_manager.speak("too_little_to_say", {})

        num_words = sum([len(i.split()) for i in text_list])

        if num_words <= limit:
            return text
        else:
            word_in_sent = 0
            for i in range(len(text_list)):
                word_in_sent += len(text_list[i].split())
                if word_in_sent > limit:
                    if i == 0 and not not_use_template:
                        return self.template_manager.speak("too_much_to_say", {})
                    else:
                        return " ".join(sent for sent in text_list[:i])
