.. _nlp_pipeline:

NLP Pipeline
================

Cobot contains a built-in NLP pipeline that extracts key NLP attributes before running the Dialog Manager.
The default modules in the pipeline are intent, NER, sentiment, and topic.

* Intent: placeholder class, default does not override ASK intent model
* NER: default uses SpaCy 2.0
* Sentiment: default uses Vader sentiment. See the SENTIMENT definitions in ``service_module_manager.py`` to see variations of sentiment responses
* Topic: default calls Alexa Prize science team's topic model, hosted by AlexaPrizeToolkitService

This page walks through how to add a new module to the NLP Pipeline in detail.

--------------
Create a Class
--------------

To add a new local module to the NLP Pipeline, start by creating a child class of LocalServiceModule.
A remote NLP module can simply use the default RemoteServiceModule class, but you can optionally
create a child class of RemoteServiceModule if you wish to override RemoteServiceModule's logic.
You can also create your child class from ToolkitServiceModule if your module will call the AlexaPrizeToolkitService.
All three classes are defined in `cobot_core.service_module <cobot_core.html#module-cobot_core.service_module>`_.
It is also possible to define a completely new class or override another Cobot class, such as IntentClassifier or TopicAnalyzer.
The base classes for the default NLP Modules are defined in cobot_core.nlp.nlp_modules.

---------------------------
Override the Execute Method
---------------------------

The execute method is called when the NLP Pipeline runs and should return a string, list, or dict.
The method can contain custom logic or simply make a call to a remote service.
A local NLP module must define a new execute method.
A remote NLP module can use the default RemoteServiceModule execute method, or optionally override the execute method.

----------------------
Add Remote Module Code
----------------------

If your new module will be a remote service module, there are a few extra steps to follow.

1. Add a new module to the docker directory in your own Cobot directory. Follow the greeter and retrieval modules in sample_cobot/docker for examples. We recommend creating a template using bin/add-response-generator and modifying the get_required_context and handle_message methods in response_generator.py.
2. Add the new module name to the cobot_modules section of modules.yaml in your Cobot directory. This name should match the name of your module docker directory.
3. Run git push to add all new module code to your repository. You can continue to Step 4 while this push is deploying.
4. Run cobot update to add the new remote module to your Cobot stack.

Limitation Note: CodePipeline currently only supports `10 parallel actions at a time <https://docs.aws.amazon.com/codepipeline/latest/userguide/limits.html>`_.
In the Beta stage, the Build-LambdaFunction action is in parallel with the remote modules, so you can only add up to 9 additional modules.
If you need to add more than 9 remote modules, we recommend that you try to combine smaller modules to run in the same docker container.
For larger projects, we will need to explore other ways around this limitation.

---------------------------
Define the Module in Lambda
---------------------------

Each custom NLP module needs to be defined in your lambda_handler function.
The config definition is a dictionary with the keys corresponding to the parameters of `cobot_core.service_module_config <cobot_core.html#module-cobot_core.service_module_config>`_.
For a local NLP module the required keys are name and class.
For a remote NLP module the required keys are name, class, and url.

-----------------
Upsert the Module
-----------------

Inside your lambda_handler, run cobot.upsert_module with the new module definition to register the module with the NLP pipeline.
This method will update an existing module with the same name or insert a new module to the pipeline.

-----------------------
Run create_nlp_pipeline
-----------------------

The last step is to run cobot.create_nlp_pipeline inside your lambda handler.
The input parameter is a List of List of module names, where each module name matches the name key in the corresponding module definition.
The default pipeline contains [["intent", "ner", "sentiment", "topic"]].
Modules in the same list run in parallel and independently, while modules in a previous list run before modules in a later list,
e.g. [['ner', 'topic'],['coreference']] => ner and topic modules run in parallel before coreference module, and coreference module can have access to ner and topic results from previous modules.

----------------------
Example Implementation
----------------------

.. code-block:: python

   # Create a Class
   class Coreference(LocalServiceModule):
       # Override the Execute Method
       def execute(self):
           # add your own coreference logic here, using input_data['text'][0..2]
           # return a string, list, or dict
           return {}

   def lambda_handler(event, context):
       cobot = Cobot.handler(event,
                             context,
                             app_id=<APP_ID>,
                             user_table_name='UserTable',
                             save_before_response=True,
                             state_table_name='StateTable',
                             overrides=overrides,
                             api_key=<API_KEY>)

       # Define the Module in Lambda
       COREFERENCE = {
           'name': "coreference",
           'class': Coreference,
           'url': 'local',
           'context_manager_keys': ['text'],
           'history_turns': 2
       }

       # Upsert the Module
       cobot.upsert_module(COREFERENCE)

       # Run create_nlp_pipeline
       cobot.create_nlp_pipeline([["intent", "ner", "sentiment", "topic"], ["coreference"]])

