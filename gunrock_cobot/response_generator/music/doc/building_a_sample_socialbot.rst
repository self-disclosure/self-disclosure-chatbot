.. _building_a_sample_socialbot:

Building a Sample Socialbot
===========================

-----------
Entry Point
-----------

The entry point for a Cobot calls a single method, the handler, in the main Cobot class.

.. code-block:: python

   import cobot_core as Cobot

   ...

   def lambda_handler(event, context):
       cobot = Cobot.handler(event,
                             context,
                             app_id=None,
                             user_table_name='UserTable',
                             save_before_response=True,
                             state_table_name='StateTable',
                             overrides=overrides)
       ...
       return cobot.execute()

The handler is invoked every time a LaunchRequest or IntentRequest is received from your ASK skill. It will pass an incoming LaunchRequest or IntentRequest through the Cobot nlp pipeline, manage any state information needed (Lambda is a stateless framework), and then follow the dialog management flow defined for your bot. By default, this dialog manager runs the configured SelectingStrategy to choose a set of response generator candidates, then a RankingStrategy to select the best response candidate from those candidate responses.

For more details on the handler method, see `the cobot_core API documentation`__.

.. __: cobot_core.html#module-cobot_core.cobot_handler

---------------------------
Overriding Default Behavior
---------------------------

The sample_cobot configures a simple selector to route all traffic to the Greeter module.

.. code-block:: python

   class CustomSelectingStrategy(Cobot.SelectingStrategy):
       def __init__(self):
           pass

       def select_response_mode(self, input):
           return ["GREETER"]


   def overrides(binder):
       binder.bind(Cobot.SelectingStrategy, to=CustomSelectingStrategy)

The *overrides(binder)* method you implement in your Cobot instance defines the dialog management strategy used by your Cobot, and allows you to customize any aspect of your bot's behavior by overriding one of the Core library interfaces.



-------------------
Response Generators
-------------------

Next, you'll want to configure a *response generator*. A response generator can be thought of as a mini-skill, bot or model that makes up part or all of your bot's conversational experience. A response generator takes the **state**, **intent**, **ner**, **text** and other pre-processed inputs and maps them to a response. A response generator can represent a scripted or modeled dialog about a certain topic or domain, a generative model or retrieval based system to generate responses.

A Cobot instance can run one or more response generators - most open domain bots will depend on many different strategies (an *ensemble approach*) for generating effective responses and holding engaging conversations and will thus likely use multiple response generator modules. However, simpler or bots with a narrower domain may only use a single response generator.

A simple response generators can be a class that runs locally within your Cobot's Lambda, but more sophisticated models will generally need to be hosted remotely (loading complicated libraries and large models is not possible in the constrained Lambda environment).

Defining a response generator that runs remotely within its own Docker container is easy:

.. code-block:: python

        RemoteGreeterBot = {
                'name': "GREETER",
                'class': RemoteServiceModule,
                'url': ServiceURLLoader.get_url_for_module("GREETER"),
                'context_manager_keys': ['intent','slots']
        }

        cobot.add_response_generators([RemoteGreeterBot])

In the above example, the response generator is given a name, a class to instantiate, a list of information from the state manager that the response generator depends on, and a url that is the endpoint for the response generator (note that the mechanics of this are all handled internally by the Cobot Toolkit - the ServiceURLLoader class will have access to the load balancer endpoint that allows it to reach the response generator).

See `cobot_core.service_module_config <cobot_core.html#module-cobot_core.service_module_config>`_ for more details on the arguments expected.

---------------
Greeter Example
---------------

The Greeter response generator is a semi-scripted greeting sequence. It runs a dialog model built using the *rasa_core* library, a third party open source dialog modelling toolkit. Rasa_core is not part of the Cobot Toolkit, but can be used with it.

The Greeter model is defined by the domain.yml and data/stories.md files (all located in the COBOT_HOME/sample_cobot/docker/greeter/app directory).

The domain.yml file defines a series of intents, slots and entities that this response generator understands, and a series of possible actions (responses) it can take. The stories.md file defines a series of dialog interactions using these intents and actions:

.. code-block:: bash

   ## happy_path
   * _greet
     - utter_greet
   * _mood_great
     - utter_happy

   ## sad_path_1
   * _greet
     - utter_greet
   * _mood_unhappy
     - utter_cheer_up
     - utter_did_that_help
   * _yes_intent
     - utter_happy

   ...

After defining or changing the intents and actions in the domain.yml file or the dialog sequences in the story file, the dialog model needs to be re-trained. Running "make core-train" in the greeter/app directory will re-build the appropriate models, which can then be tested locally, and added and pushed to CodeCommit for deployment.
