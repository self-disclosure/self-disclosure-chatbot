��m9      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �target���)��}�(h�.. _testing_cobot_components:�h]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��refid��testing-cobot-components�u�tagname�h	�line�K�parent�hhh�source��:/Users/raeferg/cobot_base/doc/testing_cobot_components.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Testing Cobot Components�h]�h �Text����Testing Cobot Components�����}�(hh+hh)hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh$hhh h!hKubh#)��}�(hhh]�(h()��}�(h�
Unit Tests�h]�h.�
Unit Tests�����}�(hh>hh<hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh9hhh h!hK	ubh �	paragraph���)��}�(h��The test directory of your Cobot Toolkit installation contains unit tests for many of the library classes provided by Cobot. But you can, and should, add tests to a directory that matches the name of your Cobot instance (i.e. test/sample_cobot).�h]�h.��The test directory of your Cobot Toolkit installation contains unit tests for many of the library classes provided by Cobot. But you can, and should, add tests to a directory that matches the name of your Cobot instance (i.e. test/sample_cobot).�����}�(hhNhhLhhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKhh9hhubhK)��}�(hX�  While you can write unit tests for any class you implement, the most critical tests you will write are module tests for your remote modules. Because Docker deployments are relatively time consuming, you will want to ensure that your remote modules are functionally correct before pushing an update to CodeCommit, which will result in rebuilding your skill, Lambda, and Cobot Docker modules.�h]�h.X�  While you can write unit tests for any class you implement, the most critical tests you will write are module tests for your remote modules. Because Docker deployments are relatively time consuming, you will want to ensure that your remote modules are functionally correct before pushing an update to CodeCommit, which will result in rebuilding your skill, Lambda, and Cobot Docker modules.�����}�(hh\hhZhhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKhh9hhubhK)��}�(h��A unit test of your remote modules validates that the Flask API implemented correctly handles the serialized state inputs passed to it:�h]�h.��A unit test of your remote modules validates that the Flask API implemented correctly handles the serialized state inputs passed to it:�����}�(hhjhhhhhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKhh9hhubh �literal_block���)��}�(hX  import unittest
from sample_cobot.docker.greeter.app import app
import json

class TestFlaskApi(unittest.TestCase):
    """
    Tests can be run by python3 -m unittest test_greeter.py after you are at test/sample_cobot/, or by running run-test sample_cobot greeter
    from the bin directory
    Precondition: dialog models must exist in test/sample_cobot/models/dialogue directory
    """

    def setUp(self):
        app.app.testing = True
        self.app = app.app.test_client()

    def test_hello_world(self):
        response = self.app.get('/')
        self.assertEqual(response.get_data().decode(sys.getdefaultencoding()), 'Welcome')

        response = self.app.post('/greeter', data=json.dumps({'text': 'good', 'intent': 'greet'}))
        print(json.loads(response.get_data().decode(sys.getdefaultencoding())))
        self.assertEqual(json.loads(response.get_data().decode(sys.getdefaultencoding())).get('response')[0],
                         'Hey! How are you?')

if __name__ == "__main__":
    unittest.main()�h]�h.X  import unittest
from sample_cobot.docker.greeter.app import app
import json

class TestFlaskApi(unittest.TestCase):
    """
    Tests can be run by python3 -m unittest test_greeter.py after you are at test/sample_cobot/, or by running run-test sample_cobot greeter
    from the bin directory
    Precondition: dialog models must exist in test/sample_cobot/models/dialogue directory
    """

    def setUp(self):
        app.app.testing = True
        self.app = app.app.test_client()

    def test_hello_world(self):
        response = self.app.get('/')
        self.assertEqual(response.get_data().decode(sys.getdefaultencoding()), 'Welcome')

        response = self.app.post('/greeter', data=json.dumps({'text': 'good', 'intent': 'greet'}))
        print(json.loads(response.get_data().decode(sys.getdefaultencoding())))
        self.assertEqual(json.loads(response.get_data().decode(sys.getdefaultencoding())).get('response')[0],
                         'Hey! How are you?')

if __name__ == "__main__":
    unittest.main()�����}�(hhhhxubah}�(h]�h]�h]�h]�h]��	xml:space��preserve��language��python��linenos���highlight_args�}�uhhvh h!hKhh9hhubhK)��}�(h�kFor more information on writing unit tests see `the unittest documentation`__ and the Flask_ documentation.�h]�(h.�/For more information on writing unit tests see �����}�(h�/For more information on writing unit tests see �hh�hhh NhNubh �	reference���)��}�(h�`the unittest documentation`__�h]�h.�the unittest documentation�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]��name��the unittest documentation��	anonymous�K�refuri��/https://docs.python.org/3/library/unittest.html�uhh�hh��resolved�Kubh.�	 and the �����}�(h�	 and the �hh�hhh NhNubh�)��}�(h�Flask_�h]�h.�Flask�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]��name��Flask�h��)http://flask.pocoo.org/docs/0.12/testing/�uhh�hh�h�Kubh.� documentation.�����}�(h� documentation.�hh�hhh NhNubeh}�(h]�h]�h]�h]�h]�uhhJh h!hK.hh9hhubh
)��}�(h�7.. __:  https://docs.python.org/3/library/unittest.html�h]�h}�(h]��id2�ah]�h]�h]�h]�h�h�h�Kuhh	hK0hh9hhh h!�
referenced�Kubh
)��}�(h�4.. _Flask: http://flask.pocoo.org/docs/0.12/testing/�h]�h}�(h]��flask�ah]�h]��flask�ah]�h]�h�h�uhh	hK1hh9hhh h!h�Kubeh}�(h]��
unit-tests�ah]�h]��
unit tests�ah]�h]�uhh"hh$hhh h!hK	ubh#)��}�(hhh]�(h()��}�(h�Quick Module Tests�h]�h.�Quick Module Tests�����}�(hh�hh�hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh�hhh h!hK5ubhK)��}�(hX�  While complete unit tests are a very good idea, a simple mechanism for testing during development is to create a file called test_script in your module/app directory and use the quick-test script included in the template app. This doesn't do the detailed output validation that unit tests do, but is a simple mechanism to check for breakages during development. From within the app directory you can run:�h]�h.X�  While complete unit tests are a very good idea, a simple mechanism for testing during development is to create a file called test_script in your module/app directory and use the quick-test script included in the template app. This doesn’t do the detailed output validation that unit tests do, but is a simple mechanism to check for breakages during development. From within the app directory you can run:�����}�(hj   hh�hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hK7hh�hhubhw)��}�(h�./quick-test�h]�h.�./quick-test�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�h�h�h��bash�h��h�}�uhhvh h!hK9hh�hhubhK)��}�(h�,or use the cobot CLI to invoke a quick-test:�h]�h.�,or use the cobot CLI to invoke a quick-test:�����}�(hj  hj  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hK=hh�hhubhw)��}�(h�'cobot quick-test <botname> <modulename>�h]�h.�'cobot quick-test <botname> <modulename>�����}�(hhhj*  ubah}�(h]�h]�h]�h]�h]�h�h�h��bash�h��h�}�uhhvh h!hK?hh�hhubeh}�(h]��quick-module-tests�ah]�h]��quick module tests�ah]�h]�uhh"hh$hhh h!hK5ubh#)��}�(hhh]�(h()��}�(h�Development Workflow�h]�h.�Development Workflow�����}�(hjG  hjE  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hjB  hhh h!hKFubhK)��}�(hX  When creating a new response generator module, or developing heavily on an existing one, you will want to follow a develop-test-deploy workflow to minimize time wasted on integration testing bad deployments. We recommend running unit tests for your modules as follows:�h]�h.X  When creating a new response generator module, or developing heavily on an existing one, you will want to follow a develop-test-deploy workflow to minimize time wasted on integration testing bad deployments. We recommend running unit tests for your modules as follows:�����}�(hjU  hjS  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKHhjB  hhubhw)��}�(h�&cd bin
./run-test sample_cobot greeter�h]�h.�&cd bin
./run-test sample_cobot greeter�����}�(hhhja  ubah}�(h]�h]�h]�h]�h]�h�h�h��bash�h��h�}�uhhvh h!hKJhjB  hhubhK)��}�(h��This will instantiate your Flask API and pass it a mocked input, with intent, slot values and other state information to be processed by our module. We recommend passing a variety of different intents to ensure coverage of major behaviors.�h]�h.��This will instantiate your Flask API and pass it a mocked input, with intent, slot values and other state information to be processed by our module. We recommend passing a variety of different intents to ensure coverage of major behaviors.�����}�(hjs  hjq  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKOhjB  hhubhK)��}�(hX�  The use of assertEqual() is recommended to verify that the responses received back from your response generator are working as expected. However, some modules may not be amenable to an exact matching assertion due to randomness of your model. If you can, fixing a random seed value may be helpful - if not some output validation, even if just checking for explicit error results, is helpful.�h]�h.X�  The use of assertEqual() is recommended to verify that the responses received back from your response generator are working as expected. However, some modules may not be amenable to an exact matching assertion due to randomness of your model. If you can, fixing a random seed value may be helpful - if not some output validation, even if just checking for explicit error results, is helpful.�����}�(hj�  hj  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKQhjB  hhubeh}�(h]��development-workflow�ah]�h]��development workflow�ah]�h]�uhh"hh$hhh h!hKFubeh}�(h]�(h�id1�eh]�h]�(�testing cobot components��testing_cobot_components�eh]�h]�uhh"hhhhh h!hK�expect_referenced_by_name�}�j�  hs�expect_referenced_by_id�}�hhsubeh}�(h]�h]�h]�h]�h]��source�h!uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h'N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h!�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�N�character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��flask�]�h�as�refids�}�h]�has�nameids�}�(j�  hj�  j�  h�h�h�h�j?  j<  j�  j�  u�	nametypes�}�(j�  �j�  Nh�Nh�j?  Nj�  Nuh}�(hh$j�  h$h�h9h�h�h�h�j<  h�j�  jB  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�(h �system_message���)��}�(hhh]�(hK)��}�(h�Title underline too short.�h]�h.�Title underline too short.�����}�(hhhj$  ubah}�(h]�h]�h]�h]�h]�uhhJhj!  ubhw)��}�(h�&Testing Cobot Components
=============�h]�h.�&Testing Cobot Components
=============�����}�(hhhj2  ubah}�(h]�h]�h]�h]�h]�h�h�uhhvhj!  ubeh}�(h]�h]�h]�h]�h]��level�K�type��WARNING��line�K�source�h!uhj  hh$hhh h!hKubj   )��}�(hhh]�(hK)��}�(hhh]�h.�Title overline too short.�����}�(hhhjN  ubah}�(h]�h]�h]�h]�h]�uhhJhjK  ubhw)��}�(h�6-----------------
Quick Module Tests
-----------------�h]�h.�6-----------------
Quick Module Tests
-----------------�����}�(hhhj[  ubah}�(h]�h]�h]�h]�h]�h�h�uhhvhjK  ubeh}�(h]�h]�h]�h]�h]��level�K�type�jH  �line�K3�source�h!uhj  ubj   )��}�(hhh]�(hK)��}�(h�Title overline too short.�h]�h.�Title overline too short.�����}�(hhhjv  ubah}�(h]�h]�h]�h]�h]�uhhJhjs  ubhw)��}�(h�6-----------------
Quick Module Tests
-----------------�h]�h.�6-----------------
Quick Module Tests
-----------------�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�h�h�uhhvhjs  ubeh}�(h]�h]�h]�h]�h]��level�K�type�jH  �line�K3�source�h!uhj  hh�hhh h!hK5ubj   )��}�(hhh]�(hK)��}�(hhh]�h.�Title overline too short.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhJhj�  ubhw)��}�(h�8-----------------
Development Workflow
-----------------�h]�h.�8-----------------
Development Workflow
-----------------�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�h�h�uhhvhj�  ubeh}�(h]�h]�h]�h]�h]��level�K�type�jH  �line�KD�source�h!uhj  ubj   )��}�(hhh]�(hK)��}�(h�Title overline too short.�h]�h.�Title overline too short.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhJhj�  ubhw)��}�(h�8-----------------
Development Workflow
-----------------�h]�h.�8-----------------
Development Workflow
-----------------�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�h�h�uhhvhj�  ubeh}�(h]�h]�h]�h]�h]��level�K�type�jH  �line�KD�source�h!uhj  hjB  hhh h!hKFube�transform_messages�]�j   )��}�(hhh]�hK)��}�(hhh]�h.�>Hyperlink target "testing-cobot-components" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhJhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h!�line�Kuhj  uba�transformer�N�
decoration�Nhhub.