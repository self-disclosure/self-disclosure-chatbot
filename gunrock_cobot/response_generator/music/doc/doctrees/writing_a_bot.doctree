���P      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �target���)��}�(h�.. _writing_a_bot:�h]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��refid��writing-a-bot�u�tagname�h	�line�K�parent�hhh�source��//Users/raeferg/cobot_base/doc/writing_a_bot.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Writing A Bot�h]�h �Text����Writing A Bot�����}�(hh+hh)hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh$hhh h!hKubh#)��}�(hhh]�(h()��}�(h�Entry Point�h]�h.�Entry Point�����}�(hh>hh<hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh9hhh h!hK	ubh �	paragraph���)��}�(h�XThe entry point for a Cobot calls a single method, the handler, in the main Cobot class.�h]�h.�XThe entry point for a Cobot calls a single method, the handler, in the main Cobot class.�����}�(hhNhhLhhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKhh9hhubh �literal_block���)��}�(hX�  import cobot_core as Cobot

...

def lambda_handler(event, context):
    cobot = Cobot.handler(event,
                          context,
                          app_id=None,
                          user_table_name='UserTable',
                          save_before_response=True,
                          state_table_name='StateTable',
                          overrides=overrides)
    ...
    return cobot.execute()�h]�h.X�  import cobot_core as Cobot

...

def lambda_handler(event, context):
    cobot = Cobot.handler(event,
                          context,
                          app_id=None,
                          user_table_name='UserTable',
                          save_before_response=True,
                          state_table_name='StateTable',
                          overrides=overrides)
    ...
    return cobot.execute()�����}�(hhhh\ubah}�(h]�h]�h]�h]�h]��	xml:space��preserve��language��python��linenos���highlight_args�}�uhhZh h!hKhh9hhubhK)��}�(hX  The handler is invoked every time a LaunchRequest or IntentRequest is received from your ASK skill. It will pass an incoming IntentRequest through the Cobot's analysis pipeline, manage any state information needed (Lambda is a stateless framework), and then follow the dialog management flow defined for your bot. By default, this dialog manager runs the configured SelectingStrategy to choose a set of response generator candidates, then a RankingStrategy to select the best response candidate from those candidate responses.�h]�h.X  The handler is invoked every time a LaunchRequest or IntentRequest is received from your ASK skill. It will pass an incoming IntentRequest through the Cobot’s analysis pipeline, manage any state information needed (Lambda is a stateless framework), and then follow the dialog management flow defined for your bot. By default, this dialog manager runs the configured SelectingStrategy to choose a set of response generator candidates, then a RankingStrategy to select the best response candidate from those candidate responses.�����}�(hhshhqhhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKhh9hhubhK)��}�(h�QFor more details on the handler method, see `the cobot_core API documentation`__.�h]�(h.�,For more details on the handler method, see �����}�(h�,For more details on the handler method, see �hhhhh NhNubh �	reference���)��}�(h�$`the cobot_core API documentation`__�h]�h.� the cobot_core API documentation�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]��name�� the cobot_core API documentation��	anonymous�K�refuri��/cobot_core.html#module-cobot_core.cobot_handler�uhh�hh�resolved�Kubh.�.�����}�(h�.�hhhhh NhNubeh}�(h]�h]�h]�h]�h]�uhhJh h!hK hh9hhubh
)��}�(h�6.. __: cobot_core.html#module-cobot_core.cobot_handler�h]�h}�(h]��id2�ah]�h]�h]�h]�h�h�h�Kuhh	hK"hh9hhh h!�
referenced�Kubeh}�(h]��entry-point�ah]�h]��entry point�ah]�h]�uhh"hh$hhh h!hK	ubh#)��}�(hhh]�(h()��}�(h�Overriding Default Behavior�h]�h.�Overriding Default Behavior�����}�(hh�hh�hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh�hhh h!hK&ubhK)��}�(h�YThe sample_cobot configures a simple selector to route all traffic to the Greeter module.�h]�h.�YThe sample_cobot configures a simple selector to route all traffic to the Greeter module.�����}�(hh�hh�hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hK(hh�hhubh[)��}�(hX  class CustomSelectingStrategy(Cobot.SelectingStrategy):
    def __init__(self):
        pass

    def select_response_mode(self, input):
        return ["GREETER"]


def overrides(binder):
    binder.bind(Cobot.SelectingStrategy, to=CustomSelectingStrategy)�h]�h.X  class CustomSelectingStrategy(Cobot.SelectingStrategy):
    def __init__(self):
        pass

    def select_response_mode(self, input):
        return ["GREETER"]


def overrides(binder):
    binder.bind(Cobot.SelectingStrategy, to=CustomSelectingStrategy)�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�hjhkhl�python�hn�ho}�uhhZh h!hK*hh�hhubhK)��}�(h��The *overrides(binder)* method you implement in your Cobot instance defines the dialog management strategy used by your Cobot, and allows you to customize any aspect of your bot's behavior by overriding one of the Core library interfaces.�h]�(h.�The �����}�(h�The �hh�hhh NhNubh �emphasis���)��}�(h�*overrides(binder)*�h]�h.�overrides(binder)�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh.�� method you implement in your Cobot instance defines the dialog management strategy used by your Cobot, and allows you to customize any aspect of your bot’s behavior by overriding one of the Core library interfaces.�����}�(h�� method you implement in your Cobot instance defines the dialog management strategy used by your Cobot, and allows you to customize any aspect of your bot's behavior by overriding one of the Core library interfaces.�hh�hhh NhNubeh}�(h]�h]�h]�h]�h]�uhhJh h!hK7hh�hhubeh}�(h]��overriding-default-behavior�ah]�h]��overriding default behavior�ah]�h]�uhh"hh$hhh h!hK&ubh#)��}�(hhh]�(h()��}�(h�Response Generators�h]�h.�Response Generators�����}�(hj  hj  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hj  hhh h!hK=ubhK)��}�(hX�  Next, you'll want to configure a *response generator*. A response generator can be thought of as a mini-skill, bot or model that makes up part or all of your bot's conversational experience. A response generator takes the **state**, **intent**, **entities**, **text** and other pre-processed inputs and maps them to a response. A response generator can represent a scripted or modeled dialog about a certain topic or domain, a generative model or retrieval based system to generate responses.�h]�(h.�#Next, you’ll want to configure a �����}�(h�!Next, you'll want to configure a �hj)  hhh NhNubh�)��}�(h�*response generator*�h]�h.�response generator�����}�(hhhj2  ubah}�(h]�h]�h]�h]�h]�uhh�hj)  ubh.��. A response generator can be thought of as a mini-skill, bot or model that makes up part or all of your bot’s conversational experience. A response generator takes the �����}�(h��. A response generator can be thought of as a mini-skill, bot or model that makes up part or all of your bot's conversational experience. A response generator takes the �hj)  hhh NhNubh �strong���)��}�(h�	**state**�h]�h.�state�����}�(hhhjG  ubah}�(h]�h]�h]�h]�h]�uhjE  hj)  ubh.�, �����}�(h�, �hj)  hhh NhNubjF  )��}�(h�
**intent**�h]�h.�intent�����}�(hhhjZ  ubah}�(h]�h]�h]�h]�h]�uhjE  hj)  ubh.�, �����}�(h�, �hj)  ubjF  )��}�(h�**entities**�h]�h.�entities�����}�(hhhjm  ubah}�(h]�h]�h]�h]�h]�uhjE  hj)  ubh.�, �����}�(hjY  hj)  ubjF  )��}�(h�**text**�h]�h.�text�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhjE  hj)  ubh.�� and other pre-processed inputs and maps them to a response. A response generator can represent a scripted or modeled dialog about a certain topic or domain, a generative model or retrieval based system to generate responses.�����}�(h�� and other pre-processed inputs and maps them to a response. A response generator can represent a scripted or modeled dialog about a certain topic or domain, a generative model or retrieval based system to generate responses.�hj)  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhhJh h!hK?hj  hhubhK)��}�(hXp  A Cobot instance can run one or more response generators - most open domain bots will depend on many different strategies (an *ensemble approach*) for generating effective responses and holding engaging conversations and will thus likely use multiple response generator modules. However, simpler or bots with a narrower domain may only use a single response generator.�h]�(h.�~A Cobot instance can run one or more response generators - most open domain bots will depend on many different strategies (an �����}�(h�~A Cobot instance can run one or more response generators - most open domain bots will depend on many different strategies (an �hj�  hhh NhNubh�)��}�(h�*ensemble approach*�h]�h.�ensemble approach�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.��) for generating effective responses and holding engaging conversations and will thus likely use multiple response generator modules. However, simpler or bots with a narrower domain may only use a single response generator.�����}�(h��) for generating effective responses and holding engaging conversations and will thus likely use multiple response generator modules. However, simpler or bots with a narrower domain may only use a single response generator.�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhhJh h!hKAhj  hhubhK)��}�(hX	  A simple response generators can be a class that runs locally within your Cobot's Lambda, but more sophisticated models will generally need to be hosted remotely (loading complicated libraries and large models is not possible in the constrained Lambda environment).�h]�h.X  A simple response generators can be a class that runs locally within your Cobot’s Lambda, but more sophisticated models will generally need to be hosted remotely (loading complicated libraries and large models is not possible in the constrained Lambda environment).�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKChj  hhubhK)��}�(h�YDefining a response generator that runs remotely within its own Docker container is easy:�h]�h.�YDefining a response generator that runs remotely within its own Docker container is easy:�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKEhj  hhubh[)��}�(h��RemoteGreeterBot = {
        'name': "GREETER",
        'class': RemoteServiceModule,
        'url': ServiceURLLoader.get_url_for_module("GREETER"),
        'context_manager_keys': ['intent','slots']
}

cobot.add_response_generators([RemoteGreeterBot])�h]�h.��RemoteGreeterBot = {
        'name': "GREETER",
        'class': RemoteServiceModule,
        'url': ServiceURLLoader.get_url_for_module("GREETER"),
        'context_manager_keys': ['intent','slots']
}

cobot.add_response_generators([RemoteGreeterBot])�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�hjhkhl�python�hn�ho}�uhhZh h!hKGhj  hhubhK)��}�(hX�  In the above example, the response generator is given a name, a class to instantiate, a list of information from the state manager that the response generator depends on, and a url that is the endpoint for the response generator (note that the mechanics of this are all handled internally by the Cobot Toolkit - the ServiceURLLoader class will have access to the load balancer endpoint that allows it to reach the response generator).�h]�h.X�  In the above example, the response generator is given a name, a class to instantiate, a list of information from the state manager that the response generator depends on, and a url that is the endpoint for the response generator (note that the mechanics of this are all handled internally by the Cobot Toolkit - the ServiceURLLoader class will have access to the load balancer endpoint that allows it to reach the response generator).�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKRhj  hhubhK)��}�(h�dSee `the add_response_generators method documentation`__ for more details on the arguments expected.�h]�(h.�See �����}�(h�See �hj�  hhh NhNubh�)��}�(h�4`the add_response_generators method documentation`__�h]�h.�0the add_response_generators method documentation�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��name��0the add_response_generators method documentation�h�Kh��9cobot_core.html#module-cobot_core.add_response_generators�uhh�hj�  h�Kubh.�, for more details on the arguments expected.�����}�(h�, for more details on the arguments expected.�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhhJh h!hKThj  hhubh
)��}�(h�@.. __: cobot_core.html#module-cobot_core.add_response_generators�h]�h}�(h]��id3�ah]�h]�h]�h]�h�j  h�Kuhh	hKVhj  hhh h!h�Kubeh}�(h]��response-generators�ah]�h]��response generators�ah]�h]�uhh"hh$hhh h!hK=ubh#)��}�(hhh]�(h()��}�(h�Greeter Example�h]�h.�Greeter Example�����}�(hj1  hj/  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hj,  hhh h!hKZubhK)��}�(h��The Greeter response generator is a semi-scripted greeting sequence. It runs a dialog model built using the *rasa_core* library, a third party open source dialog modelling toolkit. Rasa_core is not part of the Cobot Toolkit, but can be used with it.�h]�(h.�lThe Greeter response generator is a semi-scripted greeting sequence. It runs a dialog model built using the �����}�(h�lThe Greeter response generator is a semi-scripted greeting sequence. It runs a dialog model built using the �hj=  hhh NhNubh�)��}�(h�*rasa_core*�h]�h.�	rasa_core�����}�(hhhjF  ubah}�(h]�h]�h]�h]�h]�uhh�hj=  ubh.�� library, a third party open source dialog modelling toolkit. Rasa_core is not part of the Cobot Toolkit, but can be used with it.�����}�(h�� library, a third party open source dialog modelling toolkit. Rasa_core is not part of the Cobot Toolkit, but can be used with it.�hj=  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhhJh h!hK\hj,  hhubhK)��}�(h�{The Greeter model is defined by the domain.yml and data/stories.md files (all located in the docker/greeter/app directory).�h]�h.�{The Greeter model is defined by the domain.yml and data/stories.md files (all located in the docker/greeter/app directory).�����}�(hja  hj_  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hK^hj,  hhubhK)��}�(hX  The domain.yml file defines a series of intents, slots and entities that this response generator understands, and a series of possible actions (responses) it can take. The stories.md file defines a series of dialog interactions using these intents and actions:�h]�h.X  The domain.yml file defines a series of intents, slots and entities that this response generator understands, and a series of possible actions (responses) it can take. The stories.md file defines a series of dialog interactions using these intents and actions:�����}�(hjo  hjm  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hK`hj,  hhubh[)��}�(h��## happy_path
* _greet
  - utter_greet
* _mood_great
  - utter_happy

## sad_path_1
* _greet
  - utter_greet
* _mood_unhappy
  - utter_cheer_up
  - utter_did_that_help
* _yes_intent
  - utter_happy

...�h]�h.��## happy_path
* _greet
  - utter_greet
* _mood_great
  - utter_happy

## sad_path_1
* _greet
  - utter_greet
* _mood_unhappy
  - utter_cheer_up
  - utter_did_that_help
* _yes_intent
  - utter_happy

...�����}�(hhhj{  ubah}�(h]�h]�h]�h]�h]�hjhkhl�bash�hn�ho}�uhhZh h!hKbhj,  hhubhK)��}�(hXO  After defining or changing the intents and actions in the domain.yml file or the dialog sequences in the story file, the dialog model needs to be re-trained. Running "make core-train" in the greeter/app directory will re-build the appropriate models, which can then be tested locally, and added and pushed to CodeCommit for deployment.�h]�h.XS  After defining or changing the intents and actions in the domain.yml file or the dialog sequences in the story file, the dialog model needs to be re-trained. Running “make core-train” in the greeter/app directory will re-build the appropriate models, which can then be tested locally, and added and pushed to CodeCommit for deployment.�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhhJh h!hKuhj,  hhubeh}�(h]��greeter-example�ah]�h]��greeter example�ah]�h]�uhh"hh$hhh h!hKZubeh}�(h]�(h�id1�eh]�h]�(�writing a bot��writing_a_bot�eh]�h]�uhh"hhhhh h!hK�expect_referenced_by_name�}�j�  hs�expect_referenced_by_id�}�hhsubeh}�(h]�h]�h]�h]�h]��source�h!uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h'N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h!�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�N�character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}�h]�has�nameids�}�(j�  hj�  j�  h�h�j  j  j)  j&  j�  j�  u�	nametypes�}�(j�  �j�  Nh�Nj  Nj)  Nj�  Nuh}�(hh$j�  h$h�h9h�h�j  h�j&  j  j  j  j�  j,  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�(h �system_message���)��}�(hhh]�(hK)��}�(hhh]�h.�Title overline too short.�����}�(hhhj.  ubah}�(h]�h]�h]�h]�h]�uhhJhj+  ubh[)��}�(h�?-----------------
Overriding Default Behavior
-----------------�h]�h.�?-----------------
Overriding Default Behavior
-----------------�����}�(hhhj;  ubah}�(h]�h]�h]�h]�h]�hjhkuhhZhj+  ubeh}�(h]�h]�h]�h]�h]��level�K�type��WARNING��line�K$�source�h!uhj)  ubj*  )��}�(hhh]�(hK)��}�(h�Title overline too short.�h]�h.�Title overline too short.�����}�(hhhjW  ubah}�(h]�h]�h]�h]�h]�uhhJhjT  ubh[)��}�(h�?-----------------
Overriding Default Behavior
-----------------�h]�h.�?-----------------
Overriding Default Behavior
-----------------�����}�(hhhje  ubah}�(h]�h]�h]�h]�h]�hjhkuhhZhjT  ubeh}�(h]�h]�h]�h]�h]��level�K�type�jQ  �line�K$�source�h!uhj)  hh�hhh h!hK&ubj*  )��}�(hhh]�(hK)��}�(hhh]�h.�Title overline too short.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhJhj}  ubh[)��}�(h�7-----------------
Response Generators
-----------------�h]�h.�7-----------------
Response Generators
-----------------�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�hjhkuhhZhj}  ubeh}�(h]�h]�h]�h]�h]��level�K�type�jQ  �line�K;�source�h!uhj)  ubj*  )��}�(hhh]�(hK)��}�(h�Title overline too short.�h]�h.�Title overline too short.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhJhj�  ubh[)��}�(h�7-----------------
Response Generators
-----------------�h]�h.�7-----------------
Response Generators
-----------------�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�hjhkuhhZhj�  ubeh}�(h]�h]�h]�h]�h]��level�K�type�jQ  �line�K;�source�h!uhj)  hj  hhh h!hK=ube�transform_messages�]�j*  )��}�(hhh]�hK)��}�(hhh]�h.�3Hyperlink target "writing-a-bot" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhJhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h!�line�Kuhj)  uba�transformer�N�
decoration�Nhhub.