.. _data_analytics_visualization:

Data Analytics and Visualization with AWS Athena and QuickSight
===============================================================

In CoBot, we recommend to have a data pipeline that separates data storage from data analytics.
In the real-time data application, conversational data is persisted to NoSQL DynamoDB tables.
Some additional data is also uploaded to your S3 bucket daily (rating and feedback --
which users are prompted to provide at the end of a conversation with the bot), or weekly 
(coherence and engagement metrics -- which are derived from manually annotated sample of conversations). 
CoBot makes it easy to use AWS Athena to easily query and analyze data directly in Amazon S3 using standard SQL. 
In addition, AWS QuickSight can be used visualize this data and create live dashboards.

For more detail on the data pipeline, refer to https://aws.amazon.com/blogs/database/how-to-perform-advanced-analytics-and-build-visualizations-of-your-amazon-dynamodb-data-by-using-amazon-athena/

------------
Prerequisite
------------
To install boto3 python module:

.. code-block:: bash

    python3 -m pip install boto3

-----
Steps
-----

CoBot includes commands and scripts to help set the data pipeline from your DynamoDB StateTable to S3 in a format that lets you work with Athena and Quicksight. Some of this is automated while some of the steps have to be done maually. 

1. View DynamoDB Table settings
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You should already have a DynamoDB state table which was created by CoBot. Open the AWS DynamoDB console and view "Overview" for this table.
Under "Stream Details" if the steam is already enabled (presumably because you already have a pipeline that uses the stream) then go to Step 3

2. Enable Stream
^^^^^^^^^^^^^^^^
Click "Manage Stream". Select 'New Image' in the pop-up dialog, and click Enable

3. Get Stream ARN
^^^^^^^^^^^^^^^^^
Copy the stream ARN to the clipboard. You will need this in a later step.

.. _dynamodb_to_s3_deploy:

4. Create the Pipeline
^^^^^^^^^^^^^^^^^^^^^^
This step will create a pipeline from your DynamoDB table to an S3 bucket. After this step any new records added to your State Table will automatically
be copied over to your S3 bucket. If you have additional fields in your StateTable in addition to the default ones created by Cobot, those will also carry over.
The S3 bucket name is automatically selected for you and will be printed at the end. The data will be stored in the bucket in a folder called "firehose"

.. code-block:: bash

   cobot dynamodb-to-s3 deploy -t <state_table_name> -s <stream Arn from previous step>


.. image:: images/dynamodb-to-s3-deploy-ouput.png

5. Find the S3 Bucket name
^^^^^^^^^^^^^^^^^^^^^^^^^^
The name is printed out in the previous step.  You can also find the name of the S3 bucket by going to Kinesis in the AWS console. Click on "Data Firehose". You will see a stream listed with the name StateTableStream. The S3 bucket name is in the Destination column. You will need this name in later steps.

.. _dynamodb_to_s3_backfill:

6. [Optional & Recommended] Backfill DynamoDB Table to S3
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This step copies existing records in your DynamoDB table directly to S3. 

.. code-block:: bash

   cobot dynamodb-to-s3 backfill  -t <state_table_name> -b <output_s3_bucket> 

The <output_s3_bucket> is the S3 bucket that was created for you in the previous step. The data will be copied to the same folder "firehose" where streaming data is getting
copied. Athena will be able to combine both data sources seamlessly.

7. Create tables in Athena
^^^^^^^^^^^^^^^^^^^^^^^^^^
To learn more about Athena visit https://docs.aws.amazon.com/athena/latest/ug/getting-started.html
Now create an Athena table for your data, following the sample query below.

.. code-block:: SQL

    CREATE EXTERNAL TABLE `STATE_TABLE` (
      `session_id` string,
      `creation_date_time` timestamp, 
      `candidate_response` string, 
      `conversation_id` string,
      `user_id` string, 
      `request_type` string, 
      `intent` string, 
      `text` string, 
      `sentiment` string, 
      `topic` string, 
      `response` string,
      `ner` string,
      `slots` string,
      `asr` string,
      `baseline` string
    ) 
    ROW FORMAT SERDE 
      'org.openx.data.jsonserde.JsonSerDe' 
    LOCATION
      's3://<output_s3_bucket>/firehose'

You may specify additional fields in your table if your state table has additional data. Once the table is created you can query it with standard SQL. As new data gets added to S3, it will automatically be pulled into Athena at query time.
Here is an example query to get count by Topic :

.. code-block:: SQL
    
    SELECT topic, count(*) AS cnt
    FROM STATE_TABLE
    GROUP BY topic

Here is example to get Intent counts since a particular date:

.. code-block:: SQL
    
    SELECT intent, count(*) as cnt  
    FROM SAMPLE_TABLE  
    WHERE date(creation_date_time) > date('2018-05-22') 
    GROUP BY intent 
    ORDER BY cnt DESC;


8. Visualize and Create Dashboard  in QuickSight
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
To learn more about Quicksight visit https://docs.aws.amazon.com/quicksight/latest/user/quickstart-createanalysis.html
You can easily create visuals such as Histograms and Pie charts with your Athena Tables. You can Filter and Group data based on your requirements.

For example, to create a Pie Chart by Topic:
Click on "New Analysis"  --> "New Data Set" --> "Athena"
Give any name to the data source and then specify the Athena Database and Table name.
Select "Directly query your data" and then click "Visualize". Select "Topic" from the Field List and then Click on Pie Chart under Visual Types.

9. Import other data
^^^^^^^^^^^^^^^^^^^^
In a future update we will describe how to include other data sources (e.g. Ratings csv file) into Athena as a table, and do joins wth the state table based on convesation_id.
