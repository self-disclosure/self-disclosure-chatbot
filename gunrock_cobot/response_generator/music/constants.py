from nlu import intentmap_scheme

# moving all the constants to this namespace
from .karaoke import karaoke_songs  # noqa: F401
from .genre100 import genre_100  # noqa: F401
from .genre_artist import genre_artist_dict  # noqa: F401


BACKSTORY_THRESHOLD = 0.82


regex_lists = dict(
    exit=dict(
        whitelist=(
            r"can we talk | stop|can we chat|change topic| go away| go now| don't listen to music|"
            r"talk about something else|done chatting|quit|talk about something else|"
            r"don't want to talk about music|don't like music|hate music|turn off|"
            r"i'm not feeling too music|stop talking about music|change the conversation|"
            r"stop talking to me|change from social bot|do not like music|don't talk to me"
        )
    ),
    play_music=dict(
        # whitelist=(
        #     r"play me|hear you|put it|play it|hear it|sing it|let's listen to| you play a sample|can you play|"
        #     r"turn the music up| play music|turn the music up|could you play|can we play|listen to it|"
        #     r"play some music|put on a song|i want to hear|i would like to hear"
        # ),
        # blacklist=r"i play it|i listen to it|i play music"
        whitelist=(
            intentmap_scheme.sys_re_patterns['req_play_music']
        ),
        blacklist=(
            r"\b(play instruments?)\b"
        )
    ),
    repeat=dict(
        whitelist=(r"\b(^what$|ask that question again|sorry what|say what)\b")
    ),
    asking_for_recommendation=dict(
        whitelist=r"recommend|suggest"
    ),
    other_answer=dict(
        whitelist=(
            r"don't know|can't remember|none|don't really have one|not sure|too many artist|"
            r"no idea|don't have a favorite|lot of favorite songs|that's tough|all of them|nobody|"
            r"that's a tough one|can't think of"
        )
    ),
    saying_all_of_them=dict(
        whitelist=(
            r"many|all|all of it|everything"
        )
    ),
    rapport=dict(
        whitelist=(
            r"me too|do too| same| like it too| think so too|me as well|i'm too|mine too|like her"
        )
    ),
    user_like_to_sing=dict(
        whitelist=(
            r"(like (to sing|singing)|singing|sing(ing)? songs|(sing(ing)?|hum(ming)?) to (songs?|tunes?)|\bsing$)"
        )
    ),
    lets_talk_about=dict(
        whitelist=(
            r"\b(let('?s| us)|i (wanna|want to)) (talk|chat|discuss) about \b"
        )
    ),
    false_songs=dict(
        whitelist=(
            r"^(ok|okay|no|yes)$"
        )
    ),
    command_sing_a_song=dict(
        whitelist=(
            r"sing a song|sing me a song|sing me another song|sing a |sing for me"
        ),
        blacklist=(
            r"i like to sing|i want to sing|i will sing| sing it to me|sing at|\bsing$"
        )
    ),
    user_sing_us_a_song=dict(
        whitelist=(
            r"sing you a song|sing for you"
        )
    ),
    asking_your_favorite=dict(
        whitelist=(
            r"\b(anything|what(ever)? (do )?you like|(what|who)('s| is) your(s| favorite)|what about you)\b"
        )
    ),
)


# TAG: UNUSED BY GT
sys_intents = {"ask_recommend", "exit_bookRG"}

evi_filter = (
    r"an opinion on that|tough to explain|I didn’t get that.|Sorry, I didn’t catch that|"
    r"Alexa Original|'favorite' is usually defined|is a plural form of|Hi there|Hi|"
    r"Sorry, I can’t find the answer to the question I heard.|I don't have an opinion on that.|"
    r"You could ask me about music or geography.|You can ask me anything you like.|"
    r"I can answer questions about people, places and more.|^As a|I didn't get that|"
    r"'Artist' is usually defined|The Place I Belong, the movie released|"
    r"'Talking' is usually defined|usually defined as plural of what|sound is a vibration|"
    r"something that would happen|Say is a town in southwest Niger|An album is a collection of|"
    r"I love dancing|I pronounce that 'would'|I pronounce that 'do'|'Was' is related to a word|"
    r"'Next' is usually defined|Dreams aren't well understood|\"Me Too\" or \"#MeToo\"|"
    r"Yours, the chain of women's clothing outlets.|the computer application developed by Apricorn|"
    r"The movie \"Instrument\"|A Little Bit is by Jessica Simpson|'S is by Jim Bianco"
)

entity_ignore = ['okay', 'ok', 'music']
ignore_entity = (
    r"my favorite|^music$|^artist$|^siri$|good question|hard question|"
    r"good question|any kind|^okay$|i like|just like vinyl|i have a tribe|"
    r"^yes$|^yeah$|^guess$|^good$|the 1975|please|download|question|sure|"
    r"i like a lot|everything|all that|right now|^fun$|multiple|hello|talk|"
    r"bizarre|anything|like birds|your favorite|play|world|no doubt|me too|"
    r"someone|^me$|love|the first|three |s.h.e|normal |the normal |dinner |"
    r"^one$|oh no oh my|hits|listening|i'm not a gun|^ha$|you say party|"
    r"another|^swami$|^you$|^my$|I like you|this is my fist|^am i too$|^b$|"
    r"^sing sing$|^i$|^because$|^awesome$|^t i$|^t. i$|^hey$|^powers that be$|"
    r"^back to back$|^when$|^pop-a-lot$|^think i care$|^a-ha$|^lol$|"
    r"^bow wow wow$|^happy birthday$"
)

rapport_match = regex_lists['rapport']['whitelist']
skip_list = r"taylor swift|maroon 5|imagine dragons|drake|michael jackson"
recommend_terms = regex_lists['asking_for_recommendation']['whitelist']
modules_mapping = {
    "ANIMALCHAT": "animals", "MOVIECHAT": "movies", "GAMECHAT": "games", "FOODCHAT": "food", "BOOKCHAT": "books",
    "PSYPHICHAT": "psychology and philosophy", "SPORT": "sports",
    "TECHSCIENCECHAT": "technology and science", "TRAVELCHAT": "traveling",
}
special_artists = {
    "two pac": "2pac",
    "m eighty three": "m83",
    "u two": "u2",
    "tech nine": "tech n9ne",
    "black": "6lack",
    "three oh three": "3oh three",
    "rehab": "r3hab",
    "ub forty": "ub40",
    "j cole": "j. cole",
    "maroon five": "maroon 5",
    "g eazy": "g-eazy",
    "panic at the disco": "panic! at the disco",
    "asap rocky": "a$ap rocky",
    "twenty one savage": "21 savage",
    "mumford and sons": "mumford & sons",
    "two chainz": "2 chainz",
    "blink one hundred and eighty two": "blink-182",
    "the one thousand  nine hundred and seventy five": "the 1975",
    "bob marley the wailers": "bob marley & the wailers",
    "the notorious b i g": "the notorious b.i.g.",
    "fifty cent": "50 cent",
    "b o b": "b.o.b",
    "ty dolla sign": "ty dolla $ign",
    "r kelly": "r. kelly",
    "t pain": "t-pain",
    "ac dc": "ac/dc",
    "alt j": "alt-j",
    "macklemore and ryan lewis": "macklemore & ryan lewis",
    "ne yo": "ne-yo",
    "pink floyd": "pink floyd",
    "pink": "p!nk",
    "florence the machine": "florence + the machine",
    "tyler the creator": "tyler, the creator",
    "dr dre": "dr. dre",
    "the all american rejects": "the all-american rejects",
    "daryl hall john oates": "daryl hall & john oates",
    "five seconds of summer": "5 seconds of summer",
    "nsync": "*nsync",
    "earth wind fire": "earth, wind & fire",
    "portugal the man": "portugal. the man",
    "the jackson five": "the jackson 5",
    "fun": "fun.",
    "joey bada": "joey bada$$",
    "simon garfunkel": "simon & garfunkel",
    "edward sharpe the magnetic zeros": "edward sharpe & the magnetic zeros",
    "three doors down": "3 doors down",
    "dan shay": "dan + shay",
    "marc e bassy": "marc e. bassy",
    "mike will made it": "mike will made-it",
    "hillsong young free": "hillsong young & free",
    "rob tone": "rob $tone",
    "sum forty one": "sum 41",
    "iron wine": "iron & wine",
    "brooks dunn": "brooks & dunn",
    "boyz two men": "boyz ii men",
    "all sons daughters": "all sons & daughters",
    "mary j blige": "mary j. blige",
    "shane shane": "shane & shane",
    "wisin yandel": "wisin & yandel",
    "o t genasis": "o.t. genasis",
    "wu tang clan": "wu-tang clan",
    "donnie trumpet the social experiment": "donnie trumpet & the social experiment",
    "lil jon the east side boyz": "lil jon & the east side boyz",
    "ayo teo": "ayo & teo",
    "selena gomez the scene": "selena gomez & the scene",
    "will i am": "will.i.am",
    "three hundred and eleven": "311",
    "angus julia stone": "angus & julia stone",
    "yusuf cat stevens": "yusuf / cat stevens",
    "ruth b": "ruth b.",
    "magic": "magic!",
    "anderson paak": "anderson .paak",
    "e forty": "e-40",
    "years years": "years & years",
    "frankie valli the four seasons": "frankie valli & the four seasons",
    "ms lauryn hill": "ms. lauryn hill",
    "wham": "wham!",
    "for king country": "for king & country",
    "bone thugs n harmony": "bone thugs-n-harmony",
    "st lucia": "st. lucia",
    "run d m c": "run–d.m.c.",
    "hank williams jr": "hank williams, jr.",
    "nico vinz": "nico & vinz",
    "axwell ingrosso": r"axwell /\ ingrosso",
    "young m a": "young m.a",
    "judah the lion": "judah & the lion",
    "salt n pepa": "salt-n-pepa",
    "kool the gang": "kool & the gang",
    "she him": "she & him",
    "mr probz": "mr. probz",
    "eazy e": "eazy-e",
    "uicideboy": "$uicideboy$",
    "calibre fifty": "calibre 50",
    "dr dog": "dr. dog",
    "one hundred and twelve": "112",
    "big rich": "big & rich",
    "dimitri vegas like mike": "dimitri vegas & like mike",
    "leslie odom jr": "leslie odom jr.",
    "the dream": "the-dream",
    "zion and lennox": "zion & lennox",
    "samuel e wright": "samuel e. wright",
    "of mice men": "of mice & men",
}

artist_re = [
    r"^have you listened (?!to|a|it|the|something|what|your|my|both|some|this|that|no|one)(\w+)$",
    r"^have you listened (?!to|a|it|the|something|what|your|my|both|some|this|that|no|one)(\w+ \w+)$",
    r"have you listened (?!to|a|it|the|something|what|your|my|both|some|this|that|no|one)(\w+ \w+ \w+)$",
    r"have you listened (?!to|a|it|the|something|what|your|my|both|some|this|that|no|one)(\w+ \w+ \w+ \w+)$",
    r"have you listened (?!to|a|it|the|something|what|your|my|both|some|this|that|no|one)(\w+ \w+ \w+ \w+ \w+)$",
    r"have you listened (?!to|a|it|the|something|what|your|my|both|some|this|that|no|one)(\w+ \w+ \w+ \w+ \w+)$",
    (r"have you listened to the song (\w+)$|have you listened to the song (\w+ \w+)$|"
     r"have you listened to the song (\w+ \w+ \w+)$|have you listened to the song (\w+ \w+ \w+ \w+)$"),
    (r"favorite artist is (\w+)$|favorite artist is (\w+ \w+)$|favorite artist is (\w+ \w+ \w+)$|"
     r"favorite artist is (\w+ \w+ \w+ \w+)$"),
    r"favorite artist is (\w+ \w+ \w+ \w+ \w+ \w+)$|know the artist (\w+)$|know the artist (\w+ \w+)$",
    (r"about the artist (\w+)|about the artist (\w+ \w+)$|about the artist (\w+ \w+ \w+)$|"
     r"about the artist (\w+ \w+ \w+ \w+)$"),
    r"lets talk about (?!music)",
    r"tell me about (\w+)$|tell me about (\w+ \w+)$|tell me about (\w+ \w+ \w+)$|tell me about (\w+ \w+ \w+ \w+)$",
    (r"what are some artists like (\w+)$|what are some artists like (\w+ \w+)$|"
     r"what are some artists like (\w+ \w+ \w+)$|what are some artists like (\w+ \w+ \w+ \w+)$"),
    (r"favorite character in (\w+)$|favorite character in (\w+ \w+)$|favorite character in (\w+ \w+ \w+)$|"
     r"favorite character in (\w+ \w+ \w+ \w+)$")
]

profanity_filter = (
    r"abo|abortion|abuse|acrotomophilia|addict|addict|adult|adultery|africa|african|alla|allah|"
    r"alligatorbait|amateur|amcik|american|anal|analannie|analsex|andskota|angie|angry|anilingus|"
    r"antiestablishiarism|anus|apeshit|arab|arabs|areola|argie|aroused|arschloch|arse|arsehole|"
    r"asholes|asian|aslan|ass|assassin|assassinate|assassination|assault|assbagger|assblaster|"
    r"assclown|asscowboy|asses|assface|assfuck|assfucker|asshat|asshole|assholes|assholz|asshore|"
    r"assjockey|asskiss|asskisser|assklown|asslick|asslicker|asslover|assman|assmonkey|assmunch|"
    r"assmuncher|asspacker|asspirate|asspuppies|assrammer|assranger|asswhore|asswipe|athletesfoot|"
    r"attack|australian|autoerotic|ayir|azzhole|babe|babeland|babies|backdoor|backdoorman|backseat|"
    r"badfuck|ballache|balllicker|balls|ballsack|bang|bangbros|banging|baptist|bareback|barelylegal|"
    r"barenaked|barf|barface|barfface|bassterds|bast|bastard|bastard|bastardo|bastards|bastardz|"
    r"basterds|basterdz|bastinado|batshit|bazongas|bazooms|bbw|bch|bdsm|beaner|beast|beastality|"
    r"beastial|beastiality|beatoff|beatyourmeat|beaver|bellend|bestial|bestiality|biatch|bible|"
    r"bich|bicurious|bieber|bigass|bigbastard|bigbutt|bigger|biiitch|biiittch|bimbos|birdlock|"
    r"bisexual|bitch|bitcher|bitches|bitchez|bitchin|bitching|bitchslap|bitchtits|bitchy|biteme|"
    r"black|blackman|blackout|blacks|blind|bloodclaat|blow|blowjob|blueballs|blumpkin|boang|bob|"
    r"boffing|bogan|bohunk|boiolas|bollick|bollock|bollocks|bomb|bombers|bombing|bombs|bomd|"
    r"bondage|boned|boner|bong|boob|boobies|boobs|booby|boody|boom|boong|boonga|boonie|booty|"
    r"bootycall|bountybar|bra|brainfuck|breast|breastjob|breastlover|breastman|breasts|brothel|"
    r"btch|buceta|bugger|buggered|buggery|bukkake|bullcrap|bulldike|bulldyke|bulldykes|bullshit|"
    r"bullshitter|bullshittin|bullshitting|bumblefuck|bumfuck|bumsucker|bumtag|bunga|bunghole|"
    r"buried|burn|busty|butchbabes|butchdike|butchdyke|butt|buttbang|buttcheeks|buttface|buttfuck|"
    r"buttfucker|buttfuckers|butthead|butthole|buttman|buttmunch|buttmuncher|buttpirate|buttplug|"
    r"buttstain|buttwipe|byatch|cabron|cacker|cameljockey|cameltoe|camgirl|camslut|camwhore|"
    r"canada|canadian|cancer|carpetmuncher|carruth|catholic|catholics|cawk|cawks|cazzo|cemetery|"
    r"chav|cherrypopper|chickenshit|chickslick|chin|chinaman|chinamen|chinc|chinese|chink|chinky|"
    r"choad|chode|chollo|cholo|chord|chraa|christ|christian|chuj|church|cigarette|cigs|cipa|"
    r"circlejerk|clamdigger|clamdiver|clit|clitface|clitoris|clits|clogwog|clunge|clusterfuck|"
    r"cnts|cntz|cocaine|cock|cockblock|cockblocker|cockcowboy|cockfight|cockhead|cockknob|"
    r"cocklicker|cocklover|cocknob|cockqueen|cockrider|cocks|cocksman|cocksmith|cocksmoker|"
    r"cocksucer|cocksuck|cocksuck|cocksucked|cocksucker|cocksuckers|cocksuckin|cocksucking|"
    r"cocktail|cocktease|cockteaser|cocky|cohee|coitus|color|colored|coloured|commie|communist|"
    r"condom|conservative|conspiracy|coolie|cooly|coon|coondog|coprolagnia|coprophilia|copulate|"
    r"cornhole|corruption|crabs|crack|crackpipe|crackwhore|crap|crapola|crapper|crappy|crash|"
    r"creamy|crime|crimes|criminal|criminals|crotch|crotchjockey|crotchmonkey|crotchrot|cum|"
    r"cumbubble|cumfest|cumjockey|cumm|cummer|cumming|cumquat|cumqueen|cumshot|cunilingus|"
    r"cunillingus|cunn|cunnilingus|cunntt|cunt|cunted|cunteyed|cuntface|cuntfaced|cuntfuck|"
    r"cuntfucker|cunthead|cuntlick|cuntlicker|cuntlicking|cunts|cuntsucker|cunty|cuntz|cybersex|"
    r"cyberslimer|dago|dagoes|dahmer|dammit|damn|damnation|damnit|dark|darkie|darky|daterape|"
    r"datnigga|daygo|dead|deapthroat|death|deepthroat|defecate|dego|demon|deposit|depression|"
    r"desire|destroy|deth|devil|devilworshipper|dick|dickbag|dickbeaters|dickbrain|dickface|"
    r"dickforbrains|dickhead|dickhole|dickjuice|dickless|dicklick|dicklicker|dickman|dickmilk|"
    r"dickmonger|dickslap|dicksplash|dicksplat|dicksucker|dicksucking|dickwad|dickweasel|"
    r"dickweed|dickwod|diddle|diddyride|die|died|dies|dike|dildo|dildos|dingleberry|dink|"
    r"dipshit|dipstick|dirsa|dirtbox|dirty|disapointing|disease|diseases|disgusting|disturbed|"
    r"dive|dix|dixiedike|dixiedyke|doggiestyle|doggystyle|dolcett|domination|dominatricks|"
    r"dominatrics|dominatrix|dommes|dong|doochbag|doodoo|doom|dope|dothead|douche|douchebag|"
    r"douchebags|dragqueen|dragqween|dripdick|driving|drug|drunk|drunken|dumb|dumbass|"
    r"dumbbitch|dumbfuck|dumfuck|dupa|dyefly|dying|dyke|dziwka|easyslut|eatballs|eatme|"
    r"eatpussy|ecchi|ecstacy|ejackulate|ejaculate|ejaculated|ejaculating|ejaculation|"
    r"ejakulate|ekrem|ekto|enculer|enema|enemy|erect|erection|ero|erotic|erotism|escort|"
    r"ethiopian|ethnic|eunuch|european|evl|excrement|execute|executed|execution|executioner|"
    r"existance|existing|explosion|eyefuck|eyefucked|eyefucking|facefuck|facefucker|"
    r"facesitting|faeces|faen|fag|faget|fagging|faggit|faggot|fagit|fagot|fags|fagz|faig|"
    r"faigs|failed|failure|fairies|fairy|faith|fanculo|fanny|fannybatter|fannyfucker|fart|"
    r"farted|farting|farty|fastfuck|fat|fatah|fatass|fatfuck|fatfucker|fatso|fckcum|fckn|"
    r"fcuk|fear|fecal|feces|feg|felatio|felch|felcher|felching|fellatio|feltch|feltcher|"
    r"feltching|femdom|fetish|ficken|figging|fight|filipina|filipino|fingerfood|fingerfuck|"
    r"fingerfucked|fingerfucker|fingerfuckers|fingerfucking|fingering|fire|firing|fister|"
    r"fistfuck|fistfucked|fistfucker|fistfucking|fisting|fitt|fizuk|flange|flasher|flatulence|"
    r"flikker|flipping|floo|fluck|flydie|flydye|fok|fondle|footaction|footfuck|footfucker|"
    r"footjob|footlicker|footstar|fore|foreskin|forni|fornicate|fotze|foursome|fourtwenty|"
    r"fraud|freakfuck|freakyfucker|freefuck|frotting|fu|fubar|fuc|fucc|fucck|fuchah|fuck|"
    r"fucka|fuckable|fuckbag|fuckbuddy|fucked|fuckedup|fucker|fuckers|fuckery|fuckface|"
    r"fuckfest|fuckfreak|fuckfriend|fuckhead|fuckheads|fuckher|fuckin|fuckina|fucking|"
    r"fuckingbitch|fuckinnuts|fuckinright|fuckit|fuckity|fuckknob|fuckload|fuckmachine|"
    r"fuckme|fuckmehard|fuckmonkey|fuckoff|fuckpig|fucks|fuckstick|fucktard|fuckton|"
    r"fuckwhore|fuckwit|fuckyou|fudgepacker|fugly|fuk|fukah|fuken|fuker|fukin|fukk|"
    r"fukka|fukkah|fukken|fukker|fukkin|fukkk|fukload|fuks|fuktard|funeral|funfuck|"
    r"fungus|futanari|futkretzn|fuuck|game|gangbang|gangbanged|gangbanger|gangsta|"
    r"gatorbait|gay|gaybob|gaybor|gayboy|gaydo|gaygirl|gaymuthafuckinwhore|gays|"
    r"gaysex|gaytard|gaywad|gayz|geez|geezer|geni|genital|genitals|german|getiton|gin|"
    r"ginzo|gipp|gippo|girls|gism|givehead|glazeddonut|glitter|goatcx|goatse|gob|god|"
    r"godammit|goddamit|goddamm|goddammit|goddamn|goddamned|goddamnes|goddamnit|"
    r"goddamnmuthafucker|gokkun|goldenshower|golliwog|golliwogg|gonorrehea|gonzagas|goodpoop|"
    r"gook|goregasm|gotohell|goy|goyim|grammaranal|greaseball|gringo|groe|grope|gross|"
    r"grostulation|gubba|guiena|gummer|gun|guro|gyp|gypo|gypp|gyppie|gyppo|gyppy|halfrican|"
    r"hamas|handjob|hapa|happy|hardcore|harder|hardon|harem|hatefuck|headfuck|headlights|"
    r"hebe|heeb|hell|hells|helvete|henhouse|hentai|heroin|herpes|heterosexual|hijack|"
    r"hijacker|hijacking|hillbillies|hindoo|hiscock|hitler|hitlerism|hitlerist|hiv|ho|"
    r"hoar|hobo|hodgie|hoer|hoes|hole|holestuffer|holocaust|homicide|homo|homobangers|"
    r"homodunshit|homoerotic|homosexual|honger|honk|honkers|honkey|honky|hook|hooker|"
    r"hookers|hoor|hoore|hooters|hore|hork|horn|horney|horniest|horny|horseshit|hosejob|"
    r"hoser|hostage|hotdamn|hotpussy|hottotrot|huevon|hui|hummer|humping|husky|hussy|"
    r"hustler|hymen|hymie|iblowu|idiot|ikey|illegal|incest|injun|insest|intercourse|"
    r"interracial|intheass|inthebuff|israel|israeli|italiano|itch|jackass|jackoff|"
    r"jackshit|jacktheripper|jade|jailbait|jap|japanese|japcrap|japs|jebus|jeez|jerkoff|"
    r"jesus|jesuschrist|jew|jewboy|jewed|jewing|jewish|jiga|jigaboo|jigg|jigga|jiggabo|"
    r"jiggaboo|jigger|jiggerboo|jiggy|jihad|jijjiboo|jimfish|jisim|jism|jiss|jiz|jizim|"
    r"jizjuice|jizm|jizm|jizz|jizzim|jizzum|joint|juggalo|juggs|jugs|junglebunny|kaffer|"
    r"kaffir|kaffre|kafir|kanake|kanker|kawk|kid|kigger|kike|kill|killed|killer|killing|"
    r"kills|kinbaku|kink|kinkster|kinky|kissass|kkk|klootzak|klusterfuck|klusterfuk|"
    r"knife|knob|knobbing|knobs|knobz|knockers|knulle|kock|kondum|kooch|koon|kootch|"
    r"kotex|krap|krappy|kraut|kuk|kuksuger|kum|kumbubble|kumbullbe|kummer|kumming|"
    r"kumquat|kums|kunilingus|kunnilingus|kunt|kunts|kuntz|kurac|kurwa|kusi|ky|kyke|"
    r"kyrpa|labia|lactate|laid|lapdance|lardass|latin|lesbain|lesbayn|lesbian|lesbin|"
    r"lesbo|lez|lezbe|lezbefriends|lezbo|lezz|lezza|lezzer|lezzian|lezzo|liberal|libido|"
    r"licker|licking|lickme|lies|light|limey|limpdick|limy|lingerie|lipshits|lipshitz|"
    r"liquor|livesex|loadedgun|lolita|looser|loser|lotion|lovebone|lovegoo|lovegun|"
    r"lovejuice|lovemaking|lovemuscle|lovepistol|loverocket|lowlife|lsd|lubejob|"
    r"lucifer|luckycammeltoe|lugan|luigi|lynch|macaca|mad|mafia|magicwand|mamhoon|"
    r"mams|manhater|manmuck|manpaste|mardarse|marijuana|mario|masochist|masokist|"
    r"massterbait|masstrbait|masstrbate|mastabate|mastabater|masterbaiter|masterbat|"
    r"masterbate|masterbates|masterblaster|mastrabator|masturbat|masturbate|masturbated|"
    r"masturbating|masturbation|mattressprincess|meatbeatter|meatrack|meme|merd|meth|"
    r"mexcrement|mexican|mgger|mggor|mibun|mickeyfinn|mideast|milf|mindfuck|minge|"
    r"minority|mockey|mockie|mocky|moffie|mofo|moky|moles|molest|molestation|molester|"
    r"molestor|mom|moneyshot|monkleigh|mooncricket|mormon|moron|moslem|mosshead|motha|"
    r"mothafuck|mothafucka|mothafuckaz|mothafucked|mothafucker|mothafuckin|mothafucking|"
    r"mothafuckings|mothafuker|mothafukkah|mothafukker|motherfuck|motherfucked|"
    r"motherfucker|motherfuckers|motherfuckin|motherfucking|motherfuckings|motherfukah|"
    r"motherfuker|motherfukkah|motherfukker|motherlovebone|mouliewop|muff|muffdive|"
    r"muffdiver|muffdiving|muffindiver|mufflikcer|muie|mulatto|mulkku|muncher|munt|"
    r"murder|murderer|murdering|muschi|muslim|mutha|muthafucka|muthafuckas|muthafucker|"
    r"muthafuckin|muthafucking|muthafukah|muthafukas|muthafuker|muthafukkah|muthafukker|"
    r"muthaphuckkin|muthaphukkin|muzzie|naked|nambla|nappyhead|narcotic|nasa|nastt|nasty|"
    r"nastybitch|nastyho|nastyslut|nastywhore|nawashi|nazi|nazis|necro|necrophilia|negress|"
    r"negro|negroes|negroid|neonazi|nepesaurio|nig|nigaboo|niger|nigerian|nigerians|nigg|"
    r"nigga|niggah|niggaracci|niggard|niggarded|niggarding|niggardliness|niggardly|"
    r"niggards|niggas|niggaz|nigger|niggerhead|niggerhole|niggers|niggle|niggled|"
    r"niggles|niggling|nigglings|niggor|niggs|niggur|niglet|nignog|nigr|nigra|nigre|"
    r"nigur|niiger|niigr|nimphomania|nintendo|nip|nipple|nipplering|nipples|nittit|"
    r"nlgger|nlggor|nofuckingway|nook|nookey|nookie|noonan|nooner|nude|nudger|nudity|"
    r"nuke|nutfucker|nutsack|nymph|nympho|nymphomania|obama|octopussy|omorashi|ontherag|"
    r"orafis|oral|orga|orgasim|orgasim|orgasm|orgasum|orgies|orgy|oriface|orifice|"
    r"orifiss|orospu|osama|packi|packie|packy|paedophile|paki|pakie|paky|palesimian|"
    r"palestinian|panooch|pansies|pansy|panti|panties|panty|paska|payo|pearlnecklace|"
    r"peck|pecker|peckerwood|pedobear|pedophile|pee|peeenus|peeenusss|peehole|peenus|"
    r"peepshow|peepshpw|pegging|peinus|penas|pendy|penetration|penile|penis|penisbanger|"
    r"penisbreath|penises|penispuffer|penthouse|penus|penuus|pepsi|period|perse|perv|"
    r"phalus|phonesex|phuc|phuck|phuk|phuked|phuker|phuking|phukked|phukker|phukking|"
    r"phungky|phuq|picaninny|piccaninny|picka|pickaninny|pierdol|piker|pikey|piky|"
    r"pillu|pimmel|pimp|pimped|pimper|pimpis|pimpjuic|pimpjuice|pimpsimp|pindick|"
    r"piss|pissed|pisser|pisses|pisshead|pissin|pissing|pissoff|pisspig|pistol|pixie|"
    r"pixy|pizda|playboy|playgirl|pocha|pocho|pocketpool|pohm|polac|polack|polak|"
    r"polesmoker|pom|pommie|pommy|ponyplay|poo|poof|pooftah|poon|poonani|poonany|"
    r"poontang|poontsee|poop|poopchute|pooper|pooperscooper|pooping|poorwhitetrash|"
    r"popery|popimp|popish|porchmonkey|porn|pornflick|pornking|porno|pornography|"
    r"pornprincess|pot|poverty|premature|preteen|pric|prick|prickhead|primetime|"
    r"proddy|propaganda|pros|prostitute|protestant|pthc|pube|pubes|pubic|pubiclice|"
    r"pud|pudboy|pudd|puddboy|puke|pula|pule|punani|punanny|punany|puntang|"
    r"purinapricness|puss|pusse|pussee|pussie|pussies|pussy|pussycat|pussyeater|"
    r"pussyfucker|pussylicker|pussylicking|pussylips|pussylover|pussypounder|"
    r"pussywhipped|pusy|puta|putin|puto|puuke|puuker|pzychobitch|qahbeh|quashie|"
    r"queaf|queef|queer|queerbait|queerhole|queers|queerz|quickie|quim|qweers|"
    r"qweerz|qweir|rabbi|racial|racist|radical|radicals|raghead|randy|rape|raped|"
    r"raper|raping|rapist|rautenberg|rearend|rearentry|recktum|rectum|redlight|"
    r"redneck|reefer|reestie|refugee|reject|remains|rentafuck|reprobate|republican|"
    r"rere|retard|retarded|ribbed|rigger|rimjob|rimming|roach|robber|roundeye|rump|"
    r"ruski|russia|russki|russkie|russky|sad|sadis|sadism|sadist|sadom|sambo|"
    r"samckdaddy|sandm|sandnigger|satan|satisfying|scag|scallywag|scank|scat|"
    r"schaffer|scheiss|schlampe|schlong|schmuck|scientology|scissoring|screw|"
    r"screwing|screwyou|scrotum|scum|semen|seppo|servant|sex|sexed|sexfarm|"
    r"sexhound|sexhouse|sexing|sexkitten|sexo|sexpot|sexslave|sextogo|sextoy|"
    r"sextoys|sexual|sexually|sexwhore|sexx|sexxx|sexy|sexymoma|shag|shaggin|"
    r"shagging|sharmuta|sharmute|shat|shav|shawtypimp|shebnon|sheeney|shemale|"
    r"shhit|shibari|shinola|shipal|shit|shitbird|shitbiscuit|shitburger|shitcan|"
    r"shitdick|shite|shiteater|shited|shitface|shitfaced|shitfit|shitforbrains|"
    r"shitfuck|shitfucker|shitfull|shithapens|shithappens|shithead|shitheaded|"
    r"shitheads|shithouse|shiting|shitlist|shitload|shitlord|shitmobile|shitola|"
    r"shitoutofluck|shits|shitstain|shitstorm|shitt|shitted|shitter|shitting|shitty|"
    r"shitty|shity|shitz|shiz|shizz|shlong|shoot|shooting|shortfuck|shota|showtime|"
    r"shrimping|sht|shyt|shyte|shytty|sick|sin|sissy|sixsixsix|sixtynine|sixtyniner|"
    r"skanck|skank|skankbitch|skankee|skankey|skankfuck|skanks|skankwhore|skanky|"
    r"skankybitch|skankywhore|skinflute|skribsex|skullfuck|skum|skumbag|slant|"
    r"slanteye|slapper|slaughter|slav|slave|slavedriver|slavery|sleezebag|sleezeball|"
    r"slideitin|slime|slimeball|slimebucket|slopehead|slopey|slopy|slut|sluts|slutt|"
    r"slutting|slutty|slutwear|slutwhore|slutz|smack|smackthemonkey|smegma|smut|"
    r"snatch|snatchpatch|snigger|sniggered|sniggering|sniggers|sniper|snot|snowback|"
    r"snowballing|snownigger|sob|sodom|sodomise|sodomite|sodomize|sodomy|sonofabitch|"
    r"sonofbitch|sonovabitch|sooty|sos|soviet|spaghetti|spaghettibender|"
    r"spaghettinigger|spank|spankthemonkey|spearchucker|sperm|spermacide|spermbag|"
    r"spermhearder|spermherder|spic|spick|spig|spigotty|spik|spit|spitter|splittail|"
    r"splooge|spooge|spork|sporking|spreadeagle|spunk|spunky|squaw|stagg|starfucker|"
    r"stiffy|strapon|strappado|strfkr|stringer|stripclub|stroke|stroking|stupid|"
    r"stupidfuck|stupidfucker|suck|suckdick|sucker|suckme|suckmyass|suckmydick|suckmytit|"
    r"suckoff|sucks|suicide|sumbitch|supernigger|swallow|swallower|swalow|swastika|"
    r"sweden|sweetness|swinger|sx|syphilis|taboo|tadger|taff|taig|tallywhacker|tampon|"
    r"tang|tantra|tarbaby|tarbrush|tard|teasing|teat|teets|teez|terror|terrorist|teste|"
    r"testical|testicle|testicles|thicklips|thirdeye|thirdleg|threesome|threeway|"
    r"throating|thundercunt|timbernigger|tinkle|tit|titbitnipply|titfuck|titfucker|"
    r"titfuckin|titjob|titlicker|titlover|tits|titt|tittie|titties|titty|tnt|todger|"
    r"toilet|tongethruster|tongue|tonguethrust|tonguetramp|topless|tortur|torture|"
    r"tossa|tossbag|tosser|towelhead|trailertrash|tramp|trannie|tranny|transexual|"
    r"transsexual|transvestite|tribadism|triplex|trisexual|trojan|trolling|trots|"
    r"trump|tubgirl|tuckahoe|tunneloflove|turd|turnon|tushy|twat|twatlips|twats|"
    r"twink|twinkie|twobitwhore|twot|twunt|uck|uk|undressing|unfuckable|unicorns|"
    r"upskirt|uptheass|upthebutt|urinary|urinate|urine|urophilia|usa|usama|uterus|"
    r"vadge|vagiina|vagina|vaginal|vagjuice|vajazzle|vajina|vatican|vibr|vibrater|"
    r"vibrator|vietcong|violence|virgin|virginbreaker|vjayjay|vomit|vorarephilia|"
    r"voyeur|vullva|vulva|wab|wank|wanker|wanking|waysted|weapon|weenie|weewee|"
    r"welcher|welfare|wetb|wetbac|wetback|wetbacks|wetblack|wetblacks|wetspot|"
    r"whacker|whash|whigger|whiskey|whiskeydick|whiskydick|whit|whitenigger|"
    r"whites|whitetrash|whitey|whiz|whoar|whop|whore|whoreahole|whorefucker|"
    r"whorehouse|wigga|wigger|willie|williewanker|willy|wn|wog|wogs|woofter|"
    r"wop|wtf|wuss|wuzzie|xrated|xtc|xx|xxx|yankee|yaoi|yellowman|yidabbo|"
    r"yiffy|zigabo|zipperhead|zoophiliasex"
)
