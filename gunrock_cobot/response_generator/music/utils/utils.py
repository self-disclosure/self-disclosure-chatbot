import logging

from nlu.constants import DialogAct
from response_generator.fsm2 import Tracker

logger = logging.getLogger(__name__)


def is_open_question(tracker: Tracker):
    return tracker.returnnlp.has_dialog_act({
        DialogAct.OPEN_QUESTION,
        DialogAct.OPEN_QUESTION_FACTUAL,
        DialogAct.OPEN_QUESTION_OPINION,
        DialogAct.YES_NO_QUESTION,
    })
