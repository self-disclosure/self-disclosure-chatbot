import re
from typing import Callable, List, Optional

from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, FSMLogger
from response_generator.fsm2.events import Event, NextState
from template_manager import Template

from nlu.command_detector import CommandDetector
from nlu.question_detector import QuestionDetector
from question_handling.quetion_handler import QuestionHandler, QuestionResponse, QuestionResponseTag

from response_generator.music import states
from response_generator.music.utils import entity, regex_match


logger = FSMLogger('MUSIC', 'CommandQuestionHandler', 'music.utils.command_question_handler')
BACKSTORY_THRESHOLD = 0.85


class CommandQuestionHandler:

    def __init__(self, tracker: Tracker):
        self.command_detector = CommandDetector(
            tracker.input_text, tracker.input_data['returnnlp']
        )
        self.question_detector = QuestionDetector(
            tracker.input_text, tracker.input_data['returnnlp']
        )
        self.question_handler = QuestionHandler(
            tracker.input_text, tracker.input_data['returnnlp'],
            backstory_threshold=BACKSTORY_THRESHOLD,
            system_ack=tracker.input_data['system_acknowledgement'],
            user_attributes_ref=tracker.user_attributes.ua_ref
        )

    def handler_used_blender(self, tracker: Tracker):
        return tracker.volatile_store.get('hander_used_blender') or False

    # MARK: - Default Command Handling Logic

    def handle_request_jumpout(self):
        pass

    def handle_request_tellmore(self):
        pass

    # MARK: - Default detector functions

    def is_do_you_like_question(
        self, tracker: Tracker,
        check_artists: Callable[[], Optional[List[entity.ArtistItem]]],
        check_genres: Callable[[], Optional[List[entity.GenreItem]]],
    ):
        return (
            re.search(r"do you (like|love)", tracker.returnnlp[-1].text) and
            len(tracker.returnnlp) >= 1 and
            (
                bool(check_artists()) or
                bool(check_genres())
            )
        )

    def is_whos_your_favorite_question(
        self, tracker: Tracker,
        check_genres: Callable[[], Optional[List[entity.GenreItem]]],
    ):
        return (
            (
                re.search(r"who('s| is) your favorite", tracker.input_text) and
                len(tracker.returnnlp) >= 1 and
                (
                    bool(check_genres())
                )
            ) or
            (
                re.search(r"who('s| is) your favorite (\w+)? ?artist", tracker.input_text) and
                len(tracker.returnnlp) >= 1

            )
        )
    # MARK: - API

    def handle_command(
        self, *,
        handle_request_jumpout=None,
        handle_request_tellmore=None
    ):
        # patch to only handle question once per turn
        if self.tracker.volatile_store.get('handle_command_is_handled', False):
            return None
        self.tracker.volatile_store['handle_command_is_handled'] = True

        if self.command_detector.is_request_jumpout():
            return (handle_request_jumpout or self.handle_request_jumpout)()
        elif self.command_detector.is_request_tellmore():
            return (handle_request_tellmore or self.handle_request_tellmore)()
        else:
            return

    def has_question_only(
        self, tracker: Tracker, *,
        is_do_you_like_question: Callable[[], bool] = None,
    ):
        return (
            len(tracker.returnnlp) <= 1 and
            self.has_question(tracker, is_do_you_like_question=is_do_you_like_question)
        )

    def has_question(
        self, tracker: Tracker, *,
        is_question: List[Callable[[], bool]] = None,
        is_do_you_like_question: Callable[[], bool] = None,
        is_whos_your_favorite_question: Callable[[], bool] = None,
    ):
        flag = tracker.volatile_store.get('__handler_has_question_handled__') or False

        def check_artists():
            artists = entity.ArtistItem.detect(
                tracker, rnlp_slice=slice(len(tracker.returnnlp) - 1, len(tracker.returnnlp)))
            return artists

        def check_genres():
            # TODO: add slice
            genres = entity.GenreItem.detect(
                tracker, rnlp_slice=slice(len(tracker.returnnlp) - 1, len(tracker.returnnlp)))
            return genres

        cond = [
            tracker.returnnlp[-1].text,
            re.search(r"who('s| is) your favorite", tracker.input_text),
            check_genres(),
            re.search(r"who('s| is) your favorite (\w+)? ?artist", tracker.input_text),
        ]

        logger.debug(f"cond = {cond}")

        def generator(q):
            try:
                return q()
            except Exception as e:
                logger.warning(e)
                return False

        if (
            flag or
            regex_match.is_user_sing_us_a_song(tracker)
        ):
            return False
        else:
            ret = (
                self.question_detector.has_question() or
                (is_question and any(generator(q) for q in is_question)) or
                (
                    is_do_you_like_question() if is_do_you_like_question is not None else
                    self.is_do_you_like_question(tracker, check_artists, check_genres)
                ) or
                (
                    is_whos_your_favorite_question() if is_whos_your_favorite_question is not None else
                    self.is_whos_your_favorite_question(tracker, check_genres)
                )
            )
            logger.debug(f"has_question = {self.question_detector.has_question()}")
            logger.debug(f"has_question check: text = {tracker.input_text}, value = {ret}")
            return ret

    def handle_question(
        self, dispatcher: Dispatcher, tracker: Tracker,
        *,
        # required handler
        ask_back_handler: Callable[[QuestionResponse], Optional[List[Event]]],
        follow_up_handler: Callable[[QuestionResponse], Optional[List[Event]]],
        # optional handler
        override_handler: Callable[[QuestionResponse], Optional[List[Event]]] = None,
        elif_handler: Callable[[QuestionResponse], Optional[List[Event]]] = None,
        do_you_like_artist_handler: Callable[[QuestionResponse, List[entity.ArtistItem]], Optional[List[Event]]] = None,
        do_you_like_genre_handler: Callable[[QuestionResponse, List[entity.GenreItem]], Optional[List[Event]]] = None,
        do_you_like_handler: Callable[[QuestionResponse], Optional[List[Event]]] = None,
        whos_your_favorite_handler: Callable[[QuestionResponse, List[entity.GenreItem]], Optional[List[Event]]] = None,
        # optional detector
        is_elif_question: Callable[[], bool] = None,
        is_ask_back_question: Callable[[], bool] = None,
        is_follow_up_question: Callable[[], bool] = None,
        is_do_you_like_question: Callable[[], bool] = None,
        is_whos_your_favorite_question: Callable[[], bool] = None,
        # optional utils
        ignore_idk_response: Callable[[QuestionResponse], bool] = None,
        skip_blender_response: Callable[[QuestionResponse], bool] = None,
        inspect_question_response: Callable[[QuestionResponse], None] = None,
    ) -> Optional[List[Event]]:

        tracker.volatile_store['__handler_has_question_handled__'] = True

        question_response = self.question_handler.handle_question()
        logger.debug(
            f"question_resposne = {question_response}, "
            f"ask_back = {self.question_detector.is_ask_back_question()}, "
            f"follow_up = {self.question_detector.is_follow_up_question()}, "
        )

        artists = None
        genres = None

        def check_artists():
            nonlocal artists
            artists = entity.ArtistItem.detect(
                tracker, rnlp_slice=slice(len(tracker.returnnlp) - 1, len(tracker.returnnlp)))
            return artists

        def check_genres():
            # TODO: add slice
            nonlocal genres
            genres = entity.GenreItem.detect(
                tracker, rnlp_slice=slice(len(tracker.returnnlp) - 1, len(tracker.returnnlp)))
            return genres

        if question_response and question_response.tag is QuestionResponseTag.BLENDER:
            tracker.volatile_store['handler_used_blender'] = True

        if inspect_question_response:
            inspect_question_response(question_response)

        if override_handler:
            logger.debug(f"override_handler")
            return override_handler(question_response)

        elif (
            is_ask_back_question() if is_ask_back_question is not None else
            (
                self.question_detector.is_ask_back_question() or
                re.search(r"do you$", tracker.returnnlp[-1].text)
            )
        ):
            logger.debug(f"ask_back_handler, custom = {bool(is_ask_back_question)}")
            return ask_back_handler(question_response)

        elif (
            is_follow_up_question() if is_follow_up_question is not None else
            self.question_detector.is_follow_up_question()
        ):
            logger.debug(f"follow_up_qustion, custom = {bool(is_follow_up_question)}")
            return follow_up_handler(question_response)

        elif (
            is_do_you_like_question() if is_do_you_like_question is not None else
            self.is_do_you_like_question(tracker, check_artists, check_genres)
        ):
            if artists:
                if do_you_like_artist_handler:
                    return do_you_like_artist_handler(question_response, artists)
                else:
                    dispatcher.respond("Personally, I like all artists.")
                    return

            elif genres:
                if do_you_like_genre_handler:
                    return do_you_like_genre_handler(question_response, genres)
                else:
                    dispatcher.respond("Personally, I like all music genres.")
                    return
            elif do_you_like_handler:
                return do_you_like_handler(question_response)

        elif (
            is_whos_your_favorite_question() if is_whos_your_favorite_question is not None else
            self.is_whos_your_favorite_question(tracker, check_genres)
        ):
            if (
                genres or
                (
                    re.search(r"who('s| is) your favorite (\w+)? ?artist", tracker.input_text) and
                    len(tracker.returnnlp) >= 1
                )
            ):
                """e.g. who's your favorite jazz artist"""
                genre = next(iter(genres), None) if genres else None
                if whos_your_favorite_handler:
                    return whos_your_favorite_handler(question_response, genres or [])
                else:
                    utt = states.respond_list_my_favorite(genre)
                    dispatcher.respond(utt)
                    # TODO: check if proposed?
                    if genre:
                        states.FavoriteGenreArtistPropose.require_entity(tracker, genre)
                        return [NextState(states.FavoriteGenreArtistPropose.name, jump=True)]
                    else:
                        states.FavArtistEnter.increment_counter(tracker)
                        return [NextState(states.FavArtistPropose.name, jump=True)]

        elif (
            is_elif_question is not None and elif_handler is not None and
            is_elif_question()
        ):
            logger.debug(f"elif_handler")
            return elif_handler(question_response)

        elif (
            question_response and question_response.bot_propose_module and
            question_response.bot_propose_module != TopicModule.MUSIC
        ):
            logger.debug(f"default")
            dispatcher.respond(question_response.response)
            dispatcher.respond_template(
                f"propose_topic_short/{question_response.bot_propose_module.value}", {},
                template=Template.transition)

            dispatcher.propose_continue('UNCLEAR', question_response.bot_propose_module)
            return [NextState(states.ProposeUnclearRespond.name)]

        elif (
            skip_blender_response and skip_blender_response(question_response) and
            question_response.tag is QuestionResponseTag.BLENDER
        ):
            logger.debug(f"skipping blender response: {question_response}")
            return None

        elif (
            not (
                ignore_idk_response and ignore_idk_response(question_response) and
                question_response.tag is QuestionResponseTag.IDK
            )
        ):
            logger.debug(f"default")
            dispatcher.respond(question_response.response)
            return None
