import logging
import itertools
import re
from dataclasses import dataclass
from functools import reduce
from typing import Dict, Iterable, Iterator, List, Optional, Tuple, Type

from util_redis import RedisHelper
from nlu.dataclass import ReturnNLP, ReturnNLPSegment

from response_generator.fsm2 import Tracker
from response_generator.fsm2.utils.detector import EntityItem, Detector

from response_generator.music.constants import genre_100
from response_generator.music.utils import dynamodb, regex_match
from response_generator.music.utils.music_ner import MusicNERResponse, fetch_music_ner


logger = logging.getLogger('music.utils.entity')


def cachable(func, *, key: str = None):

    def inner(cls, tracker: Tracker, *args, **kwargs):
        logger.debug(f"[ENTITY] cachable: {args}, {kwargs}")
        _key = f"{cls.__name__}" + ("" if not key else f".{key}")
        if tracker.volatile_store.get(_key) is None:
            tracker.volatile_store[_key] = func(cls, tracker, *args, **kwargs)
        ret = tracker.volatile_store.get(_key, [])
        logger.debug(f"[CACHE] tracker: {repr(tracker)}")
        return ret

    return inner


def save(entity: EntityItem, tracker: Tracker):
    if not tracker.persistent_store.get('mentioned_entities'):
        tracker.persistent_store['mentioned_entities'] = {}
    tracker.persistent_store['mentioned_entities'][entity.__class__.__name__] = \
        (tracker.persistent_store['mentioned_entities'].get(entity.__class__.__name__) or []) + [entity]


def recall(entity: Type[EntityItem], tracker: Tracker):
    entities = (tracker.persistent_store['mentioned_entities'].get(entity.__class__.__name__) or [])
    if entities:
        return entities[-1]


class ArtistItem(EntityItem):

    @classmethod
    def canonical_map(cls) -> dict:
        return {
            'bts': r"\bb.? ?t.? ?s.?\b",
        }

    @classmethod
    def default_description(cls) -> str:
        return 'artist ner'

    @classmethod
    def from_ner_people(cls, people: MusicNERResponse.People, positivity=True) -> 'ArtistItem':
        return cls(
            positivity=positivity,
            noun=people.name,
            description=cls.default_description(),
            confidence=10000.0)

    @classmethod
    def whiteblacklist(cls) -> dict:
        return dict(
            description=dict(
                whitelist=r"\b(american singer|artist|rapper|composer)\b",
            ),
            noun=dict(
                blacklist=(
                    r"popular music"
                ),
            ),
        )

    def __hash__(self):
        return hash(self.noun)

    def __eq__(self, other):
        if isinstance(other, str):
            return self.is_equal_to_name_str(other)
        else:
            return super().__eq__(other)

    def is_equal_to_name_str(self, name: str):
        name = name.lower()
        noun = self.noun.lower()
        for prefix in ('the', 'a'):
            if noun.startswith(prefix + " "):
                return noun[len(prefix):].strip() in name
        return noun in name

    @classmethod
    # @cachable
    def detect(
        cls, tracker: Tracker, *,
        include_negative_positivity=False,
        rnlp_slice: slice = None
    ) -> List['ArtistItem']:
        if not rnlp_slice:
            use_input_text = True
            rnlp_slice = slice(len(tracker.returnnlp))
        else:
            use_input_text = False

        def from_music_ner():
            positivity = not tracker.returnnlp[rnlp_slice].has_intent({'ans_dislike'})
            logger.debug(f"positivity = {positivity}, include_neg = {include_negative_positivity}")
            if not positivity and not include_negative_positivity:
                logger.debug(f"returning empty iter")
                return iter(())
            music_ner = fetch_music_ner(
                tracker.input_text if use_input_text else " ".join(tracker.returnnlp[rnlp_slice].text),
                tracker.last_bot_response(ssml=False))
            logger.debug(f"music_ner = {music_ner}")
            return (
                cls.from_ner_people(people, positivity=positivity)
                for people in (music_ner.people if music_ner else [])
            )

        def return_filter(artist: 'ArtistItem'):
            ret = artist.is_equal_to_name_str(
                tracker.input_text if use_input_text else " ".join(tracker.returnnlp[rnlp_slice].text))
            logger.debug(f"return filter for {artist}, keep = {ret}")
            return ret

        return Detector(
            cls,
            extra_entities=lambda: (
                artist for artist in from_music_ner()
                if artist.is_equal_to_name_str(" ".join(tracker.returnnlp[rnlp_slice].text))
            ),
        )(tracker, include_negative_positivity=include_negative_positivity, rnlp_slice=rnlp_slice)

    def artist_details(self, tracker: Tracker) -> Optional[dynamodb.ArtistDetails]:
        return cachable(
            lambda cls, tracker: dynamodb.get_artist_details(self.noun),
            key=self.noun
        )(self.__class__, tracker)

    def songs_from_db(self, tracker: Tracker) -> List[str]:
        return cachable(
            lambda cls, tracker: dynamodb.get_songs_by_artist(self.noun.lower()),
            key=self.noun
        )(self.__class__, tracker)


@dataclass(frozen=True)
class SongItem(EntityItem):

    artist_name: str = ""

    @classmethod
    def default_description(cls) -> str:
        return 'song ner'

    @classmethod
    def from_ner_song(cls, song: MusicNERResponse.Song) -> 'SongItem':
        return cls(noun=song.song_name,
                   description=cls.default_description(),
                   confidence=10000.0,
                   artist_name=song.artist_name)

    @classmethod
    @cachable
    def detect(cls, tracker: Tracker) -> List['SongItem']:
        # last_bot_utt = tracker.last_utt_store.get('last_bot_utt', '')
        if regex_match.is_false_songs(tracker):
            return []
        music_ner = fetch_music_ner(tracker.input_text, tracker.last_bot_response(ssml=False))
        return [cls.from_ner_song(song) for song in (music_ner.songs if music_ner else [])]


class InstrumentItem(EntityItem):

    @classmethod
    def canonical_map(cls) -> Dict[str, str]:
        return {
            'guitar': r"\b(guitar)\b",
            'piano': r"\b(piano)\b",
        }

    @classmethod
    def default_description(cls) -> str:
        return 'musical instrument'

    @classmethod
    def whiteblacklist(cls) -> dict:
        return dict(
            description=dict(
                whitelist=r"\b((musical ?)?instruments?)\b",
            ),
        )


class GenreItem(EntityItem):

    @classmethod
    def genre_100(cls) -> Iterator[Tuple[str, str]]:
        for genre in genre_100:
            yield genre['name'], r"\b({})\b".format(genre['regex'].pattern)

    @classmethod
    def canonical_map(cls) -> Dict[str, str]:
        return {
            'pop': r"\b(pop|popular music)\b",
            'rnb': r"\b(r and b|r.? ?(n.?|and) ?b.?)\b",
            'irish': r"\b(irish( tunes?| songs?)?)\b",
            'gaelic': r"\b(gaelic( tunes?| songs?)?)\b",
            'scottish': r"\b(scottish( tunes?| songs?)?)\b",
        }

    @classmethod
    def default_description(cls) -> str:
        return 'music genre'

    @classmethod
    def whiteblacklist(cls) -> dict:
        return dict(
            description=dict(
                whitelist=r"\b(musical style)\b",
            ),
        )

    def is_equal_to_name_str(self, name: str):
        return name in self.noun

    @classmethod
    def from_regex_match(cls, noun: str, canonical: str, positivity=True):
        return cls(
            positivity=positivity,
            noun=re.sub(r"\.", "", noun.lower()),
            canonical=canonical,
            description=cls.default_description(), confidence=1000000.0)

    @classmethod
    def detect(
        cls, tracker: Tracker, *,
        include_negative_positivity=False,
        rnlp_slice: slice = None,
    ) -> List['GenreItem']:

        if not rnlp_slice:
            rnlp_slice = slice(len(tracker.returnnlp))

        redis_helper = RedisHelper()
        redis_substitute = r"\b(music|songs?)\b"

        def concept_validator(rnlp: ReturnNLP) -> Iterable['GenreItem']:
            for segment in rnlp:
                segment: ReturnNLPSegment
                positivity = not segment.has_intent({'ans_dislike'})
                if not positivity and not include_negative_positivity:
                    continue
                for concept in segment.concept:
                    filtered_cds = (
                        cd for cd in concept.data
                        if ('genre' in cd.description and
                            redis_helper.is_known_genre(re.sub(redis_substitute, "", concept.noun).strip()))
                    )
                    try:
                        concept_data = reduce(
                            lambda prev, cd: cd if cd.confidence > prev.confidence else prev,
                            filtered_cds)
                        yield cls.from_conceptdata(concept.noun, concept_data, positivity=positivity)
                    except TypeError:
                        pass

        def googlekg_validator(rnlp: ReturnNLP) -> Iterable['GenreItem']:
            for segment in rnlp:
                segment: ReturnNLPSegment
                positivity = not segment.has_intent({'ans_dislike'})
                if not positivity and not include_negative_positivity:
                    continue
                for kg in segment.googlekg:
                    if redis_helper.is_known_genre(re.sub(redis_substitute, "", kg.name).strip()):
                        yield cls.from_googlekg(kg, positivity=positivity)

        def extra_entities() -> Iterable['GenreItem']:
            def genre_100_check():
                for noun, regex in cls.genre_100():
                    # logger.debug(f"[GENRE] genre_100: {(noun, regex)}")
                    for segment in tracker.returnnlp[rnlp_slice]:
                        segment: ReturnNLPSegment
                        positivity = not segment.has_intent({'ans_dislike'})
                        if not positivity and not include_negative_positivity:
                            continue
                        match = re.search(regex, segment.text)
                        if match:
                            yield cls.from_regex_match(
                                noun=match.group().lower(), canonical=noun, positivity=positivity)

            def noun_phrase_redis_check():
                for segment in tracker.returnnlp[rnlp_slice]:
                    segment: ReturnNLPSegment
                    positivity = not segment.has_intent({'ans_dislike'})
                    if not positivity and not include_negative_positivity:
                        continue
                    for nps in segment.noun_phrase:
                        for np in nps:
                            np = re.sub(redis_substitute, "", np, re.I).strip()
                            if redis_helper.is_known_genre(np):
                                yield cls.from_regex_match(noun=np, canonical=None, positivity=positivity)

            return itertools.chain(genre_100_check(), noun_phrase_redis_check())

        return Detector(
            cls,
            concept_validator=concept_validator,
            googlekg_validator=googlekg_validator,
            extra_entities=extra_entities,
        )(tracker, include_negative_positivity=include_negative_positivity, rnlp_slice=rnlp_slice)
