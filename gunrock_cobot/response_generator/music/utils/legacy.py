import json
import logging
import random
import re
import requests
import warnings
from typing import List

from fuzzywuzzy import fuzz
import boto3
from boto3.dynamodb.conditions import Key

from cobot_common.service_client import get_client
from constants_api_keys import COBOT_API_KEY

from util_redis import RedisHelper

from ..constants import (
    ignore_entity, special_artists, rapport_match,
    karaoke_songs, genre_100
)


logger = logging.getLogger("music.utils.legacy")
redis_helper = RedisHelper()
dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
table = dynamodb.Table('MusicArtists')
table1 = dynamodb.Table('music_songs')


# MARK: ARTIST DETECTION

def detect_artist(text: str,
                  ner=None, phrases=None, concept=None, API=False):
    warnings.warn("use utils.entity", DeprecationWarning, stacklevel=2)
    """
    find a canonical artist name from user utterance.

    Arguments:
        text {str} -- user utterance or text to be detected
        ner {Any} -- old nlu output of 'ner' from 'features'
        phrases {list} -- googlekg in old form (a size-4 vector)
        concept {Any} -- returnnlp concept in old form
        API {bool} -- bool flag to allow search online
    """
    artist = None
    if API:
        url = f"http://musicbrainz.org/ws/2/artist/?query={text}&fmt=json"
        try:
            response = requests.get(url, timeout=.6)
            data = json.loads(response.content)
            if 'artists' in data:
                if len(data['artists']) > 1:
                    artist = data['artists'][0]['name'].lower()
                    if artist_check(artist, text):
                        return artist
        except Exception as e:
            logging.error("[MUSIC] API error: {}".format(e), exc_info=True)

    if concept:
        for item in concept:
            if 'data' in item:
                if 'genre' in item['data']:
                    continue
                if any(i in item['data'] for i in {'artist', 'musician', 'band', 'singer', 'american artist'}):
                    if artist_check(item['noun'].lower(), text):
                        return item['noun'].lower()
    if phrases is not None and len(phrases) > 0:
        for phrase in phrases:
            if len(phrase) > 3 and phrase[3] == 'music' and float(phrase[2]) > 600.0:
                if re.search(ignore_entity, phrase[0]):
                    continue
                else:
                    search = query_db(phrase[0].lower())
                    if search and len(search) > 0:
                        artist = search[0]
                        if artist_check(phrase[0].lower(), text):
                            return phrase[0].lower()
    if ner:
        for entity in ner:
            if entity['label'] == 'PERSON':
                artist = entity['text']
                search = query_db(artist.lower())
                if search and len(search) > 0:
                    if artist_check(entity['text'].lower(), text):
                        return entity['text'].lower()
    return artist


def artist_check(artist, text):
    items = artist.split(" ")
    for item in items:
        if item in text:
            return True
    return False


# MARK: INTENT / GOOGLEKG / LEXICAL_INTENT

# TAG: UNUSED BY GT
def check_music_knowledge(phrases):
    if phrases is not None and len(phrases) > 0:
        for phrase in phrases:
            if phrase[3] == 'music':
                if re.search(r'my favorite', phrase[0]):
                    continue
                else:
                    return True
    return False


def detect_intent(text):
    """
    Detect intents
    TODO: instead of if elif hierarchy, try all regex and add to dictionary successful matches
    """
    # System level: when this is returned to state response function, start
    # new dialogue flow
    if re.search((r"(stop|don't|do not).* (talk|discuss|tell.* me).* book|\bi\b (hate|don't|do not|never).* "
                  r"(read|like.* book)|(talk|discuss|tell.* me).* about.* something else"), text):
        return "exit_musicRG"

    # Lexical level: narrow to broad
    elif re.search(r"yes|yeah|sure|\bi\b (\bdo\b|\bam\b|\bwant to\b)", text):
        return "answer_yes"
    # TODO: add not really
    elif re.search(r"\bno\b|\bnope\b|(don't|do not) (like|enjoy|want)", text):
        return "answer_no"
    # TODO: add don't know
    # asking question at beginning of utterance
    elif re.search((r"(\bi\b|\bi'm\b).* (not.* sure|unsure|uncertain| (don't|do not| don't know).* know|"
                    r"(don't|do not).* have)"), text):
        return "answer_uncertain"
    return None


# TAG: UNUSED BY GT
def handle_sys_intent(input, intent, tm_music):
    if intent == "exit_bookRG":
        return {
            "next": "s_exitbookRG",
            "response": tm_music.speak('sys_intent_prompt', {}),
            "context": input["currentstate"],
            "syscode":
                "exit"  # cobot can potentially use this to overwrite reponse or whatever
        }
    elif intent == "ask_recommend":
        text = input["text"]
        # concept = input["concept"]
        if input["coreference"] is not None:
            text = input["coreference"]["text"]
        ner = input["nlu"]["ner"]
        artist = detect_artist(text, ner)
        # begin = ""
        redditdata = ""
        # alternate = False
        if redditdata is None:
            redditdata = "That's great! "
        if artist is None:
            artist = "Katy Perry"
            # alternate = True
    return None


# MARK: - Utils

def isEnglish(s):
    return is_english(s)


def is_english(s: str):
    try:
        s.encode(encoding='utf-8').decode('ascii')
    except UnicodeDecodeError:
        return False
    else:
        return True


def get_chitchat_ack(input, chitchat, tm_music):
    text = input['text']
    lexical = input["nlu"]["lexical"]
    knowledge = input["knowledge"]
    concept = input["concept"]
    keyword = chitchat["keyword"]
    topic_keyword = input["topic_keyword"]
    noun_phrases = input["local_noun_phrase"]

    if keyword == 'important':
        if re.search(r'not important|not | not really', text):
            return tm_music.speak('important/not_important', {}), keyword
        if re.search(r'very important|really important|most important|important|love music|very', text):
            return tm_music.speak('important/important', {}), keyword

    if keyword == 'remove_music':
        if re.search((r"sad|depressed|lonely|impossible|said|horrible|bad|devastated|awful|terrible|go insane|"
                      r"upset|heart broken|not so good|dead inside|alone|die|cry|angry"), text):
            return tm_music.speak('remove_music/sad', {}), keyword
        if re.search(r'good|happy|not feel anything', text):
            return tm_music.speak('remove_music/happy', {}), keyword

    if keyword == 'least_fav_music':
        genre = detect_genre(topic_keyword, noun_phrases, concept, text)
        if genre is not None:
            return tm_music.speak('least_fav_music/present', {'genre': genre}), keyword

    if keyword == 'when_music':
        if re.search((r"mornings | evenings| gym| driving| bored| working| shower| drive| mood| feel like it|"
                      r"free time|night|about a week|sometimes|sad|not so much|in the car"), text):
            return tm_music.speak('when_music/not_music_fan', {}), keyword
        if re.search(r'all the time|most of the time|all day|every day|everyday|most of the time', text):
            return tm_music.speak('when_music/music_fan', {}), keyword

    if keyword == 'how_music':
        if re.search((r"three days| thirty minutes|a little bit|five minutes|two hours|couple hours|minutes|"
                      r"sometimes|not much"), text):
            return tm_music.speak('how_music/not_very_much', {}), keyword
        if re.search((r"twenty four hours|a lot| music as possible| all my time| everyday|all the time|"
                      r"most of the time|extremely|every day|every single day|every single day|"
                      r"twenty four seven|everyday|pretty often"), text):
            return tm_music.speak('how_music/very_much', {}), keyword

    if keyword == 'emotional':
        song = detect_song(concept, knowledge)
        if song is not None:
            return tm_music.speak('emotional/present', {'song': song}), keyword

    if keyword == 'instrument':
        instrument = detect_instrument(concept, knowledge)
        if instrument is not None:
            if instrument == 'ukelele':
                return tm_music.speak('rapport_match', {}), keyword
            else:
                return tm_music.speak('instrument/present', {'instrument': instrument}), keyword

    if keyword == 'song_again':
        song = detect_song(concept, knowledge)
        if song is not None and not re.search(r'every single time', text):
            return tm_music.speak('song_again/present', {'song': song}), keyword
        if 'ans_pos' in lexical or re.search(r"all the time| i've done that|absolutely", text):
            return tm_music.speak('song_again/not_present_yes', {}), keyword
        if 'ans_neg' in lexical:
            return tm_music.speak('song_again/not_present_no', {}), keyword

    if keyword == 'karaoke':
        if re.search(r'i wanna dance with somebody', text):
            return tm_music.speak('rapport_match', {}), keyword
        song = detect_song(concept, knowledge)
        if song is not None:
            if not re.search(r'the name|something|a song|the song', song):
                return tm_music.speak('karaoke/present', {'song': song}), keyword
        else:
            for k_song in karaoke_songs:
                if re.search(k_song['regex'], text):
                    song = k_song['name']
                    return tm_music.speak('karaoke/present', {'song': song}), keyword
        if 'ans_neg' in lexical:
            return tm_music.speak('karaoke/neg_response', {}), 'ans_neg'
        if 'ans_unknown' in lexical:
            return tm_music.speak('unknown_ack', {}), 'unknown_ack'

    if re.search(rapport_match, text):
        return tm_music.speak('rapport_match', {}), 'rapport_match'

    if 'ans_unknown' in lexical:
        return tm_music.speak('unknown_ack', {}), 'unknown_ack'

    if len(text.split(" ")) > 5:
        return tm_music.speak('long_sentences_ack', {}), 'long_sentences_ack'
    else:
        return tm_music.speak('ack_general', {}), 'ack_general'


# MARK: Detect stuff

def detect_instrument(concept=None, phrases=None):
    warnings.warn("Use utils.entity", DeprecationWarning, stacklevel=2)
    if concept:
        for item in concept:
            if 'data' in item:
                if 'instrument' in item['data']:
                    return item['noun']
    return None


def detect_song(concept=None, phrases=None):
    warnings.warn("Use utils.entity", DeprecationWarning, stacklevel=2)
    if concept:
        for item in concept:
            if 'data' in item:
                if 'song' in item['data'] and not re.search(r'the name|something|a song|the song|^hey$', item['noun']):
                    return item['noun']
    return None


def detect_genre(keywords, noun_phrases, concept, text=None):
    warnings.warn("use utils.entity", DeprecationWarning, stacklevel=2)

    logger.debug(f"[MUSIC UTIL LEGACY] detect_genre. "
                 f"keywords: {keywords}, noun_phrases: {noun_phrases}, concept: {concept}, text: {text}")
    if concept:
        for item in concept:
            if 'data' in item:
                logger.debug(f"[MUSIC UTIL LEGACY] detect_genre; concept; data in item. item: {item}")
                if 'genre' in item['data']:
                    logger.debug(
                        f"[MUSIC UTIL LEGACY] detect_genre; concept; redis:"
                        "noun: {}, result: {}".format(
                            re.sub(r"\b(music|songs?)\b", "", item['noun']),
                            redis_helper.is_known_genre(re.sub(r"\b(music|songs?)\b", "", item['noun']).strip())
                        ))
                    if redis_helper.is_known_genre(re.sub(r"\b(music|songs?)\b", "", item['noun']).strip()):
                        return item['noun']
                if re.search(r'country', item['noun']):
                    return item['noun']

    for noun_phrase in noun_phrases:
        logger.debug(
            f"[MUSIC UTIL LEGACY] detect_genre; noun_phrase; redis.is_known_genre: "
            "noun: {}, result: {}".format(
                re.sub(r"\b(music|songs?)\b", "", noun_phrase),
                redis_helper.is_known_genre(re.sub(r"\b(music|songs?)\b", "", noun_phrase).strip())
            ))
        if redis_helper.is_known_genre(re.sub(r"\b(music|songs?)\b", "", noun_phrase).strip()):
            return noun_phrase
    words = text.split(' ')
    logger.debug(f"[MUSIC UTIL LEGACY] detect_genre; word split: {words}")
    for word in words:
        for genre in genre_100:
            if re.search(genre['regex'], word):
                logger.debug(f"[MUSIC UTIL LEGACY] detect_genre; found in genre_100")
                return genre['name']
    return None


def get_music_entity(concept=None, phrases=None):
    if concept:
        for item in concept:
            if 'data' in item:
                if any(i in item['data'] for i in {'artist', 'musician', 'band', 'music', 'genre', 'song'}):
                    return item['noun']
    if phrases is not None and len(phrases) > 0:
        for phrase in phrases:
            if phrase[3] == 'music':
                if re.search(ignore_entity, phrase[0]):
                    continue
                else:
                    return phrase[0]
    return None


def identify_entity(keywords, noun_phrases):
    if len(keywords) > 0:
        if keywords != 'music':
            return keywords
    if len(noun_phrases) > 0:
        return noun_phrases[0]
    else:
        return None


# MARK: REGEX MATCHES

def play_music_match(utterance: str):
    """
    (Deprecated) regex to match the PLAY_MUSIC dialog act
    """
    warnings.warn("use utils.regex_match", DeprecationWarning, stacklevel=2)
    # remove PLAY_MUSIC check from music
    return None

    patterns = (r"play me|hear you|put it|play it|hear it|sing it|let's listen to| you play a sample|can you play|"
                r"turn the music up| play music|turn the music up|could you play|can we play|listen to it|"
                r"play some music|put on a song|i want to hear|i would like to hear")
    if re.search(r"i play it|i listen to it|i play music", utterance):
        return False
    match = re.search(patterns, utterance)
    return match


def concert_search(artist):
    url = 'https://api.seatgeek.com/2/events?client_id=MTI0NTE0NTd8MTUzMjgzMjk3Ni41Mw&performers.slug='
    # updated_url = url + artist.replace(' ', '-') + '&datetime_utc.gt=2018-07-28'
    response = requests.get(url, timeout=.6)
    concerts = json.loads(response.content.decode('utf-8'))
    concert = concerts['events'][0]
    concert_details = {}
    if 'title' in concert:
        concert_details['title'] = concert['title']
    if 'venue' in concert:
        concert_details['address'] = concert['venue']['city'] + ', ' + concert['venue']['state']
    if 'datetime_utc' in concert:
        concert_details['date'] = concert['datetime_utc']
    return concert_details


def exit_match(input):
    patterns = (r"can we talk | stop|can we chat|change topic| go away| go now| don't listen to music|"
                r"talk about something else|done chatting|quit|talk about something else|"
                r"don't want to talk about music|don't like music|hate music|turn off|"
                r"i'm not feeling too music|stop talking about music|change the conversation|"
                r"stop talking to me|change from social bot|do not like music|don't talk to me")
    match = re.search(patterns, input['text'])
    ner = input['nlu']['ner']
    concept = input['concept']
    knowledge = input['knowledge']
    if detect_artist(input['text'], ner, knowledge, concept) is None:
        return match
    else:
        return False


def repeat_match(input):
    warnings.warn("use utils.regex_match", DeprecationWarning, stacklevel=2)
    return any(input['text'] == i for i in {"what", "ask that question again", "sorry what", "say what"})


def unknown_match(utterance):
    """
    (Deprecated) User response i don't know or equivalent
    """
    warnings.warn("use utils.regex_match", DeprecationWarning, stacklevel=2)
    patterns = (r"don't know|don't|can't remember|none|don't really have one|not sure|too many artist|"
                r"no idea|don't have a favorite|lot of favorite songs|that's tough|all of them|nobody|"
                r"that's a tough one|can't think of")
    match = re.search(patterns, utterance)
    return match


def EVI_match(utterance):
    patterns = (r"what *.time|what day|what date|how old is|(what's|whats) (?!.*you|it|that|this|what|"
                r"w\bmy\b|next|going)|(what is|what's|whats) (?!.*you|it|that|this|what|\bmy\b|next|going)|"
                r"fact|when is (?!.*you)|when's (?!.*you)|where does|what about (?!.*you)|what does "
                r"(?!.*it|that|this|he|she)|today in history|in the world|who is (?!.*you|\bgood\b|in|he|she|"
                r"better|that|it|that|this|there|great)|how tall is the|^how rich|what does .* mean|"
                r"how high is|what.* highest|how many|who won |when are \bthe\b|how about the|who invented|"
                r"how big|how long|how tall|how .* can a|how fast is |wikipedia|plus|minus|divided|where is the|"
                r"how heavy|^define|what was the|who's|top hits|who")
    match = re.search(patterns, utterance)
    return match


def EVI_bot(utterance):  # TODO: get EVI response
    warnings.warn("use response_generator.common.evi.query", DeprecationWarning, stacklevel=2)
    try:
        client = get_client(api_key=COBOT_API_KEY)
        r = client.get_answer(question=utterance, timeout_in_millis=700)
        if r["response"] == "" or r["response"].startswith('skill://'):
            return None
        if re.search((r"an opinion on that|tough to explain|I didn’t get that.|Sorry, I didn’t catch that|"
                      r"Alexa Original|'favorite' is usually defined|is a plural form of|Hi there|Hi|"
                      r"Sorry, I can’t find the answer to the question I heard.|I don't have an opinion on that.|"
                      r"You could ask me about music or geography.|You can ask me anything you like.|"
                      r"I can answer questions about people, places and more.|^As a|I didn't get that|"
                      r"'Artist' is usually defined|The Place I Belong, the movie released|"
                      r"'Talking' is usually defined|usually defined as plural of what|sound is a vibration|"
                      r"something that would happen|Say is a town in southwest Niger|An album is a collection of|"
                      r"I love dancing|I pronounce that 'would'|I pronounce that 'do'|'Was' is related to a word|"
                      r"'Next' is usually defined|Dreams aren't well understood|\"Me Too\" or \"#MeToo\"|"
                      r"Yours, the chain of women's clothing outlets.|the computer application developed by Apricorn|"
                      r"The movie \"Instrument\"|A Little Bit is by Jessica Simpson|'S is by Jim Bianco"),
                     r["response"]):
            return None
        r["response"].replace("quot;", '')
        return r["response"] + " "
    except Exception:
        return None


def genre_match(text):
    warnings.warn("don't use", DeprecationWarning, stacklevel=2)
    f = open('genre_list.txt', 'r')
    genre = []
    for line in f:
        genre.append(line.strip('\n'))
    for item in genre:
        if text.lower() == item.lower():
            return True
    return False


# MARK: check stuff

def is_negative(dialog_acts, lexical, intent):
    for dialog_act in dialog_acts:
        if "DA" in dialog_act:
            if dialog_act["DA"] == "neg_answer":
                return True

    if 'ans_neg' in lexical:
        return True

    if intent == 'answer_no':
        return True

    return False


def is_open_question(dialog_acts: List[dict]):
    warnings.warn("Has migrated util function.", DeprecationWarning, stacklevel=2)
    for dialog_act in dialog_acts:
        if "DA" in dialog_act:
            if dialog_act["DA"] == "open_question":
                return True
            if dialog_act["DA"] == "open_question_factual":
                return True
            if dialog_act["DA"] == "open_question_opinion":
                return True
            if dialog_act["DA"] == "yes_no_question":
                return True

    return False


def replace_pronoun_in_artist_trivia(artist_name: str, trivia: str):
    pronouns = ["he", "she", "his", "her", "they", "their", "this"]
    possible = None
    for pronoun in pronouns:
        if pronoun in trivia.lower()[:len(pronoun)]:
            if possible:
                if len(pronoun) > len(possible):
                    possible = pronoun
            else:
                possible = pronoun
    if possible:
        trivia = trivia[len(possible) + 1:]
    artists = artist_name.split(" ")
    for t in artists:
        if t in trivia.lower()[:len(t)]:
            trivia = trivia[len(t) + 1:]

    return trivia


def generate_artist_response(artist, fact, tm_music):
    """return slots only if tm_music is None"""
    return tm_music.speak('artist_response', {'artist': artist, 'fact': replace_pronoun_in_artist_trivia(artist, fact)})


def query_db(name: str):
    """
    Returns table data for database/graph ID. Dynamo partitioned on graph
    id
    :param name (str): artist name
    :return: data
    """
    warnings.warn("use utils.dynamodb", DeprecationWarning, stacklevel=2)
    try:
        response = table.query(
            KeyConditionExpression=Key('name').eq(name)
        )
        if response:
            response = response['Items']
    except Exception as e:
        logging.error("[MUSIC] query db error: {}".format(e), exc_info=True)
        return None
    return response


def get_songs(name):
    try:
        response = table1.query(
            KeyConditionExpression=Key('name').eq(name)
        )
        if response:
            response = response['Items']
    except Exception as e:
        logging.error("[MUSIC] query db error: {}".format(e), exc_info=True)
        response = None
    if response:
        songs = response[0]['songs']
        if songs:
            sort = sorted(songs.items(), key=lambda x: x[1]['count'], reverse=True)
            if len(sort) > 1:
                if len(sort[0]) > 1:
                    return sort[0][0]
    return None


def get_all_songs(name):
    try:
        response = table1.query(
            KeyConditionExpression=Key('name').eq(name)
        )
        if response:
            response = response['Items']
    except Exception as e:
        logging.error("[MUSIC] query db error: {}".format(e), exc_info=True)
        response = None
    if response:
        songs = response[0]['songs']
        if songs:
            sort = sorted(songs.items(), key=lambda x: x[1]['count'], reverse=True)
            final_songs = []
            for item in sort:
                final_songs.append(item[0].lower())
            return final_songs[:100]
    return None


def asr_check(text, phrases, asr_correction):
    try:
        if asr_correction:
            updated_text = ''
            for key, corrected in asr_correction.items():
                score = 85
                for candidate in corrected:
                    check_score = fuzz.ratio(text.lower(), candidate.lower())
                    if check_score > score:
                        score = check_score
                        updated_text = candidate.lower()
                if updated_text == '':
                    for phrase in phrases:
                        for candidate in corrected:
                            check_score = fuzz.ratio(phrase.lower(), candidate.lower())
                            if check_score > score:
                                score = check_score
                                updated_text = candidate.lower()
            if updated_text != '':
                return updated_text
            else:
                return text
    except Exception as e:
        logging.error("[MUSIC] asr check error: {}".format(e), exc_info=True)
        return text


def check_special_artist(text):
    for artist in special_artists:
        if re.search(re.compile(artist), text):
            return special_artists[artist]
    return text


def handle_intent(text, intent):
    if intent == "ask_opinion" or intent == "ask_question":
        if re.search(r"you.* favorite.* book|you.* (like|love).* read", text):
            return ("My favorite book is the Great Gatsby. And I quote, So we beat on, boats against the current, "
                    "borne back ceaselessly into the past. ")
        elif re.search(r"favorite.* genre", text):
            return "I always liked science fiction, espcially stories about AI. "
        elif re.search(r"favorite.* character", text):
            return "Hal from 2001 A Space Odyssey is my favorite character. I'm not as scary though. "
        else:
            evidata = EVI_bot(text)
            if evidata:
                return evidata + " "
            return "Hm. I'm not sure how I feel about that. "

    return ""


def constructDynamicResponse(candidateResponses):
    if isinstance(candidateResponses, list):
        selectedResponse = random.choice(candidateResponses)
    else:
        selectedResponse = candidateResponses
    finalString = ""
    choicesBuffer = ""
    flag = False
    for char in selectedResponse:
        if char == "(":
            flag = True
        elif char == ")":
            flag = False
            choices = choicesBuffer.split("|")
            finalString += random.choice(choices)
            choicesBuffer = ""
        if flag and char != "(" and char != ")":
            choicesBuffer += char
        elif flag is False and char != "(" and char != ")":
            finalString += char
    finalString = " ".join(finalString.split())
    return finalString
