def has_profanity(text: str) -> bool:
    BLACKLIST = ["ass", "asshole", "assholes", "anal", "anus", "arsehole", "arse", "bitch", "bangbros", "bastards",
                 "bastard", "bitch", "tit", "butt", "blow job", "blowjob", "boob", "bra", "cock", "cum", "cunt",
                 "dick", "shit", "cocaine", "sex", "erotic", "fuck", "fxxk", "f**k", "f**c", "f*ck", "fcuk",
                 "fcking", "fck", "fucken", "fucked", "fucks", "gang bang", "gangbang", "genital", "damn", "damnit",
                 "horny", "jerk off", "jerkoff", "masterbate", "masterbates", "masterbated", "naked", "negro",
                 "nigger", "nigga", "orgy", "pussy", "penis", "perve", "rape", "fucked", "fucking", "sexy",
                 "strip club", "vagina", "faggot", "fag", "weed", "lsd", "murder", "incel", "prostitute", "slut",
                 "whore", "hooker", "cuck", "cuckold", "shooting", "nazis", "nazism", "nazi", "sgw", "balls", "thot",
                 "virgin", "virginity", "cumshot", "cumming", "dildo", "vibrator", "sexual", "douche", "douche",
                 "bukkake", "douchebag", "porn", "porngraph", "porngraphy", "killing", "murdering", "murders",
                 "clit", "clitoris", "masterbate tatoo", "dammit", "toxicity", "wtf", "breasts", "tits", "boobies",
                 "dead", "sperm", "death", "pornhub", "intercourse", "pornography", "porno", "boobs", "penisis",
                 "my husband", "my boyfriend", "my girlfriend", "my wife", "end my life", "reddit", "reddit,",
                 "reddit:", "subreddit", "tifu", "post raw", "show discussion thread", "penises", "dildos",
                 "vaginas", "school shooting", "breasts", "showerthoughts", "todayilearned", "r/.*", "u/.*"]
    for word in text.lower().strip().replace('.', '').replace("'s ", ' ').replace('"', '').replace("'", '').split():
        if word in BLACKLIST:
            return True
    return False
