import re
import itertools
import logging
from typing import Match, Optional

# from nlu import intentmap_scheme
from nlu.constants import TopicModule
from response_generator.fsm2 import Tracker

from ..constants import regex_lists
from ..utils import entity


logger = logging.getLogger('utils.regex_match')


def match(tracker: Tracker, whitelist: str = None, blacklist: str = None) -> Optional[Match]:
    return re.search(whitelist, tracker.input_text, re.I) if (
        whitelist and not (blacklist and re.search(blacklist, tracker.input_text, re.I))
    ) else None


def is_play_music(tracker: Tracker) -> bool:
    # remove PLAY_MUSIC check from music
    # use selecting strategy's logic
    # tracker.returnnlp[0].detect_topic_module_from_system_level()

    # all(conditions) check
    conditions = (
        # if user is saying "play music" but last turn is open question
        not bool(
            re.search(r"\b(play (some)? ?music)( on my [\w]+)?$", tracker.input_text, re.I) and
            tracker.is_last_state_open_question,
        ),
        # not 'sing a song' or related command
        not is_command_sing_a_song(tracker),
        # matches 'play_music' from intentmap_scheme, custom blacklist like 'play instruments'
        match(tracker, **regex_lists['play_music']),
        # topic module from system is MUSICCHAT
        TopicModule.MUSIC.value in itertools.chain.from_iterable(
            r.detect_topic_module_from_system_level() for r in tracker.returnnlp),
        # sentence length is less than threshold, threshold from SS
        len(tracker.input_text.split(' ')) < 8,
        # does not contain instruments in text
        not entity.InstrumentItem.detect(tracker)
    )

    logger.debug("[MUSIC] is_play_music check: text={}, {}".format(
        tracker.input_text,
        [
            f"{label}: {value}"
            for label, value in zip(
                ['last_state_open_question', 'sys_re_patterns', 'topic_module', 'len', 'no_instrument'], conditions)
        ]
    ))

    return all(conditions)


def is_repeat(tracker: Tracker) -> Optional[Match]:
    return match(tracker, **regex_lists['repeat'])


def is_asking_for_recommendation(tracker: Tracker) -> Optional[Match]:
    """recommend_terms"""
    return match(tracker, **regex_lists['asking_for_recommendation'])


def is_other_answer(tracker: Tracker) -> Optional[Match]:
    """unknown_match"""
    return match(tracker, **regex_lists['other_answer'])


def is_saying_all_of_them(tracker: Tracker) -> Optional[Match]:
    return match(tracker, **regex_lists['saying_all_of_them'])


def is_rapport(tracker: Tracker) -> Optional[Match]:
    return match(tracker, **regex_lists['rapport'])


def is_lets_talk_about(tracker: Tracker) -> Optional[Match]:
    return match(tracker, **regex_lists['lets_talk_about'])


def is_user_like_to_sing(tracker: Tracker) -> Optional[Match]:
    return match(tracker, **regex_lists['user_like_to_sing'])


def is_exit(tracker: Tracker) -> Optional[Match]:
    if next(iter(entity.ArtistItem.detect(tracker)), None):
        return match(tracker, **regex_lists['exit'])


def is_false_songs(tracker: Tracker) -> Optional[Match]:
    return match(tracker, **regex_lists['false_songs'])


def is_command_sing_a_song(tracker: Tracker) -> Optional[Match]:
    return (
        tracker.returnnlp.has_intent('req_play_music') and
        match(tracker, **regex_lists['command_sing_a_song'])
    )


def is_user_sing_us_a_song(tracker: Tracker) -> Optional[Match]:
    return (
        not tracker.returnnlp.has_intent('req_play_music') and
        match(tracker, **regex_lists['user_sing_us_a_song'])
    )


def is_asking_your_favorite(tracker: Tracker) -> Optional[Match]:
    return match(tracker, **regex_lists['asking_your_favorite'])
