import logging
import requests
from dataclasses import dataclass
from json import JSONDecodeError
from typing import Any, Dict, List, Optional

from . import has_profanity


@dataclass
class MusicNERResponse:

    @dataclass
    class People:
        name: str
        artist_type: str
        area: str
        area_type: str
        artist_id: str
        begin: int
        end: int
        gender: str
        genre: List[str]

        @classmethod
        def from_response(cls, response: Dict[str, Any]):
            name = response.get('name', "")
            if has_profanity(name):
                return
            else:
                return cls(
                    name=response.get('name', ""),
                    artist_type=response.get('type', ""),
                    area=response.get('area', ""),
                    area_type=response.get('area.type', ""),
                    artist_id=response.get('artist.id', ""),
                    begin=response.get('begin', -10000),
                    end=response.get('end', -10000),
                    gender=response.get('gender', ""),
                    genre=[g for g in response.get('genre') if isinstance(g, str)],
                )

        def __bool__(self):
            return bool(self.name)

    @dataclass
    class Song:
        artist_name: str
        song_name: str

        @classmethod
        def from_response(cls, response: Dict[str, Any]):
            artist_name = response.get('artistName', '')
            song_name = response.get('songName', '')
            if any(has_profanity(i) for i in (artist_name, song_name)):
                return cls(artist_name="", song_name="")
            else:
                return cls(artist_name=artist_name, song_name=song_name)

        def __bool__(self):
            return bool(self.artist_name) and bool(self.song_name)

    genre: List[str]
    people: List[People]
    songs: List[Song]

    @classmethod
    def from_response(cls, response: Dict[str, Any]):
        return cls(
            genre=list(g for g in response.get('genres', []) if isinstance(g, str)),
            people=list(filter(lambda a: bool(a),
                               (MusicNERResponse.People.from_response(i) for i in response.get('people', [])))),
            songs=list(filter(lambda a: bool(a),
                              (MusicNERResponse.Song.from_response(i) for i in response.get('song', []))))
        )


def fetch_music_ner(user_utterance, last_bot_response) -> Optional[MusicNERResponse]:
    MUSIC_NER_URL = "http://52.20.135.52:5017/music_NER"
    TIMEOUT = (0.3, 0.4)

    data = {"user_utterance": user_utterance or "__EMPTY__", "last_bot_response": last_bot_response or "__EMPTY__"}
    logging.debug(f"[fetch_music_ner] request: {data}")

    try:
        response = requests.post(MUSIC_NER_URL, json=data, timeout=TIMEOUT)
        result = response.json()

        logging.debug(f"[fetch_music_ner] results: {result}")
        return MusicNERResponse.from_response(result)

    except (requests.RequestException, JSONDecodeError, Exception) as e:
        logging.warning(f"[fetch_music_ner] {e}", exc_info=True)
