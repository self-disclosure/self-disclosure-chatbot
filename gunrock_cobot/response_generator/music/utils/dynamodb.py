import decimal
import logging
import random
import re
from dataclasses import dataclass
from typing import List

import boto3
from botocore.exceptions import ClientError

from response_generator.music.utils import legacy
from response_generator.music.constants import profanity_filter


logger = logging.getLogger(__name__)


dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
table_music_artists = dynamodb.Table('MusicArtists')
table_music_songs = dynamodb.Table('music_songs')


@dataclass
class ArtistDetails:
    albums: List[str] = None
    count: int = None
    name: str = None
    songs: List[str] = None
    trivias: List[str] = None

    @classmethod
    def from_table(cls, result: dict):
        return cls(
            albums=[
                a for a in result.get('albums', [])
                if a and isinstance(a, str) and not re.search(profanity_filter, a) and legacy.is_english(a)],
            count=int(result.get('count')) if isinstance(result.get('count'), decimal.Decimal) else 0,
            name=(result.get('name') or '').strip(),
            songs=[
                s for s in result.get('songs', [])
                if s and isinstance(s, str) and not re.search(profanity_filter, s)],
            trivias=[
                t for t in result.get('trivias', [])
                if t and isinstance(t, str) and not re.search(profanity_filter, t)]
        )

    def artist_dict(self):
        """
        deprecated
        """
        artist_dict = {}
        if self.name:
            artist_dict['name'] = self.name
        if self.songs:
            artist_dict['songs'] = [legacy.get_songs(self.name), legacy.get_songs(self.name)]
        if self.albums:
            artist_dict['albums'] = [random.choice(self.albums), random.choice(self.albums)]
        if self.trivias:
            artist_dict['trivias'] = [random.choice(self.trivias), random.choice(self.trivias)]
        return artist_dict

    def __bool__(self):
        return bool(self.name)


def get_artist_details(artist_name: str):
    try:
        response = table_music_artists.get_item(
            Key={
                'name': artist_name,
            }
        )
        return ArtistDetails.from_table(response.get('Item', {}))
    except ClientError as e:
        logging.error("[MUSIC] query db error: {}".format(e), exc_info=True)


def get_songs_by_artist(artist_name: str):
    try:
        response = table_music_songs.get_item(
            Key={
                'name': artist_name,
            }
        )
        songs = response.get('Item', {}).get('songs')
        logging.debug(f"[MUSIC] get_song_by_artist: raw: {songs}")
        if not isinstance(songs, dict):
            return []

        # logging.debug(f"[ASDF] {songs}")

        songs = [k for k, _ in sorted(songs.items(), key=lambda i: i[1]['count'], reverse=True)]
        logging.debug(f"[MUSIC] get_song_by_artist: songs: {songs}")
        return songs
    except ClientError as e:
        logging.error("[MUSIC] query db error: {}".format(e), exc_info=True)
