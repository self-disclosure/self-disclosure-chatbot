from typing import List, Optional, Union

from response_generator.fsm2 import Tracker, FSMLogger
from user_profiler.user_profile import UserProfile as GlobalUserProfile

from response_generator.music.utils import entity


"""
- fav_genre
    - idk: bool
    - hist: []

- fav_artist
    - idk: bool
    - hist: []
- my_fav_artist
    - propose_counter: int
    - hist: []
- why_like_artist
    - propose_counter: int
    - hist: []

- know_instruments
    - hist: []
    - know_none: bool
"""

logger = FSMLogger('MUSIC', 'user_profile', 'music.utils')


class UserProfile:

    def __init__(self, tracker: Tracker):
        self.tracker = tracker
        self.global_user_profile = GlobalUserProfile(tracker.user_attributes.ua_ref)

    """
    fav_genre
    """
    @property
    def fav_genre_idk_flag(self) -> Optional[bool]:
        return self.tracker.persistent_store.get('fav_genre_idk_flag')

    @fav_genre_idk_flag.setter
    def fav_genre_idk_flag(self, flag: bool):
        self.tracker.persistent_store['fav_genre_idk_flag'] = flag

    @property
    def fav_genre_history(self) -> List[entity.GenreItem]:
        return self.tracker.persistent_store.get('fav_genre_hist') or []

    def fav_genre_history_append(self, genre: Union[entity.GenreItem, List[entity.GenreItem]]):
        # return
        hist = self.tracker.persistent_store.get('fav_genre_hist') or []
        if isinstance(genre, entity.GenreItem):
            genre = [genre]
        elif isinstance(genre, list) and all(isinstance(i, entity.GenreItem) for i in genre):
            pass
        else:
            genre = []

        self.tracker.persistent_store['fav_genre_hist'] = [*hist, *genre]
        self.global_user_profile.topic_module_profiles.music.fav_genres.extend(i.noun for i in genre)

    """
    fav_artist
    """
    @property
    def fav_artist_idk_flag(self) -> Optional[bool]:
        return self.tracker.persistent_store.get('fav_artist_idk_flag')

    @fav_artist_idk_flag.setter
    def fav_artist_idk_flag(self, flag: bool):
        self.tracker.persistent_store['fav_artist_idk_flag'] = flag

    @property
    def fav_artist_history(self) -> List[entity.ArtistItem]:
        return self.tracker.persistent_store.get('fav_artist_hist') or []

    def fav_artist_history_append(self, artist: Union[entity.ArtistItem, List[entity.ArtistItem]]):
        # return
        hist = self.tracker.persistent_store.get('fav_artist_hist') or []
        if isinstance(artist, entity.ArtistItem):
            artist = [artist]
        elif isinstance(artist, list) and all(isinstance(i, entity.ArtistItem) for i in artist):
            pass
        else:
            artist = []
        self.tracker.persistent_store['fav_artist_hist'] = [*hist, *artist]
        self.global_user_profile.topic_module_profiles.music.fav_artists.extend(i.noun for i in artist)

    """
    my_fav_artist
    """
    @property
    def my_fav_artist_propose_counter(self) -> int:
        return self.tracker.persistent_store.get('my_fav_artist_propose_counter') or 0

    @my_fav_artist_propose_counter.setter
    def my_fav_artist_propose_counter(self, value: int):
        self.tracker.persistent_store['my_fav_artist_propose_counter'] = value

    @property
    def my_fav_artist_history(self) -> List[entity.ArtistItem]:
        return self.tracker.persistent_store.get('my_fav_artist_hist') or []

    def my_fav_artist_history_append(self, artist: Union[entity.ArtistItem, List[entity.ArtistItem]]):
        # return
        hist = self.tracker.persistent_store.get('my_fav_artist_hist') or []
        if isinstance(artist, entity.ArtistItem):
            artist = [artist]
        elif isinstance(artist, list) and all(isinstance(i, entity.ArtistItem) for i in artist):
            pass
        else:
            artist = []
        self.tracker.persistent_store['my_fav_artist_hist'] = [*hist, *artist]

    """
    why_like_artist
    """
    @property
    def why_like_artist_propose_counter(self) -> int:
        return self.tracker.persistent_store.get('why_like_artist_propose_counter') or 0

    @why_like_artist_propose_counter.setter
    def why_like_artist_propose_counter(self, value: int):
        self.tracker.persistent_store['why_like_artist_propose_counter'] = value

    @property
    def why_like_artist_history(self) -> List[entity.ArtistItem]:
        return self.tracker.persistent_store.get('why_like_artist_hist') or []

    def why_like_artist_history_append(self, artist: Union[entity.ArtistItem, List[entity.ArtistItem]]):
        # return
        hist = self.tracker.persistent_store.get('why_like_artist_hist') or []
        if isinstance(artist, entity.ArtistItem):
            artist = [artist]
        elif isinstance(artist, list) and all(isinstance(i, entity.ArtistItem) for i in artist):
            pass
        else:
            artist = []
        self.tracker.persistent_store['why_like_artist_hist'] = [*hist, *artist]

    """
    know_instrument
    """
    @property
    def user_can_play_instrument_history(self) -> List[entity.ArtistItem]:
        return self.tracker.persistent_store.get('user_can_play_instrument_hist') or []

    def user_can_play_instrument_history_append(
        self, instrument: Union[entity.InstrumentItem, List[entity.InstrumentItem]]
    ):
        # return
        hist = self.tracker.persistent_store.get('user_can_play_instrument_hist') or []
        if isinstance(instrument, entity.InstrumentItem):
            instrument = [instrument]
        elif isinstance(instrument, list) and all(isinstance(i, entity.InstrumentItem) for i in instrument):
            pass
        else:
            instrument = []
        self.tracker.persistent_store['user_can_play_instrument_hist'] = [*hist, *instrument]
        self.global_user_profile.topic_module_profiles.music.played_instruments.extend(i.noun for i in instrument)

    @property
    def user_know_zero_instruments(self) -> Optional[bool]:
        return self.tracker.persistent_store.get('user_know_zero_instruments')

    @user_know_zero_instruments.setter
    def user_know_zero_instruments(self, value: bool):
        if isinstance(value, bool):
            self.tracker.persistent_store['user_know_zero_instruments'] = value
