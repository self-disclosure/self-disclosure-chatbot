import logging
import re

from nlu.constants import TopicModule
from nlu.question_detector import QuestionDetector
from question_handling.quetion_handler import QuestionHandler, QuestionResponse
from response_generator.fsm2 import Tracker, FSMLogger
from template_manager import Template, TemplateManager
from util_redis import RedisHelper

from .constants import ignore_entity

from .utils import entity, regex_match
# moving functions out cause the file is crashing my JEDI >:(
from .utils.legacy import (
    detect_artist,
    query_db,
    constructDynamicResponse
)


logger = logging.getLogger(__name__)


class MusicAutomaton:

    """
    Structure of artist_dict:
        artist
            - ts (timestamp)
            - name
            - genre
            - songs (list of songs from artist)
                    - name
                    - preview_url
            - sim_songs (list of songs similar to given song)
                    - name
                    - artist
                    - preview_url
            - sim_artists (list of artist similar to given artist)
    """
    from .states.legacy import (
        s_init,
        s_favgenre,
        verify_genre,  # when we're not sure about genre input
        s_favmusician,
        verify_musician,  # verify if user want to talk about the musician
        s_favsong,  # opinionsong followup
        s_artist_question,  # find a artist but not in our database
        s_templates,  # top 5 artist
        verify_templates,  # if the user wants to keep talking the current top 5 artist
        s_artist_song,  # when we don't find a artist in your database
        recommend_song,
        s_morefacts,  # verify
        s_verifychitchat,  # verify if we should enter chitchat
        topic_switch,  # similar to change topic
        change_topic,  # switch to other module
        s_chitchat,  # enter chitchat initialization
        s_chitchatcont,  # ask question
        chitchat_followup,
        t_chitchatcont,  # answer user's response
        s_opinionfact,  # trivia
        s_opinionsong,  # song
        concert_init,  # concert 1
        s_concert,  # concert2
        s_other_concerts,  # cocnert3
        s_best_concert,  # concert4
        s_singing,
        s_singing_rating,
        verify_singing,
    )

    def __init__(self, tm_music: TemplateManager, tm_social, tm_transition, tracker: 'Tracker', input_dict={}):
        self.redis_helper = RedisHelper()
        self.logger = logging.getLogger()
        self.logger = FSMLogger('MUSIC', '__engine__', 'music')
        self.tm_music = tm_music
        self.tm_social = tm_social
        self.tm_transition = tm_transition
        self.updateautomaton(input_dict)
        self.tracker = tracker
        self.question_detector = QuestionDetector(
            tracker.input_text, tracker.input_data['returnnlp']
        )
        self.question_handler = QuestionHandler(
            tracker.input_text, tracker.input_data['returnnlp'],
            backstory_threshold=0.85,
            system_ack=tracker.input_data['system_acknowledgement'],
            user_attributes_ref=tracker.user_attributes.ua_ref
        )

    def play_music_response(self, artist, next_state, next_state_args, artist_include_args=False):
        """
        Generate a response dict that targets if user requests to play music
        """
        song: entity.SongItem = next(iter(entity.SongItem.detect(self.tracker)), None)
        artist_noun = (
            artist.noun
            if isinstance(artist, entity.ArtistItem) else
            artist
            if isinstance(artist, str) else
            song.artist_name if song and song.noun.lower() in self.tracker.input_text else
            None
        )
        self.tracker.one_turn_store['play_music_reponse'] = artist_noun
        ret = {}
        self.logger.debug(
            f"[MUSIC] play_music_response: "
            f"{artist}, {song}, {artist_noun}, "
            f"{next_state}, {next_state_args}, {artist_include_args}")

        if artist_noun:
            if artist_include_args:
                ret = next_state_args.copy()

            response = self.tm_music.speak(
                "play_music_ack/artist",
                {'artist': artist_noun})
            ret.update({
                'currentstate': 'verify_musician',  # because currentstate is nextstate fuck you thats why
                'response': response,
                'artist': artist.noun if isinstance(artist, entity.ArtistItem) else artist,
                'propose_continue': 'CONTINUE'
            })
            self.logger.debug(f"[MUSIC] play_music_response artist not None."
                              f"{artist}, {response}, {ret}")
        else:
            ret = next_state_args.copy()
            response = " ".join([
                self.tm_music.speak("play_music_ack/simple", {}),
                self.tm_music.speak("play_music_ack/propose_sing", {})
            ])
            ret.update({
                'currentstate': 'verify_singing',
                'response': response
            })
            self.logger.debug(f"[MUSIC] play_music_response artist is None."
                              f"{response}, {ret}")
        return ret

    def getresponse(self, input_dict, senti):
        """
        get a response based on current state by calling the corresponding
        response function
        """

        logger.debug(f"[MUSIC] getresponse: {self.tracker.input_text}, {input_dict}")

        if(input_dict['exit']):
            logger.debug(f"[MUSIC] getresponse: entering input_dict['exit'] condition")
            return{
                "response": self.tm_music.speak("stop/no_further", {}),
                "propose_continue": "STOP",
                "exit": input_dict["exit"]
            }

        elif (regex_match.is_exit(self.tracker) and
              not input_dict['song_question'] and
                input_dict['current_state'] != 's_init' and
                not re.search(r"music", self.tracker.input_text)):

            logger.debug(f"[MUSIC] getresponse: entering exit_match, not input_dict['song_question']")
            return {
                "response": self.tm_music.speak('stop/ok', {}),
                "propose_continue": "STOP"
            }
            logger.debug(f"[MUSIC] getresponse: falling out of exit_match, not input_dict['song_question']")

        # elif (not input_dict["song_question"] and
        #       not re.search(r"init|chitchatcont|favsong|templates|favmusician|genre|concert|musician",
        #                     input_dict["currentstate"]) and
        #       input_dict["module"] and len(input_dict["module"]) > 0 and
        #         "MUSICCHAT" not in input_dict["module"] and input_dict["module"][0] in modules_mapping):

        #     logger.debug(f"[MUSIC] getresponse: entering not input_dict['song_question']")
        #     return {
        #         "response": self.tm_music.speak(
        #             'stop/change_topic', {'topic': modules_mapping[input_dict['module'][0]]}),
        #         "propose_continue": "UNCLEAR",
        #         "propose_topic": input_dict["module"][0],
        #         "currentstate": input_dict["currentstate"],
        #         "context": input_dict["context"],
        #         "artist": input_dict["artist"],
        #         "first_time": input_dict["first_time"],
        #         "chitchats": input_dict["chitchats"],
        #         "templates": input_dict["templates"],
        #         "exit": input_dict["exit"],
        #     }

        elif regex_match.is_repeat(self.tracker) and input_dict['last_response'] is not None:
            logger.debug(f"[MUSIC] getresponse: entering repeat_match and input_dict['last_response'] is not None")
            return {
                "response": input_dict["last_response"],
                "currentstate": input_dict["currentstate"],
                "context": input_dict["context"],
                "artist": input_dict["artist"],
                "sim_artist": input_dict["sim_artist"],
                "propose_continue": "CONTINUE",
                "first_time": input_dict["first_time"],
                "chitchats": input_dict["chitchats"],
                "templates": input_dict["templates"],
                "exit": input_dict["exit"],
            }

        # logger.debug(f"[MUSIC] getresponse: play music check")
        elif regex_match.is_play_music(self.tracker):
            logger.debug(f"[MUSIC] getresponse: is_play_music")
            artist = next(iter(entity.ArtistItem.detect(self.tracker)), None)
            return self.play_music_response(
                artist, "s_init",
                # {"artist": input["artist"],
                #     "propose_continue": "UNCLEAR",
                #     "context": input_dict["currentstate"]
                #  }
                {"first_time": False}
            )
        # if play_match:
        #     artist = detect_artist(text, ner, knowledge)
        #     if artist is not None:
        #         return self.play_music_response(artist, "s_init",
        #                                         {"first_time": False})

        elif regex_match.is_command_sing_a_song(self.tracker):
            logger.debug(f"[MUSIC] getresponse: entering sing a song that's not backstory")
            stateoutput = self.s_singing(input_dict)
            # singing = self.tm_music.speak(f"bot_singing/default", {})
            # return {
            #     # "response": singing,
            #     "response": self.join_last_response_question(singing),
            #     "currentstate": input_dict["currentstate"],
            #     "context": input_dict["context"],
            #     "artist": input_dict["artist"],
            #     "sim_artist": input_dict["sim_artist"],
            #     "propose_continue": "CONTINUE",
            #     "first_time": input_dict["first_time"],
            #     "chitchats": input_dict["chitchats"],
            #     "templates": input_dict["templates"],
            #     "exit": input_dict["exit"],
            # }

        # elif self.question_detector.has_question():
        #     logger.debug(f"[MUSIC] getresponse: question handler")
        #     ret = self.handle_question_global(input_dict)
        #     logger.debug(f"[MUSIC] getresponse: question handler response: {ret}")
        #     if ret:
        #         return ret

        elif re.search(r"open gunrock and ", input_dict["text"]):
            input_dict["text"] = input_dict["text"].strip("open gunrock and ")

        # if user wants to talk about a specific artist

        elif regex_match.is_lets_talk_about(self.tracker):
            logger.debug(f"[MUSIC] getresponse: is lets talk about: {self.tracker.input_text}")
            artists = entity.ArtistItem.detect(self.tracker)
            # songs = entity.SongItem.detect(self.tracker)

            artist = next(iter(artists), None)
            logger.debug(f"[MUSIC] getresponse: is lets talk about: artist: {artist}")
            if artist:
                logger.debug(f"[MUSIC] getresponse: is lets talk about: jumping to s_favmusician")
                stateoutput = self.s_favmusician(input_dict)

        logger.debug(f"[MUSIC] getresponse: out of arbitrary checks, going to state: {input_dict['currentstate']}")

        nextstate = input_dict["currentstate"]
        # stateoutput = self.statesmapping[nextstate](input_dict)
        # skip stateoutput running if set in above checks
        self.logger = FSMLogger('MUSIC', nextstate, 'music')
        stateoutput = locals().get('stateoutput') or getattr(self, nextstate, lambda: {})(input_dict)
        self.logger = FSMLogger('MUSIC', '__engine__', 'music')
        logger.debug(f"[MUSIC] getresponse: stateoutput: \n{stateoutput}")
        context = {}
        artist = {}
        sim_artist = {}
        propose_continue = "CONTINUE"
        first_time = True
        chitchats = {}
        next_state = 's_init'
        templates = {}
        song_question = False
        genre = None
        propose_topic = None

        if "artist" in stateoutput:
            artist = stateoutput["artist"]
        if "sim_artist" in stateoutput:
            sim_artist = stateoutput["sim_artist"]
        if "propose_continue" in stateoutput:
            propose_continue = stateoutput["propose_continue"]
        if "first_time" in stateoutput:
            first_time = stateoutput["first_time"]
        if "chitchats" in stateoutput:
            chitchats = stateoutput["chitchats"]
        else:
            chitchats = input_dict["chitchats"]
        if "next" in stateoutput:
            next_state = stateoutput["next"]
        if "templates" in stateoutput:
            templates = stateoutput["templates"]
        if "exit" in stateoutput:
            exit_value = stateoutput["exit"]
        else:
            exit_value = input_dict["exit"]
        if "song_question" in stateoutput:
            song_question = stateoutput["song_question"]
        if "genre" in stateoutput:
            genre = stateoutput["genre"]
        if "propose_topic" in stateoutput:
            propose_topic = stateoutput["propose_topic"]

        response = {
            "response": constructDynamicResponse(stateoutput["response"]),
            "currentstate": next_state,
            "context": context,
            "artist": artist,
            "sim_artist": sim_artist,
            "propose_continue": propose_continue,
            "first_time": first_time,
            "chitchats": chitchats,
            "templates": templates,
            "exit": exit_value,
            "song_question": song_question,
            "genre": genre,
            "propose_topic": propose_topic,
        }
        logger.debug(f"[MUSIC] getresponse: response complete. response: \n{response}")

        return response

    # update the current state and context
    def updateautomaton(self, input_dict):
        if "currentstate" in input_dict:
            self.currentstate = input_dict["currentstate"]
        else:
            self.currentstate = "s_init"
        if "context" in input_dict:
            self.context = input_dict["context"]

        self.logger.debug(f"[MUSIC] current state: {self.currentstate}")

    def artist_exists(self, text, ner, keywords=None, concept=None, API=False):
        """
        Search redis for artist, otherwise look at nlu result (API disabled)
        """
        if self.redis_helper.is_known_artist(text):
            artist = text
        else:
            artist = detect_artist(text, ner, keywords, concept, API)
            if artist and isinstance(artist, str):
                if re.search(ignore_entity, artist):
                    return None
                else:
                    result = query_db(artist.lower())
                    if result is None:
                        return None
                    if len(result) == 0:
                        return None
            else:
                return None
        return artist

    def read_chitchat_content(self, chitchat_id):
        emb_q_info = {}
        emb_a_info = {}
        q = self.tm_music.speak("chitchats/contents/c{}_q".format(chitchat_id),
                                {}, embedded_info=emb_q_info)
        a = self.tm_music.speak("chitchats/contents/c{}_a".format(chitchat_id),
                                {}, embedded_info=emb_a_info)
        ret = {
            "q": q, "a": a
        }
        for k in emb_q_info:
            ret[k] = emb_q_info[k]
        return ret

    def handle_question(
            self,
            is_unanswerable_question,
            handle_topic_specific_question,
            handle_general_question):

        def _is_unanswerable_question() -> bool:
            self.logger.debug(f"default _is_unanswerable_question")
            # todo: you should decide what's answerable in you topic module
            if self.question_detector.is_ask_back_question() or self.question_detector.is_follow_up_question():
                return True
            return False

        def _handle_general_question() -> QuestionResponse:
            answer = self.question_handler.handle_question()
            return answer

        # Step 1: check if it has question
        if not self.question_detector.has_question():
            logger.debug(f"handle question: no question")
            return None

        # Step 2: Check if it's unanswerable
        if (is_unanswerable_question or _is_unanswerable_question)():
            logger.debug(f"handle question: is_unanswerable")
            return QuestionResponse(response=self.question_handler.generate_i_dont_know_response())

        # step 3:
        if handle_topic_specific_question:
            logger.debug(f"handle question: topic specific question")
            return handle_topic_specific_question()

        # Step 4
        response = (handle_general_question or _handle_general_question)()
        if response:
            logger.debug(f"handle question: handle general question")
            return response
        return None

    def handle_question_global(self, input_dict):

        def is_unanswerable_question() -> bool:
            # todo: you should decide what's answerable in you topic module
            if self.question_detector.is_ask_back_question() or self.question_detector.is_follow_up_question():
                return True
            return False

        def handle_topic_specific_question(response_dict):
            # eg. for movie, it should handle user's requests like "what movie do you recommend to me"
            return None
            # response_text = "...."  # todo: implement your own logic here
            # return QuestionResponse(response=response_text)

        def handle_general_question(response_dict):
            answer: QuestionResponse = self.question_handler.handle_question()
            self.logger.debug(f"[MUSIC] question_handler handle_general_question: {answer}")
            if answer:
                response = [answer.response]

                if answer.bot_propose_module and answer.bot_propose_module is not TopicModule.MUSIC:

                    tm_transition = TemplateManager(Template.transition, self.tm_music.user_attributes.ua_ref)
                    response.append(
                        tm_transition.speak(f"propose_topic_short/{answer.bot_propose_module.value}", {})
                    )

                    response_dict['propose_continue'] = 'UNCLEAR'
                    response_dict['propose_topic'] = answer.bot_propose_module.value

                else:
                    # trimming last utterance
                    # response.append(self.join_last_response_question())
                    pass

                response_dict['response'] = ' '.join(response)

            return bool(answer)

        ret = {
            # "response": " ".join(response),
            "currentstate": input_dict["currentstate"],
            "context": input_dict["context"],
            "artist": input_dict["artist"],
            "sim_artist": input_dict["sim_artist"],
            # "propose_continue": propose_continue,
            "first_time": input_dict["first_time"],
            "chitchats": input_dict["chitchats"],
            "templates": input_dict["templates"],
            "exit": input_dict["exit"],
        }

        # Step 1: check if it has question
        if not self.question_detector.has_question():
            return None

        # Step 2: Check if it's unanswerable
        if is_unanswerable_question():
            self.logger.debug(f"[MUSIC] question_handler is unanswerable question")
            # ret['response'] = self.join_last_response_question(self.question_handler.generate_i_dont_know_response())
            ret['response'] = self.question_handler.generate_i_dont_know_response()
            return ret

        # step 3:
        # response = handle_topic_specific_question(ret)
        # if response:
        #     return ret

        # Step 4
        response = handle_general_question(ret)
        if response:
            return ret

    def join_last_response_question(self, curr_utt: str = None):
        """Deprecated. No more joining. Will return '' or curr_utt"""
        return curr_utt or ''
        # try:
        #     last_response = re.split(r"[.!] ", self.tracker.last_bot_response())[-1].strip()
        #     joiners = [
        #         self.tm_music.template._str_dict_selector(s)
        #         for s, _ in self.tm_music.template._get_utterance_list('transition/backstory/joiner')
        #     ]
        #     for joiner in joiners:
        #         if last_response.startswith(joiner):
        #             last_response = last_response[len(joiner):].strip()
        #             break
        #     last_response = re.sub(r"<[^>]*>", "", last_response)
        #     self.logger.debug(f"[MUSIC] question_handler last respone: {last_response}")

        # except Exception as e:
        #     self.logger.debug(f"[MUSIC] question_handler failed to trim response: {e}")
        #     last_response = self.tracker.last_bot_response()

        # return " ".join(([curr_utt] if curr_utt else []) + [
        #     self.tm_music.speak('transition/backstory/joiner', {}),
        #     last_response
        # ])
