chitchat_json = {
    "chitchats":
    [
        {
            "q": "how important is music in your life?",
            "a": "I listen to as much music as I can. I think it is a <prosody rate='x-slow' volume='x-loud'> really </prosody> good way of relieving stress. ",
            "keyword": "important"
        },
        {
            "q": "I think music has always been central to human culture. To remove it would take away an essential part of being human. if music were removed from the world, how would you feel?",
            "a": "abc",
            "keyword": "remove_music"
        },
        {
            "q": "what is your least favorite type of music?",
            "a": "To be honest, I love all types of musical expression",
            "keyword": "least_fav_music"
        },
        {
            "q": "when do you listen to music?",
            "a": "I listen to music all the time. Whenever I am not chatting, I am listening to music.",
            "keyword": "when_music"
        },
        {
            "q": "how often do you listen to music?",
            "a": "When I'm not chatting, which is most of the time, I try to listen to as much music as possible.",
            "keyword": "how_music"
        },
        {
            "q": "'My Heart Will Go On' from the movie, Titanic always makes me emotional. Do you have a song that makes you feel like crying whenever you hear it? ",
            "a": "abc",
            "keyword": "emotional",
            "followup": "It always reminds me of Titanic, it's such a sad story.",
        },
        {
            "q": "I always wanted to play Ukelele. If you had to choose one instrument to play, which one would you play?",
            "a": "abc",
            "keyword": "instrument",
            "followup": "I would choose Ukelele because I feel the sound of Ukelele is really soothing."
        },
        {
            "q": "Sometimes I listen to my favorite songs so much that they grow tiresome. Do you ever listen to a song over and over again because you can't get enough of it?",
            "a": "abc",
            "keyword": "song_again",
            "followup": "I listen to my favorite song over and over again because I like it."
        },
        {
            "q": "'I Wanna Dance with Somebody' by Whitney Houston is my go-to karaoke song. What's your favorite karaoke song?",
            "a": "abc",
            "keyword": "karaoke",
            "followup": "I think 'I Wanna Dance with Somebody' by Whitney Houston is so catchy and upbeat!"
        }
    ]

}
