import logging

from cobot_core.service_module import LocalServiceModule

import nlu.util_nlu
from response_generator.fsm2 import FSMModule, FSMAttributeAdaptor, Tracker
from template_manager import Template, TemplateManager

from . import music_automaton
from .states import MusicState


USE_NEW_FSM = True

logger = logging.getLogger(__name__)


class MusicResponseGeneratorLocal(LocalServiceModule):
    def get_state_feature_by_key_or(self, feature_name, default_value):
        try:
            value = nlu.util_nlu.get_feature_by_key(
                self.state_manager.current_state, feature_name)
        except Exception:
            value = default_value
        return value

    def __init__(self, *args, **kwargs):
        LocalServiceModule.__init__(self, *args, **kwargs)
        self.tm_music = TemplateManager(Template.music, self.state_manager.user_attributes)
        self.tm_social = TemplateManager(Template.social, self.state_manager.user_attributes)
        self.tm_transition = TemplateManager(Template.transition, self.state_manager.user_attributes)

    def execute(self):

        if USE_NEW_FSM:
            module = FSMModule(
                FSMAttributeAdaptor('musicchat', self.state_manager.user_attributes),
                self.input_data,
                Template.music,
                MusicState,
                first_state='initial',
                state_manager_last_state=self.state_manager.last_state
            )
            return module.generate_response()

        # Getting ref to user_attributes

        music_chat = self.state_manager.user_attributes.musicchat
        if music_chat is None:
            music_chat = {}
        self.logger.info('[MusicRG] exec with musicchat: {}'.format(music_chat))

        currentmusicstate = music_chat.get("currentstate", "s_init")
        artist = music_chat.get("artist", {})
        sim_artist = music_chat.get("sim_artist", {})
        music_context = music_chat.get("context", {})
        # propose_continue = music_chat.get("propose_continue", "CONTINUE")
        first_time = music_chat.get("first_time", True)
        chitchats = music_chat.get("chitchats", {})
        templates = music_chat.get("templates", {})
        song_question = music_chat.get("song_question", False)
        # backstory = None
        exit_flag = music_chat.get("exit", False)
        genre = music_chat.get("genre", None)
        propose_topic = music_chat.get("propose_topic", [])

        # getting ref to legacy NLU

        lexical = nlu.util_nlu.get_feature_by_key(
            self.state_manager.current_state, 'intent_classify')['lexical']
        try:
            topic_keyword = self.state_manager.current_state.features[
                "topic_keywords"][0]['keyword']
        except Exception:
            topic_keyword = ''
        npknowledge = self.get_state_feature_by_key_or('npknowledge',
                                                       {'local_noun_phrase': []})
        noun_phrase = npknowledge['local_noun_phrase']
        knowledge = self.get_state_feature_by_key_or('knowledge', [])
        ner = self.get_state_feature_by_key_or('ner', None)
        senti = self.get_state_feature_by_key_or('sentiment', 'other')
        dialog_act = self.get_state_feature_by_key_or('dialog_act', None)
        coreference = self.get_state_feature_by_key_or('coreference', None)
        concept = self.get_state_feature_by_key_or('concept', None)

        # setting up system level context

        previous_modules = self.state_manager.user_attributes.previous_modules
        user_id = self.state_manager.user_attributes.user_id
        no_match = self.tm_social.speak('backstory/no_match', {}) + " "
        central_elem = nlu.util_nlu.get_feature_by_key(self.state_manager.current_state, 'central_elem')
        text = self.input_data["text"].lower()
        # last_state = self.state_manager.last_state if self.state_manager.last_state is not None else None
        last_state = self.state_manager.last_state
        last_response = last_state.get('response', None) if last_state is not None else None
        # heuristic on using central_elem over full text
        if len(text.split(" ")) > 7:
            text = central_elem["text"]
        module = central_elem["module"]
        try:
            asr_correction = nlu.util_nlu.get_feature_by_key(
                self.state_manager.current_state, 'asrcorrection')
        except Exception:
            asr_correction = None

        inputs = {
            "currentstate": currentmusicstate,
            "text": text,
            "nlu": {
                "sentiment": senti,
                "lexical": lexical,
                "ner": ner
            },
            "context": music_context,
            "artist": artist,
            "sim_artist": sim_artist,
            "first_time": first_time,
            "chitchats": chitchats,
            "topic_keyword": topic_keyword,
            "local_noun_phrase": noun_phrase,
            "knowledge": knowledge,
            "dialog_act": dialog_act,
            "coreference": coreference,
            "concept": concept,
            "previous_modules": previous_modules,
            "templates": templates,
            "user_id": user_id,
            "exit": exit_flag,
            "no_match": no_match,
            "last_response": last_response,
            "song_question": song_question,
            "genre": genre,
            "propose_topic": propose_topic,
            "module": module,
            "asr_correction": asr_correction,
            "a_b_test": self.state_manager.current_state.a_b_test
        }
        self.logger.info(
            f"[MusicRG] get response with input: {inputs}, sentiment: {senti}, lexical: {lexical}, ner {ner}")

        tracker = Tracker(self.input_data, FSMAttributeAdaptor('music', self.state_manager.user_attributes))
        self.logger.info(f"[MUSIC] tracker: ReturnNLP: {tracker.returnnlp}; CE: {tracker.central_element}")
        self.logger.info(f"[MUSIC] tracker: storage: {tracker.user_attributes}, {tracker.persistent_store}")

        RG = music_automaton.MusicAutomaton(self.tm_music, self.tm_social, self.tm_transition, tracker, inputs)

        try:
            outputtext = RG.getresponse(
                inputs, nlu.util_nlu.get_sentiment_key(senti, lexical))
            logger.debug(f"[MUSIC] [RG] outputtext: {outputtext}")
            response_text = outputtext["response"]
        except Exception as e:
            self.logger.error(f"[MusicRG] Bug in Music {e}", exc_info=True, stack_info=True)
            response_text = self.tm_music.speak(f"error/global", {})
            self.state_manager.user_attributes.musicchat["propose_continue"] = "STOP"
            outputtext = None

        # remove redundant data
        logger.debug(f"[MUSIC] writing to one_turn_store: {tracker.one_turn_store}")

        if outputtext is not None:
            outputtext: dict
            outputtext.pop('nlu', None)
            outputtext.pop('text', None)
            outputtext.pop('response', None)
            self.state_manager.user_attributes.musicchat = outputtext
            tracker.commit()
            if "propose_topic" in outputtext and outputtext["propose_topic"]:
                setattr(self.state_manager.user_attributes,
                        "propose_topic", outputtext["propose_topic"])

        # response_text = re.sub(r"&", ' and ', response_text)
        return response_text
