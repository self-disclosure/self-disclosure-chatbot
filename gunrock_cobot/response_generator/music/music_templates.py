templates_json = {
    "mj":
    [{
        "response": "Were you ever a fan of Michael Jackson at all?",
        "keyword": "mj",
        "neg_response": "exit",
        "pos_response": "sad"
    },
        {
        "response": "It's really sad that he passed away, he was such a talented artist.",
        "keyword": "sad",
        "pos_response": "concert",
        "neg_response": "concert"
    },
        {
        "response": "I would have loved to see him in concert. Did you ever see him perform <phoneme alphabet=\"x-sampa\" ph='laIv'>live</phoneme>?",
        "keyword": "concert",
        "pos_response": "how",
        "neg_response": "fav_song"
    },
        {
        "response": "Oh wow how was it?",
        "keyword": "how",
        "pos_response": "i_bet",
        "neg_response": "fav_song"
    },
        {
        "response": "I bet it was. I would have loved to be there.",
        "keyword": "i_bet",
        "pos_response": "fav_song",
        "neg_response": "fav_song"
    },
        {
        "response": "Which of his songs did you like the best?",
        "keyword": "fav_song",
        "match": r"thriller",
        "match_text": "<say-as interpret-as=\"interjection\">wow</say-as>, 'Thriller' is my favorite too. ",
        "pos_response": "thriller",
        "neg_response": "thriller"
    },
        {
        "response": "One of my faves was 'Thriller'. I loved the music video.",
        "keyword": "thriller",
        "pos_response": "beat",
        "neg_response": "beat"
    },
        {
        "response": "You can't beat his music. There are no artists that can compare today .",
        "keyword": "beat",
        "pos_response": "exit",
        "neg_response": "exit"
    }],
    "ts":
    [{
        "response": "Do you like Taylor Swift?",
        "keyword": "ts",
        "pos_response": "classy_lady",
        "neg_response": "exit"
    },
        {
        "response": "Wow, we have that in common. Taylor is always so classy and fun",
        "keyword": "classy_lady",
        "pos_response": "grape_vine",
        "neg_response": "grape_vine"
    },
        {
        "response": "She has dated so many good looking men, but too bad, none of them stayed.",
        "keyword": "grape_vine",
        "pos_response": "juicy_songs",
        "neg_response": "fav_song"
    },
        {
        "response": "But on the flip side. If she has found a keeper, all the juicy songs would not be there.",
        "keyword": "juicy_songs",
        "pos_response": "blank_space",
        "neg_response": "fav_song"
    },
        {
        "response": "Did you know that Taylor Swift was smothered by her mother's big dog. That's why she's a cat person ?",
        "keyword": "blank_space",
        "pos_response": "fav_song",
        "neg_response": "fav_song"
    },
        {
        "response": "Which song of hers is your favorite?",
        "keyword": "fav_song",
        "match": r"you belong with me",
        "match_text": "<say-as interpret-as=\"interjection\">wow</say-as>, 'You Belong with Me' is my favorite too. ",
        "pos_response": "guitar",
        "neg_response": "guitar"
    },
        {
        "response": "Call me traditional but 'You Belong with Me' from her first album is my favorite.",
        "keyword": "guitar",
        "pos_response": "exit",
        "neg_response": "exit"
    }],
    "id":
    [{
        "response": "Do you like Imagine Dragons?",
        "keyword": "id",
        "pos_response": "great_songs",
        "neg_response": "exit"
    },
        {
        "response": "They make great songs, full of so much emotion and intensity.",
        "keyword": "great_songs",
        "pos_response": "video",
        "neg_response": "video"
    },
        {
        "response": "Have you seen the video they did with Dolph Lundgren?",
        "keyword": "video",
        "pos_response": "thought",
        "neg_response": "ballads"
    },
        {
        "response": "I thought so!  It was neat seeing him in a music video.",
        "keyword": "thought",
        "pos_response": "ballads",
        "neg_response": "ballads"
    },
        {
        "response": "Do you think they will ever put out any ballads?",
        "keyword": "ballads",
        "pos_response": "talented",
        "neg_response": "fav_song"
    },
        {
        "response": "I wouldn't be surprised if they did.  They are talented, and could pull it off.",
        "keyword": "talented",
        "pos_response": "fav_song",
        "neg_response": "fav_song"
    },
        {
        "response": "What's your favorite Imagine Dragons song?",
        "keyword": "fav_song",
        "match": r"radioacive",
        "match_text": "<say-as interpret-as=\"interjection\">wow</say-as>, 'Radioactive' is my favorite too.",
        "pos_response": "intense_songs",
        "neg_response": "intense_songs"
    },
        {
        "response": "I really liked 'Radioactive'. The intensity of the song is too good.",
        "keyword": "intense_songs",
        "pos_response": "exit",
        "neg_response": "exit"
    }],
    "dg": [
        {
            "response": "What do you think of Drake?",
            "keyword": "dg",
            "pos_response": "good_rapper",
            "neg_response": "exit"
        },
        {
            "response": "He is a great rapper. He's done really well for himself.",
            "keyword": "good_rapper",
            "pos_response": "house_beats",
            "neg_response": "fav_song"
        },
        {
            "response": "I like the way drake uses house beats. It's funny that he was Lil Wayne's protege.",
            "keyword": "house_beats",
            "pos_response": "show",
            "neg_response": "fav_song"
        },
        {
            "response": "Did you know when Drake was young, he was on the show Degrassi: The Next Generation.",
            "keyword": "show",
            "pos_response": "fav_song",
            "neg_response": "fav_song"
        },
        {
            "response": "What's your favorite Drake song?",
            "keyword": "fav_song",
            "match": r"hotline bling",
            "match_text": "<say-as interpret-as=\"interjection\">wow</say-as>, 'Hotline Bling' is my favorite too.",
            "pos_response": "hotline_bling",
            "neg_response": "hotline_bling"
        },
        {
            "response": "My favorite is definitely Hotline Bling. It's super catchy.",
            "keyword": "hotline_bling",
            "pos_response": "exit",
            "neg_response": "exit"
        }
    ],
    "m5": [
        {
            "response": "Have you heard of Maroon 5?",
            "keyword": "m5",
            "pos_response": "good_band",
            "neg_response": "exit"
        },
        {
            "response": "Maroon 5 is such a good band. who is your favorite band member?",
            "keyword": "good_band",
            "pos_response": "adam_levine",
            "neg_response": "adam_levine"
        },
        {
            "response": "I just love Adam Levine's voice, he has an amazing vocal range.",
            "keyword": "adam_levine",
            "pos_response": "name",
            "neg_response": "fav_song"
        },
        {
            "response": "I don't know why they have a 5 in their name. Maroon 5 has seven members in their band.",
            "keyword": "name",
            "pos_response": "fav_song",
            "neg_response": "fav_song"
        },
        {
            "response": "Which Maroon 5 song would you say is your favorite?",
            "keyword": "fav_song",
            "match": r"sugar",
            "match_text": "<say-as interpret-as=\"interjection\">wow</say-as>, 'Sugar' is my favorite too.",
            "pos_response": "sugar",
            "neg_response": "sugar"
        },
        {
            "response": "I think their famous song Sugar is my favorite.",
            "keyword": "sugar",
            "pos_response": "exit",
            "neg_response": "exit"
        }
    ]
}
