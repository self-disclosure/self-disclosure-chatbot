import re

from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states import fsm as states
from response_generator.music.states.fsm.base import MusicState

from response_generator.music.utils import entity, regex_match
from response_generator.music.states.fsm.instruments import instruments as instrument_states


class Initial(MusicState):

    name = 'initial'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        # get entities from input

        # DEBUG
        return [NextState(states.FavArtistEnter.name, jump=True)]

        artist: entity.ArtistItem = next(iter(entity.ArtistItem.detect(tracker)), None)
        genre: entity.GenreItem = next(iter(entity.GenreItem.detect(tracker)), None)
        instrument: entity.InstrumentItem = next(iter(entity.InstrumentItem.detect(tracker)), None)

        self.logger.debug(f"entering. detected entities: {(artist, genre, instrument)}")

        # if user want a song rec

        if regex_match.is_asking_for_recommendation(tracker):
            self.logger.debug(f"asking for rec. Going to recommend_song")
            return [NextState(states.RecommendSong.name, jump=True)]

        # if user is talking about a genre positively

        if genre and tracker.returnnlp[0].sentiment != 'neg':
            self.logger.debug(f"taking about genre positively, going to fav_musician")
            dispatcher.respond_template('artist_question/follow_genre_question/no_pop', {'genre': genre.noun})
            return [NextState(states.FavoriteMusician.name)]

        # if users wanna talk about a specific artist

        if artist and tracker.returnnlp[0].sentiment != 'neg':
            return [NextState(states.FavoriteMusician.name, jump=True)]

        # Question Handling

        self.logger.debug(f"handle question")
        question_response = self.command_question_handler.handle_question()
        if self.command_question_handler.handle_is_proposing_topic_response(
            question_response, TopicModule.MUSIC, dispatcher, tracker
        ):
            return [NextState(states.Initial.name)]
        elif question_response:
            self.logger.debug(f"clear question_response: {question_response}")
            dispatcher.respond(question_response.response)
            dispatcher.respond_template('no_match/continue', {})
            dispatcher.respond_template('artist_question/general', {})
            return [NextState(states.FavoriteMusician.name)]

        # if users like to play musical instruments

        if instrument or re.search(r"\b(play instruments?)\b", tracker.input_text, re.I):
            # TODO: different dialog if 'play instruments'
            self.logger.debug(
                f"checking instruments for special subtopic: {instrument}, "
                f"{instrument_states.valid_instruments(instrument)}")
            if instrument_states.valid_instruments(instrument):
                return [NextState(states.InstrumentsUserPrefRespond.name, jump=True)]

            else:
                dispatcher.respond_template(
                    'start_up/open_question/play_instruments',
                    {'instrument': instrument.noun if instrument else 'instrument'})
                return [NextState(states.FavoriteGenre.name)]

        # If users wanna talk about something else, set first time flag off

        if (
            re.search(r'talk about something else|about something else', tracker.input_text) or
            self.redis_helper.is_returning_user_in_music_module(tracker.user_attributes._get_raw('user_id'))
        ):
            tracker.persistent_store['is_first_time_user'] = False
            self.logger.debug(f"is not first_time, going to fav_musician")
            dispatcher.respond_template('artist_question/general', {})
            return [NextState(states.FavoriteMusician.name)]

        # Default

        self.logger.debug(f"default response. Going to fav_genre")
        dispatcher.respond_template('start_up/default', {})
        return [NextState(states.FavoriteGenre.name)]
