from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states

from response_generator.music.utils import entity, regex_match


class BestConcert(MusicState):
    """
    concert 4
    """

    name = 'best_concert'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        artist_to_discuss: entity.ArtistItem = (
            tracker.one_turn_store.get('artist_to_discuss') or
            tracker.last_utt_store.get('artist_to_discuss') or
            None
        )

        self.logger.debug(f"entering")

        if regex_match.is_asking_for_recommendation(tracker):
            self.logger.debug(f"asking for rec. Going to recommend_song")
            tracker.one_turn_store['recommend_song_old_state'] = self.name
            return [NextState(states.RecommendSong.name, jump=True)]

        question_response = self.command_question_handler.handle_question()
        if self.command_question_handler.handle_is_proposing_topic_response(
            question_response, TopicModule.MUSIC, dispatcher, tracker
        ):
            return [NextState(states.Initial.name)]
        elif question_response:
            self.logger.debug(f"clear question_response: {question_response}")
            dispatcher.respond(question_response.response)
            tracker.one_turn_store['artist_to_discuss'] = artist_to_discuss
            dispatcher.respond_template('concert/alright', {})
            return [NextState(states.MoreFacts.name)]

        # default

        dispatcher.respond_template('concert/alright', {})
        tracker.one_turn_store['artist_to_discuss'] = artist_to_discuss
        return [NextState(states.MoreFacts.name)]
