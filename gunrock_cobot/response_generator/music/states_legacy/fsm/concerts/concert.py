from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, NextState
from template_manager import Template

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states

from response_generator.music.utils import entity, regex_match


class Concert(MusicState):
    """
    concert 2
    """

    name = 'concert'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        artist_to_discuss = (
            tracker.one_turn_store.get('artist_to_discuss') or
            tracker.last_utt_store.get('artist_to_discuss') or
            None
        )

        self.logger.debug(f"entering")

        if regex_match.is_asking_for_recommendation(tracker):
            self.logger.debug(f"asking for rec. Going to recommend_song")
            tracker.one_turn_store['recommend_song_old_state'] = self.name
            return [NextState(states.RecommendSong.name, jump=True)]

        question_response = self.command_question_handler.handle_question()
        if self.command_question_handler.handle_is_proposing_topic_response(
            question_response, TopicModule.MUSIC, dispatcher, tracker
        ):
            return [NextState(states.Initial.name)]
        elif question_response:
            self.logger.debug(f"clear question_response: {question_response}")
            tracker.one_turn_store['artist_to_discuss'] = artist_to_discuss
            dispatcher.respond(question_response.response)
            if 'ans_neg' in tracker.returnnlp.flattened_lexical_intent:
                dispatcher.respond_template('no_match/continue', {})
                dispatcher.respond_template('concert/ask_other_live', {})
                return [NextState(states.OtherConcert.name)]
            else:
                dispatcher.respond_template('backstory/no_match', {}, Template.social)
                dispatcher.respond_template('concert/ask_most_memorable_with_live', {'no_match': ""})
                return [NextState(states.BestConcert.name)]

        curr_turn_artist = next(iter(entity.ArtistItem.detect(tracker)), None)
        # entity = self.artist_exists(text, ner, knowledge, concept)
        if curr_turn_artist and curr_turn_artist == artist_to_discuss:
            self.logger.debug(f"curr_turn_artist == artist_to_discuss, going to fav_musician")
            return [NextState(states.FavoriteMusician.name, jump=True)]

        if "ans_neg" in tracker.returnnlp.flattened_lexical_intent:
            dispatcher.respond_template('concert/ask_other_live', {})
            tracker.one_turn_store['artist_to_discuss'] = artist_to_discuss
            return [NextState(states.OtherConcert.name)]
        else:
            dispatcher.respond_template('concert/ask_most_memorable_with_live', {'no_match': ''})
            tracker.one_turn_store['artist_to_discuss'] = artist_to_discuss
            return [NextState(states.BestConcert.name)]
