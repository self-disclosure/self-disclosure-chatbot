from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, NextState
from template_manager import Template

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states

from response_generator.music.utils import entity, regex_match


class ConcertInit(MusicState):
    """
    concert 1
    """

    name = 'concert_init'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        artist_to_discuss: entity.ArtistItem = (
            tracker.one_turn_store.get('artist_to_discuss') or
            tracker.last_utt_store.get('artist_to_discuss') or
            None
        )

        self.logger.debug(f"entering")
        if regex_match.is_asking_for_recommendation(tracker):
            self.logger.debug(f"asking for rec. Going to recommend_song")
            tracker.one_turn_store['recommend_song_old_state'] = self.name
            return [NextState(states.RecommendSong.name, jump=True)]

        question_response = self.command_question_handler.handle_question()
        if self.command_question_handler.handle_is_proposing_topic_response(
            question_response, TopicModule.MUSIC, dispatcher, tracker
        ):
            return [NextState(states.Initial.name)]
        elif question_response:
            self.logger.debug(f"clear question_response: {question_response}")
            dispatcher.respond(question_response.response)
            if artist_to_discuss:
                dispatcher.respond_template('no_match/continue', {})
                dispatcher.respond_template('concert/ask_artist_live', {'artist': artist_to_discuss.noun})
            else:
                dispatcher.respond_template('backstory/no_match', {}, Template.social)
                dispatcher.respond_template('concert/ask_most_memorable_with_live', {'no_match': ""})
            tracker.one_turn_store['artist_to_discuss'] = artist_to_discuss
            return [NextState(states.BestConcert.name)]

        # generate acknowledgement

        if regex_match.is_rapport(tracker):
            dispatcher.respond_template('rapport_match', {})
        elif len(tracker.input_text.split(" ")) > 4:
            dispatcher.respond_template('concert/acknowledgement', {})

        if artist_to_discuss:
            dispatcher.respond_template('concert/ask_artist_live', {'artist': artist_to_discuss.noun})
            tracker.one_turn_store['artist_to_discuss'] = artist_to_discuss
            return [NextState(states.Concert.name)]
        else:
            dispatcher.respond_template('backstory/no_match', {}, Template.social)
            dispatcher.respond_template('concert/ask_most_memorable_with_live', {'no_match': ""})
            tracker.one_turn_store['artist_to_discuss'] = artist_to_discuss
            return [NextState(states.BestConcert.name)]
