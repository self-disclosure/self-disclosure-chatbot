from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states
from response_generator.music.states.fsm.chitchat.chitchat_content import ChitchatContent

from response_generator.music.utils import entity, regex_match


selector_root = 'fav_song_by_artist'


class FavSongByArtistPropose(MusicState):

    name = 'fav_song_by_artist_propose'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        artist = (
            tracker.volatile_store.get('artist_to_discuss') or
            tracker.one_turn_store.get('artist_to_discuss') or
            tracker.last_utt_store.get('artist_to_discuss')
        )
        dispatcher.respond_template(f"{selector_root}/propose/default", dict(artist=artist.noun if artist else "the artist"))
        return [NextState(states.FavSongByArtistRespond.name)]


class FavSongByArtistRespond(MusicState):
    """
    aka s_artist_song

    when we don't find an artist in your database
    response to "what's your favorite song by <artist>?"
    """

    name = 'fav_song_by_artist_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        """
        Question Handling
        """
        if (
            self.command_question_handler.has_question()
        ):
            question_response = self.command_question_handler.handle_general_question()

            if self.command_question_handler.handle_is_proposing_topic_response(
                question_response, TopicModule.MUSIC, dispatcher, tracker
            ):
                return [NextState(states.Initial.name)]
            elif question_response:
                # TODO: use commented out code
                # self.logger.debug(f"clear question_response: {question_response}")
                # dispatcher.respond(question_response.response)
                # dispatcher.respond_template('no_match/continue', {})
                # dispatcher.respond_template('artist_question/general', {})
                # return [NextState(states.VerifyChitchat.name)]
                self.logger.debug(f"clear question_response: {question_response}")
                dispatcher.respond(question_response.response)
                return [NextState(states.ChitchatEnter.name, jump=True)]

        curr_turn_artist = next(iter(entity.ArtistItem.detect(tracker)), None)
        if curr_turn_artist:
            self.logger.debug(
                f"[MUSIC] s_artist_song: has curr_turn_artist {curr_turn_artist}, "
                f"going to s_favmusician")
            return [NextState(states.FavoriteMusician.name, jump=True)]

        # song detection
        # TODO: first, look up if the provided name matches something from the artist db
        # TODO: add transition ack
        song = next(iter(entity.SongItem.detect(tracker)), None)
        if song:
            self.logger.debug(f"[MUSIC] s_artist_song: has song {song}, going to s_chitchat")
            dispatcher.respond_template('artist_song_response', {})
            if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                return [NextState(states.Chitchat.name, jump=True)]
            else:
                return [NextState(states.ChangeTopic.name, jump=True)]

        else:
            self.logger.debug(f"[MUSIC] s_artist_song: no song {song}, going to s_chitchat")
            # TODO: acknowledge better even there's no song
            dispatcher.respond("Cool.")
            # patch to add jump to next state
            if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                return [NextState(states.Chitchat.name, jump=True)]
            else:
                return [NextState(states.ChangeTopic.name, jump=True)]

