import random
import re

from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker
from response_generator.fsm2.events import NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states
from response_generator.music.utils import entity, legacy


selector_root = 'fav_artist'


class FavArtistEnter(MusicState):

    name = 'fav_artist_enter'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.FavArtistPropose.name, jump=True)]


class FavArtistPropose(MusicState):

    name = 'fav_artist_propose'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/proposing/default", {})
        return [NextState(states.FavArtistRespond.name)]


class FavArtistRespond(MusicState):

    name = 'fav_artist_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        """
        entities
        """

        artists = entity.ArtistItem.detect(tracker, include_negative_positivity=True)
        artist: entity.ArtistItem = (
            next(filter(lambda artist: artist.positivity, artists), None) or
            next(filter(lambda artist: not artist.positivity, artists), None)
        )
        genre: entity.GenreItem = next(iter(entity.GenreItem.detect(tracker)), None)

        self.logger.debug(f"entities = {artists}, {genre}")

        """
        Question Handling
        """
        if (
            self.command_question_handler.has_question() and
            not (
                self.is_ask_back_response(tracker)
            )
        ):
            question_response = self.command_question_handler.handle_general_question()

            if self.command_question_handler.handle_is_proposing_topic_response(
                question_response, TopicModule.MUSIC, dispatcher, tracker
            ):
                return [NextState(states.Initial.name)]
            elif question_response:
                self.logger.debug(f"clear question_response: {question_response}")
                dispatcher.respond(question_response.response)
                return [NextState(states.ChitchatEnter.name, jump=True)]

        elif artist and artist.positivity:
            self.logger.debug(f"detected artist: {artist}")

            trivia, song = None, None
            if artist.artist_details(tracker):
                self.logger.debug(
                    f"favmusician detected artist db lookup: {artist.noun}, {artist.artist_details(tracker)}")
                artist_details = artist.artist_details(tracker)
                self.logger.debug(f"artist used: {artist}")

                if artist_details.trivias:
                    self.logger.debug(f"favmusician artist_dict has 'trivias': {artist_details.trivias}")
                    trivia = random.choice(artist_details.trivias)
                    if not trivia.strip().endswith('.'):
                        trivia += '.'

                if artist_details.songs:
                    song = random.choice(artist_details.songs)

            if trivia:
                self.logger.debug(f"favmusician propose random facts: {trivia}")
                dispatcher.respond_template(
                    'artist_response',
                    {'artist': artist.noun, 'fact': legacy.replace_pronoun_in_artist_trivia(artist.noun, trivia)}
                )
                if self.is_ask_back_response(tracker):
                    self.ask_back_response(dispatcher)
                tracker.one_turn_store['artist_to_discuss'] = artist
                return [NextState(states.ArtistFactsRespond.name)]

            elif song:
                self.logger.debug(f"favmusician speak artist with album: {artist}, {song}")
                dispatcher.respond_template(
                    'artist_question/follow_album', {'artist': artist.noun.replace(".", " "), 'album': song})
                if self.is_ask_back_response(tracker):
                    self.ask_back_response(dispatcher)
                tracker.one_turn_store['artist_to_discuss'] = artist
                return [NextState(states.SongNameDropRespond.name)]

            else:
                self.logger.debug(f"favmusician speak artist without album: {artist}")
                """
                what's your favorite thing about <artist>
                """
                dispatcher.respond_template(f"{selector_root}/respond/ack_pos", {})
                if self.is_ask_back_response(tracker):
                    self.ask_back_response(dispatcher)
                tracker.one_turn_store['artist_to_discuss'] = artist
                return [NextState(states.WhyLikeArtistPropose.name, jump=True)]

        elif artist and not artist.positivity:
            dispatcher.respond_template(f"{selector_root}/respond/ack_neg", dict())
            tracker.volatile_store['artist_to_discuss'] = artist
            return [NextState(states.FavoriteMusicianDislikeArtistProposing.name, jump=True)]

        elif (
            re.search(
                r'several|many|every artist|multiple| a lot of favorites|like them all',
                tracker.input_text) or
            tracker.central_element.sentiment == 'neg'
        ):
            self.logger.debug(f"favmusician no detected_artist, but 'like them all'")
            dispatcher.respond_template('artist_fav_no_choice', {})
            tracker.one_turn_store['chitchat_chain_with_ack'] = False

        else:
            dispatcher.respond_template('transition/artist_blanket_ack/to_chitchat', {})
            # if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
            #     dispatcher.respond_template('transition/artist_blanket_ack/speaking_of_music', {})
            tracker.one_turn_store['chitchat_chain_with_ack'] = False

        return [NextState(states.ChitchatEnter.name)]

