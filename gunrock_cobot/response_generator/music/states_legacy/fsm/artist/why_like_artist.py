from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states

from response_generator.music.utils import entity, regex_match


selector_root = 'why_like_artist'


class WhyLikeArtistPropose(MusicState):

    name = 'why_like_artist_propose'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        artist = (
            tracker.volatile_store.get('artist_to_discuss') or
            tracker.one_turn_store.get('artist_to_discuss') or
            tracker.last_utt_store.get('artist_to_discuss')
        )
        dispatcher.respond_template(f"{selector_root}/propose/default",
                                    dict(artist=artist.noun if artist else "the artist"))
        return [NextState(states.WhyLikeArtistRespond.name)]


class WhyLikeArtistRespond(MusicState):
    """
    aka s_artist_question

    find an artist but not in our database
    answers "why do you like <artist>"
    ask "what is your favorite song by <artist>"
    """

    name = 'why_like_artist_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        artist_to_discuss: entity.ArtistItem = tracker.last_utt_store.get('artist_to_discuss')
        curr_turn_artist: entity.ArtistItem = next(iter(entity.ArtistItem.detect(tracker)), None)
        song: entity.SongItem = next(iter(entity.SongItem.detect(tracker)), None)

        self.logger.debug(
            f"artist_to_discuss = {artist_to_discuss}, "
            f"curr_turn_artist = {curr_turn_artist}, "
            f"song = {song}"
        )

        if curr_turn_artist:
            self.logger.debug(f"[MUSIC] s_artist_question curr_turn_artist exists. going to fav_musician")
            # TODO: check this transition
            return [NextState(states.FavArtistEnter.name, jump=True)]

        """
        Question Handling
        """
        if (
            self.command_question_handler.has_question() and
            not (
                self.is_ask_back_response(tracker)
            )
        ):
            question_response = self.command_question_handler.handle_general_question()

            if self.command_question_handler.handle_is_proposing_topic_response(
                question_response, TopicModule.MUSIC, dispatcher, tracker
            ):
                return [NextState(states.Initial.name)]
            elif question_response:
                self.logger.debug(f"clear question_response: {question_response}")
                dispatcher.respond(question_response.response)
                return [NextState(states.ChitchatEnter.name, jump=True)]

        elif 'ans_neg' in tracker.returnnlp.flattened_lexical_intent:
            self.logger.debug(f"[MUSIC] s_artist_question: ans_neg")
            return [NextState(states.ChitchatEnter.name, jump=True)]

        elif song and artist_to_discuss.noun.lower() == song.artist_name.lower():
            self.logger.debug(f"jumping to s_artist_song: {song}, {artist_to_discuss}")
            tracker.volatile_store['jump_from'] = 's_artist_question'
            # return [NextState(states.FavSongByArtistPropose.name, jump=True)]
            return [NextState(states.FavoriteSongByArtistRespond.name, jump=True)]

        elif regex_match.is_other_answer(tracker):
            self.logger.debug(f"[MUSIC] s_artist_question: is_other_answer response")
            dispatcher.respond_template(f"{selector_root}/respond/dunno", {})

        else:
            self.logger.debug(f"[MUSIC] s_artist_question: default response")
            dispatcher.respond_template(f"{selector_root}/respond/default", {})

        tracker.one_turn_store['artist_to_discuss'] = artist_to_discuss
        return [NextState(states.FavSongByArtistPropose.name, jump=True)]
