from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker
from response_generator.fsm2.events import NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states
from response_generator.music.utils import entity, legacy


selector_root = 'fav_artist'


class DislikeArtistPropose(MusicState):

    name = 'dislike_artist_propose'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        artist: entity.ArtistItem = (
            tracker.volatile_store.get('artist_to_discuss') or
            tracker.one_turn_store.get('artist_to_discuss') or
            tracker.last_utt_store.get('artist_to_discuss')
        )
        dispatcher.respond_template('dislike_artist/proposing', dict(artist=artist.noun if artist else 'the artist'))
        return [NextState(states.DislikeArtistRespond.name, jump=True)]


class DislikeArtistRespond(MusicState):

    name = 'dislike_artist_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        """
        Question Handling
        """
        if (
            self.command_question_handler.has_question() and
            not (
                self.is_ask_back_response(tracker)
            )
        ):
            question_response = self.command_question_handler.handle_general_question()

            if self.command_question_handler.handle_is_proposing_topic_response(
                question_response, TopicModule.MUSIC, dispatcher, tracker
            ):
                return [NextState(states.Initial.name)]
            elif question_response:
                self.logger.debug(f"clear question_response: {question_response}")
                dispatcher.respond(question_response.response)
                return [NextState(states.ChitchatEnter.name, jump=True)]

        else:
            dispatcher.respond_template('dislike_artist/respond/default', dict())
            return [NextState(states.ChitchatEnter.name, jump=True)]
