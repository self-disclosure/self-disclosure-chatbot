from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states
from response_generator.music.states.fsm.chitchat.chitchat_content import ChitchatContent

from response_generator.music.utils import entity, regex_match


class WhyLikeArtistRespond(MusicState):
    """
    aka s_artist_question

    find an artist but not in our database
    answers "why do you like <artist>"
    ask "what is your favorite song by <artist>"
    """

    name = 'why_like_artist_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        artist_to_discuss: entity.ArtistItem = tracker.last_utt_store.get('artist_to_discuss')

        self.logger.debug(f"entering. artist_to_discuss: {artist_to_discuss}")

        curr_turn_artist: entity.ArtistItem = next(iter(entity.ArtistItem.detect(tracker)), None)
        if curr_turn_artist:
            self.logger.debug(f"[MUSIC] s_artist_question curr_turn_artist exists. going to fav_musician")
            return [NextState(states.FavoriteMusician.name, jump=True)]

        self.logger.debug(f"handle question")
        question_response = self.command_question_handler.handle_question()
        if self.command_question_handler.handle_is_proposing_topic_response(
            question_response, TopicModule.MUSIC, dispatcher, tracker
        ):
            return [NextState(states.Initial.name)]
        elif question_response:
            self.logger.debug(f"clear question_response: {question_response}")
            dispatcher.respond(question_response.response)
            dispatcher.respond_template('no_match/continue', {})
            if artist_to_discuss:
                dispatcher.respond_template('fav_song/ask_song_of_artist', {'artist': artist_to_discuss.noun})
                return [NextState(states.FavoriteSongByArtistRespond.name)]
            else:
                dispatcher.respond_template('artist_question/general', {})
                if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                    return [NextState(states.Chitchat.name)]
                else:
                    return [NextState(states.ChangeTopic.name)]

        if 'ans_neg' in tracker.returnnlp.flattened_lexical_intent:
            self.logger.debug(f"[MUSIC] s_artist_question: ans_neg")
            if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                return [NextState(states.Chitchat.name, jump=True)]
            else:
                return [NextState(states.ChangeTopic.name, jump=True)]

        song: entity.SongItem = next(iter(entity.SongItem.detect(tracker)), None)
        self.logger.debug(
            f"detected song: {song}, {song and artist_to_discuss.noun.lower() == song.artist_name.lower()}")
        if song and artist_to_discuss.noun.lower() == song.artist_name.lower():
            self.logger.debug(f"jumping to s_artist_song: {song}, {artist_to_discuss}")
            tracker.volatile_store['jump_from'] = 's_artist_question'
            return [NextState(states.FavoriteSongByArtistRespond.name, jump=True)]

        if regex_match.is_other_answer(tracker):
            self.logger.debug(f"[MUSIC] s_artist_question: is_other_answer response")
            dispatcher.respond_template('fav_song/ask_song_of_artist_unmatch', {'artist': artist_to_discuss.noun})

        else:
            self.logger.debug(f"[MUSIC] s_artist_question: default response")
            dispatcher.respond_template('fav_song/ask_song_of_artist', {'artist': artist_to_discuss.noun})

        tracker.one_turn_store['artist_to_discuss'] = artist_to_discuss
        return [NextState(states.FavoriteSongByArtistRespond.name)]
