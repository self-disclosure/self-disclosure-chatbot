import random
import re
from dataclasses import dataclass
from typing import List

from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, NextState, FSMLogger
from template_manager import TemplateManager

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states
from response_generator.music.states.fsm.chitchat.chitchat_content import ChitchatContent

from response_generator.music.utils import dynamodb, entity, regex_match


@dataclass
class TemplateArtist:

    keyword: str
    artist: str

    @classmethod
    def artist_map(cls):
        return {
            'michael_jackson': 'mj',
            'taylor_swift': 'ts',
            'imagine_dragons': 'id',
            'maroon_5': 'm5',
            'drake': 'dg',
        }

    @classmethod
    def first_entry(cls, artist: str, default_artist=True):
        if artist not in cls.artist_map():
            FSMLogger('MUSIC', 'TemplateArtist', 'music.states').warning(
                f"artist \"{artist}\" not in mapping, default_artist is {default_artist}"
            )
            if default_artist:
                artist = 'taylor_swift'
        if artist in cls.artist_map():
            return cls(keyword=cls.artist_map()[artist], artist=artist)

    @classmethod
    def from_keyword(cls, artist: str, keyword: str = None):
        if not keyword:
            return cls.first_entry(artist)
        elif artist in cls.artist_map():
            return cls(keyword=keyword, artist=artist)

    def speak(self, tracker: Tracker, dispatcher: Dispatcher):
        selector = f"template_artist/{self.artist}/{self.keyword}"
        tracker.persistent_store[f'template_artist_history_<{self.artist}>'] = (
            tracker.persistent_store.get(f'template_artist_history_<{self.artist}>') or []
        ) + [self.keyword]
        args = {}
        response = dispatcher.tm.speak(selector, {}, embedded_info=args)
        return response, args

    @classmethod
    def count(cls, artist: str, tracker: Tracker):
        return len((tracker.persistent_store.get(f'template_artist_history_<{artist}>') or []))

    @classmethod
    def remaining(cls, artist: str, tracker: Tracker, tm_music: TemplateManager):
        return len(cls._remaining_keywords(artist, tracker, tm_music)) - cls.count(artist, tracker)

    @classmethod
    def _remaining_keywords(cls, artist: str, tracker: Tracker, tm_music: TemplateManager):
        selector = f'template_artist/{artist}'
        keywords = tm_music.get_child_keys(selector)
        used_keywords = (tracker.persistent_store.get(f'template_artist_history_<{artist}>') or [])
        return [keyword for keyword in keywords if keyword not in used_keywords]

    @classmethod
    def _reset_remaining(cls, artist: str, tracker: Tracker):
        tracker.persistent_store[f'template_artist_history_<{artist}>'] = []


class TemplateArtistAck(MusicState):
    """
    aka s_templates

    top 5 artist
    """

    name = 'template_artist_ack'

    def setup(self, dispatcher: Dispatcher, tracker: Tracker):
        super().setup(dispatcher, tracker)
        self.template_current: TemplateArtist = (
            tracker.one_turn_store.get('template_current') or
            tracker.last_utt_store.get('template_current') or
            None
        )
        self.template_keyword: List[str, str] = (
            tracker.one_turn_store.get('template_keyword') or
            tracker.last_utt_store.get('template_keyword') or
            None
        )

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        preferred_artist: str = (
            tracker.one_turn_store.get('template_artist_ack_artist') or
            tracker.last_utt_store.get('template_artist_ack_artist') or
            ""
        )

        self.logger.debug(
            f"begin decision: \"{tracker.input_text}\", "
            f"preferred_artist = {preferred_artist}, "
            f"template_current = {self.template_current}, "
            f"artist_to_discuss = {self.artist_to_discuss}"
        )

        if re.search(r"why do you like|oh really", tracker.input_text):
            self.logger.debug(f"why do you like")
            tracker.one_turn_store['artist_to_discuss'] = self.artist_to_discuss
            tracker.one_turn_store['template_artist_ack_artist'] = preferred_artist
            dispatcher.respond_template('templates/why_love', {})
            return [NextState(self.name)]

        if self.template_keyword:
            self.logger.debug(f"has template_keyword: {self.template_keyword}")
            template_current = TemplateArtist.from_keyword(*self.template_keyword)
        elif preferred_artist:
            self.logger.debug(f"preferred_artist is not None: {preferred_artist}")
            template_current = TemplateArtist.first_entry(preferred_artist)
            tracker.one_turn_store['template_current'] = template_current
            self.logger.debug(f"template_current is not None templates done: {template_current}")
        else:
            template_current = self.template_current

        if regex_match.is_asking_for_recommendation(tracker):
            self.logger.debug(f"asking for rec. Going to recommend_song")
            tracker.one_turn_store['recommend_song_old_state'] = states.Chitchat.name
            return [NextState(states.RecommendSong.name, jump=True)]

        skip_artist_check = False
        skip_dialog_check = False

        if (
            re.search(r'can we talk about', tracker.input_text) and
            re.search(r"taylor swift|michael jackson|imagine dragons|maroon 5|drake|adam levine",
                      tracker.input_text)
        ):
            self.logger.debug(f"skip_dialog_check (match handpicked artist)")
            skip_dialog_check = True

        if re.search(r'imagine dragons', tracker.input_text):
            self.logger.debug(f"skip_dialog_check (match imagine dragons)")
            skip_dialog_check = True

        if template_current and template_current.keyword == "fav_song":
            self.logger.debug(f"skip_dialog_check, skip_artist_check (current in templates)")
            skip_dialog_check = True
            skip_artist_check = True

        # detected = self.artist_exists(text, ner, knowledge, concept)
        curr_turn_artist: entity.ArtistItem = next(iter(entity.ArtistItem.detect(tracker)), None)
        if curr_turn_artist and not skip_artist_check:
            self.logger.debug(f"artist_exists and not skip_artist_check")

            if (
                curr_turn_artist != self.artist_to_discuss and
                not re.search(
                    r"taylor swift|michael jackson|imagine dragons|maroon 5|drake|adam levine",
                    tracker.input_text) and
                tracker.central_element.sentiment != 'neg'
            ):
                self.logger.debug(f"jumping to s_favmusician")
                return [NextState(states.FavoriteMusician.name, jump=True)]

            elif curr_turn_artist == self.artist_to_discuss:
                self.logger.debug(
                    f"jumping to s_chitchat; "
                    f"detected {curr_turn_artist}, input_dict['artist'] {self.artist_to_discuss}")
                if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                    return [NextState(states.Chitchat.name, jump=True)]
                else:
                    return [NextState(states.ChangeTopic.name, jump=True)]

        question_response = None
        if not skip_dialog_check:
            self.logger.debug(f"handle question")

            question_response = self.command_question_handler.handle_question()
            if self.command_question_handler.handle_is_proposing_topic_response(
                question_response, TopicModule.MUSIC, dispatcher, tracker
            ):
                return [NextState(states.Initial.name)]
            elif question_response:
                self.logger.debug(f"clear question_response: {question_response}")

        if not preferred_artist or preferred_artist not in TemplateArtist.artist_map():
            self.logger.debug(f"preferred_artist is invalid {preferred_artist}")
            preferred_artist = random.choice(list(TemplateArtist.artist_map().keys()))

        if not self.artist_to_discuss:
            self.logger.debug(f"input_dict['artist'] is empty dict")
            self.artist_to_discuss = entity.ArtistItem.from_regex_match(
                preferred_artist.replace('_', ' '),
                preferred_artist.replace('_', ' ')
            )
            tracker.one_turn_store['artist_to_discuss'] = self.artist_to_discuss

        if TemplateArtist.remaining(preferred_artist, tracker, dispatcher.tm):
            self.logger.debug(f"'remaining' in templates: {template_current}")
        else:
            self.logger.debug(f"'remaining' not in templates: {template_current}")
            if regex_match.is_rapport(tracker):
                self.logger.debug(f"is rapport, going taylor swift")
                tracker.one_turn_store['artist_to_discuss'] = entity.ArtistItem.from_regex_match(
                    'taylor swift', 'taylor swift'
                )
                preferred_artist = 'taylor_swift'
            TemplateArtist._reset_remaining(preferred_artist)
            template_current = TemplateArtist.first_entry(preferred_artist)
            # template is split in two ways,
            # preferred_artist is passed (originally as param) to initiate template of an artist
            # template_current is passed when prev_turn needs to say the response, and the args is passed to next
            # see below with interaction with keywords
            tracker.one_turn_store['template_current'] = template_current
            response, args = template_current.speak(tracker, dispatcher)
            dispatcher.respond(response)
            tracker.one_turn_store['template_keyword'] = [template_current.artist, None]
            tracker.one_turn_store['template_artist_ack_artist'] = preferred_artist
            tracker.one_turn_store['artist_to_discuss'] = self.artist_to_discuss
            return [NextState(self.name)]

        # get current template

        template_utt, template_args = template_current.speak(tracker, dispatcher)
        matched_song = None
        keyword = template_args['pos_response']

        if template_current and template_current.keyword == 'fav_song':
            self.logger.debug(f"current keyword is 'fav_song'")
            keyword = template_args['neg_response']
            match = template_args.get('match')
            match_text = template_args.get('match_text')
            if self.artist_to_discuss:
                songs_by_artist = dynamodb.get_songs_by_artist(self.artist_to_discuss.noun.lower())
            else:
                songs_by_artist = None
            curr_turn_song: entity.SongItem = next(iter(entity.SongItem.detect(tracker)), None)
            matched_song = None

            if songs_by_artist:
                self.logger.debug(f"song_by_artist from dynamodb: {songs_by_artist}")
                for song in songs_by_artist:
                    pattern = re.compile(re.escape(song.lower()))
                    if re.search(pattern, tracker.input_text):
                        matched_song = song
            if (
                (not matched_song or not songs_by_artist) and
                curr_turn_song and
                curr_turn_song.artist_name.lower() == template_current.artist.replace('_', ' ')
            ):
                matched_song = curr_turn_song.noun
                self.logger.debug(f"matched_song using curr_turn_song: {curr_turn_song}")

            if match and match_text and re.search(match, tracker.input_text):
                dispatcher.respond(match_text)
                tracker.one_turn_store['artist_to_discuss'] = self.artist_to_discuss
                if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                    return [NextState(states.Chitchat.name)]
                else:
                    return [NextState(states.ChangeTopic.name)]

        if (
            'ans_neg' in tracker.returnnlp.flattened_lexical_intent and
            'req_topic_jump' not in tracker.returnnlp.flattened_sys_intent
        ):
            self.logger.debug(f"ans_neg in lexical")
            keyword = template_args['neg_response']
            dispatcher.respond("Alright.")

        elif regex_match.is_other_answer(tracker):
            self.logger.debug(f"unknown_match")
            keyword = template_args['neg_response']
            dispatcher.respond("Alright.")

        if matched_song:
            self.logger.debug(f"matched_song: {matched_song}")
            dispatcher.respond(f"{matched_song} is a good choice.")

        if keyword == 'exit':
            self.logger.debug(f"keyword == 'exit'")
            tracker.one_turn_store['artist_to_discuss'] = self.artist_to_discuss
            if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                return [NextState(states.Chitchat.name, jump=True)]
            else:
                return [NextState(states.ChangeTopic.name, jump=True)]

        # getting the next template
        self.logger.debug(f"looping templates")
        template_current = TemplateArtist.from_keyword(template_current.artist, keyword)
        template_utt, template_args = template_current.speak(tracker, dispatcher)
        # dispatcher.respond(template_utt)

        acknowledgement = ''
        if len(tracker.input_text.split(" ")) > 4 and not regex_match.is_lets_talk_about(tracker):
            # skip this if "lets talk about michael jackson.
            # Should've been coming from s_favmusician, and is a global jump"
            acknowledgement = dispatcher.tm.speak("long_sentences_ack", {})

        if (
            self.artist_to_discuss and
            all(i in tracker.returnnlp.flattened_lexical_intent for i in ['ans_neg', 'ask_leave'])
        ):
            self.logger.debug(f"lexical_intent is ['ans_neg', 'ask_leave']")
            tracker.one_turn_store['artist_to_discuss'] = self.artist_to_discuss
            dispatcher.respond(
                f"It seems you are not a {self.artist_to_discuss} fan. "
                f"Would you like to keep talking about {self.artist_to_discuss} ? ",
            )
            tracker.one_turn_store['artist_to_discuss'] = self.artist_to_discuss
            return [NextState(states.VerifyTemplateArtist.name)]

        elif question_response:
            dispatcher.respond(question_response.response)
            dispatcher.respond(f"So, speaking of {self.artist_to_discuss.noun},")
            dispatcher.respond(template_utt)
            tracker.one_turn_store['template_keyword'] = [template_current.artist, keyword]
            tracker.one_turn_store['artist_to_discuss'] = self.artist_to_discuss
            return [NextState(self.name)]
        else:
            dispatcher.respond(acknowledgement)
            dispatcher.respond(template_utt)
            tracker.one_turn_store['template_keyword'] = [template_current.artist, keyword]
            tracker.one_turn_store['artist_to_discuss'] = self.artist_to_discuss
            return [NextState(self.name)]
