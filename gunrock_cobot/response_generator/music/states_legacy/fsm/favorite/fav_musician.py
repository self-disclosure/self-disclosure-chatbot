import random
import re

from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states
from response_generator.music.states.fsm.chitchat.chitchat_content import ChitchatContent

from response_generator.music.constants import genre_artist_dict
from response_generator.music.utils import entity, regex_match
from response_generator.music.utils.legacy import replace_pronoun_in_artist_trivia


selector_root = "fav_musician"


class FavoriteMusicianProposing(MusicState):
    """
    respond to question who's your favorite singer
    """

    name = 'fav_musician_proposing'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/proposing", {})
        return [NextState(states.FavoriteMusician.name)]


class FavoriteMusicianRespond(MusicState):
    """
    respond to question who's your favorite singer
    """

    name = 'fav_musician_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/proposing", {})
        return [NextState(states.FavoriteMusician.name)]


class FavoriteMusician(MusicState):
    """
    respond to question who's your favorite singer
    """

    name = 'fav_musician'

    def ask_back_response(self, dispatcher: Dispatcher):
        dispatcher.respond_template(f"{selector_root}/respond/my_favorite", {})

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        artists = entity.ArtistItem.detect(tracker, include_negative_positivity=True)
        artist: entity.ArtistItem = (
            next(filter(lambda artist: artist.positivity, artists), None) or
            next(filter(lambda artist: not artist.positivity, artists), None)
        )
        genre: entity.GenreItem = next(iter(entity.GenreItem.detect(tracker)), None)

        self.logger.debug(f"entities = {artists}, {genre}")

        if self.command_question_handler.has_question():
            if self.is_ask_back_response(tracker):
                pass

            # general question
            else:
                question_response = self.command_question_handler.handle_general_question()

                if self.command_question_handler.handle_is_proposing_topic_response(
                    question_response, TopicModule.MUSIC, dispatcher, tracker
                ):
                    return [NextState(states.Initial.name)]
                elif question_response:
                    self.logger.debug(f"clear question_response: {question_response}")
                    dispatcher.respond(question_response.response)
                    dispatcher.respond_template('no_match/continue', {})
                    dispatcher.respond_template('artist_question/general', {})
                    # return [NextState(states.FavoriteMusicianProposing.name, jump=True)]
                    if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                        return [NextState(states.Chitchat.name)]
                    else:
                        return [NextState(states.ChangeTopic.name)]

        if regex_match.is_rapport(tracker):
            self.logger.debug(f"favmusician using rapport")
            tracker.one_turn_store['template_artist_ack_artist'] = 'taylor swift'
            return [NextState(states.TemplateArtistAck.name)]

        elif artist and artist.positivity:
            self.logger.debug(f"detected artist: {artist}")

            trivia, song = None, None
            if artist.artist_details(tracker):
                self.logger.debug(
                    f"favmusician detected artist db lookup: {artist.noun}, {artist.artist_details(tracker)}")
                artist_details = artist.artist_details(tracker)
                self.logger.debug(f"artist used: {artist}")

                if artist_details.trivias:
                    self.logger.debug(f"favmusician artist_dict has 'trivias': {artist_details.trivias}")
                    trivia = random.choice(artist_details.trivias)
                    if not trivia.strip().endswith('.'):
                        trivia += '.'

                if artist_details.songs:
                    song = random.choice(artist_details.songs)

            if trivia:
                self.logger.debug(f"favmusician propose random facts: {trivia}")
                dispatcher.respond_template(
                    'artist_response',
                    {'artist': artist.noun, 'fact': replace_pronoun_in_artist_trivia(artist.noun, trivia)}
                )
                if self.is_ask_back_response(tracker):
                    self.ask_back_response(dispatcher)
                tracker.one_turn_store['artist_to_discuss'] = artist
                return [NextState(states.ArtistFactsRespond.name)]

            elif song:
                self.logger.debug(f"favmusician speak artist with album: {artist}, {song}")
                dispatcher.respond_template(
                    'artist_question/follow_album', {'artist': artist.noun.replace(".", " "), 'album': song})
                if self.is_ask_back_response(tracker):
                    self.ask_back_response(dispatcher)
                tracker.one_turn_store['artist_to_discuss'] = artist
                return [NextState(states.SongNameDropRespond.name)]

            else:
                self.logger.debug(f"favmusician speak artist without album: {artist}")
                """
                what's your favorite thing about <artist>
                """
                dispatcher.respond_template(
                    'artist_question/why_artist', {'artist': artist.noun.replace(".", " ")})
                if self.is_ask_back_response(tracker):
                    self.ask_back_response(dispatcher)
                tracker.one_turn_store['artist_to_discuss'] = artist
                return [NextState(states.WhyLikeArtistRespond.name)]

        elif artist and not artist.positivity:
            dispatcher.respond_template(f"{selector_root}/respond/ack_neg", dict())
            tracker.volatile_store['artist_to_discuss'] = artist
            return [NextState(states.FavoriteMusicianDislikeArtistProposing.name, jump=True)]

        if (
            genre and tracker.central_element.sentiment != 'neg' and
            not re.search(r'panic at the disco', tracker.input_text)
        ):
            # if (self.redis_helper.is_known_genre(genre.replace(" music", ""))) and genre != input_dict['genre']:
            # TODO: check this
            if genre != tracker.last_utt_store.get('genre_to_discuss'):
                if (
                    not self.redis_helper.is_returning_user_in_music_module(
                        tracker.user_attributes._get_raw('user_id')) and
                    'pop' in genre.noun.lower()
                ):
                    dispatcher.respond_template("artist_question/follow_genre_question/pop", {})
                else:
                    dispatcher.respond_template("artist_question/follow_genre_question/no_pop", {'genre': genre.noun})
                tracker.one_turn_store['genre_to_discuss'] = genre
                return [NextState(self.name)]
        elif tracker.central_element.sentiment != 'neg':
            _regex = re.search(
                (r"taylor swift|michael jackson|imagine dragons|drake|maroon|adam levine"), tracker.input_text, re.I)
            self.logger.debug(
                f"favmusician going thru non neg sentiment: {tracker.input_text} regex: {_regex}")

            if _regex:
                if re.search(r"don't like", tracker.input_text):
                    if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                        return [NextState(states.Chitchat.name, jump=True)]
                    else:
                        return [NextState(states.ChangeTopic.name, jump=True)]

                elif re.search(r'taylor swift', tracker.input_text):
                    tracker.one_turn_store['template_artist_ack_artist'] = 'taylor_swift'
                elif re.search(r'michael jackson', tracker.input_text):
                    tracker.one_turn_store['template_artist_ack_artist'] = 'michael_jackson'
                elif re.search(r'drake', tracker.input_text):
                    tracker.one_turn_store['template_artist_ack_artist'] = 'drake'
                elif re.search(r'imagine dragons', tracker.input_text):
                    tracker.one_turn_store['template_artist_ack_artist'] = 'imagine_dragons'
                elif re.search(r'(maroon|adam levine)', tracker.input_text):
                    tracker.one_turn_store['template_artist_ack_artist'] = 'maroon_5'
                self.logger.debug(
                    f"favmusician jumping to templates_artist_ack: "
                    f"artist = {tracker.one_turn_store.get('template_artist_ack_artist')}")

                return [NextState(states.TemplateArtistAck.name, jump=True)]

        elif regex_match.is_other_answer(tracker):
            self.logger.debug(f"favmusician get i don't know as answer, going to chitchat")
            dispatcher.respond_template('artist_fav_no_choice', {})

        elif (
            re.search(
                r'several|many|every artist|multiple| a lot of favorites|like them all',
                tracker.input_text) or
            tracker.central_element.sentiment == 'neg'
        ):
            self.logger.debug(f"favmusician no detected_artist, but 'like them all'")
            dispatcher.respond_template('artist_fav_no_choice', {})
            tracker.one_turn_store['chitchat_chain_with_ack'] = False

        elif regex_match.is_asking_your_favorite(tracker):
            dispatcher.respond_template(
                'artist_fav',
                {'artist': genre_artist_dict[tracker.last_utt_store.get('genre_to_discuss') or 'pop']}
            )
            tracker.one_turn_store['chitchat_chain_with_ack'] = True

        else:
            dispatcher.respond_template('transition/artist_blanket_ack/to_chitchat', {})
            if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                dispatcher.respond_template('transition/artist_blanket_ack/speaking_of_music', {})
            tracker.one_turn_store['chitchat_chain_with_ack'] = False

        self.logger.debug(f"favmusician no detected artist, jumping to chitchat")

        if self.is_ask_back_response(tracker):
            self.ask_back_response(dispatcher)

        if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
            return [NextState(states.Chitchat.name, jump=True)]
        else:
            return [NextState(states.ChangeTopic.name, jump=True)]


class FavoriteMusicianDislikeArtistProposing(MusicState):

    name = 'fav_musician_dislike_artist_proposing'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        artist: entity.ArtistItem = (
            tracker.volatile_store.get('artist_to_discuss') or
            tracker.one_turn_store.get('artist_to_discuss') or
            tracker.last_utt_store.get('artist_to_discuss')
        )
        dispatcher.respond_template('dislike_artist/proposing', dict(artist=artist.noun if artist else 'the artist'))
        return [NextState(states.FavoriteMusicianDislikeArtistRespond.name)]


class FavoriteMusicianDislikeArtistRespond(MusicState):

    name = 'fav_musician_dislike_artist_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if self.command_question_handler.has_question():
            if not (
                self.is_ask_back_response(tracker)
            ):
                question_response = self.command_question_handler.handle_general_question()

                if self.command_question_handler.handle_is_proposing_topic_response(
                    question_response, TopicModule.MUSIC, dispatcher, tracker
                ):
                    return [NextState(states.Initial.name)]
                elif question_response:
                    self.logger.debug(f"clear question_response: {question_response}")
                    dispatcher.respond(question_response.response)
                    dispatcher.respond_template('no_match/continue', {})
                    dispatcher.respond_template('artist_question/general', {})
                    # return [NextState(states.FavoriteMusicianProposing.name, jump=True)]
                    if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                        return [NextState(states.Chitchat.name)]
                    else:
                        return [NextState(states.ChangeTopic.name)]

        dispatcher.respond_template('dislike_artist/respond/default', dict())
        if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
            return [NextState(states.Chitchat.name, jump=True)]
        else:
            return [NextState(states.ChangeTopic.name, jump=True)]
