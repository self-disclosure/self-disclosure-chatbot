import re
from typing import List

from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states
from response_generator.music.states.fsm.chitchat.chitchat_content import ChitchatContent

from response_generator.music.utils import dynamodb, entity, regex_match


class FavoriteSong(MusicState):
    """
    opinionsong followup
    """

    name = 'fav_song'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        self.logger.debug(f"entering")

        tracker.one_turn_store['artist_to_discuss'] = self.artist_to_discuss
        tracker.one_turn_store['current_chitchat'] = self.current_chitchat

        if regex_match.is_other_answer(tracker) or 'ans_unknown' in tracker.returnnlp.flattened_lexical_intent:
            self.logger.debug(f"is other answer")
            dispatcher.respond_template('unknown_ack', {})
            if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                return [NextState(states.Chitchat.name)]
            else:
                return [NextState(states.ChangeTopic.name)]

        if regex_match.is_rapport(tracker):
            self.logger.debug(f"is rapport")
            dispatcher.respond_template('fav_song/rap', {})
            if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                return [NextState(states.Chitchat.name)]
            else:
                return [NextState(states.ChangeTopic.name)]

        bot_said_artist: entity.ArtistItem = tracker.last_utt_store.get('bot_said_artist')

        artist_songs: List[str] = (
            bot_said_artist.songs_from_db(tracker) if bot_said_artist else
            dynamodb.get_songs_by_artist(self.artist_to_discuss.noun.lower()) if self.artist_to_discuss else
            []
        )

        for song in artist_songs:
            match = re.search(re.escape(song), tracker.input_text, re.I)
            self.logger.debug(f"[MUSIC] s_favsong: artist_songs: {artist_songs}, {match}")
            if match:
                dispatcher.respond_template('fav_song/with_song', {'song': match.group()})
                # TODO: jump?
                if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                    return [NextState(states.Chitchat.name)]
                else:
                    return [NextState(states.ChangeTopic.name)]

        self.logger.debug(f"handle question")

        question_response = self.command_question_handler.handle_question()
        if self.command_question_handler.handle_is_proposing_topic_response(
            question_response, TopicModule.MUSIC, dispatcher, tracker
        ):
            return [NextState(states.Initial.name)]
        elif question_response:
            self.logger.debug(f"clear question_response: {question_response}")
            dispatcher.respond(question_response.response)
            if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                return [NextState(states.Chitchat.name, jump=True)]
            else:
                return [NextState(states.ChangeTopic.name, jump=True)]

        if re.search(r"^no$|^yes$|^yeah$", tracker.input_text):
            if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                return [NextState(states.Chitchat.name, jump=True)]
            else:
                return [NextState(states.ChangeTopic.name, jump=True)]

        if (
            re.search(
                r"all of them|all of her songs|all of his songs|all their songs|all her songs|all his songs",
                tracker.input_text
            ) and
            self.artist_to_discuss
        ):
            dispatcher.respond("<say-as interpret-as=\"interjection\">wow</say-as>! you are a big fan of {}.".format(
                self.artist_to_discuss.noun
            ))

        else:
            dispatcher.respond_template('fav_song/general', {})

        if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
            return [NextState(states.Chitchat.name)]
        else:
            return [NextState(states.ChangeTopic.name)]
