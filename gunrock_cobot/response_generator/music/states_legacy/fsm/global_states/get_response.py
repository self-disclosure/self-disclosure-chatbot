import re

from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states
from response_generator.music.states.fsm.chitchat.chitchat_content import ChitchatContent

from response_generator.music.utils import entity, regex_match


class GetResponse(MusicState):
    """
    """

    name = 'get_response'
    special_type = {'global_state'}

    def setup(self, dispatcher: Dispatcher, tracker: Tracker):
        super().setup(dispatcher, tracker)

        self.exit_flag = (
            tracker.one_turn_store.get('exit') or
            tracker.last_utt_store.get('exit') or
            None
        )
        self.song_question = (
            tracker.one_turn_store.get('song_question') or
            tracker.last_utt_store.get('song_question') or
            None
        )

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker):

        if self.exit_flag:
            self.logger.debug(f"entering input_dict['exit'] condition")
            return self.name

        elif (
            regex_match.is_exit(tracker) and
            not self.song_question and
            self.module.state_tracker.curr_turn()[0] != states.Initial.name and
            not re.search(r"music", tracker.input_text)
        ):
            self.logger.debug(f"entering exit_match, not input_dict['song_question']")
            return self.name

        # TODO: Fix repeat
        # elif regex_match.is_repeat(tracker) and input_dict['last_response'] is not None:
        #     logger.debug(f"[MUSIC] getresponse: entering repeat_match and input_dict['last_response'] is not None")
        #     return {
        #         "response": input_dict["last_response"],
        #         "currentstate": input_dict["currentstate"],
        #         "context": input_dict["context"],
        #         "artist": input_dict["artist"],
        #         "sim_artist": input_dict["sim_artist"],
        #         "propose_continue": "CONTINUE",
        #         "first_time": input_dict["first_time"],
        #         "chitchats": input_dict["chitchats"],
        #         "templates": input_dict["templates"],
        #         "exit": input_dict["exit"],
        #     }

        elif regex_match.is_play_music(tracker):
            self.logger.debug(f"is_play_music")
            artist: entity.ArtistItem = next(iter(entity.ArtistItem.detect(tracker)), None)
            tracker.one_turn_store['artist_to_discuss'] = artist
            tracker.one_turn_store['play_music_next_state'] = states.Initial.name
            return states.PlayMusicRespond.name

        elif regex_match.is_command_sing_a_song(tracker):
            self.logger.debug(f"entering sing a song that's not backstory")
            return states.Singing.name

        elif regex_match.is_user_sing_us_a_song(tracker):
            self.logger.debug(f"user wants to sing us a song")
            return states.UserSing.name

        elif regex_match.is_lets_talk_about(tracker):
            self.logger.debug(f"is lets talk about: {tracker.input_text}")

            artist: entity.ArtistItem = next(iter(entity.ArtistItem.detect(tracker)), None)
            self.logger.debug(f"is lets talk about: artist: {artist}")
            if artist:
                self.logger.debug(f"is lets talk about: jumping to s_favmusician")
                return states.FavoriteMusician.name

        elif regex_match.is_asking_for_recommendation(tracker):
            self.logger.debug(f"asking for rec. Going to recommend_song")
            return [NextState(states.RecommendSong.name, jump=True)]

        elif (
            next(iter(self.module.state_tracker.curr_turn()), None) == states.Chitchat.name and
            ChitchatContent.remaining(tracker, dispatcher.tm) <= 0
        ):
            return states.ChangeTopic.name

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        if self.exit_flag:
            dispatcher.respond_template("stop/no_further", {})
            dispatcher.propose_continue('STOP')
            return [NextState(states.Initial.name)]

        elif (
            regex_match.is_exit(tracker) and
            not self.song_question and
            self.module.state_tracker.curr_turn()[0] != states.Initial.name and
            not re.search(r"music", tracker.input_text)
        ):
            dispatcher.respond_template('stop/ok', {})
            dispatcher.propose_continue('STOP')
            return [NextState(states.Initial.name)]
