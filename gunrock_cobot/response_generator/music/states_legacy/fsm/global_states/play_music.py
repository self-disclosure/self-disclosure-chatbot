from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states

from response_generator.music.utils import entity


class PlayMusicRespond(MusicState):
    """
    Generate a response dict that targets if user requests to play music
    """

    name = 'play_music_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        song: entity.SongItem = next(iter(entity.SongItem.detect(tracker)), None)

        tracker.one_turn_store['play_music_reponse'] = self.artist_to_discuss
        self.logger.debug(
            f"[MUSIC] play_music_response: "
            f"{self.artist_to_discuss}, {song}, "
        )

        if self.artist_to_discuss:
            self.logger.debug(f"artist not None. {self.artist_to_discuss}")
            dispatcher.respond_template("play_music_ack/artist", {'artist': self.artist_to_discuss.noun})
            tracker.one_turn_store['artist_to_discuss'] = self.artist_to_discuss
            return [NextState(states.VerifyMusician.name)]

        else:
            self.logger.debug(f"play_music_response artist is None.")
            dispatcher.respond_template("play_music_ack/simple", {})
            dispatcher.respond_template("play_music_ack/propose_sing", {})
            return [NextState(states.VerifySinging.name)]
