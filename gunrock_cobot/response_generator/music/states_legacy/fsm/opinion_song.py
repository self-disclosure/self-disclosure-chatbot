import re

from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states
from response_generator.music.states.fsm.chitchat.chitchat_content import ChitchatContent

from response_generator.music.utils import entity, regex_match
from response_generator.music.utils.legacy import detect_intent


class SongNameDropRespond(MusicState):
    """
    aka s_opinionsong

    song
    prev: have you heard of <song>
    curr: That is great. I think it is a good song... what's your favorite song of billie holiday?
    """

    name = 'song_name_drop_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        self.logger.debug(f"entering")

        intent = detect_intent(tracker.input_text)

        curr_turn_artist: entity.ArtistItem = next(iter(entity.ArtistItem.detect(tracker)), None)

        if curr_turn_artist and curr_turn_artist != self.artist_to_discuss:
            self.logger.debug(f"detected different curr_turn_artist = {curr_turn_artist}, jumping to fav_musician")
            return [NextState(states.FavoriteMusician.name, jump=True)]

        if re.search(r"don't like|not my favorite", tracker.input_text):
            self.logger.debug(f"not my favorite, jumping to chitchat")
            if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                return [NextState(states.Chitchat.name, jump=True)]
            else:
                return [NextState(states.ChangeTopic.name, jump=True)]

        if regex_match.is_other_answer(tracker) and self.artist_to_discuss:
            dispatcher.respond_template('opinionsongs/neg', {'artist': self.artist_to_discuss.noun.replace('.', ' ')})
            # TODO: song_question = True?
            tracker.one_turn_store['artist_to_discuss'] = curr_turn_artist
            tracker.one_turn_store['current_chitchat'] = self.current_chitchat
            return [NextState(states.FavoriteSong.name)]

        else:
            if intent == "answer_no" or 'ans_neg' in tracker.returnnlp.flattened_lexical_intent:
                dispatcher.respond_template(
                    'opinionsongs/neg', {'artist': self.artist_to_discuss.noun.replace(".", " ")})
            else:
                dispatcher.respond_template(
                    'opinionsongs/great', {'artist': self.artist_to_discuss.noun.replace(".", " ")})

            # "what's your favorite song by <artist>?"
            # here we save the ArtistItem for s_favsong
            tracker.one_turn_store['bot_said_artist'] = curr_turn_artist
            # TODO: song_question = True?
            tracker.one_turn_store['artist_to_discuss'] = self.artist_to_discuss
            tracker.one_turn_store['current_chitchat'] = self.current_chitchat
            return [NextState(states.FavoriteSong.name)]
