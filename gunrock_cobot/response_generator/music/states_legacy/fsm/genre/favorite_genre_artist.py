from nlu.constants import TopicModule, DialogAct
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states

from response_generator.music.utils import entity, regex_match


selector_root = "fav_genre_artist"


class FavGenreArtistPropose(MusicState):

    name = 'fav_genre_propose'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/propose", {})
        return [NextState(states.FavoriteGenreArtistRespond.name)]


class FavGenreArtistRespond(MusicState):

    name = 'fav_genre_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        artists = entity.ArtistItem.detect(tracker, include_negative_positivity=True)
        artist: entity.ArtistItem = (
            next(filter(lambda artist: artist.positivity, artists), None) or
            next(filter(lambda artist: not artist.positivity, artists), None)
        )
        genre: entity.GenreItem = next(iter(entity.GenreItem.detect(tracker)), None)

        self.logger.debug(f"entities = {artists}, {genre}")

        if (
            artist and artist.positivity
        ):
            pass
        
        return [NextState(states.Initial.name)]