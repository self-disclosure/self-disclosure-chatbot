import re

from nlu.constants import TopicModule, DialogAct
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states

from response_generator.music.utils import entity, regex_match


selector_root = "fav_genre"


class FavGenreEnter(MusicState):

    name = 'fav_genre_enter'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.FavGenrePropose.name, jump=True)]


class FavGenrePropose(MusicState):

    name = 'fav_genre_propose'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/proposing", {})
        return [NextState(states.FavoriteGenreRespond.name)]


class FavGenreRespond(MusicState):

    name = 'fav_genre_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        genres = entity.GenreItem.detect(tracker, include_negative_positivity=True)
        genre = (
            next(filter(lambda genre: genre.positivity, genres), None) or
            next(filter(lambda genre: not genre.positivity, genres), None)
        )
        artist = next(iter(entity.ArtistItem.detect(tracker, include_negative_positivity=True)), None)
        song = next(iter(entity.SongItem.detect(tracker)), None)
        fav_genre_verify_song = (
            tracker.one_turn_store.get('fav_genre_verify_song') or
            tracker.last_utt_store.get('fav_genre_verify_song') or
            0
        )

        if self.command_question_handler.has_question():
            # Topic specific question
            if self.command_question_handler.question_detector.is_ask_back_question():
                pass
                # self.logger.debug(f"user asking our favorite genre. ack then go to fav_musician")
                # dispatcher.respond_template(f"{selector_root}/respond/my_favorite", {})
                # return [NextState(states.FavoriteMusicianProposing.name, jump=True)]

            # general question
            else:
                question_response = self.command_question_handler.handle_general_question()

                if self.command_question_handler.handle_is_proposing_topic_response(
                    question_response, TopicModule.MUSIC, dispatcher, tracker
                ):
                    return [NextState(states.Initial.name)]
                elif question_response:
                    self.logger.debug(f"clear question_response: {question_response}")
                    dispatcher.respond(question_response.response)
                    dispatcher.respond_template('no_match/continue', {})
                    return [NextState(states.FavArtistPropose.name, jump=True)]

        if (
            tracker.returnnlp.has_dialog_act({DialogAct.OTHER_ANSWERS}) or
            re.search(r"\b(no idea)\b", tracker.input_text)
        ):
            self.logger.debug(f"user saying i don't know. Going to fav_musician")
            dispatcher.respond_template('unknown_ack', {})
            # return [NextState(states.FavoriteMusicianProposing.name, jump=True)]

        elif genre:
            self.logger.debug(f"entering; detected genre entity ({genre})")
            if not genre.positivity:
                self.logger.debug(f"following up with user's neg sentiment")
                dispatcher.respond_template(f"{selector_root}/respond/negative_genre", {})
                # return [NextState(states.FavoriteGenreProposing.name, jump=True)]

            else:
                # TODO: why are we checking redis again
                self.logger.debug(f"following up genre")
                # if not self.redis_helper.is_returning_user_in_music_module(
                #     tracker.user_attributes._get_raw('user_id')):
                if 'pop' in genre.noun:
                    dispatcher.respond_template(f"{selector_root}/respond/pop", {})
                else:
                    dispatcher.respond_template(f"{selector_root}/respond/non_pop", {'genre': genre.noun})
                tracker.one_turn_store['genre_to_discuss'] = genre
                # return [NextState(states.FavoriteMusicianProposing.name, jump=True)]

        elif regex_match.is_saying_all_of_them(tracker):
            self.logger.debug(f"entering user liking all of it")
            dispatcher.respond_template(f"{selector_root}/respond/all_of_them", {})
            # return [NextState(states.FavoriteMusicianProposing.name, jump=True)]

        # handler if user say a song name instead
        elif song and fav_genre_verify_song < 1:
            self.logger.debug(f"s_favgenre got song: {song}, looping fav_genre for verification")
            # temp reroute to self with ack
            tracker.volatile_store['song_to_discuss'] = song
            tracker.one_turn_store['fav_genre_verify_song'] = fav_genre_verify_song + 1
            dispatcher.respond_template("verify_song/with_ner", {'song_name': song.noun})
            return [NextState(self.name)]

        elif (
            not song and re.search(r"my favorite song is", tracker.input_text) and
            fav_genre_verify_song < 1
        ):
            tracker.volatile_store['song_to_discuss'] = song
            tracker.one_turn_store['fav_genre_verify_song'] = fav_genre_verify_song + 1
            dispatcher.respond_template("verify_song/with_ner", {'song_name': song.noun})
            return [NextState(self.name)]

        elif artist:
            return [NextState(states.FavoriteMusicianRespond.name, jump=True)]

        elif not genre:
            fav_genre_verify_genre = (
                tracker.volatile_store.get('fav_genre_verify_genre') or
                tracker.last_utt_store.get('fav_genre_verify_genre') or
                0
            )
            self.logger.debug(f"entering no detected genre entity")
            if tracker.returnnlp.answer_positivity == 'neg' and len(tracker.input_text.split(' ')) < 3:
                self.logger.debug(f"answer_positivity == neg. Decline ack and going to s_favmusician")
                dispatcher.respond_template(f"{selector_root}/respond/decline", {})
                # return [NextState(states.FavoriteMusicianProposing.name, jump=True)]

            elif fav_genre_verify_genre < 1:
                tracker.one_turn_store['fav_genre_verify_genre'] = fav_genre_verify_genre + 1
                dispatcher.respond_template(f"{selector_root}/verify_genre/default", {})
                return [NextState(self.name)]
                # return [NextState(states.VerifyGenre.name)]

            else:
                dispatcher.respond_template(f"{selector_root}/respond/decline", {})
                # return [NextState(states.FavoriteMusicianProposing.name, jump=True)]

        if self.command_question_handler.question_detector.is_ask_back_question():
            self.logger.debug(f"user asking our favorite genre. ack then go to fav_musician")
            dispatcher.respond_template(f"{selector_root}/respond/my_favorite", {})

        self.logger.debug(f"returning default return")

        return [NextState(states.FavoriteMusicianProposing.name, jump=True)]
