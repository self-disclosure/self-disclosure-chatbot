from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states
from response_generator.music.states.fsm.chitchat.chitchat_content import ChitchatContent

from response_generator.music.utils import entity
from response_generator.music.utils.legacy import detect_intent


class MoreFacts(MusicState):
    """
    verify
    """

    name = 'more_facts'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        artist_to_discuss: entity.ArtistItem = (
            tracker.one_turn_store.get('artist_to_discuss') or
            tracker.last_utt_store.get('artist_to_discuss') or
            None
        )
        current_chitchat: ChitchatContent = ChitchatContent.from_keyword((
            tracker.one_turn_store.get('current_chitchat') or
            tracker.last_utt_store.get('current_chitchat') or
            ""
        ), dispatcher.tm)

        self.logger.debug(f"handle question")

        question_response = self.command_question_handler.handle_question()
        if self.command_question_handler.handle_is_proposing_topic_response(
            question_response, TopicModule.MUSIC, dispatcher, tracker
        ):
            return [NextState(states.Initial.name)]
        elif question_response:
            self.logger.debug(f"clear question_response: {question_response}")
            dispatcher.respond(question_response.response)
            dispatcher.propose_continue('STOP')
            return [NextState(states.Initial.name)]

        artist: entity.ArtistItem = next(iter(entity.ArtistItem.detect(tracker)), None)
        if artist and artist != artist_to_discuss:
            self.logger.debug(f"found artist: {artist}, going to s_favmusician")
            return [NextState(states.FavoriteMusician.name, jump=True)]

        intent = detect_intent(tracker.input_text)
        if intent == "answer_no":
            self.logger.debug(f"custom intent: {intent}, going to s_chitchat")
            if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                return [NextState(states.Chitchat.name, jump=True)]
            else:
                return [NextState(states.ChangeTopic.name, jump=True)]

        else:
            self.logger.debug(f"default return, going to s_verifychitchat")
            dispatcher.respond_template('stop/no_change_topic', {})
            tracker.one_turn_store['current_chitchat'] = current_chitchat.keyword if current_chitchat else ""
            tracker.one_turn_store['artist_to_discuss'] = artist_to_discuss
            dispatcher.propose_continue('UNCLEAR')
            return [NextState(states.VerifyChitchat.name)]
