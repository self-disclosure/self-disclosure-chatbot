import re
from typing import Dict

from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states
from response_generator.music.utils import entity, regex_match


selector_root = "instruments"


def valid_instruments(instrument: entity.InstrumentItem) -> bool:
    return instrument.canonical in {'guitar', 'piano'} if instrument else False


class InstrumentsUserPrefRespond(MusicState):
    """
    when user says "i like to play <instrument>"
    """

    name = 'instruments_user_pref_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        instrument: entity.InstrumentItem = next(iter(entity.InstrumentItem.detect(tracker)), None)
        if not instrument:
            # TODO: error handling
            pass

        tracker.volatile_store['instrument_to_discuss'] = instrument
        dispatcher.respond("That's awesome!")
        # TODO: randomize?
        return [NextState(states.InstrumentsClassicModernPlaystyleProposing.name, jump=True)]


class InstrumentsUserPrefExit(MusicState):
    """
    when user says "i like to play <instrument>"
    """

    name = 'instruments_user_pref_exit'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        instrument: entity.InstrumentItem = (
            tracker.volatile_store.get('instrument_to_discuss') or
            tracker.one_turn_store.get('instrument_to_discuss') or
            tracker.last_utt_store.get('instrument_to_discuss') or
            next(iter(entity.InstrumentItem.detect(tracker)), None)
        )
        dispatcher.respond_template(
            f"{selector_root}/transition/exit",
            dict(instrument=instrument.noun if instrument else "instruments"))
        return [NextState(states.FavoriteGenreProposing.name, jump=True)]


class InstrumentsClassicModernPlaystyleProposing(MusicState):

    name = "instruments_classic_modern_playstyle_proposing"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        instrument: entity.InstrumentItem = (
            tracker.volatile_store.get('instrument_to_discuss') or
            tracker.one_turn_store.get('instrument_to_discuss') or
            tracker.last_utt_store.get('instrument_to_discuss') or
            next(iter(entity.InstrumentItem.detect(tracker)), None)
        )
        if not instrument:
            # TODO: error handling
            pass
        tracker.one_turn_store['instrument_to_discuss'] = instrument

        if instrument.canonical == 'guitar':
            key = 'guitar'
        elif instrument.canonical == 'piano':
            key = 'piano'
        else:
            key = 'default'

        dispatcher.respond_template(f"{selector_root}/classic_or_modern_playstyle/proposing/{key}", {})

        return [NextState(states.InstrumentsClassicModernPlaystyleRespond.name)]


class InstrumentsClassicModernPlaystyleRespond(MusicState):

    name = "instruments_classic_modern_playstyle_respond"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        if re.search(r"\b(classic(al)?)\b", tracker.input_text):
            key = 'classical'
        elif re.search(r"\b(contemporary|pop)\b", tracker.input_text):
            key = 'contemporary'
        elif re.search(r"\b(both|any)\b", tracker.input_text) or regex_match.is_saying_all_of_them(tracker):
            key = 'both'
        else:
            key = 'default'

        dispatcher.respond_template(f"{selector_root}/classic_or_modern_playstyle/respond/{key}", {})
        return [NextState(states.InstrumentsLengthExperienceProposing.name, jump=True)]


class InstrumentsLengthExperienceProposing(MusicState):

    name = "instruments_classic_length_experience_proposing"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        instrument: entity.InstrumentItem = (
            tracker.volatile_store.get('instrument_to_discuss') or
            tracker.one_turn_store.get('instrument_to_discuss') or
            tracker.last_utt_store.get('instrument_to_discuss') or
            next(iter(entity.InstrumentItem.detect(tracker)), None)
        )
        if not instrument:
            # TODO: error handling
            pass
        tracker.one_turn_store['instrument_to_discuss'] = instrument
        dispatcher.respond_template(
            f"{selector_root}/length_experience/proposing",
            dict(instrument=instrument.noun if instrument else "the instrument"))
        return [NextState(states.InstrumentsLengthExperienceRespond.name)]


class InstrumentsLengthExperienceRespond(MusicState):

    name = "instruments_length_experience_respond"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        if re.search(r"\b(just started|beginner|not long|new|month(s)?|(a|one) year)\b", tracker.input_text):
            key = 'beginner'
        elif re.search(r"\b(a while|was (young|(a (kid|child)))|years)\b", tracker.input_text):
            key = 'expert'
        else:
            key = 'default'
        dispatcher.respond_template(f"{selector_root}/length_experience/respond/{key}", {})
        return [NextState(states.InstrumentsUsualSongProposing.name, jump=True)]


class InstrumentsUsualSongProposing(MusicState):

    name = "instruments_usual_song_proposing"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        instrument: entity.InstrumentItem = (
            tracker.volatile_store.get('instrument_to_discuss') or
            tracker.one_turn_store.get('instrument_to_discuss') or
            tracker.last_utt_store.get('instrument_to_discuss') or
            next(iter(entity.InstrumentItem.detect(tracker)), None)
        )
        if not instrument:
            # TODO: error handling
            pass
        tracker.one_turn_store['instrument_to_discuss'] = instrument
        dispatcher.respond_template(f"{selector_root}/usual_song/proposing", {})
        return [NextState(states.InstrumentsUsualSongRespond.name)]


class InstrumentsUsualSongRespond(MusicState):

    name = "instruments_usual_song_respond"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond("Great.")
        return [NextState(states.InstrumentsWhosYourTeacherProposing.name, jump=True)]


class InstrumentsWhosYourTeacherProposing(MusicState):

    name = "instruments_whos_your_teacher_proposing"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        instrument: entity.InstrumentItem = (
            tracker.volatile_store.get('instrument_to_discuss') or
            tracker.one_turn_store.get('instrument_to_discuss') or
            tracker.last_utt_store.get('instrument_to_discuss') or
            next(iter(entity.InstrumentItem.detect(tracker)), None)
        )
        if not instrument:
            # TODO: error handling
            pass
        tracker.one_turn_store['instrument_to_discuss'] = instrument
        dispatcher.respond_template(f"{selector_root}/whos_your_teacher/proposing", {})
        return [NextState(states.InstrumentsWhosYourTeacherRespond.name)]


class InstrumentsWhosYourTeacherRespond(MusicState):

    name = "instruments_whos_your_teacher_respond"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        instrument: entity.InstrumentItem = (
            tracker.volatile_store.get('instrument_to_discuss') or
            tracker.one_turn_store.get('instrument_to_discuss') or
            tracker.last_utt_store.get('instrument_to_discuss') or
            next(iter(entity.InstrumentItem.detect(tracker)), None)
        )

        if re.search(r"\b(teacher|friend)\b", tracker.input_text):
            key = 'teacher'
        elif re.search(r"\b(self taught|by myself|youtube)\b", tracker.input_text):
            key = 'self'
        else:
            key = 'default'

        dispatcher.respond_template(f"{selector_root}/whos_your_teacher/respond/{key}", {})

        if not (instrument and valid_instruments(instrument)):
            self.logger.debug(f"end subtopic because instrument = {instrument}")
            return [NextState(states.InstrumentsUserPrefExit.name, jump=True)]
        else:
            return [NextState(states.InstrumentsFamousMusicianProposing.name, jump=True)]


class InstrumentsFamousMusicianProposing(MusicState):

    name = "instruments_famous_musician_proposing"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        instrument: entity.InstrumentItem = (
            tracker.volatile_store.get('instrument_to_discuss') or
            tracker.one_turn_store.get('instrument_to_discuss') or
            tracker.last_utt_store.get('instrument_to_discuss') or
            next(iter(entity.InstrumentItem.detect(tracker)), None)
        )
        if not (
            instrument and
            instrument.canonical not in {'guitar', 'piano'}
        ):
            # TODO: error handling
            pass
        tracker.one_turn_store['instrument_to_discuss'] = instrument

        args = {}
        specifics = dispatcher.tm.speak(
            f"{selector_root}/famous_musician/proposing/specific/{instrument.noun}", {}, embedded_info=args)
        self.logger.debug(f"specific args = {args}")

        dispatcher.respond_template(f"{selector_root}/famous_musician/proposing/default", args)
        dispatcher.respond(specifics)

        tracker.one_turn_store['famous_musician'] = args
        return [NextState(states.InstrumentsFamousMusicianRespond.name)]


class InstrumentsFamousMusicianRespond(MusicState):

    name = "instruments_famous_musician_respond"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        instrument: entity.InstrumentItem = (
            tracker.volatile_store.get('instrument_to_discuss') or
            tracker.one_turn_store.get('instrument_to_discuss') or
            tracker.last_utt_store.get('instrument_to_discuss') or
            next(iter(entity.InstrumentItem.detect(tracker)), None)
        )
        tracker.one_turn_store['instrument_to_discuss'] = instrument

        musician: Dict[str, str] = tracker.last_utt_store.get('famous_musician')

        if (
            musician and 'canonical' in musician and
            re.search(r"(^who)|(^no (i have(n't| not) )?who is)|\b(have(n't |not) heard of)\b", tracker.input_text)
        ):
            dispatcher.respond_template(f"{selector_root}/famous_musician/who_is/{musician['canonical']}", {})

        elif tracker.returnnlp.answer_positivity == 'neg':
            dispatcher.respond_template(
                f"{selector_root}/famous_musician/respond/default/neg",
                dict(pronoun=musician.get('pronoun') if musician else "them"))

        elif tracker.returnnlp.answer_positivity == 'pos':
            dispatcher.respond_template(f"{selector_root}/famous_musician/respond/default/pos", {})

        else:
            dispatcher.respond_template(f"{selector_root}/famous_musician/respond/default/neu", {})

        if (
            musician and 'canonical' in musician and
            dispatcher.tm.has_selector(f"{selector_root}/famous_musician/respond/specific/{musician['canonical']}")
        ):
            dispatcher.respond_template(f"{selector_root}/famous_musician/respond/specific/{musician['canonical']}", {})

        return [NextState(states.InstrumentsFamousMusicianRespondEpilogue.name)]


class InstrumentsFamousMusicianRespondEpilogue(MusicState):

    name = "instruments_famous_musician_respond_epilogue"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.InstrumentsUserPrefExit.name, jump=True)]
