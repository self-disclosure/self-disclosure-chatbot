from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states
from response_generator.music.states.fsm.chitchat.chitchat_content import ChitchatContent


class UserSing(MusicState):
    """
    when user say "can i sing you a song"
    """

    name = 'user_sing'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        self.logger.debug(f"entering")

        dispatcher.respond_template('user_sing/accept', {})
        return [NextState(states.UserSingApplause.name)]


class UserSingApplause(MusicState):
    """
    followup on user_sing
    """

    name = 'user_sing_applause'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        dispatcher.respond_template('user_sing/applause', {})

        # try to ask user if we can also try to sing
        singing_count = tracker.persistent_store.get('singing_count', 0)
        if singing_count < dispatcher.tm.count_utterances('bot_singing/default'):
            dispatcher.respond_template('user_sing/hear_me_sing/propose', {})
            return [NextState(states.UserSingHearMeSingConfirm.name)]
        else:
            if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                return [NextState(states.Chitchat.name, jump=True)]
            else:
                return [NextState(states.ChangeTopic.name, jump=True)]


class UserSingHearMeSingConfirm(MusicState):
    """
    Confirm answer to "do you want to hear me sing"
    """

    name = 'user_sing_hear_me_sing_confirm'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        if tracker.returnnlp.answer_positivity == 'pos':
            return [NextState(states.Singing.name, jump=True)]

        else:
            dispatcher.respond_template("user_sing/hear_me_sing/declined", {})
            if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                return [NextState(states.Chitchat.name, jump=True)]
            else:
                return [NextState(states.ChangeTopic.name, jump=True)]
