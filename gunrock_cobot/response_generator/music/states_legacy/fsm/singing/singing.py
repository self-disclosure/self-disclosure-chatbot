from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states
from response_generator.music.states.fsm.chitchat.chitchat_content import ChitchatContent


class Singing(MusicState):
    """
    verify if user wants to talk about the musician
    play_music_response also goes here if there's an artist
    """

    name = 'singing'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        self.logger.debug(f"entering")

        singing_count = tracker.persistent_store.get('singing_count', 0)

        if singing_count >= dispatcher.tm.count_utterances('bot_singing/default'):
            # hack to disable question_handler in chitchat, avoiding singing from backstory
            tracker.volatile_store['handle_question_is_handled'] = True
            dispatcher.respond_template(f"bot_singing/no_more_songs", {})
            # TODO: better transition?
            if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                return [NextState(states.Chitchat.name, jump=True)]
            else:
                return [NextState(states.ChangeTopic.name, jump=True)]

        else:
            dispatcher.respond_template(f"bot_singing/default", {})
            tracker.persistent_store['singing_count'] = singing_count + 1
            dispatcher.respond_template(f"bot_singing/what_do_you_think", {})

            return [NextState(states.SingingRating.name)]


class SingingRating(MusicState):

    name = 'singing_rating'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        self.logger.debug(f"entering, answer_positivity = {tracker.returnnlp.answer_positivity}")

        selector = 'bot_singing/reaction'

        if (
            tracker.returnnlp.answer_positivity == 'neg' or
            tracker.returnnlp.opinion_positivity == 'neg' or
            tracker.returnnlp.has_intent({'complaint'})
        ):
            self.logger.debug(f"neg answer, going to s_chitchat")
            dispatcher.respond_template(f"{selector}/neg", {})
            tracker.one_turn_store['chitchat_chain_with_ack'] = True

        elif (
            tracker.returnnlp.answer_positivity == 'pos' or
            tracker.returnnlp.opinion_positivity == 'pos'
        ):
            self.logger.debug(f"pos answer, going to s_chitchat")
            dispatcher.respond_template(f"{selector}/pos", {})
            tracker.one_turn_store['chitchat_chain_with_ack'] = True

        else:
            self.logger.debug(f"default, going to s_chitchat")

        if ChitchatContent.remaining(tracker, dispatcher.tm):
            self.logger.debug(f"chitchat remaining: {ChitchatContent.remaining(tracker, dispatcher.tm)}")
            if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                return [NextState(states.Chitchat.name, jump=True)]
            else:
                return [NextState(states.ChangeTopic.name, jump=True)]
        else:
            dispatcher.propose_continue('STOP')
            return [NextState(states.Initial.name)]
