from nlu.constants import Positivity, TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states
from response_generator.music.states.fsm.chitchat.chitchat_content import ChitchatContent


class ChangeTopic(MusicState):
    """
    switch to other module
    """

    name = 'change_topic'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        self.logger.debug(f"handle question")
        question_response = self.command_question_handler.handle_question()
        if self.command_question_handler.handle_is_proposing_topic_response(
            question_response, TopicModule.MUSIC, dispatcher, tracker
        ):
            return [NextState(states.Initial.name)]
        elif question_response:
            self.logger.debug(f"clear question_response: {question_response}")
            dispatcher.respond(question_response.response)
            if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                return [NextState(states.Chitchat.name, jump=True)]

        # dispatcher.respond_template('chitchats/ack', {})
        # dispatcher.respond(' ')
        dispatcher.respond_template(f"transition/exit_no_verify", {})
        dispatcher.propose_continue('STOP')
        return[NextState(states.Initial.name)]


class TopicSwitch(MusicState):
    """
    similar to change topic
    """

    name = 'topic_switch'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        self.logger.debug(f"entering")

        if tracker.returnnlp.answer_positivity is Positivity.neg:
            self.logger.debug(f"neg answer, going to s_chitchat")
            dispatcher.respond_template('chitchats/ack', {})
            dispatcher.propose_continue('STOP')
            tracker.one_turn_store['current_chitchat'] = self.current_chitchat
            if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                return [NextState(states.Chitchat.name, jump=True)]
            else:
                return [NextState(states.ChangeTopic.name, jump=True)]
        else:
            self.logger.debug(f"default, going to s_chitchat")
            if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                return [NextState(states.Chitchat.name, jump=True)]
            else:
                return [NextState(states.ChangeTopic.name, jump=True)]
