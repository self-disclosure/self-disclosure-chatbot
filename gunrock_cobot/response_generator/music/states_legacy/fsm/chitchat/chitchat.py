from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states
from response_generator.music.states.fsm.chitchat.chitchat_content import ChitchatContent

from response_generator.music.utils import entity, regex_match


class ChitchatEnter(MusicState):

    name = 'chitchat_enter'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.Chitchat.name, jump=True)]


class Chitchat(MusicState):

    """
    enter chitchat initialization
    """

    name = 'chitchat'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        self.logger.debug(f"entering")

        chain_with_acknowledgement = (
            tracker.one_turn_store.get('chitchat_chain_with_ack') or
            tracker.last_utt_store.get('chitchat_chain_with_ack') or
            False
        )

        chitchat = ChitchatContent.random(tracker, dispatcher.tm)

        if regex_match.is_asking_for_recommendation(tracker):
            self.logger.debug(f"asking for rec. Going to recommend_song")
            return [NextState(states.RecommendSong.name, jump=True)]

        elif regex_match.is_rapport(tracker) and not self.artist_to_discuss:
            self.logger.debug(f"rapport match.")
            tracker.one_turn_store['template_artist_ack_artist'] = 'taylor_swift'
            return [NextState(states.TemplateArtistAck.name)]

        self.logger.debug(f"handle question")

        question_response = self.command_question_handler.handle_question()
        if self.command_question_handler.handle_is_proposing_topic_response(
            question_response, TopicModule.MUSIC, dispatcher, tracker
        ):
            return [NextState(states.Initial.name)]
        elif question_response:
            self.logger.debug(f"clear question_response: {question_response}")
            dispatcher.respond(question_response.response)
            dispatcher.respond_template('no_match/continue', {})
            dispatcher.respond_template('artist_question/general', {})
            return [NextState(self.name)]

        artist = next(iter(entity.ArtistItem.detect(tracker)), None)
        self.logger.debug(
            f"detecting artist = {artist}. artist_to_discuss = {self.artist_to_discuss}, "
            f"sentiment = {tracker.returnnlp[0].sentiment} "
        )
        if (
            artist and (artist != self.artist_to_discuss) and
            # tracker.returnnlp[0].sentiment != 'neg' and
            tracker.volatile_store.get('entering_s_favmusician', 0) <= 0
        ):

            self.logger.debug(
                f"jumping to favmusician. "
                f"entity: {artist}, sentiment: {tracker.returnnlp[0].sentiment}")
            return [NextState(states.FavoriteMusician.name, jump=True)]

        # default return

        self.logger.debug(
            f"default response: chitchat: {chitchat}, chain_with_ack: {chain_with_acknowledgement}")

        # create ack to transition to s_chitchat
        if not chain_with_acknowledgement:
            pass
        elif (
            len(tracker.input_text.split(' ')) > 5 and
            # s_artist_question can jump directly to s_artist_song which will concatenate with things here
            # so we avoid certain ack
            tracker.volatile_store.get('jump_from') != 's_artist_question'
        ):
            dispatcher.respond_template('long_sentences_ack', {})
        elif (regex_match.is_other_answer(tracker)):
            dispatcher.respond_template('unknown_ack', {})
        elif artist:
            dispatcher.respond_template('chitchats/entity_ack', {'entity': artist.noun})
        # elif 'ans_neg' in tracker.returnnlp.flattened_lexical_intent:
        #     dispatcher.respond_template('chitchats/ack', {})
        else:
            dispatcher.respond_template('chitchats/ack', {})

        dispatcher.respond(chitchat.question)
        tracker.one_turn_store['current_chitchat'] = chitchat.keyword
        tracker.one_turn_store['self.artist_to_discuss'] = artist
        return [NextState(states.ChitchatPropose.name)]
