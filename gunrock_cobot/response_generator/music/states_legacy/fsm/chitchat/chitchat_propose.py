import re

from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states
from response_generator.music.states.fsm.chitchat.chitchat_content import ChitchatContent

from response_generator.music.utils import entity, regex_match
from response_generator.music.utils.legacy import karaoke_songs


class ChitchatPropose(MusicState):
    """
    aka s_chitchatcont
    """

    name = 'chitchat_propose'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        artist_to_discuss: entity.ArtistItem = (
            tracker.one_turn_store.get('artist_to_discuss') or
            tracker.last_utt_store.get('artist_to_discuss') or
            None
        )
        genre_to_discuss: entity.GenreItem = (
            tracker.one_turn_store.get('genre_to_discuss') or
            tracker.last_utt_store.get('genre_to_discuss') or
            None
        )
        current_chitchat: ChitchatContent = ChitchatContent.from_keyword((
            tracker.one_turn_store.get('current_chitchat') or
            tracker.last_utt_store.get('current_chitchat') or
            ""
        ), dispatcher.tm)

        next_chitchat = ChitchatContent.random(tracker, dispatcher.tm)

        self.logger.debug(
            f"entering. artist: {artist_to_discuss}, genre: {genre_to_discuss}, chitchat: {current_chitchat}")
        if regex_match.is_asking_for_recommendation(tracker):
            self.logger.debug(f"asking for rec. Going to recommend_song")
            tracker.one_turn_store['recommend_song_old_state'] = states.Chitchat.name
            return [NextState(states.RecommendSong.name, jump=True)]

        self.logger.debug(f"handle question")

        question_response = self.command_question_handler.handle_question()
        if self.command_question_handler.handle_is_proposing_topic_response(
            question_response, TopicModule.MUSIC, dispatcher, tracker
        ):
            return [NextState(states.Initial.name)]
        elif question_response:
            self.logger.debug(f"clear question_response: {question_response}")
            dispatcher.respond(question_response.response)
            if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                return [NextState(states.Chitchat.name, jump=True)]
            else:
                # dispatcher.respond_template(f"transition/exit_no_verify", {})
                return [NextState(states.ChangeTopic.name, jump=True)]

        if re.search(r'talk about|my favorite', tracker.input_text):
            self.logger.debug(f"talk about|myfavorite match")

            artist = next(iter(entity.ArtistItem.detect(tracker)), None)

            if artist and (not artist_to_discuss or artist != artist_to_discuss):
                self.logger.debug(f"match artist={artist}, going to s_favmusician")
                return [NextState(states.FavoriteMusician.name, jump=True)]

            genre = next(iter(entity.GenreItem.detect(tracker)), None)
            if genre and (not genre_to_discuss or genre != genre_to_discuss):
                self.logger.debug(f"match genre={genre}, going to s_favmusician")
                tracker.one_turn_store['artist_to_discuss'] = artist
                tracker.one_turn_store['current_chitchat'] = current_chitchat.keyword if current_chitchat else None
                return [NextState(states.FavoriteMusician.name)]

        song = next(iter(entity.SongItem.detect(tracker)), None)
        if not (
            (current_chitchat and current_chitchat.song_question) and not song and
            not re.search(r"do you like", tracker.input_text)
        ):
            if re.search(r'why', tracker.input_text) and not current_chitchat.answer and current_chitchat.followup:
                self.logger.debug(f"no song but song_question and 'why', going to chitchat_followup")
                return [NextState(states.ChitchatFollowup.name, jump=True)]

        if current_chitchat and current_chitchat.keyword in {'least_fav_music'} and tracker.input_text == 'yes':
            self.logger.debug(f"keyword == least_fav_music and input_text == yes, going to s_chitchatcont")
            dispatcher.respond_template("chitchats/pardon", {})
            tracker.one_turn_store['artist_to_discuss'] = artist_to_discuss
            tracker.one_turn_store['current_chitchat'] = current_chitchat.keyword
            return [NextState(self.name)]

        # response = ''
        optional_keyword = self.respond_chitchat(dispatcher, tracker, current_chitchat)
        # random.shuffle(chitchats)
        # if ChitchatContent.remaining(tracker, dispatcher.tm) == 0:
        if not next_chitchat:
            self.logger.debug(f"no more chitchats left")
            # response = acknowledgement
            if current_chitchat and current_chitchat.answer:
                dispatcher.respond(current_chitchat.answer)
            if optional_keyword and optional_keyword == 'ack_general':
                self.logger.debug(f"keyword == ack_general, going to change_topic immediately")
                # dispatcher.respond_template(f"transition/exit_no_verify", {})
                return [NextState(states.ChangeTopic.name, jump=True)]
            else:
                self.logger.debug(f"keyword != ack_general, going to change_topic next turn")
                # tracker.one_turn_store['current_chitchat'] = next_chitchat.keyword
                tracker.one_turn_store['artist_to_discuss'] = (
                    tracker.one_turn_store.get('artist_to_discuss') or
                    tracker.last_utt_store.get('artist_to_discuss') or
                    None
                )
                # dispatcher.respond_template(f"transition/exit_no_verify", {})
                return [NextState(states.ChangeTopic.name)]

        # prepare to go to next state
        tracker.one_turn_store['current_chitchat'] = next_chitchat.keyword
        tracker.one_turn_store['artist_to_discuss'] = (
            tracker.one_turn_store.get('artist_to_discuss') or
            tracker.last_utt_store.get('artist_to_discuss') or
            None
        )
        if current_chitchat and current_chitchat.answer:
            dispatcher.respond(current_chitchat.answer)
            self.logger.debug(f"chitchat answer is not empty, going to t_chitchatcont")
            return [NextState(states.ChitchatRespond.name, jump=True)]

        else:
            self.logger.debug(f"chitchat answer is empty")
            # song_question = next_chitchat['keyword'] in {'emotional', 'karaoke'}
            if tracker.volatile_store.get('cool_instrument') is True:
                self.logger.debug(f"going to {states.ChitchatRespond.name}")
                return [NextState(states.ChitchatRespond.name, jump=True)]
            else:
                self.logger.debug(f"going to {self.name}")
                dispatcher.respond(next_chitchat.question)
                return [NextState(self.name)]

    def respond_chitchat(self, dispatcher: Dispatcher, tracker: Tracker, chitchat: ChitchatContent):

        # TODO: THIS IS A MESS
        if chitchat and chitchat.keyword == 'important':
            if re.search(r'not important|not | not really', tracker.input_text):
                dispatcher.respond_template('important/not_important', {})
                return
            elif re.search(
                    r'very important|really important|most important|important|love music|very', tracker.input_text):
                dispatcher.respond_template('important/important', {})
                return

        if chitchat and chitchat.keyword == 'remove_music':
            if re.search((r"sad|depressed|lonely|impossible|said|horrible|bad|devastated|awful|terrible|go insane|"
                          r"upset|heart broken|not so good|dead inside|alone|die|cry|angry"), tracker.input_text):
                dispatcher.respond_template('remove_music/sad', {})
                return
            elif re.search(r'good|happy|not feel anything', tracker.input_text):
                dispatcher.respond_template('remove_music/happy', {})
                return

        if chitchat and chitchat.keyword == 'least_fav_music':
            genre: entity.GenreItem = next(iter(entity.GenreItem.detect(tracker)), None)
            if genre:
                dispatcher.respond_template('least_fav_music/present', {'genre': genre.noun})
                return

        if chitchat and chitchat.keyword == 'when_music':
            if re.search((r"mornings | evenings| gym| driving| bored| working| shower| drive| mood| feel like it|"
                          r"free time|night|about a week|sometimes|sad|not so much|in the car"), tracker.input_text):
                dispatcher.respond_template('when_music/not_music_fan', {})
                return
            if re.search(
                    r'all the time|most of the time|all day|every day|everyday|most of the time', tracker.input_text):
                dispatcher.respond_template('when_music/music_fan', {})
                return

        if chitchat and chitchat.keyword == 'how_music':
            if re.search((r"three days| thirty minutes|a little bit|five minutes|two hours|couple hours|minutes|"
                          r"sometimes|not much"), tracker.input_text):
                dispatcher.respond_template('how_music/not_very_much', {})
                return
            if re.search((r"twenty four hours|a lot| music as possible| all my time| everyday|all the time|"
                          r"most of the time|extremely|every day|every single day|every single day|"
                          r"twenty four seven|everyday|pretty often"), tracker.input_text):
                dispatcher.respond_template('how_music/very_much', {})
                return

        if chitchat and chitchat.keyword == 'emotional':
            song: entity.SongItem = next(iter(entity.SongItem.detect(tracker)), None)
            if song:
                dispatcher.respond_template('emotional/present', {'song': song.noun})
                return

        if chitchat and chitchat.keyword == 'instrument':
            instrument: entity.InstrumentItem = next(iter(entity.InstrumentItem.detect(tracker)), None)
            if instrument and instrument.noun == 'ukelele':
                dispatcher.respond_template('rapport_match', {})
                return
            elif instrument:
                dispatcher.respond_template('instrument/present', {'instrument': instrument.noun})
                tracker.volatile_store['cool_instrument'] = True
                return
            else:
                dispatcher.respond_template('instrument/none', {})
                return

        if chitchat and chitchat.keyword == 'song_again':
            song: entity.SongItem = next(iter(entity.SongItem.detect(tracker)), None)
            if song and not re.search(r'every single time', tracker.input_text):
                dispatcher.respond_template('song_again/present', {'song': song.noun})
                return
            if (
                'ans_pos' in tracker.returnnlp.flattened_lexical_intent or
                re.search(r"all the time| i've done that|absolutely", tracker.input_text)
            ):
                dispatcher.respond_template('song_again/not_present_yes', {})
                return
            if 'ans_neg' in tracker.returnnlp.flattened_lexical_intent:
                dispatcher.respond_template('song_again/not_present_no', {})
                return

        if chitchat and chitchat.keyword == 'karaoke':
            if re.search(r'i wanna dance with somebody', tracker.input_text):
                dispatcher.respond_template('rapport_match', {})
                return
            song: entity.SongItem = next(iter(entity.SongItem.detect(tracker)), None)
            if song and not re.search(r'the name|something|a song|the song', song.noun):
                dispatcher.respond_template('karaoke/present', {'song': song.noun})
                return
            else:
                for k_song in karaoke_songs:
                    if re.search(k_song['regex'], tracker.input_text):
                        song = k_song['name']
                        dispatcher.respond_template('karaoke/present', {'song': song.noun})
                        return
            if 'ans_neg' in tracker.returnnlp.flattened_lexical_intent:
                dispatcher.respond_template('karaoke/neg_response', {})
                return 'ans_neg'
            if 'ans_unknown' in tracker.returnnlp.flattened_lexical_intent:
                dispatcher.respond_template('unknown_ack', {})
                return'unknown_ack'

        if regex_match.is_rapport(tracker):
            dispatcher.respond_template('rapport_match', {})
            return 'rapport_match'

        if 'ans_unknown' in tracker.returnnlp.flattened_lexical_intent:
            dispatcher.respond_template('unknown_ack', {})
            return'unknown_ack'

        if len(tracker.input_text.split(" ")) > 5:
            dispatcher.respond_template('long_sentences_ack', {})
            return 'long_sentences_ack'
        else:
            dispatcher.respond_template('ack_general', {})
            return 'ack_general'
