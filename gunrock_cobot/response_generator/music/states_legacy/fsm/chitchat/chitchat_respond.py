import re

from nlu.constants import Positivity, TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states
from response_generator.music.states.fsm.chitchat.chitchat_content import ChitchatContent

from response_generator.music.utils import entity, regex_match


class ChitchatRespond(MusicState):
    """
    aka t_chitchatcont
    """

    name = 'chitchat_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        artist_to_discuss: entity.ArtistItem = (
            tracker.one_turn_store.get('artist_to_discuss') or
            tracker.last_utt_store.get('artist_to_discuss') or
            None
        )
        current_chitchat: ChitchatContent = ChitchatContent.from_keyword((
            tracker.one_turn_store.get('current_chitchat') or
            tracker.last_utt_store.get('current_chitchat') or
            ""
        ), dispatcher.tm)

        self.logger.debug(f"entering")

        if regex_match.is_asking_for_recommendation(tracker):
            self.logger.debug(f"asking for rec. Going to recommend_song")
            tracker.one_turn_store['recommend_song_old_state'] = states.Chitchat.name
            return [NextState(states.RecommendSong.name, jump=True)]

        if ChitchatContent.remaining(tracker, dispatcher.tm) == 0:
            self.logger.debug(f"ran out of chitchats. going to change_topic")
            # TODO: check if need return
            return [NextState(states.ChangeTopic.name, jump=True)]

        if re.search(r'talk about|my favorite', tracker.input_text):
            self.logger.debug(f"talk about|my favorite entered")
            artist: entity.ArtistItem = next(iter(entity.ArtistItem.detect(tracker)), None)
            # entity = self.artist_exists(text, ner, knowledge)
            if artist and artist != artist_to_discuss:
                self.logger.debug(f"has artist, artist={artist}, going to favmusician")
                return [NextState(states.FavoriteMusician.name, jump=True)]

            genre: entity.GenreItem = next(iter(entity.GenreItem.detect(tracker)), None)
            if genre:
                tracker.one_turn_store['current_chitchat'] = current_chitchat.keyword if current_chitchat else None
                tracker.one_turn_store['artist_to_discuss'] = artist_to_discuss
                dispatcher.respond_template('artist_question/follow_genre_question/no_pop', {'genre': genre})
                return [NextState(states.FavoriteMusician.name)]

        self.logger.debug(f"handle question")

        question_response = self.command_question_handler.handle_question()
        if self.command_question_handler.handle_is_proposing_topic_response(
            question_response, TopicModule.MUSIC, dispatcher, tracker
        ):
            return [NextState(states.Initial.name)]
        elif question_response:
            self.logger.debug(f"clear question_response: {question_response}")
            dispatcher.respond(question_response.response)
            dispatcher.respond_template('no_match/continue', {})
            dispatcher.respond(current_chitchat.question)
            tracker.one_turn_store['current_chitchat'] = current_chitchat.keyword if current_chitchat else None
            tracker.one_turn_store['artist_to_discuss'] = artist_to_discuss
            return [NextState(states.ChitchatPropose.name)]

        if ChitchatContent.count(tracker) == 3 and artist_to_discuss:
            self.logger.debug(
                f"if theres been {ChitchatContent.count(tracker)} chitchat "
                f"and an artist {artist_to_discuss} exists, go to concert_init")
            return [NextState(states.ConcertInit.name, jump=True)]
        # entity = None

        if 'ans_unknowwn' in tracker.returnnlp.flattened_lexical_intent:
            dispatcher.respond_template('unknown_ack', {})
        elif re.search(r"cannot remember", tracker.input_text):
            dispatcher.respond("That's okay.")
        elif regex_match.is_rapport(tracker):
            dispatcher.respond("That is great! We are alike.")
        elif tracker.returnnlp.answer_positivity is Positivity.neg:
            dispatcher.respond_template('chitchats/ack', {})
        elif len(tracker.input_text.split(' ')) > 5:
            dispatcher.respond_template('long_sentences_ack', {})
        elif regex_match.is_other_answer(tracker):
            dispatcher.respond_template('unknown_ack', {})

        dispatcher.respond(current_chitchat.question)
        tracker.one_turn_store['current_chitchat'] = current_chitchat.keyword if current_chitchat else None
        tracker.one_turn_store['artist_to_discuss'] = artist_to_discuss
        return [NextState(states.ChitchatPropose.name)]
