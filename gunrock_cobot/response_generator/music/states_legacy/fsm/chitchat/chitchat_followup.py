from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states
from response_generator.music.states.fsm.chitchat.chitchat_content import ChitchatContent

from response_generator.music.utils import entity


class ChitchatFollowup(MusicState):

    name = 'chitchat_followup'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        artist_to_discuss: entity.ArtistItem = (
            tracker.one_turn_store.get('artist_to_discuss') or
            tracker.last_utt_store.get('artist_to_discuss') or
            None
        )
        current_chitchat: ChitchatContent = ChitchatContent.from_keyword((
            tracker.one_turn_store.get('current_chitchat') or
            tracker.last_utt_store.get('current_chitchat') or
            ""
        ), dispatcher.tm)

        dispatcher.respond(current_chitchat.followup or "I don't know. It just how I feel about it.")
        tracker.one_turn_store['current_chitchat'] = current_chitchat.keyword if current_chitchat else None
        tracker.one_turn_store['artist_to_discuss'] = artist_to_discuss
        return [NextState(states.ChitchatPropose.name)]
