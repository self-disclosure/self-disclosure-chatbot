
import random
from dataclasses import dataclass, field, InitVar, make_dataclass
from typing import Optional

from response_generator.fsm2 import Tracker, FSMLogger
from template_manager import TemplateManager
from utils import get_working_environment


logger = FSMLogger('CHITCHAT', 'ChitchatContent', 'music.chitchat')


@dataclass
class ChitchatContent:

    keyword: str
    question: str = field(init=False)
    answer: Optional[str] = field(init=False)
    followup: Optional[str] = field(init=False)
    tm_music: InitVar[TemplateManager]

    @classmethod
    def __tracker_set__(cls, d: dict):
        return make_dataclass('ChitchatContent', [(k, v) for k, v in d.items()])

    @property
    def song_question(self):
        return self.keyword in {'emotional', 'karaoke'}

    def __post_init__(self, tm_music: TemplateManager):
        selector = f"chitchats/content/{self.keyword}"

        question_args = {}
        self.question = tm_music.speak(f"{selector}/question", {}, embedded_info=question_args)
        self.followup = question_args.get('folllowup')
        if tm_music.has_selector(f"{selector}/answer"):
            self.answer = tm_music.speak(f"{selector}/answer", {})
        else:
            self.answer = None

    @classmethod
    def from_keyword(cls, keyword: str, tm_music: TemplateManager):
        selector = "chitchats/content"
        if tm_music.has_selector(f"{selector}/{keyword}"):
            return cls(keyword=keyword, tm_music=tm_music)

    @classmethod
    def random(cls, tracker: Tracker, tm_music: TemplateManager):
        keywords = cls._remaining_keywords(tracker, tm_music)
        logger.debug(f"random, keywords = {keywords}")
        if not keywords:
            return
        if (
            get_working_environment() == 'local'
            # and False
        ):
            keywords = sorted(keywords)
            keyword = keywords[0]
        else:
            keyword = random.choice(keywords)
        logger.debug(f"random, keyword selected = {keyword}")
        tracker.persistent_store['chitchat_history'] = (
            (tracker.persistent_store.get('chitchat_history') or []) + [keyword])
        logger.debug(f"random, history = {tracker.persistent_store.get('chitchat_history')}")
        logger.debug(f"random, get = {(tracker.persistent_store.get('chitchat_history') or [])}")
        return cls(keyword=keyword, tm_music=tm_music)

    @classmethod
    def count(cls, tracker: Tracker):
        return len((tracker.persistent_store.get('chitchat_history') or []))

    @classmethod
    def remaining(cls, tracker: Tracker, tm_music: TemplateManager):
        return len(cls._remaining_keywords(tracker, tm_music))

    @classmethod
    def _remaining_keywords(cls, tracker: Tracker, tm_music: TemplateManager):
        selector = "chitchats/content"
        keywords = tm_music.get_child_keys(selector)
        used_keywords = (tracker.persistent_store.get('chitchat_history') or [])
        return [keyword for keyword in keywords if keyword not in used_keywords]
