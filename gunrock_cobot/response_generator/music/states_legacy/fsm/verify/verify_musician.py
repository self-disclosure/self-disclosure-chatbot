from nlu.constants import Positivity
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states
from response_generator.music.states.fsm.chitchat.chitchat_content import ChitchatContent

from response_generator.music.utils import entity


class VerifyMusician(MusicState):
    """
    verify if user wants to talk about the musician
    play_music_response also goes here if there's an artist
    """

    name = 'verify_musician'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        self.logger.debug(f"entering")

        tracker.one_turn_store['verify_musician_trigger'] = (
            tracker.last_utt_store.get('verify_musician_trigger', 0) + 1
        )

        artist: entity.ArtistItem = tracker.last_utt_store.get('play_music_response')

        if tracker.returnnlp.answer_positivity is Positivity.neg:
            self.logger.debug(f"neg answer, going to change_topic")
            return [NextState(states.ChangeTopic.name, jump=True)]

        elif tracker.returnnlp.answer_positivity is Positivity.pos and artist:
            self.logger.debug(f"pos answer with artist={artist}, going to s_artist_question")
            tracker.one_turn_store['artist_to_discuss'] = artist
            return [NextState(states.WhyLikeArtistRespond.name, jump=True)]

        else:
            self.logger.debug(f"default, going to s_chitchat")
            if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                return [NextState(states.Chitchat.name, jump=True)]
            else:
                return [NextState(states.ChangeTopic.name, jump=True)]
