from nlu.constants import Positivity
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states
from response_generator.music.states.fsm.chitchat.chitchat_content import ChitchatContent


class VerifyTemplateArtist(MusicState):
    """
    aka verify_templates

    if the user wants to keep talking the current top 5 artist
    """

    name = 'verify_template_artist'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        self.logger.debug(f"entering")

        if tracker.returnnlp.answer_positivity is Positivity.neg:
            self.logger.debug(f"neg answer, going to s_chitchat")
            if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                return [NextState(states.Chitchat.name, jump=True)]
            else:
                return [NextState(states.ChangeTopic.name, jump=True)]
        else:
            self.logger.debug(f"default, going to s_templates")
            return [NextState(states.TemplateArtistAck.name, jump=True)]
