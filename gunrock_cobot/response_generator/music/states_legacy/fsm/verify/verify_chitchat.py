from nlu.constants import Positivity
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states
from response_generator.music.states.fsm.chitchat.chitchat_content import ChitchatContent


class VerifyChitchat(MusicState):
    """
    verify if we should enter chitchat
    """

    name = 'verify_chitchat'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        current_chitchat: ChitchatContent = ChitchatContent.from_keyword((
            tracker.one_turn_store.get('current_chitchat') or
            tracker.last_utt_store.get('current_chitchat') or
            ""
        ), dispatcher.tm)
        if current_chitchat:
            tracker.one_turn_store['current_chitchat'] = current_chitchat.keyword

        if tracker.returnnlp.answer_positivity is Positivity.neg:
            self.logger.debug(f"answer is neg, leaving music")
            dispatcher.respond_template('chitchats/ack', {})
            dispatcher.propose_continue('STOP')
            return [NextState(states.Initial.name)]
        else:
            self.logger.debug(f"answer is not neg, going to s_chitchat")
            if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                return [NextState(states.Chitchat.name, jump=True)]
            else:
                return [NextState(states.ChangeTopic.name, jump=True)]
