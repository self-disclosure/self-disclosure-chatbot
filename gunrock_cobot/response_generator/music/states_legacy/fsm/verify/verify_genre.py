import re

from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states

from response_generator.music.utils import entity, regex_match


class VerifyGenre(MusicState):
    """
    when we're not sure about genre input
    """

    name = 'verify_genre'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        self.logger.debug(f"entering")

        if regex_match.is_asking_for_recommendation(tracker):
            self.logger.debug(f"asking for rec. Going to recommend_song")
            tracker.one_turn_store['recommend_song_old_state'] = states.Chitchat.name
            return [NextState(states.Initial.name)]

        self.logger.debug(f"handle question")

        question_response = self.command_question_handler.handle_question()
        if self.command_question_handler.handle_is_proposing_topic_response(
            question_response, TopicModule.MUSIC, dispatcher, tracker
        ):
            return [NextState(states.Initial.name)]
        elif question_response:
            self.logger.debug(f"clear question_response: {question_response}")
            dispatcher.respond(question_response.response)
            dispatcher.respond_template('no_match/continue', {})
            dispatcher.respond_template('artist_question/general', {})
            tracker.one_turn_store['current_chitchat'] = self.current_chitchat.keyword if self.current_chitchat else ""
            tracker.one_turn_store['artist_to_discuss'] = self.artist_to_discuss
            return [NextState(states.FavoriteMusician.name)]

        if regex_match.is_other_answer(tracker):
            self.logger.debug(f"other answer, going to s_favmusician")
            dispatcher.respond_template('artist_question/follow_neg', {})
            tracker.one_turn_store['current_chitchat'] = self.current_chitchat.keyword if self.current_chitchat else ""
            tracker.one_turn_store['artist_to_discuss'] = self.artist_to_discuss
            return [NextState(states.FavoriteMusician.name)]

        genre: entity.GenreItem = next(iter(entity.GenreItem.detect(tracker)), None)
        if genre:
            self.logger.debug(f"genre exists, going to s_favmusician")
            if 'pop' in genre.noun:
                dispatcher.respond_template("artist_question/follow_genre_question/pop", {})
            else:
                dispatcher.respond_template("artist_question/follow_genre_question/no_pop", {'genre': genre.noun})
            tracker.one_turn_store['current_chitchat'] = self.current_chitchat.keyword if self.current_chitchat else ""
            tracker.one_turn_store['artist_to_discuss'] = self.artist_to_discuss
            tracker.one_turn_store['genre_to_discuss'] = genre
            return [NextState(states.FavoriteMusician.name)]

        if re.search(r'many|all', tracker.input_text):
            self.logger.debug(f"said many|all, going to s_favmusician")
            dispatcher.respond_template('artist_question/follow_big_fans', {})
            tracker.one_turn_store['current_chitchat'] = self.current_chitchat.keyword if self.current_chitchat else ""
            tracker.one_turn_store['artist_to_discuss'] = self.artist_to_discuss
            return [NextState(states.FavoriteMusician.name)]

        artist: entity.ArtistItem = next(iter(entity.ArtistItem.detect(tracker)), None)
        if artist:
            self.logger.debug(f"artist exists: {artist}, going to s_favmusician")
            return [NextState(states.FavoriteMusician.name, jump=True)]

        self.logger.debug(f"default return, going to s_favmusician")
        dispatcher.respond_template('artist_question/follow_neg', {})
        tracker.one_turn_store['current_chitchat'] = self.current_chitchat.keyword if self.current_chitchat else ""
        tracker.one_turn_store['artist_to_discuss'] = self.artist_to_discuss
        return [NextState(states.FavoriteMusician.name)]
