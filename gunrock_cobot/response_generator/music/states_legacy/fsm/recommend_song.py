import random

from response_generator.common import evi
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states

from response_generator.music.utils import entity


class RecommendSong(MusicState):

    name = 'recommend_song'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        old_state: str = (
            tracker.one_turn_store.get('recommend_song_old_state') or
            tracker.last_utt_store.get('recommend_song_old_state') or
            states.Initial.name
        )

        artist: entity.ArtistItem = next(iter(entity.ArtistItem.detect(tracker)), None)

        self.logger.debug(f"entering, artist={artist}")

        artist_details = artist.artist_details(tracker) if artist else None

        if artist_details and artist_details.songs:
            song = random.choice(artist_details.songs)
            dispatcher.respond_template('recommend_song/song', {'song': song})
            tracker.one_turn_store['artist_to_discuss'] = artist
            tracker.one_turn_store['current_chitchat'] = (
                tracker.one_turn_store.get('current_chitchat') or
                tracker.last_utt_store.get('current_chitchat') or
                None
            )
            return [NextState(old_state)]

        elif artist:
            response = evi.query(f"top songs of {artist.noun}")

        else:
            response = evi.query("top ten songs")

        if response:
            dispatcher.respond_template('recommend_song/response', {'response': response})
        else:
            dispatcher.respond_template('recommend_song/unknow_artist', {})
        return [NextState(old_state)]
