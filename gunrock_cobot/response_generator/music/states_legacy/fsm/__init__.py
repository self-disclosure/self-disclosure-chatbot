from response_generator.music.states.fsm.base import MusicState

# Global States

from .global_states.get_response import GetResponse
from .global_states.play_music import PlayMusicRespond

# Normal States

from .initial import Initial

# Artist
from .artist.favorite_artist import (  # noqa: F401
    FavArtistEnter,
    FavArtistPropose, FavArtistRespond,
)
from .artist.dislike_artist import (  # noqa: F401
    DislikeArtistPropose, DislikeArtistRespond,
)
from .artist.why_like_artist import (  # noqa: F401
    WhyLikeArtistPropose, WhyLikeArtistRespond,
)
from .artist.fav_song_by_artist import (  # noqa: F401
    FavSongByArtistPropose, FavSongByArtistRespond,
)

# Genre
from .genre.favorite_genre import (  # noqa: F401
    FavGenreEnter,
    FavGenrePropose, FavGenreRespond,
)
from .genre.favorite_genre_artist import (  # noqa: F401
    FavGenreArtistPropose, FavGenreArtistRespond,
)

# fav_x

from .favorite.fav_genre import (
    FavoriteGenreProposing, FavoriteGenreRespond,
    FavoriteGenre
)
from .favorite.fav_musician import (
    FavoriteMusicianProposing, FavoriteMusicianRespond,
    FavoriteMusicianDislikeArtistProposing, FavoriteMusicianDislikeArtistRespond,
    FavoriteMusician
)
from .favorite.fav_song import FavoriteSong

from .verify.verify_genre import VerifyGenre
from .verify.verify_musician import VerifyMusician
from .verify.verify_templates import VerifyTemplateArtist
from .verify.verify_chitchat import VerifyChitchat
from .verify.verify_singing import VerifySinging

from .chitchat.chitchat import (
    ChitchatEnter,
    Chitchat
)
from .chitchat.chitchat_propose import ChitchatPropose
from .chitchat.chitchat_respond import ChitchatRespond
from .chitchat.chitchat_followup import ChitchatFollowup

from .concerts.concert_init import ConcertInit
from .concerts.concert import Concert
from .concerts.other_concert import OtherConcert
from .concerts.best_concert import BestConcert

from .singing.singing import Singing, SingingRating
from .singing.user_sing import (
    UserSing, UserSingApplause, UserSingHearMeSingConfirm
)

from .instruments.instruments import (
    InstrumentsUserPrefRespond, InstrumentsUserPrefExit,
    InstrumentsClassicModernPlaystyleProposing, InstrumentsClassicModernPlaystyleRespond,
    InstrumentsLengthExperienceProposing, InstrumentsLengthExperienceRespond,
    InstrumentsUsualSongProposing, InstrumentsUsualSongRespond,
    InstrumentsWhosYourTeacherProposing, InstrumentsWhosYourTeacherRespond,
    InstrumentsFamousMusicianProposing, InstrumentsFamousMusicianRespond,
    InstrumentsFamousMusicianRespondEpilogue,
)

from .template_artist_ack import TemplateArtistAck

from .opinion_fact import ArtistFactsRespond
from .opinion_song import SongNameDropRespond
# from .artist_question import WhyLikeArtistRespond
from .artist_song import FavoriteSongByArtistRespond

from .recommend_song import RecommendSong
from .change_topic import ChangeTopic, TopicSwitch

from .more_facts import MoreFacts
