import random

from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music.states.fsm.base import MusicState
from response_generator.music.states import fsm as states
from response_generator.music.states.fsm.chitchat.chitchat_content import ChitchatContent

from response_generator.music.utils import entity, regex_match
from response_generator.music.utils.legacy import is_english, detect_intent


class ArtistFactsRespond(MusicState):
    """
    aka s_opinionfact

    Respond to response to us disclosing facts about an artist
    """

    name = 'artist_facts_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        intent = detect_intent(tracker.input_text)

        self.logger.debug(f"handle question")
        question_response = self.command_question_handler.handle_question()
        if self.command_question_handler.handle_is_proposing_topic_response(
            question_response, TopicModule.MUSIC, dispatcher, tracker
        ):
            return [NextState(states.Initial.name)]
        elif question_response:
            self.logger.debug(f"clear question_response: {question_response}")
            dispatcher.respond(question_response.response)
            dispatcher.respond_template('no_match/continue', {})
            return [NextState(states.SongNameDropRespond.name)]

        curr_turn_artist = next(iter(entity.ArtistItem.detect(tracker)), None)
        if curr_turn_artist and curr_turn_artist != self.artist_to_discuss:
            # TODO: not just use sentiment
            if tracker.returnnlp[0].sentiment == 'neg':
                self.logger.debug(f"sentiment is neg, going to s_init")
                tracker.persistent_store['first_time'] = False
                return [NextState(states.Initial.name, jump=True)]
            else:
                self.logger.debug(f"sentiment is not neg, going to fav_musician")
                return [NextState(states.FavoriteMusician.name, jump=True)]

        if intent == "answer_no":
            dispatcher.respond_template('opinionfacts/neg_res', {})
        else:
            dispatcher.respond_template('opinionfacts/pos_res', {})

        if regex_match.is_other_answer(tracker):
            dispatcher.respond("Alright.")

        if self.artist_to_discuss:
            artist_songs = self.artist_to_discuss.songs_from_db(tracker)
        else:
            artist_songs = None
        if artist_songs and len(artist_songs) > 0:
            album = random.choice(artist_songs)
        else:
            album = None

        if album and is_english(album):

            dispatcher.respond_template('opinionfacts/ask_album', {'album': album})
            tracker.one_turn_store['artist_to_discuss'] = self.artist_to_discuss
            tracker.one_turn_store['current_chitchat'] = self.current_chitchat
            return [NextState(states.SongNameDropRespond.name)]

        else:
            if ChitchatContent.remaining(tracker, dispatcher.tm) > 0:
                return [NextState(states.Chitchat.name, jump=True)]
            else:
                return [NextState(states.ChangeTopic.name, jump=True)]
