import random
import re
from typing import TYPE_CHECKING

from nlu.constants import TopicModule

from ...utils import entity, regex_match

if TYPE_CHECKING:
    from ...music_automaton import MusicAutomaton


TOTAL_CHITCHATS_AMOUNT = 9


def s_chitchat(self: 'MusicAutomaton', input_dict, artist_check=True,
               chain_response: str = None, chain_with_acknowledgement=False):
    """
    enter chitchat initialization
    """

    self.logger.debug(f"entering. artist_check: {artist_check}, chain_response: {chain_response}")

    chitchat_count = 0
    if(len(input_dict["chitchats"]) > 0):
        chitchats = input_dict["chitchats"]["remaining"]
        chitchat_count = input_dict["chitchats"]["count"]
        chitchat_id = input_dict["chitchats"]["current"]
    else:
        chitchats = [(i+1) for i in range(TOTAL_CHITCHATS_AMOUNT)]
        random.shuffle(chitchats)
        chitchat_id = chitchats.pop()
        chitchat_count = 1

    # chitchat_id = 2
    chitchat = self.read_chitchat_content(chitchat_id)

    # start_text = self.tm_music.speak('chitchats/ack', {})
    # text = input_dict["text"]
    # concept = input_dict["concept"]
    # if input_dict["coreference"] is not None:
    #     text = input_dict["coreference"]["text"]
    # ner = input_dict["nlu"]["ner"]
    # lexical = input_dict["nlu"]["lexical"]
    # concept = input_dict["concept"]
    # knowledge = input_dict["knowledge"]
    # dialog_act = input_dict["dialog_act"]
    # play_match = play_music_match(input_dict["text"])
    # sentiment = input_dict["nlu"]["sentiment"]
    # intent = detect_intent(text)
    # noun_phrase = input_dict["local_noun_phrase"]
    # topic_keyword = input_dict["topic_keyword"]
    response = chain_response + " " if chain_response else ""

    no_match = False
    if regex_match.is_asking_for_recommendation(self.tracker):
        self.logger.debug(f"recommended_terms match.")
        return self.recommend_song(input_dict, 's_chitchat')

    elif regex_match.is_rapport(self.tracker) and not input_dict['artist']:
        self.logger.debug(f"rapport match.")
        input_dict['artist'] = 'taylor swift'
        return self.s_templates(input_dict, 'ts')
    # if play_match:
    #     self.logger.debug(f"play music match.")
    #     artist = detect_artist(text, ner, knowledge, concept)
    #     return self.play_music_response(artist, "s_chitchat",
    #                                     {"artist": input_dict["artist"],
    #                                         "context": input_dict["currentstate"],
    #                                         "propose_continue": "CONTINUE",
    #                                         "chitchats": input_dict["chitchats"]})

    self.logger.debug(f"handle question")
    if not chain_response:
        question_response = self.handle_question(
            is_unanswerable_question=None,
            handle_topic_specific_question=None,
            handle_general_question=None
        )
        if (
            question_response and question_response.bot_propose_module and
            question_response.bot_propose_module != TopicModule.MUSIC
        ):
            self.logger.debug(f"unclear question_response: {question_response}")
            return {
                'response': " ".join([
                    question_response.response,
                    self.tm_transition.speak(f"propose_topic_short/{question_response.bot_propose_module.value}", {})
                ]),
                'propose_continue': 'UNCLEAR',
                'propose_topic': question_response.bot_propose_module.value
            }
        elif question_response:
            self.logger.debug(f"clear question_response: {question_response}")
            return {
                'response': " ".join([
                    question_response.response,
                    self.tm_music.speak('no_match/continue', {}),
                    self.tm_music.speak('artist_question/general', {}),
                ]),
                "next": "s_chitchat",
                'propose_continue': 'CONTINUE',
            }
    # if dialog_act is not None:
    #     if is_open_question(dialog_act):
    #         if self.tracker.central_element.backstory.confidence >= BACKSTORY_THRESHOLD:
    #             self.logger.debug(f"enter backstory.")
    #             return {
    #                 "response": response + self.tracker.central_element.backstory.text,
    #                 "next": "s_chitchat",
    #                 "propose_continue": "CONTINUE",
    #                 "artist": input_dict["artist"],
    #                 "context": input_dict["currentstate"],
    #                 "chitchats": input_dict["chitchats"]
    #             }
    #         evi_response = EVI_bot(text)
    #         if evi_response is not None:
    #             self.logger.debug(f"enter evi.")
    #             return {
    #                 "response": response + evi_response,
    #                 "next": "s_chitchat",
    #                 "propose_continue": "CONTINUE",
    #                 "artist": input_dict["artist"],
    #                 "context": input_dict["currentstate"],
    #                 "chitchats": input_dict["chitchats"]
    #             }
    #         else:
    #             no_match = True

    artist = next(iter(entity.ArtistItem.detect(self.tracker)), None)
    # entity = self.artist_exists(text, ner, knowledge)
    if (
        artist and artist.noun.lower() != input_dict['artist'] and
        self.tracker.returnnlp[0].sentiment != 'neg' and artist_check and
        self.tracker.volatile_store.get('entering_s_favmusician', 0) <= 0
    ):

        self.logger.debug(
            f"jumping to favmusician. "
            f"entity: {artist}, sentiment: {self.tracker.returnnlp[0].sentiment}, artist_check: {artist_check}")
        return self.s_favmusician(input_dict)

    # entity = None

    # create ack to transition to s_chitchat
    if (
        len(self.tracker.input_text.split(' ')) > 5 and
        # s_artist_question can jump directly to s_artist_song which will concatenate with things here
        # so we avoid certain ack
        self.tracker.volatile_store.get('jump_from') != 's_artist_question'
    ):
        acknowledgement = self.tm_music.speak('long_sentences_ack', {})

    elif (
        regex_match.is_other_answer(self.tracker)
    ):
        acknowledgement = self.tm_music.speak('unknown_ack', {})

    elif artist:
        acknowledgement = self.tm_music.speak('chitchats/entity_ack', {'entity': artist.noun})

    # elif 'ans_neg' in self.tracker.returnnlp.lexical_intent:
    #     acknowledgement = self.tm_music.speak('chitchats/ack', {})

    else:
        acknowledgement = self.tm_music.speak('chitchats/ack', {})

    if re.search(r'emotional|karaoke', chitchat["keyword"]):
        song_question = True
    else:
        song_question = False

    if no_match:
        self.logger.debug(f"no_match: {input_dict['no_match']}, chitchat: {chitchat['q']}")
        return {
            "response": input_dict['no_match'] + chitchat["q"],
            "next": "s_chitchatcont",
            "chitchats": {
                "remaining": chitchats,
                "current": chitchat_id,
                "count": chitchat_count
            },
            "propose_continue": "CONTINUE",
            "context": input_dict["currentstate"],
            "artist": input_dict["artist"],
        }
    else:
        self.logger.debug(f"default response: {chain_response}, chitchat: {chitchat['q']}")
        response = []
        if chain_response and chain_with_acknowledgement:
            response.extend([chain_response, acknowledgement])
        elif chain_response and not chain_with_acknowledgement:
            response.append(chain_response)
        elif not chain_response:
            response.append(acknowledgement)

        response.append(chitchat['q'])

        return {
            "response": " ".join(response),
            "next": "s_chitchatcont",
            "chitchats": {
                "remaining": chitchats,
                "current": chitchat_id,
                "count": chitchat_count
            },
            "propose_continue": "CONTINUE",
            "context": input_dict["currentstate"],
            "artist": input_dict["artist"],
            "song_question": song_question,
        }
