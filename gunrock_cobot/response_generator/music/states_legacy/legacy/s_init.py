import re
from typing import Any, Dict, TYPE_CHECKING

from nlu.constants import TopicModule

from ...utils import entity, regex_match

if TYPE_CHECKING:
    from ...music_automaton import MusicAutomaton


def s_init(self: 'MusicAutomaton', input_dict: Dict[str, Any]):
    """
    Init state
    """
    self.logger.debug(f"[MUSIC] entering state: s_init")

    # if input_dict["coreference"] is not None:
    #     text = input_dict["coreference"]["text"]
    # else:
    #     text = input_dict["text"]  # input text but with huristics that choose central elem if len(words) > 7
    # concept = input_dict["concept"]
    # ner = input_dict["nlu"]["ner"]
    # intent = detect_intent(text)
    first_time = input_dict["first_time"]
    # knowledge = input_dict["knowledge"]
    # concept = input_dict["concept"]
    user_id = input_dict["user_id"]
    # sentiment = input_dict["nlu"]["sentiment"]

    artist = next(iter(entity.ArtistItem.detect(self.tracker)), None)
    genre = next(iter(entity.GenreItem.detect(self.tracker)), None)
    instrument = next(iter(entity.InstrumentItem.detect(self.tracker)), None)

    self.logger.debug(f"[MUSIC] s_init artist: {artist}")
    self.logger.debug(f"[MUSIC] s_init genre: {genre}")
    self.logger.debug(f"[MUSIC] s_init instruments: {instrument}")

    # If users want a song recommendation
    if regex_match.is_asking_for_recommendation(self.tracker):
        self.logger.debug(f"[MUSIC] s_init asking for rec")
        return self.recommend_song(input_dict, 's_init')

    # # If users want PLAY_MUSIC
    # if regex_match.is_play_music(self.tracker):
    #     self.logger.debug(f"[MUSIC] s_init matched is_play_music")
    #     return self.play_music_response(artist, "s_init",
    #                                     {"first_time": False})

    # if user is talking about a genre positivly
    if genre and self.tracker.returnnlp[0].sentiment != 'neg':
        return {
            "next": "s_favmusician",
            "response": self.tm_music.speak('artist_question/follow_genre_question/no_pop',
                                            {'genre': genre.noun}),
            "first_time": False,
            "chitchats": input_dict["chitchats"],
            "artist": input_dict["artist"]
        }

    # If users wanna talk about something else, set first time flag off
    if re.search(r'talk about something else|about something else', self.tracker.input_text):
        first_time = False

    # if users wanna talk about a specific artist
    if artist and self.tracker.returnnlp[0].sentiment != 'neg':
        return self.s_favmusician(input_dict)

    self.logger.debug(f"handle question")
    question_response = self.handle_question(
        is_unanswerable_question=None,
        handle_topic_specific_question=None,
        handle_general_question=None
    )
    if (
        question_response and question_response.bot_propose_module and
        question_response.bot_propose_module != TopicModule.MUSIC
    ):
        self.logger.debug(f"unclear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                self.tm_transition.speak(f"propose_topic_short/{question_response.bot_propose_module.value}", {})
            ]),
            'propose_continue': 'UNCLEAR',
            'propose_topic': question_response.bot_propose_module.value
        }
    elif question_response:
        self.logger.debug(f"clear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                self.tm_music.speak('no_match/continue', {}),
                self.tm_music.speak('artist_question/general', {}),
            ]),
            'next': 's_favmusician',
            'propose_continue': 'CONTINUE',
        }

    # # if users are asking an open question -> backstory test
    # if (mutils.is_open_question(self.tracker) and
    #         not re.search(r'change the topic to music|how about music|can we talk about music', text)):
    #     backstory = self.tracker.central_element.backstory
    #     if backstory.confidence >= BACKSTORY_THRESHOLD:
    #         return{
    #             "next": "s_init",
    #             "response": backstory.text,  # TODO: add redirect
    #             "first_time": False,
    #             "propose_continue": "CONTINUE"
    #         }
    #     evi_response = evi.query(text, handle_empty_response=False, response_filter=evi_filter)
    #     if evi_response is not None:
    #         return {
    #             "response": evi_response,  # TODO: add redirect
    #             "next": "s_init",
    #             "propose_continue": "CONTINUE",
    #             "first_time": False,
    #         }
    #     else:
    #         return{
    #             "response": (self.tm_music.speak('no_match/general', {}) +
    #                          self.tm_music.speak('no_match/continue', {}) +
    #                          self.tm_music.speak('artist_question/general', {})),
    #             "next": "s_favmusician",
    #             "artist": input_dict["artist"],
    #             "chitchats": input_dict["chitchats"],
    #             "propose_continue": "CONTINUE",
    #             "first_time": False
    #         }

    # if users like to sing (from open_question)
    # if re.search(r"(like (to sing|singing)|singing|sing(ing)? songs|(sing(ing)?|hum(ming)?) to (songs?|tunes?))",
    #              self.tracker.input_text):
    #     # TODO: finetune regex
    #     # TODO: merge with default flow / completely new dialogs?
    #     return {
    #         'next': "s_favgenre",
    #         'artist': {},
    #         'chitchats': {},
    #         'response': self.tm_music.speak('start_up/open_question/like_to_sing', {})
    #     }

    # if users like to play musical instruments
    if instrument or re.search(r"\b(play instruments?)\b", self.tracker.input_text, re.I):
        # TODO: different dialog if 'play instruments'
        return {
            'next': 's_favgenre',
            'artist': {},
            'chitchats': {},
            'response': self.tm_music.speak('start_up/open_question/play_instruments',
                                            {'instrument': instrument.noun if instrument else 'instrument'})
        }

    if self.redis_helper.is_returning_user_in_music_module(user_id):
        first_time = False

    if first_time:
        return {
            "next": "s_favgenre",
            "artist": {},
            "chitchats": {},
            'propose_continue': 'CONTINUE',
            "response": self.tm_music.speak('start_up/default', {})
        }
    else:
        return {
            "next": "s_favmusician",
            "response": self.tm_music.speak('artist_question/general', {}),
            "first_time": False,
            'propose_continue': 'CONTINUE',
            "chitchats": input_dict["chitchats"]
        }
