import copy
import random
import re
from typing import TYPE_CHECKING

# from response_generator.common import evi
from nlu.constants import TopicModule

from ...music_templates import templates_json as music_templates_json
from ...utils import entity, dynamodb, regex_match

if TYPE_CHECKING:
    from ...music_automaton import MusicAutomaton


def s_templates(self: 'MusicAutomaton', input_dict, preferred_artist=None):
    """
    top 5 artist
    """

    # text = input_dict["text"]
    templates = input_dict["templates"]
    # concept = input_dict["concept"]
    # ner = input_dict["nlu"]["ner"]
    # knowledge = input_dict["knowledge"]
    artists = {
        'mj': 'michael jackson',
        'ts': 'taylor swift',
        'id': 'imagine dragons',
        'm5': 'maroon 5',
        'dg': 'drake'
    }
    # lexical = input_dict["nlu"]["lexical"]
    # dialog_act = input_dict["dialog_act"]
    # sentiment = input_dict["nlu"]["sentiment"]
    # no_match = False
    matched_song = None
    skip_artist_check = False

    self.logger.debug(f"begin decision: {self.tracker.input_text} templates={templates}")

    if re.search(r"why do you like|oh really", self.tracker.input_text):
        self.logger.debug(f"why do you like")
        return {
            "next": 's_templates',
            "response": self.tm_music.speak('templates/why_love', {}),
            "artist": input_dict["artist"],
            "templates": templates,
            "propose_continue": "CONTINUE"
        }

    if preferred_artist is not None:
        self.logger.debug(f"preferred_artist is not None: {preferred_artist}")

        templates["remaining"] = copy.copy(music_templates_json.get(preferred_artist))
        templates["current"] = templates["remaining"][0]
        templates["remaining"].remove(templates["remaining"][0])
        self.logger.debug(f"preferred_artist is not None templates done: {templates}")

    if regex_match.is_asking_for_recommendation(self.tracker):
        self.logger.debug(f"is asking for recommendation")
        return self.recommend_song(input_dict, 's_chitchat')

    # if regex_match.is_play_music(self.tracker):
    #     artist = detect_artist(text, ner, knowledge)
    #     return self.play_music_response(artist, "s_templates",
    #                                     {"templates": templates,
    #                                         "artist": input_dict["artist"]})

    skip_dialog_check = False

    if (
        re.search(r'can we talk about', self.tracker.input_text) and
        re.search(r"taylor swift|michael jackson|imagine dragons|maroon 5|drake|adam levine",
                  self.tracker.input_text)
    ):
        self.logger.debug(f"skip_dialog_check (match handpicked artist)")
        skip_dialog_check = True

    if re.search(r'imagine dragons', self.tracker.input_text):
        self.logger.debug(f"skip_dialog_check (match imagine dragons)")
        skip_dialog_check = True

    if 'current' in templates and templates["current"]["keyword"] == "fav_song":
        self.logger.debug(f"skip_dialog_check, skip_artist_check (current in templates)")
        skip_dialog_check = True
        skip_artist_check = True

    # detected = self.artist_exists(text, ner, knowledge, concept)
    curr_turn_artist = next(iter(entity.ArtistItem.detect(self.tracker)), None)
    if curr_turn_artist and not skip_artist_check:
        self.logger.debug(f"artist_exists and not skip_artist_check")

        if (
            curr_turn_artist.noun != input_dict['artist'] and
            not re.search(r"taylor swift|michael jackson|imagine dragons|maroon 5|drake|adam levine",
                          self.tracker.input_text) and
            self.tracker.central_element.sentiment != 'neg'
        ):
            self.logger.debug(f"jumping to s_favmusician")
            return self.s_favmusician(input_dict)
        elif curr_turn_artist.noun == input_dict['artist']:
            self.logger.debug(f"jumping to s_chitchat; "
                              f"detected {curr_turn_artist}, input_dict['artist'] {input_dict['artist']}")
            return self.s_chitchat(input_dict)

    qhandler_response = None
    if not skip_dialog_check:
        self.logger.debug(f"handle question")
        question_response = self.handle_question(
            is_unanswerable_question=None,
            handle_topic_specific_question=None,
            handle_general_question=None
        )
        if (
            question_response and question_response.bot_propose_module and
            question_response.bot_propose_module != TopicModule.MUSIC
        ):
            self.logger.debug(f"unclear question_response: {question_response}")
            return {
                'response': " ".join([
                    question_response.response,
                    self.tm_transition.speak(f"propose_topic_short/{question_response.bot_propose_module.value}", {})
                ]),
                'propose_continue': 'UNCLEAR',
                'propose_topic': question_response.bot_propose_module.value
            }
        elif question_response:
            self.logger.debug(f"clear question_response: {question_response}")
            qhandler_response = question_response.response
            # return {
            #     "response": input_dict["no_match"] + "So, speaking of " + input_dict["artist"]+", " + response,
            #     "next": "s_templates",
            #     "artist": input_dict["artist"],
            #     "propose_continue": "CONTINUE",
            #     "first_time": False,
            #     "templates": templates
            # }
            # return {
            #     'response': " ".join([
            #         question_response.response,
            #         self.tm_music.speak('no_match/continue', {}),
            #         self.tm_music.speak('artist_question/general', {}),
            #     ]),
            #     "next": "s_chitchat",
            #     'propose_continue': 'CONTINUE',
            # }
    # if not skip_dialog_check and mutils.is_open_question(self.tracker):
    #     self.logger.debug(f"backstory")
    #     if self.tracker.central_element.backstory.confidence >= BACKSTORY_THRESHOLD:
    #         return{
    #             "next": 's_templates',
    #             "response": self.tracker.central_element.backstory.text,
    #             "artist": input_dict["artist"],
    #             "templates": templates,
    #             "propose_continue": "CONTINUE"
    #         }
    #     response = evi.query(self.tracker.input_text)
    #     if response is not None:
    #         return {
    #             "response": response,
    #             "next": "s_templates",
    #             "propose_continue": "CONTINUE",
    #             "artist": input_dict["artist"],
    #             "first_time": False,
    #             "templates": templates
    #         }
    #     else:
    #         no_match = True

    if preferred_artist is None or preferred_artist not in artists:
        self.logger.debug(f"preferred_artist is None")
        preferred_artist = random.choice(list(artists.keys()))
    if input_dict['artist'] == {}:
        self.logger.debug(f"input_dict['artist'] is empty dict")
        input_dict['artist'] = artists[preferred_artist]

    if "remaining" in templates:
        self.logger.debug(f"'remaining' in templates: {templates}")
        templates = input_dict["templates"]
    else:
        self.logger.debug(f"'remaining' not in templates: {templates}")

        if regex_match.is_rapport(self.tracker):
            self.logger.debug(f"is rapport, going taylor swift")
            input_dict['artist'] = 'taylor swift'
            preferred_artist = 'ts'
        templates["remaining"] = copy.copy(music_templates_json.get(preferred_artist))
        for item in templates["remaining"]:
            if item["keyword"] == preferred_artist:
                templates["current"] = item
                templates["remaining"].remove(item)
        return{
            "response": templates["current"]["response"],
            "next": "s_templates",
            "artist": input_dict["artist"],
            "templates": templates
        }

    keyword = templates["current"]["pos_response"]
    # acknowledgements = ["okay. ",
    #                     "<say-as interpret-as='interjection'>uh huh</say-as>. "]
    response = ''  # random.choice(acknowledgements)
    # fav_song_trig = False
    if templates["current"]["keyword"] == "fav_song":
        self.logger.debug(f"current keyword is 'fav_song'")
        keyword = templates["current"]["neg_response"]
        match = templates["current"]["match"]
        # songs = get_all_songs(input_dict['artist'].lower())
        songs_by_artist = dynamodb.get_songs_by_artist(input_dict['artist'].lower())
        curr_turn_song = next(iter(entity.SongItem.detect(self.tracker)), None)
        matched_song = None

        if songs_by_artist:
            for song in songs_by_artist:
                pattern = re.compile(re.escape(song.lower()))
                if re.search(pattern, self.tracker.input_text):
                    matched_song = song
        if (not matched_song or not songs_by_artist) and curr_turn_song:
            matched_song = curr_turn_song.noun

        # if songs:
        #     for song in songs:
        #         pattern = re.compile(re.escape(song.lower()))
        #         if re.search(pattern, text):
        #             matched_song = song
        # if not matched_song or not songs:
        #     if concept:
        #         for item in concept:
        #             if 'data' in item:
        #                 if 'song' in item['data']:
        #                     matched_song = item['noun']

        if re.search(match, self.tracker.input_text):
            response = templates["current"]["match_text"]
            return {
                "response": response,
                "next": "s_chitchat",
                "artist": input_dict["artist"],
                "propose_continue": "CONTINUE",
                "first_time": False,
                "templates": templates
            }

    if 'ans_neg' in self.tracker.returnnlp.lexical_intent:
        self.logger.debug(f"ans_neg in lexical")
        keyword = templates["current"]["neg_response"]
        response = "Alright. "

    if regex_match.is_other_answer(self.tracker):
        self.logger.debug(f"unknown_match")
        keyword = templates["current"]["neg_response"]
        response = "Alright. "

    if matched_song:
        self.logger.debug(f"matched_song: {matched_song}")
        response = matched_song + ' is a good choice. '

    if keyword == 'exit':
        self.logger.debug(f"keyword == 'exit'")
        return self.s_chitchat(input_dict)

    self.logger.debug(f"looping templates")
    for template in templates["remaining"]:
        if keyword == template["keyword"]:
            response += template["response"]
            templates["current"] = template
            templates["remaining"].remove(template)

    acknowledgement = ''
    if len(self.tracker.input_text.split(" ")) > 4 and not regex_match.is_lets_talk_about(self.tracker):
        # skip this if "lets talk about michael jackson. Should've been coming from s_favmusician, and is a global jump"
        acknowledgement = self.tm_music.speak("long_sentences_ack", {})

    if all(i in self.tracker.returnnlp.lexical_intent for i in ['ans_neg', 'ask_leave']):
        self.logger.debug(f"lexical_intent is ['ans_neg', 'ask_leave']")
        artist = input_dict['artist']
        return {
            "response": "It seems you are not a " + input_dict[
                "artist"] + " fan. Would you like to keep talking about " + input_dict["artist"] + " ? ",
            "next": "verify_templates",
            "artist": artist,
            "propose_continue": "CONTINUE",
            "first_time": False,
            "templates": templates
        }
    elif qhandler_response:
        return {
            "response": (
                qhandler_response + ". So, speaking of " + input_dict["artist"] + ", " + response
            ),
            "next": "s_templates",
            "artist": input_dict["artist"],
            "propose_continue": "CONTINUE",
            "first_time": False,
            "templates": templates
        }
    else:
        return {
            "response": (acknowledgement + " " + response),
            "next": "s_templates",
            "artist": input_dict["artist"],
            "propose_continue": "CONTINUE",
            "first_time": False,
            "templates": templates
        }
