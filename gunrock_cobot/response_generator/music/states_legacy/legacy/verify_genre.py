import re
from typing import TYPE_CHECKING

from nlu.constants import TopicModule

from ...utils import entity, regex_match

if TYPE_CHECKING:
    from ...music_automaton import MusicAutomaton


def verify_genre(self: 'MusicAutomaton', input_dict):
    """
    when we're not sure about genre input
    """

    # text = input_dict["text"]
    # concept = input_dict["concept"]
    # if input_dict["coreference"] is not None:
    #     text = input_dict["coreference"]["text"]
    # intent = detect_intent(text)
    # noun_phrase = input_dict["local_noun_phrase"]
    # dialog_act = input_dict["dialog_act"]
    # topic_keyword = input_dict["topic_keyword"]
    # ner = input_dict["nlu"]["ner"]
    # knowledge = input_dict["knowledge"]
    # concept = input_dict["concept"]

    self.logger.debug(f"entering")

    if regex_match.is_asking_for_recommendation(self.tracker):
        self.logger.debug(f"rec, going to recommend song")
        return self.recommend_song(input_dict, 's_init')

    # play_match = play_music_match(input_dict["text"])
    # if play_match:
    #     artist = detect_artist(text, ner, knowledge)
    #     response = self.tm_music.speak('play_music_ack/simple', {})
    #     if artist is not None:
    #         response = self.tm_music.speak('play_music_ack/back_to_music', {})
    #         return {
    #             "next": "verify_musician",
    #             "response": response,
    #             "artist": artist,
    #             "propose_continue": "UNCLEAR"
    #         }
    #     return{
    #         "next": "s_init",
    #         "response": response,
    #         "first_time": False
    #     }

    self.logger.debug(f"handle question")
    question_response = self.handle_question(
        is_unanswerable_question=None,
        handle_topic_specific_question=None,
        handle_general_question=None
    )
    if (
        question_response and question_response.bot_propose_module and
        question_response.bot_propose_module != TopicModule.MUSIC
    ):
        self.logger.debug(f"unclear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                self.tm_transition.speak(f"propose_topic_short/{question_response.bot_propose_module.value}", {})
            ]),
            'propose_continue': 'UNCLEAR',
            'propose_topic': question_response.bot_propose_module.value
        }
    elif question_response:
        self.logger.debug(f"clear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                self.tm_music.speak('no_match/continue', {}),
                self.tm_music.speak('artist_question/general', {}),
            ]),
            "next": "s_favmusician",
            "artist": input_dict.get("artist"),
            "chitchats": input_dict.get("chitchats"),
            'propose_continue': 'CONTINUE',
        }

    # if dialog_act is not None:
    #     if is_open_question(dialog_act):
    #         if self.tracker.central_element.backstory.confidence >= BACKSTORY_THRESHOLD:
    #             return{
    #                 "next": "s_init",
    #                 "response": self.tracker.central_element.backstory.text,
    #                 "first_time": False,
    #                 "propose_continue": "CONTINUE"
    #             }
    #         response = EVI_bot(text)
    #         if response is not None:
    #             return {
    #                 "response": response,
    #                 "next": "s_init",
    #                 "propose_continue": "CONTINUE",
    #                 "first_time": False,
    #                 "artist": input_dict["artist"],
    #                 "chitchats": input_dict["chitchats"]
    #             }
    #         else:
    #             return{
    #                 "response": input_dict['no_match'] +
    #                 self.tm_music.speak('no_match/continue', {}) +
    #                 self.tm_music.speak('artist_question/general', {}),
    #                 "next": "s_favmusician",
    #                 "artist": input_dict["artist"],
    #                 "chitchats": input_dict["chitchats"],
    #                 "propose_continue": "CONTINUE"
    #             }

    if regex_match.is_other_answer(self.tracker):
        self.logger.debug(f"other answer, going to s_favmusician")
        return{
            "next": "s_favmusician",
            "response": self.tm_music.speak('artist_question/follow_neg', {}),
            "artist": input_dict["artist"],
            "chitchats": input_dict["chitchats"]
        }

    genre = next(iter(entity.GenreItem.detect(self.tracker)), None)
    if genre:
        self.logger.debug(f"genre exists, going to s_favmusician")
        if 'pop' in genre.noun:
            response = self.tm_music.speak("artist_question/follow_genre_question/pop", {})
        else:
            response = self.tm_music.speak("artist_question/follow_genre_question/no_pop", {'genre': genre.noun})
        return {
            "next": "s_favmusician",
            "response": response,
            "first_time": False,
            "artist": input_dict["artist"],
            "chitchats": input_dict["chitchats"],
            "genre": genre.noun,
        }

    if re.search(r'many|all', self.tracker.input_text):
        self.logger.debug(f"said many|all, going to s_favmusician")
        return {
            "next": "s_favmusician",
            "response": self.tm_music.speak('artist_question/follow_big_fans', {}),
            "first_time": False,
            "artist": input_dict["artist"],
            "chitchats": input_dict["chitchats"]
        }

    artist = next(iter(entity.ArtistItem.detect(self.tracker)), None)
    if artist:
        self.logger.debug(f"artist exists: {artist}, going to s_favmusician")
        return self.s_favmusician(input_dict)

    self.logger.debug(f"default return, going to s_favmusician")
    return {
        "next": "s_favmusician",
        "response": self.tm_music.speak('artist_question/follow_neg', {}),
        "first_time": False,
        "artist": input_dict["artist"],
        "chitchats": input_dict["chitchats"]
    }
