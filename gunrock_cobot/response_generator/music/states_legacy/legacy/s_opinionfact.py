import random
from typing import TYPE_CHECKING

from nlu.constants import TopicModule

from ...utils import entity, regex_match
from ...utils.legacy import detect_intent, is_english

if TYPE_CHECKING:
    from ...music_automaton import MusicAutomaton


def s_opinionfact(self: 'MusicAutomaton', input_dict):
    """
    trivia
    """

    # text = input_dict["text"]
    # concept = input_dict["concept"]
    # if input_dict["coreference"] is not None:
    #     text = input_dict["coreference"]["text"]
    # ner = input_dict["nlu"]["ner"]
    intent = detect_intent(self.tracker.input_text)
    artist = input_dict["artist"]
    # knowledge = input_dict["knowledge"]
    # dialog_act = input_dict["dialog_act"]
    # sentiment = input_dict["nlu"]["sentiment"]
    # no_match = False

    # play_match = play_music_match(input_dict["text"])
    # if play_match:
    #     artist = detect_artist(text, ner, knowledge)
    #     return self.play_music_response(artist, "s_opinionsong", {
    #                                         "artist": artist,
    #                                         "propose_continue": "CONTINUE",
    #                                         "chitchats": input_dict["chitchats"]
    #                                     })

    self.logger.debug(f"handle question")
    question_response = self.handle_question(
        is_unanswerable_question=None,
        handle_topic_specific_question=None,
        handle_general_question=None
    )
    if (
        question_response and question_response.bot_propose_module and
        question_response.bot_propose_module != TopicModule.MUSIC
    ):
        self.logger.debug(f"unclear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                self.tm_transition.speak(f"propose_topic_short/{question_response.bot_propose_module.value}", {})
            ]),
            'propose_continue': 'UNCLEAR',
            'propose_topic': question_response.bot_propose_module.value
        }
    elif question_response:
        self.logger.debug(f"clear question_response: {question_response}")
        # TODO: check joiner
        return {
            'response': " ".join([
                question_response.response,
                self.tm_music.speak('no_match/continue', {}),
                # self.tm_music.speak('artist_question/general', {}),
            ]),
            "next": "s_opinionsong",
            "artist": artist,
            'propose_continue': 'CONTINUE',
            "chitchats": input_dict["chitchats"]
        }
    # if dialog_act is not None:
    #     if is_open_question(dialog_act):
    #         if self.tracker.central_element.backstory.confidence >= BACKSTORY_THRESHOLD:
    #             return{
    #                 "next": "s_opinionsong",
    #                 "response": self.tracker.central_element.backstory.text,
    #                 "artist": artist,
    #                 "propose_continue": "CONTINUE",
    #                 "chitchats": input_dict["chitchats"]
    #             }
    #         response = EVI_bot(text)
    #         if response is not None:
    #             return{
    #                 "next": "s_opinionsong",
    #                 "response": response,
    #                 "artist": artist,
    #                 "propose_continue": "CONTINUE",
    #                 "chitchats": input_dict["chitchats"]
    #             }
    #         else:
    #             no_match = True

    curr_turn_artist = next(iter(entity.ArtistItem.detect(self.tracker)), None)
    # entity = self.artist_exists(text, ner, knowledge, concept)
    if curr_turn_artist and curr_turn_artist.noun != artist['name']:
        # TODO: not just use sentiment
        if self.tracker.returnnlp[0].sentiment == 'neg':
            self.logger.debug(f"sentiment is neg, going to s_init")
            input_dict["first_time"] = False
            return self.s_init(input_dict)
        else:
            self.logger.debug(f"sentiment is not neg, going to s_init")
            return self.s_favmusician(input_dict)

    if intent == "answer_no":
        response = self.tm_music.speak('opinionfacts/neg_res', {})
    else:
        response = self.tm_music.speak('opinionfacts/pos_res', {})

    if regex_match.is_other_answer(self.tracker):
        response = "Alright. "

    if len(artist['songs']) > 0:
        album = random.choice(artist['songs'])
    else:
        album = None

    if album and is_english(album):

        response += self.tm_music.speak('opinionfacts/ask_album', {'album': album})
        # if no_match:
        #     return {
        #         "response": input_dict['no_match'] + response,
        #         "next": "s_opinionsong",
        #         "artist": artist,
        #         "propose_continue": "CONTINUE",
        #         "chitchats": input_dict["chitchats"]
        #     }
        return {
            "next": "s_opinionsong",
            "response": response,
            "artist": artist,
            "propose_continue": "CONTINUE",
            "chitchats": input_dict["chitchats"]
        }

    else:
        return self.s_chitchat(input_dict)
