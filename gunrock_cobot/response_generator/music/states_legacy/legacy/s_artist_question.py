from typing import TYPE_CHECKING

from nlu.constants import TopicModule

from ...utils import entity, regex_match

if TYPE_CHECKING:
    from ...music_automaton import MusicAutomaton


def s_artist_question(self: 'MusicAutomaton', input_dict):
    """
    find an artist but not in our database
    answers "why do you like <artist>"
    ask "what is your favorite song by <artist>"
    """

    artist = input_dict["artist"]
    # text = input_dict["text"]
    # concept = input_dict["concept"]
    # ner = input_dict["nlu"]["ner"]
    # knowledge = input_dict["knowledge"]
    # lexical = input_dict["nlu"]["lexical"]
    # if input_dict["coreference"] is not None:
    #     text = input_dict["coreference"]["text"]
    # play_match = play_music_match(input_dict["text"])
    # dialog_act = input_dict["dialog_act"]

    self.logger.debug(f"[MUSIC] s_artist_question: entering")

    # Patch for if artist is a string
    if isinstance(artist, str):
        artist = {'name': artist}

    # if play_match:
    #     artist = detect_artist(text, ner, knowledge)
    #     self.logger.info(f"[MUSIC] s_artist_question artist (play_match): {artist}")
    #     return self.play_music_response(artist, "s_artist_question",
    #                                     {"chitchats": input_dict["chitchats"],
    #                                         "artist": artist['name']})

    self.logger.info(f"[MUSIC] s_artist_question artist: {artist}")

    curr_turn_artist = next(iter(entity.ArtistItem.detect(self.tracker)), None)
    if curr_turn_artist:
        self.logger.debug(f"[MUSIC] s_artist_question curr_turn_artist exists.")
        return self.s_favmusician(input_dict)

    self.logger.debug(f"handle question")
    question_response = self.handle_question(
        is_unanswerable_question=None,
        handle_topic_specific_question=None,
        handle_general_question=None
    )
    if (
        question_response and question_response.bot_propose_module and
        question_response.bot_propose_module != TopicModule.MUSIC
    ):
        self.logger.debug(f"unclear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                self.tm_transition.speak(f"propose_topic_short/{question_response.bot_propose_module.value}", {})
            ]),
            'propose_continue': 'UNCLEAR',
            'propose_topic': question_response.bot_propose_module.value
        }
    elif question_response:
        self.logger.debug(f"clear question_response: {question_response}")
        if artist and artist.get('name'):
            return {
                'response': " ".join([
                    question_response.response,
                    self.tm_music.speak('no_match/continue', {}),
                    self.tm_music.speak('fav_song/ask_song_of_artist', {'artist': artist['name']}),
                ]),
                "next": "s_artist_song",
                'propose_continue': 'CONTINUE',
            }
        else:
            return {
                'response': " ".join([
                    question_response.response,
                    self.tm_music.speak('no_match/continue', {}),
                    self.tm_music.speak('artist_question/general', {}),
                ]),
                "next": "s_chitchat",
                'propose_continue': 'CONTINUE',
            }
    # if dialog_act is not None:
    #     if is_open_question(dialog_act):
    #         self.logger.debug(f"[MUSIC] s_artist_question detect dialog_act is an open question. Use backstory")
    #         if self.tracker.central_element.backstory.confidence >= BACKSTORY_THRESHOLD:
    #             return{
    #                 "next": 's_artist_question',
    #                 "response": self.tracker.central_element.backstory.text,
    #                 "artist": artist['name'],
    #                 "chitchats": input_dict["chitchats"],
    #                 "propose_continue": "CONTINUE"
    #             }
    #         response = EVI_bot(text)
    #         if response is not None:
    #             self.logger.debug(f"[MUSIC] s_artist_question backstory fallback to evi")
    #             return {
    #                 "response": response,
    #                 "next": "s_artist_question",
    #                 "propose_continue": "CONTINUE",
    #                 "first_time": False,
    #                 "artist": artist,
    #                 "chitchats": input_dict["chitchats"]
    #             }
    #         else:
    #             self.logger.debug(f"[MUSIC] s_artist_question has other dialog act. Asking favorite song by artist")
    #             return{
    #                 "response": input_dict['no_match'] + self.tm_music.speak(
    #                     'fav_song/ask_song_of_artist', {'artist': artist['name']}),
    #                 "next": "s_artist_song",
    #                 "artist": artist,
    #                 "chitchats": input_dict["chitchats"],
    #                 "propose_continue": "CONTINUE"
    #             }
    if 'ans_neg' in self.tracker.returnnlp.lexical_intent:
        self.logger.debug(f"[MUSIC] s_artist_question: ans_neg")
        return self.s_chitchat(input_dict)

    song: entity.SongItem = next(iter(entity.SongItem.detect(self.tracker)), None)
    self.logger.debug(
        f"[MUSIC] s_artist_question: detected song: {song}, "
        f"{song and artist['name'].lower() == song.artist_name.lower()}")
    if song and artist['name'].lower() == song.artist_name.lower():
        self.logger.debug(f"[MUSIC] s_artist_question jumping to s_artist_song: {song}, {artist}")
        self.tracker.volatile_store['jump_from'] = 's_artist_question'
        return self.s_artist_song(input_dict)

    if regex_match.is_other_answer(self.tracker):
        self.logger.debug(f"[MUSIC] s_artist_question: is_other_answer response")
        return {
            "next": "s_artist_song",
            "response": self.tm_music.speak('fav_song/ask_song_of_artist_unmatch', {'artist': artist['name']}),
            "artist": artist,
            "chitchats": input_dict["chitchats"]
        }
    else:
        self.logger.debug(f"[MUSIC] s_artist_question: default response")
        return {
            "next": "s_artist_song",
            "response": self.tm_music.speak('fav_song/ask_song_of_artist', {'artist': artist['name']}),
            "artist": artist,
            "chitchats": input_dict["chitchats"]
        }
