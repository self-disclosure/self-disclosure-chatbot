from typing import TYPE_CHECKING

from nlu.constants import Positivity

if TYPE_CHECKING:
    from ...music_automaton import MusicAutomaton


def verify_templates(self: 'MusicAutomaton', input_dict):
    """
    if the user wants to keep talking the current top 5 artist
    """

    # text = input_dict["text"]
    # concept = input_dict["concept"]
    # lexical = input_dict["nlu"]["lexical"]
    # dialog_act = input_dict["dialog_act"]
    # if input_dict["coreference"] is not None:
    #     text = input_dict["coreference"]["text"]
    # intent = detect_intent(text)
    # negative = is_negative(dialog_act, lexical, intent)

    self.logger.debug(f"entering")

    if self.tracker.returnnlp.answer_positivity is Positivity.neg:
        self.logger.debug(f"neg answer, going to s_chitchat")
        return self.s_chitchat(input_dict)
    else:
        self.logger.debug(f"default, going to s_templates")
        return self.s_templates(input_dict)
