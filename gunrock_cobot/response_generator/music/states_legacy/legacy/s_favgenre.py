import re
from typing import TYPE_CHECKING

from nlu.constants import Positivity, TopicModule

from ...utils import entity, regex_match

if TYPE_CHECKING:
    from ...music_automaton import MusicAutomaton


def s_favgenre(self: 'MusicAutomaton', input_dict):
    """
    States (don't know uses yet)
    answer "what's your favorite genre"
    """
    self.logger.debug(f"entering s_favgenre")

    # transitions
    # text = input_dict["text"]
    user_id = input_dict["user_id"]
    # user = {}
    # if input_dict["coreference"] is not None:
    #     text = input_dict["coreference"]["text"]
    # intent = detect_intent(text)
    # noun_phrase = input_dict["local_noun_phrase"]
    # ner = input_dict["nlu"]["ner"]
    # knowledge = input_dict["knowledge"]
    # topic_keyword = input_dict["topic_keyword"]
    # concept = input_dict["concept"]

    if regex_match.is_asking_for_recommendation(self.tracker):
        self.logger.debug(f"entering recommend_terms regex match")
        return self.recommend_song(input_dict, 's_init')

    # if regex_match.is_play_music(self.tracker):
    #     self.logger.debug(f"entering regex play_music_match")
    #     artist = entity.ArtistItem.detect(self.tracker)
    #     if artist is not None:
    #         return self.play_music_response(artist[0].noun, "s_init",
    #                                         {"first_time": False})
    #     self.logger.debug(f"falling out of regex play_music_match")

    self.logger.debug(f"handle question")

    def is_unanswerable_question():
        # self.logger.debug(f"is unanswerable? {regex_match.is_asking_your_favorite(self.tracker)}")
        if regex_match.is_asking_your_favorite(self.tracker):
            # ignore it if it's asking 'what's yours', handled below
            return False
        elif self.question_detector.is_ask_back_question() or self.question_detector.is_follow_up_question():
            return True
        else:
            return False

    question_response = self.handle_question(
        is_unanswerable_question=is_unanswerable_question,
        handle_topic_specific_question=None,
        handle_general_question=None
    )
    if (
        question_response and question_response.bot_propose_module and
        question_response.bot_propose_module != TopicModule.MUSIC
    ):
        self.logger.debug(f"unclear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                self.tm_transition.speak(f"propose_topic_short/{question_response.bot_propose_module.value}", {})
            ]),
            'propose_continue': 'UNCLEAR',
            'propose_topic': question_response.bot_propose_module.value
        }
    elif question_response:
        self.logger.debug(f"clear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                self.tm_music.speak('no_match/continue', {}),
                self.tm_music.speak('artist_question/general', {}),
            ]),
            "next": "s_favmusician",
            'propose_continue': 'CONTINUE',
        }
    # if mutils.is_open_question(self.tracker):
    #     self.logger.debug(f"entering is_open_question")
    #     backstory = self.tracker.central_element.backstory
    #     if backstory.confidence >= BACKSTORY_THRESHOLD:
    #         self.logger.debug(f"entering is_open_question passes backstory threshold")
    #         return{
    #             "next": "s_init",
    #             "response": backstory.text,
    #             "first_time": False,
    #             "chitchats": input_dict["chitchats"],
    #             "artist": input_dict["artist"],
    #             "propose_continue": "CONTINUE"
    #         }
    #     response = evi.query(self.tracker.input_text)
    #     if response is not None:
    #         self.logger.debug(f"entering is_open_question passes evi threshold")
    #         return {
    #             "response": response,
    #             "next": "s_init",
    #             "propose_continue": "CONTINUE",
    #             "first_time": False,
    #             "artist": input_dict["artist"],
    #             "chitchats": input_dict["chitchats"]
    #         }
    #     else:
    #         self.logger.debug(f"entering is_open_question fallback")
    #         return{
    #             "response": input_dict['no_match'] +
    #             self.tm_music.speak('no_match/continue', {}) +
    #             self.tm_music.speak('artist_question/general', {}),
    #             "next": "s_favmusician",
    #             "artist": input_dict["artist"],
    #             "chitchats": input_dict["chitchats"],
    #             "propose_continue": "CONTINUE"
    #         }

    if regex_match.is_other_answer(self.tracker):
        self.logger.debug(f"entering unknown_match")
        return{
            "next": "s_favmusician",
            "response":
                self.tm_music.speak('unknown_ack', {}) +
                self.tm_music.speak('artist_question/general', {}),
            "artist": input_dict["artist"],
            "chitchats": input_dict["chitchats"]
        }

    genres = entity.GenreItem.detect(self.tracker)
    if genres:
        self.logger.debug(f"entering; detected genre entity ({genres})")
        genre = genres[0]
        if self.tracker.central_element.sentiment == 'neg':
            self.logger.debug(f"following up with user's neg sentiment")
            return {
                "next": "s_favmusician",
                "response": self.tm_music.speak('follow_neg', {}),
                "first_time": False,
                "artist": input_dict["artist"],
                "chitchats": input_dict["chitchats"]
            }
        # elif (self.redis_helper.is_known_genre(re.sub(r"\b(music|songs?)\b", "", genre.noun).strip())):
        else:
            # TODO: why are we checking redis again
            self.logger.debug(f"following up genre")
            if not self.redis_helper.is_returning_user_in_music_module(user_id):
                if ('pop' in genre.noun and
                    not (self.tracker.one_turn_store.get('verify_song') or
                         self.tracker.last_utt_store.get('verify_song'))):
                    response = self.tm_music.speak("artist_question/follow_genre_question/pop", {})
                else:
                    response = self.tm_music.speak("artist_question/follow_genre_question/no_pop",
                                                   {'genre': genre.noun})
            return {
                "next": "s_favmusician",
                "response": response,
                "first_time": False,
                "chitchats": input_dict["chitchats"],
                "artist": input_dict["artist"],
                "genre": genre.noun,
            }

    if regex_match.is_saying_all_of_them(self.tracker):
        self.logger.debug(f"entering user liking all of it")
        return {
            "next": "s_favmusician",
            "response": self.tm_music.speak('artist_question/follow_big_fans', {}),
            "first_time": False,
            "artist": input_dict["artist"],
            "chitchats": input_dict["chitchats"]
        }

    artist = next(iter(entity.ArtistItem.detect(self.tracker)), None)
    self.logger.debug(f"detected artist {artist}")
    if (artist and self.tracker.returnnlp[0].sentiment != 'neg'):
        self.logger.debug(f"entering artist_exists and user sentiment is not neg")
        return self.s_favmusician(input_dict)

    songs = entity.SongItem.detect(self.tracker)
    if songs and not regex_match.is_false_songs(self.tracker):
        self.logger.debug(f"s_favgenre got songs: {songs}")
        # temp reroute to self with ack
        self.tracker.one_turn_store['verify_song'] = True
        return {
            "next": "s_favgenre",
            "artist": {},
            "chitchats": {},
            "response": self.tm_music.speak("verify_song/with_ner", {'song_name': songs[0].noun})
        }
        # return {
        #     "next": "s_favsong",
        #     "response": response,
        #     "artist": artist['name'],
        #     "propose_continue": "CONTINUE",
        #     # "song_question": True,
        #     "chitchats": input_dict["chitchats"]
        # }
    elif not songs and re.search(r"my favorite song is", self.tracker.input_text):
        self.tracker.one_turn_store['verify_song'] = True
        return {
            "next": "s_favgenre",
            "artist": {},
            "chitchats": {},
            "response": self.tm_music.speak("verify_song/no_ner", {})
        }
        # # keep going to s_favsong, but without song name or artist
        # return {
        #     "next": "s_favgenre",
        #     "response": response,
        #     # "artist": artist['name'],
        #     "propose_continue": "CONTINUE",
        #     # "song_question": True,
        #     "chitchats": input_dict["chitchats"]
        # }

    if regex_match.is_asking_your_favorite(self.tracker):
        return {
            "next": "s_favmusician",
            "response": " ".join([
                self.tm_music.speak('fav_genre/my_favorite/ack', {}),
                self.tm_music.speak('fav_genre/my_favorite/question', {})
            ]),
            "first_time": False,
            "artist": input_dict["artist"],
            "chitchats": input_dict["chitchats"]
        }

    if not genres:
        self.logger.debug(f"entering no detected genre entity")
        if self.tracker.returnnlp.answer_positivity is Positivity.neg and len(self.tracker.input_text.split(' ')) < 3:
            self.logger.debug(f"answer_positivity == neg. Decline ack and going to s_favmusician")
            return {
                "next": "s_favmusician",
                "response": " ".join([
                    self.tm_music.speak('fav_genre/decline/ack', {}),
                    self.tm_music.speak('fav_genre/decline/question', {})
                ]),
                "first_time": False,
                "artist": input_dict["artist"],
                "chitchats": input_dict["chitchats"]
            }
        else:
            return {
                "next": "verify_genre",
                "response": self.tm_music.speak('verify_genre', {}),
                "first_time": False,
                "artist": input_dict["artist"],
                "chitchats": input_dict["chitchats"]
            }

    self.logger.debug(f"returning default return")
    return {
        "next": "s_favmusician",
        "response": self.tm_music.speak('artist_question/general', {}),
        "first_time": False,
        "artist": input_dict["artist"],
        "chitchats": input_dict["chitchats"]
    }
