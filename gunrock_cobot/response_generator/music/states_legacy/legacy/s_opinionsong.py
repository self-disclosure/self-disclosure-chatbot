import re
from typing import TYPE_CHECKING

from ...utils import entity, regex_match
from ...utils.legacy import detect_intent

if TYPE_CHECKING:
    from ...music_automaton import MusicAutomaton


def s_opinionsong(self: 'MusicAutomaton', input_dict):
    """
    song
    prev: have you heard of <song>
    curr: That is great. I think it is a good song... what's your favorite song of billie holiday?
    """

    self.logger.debug(f"[MUSIC] s_opinionsong: entering")

    # text = input_dict["text"]
    # concept = input_dict["concept"]
    # if input_dict["coreference"] is not None:
    #     text = input_dict["coreference"]["text"]
    intent = detect_intent(self.tracker.input_text)
    # ner = input_dict["nlu"]["ner"]
    # lexical = input_dict["nlu"]["lexical"]
    # sentiment = input_dict["nlu"]["sentiment"]
    artist_input = input_dict["artist"]
    if isinstance(artist_input, dict):
        artist_input = artist_input["name"]
    # knowledge = input_dict["knowledge"]
    # dialog_act = input_dict["dialog_act"]
    # no_match = False
    # play_match = play_music_match(input_dict["text"])
    # if play_match:
    #     artist = detect_artist(text, ner, knowledge)
    #     return self.play_music_response(artist, "s_chitchat",
    #                                     {
    #                                         "propose_continue": "CONTINUE",
    #                                         "chitchats": input_dict["chitchats"]
    #                                     }, True)
    # if dialog_act is not None:
    #     if is_open_question(dialog_act):
    #         if self.tracker.central_element.backstory.confidence >= BACKSTORY_THRESHOLD:
    #             return{
    #                 "next": "s_chitchat",
    #                 "response": self.tracker.central_element.backstory.text,
    #                 "artist": artist,
    #                 "propose_continue": "CONTINUE",
    #                 "chitchats": input_dict["chitchats"]
    #             }
    #         response = EVI_bot(text)
    #         if response is not None:
    #             return{
    #                 "next": "s_chitchat",
    #                 "response": response,
    #                 "artist": artist,
    #                 "propose_continue": "CONTINUE",
    #                 "chitchats": input_dict["chitchats"]
    #             }
    #         else:
    #             no_match = True

    artist = next(iter(entity.ArtistItem.detect(self.tracker)), None)

    if artist and artist.noun != artist_input.replace('.', ' '):
        return self.s_favmusician(input_dict)

    # sim_artist = random.choice(json.loads(artist['sim_artists'])['list'])
    if re.search(r"don't like|not my favorite", self.tracker.input_text):
        return self.s_chitchat(input_dict)

    if regex_match.is_other_answer(self.tracker):
        response = self.tm_music.speak('opinionsongs/neg', {'artist': artist_input.replace(".", " ")})
        return {
            "next": "s_favsong",
            "response": response,
            "artist": artist['name'],
            "propose_continue": "CONTINUE",
            "song_question": True,
            "chitchats": input_dict["chitchats"]
        }
    # if no_match:
    #     return {
    #         "response": input_dict['no_match'] + self.tm_music.speak(
    #             'fav_song/ask_song_of_artist', {'artist': artist_input.replace(".", " ")}),
    #         "next": "s_favsong",
    #         "artist": artist['name'],
    #         "propose_continue": "CONTINUE",
    #         "song_question": True,
    #         "chitchats": input_dict["chitchats"]
    #     }
    else:
        if intent == "answer_no" or 'ans_neg' in self.tracker.returnnlp.lexical_intent:
            response = self.tm_music.speak('opinionsongs/neg', {'artist': artist_input.replace(".", " ")})
        else:
            response = self.tm_music.speak('opinionsongs/great', {'artist': artist_input.replace(".", " ")})

        # "what's your favorite song by <artist>?"
        # here we save the ArtistItem for s_favsong
        self.tracker.one_turn_store['bot_said_artist'] = artist
        return {
            "next": "s_favsong",
            "response": response,
            "artist": artist_input,
            "propose_continue": "CONTINUE",
            "song_question": True,
            "chitchats": input_dict["chitchats"]
        }
