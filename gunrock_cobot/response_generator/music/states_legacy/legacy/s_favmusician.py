import random
import re
from typing import TYPE_CHECKING

# from utils import get_working_environment
from nlu.constants import TopicModule

from ...constants import (
    ignore_entity,
    genre_artist_dict,
    profanity_filter
)
from ...utils import entity, regex_match
from ...utils.legacy import (
    asr_check, check_special_artist,
    isEnglish, generate_artist_response
)

if TYPE_CHECKING:
    from ...music_automaton import MusicAutomaton


def s_favmusician(self: 'MusicAutomaton', input_dict):
    """
    state (don't know uses yet)
    """
    self.tracker.volatile_store['entering_s_favmusician'] = 1

    text = input_dict["text"]
    # concept = input_dict["concept"]
    # ner = input_dict["nlu"]["ner"]
    # intent = detect_intent(text)
    lexical = input_dict["nlu"]["lexical"]
    # topic_keyword = input_dict["topic_keyword"]
    # knowledge = input_dict["knowledge"]
    noun_phrase = input_dict["local_noun_phrase"]
    # dialog_act = input_dict["dialog_act"]
    user_id = input_dict["user_id"]
    # sentiment = input_dict["nlu"]["sentiment"]
    # alternate = False
    # reddit_artist = False
    given_artist = None
    artist_exists = False
    artists = entity.ArtistItem.detect(self.tracker)
    artist = next(iter(artists), None)
    artist_dict = {}
    album = ''
    asr_correction = input_dict["asr_correction"]
    if asr_correction:
        text = asr_check(text, noun_phrase, asr_correction)
    text = check_special_artist(text)

    # save artist into storage
    self.tracker.persistent_store['s_favmusician_artist'] = artist

    if regex_match.is_asking_for_recommendation(self.tracker):
        return self.recommend_song(input_dict, 's_init')

    if regex_match.is_play_music(self.tracker):
        return self.play_music_response(artist.noun if artist else None, "s_init",
                                        {"first_time": False})

    genres = entity.GenreItem.detect(self.tracker)
    if (genres and self.tracker.central_element.sentiment != 'neg' and
            not re.search(r'panic at the disco', self.tracker.input_text)):
        genre: entity.GenreItem = next(iter(genres), None)
        # if (self.redis_helper.is_known_genre(genre.replace(" music", ""))) and genre != input_dict['genre']:
        if genre.noun != input_dict['genre']:
            response = self.tm_music.speak("artist_question/follow_genre_question/no_pop", {'genre': genre.noun})
            if not self.redis_helper.is_returning_user_in_music_module(user_id):
                if 'pop' in genre.noun.lower():
                    response = self.tm_music.speak("artist_question/follow_genre_question/pop", {})
            return {
                "next": "s_favmusician",
                "response": response,
                "first_time": False,
                "chitchats": input_dict["chitchats"],
                "artist": input_dict["artist"],
                "genre": genre.noun,
            }
    favorite_text = None
    if input_dict['genre']:
        if input_dict['genre'] in genre_artist_dict:
            favorite_text = self.tm_music.speak('artist_fav', {'artist': genre_artist_dict[input_dict['genre']]})

    # retrieve artist from db or knowledge
    # db_lookup = query_db(text)
    # self.logger.info(
    #     '[MusicAutomaton]: Querying db with: {} with result {}'.format(text, db_lookup))
    # if db_lookup and len(db_lookup) > 0:
    #     artist = text
    # else:
    # artist = self.artist_exists(text, ner, knowledge, concept, True)

    # Here be the main dialog descision parts

    self.logger.debug(f"[MUSIC] favmusician begin decision. artist: {artist}")

    self.logger.debug(f"handle question")

    def handle_topic_specific_question():
        # self.logger.debug(f"is unanswerable? {regex_match.is_asking_your_favorite(self.tracker)}")
        if regex_match.is_asking_your_favorite(self.tracker):
            # ignore it if it's asking 'what's yours', handled below
            return None

    question_response = self.handle_question(
        is_unanswerable_question=None,
        handle_topic_specific_question=handle_topic_specific_question,
        handle_general_question=None
    )
    if (
        question_response and question_response.bot_propose_module and
        question_response.bot_propose_module != TopicModule.MUSIC
    ):
        self.logger.debug(f"unclear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                self.tm_transition.speak(f"propose_topic_short/{question_response.bot_propose_module.value}", {})
            ]),
            'propose_continue': 'UNCLEAR',
            'propose_topic': question_response.bot_propose_module.value
        }
    elif question_response:
        self.logger.debug(f"clear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                self.tm_music.speak('no_match/continue', {}),
                self.tm_music.speak('artist_question/general', {}),
            ]),
            "next": "s_chitchat",
            'propose_continue': 'CONTINUE',
        }
    # if not artist and mutils.is_open_question(self.tracker):
    #     if self.tracker.central_element.backstory.confidence >= BACKSTORY_THRESHOLD:
    #         self.logger.debug(f"[MUSIC] favmusician using backstory")
    #         return{
    #             "next": "s_init",
    #             "response": self.tracker.central_element.backstory.text,
    #             "first_time": False,
    #             "chitchats": input_dict["chitchats"],
    #             "propose_continue": "CONTINUE"
    #         }
    #     response = evi.query(self.tracker.input_text)
    #     if response is not None:
    #         self.logger.debug(f"[MUSIC] favmusician using EVI")
    #         return {
    #             "response": response,
    #             "next": "s_init",
    #             "propose_continue": "CONTINUE",
    #             "first_time": False,
    #             "chitchats": input_dict["chitchats"]
    #         }
    #     else:
    #         response = input_dict['no_match']
    #         if favorite_text:
    #             response += favorite_text
    #         self.logger.debug(f"[MUSIC] favmusician no match")
    #         return{
    #             "response": response,
    #             "next": "s_chitchat",
    #             "artist": input_dict["artist"],
    #             "chitchats": input_dict["chitchats"],
    #             "propose_continue": "CONTINUE"
    #         }

    if regex_match.is_rapport(self.tracker):
        self.logger.debug(f"[MUSIC] favmusician using rapport")
        input_dict['artist'] = 'taylor swift'
        return self.s_templates(input_dict, 'ts')

    if self.tracker.central_element.sentiment != 'neg':
        _regex = re.search(
            (r"taylor swift|michael jackson|imagine dragons|drake|maroon|adam levine"),
            self.tracker.input_text, re.I)
        self.logger.debug(f"[MUSIC] favmusician going thru non neg sentiment: {self.tracker.input_text} "
                          f"regex: {_regex}")
        if _regex:
            if re.search(r"don't like", text):
                return self.s_chitchat(input_dict)
            elif re.search(r'taylor swift', text):
                input_dict['artist'], keyword = 'taylor swift', 'ts'
            elif re.search(r'michael jackson', text):
                input_dict['artist'], keyword = 'michael jackson', 'mj'
            elif re.search(r'drake', text):
                input_dict['artist'], keyword = 'drake', 'dg'
            elif re.search(r'imagine dragons', text):
                input_dict['artist'], keyword = 'imagine dragons', 'id'
            elif re.search(r'(maroon|adam levine)', text):
                input_dict['artist'], keyword = 'maroon 5', 'm5'
            self.logger.debug(f"[MUSIC] favmusician jumping to s_templates: input_dict={input_dict}, keyword={keyword}")
            ret = self.s_templates(input_dict, keyword)
            self.logger.debug(f"[MUSIC] favmusician s_templates return: {ret}")
            return ret

    if regex_match.is_other_answer(self.tracker):
        self.logger.debug(f"[MUSIC] favmusician is other answer (unknown match)")
        response = self.tm_music.speak('artist_fav_no_choice', {})
        if favorite_text:
            response += favorite_text
        return{
            "next": "s_chitchat",
            "response": response,
            "artist": None,
            "chitchats": input_dict["chitchats"]
        }

    if not artist:
        self.logger.debug(f"[MUSIC] favmusician no detected artist")
        # if len(noun_phrase) > 0:
        #     artist = noun_phrase[0]
        #     artist = artist.lower()
        #     given_artist = artist
        # else:
        # response = None
        if (
            re.search(
                r'several|many|every artist|multiple| a lot of favorites|like them all',
                self.tracker.input_text) or
            self.tracker.central_element.sentiment == 'neg'
        ):
            self.logger.debug(f"[MUSIC] favmusician no detected_artist, but 'like them all'")
            response = self.tm_music.speak('artist_fav_no_choice', {})
            if favorite_text:
                response += favorite_text
            # input_dict.update({
            #     "next": "s_chitchat",
            #     "response": response,
            #     "artist": None,
            #     "chitchats": input_dict["chitchats"]
            # })
            chain_with_ack = False
        elif regex_match.is_asking_your_favorite(self.tracker):
            response = self.tm_music.speak(
                'artist_fav', {'artist': genre_artist_dict[input_dict.get('genre') or'pop']}
            )
            chain_with_ack = True
        else:
            response = self.tm_music.speak('transition/artist_blanket_ack/to_chitchat', {})
            chain_with_ack = False
        self.logger.debug(f"[MUSIC] favmusician no detected artist, response: {response}")
        return self.s_chitchat(input_dict, chain_response=response, chain_with_acknowledgement=chain_with_ack)
        # if re.search(r'jay-z', artist):
        #     artist = 'jay z'
        # db_lookup = query_db(text)
        # self.logger.info(
        #     '[MusicAutomaton]: Querying db with: {} with result {}'.format(text, db_lookup))

        # # might be redundant??
        # if db_lookup is not None:
        #     db_lookup = query_db(
        #         artist)  # trying to find artist from entire utterance
        #     self.logger.info(
        #         '[MusicAutomaton]: Querying db with: {} with result {}'.format(artist, db_lookup))
        # if db_lookup is not None:
        #     if len(db_lookup) > 0:
        #         if 'name' in db_lookup[0]:
        #             artist_dict['name'] = db_lookup[0]['name']
        #         if 'songs' in db_lookup[0]:
        #             artist_dict['songs'] = [random.choice(
        #                                     db_lookup[0]['songs']),
        #                                     random.choice(db_lookup[0]['songs'])]
        #         if 'albums' in db_lookup[0]:
        #             artist_dict['albums'] = [random.choice(
        #                                      db_lookup[0]['albums']),
        #                                      random.choice(db_lookup[0]['albums'])]
        #         if 'trivias' in db_lookup[0]:
        #             artist_dict['trivias'] = [random.choice(
        #                                       db_lookup[0]['trivias']),
        #                                       random.choice(db_lookup[0]['trivias'])]
        #     else:
        #         artist = ''
        #         artist_exists = False
        #         given_artist = None
        # else:
        #     self.logger.info('[MusicAutomaton]: artist used:' + artist)
        #     artist_exists = False
    else:
        self.logger.debug(f"[MUSIC] detected artist: {artist}")
        given_artist = artist.noun
        # if re.search(r'cardi', artist.noun.lower()):
        #     artist = 'cardi b'
        # if re.search(r'b. t. s.', artist.noun.lower()):
        #     artist = 'bts'
        # artist = artist.lower()
        # db_lookup = query_db(artist.noun)
        # artist always exists if artists are detected
        artist_exists = True

        if artist.artist_details(self.tracker):
            self.logger.debug(
                f"[MUSIC] favmusician detected artist db lookup: {artist.noun}, {artist.artist_details(self.tracker)}")
            artist_dict = artist.artist_details(self.tracker).artist_dict()
            self.logger.debug(f"[MusicAutomaton]: artist used: {artist}")
            # artist_exists = True
        # else:
        #     artist = None
        #     artist_exists = False
        else:
            self.logger.debug(
                f"[MUSIC] favmusician db lookup no result: {artist.noun}")
            artist_dict = {
                'name': artist.noun,
            }

    if 'trivias' in artist_dict:
        self.logger.debug(f"[MUSIC] favmusician artist_dict has 'trivias': {artist_dict}")
        random_artist_facts = [a for a in artist_dict['trivias']
                               if not re.search(profanity_filter, a)]
    else:
        random_artist_facts = []
    if len(random_artist_facts) == 0:
        random_fact = '. '
    else:
        random_fact = random.choice(random_artist_facts) + ". "
    if 'songs' in artist_dict:
        album = random.choice(artist_dict['songs'])
        if album:
            if not isEnglish(album):
                album = ''
        else:
            album = ''
    else:
        # alternate = True
        pass

    if artist and re.search(ignore_entity, artist.noun):
        self.logger.debug(f"[MUSIC] favmusician in ignore_entity: {artist}")
        given_artist = ''
        artist = None
        artist_exists = False
    if re.search(r'several|many|every artist|multiple| a lot of favorites', text) or 'ans_neg' in lexical:
        self.logger.debug(f"[MUSIC] favmusician lots of favorites: {text}")
        response = self.tm_music.speak('artist_fav_no_choice', {})
        if favorite_text:
            response += favorite_text
        return {
            "next": "s_chitchat",
            "response": response,
            "artist": None,
            "chitchats": input_dict["chitchats"]
        }

    # if artist == '':
    #     self.logger.debug(f"[MUSIC] favmusician artist empty: {artist}")
    #     return self.s_chitchat(input_dict, False)
    if artist_exists:

        # if random_fact != '. ' and get_working_environment() != 'local':
        if random_fact != '. ':
            # temp disable for https://trello.com/c/fbQ3tRzp
            self.logger.debug(f"[MUSIC] favmusician propose random facts: {random_fact}")
            return{
                "next": "s_opinionfact",
                "response": generate_artist_response(artist.noun, random_fact, self.tm_music),
                "artist": artist_dict,
                "chitchats": input_dict["chitchats"]
            }
        else:
            if len(album) != 0:
                self.logger.debug(f"[MUSIC] favmusician speak artist with album: {artist}, {album}")
                return {
                    "next": "s_opinionsong",
                    "response": self.tm_music.speak(
                        'artist_question/follow_album', {'artist': artist.noun.replace(".", " "), 'album': album}),
                    "artist": artist_dict,
                    "chitchats": input_dict["chitchats"]
                }
            else:
                self.logger.debug(f"[MUSIC] favmusician speak artist without album: {artist}, {album}, {artist_dict}")
                """
                what's your favorite thing about <artist>
                """
                return {
                    "next": "s_artist_question",
                    "response": self.tm_music.speak('artist_question/why_artist',
                                                    {'artist': artist.noun.replace(".", " ")}),
                    "artist": artist_dict,
                    's_favmucian_artist': True,
                    "chitchats": input_dict["chitchats"]
                }

    else:
        self.logger.debug(f"[MUSIC] favmusician speak artist not exist: {artist_exists}, {artist}")
        return{
            "next": "s_artist_question",
            "response": self.tm_music.speak('artist_question/i_dont_know', {'artist': given_artist}),
            "artist": given_artist,
            "chitchats": input_dict["chitchats"]
        }
