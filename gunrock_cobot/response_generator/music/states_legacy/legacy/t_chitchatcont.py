import re
from typing import TYPE_CHECKING

from nlu.constants import Positivity, TopicModule

from ...utils import entity, regex_match

if TYPE_CHECKING:
    from ...music_automaton import MusicAutomaton


def t_chitchatcont(self: 'MusicAutomaton', input_dict):
    """
    answer user's response
    """

    chitchats = input_dict["chitchats"]["remaining"]
    chitchat_id = input_dict["chitchats"]["current"]
    chitchat = self.read_chitchat_content(chitchat_id)
    # text = input_dict["text"]
    # concept = input_dict["concept"]
    # if input_dict["coreference"] is not None:
    #     text = input_dict["coreference"]["text"]
    # ner = input_dict["nlu"]["ner"]
    # intent = detect_intent(text)
    # knowledge = input_dict["knowledge"]
    # dialog_act = input_dict["dialog_act"]
    count = input_dict["chitchats"]["count"]
    # noun_phrase = input_dict["local_noun_phrase"]
    # topic_keyword = input_dict["topic_keyword"]
    # lexical = input_dict["nlu"]["lexical"]
    # user_id = input_dict["user_id"]
    # no_match = False

    self.logger.debug(f"entering")

    if regex_match.is_asking_for_recommendation(self.tracker):
        self.logger.debug(f"rec, going to recommend_song")
        return self.recommend_song(input_dict, 's_chitchat')

    if (
        len(input_dict["chitchats"]["remaining"]) == 0 or
        len(chitchats) == 0
    ):
        self.logger.debug(f"ran out of chitchats. going to change_topic")
        # TODO: check if need return
        return self.change_topic(input_dict)

    # play_match = play_music_match(input_dict["text"])
    # if play_match:
    #     artist = detect_artist(text, ner, knowledge)
    #     return self.play_music_response(artist, "t_chitchatcont",
    #                                     {"artist": input_dict["artist"],
    #                                         "chitchats": {
    #                                         "remaining": chitchats,
    #                                         "current": chitchat_id,
    #                                         "count": count
    #                                     },
    #                                         "propose_continue": "CONTINUE",
    #                                         "context": input_dict["currentstate"]
    #                                     })

    if re.search(r'talk about|my favorite', self.tracker.input_text):
        self.logger.debug(f"talk about|my favorite entered")
        artist = next(iter(entity.ArtistItem.detect(self.tracker)), None)
        # entity = self.artist_exists(text, ner, knowledge)
        if artist and artist.noun != input_dict["artist"]:
            self.logger.debug(f"has artist, artist={artist}, going to favmusician")
            return self.s_favmusician(input_dict)

        genre = next(iter(entity.GenreItem.detect(self.tracker)), None)
        # entity = detect_genre(topic_keyword, noun_phrase, concept, text)
        if genre:
            return {
                "next": "s_favmusician",
                "response": self.tm_music.speak(
                    'artist_question/follow_genre_question/no_pop', {'genre': genre}),
                "first_time": False,
                "chitchats": input_dict["chitchats"],
                "artist": input_dict["artist"]
            }

    self.logger.debug(f"handle question")
    question_response = self.handle_question(
        is_unanswerable_question=None,
        handle_topic_specific_question=None,
        handle_general_question=None
    )
    if (
        question_response and question_response.bot_propose_module and
        question_response.bot_propose_module != TopicModule.MUSIC
    ):
        self.logger.debug(f"unclear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                self.tm_transition.speak(f"propose_topic_short/{question_response.bot_propose_module.value}", {})
            ]),
            'propose_continue': 'UNCLEAR',
            'propose_topic': question_response.bot_propose_module.value
        }
    elif question_response:
        self.logger.debug(f"clear question_response: {question_response}")
        response = " ".join([
            question_response.response,
            # TODO: check joiner
            self.tm_music.speak('no_match/continue', {}),
            chitchat['q'],
        ])
        return {
            "response": response,
            "next": "s_chitchatcont",
            "artist": input_dict["artist"],
            "chitchats": {
                "remaining": chitchats,
                "current": chitchat_id,
                "count": count
            },
            "propose_continue": "CONTINUE",
            "context": input_dict["currentstate"]
        }
        # return {
        #     'response': " ".join([
        #         question_response.response,
        #         self.tm_music.speak('no_match/continue', {}),
        #         self.tm_music.speak('artist_question/general', {}),
        #     ]),
        #     "next": "s_chitchat",
        #     'propose_continue': 'CONTINUE',
        # }

    # if dialog_act is not None:
    #     if is_open_question(dialog_act):
    #         if self.tracker.central_element.backstory.confidence >= BACKSTORY_THRESHOLD:
    #             return{
    #                 "response": self.tracker.central_element.backstory.text,
    #                 "next": "t_chitchatcont",
    #                 "artist": input_dict["artist"],
    #                 "chitchats": {
    #                     "remaining": chitchats,
    #                     "current": chitchat_id,
    #                     "count": count
    #                 },
    #                 "propose_continue": "CONTINUE",
    #                 "context": input_dict["currentstate"]
    #             }
    #         response = EVI_bot(text)
    #         if response is not None:
    #             return {
    #                 "response": response,
    #                 "next": "t_chitchatcont",
    #                 "artist": input_dict["artist"],
    #                 "chitchats": {
    #                     "remaining": chitchats,
    #                     "current": chitchat_id,
    #                     "count": count
    #                 },
    #                 "propose_continue": "CONTINUE",
    #                 "context": input_dict["currentstate"]
    #             }
    #         else:
    #             no_match = True

    if count == 3 and input_dict["artist"] is not None:
        self.logger.debug(
            f"if theres been {count} chitchat and an artist {input_dict['artist']} exists, go to concert_init")
        return self.concert_init(input_dict)
    # entity = None

    if 'ans_unknowwn' in self.tracker.returnnlp.lexical_intent:
        acknowledgement = self.tm_music.speak('unknown_ack', {})
    elif re.search(r"cannot remember", self.tracker.input_text):
        acknowledgement = "that's okay. "
    elif regex_match.is_rapport(self.tracker):
        acknowledgement = 'That is great! We are alike. '
    elif self.tracker.returnnlp.answer_positivity is Positivity.neg:
        acknowledgement = self.tm_music.speak('chitchats/ack', {})
    elif len(self.tracker.input_text.split(' ')) > 5:
        acknowledgement = self.tm_music.speak('long_sentences_ack', {})
    elif regex_match.is_other_answer(self.tracker):
        acknowledgement = self.tm_music.speak('unknown_ack', {})
    else:
        acknowledgement = ''

    # acknowledgement = ''
    # if (unknown_match(text)):
    #     acknowledgement = self.tm_music.speak('unknown_ack', {})
    # if len(text.split(" ")) > 5:
    #     acknowledgement = self.tm_music.speak('long_sentences_ack', {})
    # if is_negative(dialog_act, lexical, intent):
    #     acknowledgement = self.tm_music.speak('chitchats/ack', {})
    # if re.search(rapport_match, text):
    #     acknowledgement = 'That is great! We are alike. '
    # if re.search(r'cannot remember', text):
    #     acknowledgement = "that's okay. "
    # if 'ans_unknown' in lexical:
    #     acknowledgement = self.tm_music.speak('unknown_ack', {})

    # if len(chitchats) == 0:
    #     return self.change_topic(input_dict)
    # else:
        # if no_match:
        #     return {
        #         "response": input_dict['no_match'] + chitchat["q"],
        #         "next": "s_chitchatcont",
        #         "artist": input_dict["artist"],
        #         "chitchats": {
        #             "remaining": chitchats,
        #             "current": chitchat_id,
        #             "count": count
        #         },
        #         "propose_continue": "CONTINUE",
        #         "context": input_dict["currentstate"]
        #     }
        # song_question = False

    song_question = chitchat['keyword'] in {'emotional', 'karaoke'}
    # if re.search(r'emotional|karaoke', chitchat["keyword"]):
    #     song_question = True

    return {
        "response": acknowledgement + chitchat["q"],
        "next": "s_chitchatcont",
        "artist": input_dict["artist"],
        "chitchats": {
            "remaining": chitchats,
            "current": chitchat_id,
            "count": count
        },
        "propose_continue": "CONTINUE",
        "context": input_dict["currentstate"],
        "song_question": song_question
    }
