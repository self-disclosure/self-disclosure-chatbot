from typing import TYPE_CHECKING

from nlu.constants import Positivity

# from ...utils import entity

if TYPE_CHECKING:
    from ...music_automaton import MusicAutomaton


def verify_musician(self: 'MusicAutomaton', input_dict):
    """
    verify if user wants to talk about the musician
    play_music_response also goes here if there's an artist
    """

    self.logger.debug(f"entering")

    self.tracker.one_turn_store['verify_musician_trigger'] = (
        self.tracker.last_utt_store.get('verify_musician_trigger', 0) + 1
    )
    artist_noun: str = self.tracker.last_utt_store.get('play_music_reponse')

    if self.tracker.returnnlp.answer_positivity is Positivity.neg:
        self.logger.debug(f"neg answer, going to change_topic")
        return self.change_topic(input_dict)

    elif self.tracker.returnnlp.answer_positivity is Positivity.pos and artist_noun:
        self.logger.debug(f"pos answer with artist={artist_noun}, going to s_artist_question")
        return self.s_artist_question({**input_dict, 'artist': artist_noun})

    else:
        self.logger.debug(f"default, going to s_chitchat")
        return self.s_chitchat(input_dict)
