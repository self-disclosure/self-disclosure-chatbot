import random
from typing import TYPE_CHECKING

from response_generator.common import evi

from ...utils import entity

if TYPE_CHECKING:
    from ...music_automaton import MusicAutomaton


def recommend_song(self: 'MusicAutomaton', input_dict, old_state=None):
    # text = input_dict['text']
    # ner = input_dict['nlu']['ner']
    # knowledge = input_dict["knowledge"]
    # concept = input_dict["concept"]
    artist: entity.ArtistItem = next(iter(entity.ArtistItem.detect(self.tracker)), None)
    # db_lookup = query_db(text)

    self.logger.debug(f"entering, artist={artist}")

    artist_details = artist.artist_details(self.tracker)

    if artist_details and artist_details.songs:
        song = random.choice(artist_details.songs)
        return {
            "response": self.tm_music.speak('recommend_song/song', {'song': song}),
            "next": old_state,
            "artist": artist.noun,
            "chitchats": input_dict["chitchats"],
            "first_time": False,
            "propose_continue": "CONTINUE"
        }

    elif artist:
        response = evi.query(f"top songs of {artist.noun}")

    else:
        response = evi.query("top ten songs")

    if response:
        return {
            "response": self.tm_music.speak('recommend_song/response', {'response': response}),
            "next": old_state,
            "propose_continue": "CONTINUE",
            "first_time": False,
        }
    else:
        return {
            "response": self.tm_music.speak('recommend_song/unknow_artist', {}),
            "next": old_state,
            "propose_continue": "CONTINUE",
            "first_time": False,
        }

    # if db_lookup is None or len(db_lookup) == 0:
    #     if artist:
    #         db_lookup = query_db(artist.noun.lower())  # trying to find artist from entire utterance
    #         self.logger.info(
    #             '[MusicAutomaton]: Querying db with: {} with result {}'.format(artist.noun.lower(), db_lookup))
    # if db_lookup is not None:
    #     if len(db_lookup) > 0:
    #         return {
    #             "response": self.tm_music.speak('recommend_song/song',
    #                                             {'song': random.choice(db_lookup[0]['songs'])}),
    #             "next": old_state,
    #             "artist": artist.noun,
    #             "chitchats": input_dict["chitchats"],
    #             "first_time": False,
    #             "propose_continue": "CONTINUE"
    #         }
    # if not artist:
    #     response = evi.query("top ten songs")
    # else:
    #     response = evi.query(f"top songs of {artist.noun}")
    # if response is not None:
    #     return {
    #         "response": self.tm_music.speak('recommend_song/response', {'response': response}),
    #         "next": old_state,
    #         "propose_continue": "CONTINUE",
    #         "first_time": False,
    #     }
    # else:
    #     return {
    #         "response": self.tm_music.speak('recommend_song/unknow_artist', {}),
    #         "next": old_state,
    #         "propose_continue": "CONTINUE",
    #         "first_time": False,
    #     }
