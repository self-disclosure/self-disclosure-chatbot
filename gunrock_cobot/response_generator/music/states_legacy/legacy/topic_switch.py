
from typing import TYPE_CHECKING

from nlu.constants import Positivity

if TYPE_CHECKING:
    from ...music_automaton import MusicAutomaton


def topic_switch(self: 'MusicAutomaton', input_dict):
    """
    similar to change topic
    """

    # text = input_dict["text"]
    # concept = input_dict["concept"]
    # if input_dict["coreference"] is not None:
    #     text = input_dict["coreference"]["text"]
    # intent = detect_intent(text)
    # dialog_act = input_dict["dialog_act"]
    # lexical = input_dict["nlu"]["lexical"]
    # negative = is_negative(dialog_act, lexical, intent)

    self.logger.debug(f"entering")

    if self.tracker.returnnlp.answer_positivity is Positivity.neg:
        self.logger.debug(f"neg answer, going to s_chitchat")
        return {
            "response": self.tm_music.speak('chitchats/ack', {}),
            "next": "s_chitchat",
            "propose_continue": "STOP",
            "chitchats": input_dict["chitchats"],
            "context": input_dict["currentstate"],
            "propose_topic": input_dict["propose_topic"],
            "exit": True
        }
    else:
        self.logger.debug(f"default, going to s_chitchat")
        return self.s_chitchat(input_dict)
