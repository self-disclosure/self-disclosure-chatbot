import re
from typing import TYPE_CHECKING

from nlu.constants import TopicModule

from ...utils import entity, regex_match, dynamodb

if TYPE_CHECKING:
    from ...music_automaton import MusicAutomaton


def s_favsong(self: 'MusicAutomaton', input_dict):
    """
    opinionsong followup
    """

    self.logger.debug(f"[MUSIC] s_favsong: entering")

    # text = input_dict["text"]
    # concept = input_dict["concept"]
    # ner = input_dict["nlu"]["ner"]
    # knowledge = input_dict["knowledge"]
    # lexical = input_dict["nlu"]["lexical"]
    # if input_dict["coreference"] is not None:
    #     text = input_dict["coreference"]["text"]
    # play_match = play_music_match(input_dict["text"])
    # dialog_act = input_dict["dialog_act"]

    # if play_match:
    #     artist = detect_artist(text, ner, knowledge)
    #     return self.play_music_response(artist, "s_chitchat",
    #                                     {"chitchats": input_dict["chitchats"]})

    if regex_match.is_other_answer(self.tracker) or 'ans_unknown' in self.tracker.returnnlp.lexical_intent:
        self.logger.debug(f"[MUSIC] s_favsong: is other answer")
        return{
            "next": "s_chitchat",
            "response": self.tm_music.speak('unknown_ack', {}),
            "artist": input_dict["artist"],
            "chitchats": input_dict["chitchats"]
        }

    if regex_match.is_rapport(self.tracker):
        self.logger.debug(f"[MUSIC] s_favsong: is rapport")
        return{
            "next": "s_chitchat",
            "response": self.tm_music.speak('fav_song/rap', {}),
            "artist": input_dict["artist"],
            "chitchats": input_dict["chitchats"]
        }

    response = self.tm_music.speak('fav_song/general', {})

    artist: entity.ArtistItem = self.tracker.last_utt_store.get('bot_said_artist')

    # if artist and artist.artist_details(self.tracker).songs:
    #     artist_songs = artist.artist_details(self.tracker).songs
    # else:
    #     artist_songs = get_all_songs(input_dict["artist"].lower())
    artist_songs = (
        artist.songs_from_db(self.tracker) if artist else
        dynamodb.get_songs_by_artist(input_dict['artist'].lower())
    )

    self.logger.debug(f"[MUSIC] s_favsong: get_all_song: {artist_songs}")

    for song in artist_songs:
        match = re.search(re.escape(song), self.tracker.input_text, re.I)
        self.logger.debug(f"[MUSIC] s_favsong: artist_songs: {artist_songs}, {match}")
        if match:
            response = self.tm_music.speak('fav_song/with_song', {'song': match.group()})
            # TODO: jump?
            return {
                "next": "s_chitchat",
                "response": response,
                "artist": input_dict["artist"],
                "chitchats": input_dict["chitchats"]
            }

    self.logger.debug(f"handle question")
    question_response = self.handle_question(
        is_unanswerable_question=None,
        handle_topic_specific_question=None,
        handle_general_question=None
    )
    if (
        question_response and question_response.bot_propose_module and
        question_response.bot_propose_module != TopicModule.MUSIC
    ):
        self.logger.debug(f"unclear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                self.tm_transition.speak(f"propose_topic_short/{question_response.bot_propose_module.value}", {})
            ]),
            'propose_continue': 'UNCLEAR',
            'propose_topic': question_response.bot_propose_module.value
        }
    elif question_response:
        self.logger.debug(f"clear question_response: {question_response}")
        response = " ".join([
            question_response.response,
        ])
        return self.s_chitchat(input_dict, chain_response=response)
    # if dialog_act is not None:
    #     if is_open_question(dialog_act):
    #         if self.tracker.central_element.backstory.confidence >= BACKSTORY_THRESHOLD:
    #             return {
    #                 "next": 's_chitchat',
    #                 "response": self.tracker.central_element.backstory.text,
    #                 "artist": input_dict["artist"],
    #                 "chitchats": input_dict["chitchats"],
    #                 "propose_continue": "CONTINUE"
    #             }
    #         response = EVI_bot(text)
    #         if response is not None:
    #             return {
    #                 "response": response,
    #                 "next": "s_chitchat",
    #                 "propose_continue": "CONTINUE",
    #                 "first_time": False,
    #                 "artist": input_dict["artist"],
    #                 "chitchats": input_dict["chitchats"]
    #             }
    #         else:
    #             return self.s_chitchat(input_dict)

    if re.search(r"^no$|^yes$|^yeah$", self.tracker.input_text):
        return self.s_chitchat(input_dict)

    if re.search(r"all of them|all of her songs|all of his songs|all their songs|all her songs|all his songs",
                 self.tracker.input_text):
        response = "<say-as interpret-as=\"interjection\">wow</say-as>! you are a big fan of " + input_dict["artist"]
    return {
        "next": "s_chitchat",
        "response": response,
        "artist": input_dict["artist"],
        "chitchats": input_dict["chitchats"]
    }
