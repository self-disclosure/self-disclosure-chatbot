from typing import TYPE_CHECKING

from nlu.constants import TopicModule

if TYPE_CHECKING:
    from ...music_automaton import MusicAutomaton


def change_topic(self: 'MusicAutomaton', input_dict):
    """
    switch to other module
    """

    # dialog_act = input_dict["dialog_act"]
    # text = input_dict["text"]

    self.logger.debug(f"handle question")
    question_response = self.handle_question(
        is_unanswerable_question=None,
        handle_topic_specific_question=None,
        handle_general_question=None
    )
    if (
        question_response and question_response.bot_propose_module and
        question_response.bot_propose_module != TopicModule.MUSIC
    ):
        self.logger.debug(f"unclear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                self.tm_transition.speak(f"propose_topic_short/{question_response.bot_propose_module.value}", {})
            ]),
            'propose_continue': 'UNCLEAR',
            'propose_topic': question_response.bot_propose_module.value
        }
    elif question_response:
        self.logger.debug(f"clear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                # self.tm_music.speak('no_match/continue', {}),
                # self.tm_music.speak('chitchats/ack', {}),
            ]),
            "next": "s_chitchat",
            'propose_continue': 'STOP',
            "context": input_dict["currentstate"],
            'exit': True,
        }

    # if dialog_act is not None:
    #     if is_open_question(dialog_act):
    #         if self.tracker.central_element.backstory.confidence >= BACKSTORY_THRESHOLD:
    #             return{
    #                 "next": 's_init',
    #                 "response": self.tracker.central_element.backstory.text,
    #                 "artist": input_dict["artist"],
    #                 "chitchats": input_dict["chitchats"],
    #                 "propose_continue": "STOP",
    #                 "exit": True
    #             }
    #         response = EVI_bot(text)
    #         if response is not None:
    #             return {
    #                 "response": response,
    #                 "next": "s_init",
    #                 "propose_continue": "STOP",
    #                 "first_time": False,
    #                 "artist": input_dict["artist"],
    #                 "chitchats": input_dict["chitchats"],
    #                 "exit": True
    #             }
    #         else:
    #             return{
    #                 "response": input_dict['no_match'] + self.tm_music.speak('chitchats/ack', {}),
    #                 "next": "s_init",
    #                 "artist": input_dict["artist"],
    #                 "chitchats": input_dict["chitchats"],
    #                 "propose_continue": "STOP",
    #                 "exit": True
    #             }
    else:
        return {
            "response": self.tm_music.speak('chitchats/ack', {}),
            "next": "s_init",
            "propose_continue": "STOP",
            "context": input_dict["currentstate"],
            "exit": True
        }
