from typing import TYPE_CHECKING

from nlu.constants import TopicModule

from ...utils import entity

if TYPE_CHECKING:
    from ...music_automaton import MusicAutomaton


def s_artist_song(self: 'MusicAutomaton', input_dict):
    """
    when we don't find an artist in your database
    response to "what's your favorite song by <artist>?"
    """

    # get artist from s_favmusician store
    # artist: entity.ArtistItem = self.tracker.persistent_store.get('s_favmusician_artist')
    # artist = input_dict["artist"]
    # text = input_dict["text"]
    # concept = input_dict["concept"]
    # if input_dict["coreference"] is not None:
    #     text = input_dict["coreference"]["text"]
    # ner = input_dict["nlu"]["ner"]
    # knowledge = input_dict["knowledge"]
    # noun_phrase = input_dict["local_noun_phrase"]

    self.logger.debug(f"[MUSIC] s_artist_song: entering")

    # if play_music_match(input_dict["text"]):
    #     artist = detect_artist(text, ner, knowledge)
    #     return self.play_music_response(artist, "s_artist_song",
    #                                     {"artist": input_dict["artist"],
    #                                         "chitchats": input_dict["chitchats"]})

    self.logger.debug(f"handle question")
    question_response = self.handle_question(
        is_unanswerable_question=None,
        handle_topic_specific_question=None,
        handle_general_question=None
    )
    if (
        question_response and question_response.bot_propose_module and
        question_response.bot_propose_module != TopicModule.MUSIC
    ):
        self.logger.debug(f"unclear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                self.tm_transition.speak(f"propose_topic_short/{question_response.bot_propose_module.value}", {})
            ]),
            'propose_continue': 'UNCLEAR',
            'propose_topic': question_response.bot_propose_module.value
        }
    elif question_response:
        self.logger.debug(f"clear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                self.tm_music.speak('no_match/continue', {}),
                self.tm_music.speak('artist_question/general', {}),
            ]),
            "next": "s_verifychitchat",
            'propose_continue': 'CONTINUE',
        }
    # if mutils.is_open_question(self.tracker):
    #     self.logger.debug(f"[MUSIC] s_artist_song finds open question. Asking backstory")
    #     if self.tracker.central_element.backstory.confidence >= 0.8:
    #         return{
    #             "next": 's_artist_song',
    #             "response": self.tracker.central_element.backstory.text,
    #             "artist": artist,
    #             "chitchats": input_dict["chitchats"],
    #             "propose_continue": "CONTINUE"
    #         }
    #     self.logger.debug(f"[MUSIC] s_artist_song backstory falling back to evi")
    #     response = EVI_bot(text)
    #     if response:
    #         return {
    #             "response": response,
    #             "next": "s_artist_song",
    #             "propose_continue": "CONTINUE",
    #             "first_time": False,
    #             "chitchats": input_dict["chitchats"]
    #         }
    #     else:
    #         self.logger.debug(f"[MUSIC] s_artist_song open question fall back to verify chitchat")
    #         return{
    #             "response": input_dict['no_match'],
    #             "next": "s_verifychitchat",
    #             "artist": input_dict["artist"],
    #             "chitchats": input_dict["chitchats"],
    #             "propose_continue": "UNCLEAR"
    #         }
    curr_turn_artist = next(iter(entity.ArtistItem.detect(self.tracker)), None)
    # if self.artist_exists(text, ner, knowledge, concept) is not None:
    if curr_turn_artist:
        self.logger.debug(f"[MUSIC] s_artist_song: has curr_turn_artist {curr_turn_artist}, going to s_favmusician")
        return self.s_favmusician(input_dict)

    # song detection
    # TODO: first, look up if the provided name matches something from the artist db
    songs = entity.SongItem.detect(self.tracker)
    if songs:
        self.logger.debug(f"[MUSIC] s_artist_song: has song {songs}, going to s_chitchat")
        response = self.tm_music.speak('artist_song_response', {})
        # patch to add jump to next state
        next_state = self.s_chitchat(input_dict)
        next_state['response'] = response + next_state['response']
        return next_state
    else:
        self.logger.debug(f"[MUSIC] s_artist_song: no song {songs}, going to s_chitchat")
        # TODO: acknowledge better even there's no song
        response = "Cool. "
        # patch to add jump to next state
        next_state = self.s_chitchat(input_dict)
        next_state['response'] = response + next_state['response']
        return next_state
    return{
        "next": "s_chitchat",
        "response": response,
        "artist": None,
        "chitchats": input_dict["chitchats"]
    }
