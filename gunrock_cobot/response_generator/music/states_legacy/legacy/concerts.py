from typing import TYPE_CHECKING

from nlu.constants import TopicModule

from ...utils import entity, regex_match
# from ...constants import (
#     BACKSTORY_THRESHOLD,
#     recommend_terms, rapport_match
# )
# from ...utils.legacy import (
#     play_music_match, detect_artist, is_open_question, EVI_bot, get_music_entity, unknown_match
# )
if TYPE_CHECKING:
    from ...music_automaton import MusicAutomaton


def concert_init(self: 'MusicAutomaton', input_dict):
    """
    concert 1
    """

    artist = input_dict["artist"]
    # dialog_act = input_dict["dialog_act"]
    # knowledge = input_dict["knowledge"]
    # concept = input_dict["concept"]
    # text = input_dict["text"]
    # ner = input_dict["nlu"]["ner"]
    # no_match = False

    self.logger.debug(f"entering")
    if regex_match.is_asking_for_recommendation(self.tracker):
        return self.recommend_song(input_dict, 'concert_init')

    # play_match = play_music_match(input_dict["text"])
    # if play_match:
    #     artist = detect_artist(text, ner, knowledge)
    #     return self.play_music_response(artist, "concert_init",
    #                                     {
    #                                         "artist": input_dict["artist"],
    #                                         "chitchats": input_dict["chitchats"],
    #                                         "propose_continue": "CONTINUE"
    #                                     })

    question_response = self.handle_question(
        is_unanswerable_question=None,
        handle_topic_specific_question=None,
        handle_general_question=None
    )
    if (
        question_response and question_response.bot_propose_module and
        question_response.bot_propose_module != TopicModule.MUSIC
    ):
        self.logger.debug(f"unclear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                self.tm_transition.speak(f"propose_topic_short/{question_response.bot_propose_module.value}", {})
            ]),
            'propose_continue': 'UNCLEAR',
            'propose_topic': question_response.bot_propose_module.value
        }
    elif question_response:
        self.logger.debug(f"clear question_response: {question_response}")
        if artist:
            return {
                'response': " ".join([
                    question_response.response,
                    self.tm_music.speak('no_match/continue', {}),
                    self.tm_music.speak('concert/ask_artist_live', {'artist': artist}),
                    # self.tm_music.speak('no_match/continue', {}),
                    # self.tm_music.speak('artist_question/general', {}),
                ]),
                "next": "s_best_concert",
                "artist": artist,
                "chitchats": input_dict["chitchats"],
                "propose_continue": "CONTINUE"
            }
        else:
            return {
                'response': " ".join([
                    question_response.response,
                    self.tm_music.speak('concert/ask_most_memorable_with_live', {'no_match': input_dict['no_match']}),
                    # self.tm_music.speak('no_match/continue', {}),
                    # self.tm_music.speak('artist_question/general', {}),
                ]),
                "next": "s_best_concert",
                "artist": artist,
                "chitchats": input_dict["chitchats"],
                'propose_continue': 'CONTINUE',
            }

    # if dialog_act is not None:
    #     if is_open_question(dialog_act):
    #         if self.tracker.central_element.backstory.confidence >= BACKSTORY_THRESHOLD:
    #             return{
    #                 "next": "concert_init",
    #                 "response": self.tracker.central_element.backstory.text,
    #                 "artist": artist,
    #                 "propose_continue": "CONTINUE"
    #             }
    #         response = EVI_bot(text)
    #         if response is not None:
    #             return{
    #                 "next": "concert_init",
    #                 "response": response,
    #                 "artist": artist,
    #                 "propose_continue": "CONTINUE"
    #             }
    #         else:
    #             no_match = True

    if regex_match.is_rapport(self.tracker):
        acknowledgement = self.tm_music.speak('rapport_match', {})
    elif len(self.tracker.input_text.split(" ")) > 4:
        acknowledgement = self.tm_music.speak('concert/acknowledgement', {})
    else:
        acknowledgement = ''

    if not artist:
        # if no_match:
        #     return {
        #         "response": self.tm_music.speak(
        #             'concert/ask_most_memorable_with_live', {'no_match': input_dict['no_match']}),
        #         "next": "s_best_concert",
        #         "artist": artist,
        #         "chitchats": input_dict["chitchats"],
        #         "propose_continue": "CONTINUE"
        #     }
        return{
            "next": "s_best_concert",
            "response": acknowledgement + self.tm_music.speak(
                'concert/ask_most_memorable_with_live', {'no_match': input_dict['no_match']}),
            "artist": artist,
            "chitchats": input_dict["chitchats"],
            "propose_continue": "CONTINUE"
        }

    if isinstance(artist, dict):
        artist = artist["name"]

    response = acknowledgement + self.tm_music.speak('concert/ask_artist_live', {'artist': artist})
    # if no_match:
    #     return {
    #         "response": input_dict['no_match'] + self.tm_music.speak('concert/ask_artist_live', {'artist': artist}),
    #         "next": "s_best_concert",
    #         "artist": artist,
    #         "chitchats": input_dict["chitchats"],
    #         "propose_continue": "CONTINUE"
    #     }
    return {
        "next": "s_concert",
        "response": response,
        "artist": artist,
        "chitchats": input_dict["chitchats"],
        "propose_continue": "CONTINUE"
    }


def s_concert(self: 'MusicAutomaton', input_dict):
    """
    concert 2
    """

    # text = input_dict["text"]
    # concept = input_dict["concept"]
    # if input_dict["coreference"] is not None:
    #     text = input_dict["coreference"]["text"]
    # intent = detect_intent(text)
    # lexical = input_dict["nlu"]["lexical"]
    # ner = input_dict["nlu"]["ner"]
    artist = input_dict["artist"]
    # knowledge = input_dict["knowledge"]
    # dialog_act = input_dict["dialog_act"]
    # no_match = False

    if regex_match.is_asking_for_recommendation(self.tracker):
        return self.recommend_song(input_dict, 's_concert')

    # play_match = play_music_match(input_dict["text"])
    # if play_match:
    #     artist = detect_artist(text, ner, knowledge)
    #     return self.play_music_response(
    #         artist, "s_concert",
    #         {
    #                 "artist": artist,
    #                 "chitchats": input_dict["chitchats"],
    #                 "propose_continue": "CONTINUE"
    #         })

    question_response = self.handle_question(
        is_unanswerable_question=None,
        handle_topic_specific_question=None,
        handle_general_question=None
    )
    if (
        question_response and question_response.bot_propose_module and
        question_response.bot_propose_module != TopicModule.MUSIC
    ):
        self.logger.debug(f"unclear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                self.tm_transition.speak(f"propose_topic_short/{question_response.bot_propose_module.value}", {})
            ]),
            'propose_continue': 'UNCLEAR',
            'propose_topic': question_response.bot_propose_module.value
        }
    elif question_response:
        self.logger.debug(f"clear question_response: {question_response}")
        if 'ans_neg' in self.tracker.returnnlp.lexical_intent:
            return {
                'response': " ".join([
                    question_response.response,
                    self.tm_music.speak('no_match/continue', {}),
                    self.tm_music.speak('concert/ask_other_live', {})
                    # self.tm_music.speak('no_match/continue', {}),
                    # self.tm_music.speak('artist_question/general', {}),
                ]),
                # "response": input_dict['no_match'] + response,
                "next": "s_other_concerts",
                "artist": artist,
                "chitchats": input_dict["chitchats"],
                "propose_continue": "CONTINUE"
            }
        else:
            return {
                'response': " ".join([
                    question_response.response,
                    self.tm_music.speak('concert/ask_most_memorable_with_live', {'no_match': input_dict['no_match']}),
                    # self.tm_music.speak('no_match/continue', {}),
                    # self.tm_music.speak('artist_question/general', {}),
                ]),
                "next": "s_best_concert",
                "artist": artist,
                "chitchats": input_dict["chitchats"],
                'propose_continue': 'CONTINUE',
            }
    # if dialog_act is not None:
    #     if is_open_question(dialog_act):
    #         if self.tracker.central_element.backstory.confidence >= BACKSTORY_THRESHOLD:
    #             return{
    #                 "next": "s_concert",
    #                 "response": self.tracker.central_element.backstory.text,
    #                 "artist": artist,
    #                 "chitchats": input_dict["chitchats"],
    #                 "propose_continue": "CONTINUE"
    #             }
    #         response = EVI_bot(text)
    #         if response is not None:
    #             return{
    #                 "next": "s_concert",
    #                 "response": response,
    #                 "artist": artist,
    #                 "chitchats": input_dict["chitchats"],
    #                 "propose_continue": "CONTINUE"
    #             }
    #         else:
    #             no_match = True

    curr_turn_artist = next(iter(entity.ArtistItem.detect(self.tracker)), None)
    # entity = self.artist_exists(text, ner, knowledge, concept)
    if curr_turn_artist and curr_turn_artist.noun != input_dict["artist"]:
        return self.s_favmusician(input_dict)

    if "ans_neg" in self.tracker.returnnlp.lexical_intent:
        # if no_match:
        #     return {
        #         "response": input_dict['no_match'] + response,
        #         "next": "s_other_concerts",
        #         "artist": artist,
        #         "chitchats": input_dict["chitchats"],
        #         "propose_continue": "CONTINUE"
        #     }
        return {
            "next": "s_other_concerts",
            "response": self.tm_music.speak('concert/ask_other_live', {}),
            "artist": artist,
            "chitchats": input_dict["chitchats"],
            "propose_continue": "CONTINUE"
        }
    # if no_match:
    #     return {
    #         "response": self.tm_music.speak(
    #             'concert/ask_most_memorable_with_live', {'no_match': input_dict['no_match']}),
    #         "next": "s_other_concerts",
    #         "artist": artist,
    #         "chitchats": input_dict["chitchats"],
    #         "propose_continue": "CONTINUE"
    #     }
    return {
        "next": "s_best_concert",
        "response": self.tm_music.speak('concert/ask_most_memorable_with_live', {'no_match': ''}),
        "artist": artist,
        "chitchats": input_dict["chitchats"],
        "propose_continue": "CONTINUE"
    }


def s_other_concerts(self: 'MusicAutomaton', input_dict):
    """
    concert 3
    """

    # text = input_dict["text"]
    # concept = input_dict["concept"]
    # if input_dict["coreference"] is not None:
    #     text = input_dict["coreference"]["text"]
    # intent = detect_intent(text)
    # ner = input_dict["nlu"]["ner"]
    artist = input_dict["artist"]
    # knowledge = input_dict["knowledge"]
    # lexical = input_dict["nlu"]["lexical"]
    # dialog_act = input_dict["dialog_act"]
    # no_match = False

    if regex_match.is_asking_for_recommendation(self.tracker.input_text):
        return self.recommend_song(input_dict, 's_other_concerts')

    # play_match = play_music_match(input_dict["text"])
    # if play_match:
    #     artist = detect_artist(text, ner, knowledge)
    #     return self.play_music_response(artist, "s_other_concerts",
    #                                     {"artist": artist,
    #                                         "chitchats": input_dict["chitchats"],
    #                                         "propose_continue": "CONTINUE"
    #                                      })

    question_response = self.handle_question(
        is_unanswerable_question=None,
        handle_topic_specific_question=None,
        handle_general_question=None
    )
    if (
        question_response and question_response.bot_propose_module and
        question_response.bot_propose_module != TopicModule.MUSIC
    ):
        self.logger.debug(f"unclear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                self.tm_transition.speak(f"propose_topic_short/{question_response.bot_propose_module.value}", {})
            ]),
            'propose_continue': 'UNCLEAR',
            'propose_topic': question_response.bot_propose_module.value
        }
    elif question_response:
        self.logger.debug(f"clear question_response: {question_response}")
        if 'ans_neg' in self.tracker.returnnlp.lexical_intent:
            return {
                'response': " ".join([
                    question_response.response,
                    self.tm_music.speak('concert/alright', {}),
                    # self.tm_music.speak('no_match/continue', {}),
                    # self.tm_music.speak('concert/ask_other_live', {})
                ]),
                "next": "s_morefacts",
                # "response": input_dict['no_match'] + response,
                "artist": artist,
                "chitchats": input_dict["chitchats"],
                "propose_continue": "CONTINUE"
            }
        else:
            return {
                'response': " ".join([
                    question_response.response,
                    self.tm_music.speak('concert/ask_most_memorable', {'no_match': input_dict['no_match']}),
                    # 'concert/ask_most_memorable_with_live', {'no_match': input_dict['no_match']},
                    # self.tm_music.speak('no_match/continue', {}),
                    # self.tm_music.speak('artist_question/general', {}),
                ]),
                "next": "s_best_concert",
                "artist": artist,
                "chitchats": input_dict["chitchats"],
                'propose_continue': 'CONTINUE',
            }
    # if dialog_act is not None:
    #     if is_open_question(dialog_act):
    #         if self.tracker.central_element.backstory.confidence >= BACKSTORY_THRESHOLD:
    #             return{
    #                 "next": "s_other_concerts",
    #                 "response": self.tracker.central_element.backstory.text,
    #                 "artist": artist,
    #                 "chitchats": input_dict["chitchats"],
    #                 "propose_continue": "CONTINUE"
    #             }
    #         response = EVI_bot(text)
    #         if response is not None:
    #             return{
    #                 "next": "s_other_concerts",
    #                 "response": response,
    #                 "artist": artist,
    #                 "chitchats": input_dict["chitchats"],
    #                 "propose_continue": "CONTINUE"
    #             }
    #         else:
    #             no_match = True

    if 'ans_neg' in self.tracker.returnnlp.lexical_intent:
        response = self.tm_music.speak('concert/alright', {})
        # if no_match:
        #     return {
        #         "next": "s_morefacts",
        #         "response": input_dict['no_match'] + response,
        #         "artist": artist,
        #         "chitchats": input_dict["chitchats"],
        #         "propose_continue": "CONTINUE"
        #     }
        return {
            "next": "s_morefacts",
            "response": response,
            "artist": artist,
            "chitchats": input_dict["chitchats"],
            "propose_continue": "CONTINUE"
        }
    # if no_match:
    #     return {
    #         "next": "s_morefacts",
    #         "response": self.tm_music.speak('concert/ask_most_memorable', {'no_match': input_dict['no_match']}),
    #         "artist": artist,
    #         "chitchats": input_dict["chitchats"],
    #         "propose_continue": "CONTINUE"
    #     }
    return {
        "next": "s_best_concert",
        "response": self.tm_music.speak('concert/ask_most_memorable', {'no_match': ''}),
        "artist": artist,
        "chitchats": input_dict["chitchats"],
        "propose_continue": "CONTINUE"
    }


def s_best_concert(self: 'MusicAutomaton', input_dict):
    """
    concert 4
    """

    # text = input_dict["text"]
    # concept = input_dict["concept"]
    # if input_dict["coreference"] is not None:
    #     text = input_dict["coreference"]["text"]
    # intent = detect_intent(text)
    # ner = input_dict["nlu"]["ner"]
    artist = input_dict["artist"]
    # topic_keyword = input_dict["topic_keyword"]
    # noun_phrase = input_dict["local_noun_phrase"]
    # dialog_act = input_dict["dialog_act"]
    # knowledge = input_dict["knowledge"]
    # no_match = False

    if regex_match.is_asking_for_recommendation(self.tracker.input_text):
        return self.recommend_song(input_dict, 's_best_concert')
    # play_match = play_music_match(input_dict["text"])
    # if play_match:
    #     artist = detect_artist(text, ner, knowledge)
    #     return self.play_music_response(artist, "s_best_concert",
    #                                     {
    #                                         "artist": input_dict["artist"],
    #                                         "chitchats": input_dict["chitchats"],
    #                                         "propose_continue": "CONTINUE"})
    question_response = self.handle_question(
        is_unanswerable_question=None,
        handle_topic_specific_question=None,
        handle_general_question=None
    )
    if (
        question_response and question_response.bot_propose_module and
        question_response.bot_propose_module != TopicModule.MUSIC
    ):
        self.logger.debug(f"unclear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                self.tm_transition.speak(f"propose_topic_short/{question_response.bot_propose_module.value}", {})
            ]),
            'propose_continue': 'UNCLEAR',
            'propose_topic': question_response.bot_propose_module.value
        }
    elif question_response:
        self.logger.debug(f"clear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                self.tm_music.speak('concert/alright', {}),
                # self.tm_music.speak('no_match/continue', {}),
                # self.tm_music.speak('artist_question/general', {}),
            ]),
            "next": "s_morefacts",
            "artist": artist,
            "chitchats": input_dict["chitchats"],
            "propose_continue": "CONTINUE"
        }
    # if dialog_act is not None:
    #     if is_open_question(dialog_act):
    #         if self.tracker.central_element.backstory.confidence >= BACKSTORY_THRESHOLD:
    #             return{
    #                 "next": "s_best_concert",
    #                 "response": self.tracker.central_element.backstory.text,
    #                 "artist": artist,
    #                 "chitchats": input_dict["chitchats"],
    #                 "propose_continue": "CONTINUE"
    #             }
    #         response = EVI_bot(text)
    #         if response is not None:
    #             return{
    #                 "next": "s_best_concert",
    #                 "response": response,
    #                 "artist": artist,
    #                 "chitchats": input_dict["chitchats"],
    #                 "propose_continue": "CONTINUE"
    #             }
    #         else:
    #             no_match = True
    # entity = get_music_entity(concept, knowledge)
    # entity = None

    # if regex_match.is_other_answer(self.tracker.input_text):
    #     response = self.tm_music.speak('concert/unknown', {})
    # else:
    #     response = self.tm_music.speak('concert/alright', {})

    # if entity is not None:
    #     concert = entity
    #     return {
    #         "next": "s_morefacts",
    #         "response": self.tm_music.speak('concert/with_concert', {'concert': concert}),
    #         "artist": artist,
    #         "chitchats": input_dict["chitchats"],
    #         "propose_continue": "CONTINUE"
    #     }
    # if no_match:
    #     return {
    #         "next": "s_morefacts",
    #         "response": self.tm_music.speak('concert/with_concert', {'concert': input_dict['no_match']}),
    #         "artist": artist,
    #         "chitchats": input_dict["chitchats"],
    #         "propose_continue": "CONTINUE"
    #     }
    return {
        "next": "s_morefacts",
        "response": self.tm_music.speak('concert/alright', {}),
        "artist": artist,
        "chitchats": input_dict["chitchats"],
        "propose_continue": "CONTINUE"
    }
