
from typing import TYPE_CHECKING

from nlu.constants import TopicModule

from ...utils import entity
from ...utils.legacy import detect_intent

if TYPE_CHECKING:
    from ...music_automaton import MusicAutomaton


def s_morefacts(self: 'MusicAutomaton', input_dict):
    """
    verify
    """

    # text = input_dict["text"]
    # concept = input_dict["concept"]
    # if input_dict["coreference"] is not None:
    #     text = input_dict["coreference"]["text"]
    # artist = input_dict["artist"]
    # ner = input_dict["nlu"]["ner"]
    # knowledge = input_dict["knowledge"]
    # fun_fact = retrieve_bot(artist)
    # dialog_act = input_dict["dialog_act"]
    # play_match = play_music_match(input_dict["text"])
    # if play_match:
    #     artist = detect_artist(text, ner, knowledge)
    #     return self.play_music_response("artist", "s_morefacts",
    #                                     {"artist": artist,
    #                                         "chitchats": input_dict["chitchats"]})

    self.logger.debug(f"handle question")
    question_response = self.handle_question(
        is_unanswerable_question=None,
        handle_topic_specific_question=None,
        handle_general_question=None
    )
    if (
        question_response and question_response.bot_propose_module and
        question_response.bot_propose_module != TopicModule.MUSIC
    ):
        self.logger.debug(f"unclear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                self.tm_transition.speak(f"propose_topic_short/{question_response.bot_propose_module.value}", {})
            ]),
            'propose_continue': 'UNCLEAR',
            'propose_topic': question_response.bot_propose_module.value
        }
    elif question_response:
        self.logger.debug(f"clear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                # self.tm_music.speak('no_match/continue', {}),
                # self.tm_music.speak('artist_question/general', {}),
            ]),
            "next": "s_init",
            'propose_continue': 'STOP',
            "context": input_dict["currentstate"],
            "exit": False
        }

    # if dialog_act is not None:
    #     if is_open_question(dialog_act):
    #         if self.tracker.central_element.backstory.confidence >= BACKSTORY_THRESHOLD:
    #             return {
    #                 "next": "s_morefacts",
    #                 "response": self.tracker.central_element.backstory.text,
    #                 "artist": artist,
    #                 "propose_continue": "CONTINUE",
    #                 "chitchats": input_dict["chitchats"]
    #             }
    #         response = EVI_bot(text)
    #         if response is not None:
    #             return {
    #                 "next": "s_morefacts",
    #                 "response": response,
    #                 "artist": artist,
    #                 "propose_continue": "CONTINUE",
    #                 "chitchats": input_dict["chitchats"]
    #             }
    #         else:
    #             return {
    #                 "response": input_dict['no_match'],
    #                 "next": "s_init",
    #                 "propose_continue": "STOP",
    #                 "context": input_dict["currentstate"],
    #                 "exit": False
    #             }

    artist = next(iter(entity.ArtistItem.detect(self.tracker)), None)
    # entity = self.artist_exists(text, ner, knowledge, concept)
    if artist and artist.noun != input_dict["artist"]:
        self.logger.debug(f"found artist: {artist}, going to s_favmusician")
        return self.s_favmusician(input_dict)

    intent = detect_intent(self.tracker.input_text)
    if intent == "answer_no":
        self.logger.debug(f"custom intent: {intent}, going to s_chitchat")
        return self.s_chitchat(input_dict)

    self.logger.debug(f"default return, going to s_verifychitchat")
    return {
        "next": "s_verifychitchat",
        "response": self.tm_music.speak('stop/no_change_topic', {}),
        "context": input_dict["currentstate"],
        "chitchats": input_dict["chitchats"],
        "artist": artist,
        "propose_continue": "UNCLEAR"
    }
