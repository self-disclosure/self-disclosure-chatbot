import random
import re
from typing import TYPE_CHECKING

from nlu.constants import TopicModule

from ...utils import entity, regex_match
from ...utils.legacy import get_chitchat_ack

if TYPE_CHECKING:
    from ...music_automaton import MusicAutomaton


def chitchat_followup(self: 'MusicAutomaton', input_dict):
    chitchat_id = input_dict["chitchats"]["current"]
    chitchat = self.read_chitchat_content(chitchat_id)
    return {
        "response": chitchat["followup"],
        "next": "s_chitchatcont",
        "chitchats": input_dict["chitchats"],
        "propose_continue": "CONTINUE",
        "context": input_dict["currentstate"],
        "artist": input_dict["artist"],
    }


def s_chitchatcont(self: 'MusicAutomaton', input_dict):
    """
    ask question
    """

    chitchats = input_dict["chitchats"]["remaining"]
    chitchat_id = input_dict["chitchats"]["current"]
    chitchat = self.read_chitchat_content(chitchat_id)
    # lexical = input_dict["nlu"]["lexical"]
    # topic_keyword = input_dict["topic_keyword"]
    # noun_phrase = input_dict["local_noun_phrase"]
    # text = input_dict["text"]
    # concept = input_dict["concept"]
    # if input_dict["coreference"] is not None:
    #     text = input_dict["coreference"]["text"]
    # ner = input_dict["nlu"]["ner"]
    # knowledge = input_dict["knowledge"]
    # dialog_act = input_dict["dialog_act"]
    # skip_dialog = False
    next_chitchat = {}
    # user_id = input_dict["user_id"]
    # no_match = False

    self.logger.debug(f"entering")

    if len(chitchats) > 0:
        next_chitchat_id = chitchats.pop()
        next_chitchat = self.read_chitchat_content(next_chitchat_id)
    # intent = detect_intent(text)
    if regex_match.is_asking_for_recommendation(self.tracker):
        self.logger.debug(f"rec, going to recommend_song")
        return self.recommend_song(input_dict, 's_chitchat')

    # play_match = play_music_match(input_dict["text"])
    # if play_match:
    #     artist = detect_artist(text, ner, knowledge)
    #     return self.play_music_response(artist, "t_chitchatcont",
    #                                     {
    #                                         "chitchats": {
    #                                             "remaining": chitchats,
    #                                             "current": next_chitchat_id,
    #                                             "count": input_dict["chitchats"]["count"] + 1,
    #                                         },
    #                                         "propose_continue": "CONTINUE",
    #                                         "artist": input_dict["artist"],
    #                                         "context": input_dict["currentstate"]
    #                                     })

    self.logger.debug(f"handle question")
    question_response = self.handle_question(
        is_unanswerable_question=None,
        handle_topic_specific_question=None,
        handle_general_question=None
    )
    if (
        question_response and question_response.bot_propose_module and
        question_response.bot_propose_module != TopicModule.MUSIC
    ):
        self.logger.debug(f"unclear question_response: {question_response}")
        return {
            'response': " ".join([
                question_response.response,
                self.tm_transition.speak(f"propose_topic_short/{question_response.bot_propose_module.value}", {})
            ]),
            'propose_continue': 'UNCLEAR',
            'propose_topic': question_response.bot_propose_module.value
        }
    elif question_response:
        self.logger.debug(f"clear question_response: {question_response}")
        # follow no match rule
        # TODO: is this the best way?
        # return {
        #     "response": input_dict['no_match'] + " " + chitchat["a"],
        #     "next": "t_chitchatcont",
        #     "chitchats": {
        #         "remaining": chitchats,
        #         "current": next_chitchat_id,
        #         "count": input_dict["chitchats"]["count"] + 1,
        #     },
        #     "propose_continue": "CONTINUE",
        #     "artist": input_dict["artist"],
        #     "context": input_dict["currentstate"]
        # }
        return self.s_chitchat(input_dict, chain_response=question_response.response)
        # return {
        #     'response': " ".join([
        #         question_response.response,
        #         self.tm_music.speak('no_match/continue', {}),
        #         self.tm_music.speak('artist_question/general', {}),
        #     ]),
        #     "next": "s_chitchat",
        #     'propose_continue': 'CONTINUE',
        # }

    if re.search(r'talk about|my favorite', self.tracker.input_text):
        self.logger.debug(f"talk about|myfavorite match")

        artist = next(iter(entity.ArtistItem.detect(self.tracker)), None)

        # artist = self.artist_exists(text, ner, knowledge)
        if artist and artist.noun != input_dict["artist"]:
            self.logger.debug(f"match artist={artist}, going to s_favmusician")
            return self.s_favmusician(input_dict)

        genre = next(iter(entity.GenreItem.detect(self.tracker)), None)
        # genre = detect_genre(topic_keyword, noun_phrase, concept, text)
        if genre and genre.noun != input_dict['genre']:
            self.logger.debug(f"match genre={genre}, going to s_favmusician")
            return {
                "next": "s_favmusician",
                "response": self.tm_music.speak(
                    'artist_question/follow_genre_question/no_pop', {'genre': genre.noun}),
                "first_time": False,
                "chitchats": input_dict["chitchats"],
                "artist": input_dict["artist"]
            }

    song = next(iter(entity.SongItem.detect(self.tracker)), None)
    if not (
        input_dict['song_question'] and not song and
        not re.search(r"do you like", self.tracker.input_text)
    ):
        if re.search(r'why', self.tracker.input_text) and chitchat["a"] == 'abc':
            self.logger.debug(f"no song but song_question and 'why', going to chitchat_followup")
            return self.chitchat_followup(input_dict)

        # if is_open_question(dialog_act):
        #     if self.tracker.central_element.backstory.confidence >= BACKSTORY_THRESHOLD:
        #         return {
        #             "response": self.tracker.central_element.backstory.text,
        #             "next": "t_chitchatcont",
        #             "chitchats": {
        #                 "remaining": chitchats,
        #                 "current": next_chitchat_id,
        #                 "count": input_dict["chitchats"]["count"] + 1,
        #             },
        #             "propose_continue": "CONTINUE",
        #             "artist": input_dict["artist"],
        #             "context": input_dict["currentstate"]
        #         }
        #     response = EVI_bot(text)
        #     if response is not None:
        #         return {
        #             "response": response,
        #             "next": "t_chitchatcont",
        #             "chitchats": {
        #                 "remaining": chitchats,
        #                 "current": next_chitchat_id,
        #                 "count": input_dict["chitchats"]["count"] + 1,
        #             },
        #             "propose_continue": "CONTINUE",
        #             "artist": input_dict["artist"],
        #             "context": input_dict["currentstate"]
        #         }
        #     else:
        #         no_match = True

    if chitchat["keyword"] == "least_fav_music" and self.tracker.input_text == "yes":
        self.logger.debug(f"keyword == least_fav_music and input_text == yes, going to s_chitchatcont")
        return {
            "response": self.tm_music.speak("chitchats/pardon", {}),
            "next": "s_chitchatcont",
            "chitchats": input_dict["chitchats"],
            "propose_continue": "CONTINUE",
            "artist": input_dict["artist"],
            "context": input_dict["currentstate"]
        }

    response = ''
    acknowledgement, chit_keyword = get_chitchat_ack(input_dict, chitchat, self.tm_music)
    random.shuffle(chitchats)
    if len(input_dict["chitchats"]["remaining"]) == 0:
        self.logger.debug(f"no more chitchats left")
        response = acknowledgement
        if chitchat["a"] != "abc":
            response += chitchat["a"]
        if chit_keyword == 'ack_general':
            self.logger.debug(f"keyword == ack_general, going to change_topic immediately")
            return self.change_topic(input_dict)
        else:
            self.logger.debug(f"keyword != ack_general, going to change_topic next turn")
            return{
                "response": response,
                "next": "change_topic",
                "chitchats": {
                    "remaining": chitchats,
                    "current": next_chitchat_id,
                    "count": input_dict["chitchats"]["count"] + 1,
                },
                "propose_continue": "CONTINUE",
                "artist": input_dict["artist"],
                "context": input_dict["currentstate"]
            }
    if chitchat["a"] == "abc":
        self.logger.debug(f"chitchat answer is 'abc'")
        song_question = next_chitchat['keyword'] in {'emotional', 'karaoke'}

        if re.search(r'cool instrument', acknowledgement):
            response = acknowledgement
            next_state = 't_chitchatcont'
        else:
            response = acknowledgement + " " + next_chitchat["q"]
            next_state = 's_chitchatcont'

        self.logger.debug(f"{response}, going to {next_state}")

        return{
            "response": response,
            "next": next_state,
            "chitchats": {
                "remaining": chitchats,
                "current": next_chitchat_id,
                "count": input_dict["chitchats"]["count"] + 1,
            },
            "propose_continue": "CONTINUE",
            "artist": input_dict["artist"],
            "context": input_dict["currentstate"],
            "song_question": song_question,
        }
    else:
        response = acknowledgement + chitchat["a"]
        self.logger.debug(f"chitchat answer is not 'abc', {response}, going to t_chitchatcont")

    # if no_match:
    #     return {
    #         "response": input_dict['no_match'] + " " + chitchat["a"],
    #         "next": "t_chitchatcont",
    #         "chitchats": {
    #             "remaining": chitchats,
    #             "current": next_chitchat_id,
    #             "count": input_dict["chitchats"]["count"] + 1,
    #         },
    #         "propose_continue": "CONTINUE",
    #         "artist": input_dict["artist"],
    #         "context": input_dict["currentstate"]
    #     }
        return {
            "response": response,
            "next": "t_chitchatcont",
            "chitchats": {
                "remaining": chitchats,
                "current": next_chitchat_id,
                "count": input_dict["chitchats"]["count"] + 1,
            },
            "propose_continue": "CONTINUE",
            "artist": input_dict["artist"],
            "context": input_dict["currentstate"]
        }
