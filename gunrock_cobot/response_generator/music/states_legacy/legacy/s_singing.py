from typing import TYPE_CHECKING

from nlu.constants import Positivity

# from ...utils import entity

if TYPE_CHECKING:
    from ...music_automaton import MusicAutomaton


def s_singing(self: 'MusicAutomaton', input_dict):
    """
    verify if user wants to talk about the musician
    play_music_response also goes here if there's an artist
    """

    self.logger.debug(f"entering")

    singing = self.tm_music.speak(f"bot_singing/default", {})
    return {
        "response": " ".join([
            singing,
            self.tm_music.speak(f"bot_singing/what_do_you_think", {})
        ]),
        "next": 's_singing_rating',
        "context": input_dict["context"],
        "artist": input_dict["artist"],
        "sim_artist": input_dict["sim_artist"],
        "propose_continue": "CONTINUE",
        "first_time": input_dict["first_time"],
        "chitchats": input_dict["chitchats"],
        "templates": input_dict["templates"],
        "exit": input_dict["exit"],
    }


def s_singing_rating(self: 'MusicAutomaton', input_dict):

    self.logger.debug(f"entering")

    selector = 'bot_singing/reaction'

    if self.tracker.returnnlp.answer_positivity is Positivity.neg:
        self.logger.debug(f"neg answer, going to s_chitchat")
        response = self.tm_music.speak(f"{selector}/neg", {})
        return self.s_chitchat(input_dict, chain_response=response, chain_with_acknowledgement=True)

    elif self.tracker.returnnlp.answer_positivity is Positivity.pos:
        self.logger.debug(f"pos answer, going to s_chitchat")
        response = self.tm_music.speak(f"{selector}/pos", {})
        return self.s_chitchat(input_dict, chain_response=response, chain_with_acknowledgement=True)

    else:
        self.logger.debug(f"default, going to s_chitchat")
        return self.s_chitchat(input_dict)
