import re

from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, NextState
from user_profiler.user_profile import UserProfile as GlobalUserProfile

from response_generator.music import states
from response_generator.music.states.base import MusicState


selector_root = 'resume'


class ResumeRespond(MusicState):

    name = 'resume_respond'
    special_type = {'global_state'}

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker):
        try:
            last_topic_module = TopicModule(
                tracker.user_attributes.module_selection.last_topic_module
            )
        except ValueError:
            last_topic_module = TopicModule.NA

        global_user_profile = GlobalUserProfile(tracker.user_attributes.ua_ref)

        self.logger.info(
            f"last_topic_module = {last_topic_module}, "
            f"first = {tracker.persistent_store.get('__first__')}, "
            f"visit = {global_user_profile.visit}, "
        )

        if not tracker.persistent_store.get('__first__'):
            return
        if (
            (tracker.persistent_store.get('__first__') or 0) != global_user_profile.visit or
            (
                (tracker.persistent_store.get('__first__') or 0) == global_user_profile.visit and
                last_topic_module != TopicModule.MUSIC
            )
        ):
            return self.name

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        global_user_profile = GlobalUserProfile(tracker.user_attributes.ua_ref)
        if (tracker.persistent_store.get('__first__') or 0) != global_user_profile.visit:
            """second conversation"""
            second_convo = True
        else:
            second_convo = False
        prev_turn = self.module.state_tracker.prev_turn(-1) or [None]
        last_propose = prev_turn[-1]

        responses = []
        tag = None

        if (
            last_propose in {
                states.FavGenrePropose.name,
                states.DislikeGenrePropose.name,
            }
        ):
            if second_convo:
                responses.append(
                    f"If I remember correctly, we were talking about our favorite music genres last time."
                )
            else:
                responses.append(
                    f"I think we were talking about our favorite music genres earlier."
                )
            tag = 'genre'

        elif (
            last_propose in {
                states.FavoriteGenreArtistPropose.name,

                states.FavArtistPropose.name,
                states.FavArtistAnotherPropose.name,
                states.WhyLikeArtistPropose.name,
                states.DislikeArtistPropose.name,
                states.FavSongByArtistPropose.name,
                states.FavSongByArtistAnotherPropose.name,
                states.MyFavArtistPropose.name,
                states.DoYouLikeArtistPropose.name,
            }
        ):
            if second_convo:
                responses.append(
                    f"If I remember correctly, we were talking about our favorite singers last time."
                )
            else:
                responses.append(
                    f"I think we were talking about our favorite singers earlier."
                )
            tag = 'artist'

        elif (
            isinstance(last_propose, str) and
            re.search(r"instrument", last_propose)
        ):
            if second_convo:
                responses.append(
                    f"If I remember correctly, we were talking about instruments last time."
                )
            else:
                responses.append(
                    f"I think we were talking about instruments earlier."
                )
            tag = 'instrument'

        else:
            response = dispatcher.tm.speak(f"{selector_root}/propose/default", {})
            responses.append(response)
            tag = None

        states.SwitchSubtopicEnter.require_entity_from_resume(tracker, responses, tag)
        return [NextState(states.SwitchSubtopicEnter.name, jump=True)]
