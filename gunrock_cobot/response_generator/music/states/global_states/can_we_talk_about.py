import re

from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState
from response_generator.music.utils import entity


class CanWeTalkAboutEnter(MusicState):

    name = 'can_we_talk_about_enter'
    special_type = {'global_state'}

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker):

        cond = [
            self.handler.command_detector.has_command(),
            self.handler.command_detector.is_request_jumpout(),
            self.handler.question_detector.has_question(),
            self.handler.question_detector.is_ask_back_question(),
            self.handler.question_detector.is_question_in_command_form(),
        ]

        sys_ack_tag, sys_ack_utt = tracker.system_acknowledgment
        self.logger.debug(f"cond = {cond}")

        self.logger.debug(f"sys_ack = {tracker.system_acknowledgment}")
        if (
            re.search(r"(can we|want to|wanna|let('s| us)) talk about", tracker.input_text)
        ):
            artist: entity.ArtistItem = next(iter(entity.ArtistItem.detect(tracker)), None)
            if artist:
                if not (sys_ack_tag and sys_ack_utt):
                    dispatcher.respond("Oh sure.")

                if states.DoYouLikeArtistPropose.accept_transition(tracker, artist, self.user_profile):
                    states.DoYouLikeArtistPropose.require_entity(tracker, artist)
                    return states.DoYouLikeArtistPropose.name
                elif states.WhyLikeArtistPropose.accept_transition(tracker, artist, self.user_profile):
                    states.WhyLikeArtistPropose.require_entity(tracker, artist)
                    return states.WhyLikeArtistPropose.name
                else:
                    states.FavSongByArtistPropose.require_entity(tracker, artist)
                    return states.FavSongByArtistPropose.name
        return

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.SwitchSubtopicEnter.name)]
