import re

from nlu.constants import Positivity
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState
from response_generator.music.utils import entity, regex_match


selector_root = 'me_singing'


class MeSingingPropose(MusicState):

    name = 'me_singing_propose'

    @classmethod
    def accept_transition(cls, tracker: Tracker, dispatcher: Dispatcher):
        song_repo_count = dispatcher.tm.count_utterances(f"{selector_root}/respond/song")
        counter = tracker.persistent_store.get('me_singing_sung_counter') or 0
        return counter < song_repo_count

    @classmethod
    def require_entity(cls, tracker: Tracker, artist: entity.ArtistItem, with_transition=False):
        tracker.volatile_store[f'{cls.name}_with_transition'] = True
        tracker.one_turn_store['artist_to_discuss'] = artist

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        with_transition = (
            tracker.volatile_store.get(f"{self.name}_with_transition")
        )
        if with_transition:
            dispatcher.respond("However,")
        else:
            dispatcher.respond("Oh sure,")

        dispatcher.respond_template(f"{selector_root}/propose/default", dict())
        return [NextState(states.MeSingingRespond.name)]


class MeSingingRespond(MusicState):

    name = 'me_singing_respond'
    special_type = {'global_state'}

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker):
        self.logger.debug(f"regex match = {regex_match.is_command_sing_a_song(tracker)}")
        if (
            regex_match.is_command_sing_a_song(tracker) or
            (
                re.search(r"^(do the song again)$", tracker.input_text) and
                any(i in {states.MeSingingReactionRespond.name} for i in self.module.state_tracker.prev_turn() or [])
            )
        ):
            return self.name

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        if (
            tracker.returnnlp.answer_positivity is Positivity.pos or
            not any(i in {states.MeSingingPropose.name} for i in self.module.state_tracker.prev_turn() or [])
        ):
            if (
                not states.MeSingingPropose.accept_transition(tracker, dispatcher)
            ):
                dispatcher.respond_template(f"{selector_root}/reaction/no_more_songs", {})
                return [NextState(states.SwitchSubtopicEnter.name, jump=True)]

            else:
                counter = tracker.persistent_store.get('me_singing_sung_counter') or 0
                tracker.persistent_store['me_singing_sung_counter'] = counter + 1
                dispatcher.respond_template(f"{selector_root}/respond/song", {})
                return [NextState(states.MeSingingReactionPropose.name, jump=True)]

        else:
            if self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond("That's alright.")

            states.ExitTransitionEnter.require_entity(tracker, False)
            return [NextState(states.ExitTransitionEnter.name, jump=True)]


class MeSingingReactionPropose(MusicState):

    name = 'me_singing_reaction_propose'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/reaction/propose/default", dict())
        return [NextState(states.MeSingingReactionRespond.name)]


class MeSingingReactionRespond(MusicState):

    name = 'me_singing_reaction_respond'

    @classmethod
    def require_entity(cls, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.expect_opinion(True)

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        """"""
        """question"""
        def handle_question():
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                )

        if (
            self.sys_ack_tag == 'ack_opinion' and self.sys_ack_utt
        ):
            dispatcher.respond(self.sys_ack_utt)
            qr_events = handle_question()
            if qr_events:
                return qr_events

        elif (
            re.search(r"another", tracker.input_text) and
            states.MeSingingPropose.accept_transition(tracker, dispatcher)
        ):
            """use propose check, but jump to respond directly"""
            return [NextState(states.MeSingingRespond.name, jump=True)]
            # counter = tracker.persistent_store.get('me_singing_sung_counter') or 0
            # tracker.persistent_store['me_singing_sung_counter'] = counter + 1
            # dispatcher.respond_template(f"{selector_root}/respond/song", {})
            # return [NextState(states.MeSingingReactionPropose.name, jump=True)]

        elif (
            tracker.returnnlp.answer_positivity is Positivity.pos or
            tracker.returnnlp.opinion_positivity is Positivity.pos

        ):
            dispatcher.respond_template(f"{selector_root}/reaction/respond/pos", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events

        elif (
            tracker.returnnlp.answer_positivity is Positivity.neg or
            tracker.returnnlp.opinion_positivity is Positivity.neg
        ):
            dispatcher.respond_template(f"{selector_root}/reaction/respond/neg", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events

        elif (
            self.sys_ack_utt
        ):
            dispatcher.respond(self.sys_ack_utt)
            qr_events = handle_question()
            if qr_events:
                return qr_events
        else:
            dispatcher.respond_template(f"ack/alright", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events

        return [NextState(states.SwitchSubtopicEnter.name, jump=True)]
