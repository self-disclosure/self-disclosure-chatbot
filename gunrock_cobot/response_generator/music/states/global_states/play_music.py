from typing import Optional

from nlu.constants import Positivity
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState
from response_generator.music.utils import entity, regex_match


selector_root = 'play_music_intent'


class PlayMusicRespond(MusicState):

    name = 'play_music_respond'
    special_type = {'global_state'}

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker):
        if any(
            i in {states.InstrumentCannedUsualSongProposing.name}
            for i in (self.module.state_tracker.prev_turn() or [])
        ):
            return False

        if regex_match.is_play_music(tracker):
            artist = next(iter(entity.ArtistItem.detect(tracker)), None)
            PlayMusicRespond.require_entity(tracker, artist)
            return self.name

    @classmethod
    def require_entity(cls, tracker: Tracker, artist: Optional[entity.ArtistItem]):
        tracker.one_turn_store['artist_to_discuss'] = artist

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        song: entity.SongItem = next(iter(entity.SongItem.detect(tracker)), None)

        tracker.one_turn_store['play_music_reponse'] = self.artist_to_discuss
        self.logger.debug(
            f"[MUSIC] play_music_response: "
            f"{self.artist_to_discuss}, {song}, "
        )

        dispatcher.respond_template(f"{selector_root}/propose/base", {})

        if self.artist_to_discuss:
            states.PlayMusicTalkAboutArtistPropose.require_entity(tracker, self.artist_to_discuss)
            return [NextState(states.PlayMusicTalkAboutArtistPropose.name, jump=True)]

        else:
            states.MeSingingPropose.require_entity(tracker, self.artist_to_discuss, with_transition=True)
            return [NextState(states.MeSingingPropose.name, jump=True)]


class PlayMusicTalkAboutArtistPropose(MusicState):

    name = 'play_music_talk_about_artist_propose'

    @classmethod
    def require_entity(cls, tracker: Tracker, artist: entity.ArtistItem):
        tracker.one_turn_store['artist_to_discuss'] = artist

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/propose/artist", dict(artist=self.artist_to_discuss.noun))
        states.PlayMusicTalkAboutArtistRespond.require_entity(tracker, self.artist_to_discuss)
        return [NextState(states.PlayMusicTalkAboutArtistRespond.name)]


class PlayMusicTalkAboutArtistRespond(MusicState):

    name = 'play_music_talk_about_artist_respond'

    @classmethod
    def require_entity(cls, tracker: Tracker, artist: entity.ArtistItem):
        tracker.one_turn_store['artist_to_discuss'] = artist

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        if tracker.returnnlp.answer_positivity is Positivity.neg:
            states.ExitTransitionEnter.require_entity(tracker, False)
            dispatcher.respond("That's alright.")
            return [NextState(states.ExitTransitionEnter.name, jump=True)]

        elif tracker.returnnlp.answer_positivity is Positivity.pos and self.artist_to_discuss:
            states.FavArtistEnter.increment_counter(tracker)
            dispatcher.respond_template(
                f"{selector_root}/respond/pos_with_artist", dict(artist=self.artist_to_discuss.noun))

            if states.DoYouLikeArtistPropose.accept_transition(tracker, self.artist_to_discuss, self.user_profile):
                states.DoYouLikeArtistPropose.require_entity(tracker, self.artist_to_discuss)
                return [NextState(states.DoYouLikeArtistPropose.name, jump=True)]
            else:
                states.FavSongByArtistPropose.require_entity(tracker, self.artist_to_discuss)
                return [NextState(states.FavSongByArtistPropose.name, jump=True)]

        else:
            return [NextState(states.SwitchSubtopicEnter.name, jump=True)]
