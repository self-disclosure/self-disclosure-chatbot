import logging
import re

import util_redis
from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, NextState
from selecting_strategy.module_selection import GlobalState, ProposeKeywordEntity

from response_generator.music import states
from response_generator.music.states.base import MusicState
from response_generator.music.utils import entity


logger = logging.getLogger('music.module_selection')
redis = util_redis.RedisHelper(dev_mode=False)


class SystemModuleSelection(MusicState):

    name = 'module_selection_global_state'
    special_type = {'global_state'}

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker):
        try:
            last_topic_module = TopicModule(
                tracker.user_attributes.module_selection.last_topic_module
            )
        except ValueError:
            last_topic_module = TopicModule.NA

        if (
            (last_topic_module == TopicModule.MUSIC)
        ):
            return None

        keyword: GlobalState = tracker.user_attributes.module_selection.global_state
        proposed_keyword = tracker.user_attributes.module_selection.get_top_propose_keyword()

        self.logger.debug(
            f"got global_state from module_selection: "
            f"state = {keyword}, "
            f"proposed_keyword = {proposed_keyword}, "
            f"mapped = {map_proposed_keyword_to_entity(proposed_keyword)}, "
        )
        if keyword is GlobalState.ASK_FAV_MUSIC_GENRE:
            return states.FavGenreRespond.name

        elif proposed_keyword and proposed_keyword.text:
            entity_item = map_proposed_keyword_to_entity(proposed_keyword)
            if isinstance(entity_item, entity.ArtistItem):
                # if states.DoYouLikeArtistPropose.accept_transition(tracker, entity_item, self.user_profile):
                #     states.DoYouLikeArtistPropose.require_entity(tracker, entity_item)
                #     return states.DoYouLikeArtistPropose.name
                if states.WhyLikeArtistPropose.accept_transition(tracker, entity_item, self.user_profile):
                    dispatcher.respond(f"Nice! I like {entity_item.noun} too!.")
                    states.WhyLikeArtistPropose.require_entity(tracker, entity_item)
                    return states.WhyLikeArtistPropose.name
                else:
                    dispatcher.respond(f"Nice! I like {entity_item.noun} too! I'm a music fan myself.")
                    return states.SwitchSubtopicEnter.name

            elif isinstance(entity_item, entity.InstrumentItem):
                if states.InstrumentEnter.accept_transition(tracker, entity_item, self.user_profile):
                    dispatcher.respond(
                        f"Nice! You must be very talented if you can play the {entity_item.noun}!"
                    )
                    states.InstrumentEnter.require_entity(tracker, entity_item)
                    return states.InstrumentEnter.name

                else:
                    dispatcher.respond(
                        f"Nice! You must be very talented if you can play the {entity_item.noun}!"
                        f"I don't play any instruments, but I'm kind of a music fan myself."
                    )
                    return states.SwitchSubtopicEnter.name

            elif isinstance(entity_item, entity.GenreItem):
                # if states.GenreEnter.accept_transition(tracker):
                #     return states.GenreEnter.name

                # else:
                # TODO: update this
                dispatcher.respond(
                    f"Nice! I love {entity_item.noun} too! "
                    f"I like all kinds of music in general. "
                )
                states.FavoriteGenreArtistPropose.require_entity(tracker, entity_item)
                return states.FavoriteGenreArtistPropose.name

            else:
                dispatcher.respond(f"Great! I'm a music fan myself.")
                return states.SwitchSubtopicEnter.name

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.SwitchSubtopicEnter.name, jump=True)]


def type_mapping():
    return dict(
        general=r"music",
        artist="|".join([
            r"singer", r"performer", r"musician", r"band", r"person",
            r"\b(american singer|artist|rapper|composer)\b",
        ]),
        instrument=r"musical instrument",
        genre=r"genre",
    )


def map_proposed_keyword_to_entity(proposed_keyword: ProposeKeywordEntity):

    # import logging
    # logger = logging.getLogger(__name__)
    # logger.debug(f"MAP MAP = {proposed_keyword}")

    if not hasattr(proposed_keyword, 'text'):
        return
    if not proposed_keyword.text:
        return

    # entity_type = next(iter(
    #     k for (k, v) in type_mapping().items()
    #     if any(re.search(v, i, re.I) for i in (proposed_keyword.entity_type or []))
    # ), 'general')

    logger.debug(f'proposed_keyword = {proposed_keyword}')

    if redis.is_known_genre(proposed_keyword.text):
        logger.debug(f"is known genre = {proposed_keyword}")
        return entity.GenreItem.from_regex_match(
            proposed_keyword.text,
            proposed_keyword.text.lower().replace(" ", "_")
        )

    if not hasattr(proposed_keyword, 'entity_type'):
        return

    entity_type = 'general'
    for k, v in type_mapping().items():
        for i in proposed_keyword.entity_type:
            match = re.search(v, i, re.I)
            # logger.debug(f"match = ({v, i}) {match}")
            if match:
                entity_type = k

    text = proposed_keyword.text

    if entity_type == 'artist':
        return entity.ArtistItem.from_regex_match(text, text.lower().replace(" ", "_"), positivity=True)

    elif entity_type == 'instrument':
        return entity.InstrumentItem.from_regex_match(text, text.lower().replace(" ", "_"), positivity=True)

    elif entity_type == 'genre':
        return entity.GenreItem.from_regex_match(text, text.lower().replace(" ", "_"), positivity=True)

    else:
        return
