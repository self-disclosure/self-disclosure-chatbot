from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState
from response_generator.music.utils import entity


class ArtistCannedTemplatesEnter(MusicState):

    name = 'initial'

    @classmethod
    def accept_transition(self, tracker: Tracker, artist: entity.ArtistItem) -> bool:
        return False

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.FavArtistEnter.name, jump=True)]

