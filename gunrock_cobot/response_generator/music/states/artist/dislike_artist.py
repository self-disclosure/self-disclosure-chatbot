from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState
from response_generator.music.utils import entity


selector_root = 'dislike_artist'


class DislikeArtistPropose(MusicState):

    name = 'dislike_artist_propose'

    @classmethod
    def require_entity(cls, tracker: Tracker, artist: entity.ArtistItem):
        tracker.one_turn_store['artist_to_discuss'] = artist

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(
            f"{selector_root}/propose/default",
            dict(artist=self.artist_to_discuss.noun if self.artist_to_discuss else "the artist")
        )
        states.DislikeArtistRespond.require_entity(dispatcher, tracker, self.artist_to_discuss)
        return [NextState(states.DislikeArtistRespond.name)]


class DislikeArtistRespond(MusicState):

    name = 'dislike_artist_respond'

    @classmethod
    def require_entity(cls, dispatcher: Dispatcher, tracker: Tracker, artist: entity.ArtistItem):
        dispatcher.expect_opinion(True)
        tracker.one_turn_store['artist_to_discuss'] = artist

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        def handle_question():
            def ask_back_handler(qr):
                dispatcher.respond(
                    "I don't dislike any artists in particular."
                )
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=ask_back_handler,
                    follow_up_handler=lambda qr: None,
                )

        if self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
            dispatcher.respond_template(f"{selector_root}/respond/default_sys_ack", {})
        else:
            dispatcher.respond_template(f"{selector_root}/respond/default", {})

        qr_events = handle_question()
        if qr_events:
            return qr_events

        """exiting"""
        if states.FavArtistAnotherPropose.accept_transition(tracker):
            states.FavArtistAnotherPropose.require_entity(tracker, self.artist_to_discuss)
            return [NextState(states.FavArtistAnotherPropose.name, jump=True)]
        elif states.FavArtistEnter.accept_transition(tracker):
            return [NextState(states.FavArtistEnter.name, jump=True)]
        else:
            return [NextState(states.SwitchSubtopicEnter.name, jump=True)]
