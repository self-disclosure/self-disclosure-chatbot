import random
from typing import Optional

from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState
from response_generator.music.utils import entity, user_profile


selector_root = 'my_fav_artist'

artist_choices = [
    'billie_eilish',
    'harry_styles',
    'taylor_swift',
]

artist_choices_map = {
    'billie_eilish': "billie eilish",
    'harry_styles': "harry styles",
    'taylor_swift': "taylor swift",
}


def artist_in_fav_list(artist: entity.ArtistItem) -> Optional[str]:
    for (canonical, noun) in artist_choices_map.items():
        if artist.is_equal_to_name_str(noun):
            return canonical
    return None


def respond_list_my_favorite(genre: Optional[entity.GenreItem]):
    if genre and genre.noun not in {'pop'}:
        return (
            f"I'm not sure, I'm not that familiar with {genre.noun} artists."
        )
    else:
        return (
            "Oh I have several, like Billie Elish, Harry Styles, Taylor Swift and so on."
        )


class MyFavArtistPropose(MusicState):

    name = 'my_fav_artist_propose'

    @classmethod
    def accept_transition(
        cls, tracker: Tracker, artist: Optional[entity.ArtistItem], user_profile: user_profile.UserProfile
    ):
        history = user_profile.my_fav_artist_history
        remaining = len([a for a in artist_choices if a not in history])
        propose_counter = user_profile.my_fav_artist_propose_counter
        # if propose_counter >= 1:
        #     user_profile.my_fav_artist_propose_counter = 0

        return (
            propose_counter < 1 and
            (
                remaining > 0 or
                (artist and artist_in_fav_list(artist) and artist_in_fav_list(artist) not in history)
            )
        )

    @classmethod
    def require_entity(cls, tracker: Tracker, artist: Optional[entity.ArtistItem]):
        if artist:
            tracker.one_turn_store['artist_to_discuss'] = artist

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        artist = self.artist_to_discuss
        history = self.user_profile.my_fav_artist_history
        self.user_profile.my_fav_artist_propose_counter = self.user_profile.my_fav_artist_propose_counter + 1

        self.logger.debug(f"artist = {artist}, history = {history}")

        if (artist and artist_in_fav_list(artist) and artist_in_fav_list(artist) not in history):
            self.user_profile.my_fav_artist_history_append(artist)
            dispatcher.respond_template(f"{selector_root}/propose/common", dict(artist=artist.noun))
            artist_choice = entity.ArtistItem.from_regex_match(
                noun=artist.noun, canonical=artist_in_fav_list(artist)
            )
            is_proposed_by_user = True

        else:
            artist_choice = random.choice([a for a in artist_choices if a not in history])
            """convert it to an artistitem"""
            artist_choice = entity.ArtistItem.from_regex_match(
                noun=artist_choices_map[artist_choice], canonical=artist_choice)
            self.user_profile.my_fav_artist_history_append(artist)
            is_proposed_by_user = False

            if history:
                dispatcher.respond_template(f"{selector_root}/propose/default_another/{artist_choice.canonical}", {})
            else:
                dispatcher.respond_template(f"{selector_root}/propose/default/{artist_choice.canonical}", {})

        states.MyFavArtistReasonPropose.require_entity(tracker, artist_choice, is_proposed_by_user)
        return [NextState(states.MyFavArtistReasonPropose.name, jump=True)]


class MyFavArtistTriviaPropose(MusicState):

    name = 'my_fav_artist_trivia_propose'

    @classmethod
    def require_entity(cls, tracker: Tracker, artist_choice: str):
        tracker.one_turn_store['my_fav_artist_choice'] = artist_choice

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.SwitchSubtopicEnter.name)]


class MyFavArtistReasonPropose(MusicState):

    name = 'my_fav_artist_reason_propose'

    @classmethod
    def require_entity(cls, tracker: Tracker, artist_choice: entity.ArtistItem, is_proposed_by_user: bool):
        tracker.one_turn_store['my_fav_artist_choice'] = artist_choice
        tracker.one_turn_store['my_fav_artist_is_proposed_by_user'] = is_proposed_by_user

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        artist_choice: entity.ArtistItem = (
            tracker.volatile_store.get('my_fav_artist_choice') or
            tracker.one_turn_store.get('my_fav_artist_choice') or
            tracker.last_utt_store.get('my_fav_artist_choice')
        )
        is_proposed_by_user = (
            tracker.volatile_store.get('my_fav_artist_is_proposed_by_user') or
            tracker.one_turn_store.get('my_fav_artist_is_proposed_by_user') or
            tracker.last_utt_store.get('my_fav_artist_is_proposed_by_user') or
            False
        )

        if not (
            artist_choice and artist_choice.canonical and
            artist_choice.canonical in artist_choices_map and
            dispatcher.tm.has_selector(f"{selector_root}/reason/propose/{artist_choice.canonical}")
        ):
            """error handling"""
            return [NextState(states.ErrorHandlingEnter.name, jump=True)]

        dispatcher.respond_template(f"{selector_root}/reason/propose/{artist_choice.canonical}", {})

        """
        exiting
        """
        if (
            is_proposed_by_user and
            states.WhyLikeArtistPropose.accept_transition(tracker, artist_choice, self.user_profile)
        ):
            states.WhyLikeArtistPropose.require_entity(tracker, artist_choice)
            return [NextState(states.WhyLikeArtistPropose.name, jump=True)]

        elif states.DoYouLikeArtistPropose.accept_transition(tracker, artist_choice, self.user_profile):
            states.DoYouLikeArtistPropose.require_entity(tracker, artist_choice)
            return [NextState(states.DoYouLikeArtistPropose.name, jump=True)]

        elif states.FavArtistAnotherPropose.accept_transition(tracker):
            states.FavArtistAnotherPropose.require_entity(tracker, artist_choice)
            return [NextState(states.FavArtistAnotherPropose.name, jump=True)]

        elif states.FavArtistEnter.accept_transition(tracker):
            return [NextState(states.FavArtistEnter.name, jump=True)]

        else:
            return [NextState(states.SwitchSubtopicEnter.name)]
