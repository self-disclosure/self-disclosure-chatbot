from nlu.constants import DialogAct, Positivity
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState
from response_generator.music.utils import entity, user_profile


selector_root = 'why_like_artist'


class WhyLikeArtistPropose(MusicState):

    name = 'why_like_artist_propose'

    @classmethod
    def require_entity(cls, tracker: Tracker, artist: entity.ArtistItem):
        tracker.one_turn_store['artist_to_discuss'] = artist

    @classmethod
    def accept_transition(cls, tracker: Tracker, artist: entity.ArtistItem, user_profile: user_profile.UserProfile):
        return (
            artist not in user_profile.why_like_artist_history
        )

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(
            f"{selector_root}/propose/default",
            dict(artist=self.artist_to_discuss.noun if self.artist_to_discuss else "the artist")
        )
        states.WhyLikeArtistRespond.require_entity(dispatcher, tracker, self.artist_to_discuss)
        return [NextState(states.WhyLikeArtistRespond.name)]


class WhyLikeArtistRespond(MusicState):

    name = 'why_like_artist_respond'

    @classmethod
    def require_entity(cls, dispatcher: Dispatcher, tracker: Tracker, artist: entity.ArtistItem):
        dispatcher.expect_opinion(True)
        tracker.one_turn_store['artist_to_discuss'] = artist

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        artist = self.artist_to_discuss
        song: entity.SongItem = next(iter(entity.SongItem.detect(tracker)), None)

        """question"""
        def handle_question():
            def ask_back_handler(qr):
                dispatcher.respond("Personally, I like all kinds of artist.")
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=ask_back_handler,
                    follow_up_handler=lambda qr: None,
                )

        if not artist:
            """
            error handling
            """
            dispatcher.respond_template('I see.')
            qr_events = handle_question()
            if qr_events:
                return qr_events
            if states.FavArtistAnotherPropose.accept_transition(tracker):
                states.FavArtistAnotherPropose.require_entity(tracker, artist)
                return [NextState(states.FavArtistAnotherPropose.name, jump=True)]
            else:
                return [NextState(states.ErrorHandlingEnter.name, jump=True)]

        self.user_profile.why_like_artist_history_append(artist)

        if (
            self.handler.has_question_only(tracker)
        ):
            qr_events = handle_question()
            if qr_events:
                return qr_events

        elif song and artist.is_equal_to_name_str(song.artist_name):
            dispatcher.respond_template(
                f"{selector_root}/respond/song_entity",
                dict(song=song.noun)
            )
            qr_events = handle_question()
            if qr_events:
                return qr_events
            if states.FavSongByArtistAnotherPropose.accept_transition(tracker):
                return [NextState(states.FavSongByArtistAnotherPropose.name, jump=True)]
            elif states.FavArtistAnotherPropose.accept_transition(tracker):
                states.FavArtistAnotherPropose.require_entity(tracker, artist)
                return [NextState(states.FavArtistAnotherPropose.name, jump=True)]
            else:
                return [NextState(states.SwitchSubtopicEnter.name, jump=True)]

        elif tracker.returnnlp.has_dialog_act(DialogAct.OTHER_ANSWERS):
            dispatcher.respond_template(f"{selector_root}/respond/dunno", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events

        elif tracker.returnnlp.opinion_positivity is Positivity.pos:
            dispatcher.respond_template(f"{selector_root}/respond/pos", dict(artist=artist.noun))
            qr_events = handle_question()
            if qr_events:
                return qr_events

        else:
            if self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond_template(f"{selector_root}/respond/default", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events

        """
        exiting
        """

        states.FavSongByArtistPropose.require_entity(tracker, artist)
        return [NextState(states.FavSongByArtistPropose.name, jump=True)]
