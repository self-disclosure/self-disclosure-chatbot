import re
from typing import Optional

from nlu.constants import DialogAct, Positivity
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState
from response_generator.music.utils import entity


selector_root = "fav_artist"


class FavArtistEnter(MusicState):

    name = 'fav_artist_enter'

    @classmethod
    def accept_transition(cls, tracker: Tracker):
        counter = tracker.persistent_store.get('fav_artist_enter_counter') or 0
        flag = tracker.persistent_store.get('from_fav_genre') or False
        return counter < 3 or flag

    @classmethod
    def increment_counter(cls, tracker: Tracker) -> int:
        flag = tracker.volatile_store.get('fav_artist_enter_counter_incremented') or False
        counter = tracker.persistent_store.get('fav_artist_enter_counter') or 0
        if not flag:
            tracker.volatile_store['fav_artist_enter_counter_incremented'] = True
            tracker.persistent_store['fav_artist_enter_counter'] = counter + 1
        return counter

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        """update counter for subtopic transition"""
        counter = tracker.persistent_store.get('fav_artist_enter_counter') or 0
        tracker.persistent_store['fav_artist_enter_counter'] = counter + 1

        pos_hist = list(filter(lambda a: a.positivity, self.user_profile.fav_artist_history))
        self.logger.debug(
            f"counter = {counter}, "
            f"fav_artist_history = {self.user_profile.fav_artist_history}"
        )

        """get flag that identify user had came from fav_genre"""
        flag = tracker.persistent_store.get('from_fav_genre') or False
        if flag:
            tracker.persistent_store['from_fav_genre'] = False
            """temp disable this so we can stay in artist"""
            # return [NextState(states.GenreEnter.name, jump=True)]

        # FIRST TIME
        if (
            counter < 1
        ):
            """if this is the first time in this subtopic"""
            return [NextState(states.FavArtistPropose.name, jump=True)]

        # SECOND TIME
        elif (
            counter < 2 and
            pos_hist and
            states.FavArtistAnotherPropose.accept_transition(tracker)
        ):
            """second time entering"""
            states.FavArtistAnotherPropose.require_entity(tracker, pos_hist[-1])
            return [NextState(states.FavArtistAnotherPropose.name, jump=True)]

        elif (
            counter < 2 and
            states.FavArtistAnotherPropose.accept_transition(tracker)
        ):
            """second time entering, but without artist"""
            states.FavArtistAnotherPropose.require_entity(tracker, None)
            return [NextState(states.FavArtistAnotherPropose.name, jump=True)]

        elif (
            counter < 3 and
            states.MyFavArtistPropose.accept_transition(tracker, None, self.user_profile)
        ):
            """if we can't ask for another of their artists, disclose our favorite artist"""
            states.MyFavArtistPropose.require_entity(tracker, None)
            return [NextState(states.MyFavArtistPropose.name, jump=True)]

        # DEFAULT
        else:
            return [NextState(states.SwitchSubtopicEnter.name, jump=True)]


class FavArtistPropose(MusicState):

    name = 'fav_artist_propose'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/propose/default", {})

        """update counter to limit home many times proposing fav_artist"""
        # counter = tracker.persistent_store.get('fav_artist_propose_counter') or 0
        # tracker.persistent_store['fav_artist_propose_counter'] = counter + 1
        states.FavArtistEnter.increment_counter(tracker)

        return [NextState(states.FavArtistRespond.name)]


class FavArtistAnotherPropose(MusicState):

    name = 'fav_artist_another_propose'

    @classmethod
    def accept_transition(cls, tracker: Tracker):
        counter = tracker.persistent_store.get('fav_artist_propose_counter') or 0
        return counter < 2

    @classmethod
    def require_entity(cls, tracker: Tracker, artist: Optional[entity.ArtistItem]):
        tracker.one_turn_store['artist_to_discuss'] = artist

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        if self.artist_to_discuss and self.artist_to_discuss.positivity:
            dispatcher.respond_template(
                f"{selector_root}/propose/another_with_artist", dict(artist=self.artist_to_discuss.noun))

        elif self.artist_to_discuss and not self.artist_to_discuss.positivity:
            dispatcher.respond_template(
                f"{selector_root}/propose/another_prev_negative_with_artist",
                dict(artist=self.artist_to_discuss.noun))

        else:
            dispatcher.respond_template(f"{selector_root}/propose/another", {})

        # counter = tracker.persistent_store.get('fav_artist_propose_counter') or 0
        # tracker.persistent_store['fav_artist_propose_counter'] = counter + 1
        states.FavArtistEnter.increment_counter(tracker)
        return [NextState(states.FavArtistRespond.name)]


class FavArtistRespond(MusicState):

    name = 'fav_artist_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        artists = entity.ArtistItem.detect(tracker, include_negative_positivity=True)
        artist: entity.ArtistItem = (
            next(filter(lambda artist: artist.positivity, artists), None) or
            next(filter(lambda artist: not artist.positivity, artists), None)
        )
        genre: entity.GenreItem = next(iter(entity.GenreItem.detect(tracker)), None)

        self.logger.debug(f"entities = {artists}, {genre}")
        self.user_profile.fav_artist_history_append(artists)
        states.FavArtistEnter.increment_counter(tracker)

        """
        prepare question handling
        """
        def handle_question(user_artist: entity.ArtistItem = None):
            def ask_back_handler(qr):
                if user_artist and user_artist.positivity:
                    dispatcher.respond(f"I like {user_artist.noun} too!")
                else:
                    dispatcher.respond(states.respond_list_my_favorite(genre))

            def is_ask_back_question():
                return (
                    self.handler.question_detector.is_ask_back_question() or
                    re.search(r"\b(who do you like|who('s| is) your favorite)\b", tracker.input_text)
                )

            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=ask_back_handler,
                    follow_up_handler=lambda qr: None,
                    is_follow_up_question=is_ask_back_question,
                )

        """counter for yes_only answer"""
        counter = tracker.persistent_store.get('fav_artist_respond_yes_only_counter') or 0

        if (
            self.handler.has_question_only(tracker)
        ):
            qr_events = handle_question()
            if qr_events:
                return qr_events

        elif artist and artist.positivity:
            """
            if user likes a specific artist
            """
            self.logger.debug(f"detected artist: {artist}")

            if (
                states.artist_in_fav_list(artist) and
                states.MyFavArtistPropose.accept_transition(tracker, artist, self.user_profile)
            ):
                """if it's one of our favorites"""
                states.MyFavArtistPropose.require_entity(tracker, artist)
                return [NextState(states.MyFavArtistPropose.name, jump=True)]

            # first check if we have trivia about this artist
            trivia, song = None, None
            if artist.artist_details(tracker):
                pass
                # self.logger.debug(
                #     f"favmusician detected artist db lookup: {artist.noun}, {artist.artist_details(tracker)}")
                # artist_details = artist.artist_details(tracker)
                # self.logger.debug(f"artist used: {artist}")

                """
                disabling now coz it's not completed
                """
                # if artist_details.trivias:
                #     self.logger.debug(f"favmusician artist_dict has 'trivias': {artist_details.trivias}")
                #     trivia_history = tracker.persistent_store.get('artist_trivia_history') or []
                #     trivias = [
                #         t for t in artist_details.trivias
                #         if hash(f"{artist.noun} {t}") not in trivia_history]
                #     if trivias:
                #         trivia = random.choice(trivias)
                #         trivia_hash = hash(f"{artist.noun} {trivia}")
                #         tracker.persistent_store['artist_trivia_history'] = [*trivia_history, trivia_hash]
                #         if not trivia.strip().endswith('.'):
                #             trivia += '.'

                """
                disabling song for now as to keep it on artist
                """
                # if artist_details.songs:
                #     song_history = tracker.persistent_store.get('artist_song_name_drop_history') or []
                #     songs = [
                #         s for s in artist_details.songs
                #         if hash(f"{artist.noun} {s}") not in song_history
                #     ]
                #     if songs:
                #         song = random.choice(songs)
                #         song_hash = hash(f"{artist.noun} {song}")
                #         tracker.persistent_store['artist_song_name_drop_history'] = [*song_history, song_hash]

            self.logger.debug(f"trivia = {trivia}; song = {song}")

            # if trivia:
            #     states.ArtistTriviaPropose.require_entity(tracker, artist, trivia)
            #     return [NextState(states.ArtistTriviaPropose.name, jump=True)]

            # elif song:
            #     self.logger.debug(f"favmusician speak artist with album: {artist}, {song}")
            #     dispatcher.respond_template(
            #         'artist_question/follow_album', {'artist': artist.noun.replace(".", " "), 'album': song})
            #     if self.is_ask_back_response(tracker):
            #         self.ask_back_response(dispatcher)
            #     tracker.one_turn_store['artist_to_discuss'] = artist
            #     return [NextState(states.SongNameDropRespond.name)]

            if states.ArtistCannedTemplatesEnter.accept_transition(tracker, artist):
                return [NextState(states.ArtistCannedTemplatesEnter.name, jump=True)]

            else:
                self.logger.debug(f"favmusician speak artist without album: {artist}")
                """
                ack user like a specific artist
                """
                dispatcher.respond_template(f"{selector_root}/respond/ack_pos", {})
                qr_events = handle_question(artist)
                if qr_events:
                    return qr_events

                why_like_artist_counter = tracker.persistent_store.get('fav_artist_why_like_artist_counter') or 0
                if why_like_artist_counter >= 1:
                    tracker.persistent_store['fav_artist_why_like_artist_counter'] = 0

                if (
                    why_like_artist_counter < 1 and
                    states.WhyLikeArtistPropose.accept_transition(tracker, artist, self.user_profile)
                ):
                    tracker.persistent_store['fav_artist_why_like_artist_counter'] = why_like_artist_counter + 1
                    states.WhyLikeArtistPropose.require_entity(tracker, artist)
                    return [NextState(states.WhyLikeArtistPropose.name, jump=True)]
                else:
                    states.FavSongByArtistPropose.require_entity(tracker, artist)
                    return [NextState(states.FavSongByArtistPropose.name, jump=True)]

        elif artist and not artist.positivity:
            dispatcher.respond_template(f"{selector_root}/respond/ack_neg", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events
            if (
                self.module.state_tracker.prev_turn(-1) and
                self.module.state_tracker.prev_turn(-1)[-1] in {states.DislikeArtistPropose.name}
            ):
                """don't go there again if we just came from there"""
                if (
                    states.MyFavArtistPropose.accept_transition(tracker, artist, self.user_profile)
                ):
                    states.MyFavArtistPropose.require_entity(tracker, artist)
                    return [NextState(states.MyFavArtistPropose.name, jump=True)]
                else:
                    return [NextState(states.SwitchSubtopicEnter.name, jump=True)]
            else:
                states.DislikeArtistPropose.require_entity(tracker, artist)
                return [NextState(states.DislikeArtistPropose.name, jump=True)]

        elif tracker.returnnlp.has_dialog_act(DialogAct.OTHER_ANSWERS):
            dispatcher.respond_template(f"{selector_root}/respond/dunno", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events
            self.user_profile.fav_artist_idk_flag = True

        elif (
            re.search(
                r'several|many|every artist|multiple| a lot of favorites|like them all',
                tracker.input_text) or
            tracker.returnnlp.answer_positivity is Positivity.neg
        ):
            dispatcher.respond_template(f"{selector_root}/respond/many", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events

        elif (
            re.search(r"\b(yes|yea|yeah|yup)\b", tracker.input_text) and counter < 1
        ):
            tracker.persistent_store['fav_artist_respond_yes_only_counter'] = counter + 1
            dispatcher.respond_template(f"{selector_root}/respond/yes_only", {})

            return [NextState(self.name)]

        else:
            if self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond_template(f"{selector_root}/respond/default", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events

        """exiting"""
        if states.MyFavArtistPropose.accept_transition(tracker, None, self.user_profile):
            return [NextState(states.MyFavArtistPropose.name, jump=True)]
        else:
            return [NextState(states.SwitchSubtopicEnter.name, jump=True)]
