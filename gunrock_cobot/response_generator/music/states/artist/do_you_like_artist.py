import re

from nlu.constants import DialogAct, Positivity
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState
from response_generator.music.utils import entity, user_profile


selector_root = 'do_you_like_artist'


class DoYouLikeArtistPropose(MusicState):

    name = 'do_you_like_artist_propose'

    @classmethod
    def accept_transition(cls, tracker: Tracker, artist: entity.ArtistItem, user_profile: user_profile.UserProfile):
        return (
            artist not in user_profile.fav_artist_history
        )

    @classmethod
    def require_entity(cls, tracker: Tracker, artist: entity.ArtistItem):
        tracker.one_turn_store['artist_to_discuss'] = artist

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(
            f"{selector_root}/propose/default",
            dict(artist=self.artist_to_discuss.noun if self.artist_to_discuss else "the artist")
        )
        states.DoYouLikeArtistRespond.require_entity(tracker, self.artist_to_discuss)
        return [NextState(states.DoYouLikeArtistRespond.name)]


class DoYouLikeArtistRespond(MusicState):

    name = 'do_you_like_artist_respond'

    @classmethod
    def require_entity(cls, tracker: Tracker, artist: entity.ArtistItem):
        tracker.one_turn_store['artist_to_discuss'] = artist

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        artist = next(iter(entity.ArtistItem.detect(tracker)), None)

        """question"""
        def handle_question():
            def ask_back_handler(qr):
                dispatcher.respond_template(
                    f"{selector_root}/respond/self_disclose",
                    dict(artist=(
                        artist.noun if artist else self.artist_to_discuss.noun if self.artist_to_discuss else "them")))

            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=ask_back_handler,
                    follow_up_handler=lambda qr: None,
                )

        if re.search((
            r"(who (is|are) (he|she|they|this|that|.*))|"
            r"(do('t| not) know who (he|she|they|this|that|.*) (is|are))|"
            r"((never|have(n't| not)) heard of (him|her|them|.*))"
        ), tracker.input_text):
            """if user haven't heard of the artist; different from saying dunno if they're a fan"""
            dispatcher.respond_template(f"{selector_root}/respond/dunno", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events

        elif artist:
            """if user respond a positive artist"""
            qr_events = handle_question()
            if qr_events:
                return qr_events
            if states.WhyLikeArtistPropose.accept_transition(tracker, artist, self.user_profile):
                states.WhyLikeArtistPropose.require_entity(tracker, artist)
                return [NextState(states.WhyLikeArtistPropose.name, jump=True)]
            else:
                states.FavSongByArtistPropose.require_entity(tracker, artist)
                return [NextState(states.FavSongByArtistPropose.name, jump=True)]

        elif tracker.returnnlp.answer_positivity in {Positivity.pos, Positivity.neg}:
            key = tracker.returnnlp.answer_positivity.name
            if (
                any(s in {
                    states.MyFavArtistPropose.name,
                    states.MyFavArtistTriviaPropose.name,
                    states.MyFavArtistReasonPropose
                } for s in self.module.state_tracker.prev_turn() or [])
            ):
                key = f'{key}_my_fav_artist'

            dispatcher.respond_template(f"{selector_root}/respond/{key}", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events

        elif tracker.returnnlp.has_dialog_act(DialogAct.OTHER_ANSWERS):
            dispatcher.respond_template(f"{selector_root}/respond/dunno", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events

        elif tracker.returnnlp.opinion_positivity in {Positivity.pos, Positivity.neg}:
            key = tracker.returnnlp.opinion_positivity.name
            if (
                any(s in {
                    states.MyFavArtistPropose.name,
                    states.MyFavArtistTriviaPropose.name,
                    states.MyFavArtistReasonPropose
                } for s in self.module.state_tracker.prev_turn() or [])
            ):
                key = f'{key}_opinion'

            dispatcher.respond_template(f"{selector_root}/respond/{key}", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events
        else:
            if self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond("I see.")

            qr_events = handle_question()
            if qr_events:
                return qr_events

        """
        exiting
        """

        if (
            not any(i in {states.MyFavArtistPropose.name} for i in (self.module.state_tracker.prev_turn() or [])) and
            states.WhyLikeArtistPropose.accept_transition(
                tracker, artist if artist else self.artist_to_discuss,
                self.user_profile)
        ):
            states.WhyLikeArtistPropose.require_entity(tracker, artist if artist else self.artist_to_discuss)
            return [NextState(states.WhyLikeArtistPropose.name, jump=True)]

        if states.FavArtistEnter.accept_transition(tracker):
            if any(i in {states.MyFavArtistPropose.name} for i in (self.module.state_tracker.prev_turn() or [])):
                """
                if from my_fav_artist, disable self.artist_to_discuss since detection of whether user likes
                the artist is not that good right now
                """
                tracker.one_turn_store['artist_to_discuss'] = None
                tracker.last_utt_store['artist_to_discuss'] = None

            return [NextState(states.FavArtistEnter.name, jump=True)]
        else:
            return [NextState(states.SwitchSubtopicEnter.name, jump=True)]
