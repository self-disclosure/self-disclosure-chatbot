from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState
from response_generator.music.utils import entity, legacy


selector_root = 'artist_trivia'


class ArtistTriviaPropose(MusicState):

    name = 'artist_trivia_propose'

    @classmethod
    def require_entity(cls, tracker: Tracker, artist: entity.ArtistItem, trivia: str):
        tracker.one_turn_store['artist_to_discuss'] = artist
        tracker.one_turn_store['artist_trivia'] = trivia

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        artist = self.artist_to_discuss
        trivia = (
            tracker.one_turn_store.get('artist_trivia') or
            tracker.last_utt_store.get('artist_trivia')
        )
        if not (artist and trivia):
            self.logger.warning(f"missing value in tracker store! artist = {artist}, trivia = {trivia}")
            return [NextState(states.ErrorHandlingEnter.name, jump=True)]

        dispatcher.respond_template(
            f"{selector_root}/propose/default",
            dict(artist=artist.noun, fact=legacy.replace_pronoun_in_artist_trivia(artist.noun, trivia))
        )

        states.ArtistTriviaRespond.require_entity(tracker, artist, trivia)
        return [NextState(states.ArtistTriviaRespond.name)]


class ArtistTriviaRespond(MusicState):

    name = 'artist_trivia_respond'

    @classmethod
    def require_entity(cls, tracker: Tracker, artist: entity.ArtistItem, trivia: str):
        tracker.one_turn_store['artist_to_discuss'] = artist
        tracker.one_turn_store['artist_trivia'] = trivia

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        """exiting"""
        if states.FavArtistEnter.accept_transition(tracker):
            return [NextState(states.FavArtistEnter.name, jump=True)]
        else:
            return [NextState(states.SwitchSubtopicEnter.name, jump=True)]
