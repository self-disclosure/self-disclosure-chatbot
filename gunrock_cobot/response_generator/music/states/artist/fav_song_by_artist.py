import itertools
import re

from nlu.constants import DialogAct
from response_generator.fsm2 import Dispatcher, Tracker, NextState
from response_generator.fsm2.utils import state_tracker

from response_generator.music import states
from response_generator.music.states.base import MusicState
from response_generator.music.utils import entity


selector_root = 'fav_song_by_artist'


class FavSongByArtistPropose(MusicState):

    name = 'fav_song_by_artist_propose'

    @classmethod
    def accept_transition(cls, tracker: Tracker, artist: entity.ArtistItem, state_tracker: state_tracker.StateTracker):
        if any(
            i in {cls.name} for i in itertools.chain(
                state_tracker.curr_turn(),
                state_tracker.prev_turn(),
                state_tracker.prev_turn(-1),
            )
        ):
            return False

    @classmethod
    def require_entity(cls, tracker: Tracker, artist: entity.ArtistItem):
        tracker.one_turn_store['artist_to_discuss'] = artist

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        artist = self.artist_to_discuss

        dispatcher.respond_template(
            f"{selector_root}/propose/default",
            dict(artist=artist.noun if artist else "the artist")
        )
        states.FavSongByArtistRespond.require_entity(tracker, artist)
        return [NextState(states.FavSongByArtistRespond.name)]


class FavSongByArtistAnotherPropose(MusicState):

    name = 'fav_song_by_artist_another_propose'

    @classmethod
    def accept_transition(self, tracker: Tracker):
        return False

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.FavSongByArtistRespond.name)]


class FavSongByArtistRespond(MusicState):

    name = 'fav_song_by_artist_respond'

    @classmethod
    def require_entity(cls, tracker: Tracker, artist: entity.ArtistItem):
        tracker.one_turn_store['artist_to_discuss'] = artist

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        song: entity.SongItem = next(iter(entity.SongItem.detect(tracker)), None)
        self.logger.debug(
            f"entities: song = {song}"
        )

        """question"""
        def handle_question():
            def ask_back_handler(qr):
                dispatcher.respond_template(
                    f"{selector_root}/respond/self_disclose",
                    dict(artist=self.artist_to_discuss.noun if self.artist_to_discuss else "them"))

            def do_you_like_handler(qr):
                ask_back_handler(qr)

            def is_do_you_like_question():
                return (
                    re.search(r"^(who|what|which) do you (like|love)$", tracker.returnnlp[-1].text)
                )

            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=ask_back_handler,
                    follow_up_handler=lambda qr: None,
                    do_you_like_handler=do_you_like_handler,
                    is_do_you_like_question=is_do_you_like_question,
                )

        if self.handler.has_question_only(tracker):
            qr_events = handle_question()
            if qr_events:
                return qr_events

        elif not self.artist_to_discuss:
            """error handling"""
            dispatcher.respond_template('I see.')
            qr_events = handle_question()
            if qr_events:
                return qr_events

            # if states.FavArtistAnotherPropose.accept_transition(tracker):
            #     return [NextState(states.FavArtistAnotherPropose.name, jump=True)]
            if states.FavArtistEnter.accept_transition(tracker):
                return [NextState(states.FavArtistEnter.name, jump=True)]
            else:
                return [NextState(states.ErrorHandlingEnter.name, jump=True)]

        # if song and self.artist_to_discuss.is_equal_to_name_str(song.artist_name):
        elif song:
            """if user answers normally"""
            # TODO: right now any song will do, until we can make the other path
            dispatcher.respond_template(
                f"{selector_root}/respond/song_entity",
                dict(song=song.noun)
            )
            qr_events = handle_question()
            if qr_events:
                return qr_events
            # if states.FavSongByArtistAnotherPropose.accept_transition(tracker):
            #     return [NextState(states.FavSongByArtistAnotherPropose.name, jump=True)]
            if states.FavArtistEnter.accept_transition(tracker):
                return [NextState(states.FavArtistEnter.name, jump=True)]
            else:
                return [NextState(states.SwitchSubtopicEnter.name, jump=True)]

        elif tracker.returnnlp.has_dialog_act(DialogAct.OTHER_ANSWERS):
            dispatcher.respond_template(f"{selector_root}/respond/dunno", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events

        else:
            if self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond_template(f"{selector_root}/respond/default", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events

        if states.FavArtistEnter.accept_transition(tracker):
            return [NextState(states.FavArtistEnter.name, jump=True)]
        else:
            return [NextState(states.SwitchSubtopicEnter.name, jump=True)]
