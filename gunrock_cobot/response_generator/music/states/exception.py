from typing import List, Optional

from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, NextState
from selecting_strategy import module_selection

from response_generator.music import states
from response_generator.music.states.base import MusicState


class ErrorHandlingEnter(MusicState):

    name = 'error_handling_enter'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        self.logger.warning(
            f"{self.name} triggered. "
            f"tracker = {tracker}, "
            f"states = {self.module.state_tracker}, "
        )

        """get flag that identify user had came from fav_genre"""
        flag = tracker.persistent_store.get('from_fav_genre') or False
        if flag:
            tracker.persistent_store['from_fav_genre'] = False

        return [NextState(states.SwitchSubtopicEnter.name)]


class ProposeUnclearRespond(MusicState):

    name = 'propose_unclear_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        try:
            last_topic_module = TopicModule(
                tracker.user_attributes.module_selection.last_topic_module
            )
        except ValueError:
            last_topic_module = TopicModule.NA

        def handle_question():
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                )

        if (
            self.handler.has_question_only(tracker)
        ):
            qr_events = handle_question()
            if qr_events:
                return qr_events

        elif last_topic_module is TopicModule.MUSIC:
            dispatcher.respond(f"Alright.")
            qr_events = handle_question()
            if qr_events:
                return qr_events

        else:
            qr_events = handle_question()
            if qr_events:
                return qr_events

        return [NextState(states.SwitchSubtopicEnter.name, jump=True)]


class ExitTransitionEnter(MusicState):

    name = 'exit_transition_enter'

    @classmethod
    def require_entity(cls, tracker: Tracker, with_transition: bool):
        if isinstance(with_transition, bool):
            tracker.one_turn_store['with_transition'] = with_transition
        else:
            tracker.one_turn_store['with_transition'] = True

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        with_transition = (
            tracker.one_turn_store.get('with_transition')
            if tracker.one_turn_store.get('with_transition') is not None else
            tracker.last_utt_store.get('with_transition')
            if tracker.last_utt_store.get('with_transition') is not None else
            True
        )

        """get flag that identify user had came from fav_genre"""
        flag = tracker.persistent_store.get('from_fav_genre') or False
        if flag:
            tracker.persistent_store['from_fav_genre'] = False

        if with_transition:
            dispatcher.respond_template(f"transition/exit_no_verify", {})

        module_selector = module_selection.ModuleSelector(tracker.user_attributes.ua_ref)
        module_selector.add_out_of_template_topic_module(TopicModule.MUSIC.value)
        dispatcher.propose_continue('STOP')
        return [NextState(states.Initial.name)]


class PostProcess(MusicState):

    name = 'post_process_hook'
    special_type = {'post_process'}

    def post_process(self, dispatcher: Dispatcher, tracker: Tracker):
        cond = [
            states.GenreEnter.accept_transition(tracker),
            states.FavArtistEnter.accept_transition(tracker),
            states.InstrumentEnter.accept_transition(tracker, None, self.user_profile),
            states.ChitchatEnter.accept_transition(tracker, self.module.state_tracker),
        ]
        self.logger.debug(f"checking subtopics accept_transition: {cond}")
        if all(not i for i in cond):
            self.logger.debug(f"<post_response_hook> out of accept_transitions = {cond}")
            module_selector = module_selection.ModuleSelector(tracker.user_attributes.ua_ref)
            module_selector.add_out_of_template_topic_module(TopicModule.MUSIC.value)

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.SwitchSubtopicEnter.name, jump=True)]


class SwitchSubtopicEnter(MusicState):

    name = 'switch_subtopic_enter'

    @classmethod
    def require_entity_from_resume(cls, tracker: Tracker, responses: List[str], tag: Optional[str]):
        tracker.volatile_store['resume_acks'] = responses
        tracker.volatile_store['resume_tag'] = tag

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        flag = tracker.persistent_store.get('from_fav_genre') or False
        transition_ack_flag = tracker.volatile_store.get('transition_ack_flag') or False

        resume_acks = tracker.volatile_store.get('resume_acks') or []
        resume_tag = tracker.volatile_store.get('resume_tag') or None

        self.logger.debug(f"cond = {resume_acks}, {resume_tag}")

        def transition_ack(for_subtopic: Optional[str]):
            self.logger.debug(
                f"transition_ack for {for_subtopic}, "
                f"flag = {transition_ack_flag}, "
                f"curr_turn = {self.module.state_tracker.curr_turn()}, "
            )
            if (
                not transition_ack_flag and
                not any(i in {states.ResumeRespond.name} for i in self.module.state_tracker.curr_turn())
            ):
                """first time here, also not from resume mode"""
                tracker.volatile_store['transition_ack_flag'] = True

                if (any(i in {states.Initial.name} for i in self.module.state_tracker.curr_turn())):
                    dispatcher.respond_template(f"transition/subtopic/propose/first", {})
                elif for_subtopic == 'exit':
                    dispatcher.respond(
                        "It's been nice talking about music with you, but let's talk about something else.")
                    tracker.volatile_store['with_transition'] = False
                else:
                    dispatcher.respond_template(f"transition/subtopic/propose/switching", {})
            elif (
                resume_acks
            ):
                self.logger.debug(f"resume acks = {resume_tag}, {for_subtopic}")
                if resume_tag == for_subtopic:
                    for utt in resume_acks:
                        dispatcher.respond(utt)

                elif for_subtopic == 'exit':
                    dispatcher.respond(
                        "It's been nice talking about music with you, but let's talk about something else.")
                    tracker.volatile_store['with_transition'] = False

                elif resume_tag is None and for_subtopic:
                    for utt in resume_acks:
                        dispatcher.respond(utt)
                
                elif (
                    any(i in {states.ResumeRespond.name} for i in self.module.state_tracker.curr_turn())
                ):
                    dispatcher.respond_template(f"resume/propose/default", {})

        if (
            not any(
                i in {states.GenreEnter.name} for i in self.module.state_tracker.curr_turn()
            ) and
            states.GenreEnter.accept_transition(tracker)
        ):
            transition_ack(for_subtopic='genre')
            return [NextState(states.GenreEnter.name, jump=True)]

        elif (
            not any(i in {states.GenreEnter.name} for i in self.module.state_tracker.curr_turn()) and
            self.module.state_tracker.prev_turn() and
            self.module.state_tracker.prev_turn()[-1] in {states.FavoriteGenreArtistPropose.name} and
            self.genre_to_discuss
        ):
            """if user stopped right at the genre-artist transition"""
            transition_ack(for_subtopic='genre')
            states.FavoriteGenreArtistPropose.require_entity(tracker, self.genre_to_discuss)
            return [NextState(states.FavoriteGenreArtistPropose.name, jump=True)]

        elif (
            not any(i in {states.FavArtistEnter.name} for i in self.module.state_tracker.curr_turn()) and
            states.FavArtistEnter.accept_transition(tracker)
        ):
            transition_ack(for_subtopic='artist')
            return [NextState(states.FavArtistEnter.name, jump=True)]

        elif (
            not any(
                i in {states.InstrumentEnter.name, states.DoYouPlayInstrumentRespond.name}
                for i in self.module.state_tracker.curr_turn()
            ) and
            states.InstrumentEnter.accept_transition(tracker, None, self.user_profile)
        ):

            """get flag that identify user had came from fav_genre"""
            flag = tracker.persistent_store.get('from_fav_genre') or False
            if flag:
                tracker.persistent_store['from_fav_genre'] = False

            transition_ack(for_subtopic='instrument')
            states.InstrumentEnter.require_entity(tracker, None)
            return [NextState(states.InstrumentEnter.name, jump=True)]

        elif (
            not any(i in {states.ChitchatEnter.name} for i in self.module.state_tracker.curr_turn()) and
            states.ChitchatEnter.accept_transition(tracker, self.module.state_tracker)
        ):
            """get flag that identify user had came from fav_genre"""
            flag = tracker.persistent_store.get('from_fav_genre') or False
            if flag:
                tracker.persistent_store['from_fav_genre'] = False

            transition_ack(for_subtopic='chitchat')
            return [NextState(states.ChitchatEnter.name, jump=True)]

        else:
            transition_ack(for_subtopic='exit')
            states.ExitTransitionEnter.require_entity(
                tracker,
                False if tracker.volatile_store.get('with_transition') is False else True
            )
            return [NextState(states.ExitTransitionEnter.name, jump=True)]
