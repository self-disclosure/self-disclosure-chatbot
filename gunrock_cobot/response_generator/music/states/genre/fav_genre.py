import re

from nlu.constants import DialogAct
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState

from response_generator.music.utils import entity, regex_match


selector_root = "fav_genre"


class FavGenrePropose(MusicState):

    name = 'fav_genre_propose'

    @classmethod
    def accept_transition(cls, tracker: Tracker):
        counter = tracker.persistent_store.get('fav_genre_counter') or 0
        return counter < 1

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/propose", {})
        return [NextState(states.FavGenreRespond.name)]


class FavGenreRespond(MusicState):

    name = 'fav_genre_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        genres = entity.GenreItem.detect(tracker, include_negative_positivity=True)
        genre = (
            next(filter(lambda genre: genre.positivity, genres), None) or
            next(filter(lambda genre: not genre.positivity, genres), None)
        )
        artist = next(iter(entity.ArtistItem.detect(tracker, include_negative_positivity=True)), None)
        song = next(iter(entity.SongItem.detect(tracker)), None)
        fav_genre_verify_song = (
            tracker.one_turn_store.get('fav_genre_verify_song') or
            tracker.last_utt_store.get('fav_genre_verify_song') or
            0
        )
        fav_genre_verify_genre = (
            tracker.volatile_store.get('fav_genre_verify_genre') or
            tracker.last_utt_store.get('fav_genre_verify_genre') or
            0
        )
        counter = tracker.persistent_store.get('fav_genre_counter') or 0
        tracker.persistent_store['fav_genre_counter'] = counter + 1

        """
        prepare question handling
        """
        def handle_question(user_genre: entity.ArtistItem = None, skip_ask_back=False):
            def ask_back_handler(qr):
                if user_genre and user_genre.positivity:
                    dispatcher.respond_template(
                        f"{selector_root}/respond/self_disclose/given_genre", dict(genre=user_genre.noun))
                elif not skip_ask_back:
                    dispatcher.respond_template(
                        f"{selector_root}/respond/self_disclose/all", dict())

            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=ask_back_handler,
                    follow_up_handler=lambda qr: None,
                )

        """
        main
        """
        if (
            self.handler.has_question_only(tracker)
        ):
            qr_events = handle_question(genre)
            if qr_events:
                return qr_events
            return [NextState(states.GenreEnter.name, jump=True)]

        elif genre and genre.positivity:
            self.user_profile.fav_genre_history_append(genre)
            if 'pop' in genre.noun:
                dispatcher.respond_template(f"{selector_root}/respond/pop", {})
            else:
                dispatcher.respond_template(f"{selector_root}/respond/default_genre", dict(genre=genre.noun))

            qr_events = handle_question(genre)
            if qr_events:
                return qr_events

            states.FavoriteGenreArtistPropose.require_entity(tracker, genre)
            return [NextState(states.FavoriteGenreArtistPropose.name, jump=True)]

        elif genre and not genre.positivity:
            self.user_profile.fav_genre_history_append(genre)
            dispatcher.respond_template(f"{selector_root}/respond/negative_genre", {})

            qr_events = handle_question(genre)
            if qr_events:
                return qr_events

            states.DislikeGenrePropose.require_entity(tracker, genre)
            return [NextState(states.DislikeGenrePropose.name, jump=True)]

        elif (
            (
                song or
                re.search(r"my favorite song is", tracker.input_text)
            ) and
            fav_genre_verify_song < 1
        ):
            """handler if user say a song name instead"""
            # temp reroute to self with ack
            tracker.one_turn_store['fav_genre_verify_song'] = fav_genre_verify_song + 1
            if song:
                dispatcher.respond_template(f"{selector_root}/respond/verify_song/with_ner", dict(song_name=song.noun))
            else:
                dispatcher.respond_template(f"{selector_root}/respond/verify_song/no_ner", {})
            return [NextState(self.name)]

        elif (
            tracker.returnnlp.has_dialog_act({DialogAct.OTHER_ANSWERS}) or
            re.search(r"\b(no idea|do(n't| not) have a? ?(favorite|preference))\b", tracker.input_text)
        ):
            self.user_profile.fav_genre_idk_flag = True
            dispatcher.respond_template(f"{selector_root}/respond/dunno", {})

            qr_events = handle_question(genre)
            if qr_events:
                return qr_events

            return [NextState(states.GenreEnter.name, jump=True)]

        elif regex_match.is_saying_all_of_them(tracker):
            dispatcher.respond_template(f"{selector_root}/respond/all_of_them", {})
            qr_events = handle_question(genre)
            if qr_events:
                return qr_events
            return [NextState(states.GenreEnter.name, jump=True)]

        elif artist:
            return [NextState(states.FavArtistRespond.name, jump=True)]

        elif (
            re.search(r"\b(whatever you like)\b", tracker.input_text)
        ):
            dispatcher.respond_template(
                f"{selector_root}/respond/self_disclose/all", dict())
            qr_events = handle_question(genre, skip_ask_back=True)
            if qr_events:
                return qr_events
            return [NextState(states.GenreEnter.name, jump=True)]

        elif (
            fav_genre_verify_genre < 1 and
            not self.handler.has_question(tracker)
        ):
            tracker.one_turn_store['fav_genre_verify_genre'] = fav_genre_verify_genre + 1

            dispatcher.respond_template(f"{selector_root}/respond/verify_genre/default", {})
            return [NextState(self.name)]

        elif (
            self.sys_ack_utt
        ):
            dispatcher.respond(self.sys_ack_utt)
            qr_events = handle_question(genre)
            if qr_events:
                return qr_events
            return [NextState(states.GenreEnter.name, jump=True)]

        else:
            dispatcher.respond_template(f"{selector_root}/respond/decline", {})
            qr_events = handle_question(genre)
            if qr_events:
                return qr_events

            return [NextState(states.GenreEnter.name, jump=True)]
