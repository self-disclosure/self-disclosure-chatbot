from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState

from response_generator.music.utils import entity


selector_root = "fav_genre_artist"


class FavoriteGenreArtistPropose(MusicState):

    name = 'fav_genre_artist_propose'

    @classmethod
    def require_entity(cls, tracker: Tracker, genre: entity.GenreItem):
        tracker.one_turn_store['genre_to_discuss'] = genre

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if not self.genre_to_discuss:
            return [NextState(states.ErrorHandlingEnter.name, jump=True)]

        dispatcher.respond_template(f"{selector_root}/propose/default", dict(genre=self.genre_to_discuss.noun))

        """go to fav_artist for their flow"""
        """update counter to limit home many times proposing fav_artist"""
        states.FavArtistEnter.increment_counter(tracker)
        # """disable check so it say in artist"""
        tracker.persistent_store['from_fav_genre'] = True
        return [NextState(states.FavArtistRespond.name)]


class FavoriteGenreArtistRespond(MusicState):

    name = 'fav_genre_artist_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.GenreEnter.name, jump=True)]
