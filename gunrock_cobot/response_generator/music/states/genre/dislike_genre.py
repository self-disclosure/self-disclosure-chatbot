from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState

from response_generator.music.utils import entity


selector_root = "dislike_genre"


class DislikeGenrePropose(MusicState):

    name = 'dislike_genre_propose'

    @classmethod
    def require_entity(cls, tracker: Tracker, genre: entity.GenreItem):
        tracker.one_turn_store['genre_to_discuss'] = genre

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/propose/default", {})
        states.DislikeGenreRespond.require_entity(dispatcher, tracker)
        return [NextState(states.DislikeGenreRespond.name)]


class DislikeGenreRespond(MusicState):

    name = 'dislike_genre_respond'

    @classmethod
    def require_entity(cls, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.expect_opinion(True)

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
            dispatcher.respond_template(f"{selector_root}/respond/default_sys_ack", {})
        else:
            dispatcher.respond_template(f"{selector_root}/respond/default", {})
        return [NextState(states.GenreEnter.name, jump=True)]
