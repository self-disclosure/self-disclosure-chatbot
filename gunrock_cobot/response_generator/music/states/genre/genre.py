from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState


selector_root = "fav_genre"


class GenreEnter(MusicState):

    name = 'genre_enter'

    @classmethod
    def accept_transition(cls, tracker: Tracker):
        counter = tracker.persistent_store.get('genre_counter') or 0
        return counter < 1

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        counter = tracker.persistent_store.get('genre_counter') or 0
        tracker.persistent_store['genre_counter'] = counter + 1

        if (
            counter < 1 and
            states.FavGenrePropose.accept_transition(tracker)
        ):
            return [NextState(states.FavGenrePropose.name, jump=True)]

        else:
            return [NextState(states.SwitchSubtopicEnter.name, jump=True)]
