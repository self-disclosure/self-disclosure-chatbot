from typing import Optional

from nlu.constants import Positivity
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState
from response_generator.music.utils import entity


selector_root = "do_you_play_instrument"


class DoYouPlayInstrumentPropose(MusicState):

    name = 'do_you_play_instrument_propose'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        dispatcher.respond_template(f"{selector_root}/propose/default", {})
        return [NextState(states.DoYouPlayInstrumentRespond.name)]


class DoYouPlayInstrumentAnotherPropose(MusicState):

    name = 'do_you_play_instrument_another_propose'

    @classmethod
    def require_entity(cls, tracker: Tracker, instrument: Optional[entity.InstrumentItem]):
        if instrument:
            tracker.one_turn_store['instrument_to_discuss'] = instrument

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        if self.instrument_to_discuss:
            dispatcher.respond_template(
                f"{selector_root}/propose/another/with_instrument", dict(instrument=self.instrument_to_discuss.noun))

        else:
            dispatcher.respond_template(
                f"{selector_root}/propose/another/default", dict())

        return [NextState(states.DoYouPlayInstrumentRespond.name)]


class DoYouPlayInstrumentRespond(MusicState):

    name = 'do_you_play_instrument_respond'

    @classmethod
    def require_entity(cls, tracker: Tracker, instrument: Optional[entity.InstrumentItem]):
        if instrument:
            tracker.one_turn_store['instrument_to_discuss'] = instrument

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        instrument = next(iter(entity.InstrumentItem.detect(tracker)), None)
        counter = (
            tracker.one_turn_store.get('do_you_play_instrument_respond_yes_only_counter') or
            tracker.last_utt_store.get('do_you_play_instrument_respond_yes_only_counter') or
            0
        )

        def handle_question(ignore_idk_response=True):
            def ask_back_handler(qr):
                dispatcher.respond(f"Sadly I'm not talented enought to play an instrument.")

            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=ask_back_handler,
                    follow_up_handler=lambda qr: None,
                    ignore_idk_response=lambda qr: ignore_idk_response,
                )

        if (
            tracker.returnnlp.answer_positivity is Positivity.neg and
            not instrument
        ):
            user_can_play_instrument_hist = self.user_profile.user_can_play_instrument_history

            if (
                (
                    self.module.state_tracker.prev_turn() and
                    self.module.state_tracker.prev_turn()[-1] in {states.DoYouPlayInstrumentAnotherPropose.name} and
                    user_can_play_instrument_hist
                ) or
                self.instrument_to_discuss and self.instrument_to_discuss in user_can_play_instrument_hist
            ):
                """if we are asking if they know another instrument, and we already know they know at least one"""

                if self.sys_ack_utt:
                    dispatcher.respond(self.sys_ack_utt)

                qr_events = handle_question()
                if qr_events:
                    return qr_events

                dispatcher.respond_template(
                    f"{selector_root}/respond/neg_from_another_with_hist",
                    dict(instrument=(
                        self.instrument_to_discuss.noun if self.instrument_to_discuss else
                        user_can_play_instrument_hist[-1].noun
                    ))
                )

            elif self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
                qr_events = handle_question()
                if qr_events:
                    return qr_events

            else:
                dispatcher.respond_template(f"{selector_root}/respond/neg", {})
                qr_events = handle_question()
                if qr_events:
                    return qr_events
                self.user_profile.user_know_zero_instruments = True

            return [NextState(states.SwitchSubtopicEnter.name, jump=True)]

        elif (
            tracker.returnnlp.answer_positivity is Positivity.pos and
            not instrument and
            counter < 1
        ):
            """user say yes but didn't disclose what instrument it is"""
            tracker.one_turn_store['do_you_play_instrument_respond_yes_only_counter'] = counter + 1
            # TODO: better detection if user repeat it due to ner
            if self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond(f"Cool!")
            qr_events = handle_question()
            if qr_events:
                return qr_events
            dispatcher.respond_template(f"{selector_root}/respond/yes_only", {})
            return [NextState(self.name)]

        elif (
            instrument
        ):
            if states.InstrumentCannedEnter.accept_transition(tracker, instrument):
                dispatcher.respond_template(f"{selector_root}/respond/pos", {})
                states.InstrumentCannedEnter.require_entity(tracker, instrument)
                return [NextState(states.InstrumentCannedEnter.name, jump=True)]

            else:
                dispatcher.respond_template(f"{selector_root}/respond/pos", {})
                qr_events = handle_question()
                if qr_events:
                    return qr_events
                # TODO: General Flow
                if states.InstrumentEnter.accept_transition(tracker, None, self.user_profile):
                    states.InstrumentEnter.require_entity(tracker, None)
                    return [NextState(states.InstrumentEnter.name, jump=True)]
                else:
                    return [NextState(states.SwitchSubtopicEnter.name, jump=True)]

        else:
            if self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond("I see.")
            qr_events = handle_question()
            if qr_events:
                return qr_events
            if states.InstrumentEnter.accept_transition(tracker, None, self.user_profile):
                states.InstrumentEnter.require_entity(tracker, None)
                return [NextState(states.InstrumentEnter.name, jump=True)]
            else:
                return [NextState(states.SwitchSubtopicEnter.name, jump=True)]
