import re
from typing import Dict

from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState
from response_generator.music.utils import entity, regex_match


selector_root = "instrument_canned"


def valid_instruments(instrument: entity.InstrumentItem) -> bool:
    return instrument.canonical in {'guitar', 'piano'} if instrument else False


class InstrumentCannedEnter(MusicState):
    """
    when user says "i like to play <instrument>"
    """

    name = 'instrument_canned_response_enter'

    @classmethod
    def accept_transition(cls, tracker: Tracker, instrument: entity.InstrumentItem):
        counter = tracker.persistent_store.get('instrument_canned_enter_counter') or 0
        return counter < 1 and bool(instrument)

    @classmethod
    def require_entity(cls, tracker: Tracker, instrument: entity.InstrumentItem):
        tracker.one_turn_store['instrument_to_discuss'] = instrument

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        counter = tracker.persistent_store.get('instrument_canned_enter_counter') or 0
        tracker.persistent_store['instrument_canned_enter_counter'] = counter + 1

        states.InstrumentCannedClassicModernPlaystyleProposing.require_entity(tracker, self.instrument_to_discuss)
        return [NextState(states.InstrumentCannedClassicModernPlaystyleProposing.name, jump=True)]


class InstrumentCannedExit(MusicState):
    """
    when user says "i like to play <instrument>"
    """

    name = 'instrument_canned_exit'

    @classmethod
    def require_entity(cls, tracker: Tracker, instrument: entity.InstrumentItem):
        tracker.one_turn_store['instrument_to_discuss'] = instrument

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        # instrument = self.instrument_to_discuss
        # dispatcher.respond_template(
        #     f"{selector_root}/transition/exit",
        #     dict(instrument=instrument.noun if instrument else "instruments"))
        if states.InstrumentEnter.accept_transition(tracker, self.instrument_to_discuss, self.user_profile):
            states.InstrumentEnter.require_entity(tracker, self.instrument_to_discuss)
            return [NextState(states.InstrumentEnter.name, jump=True)]
        else:
            return [NextState(states.SwitchSubtopicEnter.name, jump=True)]


class InstrumentCannedClassicModernPlaystyleProposing(MusicState):

    name = "instruments_classic_modern_playstyle_proposing"

    @classmethod
    def require_entity(cls, tracker: Tracker, instrument: entity.InstrumentItem):
        tracker.one_turn_store['instrument_to_discuss'] = instrument

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        if self.instrument_to_discuss.canonical == 'guitar':
            key = 'guitar'
        elif self.instrument_to_discuss.canonical == 'piano':
            key = 'piano'
        else:
            key = 'default'

        dispatcher.respond_template(f"{selector_root}/classic_or_modern_playstyle/proposing/{key}", {})
        states.InstrumentCannedClassicModernPlaystyleRespond.require_entity(tracker, self.instrument_to_discuss)
        return [NextState(states.InstrumentCannedClassicModernPlaystyleRespond.name)]


class InstrumentCannedClassicModernPlaystyleRespond(MusicState):

    name = "instruments_classic_modern_playstyle_respond"

    @classmethod
    def require_entity(cls, tracker: Tracker, instrument: entity.InstrumentItem):
        tracker.one_turn_store['instrument_to_discuss'] = instrument

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        def handle_question():
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                )

        if re.search(r"\b(classic(al)?)\b", tracker.input_text):
            key = 'classical'
        elif re.search(r"\b(contemporary|pop)\b", tracker.input_text):
            key = 'contemporary'
        elif re.search(r"\b(both|any)\b", tracker.input_text) or regex_match.is_saying_all_of_them(tracker):
            key = 'both'
        else:
            key = 'default'

        if self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
        else:
            dispatcher.respond_template(f"{selector_root}/classic_or_modern_playstyle/respond/{key}", {})
        qr_events = handle_question()
        if qr_events:
            return qr_events
        states.InstrumentCannedLengthExperienceProposing.require_entity(tracker, self.instrument_to_discuss)
        return [NextState(states.InstrumentCannedLengthExperienceProposing.name, jump=True)]


class InstrumentCannedLengthExperienceProposing(MusicState):

    name = "instruments_classic_length_experience_proposing"

    @classmethod
    def require_entity(cls, tracker: Tracker, instrument: entity.InstrumentItem):
        tracker.one_turn_store['instrument_to_discuss'] = instrument

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(
            f"{selector_root}/length_experience/proposing",
            dict(instrument=self.instrument_to_discuss.noun if self.instrument_to_discuss else "the instrument"))
        states.InstrumentCannedLengthExperienceRespond.require_entity(tracker, self.instrument_to_discuss)
        return [NextState(states.InstrumentCannedLengthExperienceRespond.name)]


class InstrumentCannedLengthExperienceRespond(MusicState):

    name = "instruments_length_experience_respond"

    @classmethod
    def require_entity(cls, tracker: Tracker, instrument: entity.InstrumentItem):
        tracker.one_turn_store['instrument_to_discuss'] = instrument

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        def handle_question():
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                )

        if re.search(r"\b(just started|beginner|not long|new|month(s)?|(a|one) year)\b", tracker.input_text):
            key = 'beginner'
        elif re.search(r"\b(a while|was (young|(a (kid|child)))|years)\b", tracker.input_text):
            key = 'expert'
        else:
            key = 'default'
        if self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
        else:
            dispatcher.respond_template(f"{selector_root}/length_experience/respond/{key}", {})

        qr_events = handle_question()
        if qr_events:
            return qr_events
        states.InstrumentCannedUsualSongProposing.require_entity(tracker, self.instrument_to_discuss)
        return [NextState(states.InstrumentCannedUsualSongProposing.name, jump=True)]


class InstrumentCannedUsualSongProposing(MusicState):

    name = "instruments_usual_song_proposing"

    @classmethod
    def require_entity(cls, tracker: Tracker, instrument: entity.InstrumentItem):
        tracker.one_turn_store['instrument_to_discuss'] = instrument

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/usual_song/proposing", {})
        states.InstrumentCannedUsualSongRespond.require_entity(dispatcher, tracker, self.instrument_to_discuss)
        return [NextState(states.InstrumentCannedUsualSongRespond.name)]


class InstrumentCannedUsualSongRespond(MusicState):

    name = "instruments_usual_song_respond"

    @classmethod
    def require_entity(cls, dispatcher: Dispatcher, tracker: Tracker, instrument: entity.InstrumentItem):
        dispatcher.expect_opinion(True)
        tracker.one_turn_store['instrument_to_discuss'] = instrument

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        def handle_question():
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                )

        if self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
        else:
            dispatcher.respond("Great.")

        qr_events = handle_question()
        if qr_events:
            return qr_events

        states.InstrumentCannedWhosYourTeacherProposing.require_entity(tracker, self.instrument_to_discuss)
        return [NextState(states.InstrumentCannedWhosYourTeacherProposing.name, jump=True)]


class InstrumentCannedWhosYourTeacherProposing(MusicState):

    name = "instruments_whos_your_teacher_proposing"

    @classmethod
    def require_entity(cls, tracker: Tracker, instrument: entity.InstrumentItem):
        tracker.one_turn_store['instrument_to_discuss'] = instrument

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/whos_your_teacher/proposing", {})
        states.InstrumentCannedWhosYourTeacherRespond.require_entity(tracker, self.instrument_to_discuss)
        return [NextState(states.InstrumentCannedWhosYourTeacherRespond.name)]


class InstrumentCannedWhosYourTeacherRespond(MusicState):

    name = "instruments_whos_your_teacher_respond"

    @classmethod
    def require_entity(cls, tracker: Tracker, instrument: entity.InstrumentItem):
        tracker.one_turn_store['instrument_to_discuss'] = instrument

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        def handle_question():
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                )

        if re.search(r"\b(teacher|friend)\b", tracker.input_text):
            key = 'teacher'
        elif re.search(r"\b(self taught|by myself|youtube)\b", tracker.input_text):
            key = 'self'
        else:
            key = 'default'

        if self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
        else:
            dispatcher.respond_template(f"{selector_root}/whos_your_teacher/respond/{key}", {})

        qr_events = handle_question()
        if qr_events:
            return qr_events

        if states.InstrumentCannedFamousMusicianProposing.accept_transition(tracker, self.instrument_to_discuss):
            return [NextState(states.InstrumentCannedFamousMusicianProposing.name, jump=True)]
        else:
            states.InstrumentCannedExit.require_entity(tracker, self.instrument_to_discuss)
            return [NextState(states.InstrumentCannedExit.name, jump=True)]


class InstrumentCannedFamousMusicianProposing(MusicState):

    name = "instruments_famous_musician_proposing"

    @classmethod
    def accept_transition(cls, tracker: Tracker, instrument: entity.InstrumentItem):
        return valid_instruments(instrument)

    @classmethod
    def require_entity(cls, tracker: Tracker, instrument: entity.InstrumentItem):
        tracker.one_turn_store['instrument_to_discuss'] = instrument

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        instrument = self.instrument_to_discuss

        args = {}
        specifics = dispatcher.tm.speak(
            f"{selector_root}/famous_musician/proposing/specific/{instrument.noun}", {}, embedded_info=args)
        self.logger.debug(f"specific args = {args}")

        dispatcher.respond_template(f"{selector_root}/famous_musician/proposing/default", args)
        dispatcher.respond(specifics)

        states.InstrumentCannedFamousMusicianRespond.require_entity(dispatcher, tracker, instrument, args)
        return [NextState(states.InstrumentCannedFamousMusicianRespond.name)]


class InstrumentCannedFamousMusicianRespond(MusicState):

    name = "instruments_famous_musician_respond"

    @classmethod
    def require_entity(
        cls, dispatcher: Dispatcher, tracker: Tracker, instrument: entity.InstrumentItem, musician: Dict[str, str]
    ):
        dispatcher.expect_opinion(True)
        tracker.one_turn_store['instrument_to_discuss'] = instrument
        tracker.one_turn_store['famous_musician'] = musician

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        def handle_question():
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                    is_elif_question=lambda: bool(
                        re.search(
                            r"(^who)|(^no (i have(n't| not) )?who is)|\b(have(n't |not) heard of)\b",
                            tracker.input_text)
                    ),
                    elif_handler=lambda qr: None,
                )

        musician: Dict[str, str] = tracker.last_utt_store.get('famous_musician')

        if (
            musician and 'canonical' in musician and
            re.search(r"(^who)|(^no (i have(n't| not) )?who is)|\b(have(n't |not) heard of)\b", tracker.input_text)
        ):
            dispatcher.respond_template(f"{selector_root}/famous_musician/who_is/{musician['canonical']}", {})

        elif self.sys_ack_tag == 'ack_opinion' and self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)

        elif tracker.returnnlp.answer_positivity == 'neg':
            dispatcher.respond_template(
                f"{selector_root}/famous_musician/respond/default/neg",
                dict(pronoun=musician.get('pronoun') if musician else "them"))

        elif self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)

        elif tracker.returnnlp.answer_positivity == 'pos':
            dispatcher.respond_template(f"{selector_root}/famous_musician/respond/default/pos", {})

        else:
            dispatcher.respond_template(f"{selector_root}/famous_musician/respond/default/neu", {})

        qr_events = handle_question()
        if qr_events:
            return qr_events

        if (
            musician and 'canonical' in musician and
            dispatcher.tm.has_selector(f"{selector_root}/famous_musician/respond/specific/{musician['canonical']}")
        ):
            dispatcher.respond_template(f"{selector_root}/famous_musician/respond/specific/{musician['canonical']}", {})

        states.InstrumentCannedFamousMusicianRespondEpilogue.require_entity(
            dispatcher, tracker, self.instrument_to_discuss)
        return [NextState(states.InstrumentCannedFamousMusicianRespondEpilogue.name)]


class InstrumentCannedFamousMusicianRespondEpilogue(MusicState):

    name = "instruments_famous_musician_respond_epilogue"

    @classmethod
    def require_entity(cls, dispatcher: Dispatcher, tracker: Tracker, instrument: entity.InstrumentItem):
        dispatcher.expect_opinion(True)
        tracker.one_turn_store['instrument_to_discuss'] = instrument

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        def handle_question():
            def is_elif_question():
                return bool(
                    re.search(r"\b((do|did) you know) (?P<name>.*) (just )?(get |got )?married\b", tracker.input_text))

            def elif_handler(qr):
                dispatcher.respond(f"I didn't know about that! Thanks for letting me know!")

            if self.handler.has_question(tracker, is_question=[is_elif_question]):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                    elif_handler=elif_handler,
                    is_elif_question=is_elif_question,
                )

        if self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)

        qr_events = handle_question()
        if qr_events:
            return qr_events

        states.InstrumentCannedExit.require_entity(tracker, self.instrument_to_discuss)
        return [NextState(states.InstrumentCannedExit.name, jump=True)]
