from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState
from response_generator.music.utils import entity


selector_root = "play_what_instrument"


class PlayWhatInstrumentPropose(MusicState):
    """
    mainly for when user say "play instrument" in open_question
    """

    name = 'play_what_instrument_propose'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        if (
            any(i in {states.Initial.name} for i in self.module.state_tracker.curr_turn())
        ):
            dispatcher.respond_template(f"{selector_root}/transition/from_initial", {})

        dispatcher.respond_template(f"{selector_root}/propose/default", {})
        return [NextState(states.PlayWhatInstrumentRespond.name)]


class PlayWhatInstrumentRespond(MusicState):

    name = 'play_what_instrument_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        instrument: entity.InstrumentItem = next(iter(entity.InstrumentItem.detect(tracker)), None)

        if (
            instrument and
            states.InstrumentCannedEnter.accept_transition(tracker, instrument)
        ):
            dispatcher.respond_template(f"{selector_root}/respond/pos", {})
            states.InstrumentCannedEnter.require_entity(tracker, instrument)
            return [NextState(states.InstrumentCannedEnter.name, jump=True)]

        elif (
            instrument
        ):
            dispatcher.respond_template(f"{selector_root}/respond/no_canned", dict(instrument=instrument.noun))
            # TODO: go to general canned response
            # states.InstrumentCannedEnter.require_entity(tracker, instrument)
            # return [NextState(states.InstrumentCannedEnter.name, jump=True)]

        elif self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
        
        else:
            dispatcher.respond_template(f"ack/i_see", {}, postfix='.')

        if states.InstrumentEnter.accept_transition(tracker, self.instrument_to_discuss, self.user_profile):
            states.InstrumentEnter.require_entity(tracker, self.instrument_to_discuss)
            return [NextState(states.InstrumentEnter.name, jump=True)]
        else:
            return [NextState(states.SwitchSubtopicEnter.name, jump=True)]
