import re
from typing import Dict

from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState
from response_generator.music.utils import entity, regex_match


selector_root = "instrument_canned"


class LearnInstrumentWishlistPropose(MusicState):

    name = 'learn_instrument_wishlist_propose'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        return [NextState(states.DoYouPlayInstrumentRespond.name)]


class LearnInstrumentWishlistRespond(MusicState):

    name = 'learn_instrument_wishlist_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        return [NextState(states.InstrumentEnter.name, jump=True)]
