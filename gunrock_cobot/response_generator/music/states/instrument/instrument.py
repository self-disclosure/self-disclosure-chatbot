from typing import Optional

from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState
from response_generator.music.utils import entity, user_profile


selector_root = "instrument"


class InstrumentEnter(MusicState):

    name = 'instrument_enter'

    @classmethod
    def accept_transition(
        cls, tracker: Tracker, instrument: Optional[entity.InstrumentItem], user_profile: user_profile.UserProfile
    ):
        counter = InstrumentEnter.counter(tracker)
        return counter < 2 and user_profile.user_know_zero_instruments is not True

    @classmethod
    def require_entity(cls, tracker: Tracker, instrument: Optional[entity.InstrumentItem]):
        if instrument:
            tracker.one_turn_store['instrument_to_discuss'] = instrument

    @classmethod
    def counter(cls, tracker: Tracker):
        return tracker.persistent_store.get('instrument_enter_counter') or 0

    @classmethod
    def increment_counter(cls, tracker: Tracker):
        tracker.persistent_store['instrument_enter_counter'] = InstrumentEnter.counter(tracker) + 1

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        counter = InstrumentEnter.counter(tracker)
        InstrumentEnter.increment_counter(tracker)

        if (
            self.instrument_to_discuss and
            states.InstrumentCannedEnter.accept_transition(tracker, self.instrument_to_discuss)
        ):
            states.InstrumentCannedEnter.require_entity(tracker, self.instrument_to_discuss)
            return [NextState(states.InstrumentCannedEnter.name, jump=True)]

        elif (
            self.instrument_to_discuss and
            not any(i in {states.InstrumentCannedExit.name} for i in self.module.state_tracker.curr_turn())
        ):
            states.DoYouPlayInstrumentRespond.require_entity(tracker, self.instrument_to_discuss)
            return [NextState(states.DoYouPlayInstrumentRespond.name, jump=True)]

        elif self.user_profile.user_know_zero_instruments is True:
            return [NextState(states.SwitchSubtopicEnter.name, jump=True)]

        elif counter < 1:
            return [NextState(states.DoYouPlayInstrumentPropose.name, jump=True)]

        elif counter < 2:
            states.DoYouPlayInstrumentAnotherPropose.require_entity(tracker, self.instrument_to_discuss)
            return [NextState(states.DoYouPlayInstrumentAnotherPropose.name, jump=True)]

        else:
            return [NextState(states.SwitchSubtopicEnter.name, jump=True)]
