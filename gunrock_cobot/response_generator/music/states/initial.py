import re

from nlu.constants import Positivity
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState
from response_generator.music.utils import entity


selector_root = 'initial'


class Initial(MusicState):

    name = 'initial'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        artist: entity.ArtistItem = next(iter(entity.ArtistItem.detect(tracker)), None)
        instrument: entity.InstrumentItem = next(iter(entity.InstrumentItem.detect(tracker)), None)

        def handle_question():
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                )

        if (
            self.handler.has_question_only(tracker)
        ):
            qr_events = handle_question()
            if qr_events:
                return qr_events
            return [NextState(states.SwitchSubtopicEnter.name, jump=True)]

        if artist and tracker.returnnlp.answer_positivity is not Positivity.neg:
            dispatcher.respond("Oh sure.")
            if states.WhyLikeArtistPropose.accept_transition(tracker, artist, self.user_profile):
                states.WhyLikeArtistPropose.require_entity(tracker, artist)
                return [NextState(states.WhyLikeArtistPropose.name, jump=True)]
            else:
                states.FavSongByArtistPropose.require_entity(tracker, artist)
                return [NextState(states.FavSongByArtistPropose.name, jump=True)]

        elif re.search(r"\b(play instruments?)\b", tracker.input_text):
            """increment instrument counter"""
            counter = tracker.persistent_store.get('instrument_enter_counter') or 0
            tracker.persistent_store['instrument_enter_counter'] = counter + 1

            return [NextState(states.PlayWhatInstrumentPropose.name, jump=True)]

        elif instrument:
            if states.InstrumentCannedEnter.accept_transition(tracker, instrument):
                """increment instrument counter"""
                states.InstrumentEnter.increment_counter(tracker)

                """jump to canned chitchat"""
                states.InstrumentCannedEnter.require_entity(tracker, instrument)
                self.user_profile.user_can_play_instrument_history_append(instrument)
                dispatcher.respond("That's awesome!")
                return [NextState(states.InstrumentCannedEnter.name, jump=True)]

            else:
                states.InstrumentEnter.require_entity(tracker, instrument)
                return [NextState(states.InstrumentEnter.name, jump=True)]

        else:
            dispatcher.respond_template(f"{selector_root}/propose/default", {})
            return [NextState(states.SwitchSubtopicEnter.name, jump=True)]
