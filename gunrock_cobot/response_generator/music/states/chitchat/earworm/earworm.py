from nlu.constants import Positivity
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState
from response_generator.music.utils import entity


selector_root = 'earworm'


class EarwormEnter(MusicState):

    name = 'earworm_enter'

    @classmethod
    def accept_transition(cls, tracker: Tracker):
        return states.EarwormPropose.accept_transition(tracker)

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.EarwormPropose.name, jump=True)]


class EarwormExit(MusicState):

    name = 'earworm_exit'

    @classmethod
    def accept_transition(cls, tracker: Tracker):
        return states.EarwormPropose.accept_transition(tracker)

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.ChitchatExit.name, jump=True)]


class EarwormPropose(MusicState):

    name = 'earworm_propose'

    @classmethod
    def accept_transition(cls, tracker: Tracker):
        counter = tracker.persistent_store.get('earworm_propose_counter') or 0
        return counter < 1

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        counter = tracker.persistent_store.get('earworm_propose_counter') or 0
        tracker.persistent_store['earworm_propose_counter'] = counter + 1

        dispatcher.respond_template(f"{selector_root}/propose/default", dict())
        states.EarwormRespond.require_entity(dispatcher, tracker)
        return [NextState(states.EarwormRespond.name)]


class EarwormRespond(MusicState):

    name = 'earworm_respond'

    @classmethod
    def require_entity(cls, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.expect_opinion(True)

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        artist = next(iter(entity.ArtistItem.detect(tracker)), None)
        song = next(iter(entity.SongItem.detect(tracker)), None)

        def handle_question(skip_ask_back=False):
            def ask_back_handler(qr):
                if not skip_ask_back:
                    dispatcher.respond_template(f"{selector_root}/respond/self_disclose", {})
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=ask_back_handler,
                    follow_up_handler=lambda qr: None,
                )

        if (
            tracker.returnnlp.answer_positivity is Positivity.pos and song
        ):
            dispatcher.respond_template(f"{selector_root}/respond/pos_with_song", dict(song=song.noun))
            qr_events = handle_question()
            if qr_events:
                return qr_events

        elif (
            tracker.returnnlp.answer_positivity is Positivity.pos and artist
        ):
            dispatcher.respond_template(f"{selector_root}/respond/pos_with_artist", dict(artist=artist.noun))
            qr_events = handle_question()
            if qr_events:
                return qr_events
        
        elif (
            self.sys_ack_tag == 'ask_opinion' and self.sys_ack_utt
        ):
            dispatcher.respond(self.sys_ack_utt)
            qr_events = handle_question()
            if qr_events:
                return qr_events

        elif (
            tracker.returnnlp.answer_positivity is Positivity.pos
        ):
            dispatcher.respond_template(f"{selector_root}/respond/pos", dict())
            qr_events = handle_question()
            if qr_events:
                return qr_events

        elif (
            tracker.returnnlp.answer_positivity is Positivity.neg
        ):
            dispatcher.respond_template(f"{selector_root}/respond/neg", {})
            dispatcher.respond_template(f"{selector_root}/respond/self_disclose", {})
            qr_events = handle_question(skip_ask_back=True)
            if qr_events:
                return qr_events

        else:
            if self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond_template(f"{selector_root}/respond/default", {})
            qr_events = handle_question(skip_ask_back=True)
            if qr_events:
                return qr_events

        """exiting"""
        return [NextState(states.EarwormExit.name, jump=True)]
