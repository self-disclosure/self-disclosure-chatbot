from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState
from response_generator.music.utils import entity


selector_root = 'how_often_listen_music'


class HowOftenListenMusicEnter(MusicState):

    name = 'how_often_listen_music_enter'

    @classmethod
    def accept_transition(cls, tracker: Tracker):
        return states.HowOftenListenMusicPropose.accept_transition(tracker)

    @classmethod
    def require_entity(cls, tracker: Tracker, artist: entity.ArtistItem):
        tracker.one_turn_store['artist_to_discuss'] = artist

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.HowOftenListenMusicPropose.name, jump=True)]


class HowOftenListenMusicExit(MusicState):

    name = 'how_often_listen_music_exit'

    @classmethod
    def accept_transition(cls, tracker: Tracker):
        return states.HowOftenListenMusicPropose.accept_transition(tracker)

    @classmethod
    def require_entity(cls, tracker: Tracker, artist: entity.ArtistItem):
        tracker.one_turn_store['artist_to_discuss'] = artist

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.ChitchatExit.name, jump=True)]


class HowOftenListenMusicPropose(MusicState):

    name = 'how_often_listen_music_propose'

    @classmethod
    def accept_transition(cls, tracker: Tracker):
        counter = tracker.persistent_store.get('how_often_listen_music_propose_counter') or 0
        return counter < 1

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        counter = tracker.persistent_store.get('how_often_listen_music_propose_counter') or 0
        tracker.persistent_store['how_often_listen_music_propose_counter'] = counter + 1

        dispatcher.respond_template(f"{selector_root}/propose/transition", dict())
        dispatcher.respond_template(f"{selector_root}/propose/default", dict())
        states.HowOftenListenMusicRespond.require_entity(dispatcher, tracker)
        return [NextState(states.HowOftenListenMusicRespond.name)]


class HowOftenListenMusicRespond(MusicState):

    name = 'how_often_listen_music_respond'

    @classmethod
    def require_entity(cls, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.expect_opinion(True)

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        def handle_question():
            def ask_back_handler(qr):
                dispatcher.respond(
                    "I don't dislike any artists in particular."
                )
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=ask_back_handler,
                    follow_up_handler=lambda qr: None,
                )

        if self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
        else:
            dispatcher.respond_template(f"{selector_root}/respond/default", {})
        qr_events = handle_question()
        if qr_events:
            return qr_events

        """exiting"""
        return [NextState(states.HowOftenListenMusicExit.name, jump=True)]
