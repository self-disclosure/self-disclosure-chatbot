import random

from response_generator.fsm2 import Dispatcher, Tracker, NextState, FSMLogger
from response_generator.fsm2.utils import state_tracker

from response_generator.music import states
from response_generator.music.states.base import MusicState
# from response_generator.music.utils import entity


selector_root = 'when_listen_music'

buckets = [
    [
        states.WhenListenMusicEnter.name,
        # states.HowOftenListenMusicEnter.name,
        states.EarwormEnter.name,
    ],
    [
        states.ConcertEnter.name,
    ]
]


class ChitchatEnter(MusicState):

    name = 'chitchat_enter'

    @classmethod
    def accept_transition(cls, tracker: Tracker, state_tracker: state_tracker.StateTracker):
        hist = tracker.persistent_store.get('chitchat_hist') or []
        hist_idx = tracker.persistent_store.get('chitchat_hist_idx')
        avail = [
            [bi for bi in b if bi not in hist]
            for b in buckets
        ]
        avail_idx = [i for i, v in enumerate(avail) if v]

        FSMLogger('MUSICCHAT', ChitchatEnter.name, 'fsm.state').debug(
            f"hist = {hist}, "
            f"hist_idx = {hist_idx}, "
            f"avail = {avail}, "
            f"avail_idx = {avail_idx}, "
        )

        if hist_idx is not None and avail[hist_idx]:
            return True
        elif hist_idx is not None and not avail[hist_idx]:
            """has idx but the bucket ran out"""
            del tracker.persistent_store['chitchat_hist_idx']
            return False
        elif avail_idx:
            return True
        else:
            return False

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        hist = tracker.persistent_store.get('chitchat_hist') or []
        hist_idx = tracker.persistent_store.get('chitchat_hist_idx')
        avail = [
            [bi for bi in b if bi not in hist and MusicState.from_name(bi).accept_transition(tracker=tracker)]
            for b in buckets
        ]
        avail_idx = [i for i, v in enumerate(avail) if v]
        if hist_idx is not None and avail[hist_idx]:
            idx = hist_idx
        elif avail_idx:
            idx = random.choice(avail_idx)
        else:
            """no more left"""
            return [NextState(states.SwitchSubtopicEnter.name, jump=True)]

        next_state = next(iter(avail[idx]), None)
        if not next_state:
            self.logger.warning(f"no next_state: next_state = {next_state}, avail = {avail}")
            return [NextState(states.ErrorHandlingEnter.name, jump=True)]

        tracker.persistent_store['chitchat_hist'] = [*hist, next_state]
        tracker.persistent_store['chitchat_hist_idx'] = idx
        return [NextState(next_state, jump=True)]


class ChitchatExit(MusicState):

    name = 'chitchat_exit'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if states.ChitchatEnter.accept_transition(tracker, self.module.state_tracker):
            return [NextState(states.ChitchatEnter.name, jump=True)]
        else:
            return [NextState(states.SwitchSubtopicEnter.name, jump=True)]
