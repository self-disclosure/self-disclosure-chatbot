import re

from nlu.constants import Positivity
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState
# from response_generator.music.utils import entity


selector_root = 'concert'


class ConcertEnter(MusicState):

    name = 'concert_enter'

    @classmethod
    def accept_transition(cls, tracker: Tracker):
        return states.ConcertPropose.accept_transition(tracker)

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.ConcertPropose.name, jump=True)]


class ConcertExit(MusicState):

    name = 'concert_exit'

    @classmethod
    def accept_transition(cls, tracker: Tracker):
        return states.ConcertPropose.accept_transition(tracker)

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.ChitchatExit.name, jump=True)]


class ConcertPropose(MusicState):

    name = 'concert_propose'

    @classmethod
    def accept_transition(cls, tracker: Tracker):
        counter = tracker.persistent_store.get('concert_propose_counter') or 0
        return counter < 1

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        counter = tracker.persistent_store.get('concert_propose_counter') or 0
        tracker.persistent_store['concert_propose_counter'] = counter + 1

        dispatcher.respond_template(f"{selector_root}/propose/default", dict())
        return [NextState(states.ConcertRespond.name)]


class ConcertRespond(MusicState):

    name = 'concert_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        def handle_question():
            def ask_back_handler(qr):
                dispatcher.respond_template(f"{selector_root}/respond/self_disclose", {})

            def is_ask_back_question():
                return (
                    self.handler.question_detector.is_ask_back_question() or
                    re.search(r"\b(have you been to)\b", tracker.input_text)
                )

            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=ask_back_handler,
                    follow_up_handler=lambda qr: None,
                    is_ask_back_question=is_ask_back_question
                )

        if (
            tracker.returnnlp.answer_positivity is Positivity.pos
        ):
            dispatcher.respond_template(f"{selector_root}/respond/pos", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events

            return [NextState(states.ConcertDidYouEnjoyPropose.name, jump=True)]

        elif (
            tracker.returnnlp.answer_positivity is Positivity.neg
        ):
            dispatcher.respond_template(f"{selector_root}/respond/neg", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events

        elif self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
            dispatcher.respond_template(f"{selector_root}/respond/default_sys_ack", {})
        else:
            dispatcher.respond_template(f"{selector_root}/respond/default", {})

        """exiting"""
        return [NextState(states.ConcertExit.name, jump=True)]
