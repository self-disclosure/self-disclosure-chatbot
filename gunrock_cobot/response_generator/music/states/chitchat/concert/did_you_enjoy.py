from nlu.constants import DialogAct, Positivity
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState
# from response_generator.music.utils import entity


selector_root = 'concert_did_you_enjoy'


class ConcertDidYouEnjoyPropose(MusicState):

    name = 'concert_did_you_enjoy_propose'

    @classmethod
    def accept_transition(cls, tracker: Tracker):
        counter = tracker.persistent_store.get('concert_did_you_enjoy_propose_counter') or 0
        return counter < 1

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        counter = tracker.persistent_store.get('concert_did_you_enjoy_propose_counter') or 0
        tracker.persistent_store['concert_did_you_enjoy_propose_counter'] = counter + 1

        dispatcher.respond_template(f"{selector_root}/propose/default", dict())
        states.ConcertDidYouEnjoyRespond.require_entity(dispatcher, tracker)
        return [NextState(states.ConcertDidYouEnjoyRespond.name)]


class ConcertDidYouEnjoyRespond(MusicState):

    name = 'concert_did_you_enjoy_respond'

    @classmethod
    def require_entity(cls, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.expect_opinion(True)

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        def handle_question():

            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                )

        if (self.sys_ack_tag == 'ack_opinion' and self.sys_ack_utt):
            """if has blender ack"""
            dispatcher.respond(self.sys_ack_utt)
        elif (
            tracker.returnnlp.answer_positivity is Positivity.pos
        ):
            dispatcher.respond_template(f"{selector_root}/respond/pos", {})

        elif (
            tracker.returnnlp.answer_positivity is Positivity.neg
        ):
            dispatcher.respond_template(f"{selector_root}/respond/neg", {})

        elif (
            tracker.returnnlp.has_dialog_act(DialogAct.OTHER_ANSWERS)
        ):
            dispatcher.respond_template(f"{selector_root}/respond/unknown", {})
        elif self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
        else:
            dispatcher.respond("Cool.")

        qr_events = handle_question()
        if qr_events:
            return qr_events

        """exiting"""
        return [NextState(states.ConcertExit.name, jump=True)]
