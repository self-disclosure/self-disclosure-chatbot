import re

from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.music import states
from response_generator.music.states.base import MusicState
from response_generator.music.utils import entity


selector_root = 'when_listen_music'


class WhenListenMusicEnter(MusicState):

    name = 'when_listen_music_enter'

    @classmethod
    def accept_transition(cls, tracker: Tracker):
        return states.WhenListenMusicPropose.accept_transition(tracker)

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.WhenListenMusicPropose.name, jump=True)]


class WhenListenMusicExit(MusicState):

    name = 'when_listen_music_exit'

    @classmethod
    def accept_transition(cls, tracker: Tracker):
        return states.WhenListenMusicPropose.accept_transition(tracker)

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.ChitchatExit.name, jump=True)]


class WhenListenMusicPropose(MusicState):

    name = 'when_listen_music_propose'

    @classmethod
    def accept_transition(cls, tracker: Tracker):
        counter = tracker.persistent_store.get('when_listen_music_propose_counter') or 0
        return counter < 1

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        counter = tracker.persistent_store.get('when_listen_music_propose_counter') or 0
        tracker.persistent_store['when_listen_music_propose_counter'] = counter + 1

        dispatcher.respond_template(f"{selector_root}/propose/transition", dict())
        dispatcher.respond_template(f"{selector_root}/propose/default", dict())
        return [NextState(states.WhenListenMusicRespond.name)]


class WhenListenMusicRespond(MusicState):

    name = 'when_listen_music_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        def handle_question(skip_ask_back=False):
            def ask_back_handler(qr):
                if not skip_ask_back:
                    dispatcher.respond_template(f"{selector_root}/respond/self_disclose", {})

            def is_ask_back_question():
                return (
                    self.handler.question_detector.is_ask_back_question() or
                    re.search(r"\b(when do you listen( to music)?)\b", tracker.input_text)
                )

            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=ask_back_handler,
                    follow_up_handler=lambda qr: None,
                    is_ask_back_question=is_ask_back_question
                )

        genre = next(iter(entity.GenreItem.detect(tracker)), None)

        if (
            re.search(r"\b(often|always|whenever I can)\b", tracker.input_text)
        ):
            dispatcher.respond_template(f"{selector_root}/respond/often", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events

        elif (
            re.search(r"\b(seldom|rarely|do(n't| not) have (the )?time)\b", tracker.input_text)
        ):
            dispatcher.respond_template(f"{selector_root}/respond/rarely", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events

        elif (
            re.search(r"\b(under pressure|stressed|take my mind off|relax)\b", tracker.input_text)
        ):
            key = 'under_pressure'
            if genre and genre.is_equal_to_name_str('jazz'):
                key = f"{key}_jazz"

            dispatcher.respond_template(f"{selector_root}/respond/{key}", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events

        elif (
            len(tracker.returnnlp) > 1
        ):
            dispatcher.respond_template(f"{selector_root}/respond/default", {})
            dispatcher.respond_template(f"{selector_root}/respond/self_disclose", {})
            qr_events = handle_question(skip_ask_back=True)
            if qr_events:
                return qr_events

        else:
            if self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond_template(f"{selector_root}/respond/default", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events

        """exiting"""
        return [NextState(states.WhenListenMusicExit.name, jump=True)]
