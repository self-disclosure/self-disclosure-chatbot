from response_generator.fsm2 import State, Dispatcher, Tracker
from util_redis import RedisHelper

from response_generator.music.utils import entity, regex_match
from response_generator.music.utils.handler import CommandQuestionHandler
from response_generator.music.utils.user_profile import UserProfile


class MusicState(State):

    def is_ask_back_response(self, tracker: Tracker):
        return (
            self.handler.question_detector.is_ask_back_question() or
            regex_match.is_asking_your_favorite(tracker)
        )

    def setup(self, dispatcher: Dispatcher, tracker: Tracker):
        self.redis_helper = RedisHelper()
        self.handler = CommandQuestionHandler(tracker)
        self.user_profile = UserProfile(tracker)

        self.sys_ack_utt, self.sys_ack_tag, self.sys_ack_source = tracker.sys_ack()

        self.artist_to_discuss: entity.ArtistItem = (
            tracker.volatile_store.get('artist_to_discuss') or
            tracker.one_turn_store.get('artist_to_discuss') or
            tracker.last_utt_store.get('artist_to_discuss') or
            None
        )
        self.genre_to_discuss: entity.GenreItem = (
            tracker.volatile_store.get('genre_to_discuss') or
            tracker.one_turn_store.get('genre_to_discuss') or
            tracker.last_utt_store.get('genre_to_discuss') or
            None
        )
        self.instrument_to_discuss: entity.ArtistItem = (
            tracker.volatile_store.get('instrument_to_discuss') or
            tracker.one_turn_store.get('instrument_to_discuss') or
            tracker.last_utt_store.get('instrument_to_discuss') or
            None
        )
