from .base import MusicState  # noqa: F401
from .initial import Initial  # noqa: F401
from .exception import (  # noqa: F401
    ErrorHandlingEnter,
    ProposeUnclearRespond,
    ExitTransitionEnter,
    SwitchSubtopicEnter,
)

"""
GLOBAL
"""
from .global_states.can_we_talk_about import (  # noqa: F401
    CanWeTalkAboutEnter,
)
from .global_states.play_music import (  # noqa: F401
    PlayMusicRespond,
    PlayMusicTalkAboutArtistPropose, PlayMusicTalkAboutArtistRespond,
)
from .global_states.me_singing.me_singing import (  # noqa: F401
    MeSingingPropose, MeSingingRespond,
    MeSingingReactionPropose, MeSingingReactionRespond,
)
from .global_states.module_selection import (  # noqa: F401
    SystemModuleSelection,
)
from .global_states.resume import (  # noqa: F401
    ResumeRespond,
)

"""
GENRE
"""
"""general"""
from .genre.genre import (  # noqa: F401
    GenreEnter,
)
from .genre.fav_genre import (  # noqa: F401
    FavGenrePropose, FavGenreRespond,
)
from .genre.fav_genre_artist import (  # noqa: F401
    FavoriteGenreArtistPropose, FavoriteGenreArtistRespond,
)
from .genre.dislike_genre import (  # noqa: F401
    DislikeGenrePropose, DislikeGenreRespond,
)

"""
ARTIST
"""
"""general"""
from .artist.fav_artist import (  # noqa: F401
    FavArtistEnter,
    FavArtistAnotherPropose,
    FavArtistPropose, FavArtistRespond,
)
from .artist.why_like_artist import (  # noqa: F401
    WhyLikeArtistPropose, WhyLikeArtistRespond,
)
from .artist.dislike_artist import (  # noqa: F401
    DislikeArtistPropose, DislikeArtistRespond,
)
from .artist.artist_trivia import (  # noqa: F401
    ArtistTriviaPropose, ArtistTriviaRespond,
)
"""canned responses for specific artist"""
from .artist.canned_templates.canned_templates import (  # noqa: F401
    ArtistCannedTemplatesEnter,
)
"""fav song by artist"""
from .artist.fav_song_by_artist import (  # noqa: F401
    FavSongByArtistPropose, FavSongByArtistRespond,
    FavSongByArtistAnotherPropose,
)
"""self disclosure"""
from .artist.my_favorite_artist import (  # noqa: F401
    artist_in_fav_list,
    respond_list_my_favorite,
    MyFavArtistPropose,
    MyFavArtistTriviaPropose, MyFavArtistReasonPropose,
)
"""artist specific"""
from .artist.do_you_like_artist import (  # noqa: F401
    DoYouLikeArtistPropose, DoYouLikeArtistRespond,
)

"""
Song
"""

"""
Instrument
"""
from .instrument.instrument import (  # noqa: F401
    InstrumentEnter,
)
from .instrument.play_what_instrument import (  # noqa: F401
    PlayWhatInstrumentPropose, PlayWhatInstrumentRespond,
)
from .instrument.do_you_play_instrument import (  # noqa: F401
    DoYouPlayInstrumentPropose, DoYouPlayInstrumentRespond,
    DoYouPlayInstrumentAnotherPropose,
)
from .instrument.learning_wishlist import (  # noqa: F401
    LearnInstrumentWishlistPropose, LearnInstrumentWishlistRespond,
)
from .instrument.instrument_canned_response import (  # noqa: F401
    InstrumentCannedEnter,
    InstrumentCannedExit,
    InstrumentCannedClassicModernPlaystyleProposing, InstrumentCannedClassicModernPlaystyleRespond,
    InstrumentCannedLengthExperienceProposing, InstrumentCannedLengthExperienceRespond,
    InstrumentCannedUsualSongProposing, InstrumentCannedUsualSongRespond,
    InstrumentCannedWhosYourTeacherProposing, InstrumentCannedWhosYourTeacherRespond,
    InstrumentCannedFamousMusicianProposing, InstrumentCannedFamousMusicianRespond,
    InstrumentCannedFamousMusicianRespondEpilogue,
)

"""
CHITCHAT
"""
"""when listen music"""
from .chitchat.when_listen_music.when_listen_music import (  # noqa: F401
    WhenListenMusicEnter, WhenListenMusicExit,
    WhenListenMusicPropose, WhenListenMusicRespond,
)
"""how often listen"""
from .chitchat.how_often_listen_music.how_often_listen_music import (  # noqa: F401
    HowOftenListenMusicEnter, HowOftenListenMusicExit,
    HowOftenListenMusicPropose, HowOftenListenMusicRespond,
)
"""earworm"""
from .chitchat.earworm.earworm import (  # noqa: F401
    EarwormEnter, EarwormExit,
    EarwormPropose, EarwormRespond,
)
"""concert"""
from .chitchat.concert.concert import (  # noqa: F401
    ConcertEnter, ConcertExit,
    ConcertPropose, ConcertRespond,
)
from .chitchat.concert.did_you_enjoy import (  # noqa: F401
    ConcertDidYouEnjoyPropose, ConcertDidYouEnjoyRespond,
)
"""main"""
from .chitchat.chitchat import (  # noqa: F401
    ChitchatEnter, ChitchatExit,
)
