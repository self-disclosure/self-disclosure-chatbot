import logging
import random
import re
from typing import List, TYPE_CHECKING, Optional

import nlu.utils.topic_module
from nlu.intent_classifier import IntentClassifier
from nlu.question_detector import QuestionDetector
from question_handling.quetion_handler import QuestionHandler, QuestionResponse
from response_generator.fsm.events import Event, NextState, PreviousState
from response_generator.fsm.utils import Dispatcher, Tracker
from template_manager import Template
from template_manager import error as tm_error
from utils import get_working_environment
from nlu.constants import Positivity, TopicModule
from nlu.constants import DialogAct as DialogActEnum
from template_manager import Template
from .states import regexCheckPositive
from .. import utils as cutils

from .base import ComfortState

logger = logging.getLogger("comfort.states")

knockKnockRegex = r"knock knock"
knockKnockCheck = re.compile(knockKnockRegex)


class UnclearTopicHandlerRespond(ComfortState):
    name = 'unclear_topic_handler'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:

        #topicList = tracker.returnnlp.detect_topics_by_regex()
        #logger.log("[COMFORT] unclear topic handler topic list: " + str(topicList))

        if tracker.returnnlp.has_intent({'topic_coronavirus'}):
            logger.debug("[COMFORT] entering coronavirus topic handler")
            return self.name
        elif knockKnockCheck.search(tracker.input_text) is not None:
            logger.debug("[COMFORT] entering knock knock handler")
            return self.name
        else:
            return None

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        if knockKnockCheck.search(tracker.input_text) is not None:
            dispatcher.respond_with_alt_template(Template.jokes, f"jokes/knock_knock", {})
            dispatcher.propose_continue("STOP", TopicModule.SOCIAL)
            return [NextState('init')]
        dispatcher.respond_template("backchannel", {})
        dispatcher.respond("Would you like to talk about coronavirus instead? ")
        dispatcher.ua.module_selection.add_propose_keyword("corona virus")
        dispatcher.propose_continue("UNCLEAR", TopicModule.NEWS)
        return[NextState('no_enter_topic')]


class NoEnterTopic(ComfortState):
    name = 'no_enter_topic'

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        logger.debug("[COMFORT] this is dialog act: " + str(tracker.returnnlp.dialog_act_label))
        askTalk = False
        for dialog_act in tracker.returnnlp.dialog_act_label:
            if dialog_act[0] in (DialogActEnum.OTHER_ANSWERS, DialogActEnum.NEG_ANSWER, DialogActEnum.BACK_CHANNELING):
                logger.debug("[COMFORT] contains other answer")
                askTalk = True
        if askTalk and len(tracker.returnnlp.dialog_act_label) == 1:
            dispatcher.respond("Would you like to talk more about how you are feeling? ")
            return [NextState('want_talk_about_feelings')]
        else:
            return[NextState('talking_about_feelings', jump=True)]