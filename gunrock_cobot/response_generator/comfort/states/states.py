import logging
import random
import re
from typing import List, TYPE_CHECKING, Optional

import nlu.utils.topic_module
from nlu.intent_classifier import IntentClassifier
from nlu.question_detector import QuestionDetector
from question_handling.quetion_handler import QuestionHandler, QuestionResponse
from response_generator.fsm.events import Event, NextState, PreviousState
from response_generator.fsm.utils import Dispatcher, Tracker
from template_manager import Template
from template_manager import error as tm_error
from utils import get_working_environment
from nlu.constants import Positivity, TopicModule
from nlu.constants import DialogAct as DialogActEnum
from nlu.intentmap_scheme import lexical_re_patterns

from .. import utils as cutils

from .base import ComfortState

if TYPE_CHECKING:
    from nlu.dataclass import CentralElement, ReturnNLP, ReturnNLPSegment  # noqa: F401

logger = logging.getLogger("comfort.states")
#TODO: check the first part of the regex
regexForMissedNegative = r"i (don't).*you \b[a-zA-Z0-9_]+\b (want|wanna)|i (hated|don't|wouldn't) (it|want|wanna)|^\bno\b|\bpersonal\b|(i'm|i am) good$|(i'm|i am) (good|okay) (thanks|thank you)|\b(that's|that is)( about|) (it|all)\b|(i'm|i am) okay$|^(that's|that is) okay|^nothing|^never mind|^not really"
regexForMissedPositive = r"^yes$|^yeah$|^sure|^okay$"
regexForExceptions = r"^yes$|i would love.*bad joke"
regexWantMoreTalk = r"^((?!(\bdon't\b|\bdo not\b)).)*(talk|tell|share).*(my|i'm|i am) feeling"
regexNoTalk = r"^(i'm|i am) not having a hard time$|^(i'm|i am) not.*upset$|^(i'm|i am) not.*unhappy$"
regexOtherAnswer = lexical_re_patterns["ans_unknown"]
regexCheckNegative = re.compile(regexForMissedNegative)
regexCheckPositive = re.compile(regexForMissedPositive)
regexCheckExceptions = re.compile(regexForExceptions)
regexCheckWantMoreTalk = re.compile(regexWantMoreTalk)
regexCheckNoTalk = re.compile(regexNoTalk)
regexCheckOtherAnswer = re.compile(regexOtherAnswer)

regexForStress = r"\bstress\b|\bstressed\b"
regexForTired = r"\btired\b|\bsleep\b|\bexhausted\b|\bexhausting\b|\btiring\b|\bi\b worked.*hard"
regexForLonely = r"\blonely\b|\bmiss\b|\bwant\b.*\bcompany\b|\balone\b|by myself"
regexForWorried = r"\bworried\b|\bscared\b|\banxious\b"
regexForFamily = r"\bfamily\b|\bhusband\b|\bwife\b|\bstep-brother\b|\bbrother\b|\bstep-sister\b|\bsister\b|\bgrandpa\b|\bdad\b|\bgrandma\b|\bmom\b|\bdaddy\b|\bmommy\b|\bgrandson\b|\bson\b|\bgrandfather\b|\bfather\b|\bgrandmother\b|\bmother\b|\bgranddaughter\b|\bdaughter\b|\baunt\b|\buncle\b|\bnephew\b|\bcousin\b|\bgrandparents\b|\bparent\b|\bparents\b"
regexForRelationship = r"long distance relationship|\bgirlfriend\b|\bboyfriend\b|\bcouples\b|my crush|have a crush"
regexForSad = r"\bcrying|\bsad\b|\bbad\b"
regexForDeath = r"passed away|\bdied\b"
regexForSickness = r"\b((i am)|i'm|i feel)\b((?!not).)*\bsick\b|\b(i've|i have)\b((?!not).)*(\bbeen\b).*\bsick\b|stuffy nose|runny nose|sore throat|headache"

regexCheckStress = re.compile(regexForStress)
regexCheckTired = re.compile(regexForTired)
regexCheckLonely = re.compile(regexForLonely)
regexCheckWorried = re.compile(regexForWorried)
regexCheckFamily = re.compile(regexForFamily)
regexCheckRelationship = re.compile(regexForRelationship)
regexCheckSad = re.compile(regexForSad)
regexCheckDeath = re.compile(regexForDeath)
regexCheckSickness = re.compile(regexForSickness)


def detect_category(tracker):
    sickness = False
    for concept in tracker.returnnlp.concept[0]:
        for name in concept.data:
            logger.debug("[COMFORT]: This is name: " + name.description)
            if "illness" in name.description:
                logger.debug("[COMFORT]: Illness is true")
                sickness = True

    if regexCheckDeath.search(tracker.input_text) is not None:
        return "death"
    elif regexCheckSickness.search(tracker.input_text) is not None or sickness is True:
        return "sickness"
    elif regexCheckFamily.search(tracker.input_text) is not None:
        return "family"
    elif regexCheckRelationship.search(tracker.input_text) is not None:
        return "relationship"
    elif regexCheckSad.search(tracker.input_text) is not None:
        return "sad"
    elif regexCheckLonely.search(tracker.input_text) is not None:
        return "lonely"
    elif regexCheckWorried.search(tracker.input_text) is not None:
        return "worried"
    elif regexCheckStress.search(tracker.input_text) is not None:
        return "stress"
    elif regexCheckTired.search(tracker.input_text) is not None:
        return "tired"
    else:
        return "gen"


# MARK: - INIT

class Init(ComfortState):
    name = 'init'

    def run(self,
            dispatcher: Dispatcher,
            tracker: Tracker) -> List[Event]:
        #if the last module was comfort chat should not go inside it
        if dispatcher.ua.module_selection.last_topic_module == TopicModule.SAY_COMFORT.value:
            #TODO: need to implement way of not going back into module
            logger.debug("[COMFORT] said topic again")
            dispatcher.respond_template("normal", {})
            return [NextState('want_talk_about_feelings')]
        if regexCheckTired.search(tracker.input_text) is not None:
            dispatcher.respond_template("tired_handler", {})
            return [NextState('continue_from_tired')]
        else:
            dispatcher.respond_template("normal", {})
            return [NextState('want_talk_about_feelings')]


class ContinueFromTired(ComfortState):
    name = 'continue_from_tired'

    def run(self,
            dispatcher: Dispatcher,
            tracker: Tracker) -> List[Event]:
        regexLongTime = r"\blong time\b|\bforever\b|\bdepress\b|\bdepression\b"
        regexCurrent = r"\bcurrent\b|\bcurrently\b|\bright now\b|\brecent\b|\brecently\b|\bhour\b|\bhours\b|\btoday\b|\bday\b|\bnight time\b|\bday time\b|\bnow\b|\bfrom work\b\ball day\b|\ball night\b"

        regexCheckCurrent = re.compile(regexCurrent)

        #TODO: find way to help with current tiredness
        if regexCheckCurrent.search(tracker.input_text) is not None:
            dispatcher.respond_template("current_tired", {})
            dispatcher.respond_template("joke/ask_tired", {})
            tracker.one_turn_store['joke_limit'] = 0
            return [NextState('joke_tired')]
        else:
            dispatcher.respond_template("normal", {})
            return [NextState('want_talk_about_feelings')]


class HereForYou(ComfortState):
    name = 'here_for_you'

    def run(self,
            dispatcher: Dispatcher,
            tracker: Tracker) -> List[Event]:
        dispatcher.respond_template("exit/here_for_you", {})
        dispatcher.respond_template("exit/here_for_you_transition", {})
        dispatcher.propose_continue("STOP")
        return [NextState('init')]


class TalkAboutFeelings(ComfortState):
    name = 'want_talk_about_feelings'


    def run(self,
            dispatcher: Dispatcher,
            tracker: Tracker) -> List[Event]:
        logger.debug("This is talking:" + str(tracker.returnnlp))
        logger.debug("[COMFORT] this is dialog act: " + str(tracker.returnnlp.dialog_act_label))
        # TODO: talk about why this wont work if remove response template in response to follow up
        if regexCheckNoTalk.search(tracker.input_text) is not None:
            dispatcher.respond_template("exit/misunderstood", {})
            dispatcher.propose_continue("STOP")
            return [NextState('init')]
        if tracker.returnnlp.has_dialog_act(DialogActEnum.NEG_ANSWER) or regexCheckNegative.search(tracker.input_text) is not None or regexCheckOtherAnswer.search(tracker.input_text) is not None:
            dispatcher.respond_template("no_talk", {})
            return [NextState('here_for_you', jump=True)]
        else:
            tracker.one_turn_store['talk_limit'] = 1
            if regexCheckPositive.search(tracker.input_text) is not None or (tracker.returnnlp.has_dialog_act({DialogActEnum.OTHER_ANSWERS, DialogActEnum.POS_ANSWER, DialogActEnum.COMMANDS}) and len(tracker.returnnlp.dialog_act_label) == 1):
                dispatcher.respond_template("want_to_talk", {})
                tracker.one_turn_store['back_channel'] = True
                return [NextState('talking_about_feelings')]
            else:
                return [NextState('talking_about_feelings', jump=True)]


class ReturnToTalking(ComfortState):
    name = 'return_to_talking'

    def run(self,
            dispatcher: Dispatcher,
            tracker: Tracker) -> List[Event]:
        tracker.one_turn_store['talk_limit'] = 1

        dispatcher.respond_template("want_to_talk", {})
        return [NextState('talking_about_feelings')]


class TalkedAboutFeelings(ComfortState):
    name = 'talking_about_feelings'

    def run(self,
            dispatcher: Dispatcher,
            tracker: Tracker) -> List[Event]:

        talk_limit = tracker.last_utt_store.get('talk_limit')

        if talk_limit is None:
            talk_limit = 1

        logger.debug("[COMFORT] nlp of talking about feelings: " + str(tracker.returnnlp))
        logger.debug("This is talk limit: " + str(talk_limit))
        category = detect_category(tracker)
        logger.debug("This is category: " + category)

        logger.debug("[COMFORT] this is dialog act: " + str(tracker.returnnlp.dialog_act_label))

        #if the bot miss-detected them being upset
        if regexCheckNoTalk.search(tracker.input_text) is not None:
            dispatcher.respond_template("exit/misunderstood", {})
            dispatcher.propose_continue("STOP")
            return [NextState('init')]

        back_channel = tracker.last_utt_store.get('back_channel')
        if back_channel is None:
            back_channel = False
        #if the user just said a back-channel or yes response with no actual feelings
        if back_channel is False and talk_limit % 2 == 1 and (regexCheckPositive.search(tracker.input_text) is not None or (tracker.returnnlp.has_dialog_act({DialogActEnum.POS_ANSWER}) and not tracker.returnnlp.has_dialog_act({DialogActEnum.STATEMENT, DialogActEnum.OPINION}))):
            dispatcher.respond_template("want_to_talk", {})
            tracker.one_turn_store['talk_limit'] = talk_limit
            tracker.one_turn_store['back_channel'] = True
            return [NextState('talking_about_feelings')]

        #if the user is done talking after hearing a followup question
        if (talk_limit % 2 == 1 and talk_limit > 1) and (tracker.returnnlp.has_dialog_act(DialogActEnum.NEG_ANSWER) or regexCheckNegative.search(tracker.input_text) is not None or (len(tracker.input_text.split()) <= 3 and talk_limit >= 4) or regexCheckOtherAnswer.search(tracker.input_text) is not None):
            dispatcher.respond_template("exit/here_for_you", {})
            dispatcher.respond_template("joke/ask", {})
            tracker.one_turn_store['joke_limit'] = 0
            return [NextState('joke')]

        system_ack = tracker.module.response_generator_ref.input_data['system_acknowledgement']
        output_tag = system_ack['output_tag']
        acknowledgement_text = system_ack['ack']

        logger.debug("[COMFORT] acknowledgemet:" + str(output_tag) + str(acknowledgement_text))

        #back channel - specific validation
        if talk_limit == 1:
            if output_tag is 'not_sure' or output_tag is 'complaint' or output_tag is 'positive_opinion':
                dispatcher.respond_template("backchannel", {})
            elif acknowledgement_text is '':
                dispatcher.respond_template("backchannel", {})
            else:
                dispatcher.respond(acknowledgement_text)
            validate = "validate/" + category
            dispatcher.respond_template(validate, {})
            tracker.one_turn_store['talk_limit'] = talk_limit + 1
            return [NextState('talking_about_feelings')]
        #back channel - general validation - followup
        elif talk_limit == 2:
            if output_tag is 'not_sure' or output_tag is 'complaint' or output_tag is 'positive_opinion':
                dispatcher.respond_template("backchannel", {})
            elif acknowledgement_text is '':
                dispatcher.respond_template("backchannel", {})
            else:
                dispatcher.respond(acknowledgement_text)
            dispatcher.respond_template("validate/gen", {})
            dispatcher.respond_template("follow-up", {})
            tracker.one_turn_store['talk_limit'] = talk_limit + 1
            return [NextState('talking_about_feelings')]
        #back channel - followup (every even turn)
        else:
            #logger.debug("ENTERED HERE FOR:" + output_tag)
            if talk_limit % 2 == 0:
                if output_tag is 'not_sure' or output_tag is 'complaint' or output_tag is 'positive_opinion':
                    dispatcher.respond_template("backchannel", {})
                elif acknowledgement_text is '':
                    dispatcher.respond_template("backchannel", {})
                else:
                    dispatcher.respond(acknowledgement_text)
                dispatcher.respond_template("follow-up", {})
            else:
                if output_tag is 'not_sure' or output_tag is 'complaint' or output_tag is 'positive_opinion':
                    dispatcher.respond_template("backchannel", {})
                elif acknowledgement_text is '':
                    dispatcher.respond_template("backchannel", {})
                else:
                    dispatcher.respond(acknowledgement_text)
            tracker.one_turn_store['talk_limit'] = talk_limit + 1
            return [NextState('talking_about_feelings')]


class QuestionToTalk(ComfortState):
    name = "question_to_talk"

    def run(self,
            dispatcher: Dispatcher,
            tracker: Tracker) -> List[Event]:
        talk_limit = tracker.last_utt_store.get('talk_limit')

        category = detect_category(tracker)
        #used to see if there is a thank you
        system_ack = tracker.module.response_generator_ref.input_data['system_acknowledgement']
        output_tag = system_ack['output_tag']
        acknowledgement_text = system_ack['ack']

        logger.debug("[COMFORT] acknowledgemet:" + str(output_tag) + str(acknowledgement_text))

        if output_tag is 'not_sure' or output_tag is 'complaint' or output_tag is 'positive_opinion':
            dispatcher.respond_template("backchannel", {})
        elif acknowledgement_text is '':
            dispatcher.respond_template("backchannel", {})
        else:
            dispatcher.respond(acknowledgement_text)
        validate = "validate/" + category
        dispatcher.respond_template(validate, {})

        tracker.one_turn_store['talk_limit'] = talk_limit + 1
        return [NextState('talking_about_feelings')]