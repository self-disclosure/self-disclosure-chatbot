import abc
import logging
from collections import defaultdict
from enum import Enum, IntFlag, auto
from typing import List, Tuple, Optional, Union, Type

import nlu.util_nlu
import utils
from response_generator.modulechat import DA

logger = logging.getLogger(__name__)


def set_logger(child_logger):
    global logger
    logger = child_logger


class UserAttributes:
    def __init__(self):
        self.modulechat = {}
        self.prev_hash = {}


class ProposeContinue(IntFlag):
    CONTINUE, STOP, UNCLEAR = auto(), auto(), auto()


class SysIntent(Enum):
    COMPLAINT = 'complaint'
    ADJUST_SPEAK = 'adjust_speak'
    NOT_COMPLETE = 'not_complete'
    CHANGE_TOPIC = 'change_topic'
    CLARIFY = 'clarify'
    TERMINATE = 'terminate'
    SAY_COMFORT = 'say_comfort'
    SAY_THANKS = 'say_thanks'
    SAY_FUNNY = 'say_funny'
    SAY_BIO = 'say_bio'
    REQ_MORE = 'req_more'
    REQ_EASTEREGGS = 'req_eastereggs'
    REQ_PLAYGAME = 'req_playgame'
    REQ_PLAYMUSIC = 'req_playmusic'
    REQ_TASK = 'req_task'
    REQ_LOCATION = 'req_location'
    REQ_TOPIC = 'req_topic'
    REQ_TOPIC_JUMP = 'req_topic_jump'
    REQ_TOPIC_JUMP_SINGLE = 'req_topic_jump_single'


class DialogAct(Enum):
    STATEMENT = 'statement'
    ACKNOWLEDGEMENT = 'acknowledgement'
    OPINION = 'opinion'
    APPRECIATION = 'appreciation'
    ABANDONED = 'abandoned'
    YES_NO_QUESTION = 'yes_no_question'
    POS_ANSWER = 'pos_answer'
    OPENING = 'opening'
    CLOSING = 'closing'
    OPEN_QUESTION = 'open_question'
    NEG_ANSWER = 'neg_answer'
    OTHER_ANSWERS = 'other_answers'
    OTHER = 'other'
    COMMANDS = 'commands'
    HOLD = 'hold'
    NOT_UNDERSTANDING = 'not_understanding'
    APOLOGY = 'apology'
    THANKING = 'thanking'
    RESPOND_TO_APOLOGIZE = 'respond_to_apologize'


class Sentiment(Enum):
    """
    Enum class representing the lexical information from input_data
    """
    NEGATIVE, POSITIVE, UNKNOWN = 'ans_neg', 'ans_pos', 'ans_unknown'

    @classmethod
    def check(cls, lexical):
        try:
            return Sentiment(lexical)
        except ValueError:
            return Sentiment.UNKNOWN

    @classmethod
    def has_sentiment(cls, sentiment, lexicals: List[str]):
        return any(cls.check(lexical) is sentiment for lexical in lexicals)

    def in_lexical(self, lexicals: List[str]):
        return any(self.value == l for l in lexicals)

    @classmethod
    def for_request(cls, features: dict):
        sentiment = features['sentiment']
        lexical = features['intent_classify']['lexical']

        true_sentiment = nlu.util_nlu.get_sentiment_key(sentiment, lexical)
        if true_sentiment == 'pos':
            return cls.POSITIVE
        elif true_sentiment == 'neg':
            return cls.NEGATIVE
        else:
            return cls.UNKNOWN

        # TODO: incorporate lexical

        # if {sentiment} & {'pos', 'neu', 'compound'}:
        #     return cls.POSITIVE
        # elif {sentiment} & {'neg'}:
        #     return cls.NEGATIVE
        # else:  # 'empty'
        #     return cls.UNKNOWN


class QuestionFlag(Enum):
    def _generate_next_value_(name, start, count, last_values):
        return name


class Keyword:

    @classmethod
    def fromknowledge(cls, knowledge):
        return cls(name=knowledge[0].lower(), from_knowledge=True, confidence=knowledge[2])

    @classmethod
    def fromdict(cls, dict: dict):
        try:
            return cls(dict['_input_name'], dict['from_knowledge'], dict['confidence'])
        except KeyError as e:
            raise KeyError(
                "Keyword is given an invalid dict object for initialization. {}".format(e))

    @classmethod
    def fromhardcode(cls, name):
        return cls(name, False, None)

    def __init__(self, name: str, from_knowledge: bool, confidence: Optional[float]):
        self._input_name = name
        self.from_knowledge = from_knowledge
        self.confidence = confidence

    def __str__(self):
        return "{}({})".format(self.__class__.__name__, self.__dict__)


class State(Enum):

    def _generate_next_value_(name, start, count, last_values):
        return name

    @classmethod
    @abc.abstractmethod
    def default(cls) -> 'State':
        return

    @classmethod
    @abc.abstractmethod
    def interruption(cls, automaton: 'Automaton') -> Optional['State']:
        return

    def transduce(self, automaton) -> Tuple[str, ProposeContinue]:
        """
        Core function for the state to perform transition.
        :param automaton: a reference to the GameAutomaton
        :return: see ContextManager._Decorator.state()
        """
        # TODO: ASR errors, incoherence
        interrupt_state = self.interruption(automaton)

        if interrupt_state is not None:
            automaton.cm.curr_state = interrupt_state
            next_state, params = automaton.__getattribute__(
                't_' + interrupt_state.name)()
        else:
            next_state, params = automaton.__getattribute__('t_' + self.name)()
        return automaton.__getattribute__('s_' + next_state.name)(params)


class ContextManager:
    """
    GameAutomaton is created at every utterances, this class is the API class for interacting with user_attributes,
    which has a persistent, per-user, store.
    """

    def __init__(self, session_id, user_attributes_ref, state_class: Type[State]):
        self._user_attributes = user_attributes_ref  # type: UserAttributes
        self.state_class = state_class

        if not self.curr_state:
            self.curr_state = self.state_class.default()

        if not self.modulechat.get('prev_state_params'):
            self.modulechat['prev_state_params'] = {}

        if self.modulechat.get('session_id') != session_id:
            self.modulechat['session_id'] = session_id
            self.flush()

        # MARK: - references

    @property
    def user_attributes(self):
        return self._user_attributes

    @property
    @abc.abstractmethod
    def modulechat(self):
        # return self.user_attributes.gamechat
        return {}

    # MARK: - Getter/Setter - System

    @property
    def curr_state(self) -> Optional[State]:
        return self.state_class(self.modulechat.get('curr_state')) \
            if self.modulechat.get('curr_state') else None

    @curr_state.setter
    def curr_state(self, state: State):
        self.modulechat['curr_state'] = state.value

    @property
    def prev_state(self) -> Optional[State]:
        return self.state_class(self.modulechat.get('prev_state')) \
            if self.modulechat.get('curr_state') else None

    @prev_state.setter
    def prev_state(self, state: State):
        self.modulechat['prev_state'] = state.value if state else None

    @property
    def prev_state_params(self) -> dict:
        return self.modulechat['prev_state_params']

    @property
    def user_disclosure(self):
        if not self.user_attributes.get('user_disclosure'):
            self.user_attributes['user_disclosure'] = defaultdict(
                lambda: defaultdict(list))
        return self.user_attributes['user_disclosure']

    @property
    def counters(self) -> defaultdict:
        if not self.modulechat.get('counters'):
            self.modulechat['counters'] = defaultdict(lambda: 0)
        return self.modulechat['counters']

    # MARK: - APIs

    def flush(self):
        self.curr_state = self.state_class.default()
        self.prev_state = None
        self.prev_state_params.clear()


class Automaton:

    # MARK: - Classes

    class _Decorators:

        @classmethod
        def transition(cls, func):
            """
            Wrapper that uses the function as a transition state.
            :param func: function that must be named: 't_<state_name>'
                        function must return either:
                            None (goes to state with same name),
                            State,
                            None, params (params get passed to the state)
                            State, params
            :return: a decorated function
            """
            def wrapper(automaton):
                resp = func(automaton)
                if isinstance(resp, tuple):
                    next_state, params = resp  # type: Tuple[State, dict]
                else:
                    next_state = resp  # type: State
                    params = {}

                params['t_'] = automaton.cm.curr_state

                if next_state:
                    automaton.cm.curr_state = next_state
                    return next_state, params
                else:
                    return automaton.cm.curr_state, params
            return wrapper

        @classmethod
        def state(cls, func):
            """
            Wrapper that uses the function as a state.
            :param func: function that must be named: 's_<state_name>'
                        function must return:
                            utterance: str, next_state: States, propose_continue: ProposeContinue
            :return: wrapper will set the next state into user_attributes,
                    and return utterance and propose_continue
            """
            def wrapper(automaton, params):
                utt, next_state, propose_continue = func(automaton, params)
                automaton.cm.prev_state = automaton.cm.curr_state
                automaton.cm.curr_state = next_state
                return utt, propose_continue
            return wrapper

    # MARK: - Properties

    @classmethod
    def from_rg(cls, response_generator_ref, context_manager_class: Type[ContextManager], state_class: Type[State]):
        ctx = context_manager_class(session_id=response_generator_ref.state_manager.current_state.session_id,
                                    user_attributes_ref=response_generator_ref.state_manager.user_attributes,
                                    state_class=state_class)
        return cls(input_data=response_generator_ref.input_data,
                   context_manager=ctx)

    def __init__(self, input_data, context_manager: ContextManager):
        self.cm = context_manager
        self.input_data = input_data

    # MARK: - API

    def response(self):
        """
        Main API for getting a response from the automaton.
        :return: a dict with utterance and other flags as specified by the selecting strategy.
        """
        utt, propose_continue = self.cm.curr_state.transduce(automaton=self)

        self.cm.modulechat['propose_continue'] = propose_continue.name
        return utt

        # return {
        #     'response': utt,
        #     'propose_continue': propose_continue.name,
        #     # 'z_cm': (self.cm.prev_state, self.cm.curr_state)
        # }

    # MARK: - Internal API

    @property
    def features(self) -> dict:
        return self.input_data['features']

    @property
    def coreference(self) -> dict:
        return self.features.get('coreference') if self.features else None

    @property
    def input_text(self) -> str:
        return self.coreference_text if self.coreference_text else self.input_text_raw

    @property
    def input_text_raw(self) -> str:
        return self.input_data['text']

    @property
    def coreference_text(self) -> str:
        # return self.features.get('coreference', {}).get('text') if self.features else None
        return self.coreference.get('text') if self.coreference else None

    @property
    def sys_intent(self) -> Tuple[SysIntent]:
        return tuple(SysIntent(i) for i in self.features['intent_classify']['sys'])

    @property
    def lexicals(self):
        return self.features['intent_classify']['lexical']

    @property
    def keywords(self):
        return [Keyword(k['keyword'], True, k['confidence'])
                for k in self.features['topic'][0]['topicKeywords']]

    @property
    def coreference_knowledge(self):
        return self.coreference.get('kg') if self.coreference else None

    @property
    def input_knowledge(self):
        return self.features['knowledge']

    @property
    def knowledge(self):
        return self.coreference_knowledge if self.coreference_knowledge else [] + \
            self.input_knowledge if self.input_knowledge else []

    @property
    def concept(self):
        return self.features['concept']

    @property
    def dialog_act(self) -> DialogAct:
        if not self.features:
            return []
        return self.features.get('dialog_act')  # type: List[dict]


    def dialog_act_has(self, da: Union[DA, List[DA]]):
        if isinstance(da, DA):
            da = [da]
        r = any(any(d.get('DA') == a.value for a in da)
                for d in self.dialog_act)
        return r


    # MARK: - States

    """
    States are used to create the utterance
    Transitions determine what the next state is.
    Each state will specify what the next intended state is. e.g. intro -> interesting_comment,
    however, user has the ability to interrupt that by saying e.g. no, or the system can interrupt the flow.
    So the state transition always lands on the transition state for the target state,
    e.g. s_intro -> t_interesting_comment,
    and the transition state is responsible for redirecting state transitions.
    State itself is only responsible for generating the utterance
    """


# MARK: - Utils

def format_multiline_utterance(sentence: str):
    try:
        print('sdfijsidfjosdfjsdoifjs', sentence)
        utt, key = tuple(s for s in sentence.strip('\n').split('\n'))
        return utt.lstrip('utt: '), key.lstrip('key: ')
    except ValueError as e:
        raise ValueError(
            "Sentence is not a multiline utterance: {}".format(sentence))
