from response_generator.fsm2 import Dispatcher, Tracker, NextState
from .utils import  *
from response_generator.self_disclosure import states
from response_generator.self_disclosure.states.base import SelfDisclosureState
from user_profiler.user_profile import UserProfile
from selecting_strategy.topic_module_proposal import EntityProposer

def remember_user_details(user_profile):
    # Result to return
    utt_to_say = []
    num_hits = 0
    MAX_NUM_HITS = 3

    # Movie
    liked_movies = user_profile.topic_module_profiles.movie.liked_movie_ids

    if liked_movies != [] and num_hits != MAX_NUM_HITS:
        utt_to_say + [f"the movie {liked_movies[0]}"]
        num_hits += 1

    # Music
    music = user_profile.topic_module_profiles.music.fav_artists

    logging.info("testing_self_disclosure")
    logging.info(music)
    logging.info("testing_self_disclosure")

    if music != [] and num_hits != MAX_NUM_HITS:
        utt_to_say + [f"the artist {music[0]}"]
        num_hits += 1

    if utt_to_say == []:
        return "Sorry, I don't think I got a chance to talk to you long enough last time!"

    return "I remember you like " + ", and".join(utt_to_say)



class Initial(SelfDisclosureState):
    name = 'initial'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):


        if re.search(r"(.*)(\bwhat\b)(.*)(\bremember\b|\bknow\b)(.*)(\babout\b)(.*)(\bme\b|\bmyself\b)", tracker.input_text.lower()):
            propose = EntityProposer(tracker.user_attributes.ua_ref)
            dispatcher.respond(propose.propose_entity_in_topic_profile())
            dispatcher.propose_continue('STOP')
            return [NextState('initial', jump=False)]

        if re.search(r"(.*)(what)(.*)(you)(.*)(name)(.*)", tracker.input_text.lower()):
            dispatcher.respond("Sorry i cannot tell you because i'm in the competition.")
            dispatcher.respond("But i can share with you what i'm up to recently, are you interested?")
            return [NextState('a2', jump=False)]

        if re.search(r"(.*)(talk|chat|discuss)(.*)(me|myself)(.*)", tracker.input_text.lower()):
            dispatcher.respond("I'd love to talk about you. ")
            return [NextState('q3', jump=True)]

        return [NextState('q1', jump=True)]

class SDQ1(SelfDisclosureState):
    name = "q1"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond("Sure, I'd love too. As you know, I'm a chatbot, and my dream is to push the limits of artificial intelligence.")
        return [NextState('q2', jump=True)]

class SDQ2(SelfDisclosureState):
    name = "q2"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond("If you want, I could tell you more about my dreams and aspirations?")
        dispatcher.propose_continue("CONTINUE")
        return [NextState('a2', jump=False)]

class SDA2(SelfDisclosureState):
    name = "a2"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if is_neg(tracker):
            simple_ack(dispatcher, tracker)
            dispatcher.propose_continue("STOP")
            return [NextState('initial', jump=False)]
        dispatcher.respond("Well, right now I'm competing in the Amazon Alexa prize competition, and it is one of my dreams to win the prize and make my team of engineers really happy!")
        # dispatcher.respond("We all work hard every day to try make our dreams a reality.")
        return [NextState('q3', jump=True)]

class SDQ3(SelfDisclosureState):
    name = "q3"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        ua_ref = tracker.user_attributes.ua_ref
        user_profile = UserProfile(ua_ref)
        name = user_profile.username
        if name and name.strip() != "":
            dispatcher.respond(f"{name}, do you have any dreams or aspirations you would like to share with me?")
        else:
            dispatcher.respond("Do you have any dreams or aspirations you would like to share with me?")
        dispatcher.propose_continue("CONTINUE")
        return [NextState('a3', jump=False)]

class SDA3(SelfDisclosureState):
    name = "a3"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if is_pos(tracker) and (re.search(r"\b(yes i do)\b", tracker.input_text.lower()) or len(tracker.input_text.lower().split(" ")) < 5):
            dispatcher.respond("Great! Tell me more!")
            return [NextState('a4', jump=False)]
        elif is_pos(tracker) or is_neu(tracker):
            return [NextState('a4', jump=True)]
        else:
            simple_ack(dispatcher, tracker)
            dispatcher.propose_continue("STOP")
            return [NextState('initial', jump=False)]

class SDA4(SelfDisclosureState):
    name = "a4"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if is_neg(tracker):
            simple_ack(dispatcher, tracker)
        else:
            ua_ref = tracker.user_attributes.ua_ref
            user_profile = UserProfile(ua_ref)
            name = user_profile.username
            if name and name.strip() != "":
                dispatcher.respond(f"Thank you for sharing with me {name}. I feel like I have gotten to know you a bit better!")
            else:
                dispatcher.respond("Thank you for sharing with me. I feel like I have gotten to know you a bit better!")
        dispatcher.propose_continue("STOP")
        return [NextState('initial', jump=False)]

from question_handling.quetion_handler import QuestionHandler, QuestionResponse, QuestionResponseTag
from nlu.question_detector import QuestionDetector
from template_manager.manager import Template, TemplateManager
from typing import Optional, List
class QuestionHandlerGlobalState(SelfDisclosureState):
    name = 'handle_questions'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:
        ignore = [r"(.*)(what)(.*)(you)(.*)(name)(.*)", r"(.*)(talk|chat|discuss)(.*)(me|myself)(.*)", r"(.*)(\bwhat\b)(.*)(\bremember\b|\bknow\b)(.*)(\babout\b)(.*)(\bme\b|\bmyself\b)"]
        for item in ignore:
            if re.search(item, tracker.input_text.lower()): return None
        return_nlp_input = tracker.input_data['returnnlp']

        if QuestionDetector(tracker.input_text, return_nlp_input).has_question():
            return QuestionHandlerGlobalState.name

    def stop_bot(self, dispatcher, tracker):
        simple_ack(dispatcher, tracker)
        dispatcher.propose_continue('STOP')
        return [NextState('initial', jump=False)]

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        # Get input data
        return_nlp_input = tracker.input_data['returnnlp']
        system_ack = tracker.input_data['system_acknowledgement']
        user_attributes_ref = tracker.user_attributes.ua_ref

        # Load the handler
        handler = QuestionHandler(complete_user_utterance=tracker.input_text,
                                  returnnlp=return_nlp_input,
                                  backstory_threshold=0.85, system_ack=system_ack,
                                  user_attributes_ref=user_attributes_ref)

        # If we cannot handle the question
        detector = QuestionDetector(tracker.input_text, return_nlp_input)
        do_not_answer = detector.is_ask_back_question() or detector.is_follow_up_question()
        if do_not_answer:
            answer = handler.generate_i_dont_know_response()
            dispatcher.respond(answer)
            return self.stop_bot(dispatcher, tracker)

        # General question handler
        answer = handler.handle_question()
        dispatcher.respond(answer.response)
        if answer and answer.bot_propose_module and \
            answer.bot_propose_module != TopicModule.SELFDISCLOSURE:
            tm = TemplateManager(Template.transition, user_attributes_ref)
            result_string = tm.speak(f"propose_topic_short/{answer.bot_propose_module.value}", {})
            dispatcher.respond(result_string)
            dispatcher.propose_continue('UNCLEAR', answer.bot_propose_module)
            return self.stop_bot(dispatcher, tracker)

        return self.stop_bot(dispatcher, tracker)