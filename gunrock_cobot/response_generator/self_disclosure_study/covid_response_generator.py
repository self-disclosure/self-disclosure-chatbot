import logging
from dataclasses import dataclass, asdict, field

from cobot_core.service_module import LocalServiceModule
from .tracker import Tracker
from .dispatcher import Dispatcher
from .context import State
from selecting_strategy.module_selection import ModuleSelector, ProposeContinue
from nlu.constants import DialogAct, Positivity, TopicModule
from nlu.question_detector import QuestionDetector
from question_handling.quetion_handler import QuestionHandler, QuestionResponse, QuestionResponseTag

from nlu.dataclass.returnnlp import ReturnNLP, ReturnNLPSegment

import template_manager

class CovidResponseGenerator(LocalServiceModule):

    def execute(self):
        self.text = self.input_data["text"].lower()
        returnnlp_raw = getattr(self.state_manager.current_state, "returnnlp")
        self.returnnlp = ReturnNLP.from_list(returnnlp_raw)

        self.user_attributes = self.state_manager.user_attributes
        self.tracker = Tracker(self.state_manager)
        self.dispatcher = Dispatcher(self.user_attributes, self.get_abtest_condition())
        self.question_detector = QuestionDetector(self.text, returnnlp_raw)
        self.question_handler = QuestionHandler(
            self.text, returnnlp_raw, 0.83, self.tracker.system_acknowledgement, self.tracker.user_attributes)

        result = self.transduce()

        logging.info(f"[transduce] result: {result}")

        return result

    def get_abtest_condition(self):
        ab_test_condition = getattr(self.state_manager.user_attributes, "self-disclosure-study-abtest", "A")
        logging.info(f"ab_test_condition: {ab_test_condition}")

        return ab_test_condition

    def transduce(self):
        if self.tracker.enter_from_other_module:
            self.tracker.propose_continue = ProposeContinue.CONTINUE

        current_context = self.tracker.module_context
        logging.info(f"current context: {current_context}")

        state_handler = self.get_function_from_name("t_", current_context.current_state)
        logging.info(f"state_handler: {state_handler}")
        state_handler()

        responses = [
            self.dispatcher.response_text.ack,
            self.dispatcher.response_text.transition,
            self.dispatcher.response_text.main_content,
        ]

        final_response = " ".join(filter(None, responses))

        setattr(self.state_manager.user_attributes, "covidchat", asdict(self.tracker.module_context))

        logging.info(f"covid response result: {final_response}")


        return final_response

    # ========
    def get_function_from_name(self, prefix, function_name):
        return getattr(self, "{}{}".format(prefix, function_name))

    def t_init(self):
        self.s_greeting()

    def s_greeting(self):
        logging.info("s_greeting")
        self.dispatcher.main_content = self.dispatcher.generate_response("greeting/question")
        self.tracker.current_state = State.GREETING

    def t_greeting(self):
        logging.info("t_greeting")
        self.try_handle_question()

        self.try_use_system_ack()

        if not self.dispatcher.ack:
            if self.returnnlp.answer_positivity is Positivity.neg:
                self.dispatcher.ack = self.dispatcher.generate_response("greeting/ack/neg")
            if self.returnnlp.answer_positivity is Positivity.pos or len(self.text.split()) >= 5:
                self.dispatcher.ack = self.dispatcher.generate_response("greeting/ack/long_answer")
            else:
                self.dispatcher.ack = self.dispatcher.generate_response("greeting/ack/general")

        self.s_what_do_you_do_to_keep_busy()

    def s_what_do_you_do_to_keep_busy(self):
        logging.info("s_what_do_you_do_to_keep_busy")
        self.dispatcher.main_content = self.dispatcher.generate_response("what_do_you_do_to_keep_busy/question")
        self.tracker.current_state = State.WHAT_DO_YOU_DO_TO_KEEP_BUSY

    def t_what_do_you_do_to_keep_busy(self):
        self.try_handle_question()

        self.try_use_system_ack()

        if not self.dispatcher.ack:
            if self.returnnlp.answer_positivity is Positivity.neg:
                self.dispatcher.ack = self.dispatcher.generate_response("what_do_you_do_to_keep_busy/ack/neg")
            if len(self.text.split()) >= 5:
                self.dispatcher.ack = self.dispatcher.generate_response("what_do_you_do_to_keep_busy/ack/long_answer")
            else:
                self.dispatcher.ack = self.dispatcher.generate_response("what_do_you_do_to_keep_busy/ack/general")

        self.s_online_shopping()

    def s_online_shopping(self):
        self.dispatcher.main_content = self.dispatcher.generate_response("online_shopping/question")
        self.tracker.current_state = State.ONLINE_SHOPPING

    def t_online_shopping(self):
        self.try_handle_question()
        self.try_generate_general_acknowledgement()
        self.s_diet_change()

    def s_diet_change(self):
        self.dispatcher.main_content = self.dispatcher.generate_response("diet_change/question")
        self.tracker.current_state = State.DIET_CHANGE

    def t_diet_change(self):
        self.try_handle_question()
        self.try_generate_general_acknowledgement()
        self.s_feel_down()

    def s_feel_down(self):
        self.dispatcher.main_content = self.dispatcher.generate_response("feel_down/question")
        self.tracker.current_state = State.FEEL_DOWN

    def t_feel_down(self):
        self.try_handle_question()
        self.try_use_system_ack()

        if not self.dispatcher.ack:
            if self.returnnlp.answer_positivity is Positivity.neg:
                self.dispatcher.ack = self.dispatcher.generate_response("feel_down/ack/negative")
            elif self.returnnlp.answer_positivity is Positivity.pos:
                self.dispatcher.ack = self.dispatcher.generate_response("feel_down/ack/positive")
            else:
                self.try_generate_general_acknowledgement()

        self.s_physical_distance()

    def s_physical_distance(self):
        self.dispatcher.main_content = self.dispatcher.generate_response("physical_distance/question")
        self.tracker.current_state = State.PHYSICAL_DISTANCE

    def t_physical_distance(self):
        self.try_handle_question()
        self.try_generate_general_acknowledgement()
        self.s_self_care_recommendation_content()


    def s_self_care_recommendation_content(self):
        self.dispatcher.main_content = self.dispatcher.generate_response("self_care_practices/suggestion")
        self.tracker.current_state = State.SELF_CARE_RECOMMENDATION_CONTENT

    def t_self_care_recommendation_content(self):
        if self.tracker.returnnlp.answer_positivity is Positivity.pos or \
            self.tracker.returnnlp.opinion_positivity is Positivity.pos:
            self.dispatcher.ack = self.dispatcher.generate_response("self_care_practices/ack/positive")

        self.try_handle_question()
        self.try_generate_general_acknowledgement()
        self.s_ending()

    def s_ending(self):
        if self.tracker.is_first_conversation:
            self.dispatcher.main_content = self.dispatcher.generate_response("ending_first_conv")
        else:
            self.dispatcher.main_content = self.dispatcher.generate_response("ending_last_conv")

        self.tracker.module_context.propose_continue = "STOP"

    def try_handle_question(self):
        if not self.question_detector.has_question():
            return

        if self.dispatcher.ack:
            # if acknowledgement exists, that means it's already handled properly
            return

        answer = self.question_handler.handle_question()
        if answer:
            self.dispatcher.ack = answer.response
            self.dispatcher.transition = self.dispatcher.generate_response("transition_change_subtopic")

    def try_generate_general_acknowledgement(self):
        if self.dispatcher.ack:
            return False

        use_system_ack = self.try_use_system_ack()
        if use_system_ack:
            return True

        if self.tracker.returnnlp.answer_positivity is Positivity.neg or self.tracker.ans_not_sure():
            self.dispatcher.ack = self.dispatcher.generate_response("ack/negative")
            return
        if self.returnnlp.has_dialog_act([DialogAct.OPINION, DialogAct.COMMENT, DialogAct.STATEMENT]):
            if len(self.tracker.text.split()) >= 10:
                self.dispatcher.ack = self.dispatcher.generate_response("ack/general/medium")
            else:
                self.dispatcher.ack = self.dispatcher.generate_response("ack/general/short")
        else:
            self.dispatcher.ack = "I see. "

    def try_use_system_ack(self):
        if self.dispatcher.ack:
            return False
        if self.tracker.system_ack_tag in \
                ["ack_opinion", "positive_opinion", "appreciation", "thanking", "opinion_crazy",
                 "opinion_surprising", "not_sure"]:
            self.dispatcher.ack = self.tracker.system_ack_text

            return True
        return False