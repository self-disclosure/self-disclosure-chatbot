import logging
import re

from nlu.dataclass.returnnlp import ReturnNLP, ReturnNLPSegment
from nlu.constants import DialogAct, Positivity, TopicModule
from response_generator.self_disclosure_study.context import CovidContext
from selecting_strategy.module_selection import ModuleSelector, ProposeContinue


class Tracker:
    def __init__(self, state_manager):
        self.user_attributes = state_manager.user_attributes
        self.user_id = getattr(state_manager.user_attributes, "user_id")
        self.system_acknowledgement = getattr(state_manager.current_state, "system_acknowledgement")

        self.text = getattr(state_manager.current_state, "text")
        self.returnnlp = ReturnNLP.from_list(getattr(state_manager.current_state, "returnnlp"))


        module_context = getattr(state_manager.user_attributes, "covidchat")
        self.module_context = CovidContext.from_dict(module_context)

        self.module_selector = ModuleSelector(self.user_attributes)


    @property
    def is_first_conversation(self):
        s = set(self.module_selector.used_topic_modules)
        s -= {TopicModule.COVIDCHAT.value}
        result = not s
        return result

    @property
    def enter_from_other_module(self):
        last_topic = self.module_selector.last_topic_module
        return last_topic != TopicModule.COVIDCHAT.value

    @property
    def current_state(self):
        return self.module_context.current_state

    @current_state.setter
    def current_state(self, state):
        logging.info(f"covid set current_state: {state}")
        self.module_context.current_state = state

    @property
    def propose_continue(self):
        return self.module_context.propose_continue

    @propose_continue.setter
    def propose_continue(self, value: ProposeContinue):
        self.module_context.propose_continue = value.value

    @property
    def system_ack_tag(self):
        if self.system_acknowledgement:
            return self.system_acknowledgement.get("output_tag")
        return ""

    @property
    def system_ack_text(self):
        if self.system_acknowledgement:
            return self.system_acknowledgement.get("ack")
        return ""

    def ans_not_sure(self):
        not_sure_regex = r"(.*)(not sure|don't know|do not know|don't have one|do not have one)(.*)"

        return self.returnnlp.has_dialog_act(DialogAct.OTHER_ANSWERS) or \
               self.returnnlp.has_intent("ans_unknown") or \
               re.search(not_sure_regex, self.text)
