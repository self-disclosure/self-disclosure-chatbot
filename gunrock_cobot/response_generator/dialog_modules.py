from cobot_core.service_url_loader import ServiceURLLoader
from cobot_core.service_module import RemoteServiceModule

from response_generator.social_transition.launchgreeting import LaunchGreeting_ResponseGenerator
from response_generator.game.response_generator import GameResponseGeneratorLocal
from response_generator.food.response_generator import FoodResponseGeneratorLocal
from response_generator.music.response_generator import MusicResponseGeneratorLocal
from response_generator.animal.response_generator import AnimalResponseGeneratorLocal
from response_generator.fashion.response_generator import FashionResponseGeneratorLocal
from response_generator.holiday.response_generator import HolidayFunfacts_ResponseGenerator
from response_generator.travel.response_generator import Travel_Response_Generator_Local
from response_generator.outdoor.response_generator import OutdoorResponseGeneratorLocal
from response_generator.special.tellstory_funfact.response_generator import TellStoryFunfact_ResponseGenerator
from response_generator.social_transition.phatic_response_generator import Phatic_ResponseGenerator
# from response_generator.social_transition.dailylife_response_generator import Dailylife_ResponseGenerator
from response_generator.social_transition.dailylife.response_generator import DailyLifeResponseGenerator
from response_generator.social_transition.social_response_generator import Social_ResponseGenerator
from response_generator.comfort.response_generator import ComfortResponseGeneratorLocal
from response_generator.self_disclosure.response_generator import SelfDisclosureResponseGenerator
from response_generator.retrieval.response_generator import TopicRetrievalResponseGenerator
from response_generator.special.adjust_speak_response_generator import AdjustSpeak_ResponseGenerator
# from response_generator.special.jokes.response_generator import JokesResponseGenerator
from response_generator.special.special_response_generator import ReqMore_ResponseGenerator
from response_generator.special.special_response_generator import ReqTime_ResponseGenerator
from response_generator.special.special_response_generator import ReqEasterEgg_ResponseGenerator
from response_generator.special.special_response_generator import SayThanks_ResponseGenerator
from response_generator.special.req_task_response_generator import ReqTask_ResponseGenerator
from response_generator.special.special_response_generator import ReqLoc_ResponseGenerator
from response_generator.special.jokes.response_generator import JokesResponseGenerator
from response_generator.special.terminate_response_generator import Terminate_ResponseGenerator
from response_generator.special.req_play_game_response_generator import ReqPlayGame_ResponseGenerator
from response_generator.special.req_play_music_response_generator import ReqPlayMusic_ResponseGenerator
from response_generator.special.clarification_response_generator import Clarification_ResponseGenerator
from response_generator.special.controversial_opinion.response_generator import ControversialOpinion_ResponseGenerator


class DialogModules:
    def get_dialog_modules(self):
        return [
            self.LaunchGreetingBot,
            self.SocialBot,
            self.TerminateBot,
            self.MovieTVBot,
            self.BookBot,
            self.MusicBot,
            self.SportBot,
            self.FoodBot,
            self.AnimalBot,
            self.TravelBot,
            self.TechScienceBot,
            self.GameBot,
            self.FashionBot,
            self.NewsBot,
            self.WeatherBot,
            self.HolidayBot,
            self.TopicRetrievalBot,
            self.SayFunnyBot,
            self.PhaticBot,
            self.DailylifeBot,
            self.OutDoorBot,
            self.SayThanksBot,
            self.SayComfortBot,
            self.SelfDisclosureBot,
            self.StoryFunfactBot,
            self.ControOpinionBot,
            self.ReqEasterEggBot,
            self.ClarificationBot,
            self.AdjustSpeakBot,
            self.ReqTimeBot,
            self.ReqMoreBot,
            self.ReqPlayGameBot,
            self.ReqPlayMusicBot,
            self.ReqTaskBot,
            self.ReqLocBot
        ]

    LaunchGreetingBot = {
        'name': "LAUNCHGREETING",
        'class': LaunchGreeting_ResponseGenerator,
        'url': 'local',
        'context_manager_keys': ['asr', 'text', 'features', 'central_elem', 'returnnlp', 'a_b_test', 'system_acknowledgement', "bot_ask_open_question"],
    }

    AdjustSpeakBot = {
        'name': "ADJUSTSPEAK",
        'class': AdjustSpeak_ResponseGenerator,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'central_elem', 'returnnlp', 'a_b_test', 'system_acknowledgement', "bot_ask_open_question"]
    }

    TerminateBot = {
        'name': "TERMINATE",
        'class': Terminate_ResponseGenerator,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'central_elem', 'returnnlp', 'a_b_test', 'system_acknowledgement', "bot_ask_open_question"]
    }

    ClarificationBot = {
        'name': "CLARIFICATION",
        'class': Clarification_ResponseGenerator,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'central_elem', 'returnnlp', 'a_b_test', 'system_acknowledgement', "bot_ask_open_question"]
    }

    SayComfortBot = {
        'name': "SAY_COMFORT",
        'class': ComfortResponseGeneratorLocal,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'central_elem', 'returnnlp', 'a_b_test', 'system_acknowledgement', "bot_ask_open_question"]
    }

    SayThanksBot = {
        'name': "SAY_THANKS",
        'class': SayThanks_ResponseGenerator,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'central_elem', 'returnnlp', 'a_b_test', 'system_acknowledgement', "bot_ask_open_question"]
    }

    SayFunnyBot = {
        'name': "SAY_FUNNY",
        'class': JokesResponseGenerator,
        # 'class': JokesResponseGenerator,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'central_elem', 'returnnlp', 'a_b_test', 'system_acknowledgement', "bot_ask_open_question"]
    }

    ReqMoreBot = {
        'name': "REQ_MORE",
        'class': ReqMore_ResponseGenerator,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'central_elem', 'returnnlp', 'ner', 'a_b_test', 'system_acknowledgement',  "bot_ask_open_question"]
    }

    ReqTimeBot = {
        'name': "REQ_TIME",
        'class': ReqTime_ResponseGenerator,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'central_elem', 'returnnlp', 'a_b_test', 'system_acknowledgement',  "bot_ask_open_question"]
    }

    ReqEasterEggBot = {
        'name': "REQ_EASTEREGG",
        'class': ReqEasterEgg_ResponseGenerator,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'central_elem', 'returnnlp', 'a_b_test', 'system_acknowledgement', "bot_ask_open_question"]
    }

    ReqPlayGameBot = {
        'name': "PLAY_GAME",
        'class': ReqPlayGame_ResponseGenerator,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'central_elem', 'returnnlp', 'a_b_test', 'system_acknowledgement', "bot_ask_open_question"]
    }

    ReqPlayMusicBot = {
        'name': "PLAY_MUSIC",
        'class': ReqPlayMusic_ResponseGenerator,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'central_elem', 'returnnlp', 'a_b_test', 'system_acknowledgement', "bot_ask_open_question"]
    }

    ReqTaskBot = {
        'name': "REQ_TASK",
        'class': ReqTask_ResponseGenerator,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'central_elem', 'returnnlp', 'a_b_test', 'system_acknowledgement', "bot_ask_open_question"]
    }

    ReqLocBot = {
        'name': "REQ_LOC",
        'class': ReqLoc_ResponseGenerator,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'central_elem', 'returnnlp', 'a_b_test', 'system_acknowledgement', "bot_ask_open_question"]
    }

    PhaticBot = {
        'name': "PHATIC",
        'class': Phatic_ResponseGenerator,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'central_elem', 'returnnlp', 'a_b_test', 'system_acknowledgement', "bot_ask_open_question"]
    }

    DailylifeBot = {
        'name': "DAILYLIFE",
        # 'class': Dailylife_ResponseGenerator,
        'class': DailyLifeResponseGenerator,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'central_elem', 'returnnlp', 'a_b_test', 'system_acknowledgement', "bot_ask_open_question"]
    }

    SelfDisclosureBot = {
        'name': "SELFDISCLOSURE",
        # 'class': Dailylife_ResponseGenerator,
        'class': SelfDisclosureResponseGenerator,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'central_elem', 'returnnlp', 'a_b_test', 'system_acknowledgement', "bot_ask_open_question"]
    }

    ControOpinionBot = {
        'name': "CONTROVERSIALOPION",
        'class': ControversialOpinion_ResponseGenerator,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'central_elem', 'returnnlp', 'a_b_test', 'system_acknowledgement', "bot_ask_open_question"]
    }


    AnimalBot = {
        'name': "ANIMALCHAT",
        'class': AnimalResponseGeneratorLocal,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'features', 'central_elem', 'returnnlp', 'a_b_test', 'system_acknowledgement', "bot_ask_open_question"]
    }

    HolidayBot = {
        'name': "HOLIDAYCHAT",
        'class': HolidayFunfacts_ResponseGenerator,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'central_elem', 'returnnlp',  'a_b_test', 'system_acknowledgement', "bot_ask_open_question"]
    }

    FoodBot = {
        'name': "FOODCHAT",
        'class': FoodResponseGeneratorLocal,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'central_elem', 'returnnlp', 'a_b_test', 'system_acknowledgement', "bot_ask_open_question"]
    }

    FashionBot = {
        'name': "FASHIONCHAT",
        'class': FashionResponseGeneratorLocal,

        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'a_b_test', 'central_elem', 'returnnlp', 'system_acknowledgement', "bot_ask_open_question"]
    }

    OutDoorBot = {
        'name': "OUTDOORCHAT",
        'class': OutdoorResponseGeneratorLocal,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'central_elem', 'returnnlp', 'a_b_test',
                                 'system_acknowledgement']
    }

    SocialBot = {
        'name': "SOCIAL",
        'class': Social_ResponseGenerator,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'central_elem', 'returnnlp', 'system_acknowledgement', 'a_b_test', "bot_ask_open_question"]
    }

    GameBot = {
        'name': "GAMECHAT",
        'class': GameResponseGeneratorLocal,
        'url': 'local',
        'context_manager_keys': ['intent', 'intent_classify', 'slots', 'text', 'topic', 'knowledge', 'features', 'a_b_test', 'central_elem', 'returnnlp', 'system_acknowledgement', "bot_ask_open_question"]
    }

    TravelBot = {
        'name': "TRAVELCHAT",
        'class': Travel_Response_Generator_Local,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'a_b_test', 'central_elem', 'returnnlp', 'system_acknowledgement', "bot_ask_open_question"]
    }

    MusicBot = {
        'name': "MUSICCHAT",
        'class': MusicResponseGeneratorLocal,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'a_b_test', 'central_elem', 'returnnlp', 'system_acknowledgement', "bot_ask_open_question"]
    }

    WeatherBot = {
        'name': "WEATHER",
        'class': RemoteServiceModule,
        'url': ServiceURLLoader.get_url_for_module("WEATHER"),
        'context_manager_keys': ['intent', 'slots', 'text', 'features', 'central_elem', 'returnnlp', 'a_b_test', 'system_acknowledgement', "bot_ask_open_question"],
        'input_user_attributes_keys': ['location', 'ask_forecast', 'template_manager', 'module_selection'],
        'timeout_in_millis': 1000
    }

    TopicRetrievalBot = {
        'name': "RETRIEVAL",
        'class': TopicRetrievalResponseGenerator,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'topic', 'features', 'central_elem', 'returnnlp', 'a_b_test', 'system_acknowledgement', "bot_ask_open_question"]
    }



    StoryFunfactBot = {
        'name': "STORYFUNFACT",
        'class': TellStoryFunfact_ResponseGenerator,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text', 'central_elem', 'returnnlp', 'a_b_test', 'system_acknowledgement', "bot_ask_open_question"]
    }

    # REMOTE MODULES
    MovieTVBot = {
        'name': "MOVIECHAT",
        'class': RemoteServiceModule,
        'url': ServiceURLLoader.get_url_for_module('MOVIECHAT'),
        'context_manager_keys': ['intent', 'slots', 'text', 'returnnlp', 'features', 'moviechat_context', 'user_id', 'session_id', 'resp_type', 'a_b_test', "last_response", "bot_ask_open_question", "system_acknowledgement"],
        'input_user_attributes_keys': ['session_id', 'user_profile', 'suggest_keywords', 'last_module', 'previous_modules', 'template_manager', 'moviechat_user', 'response', 'module_selection', 'blender_start_time'],
        'timeout_in_millis': 3500,
        'history_turns': 1
    }

    BookBot = {
        'name': "BOOKCHAT",
        'class': RemoteServiceModule,
        'url': ServiceURLLoader.get_url_for_module("BOOKCHAT"),
        'context_manager_keys': ['intent', 'slots', 'text', 'features', 'returnnlp', 'bookchat_context', 'user_id', 'session_id', 'resp_type', 'a_b_test', "last_response", "bot_ask_open_question", 'system_acknowledgement'],
        'input_user_attributes_keys': ['session_id', 'user_profile', 'suggest_keywords', 'last_module', 'previous_modules', 'template_manager', 'bookchat_user', 'response', 'module_selection', 'blender_start_time'],
        'timeout_in_millis': 4000,
        'history_turns': 1
    }

    TechScienceBot = {
        'name': "TECHSCIENCECHAT",
        'class': RemoteServiceModule,
        'url': ServiceURLLoader.get_url_for_module("TECHSCIENCECHAT"),
        'context_manager_keys': ['text', 'session_id', 'returnnlp', 'central_elem', 'a_b_test', "last_response", "system_acknowledgement", "bot_ask_open_question"],
        'input_user_attributes_keys': ['session_id', 'user_profile', 'suggest_keywords', 'last_module', 'previous_modules', 'template_manager', 'techsciencechat', 'usr_name', 'module_selection', 'blender_start_time'],
        'timeout_in_millis': 4000,
        'history_turns': 1
    }

    SportBot = {
        'name': "SPORT",
        'class': RemoteServiceModule,
        'url': ServiceURLLoader.get_url_for_module("SPORT"),
        'context_manager_keys': ['intent', 'slots', 'text', 'features', 'central_elem', 'returnnlp', 'session_id', 'a_b_test', 'system_acknowledgement', "bot_ask_open_question"],
        'input_user_attributes_keys': ['session_id', 'user_profile', 'sportchat', 'sport_user', 'last_module', 'previous_modules', 'module_selection', 'suggest_keywords', 'template_manager', 'blender_start_time'],
        'history_turns': 1,
        'timeout_in_millis': 2000
    }

    NewsBot = {
        'name': "NEWS",
        'class': RemoteServiceModule,
        'url': ServiceURLLoader.get_url_for_module("NEWS"),
        'context_manager_keys': ['intent', 'slots', 'text', 'features', 'npknowledge', 'dialog_act', 'user_id', 'session_id', 'returnnlp', 'central_elem', 'a_b_test', "system_acknowledgement"],
        'input_user_attributes_keys': ['session_id', 'user_profile', 'suggest_keywords', 'last_module', 'previous_modules', 'template_manager', 'news_user', 'sys_env', 'usr_name', 'module_selection', 'blender_start_time'],
        'timeout_in_millis': 2500,
        # 'history_turns': 1
    }

