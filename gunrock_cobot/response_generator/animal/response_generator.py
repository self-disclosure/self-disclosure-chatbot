import nlu.util_nlu
import utils
from cobot_core.service_module import LocalServiceModule
from response_generator.animal.animal_automaton import AnimalAutomaton

import logging
from nlu.dataclass import ReturnNLP

logger = logging.getLogger(__name__)


class AnimalResponseGeneratorLocal(LocalServiceModule):

    def execute(self):
        animalchat = self.state_manager.user_attributes.animalchat
        if animalchat is None:
            animalchat = dict()

        logger.info('[AnimalChatRG] exec with animalchat: {}'.format(animalchat))

        returnnlp_raw = self.input_data.get("returnnlp", dict())
        returnnlp = ReturnNLP(returnnlp_raw)
        key_phrase_all = returnnlp.key_phrase
        key_phrase = [item for sublist in key_phrase_all for item in sublist]
        googlekg = returnnlp.googlekg
        topic_keyword_all = returnnlp.topic
        topic_keyword = list()
        for segment in topic_keyword_all:
            for keywords in segment.topic_keywords:
                topic_keyword.append(keywords.keyword)
        knowledge = [[googlekgsegment[0].name, googlekgsegment[0].description] for googlekgsegment in googlekg if googlekgsegment]
        central_elem = nlu.util_nlu.get_feature_by_key(self.state_manager.current_state, 'central_elem')
        inputs = {
            "current_state": animalchat.get('current_state', "s_init"),
            "text": self.input_data["text"].lower(),
            # "nlu": {
            #     "sentiment": senti,
            #     "lexical": lexical,
            #     "ner": ner
            # },
            "central_elem": central_elem,
            "topic_keywords": topic_keyword,
            "key_phrase": key_phrase,
            "next_state": animalchat.get('next_state', "s_init"),
            "animal_history": animalchat.get('animal_history', list()),
            "animal": animalchat.get('animal', "")[0] if isinstance(animalchat.get('animal', ""), list) else animalchat.get('animal', ""),
            "fact_loop_count": animalchat.get('fact_loop_count', -1),
            "asked_fun_fact_question_before": animalchat.get('asked_fun_fact_question_before', False),
            "chitchat_idx": animalchat.get('chitchat_idx', -1),
            "pet_chitchat_idx": animalchat.get('pet_chitchat_idx', -1),
            "has_pet": animalchat.get('has_pet', False),
            "user_pet": animalchat.get('user_pet', list()),
            "knowledge_feature": knowledge,
            "propose_topic": animalchat.get('propose_topic', None),
            # "dialog_act": dialog_act or None,
            "system_acknowledgement": self.input_data.get("system_acknowledgement", dict()),
            "returnnlp": returnnlp
        }
        logger.info('[AnimalRG] get response with input: {}'.format(inputs))
        RG = AnimalAutomaton(inputs, user_attributes=self.state_manager.user_attributes)
        outputtext = RG.getresponse(inputs)
        response_text = outputtext["response"]
        # remove redundant data
        outputtext.pop('nlu', None)
        outputtext.pop('text', None)
        outputtext.pop('response', None)
        self.state_manager.user_attributes.animalchat = outputtext

        if "propose_topic" in outputtext and outputtext["propose_topic"]:
            setattr(self.state_manager.user_attributes, "propose_topic", outputtext["propose_topic"])
        logger.info("[AnimalRG] The final response is %s", response_text)
        return response_text