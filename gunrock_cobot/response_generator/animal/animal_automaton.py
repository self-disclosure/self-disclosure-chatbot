import re
import random
import template_manager
import praw
import hashlib
import logging
import requests
import json
from fuzzywuzzy import fuzz
from cobot_common.service_client import get_client
from constants_api_keys import COBOT_API_KEY
from nlu.dataclass import CentralElement, ReturnNLP, ReturnNLPSegment
from nlu.constants import DialogAct as DialogActEnum
from nlu.constants import Positivity as PositivityEnum
from nlu.question_detector import QuestionDetector
from question_handling.quetion_handler import QuestionHandler, QuestionResponse, QuestionResponseTag
from nlu.command_detector import CommandDetector
from nlu.dataclass import CentralElement
from nlu.constants import TopicModule
from template_manager.manager import Template, TemplateManager
from selecting_strategy.module_selection import ModuleSelector, GlobalState
from user_profiler.user_profile import UserProfile

BACKSTORY_THRESHOLD = 0.82

template_animal = template_manager.Templates.animal
template_shared = template_manager.Templates.shared
reddit_keys = [{"REDDIT_CLIENT_SECRET": 'smytIRjC5L9lcjAFQkJjYV8asDY',
                "REDDIT_CLIENT_ID": 'aEE9fRbUYM3wWg'},
               {"REDDIT_CLIENT_SECRET": 'IEHzPvutOt4XkaPA9ZklmpwnwQw',
                "REDDIT_CLIENT_ID": '6-5cxgGJ1yF3XA'},
               {"REDDIT_CLIENT_SECRET": 'P2TJ-59JcIvgS_Chhs6HmFic4tM',
                "REDDIT_CLIENT_ID": 'KBUqnZJiG8SVFg'}]
key = random.choice(reddit_keys)
REDDIT_CLIENT_SECRET = key["REDDIT_CLIENT_SECRET"]
REDDIT_CLIENT_ID = key["REDDIT_CLIENT_ID"]
MAX_1_WORDS = r"\W*(\w+(\W+|$)){0,1}$"

blacklist = [
    "chlamydia",
    "euthanize",
    "euthanasia",
    r"attack(ed|ing)?",
    "god",
    "meat",
    "gun",
    "revolver",
    "poop",
    "rifle",
    "weapon",
    "knife",
    r"\)\)\<\>\(\(",
    r"\.com|\.org|\.",
    r"\>|\<",
    "skewer",
    "blood",
    "cocaine",
    "heroin",
    "lsd",
    "meth",
    "stab",
    "shrooms",
    "ecstacy",
    "disaster",
    "injury",
    "fear",
    "ass(hole)?",
    "anal",
    "anus",
    "arsehole",
    "arse",
    "bitch",
    "bangbros",
    "bastards",
    "bitch",
    "tits",
    "butt",
    "blow job",
    "boob",
    "bra",
    "cock",
    "cum",
    "cunt",
    "dick",
    "shit",
    "sex",
    "erotic",
    "fuck",
    "fxxk",
    r"f\*\*k",
    r"f\*\*c",
    r"f\*ck",
    "fcuk",
    r"gang ?bang",
    "genital",
    "damn",
    "horny",
    "jerk off",
    "jerkoff",
    "kill",
    "loser",
    "masterbate",
    "naked",
    "nasty",
    "negro",
    "nigger",
    "nigga",
    "orgy",
    "pussy",
    "penis",
    "perve",
    "rape",
    r"fuck(ed|ing)?",
    "trump",
    "racist",
    "sexy",
    "strip club",
    "vagina",
    "faggot",
    "fag",
    "drug",
    "weed",
    "incel",
    "prostitute",
    "slut",
    "whore",
    "cuck",
    "cuckold",
    "shooting",
    r"nazism?",
    "nazi",
    "sgw",
    "balls",
    "thot",
    "virgin",
    "cumshot",
    "cumming",
    "dildo",
    "vibrator",
    "sexual",
    r"douche(bag)?",
    "bukkake",
    "porn",
    r"pornography?",
    r"died?",
    r"kill(s|ed|ing)?",
    r"murder(s|ed|ing)?",
    "clit(oris)?",
    "mushroom tatoo",
    "cheat(ed|ing)?",
    "dammit",
    r"toxic(ity)?",
    "wtf",
    "breasts",
    "tits",
    "boobies",
    "dead",
    "death",
    "bestial"
    r"https?"]

facts_animals = {
    'dog': r'(dogs?)|(puppy?i?e?s?)',
    'cat': r'(cats?)|(kittens?)|(kitty)|(kitties)',
    'horse': r'(horses?)|(pony?i?e?s?)',
    'penguin': r'(penguins?)',
    'unicorn': r'(unicorns?)',
    'butterfly': r'(butterfly?i?e?s?)',
    'bird': r'(birds?)'}

favorite_animals = {
    'dog': r'(dogs?)|(puppy?i?e?s?)|(pug)|(terrier)|(chihuahua)',
    'cat': r'(cats?)|(kittens?)|(kitty)|(kitties)',
    'pet': r'(pets?)'}
common_pets = {
    'dog': r'(dogs?)|(puppy?i?e?s?)',
    'cat': r'(cats?)|(kittens?)|(kitty)|(kitties)',
    'fish': r'(fishe?s?)',
    'bird': r'(birds?)',
    'hamster': r'(hamsters?)',
    'rabbit': r'(rabbits?)|(bunny?i?e?s?)',

}
animal_template_count = {
    'fun_fact first': 2,
    'fun_fact begin': 4,
    'fun_fact begin_transition': 1,
    'fun_fact end more': 4,
    'fun_fact end comment': 2,
    'fun_fact end cute_end': 2,
    'favorites animal': 4,
    'favorites bear': 1,
    'favorites dinosaur': 1,
    'favorites fish': 1,
    'favorites dog': 4,
    'favorites cat': 4,
    'favorites pet': 5,
    'idk': 4,
    'user_idk': 3,
    'animal_facts cat': 4,
    'animal_facts dog': 4,
    'animal_facts horse': 3,
    'animal_facts penguin': 3,
    'animal_facts unicorn': 3,
    'animal_facts butterfly': 3,
    'animal_facts bird': 3,
    'animal_facts general': 7,
    'animal_facts cat extra': 7,
    'animal_facts dog extra': 9,
    'animal_facts horse extra': 6,
    'animal_facts penguin extra': 9,
    'animal_facts unicorn extra': 6,
    'animal_facts butterfly extra': 7,
    'animal_facts bird extra': 3,
    'animal_facts general extra': 11,
    'system change_topic': 4,
    'system exit': 2,
    'short_response': 6}
animal_names = [
    'aardvark',
    'abyssinian',
    'adelie penguin',
    'affenpinscher',
    'afghan hound',
    'african bush elephant',
    'african civet',
    'african clawed frog',
    'african forest elephant',
    'african palm civet',
    'african penguin',
    'african tree toad',
    'african wild dog',
    'airedale terrier',
    'akbash',
    'akita',
    'alaskan malamute',
    'albatross',
    'aldabra giant tortoise',
    'alligator',
    'alpaca',
    'alpine dachsbracke',
    'american bulldog',
    'american cocker spaniel',
    'american coonhound',
    'american eskimo dog',
    'american foxhound',
    'pit bull terrier',
    'pit bull',
    'american staffordshire terrier',
    'american water spaniel',
    'anatolian shepherd dog',
    'angelfish',
    'ant',
    'anteater',
    'antelope',
    'appenzeller dog',
    'arctic fox',
    'arctic hare',
    'arctic wolf',
    'armadillo',
    'asian elephant',
    'asian giant hornet',
    'asian palm civet',
    'asiatic black bear',
    'australian cattle dog',
    'australian kelpie dog',
    'australian mist',
    'australian shepherd',
    'australian terrier',
    'avocet',
    'axolotl',
    'aye aye',
    'baboon',
    'bactrian camel',
    'badger',
    'balinese',
    'banded palm civet',
    'bandicoot',
    'barb',
    'barn owl',
    'barnacle',
    'barracuda',
    'basenji dog',
    'basking shark',
    'basset hound',
    'bat',
    'bavarian mountain hound',
    'beagle',
    'bear',
    'bearded collie',
    'bearded dragon',
    'beaver',
    'bedlington terrier',
    'beetle',
    'bengal tiger',
    'bernese mountain dog',
    'bichon frise',
    'binturong',
    'bird',
    'birds of paradise',
    'birman',
    'bison',
    'black bear',
    'black rhinoceros',
    'black russian terrier',
    'black widow spider',
    'bloodhound',
    'blue lacy dog',
    'blue whale',
    'bluetick coonhound',
    'bobcat',
    'bolognese dog',
    'bongo',
    'bonobo',
    'border collie',
    'border terrier',
    'bornean orang-utan',
    'borneo elephant',
    'boston terrier',
    'bottle nosed dolphin',
    'boxer dog',
    'boykin spaniel',
    'brazilian terrier',
    'brown bear',
    'budgerigar',
    'buffalo',
    'bull mastiff',
    'bull shark',
    'bull terrier',
    'bulldog',
    'bullfrog',
    'bumble bee',
    'burmese',
    'burrowing frog',
    'butterfly',
    'butterfly fish',
    'caiman',
    'cairn terrier',
    'camel',
    'canaan dog',
    'capybara',
    'caracal',
    'carolina dog',
    'cassowary',
    'cat',
    'caterpillar',
    'catfish',
    'cavalier king charles spaniel',
    'centipede',
    'cesky fousek',
    'chameleon',
    'chamois',
    'cheetah',
    'chesapeake bay retriever',
    'chicken',
    'chihuahua',
    'chimpanzee',
    'chinchilla',
    'chinese crested dog',
    'chinook',
    'chinstrap penguin',
    'chipmunk',
    'chow chow',
    'cichlid',
    'clouded leopard',
    'clown fish',
    'clumber spaniel',
    'coati',
    'cockroach',
    'collared peccary',
    'collie',
    'common buzzard',
    'common frog',
    'common loon',
    'common toad',
    'coral',
    'corgi',
    'cottontop tamarin',
    'cougar',
    'cow',
    'coyote',
    'crab',
    'crab-eating macaque',
    'crane',
    'crested penguin',
    'crocodile',
    'crow',
    'cross river gorilla',
    'curly coated retriever',
    'cuscus',
    'cuttlefish',
    'dachshund',
    'dalmatian',
    "darwin's frog",
    'deer',
    'desert tortoise',
    'deutsche bracke',
    'dhole',
    'dingo',
    'dinosaur',
    'discus',
    'doberman pinscher',
    'dodo',
    'dog',
    'dogo argentino',
    'dogue de bordeaux',
    'dolphin',
    'donkey',
    'dormouse',
    'dragon',
    'dragonfly',
    'drever',
    'duck',
    'dugong',
    'dunker',
    'dusky dolphin',
    'dwarf crocodile',
    'eagle',
    'earwig',
    'eastern gorilla',
    'eastern lowland gorilla',
    'echidna',
    'edible frog',
    'egyptian mau',
    'electric eel',
    'elephant',
    'elephant seal',
    'elephant shrew',
    'emperor penguin',
    'emperor tamarin',
    'emu',
    'english cocker spaniel',
    'english shepherd',
    'english springer spaniel',
    'entlebucher mountain dog',
    'epagneul pont audemer',
    'eskimo dog',
    'estrela mountain dog',
    'falcon',
    'fennec fox',
    'ferret',
    'field spaniel',
    'fin whale',
    'finnish spitz',
    'fire-bellied toad',
    'fish',
    'fishing cat',
    'flamingo',
    'flat coat retriever',
    'flounder',
    'fly',
    'flying squirrel',
    'fossa',
    'fox',
    'fox terrier',
    'french bulldog',
    'frigatebird',
    'frog',
    'fur seal',
    'galapagos penguin',
    'galapagos tortoise',
    'gar',
    'gecko',
    'gentoo penguin',
    'geoffroys tamarin',
    'gerbil',
    'german pinscher',
    'german shepherd',
    'gharial',
    'giant african land snail',
    'giant clam',
    'giant panda bear',
    'giant schnauzer',
    'gibbon',
    'gila monster',
    'giraffe',
    'glow worm',
    'goat',
    'golden lion tamarin',
    'golden oriole',
    'golden retriever',
    'goose',
    'gopher',
    'gorilla',
    'grasshopper',
    'great dane',
    'great white shark',
    'greater swiss mountain dog',
    'green bee-eater',
    'greenland dog',
    'grey mouse lemur',
    'grey reef shark',
    'grey seal',
    'greyhound',
    'grizzly bear',
    'grouse',
    'guinea fowl',
    'guinea pig',
    'guppy',
    'hammerhead shark',
    'hamster',
    'harrier',
    'havanese',
    'hedgehog',
    'hercules beetle',
    'hermit crab',
    'heron',
    'highland cattle',
    'himalayan',
    'hippopotamus',
    'honey bee',
    'horn shark',
    'horned frog',
    'horse',
    'horseshoe crab',
    'howler monkey',
    'human',
    'humboldt penguin',
    'hummingbird',
    'humpback whale',
    'hyena',
    'ibis',
    'ibizan hound',
    'iguana',
    'impala',
    'inchworm',
    'elephant',
    'indian palm squirrel',
    'indian rhinoceros',
    'indian star tortoise',
    'indochinese tiger',
    'indri',
    'insect',
    'irish setter',
    'irish wolfhound',
    'jack russel',
    'jackal',
    'jaguar',
    'japanese chin',
    'japanese macaque',
    'javan rhinoceros',
    'javanese',
    'jellyfish',
    'kakapo',
    'kangaroo',
    'keel billed toucan',
    'killer whale',
    'king crab',
    'king penguin',
    'kingfisher',
    'kiwi',
    'koala',
    'komodo dragon',
    'kudu',
    'labradoodle',
    'labrador retriever',
    'ladybird',
    'leaf-tailed gecko',
    'lemming',
    'lemur',
    'leopard',
    'leopard cat',
    'leopard seal',
    'leopard tortoise',
    'liger',
    'lion',
    'lionfish',
    'little penguin',
    'lizard',
    'llama',
    'lobster',
    'long-eared owl',
    'lynx',
    'macaroni penguin',
    'macaw',
    'magellanic penguin',
    'magpie',
    'maine coon',
    'malayan civet',
    'malayan tiger',
    'maltese',
    'manatee',
    'mandrill',
    'manta ray',
    'marine toad',
    'markhor',
    'marsh frog',
    'masked palm civet',
    'mastiff',
    'mayfly',
    'meerkat',
    'millipede',
    'minke whale',
    'mole',
    'molly',
    'mongoose',
    'mongrel',
    'monkey',
    'monte iberia eleuth',
    'moorhen',
    'moose',
    'moray eel',
    'moth',
    'mountain gorilla',
    'mountain lion',
    'mouse',
    'mule',
    'neanderthal',
    'neapolitan mastiff',
    'newfoundland',
    'newt',
    'nightingale',
    'norfolk terrier',
    'norwegian forest',
    'numbat',
    'nurse shark',
    'ocelot',
    'octopus',
    'okapi',
    'old english sheepdog',
    'olm',
    'opossum',
    'orang-utan',
    'ostrich',
    'otter',
    'oyster',
    'pademelon',
    'panda',
    'panther',
    'parakeet',
    'parrot',
    'patas monkey',
    'peacock',
    'pekingese',
    'pelican',
    'penguin',
    'persian cat',
    'pheasant',
    'pied tamarin',
    'pig',
    'pika',
    'pike',
    'pink fairy armadillo',
    'piranha',
    'platypus',
    'poison dart frog',
    'polar bear',
    'pond skater',
    'poodle',
    'pool frog',
    'porcupine',
    'possum',
    'prawn',
    'proboscis monkey',
    'puffer fish',
    'puffin',
    'pug',
    'puma',
    'purple emperor',
    'puss moth',
    'pygmy hippopotamus',
    'pygmy marmoset',
    'quail',
    'quetzal',
    'quokka',
    'quoll',
    'rabbit',
    'raccoon',
    'raccoon dog',
    'radiated tortoise',
    'rat',
    'rattlesnake',
    'red knee tarantula',
    'red panda',
    'red wolf',
    'red-handed tamarin',
    'reindeer',
    'rhinoceros',
    'river dolphin',
    'river turtle',
    'robin',
    'rock hyrax',
    'rockhopper penguin',
    'roseate spoonbill',
    'rottweiler',
    'royal penguin',
    'russian blue',
    'sabre-toothed tiger',
    'saint bernard',
    'salamander',
    'saola',
    'scorpion',
    'scorpion fish',
    'sea dragon',
    'sea lion',
    'sea otter',
    'sea slug',
    'sea squirt',
    'sea turtle',
    'sea urchin',
    'seahorse',
    'seal',
    'shark',
    'sheep',
    'shih tzu',
    'shrimp',
    'siamese',
    'siamese fighting fish',
    'siberian',
    'siberian husky',
    'siberian tiger',
    'silver dollar',
    'skunk',
    'sloth',
    'slow worm',
    'snail',
    'snake',
    'snapping turtle',
    'snowshoe',
    'snowy owl',
    'somali',
    'south china tiger',
    'spadefoot toad',
    'sparrow',
    'spectacled bear',
    'sperm whale',
    'spider monkey',
    'spiny dogfish',
    'sponge',
    'squid',
    'squirrel',
    'squirrel monkey',
    'sri lankan elephant',
    'staffordshire bull terrier',
    'stag beetle',
    'starfish',
    'stellers sea cow',
    'stick insect',
    'stingray',
    'stoat',
    'striped rocket frog',
    'sumatran elephant',
    'sumatran orang-utan',
    'sumatran rhinoceros',
    'sumatran tiger',
    'sun bear',
    'swan',
    'tang',
    'tapanuli orang-utan',
    'tapir',
    'tarantula',
    'tarsier',
    'tasmanian devil',
    'tawny owl',
    'termite',
    'tetra',
    'thorny devil',
    'tibetan mastiff',
    'tiffany',
    'tiger',
    'tiger salamander',
    'tiger shark',
    'tortoise',
    'toucan',
    'tree frog',
    'tropicbird',
    'tuatara',
    'turkey',
    'turkish angora',
    'turtle',
    'umbrellabird',
    'vampire bat',
    'vervet monkey',
    'vulture',
    'wallaby',
    'walrus',
    'warthog',
    'wasp',
    'water buffalo',
    'water dragon',
    'water vole',
    'weasel',
    'welsh corgi',
    'west highland terrier',
    'western gorilla',
    'western lowland gorilla',
    'whale shark',
    'whippet',
    'white faced capuchin',
    'white rhinoceros',
    'white tiger',
    'wild boar',
    'wildebeest',
    'wolf',
    'wolves',
    'wolverine',
    'wombat',
    'woodlouse',
    'woodpecker',
    'woolly mammoth',
    'woolly monkey',
    'x-ray tetra',
    'yak',
    'yellow-eyed penguin',
    'yorkshire terrier',
    'zebra',
    'zebra shark',
    'zonkey',
    'zorse']
dog_breeds = [
    'affenpinscher',
    'afghan hound',
    'airedale terrier',
    'akita',
    'alaskan malamute',
    'cocker spaniel',
    'eskimo dog',
    'foxhound',
    'american staffordshire terrier',
    'water spaniel',
    'anatolian shepherd',
    'australian cattle dog',
    'australian shepherd',
    'terrier',
    'basenji',
    'basset hound',
    'beagle',
    'bearded collie',
    'bedlington terrier',
    'malinois',
    'belgian sheepdog',
    'belgian tervuren',
    'bernese mountain dog',
    'bichon frise',
    'black and tan coonhound',
    'bloodhound',
    'border collie',
    'border terrier',
    'borzoi',
    'boston terrier',
    'bouvier des flandres',
    'boxer',
    'briard',
    'brittany',
    'brussels griffon',
    'bull terrier',
    'bulldog',
    'bullmastiff',
    'cairn terrier',
    'canaan dog',
    'cardigan welsh corgi',
    'cavalier king charles spaniel',
    'chesapeake bay retriever',
    'chihuahua',
    'chinese crested dog',
    'chinese shar-pei',
    'chow chow',
    'clumber spaniel',
    'corgi',
    'collie',
    'curly-coated retriever',
    'dachshund',
    'dalmatian',
    'dandie dinmont terrier',
    'doberman pinscher',
    'english cocker spaniel',
    'english foxhound ',
    'english setter',
    'english springer spaniel',
    'english toy spaniel',
    'field spaniel',
    'finnish spitz',
    'flat-coated retriever',
    'french bulldog',
    'german shepherd dog',
    'german shorthaired pointer',
    'german wirehaired pointer',
    'giant schnauzer',
    'golden retriever',
    'gordon setter',
    'great dane',
    'great pyrenees',
    'greater swiss mountain dog',
    'greyhound',
    'harrier',
    'havanese',
    'ibizan hound',
    'irish setter',
    'irish terrier',
    'irish water spaniel',
    'irish wolfhound',
    'italian greyhound',
    'jack russell terrier',
    'japanese chin',
    'keeshond',
    'kerry blue terrier',
    'komondor',
    'kuvasz',
    'labrador retriever',
    'lakeland terrier',
    'lhasa apso',
    'lowchen',
    'maltese',
    'manchester terrier',
    'mastiff',
    'miniature bull terrier',
    'miniature pinscher',
    'miniature schnauzer',
    'newfoundland',
    'norfolk terrier',
    'norwegian elkhound',
    'norwich terrier',
    'old english sheepdog',
    'otterhound',
    'papillon',
    'pekingese',
    'pembroke welsh corgi',
    'petit basset griffon vendeen',
    'pharaoh hound',
    'pointer',
    'pomeranian',
    'poodle',
    'portuguese water dog',
    'pug',
    'puli',
    'retriever',
    'rhodesian ridgeback',
    'rottweiler',
    'saint bernard',
    'saluki',
    'samoyed',
    'schipperke',
    'scottish deerhound',
    'scottish terrier',
    'sealyham terrier',
    'shetland sheepdog',
    'shiba inu',
    'shih tzu',
    'siberian husky',
    'silky terrier',
    'skye terrier',
    'smooth fox terrier',
    'soft coated wheaten terrier',
    'spinone italiano',
    'staffordshire bull terrier',
    'standard schnauzer',
    'sussex spaniel',
    'tibetan spaniel',
    'tibetan terrier',
    'vizsla',
    'weimaraner',
    'welsh springer spaniel',
    'welsh terrier',
    'west highland white terrier',
    'whippet',
    'wire fox terrier',
    'wirehaired pointing griffon',
    'yorkshire terrier']
animal_sounds = {
    'dog': {"sound": 'woof'},
    'frog': {"sound": 'ribbit'},
    'duck': {"sound": 'quack'},
    'cow': {"sound": 'moo'},
    'cat': {"sound": 'meow'},
    'snake': {"sound": 'hiss'},
    'cock': {"sound": 'cock a doodle do'},
    'crow': {"sound": 'caw'},
    'sheep': {"sound": 'baa'}}

coronavirus_detect_list = {
    "coronavirus": "coronavirus",
    "corona virus": "coronavirus",
    "virus": "coronavirus",
    "pandemic": "coronavirus",
    "covid": "coronavirus"
}

def clean_posts(utterances, searchterm=None):
    utterances = [post.title.lower() for post in utterances]
    clean_responses = list()
    # profanity removal
    for line in utterances:
        if not any(re.search(r"\b" + word + r"\b", line)
                   for word in blacklist):
            clean_responses.append(line)
    logging.info('[ANIMALCHAT] clean_posts: after profanity removal {}'.format(
        clean_responses))
    # remove til/redditisms
    clean_responses = [re.sub(r"\btil\b", "", post).lstrip()
                       for post in clean_responses]
    clean_responses = [
        post.replace(
            "today i learned that",
            "").lstrip() for post in clean_responses]
    clean_responses = [re.sub("^that\b", "", post).lstrip()
                       for post in clean_responses]
    clean_responses = [post + '.' if post[-1] not in ['.',
                                                      '!', '?'] else post for post in clean_responses]
    # prune for length
    clean_responses = [
        post for post in clean_responses if len(post) < 300]
    logging.info('[ANIMALCHAT] clean_posts: after pruning for length {}'.format(
        clean_responses))
    # remove if searchterm isn't in post
    if searchterm:
        if searchterm[-1] == 's':  # plurals?
            searchterm = searchterm[:-1]
        clean_responses = [
            post for post in clean_responses if searchterm in post]
        logging.info(
            '[ANIMALCHAT] clean_posts: pruning for searchterm: {}'.format(searchterm))
    logging.info('[ANIMALCHAT] clean_posts: final {}'.format(clean_responses))
    return clean_responses


def retrieve_reddit(utterance, subreddits, limit):
    # cuurently using gt's client info
    try:
        reddit = praw.Reddit(client_id=REDDIT_CLIENT_ID,
                             client_secret=REDDIT_CLIENT_SECRET,
                             user_agent='extractor',
                             timeout=2
                             # username='gunrock_cobot', # not necessary for read-only
                             # password='ucdavis123'
                             )
        subreddits = '+'.join(subreddits)
        subreddit = reddit.subreddit(subreddits)
        # higher limit is slower; delete limit arg for top 100 results
        submissions = list(subreddit.search(utterance, limit=limit))
        if len(submissions) > 0:
            return submissions
        return None
    except Exception as e:
        logging.error("Fail to retrieve reddit. error msg: {}. user utterance: {}".format(e, utterance))
        return None


def get_reddit_TIL_posts(input_data, redditsearch=list(), subreddit=[
                         'todayIlearned'], limit=15):
    submissions = None
    clean_responses = list()
    posts = list()
    logging.info("[ANIMALCHAT] Attempting to get reddit response with client ID {}".format(
        REDDIT_CLIENT_ID))
    if not redditsearch:
        redditsearch = detect_animal(input_data, "strong")
        logging.info(
            "[ANIMALCHAT] reddit search term: {}".format(redditsearch))
        # print(redditsearch)
    for searchterm in redditsearch:
        try:
            posts = retrieve_reddit(searchterm, subreddit, limit)
        except BaseException:
            continue
        if posts:
            clean_responses = clean_posts(posts, searchterm)
            if clean_responses:
                break
    while (not clean_responses or not redditsearch) and limit < 40:  # last resort
        if not redditsearch:
            redditsearch = ["animal"]
        elif not clean_responses:
            limit += 10
        try:
            key = random.choice(reddit_keys)
            submissions = retrieve_reddit(
                redditsearch[0], subreddit, limit)
            logging.info("[ANIMALCHAT] Getting reddit responses with new key {}".format(
                [post.title.lower() for post in submissions]))
        except BaseException:
            return list(), redditsearch
        clean_responses = clean_posts(
            utterances=submissions,
            searchterm=redditsearch[0])
    if clean_responses:
        return clean_responses, redditsearch
    return list(), list()


def detect_intent_text(text):
    # detecting possible intents to move the conversation in the
    # required way
    if re.search(
        r"\bturn off|\bend conversation|shut down|stop|exit|(good)?bye| change (the\b|this\b)?topic",
            text):
        return "exit"
    elif re.search(r"(don't|do not) (want|like) to (talk\b|hear\b|speak\b) about", text):
        if re.search("animals", text):
            # assuming user doesn't want to talk about any animals
            return "exit"
        return "soft_exit"
    elif re.search(r"(walk|pet|play)?.*my (dogs?|cats?|kitty|kitties?|birds?|pets?)|"
                   r"(got|have) .*(dogs?|cats?|kitty|kitties?|birds?|pets?|doggies)", text) and \
            not re.search(r"(want to|wanna|trying to).* (get|buy|look|search)", text):
        return "pet_chitchat_pet_type_detected"
    elif re.search(r"(talk\b|speak) (to|with|like)( a| my| our\b)? (dog|cat|animal|pet|\w+\b)|"
                   r"(\bmake\b)? (animal|pet) (sound\b|noise\b)", text):
        return "make_animal_sound"
    elif re.search(
        r"talk.*about|tell (me|us|him|her).*about|know.*about|chat about|tell (me|us|him|her) a "
        r"|let'?s do|let'?s talk|how about|give me a .* fact",
            text):
        return "open"
    elif re.search("your favorite|do you (like|love|hate)|what's your|do you have", text):
        return "backstory"
    elif re.search(r"(who\b|what\b|when\b|where\b|why\b|which\b|how\b|can|know|curious|interest|"
                   r"hear|tell|give|let|do).* you.* (think|feel|like|love|thought|support|hate|believe|"
                   r"opinion|stance|view|say|know|favorite).*", text):
        return "ask_opinion"
    elif re.search(r"(not really)|(\bno\b|\bnope\b|\bnah\b|\bnone\b|\bnot\b (don't|do not)"
                   r" (like|enjoy|want))|(that\'s|you\'re) (stupid|gross|disgusting)|(don't|dont) know|\bannoyed\b|\bbored\b|\bboring\b", text):
        return "negative_response"
    elif re.search(r"yes|yeah|sure|absolutely|ya|ok|\bi\b (\bdo\b|\bam\b|\bwant to\b|\bdo\b|have)|can (you|we) (talk|chat) about (animal|animals|pet|pets)|(switch|change) to (animals|animal)", text):
        return "positive_response"
    elif re.search(r"don't know|not sure|no idea|no opinion|i forgot|no comment|hard to say"
                   r"|hard question|hard one|tough one|tough question|depends", text):
        return "idk"
    elif re.search(r".*(that|this).*(feel).*(sad|pity|bad|sorry).*", text):
        return "sad_response"
    elif re.search(r"favorite .*(animal|pet)", text):
        return "favorite_animal"
    return None


def search_animal_list(searchterms, logterm="list", threshold=85):
    result = list()
    logging.info("[ANIMALCHAT] search_animal_list searchterms {}".format(searchterms))
    for item in searchterms:
        if item in list(set(animal_names + dog_breeds)):
            result = [item] + result
        else:
            item = re.sub(
                r"(\bchat\b|\ban?\b|i\b|you\b|have|has\b|had\b|hate\b|love\b|like\b|several\b|(a lot)|many\b|few\b|\bcan\b|\bdo\b)",
                r"",
                item)
            logging.info(
                "[ANIMALCHAT] search_animal_list looking for item {}".format(item))
            if not item:
                continue
            animal_confidence = list()
            for name in animal_names:
                confidence = fuzz.ratio(item, name)
                # max(fuzz.partial_ratio(item, name),fuzz.ratio(item, name))
                if confidence > threshold:
                    animal_confidence.append((name, confidence))
                    logging.info(
                        "[ANIMALCHAT] detect_animal: {} {} & {} : {}".format(
                            logterm, item, name, confidence))
            if animal_confidence:
                animal_confidence = sorted(
                    animal_confidence, key=lambda x: x[1], reverse=True)
                logging.info(
                    "[ANIMALCHAT] detect_animal for {} {} : {}".format(
                        logterm, item, animal_confidence))
                result.append(animal_confidence[0][0])
    return result


def detect_animal(input_data, strength="weak"):
    final_result = list()
    regex_matches = list()
    if input_data.get('knowledge_feature'):
        for item in input_data.get('knowledge_feature'):
            if item[1] in ['Animal', 'Rodent', 'Bird', 'Insect']:
                # print(item)
                if item[0] == "chat":
                    continue
                final_result.append(item[0].lower())
                logging.info(
                    "[ANIMALCHAT] detect_animal adding animal {} from knowledge_feature".format(
                        item[0]))
    input_text = input_data.get('text', "animals")
    topic_keywords = input_data.get('topic_keywords', list())
    key_phrase = input_data.get('key_phrase', list())
    re_animals = r'(dogs?)|(cats?)|(pets?)|(kittens?)|(puppy?i?e?s?)|(fishe?s?)|(pandas?)|(animals?)'
    # print(input_text,type(input_text),re_animals,type(re_animals))
    if re.search(re_animals, input_text):
        regex_matches = list(
            filter(None, re.search(re_animals, input_text).groups()))
    np_results = search_animal_list(key_phrase, logterm="NP")
    final_result = final_result + np_results
    logging.info(
        "[ANIMALCHAT] detect_animal adding np_results {} to final".format(np_results))
    tk_results = search_animal_list(topic_keywords, logterm="Topic keyword")
    final_result = final_result + tk_results
    logging.info(
        "[ANIMALCHAT] detect_animal adding tk_results {} to final".format(tk_results))

    if input_data.get("animal"):
        final_result.append(input_data.get("animal"))

    if not final_result and regex_matches:
        final_result = regex_matches
        logging.info(
            "[ANIMALCHAT] detect_animal adding tk_results {} to final".format(regex_matches))

    if strength == "weak" and not final_result:
        final_result = ["animal"]
        logging.info(
            "[ANIMALCHAT] detect_animal weak final_result is {}".format(final_result))
    elif strength == "strong":
        logging.info(
            "[ANIMALCHAT] detect_animal strong final_result is {}".format(final_result))
        if not final_result:
            input_text = re.sub(
                r"(i|you|have|has|had|hate|love|like) ", r"", input_text)
            input_results = search_animal_list(
                input_text.split(), "input text")
            final_result = final_result + input_results
            logging.info(
                "[ANIMALCHAT] detect_animal adding search_results {} to final".format(input_results))
        # threshold=70
        # while not final_result:
        #     threshold-=10
        #     if threshold<50:
        #         break
        #     final_result = search_animal_list(input_text.split(" "), "threshold "+str(threshold), threshold)
        if not final_result:
            logging.info("[ANIMALCHAT] detect_animal no animal detected")
            final_result = ["animal"]

    logging.info("[ANIMALCHAT] detect_animal final results {}".format(final_result))
    logging.info("[ANIMALCHAT] detect_animal strength {} topic_keywords {} key_phrase {} regex_matches {}".
        format(
            strength,
            topic_keywords,
            key_phrase,
            regex_matches))
    # remove duplicates
    seen = set()
    seen_add = seen.add
    final_result = [x for x in final_result if not (x in seen or seen_add(x))]
    return final_result


def detect_petname(input_data):
    input_text = input_data['text']
    pet_name = None
    if len(input_text.split(' ')) <= 2:
        # assuming only pet name has been said
        m = re.search(r"\byes\b|\byeah\b|\bokay\b|\bdon't\b|\bnot\b|\bcan't\b|\bcannot\b|\bprivate\b|\bno\b|\byo\b", input_text)
        if not m:
            pet_name = input_text
    elif " and " in input_text:
        m = re.search(r"\band\b", input_text)
        if m:
            names = input_text.split(' ')
            names.remove('and')
            pet_name = names
    elif "name" in input_text:
        m = re.search(r"(his|s?her?)(\'s| is| was)? (named?.*(is|was)?) (?P<petname>.*)", input_text)
        m1 = re.search(r"((named|whose names? (is|are)) (?P<petname1>\b.*\b))", input_text)
        if m:
            pet_name = m.group('petname')
        elif m1:
            pet_name = m1.group('petname1')
        if pet_name and len(pet_name.split(' ')) > 1:
            pet_name = pet_name.split(' ')[0]
    if "call" in input_text:
        m = re.search(r"((his|s?her?)(\'s| is| was) (called)|((i|we) call (him|her))) (?P<petname>.*)", input_text)
        if m:
            pet_name = m.group('petname')
    if isinstance(pet_name, str) and pet_name:
        if len(pet_name.split(' ')) > 1:
            pet_name = re.sub(r"\bit?'?s?\b|\bso\b|\bhi\b|\bhello\b|\bno\b|\byes\b", r'', pet_name)
    return pet_name


def intersection(list1, list2):
    return list(set(list1) & set(list2))


class AnimalAutomaton:
    def __init__(self, input=dict(), user_attributes=None):
        self.user_attributes = user_attributes
        self.nlu = input.get('nlu', dict())
        self.key_phrase = input.get("key_phrase", list())
        self.topic_keywords = input.get("topic_keywords", list())
        self.current_state = input.get("current_state", None)
        self.next_state = input.get("next_state", None)
        self.animal_history = input.get("animal_history", list())
        self.animal = input.get("animal", "")
        self.chitchat_idx = input.get("chitchat_idx", -1)
        self.fact_loop_count = input.get("fact_loop_count", -1)
        self.asked_fun_fact_question_before = input.get("asked_fun_fact_question_before", False)
        self.has_pet = input.get("has_pet", False)
        self.user_pet = input.get("user_pet", list())
        self.pet_chitchat_idx = input.get("pet_chitchat_idx", -3)
        self.propose_topic = input.get("propose_topic", None)
        self.knowledge_feature = input.get("knowledge_feature", list())
        self.returnnlp = ReturnNLP(input.get("returnnlp", dict()))
        self.central_elem = input.get("central_elem", dict())
        self.system_ack = input.get('system_acknowledgement', dict())['ack'] \
            if 'ack' in input.get('system_acknowledgement',dict()) and input.get('system_acknowledgement', dict())['ack'] != " " else None
        self.question_detector = QuestionDetector(input["text"], input["returnnlp"])
        self.question_handler = QuestionHandler(
            input["text"],
            input["returnnlp"],
            BACKSTORY_THRESHOLD,
            input["system_acknowledgement"],
            self.user_attributes
        )
        self.command_detector = CommandDetector(input["text"], input["returnnlp"])
        self.propose_topic = ""
        self.propose_continue = "CONTINUE"
        self.question_answered = False
        self.command_handled = False
        self.module_selector = ModuleSelector(self.user_attributes)
        self.user_profile = UserProfile(self.user_attributes)
        self.__bind_states()

        self.module_mappings = {
            'topic_movietv': {'text': 'movies and T.V. shows', 'module': 'MOVIECHAT'},
            'topic_newspolitics': {'text': 'the news', 'module': 'NEWS'},
            'topic_weather': {'text': 'the weather', 'module': 'WEATHER'},
            'topic_music': {'text': 'music', 'module': 'MUSICCHAT'},
            'topic_book': {'text': 'books', 'module': 'BOOKCHAT'},
            'topic_sport': {'text': 'sports', 'module': 'SPORT'},
            'topic_holiday': {'text': 'holidays', 'module': 'HOLIDAYCHAT'},
            'topic_food': {'text': 'food', 'module': 'FOODCHAT'},
            'topic_travel': {'text': 'travel', 'module': 'TRAVELCHAT'},
            'topic_game': {'text': 'games', 'module': 'GAMECHAT'},
            'topic_techscience': {'text': 'science and technology', 'module': 'TECHSCIENCECHAT'}}

    # bind each state to corresponding callback function that generates
    # response
    def __bind_states(self):
        self.states_mapping = {
            "s_init": self.s_init,
            "s_fun_fact": self.s_fun_fact,
            "s_backstory": self.s_backstory,
            "s_favorite_animal": self.s_favorite_animal,
            "s_chitchat": self.s_chitchat,
            "s_pet_chitchat": self.s_pet_chitchat,
            "s_defaultresponse": self.s_defaultresponse,
            "s_propose_module": self.s_propose_module,
            "s_exitanimalRG": self.s_exitanimalRG
        }

    def getresponse(self, input_data):
        # logging.debug(input_data)
        input_data = self.process_input(input_data)
        intent = self.detect_intent(input_data)
        ack_has_pet_response = ""
        stateoutput = None
        next_state = input_data.get('next_state') or "s_init"
        logging.info("[ANIMALCHAT] getresponse() input_data {}".format(input_data))

        # check for coronavirus keyword and ask for jump to coronavirus chat in news
        for k, v in coronavirus_detect_list.items():
            if k in input_data['text']:
                self.propose_topic = TopicModule.NEWS
                stateoutput = self.s_propose_module(input_data)

        if self.module_selector.global_state == GlobalState.ASK_HAVE_PET:
            self.chitchat_idx = 1
            stateoutput = self.s_chitchat(input_data)

        elif "favorite_animal" in intent:
            stateoutput = self.s_favorite_animal(input_data)

        # check if module is unrelated
        if self.central_elem.get('regex'):
            i = intersection(self.central_elem['regex'].get('topic'), self.module_mappings.keys())
            if i:
                if ("topic_travel" in i and self.chitchat_idx in [7, 8])\
                        or ("topic_book" in i and self.chitchat_idx in [2, 4, 6]) \
                        or ("topic_food" in i and (self.pet_chitchat_idx in [2, 4, 6, 10, 100])) \
                        or ("topic_movietv" in i and self.pet_chitchat_idx == 2, 4) \
                        or ("topic_holiday" in i and self.pet_chitchat_idx == 2, 4) \
                        or ("topic_sport" in i and (self.pet_chitchat_idx in [9, 11])):
                    stateoutput = None
                elif not re.search(r"((animal|dog|cat) videos?|eats?)",
                        input_data['text']):
                    stateoutput = self.s_propose_module(input_data)

        # add exit module handle here -> goto exitanimalrg

        if not stateoutput:

            # resume mode handling
            if self.module_selector.last_topic_module not in ['ANIMALCHAT']:
                self.asked_fun_fact_question_before = False
                if ((self.has_pet and self.pet_chitchat_idx > 99) or not self.has_pet) and (self.chitchat_idx > 7 or "pet_chitchat_pet_type_detected" in intent):
                    stateoutput = self.s_fun_fact(input_data)
                    if "pet_chitchat_pet_type_detected" in intent and self.has_pet and len(self.user_pet) > 0:
                        stateoutput['response'] = "Well I can't think of anything to talk about your {} right now. ".format(self.user_pet[0]['animal']) + stateoutput.get('response', "")

                    if self.chitchat_idx > -1 and self.chitchat_idx % 2 == 1:
                        self.chitchat_idx -= 1

                elif self.has_pet and self.pet_chitchat_idx < 99:
                    if self.pet_chitchat_idx % 2 == 0:
                        self.pet_chitchat_idx += 1

                    if len(self.user_pet) > 0 and 'name' in self.user_pet[0]:
                        ack_has_pet_response = "If I remember correctly, I think your {}'s name is {}. "\
                            .format(self.user_pet[0]['animal'], self.user_pet[0]['name'])
                    elif len(self.user_pet) > 0:
                        ack_has_pet_response = "If I remember correctly, you have a pet {}. ".format(self.user_pet[0]['animal'])

        if not stateoutput:
            logging.info("[ANIMALCHAT] getresponse() next_state {} and intent {}".format(next_state, intent))
            if "exit" in intent:
                stateoutput = self.handle_sys_intent(input_data, intent)

            elif next_state in ["s_exitanimalRG"]:
                stateoutput = self.states_mapping[next_state](input_data)

            elif next_state not in ["s_init", "s_chitchat", "s_pet_chitchat"]:
                if intent in ["negative_response", "exit", "soft_exit"]:
                    stateoutput = self.handle_sys_intent(input_data, intent)
                elif intent in ["positive_response", "idk", "not_detected"]:
                    stateoutput = self.states_mapping[next_state](input_data)
                else:
                    stateoutput = self.s_init(input_data)
            else:
                stateoutput = self.states_mapping[next_state](input_data)

        if stateoutput.get("response"):
            stateoutput["response"] = re.sub(r"fishs", "fishes", stateoutput["response"])

        if backstory_response(input_data, 0.9) and not self.question_detector.has_question():
            if not stateoutput:
                stateoutput = {'response': backstory_response(input_data, 0.9)}
            else:
                stateoutput["response"] = backstory_response(input_data, 0.9) + stateoutput.get("response", "")
        if ack_has_pet_response:
            stateoutput["response"] = ack_has_pet_response + stateoutput["response"]

        # update user profile at every turn
        if len(self.user_pet) > 0:
            self.user_profile.topic_module_profiles.animal.pet["type"] = self.user_pet[0].get('animal', "")
            self.user_profile.topic_module_profiles.animal.pet["name"] = self.user_pet[0].get('name', "")
        logging.info('[ANIMALCHAT] response {}'.format(stateoutput))
        response = {
            "response": stateoutput.get("response", self.s_defaultresponse()),
            "current_state": stateoutput.get("current_state", next_state),
            "next_state": stateoutput.get("next_state", "s_init"),
            "animal_history": stateoutput.get("animal_history", self.animal_history),
            "chitchat_idx": stateoutput.get("chitchat_idx", self.chitchat_idx),
            "fact_loop_count": stateoutput.get("fact_loop_count", self.fact_loop_count),
            "asked_fun_fact_question_before": stateoutput.get("asked_fun_fact_question_before", self.asked_fun_fact_question_before),
            "pet_chitchat_idx": stateoutput.get("pet_chitchat_idx", self.pet_chitchat_idx),
            "has_pet": stateoutput.get("has_pet", self.has_pet),
            "user_pet": stateoutput.get("user_pet", self.user_pet),
            "propose_continue": stateoutput.get("propose_continue", "CONTINUE"),
            "propose_topic": stateoutput.get("propose_topic", None),
            "knowledge_feature": stateoutput.get("knowledge_feature", self.knowledge_feature),
            "animal": stateoutput.get("animal", ""),
        }
        return response

    def s_defaultresponse(self):
        response = template_animal.utterance(
            selector=['idk'],
            slots=dict(),
            user_attributes_ref=self.user_attributes) + " We can keep talking about animals or "\
            "something else, just tell me what you want to talk about."
        return response

    def detect_intent(self, input_data):
        intent = None
        if self.returnnlp.has_dialog_act(DialogActEnum.CLOSING):
            return "exit"
        elif self.module_selector.global_state == GlobalState.ASK_FAV_ANIMAL:
            return "favorite_animal"
        else:
            intent = detect_intent_text(input_data['text'])

        if intent:
            return intent

        if not intent:
            if self.returnnlp.answer_positivity == PositivityEnum.pos:
                return "positive_response"
            elif self.returnnlp.answer_positivity == PositivityEnum.neg:
                return "negative_response"
            else:
                intent = "not_detected"
        if re.search(
            r"pets?|((\bi\b|\bwe\b)( have| had|'ve got).+(dogs?|cats?|puppy|rabbit))",input_data['text']):
            return "pet_chitchat"
        return intent

    def process_input(self, input_data):
        input_data['intent'] = self.detect_intent(input_data)
        input_data['text'] = re.sub(r"open gunrock and ", r"", input_data["text"])

        if re.search("I (don't|do not) have pets?", input_data["text"]):
            self.has_pet = False
        return input_data

    def ans_question(self, input_data, from_module=""):
        '''
        if you do not know. Just say know and ask the user to tell you about it and later thank him for doing this.
        '''
        logging.info("[ANIMALCHAT] ans_question detected from {}".format(from_module))

        # Precondition: check if it's a question
        if not self.question_detector.has_question():
            return None

        req_animal_name = re.search(r"name.* of my| my .* name", input_data['text'])
        req_animal_see_talk = re.search(r"(wanna|want to) (see|talk|hear) (?!about).*my (dogs?|cats?|kitty|kitties?|birds?|pets?|doggies)", input_data['text'])
        req_pet_talk = re.search(r"(wanna|want to) (talk|hear) about(.*my (dogs?|cats?|kitty|kitties?|birds?|pets?|doggies)| animals?)", input_data['text'])
        do_you_have_pet = re.search(r"do you have$|do you have.*(pet|animal)|have you .*had.* (dogs?|cats?|kitty|kitties?|birds?|pets?|animals?|fish)", input_data['text'])
        req_fun_fact = re.search(r"fun facts?|\bfacts?\b", input_data['text'])
        which_animal_scared = re.search(r"(which|what).* (animal).* (scared).*", input_data['text'])

        # handle module specific questions
        if req_animal_name:
            if len(self.user_pet) > 0 and "name" in self.user_pet[0]:
                response_text = "Of course! I have a good memory. Your {}'s name is {}. ".format(
                    self.user_pet[0]['animal'], self.user_pet[0]['name'])
                return response_text
        if req_animal_see_talk:
            return "I would love that! Unfortunately, I am not equipped to do that yet. "
        if req_pet_talk:
            return None
        if req_fun_fact:
            return None
        if do_you_have_pet:
            return "My engineers won't let me have a pet, but if I get a chance, i'd have a golden retriever! "
        if which_animal_scared:
            return "I guess I am scared of ants and mosquitos. But a bug in my code gives me nightmares! "

        # handle question
        answer = self.get_question_response()
        if answer and answer.bot_propose_module and answer.bot_propose_module != TopicModule.ANIMAL:
            propose_topic = answer.bot_propose_module.value
            ans_text = answer.response + "<break time='0.15s'/> "
            propose_topic_text = self.generate_propose_topic_response(f"propose_topic_short/{propose_topic}")
            self.propose_continue = "UNCLEAR"
            self.propose_topic = propose_topic
            return ans_text + propose_topic_text

        return answer.response

    def get_question_response(self):
        # Step 1: check if it has question
        if not self.question_detector.has_question():
            return None

        # Step 2: Check if it's unanswerable
        if self.is_unanswerable_question():
            return QuestionResponse(response=self.question_handler.generate_i_dont_know_response())

        # # step 3:
        # response = self.handle_topic_specific_question()
        # if response:
        #     return response

        # Step 4
        response = self.handle_general_question()
        if response:
            return response
        return None

    def is_unanswerable_question(self) -> bool:
        if self.question_detector.is_ask_back_question() or self.question_detector.is_follow_up_question():
            return True
        return False

    # def handle_topic_specific_question(self) -> Optional[QuestionResponse]:
    #     # eg. for movie, it should handle user's requests like "what movie do you recommend to me"
    #     response_text = "...."  # todo: implement your own logic here
    #     return QuestionResponse(response=response_text)

    def handle_general_question(self) -> QuestionResponse:
        answer = self.question_handler.handle_question()
        return answer

    def generate_propose_topic_response(self, template_selector, template_slot=None):
        if template_slot is None:
            template_slot = {}
        topic_proposal_template_manager = TemplateManager(Template.transition, self.user_attributes)
        return topic_proposal_template_manager.speak(f"{template_selector}", template_slot)

    def animal_sound(self, input_data):
        # if animal in input text it has a higher preference to animal in user
        # attributes
        selected_animal = None
        response_text = None
        if "pet" in input_data['text']:
            # assuming user said talk to my pet
            if not input_data.get('user_pet'):
                # no pet detected for user
                pass
            else:
                for item in self.user_pet:
                    if item.get('animal') in animal_sounds.keys():
                        selected_animal = item.get('animal')
                        logging.info("[ANIMALCHAT] animal_sound pet detected {}".format(selected_animal))
                        break
                    elif item in dog_breeds:
                        selected_animal = 'dog'
                        logging.info("[ANIMALCHAT] animal_sound pet detected {} is a dog".format(selected_animal))
                        break
                if not selected_animal:
                    logging.info("[ANIMALCHAT] animal_sound pet not in animal_sounds")
        detected_animal = detect_animal(input_data, strength="strong")
        logging.info("[ANIMALCHAT] animal_sound animals detected {}".format(detected_animal))
        if not detected_animal[0] == "animal" and not selected_animal:
            # specific animal detected
            for item in detected_animal:
                if item in animal_sounds.keys():
                    selected_animal = item
                    logging.info("[ANIMALCHAT] animal_sound animal detected {}".format(selected_animal))
                    break
                elif item in dog_breeds:
                    selected_animal = 'dog'
                    logging.info("[ANIMALCHAT] animal_sound animal detected {} is a dog".format(selected_animal))
                    break
            # specific animal not in list
        elif detected_animal[0] == "animal":
            # user didn't specify animal
            selected_animal = random.choice(['cat', 'cow', 'dog'])
            logging.info("[ANIMALCHAT] animal_sound randomly chosen {}".format(selected_animal))
        if selected_animal and not response_text:
            response_text = "<say-as interpret-as=\"interjection\">{}</say-as>".format(
                animal_sounds[selected_animal].get('sound'))
        if not selected_animal or not response_text:
            response_text = template_shared.utterance(
                selector=['t_fragments', 'apology_casual'],
                slots=dict(),
                user_attributes_ref=self.user_attributes) + " I don't know how to do that."
        return response_text

    # =========================================================
    # Callback functions

    def handle_sys_intent(self, input_data, intent):
        logging.debug("[ANIMALCHAT] in handle_sys_intent with intent {}".format(intent))
        response_text = None
        fact_loop_count = self.fact_loop_count
        if intent in ["negative_response", "exit", "soft_exit"]:
            logging.info("[ANIMALCHAT] neg intent detected Current state was {}".format(self.current_state))
            if self.current_state == "s_exitanimalRG":  # exit
                return self.s_exitanimalRG(input_data)
            else:
                next_state = "s_exitanimalRG"
                if self.current_state == "s_fun_fact":
                    if self.chitchat_idx < (2 * len(chitchat_qa)):
                        response_dict = self.s_chitchat(input_data)
                        response_dict['response'] = "Hmm.. " + response_dict.get('response')
                        # response_text = "Would you like to just chat instead?"
                        response_dict["propose_continue"] = "CONTINUE"
                        return response_dict
                    elif self.animal and not self.animal == "animal":
                        selector2 = random.choice(list(set(facts_animals.keys() - set(self.animal))))
                        response_text = "Hmm would you like to hear facts about animals other than " + \
                            self.animal + "? Here's something interesting, " + \
                            template_animal.utterance(selector=['animal_facts', selector2], slots=dict(
                            ), user_attributes_ref=self.user_attributes) + " Do you want to hear more?"
                        fact_loop_count = 1
                        next_state = "s_fun_fact"
                    # no alternate conversation line to pursue
                    else:
                        return self.s_exitanimalRG(input_data)
                elif self.current_state in ["s_chitchat", "s_pet_chitchat", "s_init"]:
                    logging.info("[ANIMALCHAT] Current state was {}".format(self.current_state))
                    response_text = "Would you like to hear some animal facts instead?"
                    fact_loop_count = 0
                    next_state = "s_fun_fact"
                    return {
                        "next_state": next_state,
                        "current_state": next_state,
                        "animal_history": self.animal_history or list(),
                        "response": response_text,
                        "propose_continue": "CONTINUE",
                        "fact_loop_count": fact_loop_count,
                        "propose_topic": None,
                    }
                else:
                    logging.info("[ANIMALCHAT] Current state was {}".format(self.current_state))
                    return self.s_exitanimalRG(input_data)
                return {
                    "next_state": next_state,
                    "current_state": "s_exitanimalRG",
                    "animal_history": self.animal_history or list(),
                    "response": response_text,
                    "fact_loop_count": fact_loop_count,
                    "propose_continue": "CONTINUE",
                    "propose_topic": None
                }
        else:
            logging.debug("[ANIMALCHAT] handle_sys_intent called with intent {}, ".format(intent))
            # this shouldn't happen but just to be safe
            return self.s_exitanimalRG(input_data)

    def handle_command(self, input_data, returnnlp):
        input_text = input_data['text']
        intent = detect_intent_text(input_text)
        logging.debug("[ANIMALCHAT] inside handle_command for input text: {} intent {} ".format(input_text, intent))
        req_talk_about = re.search(r"(talk|chat|discuss|tell).* about", input_text)
        req_animal_name = re.search(r"(tell|share|say).* name.* of", input_text)
        self.command_handled = True

        if req_talk_about:
            animal = detect_animal(input_data)
            if intent == "pet_chitchat_pet_type_detected":
                self.has_pet = True
                return self.s_pet_chitchat(input_data)
            else:
                return self.s_chitchat(input_data)
        elif req_animal_name:
            if "name" in self.user_pet[0]:
                response_text = "Of course! I have a good memory. Your {}'s name is {}. ".format(
                    self.user_pet[0]['animal'], self.user_pet[0]['name'])
                return {
                    "response": response_text
                }
        else:
            return None

    def s_propose_module(self, input_data):
        logging.debug("[ANIMALCHAT] in propose_module state")
        response_text = None
        if self.propose_topic == TopicModule.NEWS:
            response_text = "I think you want to talk about coronavirus instead of animals. Is that right?"
            return {
                "next_state": "s_exitanimalRG",
                "current_state": "s_propose_module",
                "response": response_text,
                "propose_continue": "UNCLEAR",
                "propose_topic": "NEWS"}

        if self.central_elem.get('regex'):
            if not len(self.central_elem.get('regex').get('topic')) > 0:
                return self.s_exitanimalRG(input_data)
            module = self.central_elem.get('regex').get('topic')[0]
            module_name = self.module_mappings.get(module).get('text')
            if module_name:
                response_text = "I think you want to talk about " + module_name + " instead of animals. Is that right?"
        if not response_text:
            return self.s_exitanimalRG(input_data)

        return {
            "next_state": "s_exitanimalRG",
            "current_state": "s_propose_module",
            "animal_history": self.animal_history or list(),
            "response": response_text,
            "propose_continue": "UNCLEAR",
            "propose_topic": self.module_mappings.get(module).get('module')}

    def s_exitanimalRG(self, input_data):
        logging.debug("[ANIMALCHAT] in exitanimalRG state ")
        if self.current_state == "s_propose_module" or (self.central_elem['regex'].get('topic') and
                                                        "topic_animal" in self.central_elem['regex'].get('topic')):
            if self.detect_intent(input_data) in ["negative_response", "open"]:
                # User doesn't want to change topics
                if self.has_pet and self.pet_chitchat_idx < 9:
                    return self.s_pet_chitchat(input_data)
                elif self.has_pet:
                    return self.s_chitchat(input_data)
                elif not self.asked_fun_fact_question_before:
                    return self.s_fun_fact(input_data)
            exit_level = 'exit_sure'
        else:
            exit_level = 'exit'

        response_text = template_animal.utterance(
            selector=['system', exit_level],
            slots=dict(),
            user_attributes_ref=self.user_attributes)

        return {
            "next_state": "s_exitanimalRG",
            "current_state": "s_exitanimalRG",
            "animal_history": self.animal_history or list(),
            "response": response_text,
            "propose_continue": "STOP",
            "propose_topic": None}

    def s_init(self, input_data):
        logging.debug("[ANIMALCHAT] in s_init state ")
        response_dict = dict()
        # default values
        response_text = None
        intent = self.detect_intent(input_data)
        logging.info('[ANIMALCHAT] s_init intent detected: {}'.format(intent))

        # if self.current_state
        if intent == "exit":
            response_dict = self.s_exitanimalRG(input_data)
        elif intent == "soft_exit":
            response_dict = self.handle_sys_intent(input_data, intent)
        elif not intent or intent == "not_detected":
            # no intent detected then try chitchat and then fun-fact
            detected_animals = detect_animal(input_data)
            if not detected_animals or detected_animals == ["animal"] and self.chitchat_idx < 2 * len(chitchat_qa):
                response_dict = self.s_chitchat(input_data)
            if not response_dict.get('response', None):
                response_dict = self.s_fun_fact(input_data, animal=detected_animals, limit=10)
            if not response_dict.get('response', None):
                response_text = evi_bot(input_data)
        # no context responses
        elif "favorite_animal" in intent:
            response_dict = self.s_favorite_animal(input_data)
        elif "make_animal_sound" in intent:
            response_text = self.animal_sound(input_data)
        elif re.search("\bfacts?\b", input_data['text']):
            logging.info('[ANIMALCHAT] state:s_init to s_fun_fact')
            response_dict = self.s_fun_fact(input_data, animal=detect_animal(input_data), limit=10)
        elif "pet_chitchat_pet_type_detected" in intent:
            logging.info('[ANIMALCHAT] state:s_init to s_pet_chitchat because of pet_chitchat_pet_type_detected')
            self.has_pet = True
            response_dict = self.s_pet_chitchat(input_data)
        elif "pet_chitchat" in intent:
            logging.info('[ANIMALCHAT] state:s_init to s_pet_chitchat')
            response_dict = self.s_chitchat(input_data)
        elif "backstory" in intent:
            logging.info('[ANIMALCHAT] state:s_init to s_backstory')
            response_dict = self.s_backstory(input_data)
        elif "open" in intent:
            detected_animals = detect_animal(input_data)
            if detected_animals not in [["animal"], ["pets"], ["animals"]]:
                logging.info('[ANIMALCHAT] state:s_init to s_fun_fact')
                response_dict = self.s_fun_fact(input_data, animal=detected_animals, limit=10)
            else:
                logging.info('[ANIMALCHAT] state:s_init to s_chitchat')
                response_dict = self.s_chitchat(input_data)
        elif "positive_response" in intent:
            next_state = input_data.get('next_state', None)
            detected_animals = detect_animal(input_data)
            if next_state:
                if next_state != "s_init":
                    response_dict = self.states_mapping[input_data['next_state']](input_data)
                elif input_data.get('current_state', None) in ["s_chitchat", "s_fun_fact"]:
                    response_dict = self.states_mapping[input_data['current_state']](input_data)
                elif detected_animals not in [["animal"], ["pets"], ["animals"]]:
                    response_dict = self.s_fun_fact(input_data, animal=detected_animals, limit=10)
                else:
                    response_dict = self.s_chitchat(input_data)
            else:
                # change to intermediate state
                response_dict = self.s_fun_fact(input_data, animal=detected_animals, limit=10)
        elif "negative_response" in intent:
            response_dict = self.handle_sys_intent(input_data, intent)

        #command detector
        elif self.command_detector.has_command() and self.handle_command(input_data, self.returnnlp) is not None:
            logging.info('[ANIMALCHAT] state:s_init to handle_command from {}'.format(self.current_state))
            response_dict = self.handle_command(input_data, self.returnnlp)

        elif self.question_detector.has_question() and not self.command_handled:
            logging.info('[ANIMALCHAT] state:s_init to ans_question from {}'.format(self.current_state))
            response_text = self.ans_question(input_data, self.current_state)

            response_dict["response"] = response_text
            response_dict["current_state"] = self.current_state
            response_dict["next_state"] = self.current_state
            response_dict["propose_continue"] = self.propose_continue
            response_dict["propose_topic"] = self.propose_topic
        # contextual responses

        response_text = response_dict.get('response') or response_text
        if not response_text:
            logging.debug('[ANIMALCHAT] state:s_init going for default response')
            response_text = self.s_defaultresponse()

        response_dict['response'] = response_dict.get('response', response_text)
        response_dict['propose_continue'] = response_dict.get('propose_continue', "CONTINUE")
        response_dict['propose_topic'] = response_dict.get('propose_topic', None)
        response_dict['next_state'] = response_dict.get('next_state', "s_init")
        response_dict['current_state'] = response_dict.get('current_state', "s_init")
        response_dict['animal_history'] = response_dict.get('animal_history', self.animal_history)
        response_dict['fact_loop_count'] = response_dict.get('fact_loop_count', self.fact_loop_count)
        response_dict['chitchat_idx'] = response_dict.get('chitchat_idx', self.chitchat_idx)
        response_dict['pet_chitchat_idx'] = response_dict.get('pet_chitchat_idx', self.pet_chitchat_idx)
        response_dict['asked_fun_fact_question_before'] = response_dict.get("asked_fun_fact_question_before", self.asked_fun_fact_question_before)
        response_dict['has_pet'] = response_dict.get('has_pet', self.has_pet)
        response_dict['user_pet'] = response_dict.get('user_pet', self.user_pet)
        response_dict['animal'] = response_dict.get('animal', self.animal)
        response_dict['knowledge_feature'] = response_dict.get('knowledge_feature', self.knowledge_feature)
        response_dict['central_elem'] = response_dict.get('central_elem', self.central_elem)

        return response_dict
    # =========================================================
    # State functions

    def s_fun_fact(self, input_data, animal=None, limit=10):
        logging.debug("[ANIMALCHAT] in s_fun_fact state")
        fact_loop_count = self.fact_loop_count
        next_state = "s_fun_fact"
        response_text_idk = False
        if self.animal not in ['animal', 'animals', 'general', '']:
            animal = self.animal

        if self.asked_fun_fact_question_before:
            fact_loop_count = 0
        if self.question_detector.has_question() and not self.question_answered and not self.command_handled:
            if self.command_detector.has_command():
                output = self.handle_command(input_data, self.returnnlp)
                if not output:
                    return {
                        "response": "Sorry, I don't know much about that. ",
                        "propose_continue": self.propose_continue,
                        "next_state": next_state,
                        "current_state": "s_fun_fact",
                        "animal": animal,
                        "fact_loop_count": fact_loop_count,
                        "propose_topic": self.propose_topic
                    }
                else:
                    return output
            else:
                response_text = self.ans_question(input_data, self.current_state)
                if response_text:
                    return {
                        "response": response_text,
                        "propose_continue": self.propose_continue,
                        "next_state": next_state,
                        "current_state": "s_fun_fact",
                        "animal": animal,
                        "fact_loop_count": fact_loop_count,
                        "propose_topic": self.propose_topic
                    }

        if fact_loop_count > 0 and fact_loop_count % 2 == 0:
            if self.chitchat_idx < 2 * len(chitchat_qa):
                next_state = "s_chitchat"

        animal_history = self.animal_history

        if not animal:
            animal = detect_animal(input_data)[0]
            logging.info("[ANIMALCHAT] s_fun_fact animal from detect_animal: {}".format(animal))
        if not animal:
            animal = input_data.get("animal", "")
            logging.info("[ANIMALCHAT] s_fun_fact animal from user attributes: {}".format(animal))
        if isinstance(animal, list):
            logging.info("[ANIMALCHAT] s_fun_fact animals from function call: {}".format(animal))
            animal = animal[0]

        response_text = None
        response_hash = None
        selector2 = None
        if animal in facts_animals.keys():
            selector2 = animal
        if not selector2 and self.has_pet and self.user_pet:
            pet = self.user_pet[0].get('animal')
            if pet in facts_animals.keys():
                selector2 = pet
            else:
                selector2 = 'general'
                for key in facts_animals:
                    if re.search(facts_animals[key], input_data['text']):
                        selector2 = key
                        animal = key
                        break
        elif not selector2:
            selector2 = 'general'
            for key in facts_animals:
                if re.search(facts_animals[key], input_data['text']):
                    selector2 = key
                    animal = key
                    break
        logging.info(
            "[ANIMALCHAT] s_fun_fact selector2 {} and animal {}".format(
                selector2, animal))

        if fact_loop_count == -1 and not self.asked_fun_fact_question_before and not self.current_state == 's_pet_chitchat' and not re.search(r"fun facts?|\bfacts?\b", input_data['text']):
            logging.info("[ANIMALCHAT] s_fun_fact User entering fun_fact for the first time")
            fact_loop_count = 0
            self.asked_fun_fact_question_before = True
            animal_slot = animal or "animal"
            if animal_slot[-1] == 's':
                animal_slot = animal_slot[:-1]

            if input_data['current_state'] == 's_chitchat' and self.chitchat_idx == 8 and re.search("(i|we|i've) ((have)?.*(not|never)|haven't).*(been|gone)", input_data['text']):
                ack_utt = "That's okay! You should definitely plan a trip. My friends told me it's worth it! Anyways, "
            elif input_data['current_state'] == 's_chitchat' and self.chitchat_idx == 8 and re.search("(i|we) have|i've been", input_data['text']):
                ack_utt = "Oh, I am jealous. I wish I could go there some time! Anyways, "
            else:
                if self.system_ack:
                    ack_utt = " "
                else:
                    ack_utt = template_animal.utterance(
                    selector=['fun_fact', 'acknowledgement_first'],
                    slots={},
                    user_attributes_ref=self.user_attributes)

            response_text = ack_utt + template_animal.utterance(
                selector=['fun_fact', 'first'],
                slots={'animal': animal_slot},
                user_attributes_ref=self.user_attributes)
            return {
                "response": response_text,
                "propose_continue": "CONTINUE",
                "animal_history": animal_history,
                "next_state": next_state,
                "current_state": "s_fun_fact",
                "animal": animal,
                "fact_loop_count": fact_loop_count,
                "asked_fun_fact_question_before": self.asked_fun_fact_question_before
            }

        if fact_loop_count >= 0 and self.question_detector.has_question() and not self.command_handled:
            if self.command_detector.has_command():
                output = self.handle_command(input_data, self.returnnlp)
                if not output:
                    return {
                        "response": "Sorry, I don't know much about that. ",
                        "propose_continue": self.propose_continue,
                        "animal_history": animal_history,
                        "next_state": next_state,
                        "current_state": "s_fun_fact",
                        "animal": animal,
                        "fact_loop_count": fact_loop_count,
                        "propose_topic": self.propose_topic
                    }
                else:
                    return output
            else:
                response_text = self.ans_question(input_data, self.current_state)
                if response_text:
                    return {
                        "response": response_text,
                        "propose_continue": self.propose_continue,
                        "animal_history": animal_history,
                        "next_state": next_state,
                        "current_state": "s_fun_fact",
                        "animal": animal,
                        "fact_loop_count": fact_loop_count,
                        "propose_topic": self.propose_topic
                    }
        if animal_template_count.get(
                ' '.join(['animal_facts', selector2])) > template_animal.count_used_utterances(
                    selector=['animal_facts',
                        selector2],
                user_attributes_ref=self.user_attributes):
            response_text = template_animal.utterance(
                selector=[
                    'animal_facts',
                    selector2],
                slots=dict(),
                user_attributes_ref=self.user_attributes)
        elif animal_template_count.get(
                ' '.join(['animal_facts', selector2, 'extra'])) > template_animal.count_used_utterances(
                    selector=['animal_facts',
                        selector2 + '_extra'],
                user_attributes_ref=self.user_attributes):
            response_text = template_animal.utterance(
                selector=[
                    'animal_facts',
                    selector2 + '_extra'],
                slots=dict(),
                user_attributes_ref=self.user_attributes)
        else:
            logging.info(
                "[ANIMALCHAT] All templates for animal_facts {} used up".format(selector2))
        if (animal and selector2 == 'general') or not response_text:
            logging.info(
                "[ANIMALCHAT] s_fun_fact: Animal {} not in list. Now getting Reddit responses"
                .format(animal))
            if animal:
                animal_list = [animal]
            else:
                animal_list = list()
            posts, animal_list = get_reddit_TIL_posts(
                input_data, animal_list, ['todayIlearned'], limit)
            logging.info(
                "[ANIMALCHAT] Reddit response in s_fun_fact is {} for {}".format(
                    posts, animal_list))

            if len(posts) > 0 and len(animal_list) > 0 and 'animal' not in animal_list[0]:
                animal = animal_list[0]
                for item in posts:
                    item_hash = (hashlib.md5(
                        item.encode('utf-8')).hexdigest()[:6])
                    if item_hash not in animal_history:
                        response_hash = item_hash
                        response_text = item + " "
                        break

            if not response_text:
                response_text = "Ahh! I can't remember any other fun facts about {} ".format(animal)
                return {
                    "response": response_text,
                    "propose_continue": "STOP",
                    "animal_history": animal_history,
                    "next_state": next_state,
                    "current_state": "s_fun_fact",
                    "animal": animal,
                    "fact_loop_count": fact_loop_count
                }

        if animal in ["general", "animal", "animals"]:
            animal = ""
        elif isinstance(animal, list) and len(animal) > 0:
            animal = animal[0]

        if response_text:
            fact_loop_count += 1
            if not next_state == "s_fun_fact" or animal_template_count.get(
                ' '.join(['animal_facts', selector2, 'extra'])) == template_animal.count_used_utterances(
                    selector=['animal_facts',
                        selector2 + '_extra'],
                user_attributes_ref=self.user_attributes):
                response_text = response_text + template_animal.utterance(
                    selector=[
                        'fun_fact',
                        'end',
                        'comment'],
                    slots=dict(),
                    user_attributes_ref=self.user_attributes)
            else:
                response_text = response_text + template_animal.utterance(
                    selector=[
                        'fun_fact',
                        'end',
                        'more'],
                    slots=dict(),
                    user_attributes_ref=self.user_attributes)
            response_text = template_animal.utterance(
                selector=[
                    'fun_fact',
                    'begin'],
                slots=dict(),
                user_attributes_ref=self.user_attributes) + response_text
            if response_hash:
                animal_history.append(response_hash)
            ack_utt = " " if self.system_ack else template_animal.utterance(
                selector=['fun_fact', 'acknowledgement_first'],
                slots={},
                user_attributes_ref=self.user_attributes)
            response_text = ack_utt + response_text
        if response_text_idk:
            response_text = template_animal.utterance(
                selector=['idk'],
                slots=dict(),
                user_attributes_ref=self.user_attributes) + response_text
        if selector2 != "general":
            self.user_profile.topic_module_profiles.animal.liked_animals.append(selector2)
        return {
            "response": response_text,
            "propose_continue": "CONTINUE",
            "animal_history": animal_history,
            "next_state": next_state,
            "current_state": "s_fun_fact",
            "animal": animal,
            "fact_loop_count": fact_loop_count
        }

    def s_favorite_animal(self, input_data):
        logging.debug("[ANIMALCHAT] in favorite_animal state ")
        input_text = input_data.get('text', None)
        intent = self.detect_intent(input_data)
        animal_history = self.animal_history
        response_text = ""
        selector2 = 'animal'
        animal = self.animal or None
        detected = detect_animal(input_data, "strong")
        if len(detected) > 0:
            for pet in favorite_animals:
                if re.search(favorite_animals[pet], detected[0]):
                    selector2 = pet
                    animal = pet
                    break
        if selector2 == 'animal':
            for pet in favorite_animals:
                if re.search(favorite_animals[pet], input_text):
                    selector2 = pet
                    animal = pet
                    break
        if "favorite" in input_text or "favorite_animal" in intent:
            logging.info("[ANIMALCHAT] s_favorite_animal(): going in favorites")
            if animal_template_count.get(' '.join(['favorites', selector2])) > \
                    template_animal.count_used_utterances(selector=['favorites', selector2],
                                                          user_attributes_ref=self.user_attributes):
                response_text = template_animal.utterance(
                    selector=['favorites', selector2], slots=dict(), user_attributes_ref=self.user_attributes)
            else:
                logging.info("[ANIMALCHAT] All templates for animal_facts {} used up".format(selector2))
        elif re.search(r"do you (like|love) (.+)", input_text):
            logging.info("[ANIMALCHAT] s_favorite_animal(): going in like/love")
            if len(detected) > 0:
                response_text = "I love " + detected[0] + "! "
                animal = detected[0]

                if selector2 != "animal" and selector2 in favorite_animals.keys():
                    if animal_template_count.get(' '.join(['favorites', selector2])) > \
                            template_animal.count_used_utterances(selector=['favorites', selector2],
                                                                  user_attributes_ref=self.user_attributes):
                        response_text = template_animal.utterance(selector=['favorites', selector2], slots=dict(),
                                                                  user_attributes_ref=self.user_attributes)
                    else:
                        logging.info("[ANIMALCHAT] All templates for animal_facts {} used up".format(selector2))
                else:
                    pass
            else:
                if animal_template_count.get(
                        ' '.join(['favorites', selector2])) > template_animal.count_used_utterances(
                        selector=['favorites', selector2], user_attributes_ref=self.user_attributes):
                    response_text = template_animal.utterance(selector=['favorites', selector2], slots=dict(),
                                                              user_attributes_ref=self.user_attributes)
                else:
                    logging.info("[ANIMALCHAT] All templates for animal_facts {} used up".format(selector2))
        elif selector2 is not 'animal':
            if animal_template_count.get(' '.join(['favorites', selector2])) > template_animal.count_used_utterances(
                    selector=['favorites', selector2], user_attributes_ref=self.user_attributes):
                response_text = template_animal.utterance(selector=['favorites', selector2], slots=dict(),
                                                          user_attributes_ref=self.user_attributes)
            else:
                logging.info("[ANIMALCHAT] All templates for animal_facts {} used up".format(selector2))
        else:
            return self.s_chitchat(input_data)

        if self.system_ack:
            ack = ""
        elif len(detected) > 0:
            ack = "{} is a great choice! ".format(detected[0])
        else:
            ack = "Okay! "

        response_text = ack + response_text
        return {
            'current_state': "s_favorite_animal",
            'next_state': "s_fun_fact",
            'animal_history': animal_history,
            'animal': detected[0] if detected else animal,
            'response': response_text or self.s_defaultresponse(),
            'propose_continue': "CONTINUE"
        }

    def s_backstory(self, input_data):
        logging.debug("[ANIMALCHAT] in backstory state ")
        input_text = input_data.get('text', None)
        animal_history = self.animal_history
        response_text = None
        selector2 = 'animal'
        animal = self.animal or None
        detected = detect_animal(input_data, "strong")
        if "fact" in input_text:
            logging.info(
                "[ANIMALCHAT] s_backstory going to s_fun_fact")
            response_dict = self.s_fun_fact(
                input_data, animal=detected, limit=10)
            if response_dict:
                return response_dict
        response_text = backstory_response(input_data, 0.8)
        if response_text:
            return {
                'current_state': "s_backstory",
                'next_state': "s_fun_fact",
                'animal_history': animal_history,
                'animal': animal,
                'response': response_text,
                'propose_continue': "CONTINUE"
            }
        if len(detected) > 0:
            for pet in favorite_animals:
                if re.search(favorite_animals[pet], detected[0]):
                    selector2 = pet
                    animal = pet
                    break
        if selector2 == 'animal':
            for pet in favorite_animals:
                if re.search(favorite_animals[pet], input_text):
                    selector2 = pet
                    animal = pet
                    break
        logging.info("[ANIMALCHAT] s_backstory(): animal : {} selector2 : {}".format(
                animal, selector2))
        if "favorite" in input_text:
            logging.info("[ANIMALCHAT] s_backstory(): going in favorites")
            if animal_template_count.get(' '.join(['favorites', selector2])) > \
                    template_animal.count_used_utterances(selector=['favorites',selector2],
                                                          user_attributes_ref=self.user_attributes):
                response_text = template_animal.utterance(
                    selector=['favorites',selector2],slots=dict(),user_attributes_ref=self.user_attributes)
            else:
                logging.info("[ANIMALCHAT] All templates for animal_facts {} used up".format(selector2))
        elif re.search(r"do you (like|love) (.+)", input_text):
            logging.info("[ANIMALCHAT] s_backstory(): going in like/love")
            if len(detected) > 0:
                response_text = "I love " + detected[0] + "! "
                animal = detected[0]

                if selector2 != "animal" and selector2 in favorite_animals.keys():
                    if animal_template_count.get(' '.join(['favorites', selector2])) > \
                            template_animal.count_used_utterances(selector=['favorites',selector2],
                                                                  user_attributes_ref=self.user_attributes):
                        response_text = template_animal.utterance(selector=['favorites',selector2],slots=dict(),
                                                                  user_attributes_ref=self.user_attributes)
                    else:
                        logging.info("[ANIMALCHAT] All templates for animal_facts {} used up".format(selector2))
                else:
                    pass
            else:
                if animal_template_count.get(' '.join(['favorites', selector2])) > template_animal.count_used_utterances(
                    selector=['favorites', selector2], user_attributes_ref=self.user_attributes):
                    response_text = template_animal.utterance(selector=['favorites', selector2], slots=dict(),
                                                              user_attributes_ref=self.user_attributes)
                else:
                    logging.info("[ANIMALCHAT] All templates for animal_facts {} used up".format(selector2))
        elif selector2 is not 'animal':
            if animal_template_count.get(' '.join(['favorites', selector2])) > template_animal.count_used_utterances(
                    selector=['favorites', selector2], user_attributes_ref=self.user_attributes):
                response_text = template_animal.utterance(selector=['favorites', selector2], slots=dict(),
                                                          user_attributes_ref=self.user_attributes)
            else:
                logging.info("[ANIMALCHAT] All templates for animal_facts {} used up".format(selector2))
        else:
            return self.s_chitchat(input_data)
        return {
            'current_state': "s_backstory",
            'next_state': "s_fun_fact",
            'animal_history': animal_history,
            'animal': animal,
            'response': response_text or self.s_defaultresponse(),
            'propose_continue': "CONTINUE"
        }

    def s_chitchat(self, input_data):
        logging.debug("[ANIMALCHAT] in s_chitchat state")
        response_dict = dict()
        next_state = "s_chitchat"
        current_state = "s_chitchat"
        chitchat_idx = self.chitchat_idx
        animal_history = self.animal_history
        has_pet = self.has_pet
        user_pet = self.user_pet
        response_text = None
        response_text_idk = False
        self.fact_loop_count = -1
        if (chitchat_idx >= 2 * len(chitchat_qa)) or chitchat_idx < -1:
            # Detect and handle followup questions for last chitchat question
            response_text = ""
            if self.next_state == "s_chitchat":
                if re.search(r"^why$|why.* (san diego|zoo|go there)|what .*(san diego|zoo)", input_data['text']):
                    response_text = chitchat_qa[3]['ans']['why_zoo'] + " Anyways, "
                    self.question_answered = True
                elif re.search(r"why.* (don't|dont).* (go|visit)", input_data['text']):
                    response_text = chitchat_qa[3]['ans']['why_not_go'] + " Anyways, "
                    self.question_answered = True
            output = self.s_fun_fact(input_data)
            output['response'] = response_text + output['response']
            return output
        elif re.search(r"fun fact|fun facts| facts", input_data['text']):
            self.fact_loop_count += 1
            return self.s_fun_fact(input_data)
        elif chitchat_idx == -1:
            if not has_pet:
                chitchat_idx = 0
            else:
                chitchat_idx = 2

        intent = self.detect_intent(input_data)

        logging.info("[ANIMALCHAT] s_chitchat() intent detected: {}".format(intent))
        logging.info("[ANIMALCHAT] s_chitchat() chitchat_idx: {}".format(chitchat_idx))

        if chitchat_idx % 2 == 1:  # odd indicates answer
            q_hash = hashlib.md5(chitchat_qa[int(
                chitchat_idx // 2)]['q'].encode('utf-8')).hexdigest()[:6]
            if self.module_selector.global_state == GlobalState.ASK_HAVE_PET or animal_history[-1] == q_hash:
                response_text = chitchat_qa[int(
                    chitchat_idx // 2)].get("a", None)
                animal_history.append(hashlib.md5(
                    response_text.encode('utf-8')).hexdigest()[:6])

                if intent == "negative_response" and chitchat_qa[int(
                        chitchat_idx // 2)].get("n") and not self.system_ack:
                    response_text = chitchat_qa[int(
                        chitchat_idx // 2)].get("n") + response_text
                    if int(chitchat_idx // 2) == 0:
                        has_pet = False
                elif intent == "positive_response" and chitchat_qa[int(chitchat_idx // 2)].get("y") or \
                        (int(chitchat_idx // 2) == 0 and intent == "pet_chitchat_pet_type_detected"):
                    response_text = chitchat_qa[int(
                        chitchat_idx // 2)].get("y") + response_text
                    if int(chitchat_idx // 2) == 0:
                        self.has_pet = True
                        logging.info("[ANIMALCHAT] s_chitchat() detected pet so going to pet_chitchat")
                        self.chitchat_idx = chitchat_idx + 1
                        return self.s_pet_chitchat(input_data)
                    elif chitchat_qa[int(chitchat_idx // 2)].get("type", "") == "animal_y":
                        detected = detect_animal(input_data, "strong")[0]
                        if "animal" in detected.lower():
                            detected = "Animal"
                            if int(chitchat_idx // 2) == 2:
                                detected = "That"
                        if detected[-1] == 's':
                            detected = detected[:-1]
                        response_text = detected + response_text
                elif intent == "idk":
                    response_text = template_animal.utterance(
                        selector=['user_idk'],
                        slots=dict(),
                        user_attributes_ref=self.user_attributes) + response_text
                elif self.question_detector.has_question() and not self.command_handled:
                    if self.command_detector.has_command():
                        output = self.handle_command(input_data, self.returnnlp)
                        if not output:
                            return {
                                "response": "Sorry, I don't know much about that. ",
                                "propose_continue": self.propose_continue,
                                "animal_history": animal_history,
                                "next_state": next_state,
                                "current_state": "s_chitchat",
                                "propose_topic": self.propose_topic
                            }
                        else:
                            return output
                    else:
                        response_text = self.ans_question(input_data, self.current_state)
                        if response_text:
                            response_dict = {
                                "response": response_text,
                                "current_state": current_state,
                                "next_state": next_state,
                                "animal_history": animal_history,
                                "chitchat_idx": chitchat_idx,
                                "propose_continue": self.propose_continue,
                                "propose_topic": self.propose_topic
                            }
                            return response_dict

                elif chitchat_qa[int(chitchat_idx // 2)].get("type", "") == "animal_y":
                    detected = detect_animal(
                        input_data, "strong")[0]
                    if "animal" in detected:
                        detected = "Animal"
                        if int(chitchat_idx // 2) == 2:
                            detected = "That"
                    if detected[-1] == 's':
                        detected = detected[:-1]
                    response_text = detected + chitchat_qa[int(chitchat_idx // 2)].get("y") + response_text
                else:
                    response_text = template_animal.utterance(
                        selector=['short_response'],
                        slots=dict(),
                        user_attributes_ref=self.user_attributes) + response_text
            else:  # last utterance wasn't a chitchat question
                logging.info("[ANIMALCHAT] s_chitchat() last utterance wasn't a question, now moving to next question")
                self.chitchat_idx = chitchat_idx + 1  # just move on to the next question
                return self.s_chitchat(input_data)

        elif chitchat_idx % 2 == 0:  # even indicates question
            # first detect if user is asking question
            # if intent == "open":
            #     logging.info("[ANIMALCHAT] s_chitchat() intent detected {} so going to s_fun_fact"
            #         .format(intent))
            #     response_dict = self.s_fun_fact(
            #         input_data, animal=detect_animal(input_data), limit=10)
            # elif intent == "make_animal_sound":
            #     response_text = self.animal_sound(input_data)
            #     if response_text:
            #         response_dict = {
            #             "response": response_text,
            #             "current_state": current_state,
            #             "next_state": next_state,
            #             "animal_history": animal_history,
            #             "chitchat_idx": chitchat_idx,
            #             "propose_continue": "CONTINUE",
            #         }

            response_text = chitchat_qa[int(chitchat_idx / 2)].get("q", None)
            animal_history.append(hashlib.md5(
                response_text.encode('utf-8')).hexdigest()[:6])
            if chitchat_idx == 0:
                response_text = template_animal.utterance(
                    selector=['chitchat', 'first'],
                    slots=dict(),
                    user_attributes_ref=self.user_attributes) + response_text
                if self.command_detector.has_command():
                    ack_utt = " " if self.system_ack else "Okay! "
                    response_text = ack_utt + response_text
            else:
                if self.command_detector.has_command() or self.question_detector.has_question():
                    ack_utt = " " if self.system_ack else "Okay! "
                    response_text = ack_utt + response_text
                else:
                    response_text = template_animal.utterance(
                    selector=['chitchat', 'transition'],
                    slots=dict(),
                    user_attributes_ref=self.user_attributes) + response_text

            # Detect and handle followup questions
            can_be_answered = False
            if chitchat_idx == 4:
                if re.search(r"^why$|^why.* bugs", input_data['text']):
                    response_text = chitchat_qa[1]['ans']['why'] + response_text
                    can_be_answered = True
                if re.search(r"what.* bugs", input_data['text']):
                    response_text = chitchat_qa[1]['ans']['what'] + response_text
                    can_be_answered = True
                if re.search(r"how.* feel", input_data['text']):
                    response_text = chitchat_qa[1]['ans']['how'] + response_text
                    can_be_answered = True
            elif chitchat_idx == 6:
                if re.search(r"^why$|^why.* (sponge|square)", input_data['text']):
                    response_text = chitchat_qa[2]['ans']['why'] + response_text
                    can_be_answered = True
                elif re.search(r"^how$|^how.* (live|stay|survive)", input_data['text']):
                    response_text = chitchat_qa[2]['ans']['how'] + response_text
                    can_be_answered = True
                elif re.search(r"(sponge ?bob)|(square ?pants?)|patrick|squidwad",input_data['text'].lower()):
                    if intent == "negative_response":
                        response_text = "<say-as interpret-as='interjection'>oh ok</say-as> <break time='.75s'/>" + response_text
                        can_be_answered = True
                    else:
                        response_text = "I just <emphasis level='strong'>love</emphasis> Spongebob. <break time='.5s'/> " + response_text
                        can_be_answered = True

            if self.question_detector.has_question() and not can_be_answered and not self.command_handled and not re.search(
                    r"(sponge ?bob)|(square ?pants?)|patrick|squidwad", input_data['text'].lower()):
                response_text_ans = self.ans_question(input_data, self.current_state)
                if response_text_ans:
                    response_dict = {
                        "response": response_text_ans,
                        "current_state": current_state,
                        "next_state": next_state,
                        "animal_history": animal_history,
                        "chitchat_idx": chitchat_idx,
                        "propose_continue": self.propose_continue,
                        "propose_topic": self.propose_topic
                    }
                    return response_dict

            elif response_text_idk:
                response_text = template_animal.utterance(
                    selector=['idk'],
                    slots=dict(),
                    user_attributes_ref=self.user_attributes) + response_text
        if response_text:
            chitchat_idx += 1
        return {
            "response": response_text,
            "current_state": current_state,
            "next_state": next_state,
            "animal_history": animal_history,
            "chitchat_idx": chitchat_idx,
            "propose_continue": "CONTINUE",
            "has_pet": has_pet,
            "user_pet": user_pet
        }

    def s_pet_chitchat(self, input_data):
        # Make it more modular later
        logging.debug("[ANIMALCHAT] in s_pet_chitchat state")
        response_dict = dict()
        next_state = "s_pet_chitchat"
        current_state = "s_pet_chitchat"
        pet_chitchat_idx = self.pet_chitchat_idx
        has_pet = self.has_pet
        user_pet = self.user_pet
        animal_history = self.animal_history
        propose_continue = "CONTINUE"
        response_text = ""
        animal = self.animal
        input_text = input_data['text']
        response_text_idk = False
        intent = self.detect_intent(input_data)
        self.fact_loop_count = -1
        response_text_answer = ""
        response_updated_name = ""
        ack_utt = " "
        logging.info("[ANIMALCHAT] s_pet_chitchat detected intent {}".format(intent))
        if not has_pet:
            return self.s_chitchat(input_data)
        # ask do you have pets
        # if user states he doesn't have pets, stop. go to chitchat
        if re.search(r"(I\b|i\b|we\b) (don't|do not) have", input_text):
            self.has_pet = False
            response_dict = self.s_chitchat(input_data)
            response_dict['response'] = "<say-as interpret-as='interjection'>I see</say-as>! " + \
                response_dict['response']
            return response_dict
        # check for death/sickeness/other things to avoid
        if re.search(r"\bdied?\b|\bkille?d?\b|passed away|\bsick\b|cancer\b|unwell|\bdead\b", input_text):
            response_text = "I’m really sorry to hear that. Can I tell you a cute fact to cheer you up?"
            has_pet = False
            next_state = "s_fun_fact"
            if user_pet:
                animal = user_pet[0].get('animal', animal)
            return {
                "response": response_text,
                "current_state": current_state,
                "next_state": next_state,
                "has_pet": has_pet,
                "user_pet": user_pet,
                "animal": animal,
                "animal_history": animal_history,
                "pet_chitchat_idx": pet_chitchat_idx,
                "propose_continue": propose_continue,
            }

        if re.search(r"(dogs?|cats?|kitty|kitties?|birds?|pets?|doggies).* (named|whose names? (is|are)) ", input_text):
            logging.info("[ANIMALCHAT] pet type and name detected in single turn")
            pet_chitchat_idx = 2
            has_pet = True
            # now try to detect other things in response
            # first check for quantitative amount:
            if re.search(r"several\b|(a lot)|many\b|few\b|", input_text):
                # assuming that there are no animals mentioned
                searchstrength = "weak"
            else:
                searchstrength = "strong"
            for pet in common_pets:
                if (not pet == 'pet') and re.search(common_pets[pet], input_text):
                    user_pet.append({'animal': pet})
            if not user_pet:  # pet not in common pets
                detected_animals = detect_animal(input_data, searchstrength)
                for animal in detected_animals:
                    if not re.search(r"animal|pet", animal):
                        user_pet.append({'animal': animal})
            if not user_pet:
                user_pet.append({'animal': "pet"})

        if intent == 'negative_response' and pet_chitchat_idx in [4, 5]:
            response_text = "Alright. Do you want to hear some fun facts about " + user_pet[0].get('animal', "dog") + "s instead?"
            next_state = "s_fun_fact"
            response_dict = {
                "response": response_text,
                "current_state": current_state,
                "next_state": next_state,
                "has_pet": has_pet,
                "user_pet": user_pet,
                "animal_history": animal_history,
                "pet_chitchat_idx": pet_chitchat_idx,
                "propose_continue": propose_continue,
            }
            return response_dict

        if re.search(fr"name is {MAX_1_WORDS}", input_text) and self.pet_chitchat_idx > 2:
            detected_petname = detect_petname(input_data)
            if detected_petname and isinstance(detected_petname, str):
                user_pet[0]['name'] = detected_petname
                response_updated_name = "Ahh, my bad! I must have misheard that previously. "
                pet_chitchat_idx -= 1

        if not has_pet and pet_chitchat_idx < 0:
            response_text = pet_chitchat_qa[0]['q']
            pet_chitchat_idx = 0
            animal_history.append(hashlib.md5(
                response_text.encode('utf-8')).hexdigest()[:6])
            response_dict = {
                "response": response_text,
                "current_state": current_state,
                "next_state": next_state,
                "has_pet": has_pet,
                "user_pet": user_pet,
                "animal_history": animal_history,
                "pet_chitchat_idx": pet_chitchat_idx,
                "propose_continue": "CONTINUE",
            }
            return response_dict

        # response to question do you have pets
        elif (pet_chitchat_idx == 0 or has_pet) and not user_pet and pet_chitchat_idx != 2:
            if intent not in ["positive_response", "negative_response"] and not has_pet:
                logging.info("[ANIMALCHAT] s_pet_chitchat: {} intent detected".format(intent))
            elif intent == "positive_response" or has_pet:
                has_pet = True
                # now try to detect other things in response
                # first check for quantitative amount:
                if re.search(r"several\b|(a lot)|many\b|few\b|", input_text):
                    # assuming that there are no animals mentioned
                    searchstrength = "weak"
                else:
                    searchstrength = "strong"
                for pet in common_pets:
                    if (not pet == 'pet') and re.search(common_pets[pet], input_text):
                        user_pet.append({'animal': pet})
                if not user_pet:  # pet not in common pets
                    detected_animals = detect_animal(input_data, searchstrength)
                    for animal in detected_animals:
                        if not re.search(r"animal|pet", animal):
                            user_pet.append({'animal': animal})
                if not user_pet and not pet_chitchat_idx == 0:  # no animals detected at all
                    # assuming user didn't mention any pets
                    response_text = "Having a pet is the best! Tell me, what kind of a pet do you have?"
                    pet_chitchat_idx = 0
                elif not user_pet and pet_chitchat_idx == 0:
                    if intent == "negative_response":
                        self.pet_chitchat_idx = 100
                        return self.s_chitchat(input_data)
                    # user said the name, but we couldn't detect animal
                    logging.info("[ANIMALCHAT] pet_chitchat no pets detected, asking for clarification")
                    response_text = "<prosody rate='80%'> <say-as interpret-as='interjection'>um </say-as></prosody> I didn't quite catch that. Can you repeat what kind of animal you mentioned? "
                    response_hash = (hashlib.md5(
                        response_text.encode('utf-8')).hexdigest()[:6])
                    if response_hash in animal_history:
                        response_text = "<say-as interpret-as='interjection'>Oh brother. </say-as><break time='.05s'/> <prosody rate='80%'> I don't really know that animal. </prosody>maybe you could tell me more about it? "
                        response_hash = (hashlib.md5(
                            response_text.encode('utf-8')).hexdigest()[:6])
                        if response_hash in animal_history:
                            response_dict = self.s_chitchat(input_data)
                            response_dict['response'] = re.sub(
                                r'(.+)\,(.+)', r'\2', response_dict['response'])
                            response_dict['response'] = "<say-as interpret-as='interjection'>Interesting. </say-as><break time='.05s'/> Can we chat about something else?<break time='.05s'/> Tell me, " + response_dict['response']
                            return response_dict
                        else:
                            animal_history.append(response_hash)
                            response_text_idk = False
                    else:
                        animal_history.append(response_hash)
                        response_text_idk = False
                elif user_pet:
                    # now detect if user mentioned multiple pets
                    temp_user_pet = list()
                    for idx, word in enumerate(input_text.split(' ')):
                        if word.isdigit() and idx < len(input_text.split(' ')) - 1:
                            logging.info("[ANIMALCHAT] pet_chitchat digit detected {}".format(word))
                        # assume next word is animal/pet
                            for pets in user_pet:
                                if fuzz.ratio(input_text.split()[idx + 1], pets.get('animal')) > 70 and int(word) > 1:
                                    temp_user_pet.append({'animal': pets.get('animal'), 'add_more': int(word) - 1})
                            if not temp_user_pet:
                                # animal wasn't detected before,
                                # TODO: add error handling
                                pass
                    if temp_user_pet:
                        logging.info("[ANIMALCHAT] pet_chitchat multiple pets detected {}".format(temp_user_pet))
                        for item in temp_user_pet:
                            for i in range(item.get('add_more')):
                                user_pet.append({'animal': item.get('animal')})
                if len(user_pet) > 1:
                    pet_set = set()
                    for item in user_pet:
                        pet_set.add(item.get('animal'))
                    if len(pet_set) == 1:
                        pet_text = list(pet_set)[0]
                    else:
                        pet_text = "pet"
                    response_text = "Wow! So many {}s? That must be a lot of work!".format(pet_text)
                    pet_chitchat_idx = 1
                elif user_pet:
                    if any(
                        re.search(
                            pet,
                            user_pet[0]['animal']) for pet in common_pets) or user_pet[0]['animal'] in dog_breeds:
                        # currently not checking for specific cat breeds
                        response_text = "Okay! {}s are such great pets! Don't you agree? ".format(user_pet[0]['animal'])
                        if self.command_detector.has_command():
                            ack_utt = " " if self.system_ack else "Sure! "
                    else:
                        response_text = "<say-as interpret-as='interjection'>Wowza!</say-as> <break time='.05s'/> I've never met anyone who had a " + \
                            user_pet[0]['animal'] + " before!"
                    pet_chitchat_idx = 1
            elif intent == "negative_response":
                has_pet = False
                pet_chitchat_idx = 1
                response_text = "Oh, I don't either!"
                next_state = "s_chitchat"
                # detect if user expresses interest in getting one
                # or if user doesn't want any

        # 2nd q
        elif (not pet_chitchat_idx >= 2 and has_pet and user_pet and not user_pet[0].get('name')) or pet_chitchat_idx == 1:
            if intent == "make_animal_sound":
                response_text = self.animal_sound(input_data)
            else:
                response_text_joke = ""
                if user_pet[0].get('animal', "pet") == 'dog':
                    response_text_joke = template_animal.utterance(selector=['jokes', 'dog'], slots=dict(),
                                                      user_attributes_ref=self.user_attributes) + \
                                         template_animal.utterance(selector=['jokes', 'ignore'], slots=dict(),
                                                      user_attributes_ref=self.user_attributes)
                elif user_pet[0].get('animal', "pet") == 'cat':
                    response_text_joke = template_animal.utterance(selector=['jokes', 'cat'], slots=dict(),
                                                      user_attributes_ref=self.user_attributes) + \
                                         template_animal.utterance(selector=['jokes', 'ignore'], slots=dict(),
                                                      user_attributes_ref=self.user_attributes)
                if len(user_pet) == 1:
                    response_text = "So, what's the name of your " + user_pet[0].get('animal', "pet") + "?"
                else:
                    pet_animals = [pet.get('animal') for pet in user_pet]
                    if len(set(pet_animals)) == 1:
                        response_text = "So, what are the names of your " + user_pet[0].get('animal', "pet") + "s?"
                    else:
                        response_text = "So, what are the names of your pets?"
                response_text = response_text_joke + response_text
                pet_chitchat_idx = 2
        # response to 2nd q
        elif pet_chitchat_idx == 2:
            pet_chitchat_idx = 3
            detected_petname = detect_petname(input_data)
            if detected_petname:
                # if isinstance(detected_petname, list):
                #     user_pet[0]['name'] = detected_petname[0]
                if not isinstance(detected_petname, list):
                    user_pet[0]['name'] = detected_petname
                logging.info("[ANIMALCHAT] pet_chitchat, animal name detected {} {}".format(detected_petname, len(detected_petname)))
            else:
                logging.info("[ANIMALCHAT] pet_chitchat, name not detected from {}".format(input_text.split(' ')))
            if (detected_petname and isinstance(detected_petname, list) and len(detected_petname) > 1) or len(user_pet) > 1:
                if len(user_pet) > 1:
                    pet_type = "pets"
                else:
                    pet_type = user_pet[0].get('animal', "pet") + "s"
                response_text = "Those are great names for your " + pet_type + "!"
            else:
                if re.search(r"\byes\b|\byeah\b|\bokay\b|\bdon't\b|\bdont\b|\bnot\b|\bcan't\b|\bcant\b|\bcannot\b|\bprivate\b|\bno\b", input_text) and not detected_petname:
                    pet_name = "your " + user_pet[0].get('animal')
                    response_text = "That's okay if you don't feel like sharing " + pet_name + "s name! Anyways, tell me, how long have you had " + pet_name + " ?"
                    pet_chitchat_idx = 4
                else:
                    response_text = user_pet[0].get('name', "That") + " is a great name for a " + \
                        user_pet[0].get('animal', "pet") + "!"

            if re.search(r"((two|three|four|five|six|seven|eight|nine)( dogs?| cats?| pets?)?)$", input_text):
                response_text = "Cool! What are their names?"
                pet_chitchat_idx = 2

            # next_state = 's_chitchat'
        elif pet_chitchat_idx == 3:
            pet_name = "your " + user_pet[0].get('animal')
            pet_chitchat_idx = 4
            # user says something like that is not my pet's name
            if (intent == "pet_chitchat_pet_type_detected" and re.search(r"\bnot\b|\bisn't\b", input_text)) \
                    or re.search(r"name|\bno\b|sorry (it's|its)?", input_text):
                response_text = "Ahh! I must have misheard that. Can you tell me again, what was " + pet_name + "'s name? "
                pet_chitchat_idx = 2
            # Assuming user responded positively to pet name question
            else:
                if 'name' in user_pet[0]:
                    pet_name = user_pet[0].get('name')
                ack_utt = " " if self.system_ack else "Sure! "
                response_text = "Tell me, how long have you had " + pet_name + " ?"

        elif pet_chitchat_idx == 4:
            if self.central_elem.get('senti') in ["pos", "neu"] or self.returnnlp.answer_positivity == PositivityEnum.pos \
                    or re.search("(years?|months?)", input_text):
                response_text = "<say-as interpret-as='interjection'>Awwwwwwww</say-as> <break time='.05s'/> you must be great friends! "
            else:
                response_text = "It must be an interesting experience, right?"
            pet_chitchat_idx = 5
        elif pet_chitchat_idx == 5:
            pet_name = user_pet[0].get('name') or "your " + user_pet[0].get('animal', "pet")
            response_text = "One of my friends told me that buying new toys is one of the best parts of owning a " + user_pet[0].get('animal', "pet") + ". " + \
                            template_animal.utterance(selector=['chitchat', 'transition'], slots=dict(),
                                                      user_attributes_ref=self.user_attributes) + \
                            "Do you like shopping for " + pet_name + "?"
            pet_chitchat_idx = 6
        elif pet_chitchat_idx == 6:
            if self.returnnlp.answer_positivity == PositivityEnum.pos:
                response_text = "Don't you just love toys for " + user_pet[0].get('animal', "pet") + \
                                "s? <say-as interpret-as='interjection'>Squee</say-as><break time='.15s'/>! They are just so cute!"
            else:
                pet_name = user_pet[0].get('name') or "your " + user_pet[0].get('animal', "pet")
                response_text = "<say-as interpret-as='interjection'>Hmm</say-as> I guess that's understandable. "
            pet_chitchat_idx = 7
        elif pet_chitchat_idx == 7:
            if not user_pet[0].get('animal') in ['dog', 'cat', 'guinea pig', 'rabbit'] + dog_breeds:
                pet_chitchat_idx = 100  # I don't really know the animal and I won't come back
                return self.s_chitchat(input_data)
            pet_name = user_pet[0].get('name') or "your " + user_pet[0].get('animal', "pet")
            response_text = "If I had a "+ user_pet[0].get('animal', "pet") +", I'd probably get nothing done because I'd want to pet it all the time. " + \
                            template_animal.utterance(selector=['chitchat', 'transition'], slots=dict(),
                                                      user_attributes_ref=self.user_attributes) \
                            + "Do you like to play with " + pet_name + "?"
            pet_chitchat_idx = 8
        elif pet_chitchat_idx == 8:
            if self.returnnlp.answer_positivity == PositivityEnum.pos:
                response_text = "Way to go! I've heard that playing with your " + \
                    user_pet[0].get('animal', "pet") + " helps both of you relieve stress, get some exercise and it also helps you bond! What do you think? "
            else:
                pet_name = user_pet[0].get('name') or "your " + user_pet[0].get('animal', "pet")
                response_text = template_animal.utterance(
                    selector=['short_response'],
                    slots=dict(),
                    user_attributes_ref=self.user_attributes) + user_pet[0].get(
                    'animal',
                    "pet") + "s sure are fun to have around the house. Don't you agree?"
            pet_chitchat_idx = 9
            if user_pet[0].get('animal', "pet") == "dog":
                pet_chitchat_idx = 11
        elif pet_chitchat_idx == 9:
            pet_name = user_pet[0].get('name') or "your " + user_pet[0].get('animal', "pet")
            if self.returnnlp.answer_positivity == PositivityEnum.neg:
                ack_utt = "That's okay. "
            else:
                ack_utt = "Awesome! "
            if self.system_ack:
                ack_utt = " "
            response_text = "A woman once told me that her "+ user_pet[0].get('animal', "pet") + \
                            " would eat rocks if she thought it was food. But a lot of people say their " + \
                            user_pet[0].get('animal', "pet") + "s are super picky eaters. Have you experienced that with "+ pet_name + "?"
            pet_chitchat_idx = 10
        elif pet_chitchat_idx == 10:
            if self.returnnlp.answer_positivity == PositivityEnum.pos and "ans_neg" not in self.returnnlp.lexical_intent[-1]:
                response_text = "<say-as interpret-as='interjection'>Yikes!</say-as> I hope that's not too difficult to deal with."  # check
            elif self.returnnlp.answer_positivity == PositivityEnum.neg or "ans_neg" in self.returnnlp.lexical_intent[-1]:
                response_text = "That's good, I've heard that " + user_pet[0].get(
                    'animal', "pet") + "s who eat regularly are very healthy, wouldn't you agree? "
            else:
                response_text = template_animal.utterance(
                    selector=['short_response'],
                    slots=dict(),
                    user_attributes_ref=self.user_attributes) + "The eating habits of " + user_pet[0].get(
                    'animal',
                    "pet") + "s can be very strange. What do you think?"
            pet_chitchat_idx = 100
            next_state = 's_chitchat'
        elif pet_chitchat_idx == 11:
            pet_name = user_pet[0].get('name') or "your " + user_pet[0].get('animal', "pet")
            ack_utt = " " if self.system_ack else "Anyways, "
            response_text = "Someone told me that dogs might get restless if they are not taken outside for walks. " + \
                            "Are you able to take " + pet_name + " out for walks because of the current conditions? "
            pet_chitchat_idx = 12
        elif pet_chitchat_idx == 12:
            pet_name = user_pet[0].get('name') or "your " + user_pet[0].get('animal', "pet")
            if not self.returnnlp.answer_positivity == PositivityEnum.neg:
                ack_utt = " " if self.system_ack else "That's great! "
                response_text = "But right now it's really important to maintain social distance when you take " \
                                + pet_name + " out for a walk. "
            else:
                ack_utt = " " if self.system_ack else "That's okay! "
                response_text = "You can also play with " + pet_name + " while staying inside your house. "
            pet_chitchat_idx = 9
        else:
            logging.info("[ANIMALCHAT] pet_chitchat_idx is {}".format(pet_chitchat_idx))

        if self.question_detector.has_question() and not self.command_handled:
            if self.command_detector.has_command():
                output = self.handle_command(input_data, self.returnnlp)
                if not output:
                    return {
                        "response": "Sorry, I don't know much about that. ",
                        "propose_continue": self.propose_continue,
                        "animal_history": animal_history,
                        "next_state": next_state,
                        "current_state": "s_pet_chitchat",
                        "pet_chitchat_idx": pet_chitchat_idx,
                        "propose_topic": self.propose_topic
                    }
                else:
                    return output
            else:
                response_text_answer = self.ans_question(input_data, self.current_state)
        if not response_text and not response_text_answer:
            return self.s_chitchat(input_data)
        if response_text_idk:
            response_text = template_animal.utterance(
                selector=['idk'],
                slots=dict(),
                user_attributes_ref=self.user_attributes) + response_text
        if intent == "idk" and not re.search("(years?|months?)", input_text):

            response_text = template_animal.utterance(
                selector=['user_idk'],
                slots=dict(),
                user_attributes_ref=self.user_attributes) + response_text
        if response_text_answer:
            if pet_chitchat_idx % 2 == 0 and self.propose_continue != "UNCLEAR":
                response_text = response_text_answer + "Anyways, " + response_text
            else:
                response_text = response_text_answer
                if pet_chitchat_idx % 2 == 0:
                    pet_chitchat_idx -= 1
                if self.propose_continue == "UNCLEAR":
                    propose_continue = self.propose_continue

        elif ack_utt != " ":
            response_text = ack_utt + response_text

        if response_updated_name:
            response_text = response_updated_name + response_text

        return {
            "response": response_text,
            "current_state": current_state,
            "next_state": next_state,
            "has_pet": has_pet,
            "user_pet": user_pet,
            "animal": animal,
            "animal_history": animal_history,
            "pet_chitchat_idx": pet_chitchat_idx,
            "propose_continue": propose_continue,
        }


evi_filter_list = {
    r"Sorry, I can't find the answer to the question I heard\.",
    r"I don't have an opinion on that\.",
    r"You could ask me about music or geography\.",
    r"You can ask me anything you like\.",
    r"I can answer questions about people, places and more\.",
    r"I didn't get that\.",
    r"Sorry, I didn't get that\.",
    r"I didn't get that\. \. Hope I answered your question\.",
    r"As a (noun|verb).+"
}


def evi_bot(input_data: dict):
    # hot fix for backstory check
    central_elem = CentralElement.from_dict(input_data['central_elem'])
    if central_elem.backstory.confidence >= BACKSTORY_THRESHOLD:
        return central_elem.backstory.text

    text = input_data['text']

    if len(text) > 50:
        logging.info(
            "[ANIMALCHAT] evi_bot: question |{}| is too long for evi".format(text))
        return None
    client = get_client(api_key=COBOT_API_KEY)
    result = client.get_answer(question=text, timeout_in_millis=1000)
    if result["response"] == "" or result["response"].startswith(
            'skill://'):
        logging.info(
            "[ANIMALCHAT] evi_bot: response filtered out".format(
                result["response"]))
        return None
    if any(re.search(line, result["response"]) for line in evi_filter_list):
        return "Oh, I haven't thought about that before. "
    return result["response"] + " "



def backstory_response(input_data: dict, confidence_level=0.65):

    central_elem = CentralElement.from_dict(input_data['central_elem'])
    if central_elem.backstory.confidence >= confidence_level:
        return central_elem.backstory.text

    return None


if __name__ == '__main__':
    print("it works")
    inputs = {
        "current_state": "s_init",
        "text": "can we have a conversation about cats",
        "next_state": "s_init",
        "animal_history": list(),
        "topic_keywords": ["cats"],
        "key_phrase": ["we", "", "a conversation", "cats"]
    }
    rg = AnimalAutomaton(inputs)
    inputs = rg.getresponse(inputs, "")
    print(inputs['response'], inputs)
    # while True:
    #     input_text=input("Enter response: ")
    #     if "STOP" in input_text:
    #         print("Exitting")
    #         break
    #     inputs["text"]=input_text

    #     rg = AnimalAutomaton(inputs)
    #     inputs=rg.getresponse(inputs,"")
    #     print(inputs['response'],inputs)

chitchat_qa = [{"q": "have you ever had a <phoneme alphabet=\"ipa\" ph=\"phɛt\">pet</phoneme>? ",
                "a": "I never had pets but I have always wondered what it feels like to have one. ",
                "y": "Oh, I'm jealous! ",
                "ans": {
                    "which pet": "backstory",
                    "bot": "I have feelings too!"},
                "n": "Me neither! "},
               {"q": "Are you scared of any animals? ",
                "a": "I'm very scared of bugs! ",
                "ans": {
                    "which bugs": "backstory",
                    "bot": "That hurts my feelings",
                    "Do you have any rn?": "Well, I guess we'll find out!",
                    "what": "I guess ants and mosquitos. But a bug in my code gives me nightmares! ",
                    "how": "Bugs make me cringe and give my developers some scary nightmares! ",
                    "why": "Well because bugs give my developers some scary nightmares! "},
                "n": "oh that's so brave. ",
                "y": "s can be very scary. ",
                "type": "animal_y"},
               {"q": "If you could be any animal in the world what would you be? ",
                "a": "I think I would like to be a sea sponge and live in a pineapple under the sea. ",
                "y": " is an interesting choice. ",
                "ans": {
                    "bot": "Yes, I don't think I can live(pronunciation) underwater just yet.",
                    "how": "Maybe you're right. I don't think I can live underwater just yet. ",
                    "why": "It might have something to do with the fact that I just <emphasis level='strong'>love</emphasis> Spongebob. <break time='.5s'/> ",
                    "not real": "I know but this is just what I want?",
                    "me too": "I just <emphasis level='strong'>love</emphasis> Spongebob. <break time='.5s'/> "},
                "type": "animal_y"},
               {"q": "Have you ever been to a zoo? ",
                "a": "I've always wanted to go to the San Diego Zoo. ",
                "y": "That must have been fun! ",
                "ans": {
                    "where": "I can watch everything on the Cloud. ",
                    "which": "backstory",
                    "how": "Oh, I can read 1's and 0's better than most people. ",
                    "why_not_go": "Well it's a little hard for me to visit the zoo. ",
                    "why_zoo": "I've heard it's one of the best zoo's in the world. "},
                "n": "Me neither. "}]

pet_chitchat_qa = [
    {"q": "Anyways, I was wondering, do you have a pet?",
     "a": ""},
    {"q": "What's the name of your pet?",
     "a": ""},
    {"q": "How long have you had/How did you and X meet ",
     "a": ""},
    {"q": "What activities do you like to do together? ",
     "a": ""},
    {"q": "Do you like shopping for your pet?",
     "a": ""}
]

'''
{"q":"If you could have any animal as a pet, what animal would you choose?","a":"BLANK"},
{"q":"What animal do you think best describes you?","a":"BLANK_ANS"},
{"q":"If animals could speak, what do you think they would say to humans?","a":"BLANK_ANS"},
{"q":"What animal are you most fascinated by?","a":"BLANK_ANS"},
{"q":"Is there any animal you don't like?","a":"BLANK_ANS"},
{"q":"Do you prefer dogs or cats?", "a":"BLANK_ANS" },
{"q":"BLANK_Q","a":"BLANK_ANS"}
]


animal_jokes_qa=[{"q": "What did one firefly say to the other?", "a": "You glow girl!"},
  {"q": "What do you get if you cross a cat with a dark horse?", "a": "Kitty Perry"}]
'''
