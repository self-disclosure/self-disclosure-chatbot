import nlu.util_nlu
import utils
import logging
from cobot_core.service_module import LocalServiceModule
from response_generator.travel import travel_automaton

logger = logging.getLogger(__name__)


class Travel_Response_Generator_Local(LocalServiceModule):

    def execute(self):
        travelchat = self.state_manager.user_attributes.travelchat
        if travelchat is None:
            travelchat = {}

        logger.info('[TravelRG] exec with travelchat: {}'.format(travelchat))
        if "currentstate" not in travelchat:
            currenttravelstate = "s_init"
        else:
            currenttravelstate = travelchat["currentstate"]
        if "prevstate" not in travelchat:
            prevtravelstate = "s_init"
        else:
            prevtravelstate = travelchat["prevstate"]
        travel_context = {}
        if "context" in travelchat:
            travel_context = travelchat["context"]
        if "place" not in travelchat:
            place = {}
        else:
            place = travelchat["place"]
        if "place1" not in travelchat:
            place1 = {}
        else:
            place1 = travelchat["place1"]
        if "place2" not in travelchat:
            place2 = {}
        else:
            place2 = travelchat["place2"]
        if "places_used" not in travelchat:
            places_used = []
        else:
            places_used = travelchat["places_used"]

        senti = nlu.util_nlu.get_feature_by_key(
            self.state_manager.current_state, 'sentiment')
        lexical = nlu.util_nlu.get_feature_by_key(
            self.state_manager.current_state, 'intent_classify')['lexical']
        ner = nlu.util_nlu.get_feature_by_key(self.state_manager.current_state, 'ner')
        noun_phrase = nlu.util_nlu.get_feature_by_key(
            self.state_manager.current_state, 'noun_phrase')
        knowledge = nlu.util_nlu.get_feature_by_key(
            self.state_manager.current_state, 'knowledge')
        dialog_act = nlu.util_nlu.get_feature_by_key(
            self.state_manager.current_state, 'dialog_act')

        inputs = {
            "currentstate": currenttravelstate,
            "prevstate": prevtravelstate,
            "text": self.input_data["text"].lower(),
            "nlu": {
                "sentiment": senti,
                "lexical": lexical,
                "ner": ner
            },
            "context": travel_context,
            "noun_phrase": noun_phrase,
            "knowledge": knowledge,
            "dialog_act": dialog_act,
            "place": place,
            "place1": place1,
            "place2": place2,
            "places_used": places_used,
            "a_b_test": self.input_data['a_b_test'],
            "returnnlp": self.input_data['returnnlp'],
            "central_elem": self.input_data['central_elem'],
            "system_acknowledgement": self.input_data['system_acknowledgement']
        }

        logger.info(
            '[TravelRG] get response with input: {}, sentiment: {}, lexical: {}, ner {}'.format(inputs, senti, lexical,
                                                                                                ner))
        RG = travel_automaton.TravelAutomaton(
            inputs, user_attributes=self.state_manager.user_attributes)
        outputtext = RG.getresponse(
            inputs, nlu.util_nlu.get_sentiment_key(senti, lexical))

        response_text = outputtext["response"]
        # remove redundant data
        outputtext.pop('nlu', None)
        outputtext.pop('text', None)
        outputtext.pop('response', None)

        self.state_manager.user_attributes.travelchat = outputtext

        if "propose_topic" in outputtext and outputtext["propose_topic"]:
            setattr(self.state_manager.user_attributes,
                    "propose_topic", outputtext["propose_topic"])

        logger.info("[TravelRG] The final response is %s", response_text)
        return response_text