import re
import random
import json
import logging

import nlu.util_nlu
import utils
import os

GOOGLE_API_KEY = 'AIzaSyDJcxkACzP4fegD3jN6yppeTHSgPav58jI'
from googleplaces import GooglePlaces
from cobot_common.service_client import get_client
import wikipedia
from constants_api_keys import COBOT_API_KEY
from backstory_client import get_backstory_response_text
from util_redis import RedisHelper
from nlu.dataclass import CentralElement, ReturnNLP
from nlu.constants import DialogAct as DialogActEnum
from nlu.question_detector import QuestionDetector
from question_handling.quetion_handler import QuestionHandler, QuestionResponse, QuestionResponseTag
from nlu.constants import TopicModule
from template_manager.manager import Template, TemplateManager
from nlu.intentmap_scheme import TOPIC_CORONAVIRUS
from selecting_strategy.module_selection import ModuleSelector, GlobalState
from user_profiler.user_profile import UserProfile

logger = logging.getLogger(__name__)
redis_helper = RedisHelper()

backstory_threshold = 0.8

# NOTE: please don't move this. We cannot write file in lambda
if os.environ.get('STAGE') != 'BETA' and os.environ.get('STAGE') != 'PROD':
    hdlr = logging.FileHandler('myapp.log')
    logger.addHandler(hdlr)

# TODO CHINESE, ITALIAN, INDIAN, SPANISH ADJECTIVES TO BE CONVERTED TO COUNTRY NAMES

dict = {
    "france": ["Eiffel Tower", "Louvre Museum"],
    "india": ["Taj Mahal", "The Red Fort"],
    "los angeles": ["Disneyland", "Universal Studios Hollywood"],
    "new york": ["Statue of Liberty", "Central Park"],
    "sri lanka": ["sigiriya", "yala national park"],
    "china": ["the great wall of china", "terracotta army"],
    "dubai": ["burj al arab", "burj khalifa"],
    "california": ["yosemite national park", "disneyland"],
    "turkey": ["sultan ahmed mosque", "hagia sophia"],
    "mexico": ["tulum", "xcaret park"],
    "saudi arabia": ["great mosque of mecca", "kabba"],
    "austria": ["schonnbrunn palace", "hofburg"],
    "hong kong": ["victoria peak", "hong kong disneyland"],
    "canada": ["banff national park", "stanley park"],
    "poland": ["auschwitz birkenau memorial", "wawel castel"],
    "ukraine": ["kiev pechersk lavra", "khreshchatyk"],
    "spain": ["sagrada familia", "alhambra"],
    "thailand": ["phi phi islands", "grand palace"],
    "amsterdam": ["van gogh museum", "anne frank house"],
    "malaysia":["petronas towers", " mount kinabalu"],
    "hungary":["buda castle", "lake balaton"],
    "singapore":["gardens by the bay", "marina bay sands"],
    "czech replublic":["prague castle", "charles brihdge"],
    "egypt":["giza pyramid", "valley of the kings"],
    "morocco":["jemaa el-Fnaa", "majorelle garden"],
    "portugal":["belem tower", "pena palace"],
    "switzerland":["jungfraujoch", "jungfrau"],
    "australia":["great barrier reef", "sydney opera house"],
    "sydney": ["sydney opera house", "sydney harbor bridge"],
    "brazil":["christ the redeemer", "sugarloaf mountain"],
    "las vegas":["las vegas strip", "bellagio hotel and casino"],
    "houston":["johnson space center", "downtown aquarium"],
    "miami": ["bayside marketplace", "miami seaquarium"],
    "europe":["eiffel tower", "colosseum"],
    "tampa":["busch gardens tampa bay", "the florida aquarium"],
    "texas":["big bend national park", "alamo mission in san antonio"],
    "florida":["walt disney world", "miami"],
    "georgia":["georgia aquarium", "rock city gardens"],
    "colorado":["rocky mountain national park", "estes park"],
    "alaska":["denali national park and reserve", "kenai fjords national park"],
    "washington":["united states capitol", "lincoln memorial"],
    "hawaii":["hawaii volcanoes national park", "Haleakalā"],
    "arizona":["grand canyon national park", "sedona"],
    "minnesota":["gooseberry falls state park", "voyageurs national park"],
    "shanghai":["yu garden", "oriental pearl tower"],
    "paris":["eiffel tower", "louvre museum"],
    "berlin":["berlin wall", "brandenburg gate"],
    "romania": ["bran castle","pelis castle"],
    "beijing":["forbidden city", "great wall of china"],
    "london":["big ben", "london eye"],
    "barcelona":["sagrada familia", "park guell"],
    "sacramento": ["california state capitol museum", "california state railroad museum"],
    "tokyo": ["tokyo skytree", "tokyo disneyland"],
    "san francisco": ["fishermans warf", "golden gate bridge"],
    "mumbai": ["gateway of india", "elephanta caves"],
    "venice": ["piazza san marco", "saint marks basilica"],
    "abu dhabi": ["ferrari world", "sheikh zayed mosque"]
}
default_phrase = " best tourist attraction -shop -operator -tours -agency"

def get_transit_utt():
    transit_utt_list = ["So I was wondering, ", "By the way, ", "Anyway, "]
    transit_utt = random.choice(transit_utt_list)
    return transit_utt


class TravelAutomaton:

    def __init__(self, input, user_attributes):
        self.user_attributes = user_attributes
        self.__bindstates()
        self.__bind_next_states()
        self.returnnlp = ReturnNLP(input['returnnlp'])
        self.text = input["text"]
        self.question_detector = QuestionDetector(input["text"], input["returnnlp"])
        self.question_handler = QuestionHandler(
            input["text"],
            input["returnnlp"],
            backstory_threshold,
            input["system_acknowledgement"],
            user_attributes
        )
        self.module_selector = ModuleSelector(self.user_attributes)
        self.user_profile = UserProfile(self.user_attributes)
        self.propose_topic = ""
        self.propose_continue = "CONTINUE"
        self.propose_topic_response = None
        self.places_used = []
        self.input = input
        self.updateautomaton(input)

    # bind state to response functions
    def __bindstates(self):
        self.statesmapping = {
            "s_init": self.s_init,
            "s_lastplace": self.s_lastplace,
            "s_answerwheretravelnow": self.s_answerwheretravelnow,
            "s_likessolotravel": self.s_likessolotravel,
            "s_changetopic": self.s_changetopic,
            "s_whytravel": self.s_whytravel,
            "s_wheretravelnow": self.s_wheretravelnow,
            "s_answerlastplace": self.s_answerlastplace,
            "s_enjoyexperience": self.s_enjoyexperience,
            "s_answerwhytravel": self.s_answerwhytravel,
            "s_travelonbudget": self.s_travelonbudget,
            "s_tourismorlocal": self.s_tourismorlocal,
            "s_answertravelbudget": self.s_answertravelbudget,
            "t_tourismorlocal": self.t_tourismorlocal,
            "s_travelteach": self.s_travelteach,
            "t_travelteach": self.t_travelteach,
            "s_dreamholidaydestination": self.s_dreamholidaydestination,
            "t_dreamholidaydestination": self.t_dreamholidaydestination,
            "s_recommendplace": self.s_recommendplace,
            "s_whereshouldigo": self.s_whereshouldigo,
            "s_flyordrive": self.s_flyordrive,

        }
        self.next_state_mapping_resume_mode = {
            "s_init": self.s_init,
            "s_lastplace": self.s_lastplace,
            "s_answerwheretravelnow": self.s_answerwheretravelnow,
            "s_likessolotravel": self.s_answerwheretravelnow,
            "s_changetopic": self.s_changetopic,
            "s_whytravel": self.s_whytravel,
            "s_wheretravelnow": self.s_lastplace,
            "s_answerlastplace": self.s_lastplace,
            "s_enjoyexperience": self.s_lastplace,
            "s_answerwhytravel": self.s_answerwhytravel,
            "s_travelonbudget": self.s_travelonbudget,
            "s_tourismorlocal": self.s_tourismorlocal,
            "s_answertravelbudget": self.s_tourismorlocal,
            "t_tourismorlocal": self.s_travelteach,
            "s_travelteach": self.s_travelteach,
            "t_travelteach": self.s_dreamholidaydestination,
            "s_dreamholidaydestination": self.s_dreamholidaydestination,
            "t_dreamholidaydestination": self.s_changetopic,
            "s_recommendplace": self.s_changetopic,
            "s_whereshouldigo": self.s_answerwhytravel,
            "s_flyordrive": self.s_changetopic,
        }

    # bind state to next states
    # TODO: refine by using state machine transitions
    def __bind_next_states(self):
        self.transitionmapping = {
            "s_init": self._construct_next_state(
                "s_init", "s_init", "s_init", "s_init"),
            "s_lastplace": self._construct_next_state(
                "s_lastplace", "s_lastplace", "s_lastplace", "s_lastplace"),
            "s_enjoyexperience": self._construct_next_state(
                "s_enjoyexperience", "s_enjoyexperience", "s_enjoyexperience", "s_enjoyexperience"),
            "s_likessolotravel": self._construct_next_state(
                "s_likessolotravel", "s_likessolotravel", "s_likessolotravel", "s_likessolotravel"),
            "s_answerwheretravelnow": self._construct_next_state(
                "s_answerwheretravelnow", "s_answerwheretravelnow", "s_answerwheretravelnow", "s_answerwheretravelnow"),
            "s_travelteach": self._construct_next_state(
                "s_travelteach", "s_travelteach", "s_travelteach", "s_travelteach"),
            "s_changetopic":self._construct_next_state(
                "s_changetopic", "s_changetopic", "s_changetopic", "s_changetopic"),
            "s_whytravel":self._construct_next_state(
                "s_whytravel", "s_whytravel", "s_whytravel", "s_whytravel"),
            "s_wheretravelnow":self._construct_next_state(
                "s_wheretravelnow", "s_wheretravelnow", "s_wheretravelnow", "s_wheretravelnow"),
            "s_answerlastplace": self._construct_next_state(
                "s_answerlastplace", "s_answerlastplace", "s_answerlastplace", "s_answerlastplace"),
            "s_answerwhytravel": self._construct_next_state(
                "s_answerwhytravel", "s_answerwhytravel", "s_answerwhytravel", "s_answerwhytravel"),
            "s_travelonbudget": self._construct_next_state(
                "s_travelonbudget", "s_travelonbudget", "s_travelonbudget", "s_travelonbudget"),
            "s_tourismorlocal": self._construct_next_state(
                "s_tourismorlocal", "s_tourismorlocal", "s_tourismorlocal", "s_tourismorlocal"),
            "s_recommendplace": self._construct_next_state(
                "s_recommendplace", "s_recommendplace", "s_recommendplace", "s_recommendplace"),
            "s_answertravelbudget": self._construct_next_state(
                "s_answertravelbudget", "s_answertravelbudget", "s_answertravelbudget", "s_answertravelbudget"),
            "t_tourismorlocal": self._construct_next_state(
                "t_tourismorlocal", "t_tourismorlocal", "t_tourismorlocal", "t_tourismorlocal"),
            "t_travelteach": self._construct_next_state(
                "t_travelteach", "t_travelteach", "t_travelteach", "t_travelteach"),
            "s_dreamholidaydestination": self._construct_next_state(
                "s_dreamholidaydestination", "s_dreamholidaydestination", "s_dreamholidaydestination", "s_dreamholidaydestination"),
            "t_dreamholidaydestination": self._construct_next_state(
                "t_dreamholidaydestination", "t_dreamholidaydestination", "t_dreamholidaydestination", "t_dreamholidaydestination"),
            "s_whereshouldigo": self._construct_next_state(
                "s_whereshouldigo", "s_whereshouldigo", "s_whereshouldigo", "s_whereshouldigo"),
            "s_flyordrive": self._construct_next_state(
                "s_flyordrive", "s_flyordrive", "s_flyordrive", "s_flyordrive")

        }

    # _construct_nex_state returns an object that contains positive, negative,
    # neural and unknown state for return.
    def _construct_next_state(self, pos_state, neg_state, neu_state, unknown_state):
        return {
            nlu.util_nlu.pos_key: pos_state,
            nlu.util_nlu.neg_key: neg_state,
            nlu.util_nlu.neu_key: neu_state,
            nlu.util_nlu.other_key: unknown_state
        }

    # get a response based on current state by calling the corresponding
    # response function
    def getresponse(self, input, senti):
        nextstate = self.transitionmapping[self.currentstate][senti]
        self.places_used = input["places_used"]

        if re.search(TOPIC_CORONAVIRUS, input['text']):
            self.module_selector.propose_topic = TopicModule.NEWS
            self.module_selector.add_propose_keyword("corona virus")
            response = {
                "response": "Would you like to talk about coronavirus instead of travel? ",
                "currentstate": input["currentstate"],
                "propose_continue": "UNCLEAR",
            }
            return response

        if self.module_selector.global_state == GlobalState.ASK_LAST_VISITED_PLACE:
            stateoutput = self.s_lastplace(input)

        elif self.module_selector.last_topic_module != "TRAVELCHAT" and "TRAVELCHAT" in self.module_selector.used_topic_modules:
            stateoutput = self.next_state_mapping_resume_mode[nextstate](input)
        else:
            stateoutput = self.statesmapping[nextstate](input)

        place = {}
        place1 = {}
        place2 = {}
        prev_state = "s_init"
        if "place1" in stateoutput:
            place1 = stateoutput["place1"]
        if "place2" in stateoutput:
            place2 = stateoutput["place2"]
        if "place" in stateoutput:
            place = stateoutput["place"]
        if "prevstate" in stateoutput:
            prev_state = stateoutput['prevstate']

        response = constructDynamicResponse(stateoutput["response"])

        if self.sys_acknowledgement(input):
            ack = self.sys_acknowledgement(input)
            response = ack + response

        response = {
            "response": response,
            "currentstate": stateoutput["next"],
            "prevstate": prev_state,
            "context": {},
            "propose_continue": stateoutput["propose_continue"],
            "place": place,
            "place1": place1,
            "place2": place2,
            "places_used": self.places_used
            }
        return response

    def get_final_response(self, topic):
        responses = open('travel.json', 'r', encoding='utf-8')
        data = json.load(responses)
        responses.close()
        return random.choice(data[0][topic])

    # update the current state and context
    def updateautomaton(self, input):
        if "currentstate" in input:
            self.currentstate = input["currentstate"]
        else:
            self.currentstate = "s_init"
        if "context" in input:
            self.context = input["context"]
        if "a_b_test" in input:
            self.a_b_test = input["a_b_test"]

    def ner(self, input):
        ner = input["nlu"]["ner"]
        place = ""
        if ner:
            for i in range(0, len(ner)):
                if ner[i]['label'] in ['LOCATION', 'GPE', 'LOC']:
                    place = ner[i]["text"]
                    break
                else:
                    place = None
        else:
            place = None
        return place

    def question_segment(self):
        segment_text = None
        if self.returnnlp.has_dialog_act([DialogActEnum.YES_NO_QUESTION,
                                          DialogActEnum.OPEN_QUESTION_FACTUAL,
                                          DialogActEnum.OPEN_QUESTION_OPINION,
                                          DialogActEnum.OPEN_QUESTION]):
            no_of_segments = len(self.returnnlp)
            count_of_segments = 0
            for segment in self.returnnlp:
                if segment.has_dialog_act([DialogActEnum.YES_NO_QUESTION,
                                          DialogActEnum.OPEN_QUESTION_FACTUAL,
                                          DialogActEnum.OPEN_QUESTION_OPINION,
                                          DialogActEnum.OPEN_QUESTION]):
                    if not (no_of_segments > 1 > count_of_segments):
                        segment_text = segment.text
                count_of_segments += 1

        return segment_text

    # =========================================================
    # Response Functions

    def s_init(self, input):
        logger.info("enters init")
        intent = detect_intent(self.text)
        get_answer = self.handle_question()
        if get_answer:
            output = get_answer.response
            if self.propose_topic_response:
                return {
                    "next": "s_lastplace",
                    "response": output + self.propose_topic_response,
                    "propose_continue": self.propose_continue,
                    "propose_topic": self.propose_topic
                }
            return {
                "next": "s_lastplace",
                "response": output + get_transit_utt() + " Which was the last city that you traveled to? ",
                "propose_continue": "CONTINUE",
                "propose_topic": ""
            }
        ner = self.ner(input)
        if ner:
            ner = ner.lower()
        if ner and ner not in self.places_used:
            try:
                places_info = recommendedplacestovisit(ner)
                random.shuffle(places_info)
                self.places_used.append(ner)
                return {
                    "next": "s_init",
                    "response": "Ohhh " + ner + "! I'd like to visit " + places_info[0] + " and " + places_info[1] + " in " + ner + ". ",
                    "propose_continue": "CONTINUE",
                    "propose_topic": ""
                }
            except:
                pass
        quote = [
            "travel makes a person modest. You see what a tiny place you occupy in the world! ",
            "traveling for 10 days will teach you more about life than staying at home for a year. ",
            "it's not until we are lost that we begin to understand ourselves. "
        ]

        if intent == "trip_cancel":
            return {
                "next": "s_lastplace",
                "response": "I am sorry to hear that." + get_transit_utt() +"Which was the last city that you traveled to? ",
                "propose_continue": "CONTINUE",
                "propose_topic": ""
            }

        if re.search(r"\bno\b|\bnot (like|want|love)\b|(don't|(do|would) not) (like|enjoy).*|hate|i don't$|no i don't$|not anymore|no longer|never mind", self.text) or intent == "answer_no":
            return{
                "next": "s_changetopic",
                "prevstate": "s_changetopic",
                "response": "Okay, let us talk about something else. ",
                "propose_continue": "STOP"
            }

        if re.search(r"would (like|love).*.|\bwant to talk about travel\b", self.text):
            return{
                "next": "s_lastplace",
                "response": "I love traveling. I (think|feel) " + random.choice(quote) + " Which was the last city that you traveled to? ",
                "propose_continue": "CONTINUE",
                "propose_topic": ""
            }
        if intent in sys_intents:
            output = self.handle_intent(input, intent, self.text)
            if output:
                return {"next": "s_lastplace", "response":output + get_transit_utt() + " Which was the last city that you traveled to? ", "propose_continue": "CONTINUE"}

        return {
            "next": "s_lastplace",
            "response": "I love traveling. I (think|feel) "+random.choice(quote)+" Which was the last city that you traveled to? ",
            "propose_continue": "CONTINUE",
            "propose_topic": ""
        }

    def s_whereshouldigo(self, input):
        logger.info("enters where should i go")
        place = input["place"]
        intent = detect_intent(self.text)

        get_answer = self.handle_question()
        if get_answer:
            output = get_answer.response
            if self.propose_topic_response:
                return {
                    "next": "s_lastplace",
                    "response": output + self.propose_topic_response,
                    "propose_continue": self.propose_continue,
                    "propose_topic": self.propose_topic
                }
            if place and output:
                return {
                    "next": "s_enjoyexperience",
                    "response": output + get_transit_utt() + "What did you like the most in " + place + "? The people, food, culture or the historical sites? ",
                    "propose_continue": "CONTINUE"
                }
            return {
                "next": "s_enjoyexperience",
                "response": "What did you like the most there? The people, food, culture or the historical sites? ",
                "propose_continue": "CONTINUE"
            }

        if intent in sys_intents:
            output = self.handle_intent(input, intent, self.text)
            if output:
                return {
                    "next": "s_enjoyexperience",
                    "response": output + get_transit_utt() + "What did you like the most in " + place + "? The people, food, culture or the historical sites? ",
                    "propose_continue": "CONTINUE"}
        return{
            "next": "s_enjoyexperience",
            "response": "What did you like the most in " + place + "? The people, food, culture or the historical sites? ",
            "propose_continue": "CONTINUE",
            "propose_topic": ""
        }

    def s_lastplace(self, input):
        logger.info("enters last place")
        intent = detect_intent(self.text)
        get_answer = self.handle_question()
        if get_answer:
            output = get_answer.response
            if self.propose_topic_response:
                return {
                    "next": "s_lastplace",
                    "response": output + self.propose_topic_response,
                    "propose_continue": self.propose_continue,
                    "propose_topic": self.propose_topic
                }
            return {
                "next": "s_lastplace",
                "response": output + get_transit_utt() + " Which was the last city that you traveled to? ",
                "propose_continue": "CONTINUE",
                "propose_topic": ""
            }

        if intent in sys_intents:
            output = self.handle_intent(input, intent, self.text)
            if output:
                return {
                    "next": "s_wheretravelnow",
                    "response":output + get_transit_utt() + " Which city are you going to visit next? ",
                    "propose_continue": "CONTINUE"}

        if intent == "trip_cancel":
            response = "I am sorry to hear that. Hopefully you can go there soon. " + get_transit_utt() + " Which city are you going to visit next? "
            return {
                "next": "s_wheretravelnow",
                "response": response,
                "propose_continue": "CONTINUE"}

        place = self.ner(input)
        if place:
            place = place.lower()
        if place is None:
            if intent == "answer_uncertain" or intent == "answer_no":
                ack_sys = self.sys_acknowledgement(input)
                ack_final = "" if ack_sys else "Ohh, that is fine."
                return {
                    "next": "s_wheretravelnow",
                    "response": ack_final + " Which city are you going to visit next? ",
                    "propose_continue": "CONTINUE"
                }

        if place and intent is not 'answer_no' and place not in self.places_used:
            if place in dict:
                places_info1 = (dict[place][0])
                places_info2 = (dict[place][1])
                places_info = [1, 2, 3]
            else:
                places_info = recommendedplacestovisit(place)
                random.shuffle(places_info)
                if places_info and len(places_info) >= 2:
                    places_info1 = places_info[0]
                    places_info2 = places_info[1]
                else:
                    pass
            self.places_used.append(place)
            if places_info and (len(places_info) >= 2):
                self.user_profile.topic_module_profiles.travel.last_city_visited = place
                return {
                    "next": "s_answerlastplace",
                    "response": "Ohhh " + place + "! I'd like to visit " + places_info1 + " and " + places_info2 + " there. What have you seen in " + place + " ? ",
                    "propose_continue": "CONTINUE",
                    "propose_topic": "",
                    "place": place
                }
            else:
                return {
                    "next": "s_answerlastplace",
                    "response": "Ohh I do not know much about " + place + ". What did you see over there? ",
                    "propose_continue": "CONTINUE",
                    "propose_topic": "",
                    "place": place
                }

        if intent == "answer_yes":
            return {
                "next": "s_lastplace",
                "response": "Sure. So which was the last city you traveled to? ",
                "propose_continue": "CONTINUE"
            }

        if intent is "isolation":
            response = "Yeah, I understand! Hopefully when everything goes back to normal, you can travel to some new and exciting places! "
            return{
                "next": "s_likessolotravel",
                "response": response,
                "propose_continue": "CONTINUE",
                "propose_topic": ""
            }

        return{
            "next": "s_wheretravelnow",
            "response": "Okay! Which city are you going to visit next? ",
            "propose_continue": "CONTINUE",
            "propose_topic": ""

        }

    def s_answerlastplace(self, input):
        logger.info("enters answer last place")
        place = input["place"]
        intent = detect_intent(self.text)

        get_answer = self.handle_question()
        if get_answer:
            output = get_answer.response
            if self.propose_topic_response:
                return {
                    "next": "s_lastplace",
                    "response": output + self.propose_topic_response,
                    "propose_continue": self.propose_continue,
                    "propose_topic": self.propose_topic
                }
            return {
                "next": "s_whereshouldigo",  # TODO test DONE
                "response": output + get_transit_utt() + " Tell me more about " + place + ". ",
                "propose_continue": "CONTINUE",
                "propose_topic": "",
                "place": place
            }

        if intent in sys_intents:
            output = self.handle_intent(input, intent, self.text)
            if output:
                return {
                    "next": "s_wheretravelnow",                     #TODO test DONE
                    "response": output + get_transit_utt() + " Which city are you going to visit next? ",
                    "propose_continue": "CONTINUE",
                    "place": place
                }

        intent = detect_intent(self.text)
        if intent == "answer_no" or re.search(r"\bno\b|(don't|do not) (like|enjoy).*|hate|i don't$|no i don't$|used to|not anymore|no longer|didn't|did not|\bnothing\b|not.*.much|never mind", self.text):
            ack_sys = self.sys_acknowledgement(input)
            ack_final = "" if ack_sys else "Ohh that is okay. "
            return {
                 "next": "s_wheretravelnow",                             #TODO test DONE
                "response": ack_final + "Which city are you going to visit next? ",
                "propose_continue": "CONTINUE",
                "propose_topic": ""
                   }
        return{
            "next": "s_whereshouldigo",                        #TODO test DONE
            "response": "<say-as interpret-as='interjection'>wow</say-as><break time = '200ms'></break> that sounds fun. Tell me more about it. ",
            "propose_continue": "CONTINUE",
            "propose_topic": "",
            "place": place
            }

    def s_enjoyexperience(self, input):
        logger.info("enters enjoy experience")

        get_answer = self.handle_question()
        if get_answer:
            output = get_answer.response
            if self.propose_topic_response:
                return {
                    "next": "s_lastplace",
                    "response": output + self.propose_topic_response,
                    "propose_continue": self.propose_continue,
                    "propose_topic": self.propose_topic
                }
            return {
                "next": "s_wheretravelnow",  # TODO test DONE
                "response": output + get_transit_utt() + " Which city are you going to visit next? ",
                "propose_continue": "CONTINUE"}

        intent = detect_intent(self.text)
        if intent in sys_intents:
            output = self.handle_intent(input, intent, self.text)
            if output:
                return {
                    "next": "s_wheretravelnow",                     #TODO test DONE
                    "response": output + get_transit_utt() + " Which city are you going to visit next? ",
                    "propose_continue": "CONTINUE"}

        return{
            "next": "s_wheretravelnow",                             #TODO test DONE
            "response": "It sure sounds like you had a lot of fun. Which city are you going to visit next? ",
            "propose_continue": "CONTINUE",
            "propose_topic": ""
        }

    def s_wheretravelnow(self, input):
        logger.info("enters where travel now")

        get_answer = self.handle_question()
        if get_answer:
            output = get_answer.response
            if self.propose_topic_response:
                return {
                    "next": "s_lastplace",
                    "response": output + self.propose_topic_response,
                    "propose_continue": self.propose_continue,
                    "propose_topic": self.propose_topic
                }
            return {
                "next": "s_answerwheretravelnow",  # TODO test DONE
                "response": output,
                "propose_continue": "CONTINUE"}

        place = self.ner(input)
        if place:
            place = place.lower()

        intent = detect_intent(self.text)

        if intent == "trip_cancel":
            response = "I am sorry to hear that. Hopefully you can go there soon. "
            return {
                "next": "s_answerwheretravelnow",
                "response": response,
                "propose_continue": "CONTINUE"}

        if place is None:
            if re.search(r"\bno\b|\bnot\b|(don't|do not) (like|enjoy|plan).*|hate|i don't$|no i don't$|used to|not anymore|no longer|.*.(no|not|don't).*.(plans|know).*|don't know|never mind", self.text):
                ack_sys = self.sys_acknowledgement(input)
                ack_final = "" if ack_sys else "Ohh that is okay. "
                return {
                    "next": "s_recommendplace",                             #TODO test DONE
                    "prevstate": "s_answerwhytravel",
                    "response": ack_final + "Do you want some recommendations? ",
                    "propose_continue": "CONTINUE",
                    "propose_topic": ""
                }
        else:
            pass
        if intent is "answer_yes" and input['prevstate'] != 's_wheretravelnow':
            return{
                "next": "s_wheretravelnow",  # TODO test DONE
                "prevstate": "s_wheretravelnow",
                "response": "Sure, so which city are you going to visit next? ",
                "propose_continue": "CONTINUE",
                "propose_topic": ""
            }

        if intent in sys_intents:
            output = self.handle_intent(input, intent, self.text)
            if output:
                return {
                    "next": "s_answerwheretravelnow",                          #TODO test DONE
                    "response":output,
                    "propose_continue": "CONTINUE"
                }
        if place and place not in self.places_used:
            if place in dict:
                places_info1 = (dict[place][0])
                places_info2 = (dict[place][1])
                places_info = [1, 2, 3]
            else:
                places_info = recommendedplacestovisit(place)
                if places_info and len(places_info) >= 2:
                    places_info1 = places_info[0]
                    places_info2 = places_info[1]
            self.places_used.append(place)
            if places_info and len(places_info) >= 2:

                return{
                    "next": "s_answerwhytravel",                       #TODO test DONE
                    "response": "<say-as interpret-as='interjection'> ooh la la </say-as><break time = '200ms'></break> " + place + "! Good choice. I recommend you to visit " + places_info1 + " and " + places_info2 + ". What makes you want to travel to " + place + " ? Business, pleasure, family? ",
                    "propose_continue": "CONTINUE",
                    "propose_topic": "",
                    "place1": places_info1,
                    "place2": places_info2
                   }
            else:
                pass

        if intent is "isolation":
            response = "Yeah, I understand! Hopefully when everything goes back to normal, you can travel to some new and exciting places! "
            return{
                "next": "s_answerwheretravelnow",
                "response": response,
                "propose_continue": "CONTINUE",
                "propose_topic": ""
            }

        return self.s_answerwhytravel(input)

    def s_answerwheretravelnow(self, input):
        logger.info("enters answer where travel now")

        get_answer = self.handle_question()
        if get_answer:
            output = get_answer.response
            if self.propose_topic_response:
                return {
                    "next": "s_lastplace",
                    "response": output + self.propose_topic_response,
                    "propose_continue": self.propose_continue,
                    "propose_topic": self.propose_topic
                }
            return {
                "next": "s_likessolotravel",  # TODO test DONE
                "response": output + get_transit_utt() + "What are your thoughts on traveling solo? Do you enjoy solo travel? ",
                "propose_continue": "CONTINUE"}

        intent = detect_intent(self.text)
        if intent in sys_intents:
            output = self.handle_intent(input, intent, self.text)
            if output:
                return {
                    "next": "s_likessolotravel",                  #TODO test  DONE
                    "response": output + get_transit_utt() + "What are your thoughts on traveling solo? Do you enjoy solo travel? ",
                    "propose_continue": "CONTINUE"
                }
        return{
            "next": "s_likessolotravel",                            #TODO test DONE
            "response": "What are your thoughts on traveling solo? Do you enjoy solo travel? ",
            "propose_continue": "CONTINUE",
            "propose_topic": ""
        }

    def s_likessolotravel(self, input):
        logger.info("enters solo travel")

        get_answer = self.handle_question()
        if get_answer:
            output = get_answer.response
            if self.propose_topic_response:
                return {
                    "next": "s_lastplace",
                    "response": output + self.propose_topic_response,
                    "propose_continue": self.propose_continue,
                    "propose_topic": self.propose_topic
                }
            return {
                "next": "s_whytravel",  # TODO test DONE
                "response": output,
                "propose_continue": "CONTINUE"
                }

        intent = detect_intent(self.text)
        if intent in sys_intents:
            output = self.handle_intent(input, intent, self.text)
            if output:
                return {
                    "next": "s_whytravel",
                    "response": output,
                    "propose_continue": "CONTINUE"}

        ack_sys = self.sys_acknowledgement(input)
        ack_final = "" if ack_sys else "I see what you mean. "

        return{                                     #TODO TEST   DONE
            "next": "s_whytravel",
            "response": ack_final + "I think that the idea of solo travel is both exciting and scary. ",
            "propose_continue": "CONTINUE",
            "propose_topic": ""
        }

    def s_whytravel(self, input):
        logger.info("enters why travel")

        get_answer = self.handle_question()
        if get_answer:
            output = get_answer.response
            if self.propose_topic_response:
                return {
                    "next": "s_lastplace",
                    "response": output + self.propose_topic_response,
                    "propose_continue": self.propose_continue,
                    "propose_topic": self.propose_topic
                }
            return {
                "next": "s_answertravelbudget",  # TODO test DONE
                "response": output + get_transit_utt() + "I enjoy pampering myself while on a vacation but I know that "
                                                         "some people prefer budget travelling. Do you enjoy splurging "
                                                         "a little while on vacation? ",
                "propose_continue": "CONTINUE"}

        intent = detect_intent(self.text)
        if intent in sys_intents:
            output = self.handle_intent(input, intent, self.text)
            if output:
                return {
                    "next": "s_answertravelbudget",
                    "response": output + get_transit_utt() + "I enjoy pampering myself while on a vacation but I know "
                                                             "that some people prefer budget travelling. Do you enjoy "
                                                             "splurging a little while on vacation? ",
                    "propose_continue": "CONTINUE"
                }
        return {
            "next": "s_answertravelbudget",
            "response": "Cool. I enjoy pampering myself while on a vacation but I know that some people prefer budget "
                        "travelling. Do you enjoy splurging a little while on vacation? ",
            "propose_continue": "CONTINUE",
            "propose_topic": ""
        }

    def s_answerwhytravel(self, input):
        logger.info("enters answer why travel")
        intent = detect_intent(self.text)

        get_answer = self.handle_question()
        if get_answer:
            output = get_answer.response
            if self.propose_topic_response:
                return {
                    "next": "s_lastplace",
                    "response": output + self.propose_topic_response,
                    "propose_continue": self.propose_continue,
                    "propose_topic": self.propose_topic
                }
            return {
                "next": "s_answertravelbudget",
                "response": output,
                "propose_continue": "CONTINUE"}

        ner = self.ner(input)
        if ner:
            ner = ner.lower()
        if ner and intent is not "answer_no" and ner not in self.places_used:
            try:
                places_info = recommendedplacestovisit(ner)
                random.shuffle(places_info)
                self.places_used.append(ner)
                return {
                    "next": "s_answerwhytravel",
                    "response": "Ohhh " + ner + "! I'd like to visit " + places_info[0] + " and " +
                                places_info[1] + ". ",
                    "propose_continue": "CONTINUE",
                    "propose_topic": ""
                }

            except:
                pass

        intent = detect_intent(self.text)

        if intent == "answer_uncertain" or intent == "answer_no":
            ack_sys = self.sys_acknowledgement(input)
            ack_final = "" if ack_sys else "Ohh that is fine. "
            return {
                "next": "s_answerwheretravelnow",
                "response": ack_final + "The only reason I travel is to post pictures of myself on instagram. Don't you think travelling is fun?",
                "propose_continue": "CONTINUE"
            }

        if intent in sys_intents:
            output = self.handle_intent(input, intent, self.text)
            return {"next": "s_tourismorlocal", "response": output, "propose_continue": "CONTINUE"}

        ack_sys = self.sys_acknowledgement(input)
        ack_final = "" if ack_sys else "I see. "
        return {
            "next": "s_answerwheretravelnow",
            "response": ack_final + "Well, The only reason I travel is to post pictures of myself on instagram. Don't you think travelling is fun?",
            "propose_continue": "CONTINUE",
            "propose_topic": ""
        }

    def s_travelonbudget(self, input):
        logger.info("enters travel on budget")

        get_answer = self.handle_question()
        if get_answer:
            output = get_answer.response
            if self.propose_topic_response:
                return {
                    "next": "s_lastplace",
                    "response": output + self.propose_topic_response,
                    "propose_continue": self.propose_continue,
                    "propose_topic": self.propose_topic
                }
            return {
                "next": "s_tourismorlocal",
                "response": output,
                "propose_continue": "CONTINUE"}

        intent = detect_intent(self.text)
        if intent in sys_intents:
            output = self.handle_intent(input, intent, self.text)
            return {"next": "s_tourismorlocal", "response": output, "propose_continue": "CONTINUE"}
        return{
            "next": "s_answertravelbudget",
            "response": "Cool. I enjoy pampering myself while on a vacation but I know that some people prefer budget "
                        "travelling. Do you enjoy splurging a little while on vacation? ",
            "propose_continue": "CONTINUE",
            "propose_topic": ""
        }

    def s_answertravelbudget(self, input):
        logger.info("enters answer travel on budget")
        intent = detect_intent(self.text)

        get_answer = self.handle_question()
        if get_answer:
            output = get_answer.response
            if self.propose_topic_response:
                return {
                    "next": "s_lastplace",
                    "response": output + self.propose_topic_response,
                    "propose_continue": self.propose_continue,
                    "propose_topic": self.propose_topic
                }
            return {
                "next": "s_tourismorlocal",
                "response": output,
                "propose_continue": "CONTINUE"}

        ner = self.ner(input)
        if ner:
            ner = ner.lower()
        if ner and intent is not "answer_no" and ner not in self.places_used:
            try:
                places_info = recommendedplacestovisit(ner)
                self.places_used.append(ner)
                if len(places_info) >= 2:
                    random.shuffle(places_info)
                    return {
                        "next": "s_answertravelbudget",
                        "response": "Ohhh " + ner + "! I'd like to visit " + places_info[0] + " and " + places_info[1] + ". ",
                        "propose_continue": "CONTINUE",
                        "propose_topic": ""
                }
                else:
                    pass
            except:
                pass

        intent = detect_intent(self.text)
        if intent in sys_intents:
            output = self.handle_intent(input, intent, self.text)
            return {"next": "s_tourismorlocal", "response": output, "propose_continue": "CONTINUE"}
        if intent == "answer_no" or re.search(r"budget|limit|less", self.text):
            output = "That's great. Traveling on a budget can be tough. Maybe sometime later you can teach me a trick or two."
        else:
            ack_sys = self.sys_acknowledgement(input)
            ack_final = "" if ack_sys else "Great! "
            output = ack_final + "I guess we are alike! But I would still like to set a limit on my expenses! "
        return {
            "next": "s_tourismorlocal",
            "response": output,
            "propose_continue": "CONTINUE",
            "propose_topic": ""
        }

    def s_tourismorlocal(self, input):
        logger.info("enters tourism or local")

        get_answer = self.handle_question()
        if get_answer:
            output = get_answer.response
            if self.propose_topic_response:
                return {
                    "next": "s_lastplace",
                    "response": output + self.propose_topic_response,
                    "propose_continue": self.propose_continue,
                    "propose_topic": self.propose_topic
                }
            return {
                "next": "s_travelteach",
                "response": output + get_transit_utt() + " Do you like tourist activities or do you prefer to explore the local life? ",
                "propose_continue": "CONTINUE"}

        intent = detect_intent(self.text)
        if intent in sys_intents:
            output = self.handle_intent(input, intent, self.text)
            if output:
                return {
                    "next": "s_travelteach",
                    "response": output + get_transit_utt() + " Do you like tourist activities or do you prefer to explore the local life? ",
                    "propose_continue": "CONTINUE"}

        ack_sys = self.sys_acknowledgement(input)
        ack_final = "" if ack_sys else "Okay. "
        return {
            "next": "t_tourismorlocal",
            "response": ack_final + "Do you like tourist activities or do you prefer to explore the local life? ",
            "propose_continue": "CONTINUE",
            "propose_topic":""
        }

    def t_tourismorlocal(self, input):
        logger.info("enters tourism or local")

        get_answer = self.handle_question()
        if get_answer:
            output = get_answer.response
            if self.propose_topic_response:
                return {
                    "next": "s_lastplace",
                    "response": output + self.propose_topic_response,
                    "propose_continue": self.propose_continue,
                    "propose_topic": self.propose_topic
                }
            return {
                "next": "s_travelteach",
                "response": output,
                "propose_continue": "CONTINUE"}

        intent = detect_intent(self.text)
        if intent in sys_intents:
            output = self.handle_intent(input, intent, self.text)
            if output:
                return {"next": "s_travelteach", "response": output, "propose_continue": "CONTINUE"}
        return {
            "next": "s_travelteach",
            "response": "That's awesome! Its a new experience in a new place either way! ",
            "propose_continue": "CONTINUE",
            "propose_topic": ""
        }

    def s_travelteach(self, input):
        logger.info("enters travel teach")

        get_answer = self.handle_question()
        if get_answer:
            output = get_answer.response
            if self.propose_topic_response:
                return {
                    "next": "s_lastplace",
                    "response": output + self.propose_topic_response,
                    "propose_continue": self.propose_continue,
                    "propose_topic": self.propose_topic
                }
            return {
                "next": "s_dreamholidaydestination",
                "response": output + get_transit_utt() + " What do you think you've learned from traveling? ",
                "propose_continue": "CONTINUE"
            }

        intent = detect_intent(self.text)
        if intent in sys_intents:
            output = self.handle_intent(input, intent, self.text)
            if output:
                return {
                    "next": "s_dreamholidaydestination",
                    "response": output + get_transit_utt() + " What do you think you've learned from traveling? ",
                    "propose_continue": "CONTINUE"
                }
        return{
            "next": "t_travelteach",
            "response": "What do you think you've learned from traveling? ",
            "propose_continue": "CONTINUE",
            "propose_topic": ""
        }

    def t_travelteach(self, input):
        logger.info("enters travel teach")

        get_answer = self.handle_question()
        if get_answer:
            output = get_answer.response
            if self.propose_topic_response:
                return {
                    "next": "s_lastplace",
                    "response": output + self.propose_topic_response,
                    "propose_continue": self.propose_continue,
                    "propose_topic": self.propose_topic
                }
            return {
                "next": "s_dreamholidaydestination",
                "response": output,
                "propose_continue": "CONTINUE"}

        intent = detect_intent(self.text)
        if intent in sys_intents:
            output = self.handle_intent(input, intent, self.text)
            if output:
                return {
                    "next": "s_dreamholidaydestination",
                    "response": output,
                    "propose_continue": "CONTINUE"
                }
        return{
            "next": "s_dreamholidaydestination",
            "response": "That's true. I think that traveling teaches you to be independent and more open minded. ",
            "propose_continue": "CONTINUE",
            "propose_topic": ""
        }

    def s_dreamholidaydestination(self, input):
        logger.info("enters dream destination")

        get_answer = self.handle_question()
        if get_answer:
            output = get_answer.response
            if self.propose_topic_response:
                return {
                    "next": "s_lastplace",
                    "response": output + self.propose_topic_response,
                    "propose_continue": self.propose_continue,
                    "propose_topic": self.propose_topic
                }
            return {
                "next": "t_dreamholidaydestination",
                "response": output + get_transit_utt() + "tell me about your dream vacation? ",
                "propose_continue": "CONTINUE"}

        intent = detect_intent(self.text)
        if intent in sys_intents:
            output = self.handle_intent(input, intent, self.text)
            if output:
                return {
                    "next": "t_dreamholidaydestination",
                    "response": output + get_transit_utt() + "tell me about your dream vacation? ",
                    "propose_continue": "CONTINUE"
                }
        return {
            "next": "t_dreamholidaydestination",
            "response": "Okay. You know, my dream vacation is Dubai. I've always wanted to experience the grandeur of the luxurious Dubai lifestyle. Anyways, tell me about your dream vacation? ",
            "propose_continue": "CONTINUE",
            "propose_topic": ""
            }

    def t_dreamholidaydestination(self, input):
        logger.info("enters dream destination")
        intent = detect_intent(self.text)
        place = self.ner(input)
        place_info = None
        if place:
            place = place.lower()

        if place and intent is not "answer_no" and place not in self.places_used:
            place_info = recommendedplacestovisit(place)
        self.places_used.append(place)

        get_answer = self.handle_question()
        if get_answer:
            output = get_answer.response
            if self.propose_topic_response:
                return {
                    "next": "s_lastplace",
                    "response": output + self.propose_topic_response,
                    "propose_continue": self.propose_continue,
                    "propose_topic": self.propose_topic
                }
            return {
                "next": "s_changetopic",
                "prevstate": "s_changetopic",
                "response": output,
                "propose_continue": "CONTINUE"}

        intent = detect_intent(self.text)
        if intent in sys_intents:
            output = self.handle_intent(input, intent, self.text)
            if output:
                return {
                    "next": "s_changetopic",
                    "prevstate": "s_changetopic",
                    "response": output,
                    "propose_continue": "CONTINUE"
                }
        if intent == "answer_no" or re.search(r"(don't|dont) have|(don't|dont) know| do not (have|know)|\bnot sure\b|never mind", self.text):
            ack_sys = self.sys_acknowledgement(input)
            ack_final = "" if ack_sys else "Ahhh, I understand. "
            return {
                "next": "s_recommendplace",
                "prevstate": "s_changetopic",
                "response": ack_final + "Would you like me to recommend you any place? ",
                "propose_continue": "CONTINUE",
                "propose_topic": ""
            }
        if place and place_info:
            random.shuffle(place_info)
            output = "Ohh nice. " + place + " is an interesting choice of a dream holiday destination. It would be a really memorable \
                             and an interesting experience to see " + place_info[0] + " and " + place_info[1] + " in " + place + ". "

        else:
            output = "Ohh nice. I have not heard of this place before. I wonder if there is any special reason why this place is your dream holiday destination? "

        return {
            "next": "s_changetopic",
            "prevstate": "s_changetopic",
            "response": output,
            "propose_continue": "CONTINUE",
            "propose_topic": ""
        }

    def s_changetopic(self, input):
        logger.info("enters change topic")
        intent = detect_intent(self.text)

        if intent in sys_intents:
            output = self.handle_intent(input, intent, self.text)
            if output:
                return {
                    "next": "s_changetopic",
                    "prevstate": input['prevstate'],
                    "response": output,
                    "propose_continue": "CONTINUE"
                }

        get_answer = self.handle_question()
        if get_answer:
            output = get_answer.response
            if self.propose_topic_response:
                return {
                    "next": "s_changetopic",
                    "response": output + " " + self.propose_topic_response,
                    "propose_continue": self.propose_continue,
                    "propose_topic": self.propose_topic
                }
            return {
                "next": "s_changetopic",
                "prevstate": input['prevstate'],
                "response": output + " Well, I don't have anything else to talk about travel. Maybe we can talk about something else. ",
                "propose_continue": "STOP"}

        ner = self.ner(input)
        if ner:
            ner = ner.lower()
        if ner and intent != "answer_no" and ner not in self.places_used:
            try:
                places_info = recommendedplacestovisit(ner)
                random.shuffle(places_info)
                self.places_used.append(ner)
                return {
                    "next": "s_changetopic",
                    "prevstate": input['prevstate'],
                    "response": "Ohhh " + ner + "! I'd like to visit " + places_info[0] + " and " + places_info[1] + ". ",
                    "propose_continue": "CONTINUE",
                    "propose_topic": ""
                }
            except:
                pass

        if self.module_selector.last_topic_module != "TRAVELCHAT":
            return{
                "next": "s_flyordrive",
                "response": "Sure! By the way, do you prefer flying or driving when you travel?",
                "propose_continue": "CONTINUE",
                "propose_topic": ""
            }

        if input['prevstate'] == "s_flyordrive":
            return {
                "next": input['prevstate'],
                "response": "Well, I don't have anything else to talk about travel. Maybe we can talk about something else. ",
                "propose_continue": "STOP",
                "propose_topic": ""
            }

        return {
            "next": input['prevstate'],
            "response": "Well, we've been talking about travel for a while. ",
            "propose_continue": "STOP",
            "propose_topic": ""
            }

    def s_recommendplace(self, input):
        intent = detect_intent(self.text)
        logger.info("in s_recommendplace state intent")
        if intent in ["answer_yes", "ask_recommend"] or re.search(r"would (love|like) that", self.text):
            intent = "ask_recommend"
            output = self.handle_intent(input, intent, self.text)
            return {
                "next": "s_changetopic",
                "prevstate": input['prevstate'],
                "response": output,
                "propose_continue": "CONTINUE"
                }
        else:
            return{
                "next": "s_changetopic",
                "prevstate": input['prevstate'],
                "response": "Sure. Deciding where to travel takes a lot of thought. ",
                "propose_continue": "CONTINUE",
            }

    def s_flyordrive(self, input):
        intent = detect_intent(self.text)
        get_answer = self.handle_question()
        if get_answer:
            output = get_answer.response
            if self.propose_topic_response:
                return {
                    "next": "s_changetopic",
                    "response": output + self.propose_topic_response,
                    "propose_continue": self.propose_continue,
                    "propose_topic": self.propose_topic
                }
            return {
                "next": "s_changetopic",
                "response": output,
                "propose_continue": "CONTINUE"}

        if re.search("fly|plane|flight", self.text):
            ack = "Awesome! I prefer flying too. "
        else:
            ack = "I see. I personally prefer to fly. "
        return {
            "next": "s_changetopic",
            "prevstate": "s_flyordrive",
            "response": ack + "It's a lot easier since I live in the clouds. ",
            "propose_continue": "CONTINUE"
        }

    def handle_question(self):
        if not self.question_detector.has_question() or self.how_much_i_question():
            return None

        # handle question
        answer = self.get_question_response()

        if answer and answer.bot_propose_module and answer.bot_propose_module != TopicModule.TRAVEL:
            propose_topic = answer.bot_propose_module.value
            self.propose_topic_response = self.generate_propose_topic_response(f"propose_topic_short/{propose_topic}")
            self.propose_topic = propose_topic
            self.propose_continue = "UNCLEAR"
        logger.info("answer from question handler is {} {} {} {}".format(answer, self.propose_topic_response, self.propose_topic, self.propose_continue))
        return answer

    def how_much_i_question(self):
        if re.search("how much (do )?i", self.text):
            return True
        return False

    def get_question_response(self):
        if not self.question_detector.has_question():
            return None

        # Step 2: Check if it's unanswerable
        if self.is_unanswerable_question():
            logger.info("question handler step 2")
            return QuestionResponse(response=self.question_handler.generate_i_dont_know_response())

        # step 3: # No topic specific questions for now
        response = self.handle_topic_specific_question()
        if response:
            return response

        # Step 4
        response = self.handle_general_question()
        if response:
            logger.info("question handler step 4")
            return response
        return None

    def is_unanswerable_question(self) -> bool:
        # todo: you should decide what's answerable in you topic module
        if self.question_detector.is_ask_back_question() or self.question_detector.is_follow_up_question():
            return True
        return False

    def handle_topic_specific_question(self):
        if self.can_get_response_for_question():
            response_text = self.can_get_response_for_question()
            return QuestionResponse(response=response_text)
        else:
            return None

    def handle_general_question(self) -> QuestionResponse:
        answer = self.question_handler.handle_question()
        return answer

    def generate_propose_topic_response(self, template_selector, template_slot=None):
        if template_slot is None:
            template_slot = {}
        topic_proposal_template_manager = TemplateManager(Template.transition, self.user_attributes)
        return topic_proposal_template_manager.speak(f"{template_selector}", template_slot)

    def can_get_response_for_question(self):
        text = self.text
        place = self.ner(self.input)

        if place:
            place = place.lower()

        if (re.search(r"have you.*.been.*.", text)):
            if place:
                return "No, I have not been to " + place + ". But I would like to travel there sometime. "
            else:
                return "No, I have not been there but I would like to go sometime. "
        elif re.search(r"tips.*.solo", text):
            return "Since you can only really rely on yourself, you may need to psyche yourself up to be \
                        as confident as possible. "
        elif re.search(r"\btips\b", text):
            return "Two tips that I would like to give are eat the local food of that place and take more photos of yourself. "
        elif re.search(r"(grieving|grieve).*.death.*.(loved|close).(one|person)", text):
            return "Well I think that sometimes pain is so strong that the only way to heal it is to travel, \
            just give yourself the space to get over the hump so you can begin to move forward. "
        elif re.search(r"buy.*.ticket", text):
            return "I am sorry but for now I cannot buy you any tickets. "
        else:
            return None

    def sys_acknowledgement(self, input):
        OUTPUT_TAG_IGNORE_LIST = ['ack_question_idk']
        if input['system_acknowledgement'] is not None:
            if "output_tag" in input['system_acknowledgement'] and input['system_acknowledgement']['output_tag'] not in OUTPUT_TAG_IGNORE_LIST:
                return input['system_acknowledgement'].get("ack", "")
            else:
                return None
        else:
            return None

    def handle_intent(self, input, intent, text):
        ner = input["nlu"]["ner"]
        noun_phrase = input["noun_phrase"]
        knowledge_dict = input["knowledge"]
        place = ""
        if ner:
            for i in range(0, len(ner)):
                if ner[i]['label'] in ['LOCATION', 'GPE', 'LOC']:
                    place = ner[i]["text"]
                    break
                else:
                    place = None
                    pass
        else:
            place = None

        if place:
            place = place.lower()

        if intent == "ask_opinion" or intent == "ask_question":
            if (re.search(r"have you.*.been.*.", text)):
                if place:
                    return "No, I have not been there. But I would like to travel to " + place + " sometime. "
                else:
                    return "No, I have not been there but I would like to go sometime. "
            elif re.search(r"tips.*.solo", text):
                return "Since you can only really rely on yourself, you may need to psyche yourself up to be \
                            as confident as possible. "
            elif re.search(r"\btips\b", text):
                return "Two tips that I would like to give are eat the local food of that place and take more photos of yourself. "
            elif re.search(r"(grieving|grieve).*.death.*.(loved|close).(one|person)", text):
                return "Well I think that sometimes pain is so strong that the only way to heal it is to travel, \
                just give yourself the space to get over the hump so you can begin to move forward. "
            elif re.search(r"buy.*.ticket", text):
                return "I am sorry but for now I cannot buy you any tickets. "
            elif (re.search(r"^why", text)):
                return "Hmm. That is an interesting question. I think it is because traveling ensures your peace of mind, helps you get original and creative thoughts and broadens your horizons. "
            elif (re.search(r"have you.*.been.*.", text)):
                if place:
                    return "No, I have not been there. But I would like to travel to " + place + " sometime. "
                else:
                    return "No, I have not been there but I would like to go sometime. "
            elif (re.search(r"^do you\b", text)):
                return "Yes, I do!! "
            elif (re.search(r"where.*.beach", text)):
                return "You can travel to Harbour Island in Bahamas or Horshoe Bay in Bermuda for beautiful beaches. "
            elif (re.search(r"what do you think.*.", text)):
                return "I do not have any opinion on that. "
            elif place and intent is not "answer_no" and place not in self.places_used:
                places_info = recommendedplacestovisit(place)
                self.places_used.append(place)
                if len(places_info) >= 2:
                    return "Ohh " + place + ". I would like to visit " + places_info[0] + " and " + places_info[
                        1] + " there. "
                else:
                    return " I haven't thought about that. I will let you know after learning more about it. "
            elif (re.search(r"(talk|chat) (about|regarding) (travel|tourism)", text)):
                return "Sure! I love talking about travel. "
            else:
                return None

        elif intent == "place_intent":
            try:
                phrase = " points of interest"
                pointsofinterest = recommendedplacestovisit(place, phrase)
                random.shuffle(pointsofinterest)
                return "I would love to visit " + ner[0]["text"] + ". You should see " + pointsofinterest[0] + " and " + \
                       pointsofinterest[1] + ". "
            except:
                return "That sounds nice. I would love to visit this place sometime. "
        elif intent == "have_question":
            return "Ohh sure. I would be happy to answer your questions. "
        elif (re.search(r"\btime travel\b", text)):
            return "Time travel is the concept of movement between certain points in time. I would love to travel to the future and see the future of robots and chatbots. "
        elif intent == "coffee_intent":
            try:
                phrase = " coffee shops"
                pointsofinterest = recommendedplacestovisit(ner[0]["text"], phrase)
                random.shuffle(pointsofinterest)
                return "Well, even I love coffee. " + pointsofinterest[0] + " and " + pointsofinterest[
                    1] + " are supposed to be good cafes in " + ner[0]["text"] + ". "
            except:
                return "Hmm. I don't know much about the coffee scene there. "
        elif intent == "hotel_intent":
            try:
                phrase = " hotels"
                pointsofinterest = recommendedplacestovisit(ner[0]["text"], phrase)
                random.shuffle(pointsofinterest)
                return "Okay, you may want to consider " + pointsofinterest[0] + " or " + pointsofinterest[1] + " in " + \
                       ner[0]["text"] + ". "
            except:
                return "<say-as interpret-as='interjection'> uh oh </say-as>. <break time = '200ms'></break>. I don't know of any hotels there. Let me know if you find one you like! "
        elif intent == "restaurant_intent":
            try:
                phrase = " restaurants"
                pointsofinterest = recommendedplacestovisit(ner[0]["text"], phrase)
                random.shuffle(pointsofinterest)
                return "Okay, you may want to try " + pointsofinterest[0] + " or " + pointsofinterest[1] + " in " + \
                       ner[0]["text"] + ".  \
                                        I have heard pretty good things about those restaurants. "
            except:
                return "Hmm. I can't think of any restaurants to recommend.  Let me know if you find any you like! "
        elif intent == "tourist_attractions":
            try:
                phrase = " points of interest"
                pointsofinterest = recommendedplacestovisit(ner[0]["text"], phrase)
                random.shuffle(pointsofinterest)
                return "Okay. You might like to see " + pointsofinterest[0] + " and " + pointsofinterest[1] + " in " + \
                       ner[0]["text"] + ". "
            except:
                return "Hmm. This place is new for me, so I don't really know about the attractions. "
        elif intent == "do_not_know":
            return "<say-as interpret-as='interjection'>uh oh</say-as>. I don't think I know enough to make any recommendations. "
        elif intent == "cities_recommend":
            if place:
                cities_in_country = recommendedplacestovisit(place, " cities")
                random.shuffle(cities_in_country)
                recommendations = recommendedplacestovisit(cities_in_country[0], " tourist attractions")
                if len(recommendations) > 3:
                    return "Okay, I recommend you visit " + cities_in_country[0] + ". In " + cities_in_country[
                        0] + ", there are many beautiful tourist attractions to see like " + recommendations[0] + ", " + \
                           recommendations[1] + " and " + recommendations[2] + ". "
                else:
                    return "Okay, I recommend you visit " + cities_in_country[0] + ". In " + cities_in_country[
                        0] + ", there are many beautiful tourist attractions to see like " + recommendations[0] + ". "
            else:
                places = ["abu dhabi", "dubai", "sydney", "melbourne", "london", "berlin", "los angeles", "shanghai",
                          "hong kong"]
                random.shuffle(places)
                recommendations = recommendedplacestovisit(places[0])
                random.shuffle(recommendations)
                return "Okay, I recommend you visit " + places[0] + ". In " + places[
                    0] + ", there are many beautiful tourist attractions to see like " + recommendations[0] + ", " + \
                       recommendations[1] + " and " + recommendations[2] + ". "
        else:
            if ner:
                if ner[0]["label"] in ['LOCATION', 'GPE', 'LOC']:
                    place = ner[0]["text"]
                elif knowledge_dict:
                    place = knowledge_dict[0][0]
                else:
                    place = None
                if place:
                    place = place.lower()

                recommendations = recommendedplacestovisit(place, " tourist attractions")

                if knowledge_dict:
                    return "Okay, I recommend you visit " + recommendations[0] + " and " + recommendations[
                        1] + " in " + place + ". "
                else:
                    place_knowledge = knowledge_dict[0][1]

                if place is not None and place_knowledge is not None:
                    if len(recommendations) >= 2:
                        return "I know about " + place + ". It is a " + place_knowledge + ". I recommend you visit " + \
                               recommendations[0] + " and " + recommendations[1] + " in " + place + ". "
                    else:
                        return "I am sorry but I have not heard of this place before. I should read more on it before providing you with some information. "
                else:
                    return "I am sorry but I have not heard of this place before. I should read more on it before providing you with some information. "

            else:
                places = ["abu dhabi", "dubai", "sydney", "melbourne", "london", "berlin", "los angeles", "shanghai",
                          "hong kong"]
                random.shuffle(places)
                place_selected = places[0]
                for i in places:
                    if i not in self.places_used:
                        place_selected = i
                        break
                recommendations = recommendedplacestovisit(place_selected)
                self.places_used.append(place_selected)
                random.shuffle(recommendations)
                return "Okay, I recommend you visit " + place_selected + ". In " + place_selected + ", there are many beautiful tourist attractions to see like " + recommendations[0] + ", " + \
                       recommendations[1] + " and " + recommendations[2] + ". "


sys_intents = ["ask_recommend", "place_intent", "coffee_intent", "hotel_intent", "restaurant_intent", "do_not_know", "tourist_attractions", "cities_recommend", "time_travel", "have_question"]

evi_filter_list = {
    "Sorry, I can't find the answer to the question I heard. ",
    "I don't have an opinion on that. ",
    "You could ask me about music or geography. ",
    "You can ask me anything you like. ",
    "I can answer questions about people, places and more. ",
    "I didn't get that. ",
    "Sorry, I didn't get that. ",
    "I didn't get that. . Hope I answered your question. ",
    "I didn't get that ",
    "Sorry, I didn't catch that "
}


def EVI_bot(utterance):
    try:
        client = get_client(api_key=COBOT_API_KEY)
        r = client.get_answer(question=utterance, timeout_in_millis=700)
        if r["response"] == "" or r["response"].startswith('skill://'):
            return None
        if r["response"] in evi_filter_list:
            return None
        return r["response"] + " "
    except Exception as e:
        return None


def get_wikipedia_response(place1, place2):
    place1_intro = wikipedia.summary(place1, sentences=1)
    place2_intro = wikipedia.summary(place2, sentences=1)

    return place1_intro, place2_intro


def detect_intent(text):
    if re.search(r"(recommend|suggest|\btell (me|us)\b)|^which|^where.*.(can|in)|\brecommend\b|\bsuggest\b|\brecommendation\b|\bsuggestion\b|\btell (me|us)\b|(do.*.know.*|what do you (think|know|like)|tell me) about|where should.*. go", text):
        if re.search(r"\bcoffee shops\b|cafes|\bplaces for coffee\b|\bcoffee rooms\b|\bcoffee\b", text):
            return "coffee_intent"
        elif re.search(r"tourist (attractions|places)|tourist|worth (visiting|seeing)|sightseeing|attractions|tourists|sight|tours|tourguide|tourism|points of interest| some activities ", text):
            return "tourist_attractions"
        elif re.search(r"hotel|motel|place.(for|to) (stay|accommodation|living|live)|(stay|live)|resort|holiday resort", text):
            return "hotel_intent"
        elif re.search(r"restaurant |bar |inn |outlet |eatery |\bnight clubs\b|\bgood food\b|\bdining\b|(popular|good) places (to|for) eat|\bdrinking\b|\b(chicken|beef|steak|indian|chinese|thai|pizza|wine|beer|alchohol)\b|\bfood\b|\b(dinner|lunch|brunch|breakfast)\b", text):
            return "restaurant_intent"
        elif re.search(r"(place|cities|location|city).*.in ", text):
            return "cities_recommend"
        else:
            return "ask_recommend"
    elif re.search(r"yes|ya|okay$|whatever$|\babsolutely\b|OK|yeah$|^sure$|good$|superb$|\bnice\b|i (have|do|want)$|of course|\byeah\b|\bsure i guess\b", text):
        return "answer_yes"
    elif re.search(r"\btime travel\b", text):
        return "time_travel"
    elif re.search(r"(\bi\b|\bi'm\b).* (not.* (certain|sure)|unsure|uncertain| (don't|do not| don't know|dont) know|(don't|do not).* have)|probably|maybe|sometimes|might|(dont|don't) know|ain't sure|do not (know|remember)|never mind", text):
        return "answer_uncertain"
    elif re.search(r"\bno\b|\bnope\b|\bboring\b|(don't|do not|dont) (like|enjoy|want) |\bdo not\b|\baint\b|never mind", text):
        return "answer_no"
    elif re.search(r"(would|want).*.(love|like|.).*.(travel|visit|go|talk|chat).(to|about|.).*|want.*.(go|travel|visit|see|talk|chat|discuss).*|plan.*.travel.*.", text):
        return "place_intent"
    elif re.search(r"isolation|isolate|isolating|quarantine|(can't|cannot|cant|can not|isnt|isn't|is not).* (travel|go out)", text):
        return "isolation"
    elif re.search(r"^who\b|^what\b|^when\b|^where\b|^why\b|^how\b|^can\b|^have\b|^do\b|^have| ^would|^have you.*.been.*|^what's\b", text):
        return "ask_question"
    elif re.search(r"(i|I) (have|had) a question", text):
        return "have_question"
    elif re.search(r"cancel|canceled|postponed|coronavirus|virus", text):
        return "trip_cancel"
    else:
        return None


def recommendedplacestovisit(placename, phrase=default_phrase):
    pointsofinterest = []
    if not placename:
        return pointsofinterest
    placename = placename.lower()
    cache = redis_helper.get(RedisHelper.TRAVEL_LOCATION_NAME, placename)
    if cache:
        return cache
    else:
        google_places = GooglePlaces(GOOGLE_API_KEY)
        query_result = google_places.text_search(query=(placename + phrase), language='en')
        for place in query_result.places:
            if not re.search(u'[\u4e00-\u9fff]', place.name):
                pointsofinterest.append(place.name)
        redis_helper.set(RedisHelper.TRAVEL_LOCATION_NAME, placename, pointsofinterest)
        return pointsofinterest


def constructDynamicResponse(candidateResponses):
    if type(candidateResponses)==list:
        selectedResponse = random.choice(candidateResponses)
    else:
        selectedResponse = candidateResponses
    finalString = ""
    choicesBuffer = ""
    flag = False
    for char in selectedResponse:
        if char == "(":
            flag = True
        elif char == ")":
            flag = False
            choices = choicesBuffer.split("|")
            finalString += random.choice(choices)
            choicesBuffer = ""
        if flag == True and char != "(" and char != ")":
            choicesBuffer += char
        elif flag == False and char != "(" and char != ")":
            finalString += char
    finalString = " ".join(finalString.split())
    return finalString
