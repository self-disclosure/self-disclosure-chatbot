from cobot_core.service_module import LocalServiceModule

from response_generator.fsm2 import FSMAttributeAdaptor, FSMModule
from template_manager import Template

from response_generator.game.states import GameState


class GameResponseGeneratorLocal(LocalServiceModule):

    def execute(self):
        module = FSMModule(
            FSMAttributeAdaptor('gamechat', self.state_manager.user_attributes),
            self.input_data,
            Template.games,
            GameState,
            first_state='initial',
            state_manager_last_state=self.state_manager.last_state
        )

        return module.generate_response()
