import logging
import random
import re
from dataclasses import field
from typing import Optional

from response_generator.fsm2.utils.detector import EntityItem
from template_manager import Template

from . import canonical_maps
from .igdb import igdb


logger = logging.getLogger(__name__)

game_canonical_group = {
    "fallout": {"fallout_4", "fallout_76"},
    "halo": {"halo_5"},
    "pokemon": {"pokemon_go"},
}


non_games_module_map = {
    "pool": "SPORT",
}


class GameItem(EntityItem):

    @classmethod
    def canonical_map(cls):
        return canonical_maps.game_canonical_map

    @classmethod
    def default_description(cls):
        return 'game'

    @classmethod
    def whiteblacklist(cls):
        return dict(
            description=dict(
                whitelist=r"\b(game)\b",
                blacklist=r"\b(game show|game developer|console)\b",
            ),
            noun=dict(
                blacklist=(
                    r"\b((rpgs?|shooters?|fps)( games?|))|"
                    r"(^store$)|"
                    r"(about games?)|"
                    r"(a |the |this |)games?|"
                    r"(chess|computers?)\b")
            )
        )

    @classmethod
    def from_regex_match(cls, noun: str, canonical: str, positivity=True):
        noun = canonical_maps.game_noun_map.get(canonical) or noun.lower()
        return super().from_regex_match(noun, canonical, positivity)

    def __post_init__(self):
        if not self.canonical:
            for canonical, (noun, regex) in canonical_maps.game_map.items():
                if re.search(regex, self.noun):
                    object.__setattr__(self, 'canonical', canonical)
                    object.__setattr__(self, 'noun', noun)
                    return
            try:
                object.__setattr__(self, 'canonical', noun.lower().replace(" ", "_").strip())
            except Exception as e:
                logger.warning(e, exc_info=True)
                object.__setattr__(self, 'canonical', "")

    def has_hardcode_chitchat(self) -> bool:
        tm = Template.games  # type: Template
        return tm.has_selector(f"game_canned_response/{self.canonical}")

    def igdb_info(self) -> igdb.Game:
        return igdb.IGDB().search_game(self.noun)


class ConsoleItem(EntityItem):

    @classmethod
    def canonical_map(cls):
        return canonical_maps.console_canonical_map

    @classmethod
    def default_description(cls):
        return 'console'

    @classmethod
    def whiteblacklist(cls):
        return dict(
            description=dict(
                whitelist=r"\b(console)\b",
                blacklist=r"\b(game show|game developer)\b",
            ),
        )

    def has_knowledge(self) -> bool:
        tm = Template.games  # type: Template
        return tm.has_selector(f"console/transition/console_game_template/{self.canonical}")


class GenreItem(EntityItem):

    slug: Optional[str] = field(default=None)

    @classmethod
    def canonical_map(cls):
        return canonical_maps.genre_canonical_map

    @classmethod
    def default_description(cls):
        return 'game_genre'

    @classmethod
    def detection_mode(cls):
        return 'local'

    def speakable_noun(self, *, game_suffix=False):
        speakable = canonical_maps.genre_speakable_map.get(self.canonical)
        if speakable:
            if game_suffix:
                speakable = [s for s in speakable if s.endswith('game')] or speakable
            ret = random.choice(speakable)  # type: str
        else:
            ret = self.canonical.replace('_', ' ')  # type: str

        if game_suffix and not ret.endswith('game'):
            return f"{ret} game"
        elif not game_suffix and ret.endswith('game'):
            return ret[:-len('game')].rstrip()
        else:
            return ret

    def __post_init__(self):
        super().__post_init__()
        if not self.slug:
            self.slug = canonical_maps.genre_igdb_slug_map.get(self.canonical)

    @classmethod
    def from_igdb(cls, genre: igdb.Genre):
        return cls(
            noun=genre.name, description='igdb', confidence=10000.0, canonical=None, slug=genre.slug, positivity=True)


class FalseGameItem(EntityItem):

    @classmethod
    def canonical_map(cls):
        return canonical_maps.false_games_canonical_map

    @classmethod
    def default_description(cls):
        return 'false_games'

    @classmethod
    def whiteblacklist(cls):
        return dict(
            description=dict(
                blacklist=r"\b(game show|game developer)\b",
                whitelist=r"\b(game)\b"
            ),
        )

    @classmethod
    def detection_mode(cls):
        return 'local'


class LoLChampionItem(EntityItem):

    @classmethod
    def canonical_map(cls):
        return canonical_maps.league_of_legends_champions

    @classmethod
    def default_description(cls):
        return 'league_of_legends_champion'

    @classmethod
    def detection_mode(cls):
        return 'local'


class LoLRoleItem(EntityItem):

    @classmethod
    def canonical_map(cls):
        return canonical_maps.league_of_legends_roles

    @classmethod
    def default_description(cls):
        return 'league_of_legends_role'

    @classmethod
    def detection_mode(cls):
        return 'local'


class LoLMapItem(EntityItem):

    @classmethod
    def canonical_map(cls):
        return canonical_maps.league_of_legends_maps

    @classmethod
    def default_description(cls):
        return 'league_of_legends_map'

    @classmethod
    def detection_mode(cls):
        return 'local'

    @classmethod
    def from_regex_match(cls, noun: str, canonical: str, positivity=True):
        noun = canonical or noun.lower()
        return super().from_regex_match(noun, canonical, positivity)
