from typing import List, Union


def normalize(sequence: List[Union[int, float]]) -> List[float]:
    sum_ = sum(sequence)
    return [i / sum_ for i in sequence]


def remove_game_suffix(text: str):
    text = text.strip()
    if text.endswith('games'):
        return text[:-len('games')].strip()
    elif text.endswith('game'):
        return text[:-len('game')].strip()
    else:
        return text
