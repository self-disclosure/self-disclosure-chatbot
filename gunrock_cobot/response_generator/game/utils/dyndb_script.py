import logging
from pathlib import Path
import pprint
import yaml

import boto3
from botocore.exceptions import ClientError

logger = logging.getLogger('game.utils')


dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
table_game_funfacts = dynamodb.Table('game_funfacts')


def dict_to_item(raw):
    if type(raw) is dict:
        resp = {}
        for k, v in raw.items():
            if type(v) is str:
                resp[k] = {
                    'S': v
                }
            elif type(v) is int:
                resp[k] = {
                    'I': str(v)
                }
            elif type(v) is dict:
                resp[k] = {
                    'M': dict_to_item(v)
                }
            elif type(v) is list:
                resp[k] = []
                for i in v:
                    resp[k].append(dict_to_item(i))

        return resp
    elif type(raw) is str:
        return {
            'S': raw
        }
    elif type(raw) is int:
        return {
            'I': str(raw)
        }


# data = [
#     dict(
#         canonical='death_stranding',
#         funfacts=[
#             dict(
#                 fact=(
#                     "Did you know the creator Hideo Kojima complained about major game reviewers not understanding what the game is about and giving it a low score? True to Kojima's fashion, the game is pretty avant-garde."
#                 ),
#                 key="game_how_often_play",
#                 question="Did you get a chance to play the game?"
#             )
#         ]
#     )
# ]

" /home/ubuntu/gunrock/gunrock_cobot/response_templates/games_template.yml "

game_yml = Path(__file__).absolute().parent.parent.parent.parent / 'response_templates/games_template.yml'
print(game_yml)

template = yaml.load(open(game_yml, 'r'), Loader=yaml.FullLoader)
# print(template)

db = template['funfact_db']['game']
db = [
    dict(
        canonical=k,
        funfacts=[
            dict(
                fact=vi['utt'],
                key=vi['args']['key'],
                question=vi['args']['question'],
            )
            for vi in v
        ]
    )
    for k, v in db.items()
    if k not in {
        'death_stranding',
        'cod',
    }
]

pprint.pprint(db)


def put_item(data: list):
    for d in data:
        try:
            table_game_funfacts.put_item(Item=d)
        except ClientError as e:
            print(e)


if __name__ == '__main__':
    # put_item(db)
    pass
