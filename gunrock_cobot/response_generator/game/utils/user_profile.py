from typing import List, Union

from response_generator.fsm2 import Tracker, FSMLogger
from user_profiler.user_profile import UserProfile as GlobalUserProfile

from response_generator.game.utils import entity


"""
- discussed_game
    hist: []
"""


logger = FSMLogger('GAME', 'user_profile', 'game.utils')


class UserProfile:

    def __init__(self, tracker: Tracker):
        self.tracker = tracker
        self.global_user_profile = GlobalUserProfile(tracker.user_attributes.ua_ref)

    @property
    def discussed_game_hist(self):
        key = 'discussed_game_hist'
        if key not in self.tracker.persistent_store:
            self.tracker.persistent_store[key] = list()
        return self.tracker.persistent_store[key]

    def discussed_game_hist_append(self, game: Union[entity.GameItem, List[entity.GameItem]]):
        if game in self.discussed_game_hist:
            return
        if isinstance(game, entity.GameItem):
            game = [game]
        elif isinstance(game, list) and all(isinstance(i, entity.GameItem) for i in game):
            pass
        else:
            game = []
        logger.debug(f"FUCK: {game}")
        key = 'discussed_game_hist'
        self.tracker.persistent_store[key] = game
        logger.debug(f"FUCK: {self.discussed_game_hist}")
        self.global_user_profile.topic_module_profiles.game.mentioned_games.extend(g.noun for g in game)
