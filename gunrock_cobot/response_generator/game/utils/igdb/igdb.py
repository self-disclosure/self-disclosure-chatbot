import logging
import requests
from dataclasses import dataclass
from json import JSONDecodeError
from typing import List

from utils import get_working_environment

from .cache import Cache

logger = logging.getLogger('game.igdb')


USER_KEY = "6db9b485909ed271fe928b120d89a663" if get_working_environment() else None


# class IGDBEntity:

#     def field

@dataclass
class Genre:
    id: int
    name: str
    slug: str


@dataclass
class Game:
    id: int
    name: str
    slug: str
    summary: str
    genres: List[Genre]

    def __post_init__(self):
        self.genres = [Genre(**d) for d in self.genres]


class IGDB:

    BASE_URL = "https://api-v3.igdb.com/"

    def __init__(self):
        self.cache = Cache()

    def _get(self, endpoint: str, data: str):
        logger.debug(f"[IGDB] getting: {self.BASE_URL + endpoint}")
        try:
            response = requests.post(
                self.BASE_URL + endpoint,
                data=data,
                headers={'user-key': USER_KEY}
            )
            logger.debug(f"[IGDB] response: {response.status_code}")
            return response.json()

        except (requests.exceptions.HTTPError, JSONDecodeError) as e:
            logger.warning(f"[IGDB] error: {e}", exc_info=True)

    def search_game(self, query: str, limit=1):

        def search(key: str):
            data = (
                f"search \"{key}\"; "
                f"fields name; "
                f"fields name, slug, summary, genres.name, genres.slug;"
                f"where name != null; where slug != null; where summary != null; "
                f"where genres.name != null; where genres.slug != null; "
                f"limit {limit}; "
            )
            response = self._get('games/', data)
            logger.debug(f"[IGDB] search_game response: {response[0]}")
            return response

        def cast(result: dict):
            if isinstance(result, list):
                result = next(iter(result), None)
            try:
                return result, Game(**result)
            except Exception as e:
                logger.warning(f"[IGDB] search_game cast: {e}", exc_info=True)
                return None, None

        # check if there is a matching slug in redis
        slug = self.cache.search_slug(query)
        if slug:
            # slug exists, use cache
            game_raw, game = cast(self.cache.search_game(slug))
            if (game and game_raw):
                # if slug pass cast test
                return game

        # cache fail cast test or slug check
        game_raw, game = cast(search(query))
        if (game and game_raw):
            # fetch pass cast test
            self.cache.write_game(game.slug, game_raw)
            self.cache.write_slug(query, game.slug)
            return game
