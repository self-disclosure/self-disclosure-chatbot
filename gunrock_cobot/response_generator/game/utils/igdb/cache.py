import logging

import util_redis
from utils import get_working_environment


logger = logging.getLogger(__name__)


def verify_cachable(value):
    def traverse(d, key: str = None):
        if isinstance(d, dict):
            for k, v in d.items():
                traverse(v, '/'.join((str(key), k)))
        elif isinstance(d, list):
            for i in d:
                traverse(i, key)
        else:
            yield d, key

    for child_leaf, key in traverse(value):
        if not isinstance(child_leaf, (str, int, float)):
            logger.warning(f"[GAME] [CACHE] value is uncachable: k={key}, v={child_leaf}")
            return False
    return True


class Cache:

    def __init__(self):
        logger.debug(f"[GAME] [CACHE] Redis debug mode: {get_working_environment() == 'local'}")
        self.redis = util_redis.RedisHelper(dev_mode=get_working_environment() == 'local')

    def hit(self, prefix: str, key: str):
        cache = self.redis.get(prefix, key)
        if cache:
            logger.debug(f"[GAME] [CACHE]: cache hit: key='{key}', value={cache}")
            return cache
        else:
            logger.debug(f"[GAME] [CACHE]: cache miss: key='{key}'")

    def write(self, prefix: str, key: str, value):
        if verify_cachable(value):
            self.redis.set(prefix, key, value, 30 * 24 * 60 * 60)
            logger.debug(f"[GAME] [CACHE]: wrote to cache: key={key}, value={value}")

    def search_slug(self, game_noun: str):
        return self.hit(self.redis.PERFIX_GAME_IGDB_GAME_SLUG, game_noun)

    def write_slug(self, game_noun: str, slug: str):
        self.write(self.redis.PERFIX_GAME_IGDB_GAME_SLUG, game_noun, slug)

    def search_game(self, game_slug: str):
        return self.hit(self.redis.PERFIX_GAME_IGDB_GAME, game_slug)

    def write_game(self, game_slug: str, game: dict):
        self.write(self.redis.PERFIX_GAME_IGDB_GAME, game_slug, game)
