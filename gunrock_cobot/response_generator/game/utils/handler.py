import re
from typing import Callable, List, Optional

from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, FSMLogger
from response_generator.fsm2.events import Event, NextState
from template_manager import Template

from nlu.command_detector import CommandDetector
from nlu.question_detector import QuestionDetector
from question_handling.quetion_handler import QuestionHandler, QuestionResponse, QuestionResponseTag

from response_generator.game import states
from response_generator.game.utils import entity, user_profile


logger = FSMLogger('MUSIC', 'CommandQuestionHandler', 'music.utils.command_question_handler')
BACKSTORY_THRESHOLD = 0.85


class CommandQuestionHandler:

    def __init__(self, tracker: Tracker):
        self.command_detector = CommandDetector(
            tracker.input_text, tracker.input_data['returnnlp']
        )
        self.question_detector = QuestionDetector(
            tracker.input_text, tracker.input_data['returnnlp']
        )
        self.question_handler = QuestionHandler(
            tracker.input_text, tracker.input_data['returnnlp'],
            backstory_threshold=BACKSTORY_THRESHOLD,
            system_ack=tracker.input_data['system_acknowledgement'],
            user_attributes_ref=tracker.user_attributes.ua_ref
        )

    def handler_used_blender(self, tracker: Tracker):
        return tracker.volatile_store.get('hander_used_blender') or False

    # MARK: - Default Command Handling Logic

    def request_jumpout_handler(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[List[Event]]:
        return

    def request_tellmore_handler(
        self, dispatcher: Dispatcher, tracker: Tracker, *,
        skip_game_entity=False, skip_console_entity=False,
    ) -> Optional[List[Event]]:
        game = next(iter(entity.GameItem.detect(tracker)), None)
        console = next(iter(entity.ConsoleItem.detect(tracker)), None)

        entities = list(filter(
            lambda e: e.module and e.module not in {TopicModule.GAME},
            tracker.returnnlp.detect_entities()
        ))

        if not skip_game_entity and game:
            states.TalkAboutGameRespond.require_entity(tracker, game)
            return [NextState(states.TalkAboutGameRespond.name, jump=True)]

        elif not skip_console_entity and console:
            states.TalkAboutConsoleRespond.require_entity(tracker, console)
            return [NextState(states.TalkAboutConsoleRespond.name, jump=True)]

        elif entities:
            ent = entities[-1]
            dispatcher.respond_template(f"exit/entity_jump_out", dict(entity=ent.entity_detected))
            dispatcher.propose_continue('STOP', ent.module)
            return [NextState(states.Initial.name)]

        else:
            return

    # MARK: - Default detector functions

    # MARK: - API

    def has_command(
        self, tracker: Tracker, *,
        skip_flag_check=False,
    ):
        flag = (tracker.volatile_store.get('__handler_has_command_handled__') or False)
        return (
            (True if skip_flag_check else flag) or
            self.command_detector.has_command()
        )

    def handle_command(
        self, dispatcher: Dispatcher, tracker: Tracker, *,
        request_jumpout_handler: Callable[[], Optional[List[Event]]] = None,
        request_tellmore_handler: Callable[[], Optional[List[Event]]] = None,
    ):

        foreign_entities = list(filter(
            lambda e: e.module and e.module not in {TopicModule.GAME},
            tracker.returnnlp.detect_entities()
        ))

        cond = [
            self.command_detector.is_request_jumpout(topic_name=r"(video)? ?games?"),
            tracker.input_text,
            tracker.returnnlp[-1].text,
            tracker.returnnlp.detect_entities(),
            foreign_entities,
            re.search(r"^tell me more about", tracker.returnnlp[-1].text),
            next(iter(entity.GameItem.detect(tracker)), None),
            next(iter(entity.ConsoleItem.detect(tracker)), None),
            re.search(r"^tell me more about", tracker.returnnlp[-1].text) and
            (
                next(iter(entity.GameItem.detect(tracker)), None) or
                next(iter(entity.ConsoleItem.detect(tracker)), None)
            )
        ]

        logger.info(
            f"handle command, "
            f"{self.command_detector.is_request_jumpout()}, "
            f"{cond}, "
        )

        if self.command_detector.is_request_jumpout(topic_name=r"(video)? ?games?"):
            if request_jumpout_handler is not None:
                return request_jumpout_handler()
            else:
                return self.request_jumpout_handler(dispatcher, tracker)

        elif (
            self.command_detector.is_request_tellmore() or
            (
                re.search(r"^tell me more about", tracker.returnnlp[-1].text) and
                (
                    next(iter(entity.GameItem.detect(tracker)), None) or
                    next(iter(entity.ConsoleItem.detect(tracker)), None)
                )
            )
        ):
            if request_tellmore_handler is not None:
                return request_tellmore_handler()
            else:
                return self.request_tellmore_handler(dispatcher, tracker)

        elif foreign_entities:
            fe = foreign_entities[-1]
            dispatcher.respond(
                f"Seems like you want to talk about {fe.entity_detected}. Would you like to talk about that?")
            dispatcher.propose_continue('UNCLEAR', fe.module)
            return [NextState(states.ProposeUnclearRespond.name)]

        else:
            return

    def has_question_only(
        self, tracker: Tracker, *,
        skip_flag_check=False,
        is_questions: Optional[List[Callable[[], bool]]] = None,
        override=False
    ):
        return (
            len(tracker.returnnlp) <= 1 and
            self.has_question(tracker, skip_flag_check=skip_flag_check, is_questions=is_questions, override=override)
        )

    def has_question(
        self, tracker: Tracker, *,
        skip_flag_check=False,
        is_questions: Optional[List[Callable[[], bool]]] = None,
        override=False
    ):
        def generator(q):
            try:
                return q()
            except Exception as e:
                logger.warning(e)
                return False

        flag = (tracker.volatile_store.get('__handler_has_question_handled__') or False)

        default_cond = False if override else any(i(tracker) for i in [
            self.is_do_you_know_question,
            self.is_when_entity_released_question,
        ])

        logger.debug(
            f"has_question = {flag, default_cond, is_questions, override}"
        )

        return (
            (True if skip_flag_check else not flag) and (
                self.question_detector.has_question() or
                default_cond or
                any(generator(q) for q in is_questions or [])
            ))

    def handle_question(
        self, dispatcher: Dispatcher, tracker: Tracker,
        *,
        # required handler
        ask_back_handler: Callable[[QuestionResponse], Optional[List[Event]]],
        follow_up_handler: Callable[[QuestionResponse], Optional[List[Event]]],
        # optional handler
        do_you_know_handler: Callable[[QuestionResponse], Optional[List[Event]]] = None,
        when_entity_released_handler: Callable[[QuestionResponse], Optional[List[Event]]] = None,
        have_you_played_handler: Callable[[QuestionResponse], Optional[List[Event]]] = None,
        override_handler: Callable[[QuestionResponse], Optional[List[Event]]] = None,
        elif_handler: Callable[[QuestionResponse], Optional[List[Event]]] = None,
        # optional detector
        is_ask_back_question: Callable[[], bool] = None,
        is_follow_up_question: Callable[[], bool] = None,
        is_do_you_know_question: Callable[[], bool] = None,
        is_when_entity_released_question: Callable[[], bool] = None,
        is_have_you_played_question: Callable[[], bool] = None,
        # optional utils
        ignore_idk_response: Callable[[QuestionResponse], bool] = None,
        skip_blender_response: Callable[[QuestionResponse], bool] = None,
        inspect_question_response: Callable[[QuestionResponse], None] = None,
    ) -> Optional[List[Event]]:

        tracker.volatile_store['__handler_has_question_handled__'] = True

        question_response = self.question_handler.handle_question()
        logger.debug(
            f"question_resposne = {question_response}, "
            f"ask_back = {self.question_detector.is_ask_back_question()}, "
            f"follow_up = {self.question_detector.is_follow_up_question()}, "
        )

        if question_response and question_response.tag is QuestionResponseTag.BLENDER:
            tracker.volatile_store['handler_used_blender'] = True

        if inspect_question_response:
            inspect_question_response(question_response)

        if override_handler:
            logger.debug(f"override_handler")
            return override_handler(question_response)

        elif (
            is_ask_back_question() if is_ask_back_question is not None else
            (
                self.question_detector.is_ask_back_question() or
                re.search(r"do you$", tracker.returnnlp[-1].text)
            )
        ):
            logger.debug(f"ask_back_handler, custom = {bool(is_ask_back_question)}")
            return ask_back_handler(question_response)

        elif (
            is_follow_up_question() if is_follow_up_question is not None else
            self.question_detector.is_follow_up_question()
        ):
            logger.debug(f"follow_up_qustion, custom = {bool(is_follow_up_question)}")
            return follow_up_handler(question_response)

        elif (
            is_do_you_know_question() if is_do_you_know_question is not None else
            self.is_do_you_know_question(tracker)
        ):
            logger.debug(f"do_you_know_question, custom = {bool(is_follow_up_question)}")
            if do_you_know_handler is not None:
                return do_you_know_handler(question_response)
            else:
                return self.do_you_know_handler(dispatcher, tracker, question_response)

        elif (
            is_when_entity_released_question() if is_when_entity_released_question is not None else
            self.is_when_entity_released_question(tracker)
        ):
            if when_entity_released_handler is not None:
                return when_entity_released_handler(question_response)
            else:
                return self.when_entity_released_handler(dispatcher, tracker, question_response)

        elif (
            is_have_you_played_question() if is_have_you_played_question is not None else
            self.is_have_you_played_question(tracker)
        ):
            if have_you_played_handler is not None:
                return have_you_played_handler(question_response)
            else:
                return self.have_you_played_handler(dispatcher, tracker, question_response)

        elif elif_handler:
            logger.debug(f"elif_handler")
            return elif_handler(question_response)

        elif (
            question_response and question_response.bot_propose_module and
            question_response.bot_propose_module != TopicModule.GAME
        ):
            logger.debug(f"default")
            dispatcher.respond(question_response.response)
            dispatcher.respond_template(
                f"propose_topic_short/{question_response.bot_propose_module.value}", {},
                template=Template.transition)

            dispatcher.propose_continue('UNCLEAR', question_response.bot_propose_module)
            return [NextState(states.Initial.name)]

        elif (
            skip_blender_response and skip_blender_response(question_response) and
            question_response.tag is QuestionResponseTag.BLENDER
        ):
            logger.debug(f"skipping blender response: {question_response}")
            return None

        elif (
            not (
                ignore_idk_response and ignore_idk_response(question_response) and
                question_response.tag is QuestionResponseTag.IDK
            )
        ):
            logger.debug(f"default: {question_response}")
            dispatcher.respond(question_response.response)
            return None

    """extra handlers"""

    def is_do_you_know_question(self, tracker: Tracker):

        regex = re.search(
            r"do you know (anything)? ?about|i like (to play|playing) .* (have you heard|do you (know|play))",
            tracker.input_text)
        regex_alt = re.search(r"do you know (anything)? ?about games?", tracker.input_text)
        game = next(iter(entity.GameItem.detect(tracker)), None)
        console = next(iter(entity.ConsoleItem.detect(tracker)), None)

        if not (game or console):
            return bool(regex_alt)
        else:
            return regex and (game or console)

    def do_you_know_handler(self, dispatcher: Dispatcher, tracker: Tracker, qr: QuestionResponse):

        regex_alt = re.search(r"do you know (anything)? ?about games?", tracker.input_text)
        game = next(iter(entity.GameItem.detect(tracker)), None)
        console = next(iter(entity.ConsoleItem.detect(tracker)), None)

        logger.debug(f"do you know handler: cond = {regex_alt, game, console}, ")

        if not (game or console) and regex_alt:

            dispatcher.respond_template(f"question/do_you_know/respond/alt", {})
            return [NextState(states.TalkAboutGamePropose.name, jump=True)]

        elif game:
            states.TalkAboutGameRespond.require_entity(tracker, game)
            dispatcher.respond(f"I know {game.noun}.")
            return [NextState(states.TalkAboutGameRespond.name, jump=True)]

        elif console:
            states.TalkAboutConsoleRespond.require_entity(tracker, console)
            return [NextState(states.TalkAboutConsoleRespond.name, jump=True)]

    def is_when_entity_released_question(self, tracker: Tracker):

        regex = re.search(r"when (is|was|are|were) (?P<entity>.+) (released|created|published)", tracker.input_text)
        game = next(iter(entity.GameItem.detect(tracker)), None)
        console = next(iter(entity.ConsoleItem.detect(tracker)), None)

        # logger.debug(f"when entity released handler: cond = {regex, game, console}, ")

        return regex and (game or console)

    def when_entity_released_handler(self, dispatcher: Dispatcher, tracker: Tracker, qr: QuestionResponse):

        # TODO: db for this
        # game = next(iter(entity.GameItem.detect(tracker)), None)
        # console = next(iter(entity.ConsoleItem.detect(tracker)), None)

        logger.debug(f"when entity released: qr = {qr}")

        dispatcher.respond(qr.response)

    def is_have_you_played_question(self, tracker: Tracker):

        regex = re.search(r"have you (played|heard of)", tracker.input_text)
        game = next(iter(entity.GameItem.detect(tracker)), None)

        return regex and game

    def have_you_played_handler(self, dispatcher: Dispatcher, tracker: Tracker, qr: QuestionResponse):
        game = next(iter(entity.GameItem.detect(tracker)), None)
        if not game:
            return

        up = user_profile.UserProfile(tracker)

        if game and states.GameCannedResponseEnter.accept_transition(tracker, game, up):
            states.GameCannedResponseEnter.require_entity(tracker, game)
            return [NextState(states.GameCannedResponseEnter.name, jump=True)]

        elif (
            game and
            states.GameFunfactEnter.accept_transition(tracker, game) and
            states.GameFunfactPropose.still_has_funfact(tracker, dispatcher, game)
        ):
            states.GameFunfactPropose.require_entity(tracker, game)
            return [NextState(states.GameFunfactPropose.name, jump=True)]

        else:
            states.DunnoGamePropose.require_entity(tracker, game, use_blender=False)
            return [NextState(states.DunnoGamePropose.name, jump=True)]
