import json
import logging
import random
from dataclasses import dataclass
from typing import Optional

import boto3
from botocore.exceptions import ClientError

from response_generator.fsm2 import Tracker

from response_generator.game.utils import entity


logger = logging.getLogger('game.utils')


dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
table_game_funfacts = dynamodb.Table('game_funfacts')

has_hardcode_funfacts_cache = {}
funfacts_count_cache = {}


@dataclass(frozen=True)
class Funfact:
    fact: str
    key: str
    question: str
    oh_i_have_heard: Optional[str]
    oh_i_have_heard_ack: Optional[str]

    def __ua_encode__(self) -> str:
        return json.dumps(self.__dict__)

    @classmethod
    def __ua_decode__(cls, value: str):
        try:
            value = json.loads(value)
            return cls(**value)
        except (json.JSONDecodeError, TypeError) as e:
            logger.warning(e, exc_info=True)
            return None


def get_funfacts(game: entity.GameItem):

    try:
        response = table_game_funfacts.get_item(
            Key={
                'canonical': game.canonical,
            }
        )
        return response.get('Item', {}).get('funfacts', [])
    except ClientError as e:
        logger.warning(f"[GAME] <funfacts> query db error: {e}", exc_info=True)


def has_hardcode_funfacts(game: entity.GameItem):
    if game.canonical in has_hardcode_funfacts_cache:
        logger.debug(f"has_hardcode_funfacts cache hit: {game}, {has_hardcode_funfacts_cache}")
        return has_hardcode_funfacts_cache[game.canonical]
    else:
        logger.debug(f"has_hardcode_funfacts cache miss: {game}, {has_hardcode_funfacts_cache}")
        ret = bool(get_funfacts(game))
        # global has_hardcode_funfacts_cache
        has_hardcode_funfacts_cache[game.canonical] = ret
        return ret


def still_has_funfacts(tracker: Tracker, game: entity.GameItem):
    funfact_counter = tracker.persistent_store.get(f"funfact_<{game.canonical}>_counter") or 0
    if game.canonical in funfacts_count_cache:
        logger.debug(f"still_has_funfacts cache hit: {game}, {funfacts_count_cache}")
        funfact_total = funfacts_count_cache[game.canonical]
    else:
        logger.debug(f"still_has_funfacts cache miss: {game}, {funfacts_count_cache}")
        funfact_total = len(get_funfacts(game) or [])

    return funfact_counter < funfact_total


def funfact_counter(tracker: Tracker, game: entity.GameItem):
    return tracker.persistent_store.get(f"funfact_<{game.canonical}>_counter") or 0


def increment_funfact_counter(tracker: Tracker, game: entity.GameItem):
    funfact_counter = tracker.persistent_store.get(f"funfact_<{game.canonical}>_counter") or 0
    tracker.persistent_store[f"funfact_<{game.canonical}>_counter"] = funfact_counter + 1


def funfact_hist(tracker: Tracker, game: entity.GameItem):
    return tracker.persistent_store.get(f"funfact_<{game.canonical}>_used") or []


def get_random_funfact(tracker: Tracker, game: entity.GameItem, save_hist=True) -> Optional[Funfact]:

    hist = funfact_hist(tracker, game)
    funfacts = [
        (idx, s)
        for (idx, s) in enumerate(get_funfacts(game) or [])
        if idx not in hist
    ]
    if not funfacts:
        return None

    idx, funfact = random.choice(funfacts)
    if not funfact or (funfact and not all(i in funfact for i in ['fact', 'key', 'question'])):
        return None
    oh_i_have_heard = funfact.get('oh_i_have_heard')
    oh_i_have_heard_ack = funfact.get('oh_i_have_heard_ack')
    if not isinstance(oh_i_have_heard, str):
        oh_i_have_heard = None
    if not isinstance(oh_i_have_heard_ack, str):
        oh_i_have_heard_ack = None

    funfact = Funfact(
        fact=funfact['fact'],
        key=funfact['key'],
        question=funfact['question'],
        oh_i_have_heard=oh_i_have_heard,
        oh_i_have_heard_ack=oh_i_have_heard_ack)

    logger.info(
        f"get_random_funfact: "
        f"raw: {idx, funfact}, "
        f"funfact = {funfact}, "
    )
    if save_hist:
        tracker.persistent_store[f"funfact_<{game.canonical}>_used"] = [*hist, idx]
    return funfact
