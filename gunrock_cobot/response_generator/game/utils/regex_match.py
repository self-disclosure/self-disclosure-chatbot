import re
from typing import Match, Optional, TYPE_CHECKING

if TYPE_CHECKING:
    from response_generator.fsm import Tracker


def match(tracker: 'Tracker', whitelist: str = None, blacklist: str = None) -> Optional[Match]:
    return re.search(whitelist, tracker.input_text, re.I) if (
        whitelist and not (blacklist and re.search(blacklist, tracker.input_text, re.I))
    ) else None


def is_playing_alone(tracker: 'Tracker') -> Optional[Match]:
    return match(
        tracker,
        whitelist=(
            r"alone|just me|myself|on my own|single ?player|offline"
        ),
        blacklist=(
            r"multi ?player|offline with|with (my|some( of my)?)? ?friends"
        )
    )


def is_jumpout_phrase(tracker: 'Tracker') -> Optional[Match]:
    return match(
        tracker,
        whitelist=(
            r"(i (do|did)(n'?t| not) (really)? ?(like|prefer|enjoy|love|wanna|want to|) (talking |(to |)talk about |playing |to play |)((another )?(video |)games?|this))|"  # noqa: E501
            r"(i (hate|dislike) (video)? ?games?)|"
            r"((i do(n'?t| not)|i('m| am) not) (play|care about|interested in) (playing )?(video |)games|(^no games$))|"
            r"(i do(n't| not) (really)? ? (like|enjoy) (video)? ?games?)|"
            r"(i do(n't| not) know (much|anything) about (video)? ?games?)"
        )
    )
