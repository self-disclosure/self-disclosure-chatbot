import random
from typing import List, Optional

from response_generator.fsm2 import Dispatcher, Tracker, NextState
from utils import get_working_environment

from response_generator.game import states
from response_generator.game.states.base import GameState


selector_root = "qa"


def qa_paths():
    return [[
        states.QaFavGenreEnter.name,
        states.QaNextGenConsoleEnter.name,
        states.QaPlayAloneEnter.name,
    ], [
        states.QaWitcherThreeEnter.name,
    ]]


def qa_path_select(tracker: Tracker, add_to_hist=True) -> Optional[str]:
    hist = tracker.persistent_store.get('qa_path_hist') or []
    paths = [[i for i in p if i not in hist] for p in qa_paths()]
    if paths[0]:
        if get_working_environment() == 'local':
            path = next(iter(paths[0]), None)
        else:
            path = random.choice(paths[0])
    elif paths[1]:
        path = next(iter(paths[1]), None)
    else:
        path = None

    if path and add_to_hist:
        tracker.persistent_store['qa_path_hist'] = [*hist, path]
    return path


def qa_path_hist(tracker: Tracker) -> List[str]:
    return tracker.persistent_store.get('qa_path_hist') or []


class QaEnter(GameState):

    name = "qa_enter"

    @classmethod
    def accept_transition(cls, tracker: Tracker):
        return states.QaPropose.accept_transition(tracker)

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if states.QaPropose.accept_transition(tracker):

            if (
                any(i in {
                    states.QaExit.name,
                    states.FavGenreGameGeneralRespond.name,
                    states.FavGenreGameRespond.name,
                    states.DunnoGameRespond.name,
                    states.TalkAboutGameExit.name,  # if we just came from there, we def switching subtopic
                } for i in self.module.state_tracker.curr_turn())
            ):
                dispatcher.respond_template("transition/general_qa_ask/ask_you_something_else", {})

            elif not any(i in {
                states.Initial.name,
                states.ResumeRespond.name
            } for i in self.module.state_tracker.curr_turn()):
                dispatcher.respond_template("transition/general_qa_ask/anyway", {})

            return [NextState(states.QaPropose.name, jump=True)]
        else:
            states.SubtopicSwitchEnter.require_entity(tracker, responses=[], tag=[])
            return [NextState(states.SubtopicSwitchEnter.name, jump=True)]


class QaExit(GameState):

    name = "qa_exit"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.QaEnter.name, jump=True)]


class QaPropose(GameState):

    name = "qa_propose"

    @classmethod
    def accept_transition(cls, tracker: Tracker):
        return bool(qa_path_select(tracker, add_to_hist=False))

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        path = qa_path_select(tracker)
        return [NextState(path, jump=True)]
