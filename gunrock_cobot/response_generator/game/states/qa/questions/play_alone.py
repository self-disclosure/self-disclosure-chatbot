import re

from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.game.states.base import GameState
from response_generator.game import states


selector_root = "qa/play_alone"


class QaPlayAloneEnter(GameState):

    name = 'qa_play_alone_enter'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.QaPlayAlonePropose.name, jump=True)]


class QaPlayAloneExit(GameState):

    name = 'qa_play_alone_exit'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.QaExit.name, jump=True)]


class QaPlayAlonePropose(GameState):

    name = 'qa_play_alone_propose'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/propose", {})
        return [NextState(states.QaPlayAloneRespond.name)]


class QaPlayAloneRespond(GameState):

    name = 'qa_play_alone_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        def handle_question():
            def ask_back_handler(qr):
                dispatcher.respond_template(f"{selector_root}/respond/ask_back", {})

            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=ask_back_handler,
                    follow_up_handler=lambda qr: None,
                )

        if re.search(r"alone|just me|myself|on my own|single ?player", tracker.input_text):
            key = "alone"
        elif re.search(r"with friends|multi ?player|someone else|with some(one|body)", tracker.input_text):
            key = "with_friends"
        else:
            key = None

        if self.sys_ack_tag != 'ack_question_idk' and self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
        else:
            dispatcher.respond_template(f"{selector_root}/respond/ack", {})

        qr_events = handle_question()
        if qr_events:
            return qr_events

        if key:
            dispatcher.respond_template(f"{selector_root}/respond/{key}", {})

        return [NextState(states.QaPlayAloneExit.name, jump=True)]
