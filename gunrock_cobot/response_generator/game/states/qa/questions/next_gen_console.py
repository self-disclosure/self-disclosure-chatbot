import re

from nlu.constants import DialogAct, Positivity
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.game import states
from response_generator.game.states.base import GameState
from response_generator.game.utils import entity


selector_root = "qa/next_gen_console"


class QaNextGenConsoleEnter(GameState):

    name = 'qa_next_gen_console_enter'
    @classmethod
    def require_entity(cls, tracker: Tracker, key=None):
        tracker.volatile_store['qa_next_gen_key'] = key

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        states.QaNextGenConsolePropose.require_entity(tracker, key=tracker.volatile_store.get('qa_next_gen_key'))
        return [NextState(states.QaNextGenConsolePropose.name, jump=True)]


class QaNextGenConsoleExit(GameState):

    name = 'qa_next_gen_console_exit'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.QaEnter.name, jump=True)]


class QaNextGenConsolePropose(GameState):

    name = 'qa_next_gen_console_propose'

    @classmethod
    def require_entity(cls, tracker: Tracker, key=None):
        tracker.volatile_store['qa_next_gen_key'] = key

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if tracker.volatile_store.get('qa_next_gen_key') == 'question_only':
            dispatcher.respond_template(f"{selector_root}/propose_question_only", {})
        else:
            dispatcher.respond_template(f"{selector_root}/propose", {})
        return [NextState(states.QaNextGenConsoleRespond.name)]


class QaNextGenConsoleRespond(GameState):

    name = 'qa_next_gen_console_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        consoles = entity.ConsoleItem.detect(tracker)
        alt_console = next(iter(
            i for i in consoles if i.canonical not in {'ps4', 'ps5', 'xbox_one', 'xbox_series_x'}), None)

        def handle_question():

            regex_how_much = r"\b(how much|what('s| is) the cost)\b"

            def ask_back_handler(qr):
                pass

            def follow_up_handler(qr):
                if re.search(regex_how_much, tracker.input_text):
                    dispatcher.respond("If i remember correctly, they're likely around 500 dollars.")

            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=ask_back_handler,
                    follow_up_handler=follow_up_handler,
                    is_follow_up_question=lambda: (
                        self.handler.question_detector.is_follow_up_question() or
                        re.search(regex_how_much, tracker.input_text)
                    )
                )

        def gen_ack(alt_ack: str = "i_see"):
            if self.handler.has_question_only(tracker):
                return
            if (
                self.sys_ack_tag != 'ack_question_idk' and self.sys_ack_utt and
                not tracker.returnnlp.has_dialog_act(DialogAct.OTHER_ANSWERS)
            ):
                dispatcher.respond(self.sys_ack_utt)
            elif alt_ack == 'thats_alright':
                dispatcher.respond_template(f"ack/thats_alright", {})
            else:
                dispatcher.respond_template(f"ack/i_see", {})

        if alt_console:
            gen_ack()
            states.TalkAboutConsoleRespond.require_entity(tracker, alt_console)
            return [NextState(states.TalkAboutConsoleRespond.name, jump=True)]

        elif (
            any(i.canonical in {'ps4', 'ps5'} for i in consoles) or
            re.search(r"\b(ps ?5|ps ?five|sony|play ?station)\b", tracker.input_text, re.I)
        ):
            gen_ack()
            dispatcher.respond_template(f"{selector_root}/respond/buy_sony", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events
            states.QaNextGenConsoleBuyXPropose.require_entity(tracker, 'sony')
            return [NextState(states.QaNextGenConsoleBuyXPropose.name, jump=True)]

        elif (
            any(i.canonical in {'xbox_one', 'xbox_series_x'} for i in consoles) or
            re.search(r"\b(xbox series x|microsoft|x ?box)\b", tracker.input_text, re.I)
        ):
            gen_ack()
            dispatcher.respond_template(f"{selector_root}/respond/buy_microsoft", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events
            states.QaNextGenConsoleBuyXPropose.require_entity(tracker, 'microsoft')
            return [NextState(states.QaNextGenConsoleBuyXPropose.name, jump=True)]

        elif tracker.returnnlp.answer_positivity is Positivity.neg:
            gen_ack()
            dispatcher.respond_template(f"{selector_root}/respond/neg", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events
            return [NextState(states.QaNextGenConsoleExit.name, jump=True)]

        elif tracker.returnnlp.answer_positivity in {Positivity.neu, Positivity.pos}:
            gen_ack()
            qr_events = handle_question()
            if qr_events:
                return qr_events
            dispatcher.respond_template(f"{selector_root}/respond/neu", {})
            return [NextState(states.QaNextGenConsoleExit.name, jump=True)]

        else:
            gen_ack()
            qr_events = handle_question()
            if qr_events:
                return qr_events
            return [NextState(states.QaNextGenConsoleExit.name, jump=True)]


class QaNextGenConsoleBuyXPropose(GameState):

    name = 'qa_next_gen_console_buy_x_propose'

    @classmethod
    def require_entity(cls, tracker: Tracker, buy_x: str):
        tracker.one_turn_store['buy_x'] = buy_x

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/buy_x/propose", {})
        return [NextState(states.QaNextGenConsoleRespond.name)]


class QaNextGenConsoleBuyXRespond(GameState):

    name = 'qa_next_gen_console_buy_x_respond'

    @classmethod
    def require_entity(cls, tracker: Tracker, buy_x: str):
        tracker.volatile_store['buy_x'] = buy_x

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        buy_x = (
            tracker.volatile_store.get('buy_x') or
            tracker.one_turn_store.get('buy_x') or
            tracker.last_utt_store.get('buy_x') or
            None
        )

        def handle_question():

            def ask_back_handler(qr):
                dispatcher.respond(f"Personally, I do think they look cool.")

            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=ask_back_handler,
                    follow_up_handler=lambda qr: None,
                )

        if buy_x in {'microsoft', 'sony'}:
            """get key by answer_positivity"""
            if tracker.returnnlp.has_dialog_act(DialogAct.OTHER_ANSWERS):
                key = 'neu'
            else:
                key = tracker.returnnlp.answer_positivity.name

            """build ack"""
            if (
                self.sys_ack_tag != 'ack_question_idk' and self.sys_ack_utt and
                not tracker.returnnlp.has_dialog_act(DialogAct.OTHER_ANSWERS)
            ):
                dispatcher.respond(self.sys_ack_utt)
            elif key == 'pos':
                dispatcher.respond(f"Oh cool!")
            else:
                dispatcher.respond_template(f"ack/i_see", {})
                if key == 'neg':
                    dispatcher.respond_template(f"ack/thats_alright", {})

            """respond"""
            dispatcher.respond_template(f"{selector_root}/buy_x/respond/{buy_x}/{key}", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events

        else:
            if (
                self.sys_ack_tag != 'ack_question_idk' and self.sys_ack_utt and
                not tracker.returnnlp.has_dialog_act(DialogAct.OTHER_ANSWERS)
            ):
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond_template(f"{selector_root}/buy_x/respond/others", {})

            qr_events = handle_question()
            if qr_events:
                return qr_events

        return [NextState(states.QaNextGenConsoleExit.name, jump=True)]
