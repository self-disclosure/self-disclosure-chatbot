import re

from nlu.constants import Positivity
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.game.states.base import GameState
from response_generator.game import states


selector_root = "qa/witcher_3"


class QaWitcherThreeEnter(GameState):

    name = 'qa_witcher_three_enter'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.QaWitcherThreePropose.name, jump=True)]


class QaWitcherThreeExit(GameState):

    name = 'qa_witcher_three_exit'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.QaExit.name, jump=True)]


class QaWitcherThreePropose(GameState):

    name = 'qa_witcher_three_propose'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/propose", {})
        return [NextState(states.QaWitcherThreeRespond.name)]


class QaWitcherThreeRespond(GameState):

    name = 'qa_witcher_three_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        regex_follow_up = r"\b(what('s| is) (it|this|that|(the )?witcher|(the|a) show))\b"

        def is_follow_up_question():
            return (
                self.handler.question_detector.is_follow_up_question() or
                bool(re.search(regex_follow_up, tracker.input_text))
            )

        def handle_question(skip_ask_info=False):

            def ask_back_handler(qr):
                pass

            def follow_up_handler(qr):
                if not skip_ask_info and re.search(regex_follow_up, tracker.input_text):
                    dispatcher.respond_template(f"{selector_root}/respond/ask_info", {})
                    return [NextState(states.QaWitcherThreeViewOnAdaptationPropose.name, jump=True)]

            if self.handler.has_question(tracker, is_questions=[is_follow_up_question]):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=ask_back_handler,
                    follow_up_handler=follow_up_handler,
                    is_follow_up_question=is_follow_up_question,
                )

        if (
            self.handler.has_question_only(tracker, is_questions=[is_follow_up_question]) or
            # override for ask_info
            re.search(regex_follow_up, tracker.input_text)
        ):
            qr_events = handle_question()
            if qr_events:
                return qr_events
            # Fallback if handle_question did not return a valid next_state
            return [NextState(states.QaWitcherThreeViewOnAdaptationPropose.name, jump=True)]
        elif (
            tracker.returnnlp.answer_positivity is Positivity.pos or
            re.search(r"\b(i('ve| have)? watch(ed)?)\b", tracker.input_text)
        ):
            """pos path"""
            if self.sys_ack_tag not in {'ack_question_idk'} and self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond_template(f"{selector_root}/respond/pos_ack", {})

            qr_events = handle_question(skip_ask_info=True)
            if qr_events:
                return qr_events
            dispatcher.respond_template(f"{selector_root}/respond/pos", {})
            return [NextState(states.QaWitcherThreeHaveYouAlsoPlayedPropose.name, jump=True)]

        elif tracker.returnnlp.answer_positivity in {Positivity.neg, Positivity.neu}:
            """neg path sans ask_info"""
            if self.sys_ack_tag not in {'ack_question_idk'} and self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond_template(f"{selector_root}/respond/neg_ack", {})

            qr_events = handle_question(skip_ask_info=True)
            if qr_events:
                return qr_events
            dispatcher.respond_template(f"{selector_root}/respond/neg", {})
            return [NextState(states.QaWitcherThreeViewOnAdaptationPropose.name, jump=True)]

        else:
            """fallthru protection"""
            if self.sys_ack_tag not in {'ack_question_idk'} and self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond_template(f"{selector_root}/respond/neg_ack", {})

            qr_events = handle_question(skip_ask_info=True)
            if qr_events:
                return qr_events
            return [NextState(states.QaWitcherThreeExit.name, jump=True)]


class QaWitcherThreeHaveYouAlsoPlayedPropose(GameState):

    name = 'qa_witcher_three_have_you_also_played_propose'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/have_you_also_played/propose", {})
        return [NextState(states.QaWitcherThreeHaveYouAlsoPlayedRespond.name)]


class QaWitcherThreeHaveYouAlsoPlayedRespond(GameState):

    name = 'qa_witcher_three_have_you_also_played_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        def handle_question():
            def ask_back_handler(qr):
                """have you also played witcher 3"""
                dispatcher.respond_template(f"{selector_root}/have_you_also_played/respond/ask_back", {})

            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=ask_back_handler,
                    follow_up_handler=lambda qr: None,
                )

        if self.handler.has_question_only(tracker):
            qr_events = handle_question()
            if qr_events:
                return qr_events
            return [NextState(states.QaWitcherThreeExit.name, jump=True)]

        elif re.search(r"\b(too young|not old enough|not allowed to)\b", tracker.input_text):
            dispatcher.respond_template(f"{selector_root}/have_you_also_played/respond/too_young", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events
            return [NextState(states.QaWitcherThreeExit.name, jump=True)]

        else:
            key = 'pos' if (
                tracker.returnnlp.answer_positivity is Positivity.pos or
                tracker.returnnlp.opinion_positivity is Positivity.pos
            ) else 'neg'
            if self.sys_ack_tag not in {'ack_question_idk'} and self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            elif key == 'pos':
                dispatcher.respond("Cool!")
            else:
                dispatcher.respond_template("t_fragments/ack_i_see", {})

            dispatcher.respond_template(f"{selector_root}/have_you_also_played/respond/{key}", {})

            qr_events = handle_question()
            if qr_events:
                return qr_events
            return [NextState(states.QaWitcherThreeExit.name, jump=True)]


class QaWitcherThreeViewOnAdaptationPropose(GameState):

    name = 'qa_witcher_three_view_on_adaptation_propose'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/view_on_adaptation/propose", {})
        return [NextState(states.QaWitcherThreeViewOnAdaptationRespond.name)]


class QaWitcherThreeViewOnAdaptationRespond(GameState):

    name = 'qa_witcher_three_view_on_adaptation_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        def handle_question():
            def ask_back_handler(qr):
                """have you also played witcher 3"""
                dispatcher.respond_template(f"{selector_root}/view_on_adaptation/respond/ask_back", {})

            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=ask_back_handler,
                    follow_up_handler=lambda qr: None,
                )

        if self.handler.has_question_only(tracker):
            qr_events = handle_question()
            if qr_events:
                return qr_events
            return [NextState(states.QaWitcherThreeExit.name, jump=True)]

        elif re.search(r"\b(too young|not old enough|not allowed to)\b", tracker.input_text):
            dispatcher.respond_template(f"{selector_root}/view_on_adaptation/respond/too_young", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events
            return [NextState(states.QaWitcherThreeExit.name, jump=True)]

        else:
            if self.sys_ack_tag not in {'ack_question_idk'} and self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond_template("ack/i_see", {})

            key = tracker.returnnlp.opinion_positivity.name
            dispatcher.respond_template(f"{selector_root}/view_on_adaptation/respond/{key}", {})

            qr_events = handle_question()
            if qr_events:
                return qr_events
            return [NextState(states.QaWitcherThreeExit.name, jump=True)]
