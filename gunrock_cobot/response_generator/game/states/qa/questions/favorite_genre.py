from typing import Optional

from nlu.constants import DialogAct
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.game import states
from response_generator.game.states.base import GameState
from response_generator.game.utils import entity, utils


selector_root = "qa/favorite_genre"


class QaFavGenreEnter(GameState):

    name = "qa_fav_genre_enter"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.QaFavGenrePropose.name, jump=True)]


class QaFavGenreExit(GameState):

    name = "qa_fav_genre_exit"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.QaExit.name, jump=True)]


class QaFavGenrePropose(GameState):

    name = "qa_fav_genre_propose"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/propose", {})
        return [NextState(states.QaFavGenreRespond.name)]


class QaFavGenreRespond(GameState):

    name = "qa_fav_genre_respond"

    question_map = dict(
        fav_genre_game={
            'action', 'shooter', 'platform', 'fighting', 'battle_royale', 'horror', 'adventure', 'visual_novels',
            'rpg', 'mmorpg', 'roguelikes', 'simulation', 'sandbox', 'strategy', 'moba', 'rts', 'tower_defence',
            'sports', 'racing', 'casual_games', 'ccg', 'mobile_game', 'word_game',
            'mmo', 'idle_gaming',
        },
        fav_general_game={
            'board_game', 'card_game',
        }
    )

    @classmethod
    def require_entity(cls, tracker: Tracker, genre: Optional[entity.GenreItem]):
        tracker.one_turn_store['genre_to_discuss'] = genre

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        """"""

        """getting available genres and false games"""
        available_genres = set(
            dispatcher.tm.get_child_keys(f'{selector_root}/respond/genre')) - set(['default', 'default_no_ack'])
        available_false_games = (
            set(dispatcher.tm.get_child_keys(f'{selector_root}/respond/false_games')) - set(['default'])
        )

        genre = (
            self.genre_to_discuss or
            next(iter(entity.GenreItem.detect(tracker)), None)
        )
        false_games = entity.FalseGameItem.detect(tracker)
        game = next(iter(entity.GameItem.detect(tracker)), None)
        console = next(iter(entity.ConsoleItem.detect(tracker)), None)

        def handle_question():
            def ask_back_handler(qr):
                dispatcher.respond("I like all kinds of games.")

            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=ask_back_handler,
                    follow_up_handler=lambda qr: None,
                )

        if genre and genre.canonical in available_genres:
            dispatcher.respond_template(
                f"{selector_root}/respond/genre/{genre.canonical}", dict(genre=utils.remove_game_suffix(genre.noun)))
            qr_events = handle_question()
            if qr_events:
                return qr_events

            if genre.canonical in self.question_map['fav_genre_game']:
                question = 'fav_genre_game'
            else:
                question = 'fav_general_game'
            states.FavGenreGamePropose.require_entity(tracker, genre, question)
            return [NextState(states.FavGenreGamePropose.name,  jump=True)]

        elif not genre and false_games:
            false_game = next((e for e in false_games if e.canonical in available_false_games), None)
            if false_game:
                dispatcher.respond_template(
                    f"{selector_root}/respond/false_games/{false_game.canonical}", dict(game=false_game.noun))
            else:
                dispatcher.respond_template(f"{selector_root}/respond/false_games/default", dict(game="it"))
            qr_events = handle_question()
            if qr_events:
                return qr_events
            states.FavGenreGamePropose.require_entity(tracker, genre, 'fav_general_game')
            return [NextState(states.FavGenreGamePropose.name,  jump=True)]

        elif game:
            states.TalkAboutGameRespond.require_entity(tracker, game)
            return [NextState(states.TalkAboutGameRespond.name, jump=True)]

        elif console:
            states.TalkAboutConsoleRespond.require_entity(tracker, console)
            return [NextState(states.TalkAboutConsoleRespond.name, jump=True)]

        else:
            if tracker.returnnlp.has_dialog_act(DialogAct.OTHER_ANSWERS):
                dispatcher.respond_template(f"ack/thats_alright", {})
            elif self.sys_ack_tag != 'ack_question_idk' and self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond_template(f"ack/i_see", {})

            qr_events = handle_question()
            if qr_events:
                return qr_events

            dispatcher.respond_template(f"{selector_root}/respond/default", dict())
            return [NextState(states.QaFavGenreExit.name, jump=True)]
