from typing import Optional

from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.game import states
from response_generator.game.states.base import GameState
from response_generator.game.utils import entity, user_profile


# MARK - Game Discussion Specific

class GameCannedResponseEnter(GameState):

    name = "game_canned_response_enter"

    @classmethod
    def accept_transition(
        cls, tracker: Tracker, game: entity.GameItem, user_profile: Optional[user_profile.UserProfile] = None
    ):
        if user_profile:
            return game.has_hardcode_chitchat() and game not in GameCannedResponseEnter.hist(tracker)
        else:
            return game.has_hardcode_chitchat()

    @classmethod
    def require_entity(self, tracker: Tracker, game: entity.GameItem):
        tracker.one_turn_store['game_to_discuss'] = game

    @classmethod
    def hist(cls, tracker: Tracker):
        return tracker.persistent_store.get('game_canned_hist') or []

    @classmethod
    def hist_append(cls, tracker: Tracker, game: entity.GameItem):
        hist = GameCannedResponseEnter.hist(tracker)
        tracker.persistent_store['game_canned_hist'] = [*hist, game]

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        game: entity.GameItem = (
            self.game_to_discuss or
            next(iter(entity.detect_games(tracker)), None)
        )
        if (
            (not game and GameState.has_subclass(f"game_canned_<{game.canonical}>")) or
            not game and game.has_hardcode_chitchat()
        ):
            return [NextState(states.ErrorHandlingEnter.name, jump=True)]

        if game in GameCannedResponseEnter.hist(tracker):
            if states.QaEnter.accept_transition(tracker):
                dispatcher.respond_template(
                    f"game_canned_response_utils/transition/discussed_before", dict(game=game.noun)
                )
                return [NextState(states.QaEnter.name, jump=True)]
            elif states.TalkAboutAnotherGamePropose.accept_transition(tracker):
                dispatcher.respond_template(
                    f"game_canned_response_utils/transition/discussed_before_another_game", dict(game=game.noun)
                )
            else:
                states.ExitTransitionEnter.require_entity(tracker, flag='transition')
                return [NextState(states.ExitTransitionEnter.name, jump=True)]
        else:
            GameCannedResponseEnter.hist_append(tracker, game)

        return [NextState(f"game_canned_<{game.canonical}>", jump=True)]


class GameCannedResponseExit(GameState):

    name = "game_canned_response_exit"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if (
            tracker.volatile_store.get('game_specific_error_flag', False)
        ):
            dispatcher.respond_error("return_to_general_qa_ask", {})
        else:
            dispatcher.respond("Let me ask you something else.")

        states.SubtopicSwitchEnter.require_entity(tracker, responses=[], tag=[])
        return [NextState(states.SubtopicSwitchEnter.name, jump=True)]
