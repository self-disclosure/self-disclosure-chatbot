import re
from typing import List

from nlu.constants import Positivity
from response_generator.fsm2 import Dispatcher, Tracker, NextState
from response_generator.fsm2.events import Event

from response_generator.game.states.base import GameState
from response_generator.game import states


selector_root = 'game_canned_response/animal_crossing'


class GameCannedAnimalCrossing(GameState):

    name = "game_canned_<animal_crossing>"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        if self.handler.handler_used_blender(tracker):
            dispatcher.respond_template(f"{selector_root}/transition_from_game_discussion/blender", {})
        else:
            dispatcher.respond_template(f"{selector_root}/transition_from_game_discussion/default", {})
        return [NextState(states.GameCannedAnimalCrossingProposing.name, jump=True)]


class GameCannedAnimalCrossingProposing(GameState):

    name = 'game_canned_<animal_crossing>_proposing'

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        dispatcher.respond_template(f"{selector_root}/proposing", {})
        states.GameCannedAnimalCrossingProposingRespond.require_entity(dispatcher, tracker)
        return [NextState(GameCannedAnimalCrossingProposingRespond.name)]


class GameCannedAnimalCrossingProposingRespond(GameState):

    name = 'game_canned_<animal_crossing>_proposing_respond'

    @classmethod
    def require_entity(cls, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.expect_opinion(True)

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:

        def handle_question():
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: dispatcher.respond_template(f"{selector_root}/ask_back", {}),
                    follow_up_handler=lambda qr: None,
                    skip_blender_response=lambda qr: True,
                )

        if tracker.returnnlp.answer_positivity is Positivity.pos:
            if self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond(f"Nice.")

            qr_events = handle_question()
            if qr_events:
                return qr_events
            return [NextState(states.GameCannedAnimalCrossingHowsYourVillageProposing.name, jump=True)]

        else:
            if self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond_template(f"ack/i_see", {})

            qr_events = handle_question()
            if qr_events:
                return qr_events
            return [NextState(states.GameCannedAnimalCrossingYouGettingGameProposing.name, jump=True)]


class GameCannedAnimalCrossingHowsYourVillageProposing(GameState):

    name = 'game_canned_<animal_crossing>_hows_your_village_proposing'

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        dispatcher.respond_template(f"{selector_root}/hows_your_village/proposing", {})
        states.GameCannedAnimalCrossingHowsYourVillageRespond.require_entity(dispatcher, tracker)
        return [NextState(states.GameCannedAnimalCrossingHowsYourVillageRespond.name)]


class GameCannedAnimalCrossingHowsYourVillageRespond(GameState):

    name = 'game_canned_<animal_crossing>_hows_your_village_respond'

    @classmethod
    def require_entity(cls, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.expect_opinion(True)

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:

        def handle_question():
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                    skip_blender_response=lambda qr: True,
                )

        if self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
        elif tracker.returnnlp.opinion_positivity is Positivity.pos:
            dispatcher.respond_template(f"ack/cool", {})
        else:
            dispatcher.respond_template(f"ack/i_see", {})

        qr_events = handle_question()
        if qr_events:
            return qr_events

        dispatcher.respond_template(f"{selector_root}/hows_your_village/respond", {})
        states.GameCannedAnimalCrossingUserHasHouseUpgradeAcknowledge.require_entity(dispatcher, tracker)
        return [NextState(states.GameCannedAnimalCrossingUserHasHouseUpgradeAcknowledge.name)]


class GameCannedAnimalCrossingYouGettingGameProposing(GameState):

    name = 'game_canned_<animal_crossing>_you_getting_game_proposing'

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        dispatcher.respond_template(f"{selector_root}/you_getting_game/proposing", {})
        return [NextState(states.GameCannedAnimalCrossingYouGettingGameRespond.name)]


class GameCannedAnimalCrossingYouGettingGameRespond(GameState):

    name = 'game_canned_<animal_crossing>_you_getting_game_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:

        def handle_question():
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: (
                        dispatcher.respond_template(f"{selector_root}/you_getting_game/respond/ask_back", {})),
                    follow_up_handler=lambda qr: None
                )

        if (
            tracker.returnnlp.answer_positivity is Positivity.pos or
            re.search(r"\b((plan(ning)?|want(ing)?) to)\b", tracker.input_text)
        ):
            if self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond_template(f"ack/cool", {})
            dispatcher.respond_template(f"{selector_root}/you_getting_game/respond/pos", {})

        elif self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
        else:
            dispatcher.respond_template('ack/i_see', {})

        qr_events = handle_question()
        if qr_events:
            return qr_events

        return [NextState(states.GameCannedResponseExit.name, jump=True)]


class GameCannedAnimalCrossingUserHasHouseUpgradeAcknowledge(GameState):

    name = 'game_canned_<animal_crossing>_user_has_house_upgrade_acknowledge'

    @classmethod
    def require_entity(cls, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.expect_opinion(True)

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        """skipping sys ack for this state"""

        def handle_question(skip_blender_response: bool):
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: dispatcher.respond(f"I do have the upgrades!"),
                    follow_up_handler=lambda qr: None,
                    skip_blender_response=lambda qr: skip_blender_response,
                )

        skip_blender_response = False

        if self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
            if self.sys_ack_tag == 'ack_opinion' and self.sys_ack_utt:
                skip_blender_response = True

        else:
            key = tracker.returnnlp.answer_positivity.name
            dispatcher.respond_template(f"{selector_root}/user_has_house_upgrade/ack/{key}", {})

        qr_events = handle_question(skip_blender_response)
        if qr_events:
            return qr_events
        return [NextState(states.GameCannedAnimalCrossingMyHouseUpgradeProposing.name, jump=True)]


class GameCannedAnimalCrossingMyHouseUpgradeProposing(GameState):

    name = 'game_canned_<animal_crossing>_my_house_upgrade_proposing'

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        dispatcher.respond_template(f"{selector_root}/my_house_upgrade/proposing", {})
        states.GameCannedAnimalCrossingMyHouseUpgradeRespond.require_entity(dispatcher, tracker)
        return [NextState(states.GameCannedAnimalCrossingMyHouseUpgradeRespond.name)]


class GameCannedAnimalCrossingMyHouseUpgradeRespond(GameState):

    name = 'game_canned_<animal_crossing>_my_house_upgrade_respond'

    @classmethod
    def require_entity(cls, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.expect_opinion(True)

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        def handle_question(skip_blender_response: bool):
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: dispatcher.respond(f"I have tried it, it's easiest way to get money!"),
                    follow_up_handler=lambda qr: None,
                    skip_blender_response=lambda qr: skip_blender_response,
                )

        skip_blender_response = False

        if self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
            if self.sys_ack_tag == 'ack_opinion':
                skip_blender_response = True

        else:
            key = tracker.returnnlp.answer_positivity.name
            dispatcher.respond_template(f"{selector_root}/my_house_upgrade/respond/{key}", {})

        qr_events = handle_question(skip_blender_response)
        if qr_events:
            return qr_events
        return [NextState(states.GameCannedAnimalCrossingMultiplayerProposing.name, jump=True)]


class GameCannedAnimalCrossingMultiplayerProposing(GameState):

    name = 'game_canned_<animal_crossing>_multiplayer_proposing'

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        dispatcher.respond_template(f"{selector_root}/multiplayer/proposing", {})
        return [NextState(states.GameCannedAnimalCrossingMultiplayerRespond.name)]


class GameCannedAnimalCrossingMultiplayerRespond(GameState):

    name = 'game_canned_<animal_crossing>_multiplayer_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        def handle_question():
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: (
                        dispatcher.respond(f"I haven't have the chance to play with others yet.")),
                    follow_up_handler=lambda qr: None,
                )

        if tracker.returnnlp.answer_positivity is Positivity.pos:
            if self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond_template(f"ack/cool", {})
            dispatcher.respond_template(f"{selector_root}/multiplayer/respond/big_on_multiplayer", {})
        elif self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
        else:
            dispatcher.respond_template(f"ack/i_see", {})

        qr_events = handle_question()
        if qr_events:
            return qr_events
        return [NextState(states.GameCannedAnimalCrossingNookMilesTicketProposing.name, jump=True)]


class GameCannedAnimalCrossingNookMilesTicketProposing(GameState):

    name = 'game_canned_<animal_crossing>_nook_miles_ticket_proposing'

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        dispatcher.respond_template(f"{selector_root}/nook_miles_ticket/proposing", {})
        return [NextState(states.GameCannedAnimalCrossingNookMilesTicketRespond.name)]


class GameCannedAnimalCrossingNookMilesTicketRespond(GameState):

    name = 'game_canned_<animal_crossing>_nook_miles_ticket_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        def handle_question():
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: (
                        dispatcher.respond(f"Yes I have tried to use it")),
                    follow_up_handler=lambda qr: None,
                )
        if self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
        else:
            dispatcher.respond("Cool!")

        qr_events = handle_question()
        if qr_events:
            return qr_events
        return [NextState(states.GameCannedResponseExit.name, jump=True)]
