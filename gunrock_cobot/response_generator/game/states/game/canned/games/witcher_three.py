from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.game import states
from response_generator.game.states.base import GameState
from response_generator.game.utils import entity


selector_root = 'game_canned_response/witcher_3'


class GameCannedWitcherThree(GameState):

    name = "game_canned_<witcher_3>"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        hist = states.qa_path_hist(tracker)
        if hist and states.QaWitcherThreeEnter.name in hist:
            """if already proposed"""
            states.TalkedAboutEntityAlreadyRespond.require_entity(
                tracker,
                game=entity.GameItem.from_regex_match("witcher three", 'witcher_3'),
                console=None,
            )
            return [NextState(states.TalkedAboutEntityAlreadyRespond.name, jump=True)]

        else:
            dispatcher.respond_template(f"{selector_root}/transition_from_game_discussion/default", {})
            dispatcher.respond_template(f"{selector_root}/transition_from_game_discussion/recursion", {})
            return [NextState(states.QaWitcherThreeEnter.name, jump=True)]
