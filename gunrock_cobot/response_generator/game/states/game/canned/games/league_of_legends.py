import random
from typing import List

from nlu.constants import DialogAct
from response_generator.fsm2 import Dispatcher, Tracker, NextState
from response_generator.fsm2.events import Event

from response_generator.game.utils import entity
from response_generator.game.states.base import GameState
from response_generator.game import states


selector_root = "game_canned_response/league_of_legends"


class GameCannedLeagueOfLegends(GameState):

    name = "game_canned_<league_of_legends>"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        # TODO: get key to switch template?
        dispatcher.respond_template(f"{selector_root}/transition_from_game_discussion/default", {})
        return [NextState(states.GameCannedLeagueOfLegendsProposing.name, jump=True)]


class GameCannedLeagueOfLegendsProposing(GameState):

    name = "game_canned_<league_of_legends>_proposing"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:

        used_paths = tracker.persistent_store.get('game_canned_<minecraft>_used_path') or []
        paths, weights = list(zip(*(path for path in (
            ('favorite_champion', 0.95),
            ('favorite_map', 0.05)
        ) if path[0] not in used_paths))) or ([], [])

        if not (paths and weights):
            # ran out of paths
            return [NextState(states.GameCannedResponseExit.name, jump=True)]

        prev_state = self.module.state_tracker.curr_turn().prev_state()
        if (
            isinstance(prev_state, str) and
            prev_state.startswith("game_canned_<league_of_legends>") and
            prev_state != states.GameCannedLeagueOfLegends.name
        ):
            dispatcher.respond_template(f"{selector_root}/transition_from_game_discussion/recursion", {})

        chosen_path = random.choices(paths, weights=weights, k=1)[0]
        tracker.persistent_store['game_canned_<minecraft>_used_path'] = [*used_paths, chosen_path]
        return [NextState(f"game_canned_<league_of_legends>_{chosen_path}_proposing", jump=True)]


# MARK: - First Level

class GameCannedLeagueOfLegendsFavoriteChampionProposing(GameState):

    name = "game_canned_<league_of_legends>_favorite_champion_proposing"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        put_most_time_into_flag = tracker.volatile_store.get('put_most_time_into_flag', False)
        if put_most_time_into_flag:
            tracker.one_turn_store['put_most_time_into_flag'] = True
            dispatcher.respond_template(f"{selector_root}/favorite_champion/proposing/put_most_time_into", {})
        else:
            dispatcher.respond_template(f"{selector_root}/favorite_champion/proposing/fav_champion", {})

        return [NextState(states.GameCannedLeagueOfLegendsFavoriteChampionRespond.name)]


class GameCannedLeagueOfLegendsFavoriteChampionRespond(GameState):

    name = "game_canned_<league_of_legends>_favorite_champion_respond"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        champion: entity.LoLChampionItem = next(iter(entity.LoLChampionItem.detect(tracker)), None)
        lol_role: entity.LoLRoleItem = next(iter(entity.LoLRoleItem.detect(tracker)), None)
        put_most_time_into_flag = (
            tracker.volatile_store.get('put_most_time_into_flag', False) or
            tracker.one_turn_store.get('put_most_time_into_flag', False) or
            tracker.last_utt_store.get('put_most_time_into_flag', False)
        )
        reprompt_counter = (
            tracker.persistent_store.get('<league_of_legends>_favorite_champion_respond_reprompt') or 0
        )

        def handle_question(skip_ask_back=False):

            def ask_back_handler(qr):
                if not skip_ask_back:
                    dispatcher.respond_template(f"{selector_root}/favorite_champion/respond/ask_back", {})
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=ask_back_handler,
                    follow_up_handler=lambda qr: None,
                )

        if champion:
            dispatcher.respond_template(
                f"{selector_root}/favorite_champion/respond/with_name", dict(champion_name=champion.noun))
            qr_events = handle_question(skip_ask_back=True)
            if qr_events:
                return qr_events
            dispatcher.respond_template(f"{selector_root}/favorite_champion/epilogue/ack", dict())
            return [NextState(states.GameCannedLeagueOfLegendsChampionInFairSpotProposing.name, jump=True)]

        elif lol_role and not put_most_time_into_flag:
            return [NextState(states.GameCannedLeagueOfLegendsFavoriteRoleRespond.name, jump=True)]

        elif tracker.returnnlp.has_dialog_act({DialogAct.OTHER_ANSWERS}):
            dispatcher.respond_template(f"ack/i_see", {})
            qr_events = handle_question(skip_ask_back=True)
            if qr_events:
                return qr_events
            dispatcher.respond_template(f"{selector_root}/favorite_champion/epilogue/ack", dict())
            return [NextState(
                states.GameCannedLeagueOfLegendsProposing.name, jump=True)]

        elif reprompt_counter < 1:
            tracker.persistent_store['<league_of_legends>_favorite_champion_respond_reprompt'] = reprompt_counter + 1
            if not self.handler.has_question(tracker):
                dispatcher.respond_template(f"{selector_root}/favorite_champion/exception/reprompt", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events
            tracker.volatile_store['put_most_time_into_flag'] = put_most_time_into_flag
            return [NextState(states.GameCannedLeagueOfLegendsFavoriteChampionProposing.name, jump=True)]

        else:
            if self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond("That's an interesting thought.")
            qr_events = handle_question(skip_ask_back=True)
            if qr_events:
                return qr_events
            dispatcher.respond_template(f"{selector_root}/favorite_champion/epilogue/ack", dict())
            return [NextState(states.GameCannedLeagueOfLegendsProposing.name, jump=True)]


class GameCannedLeagueOfLegendsFavoriteRoleProposing(GameState):

    name = "game_canned_<league_of_legends>_favorite_role_proposing"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        return [NextState(states.ErrorHandlingEnter.name, jump=True)]


class GameCannedLeagueOfLegendsFavoriteRoleRespond(GameState):

    name = "game_canned_<league_of_legends>_favorite_role_respond"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        lol_role: entity.LoLRoleItem = next(iter(entity.LoLRoleItem.detect(tracker)), None)
        if not lol_role:
            self.logger.warning(f"missing role: {lol_role}")

        def handle_question():
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                )

        if lol_role:
            dispatcher.respond_template(f"ack/cool", {})
        if self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
        else:
            dispatcher.respond_template(f"ack/i_see", {})

        qr_events = handle_question()
        if qr_events:
            return qr_events

        dispatcher.respond_template(
            f"{selector_root}/favorite_role/respond/with_role",
            dict(role=lol_role.noun if lol_role else ""))

        tracker.volatile_store['put_most_time_into_flag'] = True

        return [NextState(states.GameCannedLeagueOfLegendsFavoriteChampionProposing.name, jump=True)]


class GameCannedLeagueOfLegendsFavoriteMapProposing(GameState):

    name = "game_canned_<league_of_legends>_favorite_map_proposing"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        dispatcher.respond_template(f"{selector_root}/favorite_map/proposing/default", {})
        return [NextState(states.GameCannedLeagueOfLegendsFavoriteMapRespond.name)]


class GameCannedLeagueOfLegendsFavoriteMapRespond(GameState):

    name = "game_canned_<league_of_legends>_favorite_map_respond"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        lol_map: entity.LoLMapItem = next(iter(entity.LoLMapItem.detect(tracker)), None)

        reprompt_counter = (
            tracker.persistent_store.get('<league_of_legends>_favorite_map_respond_reprompt') or 0
        )

        def handle_question():
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                )

        if lol_map and lol_map.canonical == 'summoners_rift':
            dispatcher.respond_template(f"{selector_root}/favorite_map/respond/summoners_rift", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events
            tracker.one_turn_store['map_to_discuss'] = lol_map
            return [NextState(states.GameCannedLeagueOfLegendsChampionLikeMapReasonProposing.name, jump=True)]

        elif lol_map:
            dispatcher.respond_template(f"{selector_root}/favorite_map/respond/with_map", dict(map=lol_map.noun))
            qr_events = handle_question()
            if qr_events:
                return qr_events
            tracker.one_turn_store['map_to_discuss'] = lol_map
            return [NextState(
                states.GameCannedLeagueOfLegendsChampionLikeMapReasonProposing.name, jump=True)]

        elif tracker.returnnlp.has_dialog_act({DialogAct.OTHER_ANSWERS}):
            dispatcher.respond_template(f"{selector_root}/favorite_map/respond/dunno", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events
            return [NextState(states.GameCannedLeagueOfLegendsProposing.name, jump=True)]

        elif reprompt_counter < 1:
            tracker.persistent_store['<league_of_legends>_favorite_map_respond_reprompt'] = reprompt_counter + 1
            qr_events = handle_question()
            if qr_events:
                return qr_events
            dispatcher.respond_template(f"{selector_root}/favorite_map/respond/exception/reprompt", {})
            return [NextState(self.name)]

        else:
            if self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond("That's an interesting thought.")
            qr_events = handle_question()
            if qr_events:
                return qr_events
            return [NextState(states.GameCannedLeagueOfLegendsProposing.name, jump=True)]

# MARK: - Second Level


class GameCannedLeagueOfLegendsChampionInFairSpotProposing(GameState):

    name = "game_canned_<league_of_legends>_champion_in_fair_spot_proposing"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        champion: entity.LoLChampionItem = next(iter(entity.LoLChampionItem.detect(tracker)), None)
        if not champion:
            self.logger.warning(f"missing champion: {champion}")

        tracker.one_turn_store['champion_to_discuss'] = champion
        dispatcher.respond_template(
            f"{selector_root}/champion_in_fair_spot/proposing",
            dict(champion_name=champion.noun if champion else "the champion"))

        states.GameCannedLeagueOfLegendsChampionInFairSpotRespond.require_entity(dispatcher, tracker)
        return [NextState(states.GameCannedLeagueOfLegendsChampionInFairSpotRespond.name)]


class GameCannedLeagueOfLegendsChampionInFairSpotRespond(GameState):

    name = "game_canned_<league_of_legends>_champion_in_fair_spot_respond"

    @classmethod
    def require_entity(cls, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.expect_opinion(True)

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        champion: entity.LoLChampionItem = (
            tracker.last_utt_store.get('champion_to_discuss') or
            next(iter(entity.LoLChampionItem.detect(tracker)), None)
        )

        def handle_question(skip_blender_response: bool):
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: dispatcher.respond("I like to think they are."),
                    follow_up_handler=lambda qr: None,
                    skip_blender_response=lambda qr: skip_blender_response,
                )

        key = (
            'dunno' if tracker.returnnlp.has_dialog_act(DialogAct.OTHER_ANSWERS) else
            'pos' if tracker.returnnlp.answer_positivity == 'pos' else
            'neg' if tracker.returnnlp.answer_positivity == 'neg' else
            None
        )
        skip_blender_response = False

        if self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
            if self.sys_ack_tag == 'ack_opinion':
                skip_blender_response = False

        elif not key:
            dispatcher.respond_template(f"ack/i_see", {})

        else:
            dispatcher.respond_template(
                f"{selector_root}/champion_in_fair_spot/respond/{key}",
                dict(champion_name=champion.noun if champion else "that champion"))

        qr_events = handle_question(skip_blender_response)
        if qr_events:
            return qr_events

        return [NextState(states.GameCannedLeagueOfLegendsProposing.name, jump=True)]


class GameCannedLeagueOfLegendsChampionLikeMapReasonProposing(GameState):

    name = "game_canned_<league_of_legends>_champion_like_map_reason_proposing"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        dispatcher.respond_template(f"{selector_root}/like_map_reason/proposing", dict())
        states.GameCannedLeagueOfLegendsChampionLikeMapReasonRespond.require_entity(dispatcher, tracker)
        return [NextState(states.GameCannedLeagueOfLegendsChampionLikeMapReasonRespond.name)]


class GameCannedLeagueOfLegendsChampionLikeMapReasonRespond(GameState):

    name = "game_canned_<league_of_legends>_champion_like_map_reason_respond"

    @classmethod
    def require_entity(cls, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.expect_opinion(True)

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        lol_map: entity.LoLMapItem = (
            tracker.one_turn_store.get('map_to_discuss') or
            tracker.last_utt_store.get('map_to_discuss') or
            None
        )

        def handle_question(skip_blender_response: bool):
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                    skip_blender_response=lambda qr: skip_blender_response,
                )

        if tracker.returnnlp.has_dialog_act({DialogAct.OTHER_ANSWERS}):
            key = 'dunno'
        elif len(tracker.input_text.split(' ')) > 3:
            key = 'with_reason'
        else:
            key = None

        skip_blender_response = False

        if self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
            if self.sys_ack_tag == 'ack_opinion':
                skip_blender_response = True

        elif not key:
            dispatcher.respond_template(f"ack/i_see", {})

        else:
            dispatcher.respond_template(
                f"{selector_root}/like_map_reason/respond/{key}",
                dict(map=lol_map.noun if lol_map else "the map"))

        qr_events = handle_question(skip_blender_response)
        if qr_events:
            return qr_events

        return [NextState(states.GameCannedLeagueOfLegendsProposing.name, jump=True)]
