import random
import re

import utils as sys_utils
from nlu.constants import DialogAct
from nlu.constants import Positivity
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.game import states
from response_generator.game.states.base import GameState
from response_generator.game.utils import regex_match


selector_root = 'game_canned_response/minecraft'


class GameCannedMinecraft(GameState):

    name = "game_canned_<minecraft>"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/transition_from_game_discussion/default", {})
        dispatcher.respond_template(f"{selector_root}/transition_from_game_discussion/recursion", {})
        return [NextState(states.GameCannedMinecraftProposing.name, jump=True)]


class GameCannedMinecraftProposing(GameState):

    name = "game_canned_<minecraft>_proposing"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        used_paths = tracker.persistent_store.get('game_canned_<minecraft>_used_path') or []
        paths, weights = list(zip(*(path for path in (
            ('singleplayer', 0.3),
            ('ender_dragon', 0.3),
            ('game_mode', 0.3),
            # ('why_soothing', 0.1)
        ) if path[0] not in used_paths))) or ([], [])
        if not (paths and weights):
            # ran out of paths
            return [NextState(states.GameCannedResponseExit.name, jump=True)]

        prev_state = self.module.state_tracker.curr_turn().prev_state()
        if (
            isinstance(prev_state, str) and
            prev_state.startswith("game_canned_<minecraft>") and
            prev_state != states.GameCannedMinecraft.name
        ):
            dispatcher.respond_template(
                f"{selector_root}/transition_from_game_discussion/recursion", {})

        if sys_utils.get_working_environment() == 'local':
            chosen_path = paths[0]
        else:
            chosen_path = random.choices(paths, weights=weights, k=1)[0]
        tracker.persistent_store['game_canned_<minecraft>_used_path'] = [*used_paths, chosen_path]
        return [NextState(f"game_canned_<minecraft>_{chosen_path}_proposing", jump=True)]


# MARK: - First Level

class GameCannedMinecraftSingleplayerProposing(GameState):

    name = "game_canned_<minecraft>_singleplayer_proposing"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/singleplayer/proposing", {})
        states.GameCannedMinecraftSingleplayerRespond.require_entity(dispatcher, tracker)
        return [NextState(states.GameCannedMinecraftSingleplayerRespond.name)]


class GameCannedMinecraftSingleplayerRespond(GameState):

    name = "game_canned_<minecraft>_singleplayer_respond"

    @classmethod
    def require_entity(cls, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.expect_opinion(True)

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        def handle_question(skip_blender_response: bool):
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                    skip_blender_response=lambda qr: skip_blender_response,
                )

        skip_blender_response = False
        if self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
            if self.sys_ack_tag == 'ack_opinion':
                skip_blender_response = True
        else:
            dispatcher.respond_template(f"{selector_root}/singleplayer/respond/default", {})

        qr_events = handle_question(skip_blender_response)
        if qr_events:
            return qr_events
        if regex_match.is_playing_alone(tracker):
            return [NextState(states.GameCannedMinecraftDreamHomeProposing.name, jump=True)]
        else:
            # TODO: more check
            return [NextState(states.GameCannedMinecraftGriefingProposing.name, jump=True)]


class GameCannedMinecraftEnderDragonProposing(GameState):

    name = "game_canned_<minecraft>_ender_dragon_proposing"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/defeat_ender_dragon/proposing", {})
        return [NextState(states.GameCannedMinecraftEnderDragonRespond.name)]


class GameCannedMinecraftEnderDragonRespond(GameState):

    name = "game_canned_<minecraft>_ender_dragon_respond"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        def handle_question():
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: dispatcher.respond(f"Unfortunately I have not."),
                    follow_up_handler=lambda qr: None,
                )

        if tracker.returnnlp.answer_positivity is Positivity.pos:
            key = 'pos'
        else:
            key = 'neg'

        if self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
        else:
            dispatcher.respond_template(f"{selector_root}/defeat_ender_dragon/respond/{key}", {})
        qr_events = handle_question()
        if qr_events:
            return qr_events
        return [NextState(states.GameCannedMinecraftProposing.name, jump=True)]


class GameCannedMinecraftGameModeProposing(GameState):

    name = "game_canned_<minecraft>_game_mode_proposing"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/game_mode/proposing", {})
        return [NextState(states.GameCannedMinecraftGameModeRespond.name)]


class GameCannedMinecraftGameModeRespond(GameState):

    name = "game_canned_<minecraft>_game_mode_respond"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        def handle_question():
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                )

        if re.search(r"\b(survival)\b", tracker.input_text):
            selector = f"{selector_root}/game_mode/respond/survival"
        elif re.search(r"\b(creative)\b", tracker.input_text):
            selector = f"{selector_root}/game_mode/respond/creative"
        elif tracker.returnnlp.has_dialog_act({DialogAct.OTHER_ANSWERS}):
            # TODO: better detection
            selector = f"{selector_root}/game_mode/respond/depends"
        else:
            # TODO: better ack
            if self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond_template("ack/i_see", {})
            selector = None

        if selector:
            dispatcher.respond_template(selector, {})

        qr_events = handle_question()
        if qr_events:
            return qr_events
        return [NextState(states.GameCannedMinecraftProposing.name, jump=True)]


class GameCannedMinecraftWhySoothingProposing(GameState):

    name = "game_canned_<minecraft>_why_soothing_proposing"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/why_soothing/proposing", {})
        states.GameCannedMinecraftWhySoothingRespond.require_entity(dispatcher, tracker)
        return [NextState(states.GameCannedMinecraftWhySoothingRespond.name)]


class GameCannedMinecraftWhySoothingRespond(GameState):

    name = "game_canned_<minecraft>_why_soothing_respond"

    @classmethod
    def require_entity(cls, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.expect_opinion(True)

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        def handle_question(skip_blender_response: bool, skip_ask_back=False):
            def ask_back_handler(qr):
                dispatcher.respond(f"I believe it's the soundtrack. It's really chill and suits the vibe of the game.")
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=ask_back_handler,
                    follow_up_handler=lambda qr: None,
                    skip_blender_response=lambda qr: skip_blender_response,
                )

        if self.sys_ack_tag == 'ack_opinion' and self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
            qr_events = handle_question(skip_blender_response=True)
            if qr_events:
                return qr_events
        if re.search(r"\b(music|sountrack|songs?)", tracker.input_text):
            # TODO: not use regex like this
            selector = f"{selector_root}/why_soothing/respond/music"
            dispatcher.respond_template(selector, {})
            qr_events = handle_question(skip_ask_back=True)
            if qr_events:
                return qr_events
        elif self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
            qr_events = handle_question()
            if qr_events:
                return qr_events
        else:
            selector = f"{selector_root}/why_soothing/respond/default"
            dispatcher.respond_template(selector, {})
            qr_events = handle_question(skip_ask_back=True)
            if qr_events:
                return qr_events

        return [NextState(states.GameCannedMinecraftProposing.name, jump=True)]


# MARK: - Second Level

class GameCannedMinecraftDreamHomeProposing(GameState):

    name = "game_canned_<minecraft>_dream_home_proposing"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/dream_home/proposing", {})
        return [NextState(states.GameCannedMinecraftDreamHomeRespond.name)]


class GameCannedMinecraftDreamHomeRespond(GameState):

    name = "game_canned_<minecraft>_dream_home_respond"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        def handle_question():
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: dispatcher.respond_template(f"{selector_root}/dream_home/respond/")
                )

        if tracker.returnnlp.answer_positivity is Positivity.pos:
            dispatcher.respond_template(f"{selector_root}/dream_home/respond/pos", {})

        elif (
            tracker.returnnlp.answer_positivity is Positivity.neg and
            len(tracker.returnnlp) > 1 and
            tracker.returnnlp[1:].has_dialog_act({DialogAct.OPINION, DialogAct.COMMENT})
        ):
            dispatcher.respond_template(f"{selector_root}/dream_home/respond/neg_but", {})

        elif tracker.returnnlp.answer_positivity is Positivity.neg:
            dispatcher.respond_template(f"{selector_root}/dream_home/respond/neg", {})

        elif self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)

        else:
            dispatcher.respond_template(f"ack/i_see", {})

        qr_events = handle_question()
        if qr_events:
            return qr_events

        return [NextState(states.GameCannedMinecraftProposing.name, jump=True)]


class GameCannedMinecraftGriefingProposing(GameState):

    name = "game_canned_<minecraft>_griefing_proposing"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/mess_with_each_other/proposing", {})
        return [NextState(states.GameCannedMinecraftGriefingRespond.name)]


class GameCannedMinecraftGriefingRespond(GameState):

    name = "game_canned_<minecraft>_griefing_respond"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        def handle_question(skip_ask_back=False):
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                )
        if tracker.returnnlp.answer_positivity is Positivity.neg:
            if self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond_template(f"ack/i_see", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events
            return [NextState(states.GameCannedMinecraftThoughtsOnGriefingProposing.name, jump=True)]

        elif tracker.returnnlp.answer_positivity is Positivity.pos:
            dispatcher.respond_template(f"{selector_root}/mess_with_each_other/respond/pos", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events
            return [NextState(states.GameCannedMinecraftThoughtsOnGriefingProposing.name, jump=True)]

        else:
            if self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond_template(f"ack/i_see", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events
            return [NextState(states.GameCannedMinecraftProposing.name, jump=True)]


# MARK: - Third Level

class GameCannedMinecraftThoughtsOnGriefingProposing(GameState):

    name = "game_canned_<minecraft>_thoughts_on_griefing_proposing"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.respond_template(f"{selector_root}/friend_grieving/proposing", {})
        states.GameCannedMinecraftThoughtsOnGriefingRespond.require_entity(dispatcher, tracker)
        return [NextState(states.GameCannedMinecraftThoughtsOnGriefingRespond.name)]


class GameCannedMinecraftThoughtsOnGriefingRespond(GameState):

    name = "game_canned_<minecraft>_thoughts_on_griefing_respond"

    @classmethod
    def require_entity(cls, dispatcher: Dispatcher, tracker: Tracker):
        dispatcher.expect_opinion(True)

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        def handle_question(skip_blender_response=False, skip_ask_back=False):
            def ask_back_handler(qr):
                if not skip_ask_back:
                    dispatcher.respond(f"I find it bit funny, but I only do so if others are okay with it.")
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                    skip_blender_response=lambda qr: skip_blender_response,
                )

        if self.sys_ack_tag == 'ack_opinion' and self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
            qr_events = handle_question(skip_blender_response=True, skip_ask_back=True)
            if qr_events:
                return qr_events

        elif re.search(r"\b(funny|like it|(do(es)?(n'?t| not) (bother|care|mind)))\b", tracker.input_text):
            # TODO: not use regex like this
            selector = f"{selector_root}/friend_grieving/respond/funny"
            dispatcher.respond_template(selector, {})
            qr_events = handle_question(skip_ask_back=True)
            if qr_events:
                return qr_events
        elif re.search(
            r"\b(annoying|((do(n't| not|esn't|es not) (like|enjoy|do))|(hate|dislike)) ?(it|that|this)?)\b",
            tracker.input_text
        ):
            # TODO: more check
            selector = f"{selector_root}/friend_grieving/respond/annoying"
            dispatcher.respond_template(selector, {})
            qr_events = handle_question()
            if qr_events:
                return qr_events

        elif self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
            qr_events = handle_question()
            if qr_events:
                return qr_events

        else:
            dispatcher.respond_template(f"ack/i_see", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events

        return [NextState(states.GameCannedMinecraftProposing.name, jump=True)]
