import re
from typing import Optional

from nlu.constants import DialogAct, Positivity, TopicModule
from nlu.utils import topic_module as topic_module_utils
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.game import states
from response_generator.game.states.base import GameState
from response_generator.game.utils import entity


selector_root = "talk_about_game"
propose_limit = 3


class TalkAboutGameEnter(GameState):

    name = "talk_about_game_enter"

    @classmethod
    def counter(cls, tracker: Tracker):
        return tracker.persistent_store.get('talk_about_game_propose_counter') or 0

    @classmethod
    def increment_counter(cls, tracker: Tracker):
        flag = tracker.volatile_store.get('talk_about_game_propose_counter_flag') or False
        if not flag:
            tracker.persistent_store['talk_about_game_propose_counter'] = TalkAboutGameEnter.counter(tracker) + 1
            tracker.volatile_store['talk_about_game_propose_counter_flag'] = True

    @classmethod
    def accept_transition(cls, tracker: Tracker):
        """optional"""
        return cls.counter(tracker) < propose_limit

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if (
            TalkAboutGameEnter.counter(tracker) < min(propose_limit, 1)
        ):
            return [NextState(states.TalkAboutGamePropose.name, jump=True)]
        else:
            states.TalkAboutAnotherGamePropose.require_entity(tracker, None)
            return [NextState(states.TalkAboutAnotherGamePropose.name, jump=True)]


class TalkAboutGameExit(GameState):

    name = "talk_about_game_exit"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if states.TalkAboutGameEnter.accept_transition(tracker):
            return [NextState(states.TalkAboutGameEnter.name, jump=True)]
        else:
            states.SubtopicSwitchEnter.require_entity(tracker, responses=[], tag=[])
            return [NextState(states.SubtopicSwitchEnter.name, jump=True)]


class TalkAboutGamePropose(GameState):
    """
    propose "what game would you like to talk about?"
    """

    name = "talk_about_game_propose"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if any(
            i in {states.QaExit.name} and
            i not in {states.ResumeRespond.name}
            for i in self.module.state_tracker.curr_turn()
        ):
            dispatcher.respond_template(f"{selector_root}/transition/default", {})

        dispatcher.respond_template(f"{selector_root}/propose/default", {})
        states.TalkAboutGameEnter.increment_counter(tracker)
        states.TalkAboutGameRespond.require_entity(tracker, None)
        return [NextState(states.TalkAboutGameRespond.name)]


class TalkAboutGameRespond(GameState):

    name = "talk_about_game_respond"

    @classmethod
    def require_entity(cls, tracker: Tracker, game: Optional[entity.GameItem], i_have_heard_of_game_ack=True):
        """for when we already detect a game, we can come here with this function"""
        tracker.one_turn_store['game_to_discuss'] = game
        tracker.one_turn_store['i_have_heard_of_game_ack'] = i_have_heard_of_game_ack

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        # the func should avoid duplicate count. But this allow us to inc it if didn't come from proposes
        # if (
        #     self.module.state_tracker.prev_turn() and
        #     self.module.state_tracker.prev_turn()[-1] not in {states.TalkAboutGameRespond.name}
        # ):
        states.TalkAboutGameEnter.increment_counter(tracker)

        topic_modules = tracker.returnnlp.topic_intent_modules - {TopicModule.GAME}
        topic_module = next(iter(topic_modules), None)
        game: entity.GameItem = (
            self.game_to_discuss or
            next(iter(entity.GameItem.detect(tracker)), None)
        )
        console: entity.ConsoleItem = next(iter(entity.ConsoleItem.detect(tracker)), None)
        genre = next(iter(entity.GenreItem.detect(tracker)), None)

        genre_counter = tracker.last_utt_store.get(f'{self.name}_genre_counter') or 0
        positivity_counter = tracker.last_utt_store.get(f"{self.name}_positivity_counter") or 0

        cond = [
            re.search(r"ye(s|a|ah)$", tracker.input_text),
            positivity_counter >= 1,
            (self.sys_ack_tag, self.sys_ack_utt),
            game,
        ]

        self.logger.debug(f"talk about game cond = {cond}")

        def handle_question(skip_blender_response=False, ignore_idk_response=False):
            # if self.handler.has_command(tracker):
            #     self.handler.handle_command(dispatcher, tracker)

            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                    skip_blender_response=lambda qr: skip_blender_response,
                    ignore_idk_response=lambda qr: ignore_idk_response,
                )

        if game:
            self.user_profile.discussed_game_hist_append(game)
            self.logger.debug(
                f"append game: game = {game}"
                f'hist = { self.user_profile.discussed_game_hist }'
            )

        if game and states.GameCannedResponseEnter.accept_transition(tracker, game):
            qr_events = handle_question(ignore_idk_response=True)
            if qr_events:
                return qr_events
            states.GameCannedResponseEnter.require_entity(tracker, game)
            return [NextState(states.GameCannedResponseEnter.name, jump=True)]

        elif game and states.GameFunfactEnter.accept_transition(tracker, game):
            qr_events = handle_question(ignore_idk_response=True)
            if qr_events:
                return qr_events

            self.logger.debug(
                f"idk ack = {(self.handler.has_question(tracker, skip_flag_check=True))}, "
                f"idk ack = {(False if self.handler.has_question(tracker, skip_flag_check=True) else True)}, "
            )
            i_have_heard_of_game_ack = tracker.one_turn_store.get('i_have_heard_of_game_ack', True)

            states.GameFunfactEnter.require_entity(tracker, game, i_have_heard_of_game_ack=(
                False if self.handler.has_question(tracker, skip_flag_check=True) else i_have_heard_of_game_ack))
            return [NextState(states.GameFunfactEnter.name, jump=True)]

        elif console:
            qr_events = handle_question(ignore_idk_response=True)
            if qr_events:
                return qr_events
            states.TalkAboutConsoleRespond.require_entity(tracker, console)
            return [NextState(states.TalkAboutConsoleRespond.name, jump=True)]

        elif genre and genre_counter < 1:
            tracker.one_turn_store[f'{self.name}_genre_counter'] = genre_counter + 1
            states.TalkAboutGenreRespond.require_entity(tracker, genre)
            return [NextState(states.TalkAboutGenreRespond.name, jump=True)]

        elif genre and genre_counter >= 1:
            # TODO: go to my_fav_game
            states.TalkAboutRandomGamePropose.require_entity(tracker, prefix='False')
            return [NextState(states.TalkAboutRandomGamePropose.name, jump=True)]

        elif topic_module:
            qr_events = handle_question()
            if qr_events:
                return qr_events
            dispatcher.respond_template(
                f"exception/topic_transition/module", dict(module=topic_module_utils.module_utterable(topic_module)))
            dispatcher.propose_continue('UNCLEAR', topic_module)
            return [NextState(states.ProposeUnclearRespond.name)]

        elif re.search(r"\b((which|what) games?)\b", tracker.input_text, re.I):
            self.logger.debug(f"got regex = 'which|what games', going to 'game_discussion_random_game'")
            qr_events = handle_question(skip_blender_response=True)
            if qr_events:
                return qr_events
            if self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond_template(f"{selector_root}/propose_another/which_game_ack", {})
            states.TalkAboutRandomGamePropose.require_entity(tracker, prefix='False')
            return [NextState(states.TalkAboutRandomGamePropose.name, jump=True)]

        elif game:
            """if there is a game entity but we don't know anything about it"""
            qr_events = handle_question(skip_blender_response=True, ignore_idk_response=True)
            if qr_events:
                return qr_events
            states.DunnoGamePropose.require_entity(
                tracker, game, use_blender=self.handler.handler_used_blender(tracker))
            return [NextState(states.DunnoGamePropose.name, jump=True)]

        elif tracker.returnnlp.has_dialog_act(DialogAct.OTHER_ANSWERS):
            dispatcher.respond_template(f"ack/thats_alright", {})
            """let fall thru"""

        elif tracker.returnnlp.answer_positivity in {Positivity.pos, Positivity.neu} and positivity_counter < 1:
            tracker.one_turn_store[f"{self.name}_positivity_counter"] = positivity_counter + 1

            if self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond_template(f"ack/affirm", {})
                dispatcher.respond('.')
            qr_events = handle_question(skip_blender_response=True)
            if qr_events:
                return qr_events
            dispatcher.respond_template(f"{selector_root}/propose/default", {})
            return [NextState(self.name)]

        elif (
            tracker.returnnlp.answer_positivity is Positivity.neg or
            re.search(r"\b(no (more)? ?games?)\b", tracker.input_text)
        ):
            states.ExitTransitionEnter.require_entity(tracker, 'alright')
            return [NextState(states.ExitTransitionEnter.name, jump=True)]

        elif (
            re.search(r"ye(s|a|ah)$", tracker.input_text) and positivity_counter >= 1
        ):
            states.ExitTransitionEnter.require_entity(tracker, 'something_else')
            return [NextState(states.ExitTransitionEnter.name, jump=True)]

        """fall thru"""
        if states.TalkAboutGameEnter.counter(tracker) < propose_limit:
            # TODO: don't use random
            states.TalkAboutRandomGamePropose.require_entity(tracker, prefix='True')
            return [NextState(states.TalkAboutRandomGamePropose.name, jump=True)]
        else:
            return [NextState(states.TalkAboutGameExit.name, jump=True)]
