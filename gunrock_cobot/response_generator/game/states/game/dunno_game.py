import re

from nlu.constants import DialogAct as DialogActEnum
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.game.utils import entity
from response_generator.game.states.base import GameState
from response_generator.game import states


selector_root = "dunno_game"


class DunnoGamePropose(GameState):

    name = "dunno_game_propose"

    @classmethod
    def require_entity(cls, tracker: Tracker, game: entity.GameItem, use_blender=False):
        tracker.volatile_store['use_blender'] = use_blender
        tracker.one_turn_store['game_to_discuss'] = game

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if (tracker.volatile_store.get('use_blender') or False):
            dispatcher.respond_template(f"{selector_root}/propose/use_blender", {})
        else:
            dispatcher.respond_template(
                f"{selector_root}/propose/default",
                dict(game=self.game_to_discuss.noun if self.game_to_discuss else "this game")
            )
        states.DunnoGameRespond.require_entity(dispatcher, tracker, self.game_to_discuss)
        return [NextState(states.DunnoGameRespond.name)]


class DunnoGameRespond(GameState):

    name = "dunno_game_respond"

    @classmethod
    def require_entity(cls, dispatcher: Dispatcher, tracker: Tracker, game: entity.GameItem):
        dispatcher.expect_opinion(True)
        tracker.one_turn_store['game_to_discuss'] = game

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        custom_threshold = (0.1, 0.1)
        game = self.game_to_discuss
        tracker.one_turn_store['game_to_discuss'] = game
        dunno_game_recursion: int = (
            tracker.one_turn_store.get('dunno_game_recursion') or
            tracker.last_utt_store.get('dunno_game_recursion') or
            0
        )

        def handle_question(skip_blender_response=False, ignore_idk_response=False):
            # if self.handler.has_command(tracker):
            #     self.handler.handle_command(dispatcher, tracker)

            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                    skip_blender_response=lambda qr: skip_blender_response,
                    ignore_idk_response=lambda qr: ignore_idk_response,
                )

        switch_game = (r"((talk about|switch to( talking about)?) (some )?(((an)?other)|(a )?different) games?)")

        if re.search(switch_game, tracker.input_text):
            # TODO: not using random?
            # states.TalkAboutRandomGamePropose.require_entity(tracker, prefix='True')
            # return [NextState(states.TalkAboutRandomGamePropose.name, jump=True)]
            tracker.one_turn_store['game_to_disucss'] = None
            return [NextState(states.TalkAboutGamePropose.name, jump=True)]

        elif tracker.returnnlp.has_dialog_act({DialogActEnum.NEG_ANSWER}):
            """user should be telling us about the game"""
            dispatcher.respond_template(f"{selector_root}/respond/decline", {})
            qr_events = handle_question(skip_blender_response=True, ignore_idk_response=True)
            if qr_events:
                return qr_events
            states.TalkAboutAnotherGamePropose.require_entity(tracker, None)
            return [NextState(states.TalkAboutAnotherGamePropose.name, jump=True)]

        elif tracker.returnnlp.has_dialog_act({
            DialogActEnum.STATEMENT,
            DialogActEnum.COMMENT,
            DialogActEnum.OPINION,
        }, threshold=custom_threshold):
            # if user is describing the game:
            is_statement_only = (
                tracker.returnnlp.has_dialog_act({DialogActEnum.STATEMENT}, threshold=custom_threshold) and
                not tracker.returnnlp.has_dialog_act({
                    DialogActEnum.COMMENT,
                    DialogActEnum.OPINION
                }, threshold=custom_threshold)
            )

            self.logger.debug(
                f"dunno_game_recurssion: {dunno_game_recursion < 1}, "
                f"{tracker.input_text.split()}, "
                f"{len(tracker.input_text.split()) < 7}")
            if (dunno_game_recursion < 1 and len(tracker.input_text.split()) < 7):
                """if we do recurse, and heuristically only ask again if response is short"""
                tracker.one_turn_store['dunno_game_recursion'] = dunno_game_recursion + 1

                self.logger.debug(f"gonna recurse")
                if self.sys_ack_utt:
                    dispatcher.respond(self.sys_ack_utt)
                else:
                    dispatcher.respond_template(f"{selector_root}/respond/recursion/ack", {})

                qr_events = handle_question(skip_blender_response=True, ignore_idk_response=True)
                if qr_events:
                    return qr_events

                dispatcher.respond_template((
                    f"{selector_root}/respond/recursion/question/"
                    f"{'elaborate' if is_statement_only else 'tell_me_more'}"
                ), {})
                DunnoGameRespond.require_entity(dispatcher, tracker, game)
                return [NextState(self.name)]
            else:
                self.logger.debug(f"not gonna recurse")
                if self.sys_ack_utt:
                    dispatcher.respond(self.sys_ack_utt)
                    dispatcher.respond_template(
                        f"{selector_root}/respond/default_no_ack", dict(game=game.noun if game else "the game"))
                else:
                    dispatcher.respond_template(
                        f"{selector_root}/respond/default", dict(game=game.noun if game else "the game"))

                qr_events = handle_question(skip_blender_response=True, ignore_idk_response=True)
                if qr_events:
                    return qr_events

                return [NextState(states.TalkAboutGameExit.name, jump=True)]

        else:
            if self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond_template(
                    f"{selector_root}/respond/irrelavant", dict(game=game.noun if game else "the game"))

            qr_events = handle_question(skip_blender_response=True, ignore_idk_response=True)
            if qr_events:
                return qr_events

            return [NextState(states.TalkAboutGameExit.name, jump=True)]
