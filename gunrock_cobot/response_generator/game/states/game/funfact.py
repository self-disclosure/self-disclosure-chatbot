from typing import Optional

from nlu.constants import Positivity, TopicModule

from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.game import states
from response_generator.game.states.base import GameState
from response_generator.game.utils import funfact, entity


selector_root = "game_funfact"
# funfact_db = "funfact_db/game"


class GameFunfactEnter(GameState):

    name = "game_funfact_enter"

    @classmethod
    def accept_transition(cls, tracker: Tracker, game: entity.GameItem):
        return funfact.has_hardcode_funfacts(game)

    @classmethod
    def require_entity(
        cls, tracker: Tracker, game: entity.GameItem, *,
        i_have_heard_of_game_ack=True
    ):
        tracker.one_turn_store['game_to_discuss'] = game
        tracker.one_turn_store['i_have_heard_of_game_ack'] = i_have_heard_of_game_ack

    def setup(self, dispatcher: Dispatcher, tracker: Tracker):
        super().setup(dispatcher, tracker)
        self.i_have_heard_of_game_ack = tracker.one_turn_store.get('i_have_heard_of_game_ack') or False

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        """"""

        """error handling"""
        if not self.game_to_discuss or not funfact.has_hardcode_funfacts(self.game_to_discuss):
            return [NextState(states.ErrorHandlingEnter.name, jump=True)]

        """count the remaining funfacts. If run out, gracefully exit"""

        if states.GameFunfactPropose.still_has_funfact(tracker, dispatcher, self.game_to_discuss):
            fact = funfact.get_random_funfact(tracker, self.game_to_discuss, save_hist=True)
            if (
                self.i_have_heard_of_game_ack and
                states.GameFunfactPropose.funfact_counter(tracker, self.game_to_discuss) <= 0
            ):
                if not (
                    any(i in {states.Initial.name} for i in self.module.state_tracker.curr_turn())
                ):
                    """no need ack as Initial will say "oh sure"."""
                    """also no need ack if user talked about this game before"""
                    if fact.oh_i_have_heard_ack is not None:
                        dispatcher.respond(fact.oh_i_have_heard_ack.format(game=self.game_to_discuss.noun))
                    else:
                        dispatcher.respond_template(f"{selector_root}/propose/ive_heard_of_game_ack", dict())
                if fact.oh_i_have_heard is not None:
                    dispatcher.respond(fact.oh_i_have_heard.format(game=self.game_to_discuss.noun))
                else:
                    dispatcher.respond_template(
                        f"{selector_root}/propose/ive_heard_of_game", dict(game=self.game_to_discuss.noun))
            states.GameFunfactPropose.require_entity(tracker, self.game_to_discuss, fact)
            return [NextState(states.GameFunfactPropose.name, jump=True)]
        else:
            # TODO: where to go when running out of funfacts?
            dispatcher.respond_template(
                f"{selector_root}/transition/out_of_funfact", dict(game=self.game_to_discuss.noun))

            states.SubtopicSwitchEnter.require_entity(tracker, [], [], exit_transition_enter_flag="None")
            return [NextState(states.SubtopicSwitchEnter.name, jump=True)]


class GameFunfactExit(GameState):

    name = "game_funfact_exit"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        tracker.one_turn_store['game_to_discuss'] = None
        tracker.last_utt_store['game_to_discuss'] = None
        return [NextState(states.TalkAboutGameExit.name, jump=True)]


class GameFunfactPropose(GameState):

    name = "game_funfact_propose"

    @classmethod
    def require_entity(cls, tracker: Tracker, game: entity.GameItem, funfact_item: Optional[funfact.Funfact] = None):
        tracker.one_turn_store['game_to_discuss'] = game
        if funfact_item:
            tracker.one_turn_store['funfact_item'] = funfact_item

    @classmethod
    def still_has_funfact(cls, tracker: Tracker, dispatcher: Dispatcher, game: entity.GameItem):
        return funfact.still_has_funfacts(tracker, game)

    @classmethod
    def funfact_counter(cls, tracker: Tracker, game: entity.GameItem):
        return funfact.funfact_counter(tracker, game)

    @classmethod
    def increment_funfact_counter(cls, tracker: Tracker, game: entity.GameItem):
        funfact.increment_funfact_counter(tracker, game)

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        GameFunfactPropose.increment_funfact_counter(tracker, self.game_to_discuss)

        fact = (
            tracker.one_turn_store.get('funfact_item') or
            tracker.last_utt_store.get('funfact_item') or
            funfact.get_random_funfact(tracker, self.game_to_discuss)
        )

        if (
            not fact or
            (
                fact and
                any(
                    not (i and isinstance(i, str))
                    for i in [fact.fact, fact.key, fact.question]
                )
            )
        ):
            self.logger.warning(f"did not get valid template args. args: {fact}")
            question = dispatcher.tm.speak(f"error/default_game_funfact_respond", {}, {})
            key = 'game_how_often_play'
        else:
            dispatcher.respond(fact.fact.format(
                game=self.game_to_discuss.noun if self.game_to_discuss else "this game"))
            key, question = fact.key, fact.question

        if not states.GameFunfactRespond.accept_transition(tracker, dispatcher, key):
            key = 'game_how_often_play'
            question = "How often do you play it?"

        dispatcher.respond(question.format(game=self.game_to_discuss.noun if self.game_to_discuss else "this game"))
        states.GameFunfactRespond.require_entity(tracker, key, self.game_to_discuss)
        return [NextState(states.GameFunfactRespond.name)]


class GameFunfactRespond(GameState):

    name = "game_funfact_respond"

    @classmethod
    def accept_transition(cls, tracker: Tracker, dispatcher: Dispatcher, question_key: str):
        return states.is_valid_followup_key(question_key)
        # return GameState.from_name(f"game_funfact_{question_key}_respond") is not None

    @classmethod
    def require_entity(cls, tracker: Tracker, question_key: str, game: entity.GameItem):
        tracker.one_turn_store['funfact_question_key'] = question_key
        tracker.one_turn_store['game_to_discuss'] = game

    def setup(self, dispatcher: Dispatcher, tracker: Tracker):
        super().setup(dispatcher, tracker)
        # TODO: better error handling
        self.question_key = (
            tracker.one_turn_store.get('funfact_question_key') or
            tracker.last_utt_store.get('funfact_question_key') or
            'game_how_often_play'
        )

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        next_state: GameState = GameState.from_name(f"game_funfact_{self.question_key}_respond")
        if next_state:
            next_state.require_entity(tracker=tracker, game=self.game_to_discuss)
            return [NextState(next_state.name, jump=True)]
        elif not next_state and states.is_valid_followup_key(self.question_key):
            states.GameFunfactDefaultHandlingRespond.require_entity(
                dispatcher, tracker, self.game_to_discuss, self.question_key)
            return [NextState(states.GameFunfactDefaultHandlingRespond.name, jump=True)]
        else:
            return [NextState(states.ErrorHandlingEnter.name, jump=True)]


class GameFunfactDirectProposePropose(GameState):

    name = 'game_funfact_direct_propose_propose'

    @classmethod
    def require_entity(cls, tracker: Tracker, game: entity.GameItem):
        tracker.one_turn_store['game_to_discuss'] = game

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if not self.game_to_discuss:
            dispatcher.responses.clear()
            return [NextState(states.ErrorHandlingEnter.name, jump=True)]
        dispatcher.respond_template(f"{selector_root}/direct_propose/default", dict(game=self.game_to_discuss))
        states.GameFunfactDirectProposeRespond.require_entity(tracker, self.game_to_discuss)
        return [NextState(states.GameFunfactDirectProposeRespond.name)]


class GameFunfactDirectProposeRespond(GameState):

    name = 'game_funfact_direct_propose_respond'

    @classmethod
    def require_entity(cls, tracker: Tracker, game: entity.GameItem):
        tracker.one_turn_store['game_to_discuss'] = game

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        if not self.game_to_discuss:
            dispatcher.responses.clear()
            return [NextState(states.ErrorHandlingEnter.name, jump=True)]

        topic_modules = tracker.returnnlp.topic_intent_modules - {TopicModule.GAME}
        topic_module = next(iter(topic_modules), None)
        game = next(iter(entity.GameItem.detect(tracker)), None)
        console: entity.ConsoleItem = next(iter(entity.ConsoleItem.detect(tracker)), None)
        genre = next(iter(entity.GenreItem.detect(tracker)), None)

        if (
            topic_module or
            (game and game != self.game_to_discuss) or
            console or
            genre
        ):
            states.TalkAboutGameRespond.require_entity(tracker, game)
            return [NextState(states.TalkAboutGameRespond.name, jump=True)]

        elif tracker.returnnlp.answer_positivity in {Positivity.pos}:
            states.GameFunfactPropose.require_entity(tracker, self.game_to_discuss)
            return [NextState(states.GameFunfactPropose.name, jump=True)]

        else:
            dispatcher.respond_template(f"exit/alright", {})
            dispatcher.propose_continue("STOP")
            return [NextState(states.Initial.name)]
