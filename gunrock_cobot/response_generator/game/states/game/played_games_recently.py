from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.game import states
from response_generator.game.states.base import GameState
from response_generator.game.utils import entity


class PlayedGamesRecentlyRespond(GameState):

    name = 'played_games_recently_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        game = next(iter(entity.GameItem.detect(tracker)), None)

        counter = tracker.persistent_store.get(f"{self.name}_pos_counter") or 0

        if game:
            states.TalkAboutGameRespond.require_entity(tracker, game)
            return [NextState(states.TalkAboutGameRespond.name, jump=True)]

        elif tracker.returnnlp.answer_positivity == 'pos' and counter < 1:
            tracker.persistent_store[f"{self.name}_pos_counter"] = counter + 1
            dispatcher.respond("Cool! What game did you play?")
            return [NextState(self.name)]

        elif tracker.returnnlp.answer_positivity in {'neu', 'neg'}:
            dispatcher.respond("I see.")

        return [NextState(states.QaEnter.name, jump=True)]
