import random

from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.game import states
from response_generator.game.states.base import GameState
from response_generator.game.utils import entity


selector_root = "talk_about_random_game"


class TalkAboutRandomGamePropose(GameState):

    name = "talk_about_random_game_propose"

    @classmethod
    def require_entity(cls, tracker: Tracker, *, prefix: str):
        """prefix = 'True', 'False', 'no_affirm'"""
        tracker.volatile_store['gd_random_prefix'] = prefix

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        games_with_funfact = filter(
            lambda g: (
                g not in self.user_profile.discussed_game_hist and
                states.GameFunfactEnter.accept_transition(tracker, g)
            ), (
                entity.GameItem.from_regex_match(noun=k.replace('_', ' '), canonical=k)
                for k in entity.GameItem.canonical_map()
            ))

        if not games_with_funfact:
            states.SubtopicSwitchEnter.require_entity(tracker, responses=[], tag=[])
            return [NextState(states.SubtopicSwitchEnter.name, jump=True)]

        game = random.choice(list(games_with_funfact))  # type: entity.GameItem
        self.user_profile.discussed_game_hist_append(game)

        gd_random_prefix = tracker.volatile_store.get('gd_random_prefix') or 'False'
        if gd_random_prefix == 'True':
            dispatcher.respond_template(f"{selector_root}/propose/with_affirm", dict(game=game.noun))
        elif gd_random_prefix == 'no_affirm':
            dispatcher.respond_template(f"{selector_root}/propose/default", dict(game=game.noun))

        states.GameFunfactEnter.require_entity(tracker, game, i_have_heard_of_game_ack=False)
        return [NextState(states.GameFunfactEnter.name, jump=True)]
