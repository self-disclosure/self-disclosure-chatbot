import base64
import random
import zlib
from typing import List

import boto3
from botocore.exceptions import ClientError

from response_generator.fsm2 import Dispatcher, Tracker, NextState, FSMLogger

from response_generator.game import states
from response_generator.game.states.base import GameState
from response_generator.game.utils import entity, funfact

logger = FSMLogger('GAME', 'funfact_followup', 'game')

selector_root = "game_funfact_followup"

dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
table_funfact_followup = dynamodb.Table('game_funfacts_followup')


def get_followup_template(key: str):
    try:
        response = table_funfact_followup.get_item(
            Key={
                'key': key,
            }
        )
        return response.get('Item', {})
    except ClientError as e:
        logger.warning(f"[GAME] <funfacts_followup> query db error: {e}", exc_info=True)


def is_valid_followup_key(key: str):
    return bool(get_followup_template(key))


def get_random_template(tracker: Tracker, key: str, templates: List[str]):

    def gen_hash(t: str):
        value = '/'.join([key, t]).encode()
        uid = zlib.adler32(value).to_bytes(4, byteorder='big')
        return base64.b64encode(uid).decode()[:-2]

    hist = tracker.persistent_store.get('funfact_followup_templates') or []

    if len(templates) <= 1:
        return templates[0]
    else:
        templates = [t for t in templates if gen_hash(t) not in hist]
        if not templates:
            tracker.persistent_store['funfact_followup_templates'] = hist[-1]
        templates = [t for t in templates if gen_hash(t) not in hist]
        return random.choice(templates)


def handle_followup_template(dispatcher: Dispatcher, tracker: Tracker, key: str, game: entity.GameItem):

    followup = get_followup_template(key)
    logger.info(f"handle_followup_template raw = {followup}")
    if followup:
        followup_type = followup.get('type') or 'neu'
    else:
        followup_type = None
    next_state = None

    if followup_type == 'neu':
        templates = followup.get('respond', {}).get('neu') or []
        template = get_random_template(tracker, key, templates)
        dispatcher.respond(template.format(game=game.noun))
        next_state = followup.get('next_state', {}).get('neu')

    elif followup_type == 'answer_positivity':
        ans = tracker.returnnlp.answer_positivity.name
        templates = followup.get('respond', {}).get(ans) or []
        template = get_random_template(tracker, key, templates)
        dispatcher.respond(template.format(game=game.noun))
        next_state = followup.get('next_state', {}).get(ans)
        logger.debug(f"getting next_state = {next_state}, {followup.get('next_state')}")

    if next_state:
        question, next_key, jump = tuple(next_state.get(i) for i in ['question', 'key', 'jump'])
        jump = True if jump else False
        if (
            question and isinstance(question, str) and next_key and isinstance(next_key, str)
        ):
            if (
                GameState.has_subclass(f"game_funfact_{next_key}_respond")
            ):
                dispatcher.respond(question.format(game=game.noun))
                return [NextState(f"game_funfact_{next_key}_respond", jump=jump)]
            elif (
                is_valid_followup_key(next_key)
            ):
                dispatcher.respond(question.format(game=game.noun))
                states.GameFunfactDefaultHandlingRespond.require_entity(dispatcher, tracker, game, next_key)
                return [NextState(states.GameFunfactDefaultHandlingRespond.name, jump=jump)]


class GameFunfactGameHowOftenPlayRespond(GameState):

    name = "game_funfact_game_how_often_play_respond"

    @classmethod
    def require_entity(cls, tracker: Tracker, game: entity.GameItem):
        tracker.one_turn_store['game_to_discuss'] = game

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        game = next(iter(entity.GameItem.detect(tracker)), None)

        def handle_command():
            def request_tellmore_handler():
                if game and game == self.game_to_discuss:
                    if states.GameCannedResponseEnter.accept_transition(tracker, game):
                        states.GameCannedResponseEnter.require_entity(tracker, game)
                        return [NextState(states.GameCannedResponseEnter.name, jump=True)]
                    elif states.GameFunfactEnter.accept_transition(tracker, game):
                        states.GameFunfactEnter.require_entity(tracker, game, i_have_heard_of_game_ack=False)
                        return [NextState(states.GameFunfactEnter.name, jump=True)]
                    else:
                        dispatcher.respond(f"That's all I know about {game.noun}.")
                elif game and game != self.game_to_discuss:
                    states.TalkAboutGameRespond.require_entity(tracker, game)
                    return [NextState(states.TalkAboutGameRespond.name, jump=True)]
                else:
                    return self.handler.request_tellmore_handler(dispatcher, tracker, skip_game_entity=True)

            if self.handler.has_command(tracker):
                return self.handler.handle_command(
                    dispatcher, tracker,
                    request_tellmore_handler=request_tellmore_handler,
                )

        def handle_question(ignore_idk_response=False, skip_ask_back=False):
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: (
                        dispatcher.respond("I don't play it often.") if not skip_ask_back else None),
                    follow_up_handler=lambda qr: None,
                    ignore_idk_response=lambda qr: ignore_idk_response,
                )

        cm_events = handle_command()
        if cm_events:
            return cm_events

        if game and game != self.game_to_discuss:
            states.TalkAboutGameRespond.require_entity(tracker, game)
            return [NextState(states.TalkAboutGameRespond.name, jump=True)]

        elif self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
            qr_events = handle_question()
        else:
            dispatcher.respond_template(f"ack/i_see", {})
            qr_events = handle_question()

        if qr_events:
            return qr_events

        ft_events = handle_followup_template(dispatcher, tracker, 'game_how_often_play', self.game_to_discuss)
        if ft_events:
            return ft_events
        return [NextState(states.GameFunfactExit.name, jump=True)]


class GameFunfactHaveYouPlayedThisGameRespond(GameState):

    name = "game_funfact_have_you_played_this_game_respond"

    @classmethod
    def require_entity(cls, tracker: Tracker, game: entity.GameItem):
        tracker.one_turn_store['game_to_discuss'] = game

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        states.GameFunfactDefaultHandlingRespond.require_entity(
            dispatcher, tracker, self.game_to_discuss, 'have_you_played_this_game')
        return [NextState(states.GameFunfactDefaultHandlingRespond.name, jump=True)]


class GameFunfactDefaultHandlingRespond(GameState):

    name = "game_funfact_default_handling_respond"

    @classmethod
    def require_entity(cls, dispatcher: Dispatcher, tracker: Tracker, game: entity.GameItem, key: str):
        tracker.one_turn_store['funfact_followup_key'] = key
        tracker.one_turn_store['game_to_discuss'] = game
        if key:
            fact = get_followup_template(key)
            if fact and isinstance(fact, dict):
                use_blender = True if fact.get('use_blender') else False
                dispatcher.expect_opinion(use_blender)
                logger.debug(f"default_handling_state: use_blender = {use_blender}")

    def setup(self, dispatcher: Dispatcher, tracker: Tracker):
        super().setup(dispatcher, tracker)
        self.followup_key = (
            tracker.one_turn_store.get('funfact_followup_key') or
            tracker.last_utt_store.get('funfact_followup_key') or
            ''
        )

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        game = next(iter(entity.GameItem.detect(tracker)), None)

        def handle_command():
            def request_tellmore_handler():
                if game and game == self.game_to_discuss:
                    if states.GameCannedResponseEnter.accept_transition(tracker, game):
                        states.GameCannedResponseEnter.require_entity(tracker, game)
                        return [NextState(states.GameCannedResponseEnter.name, jump=True)]
                    elif states.GameFunfactEnter.accept_transition(tracker, game):
                        states.GameFunfactEnter.require_entity(tracker, game, i_have_heard_of_game_ack=False)
                        return [NextState(states.GameFunfactEnter.name, jump=True)]
                    else:
                        dispatcher.respond(f"That's all I know about {game.noun}.")
                elif game and game != self.game_to_discuss:
                    states.TalkAboutGameRespond.require_entity(tracker, game)
                    return [NextState(states.TalkAboutGameRespond.name, jump=True)]
                else:
                    return self.handler.request_tellmore_handler(dispatcher, tracker, skip_game_entity=True)

            if self.handler.has_command(tracker):
                return self.handler.handle_command(
                    dispatcher, tracker,
                    request_tellmore_handler=request_tellmore_handler,
                )

        def handle_question(ignore_idk_response=False, skip_ask_back=False):
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: (
                        dispatcher.respond("I don't play it often.") if not skip_ask_back else None),
                    follow_up_handler=lambda qr: None,
                    ignore_idk_response=lambda qr: ignore_idk_response,
                )

        cm_events = handle_command()
        if cm_events:
            return cm_events

        if game and game != self.game_to_discuss:
            states.TalkAboutGameRespond.require_entity(tracker, game)
            return [NextState(states.TalkAboutGameRespond.name, jump=True)]

        elif self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
            qr_events = handle_question()
        else:
            dispatcher.respond_template(f"ack/i_see", {})
            qr_events = handle_question()

        if qr_events:
            return qr_events

        ft_events = handle_followup_template(dispatcher, tracker, self.followup_key, self.game_to_discuss)
        if ft_events:
            return ft_events

        return [NextState(states.GameFunfactExit.name, jump=True)]
