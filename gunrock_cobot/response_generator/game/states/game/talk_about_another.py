import re
from typing import Optional

from nlu.constants import Positivity, TopicModule
from nlu.utils.topic_module import module_utterable
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.game import states
from response_generator.game.states.base import GameState
from response_generator.game.utils import entity


selector_root = "talk_about_game"


class TalkAboutAnotherGamePropose(GameState):
    """
    propose "do you want to talk about another game?"
    """

    name = "talk_about_another_game_propose"

    @classmethod
    def accept_transition(cls, tracker: Tracker):
        return states.TalkAboutGameEnter.accept_transition(tracker)

    @classmethod
    def require_entity(cls, tracker: Tracker, game: Optional[entity.GameItem]):
        tracker.one_turn_store['game_to_discuss'] = game

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        if not TalkAboutAnotherGamePropose.accept_transition(tracker):
            return [NextState(states.TalkAboutGameExit.name, jump=True)]
        else:
            states.TalkAboutGameEnter.increment_counter(tracker)

        if self.game_to_discuss and states.GameCannedResponseEnter.accept_transition(tracker, self.game_to_discuss):
            dispatcher.respond_template(
                f"{selector_root}/propose_another/with_game", dict(game=self.game_to_discuss.noun))
            states.GameCannedResponseEnter.require_entity(tracker, self.game_to_discuss)
            return [NextState(states.GameCannedResponseEnter.name, jump=True)]

        elif self.game_to_discuss and states.GameFunfactEnter.accept_transition(tracker, self.game_to_discuss):
            dispatcher.respond_template(
                f"{selector_root}/propose_another/with_game", dict(game=self.game_to_discuss.noun))
            states.GameFunfactEnter.require_entity(tracker, self.game_to_discuss, i_have_heard_of_game_ack=False)
            return [NextState(states.GameCannedResponseEnter.name, jump=True)]

        else:
            dispatcher.respond_template(f"{selector_root}/propose_another/default", {})
            return [NextState(states.TalkAboutAnotherGameRespond.name)]


class TalkAboutAnotherGameRespond(GameState):

    name = "talk_about_another_game_respond"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        """they are similar enough"""
        return [NextState(states.TalkAboutGameRespond.name, jump=True)]

        topic_modules = tracker.returnnlp.topic_intent_modules - {TopicModule.GAME}
        topic_module = next(iter(topic_modules), None)
        game: entity.GameItem = next(iter(entity.GameItem.detect(tracker)), None)
        console: entity.ConsoleItem = next(iter(entity.ConsoleItem.detect(tracker)), None)
        genre: entity.GenreItem = next(iter(entity.GenreItem.detect(tracker)), None)

        counter = tracker.last_utt_store.get(f"{self.name}_counter") or 0

        def handle_question():
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                )

        if game:
            states.TalkAboutGameRespond.require_entity(tracker, game)
            return [NextState(states.TalkAboutGameRespond.name, jump=True)]

        elif console:
            tracker.volatile_store['console_to_discuss'] = console
            states.TalkAboutConsoleRespond.require_entity(tracker, console)
            return [NextState(states.TalkAboutConsoleRespond.name, jump=True)]

        elif genre:
            states.TalkAboutGenreRespond.require_entity(tracker, genre)
            return [NextState(states.TalkAboutGenreRespond.name, jump=True)]

        elif topic_module:
            dispatcher.respond_template(
                f"exception/topic_transition/module", dict(module=module_utterable(topic_module)))
            dispatcher.propose_continue('UNCLEAR', topic_module)
            return [NextState(states.ProposeUnclearRespond.name)]

        elif re.search(r"\b((which|what) games?)\b", tracker.input_text, re.I):
            self.logger.debug(f"got regex = 'which|what games', going to 'game_discussion_random_game'")
            dispatcher.respond_template(f"{selector_root}/propose_another/which_game_ack", {})
            states.TalkAboutRandomGamePropose.require_entity(tracker, prefix='False')
            return [NextState(states.TalkAboutRandomGamePropose.name, jump=True)]

        elif tracker.returnnlp.answer_positivity in {Positivity.pos, Positivity.neu} and counter < 1:
            tracker.one_turn_store[f"{self.name}_counter"] = counter + 1
            dispatcher.respond_template(f"{selector_root}/propose/with_affirm", {})
            return [NextState(self.name)]

        elif (
            tracker.returnnlp.answer_positivity is Positivity.neg or
            re.search(r"\b(no (more)? ?games?)\b", tracker.input_text)
        ):
            states.ExitTransitionEnter.require_entity(tracker, 'alright')
            return [NextState(states.ExitTransitionEnter.name, jump=True)]

        # TODO: reenable this?
        # elif tracker.central_element.dialog_act.label is DialogActEnum.OTHER_ANSWERS:
        #     return [NextState('dunno_game_propose', jump=True)]

        else:
            states.TalkAboutRandomGamePropose.require_entity(tracker, prefix='True')
            return [NextState(states.TalkAboutRandomGamePropose.name, jump=True)]
