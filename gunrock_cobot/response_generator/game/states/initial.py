import re

from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.game.utils import entity
from response_generator.game.states.base import GameState
from response_generator.game import states


class Initial(GameState):

    name = 'initial'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        game = next(iter(entity.GameItem.detect(tracker)), None)
        console = next(iter(entity.ConsoleItem.detect(tracker)), None)
        genre = next(iter(entity.GenreItem.detect(tracker)), None)

        def handle_question():
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                )

        if re.search(r"\b(let('?s| us) (talk|chat) about)\b", tracker.input_text, re.I):
            dispatcher.respond_template("ack/oh_sure", {})

        if game:
            states.TalkAboutGameRespond.require_entity(tracker, game)
            return [NextState(states.TalkAboutGameRespond.name, jump=True)]

        elif console:
            states.TalkAboutConsoleRespond.require_entity(tracker, console)
            return [NextState(states.TalkAboutConsoleRespond.name, jump=True)]

        elif genre:
            states.QaFavGenreRespond.require_entity(tracker, genre)
            return [NextState(states.QaFavGenreRespond.name, jump=True)]

        qr_events = handle_question()
        if qr_events:
            return qr_events
        if states.QaEnter.accept_transition(tracker):
            if self.handler.has_question(tracker):
                dispatcher.respond_template("transition/general_qa_ask/anyway", {})
            return [NextState(states.QaEnter.name, jump=True)]
        else:
            states.SubtopicSwitchEnter.require_entity(tracker, responses=[], tag=[])
            return [NextState(states.SubtopicSwitchEnter.name, jump=True)]
