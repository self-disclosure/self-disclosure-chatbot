import re

from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.game import states
from response_generator.game.states.base import GameState
from response_generator.game.utils import entity


selector_root = "talk_about_console"


class TalkAboutConsoleRespond(GameState):

    name = "talk_about_console_respond"

    @classmethod
    def require_entity(cls, tracker: Tracker, console: entity.ConsoleItem):
        tracker.one_turn_store['console_to_discuss'] = console

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        console: entity.ConsoleItem = (
            self.console_to_discuss or
            next(iter(entity.ConsoleItem.detect(tracker)), None)
        )
        if not console:
            self.logger.warning(f"missing console item.")
            return [NextState(states.ErrorHandlingEnter.name, jump=True)]

        def handle_question():
            if self.handler.has_command(tracker):
                return self.handler.handle_command(
                    dispatcher, tracker,
                )
            elif self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                    ignore_idk_response=lambda qr: True,
                )

        if (
            console.canonical in {'ps5', 'xbox_series_x'} and
            states.QaNextGenConsoleEnter.name not in states.qa_path_hist(tracker)
        ):
            tracker.persistent_store['qa_path_hist'] = [
                *states.qa_path_hist(tracker), states.QaNextGenConsoleEnter.name]

            if re.search(r"have you (played|heard of)", tracker.input_text):
                dispatcher.respond(f"I've heard of {console.noun}! I'm excited to see them released this year!")

            states.QaNextGenConsoleEnter.require_entity(tracker, key='question_only')
            return [NextState(states.QaNextGenConsoleEnter.name, jump=True)]

        elif (
            console.canonical in {'ps5', 'xbox_series_x'}
        ):
            dispatcher.respond(f"I'm excited for the release of {console.noun}!")
            return [NextState(states.SubtopicSwitchEnter.name, jump=True)]

        # acknowledgment
        if self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
        else:
            dispatcher.respond_template("ack/i_see", {})

        ack_selector = f"{selector_root}/ack/{console.noun}"
        if dispatcher.tm.has_selector(ack_selector):
            dispatcher.respond_template(ack_selector, {})

        # handle question
        qr_events = handle_question()
        if qr_events:
            return qr_events

        # build transition that goes out to game
        console_game_template_selector = f"{selector_root}/transition/console_game_template/{console.noun}"
        if dispatcher.tm.has_selector(console_game_template_selector):
            console_game_template = dispatcher.tm.speak(console_game_template_selector, {})
        else:
            console_game_template = f"games on {console.noun}"

        console_transition_selector = f"{selector_root}/transition/question"
        dispatcher.respond_template(
            console_transition_selector, dict(console_game_template=console_game_template, console=console.noun))

        # TODO: talk_about_game can't handle all this
        states.TalkAboutGameRespond.require_entity(tracker, None)
        return [NextState(states.TalkAboutGameRespond.name)]
