from typing import List, Optional

from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, NextState
from selecting_strategy import module_selection

from response_generator.game import states
from response_generator.game.states.base import GameState
from response_generator.game.utils import entity


class ErrorHandlingEnter(GameState):

    name = "error_handling_enter"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        if states.QaEnter.accept_transition(tracker):
            dispatcher.respond_error("return_to_general_qa_ask", {})
            return [NextState(states.QaEnter.name, jump=True)]
        else:
            states.SubtopicSwitchEnter.require_entity(tracker, responses=[], tag=[])
            return [NextState(states.SubtopicSwitchEnter.name, jump=True)]


class TalkedAboutEntityAlreadyRespond(GameState):

    name = "talked_about_entity_already_respond"

    @classmethod
    def require_entity(
        cls, tracker: Tracker, *,
        game: Optional[entity.GameItem],
        console: Optional[entity.ConsoleItem],
    ):
        tracker.one_turn_store['game_to_discuss'] = game
        tracker.one_turn_store['console_to_discuss'] = console

    @classmethod
    def hist(cls, tracker: Tracker):
        return tracker.persistent_store.get('talked_about_entity') or []

    @classmethod
    def hist_append(cls, tracker: Tracker, entity: entity.EntityItem):
        tracker.persistent_store['talked_about_entity'] = [*TalkedAboutEntityAlreadyRespond.hist(tracker), entity]

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        game = self.game_to_discuss
        console = self.console_to_discuss

        hist = tracker.persistent_store.get('talked_about_entity') or []

        if game and game not in hist:
            tracker.persistent_store['talked_about_entity'] = [*hist, game]
            dispatcher.respond_template(f"exception/talked_about_entity/with_prompt", dict(entity=game.noun))
            states.SubtopicSwitchEnter.require_entity(tracker, [], [], exit_transition_enter_flag="None")
            return [NextState(states.SubtopicSwitchEnter.name, jump=True)]

        elif game and game in hist:
            dispatcher.respond_template(f"exception/talked_about_entity/default", dict(entity=game.noun))
            states.SubtopicSwitchEnter.require_entity(tracker, [], [], exit_transition_enter_flag="something_else")
            return [NextState(states.SubtopicSwitchEnter.name, jump=True)]

        elif console and console not in hist:
            tracker.persistent_store['talked_about_entity'] = [*hist, console]
            dispatcher.respond_template(f"exception/talked_about_entity/with_prompt", dict(entity=console.noun))
            states.SubtopicSwitchEnter.require_entity(tracker, [], [], exit_transition_enter_flag="None")
            return [NextState(states.SubtopicSwitchEnter.name, jump=True)]

        elif console and console in hist:
            dispatcher.respond_template(f"exception/talked_about_entity/default", dict(entity=console.noun))
            states.SubtopicSwitchEnter.require_entity(tracker, [], [], exit_transition_enter_flag="something_else")
            return [NextState(states.SubtopicSwitchEnter.name, jump=True)]

        else:
            return [NextState(states.ErrorHandlingEnter.name, jump=True)]


class ProposeUnclearRespond(GameState):

    name = 'propose_unclear_respond'

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        try:
            last_topic_module = TopicModule(
                tracker.user_attributes.module_selection.last_topic_module
            )
        except ValueError:
            last_topic_module = TopicModule.NA

        def handle_question():
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                )

        if (
            self.handler.has_question_only(tracker)
        ):
            qr_events = handle_question()
            if qr_events:
                return qr_events

        elif last_topic_module is TopicModule.GAME:
            dispatcher.respond(f"Alright.")
            qr_events = handle_question()
            if qr_events:
                return qr_events

        else:
            qr_events = handle_question()
            if qr_events:
                return qr_events

        states.SubtopicSwitchEnter.require_entity(tracker, responses=[], tag=[])
        return [NextState(states.SubtopicSwitchEnter.name, jump=True)]


class ExitTransitionEnter(GameState):

    name = 'exit_transition_enter'

    @classmethod
    def require_entity(cls, tracker: Tracker, flag='transition'):
        tracker.one_turn_store[f"{cls.name}_flag"] = flag

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        flag = (
            tracker.one_turn_store.get(f"{self.name}_flag") or
            tracker.last_utt_store.get(f"{self.name}_flag") or
            None
        )
        if flag == 'alright':
            dispatcher.respond_template(f"exit/alright", {})
        elif flag == 'something_else':
            dispatcher.respond_template(f"exit/something_else", {})
        elif flag == 'transition':
            dispatcher.respond_template(f"exit/transition", {})

        module_selector = module_selection.ModuleSelector(tracker.user_attributes.ua_ref)
        module_selector.add_out_of_template_topic_module(TopicModule.MUSIC.value)
        dispatcher.propose_continue('STOP')
        return [NextState(states.Initial.name)]


class PostProcess(GameState):

    name = 'post_process_hook'
    special_type = {'post_process'}

    def post_process(self, dispatcher: Dispatcher, tracker: Tracker):
        cond = [
            states.QaEnter.accept_transition(tracker),
            states.TalkAboutGameEnter.accept_transition(tracker),
        ]
        self.logger.debug(f"checking subtopics accept_transition: {cond}")
        if all(not i for i in cond):
            self.logger.debug(f"<post_response_hook> out of accept_transitions = {cond}")
            module_selector = module_selection.ModuleSelector(tracker.user_attributes.ua_ref)
            module_selector.add_out_of_template_topic_module(TopicModule.MUSIC.value)

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.SwitchSubtopicEnter.name, jump=True)]


class SubtopicSwitchEnter(GameState):

    name = 'subtopic_switch_enter'

    @classmethod
    def require_entity(
        cls, tracker: Tracker, responses: List[str], tag: Optional[str], *,
        exit_responses: List[str] = [],
        exit_transition_enter_flag=None,
    ):
        tracker.volatile_store['resume_acks'] = responses
        tracker.volatile_store['resume_tag'] = tag
        tracker.volatile_store['exit_responses'] = exit_responses
        if exit_transition_enter_flag:
            tracker.volatile_store['exit_transition_enter_flag'] = exit_transition_enter_flag

    def setup(self, dispatcher: Dispatcher, tracker: Tracker):
        super().setup(dispatcher, tracker)

        self.resume_acks = tracker.volatile_store.get('resume_acks') or []
        self.resume_tag = tracker.volatile_store.get('resume_tag') or None
        self.exit_acks = tracker.volatile_store.get('exit_responses') or []

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        def respond_ack():
            for ack in self.resume_acks:
                dispatcher.respond(ack)

        def exit_ack():
            for ack in self.exit_acks:
                dispatcher.respond(ack)

        if states.QaEnter.accept_transition(tracker):
            if not any(i in {states.TalkAboutGameExit.name} for i in self.module.state_tracker.curr_turn()):
                respond_ack()
            return [NextState(states.QaEnter.name, jump=True)]

        elif states.TalkAboutGameEnter.accept_transition(tracker):
            respond_ack()
            return [NextState(states.TalkAboutGameEnter.name, jump=True)]

        else:
            exit_ack()
            flag = tracker.volatile_store.get('exit_transition_enter_flag', 'transition')
            states.ExitTransitionEnter.require_entity(tracker, flag=flag)
            return [NextState(states.ExitTransitionEnter.name, jump=True)]
