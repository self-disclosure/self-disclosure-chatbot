from typing import List

from response_generator.fsm2 import Dispatcher, Tracker, NextState
from response_generator.fsm2.events import Event

from response_generator.game.utils import entity
from response_generator.game.states.base import GameState
from response_generator.game import states


selector_root = "talk_about_genre"


class TalkAboutGenreRespond(GameState):

    name = "talk_about_genre_respond"

    @classmethod
    def require_entity(cls, tracker: Tracker, genre: entity.GenreItem):
        tracker.one_turn_store['genre_to_discuss'] = genre

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:

        def handle_question():
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: None,
                    follow_up_handler=lambda qr: None,
                )
        genre: entity.GenreItem = (
            self.genre_to_discuss or
            next(iter(entity.GenreItem.detect(tracker)), None)
        )
        if not genre:
            self.logger.warning(f"missing console item.")
            return [NextState(states.ErrorHandlingEnter.name, jump=True)]

        # acknowledgment
        ack_selector = f"{selector_root}/ack/{genre.noun}"
        self.logger.debug(f"ack_selector = {ack_selector}")
        if dispatcher.tm.has_selector(ack_selector):
            dispatcher.respond_template(ack_selector, {})
        elif self.sys_ack_tag != 'ack_question_idk' and self.sys_ack_utt:
            dispatcher.respond(self.sys_ack_utt)
        else:
            dispatcher.respond_template("ack/i_see", {})

        qr_events = handle_question()
        if qr_events:
            return qr_events

        # build transition that goes out to game
        dispatcher.respond_template(f"{selector_root}/transition/question", dict(genre=genre.speakable_noun()))

        # TODO: check transition
        states.TalkAboutGameRespond.require_entity(tracker, None)
        return [NextState(states.TalkAboutGameRespond.name)]
