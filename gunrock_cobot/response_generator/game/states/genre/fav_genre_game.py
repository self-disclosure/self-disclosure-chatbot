import re

from nlu.constants import DialogAct, Positivity
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.game import states
from response_generator.game.states.base import GameState
from response_generator.game.utils import entity


selector_root = "fav_genre_game"


class FavGenreGamePropose(GameState):

    name = "fav_genre_game_propose"

    @classmethod
    def require_entity(cls, tracker: Tracker, genre: entity.GenreItem, question: str):
        """
        question = ( fav_genre_game | fav_general_game )
        """
        tracker.one_turn_store['genre_to_discuss'] = genre
        tracker.one_turn_store[f"{cls.name}_question"] = question

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        question: str = (
            tracker.one_turn_store.get(f"{self.name}_question") or
            tracker.last_utt_store.get(f"{self.name}_question")
        )
        if self.genre_to_discuss and question == 'fav_genre_game':
            dispatcher.respond_template(
                f"{selector_root}/propose/default",
                dict(genre_game=self.genre_to_discuss.speakable_noun(game_suffix=True)))
            return [NextState(states.FavGenreGameRespond.name)]
        elif self.genre_to_discuss and question == 'fav_general_game':
            dispatcher.respond_template(f"{selector_root}/propose/focus_on_video_games", {})
            return [NextState(states.FavGenreGameGeneralRespond.name)]
        else:
            dispatcher.respond_template(f"{selector_root}/propose/default_no_genre_game", {})
            return [NextState(states.FavGenreGameRespond.name)]


class FavGenreGameRespond(GameState):

    name = "fav_genre_game_respond"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        game = next(iter(entity.GameItem.detect(tracker)), None)
        genre = next(iter(entity.GenreItem.detect(tracker)), None)  # type: entity.GenreItem
        # false_games = entity.FalseGameItem.detect(tracker)

        genre_counter = tracker.persistent_store.get(f"{self.name}_counter") or 0

        def handle_question():
            if self.handler.has_question(tracker):
                return self.handler.handle_question(
                    dispatcher, tracker,
                    ask_back_handler=lambda qr: dispatcher.respond(f"I'm not sure, I like all kinds of games."),
                    follow_up_handler=lambda qr: None,
                )

        if game:
            states.TalkAboutGameRespond.require_entity(tracker, game)
            return [NextState(states.TalkAboutGameRespond.name, jump=True)]

        elif not game and genre and genre_counter < 1:
            tracker.persistent_store[f"{self.name}_counter"] = genre_counter + 1
            if self.sys_ack_tag != 'ack_question_idk' and self.sys_ack_utt:
                dispatcher.respond(self.sys_ack_utt)
            else:
                dispatcher.respond_template("ack/i_see", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events
            dispatcher.respond_template(
                "qa/favorite_genre/respond/genre/default_no_ack", dict(genre=genre.speakable_noun(game_suffix=False)))
            return [NextState(self.name)]

        elif re.search(r"\b(ye(s|a|ah))\b", tracker.input_text) and genre_counter < 1:
            tracker.persistent_store[f"{self.name}_counter"] = genre_counter + 1
            dispatcher.respond_template(f"{selector_root}/respond/yes_only", {})
            return [NextState(self.name)]

        else:
            if (
                tracker.returnnlp.answer_positivity is Positivity.neg or
                tracker.returnnlp.has_dialog_act(DialogAct.OTHER_ANSWERS)
            ):
                dispatcher.respond_template(f"ack/thats_alright", {})
            else:
                dispatcher.respond_template("ack/i_see", {})
            qr_events = handle_question()
            if qr_events:
                return qr_events
            states.SubtopicSwitchEnter.require_entity(tracker, responses=[], tag=[])
            return [NextState(states.SubtopicSwitchEnter.name, jump=True)]


class FavGenreGameGeneralRespond(GameState):

    name = "fav_genre_game_general_respond"

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.FavGenreGameRespond.name, jump=True)]
