import re
from typing import List, Optional

import nlu.utils.topic_module
import response_generator.common.evi as evi
from nlu.constants import TopicModule
from nlu.constants import DialogAct as DialogActEnum
from response_generator.fsm2 import Dispatcher, Tracker, NextState
from response_generator.fsm2.events import Event
from selecting_strategy.module_selection import GlobalState

from response_generator.game import states
from response_generator.game.states.base import GameState

from response_generator.game.utils import entity, funfact, regex_match


# MARK: - Global States

class SelfDisclosure(GameState):

    name = "self_disclosure"
    # special_type = "global_state"

    def enter_condition(self,
                        dispatcher: Dispatcher,
                        tracker: Tracker) -> Optional[str]:
        question_map = {
            "what_you_know": r"(do|what|what do) you know (|anything )about (|\w+ )game",
            "when_was_<game>_released": r"when was [\w\s]+ released( in|)$",
        }
        game = next(iter(entity.GameItem.detect(tracker)), None)

        if re.search(question_map['what_you_know'], tracker.input_text, re.IGNORECASE):
            tracker.volatile_store['self_disclosure'] = {
                'question': 'what_you_know',
            }
            if game:
                tracker.volatile_store['game_to_discuss'] = game
            return SelfDisclosure.name
        elif re.search(question_map['when_was_<game>_released'], tracker.input_text, re.IGNORECASE):
            tracker.volatile_store['self_disclosure'] = {
                'question': 'when_was_<game>_released',
            }
            return SelfDisclosure.name
        return None

    def run(self,
            dispatcher: Dispatcher,
            tracker: Tracker) -> List[Event]:

        self_disclosure = tracker.volatile_store['self_disclosure']

        if self_disclosure['question'] == 'what_you_know':
            game = tracker.volatile_store.get('game_to_discuss')
            if not game:
                dispatcher.respond(f"I know a few games.")
                states.TalkAboutRandomGamePropose.require_entity(tracker, prefix='False')
                return [NextState(states.TalkAboutRandomGamePropose.name, jump=True)]

            else:
                return [NextState("game_discussion", jump=True)]

        elif self_disclosure['question'] == 'when_was_<game>_released':
            utt = evi.EVI_bot(tracker.input_text.lower(), handle_empty_response=False)
            game = next(iter(entity.GameItem.detect(tracker)), None)
            self.logger.debug(f"[GAME] evi response to '{tracker.input_text.lower()}': {utt}")

            if not utt and game and funfact.has_hardcode_funfacts(game):
                tracker.volatile_store['game_to_discuss'] = game
                dispatcher.respond("Hmm, I'm not sure how to answer that.")
                dispatcher.respond(f"Though I know something about {game.noun}.")
                return [NextState("game_funfact", jump=True)]

            elif not utt:
                dispatcher.respond("Hmm, I'm not sure how to answer that.")
                dispatcher.respond_template(f"chitchat/postfix/another_game", {})
                return [NextState("game_discussion_another_game_respond")]

            elif utt:

                dispatcher.respond(utt)
                if game:
                    dispatcher.respond(f"Anyway,")
                    states.GameFunfactProposeConfirm.require_entity(tracker, game)
                    return [NextState(states.GameFunfactDirectProposePropose.name, jump=True)]
                else:
                    states.TalkAboutRandomGamePropose.require_entity(tracker, prefix='True')
                    return [NextState(states.TalkAboutRandomGamePropose.name, jump=True)]


class ForceTalkAboutGameRespond(GameState):

    name = "force_talk_about_game_respond"
    special_type = {'global_state'}

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker):
        # regex = (
        #     r"\b((^lets |^i want to )talk about[\w\s+])|(^i (mean|said))\b"
        # )
        regex = "\b({})\b".format("|".join([
            r"(^lets |^i want to )talk about[\w\s+])",
            r"(^i (mean|said)",
            r"(can (we|you) ((talk|chat) about|tell (me|us)) ?(about)?)",
        ]))
        regex_extended = (
            r"\b((^lets |^i want to )talk about[\w\s+])|(^i (mean|said)|"
            r"no but i('m| am) interested (in|to))\b"
        )
        prev_state = next(iter(self.module.state_tracker.prev_turn() or []), None)
        game = next(iter(entity.GameItem.detect(tracker)), None)
        console = next(iter(entity.ConsoleItem.detect(tracker)), None)

        cond = [
            re.search(regex, tracker.input_text),
            re.search(regex_extended, tracker.input_text),
            game,
            console,
            prev_state
        ]

        self.logger.debug(f"force cond = {cond}")

        if (
            (
                re.search(regex, tracker.input_text) and (game or console) and prev_state and prev_state not in {
                    states.Initial.name,
                    states.TalkAboutGamePropose.name,
                    states.TalkAboutAnotherGamePropose.name,
                    states.GameFunfactPropose.name,
                }
            ) or
            (
                re.search(regex_extended, tracker.input_text) and (game or console) and
                prev_state and prev_state not in {
                    states.TalkAboutGamePropose.name,
                    states.TalkAboutAnotherGamePropose.name,
                    states.GameFunfactPropose.name,
                }
            )
        ):
            if game:
                self.logger.debug(f"force_talk_about_game_respond wants to enter. prev_state: {prev_state}")
                states.TalkAboutGameRespond.require_entity(tracker, game)
                return states.TalkAboutGameRespond.name
            elif console:
                states.TalkAboutConsoleRespond.require_entity(tracker, console)
                return states.TalkAboutConsoleRespond.name

        elif (
            re.search(regex, tracker.input_text) and
            not (game or console) and prev_state and tracker.returnnlp.has_dialog_act({DialogActEnum.COMMANDS})
        ):
            # TODO: avoid random game
            self.logger.debug(f"force_talk_about_game_respond wants to enter. prev_state: {prev_state}")
            states.TalkAboutRandomGamePropose.require_entity(tracker, prefix='True')
            return states.TalkAboutRandomGamePropose.name

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        states.SubtopicSwitchEnter.require_entity(tracker, responses=[], tag=[])
        return [NextState(states.SubtopicSwitchEnter.name, jump=True)]


class GlobalTopicTransition(GameState):

    name = "global_topic_transition"
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:
        topic_modules = tracker.returnnlp.topic_intent_modules - {TopicModule.GAME}

        # if topic_modules and False:
        #     # TODO: This might catches many things
        #     module = next(iter(topic_modules))  # TopicModule
        #     tracker.volatile_store['exception_flag'] = "topic_transition_with_module"
        #     tracker.volatile_store['module'] = module
        #     return GlobalTopicTransition.name

        if regex_match.is_jumpout_phrase(tracker):
            tracker.volatile_store['exception_flag'] = "exit_with_phrase"
            if topic_modules:
                module = next(iter(topic_modules), None)  # TopicModule
                tracker.volatile_store['module'] = module
            return GlobalTopicTransition.name

        else:
            return None

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        flag = tracker.volatile_store.get('exception_flag')

        try:
            if not flag or flag not in {'topic_transition_with_module',
                                        'exit_with_phrase'}:
                raise ValueError(f"[GAME] {GlobalTopicTransition.name} did not receive valid flag")

            elif flag == 'topic_transition_with_module':
                """
                if user triggers topic_transition
                """
                module = tracker.volatile_store['module']
                if module:
                    module_utt = nlu.utils.topic_module.module_utterable(module)
                    dispatcher.propose_continue('STOP', module)
                    dispatcher.respond_template(f"exception/topic_transition/module", {'module': module_utt})
                else:
                    dispatcher.propose_continue('STOP')
                    dispatcher.respond_template(f"exception/topic_transition/no_module_error", {})
                return [NextState(states.Initial.name)]

            elif flag == 'exit_with_phrase':
                """
                if user wants to quit and it matches regex
                """
                module = tracker.volatile_store.get('module')
                if module:
                    dispatcher.propose_continue('STOP', module)
                else:
                    dispatcher.propose_continue('STOP')

                if (self.sys_ack_tag and self.sys_ack_utt):
                    pass
                else:
                    dispatcher.respond_template(f"exit/alright", {})
                return [NextState("initial")]

        except ValueError as e:
            self.logger.warning(e, exc_info=True)
