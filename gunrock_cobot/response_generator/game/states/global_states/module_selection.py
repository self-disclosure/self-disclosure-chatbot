import re

from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, NextState
from selecting_strategy.module_selection import GlobalState, ProposeKeywordEntity

from response_generator.game import states
from response_generator.game.states.base import GameState

from response_generator.game.utils import entity


# MARK: - Global States

class ModuleSelectionGlobalState(GameState):

    name = 'module_selection_global_state'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker):
        try:
            last_topic_module = TopicModule(
                tracker.user_attributes.module_selection.last_topic_module
            )
        except ValueError:
            last_topic_module = TopicModule.NA

        if (
            (last_topic_module == TopicModule.GAME)
        ):
            return None

        keyword: GlobalState = tracker.user_attributes.module_selection.global_state

        proposed_keyword = tracker.user_attributes.module_selection.get_top_propose_keyword()

        self.logger.debug(
            f"got global_state from module_selection: "
            f"state = {keyword}, "
            f"proposed_keyword = {proposed_keyword}, "
            f"mapped = {map_proposed_keyword_to_entity(proposed_keyword)}, "
        )
        if keyword is GlobalState.ASK_RECENT_PLAYED_GAME:
            return states.PlayedGamesRecentlyRespond.name

        elif proposed_keyword and proposed_keyword.text:
            entity_item = map_proposed_keyword_to_entity(proposed_keyword)
            if isinstance(entity_item, entity.GameItem):
                if self.sys_ack_utt:
                    dispatcher.respond(self.sys_ack_utt)
                else:
                    dispatcher.respond(f"Cool!")
                states.TalkAboutGameRespond.require_entity(tracker, entity_item, i_have_heard_of_game_ack=False)
                return states.TalkAboutGameRespond.name

            elif isinstance(entity_item, entity.ConsoleItem):
                if self.sys_ack_utt:
                    dispatcher.respond(self.sys_ack_utt)
                else:
                    dispatcher.respond(f"Cool!")
                states.TalkAboutConsoleRespond.require_entity(tracker, entity_item)
                return states.TalkAboutConsoleRespond.name

            else:
                dispatcher.respond(f"Great! I'm a bit of a gamer myself.")
                return states.SubtopicSwitchEnter.name

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return [NextState(states.SubtopicSwitchEnter.name, jump=True)]


def type_mapping():
    return dict(
        game=(
            r"game",
            "|".join([
                r"game show|game developer|console"
            ])
        ),
        console=(
            r"console",
            "|".join([
                r"game show|game developer"
            ])
        )
    )


def map_proposed_keyword_to_entity(proposed_keyword: ProposeKeywordEntity):

    # import logging
    # logger = logging.getLogger(__name__)
    # logger.debug(f"MAP MAP = {proposed_keyword}")

    if not proposed_keyword.text:
        return

    # entity_type = next(iter(
    #     k for (k, v) in type_mapping().items()
    #     if any(re.search(v, i, re.I) for i in (proposed_keyword.entity_type or []))
    # ), 'general')

    entity_type = 'general'
    for k, (white, black) in type_mapping().items():
        for i in proposed_keyword.entity_type:
            if re.search(black, i, re.I):
                break
            match = re.search(white, i, re.I)
            # logger.debug(f"match = ({v, i}) {match}")
            if match:
                entity_type = k

    text = proposed_keyword.text

    if entity_type == 'game':
        return entity.GameItem.from_regex_match(text, text.lower().replace(" ", "_"))

    elif entity_type == 'console':
        return entity.ConsoleItem.from_regex_match(text, text.lower().replace(" ", "_"))

    else:
        return
