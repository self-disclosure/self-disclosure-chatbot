from nlu.constants import TopicModule
from response_generator.fsm2 import Dispatcher, Tracker, NextState
from user_profiler.user_profile import UserProfile as GlobalUserProfile

from response_generator.game import states
from response_generator.game.states.base import GameState
from response_generator.game.utils import entity


selector_root = 'resume'


class ResumeRespond(GameState):

    name = 'resume_respond'
    special_type = {'global_state'}

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker):
        try:
            last_topic_module = TopicModule(
                tracker.user_attributes.module_selection.last_topic_module
            )
        except ValueError:
            last_topic_module = TopicModule.NA

        global_user_profile = GlobalUserProfile(tracker.user_attributes.ua_ref)

        self.logger.info(
            f"last_topic_module = {last_topic_module}, "
            f"first = {tracker.persistent_store.get('__first__')}, "
            f"visit = {global_user_profile.visit}, "
        )

        if not tracker.persistent_store.get('__first__'):
            return
        if (
            (tracker.persistent_store.get('__first__') or 0) != global_user_profile.visit or
            (
                (tracker.persistent_store.get('__first__') or 0) == global_user_profile.visit and
                last_topic_module != TopicModule.GAME
            )
        ):
            return self.name

    def run(self, dispatcher: Dispatcher, tracker: Tracker):

        # prev_turn = self.module.state_tracker.prev_turn(-1) or [None]
        # last_propose = prev_turn[-1]
        responses = []
        tag = None
        # global_user_profile = GlobalUserProfile(tracker.user_attributes.ua_ref)
        # if (tracker.persistent_store.get('__first__') or 0) != global_user_profile.visit:
        #     """second conversation"""
        #     second_convo = True
        # else:
        #     second_convo = False

        game = next(iter(entity.GameItem.detect(tracker)), None)
        console = next(iter(entity.ConsoleItem.detect(tracker)), None)

        if self.sys_ack_tag != 'ack_question_idk' and self.sys_ack_utt:
            responses.append(self.sys_ack_utt)
        else:
            responses.append(f"Sure! I love to talk about video games.")

        # TODO: more granular ack?

        if game:
            states.TalkAboutGameRespond.require_entity(tracker, game)
            return [NextState(states.TalkAboutGameRespond.name, jump=True)]

        elif console:
            states.TalkAboutConsoleRespond.require_entity(tracker, console)
            return [NextState(states.TalkAboutConsoleRespond.name, jump=True)]

        # TODO: check previous states to go to different item
        states.SubtopicSwitchEnter.require_entity(tracker, responses, tag)
        return [NextState(states.SubtopicSwitchEnter.name, jump=True)]
