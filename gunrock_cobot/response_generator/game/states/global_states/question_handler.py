import re
from typing import List, Optional

from nlu.constants import TopicModule
from nlu.question_detector import QuestionDetector
from question_handling.quetion_handler import QuestionHandler, QuestionResponse
from response_generator.fsm2 import Dispatcher, Tracker, NextState, PreviousState
from response_generator.fsm2.events import Event
from template_manager import Template

from response_generator.game.utils import entity

from response_generator.game.states.base import GameState
from response_generator.game import states


class QuestionHandlerRespond(GameState):

    name = 'question_handler'
    special_type = 'global_state'

    def setup(self, dispatcher: Dispatcher, tracker: Tracker):
        super().setup(dispatcher, tracker)
        self.question_detector = QuestionDetector(
            tracker.input_text, self.module.input_data['returnnlp']
        )
        self.question_handler = QuestionHandler(
            tracker.input_text, self.module.input_data['returnnlp'],
            backstory_threshold=0.85,
            system_ack=self.module.input_data['system_acknowledgement'],
            user_attributes_ref=tracker.user_attributes.ua_ref
        )

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:

        # TODO: workaround before question handler becomes part of each state
        game = next(iter(entity.GameItem.detect(tracker)), None)
        if (
            re.search(r"\b((can we|let('?s| us)) talk about)\b", tracker.input_text, re.I) and
            game
        ):
            # not a question if asking 'can we talk about <game>';
            self.logger.debug(f"'can we talk about' with game={game}, skipping global question")
            return None

        elif (
            re.search(r"\b(do you know (about)?)\b", tracker.input_text, re.I) and
            game
        ):
            # not a question if asking 'can we talk about <game>';
            self.logger.debug(f"'can we talk about' with game={game}, skipping global question")
            states.TalkAboutGameRespond.require_entity(tracker, game)
            return states.TalkAboutGameRespond.name

        elif (
            re.search(r"\b((which|what) games?)\b", tracker.input_text, re.I)
        ):
            self.logger.debug(f"which|what games, skipping global question")
            return None

        elif self.question_detector.has_question():
            self.logger.debug(f"[GAME] question_handler is entering")
            return self.name

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:

        # Step 2: Check if it's unanswerable
        if self.is_unanswerable_question(dispatcher, tracker):
            self.logger.debug(f"[GAME] question_handler is unanswerable question")
            dispatcher.respond(self.question_handler.generate_i_dont_know_response())
            return [PreviousState()]

        # step 3:
        next_state = self.handle_topic_specific_question(dispatcher, tracker)
        if next_state:
            self.logger.debug(f"[GAME] question_handler is topic specific question: {next_state}")
            return next_state

        # Step 4
        next_state = self.handle_general_question(dispatcher, tracker)
        if next_state:
            self.logger.debug(f"[GAME] question_handler is general question: {next_state}")
            return next_state

    def is_unanswerable_question(self, dispatcher: Dispatcher, tracker: Tracker) -> bool:
        # todo: you should decide what's answerable in you topic module
        if self.question_detector.is_ask_back_question() or self.question_detector.is_follow_up_question():
            return True
        return False

    def handle_topic_specific_question(self, dispatcher: Dispatcher, tracker: Tracker):
        # eg. for movie, it should handle user's requests like "what movie do you recommend to me"
        self_disclosure = states.SelfDisclosure(dispatcher.tm).enter_condition(dispatcher, tracker)
        self.logger.debug(f"[GAME] question_handler self_disclosure: {self_disclosure}")
        if self_disclosure:
            return [NextState(self_disclosure, jump=True)]

    def handle_general_question(self, dispatcher: Dispatcher, tracker: Tracker) -> QuestionResponse:
        answer = self.question_handler.handle_question()
        self.logger.debug(f"[GAME] question_handler general_question: {answer}")

        dispatcher.respond(answer.response)

        if answer and answer.bot_propose_module and answer.bot_propose_module is not TopicModule.GAME:
            self.logger.debug(f"[GAME] question_handler has bot_propose_module: {answer.bot_propose_module}")
            # backstory wants to propose a topic
            dispatcher.respond_template(
                f"propose_topic_short/{answer.bot_propose_module.value}", {}, template=Template.transition
            )
            dispatcher.propose_continue('UNCLEAR', answer.bot_propose_module)

        prev_state = self.module.state_tracker.curr_turn()[0]
        # temp fix PreviousState not working
        return [NextState(prev_state)]
        # return [PreviousState()]
