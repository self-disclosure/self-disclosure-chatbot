from nlu.constants import DialogAct
from response_generator.fsm2 import Dispatcher, Tracker, NextState

from response_generator.game.utils import entity
from response_generator.game.states.base import GameState
from response_generator.game import states


class HaveYouPlayedGameRespond(GameState):

    played_games = [
        'overwatch',
        
    ]

    @classmethod
    def require_entity(cls, dispatcher: Dispatcher, tracker: Tracker, game: entity.GameItem):
        tracker.one_turn_store['game_to_discuss'] = game

    def run(self, dispatcher: Dispatcher, tracker: Tracker):
        return
