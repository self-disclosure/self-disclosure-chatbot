from typing import Optional

from response_generator.fsm2 import State, Dispatcher, Tracker

from response_generator.game.utils import entity, handler, user_profile


# MARK: - Setup

class GameState(State):

    def setup(self, dispatcher: Dispatcher, tracker: Tracker):

        # sys_ack
        self.sys_ack_utt, self.sys_ack_tag, self.sys_ack_source = tracker.sys_ack()

        # question/command handling
        self.handler = handler.CommandQuestionHandler(tracker)

        # user_profile
        self.user_profile = user_profile.UserProfile(tracker)

        # entity convenience
        self.game_to_discuss = (
            tracker.volatile_store.get('game_to_discuss') or
            tracker.one_turn_store.get('game_to_discuss') or
            tracker.last_utt_store.get('game_to_discuss') or
            None
        )  # type: Optional[entity.GameItem]
        self.console_to_discuss = (
            tracker.volatile_store.get('console_to_discuss') or
            tracker.one_turn_store.get('console_to_discuss') or
            tracker.last_utt_store.get('console_to_discuss') or
            None
        )  # type: Optional[entity.ConsoleItem]
        self.genre_to_discuss = (
            tracker.volatile_store.get('genre_to_discuss') or
            tracker.one_turn_store.get('genre_to_discuss') or
            tracker.last_utt_store.get('genre_to_discuss') or
            None
        )  # type: Optional[entity.GenreItem]
