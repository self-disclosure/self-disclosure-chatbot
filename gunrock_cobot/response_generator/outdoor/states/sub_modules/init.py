from ..base import *
from ..utils import *
from response_generator.fsm.events import Event, NextState, PreviousState
from response_generator.fsm.utils import Dispatcher, Tracker
from typing import List
from user_profiler.user_profile import UserProfile
from nlu.constants import TopicModule

# Logging
import logging
logger = logging.getLogger("outdoor.states")

class FollowUp(OutdoorState):

    name = 'follow_up'

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        return stop(dispatcher, tracker)

class Init(OutdoorState):

    name = 'init'

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:

        if "first_start" not in tracker.persistent_store:
            ack(dispatcher, tracker)
            tracker.persistent_store["first_start"] = True

        if "avoid_global_catches" in tracker.persistent_store and not "should_ignore_ack" in tracker.persistent_store:
            ack(dispatcher, tracker)

        try:
            del tracker.persistent_store["should_ignore_ack"]
        except:
            pass

        dispatcher.propose_continue("STOP")

        try:
            del tracker.persistent_store["first_start"]
        except:
            pass

        return [NextState("init", jump=False)]