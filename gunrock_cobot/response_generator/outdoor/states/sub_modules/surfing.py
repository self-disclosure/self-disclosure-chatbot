from ..base import *
from ..utils import *
from response_generator.fsm.events import Event, NextState
from response_generator.fsm.utils import Dispatcher, Tracker
from typing import List

import logging
logger = logging.getLogger("outdoor.states")

class SurfingAck(OutdoorState):
    name = "surfing_ack"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        # ack(dispatcher, tracker)
        tracker.persistent_store["avoid_global_catches"] = True
        dispatcher.respond("I love surfing! By my calculations, the average person has a 99.97% chance of falling flat on their face when trying surfing for the first time.")
        return [NextState('init', jump=True)]