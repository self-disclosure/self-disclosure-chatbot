from ..base import *
from ..utils import *
from typing import Optional, List
from response_generator.fsm.events import Event, NextState, PreviousState
from response_generator.fsm.utils import Dispatcher, Tracker
import re

# Logging
import logging
logger = logging.getLogger("outdoor.states")

def avoid(tracker, key="avoid_global_catches"):
    tracker.persistent_store["first_start"] = True
    if key not in tracker.persistent_store:
        tracker.persistent_store["should_ignore_ack"] = True
    return key not in tracker.persistent_store

class WaterPark(OutdoorState):
    name = 'water_park_global'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:
        if re.search(r"\b(water park)\b", tracker.input_text) and avoid(tracker):
            return WaterPark.name
        return None

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        return [NextState("water_park_ack", jump=True)]

class Hiking(OutdoorState):
    name = 'hiking_global'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:
        if re.search(r"\b(hiking|hike)\b", tracker.input_text) and avoid(tracker):
            return Hiking.name
        return None

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        return [NextState("hiking_q1", jump=True)]

class Ski(OutdoorState):
    name = 'skiing'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:
        if re.search(r"\b(ski|snowboard|skiing|snowboarding)\b", tracker.input_text) and avoid(tracker):
            return Ski.name
        return None

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        return [NextState("ski_ack", jump=True)]

class Walk(OutdoorState):
    name = 'walking'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:
        if re.search(r"\b(walk|jog|walking|walks)\b", tracker.input_text) and avoid(tracker):
            return Walk.name
        return None

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        return [NextState("walk_ack", jump=True)]

class Biking(OutdoorState):
    name = 'biking'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:
        if re.search(r"\b(biking|bike|cycle|cycling|bicycle|bikes|bicycles|cycles)\b", tracker.input_text) and avoid(tracker):
            return Biking.name
        return None

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        return [NextState("biking_ack", jump=True)]

class Surfing(OutdoorState):
    name = 'surfing'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:
        if re.search(r"\b(surfing|surf)\b", tracker.input_text) and avoid(tracker):
            return Surfing.name
        return None

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        return [NextState("surfing_ack", jump=True)]

class Fishing(OutdoorState):
    name = 'fishing'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:
        if re.search(r"\b(fishing|fish)\b", tracker.input_text) and avoid(tracker):
            return Fishing.name
        return None

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        return [NextState("fishing_ack", jump=True)]

class Kayaking(OutdoorState):
    name = 'kayaking'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:
        if re.search(r"\b(kayaking|kayake|canoe|canoing)\b", tracker.input_text) and avoid(tracker):
            return Kayaking.name
        return None

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        return [NextState("kayaking_ack", jump=True)]

class GoToPark(OutdoorState):
    name = 'park'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:
        if re.search(r"\b(go to the park|walk in the park|the park)\b", tracker.input_text) and avoid(tracker):
            return GoToPark.name
        return None

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        return [NextState("park_ack", jump=True)]

class PlayOutside(OutdoorState):
    name = 'play_outside'
    special_type = 'global_state'

    def enter_condition(self, dispatcher: Dispatcher, tracker: Tracker) -> Optional[str]:
        if re.search(r"\b(play outside|play with friends|playground|play ground)\b", tracker.input_text) and avoid(tracker):
            return PlayOutside.name
        return None

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        return [NextState("play_outside_ack", jump=True)]
