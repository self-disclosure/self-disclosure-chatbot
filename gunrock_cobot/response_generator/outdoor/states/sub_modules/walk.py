from ..base import *
from ..utils import *
from response_generator.fsm.events import Event, NextState
from response_generator.fsm.utils import Dispatcher, Tracker
from typing import List

import logging
logger = logging.getLogger("outdoor.states")

class WalkAck(OutdoorState):
    name = "walk_ack"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        # ack(dispatcher, tracker)
        tracker.persistent_store["avoid_global_catches"] = True
        dispatcher.respond("Going for a walk is a healthy activity. Research shows it reduces stress and improves ones mood!")
        return [NextState('init', jump=True)]