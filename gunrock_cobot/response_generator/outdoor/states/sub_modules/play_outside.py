from ..base import *
from ..utils import *
from response_generator.fsm.events import Event, NextState
from response_generator.fsm.utils import Dispatcher, Tracker
from typing import List

import logging
logger = logging.getLogger("outdoor.states")

class PlayOutsideAck(OutdoorState):
    name = "play_outside_ack"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        # ack(dispatcher, tracker)
        tracker.persistent_store["avoid_global_catches"] = True
        dispatcher.respond("I like to play outside with my friends too! I like to go on the swing in the playground.")
        return [NextState('init', jump=True)]