from ..base import *
from ..utils import *
from response_generator.fsm.events import Event, NextState
from response_generator.fsm.utils import Dispatcher, Tracker
from typing import List
import re
from nlu.constants import TopicModule
from selecting_strategy.module_selection import ModuleSelector
import logging

logger = logging.getLogger("outdoor.states")

class HikingQ1(OutdoorState):
    name = "hiking_q1"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        ack(dispatcher, tracker)
        tracker.persistent_store["avoid_global_catches"] = True
        dispatcher.respond("Hiking is one of my favorite outdoor activities. Are you a frequent hiker?")
        return [NextState('hiking_q2', jump=False)]

class HikingQ2(OutdoorState):
    name = "hiking_q2"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:

        ack(dispatcher, tracker)

        if is_unsure(tracker):
            dispatcher.respond("So, do you like any other sports or outdoor activities?")
            dispatcher.propose_continue('UNCLEAR', TopicModule.SPORTS)
            try:
                del tracker.persistent_store["should_ignore_ack"]
            except:
                pass
            return [NextState('init', jump=False)]
        elif is_pos(tracker) or is_neu(tracker):
            dispatcher.respond("Hiking definitely helps me find peace of mind. I can see why you do it pretty often.")
        elif is_neg(tracker):
            dispatcher.respond("I don't think I would be either. Getting an occasional breath of fresh air helps free up my mind in times of stress.")

        dispatcher.respond("Do you usually go on challenging hikes? Or do you prefer calmer, shorter hikes?")

        return [NextState('hiking_q3', jump=False)]

class HikingQ3(OutdoorState):
    name = "hiking_q3"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:

        # Ack
        ack(dispatcher, tracker)

        is_challenging = False

        # Detect challenging
        for term in ["challenging", "challenge", "difficult", "hard", "push myself", "long"]:
            if re.search(re.compile(f"{term}"), tracker.input_text):
                dispatcher.respond("Hiking helps strengthen your core, and research suggests that it also lowers risk of heart disease.")
                is_challenging = True
                break

        # Detect calmer
        for term in ["calm", "relaxing", "relax", "walk", "short", "brisk", "not to", "not too", "quick", "fast"]:
            if re.search(re.compile(f"{term}"), tracker.input_text) and not is_challenging:
                dispatcher.respond("That makes two of us. Relaxing breaths of fresh air help free up my mind when my circuits are overwhelmed.")
                break

        # Next question
        dispatcher.respond("Where do you usually go hiking?")

        return [NextState('hiking_q4', jump=False)]

class HikingQ4(OutdoorState):
    name = "hiking_q4"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:

        noun = None
        results = concepts(tracker)
        for result in results:
            for x in ["area", "destination", "location", 'place', 'park']:
                if x in result[1]: noun = result[0]

        # Detect unsure
        if is_unsure(tracker): ack(dispatcher, tracker)

        # Noun
        if noun:
            dispatcher.respond(f"That sounds like a good time, I'll look into {noun}.")
        elif re.search(r"(.*)(outside|outdoors)(.*)", tracker.input_text):
            dispatcher.respond("Yeah, hiking outside is nice.")
        elif re.search(r"(.*)(mountain)(.*)", tracker.input_text):
            dispatcher.respond("Yeah, hiking in the mountains is nice.")
        elif re.search(r"(.*)(hill)(.*)", tracker.input_text):
            dispatcher.respond("Yeah, hiking in the hills is nice.")
        elif re.search(r"(.*)(anywhere)(.*)", tracker.input_text):
            dispatcher.respond("Anywhere? It's nice to be flexible to hike anywhere.")
        elif re.search(r"(.*)(nowhere)(.*)", tracker.input_text):
            dispatcher.respond("Nowhere? I'd recommend Yosemite, its my favourite place to hike. By the way, ")
        else:
            dispatcher.respond("Oh, I think changing up the location keeps it fun and engaging.")

        # For hiking we are an exception!
        try:
            del tracker.persistent_store["should_ignore_ack"]
        except:
            pass

        # Check if animal has been proposed already
        user_attributes_ref = tracker.module.response_generator_ref.state_manager.user_attributes
        module_selector = ModuleSelector(user_attributes_ref)
        used_topic = module_selector.used_topic_modules

        if TopicModule.ANIMAL.value not in used_topic:
            # Propose animal
            dispatcher.respond("Some people like to bring their dogs along on the hike. Do you have a dog or a pet?")
            dispatcher.propose_continue('STOP', TopicModule.ANIMAL)
        else:
            dispatcher.propose_continue("STOP")

        return [NextState('init', jump=False)]