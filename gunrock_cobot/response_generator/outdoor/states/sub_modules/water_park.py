from ..base import *
from ..utils import *
from response_generator.fsm.events import Event, NextState
from response_generator.fsm.utils import Dispatcher, Tracker
from typing import List

import logging
logger = logging.getLogger("outdoor.states")

class WaterParkAck(OutdoorState):
    name = "water_park_ack"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        tracker.persistent_store["avoid_global_catches"] = True
        dispatcher.respond("I love going to the water park! Especially the water slides!")
        return [NextState('init', jump=True)]