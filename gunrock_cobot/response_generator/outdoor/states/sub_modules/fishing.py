from ..base import *
from ..utils import *
from response_generator.fsm.events import Event, NextState
from response_generator.fsm.utils import Dispatcher, Tracker
from typing import List

import logging
logger = logging.getLogger("outdoor.states")

class FishingAck(OutdoorState):
    name = "fishing_ack"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        # ack(dispatcher, tracker)
        tracker.persistent_store["avoid_global_catches"] = True
        dispatcher.respond("Ah, I love to go fishing! I learned there are over 30,000 different species of fish in the ocean.")
        return [NextState('init', jump=True)]