from ..base import *
from ..utils import *
from response_generator.fsm.events import Event, NextState
from response_generator.fsm.utils import Dispatcher, Tracker
from typing import List
import re
import logging

logger = logging.getLogger("outdoor.states")

class SkiAck(OutdoorState):
    name = "ski_ack"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        # ack(dispatcher, tracker)
        tracker.persistent_store["avoid_global_catches"] = True
        dispatcher.respond("Skiing can be really fun, especially skiing through powder where the snow is all light and fluffy.")
        dispatcher.respond("So, do you prefer to go on green, blue, or black diamond runs?")
        return [NextState('ski_q1', jump=False)]


class SkiQ1(OutdoorState):
    name = "ski_q1"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        green = re.search(r"\b(green)\b", tracker.input_text.lower())
        blue = re.search(r"\b(blue)\b", tracker.input_text.lower())
        black = re.search(r"\b(black|diamond)\b", tracker.input_text.lower())

        if green and not (blue or black):
            dispatcher.respond("That's great. Green runs are nice for learning how to ski.")
        elif blue and not (green or black):
            dispatcher.respond("That's great. Blue runs are nice because they're not too easy, but they're not too hard either, they're a good challenge!")
        elif black and not (green or blue):
            dispatcher.respond("Wow, that's crazy! Black diamond runs are really difficult. I'm impressed.")
        else:
            ack(dispatcher, tracker)

        dispatcher.respond("Do you have a favourite place you like to go skiing at?")
        return [NextState('ski_q2', jump=False)]

class SkiQ2(OutdoorState):
    name = "ski_q2"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:

        # Code for detecting a noun
        noun = None
        results = concepts(tracker)
        for result in results:
            for x in ["area", "destination", "location", 'place', 'park', "mountain", "resort", "skiing", "vacation", "snow", "lake", 'adirondack mountain attraction', 'high peak', 'client', 'community', 'town']:
                if x in result[1]: noun = result[0]

        # Detect unsure
        if noun:
            dispatcher.respond(f"Oh, I know about {noun}, I love that place.")
        else:
            ack(dispatcher, tracker)

        return [NextState('init', jump=True)]