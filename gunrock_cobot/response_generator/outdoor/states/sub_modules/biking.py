from ..base import *
from ..utils import *
from response_generator.fsm.events import Event, NextState
from response_generator.fsm.utils import Dispatcher, Tracker
from typing import List
import re
import logging

logger = logging.getLogger("outdoor.states")

class BikingAck(OutdoorState):
    name = "biking_ack"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        tracker.persistent_store["avoid_global_catches"] = True
        # dispatcher.respond("Biking sounds like a lot of fun, although it would be hard for me to ride a bike because I have no arms or legs.")
        dispatcher.respond("I love to go biking!")
        dispatcher.respond("Do you prefer to go biking with friends or by yourself?")
        return [NextState('biking_q1', jump=False)]


class BikingQ1(OutdoorState):
    name = "biking_q1"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        friends = re.search(r"\b(friend|friends)\b", tracker.input_text.lower())
        alone = re.search(r"\b(by myself|alone|without anyone)\b", tracker.input_text.lower())
        both = re.search(r"\b(both|i don't mind|i dont mind|i don't care|i dont care|either)\b", tracker.input_text.lower())

        if friends:
            dispatcher.respond("Yeah, biking with friends is always fun.")
        elif alone:
            dispatcher.respond("Yeah, I get you. Biking by yourself can be relaxing.")
        else:
            ack(dispatcher, tracker)
            dispatcher.respond("Biking with friends or by yourself are both nice.")

        dispatcher.respond("Do you prefer to ride your bike really fast, or take a more relaxing ride?")

        return [NextState('biking_q2', jump=False)]

class BikingQ2(OutdoorState):
    name = "biking_q2"

    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        fast = re.search(r"\b(fast|down hill|quick|intense|fun|faster)\b", tracker.input_text.lower())
        slow = re.search(r"\b(calm|relaxing|slow|relax)\b", tracker.input_text.lower())

        if fast:
            dispatcher.respond("Fast bike rides are a lot of fun!")
        elif slow:
            dispatcher.respond("It's nice to have more relaxing rides sometimes.")
        else:
            ack(dispatcher, tracker)

        return [NextState('init', jump=True)]