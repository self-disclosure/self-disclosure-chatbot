from .base import *
import json
import logging
import re
from dataclasses import dataclass, field, InitVar
from typing import Callable, Dict, List, Optional, TYPE_CHECKING, Type
from nlu.intentmap_scheme import lexical_re_patterns
from nlu.intentmap_scheme import sys_re_patterns
from response_generator.fsm.events import Event, NextState, PreviousState
from response_generator.fsm.utils import Dispatcher, Tracker
from response_generator.fsm.state import State
from nlu.util_nlu import ReturnNLPSegment
from user_profiler.user_profile import UserProfile
from nlu.constants import *

import logging
logger = logging.getLogger("outdoor.states")

def ack(dispatcher: Dispatcher, tracker: Tracker):
    if is_unsure(tracker): dispatcher.respond_template("ack/uncertain", {})
    elif is_pos(tracker): dispatcher.respond_template("ack/pos", {})
    elif is_neg(tracker): dispatcher.respond_template("ack/neg", {})
    elif is_neu(tracker): dispatcher.respond_template("ack/neu", {})

def is_pos(tracker: Tracker):
    if tracker.returnnlp.answer_positivity is Positivity.pos: return True
    return False

def is_neu(tracker: Tracker):
    if tracker.returnnlp.answer_positivity is Positivity.neu: return True
    return False

def is_neg(tracker: Tracker):
    if tracker.returnnlp.answer_positivity is Positivity.neg: return True
    return False

def is_unsure(tracker: Tracker):
    if re.search(lexical_re_patterns["ans_unknown"], tracker.input_text) \
    or re.search(
        r"(.*)(i'm not sure|im not sure|i am not sure|i dont know|i don't know|i do not know)(.*)",
        tracker.input_text
    ): return True
    return False

def first_noun(tracker: Tracker):
    possessive_pronouns = ['their', 'him', 'her', 'our', 'your', 'it', 'mine', 'a', "i", 'like', 'wear', 'to']

    try:
        noun_phrase = tracker.returnnlp.noun_phrase[0][0]

        for pro_noun in possessive_pronouns:
            noun_phrase = noun_phrase.replace(pro_noun+' ', '')
            noun_phrase = noun_phrase.replace(pro_noun + '\'s ', '')
            noun_phrase = noun_phrase.replace(pro_noun + 's ', '')

        if noun_phrase.split(" ")[0].lower() == "the":
            noun_phrase = noun_phrase.replace('the ', '', 1)
        return noun_phrase

    except Exception as error:
        logger.info(error)
        return ""

def stop(dispatcher: Dispatcher, tracker: Tracker):
    ack(dispatcher, tracker)
    dispatcher.propose_continue("STOP")
    return [NextState("init", jump=False)]

def concepts(tracker):
    result = []
    for x in tracker.returnnlp.concept:
        for y in x:
            try:
                result.append((y.noun, [z.description for z in y.data]))
            except:
                pass
    return result