from response_generator.fsm.state import State
from response_generator.fsm.events import NextState, Event
from response_generator.fsm.utils import Dispatcher, Tracker
from typing import Optional, List

# Logging
import logging
logger = logging.getLogger("outdoor.states")

class OutdoorState(State):
    def run(self, dispatcher: Dispatcher, tracker: Tracker) -> List[Event]:
        pass