from cobot_core.service_module import LocalServiceModule
import response_generator.outdoor.outdoorchat as outdoorchat
import logging

logger = logging.getLogger(__name__)

class OutdoorResponseGeneratorLocal(LocalServiceModule):
    def execute(self):
        module = outdoorchat.OutdoorModule(self)
        response = module.generate_response()
        return response
