"""Register Segments in AWS Xray
"""

from aws_xray_sdk.core import xray_recorder


def register_segments():
    with xray_recorder.in_segment('gunrock_cobot') as segment:
        # Add metadata or annotation here if necessary
        segment.put_metadata('system', str, '[SYSTEM]')
        with xray_recorder.in_subsegment('[NLP Modules]') as subsegment:
            subsegment.put_annotation('nlp_module', 'start')
            # Do something here