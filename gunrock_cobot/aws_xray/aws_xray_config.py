from aws_xray_sdk.core import xray_recorder

# AWS_XRAY_TRACING_NAME=GUNROCK_COBOT
# AWS_XRAY_CONTEXT_MISSING
# AWS_XRAY_DAEMON_ADDRESS

def config():
    xray_recorder.configure(
        sampling=False,
        context_missing='LOG_ERROR',
        plugins=('EC2Plugin',),
        daemon_address='ec2-34-229-107-43.compute-1.amazonaws.com:2000',
    )