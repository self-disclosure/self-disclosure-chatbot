import re
import logging

from cobot_core.asr_processor import ASRProcessor
import template_manager
from nlu.intent_classifier import IntentClassifier
from utils import _previous_repeat_requests_count, print_log_for_local_env, \
    save_template_keys_to_current_state_and_reset, process_prosody, is_common_name

logger = logging.getLogger(__name__)

error_template = template_manager.Templates.error
template = template_manager.Templates.sys_rg

class CustomASRProcessor(ASRProcessor):
    @staticmethod
    def _correct_covid_mispronunciation(text):
        CORONA_SUFFIXES = [
                "bar", "barbara", "barbaras", "barca", "barrasser", "bars",
                "bears", "beatles", "beira", "bias", "biros", "boris",
                "bowers", "bratworse", "burgers", "butter", "butters", "buyers",
                "by louis", "by earth", "byrers", "bye",
                "ever irish",
                "files", "fires", "fireston", "flowers", "fyrus",
                "harris",
                "iris", "irish",
                "maris", "mars", "my list", "myers",
                "paris", "pirates", "powers",
                "raivis", "rice", "riders",
                "vargas", "various", "venice", "verse", "versed", "versus",
                "vice", "violence", "violet",
                "waitress", "wallets", "waters", "we're not", "wireless", "wires",
                "wiress", "writer us", "writers", "wyatt",
                ]
        NINETEEN_PREFIXES = [
                "carova", "carpet", "carving",
                "clovis", "clovy",
                "club", "cobick", "cobi", "cocovan", "coded", "code", "coding",
                "coffee", "coke", "cokie", "colbert", "colbie", "colby", "cold", "colvig", "cold in",
                "colvin", "come", "comic", "common", "confident", "convict", "cope", "copic",
                "corbin", "corona", "corvette", "corvite", "cough", "covan", "covered", "cover", "corbett",
                "covert", "covet", "covidh", "covin", "covista", "kovacs", "kovic", "kaven", "kobe",
                "covan dash",
                ]
        VIRUS_PREFIXES = [
                "cabrona", "called", "canada", "canola", "caranova", "carano",
                "career", "caribbean", "carnivirous", "carrand", "car", "clone the",
                "chore", "chromebook", "chrome", "chronic", "chrono",
                "close", "clothes", "coded", "cold", "cologne", "colonel", "cold in",
                "colorado", "color", "commander", "comola", "cooler", "coretta",
                "cornell", "corner", "corolla", "coronal", "coronary", "coronas",
                "corroto", "cortana", "corved", "covered",
                "creative", "credit", "crew", "crime", "cross", "crowbar",
                "crowded", "crowder", "crown", "crow", "cry", "current", "quran a", "karen's",
                ]
        PANDEMIC_PREFIXES = [
                "coded",
                ]

        for sf in CORONA_SUFFIXES:
            text = re.sub(fr'\bcorona {sf}\b', 'corona virus', text)
        for pf in NINETEEN_PREFIXES:
            text = re.sub(fr'\b{pf} nineteen\b', 'covid nineteen', text)
        for pf in PANDEMIC_PREFIXES:
            text = re.sub(fr'\b{pf} pandemic\b', 'covid pandemic', text)
        for pf in VIRUS_PREFIXES:
            text = re.sub(fr'\b{pf} virus\b', 'corona virus', text)
            text = re.sub(fr'\b{pf} viruses\b', 'corona virus', text)

        return text

    def process(self, input=None):
        text = self.state_manager.current_state.text
        logger.info("[ASR] input text: " + text)

        if not text:
            return None

        text = self._correct_covid_mispronunciation(text)
        self.state_manager.current_state.text = text
        logger.info("[ASR] input text corrected: " + text)

        if self.state_manager.current_state.request_type == 'LaunchRequest':
            return None
        try:
            intent_classifier = IntentClassifier(text)

            if intent_classifier.is_back_channeling_expression():
                return None
            if intent_classifier.is_whitelisted_ner_expression():
                return None

            repeat_requests = _previous_repeat_requests_count(self.state_manager.session_history)

            if intent_classifier.is_incomplete_sentence():
                if repeat_requests >= 2:
                    return None
                setattr(self.state_manager.current_state, "resp_type", "incomplete")
                response = template.utterance(selector=['preprocess', 'not_complete'], slots={},
                                              user_attributes_ref=self.state_manager.user_attributes)
                save_template_keys_to_current_state_and_reset(
                    self.state_manager)
                print_log_for_local_env(
                    "Response handled by CustomASRProcessor - incomplete utterance handler")
                return process_prosody(response, self.state_manager)

            if intent_classifier.is_hesitant():
                setattr(self.state_manager.current_state, "resp_type", "hesitant")
                response = template.utterance(selector=['preprocess', 'hesitate'], slots={},
                                              user_attributes_ref=self.state_manager.user_attributes)
                save_template_keys_to_current_state_and_reset(
                    self.state_manager)
                print_log_for_local_env(
                    "Response handled by CustomASRProcessor - hesitant utterance handler")
                return process_prosody(response, self.state_manager)

            if intent_classifier.is_request_allow_tell_sth():
                setattr(self.state_manager.current_state, "resp_type", "req_allow_tell_sth")
                response = template.utterance(selector=['preprocess', 'req_allow_tell_sth'], slots={},
                                              user_attributes_ref=self.state_manager.user_attributes)
                save_template_keys_to_current_state_and_reset(self.state_manager)
                print_log_for_local_env(
                    "Response handled by CustomASRProcessor - req_allow_tell_sth handler")
                return process_prosody(response, self.state_manager)

            if intent_classifier.is_req_ask_question():
                setattr(self.state_manager.current_state, "resp_type", "req_allow_ask_question")
                response = template.utterance(selector=['preprocess', 'req_allow_ask_question'], slots={},
                                              user_attributes_ref=self.state_manager.user_attributes)
                save_template_keys_to_current_state_and_reset(
                    self.state_manager)
                print_log_for_local_env(
                    "Response handled by CustomASRProcessor - req_allow_ask_question handler")
                return process_prosody(response, self.state_manager)

            if self._is_asr_input():
                logger.info("[ASR] asr enabled asr: {}".format(
                    self.state_manager.current_state.asr))

                if repeat_requests >= 2:
                    return None

                # NOTE: checking for names is also done in _is_low_asr_score_v2
                # session_history = self.state_manager.session_history
                # if session_history:
                #     error_state_response = self.state_manager.session_history[repeat_requests]["response"].lower()
                #     if 'your name' in error_state_response and any([is_common_name(w) for w in text.split()]):
                #         return None
                #     if is_common_name(text):
                #         return None

                if self._is_low_asr_score_v2():
                    # Check profanity first to avoid bot repeating profanity word from user
                    if intent_classifier.is_profanity():
                        logging.info('[Utils] ASR preprocess with profane results: {}'.format(text))
                        setattr(self.state_manager.current_state, "resp_type", "profanity_request")
                        response = error_template.utterance(selector='profanity_req', slots={},
                                                            user_attributes_ref=self.state_manager.user_attributes)
                        save_template_keys_to_current_state_and_reset(self.state_manager)
                        print_log_for_local_env(
                            "Response handled by CustomASRProcessor - profanity utterance handler")
                        return process_prosody(response, self.state_manager)

                    elif intent_classifier.is_coronavirus_related():
                        return None
                    else:
                        setattr(self.state_manager.current_state, "resp_type", "asr_error")
                        response = error_template.utterance(selector='asr_processor/asr_error', slots={"request": text},
                                                            user_attributes_ref=self.state_manager.user_attributes)
                        save_template_keys_to_current_state_and_reset(self.state_manager)
                        return process_prosody(response, self.state_manager)

                return None
        except Exception as e:
            logger.error(
                "[ASR] asr error exception: {}".format(e), exc_info=True)
            return None

        return None

    def _is_asr_input(self):
        """
        Sample asr input:
        [
           {
              "tokens":[
                 {
                    "value":"lets",
                    "confidence":0.8,
                    "startOffsetInMilliseconds":1000,
                    "endOffsetInMilliseconds":2000
                 },
                 {
                    "value":"talk",
                    "confidence":0.8,
                    "startOffsetInMilliseconds":3000,
                    "endOffsetInMilliseconds":4000
                 },
                 {
                    "value":"about",
                    "confidence":0.8,
                    "startOffsetInMilliseconds":5000,
                    "endOffsetInMilliseconds":6000
                 },
                 {
                    "value":"movies",
                    "confidence":0.8,
                    "startOffsetInMilliseconds":7000,
                    "endOffsetInMilliseconds":8000
                 }
              ],
              "confidence":0.09
           }
        ]
        """
        return isinstance(self.state_manager.current_state.asr, list) and len(self.state_manager.current_state.asr) > 0

    def _is_low_asr_score_v2(self):
        CONFIDENCE_THRESHOLD = .1
        COVERAGE_THRESHOLD = .34

        def _should_ignore(token):
            if token.endswith('.'):
                return True
            if token in {"he's", "she's", "they're", "you're", "i'm", "i'll", "i'd", "i've", "that's", "it's", "who's", "ma'am"}:
                return True
            if token in {'na', 'uh', 'tv', 'dmv', 'rpg', 'paparazzi'}:
                return True
            return False

        asr_hyp = self.state_manager.current_state.asr[0]
        if asr_hyp.get('confidence', 1) > CONFIDENCE_THRESHOLD:
            return False

        length = 1. * len(asr_hyp['tokens'])
        low_conf_tokens = [t['value'] for t in asr_hyp['tokens'] if t['confidence'] < CONFIDENCE_THRESHOLD]
        low_conf_tokens = [t for t in low_conf_tokens if not _should_ignore(t)]
        if (len(low_conf_tokens) / length) < COVERAGE_THRESHOLD:
            return False

        low_conf_tokens = [t for t in low_conf_tokens if not is_common_name(t)]
        if (len(low_conf_tokens) / length) < COVERAGE_THRESHOLD:
            return False

        return True
