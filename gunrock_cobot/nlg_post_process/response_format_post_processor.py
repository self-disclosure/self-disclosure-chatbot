from cobot_core.log.logger import LoggerFactory  # noqa


class ResponseFormatProcessor:
    def __init__(self, response):
        self.response = response
        self.logger = LoggerFactory.setup(self)

    def process(self):
        self.logger.info("[ResponseFormatProcessor] start processing response: {}".format(self.response))

        self._remove_malformed_empty_content()
        self.remove_no_user_name_tag()
        self.adjust_punctuation()

        self.logger.info("[ResponseFormatProcessor] response after process: {}".format(self.response))
        return self.response

    def _remove_malformed_empty_content(self):
        self.response \
            .replace('\{\}', "") \
            .replace('{}', "")

    def remove_no_user_name_tag(self):
        self.response \
            .replace('<no_name>', "")

    def adjust_punctuation(self):
        self.response = self.response \
            .replace('&', ' and ') \
            .replace('_', " ") \
            .replace(' - ', '-') \
            .replace('`', "") \
            .replace('\{\}', "") \
            .replace('{}', "") \
            .replace('\' .', "\'.")
