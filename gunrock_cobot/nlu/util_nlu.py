import json
import logging
import warnings

# compatibility
from .dataclass import ReturnNLP, ReturnNLPSegment, CentralElement  # noqa: F401

logger = logging.getLogger(__name__)

pos_key = "pos"
neg_key = "neg"
neu_key = "neu"
other_key = "others"
sentiment_attitude_map = {
    "pos": pos_key,
    "neg": neg_key,
    "neu": neu_key,
    "compound": pos_key,
    "unknown": other_key
}


# TODO: Migrate to new Returnnlp
# find if a tag is in the return nlp dialog act
def in_dialog_act(tag, features, threshold=0.5):
    """Deprecated"""
    warnings.warn("Use ReturnNLP.", DeprecationWarning, stacklevel=2)

    if (features is not None and "returnnlp" in features and isinstance(features["returnnlp"], list)):
        for sentences in features["returnnlp"]:
            if (isinstance(sentences, dict) and "dialog_act" in sentences and sentences[
                    "dialog_act"] is None and isinstance(sentences["dialog_act"], list)):
                continue
            if (sentences["dialog_act"][0] == tag and float(sentences["dialog_act"][1]) > threshold):
                return True
    return False

def get_feature_by_key(state, key):
    """Deprecated"""
    warnings.warn("Use ReturnNLP.", DeprecationWarning, stacklevel=2)

    logging.debug(
        "[NLU] Get feature with key: {}".format(key, state))
    features_dict = json.loads(state.to_json())["features"]
    return features_dict.get(
        key, None) if features_dict[key] is not None else None


def get_sentiment(state, lexical):
    """Deprecated"""
    warnings.warn("Use ReturnNLP.", DeprecationWarning, stacklevel=2)

    # TODO: define specific sentiment
    senti = get_feature_by_key(state, 'sentiment')
    logging.debug(
        "[NLU] get sentiment current: {}".format(senti))
    if senti is None:
        return "unknown"
    if "pos" in senti or "neg" in senti:
        logging.debug(
            "[NLU] get sentiment: {}".format(senti))
        return senti
    if "compound" in senti:
        logging.debug(
            "[NLU] get sentiment: pos from coumpound")
        return "pos"
    if 'ans_like' in lexical or 'ans_pos' in lexical:
        logging.debug(
            "[NLU] get sentiment: pos from lexical")
        return 'pos'
    if 'ans_neg' in lexical:
        logging.debug(
            "[NLU] get sentiment: neg from lexical")
        return 'neg'
    logging.debug(
        "[NLU] get sentiment: unknown")
    return "unknown"


def get_sentiment_key(sentiment, lexical):
    """Deprecated"""
    warnings.warn("Use ReturnNLP.", DeprecationWarning, stacklevel=2)

    # TODO: fine tune
    logging.info(
        "[NLU] Get sentiment key with sentiment: {}, lexical: {}".format(
            sentiment, lexical))
    if 'ans_like' in lexical or 'ans_pos' in lexical:
        return sentiment_attitude_map["pos"]
    if 'ans_neg' in lexical:
        return sentiment_attitude_map["neg"]
    if 'ans_unknown' in lexical:
        return sentiment_attitude_map["unknown"]
    if type(sentiment) is list:
        sentiment = "neu"
    if sentiment in sentiment_attitude_map:
        return sentiment_attitude_map[sentiment]
    return other_key


def clean_nlp_inputs(input_data):
    """Retrieve NLU metadata

    Parameters:
        input_data:  raw input dictionary from system module, msg in handle_msg in response generator

    Set Instance Variables:
        central_e - central element
        returnnlp - returnnlp

    Output:
        None
    """
    if isinstance(input_data["central_elem"], list):
        central_elem = input_data["central_elem"][0]
    else:
        central_elem = input_data["central_elem"]

    def remove_none_data(data):
        """remove none value in data"""
        for key in data:
            if data[key] is None:
                data[key] = {}
        return data
    length = len(input_data["returnnlp"])
    if length == 1 and isinstance(input_data["returnnlp"][0], list):
        returnnlp = input_data["returnnlp"][0]
    else:
        returnnlp = input_data["returnnlp"]
    output_returnnlp = []

    def get_returnnlp(nlp):
        """process special nlp data"""
        if nlp is None:
            return
        if isinstance(nlp, dict):
            output_returnnlp.append(remove_none_data(nlp))
        if isinstance(nlp, list):
            for val in nlp:
                get_returnnlp(val)
    for nlp in returnnlp:
        get_returnnlp(nlp)
    logger.info("Retrieve NLU Data Successful")
