from enum import Enum
import re

class CommonActivity(Enum):
    unknown = 0
    movie = 1  # watch movies
    tv = 2  # watch tv
    walk = 3  # go for walks
    games = 4  # play any type of game
    mall = 5  # go shopping, go to the mall
    play = 6  # play outside, etc
    hangout = 7  # just spending time together, hanging out
    talk = 8
    netflix = 9
    sport = 10
    shopping = 11
    photography = 12
    singing = 13
    facetime = 14
    dancing = 15
    # text
    # play sport

class FriendshipActivityClassifier:

    @classmethod
    def classify(cls, input_text) -> CommonActivity:
        if re.search(r"\b(movie|movies)\b", input_text): return CommonActivity.movie
        if re.search(r"\b(tv|television)\b", input_text): return CommonActivity.tv
        if re.search(r"\b(walk|walks)\b", input_text): return CommonActivity.walk
        if re.search(r"\b(game|games|video games)\b", input_text): return CommonActivity.games
        if re.search(r"\b(mall)\b", input_text): return CommonActivity.mall
        if re.search(r"\b(hangout|spend time)\b", input_text): return CommonActivity.hangout
        if re.search(r"\b(talk|chat)\b", input_text): return CommonActivity.talk
        if re.search(r"\b(netflix)\b", input_text): return CommonActivity.netflix
        if re.search(r"\b(sport|sports|basketball|soccer|football|tennis)\b",input_text): return CommonActivity.sport
        if re.search(r"\b(play|play outside)\b", input_text): return CommonActivity.play
        if re.search(r"\b(shopping|shop)\b", input_text): return CommonActivity.shopping
        if re.search(r"\b(photo|photos|photography|pictures|picture)\b", input_text): return CommonActivity.photography
        if re.search(r"\b(sing|singing|song|songs)\b", input_text): return CommonActivity.singing
        if re.search(r"\b(video call|facetime|face time)\b", input_text): return CommonActivity.facetime
        if re.search(r"\b(dance|dancing)\b", input_text): return CommonActivity.dancing
        return CommonActivity.unknown
