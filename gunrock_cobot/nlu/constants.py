from enum import auto, Enum, Flag, unique
from typing import Set, Union

from classproperty import classproperty


@unique
class DialogAct(Enum):
    OPEN_QUESTION_FACTUAL = "open_question_factual"
    OPEN_QUESTION_OPINION = "open_question_opinion"
    OPEN_QUESTION = "open_question"
    YES_NO_QUESTION = "yes_no_question"
    POS_ANSWER = "pos_answer"
    NEG_ANSWER = "neg_answer"
    OTHER_ANSWERS = "other_answers"
    STATEMENT = "statement"
    OPINION = "opinion"
    COMMENT = "comment"
    COMMANDS = "commands"
    DEV_COMMAND = "dev_command"
    BACK_CHANNELING = "back-channeling"
    APPRECIATION = "appreciation"
    ABANDONED = "abandoned"
    OPENING = "opening"
    CLOSING = "closing"
    HOLD = "hold"
    COMPLAINT = "complaint"
    THANKING = "thanking"
    APOLOGY = "apology"
    RESPOND_TO_APOLOGY = "respond_to_apology"
    NONSENSE = "nonsense"
    OTHER = "other"
    NA = "NA"


@unique
class Positivity(Flag):
    pos = auto()
    neg = auto()
    neu = auto()

    def __bool__(self):
        return self == Positivity.pos

    def __hash__(self):
        return self.value

    def __eq__(self, other):
        if isinstance(other, Positivity):
            return self is other
        elif isinstance(other, str):
            return self.name == other
        else:
            return super().__eq__(other)

    def as_int(self):
        return (
            1 if self is Positivity.pos else
            -1 if self is Positivity.neg else
            0
        )

    @classmethod
    def from_int(self, o: Union[int, float]):
        return (
            Positivity.pos if o >= 1 else
            Positivity.neg if o <= -1 else
            Positivity.neu
        )


@unique
class TopicModule(Enum):

    # Social related modules
    LAUNCHGREETING = 'LAUNCHGREETING'
    SOCIAL = 'SOCIAL'
    PHATIC = 'PHATIC'
    DAILY_LIFE = 'DAILYLIFE'
    OUTDOOR = 'OUTDOORCHAT'
    SELFDISCLOSURE = 'SELFDISCLOSURE'

    # Emotional modules - comfort or funny content
    SAY_COMFORT = 'SAY_COMFORT'
    SAY_FUNNY = 'SAY_FUNNY'  # Jokes
    FUNFACT = 'STORYFUNFACT'

    # Main topic module
    MOVIE = 'MOVIECHAT'
    BOOK = 'BOOKCHAT'
    MUSIC = 'MUSICCHAT'
    GAME = 'GAMECHAT'
    NEWS = 'NEWS'
    TRAVEL = 'TRAVELCHAT'
    TECHSCI = 'TECHSCIENCECHAT'
    SPORTS = 'SPORT'
    ANIMAL = 'ANIMALCHAT'
    FOOD = 'FOODCHAT'
    FASHION = 'FASHIONCHAT'
    RETRIEVAL = 'RETRIEVAL'

    # Small modules
    HOLIDAY = 'HOLIDAYCHAT'
    WEATHER = 'WEATHER'

    # Device function or one turn service modules
    ADJUST_SPEAK = 'ADJUSTSPEAK'
    TERMINATE = 'TERMINATE'
    REQ_PLAY_GAME = 'PLAY_GAME'
    REQ_TASK = 'REQ_TASK'
    CLARIFICATION = 'CLARIFICATION'
    CONTROVERSIAL = 'CONTROVERSIALOPION'

    # Deprecated?
    REQ_PLAY_MUSIC = 'PLAY_MUSIC'
    REQ_MORE = 'REQ_MORE'
    REQ_TIME = 'REQ_TIME'
    REQ_EASTER_EGG = 'REQ_EASTEREGG'
    REQ_LOC = 'REQ_LOC'
    EVI = 'EVI'
    SAY_THANKS = 'SAY_THANKS'

    # Self-disclosure study
    COVIDCHAT = 'COVIDCHAT'

    NA = 'NA'

    @property
    def is_proposable(self) -> bool:
        from selecting_strategy.module_selection import topics_available_for_proposal
        return self.value in topics_available_for_proposal

    @classproperty
    def proposable(cls) -> Set['TopicModule']:
        return set(filter(lambda t: t.is_proposable, TopicModule))


if __name__ == '__main__':
    print(TopicModule(None))
