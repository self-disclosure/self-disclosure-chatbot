import logging
import nlu.intentmap_scheme as intentmap_scheme
from cobot_common.service_client import get_client
from cobot_core.utils import parse_blacklist_class_from_profanity_response
from constants_api_keys import COBOT_API_KEY
from util_redis import RedisHelper
from concurrent.futures import ThreadPoolExecutor

CompiledPtn = intentmap_scheme.CompiledPattern()


logger = logging.getLogger(__name__)


def _query_profanity(text):
    client = get_client(api_key=COBOT_API_KEY, timeout_in_millis=500)
    r = client.batch_detect_profanity(utterances=[text], timeout_in_millis=500)
    logger.info('[PROFANITYCHECK] get offensive result response {}'.format(r))
    return parse_blacklist_class_from_profanity_response(r)[0]


class IntentClassifier:
    """intent classifier is used to compile and match the input string"""
    text = ""

    def __init__(self, text):
        self.text = text.lower()

    def is_profanity(self):
        WHITE_LIST = [
            'jessie',
            'stalin',
            'moby dick',
            'fucking',
            'sex and the city'
        ]

        text = self.text

        if not text:
            return

        logger.info('[PROFANITYCHECK] start execute profanity check with text: {}'.format(text))
        for phrase in WHITE_LIST:
            if phrase in text:
                return False
        if self.checkIntent("topic_profanity"):
            return True
        try:
            with ThreadPoolExecutor() as executor:
                future = executor.submit(_query_profanity, self.text)

            if len(self.text.split()) <= 6:
                result = RedisHelper().get(RedisHelper.PREFIX_PROFANITY, self.text)
                logger.info(f'[PROFANITYCHECK] get offensive result from redis: {result}')
                if result is None:
                    result = future.result()
                    logger.info(f'[PROFANITYCHECK] get offensive result from amazon: {result}')
                    RedisHelper().set(RedisHelper.PREFIX_PROFANITY, self.text, result)
            else:
                result = future.result()

            return result == 1

        except Exception:
            logger.error(
                '[PROFANITYCHECK] Exception in Profanity Check', exc_info=True)
            return False

    def is_back_channeling_expression(self):
        BACK_CHANNELING_EXP = {'oh okay', 'cool', 'aha', 'oh no', 'oh yeah', 'yep',
                               'ha-ha', 'right', 'wow', 'nah', 'uh huh', 'ha-ha ha', 'what', 'yup',
                               'sure', 'good', 'hi', 'huh', 'yes', 'uh-huh', 'yeah', 'na', 'okay', 'no'}
        return self.text in BACK_CHANNELING_EXP

    def is_whitelisted_ner_expression(self):
        WHITELISTED_NER_EXP = \
            {
             # general
             'corona virus', 'corona iris', 'coronavirus', 'clinton',
             'stanford', 'shanghai', 'new york', 'china', 'african', 'korea', 'greece',
             'john wayne', 'mike pence', 'unicorn', 'beagle', 'iraq', 'd. c.', 'bars',
             'acapella', 'chinatown', 'china town',
             # movie
             'hobbit', 'parasite', 'joker', 'americana', 'disney', 'spider-man', 'harriet',
             'aquaman', 'twilight', 'dolittle', 'the awaken', 'galifianakis', 'anna faris',
             'groot', 'parasites', 'sombies', 'sombies too', "zomby's too", 'braveheart',
             'snowpiercer', 'smurfs', 'the smurfs', 'nightbreed', 'onward',
             'film noir',

             # music
             'tic tock', 'tik tok', 'ukulele',
             'senorita', 'beethoven', 'eminem', 'alicia keys',
             'anson zebra', 'anson sibera',
             # book
             'the quran', 'roald dahl', 'e-books',
             # food
             'lo mein',
             # sport
             'tom brady', 'giannis', 'giannis antetokounmpo', 'phillie phanatic',
             'gerrit cole', 'garrett cole', 'ram', 'king', 'giant', 'fifa', 'jeremy lin',
             'n. f. l.', 'west ham',
             # game
             'half-life', 'doom eternal',
             }

        return self.text in WHITELISTED_NER_EXP

    def is_coronavirus_related(self):
        KEYWORDS = ['corona', 'virus', 'quarantine', 'covid', 'code nineteen']
        for kw in KEYWORDS:
            if kw in self.text:
                return True
        return False

    def has_intent(self, intent: str):
        return self.checkIntent(intent) is not None

    def is_intent(self, intent: str):
        return self.checkIntent(intent)

    def is_command(self):
        return self.checkIntent("command")

    def is_clarify(self):
        return self.checkIntent("clarify")

    def is_incomplete_sentence(self):
        return self.checkIntent("not_complete")

    def is_hesitant(self):
        return self.checkIntent("hesitate")

    def is_req_task(self):
        return self.checkIntent("req_task")

    def is_terminate(self):
        return self.checkIntent("terminate")

    def is_complaint(self):
        return self.checkIntent("complaint")

    def is_not_sure(self):
        return self.checkIntent("ans_unknown")

    def is_request_allow_tell_sth(self):
        return self.checkIntent("req_allow_tell_sth")

    def is_req_ask_question(self):
        return self.checkIntent("req_allow_ask_question")

    def is_ask_back(self):
        return self.checkIntent("ask_back")

    def is_ans_same(self):
        return self.checkIntent("ans_same")

    def is_ans_neg(self):
        return self.checkIntent("ans_neg")

    def is_topic_saycomfort(self):
        return self.checkIntent("topic_saycomfort")


    def getIntents(self):
        """ Detect and categorize intents - rule based """
        intent_output = {'sys': [], 'topic': [], 'lexical': []}
        for intent in intentmap_scheme.sys_re_patterns.keys():
            if CompiledPtn.get_sys_regex(intent).search(self.text):
                intent_output['sys'].append(intent)
        for intent in intentmap_scheme.topic_re_patterns.keys():
            if CompiledPtn.get_topic_regex(intent).search(self.text):
                intent_output['topic'].append(intent)
        for intent in intentmap_scheme.lexical_re_patterns.keys():
            if CompiledPtn.get_lexical_regex(intent).search(self.text):
                intent_output['lexical'].append(intent)

        # Compatibility for old intent
        if {"ans_positive", "opinion_positive"}.intersection(intent_output['lexical']):
            intent_output['lexical'].append("ans_pos")
        if {"ans_negative", "opinion_negative"}.intersection(intent_output['lexical']):
            intent_output['lexical'].append("ans_neg")

        return intent_output

    def checkIntent(self, intent=""):
        'check the input text with respect to the given intent'
        if intent in intentmap_scheme.sys_re_patterns.keys():
            return CompiledPtn.get_sys_regex(intent).search(self.text)
        if intent in intentmap_scheme.topic_re_patterns.keys():
            return CompiledPtn.get_topic_regex(intent).search(self.text)
        if intent in intentmap_scheme.lexical_re_patterns.keys():
            return CompiledPtn.get_lexical_regex(intent).search(self.text)
