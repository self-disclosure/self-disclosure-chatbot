import pprint
import pytest

from response_generator.music.utils import music_ner
from response_generator.music.utils.music_ner import MusicNERResponse


pp = pprint.PrettyPrinter(width=180)


@pytest.mark.parametrize("user, bot, expected", [
    ('katy perry', "who's your favorite artist?",
     MusicNERResponse(
         genre=[],
         people=[
             MusicNERResponse.People(
                 name='Katy Perry', artist_type='Person', area='United States', area_type='Country',
                 artist_id='315753', begin=1984, end=0, gender='Female',
                 genre=[
                     'dance', 'dance-pop', 'electro', 'electropop', 'house',
                     'pop', 'pop rock', 'rock', 'trap'
                 ]
             )
         ],
         songs=[]
     ))
])
def test_music_ner(user, bot, expected):
    result = music_ner.fetch_music_ner(user, bot)
    print(result)
    assert result == expected


@pytest.mark.parametrize("user, bot, expected", [
    ("i like all genres of music in particular are and b. pop and jazz",
     "Cool, I'm glad you like music too. Pop is my go-to genre. What type of music do you like?",
     MusicNERResponse(
         genre=['r&b', 'pop', 'jazz'],
         people=[],
         songs=[]
     )),
    ("dixieland jazz and classical music",
     "Cool, I'm glad you like music too. Pop is my go-to genre. What type of music do you like?",
     MusicNERResponse(
         genre=['jazz', 'classical'],
         people=[],
         songs=[]
     )),
])
def test_genre(user, bot, expected):
    result = music_ner.fetch_music_ner(user, bot)
    print(result)
    assert result == expected


@pytest.mark.parametrize("user, bot", [
    ('billie eilish', "who's your favorite aritst?"),
    ('billie holiday', "who's your favorite aritst?"),
])
def test_artist(user, bot):
    result = music_ner.fetch_music_ner(user, bot)
    pp.pprint(result)
    assert result
    