import pytest
from pprint import pprint

# from response_generator.music import music_automaton
from response_generator.music.utils import legacy


@pytest.mark.parametrize("name, expected", [
    ('katy perry', True),
    ('billie eilish', True),
    ('lady gaga', True),
])
def test_query_db(name: str, expected: str):
    response = legacy.query_db(name)
    pprint(response, width=180)
    assert bool(response) is expected
