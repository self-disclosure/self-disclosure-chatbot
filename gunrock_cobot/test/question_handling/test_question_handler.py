import pytest
from unittest.mock import patch
from nlu.constants import DialogAct as DialogActEnum
from question_handling.quetion_handler import QuestionHandler
from nlu.constants import TopicModule


@pytest.mark.parametrize("returnnlp", [
    ([
        {
            "text": "what's your favorite food",
            "dialog_act": [DialogActEnum.OPEN_QUESTION_OPINION, "1.0"],
            "dialog_act2": [DialogActEnum.NA, "0.0"],
        }
    ]),
    ([
        {
            "text": "who do you work for",
            "dialog_act": [DialogActEnum.OPEN_QUESTION_OPINION, "1.0"],
            "dialog_act2": [DialogActEnum.NA, "0.0"],
        }
    ]),
([
        {
            "text": "i wanna know how many persons died from the virus",
        }
    ]),
])
def test_question_handler_backstory(returnnlp):
    question_handler = QuestionHandler(returnnlp[0]["text"], returnnlp)
    result = question_handler.handle_question()

    print(result)
    assert result.response != ""



@pytest.mark.parametrize("returnnlp", [
    ([
        {
            "text": "how long can a dog live",
            "dialog_act": [DialogActEnum.OPEN_QUESTION_FACTUAL, "1.0"],
            "dialog_act2": [DialogActEnum.NA, "0.0"],
        }
    ]),
    ([
        {
            "text": "what is netflix",
            "dialog_act": [DialogActEnum.OPEN_QUESTION_FACTUAL, "1.0"],
            "dialog_act2": [DialogActEnum.NA, "0.0"],
        }
    ]),
    ([
        {
            "text": "what's a scandal",
            "dialog_act": [DialogActEnum.OPEN_QUESTION_FACTUAL, "1.0"],
            "dialog_act2": [DialogActEnum.NA, "0.0"],
        }
    ])
])
@patch('template_manager.manager.TemplateManager.speak')
def test_question_handler_factual(speak, returnnlp):
    # Given
    question_handler = QuestionHandler("", returnnlp)
    speak.return_value = "i don't know"

    # When
    result = question_handler.handle_question()

    # Then
    print(result)
    assert result.response is not None
    assert result.user_intent_module == None


@pytest.mark.parametrize("response", [
    "my name is Joseph",
    "i'm a singer",
    "i'm a teacher",
    "Sure, i'm a singer",
    "i would love to do that",
    "i like snuf films too",
    "i was so mad",
    "i've had a few dogs and cats",
    "i have two dogs",
    "i had a cat",
    "i'm going to a concert. I'm excited",
    "i'm going to watch some anime",
    "i live in the Bronx"
])
def test_question_handler_blender_filter(response):
    result = QuestionHandler.should_filter_out_inappropriate_blender_response(response)
    assert result is True


@pytest.mark.parametrize("response", [
    "I know because I have one",
    "I like action and adventure movies",
    "I love shopping online",
    "I would be a dog",
    "Yes, it is a dinosaur"
])
def test_question_handler_blender_filter_false(response):
    result = QuestionHandler.should_filter_out_inappropriate_blender_response(response)
    assert result is False


@pytest.mark.parametrize("returnnlp", [
    ([
        {
            "text": "what is the best mexican food in the world",
            "dialog_act": [DialogActEnum.OPEN_QUESTION_FACTUAL, "1.0"],
            "dialog_act2": [DialogActEnum.NA, "0.0"],
        }
    ]),
])
@patch('template_manager.manager.TemplateManager.speak')
def test_question_handler_factual_no_response(speak, returnnlp):
    # Given
    question_handler = QuestionHandler("", returnnlp)
    speak.return_value = "i don't know"

    # When
    result = question_handler.handle_question()

    # Then
    print(result)
    assert result.user_intent_module == None
    assert result.tag == "ack_question_idk"

@pytest.mark.parametrize("returnnlp", [
    ([
        {
            "text": "what do you think of abc",
            "dialog_act": [DialogActEnum.OPEN_QUESTION_OPINION, "1.0"],
            "dialog_act2": [DialogActEnum.NA, "0.0"],
        }
    ]),
    ([
        {
            "text": "what's that",
            "dialog_act": [DialogActEnum.OPEN_QUESTION_OPINION, "1.0"],
            "dialog_act2": [DialogActEnum.NA, "0.0"],
        }
    ]),
])
@patch('template_manager.manager.TemplateManager.speak')
def test_question_handler_opinion_no_response(speak, returnnlp):
    # Given
    question_handler = QuestionHandler("", returnnlp)
    speak.return_value = "i don't know"

    # when
    result = question_handler.handle_question()

    # Then
    print(result)
    assert result.user_intent_module == None
    assert result.tag == "ack_question_idk"
    assert result.response == "i don't know"



@pytest.mark.parametrize("returnnlp", [
    ([
        {
            "text": "why is that",
            "dialog_act": [DialogActEnum.OPEN_QUESTION, "1.0"],
            "dialog_act2": [DialogActEnum.NA, "0.0"],
        }
    ]),
])
@patch('template_manager.manager.TemplateManager.speak')

def test_question_handler_general_no_response(speak, returnnlp):
    # Given
    question_handler = QuestionHandler("", returnnlp)
    speak.return_value = "i don't know"

    # When
    result = question_handler.handle_question()

    # Then
    print(result)
    assert result.user_intent_module == None
    assert result.tag == "ack_question_idk"
    assert result.response == "i don't know"


@pytest.mark.parametrize("text, expected_result", [
    ("i wanna know how many persons died from the virus", "how many persons died from the virus"),
    ("what is your name", "what is your name"),
    ("i wanna know who created you", "who created you")
])
def test_question_handler_remove_noisy_prefix(text, expected_result):
    result = QuestionHandler.remove_noisy_prefix(text)
    assert result == expected_result
