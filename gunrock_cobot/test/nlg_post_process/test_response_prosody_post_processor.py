from nlg_post_process.response_prosody_post_processor import ResponseProsodyProcessor
import pytest

def test_prosody_process():
    processor = ResponseProsodyProcessor("abc<prosody pitch = '-5%'> Ah1, </prosody> <amazon:emotion name='excited' intensity='low'>That sounds cool!</amazon:emotion> <prosody pitch = '-5%'> Ah2, </prosody> abc I <emphasis level='strong'>love</emphasis> talking about animals! Steve Jobs mentioned the design of a tablet as early as 1983.  His exact words were . <break strength='strong'/> <amazon:domain name='news'> What we want to do is we want to put an incredibly great computer in a book that you can carry around with you and learn how to use in 20 minutes. </amazon:domain> ")
    processor.remove_pitch()
    processor.remove_domain()
    processor.remove_emphasis()
    processor.remove_emotion()
    processor.wrap_emotion("low")
    print(processor.response)

    # assert processor.response == "<prosody rate='95%'>abc Ah1,  That sounds cool!  Ah2,  abc I love talking about animals! <break time="150ms" /> Steve Jobs mentioned the design of a tablet as early as 1983.  His exact words were . <break strength='strong'/>  What we want to do is we want to put an incredibly great computer in a book that you can carry around with you and learn how to use in 20 minutes.  </prosody>"

@pytest.mark.parametrize("text, expected_result", [
    ("i like J.K. Rowling. She's great", "i like J.K. Rowling. <break time='150ms'/> She's great"),
    ("i love a.i. it's my favorite", "i love a.i. it's my favorite"),
    ("bla bla bla i.e. bla bla", "bla bla bla i.e. bla bla"),
    ("i think u.s. is a good place.", "i think u.s. is a good place."),
    ("<prosody rate='95%'>i love a.i. it's my favorite</prosody>", "<prosody rate='95%'>i love a.i. it's my favorite</prosody>"),
    ("i think mr. johnson is doing a great job! He's the best.", "i think mr. johnson is doing a great job! <break time='150ms'/> He's the best."),
    ("i heard this about the movie: It's filmed in 1980. Any thought?", "i heard this about the movie: <break time='150ms'/> It's filmed in 1980. <break time='150ms'/> Any thought?"),
    ("So, I'm wondering, do you like movie? I love it. It's fun. ", "So, I'm wondering, do you like movie? <break time='150ms'/> I love it. <break time='150ms'/> It's fun. ")
])
def test_pause_add_pause(text, expected_result):
    processor = ResponseProsodyProcessor(text)
    processor.add_pauses()

    assert processor.response == expected_result

def test_prosody_remove_interjection():
    processor = ResponseProsodyProcessor("<amazon:emotion name='excited' intensity='medium'><prosody rate='95%'><say-as interpret-as='interjection'> hmm </say-as> <break time='300ms'/> probably not a good idea. I heard Vegas hire security guards to watch  each player</prosody></amazon:emotion>")
    processor.remove_interjection()
    print(processor.response)
    assert processor.response == "<amazon:emotion name='excited' intensity='medium'><prosody rate='95%'> hmm  <break time='300ms'/> probably not a good idea. I heard Vegas hire security guards to watch  each player</prosody></amazon:emotion>"

def test_prosody_add_period_at_the_end_of_interjection():
    processor = ResponseProsodyProcessor("<say-as interpret-as='interjection'>My bad!</say-as><break time ='300ms'/>, sorry. </prosody>")
    processor.adjust_punctuation_around_interjection()
    assert processor.response == "<say-as interpret-as='interjection'>My bad!</say-as> sorry. </prosody>"

    processor = ResponseProsodyProcessor("<say-as interpret-as='interjection'>My bad</say-as><break time ='300ms'/> sorry. </prosody>")
    processor.adjust_punctuation_around_interjection()
    assert processor.response == "<say-as interpret-as='interjection'>My bad. </say-as> sorry. </prosody>"

    processor = ResponseProsodyProcessor("<prosody pitch = '-10%'><say-as interpret-as='interjection'>My bad</say-as></prosody><break time = '300ms'/>, my apologies, I think I missed the last word. can you say that again?")
    processor.adjust_punctuation_around_interjection()
    assert processor.response == "<prosody pitch = '-10%'><say-as interpret-as='interjection'>My bad. </say-as></prosody> my apologies, I think I missed the last word. can you say that again?"

    processor = ResponseProsodyProcessor("<prosody pitch = '-10%'><say-as interpret-as='interjection'>My bad</say-as>, sorry about it")
    processor.adjust_punctuation_around_interjection()
    assert processor.response == "<prosody pitch = '-10%'><say-as interpret-as='interjection'>My bad. </say-as> sorry about it"

    processor = ResponseProsodyProcessor(
        "<say-as interpret-as='interjection'>jiminy cricket </say-as><break time='200ms'></break>I'd rather talk about something else, considering we just met. By the way, my preferred pastime is actually playing video games. I may be a bot, but I play fair and square. Do you like video games too? ")
    processor.adjust_punctuation_around_interjection()
    assert processor.response == "<say-as interpret-as='interjection'>jiminy cricket . </say-as>I'd rather talk about something else, considering we just met. By the way, my preferred pastime is actually playing video games. I may be a bot, but I play fair and square. Do you like video games too? "

    # TODO: this one has a bug. need to fix it if we want to use this function!
    processor = ResponseProsodyProcessor(
        "<prosody rate='95%'><<say-as interpret-as='interjection'>ouch</say-as>. I'm sorry! <break time='150ms' /> How about we talk about something else? <say-as interpret-as='interjection'>Alright! </say-as>laura, I read an article claiming nowadays many people have side projects. <break time='150ms' /> Are you working on something cool in your free time? </prosody>")
    processor.adjust_punctuation_around_interjection()
    print(processor.response)


@pytest.mark.parametrize("text, expected_result", [
    ("Ah, I know about Lilo & Stitch! <say-as interpret-as='interjection'>hmm</say-as>. <break time='150ms' /> What did you think of the movie? <break time='150ms' /> Did you like it?",
     "Ah, I know about Lilo and Stitch! <say-as interpret-as='interjection'>hmm</say-as>. <break time='150ms' /> What did you think of the movie? <break time='150ms' /> Did you like it?"),
    ("On the anniversary of their hire date at microsoft, employees celebrate their employment by taking a pound of M&Ms to work.",
     "On the anniversary of their hire date at microsoft, employees celebrate their employment by taking a pound of M and Ms to work."),
])
def test_replace_unallowed_character(text, expected_result):
    processor = ResponseProsodyProcessor(text)
    processor.replace_unallowed_character()
    assert processor.response == expected_result

@pytest.mark.parametrize("text, expected_result", [
    ("<prosody rate='95%'>Nice. </prosody>. Well.", "<prosody rate='95%'>Nice.</prosody>. Well.")
])
def test_avoid_dot_being_pronounced(text, expected_result):
    processor = ResponseProsodyProcessor(text)
    processor.avoid_dot_being_pronounced()
    assert processor.response == expected_result

def test_ab_test():
    processor = ResponseProsodyProcessor("i love animal", "B")
    processor.wrap_emotion_with_ab_test()

    print(processor.response)

