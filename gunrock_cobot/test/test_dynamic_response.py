from regex_intent_handler import constructDynamicResponse

responses = [
    "(|Hmm.) I didn’t (enjoy|like) (it|that one) (|very) much.",
    "That one didn’t impress me (|very) much."
]

for i in range(6):
    print(constructDynamicResponse(responses))