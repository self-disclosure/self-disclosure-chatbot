import pytest
from types import SimpleNamespace

from response_generator.fsm2 import Tracker, FSMAttributeAdaptor
from response_generator.game.utils import funfact, entity


@pytest.mark.parametrize("noun, canonical, expected", [
    ('fortnite', 'fortnite', True),
    ('minecraft', 'minecraft', True),
    ('red dead redemption', 'red_dead_redemption', True),
    ('test', 'test', False),
])
def test_get_random_funfact(noun: str, canonical: str, expected: bool):

    tracker = Tracker(
        input_data=dict(),
        ua_adaptor=FSMAttributeAdaptor('gamechat', SimpleNamespace(), auto_add_missing_keys=True)
    )

    game = entity.GameItem.from_regex_match(noun=noun, canonical=canonical)

    fact = funfact.get_random_funfact(tracker, game)
    assert bool(fact) == expected, f"{fact}"
