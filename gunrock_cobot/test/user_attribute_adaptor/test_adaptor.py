import pytest
from types import SimpleNamespace

import user_attribute_adaptor as uaa


cases = {
    'movie_simplenamespace': (
        'moviechat_user',
        SimpleNamespace(**{
            "moviechat_user": {
                "current_loop": {}, "subdialog_context": {}, "ask_movietv_repeat": True,
                "current_transition": "t_ask_movietv", "current_state": "s_echo", "flow_interruption": False,
                "not_first_time": True, "propose_continue": "CONTINUE", "tags": ["movie_user_qa_answer_only"],
                "current_subdialog": None, "propose_topic": None, "candidate_module": None
            },
            "propose_topic": None,
            "template_manager": {
                "prev_hash": {
                    "MUoGKQ": ["Vc4kFA"], "jSoKmw": ["C6kjgg"], "ocgSRA": ["c4IZmw", "VS0VHQ"],
                    "KxoF0w": [], "/M0N7w": ["t3gtIQ", "5SE7MQ"], "sicL6A": []
                },
                "selector_history": [
                    "error::s_fragments/reprompt/thanks_for_sharing", "error::s_fragments/reprompt/feedback",
                    "error::reprompt", "movies::what_movie_do_you_like"
                ]
            }
        }),
    ),
    'movie_dict': (
        'moviechat_user',
        {
            "moviechat_user": {
                "current_loop": {}, "subdialog_context": {}, "ask_movietv_repeat": True,
                "current_transition": "t_ask_movietv", "current_state": "s_echo", "flow_interruption": False,
                "not_first_time": True, "propose_continue": "CONTINUE", "tags": ["movie_user_qa_answer_only"],
                "current_subdialog": None, "propose_topic": None, "candidate_module": None
            },
            "propose_topic": None,
            "template_manager": {
                "prev_hash": {
                    "MUoGKQ": ["Vc4kFA"], "jSoKmw": ["C6kjgg"], "ocgSRA": ["c4IZmw", "VS0VHQ"], "KxoF0w": [],
                    "/M0N7w": ["t3gtIQ", "5SE7MQ"], "sicL6A": []
                },
                "selector_history": [
                    "error::s_fragments/reprompt/thanks_for_sharing", "error::s_fragments/reprompt/feedback",
                    "error::reprompt", "movies::what_movie_do_you_like"
                ]
            }
        }
    ),
}


@pytest.mark.parametrize('module_name, user_attributes_ref', [
    v for v in cases.values()
])
def test_adaptor(module_name, user_attributes_ref, caplog):

    caplog.set_level('DEBUG')

    if isinstance(user_attributes_ref, SimpleNamespace):
        ua_items = user_attributes_ref.__dict__[module_name].items()
        expected_mode = 'DIRECT'
    elif isinstance(user_attributes_ref, dict):
        ua_items = user_attributes_ref.items()
        expected_mode = 'DICT'
    else:
        raise ValueError(f"user_attribute_ref invalid type")

    ua = uaa.UserAttributeAdaptor(
        module_name, user_attributes_ref
    )
    assert ua
    assert ua.mode == expected_mode

    for k, v in ua_items:
        assert ua[k] == v


@pytest.mark.parametrize('module_name, user_attributes_ref, ua_mode', [
    (*v,  'DICT' if k.endswith('dict') else 'DIRECT')
    for k, v in cases.items()  # if k.endswith('dict')
])
def test_adaptor_ua_ref_compatibility(module_name, user_attributes_ref, ua_mode, caplog):

    caplog.set_level('DEBUG')

    ua = uaa.UserAttributeAdaptor(module_name, user_attributes_ref)

    assert ua
    assert ua.mode == ua_mode

    # assert False, f"{ua.ua_ref};\n{user_attributes_ref}"
    for key in [module_name, 'propose_topic', 'template_manager']:
        assert hasattr(ua.ua_ref, key), key
