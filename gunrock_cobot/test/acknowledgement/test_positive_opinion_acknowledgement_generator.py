import pytest

from nlu.constants import DialogAct as DialogActEnum
from acknowledgement.positive_opinion_acknowledgement_generator import PositiveOpinionAcknowledgementGenerator

def generate_ack(returnnlp):
    return PositiveOpinionAcknowledgementGenerator(returnnlp).generate_ack()


@pytest.mark.parametrize("returnnlp, adj", [
    ([
        {
            "text": "that's really interesting",
            "dialog_act": [DialogActEnum.APPRECIATION, "0.59"],
            "dialog_act2": [DialogActEnum.NA, "0.0"],
        }
    ], "interesting"),
    ([
         {
             "text": "i didn't that's really interesting",
             "dialog_act": [DialogActEnum.NEG_ANSWER, "0.56"],
             "dialog_act2": [DialogActEnum.NA, "0.0"],
         }
     ], "interesting"),
    ([
         {
             "text": "oh that is interesting",
             "dialog_act": [DialogActEnum.APPRECIATION, "0.30"],
             "dialog_act2": [DialogActEnum.NA, "0.0"],
         }
     ], "interesting"),
    ([
         {
             "text": "yes very interesting",
             "dialog_act": [DialogActEnum.APPRECIATION, "0.08"],
             "dialog_act2": [DialogActEnum.NA, "0.0"],
         }
     ], "interesting"),
    ([
         {
             "text": "i find it very interesting",
             "dialog_act": [DialogActEnum.OPINION, "0.56"],
             "dialog_act2": [DialogActEnum.NA, "0.0"],
         }
     ], "interesting"),
    ([
         {
             "text": "no that's interesting too",
             "dialog_act": [DialogActEnum.NEG_ANSWER, "0.56"],
             "dialog_act2": [DialogActEnum.NA, "0.0"],
         }
     ], "interesting"),
    ([
         {
             "text": "interesting",
             "dialog_act": [DialogActEnum.APPRECIATION, "0.56"],
             "dialog_act2": [DialogActEnum.NA, "0.0"],
         }
     ], "interesting"),
])
def test_positive_opinion_with_adj(returnnlp, adj):
    ack = generate_ack(returnnlp)
    assert ack["key"] == "user_positive_opinion/with_keyword"
    assert ack["slots"]["content"] == adj


@pytest.mark.parametrize("returnnlp, adj", [
    ([
        {
            "text": "that's pretty interesting",
            "dialog_act": [DialogActEnum.APPRECIATION, "0.59"],
            "dialog_act2": [DialogActEnum.NA, "0.0"],
            "intent": {"sys": [], "topic": [], "lexical": ["opinion_positive"]}
        }
    ], "interesting"),
])
def test_positive_opinion_with_adj(returnnlp, adj):
    ack = generate_ack(returnnlp)
    assert ack["key"] == "user_positive_opinion/with_keyword"
    assert ack["slots"]["content"] == adj


@pytest.mark.parametrize("returnnlp", [
    ([
        {
        "text": "that's unbelievable",
        "dialog_act": [DialogActEnum.APPRECIATION, "0.59"],
        "dialog_act2": [DialogActEnum.NA, "0.0"],
        "intent": {"sys": [], "topic": [""], "lexical": ["opinion_positive"]}
        }
    ]),
])
def test_positive_opinion_without_adj(returnnlp):
    ack = generate_ack(returnnlp)
    assert ack["key"] == "user_positive_opinion/without_keyword"
    assert ack["slots"]["content"] == {}


@pytest.mark.parametrize("returnnlp", [
    ([
        {
            "text": "that's really interesting",
            "dialog_act": [DialogActEnum.APPRECIATION, "0.59"],
            "dialog_act2": [DialogActEnum.NA, "0.0"],
        },
        {
            "text": "tell me more about it",
            "dialog_act": [DialogActEnum.COMMANDS, "0.5"],
            "dialog_act2": [DialogActEnum.NA, "0.0"],
        }
    ]),
    ([
        {
            "text": "that's really interesting",
            "dialog_act": [DialogActEnum.APPRECIATION, "0.59"],
            "dialog_act2": [DialogActEnum.NA, "0.0"],
        },
        {
            "text": "tell me more about it",
            "dialog_act": [DialogActEnum.OPEN_QUESTION_OPINION, "0.5"],
            "dialog_act2": [DialogActEnum.NA, "0.0"],
        }
    ]),
])
def test_positive_opinion_with_question_command_at_end(returnnlp):
    ack = generate_ack(returnnlp)
    assert ack is None


@pytest.mark.parametrize("text", [
    "that's pretty interesting"
])
def test_detect_positive_echoeable_adj(text):
    result = PositiveOpinionAcknowledgementGenerator.detect_positive_echoeable_adj(text)
    assert result is not None