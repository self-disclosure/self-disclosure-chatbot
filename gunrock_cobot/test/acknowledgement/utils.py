
sorry = {"user_attributes": {'user_id': 'localtest-user-2336dfa7-f4e1-41a0-bd9d-b23da3f3b32e', 'map_attributes': {'conversationId': None, 'news_user': {'propose_continue': 'STOP'}, 'previous_modules': ['TECHSCIENCECHAT', 'LAUNCHGREETING', 'LAUNCHGREETING'], 'prev_hash': {'sys_env': 'local'}, 'user_profile': {'gender': 'male', 'username': 'kevin'}, 'template_manager': {'prev_hash': {'PPsGsg': ['FlAjjQ'], 'gN4J9Q': ['1rwq/w'], 'rJ8LnQ': [], 'yM0X9w': ['Lao5zg'], '4bQYdA': [], 'is0KPQ': []}, 'selector_history': []}, 'last_session_id': 'localtest-session-24b97ac4-3600-430e-9b67-743660aacfa7', 'techsciencechat': {'module_sentiment_tracker': {'pos': [0.05, 0.05], 'neg': [0.05, 0.05], 'neu': [0.9, 0.9]}, 'chitchat_tracker': {'unfinished_ids': {}, 'curr_turn': -1, 'completed_ids': []}, 'current_transition': 't_intro', 'tech_propose_topic': ['artificial_intelligence'], 'not_first_time': True, 'topic_history': {'artificial_intelligence': 1}, 'prev_state': 's_intro', 'propose_continue': 'CONTINUE', 'context_topic': {'technology': {'technology': 3}}, 'new_topics': ['kevin']}, 'animalchat': {'propose_continue': 'STOP'}, 'fashionchat': {'propose_continue': 'STOP'}, 'timechat': {'propose_continue': 'STOP'}, 'module_selection': {'used_topic': ['LAUNCHGREETING', 'TECHSCIENCECHAT']}, 'topic_to_replace': [], 'cur_selected_modules': 'TECHSCIENCECHAT', 'foodchat': {'propose_continue': 'STOP'}, 'sys_env': 'local', 'bookchat_user': {'propose_continue': 'STOP'}, 'socialchat': {'curr_state': 's_init', 'propose_continue': 'STOP'}, 'module_rank': {'MUSICCHAT': 0, 'GAMECHAT': 0, 'SPORT': 0, 'FASHIONCHAT': 0, 'TECHSCIENCECHAT': 0, 'TRAVELCHAT': 0, 'NEWS': 0, 'BOOKCHAT': 0, 'ANIMALCHAT': 0, 'FOODCHAT': 0, 'MOVIECHAT': 0}, 'travelchat': {'propose_continue': 'STOP'}, 'gamechat': {'propose_continue': 'STOP'}, 'weatherchat': {'propose_continue': 'STOP'}, 'usr_name': 'kevin', 'moviechat_user': {'propose_continue': 'STOP'}, 'sportchat': {'propose_continue': 'STOP'}, 'last_module': 'TECHSCIENCECHAT', 'tedtalkchat': {'propose_continue': 'STOP'}, 'launchgreeting': {'volatile': {'utt': 'Hi, kevin.', 'check_input_for_name': 'kevin', 'is_name_common': True}, 'persistent_store': {}, 'propose_continue': 'STOP', 'states': {'state': 'how_are_you'}}, 'comfortchat': {'propose_continue': 'STOP'}, 'holidaychat': {'propose_continue': 'STOP'}, 'dailylife': {'propose_continue': 'STOP'}, 'musicchat': {'propose_continue': 'STOP'}, 'visit': 1}},
"current_state": {'user_id': 'localtest-user-2336dfa7-f4e1-41a0-bd9d-b23da3f3b32e', 'conversation_id': None, 'session_id': 'localtest-session-24b97ac4-3600-430e-9b67-743660aacfa7', 'creation_date_time': '2020-01-21T23:39:59.953921', 'request_type': 'IntentRequest', 'intent': 'general', 'slots': {'text': {'name': 'text', 'value': 'i am so sorry', 'confirmationStatus': 'NONE', 'source': 'USER'}}, 'topic': [{'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'i am so sorry', 'topicKeywords': []}], 'asr': [], 'text': 'i am so sorry', 'response': {}, 'mode': None, 'a_b_test': 'B', 'converttext': 'i am so sorry', 'sentence_completion_text': 'i am so sorry', 'segmentation': {'segmented_text': ['i am so sorry'], 'segmented_text_raw': ['i am so sorry']}, 'dependency_parsing': {'dependency_parent': [[4, 4, 4, 0]], 'dependency_label': [['nsubj', 'cop', 'advmod', 'ccomp']], 'pos_tagging': [['PRON', 'AUX', 'ADV', 'ADJ']]}, 'nounphrase2': {'noun_phrase': [[]], 'local_noun_phrase': [[]]}, 'googlekg': [[]], 'concept': [[]], 'intent_classify': {'sys': [], 'topic': [], 'lexical': []}, 'intent_classify2': [{'sys': [], 'topic': [], 'lexical': []}], 'sentiment2': [{'neg': '0.443', 'neu': '0.557', 'pos': '0.0', 'compound': '-0.1513'}], 'sentiment': 'neu', 'sentiment_allennlp': [{'compound': '0.5', 'neg': '0.9', 'neu': '0.0', 'pos': '0.1'}], 'dialog_act': [{'text': 'i am so sorry', 'DA': 'commands', 'confidence': 0.4156193137168884}], 'topic2': [{'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'i am so sorry', 'topicKeywords': []}], 'amazon_topic': 'Other', 'npknowledge': {'knowledge': [], 'noun_phrase': [], 'local_noun_phrase': []}, \
'returnnlp': \
[{'text': 'i am so sorry', \
'dialog_act': ['apology', '0.99'], \
'intent': {'sys': [], 'topic': [], 'lexical': []}, \
'sentiment': {'compound': '0.5', 'neg': '0.9', 'neu': '0.0', 'pos': '0.1'}, \
'googlekg': [], 'noun_phrase': [], \
'topic': {'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'i am so sorry', 'topicKeywords': []}, \
'concept': [], 'amazon_topic': 'Other', \
'dependency_parsing': {'head': [4, 4, 4, 0], \
'label': ['nsubj', 'cop', 'advmod', 'ccomp']}, \
'pos_tagging': ['PRON', 'AUX', 'ADV', 'ADJ']}], \
'central_elem': {'senti': 'neg', 'regex': {'sys': [], 'topic': [], 'lexical': []}, 'DA': 'commands', 'DA_score': '0.4156193137168884', 'module': [], 'np': [], 'text': 'i am so sorry', 'backstory': {'confidence': 0.6691908240318298, 'followup': '', 'module': '', 'neg': '', 'pos': '', 'postfix': '', 'question': 'I do not like you', 'reason': '', 'tag': '', 'text': "ow, ow! i'm sorry you feel that way."}}, 'last_response': 'Oh nice.  I also love technology. I would be very happy to know what are you interested in.   Do you want to talk about a i ?', 'topic_class': 'Phatic', 'features': {'concept': [], 'intent': 'general', 'nounphrase2': {'noun_phrase': [[]], 'local_noun_phrase': [[]]}, 'googlekg': [[]], 'topic': [{'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'i am so sorry', 'topicKeywords': []}], 'converttext': 'i am so sorry', 'sentiment2': [{'neg': '0.443', 'neu': '0.557', 'pos': '0.0', 'compound': '-0.1513'}], 'sentiment_allennlp': [{'compound': '0.5', 'neg': '0.9', 'neu': '0.0', 'pos': '0.1'}], 'spacynp': None, 'dependency_parsing': {'dependency_parent': [[4, 4, 4, 0]], 'dependency_label': [['nsubj', 'cop', 'advmod', 'ccomp']], 'pos_tagging': [['PRON', 'AUX', 'ADV', 'ADJ']]}, 'sentiment': 'neu', 'asrcorrection': None, 'coreference': None, 'intent_classify': {'sys': [], 'topic': [], 'lexical': []}, 'central_elem': {'senti': 'neg', 'regex': {'sys': [], 'topic': [], 'lexical': []}, 'DA': 'commands', 'DA_score': '0.4156193137168884', 'module': [], 'np': [], 'text': 'i am so sorry', 'backstory': {'confidence': 0.6691908240318298, 'followup': '', 'module': '', 'neg': '', 'pos': '', 'postfix': '', 'question': 'I do not like you', 'reason': '', 'tag': '', 'text': "ow, ow! i'm sorry you feel that way."}}, 'dialog_act': [{'text': 'i am so sorry', 'DA': 'commands', 'apology': 0.99}], 'text': 'i am so sorry', 'ner': None, 'segmentation': {'segmented_text': ['i am so sorry'], 'segmented_text_raw': ['i am so sorry']}, 'amazon_topic': 'Other', 'topic2': [{'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'i am so sorry', 'topicKeywords': []}], 'returnnlp': [{'text': 'i am so sorry', 'dialog_act': ['apology', '0.99'], 'intent': {'sys': [], 'topic': [], 'lexical': []}, 'sentiment': {'compound': '0.5', 'neg': '0.9', 'neu': '0.0', 'pos': '0.1'}, 'googlekg': [], 'noun_phrase': [], 'topic': {'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'i am so sorry', 'topicKeywords': []}, 'concept': [], 'amazon_topic': 'Other', 'dependency_parsing': {'head': [4, 4, 4, 0], 'label': ['nsubj', 'cop', 'advmod', 'ccomp']}, 'pos_tagging': ['PRON', 'AUX', 'ADV', 'ADJ']}], 'npknowledge': {'knowledge': [], 'noun_phrase': [], 'local_noun_phrase': []}, 'intent_classify2': [{'sys': [], 'topic': [], 'lexical': []}], 'sentence_completion_text': 'i am so sorry', 'topic_module': '', 'topic_keywords': [], 'knowledge': [], 'noun_phrase': []}},
"user_utterance_features":  {'concept': [], 'intent': 'general', 'nounphrase2': {'noun_phrase': [[]], 'local_noun_phrase': [[]]}, 'googlekg': [[]], 'topic': [{'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'i am so sorry', 'topicKeywords': []}], 'converttext': 'i am so sorry', 'sentiment2': [{'neg': '0.443', 'neu': '0.557', 'pos': '0.0', 'compound': '-0.1513'}], 'sentiment_allennlp': [{'compound': '0.5', 'neg': '0.9', 'neu': '0.0', 'pos': '0.1'}], 'spacynp': None, 'dependency_parsing': {'dependency_parent': [[4, 4, 4, 0]], 'dependency_label': [['nsubj', 'cop', 'advmod', 'ccomp']], 'pos_tagging': [['PRON', 'AUX', 'ADV', 'ADJ']]}, 'sentiment': 'pos', 'asrcorrection': None, 'coreference': None, 'intent_classify': {'sys': [], 'topic': [], 'lexical': []}, 'central_elem': {'senti': 'neg', 'regex': {'sys': [], 'topic': [], 'lexical': []}, 'DA': 'commands', 'DA_score': '0.4156193137168884', 'module': [], 'np': [], 'text': 'i am so sorry', 'backstory': {'confidence': 0.6691908240318298, 'followup': '', 'module': '', 'neg': '', 'pos': '', 'postfix': '', 'question': 'I do not like you', 'reason': '', 'tag': '', 'text': "ow, ow! i'm sorry you feel that way."}}, 'dialog_act': [{'text': 'i am so sorry', 'DA': 'commands', 'confidence': 0.4156193137168884}], 'text': 'i am so sorry', 'ner': None, 'segmentation': {'segmented_text': ['i am so sorry'], 'segmented_text_raw': ['i am so sorry']}, 'amazon_topic': 'Other', 'topic2': [{'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'i am so sorry', 'topicKeywords': []}], 'returnnlp': [{'text': 'i am so sorry', 'dialog_act': ['commands', '0.4156193137168884'], 'intent': {'sys': [], 'topic': [], 'lexical': []}, 'sentiment': {'compound': '0.5', 'neg': '0.9', 'neu': '0.0', 'pos': '0.1'}, 'googlekg': [], 'noun_phrase': [], 'topic': {'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'i am so sorry', 'topicKeywords': []}, 'concept': [], 'amazon_topic': 'Other', 'dependency_parsing': {'head': [4, 4, 4, 0], 'label': ['nsubj', 'cop', 'advmod', 'ccomp']}, 'pos_tagging': ['PRON', 'AUX', 'ADV', 'ADJ']}], 'npknowledge': {'knowledge': [], 'noun_phrase': [], 'local_noun_phrase': []}, 'intent_classify2': [{'sys': [], 'topic': [], 'lexical': []}], 'sentence_completion_text': 'i am so sorry', 'topic_module': '', 'topic_keywords': [], 'knowledge': [], 'noun_phrase': []}
}

positive_opinion = {"user_attributes": {}}

thanking = {}

appreciation = {}

complaint = {}

ask_more = {}

cases = {"sorry": sorry, "positive_opinion":positive_opinion, "thanking":thanking, "appreciation": appreciation,\
         "complaint": complaint, "ask_more":ask_more}

def get_user_utterances(key):
    return cases[key]["user_utterance_features"]

        

class MockStateManager:
    def __init__(self, key):
        self.metadata = cases[key]
        self.state = MockCurrentState(self.metadata["current_state"])
        self.system_acknowledgement = {}

    @property
    def current_state(self):
        return self.state

    @property
    def user_attributes(self):
        return UserAttributes(self.metadata["user_attributes"])

    @property
    def last_state(self):
        return {"system_acknowledgement": self.system_acknowledgement}

    def set_system_acknowledgement(self, input_tag):
        self.system_acknowledgement["input_tag"] = input_tag

    def set_last_response(self, last_response):
        self.state.set_last_response(last_response)

    def set_text(self, text):
        self.state.set_text(text)

class UserAttributes:
    def __init__(self, metadata):
        self.metadata = metadata["map_attributes"]

    @property
    def template_manager(self):
        return self.metadata["template_manager"]


class MockCurrentState:
    def __init__(self, metadata):
        self.metadata = metadata

    @property
    def features(self):
        return self.metadata["features"]

    @property
    def text(self):
        return self.metadata["text"]

    @property
    def last_response(self):
        return self.metadata["last_response"]

    def set_last_response(self, last_response):
        self.metadata["last_response"] = last_response

    def set_text(self, text):
        self.metadata["text"] = text

