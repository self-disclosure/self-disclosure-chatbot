import pytest

from nlg.blender import BlenderResponseGenerator


@pytest.mark.parametrize("response", [
    "my name is jason",
    "my instagram is @james",
    "i miss my family and friends",
    "My mom is a blonde, blue eyed woman. ",
    "I'm talking about my sister. ",
    "I use them to power my car. ",
    "I like to play with my dog. ",
    "I have a pet squirrell.",
    "I love my pet squirell. ",
    "I have two dogs.",
    "I have a sister and a brother.",
    "I have a human body.",
    "Yes, I have an actual body."
    "I love to call my grandparents. ",
    "I was sad because I was thinking about my ex-wife. ",
    "I was moving to a different city.",
    "I love my mom. ",
    "I am talking about my daughter. ",
    "I live in woodland",
    "I have been to the zoo.",
    "No, but I have had a cold sore.",
    "I have a body, it is a human body.",
    "I have been married",
])
def test_should_filter_out_inappropriate_blender_response_True(response):
    result = BlenderResponseGenerator.should_filter_out_inappropriate_blender_response(response)
    assert result is True



@pytest.mark.parametrize("response", [
    "I have not, but I have heard of it.",
    "I have seen it."
])
def test_should_filter_out_inappropriate_blender_response_false(response):
    result = BlenderResponseGenerator.should_filter_out_inappropriate_blender_response(response)
    assert result is False
