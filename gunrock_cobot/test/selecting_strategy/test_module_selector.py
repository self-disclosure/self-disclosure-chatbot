import pytest
from types import SimpleNamespace
from nlu.constants import TopicModule
from selecting_strategy.module_selection import ModuleSelector, GlobalState
from unittest.mock import patch
from user_profiler.user_profile import UserProfile

@pytest.fixture
def module_selector():
    ua_ref = SimpleNamespace()
    return ModuleSelector(ua_ref)


@patch('selecting_strategy.module_selection.ModuleSelector.current_selected_module')
@patch('selecting_strategy.module_selection.ModuleSelector._get_previous_modules_raw')
def test_get_last_topic_module(_get_previous_modules_raw, current_selected_module, module_selector):
    # Given
    _get_previous_modules_raw.return_value = [
        TopicModule.FOOD.value,
        TopicModule.LAUNCHGREETING.value,
        TopicModule.FASHION.value,
        TopicModule.SPORTS.value,
    ]

    current_selected_module.return_value = TopicModule.FOOD.value

    # When
    last_topic_module = module_selector.last_topic_module

    # Then
    assert last_topic_module == TopicModule.FOOD.value


@patch('selecting_strategy.module_selection.ModuleSelector.current_selected_module')
@patch('selecting_strategy.module_selection.ModuleSelector._get_previous_modules_raw')
def test_get_last_topic_module_same_module(_get_previous_modules_raw, current_selected_module, module_selector):
    # Given
    _get_previous_modules_raw.return_value = [
        TopicModule.FOOD.value,
        TopicModule.LAUNCHGREETING.value,
        TopicModule.FASHION.value,
        TopicModule.SPORTS.value,
    ]

    current_selected_module.return_value = TopicModule.FOOD.value

    # When
    last_topic_module = module_selector.last_topic_module

    # Then
    assert last_topic_module == TopicModule.FOOD.value


@patch('selecting_strategy.module_selection.ModuleSelector.current_selected_module')
@patch('selecting_strategy.module_selection.ModuleSelector._get_previous_modules_raw')
def test_get_last_topic_module_clarification(_get_previous_modules_raw, current_selected_module, module_selector):
    # Given
    _get_previous_modules_raw.return_value = [
        TopicModule.CLARIFICATION.value,
        TopicModule.FOOD.value,
        TopicModule.LAUNCHGREETING.value,
        TopicModule.FASHION.value,
        TopicModule.SPORTS.value,
    ]

    current_selected_module.return_value = TopicModule.FOOD.value

    # When
    last_topic_module = module_selector.last_topic_module

    # Then
    assert last_topic_module == TopicModule.FOOD.value


@patch('selecting_strategy.module_selection.ModuleSelector.current_selected_module')
@patch('selecting_strategy.module_selection.ModuleSelector._get_previous_modules_raw')
def test_get_last_topic_module_different_module(_get_previous_modules_raw, current_selected_module, module_selector):
    # Given
    _get_previous_modules_raw.return_value = [
        TopicModule.CLARIFICATION.value,
        TopicModule.FOOD.value,
        TopicModule.LAUNCHGREETING.value,
        TopicModule.FASHION.value,
        TopicModule.SPORTS.value,
    ]

    current_selected_module.return_value = TopicModule.MOVIE.value

    # When
    last_topic_module = module_selector.last_topic_module

    # Then
    assert last_topic_module == TopicModule.FOOD.value


def test_global_state(module_selector):
    # Given
    module_selector.global_state = GlobalState.ASK_RECENT_MOVIE

    # When
    global_state = module_selector.global_state

    # Then
    assert global_state == GlobalState.ASK_RECENT_MOVIE
    assert module_selector['global_state'] == "ask_recent_movie"


def test_global_state_none(module_selector):
    # When
    global_state = module_selector.global_state

    # Then
    assert global_state == GlobalState.NA
    assert module_selector['global_state'] == "na"


def test_propose_topic_topic_module(module_selector):
    # Given
    module_selector.current_selected_module = TopicModule.MOVIE

    # When
    selected_module = module_selector.current_selected_module

    # Then
    assert selected_module == TopicModule.MOVIE


def test_propose_keywords_str(module_selector):
    # When
    module_selector.add_propose_keyword("corona virus")

    # Then
    assert module_selector.propose_keywords == [
        {
            "text": "corona virus"
        }
    ]
    assert module_selector.get_top_propose_keyword().text == "corona virus"
    assert module_selector.get_propose_keywords_text_only() == ["corona virus"]


def test_propose_keywords_dict(module_selector):
    # When
    module_selector.add_propose_keyword({
        "text": "rocket",
        "sport_type": "basketball",
        "sport_team": "houston rocket"
    })

    # Then
    assert module_selector.propose_keywords == [{
        "text": "rocket",
        "sport_type": "basketball",
        "sport_team": "houston rocket"
    }]
    assert module_selector.get_top_propose_keyword().text == "rocket"
    assert module_selector.get_propose_keywords_text_only() == ["rocket"]


def test_propose_keywords_user_preferred_entity(module_selector):
    # When
    module_selector.add_propose_keyword(
        UserProfile.PreferrdEntityTopic.from_dict({
            "entity_in_utterance": "abc",
            "entity_detected": "abcd",
            "entity_type": ["type_1", "type_2"],
            "module": TopicModule.SPORTS.value}
        )
    )

    # Then
    assert module_selector.get_top_propose_keyword().text == "abcd"
    assert module_selector.get_top_propose_keyword().entity_type == ["type_1", "type_2"]
    assert module_selector.get_propose_keywords_text_only() == ["abcd"]

def test_propose_keywords_none(module_selector):
    # When
    module_selector.propose_keywords = None

    # When
    assert module_selector.propose_keywords == []
    assert module_selector.get_top_propose_keyword().text == ""
    assert module_selector.get_propose_keywords_text_only() == []


@patch('selecting_strategy.module_selection.ModuleSelector.current_selected_module')
@patch('selecting_strategy.module_selection.ModuleSelector._get_previous_modules_raw')
def test_get_last_topic_module_different_module_social(_get_previous_modules_raw, current_selected_module, module_selector):
    # Given
    _get_previous_modules_raw.return_value = [
        TopicModule.SOCIAL.value,
        TopicModule.FOOD.value,
        TopicModule.LAUNCHGREETING.value,
        TopicModule.FASHION.value,
        TopicModule.SPORTS.value,
    ]

    current_selected_module.return_value = TopicModule.MOVIE.value

    # When
    last_topic_module = module_selector.last_topic_module

    # Then
    assert last_topic_module == TopicModule.SOCIAL.value


