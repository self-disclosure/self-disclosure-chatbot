from copy import deepcopy
import unittest
from pprint import pprint
from types import SimpleNamespace

import user_profiler.user_preference_manager as upm


def format_for_assert(d: dict):
    d = deepcopy(d)
    for k, v in d.items():
        for ki, vi in v.items():
            v[ki] = set(v[ki])
    return d


class UserPreferenceManagerTest(unittest.TestCase):

    cases = [
        (
            ['<like> <movie:zootopia>'],
            [('like', 'movie', 'zootopia')],
            ['<like> <*:*>'],
            {
                'like': {
                    'movie': ['zootopia'],
                }
            }
        ), (
            ['<seen> <movie:zootopia>',
             '<seen> <movie:my little pony>',
             '<like> <actor:bruce willis>',
             '<dislike> <genre:romance>'],
            [('seen', 'movie', 'zootopia'),
             ('seen', 'movie', 'my little pony'),
             ('like', 'actor', 'bruce willis'),
             ('like', 'genre', 'action'),
             ('dislike', 'genre', 'romance')],
            ['<*> <*:*>'],
            {
                'like': {'actor': ['bruce willis']},
                'dislike': {'genre': ['romance']},
                'seen': {'movie': ['my little pony', 'zootopia']},
            }
        ),
    ]

    def test_add(self):

        for case in UserPreferenceManagerTest.cases:
            with self.subTest():
                ua_ref = SimpleNamespace()
                manager = upm.UserPreferenceManager(ua_ref)
                manager.add_preference(case[0])
                assert not bool(set(manager.ua.preferences) - set(case[1]))

    def test_query(self):

        for case in UserPreferenceManagerTest.cases:
            with self.subTest():
                ua_ref = SimpleNamespace()
                manager = upm.UserPreferenceManager(ua_ref)
                manager.add_preference(case[0])
                output = manager.query_preference(case[2])
                pprint(output)
                pprint(case[3])
                pprint(format_for_assert(output))
                pprint(format_for_assert(case[3]))
                assert format_for_assert(output) == format_for_assert(case[3])
