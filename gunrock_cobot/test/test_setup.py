import os
import unittest
import subprocess

class SystemSetupTest(unittest.TestCase):

    def test_path_validate(self):
        """Validate all the system path variable works"""
        try:
            cobot_home = os.getenv("COBOT_HOME")
            self.assertTrue(cobot_home is not None)
            subprocess.call("cobot")
        except OSError:
            print("Cobot Home not setup correctly:  COBOT_HOME={0}".format(cobot_home))

    def test_pythonpaths(self):
        cobot_home = os.getenv("COBOT_HOME")
        gunrock_cobot_home = os.path.join(cobot_home, "gunrock_cobot")
        try:
            python_paths = os.environ["PYTHONPATH"].split(os.pathsep)
            self.assertTrue(cobot_home in python_paths)
            self.assertTrue(gunrock_cobot_home in python_paths)
        except KeyError:
            print("PythonPath not set")

    def test_paths(self):
        cobot_home = os.getenv("COBOT_HOME")
        gunrock_cobot_home = os.path.join(cobot_home, "gunrock_cobot")
        try:
            python_paths = os.environ["PATH"].split(os.pathsep)
            self.assertTrue(cobot_home in python_paths)
            self.assertTrue(gunrock_cobot_home in python_paths)
        except KeyError:
            print("PythonPath not set")
            print("==========================\n\n")

    def test_imports(self):
        cobot_home = os.path.join(os.getenv("COBOT_HOME"), "gunrock_cobot")
        print("Import Tests \n\n")
        for path in os.listdir(cobot_home):
            try:
                if path.endswith(".py"):
                    path = path.replace(".py", "")     
                    res = exec("import {0}".format(path))
                    self.assertNotIsInstance(res, ModuleNotFoundError)
                if os.path.isdir(path):
                    res = exec("import {0}".format(path))
                    self.assertNotIsInstance(res, ModuleNotFoundError)             
            except ImportError as e:
                print("Import Failed at " + path)
                print(e)
                print("===================\n\n")
            except FileNotFoundError as e2:
                print("FileNotFound Failed at " + path)
                print(e2)
                print("===================\n\n")
            except OSError as e3:
                print("OSError Failed at " + path)
                print(e3)
                print("===================\n\n")
            except Exception as e4:
                print(e4)

