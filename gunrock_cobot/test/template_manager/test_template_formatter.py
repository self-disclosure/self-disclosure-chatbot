import calendar
import random
import unittest.mock as mock
from datetime import datetime, timedelta
from types import SimpleNamespace
import os

from template_manager import Template, TemplateManager, TemplateUserAttributes
from template_manager.formatter import TemplateFormatter

TEMPLATE_ROOT = os.path.join(os.getenv("COBOT_HOME"), "gunrock_cobot", "response_templates")


@mock.patch('template_manager.formatter.datetime')
def test_formatting_time_of_day(mock_dt):

    ua_ref = SimpleNamespace(**{})

    template = Template.games
    tua = TemplateUserAttributes(ua_ref)

    formatter = TemplateFormatter(template)

    expected_ss = [
        ('Good morning', datetime(2020, 1, 1, 8, 0, 0) + timedelta(hours=7)),
        ('Good afternoon', datetime(2020, 1, 1, 13, 0, 0) + timedelta(hours=7)),
        ('Good evening', datetime(2020, 1, 1, 20, 0, 0) + timedelta(hours=7)),
    ]
    test_s = "Good {t_time_of_day}"

    for expected_s, dt in expected_ss:
        mock_dt.utcnow = mock.Mock(return_value=dt)

        actual_s = formatter.format_string(
            test_s, '/', {}, tua
        )

        assert actual_s == expected_s


@mock.patch('template_manager.formatter.datetime')
def test_formatting_weekday(mock_dt):
    ua_ref = SimpleNamespace(**{})

    template = Template.games
    tua = TemplateUserAttributes(ua_ref)

    formatter = TemplateFormatter(template)

    expected_ss = [
        (f"It's {calendar.day_name[day.weekday()]}", day)
        for day in (
            datetime(2019, 9, i, 8, 0, 0) + timedelta(hours=7)
            for i in range(23, 23 + 7)
        )
    ]
    test_s = "It's {t_weekday}"

    for expected_s, dt in expected_ss:
        mock_dt.utcnow = mock.Mock(return_value=dt)

        actual_s = formatter.format_string(
            test_s, '/', {}, tua
        )

        assert actual_s == expected_s


def test_formatting_username():
    ua_ref = SimpleNamespace()
    from user_profiler.user_profile import UserProfile
    user_profile = UserProfile(ua_ref)

    template = Template.games
    tua = TemplateUserAttributes(ua_ref)
    formatter = TemplateFormatter(template)

    expected_names = [
        "Austin", "Kai-hui", "Dian", "Mingyang", "Josh", "Ishan", "Zhou"
    ]

    def _comparitor(expected_name):
        expected_s = f"Your name is {expected_name}, right?"
        test_s = "Your name is {t_username}, right?"

        actual_s = formatter.format_string(
            test_s, '/', {}, tua
        )

        assert actual_s == expected_s

    for expected_name in expected_names:
        user_profile._username = expected_name
        _comparitor(expected_name)

    # for compatibility

    ua_ref.user_profile = None

    for expected_name in expected_names:
        ua_ref.usr_name = expected_name
        _comparitor(expected_name)


@mock.patch('random.sample', lambda pop, k: [list(sorted(pop))[0]])
def test_formatting_talk_about_module():
    ua_ref = SimpleNamespace()
    from selecting_strategy.module_selection import ModuleSelector, topics_available_for_proposal, topic_name_map
    ms = ModuleSelector(ua_ref)
    ms.module_rank = {k: 0.0 for k in topics_available_for_proposal}

    for module in ms.module_rank:
        ms.module_rank[module] = 1.0

        template = Template.games
        tua = TemplateUserAttributes(ua_ref)
        formatter = TemplateFormatter(template)

        expected_module = max(ms.module_rank.keys(), key=lambda k: ms.module_rank[k])
        expected_module_utt = random.sample(topic_name_map[expected_module], k=1)[0]
        expected_s = f"Let's talk about {expected_module_utt}, shall we?"  # noqa: F841
        test_s = "Let's talk about {t_talk_about_module}, shall we?"

        actual_s = formatter.format_string(  # noqa: F841
            test_s, '/', {}, tua
        )

        # assert actual_s == expected_s
        # TODO: result random sample test cases


@mock.patch('random.choice', lambda l: l[0])
def test_formatting_t_fragments():
    ua_ref = SimpleNamespace()

    template = Template.games
    tua = TemplateUserAttributes(ua_ref)
    formatter = TemplateFormatter(template)

    tm = TemplateManager(Template.shared, SimpleNamespace())

    import yaml

    shared_template = yaml.load(open(os.path.join(TEMPLATE_ROOT, 'shared_template.yml')), Loader=yaml.FullLoader)

    t_fragments = list(shared_template['t_fragments'].keys())
    t_fragments[:] = [f"t_fragments/{s}" for s in t_fragments]

    expected_ss = [f"{tm.speak(s, {})}, yeah" for s in t_fragments]

    test_ss = [f"{{{s}}}, yeah" for s in t_fragments]

    actual_ss = [
        formatter.format_string(s, '/', {}, tua)
        for s in test_ss
    ]

    for e, a in zip(expected_ss, actual_ss):
        assert e == a


# TODO: testing s_fragments
# TODO: testing t_
# TODO: testing s_
