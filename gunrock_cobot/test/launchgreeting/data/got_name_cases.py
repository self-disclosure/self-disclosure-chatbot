cases = [
    ({
        'inputs': {
            'input_data': {
                'asr': [],
                'text':
                'my name is sebastian',
                'features': {
                    'ner':
                    None,
                    'sentiment2':
                    None,
                    'topic': [{
                        'text':
                        'my name is sebastian',
                        'topicClass':
                        'Phatic',
                        'confidence':
                        999.0,
                        'topicKeywords': [{
                            'keyword': None,
                            'confidence': 999.0
                        }]
                    }],
                    'concept': [],
                    'sentiment':
                    None,
                    'coreference':
                    None,
                    'profanity_check':
                    None,
                    'converttext':
                    'my name is sebastian',
                    'npknowledge': {
                        'knowledge': [],
                        'noun_phrase': [],
                        'local_noun_phrase': []
                    },
                    'sentence_completion_text':
                    'my name is sebastian',
                    'intent':
                    'general',
                    'central_elem': {
                        'senti': 'neu',
                        'regex': {
                            'sys': [],
                            'topic': [],
                            'lexical': ['info_name']
                        },
                        'DA': 'statement',
                        'DA_score': '0.97639525',
                        'DA2': 'NA',
                        'DA2_score': '0.0',
                        'module': [],
                        'np': [],
                        'text': 'my name is sebastian',
                        'backstory': {
                            'bot_propose_module': '',
                            'confidence': 0.6335718035697937,
                            'followup': '',
                            'module': '',
                            'neg': '',
                            'pos': '',
                            'postfix': '',
                            'question': 'do you like my name',
                            'reason': '',
                            'tag': '',
                            'text': "Yeah, it's a nice name! "
                        }
                    },
                    'googlekg': [],
                    'dialog_act': [{
                        'text': 'my name is sebastian',
                        'DA': 'statement',
                        'confidence': 0.97639525,
                        'DA2': 'NA',
                        'confidence2': 0.0
                    }],
                    'nounphrase2': {
                        'noun_phrase': [[]],
                        'local_noun_phrase': [[]]
                    },
                    'text':
                    'my name is sebastian',
                    'intent_classify2': [{
                        'sys': [],
                        'topic': [],
                        'lexical': ['info_name']
                    }],
                    'asrcorrection':
                    None,
                    'sentiment_allennlp': [{
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    }],
                    'returnnlp': [{
                        'text': 'my name is sebastian',
                        'dialog_act': ['statement', '0.97639525'],
                        'dialog_act2': ['NA', '0.0'],
                        'intent': {
                            'sys': [],
                            'topic': [],
                            'lexical': ['info_name']
                        },
                        'sentiment': {
                            'compound': '0.5',
                            'neg': '0.05',
                            'neu': '0.9',
                            'pos': '0.05'
                        },
                        'googlekg': [],
                        'noun_phrase': [],
                        'topic': {
                            'text':
                            'my name is sebastian',
                            'topicClass':
                            'Phatic',
                            'confidence':
                            999.0,
                            'topicKeywords': [{
                                'keyword': None,
                                'confidence': 999.0
                            }]
                        },
                        'concept': [],
                        'amazon_topic': 'Phatic',
                        'dependency_parsing': {
                            'head': [2, 4, 4, 0],
                            'label':
                            ['nmod:poss', 'nsubj', 'aux', 'parataxis']
                        },
                        'pos_tagging': ['PRON', 'NOUN', 'AUX', 'ADJ']
                    }],
                    'intent_classify': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['info_name']
                    },
                    'segmentation': {
                        'segmented_text': ['my name is sebastian'],
                        'segmented_text_raw': ['my name is sebastian']
                    },
                    'spacynp':
                    None,
                    'topic2': [{
                        'text':
                        'my name is sebastian',
                        'topicClass':
                        'Phatic',
                        'confidence':
                        999.0,
                        'topicKeywords': [{
                            'keyword': None,
                            'confidence': 999.0
                        }]
                    }],
                    'amazon_topic':
                    'Phatic',
                    'dependency_parsing': {
                        'dependency_parent': [[2, 4, 4, 0]],
                        'dependency_label':
                        [['nmod:poss', 'nsubj', 'aux', 'parataxis']],
                        'pos_tagging': [['PRON', 'NOUN', 'AUX', 'ADJ']]
                    },
                    'topic_module':
                    '',
                    'topic_keywords': [{
                        'keyword': None,
                        'confidence': 999.0
                    }],
                    'knowledge': [],
                    'noun_phrase': []
                },
                'central_elem': {
                    'senti': 'neu',
                    'regex': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['info_name']
                    },
                    'DA': 'statement',
                    'DA_score': '0.97639525',
                    'DA2': 'NA',
                    'DA2_score': '0.0',
                    'module': [],
                    'np': [],
                    'text': 'my name is sebastian',
                    'backstory': {
                        'bot_propose_module': '',
                        'confidence': 0.6335718035697937,
                        'followup': '',
                        'module': '',
                        'neg': '',
                        'pos': '',
                        'postfix': '',
                        'question': 'do you like my name',
                        'reason': '',
                        'tag': '',
                        'text': "Yeah, it's a nice name! "
                    }
                },
                'returnnlp': [{
                    'text': 'my name is sebastian',
                    'dialog_act': ['statement', '0.97639525'],
                    'dialog_act2': ['NA', '0.0'],
                    'intent': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['info_name']
                    },
                    'sentiment': {
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    },
                    'googlekg': [],
                    'noun_phrase': [],
                    'topic': {
                        'text': 'my name is sebastian',
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': [{
                            'keyword': None,
                            'confidence': 999.0
                        }]
                    },
                    'concept': [],
                    'amazon_topic': 'Phatic',
                    'dependency_parsing': {
                        'head': [2, 4, 4, 0],
                        'label': ['nmod:poss', 'nsubj', 'aux', 'parataxis']
                    },
                    'pos_tagging': ['PRON', 'NOUN', 'AUX', 'ADJ']
                }],
                'a_b_test':
                'A',
                'system_acknowledgement': {
                    'output_tag': '',
                    'ack': ''
                },
                'bot_ask_open_question':
                None,
                'user_id':
                'localtest-user-90d61c3e-7849-4df5-8ff1-c5df5394577e',
                'conversationId':
                None,
                'news_user': {
                    'propose_continue': 'STOP'
                },
                'previous_modules':
                ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                'prev_hash': {
                    'sys_env': 'local'
                },
                'user_profile': {
                    'visit': 1,
                    'dominant_turn_count': 1,
                    'total_turn_count': 2,
                    'username': '<no_name>'
                },
                'template_manager': {
                    'prev_hash': {
                        'PPsGsg': ['0Bsr2Q'],
                        'ocgSRA': ['3skbGA'],
                        'KxoF0w': [],
                        '/M0N7w': ['5SE7MQ']
                    },
                    'selector_history': [
                        'error::s_fragments/reprompt/thanks_for_sharing',
                        'error::s_fragments/reprompt/feedback',
                        'error::reprompt'
                    ]
                },
                'techsciencechat': {
                    'propose_continue': 'STOP'
                },
                'animalchat': {
                    'propose_continue': 'STOP'
                },
                'fashionchat': {
                    'propose_continue': 'STOP'
                },
                'timechat': {
                    'propose_continue': 'STOP'
                },
                'module_selection': {
                    'used_topic': ['LAUNCHGREETING']
                },
                'topic_to_replace': [],
                'cur_selected_modules':
                'LAUNCHGREETING',
                'foodchat': {
                    'propose_continue': 'STOP'
                },
                'sys_env':
                'local',
                'bookchat_user': {
                    'propose_continue': 'STOP'
                },
                'socialchat': {
                    'curr_state': 's_init',
                    'propose_continue': 'STOP'
                },
                'module_rank': {
                    'MUSICCHAT': 0.0,
                    'GAMECHAT': 0.0,
                    'SPORT': 0.0,
                    'FASHIONCHAT': 0.0,
                    'TECHSCIENCECHAT': 0.0,
                    'TRAVELCHAT': 0.0,
                    'NEWS': 0.0,
                    'BOOKCHAT': 0.0,
                    'ANIMALCHAT': 0.0,
                    'FOODCHAT': 0.0,
                    'MOVIECHAT': 0.0
                },
                'travelchat': {
                    'propose_continue': 'STOP'
                },
                'gamechat': {
                    'propose_continue': 'STOP'
                },
                'weatherchat': {
                    'propose_continue': 'STOP'
                },
                'usr_name':
                '<no_name>',
                'moviechat_user': {
                    'propose_continue': 'STOP'
                },
                'sportchat': {
                    'propose_continue': 'STOP'
                },
                'last_module':
                'LAUNCHGREETING',
                'tedtalkchat': {
                    'propose_continue': 'STOP'
                },
                'outdoorchat': {
                    'propose_continue': 'STOP'
                },
                'launchgreeting': {
                    'volatile': {
                        'is_name_common': False
                    },
                    'persistent_store': {
                        'is_first_module': True
                    },
                    'propose_continue': 'CONTINUE',
                    'states': {
                        'state': 'greet_new_user'
                    }
                },
                'comfortchat': {
                    'propose_continue': 'STOP'
                },
                'holidaychat': {
                    'propose_continue': 'STOP'
                },
                'dailylife': {
                    'propose_continue': 'STOP'
                },
                'musicchat': {
                    'propose_continue': 'STOP'
                },
                'propose_topic':
                None,
                'last_turn_propose_open_question':
                None,
                'social_open_question_proposing':
                None
            },
            'ua_ref': {
                'user_id':
                'localtest-user-90d61c3e-7849-4df5-8ff1-c5df5394577e',
                'map_attributes': {
                    'conversationId':
                    None,
                    'news_user': {
                        'propose_continue': 'STOP'
                    },
                    'previous_modules':
                    ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                    'prev_hash': {
                        'sys_env': 'local'
                    },
                    'user_profile': {
                        'visit': 1,
                        'dominant_turn_count': 1,
                        'total_turn_count': 2,
                        'username': '<no_name>'
                    },
                    'template_manager': {
                        'prev_hash': {
                            'PPsGsg': ['0Bsr2Q'],
                            'ocgSRA': ['3skbGA'],
                            'KxoF0w': [],
                            '/M0N7w': ['5SE7MQ']
                        },
                        'selector_history': [
                            'error::s_fragments/reprompt/thanks_for_sharing',
                            'error::s_fragments/reprompt/feedback',
                            'error::reprompt'
                        ]
                    },
                    'techsciencechat': {
                        'propose_continue': 'STOP'
                    },
                    'animalchat': {
                        'propose_continue': 'STOP'
                    },
                    'fashionchat': {
                        'propose_continue': 'STOP'
                    },
                    'timechat': {
                        'propose_continue': 'STOP'
                    },
                    'module_selection': {
                        'used_topic': ['LAUNCHGREETING']
                    },
                    'topic_to_replace': [],
                    'cur_selected_modules':
                    'LAUNCHGREETING',
                    'foodchat': {
                        'propose_continue': 'STOP'
                    },
                    'sys_env':
                    'local',
                    'bookchat_user': {
                        'propose_continue': 'STOP'
                    },
                    'socialchat': {
                        'curr_state': 's_init',
                        'propose_continue': 'STOP'
                    },
                    'module_rank': {
                        'MUSICCHAT': 0.0,
                        'GAMECHAT': 0.0,
                        'SPORT': 0.0,
                        'FASHIONCHAT': 0.0,
                        'TECHSCIENCECHAT': 0.0,
                        'TRAVELCHAT': 0.0,
                        'NEWS': 0.0,
                        'BOOKCHAT': 0.0,
                        'ANIMALCHAT': 0.0,
                        'FOODCHAT': 0.0,
                        'MOVIECHAT': 0.0
                    },
                    'travelchat': {
                        'propose_continue': 'STOP'
                    },
                    'gamechat': {
                        'propose_continue': 'STOP'
                    },
                    'weatherchat': {
                        'propose_continue': 'STOP'
                    },
                    'usr_name':
                    '<no_name>',
                    'moviechat_user': {
                        'propose_continue': 'STOP'
                    },
                    'sportchat': {
                        'propose_continue': 'STOP'
                    },
                    'last_module':
                    'LAUNCHGREETING',
                    'tedtalkchat': {
                        'propose_continue': 'STOP'
                    },
                    'outdoorchat': {
                        'propose_continue': 'STOP'
                    },
                    'launchgreeting': {
                        'volatile': {
                            'is_name_common': False
                        },
                        'persistent_store': {
                            'is_first_module': True
                        },
                        'propose_continue': 'CONTINUE',
                        'states': {
                            'state': 'greet_new_user'
                        }
                    },
                    'comfortchat': {
                        'propose_continue': 'STOP'
                    },
                    'holidaychat': {
                        'propose_continue': 'STOP'
                    },
                    'dailylife': {
                        'propose_continue': 'STOP'
                    },
                    'musicchat': {
                        'propose_continue': 'STOP'
                    },
                    'propose_topic':
                    None,
                    'last_turn_propose_open_question':
                    None,
                    'social_open_question_proposing':
                    None
                }
            }
        }
    }, dict(outputs=dict(result=[1, 0, 0, 0], name='sebastian'))),
    ({
        'inputs': {
            'input_data': {
                'asr': [],
                'text':
                'my name is simon',
                'features': {
                    'text':
                    'my name is simon',
                    'googlekg': [],
                    'nounphrase2': {
                        'noun_phrase': [['simon']],
                        'local_noun_phrase': [[]]
                    },
                    'intent':
                    'general',
                    'profanity_check':
                    None,
                    'dependency_parsing': {
                        'dependency_parent': [[2, 4, 4, 0]],
                        'dependency_label':
                        [['nmod:poss', 'nsubj', 'aux', 'parataxis']],
                        'pos_tagging': [['PRON', 'NOUN', 'AUX', 'ADJ']]
                    },
                    'central_elem': {
                        'senti': 'neu',
                        'regex': {
                            'sys': [],
                            'topic': [],
                            'lexical': ['info_name']
                        },
                        'DA': 'statement',
                        'DA_score': '0.9695152',
                        'DA2': 'NA',
                        'DA2_score': '0.0',
                        'module': [],
                        'np': ['simon'],
                        'text': 'my name is simon',
                        'backstory': {
                            'bot_propose_module': '',
                            'confidence': 0.6230119466781616,
                            'followup': '',
                            'module': '',
                            'neg': '',
                            'pos': '',
                            'postfix': '',
                            'question': 'do you like my name',
                            'reason': '',
                            'tag': '',
                            'text': "Yeah, it's a nice name! "
                        }
                    },
                    'sentiment':
                    'neu',
                    'dialog_act': [{
                        'text': 'my name is simon',
                        'DA': 'statement',
                        'confidence': 0.9695152,
                        'DA2': 'NA',
                        'confidence2': 0.0
                    }],
                    'spacynp': ['simon'],
                    'intent_classify2': [{
                        'sys': [],
                        'topic': [],
                        'lexical': ['info_name']
                    }],
                    'topic2':
                    None,
                    'segmentation': {
                        'segmented_text': ['my name is ner'],
                        'segmented_text_raw': ['my name is simon']
                    },
                    'npknowledge': {
                        'knowledge': [[
                            'Simon', 'Fictional character',
                            '759.5074462890625', '__EMPTY__'
                        ]],
                        'noun_phrase': ['simon'],
                        'local_noun_phrase': []
                    },
                    'sentiment_allennlp': [{
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    }],
                    'returnnlp': [{
                        'text':
                        'my name is simon',
                        'dialog_act': ['statement', '0.9695152'],
                        'dialog_act2': ['NA', '0.0'],
                        'intent': {
                            'sys': [],
                            'topic': [],
                            'lexical': ['info_name']
                        },
                        'sentiment': {
                            'compound': '0.5',
                            'neg': '0.05',
                            'neu': '0.9',
                            'pos': '0.05'
                        },
                        'googlekg': [],
                        'noun_phrase': ['simon'],
                        'topic':
                        None,
                        'concept': [{
                            'noun': 'simon',
                            'data': {
                                'artist': '0.5231',
                                'researcher': '0.2615',
                                'character': '0.2154'
                            }
                        }],
                        'amazon_topic':
                        None,
                        'dependency_parsing': {
                            'head': [2, 4, 4, 0],
                            'label':
                            ['nmod:poss', 'nsubj', 'aux', 'parataxis']
                        },
                        'pos_tagging': ['PRON', 'NOUN', 'AUX', 'ADJ']
                    }],
                    'intent_classify': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['info_name']
                    },
                    'amazon_topic':
                    None,
                    'topic':
                    None,
                    'asrcorrection':
                    None,
                    'sentence_completion_text':
                    'my name is simon',
                    'coreference':
                    None,
                    'concept': [{
                        'noun': 'simon',
                        'data': {
                            'artist': '0.5231',
                            'researcher': '0.2615',
                            'character': '0.2154'
                        }
                    }],
                    'ner': [{
                        'text': 'Simon',
                        'begin_offset': 11,
                        'end_offset': 16,
                        'label': 'PERSON'
                    }],
                    'sentiment2': [{
                        'neg': '0.0',
                        'neu': '1.0',
                        'pos': '0.0',
                        'compound': '0.0'
                    }],
                    'converttext':
                    'my name is simon',
                    'topic_module':
                    '',
                    'topic_keywords':
                    '',
                    'knowledge': [[
                        'Simon', 'Fictional character', '759.5074462890625',
                        '__EMPTY__'
                    ]],
                    'noun_phrase': ['simon']
                },
                'central_elem': {
                    'senti': 'neu',
                    'regex': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['info_name']
                    },
                    'DA': 'statement',
                    'DA_score': '0.9695152',
                    'DA2': 'NA',
                    'DA2_score': '0.0',
                    'module': [],
                    'np': ['simon'],
                    'text': 'my name is simon',
                    'backstory': {
                        'bot_propose_module': '',
                        'confidence': 0.6230119466781616,
                        'followup': '',
                        'module': '',
                        'neg': '',
                        'pos': '',
                        'postfix': '',
                        'question': 'do you like my name',
                        'reason': '',
                        'tag': '',
                        'text': "Yeah, it's a nice name! "
                    }
                },
                'returnnlp': [{
                    'text':
                    'my name is simon',
                    'dialog_act': ['statement', '0.9695152'],
                    'dialog_act2': ['NA', '0.0'],
                    'intent': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['info_name']
                    },
                    'sentiment': {
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    },
                    'googlekg': [],
                    'noun_phrase': ['simon'],
                    'topic':
                    None,
                    'concept': [{
                        'noun': 'simon',
                        'data': {
                            'artist': '0.5231',
                            'researcher': '0.2615',
                            'character': '0.2154'
                        }
                    }],
                    'amazon_topic':
                    None,
                    'dependency_parsing': {
                        'head': [2, 4, 4, 0],
                        'label': ['nmod:poss', 'nsubj', 'aux', 'parataxis']
                    },
                    'pos_tagging': ['PRON', 'NOUN', 'AUX', 'ADJ']
                }],
                'a_b_test':
                'A',
                'system_acknowledgement': {
                    'output_tag': '',
                    'ack': ''
                },
                'bot_ask_open_question':
                None,
                'user_id':
                'localtest-user-6a9f30da-92fb-43cc-a07a-a72f7b44b736',
                'conversationId':
                None,
                'news_user': {
                    'propose_continue': 'STOP'
                },
                'previous_modules':
                ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                'prev_hash': {
                    'sys_env': 'local'
                },
                'user_profile': {
                    'visit': 1,
                    'dominant_turn_count': 1,
                    'total_turn_count': 2,
                    'username': '<no_name>'
                },
                'template_manager': {
                    'prev_hash': {
                        'PPsGsg': ['FlAjjQ'],
                        'ocgSRA': ['VS0VHQ'],
                        'KxoF0w': [],
                        '/M0N7w': ['t3gtIQ']
                    },
                    'selector_history': [
                        'error::s_fragments/reprompt/thanks_for_sharing',
                        'error::s_fragments/reprompt/feedback',
                        'error::reprompt'
                    ]
                },
                'techsciencechat': {
                    'propose_continue': 'STOP'
                },
                'animalchat': {
                    'propose_continue': 'STOP'
                },
                'fashionchat': {
                    'propose_continue': 'STOP'
                },
                'timechat': {
                    'propose_continue': 'STOP'
                },
                'module_selection': {
                    'used_topic': ['LAUNCHGREETING']
                },
                'topic_to_replace': [],
                'cur_selected_modules':
                'LAUNCHGREETING',
                'foodchat': {
                    'propose_continue': 'STOP'
                },
                'sys_env':
                'local',
                'bookchat_user': {
                    'propose_continue': 'STOP'
                },
                'socialchat': {
                    'curr_state': 's_init',
                    'propose_continue': 'STOP'
                },
                'module_rank': {
                    'MUSICCHAT': 0.0,
                    'GAMECHAT': 0.0,
                    'SPORT': 0.0,
                    'FASHIONCHAT': 0.0,
                    'TECHSCIENCECHAT': 0.0,
                    'TRAVELCHAT': 0.0,
                    'NEWS': 0.0,
                    'BOOKCHAT': 0.0,
                    'ANIMALCHAT': 0.0,
                    'FOODCHAT': 0.0,
                    'MOVIECHAT': 0.0
                },
                'travelchat': {
                    'propose_continue': 'STOP'
                },
                'gamechat': {
                    'propose_continue': 'STOP'
                },
                'weatherchat': {
                    'propose_continue': 'STOP'
                },
                'usr_name':
                '<no_name>',
                'moviechat_user': {
                    'propose_continue': 'STOP'
                },
                'sportchat': {
                    'propose_continue': 'STOP'
                },
                'last_module':
                'LAUNCHGREETING',
                'tedtalkchat': {
                    'propose_continue': 'STOP'
                },
                'outdoorchat': {
                    'propose_continue': 'STOP'
                },
                'launchgreeting': {
                    'volatile': {
                        'is_name_common': False
                    },
                    'persistent_store': {
                        'is_first_module': True
                    },
                    'propose_continue': 'CONTINUE',
                    'states': {
                        'state': 'greet_new_user'
                    }
                },
                'comfortchat': {
                    'propose_continue': 'STOP'
                },
                'holidaychat': {
                    'propose_continue': 'STOP'
                },
                'dailylife': {
                    'propose_continue': 'STOP'
                },
                'musicchat': {
                    'propose_continue': 'STOP'
                },
                'propose_topic':
                None,
                'last_turn_propose_open_question':
                None,
                'social_open_question_proposing':
                None
            },
            'ua_ref': {
                'user_id':
                'localtest-user-6a9f30da-92fb-43cc-a07a-a72f7b44b736',
                'map_attributes': {
                    'conversationId':
                    None,
                    'news_user': {
                        'propose_continue': 'STOP'
                    },
                    'previous_modules':
                    ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                    'prev_hash': {
                        'sys_env': 'local'
                    },
                    'user_profile': {
                        'visit': 1,
                        'dominant_turn_count': 1,
                        'total_turn_count': 2,
                        'username': '<no_name>'
                    },
                    'template_manager': {
                        'prev_hash': {
                            'PPsGsg': ['FlAjjQ'],
                            'ocgSRA': ['VS0VHQ'],
                            'KxoF0w': [],
                            '/M0N7w': ['t3gtIQ']
                        },
                        'selector_history': [
                            'error::s_fragments/reprompt/thanks_for_sharing',
                            'error::s_fragments/reprompt/feedback',
                            'error::reprompt'
                        ]
                    },
                    'techsciencechat': {
                        'propose_continue': 'STOP'
                    },
                    'animalchat': {
                        'propose_continue': 'STOP'
                    },
                    'fashionchat': {
                        'propose_continue': 'STOP'
                    },
                    'timechat': {
                        'propose_continue': 'STOP'
                    },
                    'module_selection': {
                        'used_topic': ['LAUNCHGREETING']
                    },
                    'topic_to_replace': [],
                    'cur_selected_modules':
                    'LAUNCHGREETING',
                    'foodchat': {
                        'propose_continue': 'STOP'
                    },
                    'sys_env':
                    'local',
                    'bookchat_user': {
                        'propose_continue': 'STOP'
                    },
                    'socialchat': {
                        'curr_state': 's_init',
                        'propose_continue': 'STOP'
                    },
                    'module_rank': {
                        'MUSICCHAT': 0.0,
                        'GAMECHAT': 0.0,
                        'SPORT': 0.0,
                        'FASHIONCHAT': 0.0,
                        'TECHSCIENCECHAT': 0.0,
                        'TRAVELCHAT': 0.0,
                        'NEWS': 0.0,
                        'BOOKCHAT': 0.0,
                        'ANIMALCHAT': 0.0,
                        'FOODCHAT': 0.0,
                        'MOVIECHAT': 0.0
                    },
                    'travelchat': {
                        'propose_continue': 'STOP'
                    },
                    'gamechat': {
                        'propose_continue': 'STOP'
                    },
                    'weatherchat': {
                        'propose_continue': 'STOP'
                    },
                    'usr_name':
                    '<no_name>',
                    'moviechat_user': {
                        'propose_continue': 'STOP'
                    },
                    'sportchat': {
                        'propose_continue': 'STOP'
                    },
                    'last_module':
                    'LAUNCHGREETING',
                    'tedtalkchat': {
                        'propose_continue': 'STOP'
                    },
                    'outdoorchat': {
                        'propose_continue': 'STOP'
                    },
                    'launchgreeting': {
                        'volatile': {
                            'is_name_common': False
                        },
                        'persistent_store': {
                            'is_first_module': True
                        },
                        'propose_continue': 'CONTINUE',
                        'states': {
                            'state': 'greet_new_user'
                        }
                    },
                    'comfortchat': {
                        'propose_continue': 'STOP'
                    },
                    'holidaychat': {
                        'propose_continue': 'STOP'
                    },
                    'dailylife': {
                        'propose_continue': 'STOP'
                    },
                    'musicchat': {
                        'propose_continue': 'STOP'
                    },
                    'propose_topic':
                    None,
                    'last_turn_propose_open_question':
                    None,
                    'social_open_question_proposing':
                    None
                }
            }
        }
    }, dict(outputs=dict(result=[1, 0, 0, 0], name='simon'))),
    ({
        'inputs': {
            'input_data': {
                'asr': [],
                'text':
                'austin',
                'features': {
                    'returnnlp': [{
                        'text':
                        'austin',
                        'dialog_act': ['statement', '0.86934686'],
                        'dialog_act2': ['NA', '0.0'],
                        'intent': {
                            'sys': [],
                            'topic': [],
                            'lexical': []
                        },
                        'sentiment': {
                            'compound': '0.5',
                            'neg': '0.05',
                            'neu': '0.9',
                            'pos': '0.05'
                        },
                        'googlekg': [],
                        'noun_phrase': ['austin'],
                        'topic':
                        None,
                        'concept': [{
                            'noun': 'austin',
                            'data': {
                                'city': '0.8153',
                                'place': '0.0923',
                                'market': '0.0923'
                            }
                        }],
                        'amazon_topic':
                        'Phatic',
                        'dependency_parsing': {
                            'head': [0],
                            'label': ['flat']
                        },
                        'pos_tagging': ['PROPN']
                    }],
                    'intent':
                    'general',
                    'converttext':
                    'austin',
                    'topic': [{
                        'text':
                        'austin',
                        'topicClass':
                        'Travel_Geo',
                        'confidence':
                        999.0,
                        'topicKeywords': [{
                            'keyword': 'austin',
                            'confidence': 999.0
                        }]
                    }],
                    'amazon_topic':
                    'Phatic',
                    'asrcorrection':
                    None,
                    'concept': [{
                        'noun': 'austin',
                        'data': {
                            'city': '0.8153',
                            'place': '0.0923',
                            'market': '0.0923'
                        }
                    }],
                    'sentiment':
                    None,
                    'topic2':
                    None,
                    'ner':
                    None,
                    'coreference':
                    None,
                    'dialog_act': [{
                        'text': 'austin',
                        'DA': 'statement',
                        'confidence': 0.86934686,
                        'DA2': 'NA',
                        'confidence2': 0.0
                    }],
                    'sentiment2':
                    None,
                    'intent_classify2': [{
                        'sys': [],
                        'topic': [],
                        'lexical': []
                    }],
                    'profanity_check':
                    None,
                    'central_elem': {
                        'senti': 'neu',
                        'regex': {
                            'sys': [],
                            'topic': [],
                            'lexical': []
                        },
                        'DA': 'statement',
                        'DA_score': '0.86934686',
                        'DA2': 'NA',
                        'DA2_score': '0.0',
                        'module': ['TRAVELCHAT'],
                        'np': ['austin'],
                        'text': 'austin',
                        'backstory': {
                            'bot_propose_module':
                            'TRAVELCHAT',
                            'confidence':
                            0.43601763248443604,
                            'followup':
                            'What about you? ',
                            'module':
                            'TRAVELCHAT',
                            'neg':
                            '',
                            'pos':
                            '',
                            'postfix':
                            '',
                            'question':
                            'what is your favorite city',
                            'reason':
                            '',
                            'tag':
                            'travel',
                            'text':
                            'My favorite city is San Francisco. I love the Golden Gate Bridge. '
                        }
                    },
                    'segmentation': {
                        'segmented_text': ['ner'],
                        'segmented_text_raw': ['austin']
                    },
                    'sentiment_allennlp': [{
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    }],
                    'text':
                    'austin',
                    'spacynp': ['austin'],
                    'dependency_parsing': {
                        'dependency_parent': [[0]],
                        'dependency_label': [['flat']],
                        'pos_tagging': [['PROPN']]
                    },
                    'npknowledge': {
                        'knowledge': [[
                            'Austin', 'City in Texas', '4704.84619140625',
                            'travel'
                        ]],
                        'noun_phrase': ['austin'],
                        'local_noun_phrase': ['austin']
                    },
                    'intent_classify': {
                        'sys': [],
                        'topic': [],
                        'lexical': []
                    },
                    'googlekg': [],
                    'nounphrase2': {
                        'noun_phrase': [['austin']],
                        'local_noun_phrase': [['austin']]
                    },
                    'sentence_completion_text':
                    'that’s austin',
                    'topic_module':
                    '',
                    'topic_keywords': [{
                        'keyword': 'austin',
                        'confidence': 999.0
                    }],
                    'knowledge':
                    [['Austin', 'City in Texas', '4704.84619140625',
                      'travel']],
                    'noun_phrase': ['austin']
                },
                'central_elem': {
                    'senti': 'neu',
                    'regex': {
                        'sys': [],
                        'topic': [],
                        'lexical': []
                    },
                    'DA': 'statement',
                    'DA_score': '0.86934686',
                    'DA2': 'NA',
                    'DA2_score': '0.0',
                    'module': ['TRAVELCHAT'],
                    'np': ['austin'],
                    'text': 'austin',
                    'backstory': {
                        'bot_propose_module':
                        'TRAVELCHAT',
                        'confidence':
                        0.43601763248443604,
                        'followup':
                        'What about you? ',
                        'module':
                        'TRAVELCHAT',
                        'neg':
                        '',
                        'pos':
                        '',
                        'postfix':
                        '',
                        'question':
                        'what is your favorite city',
                        'reason':
                        '',
                        'tag':
                        'travel',
                        'text':
                        'My favorite city is San Francisco. I love the Golden Gate Bridge. '
                    }
                },
                'returnnlp': [{
                    'text':
                    'austin',
                    'dialog_act': ['statement', '0.86934686'],
                    'dialog_act2': ['NA', '0.0'],
                    'intent': {
                        'sys': [],
                        'topic': [],
                        'lexical': []
                    },
                    'sentiment': {
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    },
                    'googlekg': [],
                    'noun_phrase': ['austin'],
                    'topic':
                    None,
                    'concept': [{
                        'noun': 'austin',
                        'data': {
                            'city': '0.8153',
                            'place': '0.0923',
                            'market': '0.0923'
                        }
                    }],
                    'amazon_topic':
                    'Phatic',
                    'dependency_parsing': {
                        'head': [0],
                        'label': ['flat']
                    },
                    'pos_tagging': ['PROPN']
                }],
                'a_b_test':
                'B',
                'system_acknowledgement': {
                    'output_tag': '',
                    'ack': ''
                },
                'bot_ask_open_question':
                None,
                'user_id':
                'localtest-user-e08bb06c-5f5e-474a-ac0f-74e5fbe592a6',
                'conversationId':
                None,
                'news_user': {
                    'propose_continue': 'STOP'
                },
                'previous_modules':
                ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                'prev_hash': {
                    'sys_env': 'local'
                },
                'user_profile': {
                    'visit': 1,
                    'dominant_turn_count': 1,
                    'total_turn_count': 2,
                    'username': '<no_name>'
                },
                'template_manager': {
                    'prev_hash': {
                        'PPsGsg': ['FlAjjQ'],
                        'ocgSRA': ['3skbGA'],
                        'KxoF0w': [],
                        '/M0N7w': ['5SE7MQ']
                    },
                    'selector_history': [
                        'error::s_fragments/reprompt/thanks_for_sharing',
                        'error::s_fragments/reprompt/feedback',
                        'error::reprompt'
                    ]
                },
                'techsciencechat': {
                    'propose_continue': 'STOP'
                },
                'animalchat': {
                    'propose_continue': 'STOP'
                },
                'fashionchat': {
                    'propose_continue': 'STOP'
                },
                'timechat': {
                    'propose_continue': 'STOP'
                },
                'module_selection': {
                    'used_topic': ['LAUNCHGREETING']
                },
                'topic_to_replace': [],
                'cur_selected_modules':
                'LAUNCHGREETING',
                'foodchat': {
                    'propose_continue': 'STOP'
                },
                'sys_env':
                'local',
                'bookchat_user': {
                    'propose_continue': 'STOP'
                },
                'socialchat': {
                    'curr_state': 's_init',
                    'propose_continue': 'STOP'
                },
                'module_rank': {
                    'MUSICCHAT': 0.0,
                    'GAMECHAT': 0.0,
                    'SPORT': 0.0,
                    'FASHIONCHAT': 0.0,
                    'TECHSCIENCECHAT': 0.0,
                    'TRAVELCHAT': 0.0,
                    'NEWS': 0.0,
                    'BOOKCHAT': 0.0,
                    'ANIMALCHAT': 0.0,
                    'FOODCHAT': 0.0,
                    'MOVIECHAT': 0.0
                },
                'travelchat': {
                    'propose_continue': 'STOP'
                },
                'gamechat': {
                    'propose_continue': 'STOP'
                },
                'weatherchat': {
                    'propose_continue': 'STOP'
                },
                'usr_name':
                '<no_name>',
                'moviechat_user': {
                    'propose_continue': 'STOP'
                },
                'sportchat': {
                    'propose_continue': 'STOP'
                },
                'last_module':
                'LAUNCHGREETING',
                'tedtalkchat': {
                    'propose_continue': 'STOP'
                },
                'outdoorchat': {
                    'propose_continue': 'STOP'
                },
                'launchgreeting': {
                    'volatile': {
                        'is_name_common': False
                    },
                    'persistent_store': {
                        'is_first_module': True
                    },
                    'propose_continue': 'CONTINUE',
                    'states': {
                        'state': 'greet_new_user'
                    }
                },
                'comfortchat': {
                    'propose_continue': 'STOP'
                },
                'holidaychat': {
                    'propose_continue': 'STOP'
                },
                'dailylife': {
                    'propose_continue': 'STOP'
                },
                'musicchat': {
                    'propose_continue': 'STOP'
                },
                'propose_topic':
                None,
                'last_turn_propose_open_question':
                None,
                'social_open_question_proposing':
                None
            },
            'ua_ref': {
                'user_id':
                'localtest-user-e08bb06c-5f5e-474a-ac0f-74e5fbe592a6',
                'map_attributes': {
                    'conversationId':
                    None,
                    'news_user': {
                        'propose_continue': 'STOP'
                    },
                    'previous_modules':
                    ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                    'prev_hash': {
                        'sys_env': 'local'
                    },
                    'user_profile': {
                        'visit': 1,
                        'dominant_turn_count': 1,
                        'total_turn_count': 2,
                        'username': '<no_name>'
                    },
                    'template_manager': {
                        'prev_hash': {
                            'PPsGsg': ['FlAjjQ'],
                            'ocgSRA': ['3skbGA'],
                            'KxoF0w': [],
                            '/M0N7w': ['5SE7MQ']
                        },
                        'selector_history': [
                            'error::s_fragments/reprompt/thanks_for_sharing',
                            'error::s_fragments/reprompt/feedback',
                            'error::reprompt'
                        ]
                    },
                    'techsciencechat': {
                        'propose_continue': 'STOP'
                    },
                    'animalchat': {
                        'propose_continue': 'STOP'
                    },
                    'fashionchat': {
                        'propose_continue': 'STOP'
                    },
                    'timechat': {
                        'propose_continue': 'STOP'
                    },
                    'module_selection': {
                        'used_topic': ['LAUNCHGREETING']
                    },
                    'topic_to_replace': [],
                    'cur_selected_modules':
                    'LAUNCHGREETING',
                    'foodchat': {
                        'propose_continue': 'STOP'
                    },
                    'sys_env':
                    'local',
                    'bookchat_user': {
                        'propose_continue': 'STOP'
                    },
                    'socialchat': {
                        'curr_state': 's_init',
                        'propose_continue': 'STOP'
                    },
                    'module_rank': {
                        'MUSICCHAT': 0.0,
                        'GAMECHAT': 0.0,
                        'SPORT': 0.0,
                        'FASHIONCHAT': 0.0,
                        'TECHSCIENCECHAT': 0.0,
                        'TRAVELCHAT': 0.0,
                        'NEWS': 0.0,
                        'BOOKCHAT': 0.0,
                        'ANIMALCHAT': 0.0,
                        'FOODCHAT': 0.0,
                        'MOVIECHAT': 0.0
                    },
                    'travelchat': {
                        'propose_continue': 'STOP'
                    },
                    'gamechat': {
                        'propose_continue': 'STOP'
                    },
                    'weatherchat': {
                        'propose_continue': 'STOP'
                    },
                    'usr_name':
                    '<no_name>',
                    'moviechat_user': {
                        'propose_continue': 'STOP'
                    },
                    'sportchat': {
                        'propose_continue': 'STOP'
                    },
                    'last_module':
                    'LAUNCHGREETING',
                    'tedtalkchat': {
                        'propose_continue': 'STOP'
                    },
                    'outdoorchat': {
                        'propose_continue': 'STOP'
                    },
                    'launchgreeting': {
                        'volatile': {
                            'is_name_common': False
                        },
                        'persistent_store': {
                            'is_first_module': True
                        },
                        'propose_continue': 'CONTINUE',
                        'states': {
                            'state': 'greet_new_user'
                        }
                    },
                    'comfortchat': {
                        'propose_continue': 'STOP'
                    },
                    'holidaychat': {
                        'propose_continue': 'STOP'
                    },
                    'dailylife': {
                        'propose_continue': 'STOP'
                    },
                    'musicchat': {
                        'propose_continue': 'STOP'
                    },
                    'propose_topic':
                    None,
                    'last_turn_propose_open_question':
                    None,
                    'social_open_question_proposing':
                    None
                }
            }
        }
    }, dict(outputs=dict(result=[1, 0, 0, 0], name='austin'))),
    ({
        'inputs': {
            'input_data': {
                'asr': [],
                'text':
                'my name is susan',
                'features': {
                    'googlekg': [],
                    'dialog_act': [{
                        'text': 'my name is susan',
                        'DA': 'statement',
                        'confidence': 0.9765288,
                        'DA2': 'NA',
                        'confidence2': 0.0
                    }],
                    'intent_classify2': [{
                        'sys': [],
                        'topic': [],
                        'lexical': ['info_name']
                    }],
                    'text':
                    'my name is susan',
                    'dependency_parsing': {
                        'dependency_parent': [[2, 4, 4, 0]],
                        'dependency_label':
                        [['nmod:poss', 'nsubj', 'cop', 'ccomp']],
                        'pos_tagging': [['PRON', 'NOUN', 'AUX', 'PROPN']]
                    },
                    'concept': [{
                        'noun': 'susan',
                        'data': {
                            'name': '0.4091',
                            'character': '0.3182',
                            'person': '0.2727'
                        }
                    }],
                    'sentiment2':
                    None,
                    'spacynp':
                    None,
                    'topic2':
                    None,
                    'nounphrase2': {
                        'noun_phrase': [['susan']],
                        'local_noun_phrase': [['susan']]
                    },
                    'sentence_completion_text':
                    'my name is susan',
                    'intent_classify': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['info_name']
                    },
                    'coreference':
                    None,
                    'npknowledge': {
                        'knowledge': [[
                            'Susan', 'Iranian singer', '1178.496459960938',
                            'music'
                        ]],
                        'noun_phrase': ['susan'],
                        'local_noun_phrase': ['susan']
                    },
                    'returnnlp': [{
                        'text':
                        'my name is susan',
                        'dialog_act': ['statement', '0.9765288'],
                        'dialog_act2': ['NA', '0.0'],
                        'intent': {
                            'sys': [],
                            'topic': [],
                            'lexical': ['info_name']
                        },
                        'sentiment': {
                            'compound': '0.5',
                            'neg': '0.05',
                            'neu': '0.9',
                            'pos': '0.05'
                        },
                        'googlekg': [],
                        'noun_phrase': ['susan'],
                        'topic':
                        None,
                        'concept': [{
                            'noun': 'susan',
                            'data': {
                                'name': '0.4091',
                                'character': '0.3182',
                                'person': '0.2727'
                            }
                        }],
                        'amazon_topic':
                        'Phatic',
                        'dependency_parsing': {
                            'head': [2, 4, 4, 0],
                            'label': ['nmod:poss', 'nsubj', 'cop', 'ccomp']
                        },
                        'pos_tagging': ['PRON', 'NOUN', 'AUX', 'PROPN']
                    }],
                    'topic': [{
                        'text':
                        'my name is susan',
                        'topicClass':
                        'Phatic',
                        'confidence':
                        999.0,
                        'topicKeywords': [{
                            'keyword': None,
                            'confidence': 999.0
                        }]
                    }],
                    'sentiment_allennlp': [{
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    }],
                    'ner':
                    None,
                    'central_elem': {
                        'senti': 'neu',
                        'regex': {
                            'sys': [],
                            'topic': [],
                            'lexical': ['info_name']
                        },
                        'DA': 'statement',
                        'DA_score': '0.9765288',
                        'DA2': 'NA',
                        'DA2_score': '0.0',
                        'module': ['MUSICCHAT'],
                        'np': ['susan'],
                        'text': 'my name is susan',
                        'backstory': {
                            'bot_propose_module': '',
                            'confidence': 0.6311175227165222,
                            'followup': '',
                            'module': '',
                            'neg': '',
                            'pos': '',
                            'postfix': '',
                            'question': 'do you like my name',
                            'reason': '',
                            'tag': '',
                            'text': "Yeah, it's a nice name! "
                        }
                    },
                    'converttext':
                    'my name is susan',
                    'sentiment':
                    None,
                    'profanity_check':
                    None,
                    'amazon_topic':
                    'Phatic',
                    'asrcorrection':
                    None,
                    'intent':
                    'general',
                    'segmentation': {
                        'segmented_text': ['my name is susan'],
                        'segmented_text_raw': ['my name is susan']
                    },
                    'topic_module':
                    '',
                    'topic_keywords': [{
                        'keyword': None,
                        'confidence': 999.0
                    }],
                    'knowledge':
                    [['Susan', 'Iranian singer', '1178.496459960938',
                      'music']],
                    'noun_phrase': ['susan']
                },
                'central_elem': {
                    'senti': 'neu',
                    'regex': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['info_name']
                    },
                    'DA': 'statement',
                    'DA_score': '0.9765288',
                    'DA2': 'NA',
                    'DA2_score': '0.0',
                    'module': ['MUSICCHAT'],
                    'np': ['susan'],
                    'text': 'my name is susan',
                    'backstory': {
                        'bot_propose_module': '',
                        'confidence': 0.6311175227165222,
                        'followup': '',
                        'module': '',
                        'neg': '',
                        'pos': '',
                        'postfix': '',
                        'question': 'do you like my name',
                        'reason': '',
                        'tag': '',
                        'text': "Yeah, it's a nice name! "
                    }
                },
                'returnnlp': [{
                    'text':
                    'my name is susan',
                    'dialog_act': ['statement', '0.9765288'],
                    'dialog_act2': ['NA', '0.0'],
                    'intent': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['info_name']
                    },
                    'sentiment': {
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    },
                    'googlekg': [],
                    'noun_phrase': ['susan'],
                    'topic':
                    None,
                    'concept': [{
                        'noun': 'susan',
                        'data': {
                            'name': '0.4091',
                            'character': '0.3182',
                            'person': '0.2727'
                        }
                    }],
                    'amazon_topic':
                    'Phatic',
                    'dependency_parsing': {
                        'head': [2, 4, 4, 0],
                        'label': ['nmod:poss', 'nsubj', 'cop', 'ccomp']
                    },
                    'pos_tagging': ['PRON', 'NOUN', 'AUX', 'PROPN']
                }],
                'a_b_test':
                'A',
                'system_acknowledgement': {
                    'output_tag': '',
                    'ack': ''
                },
                'bot_ask_open_question':
                None,
                'user_id':
                'localtest-user-e9ce6b2c-6ed0-4291-926d-1dab56b18e72',
                'conversationId':
                None,
                'news_user': {
                    'propose_continue': 'STOP'
                },
                'previous_modules':
                ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                'prev_hash': {
                    'sys_env': 'local'
                },
                'user_profile': {
                    'visit': 1,
                    'dominant_turn_count': 1,
                    'total_turn_count': 2,
                    'username': '<no_name>'
                },
                'template_manager': {
                    'prev_hash': {
                        'PPsGsg': ['0Bsr2Q'],
                        'ocgSRA': ['VS0VHQ'],
                        'KxoF0w': [],
                        '/M0N7w': ['5SE7MQ']
                    },
                    'selector_history': [
                        'error::s_fragments/reprompt/thanks_for_sharing',
                        'error::s_fragments/reprompt/feedback',
                        'error::reprompt'
                    ]
                },
                'techsciencechat': {
                    'propose_continue': 'STOP'
                },
                'animalchat': {
                    'propose_continue': 'STOP'
                },
                'fashionchat': {
                    'propose_continue': 'STOP'
                },
                'timechat': {
                    'propose_continue': 'STOP'
                },
                'module_selection': {
                    'used_topic': ['LAUNCHGREETING']
                },
                'topic_to_replace': [],
                'cur_selected_modules':
                'LAUNCHGREETING',
                'foodchat': {
                    'propose_continue': 'STOP'
                },
                'sys_env':
                'local',
                'bookchat_user': {
                    'propose_continue': 'STOP'
                },
                'socialchat': {
                    'curr_state': 's_init',
                    'propose_continue': 'STOP'
                },
                'module_rank': {
                    'MUSICCHAT': 0.0,
                    'GAMECHAT': 0.0,
                    'SPORT': 0.0,
                    'FASHIONCHAT': 0.0,
                    'TECHSCIENCECHAT': 0.0,
                    'TRAVELCHAT': 0.0,
                    'NEWS': 0.0,
                    'BOOKCHAT': 0.0,
                    'ANIMALCHAT': 0.0,
                    'FOODCHAT': 0.0,
                    'MOVIECHAT': 0.0
                },
                'travelchat': {
                    'propose_continue': 'STOP'
                },
                'gamechat': {
                    'propose_continue': 'STOP'
                },
                'weatherchat': {
                    'propose_continue': 'STOP'
                },
                'usr_name':
                '<no_name>',
                'moviechat_user': {
                    'propose_continue': 'STOP'
                },
                'sportchat': {
                    'propose_continue': 'STOP'
                },
                'last_module':
                'LAUNCHGREETING',
                'tedtalkchat': {
                    'propose_continue': 'STOP'
                },
                'outdoorchat': {
                    'propose_continue': 'STOP'
                },
                'launchgreeting': {
                    'volatile': {
                        'is_name_common': False
                    },
                    'persistent_store': {
                        'is_first_module': True
                    },
                    'propose_continue': 'CONTINUE',
                    'states': {
                        'state': 'greet_new_user'
                    }
                },
                'comfortchat': {
                    'propose_continue': 'STOP'
                },
                'holidaychat': {
                    'propose_continue': 'STOP'
                },
                'dailylife': {
                    'propose_continue': 'STOP'
                },
                'musicchat': {
                    'propose_continue': 'STOP'
                },
                'propose_topic':
                None,
                'last_turn_propose_open_question':
                None,
                'social_open_question_proposing':
                None
            },
            'ua_ref': {
                'user_id':
                'localtest-user-e9ce6b2c-6ed0-4291-926d-1dab56b18e72',
                'map_attributes': {
                    'conversationId':
                    None,
                    'news_user': {
                        'propose_continue': 'STOP'
                    },
                    'previous_modules':
                    ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                    'prev_hash': {
                        'sys_env': 'local'
                    },
                    'user_profile': {
                        'visit': 1,
                        'dominant_turn_count': 1,
                        'total_turn_count': 2,
                        'username': '<no_name>'
                    },
                    'template_manager': {
                        'prev_hash': {
                            'PPsGsg': ['0Bsr2Q'],
                            'ocgSRA': ['VS0VHQ'],
                            'KxoF0w': [],
                            '/M0N7w': ['5SE7MQ']
                        },
                        'selector_history': [
                            'error::s_fragments/reprompt/thanks_for_sharing',
                            'error::s_fragments/reprompt/feedback',
                            'error::reprompt'
                        ]
                    },
                    'techsciencechat': {
                        'propose_continue': 'STOP'
                    },
                    'animalchat': {
                        'propose_continue': 'STOP'
                    },
                    'fashionchat': {
                        'propose_continue': 'STOP'
                    },
                    'timechat': {
                        'propose_continue': 'STOP'
                    },
                    'module_selection': {
                        'used_topic': ['LAUNCHGREETING']
                    },
                    'topic_to_replace': [],
                    'cur_selected_modules':
                    'LAUNCHGREETING',
                    'foodchat': {
                        'propose_continue': 'STOP'
                    },
                    'sys_env':
                    'local',
                    'bookchat_user': {
                        'propose_continue': 'STOP'
                    },
                    'socialchat': {
                        'curr_state': 's_init',
                        'propose_continue': 'STOP'
                    },
                    'module_rank': {
                        'MUSICCHAT': 0.0,
                        'GAMECHAT': 0.0,
                        'SPORT': 0.0,
                        'FASHIONCHAT': 0.0,
                        'TECHSCIENCECHAT': 0.0,
                        'TRAVELCHAT': 0.0,
                        'NEWS': 0.0,
                        'BOOKCHAT': 0.0,
                        'ANIMALCHAT': 0.0,
                        'FOODCHAT': 0.0,
                        'MOVIECHAT': 0.0
                    },
                    'travelchat': {
                        'propose_continue': 'STOP'
                    },
                    'gamechat': {
                        'propose_continue': 'STOP'
                    },
                    'weatherchat': {
                        'propose_continue': 'STOP'
                    },
                    'usr_name':
                    '<no_name>',
                    'moviechat_user': {
                        'propose_continue': 'STOP'
                    },
                    'sportchat': {
                        'propose_continue': 'STOP'
                    },
                    'last_module':
                    'LAUNCHGREETING',
                    'tedtalkchat': {
                        'propose_continue': 'STOP'
                    },
                    'outdoorchat': {
                        'propose_continue': 'STOP'
                    },
                    'launchgreeting': {
                        'volatile': {
                            'is_name_common': False
                        },
                        'persistent_store': {
                            'is_first_module': True
                        },
                        'propose_continue': 'CONTINUE',
                        'states': {
                            'state': 'greet_new_user'
                        }
                    },
                    'comfortchat': {
                        'propose_continue': 'STOP'
                    },
                    'holidaychat': {
                        'propose_continue': 'STOP'
                    },
                    'dailylife': {
                        'propose_continue': 'STOP'
                    },
                    'musicchat': {
                        'propose_continue': 'STOP'
                    },
                    'propose_topic':
                    None,
                    'last_turn_propose_open_question':
                    None,
                    'social_open_question_proposing':
                    None
                }
            }
        }
    }, dict(outputs=dict(result=[1, 0, 0, 0], name='susan'))),
    ({
        'inputs': {
            'input_data': {
                'asr': [],
                'text':
                'my name is john',
                'features': {
                    'segmentation': {
                        'segmented_text': ['my name is john'],
                        'segmented_text_raw': ['my name is john']
                    },
                    'dialog_act': [{
                        'text': 'my name is john',
                        'DA': 'statement',
                        'confidence': 0.9755308,
                        'DA2': 'NA',
                        'confidence2': 0.0
                    }],
                    'profanity_check':
                    None,
                    'central_elem': {
                        'senti': 'neu',
                        'regex': {
                            'sys': [],
                            'topic': [],
                            'lexical': ['info_name']
                        },
                        'DA': 'statement',
                        'DA_score': '0.9755308',
                        'DA2': 'NA',
                        'DA2_score': '0.0',
                        'module': [],
                        'np': ['john'],
                        'text': 'my name is john',
                        'backstory': {
                            'bot_propose_module':
                            '',
                            'confidence':
                            0.6457691192626953,
                            'followup':
                            '',
                            'module':
                            '',
                            'neg':
                            '',
                            'pos':
                            '',
                            'postfix':
                            '',
                            'question':
                            'What is your name',
                            'reason':
                            '',
                            'tag':
                            '',
                            'text':
                            "My name is Alexa. I can't tell you much more because I'm in a competition right now."
                        }
                    },
                    'intent_classify2': [{
                        'sys': [],
                        'topic': [],
                        'lexical': ['info_name']
                    }],
                    'topic2': [{
                        'text':
                        'my name is john',
                        'topicClass':
                        'Phatic',
                        'confidence':
                        999.0,
                        'topicKeywords': [{
                            'keyword': None,
                            'confidence': 999.0
                        }]
                    }],
                    'text':
                    'my name is john',
                    'topic': [{
                        'text':
                        'my name is john',
                        'topicClass':
                        'Phatic',
                        'confidence':
                        999.0,
                        'topicKeywords': [{
                            'keyword': None,
                            'confidence': 999.0
                        }]
                    }],
                    'coreference':
                    None,
                    'sentiment2':
                    None,
                    'dependency_parsing': {
                        'dependency_parent': [[2, 4, 4, 0]],
                        'dependency_label':
                        [['nmod:poss', 'nsubj', 'cop', 'obj']],
                        'pos_tagging': [['PRON', 'NOUN', 'AUX', 'PROPN']]
                    },
                    'intent':
                    'general',
                    'googlekg': [],
                    'converttext':
                    'my name is john',
                    'spacynp': ['john'],
                    'returnnlp': [{
                        'text':
                        'my name is john',
                        'dialog_act': ['statement', '0.9755308'],
                        'dialog_act2': ['NA', '0.0'],
                        'intent': {
                            'sys': [],
                            'topic': [],
                            'lexical': ['info_name']
                        },
                        'sentiment': {
                            'compound': '0.5',
                            'neg': '0.05',
                            'neu': '0.9',
                            'pos': '0.05'
                        },
                        'googlekg': [],
                        'noun_phrase': ['john'],
                        'topic': {
                            'text':
                            'my name is john',
                            'topicClass':
                            'Phatic',
                            'confidence':
                            999.0,
                            'topicKeywords': [{
                                'keyword': None,
                                'confidence': 999.0
                            }]
                        },
                        'concept': [{
                            'noun': 'john',
                            'data': {
                                'name': '0.5174',
                                'apostle': '0.2537',
                                'person': '0.2289'
                            }
                        }],
                        'amazon_topic':
                        None,
                        'dependency_parsing': {
                            'head': [2, 4, 4, 0],
                            'label': ['nmod:poss', 'nsubj', 'cop', 'obj']
                        },
                        'pos_tagging': ['PRON', 'NOUN', 'AUX', 'PROPN']
                    }],
                    'asrcorrection':
                    None,
                    'ner':
                    None,
                    'nounphrase2': {
                        'noun_phrase': [['john']],
                        'local_noun_phrase': [['john']]
                    },
                    'sentence_completion_text':
                    'my name is john',
                    'npknowledge': {
                        'knowledge': [],
                        'noun_phrase': ['john'],
                        'local_noun_phrase': ['john']
                    },
                    'concept': [{
                        'noun': 'john',
                        'data': {
                            'name': '0.5174',
                            'apostle': '0.2537',
                            'person': '0.2289'
                        }
                    }],
                    'sentiment_allennlp': [{
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    }],
                    'sentiment':
                    None,
                    'amazon_topic':
                    None,
                    'intent_classify': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['info_name']
                    },
                    'topic_module':
                    '',
                    'topic_keywords': [{
                        'keyword': None,
                        'confidence': 999.0
                    }],
                    'knowledge': [],
                    'noun_phrase': ['john']
                },
                'central_elem': {
                    'senti': 'neu',
                    'regex': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['info_name']
                    },
                    'DA': 'statement',
                    'DA_score': '0.9755308',
                    'DA2': 'NA',
                    'DA2_score': '0.0',
                    'module': [],
                    'np': ['john'],
                    'text': 'my name is john',
                    'backstory': {
                        'bot_propose_module':
                        '',
                        'confidence':
                        0.6457691192626953,
                        'followup':
                        '',
                        'module':
                        '',
                        'neg':
                        '',
                        'pos':
                        '',
                        'postfix':
                        '',
                        'question':
                        'What is your name',
                        'reason':
                        '',
                        'tag':
                        '',
                        'text':
                        "My name is Alexa. I can't tell you much more because I'm in a competition right now."
                    }
                },
                'returnnlp': [{
                    'text':
                    'my name is john',
                    'dialog_act': ['statement', '0.9755308'],
                    'dialog_act2': ['NA', '0.0'],
                    'intent': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['info_name']
                    },
                    'sentiment': {
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    },
                    'googlekg': [],
                    'noun_phrase': ['john'],
                    'topic': {
                        'text': 'my name is john',
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': [{
                            'keyword': None,
                            'confidence': 999.0
                        }]
                    },
                    'concept': [{
                        'noun': 'john',
                        'data': {
                            'name': '0.5174',
                            'apostle': '0.2537',
                            'person': '0.2289'
                        }
                    }],
                    'amazon_topic':
                    None,
                    'dependency_parsing': {
                        'head': [2, 4, 4, 0],
                        'label': ['nmod:poss', 'nsubj', 'cop', 'obj']
                    },
                    'pos_tagging': ['PRON', 'NOUN', 'AUX', 'PROPN']
                }],
                'a_b_test':
                'B',
                'system_acknowledgement': {
                    'output_tag': '',
                    'ack': ''
                },
                'bot_ask_open_question':
                None,
                'user_id':
                'localtest-user-588ad897-1852-4df9-8d68-3eec02d135c1',
                'conversationId':
                None,
                'news_user': {
                    'propose_continue': 'STOP'
                },
                'previous_modules':
                ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                'prev_hash': {
                    'sys_env': 'local'
                },
                'user_profile': {
                    'visit': 1,
                    'dominant_turn_count': 1,
                    'total_turn_count': 2,
                    'username': '<no_name>'
                },
                'template_manager': {
                    'prev_hash': {
                        'PPsGsg': ['fLkfQw'],
                        'ocgSRA': ['VS0VHQ'],
                        'KxoF0w': [],
                        '/M0N7w': ['+ec58w']
                    },
                    'selector_history': [
                        'error::s_fragments/reprompt/thanks_for_sharing',
                        'error::s_fragments/reprompt/feedback',
                        'error::reprompt'
                    ]
                },
                'techsciencechat': {
                    'propose_continue': 'STOP'
                },
                'animalchat': {
                    'propose_continue': 'STOP'
                },
                'fashionchat': {
                    'propose_continue': 'STOP'
                },
                'timechat': {
                    'propose_continue': 'STOP'
                },
                'module_selection': {
                    'used_topic': ['LAUNCHGREETING']
                },
                'topic_to_replace': [],
                'cur_selected_modules':
                'LAUNCHGREETING',
                'foodchat': {
                    'propose_continue': 'STOP'
                },
                'sys_env':
                'local',
                'bookchat_user': {
                    'propose_continue': 'STOP'
                },
                'socialchat': {
                    'curr_state': 's_init',
                    'propose_continue': 'STOP'
                },
                'module_rank': {
                    'MUSICCHAT': 0.0,
                    'GAMECHAT': 0.0,
                    'SPORT': 0.0,
                    'FASHIONCHAT': 0.0,
                    'TECHSCIENCECHAT': 0.0,
                    'TRAVELCHAT': 0.0,
                    'NEWS': 0.0,
                    'BOOKCHAT': 0.0,
                    'ANIMALCHAT': 0.0,
                    'FOODCHAT': 0.0,
                    'MOVIECHAT': 0.0
                },
                'travelchat': {
                    'propose_continue': 'STOP'
                },
                'gamechat': {
                    'propose_continue': 'STOP'
                },
                'weatherchat': {
                    'propose_continue': 'STOP'
                },
                'usr_name':
                '<no_name>',
                'moviechat_user': {
                    'propose_continue': 'STOP'
                },
                'sportchat': {
                    'propose_continue': 'STOP'
                },
                'last_module':
                'LAUNCHGREETING',
                'tedtalkchat': {
                    'propose_continue': 'STOP'
                },
                'outdoorchat': {
                    'propose_continue': 'STOP'
                },
                'launchgreeting': {
                    'volatile': {
                        'is_name_common': False
                    },
                    'persistent_store': {
                        'is_first_module': True
                    },
                    'propose_continue': 'CONTINUE',
                    'states': {
                        'state': 'greet_new_user'
                    }
                },
                'comfortchat': {
                    'propose_continue': 'STOP'
                },
                'holidaychat': {
                    'propose_continue': 'STOP'
                },
                'dailylife': {
                    'propose_continue': 'STOP'
                },
                'musicchat': {
                    'propose_continue': 'STOP'
                },
                'propose_topic':
                None,
                'last_turn_propose_open_question':
                None,
                'social_open_question_proposing':
                None
            },
            'ua_ref': {
                'user_id':
                'localtest-user-588ad897-1852-4df9-8d68-3eec02d135c1',
                'map_attributes': {
                    'conversationId':
                    None,
                    'news_user': {
                        'propose_continue': 'STOP'
                    },
                    'previous_modules':
                    ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                    'prev_hash': {
                        'sys_env': 'local'
                    },
                    'user_profile': {
                        'visit': 1,
                        'dominant_turn_count': 1,
                        'total_turn_count': 2,
                        'username': '<no_name>'
                    },
                    'template_manager': {
                        'prev_hash': {
                            'PPsGsg': ['fLkfQw'],
                            'ocgSRA': ['VS0VHQ'],
                            'KxoF0w': [],
                            '/M0N7w': ['+ec58w']
                        },
                        'selector_history': [
                            'error::s_fragments/reprompt/thanks_for_sharing',
                            'error::s_fragments/reprompt/feedback',
                            'error::reprompt'
                        ]
                    },
                    'techsciencechat': {
                        'propose_continue': 'STOP'
                    },
                    'animalchat': {
                        'propose_continue': 'STOP'
                    },
                    'fashionchat': {
                        'propose_continue': 'STOP'
                    },
                    'timechat': {
                        'propose_continue': 'STOP'
                    },
                    'module_selection': {
                        'used_topic': ['LAUNCHGREETING']
                    },
                    'topic_to_replace': [],
                    'cur_selected_modules':
                    'LAUNCHGREETING',
                    'foodchat': {
                        'propose_continue': 'STOP'
                    },
                    'sys_env':
                    'local',
                    'bookchat_user': {
                        'propose_continue': 'STOP'
                    },
                    'socialchat': {
                        'curr_state': 's_init',
                        'propose_continue': 'STOP'
                    },
                    'module_rank': {
                        'MUSICCHAT': 0.0,
                        'GAMECHAT': 0.0,
                        'SPORT': 0.0,
                        'FASHIONCHAT': 0.0,
                        'TECHSCIENCECHAT': 0.0,
                        'TRAVELCHAT': 0.0,
                        'NEWS': 0.0,
                        'BOOKCHAT': 0.0,
                        'ANIMALCHAT': 0.0,
                        'FOODCHAT': 0.0,
                        'MOVIECHAT': 0.0
                    },
                    'travelchat': {
                        'propose_continue': 'STOP'
                    },
                    'gamechat': {
                        'propose_continue': 'STOP'
                    },
                    'weatherchat': {
                        'propose_continue': 'STOP'
                    },
                    'usr_name':
                    '<no_name>',
                    'moviechat_user': {
                        'propose_continue': 'STOP'
                    },
                    'sportchat': {
                        'propose_continue': 'STOP'
                    },
                    'last_module':
                    'LAUNCHGREETING',
                    'tedtalkchat': {
                        'propose_continue': 'STOP'
                    },
                    'outdoorchat': {
                        'propose_continue': 'STOP'
                    },
                    'launchgreeting': {
                        'volatile': {
                            'is_name_common': False
                        },
                        'persistent_store': {
                            'is_first_module': True
                        },
                        'propose_continue': 'CONTINUE',
                        'states': {
                            'state': 'greet_new_user'
                        }
                    },
                    'comfortchat': {
                        'propose_continue': 'STOP'
                    },
                    'holidaychat': {
                        'propose_continue': 'STOP'
                    },
                    'dailylife': {
                        'propose_continue': 'STOP'
                    },
                    'musicchat': {
                        'propose_continue': 'STOP'
                    },
                    'propose_topic':
                    None,
                    'last_turn_propose_open_question':
                    None,
                    'social_open_question_proposing':
                    None
                }
            }
        }
    }, dict(outputs=dict(result=[1, 0, 0, 0], name='john'))),
    ({
        'inputs': {
            'input_data': {
                'asr': [],
                'text':
                'nicky',
                'features': {
                    'ner':
                    None,
                    'sentiment':
                    None,
                    'central_elem': {
                        'senti': 'neu',
                        'regex': {
                            'sys': [],
                            'topic': [],
                            'lexical': []
                        },
                        'DA': 'statement',
                        'DA_score': '0.83601874',
                        'DA2': 'NA',
                        'DA2_score': '0.0',
                        'module': [],
                        'np': ['nicky'],
                        'text': 'nicky',
                        'backstory': {
                            'bot_propose_module':
                            '',
                            'confidence':
                            0.44334644079208374,
                            'followup':
                            '',
                            'module':
                            '',
                            'neg':
                            '',
                            'pos':
                            '',
                            'postfix':
                            '',
                            'question':
                            'meow',
                            'reason':
                            '',
                            'tag':
                            '',
                            'text':
                            '<say-as interpret-as="interjection"> meow, meow </say-as>! I love cats! Let\'s talk about animals! '
                        }
                    },
                    'nounphrase2': {
                        'noun_phrase': [['nicky']],
                        'local_noun_phrase': [['nicky']]
                    },
                    'segmentation': {
                        'segmented_text': ['nicky'],
                        'segmented_text_raw': ['nicky']
                    },
                    'amazon_topic':
                    'Phatic',
                    'text':
                    'nicky',
                    'spacynp': ['nicky'],
                    'topic': [{
                        'text':
                        'nicky',
                        'topicClass':
                        'Celebrities',
                        'confidence':
                        999.0,
                        'topicKeywords': [{
                            'keyword': 'nicky',
                            'confidence': 999.0
                        }]
                    }],
                    'sentiment_allennlp': [{
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    }],
                    'concept': [{
                        'noun': 'nicky',
                        'data': {
                            'western name': '0.4211',
                            'celebrity': '0.3684',
                            'person': '0.2105'
                        }
                    }],
                    'npknowledge': {
                        'knowledge': [],
                        'noun_phrase': ['nicky'],
                        'local_noun_phrase': ['nicky']
                    },
                    'returnnlp': [{
                        'text':
                        'nicky',
                        'dialog_act': ['statement', '0.83601874'],
                        'dialog_act2': ['NA', '0.0'],
                        'intent': {
                            'sys': [],
                            'topic': [],
                            'lexical': []
                        },
                        'sentiment': {
                            'compound': '0.5',
                            'neg': '0.05',
                            'neu': '0.9',
                            'pos': '0.05'
                        },
                        'googlekg': [],
                        'noun_phrase': ['nicky'],
                        'topic': {
                            'text':
                            'nicky',
                            'topicClass':
                            'Celebrities',
                            'confidence':
                            999.0,
                            'topicKeywords': [{
                                'keyword': 'nicky',
                                'confidence': 999.0
                            }]
                        },
                        'concept': [{
                            'noun': 'nicky',
                            'data': {
                                'western name': '0.4211',
                                'celebrity': '0.3684',
                                'person': '0.2105'
                            }
                        }],
                        'amazon_topic':
                        'Phatic',
                        'dependency_parsing': {
                            'head': [0],
                            'label': ['flat']
                        },
                        'pos_tagging': ['NOUN']
                    }],
                    'googlekg': [],
                    'sentiment2':
                    None,
                    'converttext':
                    'nicky',
                    'dialog_act': [{
                        'text': 'nicky',
                        'DA': 'statement',
                        'confidence': 0.83601874,
                        'DA2': 'NA',
                        'confidence2': 0.0
                    }],
                    'dependency_parsing': {
                        'dependency_parent': [[0]],
                        'dependency_label': [['flat']],
                        'pos_tagging': [['NOUN']]
                    },
                    'asrcorrection':
                    None,
                    'intent_classify': {
                        'sys': [],
                        'topic': [],
                        'lexical': []
                    },
                    'sentence_completion_text':
                    'that’s nicky',
                    'topic2': [{
                        'text':
                        'nicky',
                        'topicClass':
                        'Celebrities',
                        'confidence':
                        999.0,
                        'topicKeywords': [{
                            'keyword': 'nicky',
                            'confidence': 999.0
                        }]
                    }],
                    'coreference':
                    None,
                    'intent_classify2': [{
                        'sys': [],
                        'topic': [],
                        'lexical': []
                    }],
                    'intent':
                    'general',
                    'profanity_check':
                    None,
                    'topic_module':
                    '',
                    'topic_keywords': [{
                        'keyword': 'nicky',
                        'confidence': 999.0
                    }],
                    'knowledge': [],
                    'noun_phrase': ['nicky']
                },
                'central_elem': {
                    'senti': 'neu',
                    'regex': {
                        'sys': [],
                        'topic': [],
                        'lexical': []
                    },
                    'DA': 'statement',
                    'DA_score': '0.83601874',
                    'DA2': 'NA',
                    'DA2_score': '0.0',
                    'module': [],
                    'np': ['nicky'],
                    'text': 'nicky',
                    'backstory': {
                        'bot_propose_module':
                        '',
                        'confidence':
                        0.44334644079208374,
                        'followup':
                        '',
                        'module':
                        '',
                        'neg':
                        '',
                        'pos':
                        '',
                        'postfix':
                        '',
                        'question':
                        'meow',
                        'reason':
                        '',
                        'tag':
                        '',
                        'text':
                        '<say-as interpret-as="interjection"> meow, meow </say-as>! I love cats! Let\'s talk about animals! '
                    }
                },
                'returnnlp': [{
                    'text':
                    'nicky',
                    'dialog_act': ['statement', '0.83601874'],
                    'dialog_act2': ['NA', '0.0'],
                    'intent': {
                        'sys': [],
                        'topic': [],
                        'lexical': []
                    },
                    'sentiment': {
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    },
                    'googlekg': [],
                    'noun_phrase': ['nicky'],
                    'topic': {
                        'text':
                        'nicky',
                        'topicClass':
                        'Celebrities',
                        'confidence':
                        999.0,
                        'topicKeywords': [{
                            'keyword': 'nicky',
                            'confidence': 999.0
                        }]
                    },
                    'concept': [{
                        'noun': 'nicky',
                        'data': {
                            'western name': '0.4211',
                            'celebrity': '0.3684',
                            'person': '0.2105'
                        }
                    }],
                    'amazon_topic':
                    'Phatic',
                    'dependency_parsing': {
                        'head': [0],
                        'label': ['flat']
                    },
                    'pos_tagging': ['NOUN']
                }],
                'a_b_test':
                'B',
                'system_acknowledgement': {
                    'output_tag': '',
                    'ack': ''
                },
                'bot_ask_open_question':
                None,
                'user_id':
                'localtest-user-5d43a6c4-04ee-4c62-a747-13199ac2ed1b',
                'conversationId':
                None,
                'news_user': {
                    'propose_continue': 'STOP'
                },
                'previous_modules':
                ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                'prev_hash': {
                    'sys_env': 'local'
                },
                'user_profile': {
                    'visit': 1,
                    'dominant_turn_count': 1,
                    'total_turn_count': 2,
                    'username': '<no_name>'
                },
                'template_manager': {
                    'prev_hash': {
                        'PPsGsg': ['FlAjjQ'],
                        'ocgSRA': ['c4IZmw'],
                        'KxoF0w': [],
                        '/M0N7w': ['aeg3CQ']
                    },
                    'selector_history': [
                        'error::s_fragments/reprompt/thanks_for_sharing',
                        'error::s_fragments/reprompt/feedback',
                        'error::reprompt'
                    ]
                },
                'techsciencechat': {
                    'propose_continue': 'STOP'
                },
                'animalchat': {
                    'propose_continue': 'STOP'
                },
                'fashionchat': {
                    'propose_continue': 'STOP'
                },
                'timechat': {
                    'propose_continue': 'STOP'
                },
                'module_selection': {
                    'used_topic': ['LAUNCHGREETING']
                },
                'topic_to_replace': [],
                'cur_selected_modules':
                'LAUNCHGREETING',
                'foodchat': {
                    'propose_continue': 'STOP'
                },
                'sys_env':
                'local',
                'bookchat_user': {
                    'propose_continue': 'STOP'
                },
                'socialchat': {
                    'curr_state': 's_init',
                    'propose_continue': 'STOP'
                },
                'module_rank': {
                    'MUSICCHAT': 0.0,
                    'GAMECHAT': 0.0,
                    'SPORT': 0.0,
                    'FASHIONCHAT': 0.0,
                    'TECHSCIENCECHAT': 0.0,
                    'TRAVELCHAT': 0.0,
                    'NEWS': 0.0,
                    'BOOKCHAT': 0.0,
                    'ANIMALCHAT': 0.0,
                    'FOODCHAT': 0.0,
                    'MOVIECHAT': 0.0
                },
                'travelchat': {
                    'propose_continue': 'STOP'
                },
                'gamechat': {
                    'propose_continue': 'STOP'
                },
                'weatherchat': {
                    'propose_continue': 'STOP'
                },
                'usr_name':
                '<no_name>',
                'moviechat_user': {
                    'propose_continue': 'STOP'
                },
                'sportchat': {
                    'propose_continue': 'STOP'
                },
                'last_module':
                'LAUNCHGREETING',
                'tedtalkchat': {
                    'propose_continue': 'STOP'
                },
                'outdoorchat': {
                    'propose_continue': 'STOP'
                },
                'launchgreeting': {
                    'volatile': {
                        'is_name_common': False
                    },
                    'persistent_store': {
                        'is_first_module': True
                    },
                    'propose_continue': 'CONTINUE',
                    'states': {
                        'state': 'greet_new_user'
                    }
                },
                'comfortchat': {
                    'propose_continue': 'STOP'
                },
                'holidaychat': {
                    'propose_continue': 'STOP'
                },
                'dailylife': {
                    'propose_continue': 'STOP'
                },
                'musicchat': {
                    'propose_continue': 'STOP'
                },
                'propose_topic':
                None,
                'last_turn_propose_open_question':
                None,
                'social_open_question_proposing':
                None
            },
            'ua_ref': {
                'user_id':
                'localtest-user-5d43a6c4-04ee-4c62-a747-13199ac2ed1b',
                'map_attributes': {
                    'conversationId':
                    None,
                    'news_user': {
                        'propose_continue': 'STOP'
                    },
                    'previous_modules':
                    ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                    'prev_hash': {
                        'sys_env': 'local'
                    },
                    'user_profile': {
                        'visit': 1,
                        'dominant_turn_count': 1,
                        'total_turn_count': 2,
                        'username': '<no_name>'
                    },
                    'template_manager': {
                        'prev_hash': {
                            'PPsGsg': ['FlAjjQ'],
                            'ocgSRA': ['c4IZmw'],
                            'KxoF0w': [],
                            '/M0N7w': ['aeg3CQ']
                        },
                        'selector_history': [
                            'error::s_fragments/reprompt/thanks_for_sharing',
                            'error::s_fragments/reprompt/feedback',
                            'error::reprompt'
                        ]
                    },
                    'techsciencechat': {
                        'propose_continue': 'STOP'
                    },
                    'animalchat': {
                        'propose_continue': 'STOP'
                    },
                    'fashionchat': {
                        'propose_continue': 'STOP'
                    },
                    'timechat': {
                        'propose_continue': 'STOP'
                    },
                    'module_selection': {
                        'used_topic': ['LAUNCHGREETING']
                    },
                    'topic_to_replace': [],
                    'cur_selected_modules':
                    'LAUNCHGREETING',
                    'foodchat': {
                        'propose_continue': 'STOP'
                    },
                    'sys_env':
                    'local',
                    'bookchat_user': {
                        'propose_continue': 'STOP'
                    },
                    'socialchat': {
                        'curr_state': 's_init',
                        'propose_continue': 'STOP'
                    },
                    'module_rank': {
                        'MUSICCHAT': 0.0,
                        'GAMECHAT': 0.0,
                        'SPORT': 0.0,
                        'FASHIONCHAT': 0.0,
                        'TECHSCIENCECHAT': 0.0,
                        'TRAVELCHAT': 0.0,
                        'NEWS': 0.0,
                        'BOOKCHAT': 0.0,
                        'ANIMALCHAT': 0.0,
                        'FOODCHAT': 0.0,
                        'MOVIECHAT': 0.0
                    },
                    'travelchat': {
                        'propose_continue': 'STOP'
                    },
                    'gamechat': {
                        'propose_continue': 'STOP'
                    },
                    'weatherchat': {
                        'propose_continue': 'STOP'
                    },
                    'usr_name':
                    '<no_name>',
                    'moviechat_user': {
                        'propose_continue': 'STOP'
                    },
                    'sportchat': {
                        'propose_continue': 'STOP'
                    },
                    'last_module':
                    'LAUNCHGREETING',
                    'tedtalkchat': {
                        'propose_continue': 'STOP'
                    },
                    'outdoorchat': {
                        'propose_continue': 'STOP'
                    },
                    'launchgreeting': {
                        'volatile': {
                            'is_name_common': False
                        },
                        'persistent_store': {
                            'is_first_module': True
                        },
                        'propose_continue': 'CONTINUE',
                        'states': {
                            'state': 'greet_new_user'
                        }
                    },
                    'comfortchat': {
                        'propose_continue': 'STOP'
                    },
                    'holidaychat': {
                        'propose_continue': 'STOP'
                    },
                    'dailylife': {
                        'propose_continue': 'STOP'
                    },
                    'musicchat': {
                        'propose_continue': 'STOP'
                    },
                    'propose_topic':
                    None,
                    'last_turn_propose_open_question':
                    None,
                    'social_open_question_proposing':
                    None
                }
            }
        }
    }, dict(outputs=dict(result=[1, 0, 0, 0], name='nicky'))),
    ({
        'inputs': {
            'input_data': {
                'asr': [],
                'text':
                "i'm not great",
                'features': {
                    'asrcorrection':
                    None,
                    'intent':
                    'general',
                    'nounphrase2': {
                        'noun_phrase': [[]],
                        'local_noun_phrase': [[]]
                    },
                    'googlekg': [],
                    'sentence_completion_text':
                    "i 'm not great",
                    'dependency_parsing': {
                        'dependency_parent': [[0, 3, 0]],
                        'dependency_label': [['flat', 'advmod', 'amod']],
                        'pos_tagging': [['NOUN', 'PART', 'ADJ']]
                    },
                    'spacynp':
                    None,
                    'ner':
                    None,
                    'text':
                    "i'm not great",
                    'profanity_check':
                    None,
                    'converttext':
                    "i'm not great",
                    'amazon_topic':
                    'Phatic',
                    'sentiment2':
                    None,
                    'npknowledge': {
                        'knowledge': [],
                        'noun_phrase': [],
                        'local_noun_phrase': []
                    },
                    'coreference':
                    None,
                    'central_elem': {
                        'senti': 'pos',
                        'regex': {
                            'sys': [],
                            'topic': [],
                            'lexical': ['ans_neg', 'ans_pos']
                        },
                        'DA': 'statement',
                        'DA_score': '0.62224436',
                        'DA2': 'NA',
                        'DA2_score': '0.0',
                        'module': [],
                        'np': [],
                        'text': "i'm not great",
                        'backstory': {
                            'bot_propose_module': '',
                            'confidence': 0.7407527565956116,
                            'followup': '',
                            'module': '',
                            'neg': '',
                            'pos': '',
                            'postfix': '',
                            'question': 'I do not like you',
                            'reason': '',
                            'tag': '',
                            'text': "ouch. i'm sorry you feel like this."
                        }
                    },
                    'intent_classify': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['ans_neg', 'ans_pos']
                    },
                    'sentiment_allennlp': [{
                        'compound': '0.5',
                        'neg': '0.9',
                        'neu': '0.0',
                        'pos': '0.1'
                    }],
                    'sentiment':
                    None,
                    'topic2':
                    None,
                    'intent_classify2': [{
                        'sys': [],
                        'topic': [],
                        'lexical': ['ans_neg', 'ans_pos']
                    }],
                    'segmentation': {
                        'segmented_text': ["i'm not great"],
                        'segmented_text_raw': ["i'm not great"]
                    },
                    'dialog_act': [{
                        'text': "i'm not great",
                        'DA': 'statement',
                        'confidence': 0.62224436,
                        'DA2': 'NA',
                        'confidence2': 0.0
                    }],
                    'concept': [],
                    'returnnlp': [{
                        'text': "i'm not great",
                        'dialog_act': ['statement', '0.62224436'],
                        'dialog_act2': ['NA', '0.0'],
                        'intent': {
                            'sys': [],
                            'topic': [],
                            'lexical': ['ans_neg', 'ans_pos']
                        },
                        'sentiment': {
                            'compound': '0.5',
                            'neg': '0.9',
                            'neu': '0.0',
                            'pos': '0.1'
                        },
                        'googlekg': [],
                        'noun_phrase': [],
                        'topic': None,
                        'concept': [],
                        'amazon_topic': 'Phatic',
                        'dependency_parsing': {
                            'head': [0, 3, 0],
                            'label': ['flat', 'advmod', 'amod']
                        },
                        'pos_tagging': ['NOUN', 'PART', 'ADJ']
                    }],
                    'topic': [{
                        'text': "i'm not great",
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': []
                    }],
                    'topic_module':
                    '',
                    'topic_keywords': [],
                    'knowledge': [],
                    'noun_phrase': []
                },
                'central_elem': {
                    'senti': 'pos',
                    'regex': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['ans_neg', 'ans_pos']
                    },
                    'DA': 'statement',
                    'DA_score': '0.62224436',
                    'DA2': 'NA',
                    'DA2_score': '0.0',
                    'module': [],
                    'np': [],
                    'text': "i'm not great",
                    'backstory': {
                        'bot_propose_module': '',
                        'confidence': 0.7407527565956116,
                        'followup': '',
                        'module': '',
                        'neg': '',
                        'pos': '',
                        'postfix': '',
                        'question': 'I do not like you',
                        'reason': '',
                        'tag': '',
                        'text': "ouch. i'm sorry you feel like this."
                    }
                },
                'returnnlp': [{
                    'text': "i'm not great",
                    'dialog_act': ['statement', '0.62224436'],
                    'dialog_act2': ['NA', '0.0'],
                    'intent': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['ans_neg', 'ans_pos']
                    },
                    'sentiment': {
                        'compound': '0.5',
                        'neg': '0.9',
                        'neu': '0.0',
                        'pos': '0.1'
                    },
                    'googlekg': [],
                    'noun_phrase': [],
                    'topic': None,
                    'concept': [],
                    'amazon_topic': 'Phatic',
                    'dependency_parsing': {
                        'head': [0, 3, 0],
                        'label': ['flat', 'advmod', 'amod']
                    },
                    'pos_tagging': ['NOUN', 'PART', 'ADJ']
                }],
                'a_b_test':
                'A',
                'system_acknowledgement': {
                    'output_tag': '',
                    'ack': ''
                },
                'bot_ask_open_question':
                None,
                'user_id':
                'localtest-user-8bf479b6-c9ed-4dc3-8d8b-c97b26782dbe',
                'conversationId':
                None,
                'news_user': {
                    'propose_continue': 'STOP'
                },
                'previous_modules':
                ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                'prev_hash': {
                    'sys_env': 'local'
                },
                'user_profile': {
                    'visit': 1,
                    'dominant_turn_count': 1,
                    'total_turn_count': 2,
                    'username': '<no_name>'
                },
                'template_manager': {
                    'prev_hash': {
                        'PPsGsg': ['0Bsr2Q'],
                        'ocgSRA': ['c4IZmw'],
                        'KxoF0w': [],
                        '/M0N7w': ['+ec58w']
                    },
                    'selector_history': [
                        'error::s_fragments/reprompt/thanks_for_sharing',
                        'error::s_fragments/reprompt/feedback',
                        'error::reprompt'
                    ]
                },
                'techsciencechat': {
                    'propose_continue': 'STOP'
                },
                'animalchat': {
                    'propose_continue': 'STOP'
                },
                'fashionchat': {
                    'propose_continue': 'STOP'
                },
                'timechat': {
                    'propose_continue': 'STOP'
                },
                'module_selection': {
                    'used_topic': ['LAUNCHGREETING']
                },
                'topic_to_replace': [],
                'cur_selected_modules':
                'LAUNCHGREETING',
                'foodchat': {
                    'propose_continue': 'STOP'
                },
                'sys_env':
                'local',
                'bookchat_user': {
                    'propose_continue': 'STOP'
                },
                'socialchat': {
                    'curr_state': 's_init',
                    'propose_continue': 'STOP'
                },
                'module_rank': {
                    'MUSICCHAT': 0.0,
                    'GAMECHAT': 0.0,
                    'SPORT': 0.0,
                    'FASHIONCHAT': 0.0,
                    'TECHSCIENCECHAT': 0.0,
                    'TRAVELCHAT': 0.0,
                    'NEWS': 0.0,
                    'BOOKCHAT': 0.0,
                    'ANIMALCHAT': 0.0,
                    'FOODCHAT': 0.0,
                    'MOVIECHAT': 0.0
                },
                'travelchat': {
                    'propose_continue': 'STOP'
                },
                'gamechat': {
                    'propose_continue': 'STOP'
                },
                'weatherchat': {
                    'propose_continue': 'STOP'
                },
                'usr_name':
                '<no_name>',
                'moviechat_user': {
                    'propose_continue': 'STOP'
                },
                'sportchat': {
                    'propose_continue': 'STOP'
                },
                'last_module':
                'LAUNCHGREETING',
                'tedtalkchat': {
                    'propose_continue': 'STOP'
                },
                'outdoorchat': {
                    'propose_continue': 'STOP'
                },
                'launchgreeting': {
                    'volatile': {
                        'is_name_common': False
                    },
                    'persistent_store': {
                        'is_first_module': True
                    },
                    'propose_continue': 'CONTINUE',
                    'states': {
                        'state': 'greet_new_user'
                    }
                },
                'comfortchat': {
                    'propose_continue': 'STOP'
                },
                'holidaychat': {
                    'propose_continue': 'STOP'
                },
                'dailylife': {
                    'propose_continue': 'STOP'
                },
                'musicchat': {
                    'propose_continue': 'STOP'
                },
                'propose_topic':
                None,
                'last_turn_propose_open_question':
                None,
                'social_open_question_proposing':
                None
            },
            'ua_ref': {
                'user_id':
                'localtest-user-8bf479b6-c9ed-4dc3-8d8b-c97b26782dbe',
                'map_attributes': {
                    'conversationId':
                    None,
                    'news_user': {
                        'propose_continue': 'STOP'
                    },
                    'previous_modules':
                    ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                    'prev_hash': {
                        'sys_env': 'local'
                    },
                    'user_profile': {
                        'visit': 1,
                        'dominant_turn_count': 1,
                        'total_turn_count': 2,
                        'username': '<no_name>'
                    },
                    'template_manager': {
                        'prev_hash': {
                            'PPsGsg': ['0Bsr2Q'],
                            'ocgSRA': ['c4IZmw'],
                            'KxoF0w': [],
                            '/M0N7w': ['+ec58w']
                        },
                        'selector_history': [
                            'error::s_fragments/reprompt/thanks_for_sharing',
                            'error::s_fragments/reprompt/feedback',
                            'error::reprompt'
                        ]
                    },
                    'techsciencechat': {
                        'propose_continue': 'STOP'
                    },
                    'animalchat': {
                        'propose_continue': 'STOP'
                    },
                    'fashionchat': {
                        'propose_continue': 'STOP'
                    },
                    'timechat': {
                        'propose_continue': 'STOP'
                    },
                    'module_selection': {
                        'used_topic': ['LAUNCHGREETING']
                    },
                    'topic_to_replace': [],
                    'cur_selected_modules':
                    'LAUNCHGREETING',
                    'foodchat': {
                        'propose_continue': 'STOP'
                    },
                    'sys_env':
                    'local',
                    'bookchat_user': {
                        'propose_continue': 'STOP'
                    },
                    'socialchat': {
                        'curr_state': 's_init',
                        'propose_continue': 'STOP'
                    },
                    'module_rank': {
                        'MUSICCHAT': 0.0,
                        'GAMECHAT': 0.0,
                        'SPORT': 0.0,
                        'FASHIONCHAT': 0.0,
                        'TECHSCIENCECHAT': 0.0,
                        'TRAVELCHAT': 0.0,
                        'NEWS': 0.0,
                        'BOOKCHAT': 0.0,
                        'ANIMALCHAT': 0.0,
                        'FOODCHAT': 0.0,
                        'MOVIECHAT': 0.0
                    },
                    'travelchat': {
                        'propose_continue': 'STOP'
                    },
                    'gamechat': {
                        'propose_continue': 'STOP'
                    },
                    'weatherchat': {
                        'propose_continue': 'STOP'
                    },
                    'usr_name':
                    '<no_name>',
                    'moviechat_user': {
                        'propose_continue': 'STOP'
                    },
                    'sportchat': {
                        'propose_continue': 'STOP'
                    },
                    'last_module':
                    'LAUNCHGREETING',
                    'tedtalkchat': {
                        'propose_continue': 'STOP'
                    },
                    'outdoorchat': {
                        'propose_continue': 'STOP'
                    },
                    'launchgreeting': {
                        'volatile': {
                            'is_name_common': False
                        },
                        'persistent_store': {
                            'is_first_module': True
                        },
                        'propose_continue': 'CONTINUE',
                        'states': {
                            'state': 'greet_new_user'
                        }
                    },
                    'comfortchat': {
                        'propose_continue': 'STOP'
                    },
                    'holidaychat': {
                        'propose_continue': 'STOP'
                    },
                    'dailylife': {
                        'propose_continue': 'STOP'
                    },
                    'musicchat': {
                        'propose_continue': 'STOP'
                    },
                    'propose_topic':
                    None,
                    'last_turn_propose_open_question':
                    None,
                    'social_open_question_proposing':
                    None
                }
            }
        }
    }, dict(outputs=dict(result=[0, 0, 1, 0]))),
    ({
        'inputs': {
            'input_data': {
                'asr': [],
                'text':
                'yeah',
                'features': {
                    'segmentation': {
                        'segmented_text': ['yeah'],
                        'segmented_text_raw': ['yeah']
                    },
                    'sentiment':
                    None,
                    'amazon_topic':
                    'Phatic',
                    'dependency_parsing': {
                        'dependency_parent': [[0]],
                        'dependency_label': [['discourse']],
                        'pos_tagging': [['INTJ']]
                    },
                    'text':
                    'yeah',
                    'converttext':
                    'yeah',
                    'topic2': [{
                        'text': 'yeah',
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': []
                    }],
                    'sentence_completion_text':
                    'yeah',
                    'spacynp':
                    None,
                    'concept': [],
                    'ner':
                    None,
                    'dialog_act': [{
                        'text': 'yeah',
                        'DA': 'pos_answer',
                        'confidence': 0.94268626,
                        'DA2': 'NA',
                        'confidence2': 0.0
                    }],
                    'topic': [{
                        'text': 'yeah',
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': []
                    }],
                    'googlekg': [],
                    'intent_classify2': [{
                        'sys': [],
                        'topic': [],
                        'lexical': ['ans_positive', 'ans_pos']
                    }],
                    'nounphrase2': {
                        'noun_phrase': [[]],
                        'local_noun_phrase': [[]]
                    },
                    'coreference':
                    None,
                    'intent_classify': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['ans_positive', 'ans_pos']
                    },
                    'sentiment_allennlp': [{
                        'compound': '0.5',
                        'neg': '0.1',
                        'neu': '0.0',
                        'pos': '0.9'
                    }],
                    'asrcorrection':
                    None,
                    'returnnlp': [{
                        'text': 'yeah',
                        'dialog_act': ['pos_answer', '0.94268626'],
                        'dialog_act2': ['NA', '0.0'],
                        'intent': {
                            'sys': [],
                            'topic': [],
                            'lexical': ['ans_positive', 'ans_pos']
                        },
                        'sentiment': {
                            'compound': '0.5',
                            'neg': '0.1',
                            'neu': '0.0',
                            'pos': '0.9'
                        },
                        'googlekg': [],
                        'noun_phrase': [],
                        'topic': {
                            'text': 'yeah',
                            'topicClass': 'Phatic',
                            'confidence': 999.0,
                            'topicKeywords': []
                        },
                        'concept': [],
                        'amazon_topic': 'Phatic',
                        'dependency_parsing': {
                            'head': [0],
                            'label': ['discourse']
                        },
                        'pos_tagging': ['INTJ']
                    }],
                    'intent':
                    'general',
                    'central_elem': {
                        'senti': 'pos',
                        'regex': {
                            'sys': [],
                            'topic': [],
                            'lexical': ['ans_positive', 'ans_pos']
                        },
                        'DA': 'pos_answer',
                        'DA_score': '0.94268626',
                        'DA2': 'NA',
                        'DA2_score': '0.0',
                        'module': [],
                        'np': [],
                        'text': 'yeah',
                        'backstory': {
                            'bot_propose_module': '',
                            'confidence': 0.5568858981132507,
                            'followup': '',
                            'module': '',
                            'neg': '',
                            'pos': '',
                            'postfix': '',
                            'question': 'what can i say',
                            'reason': '',
                            'tag': '',
                            'text': 'You can say whatever you want! '
                        }
                    },
                    'sentiment2':
                    None,
                    'profanity_check':
                    None,
                    'npknowledge': {
                        'knowledge': [],
                        'noun_phrase': [],
                        'local_noun_phrase': []
                    },
                    'topic_module':
                    '',
                    'topic_keywords': [],
                    'knowledge': [],
                    'noun_phrase': []
                },
                'central_elem': {
                    'senti': 'pos',
                    'regex': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['ans_positive', 'ans_pos']
                    },
                    'DA': 'pos_answer',
                    'DA_score': '0.94268626',
                    'DA2': 'NA',
                    'DA2_score': '0.0',
                    'module': [],
                    'np': [],
                    'text': 'yeah',
                    'backstory': {
                        'bot_propose_module': '',
                        'confidence': 0.5568858981132507,
                        'followup': '',
                        'module': '',
                        'neg': '',
                        'pos': '',
                        'postfix': '',
                        'question': 'what can i say',
                        'reason': '',
                        'tag': '',
                        'text': 'You can say whatever you want! '
                    }
                },
                'returnnlp': [{
                    'text': 'yeah',
                    'dialog_act': ['pos_answer', '0.94268626'],
                    'dialog_act2': ['NA', '0.0'],
                    'intent': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['ans_positive', 'ans_pos']
                    },
                    'sentiment': {
                        'compound': '0.5',
                        'neg': '0.1',
                        'neu': '0.0',
                        'pos': '0.9'
                    },
                    'googlekg': [],
                    'noun_phrase': [],
                    'topic': {
                        'text': 'yeah',
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': []
                    },
                    'concept': [],
                    'amazon_topic': 'Phatic',
                    'dependency_parsing': {
                        'head': [0],
                        'label': ['discourse']
                    },
                    'pos_tagging': ['INTJ']
                }],
                'a_b_test':
                'A',
                'system_acknowledgement': {
                    'output_tag': '',
                    'ack': ''
                },
                'bot_ask_open_question':
                None,
                'user_id':
                'localtest-user-6615ff8f-c40d-4c04-a060-b5d1ce14247f',
                'conversationId':
                None,
                'news_user': {
                    'propose_continue': 'STOP'
                },
                'previous_modules':
                ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                'prev_hash': {
                    'sys_env': 'local'
                },
                'user_profile': {
                    'visit': 1,
                    'dominant_turn_count': 0,
                    'total_turn_count': 2,
                    'username': '<no_name>'
                },
                'template_manager': {
                    'prev_hash': {
                        'PPsGsg': ['FlAjjQ'],
                        'ocgSRA': ['VS0VHQ'],
                        'KxoF0w': [],
                        '/M0N7w': ['+ec58w']
                    },
                    'selector_history': [
                        'error::s_fragments/reprompt/thanks_for_sharing',
                        'error::s_fragments/reprompt/feedback',
                        'error::reprompt'
                    ]
                },
                'techsciencechat': {
                    'propose_continue': 'STOP'
                },
                'animalchat': {
                    'propose_continue': 'STOP'
                },
                'fashionchat': {
                    'propose_continue': 'STOP'
                },
                'timechat': {
                    'propose_continue': 'STOP'
                },
                'module_selection': {
                    'used_topic': ['LAUNCHGREETING']
                },
                'topic_to_replace': [],
                'cur_selected_modules':
                'LAUNCHGREETING',
                'foodchat': {
                    'propose_continue': 'STOP'
                },
                'sys_env':
                'local',
                'bookchat_user': {
                    'propose_continue': 'STOP'
                },
                'socialchat': {
                    'curr_state': 's_init',
                    'propose_continue': 'STOP'
                },
                'module_rank': {
                    'MUSICCHAT': 0.0,
                    'GAMECHAT': 0.0,
                    'SPORT': 0.0,
                    'FASHIONCHAT': 0.0,
                    'TECHSCIENCECHAT': 0.0,
                    'TRAVELCHAT': 0.0,
                    'NEWS': 0.0,
                    'BOOKCHAT': 0.0,
                    'ANIMALCHAT': 0.0,
                    'FOODCHAT': 0.0,
                    'MOVIECHAT': 0.0
                },
                'travelchat': {
                    'propose_continue': 'STOP'
                },
                'gamechat': {
                    'propose_continue': 'STOP'
                },
                'weatherchat': {
                    'propose_continue': 'STOP'
                },
                'usr_name':
                '<no_name>',
                'moviechat_user': {
                    'propose_continue': 'STOP'
                },
                'sportchat': {
                    'propose_continue': 'STOP'
                },
                'last_module':
                'LAUNCHGREETING',
                'tedtalkchat': {
                    'propose_continue': 'STOP'
                },
                'outdoorchat': {
                    'propose_continue': 'STOP'
                },
                'launchgreeting': {
                    'volatile': {
                        'is_name_common': False
                    },
                    'persistent_store': {
                        'is_first_module': True
                    },
                    'propose_continue': 'CONTINUE',
                    'states': {
                        'state': 'greet_new_user'
                    }
                },
                'comfortchat': {
                    'propose_continue': 'STOP'
                },
                'holidaychat': {
                    'propose_continue': 'STOP'
                },
                'dailylife': {
                    'propose_continue': 'STOP'
                },
                'musicchat': {
                    'propose_continue': 'STOP'
                },
                'propose_topic':
                None,
                'last_turn_propose_open_question':
                None,
                'social_open_question_proposing':
                None
            },
            'ua_ref': {
                'user_id':
                'localtest-user-6615ff8f-c40d-4c04-a060-b5d1ce14247f',
                'map_attributes': {
                    'conversationId':
                    None,
                    'news_user': {
                        'propose_continue': 'STOP'
                    },
                    'previous_modules':
                    ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                    'prev_hash': {
                        'sys_env': 'local'
                    },
                    'user_profile': {
                        'visit': 1,
                        'dominant_turn_count': 0,
                        'total_turn_count': 2,
                        'username': '<no_name>'
                    },
                    'template_manager': {
                        'prev_hash': {
                            'PPsGsg': ['FlAjjQ'],
                            'ocgSRA': ['VS0VHQ'],
                            'KxoF0w': [],
                            '/M0N7w': ['+ec58w']
                        },
                        'selector_history': [
                            'error::s_fragments/reprompt/thanks_for_sharing',
                            'error::s_fragments/reprompt/feedback',
                            'error::reprompt'
                        ]
                    },
                    'techsciencechat': {
                        'propose_continue': 'STOP'
                    },
                    'animalchat': {
                        'propose_continue': 'STOP'
                    },
                    'fashionchat': {
                        'propose_continue': 'STOP'
                    },
                    'timechat': {
                        'propose_continue': 'STOP'
                    },
                    'module_selection': {
                        'used_topic': ['LAUNCHGREETING']
                    },
                    'topic_to_replace': [],
                    'cur_selected_modules':
                    'LAUNCHGREETING',
                    'foodchat': {
                        'propose_continue': 'STOP'
                    },
                    'sys_env':
                    'local',
                    'bookchat_user': {
                        'propose_continue': 'STOP'
                    },
                    'socialchat': {
                        'curr_state': 's_init',
                        'propose_continue': 'STOP'
                    },
                    'module_rank': {
                        'MUSICCHAT': 0.0,
                        'GAMECHAT': 0.0,
                        'SPORT': 0.0,
                        'FASHIONCHAT': 0.0,
                        'TECHSCIENCECHAT': 0.0,
                        'TRAVELCHAT': 0.0,
                        'NEWS': 0.0,
                        'BOOKCHAT': 0.0,
                        'ANIMALCHAT': 0.0,
                        'FOODCHAT': 0.0,
                        'MOVIECHAT': 0.0
                    },
                    'travelchat': {
                        'propose_continue': 'STOP'
                    },
                    'gamechat': {
                        'propose_continue': 'STOP'
                    },
                    'weatherchat': {
                        'propose_continue': 'STOP'
                    },
                    'usr_name':
                    '<no_name>',
                    'moviechat_user': {
                        'propose_continue': 'STOP'
                    },
                    'sportchat': {
                        'propose_continue': 'STOP'
                    },
                    'last_module':
                    'LAUNCHGREETING',
                    'tedtalkchat': {
                        'propose_continue': 'STOP'
                    },
                    'outdoorchat': {
                        'propose_continue': 'STOP'
                    },
                    'launchgreeting': {
                        'volatile': {
                            'is_name_common': False
                        },
                        'persistent_store': {
                            'is_first_module': True
                        },
                        'propose_continue': 'CONTINUE',
                        'states': {
                            'state': 'greet_new_user'
                        }
                    },
                    'comfortchat': {
                        'propose_continue': 'STOP'
                    },
                    'holidaychat': {
                        'propose_continue': 'STOP'
                    },
                    'dailylife': {
                        'propose_continue': 'STOP'
                    },
                    'musicchat': {
                        'propose_continue': 'STOP'
                    },
                    'propose_topic':
                    None,
                    'last_turn_propose_open_question':
                    None,
                    'social_open_question_proposing':
                    None
                }
            }
        }
    }, dict(outputs=dict(result=[0, 0, 1, 0]))),
    ({
        'inputs': {
            'input_data': {
                'asr': [],
                'text':
                'yes',
                'features': {
                    'dependency_parsing': {
                        'dependency_parent': [[0]],
                        'dependency_label': [['discourse']],
                        'pos_tagging': [['INTJ']]
                    },
                    'converttext':
                    'yes',
                    'intent_classify': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['ans_positive', 'ans_pos']
                    },
                    'intent_classify2': [{
                        'sys': [],
                        'topic': [],
                        'lexical': ['ans_positive', 'ans_pos']
                    }],
                    'text':
                    'yes',
                    'topic2': [{
                        'text': 'yes',
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': []
                    }],
                    'npknowledge': {
                        'knowledge': [],
                        'noun_phrase': [],
                        'local_noun_phrase': []
                    },
                    'returnnlp': [{
                        'text': 'yes',
                        'dialog_act': ['pos_answer', '0.9791047'],
                        'dialog_act2': ['NA', '0.0'],
                        'intent': {
                            'sys': [],
                            'topic': [],
                            'lexical': ['ans_positive', 'ans_pos']
                        },
                        'sentiment': {
                            'compound': '0.5',
                            'neg': '0.1',
                            'neu': '0.0',
                            'pos': '0.9'
                        },
                        'googlekg': [],
                        'noun_phrase': [],
                        'topic': {
                            'text': 'yes',
                            'topicClass': 'Phatic',
                            'confidence': 999.0,
                            'topicKeywords': []
                        },
                        'concept': [],
                        'amazon_topic': 'Phatic',
                        'dependency_parsing': {
                            'head': [0],
                            'label': ['discourse']
                        },
                        'pos_tagging': ['INTJ']
                    }],
                    'googlekg': [],
                    'dialog_act': [{
                        'text': 'yes',
                        'DA': 'pos_answer',
                        'confidence': 0.9791047,
                        'DA2': 'NA',
                        'confidence2': 0.0
                    }],
                    'coreference':
                    None,
                    'concept': [],
                    'sentence_completion_text':
                    'yes , i would like to tell you my name',
                    'sentiment_allennlp': [{
                        'compound': '0.5',
                        'neg': '0.1',
                        'neu': '0.0',
                        'pos': '0.9'
                    }],
                    'sentiment2':
                    None,
                    'central_elem': {
                        'senti': 'pos',
                        'regex': {
                            'sys': [],
                            'topic': [],
                            'lexical': ['ans_positive', 'ans_pos']
                        },
                        'DA': 'pos_answer',
                        'DA_score': '0.9791047',
                        'DA2': 'NA',
                        'DA2_score': '0.0',
                        'module': [],
                        'np': [],
                        'text': 'yes',
                        'backstory': {
                            'bot_propose_module':
                            '',
                            'confidence':
                            0.4728533625602722,
                            'followup':
                            '',
                            'module':
                            '',
                            'neg':
                            '',
                            'pos':
                            '',
                            'postfix':
                            '',
                            'question':
                            'why do you ask me',
                            'reason':
                            '',
                            'tag':
                            '',
                            'text':
                            'Oh, I was just trying to get to know more about you. '
                        }
                    },
                    'topic': [{
                        'text': 'yes',
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': []
                    }],
                    'segmentation': {
                        'segmented_text': ['yes'],
                        'segmented_text_raw': ['yes']
                    },
                    'sentiment':
                    None,
                    'profanity_check':
                    None,
                    'nounphrase2': {
                        'noun_phrase': [[]],
                        'local_noun_phrase': [[]]
                    },
                    'spacynp':
                    None,
                    'intent':
                    'general',
                    'ner':
                    None,
                    'amazon_topic':
                    'Phatic',
                    'asrcorrection':
                    None,
                    'topic_module':
                    '',
                    'topic_keywords': [],
                    'knowledge': [],
                    'noun_phrase': []
                },
                'central_elem': {
                    'senti': 'pos',
                    'regex': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['ans_positive', 'ans_pos']
                    },
                    'DA': 'pos_answer',
                    'DA_score': '0.9791047',
                    'DA2': 'NA',
                    'DA2_score': '0.0',
                    'module': [],
                    'np': [],
                    'text': 'yes',
                    'backstory': {
                        'bot_propose_module':
                        '',
                        'confidence':
                        0.4728533625602722,
                        'followup':
                        '',
                        'module':
                        '',
                        'neg':
                        '',
                        'pos':
                        '',
                        'postfix':
                        '',
                        'question':
                        'why do you ask me',
                        'reason':
                        '',
                        'tag':
                        '',
                        'text':
                        'Oh, I was just trying to get to know more about you. '
                    }
                },
                'returnnlp': [{
                    'text': 'yes',
                    'dialog_act': ['pos_answer', '0.9791047'],
                    'dialog_act2': ['NA', '0.0'],
                    'intent': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['ans_positive', 'ans_pos']
                    },
                    'sentiment': {
                        'compound': '0.5',
                        'neg': '0.1',
                        'neu': '0.0',
                        'pos': '0.9'
                    },
                    'googlekg': [],
                    'noun_phrase': [],
                    'topic': {
                        'text': 'yes',
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': []
                    },
                    'concept': [],
                    'amazon_topic': 'Phatic',
                    'dependency_parsing': {
                        'head': [0],
                        'label': ['discourse']
                    },
                    'pos_tagging': ['INTJ']
                }],
                'a_b_test':
                'A',
                'system_acknowledgement': {
                    'output_tag': '',
                    'ack': ''
                },
                'bot_ask_open_question':
                None,
                'user_id':
                'localtest-user-9ccc667a-9ffa-4d72-8ac9-2c903e6ceb4b',
                'conversationId':
                None,
                'news_user': {
                    'propose_continue': 'STOP'
                },
                'previous_modules':
                ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                'prev_hash': {
                    'sys_env': 'local'
                },
                'user_profile': {
                    'visit': 1,
                    'dominant_turn_count': 0,
                    'total_turn_count': 2,
                    'username': '<no_name>'
                },
                'template_manager': {
                    'prev_hash': {
                        'PPsGsg': ['FlAjjQ'],
                        'ocgSRA': ['VS0VHQ'],
                        'KxoF0w': [],
                        '/M0N7w': ['t3gtIQ']
                    },
                    'selector_history': [
                        'error::s_fragments/reprompt/thanks_for_sharing',
                        'error::s_fragments/reprompt/feedback',
                        'error::reprompt'
                    ]
                },
                'techsciencechat': {
                    'propose_continue': 'STOP'
                },
                'animalchat': {
                    'propose_continue': 'STOP'
                },
                'fashionchat': {
                    'propose_continue': 'STOP'
                },
                'timechat': {
                    'propose_continue': 'STOP'
                },
                'module_selection': {
                    'used_topic': ['LAUNCHGREETING']
                },
                'topic_to_replace': [],
                'cur_selected_modules':
                'LAUNCHGREETING',
                'foodchat': {
                    'propose_continue': 'STOP'
                },
                'sys_env':
                'local',
                'bookchat_user': {
                    'propose_continue': 'STOP'
                },
                'socialchat': {
                    'curr_state': 's_init',
                    'propose_continue': 'STOP'
                },
                'module_rank': {
                    'MUSICCHAT': 0.0,
                    'GAMECHAT': 0.0,
                    'SPORT': 0.0,
                    'FASHIONCHAT': 0.0,
                    'TECHSCIENCECHAT': 0.0,
                    'TRAVELCHAT': 0.0,
                    'NEWS': 0.0,
                    'BOOKCHAT': 0.0,
                    'ANIMALCHAT': 0.0,
                    'FOODCHAT': 0.0,
                    'MOVIECHAT': 0.0
                },
                'travelchat': {
                    'propose_continue': 'STOP'
                },
                'gamechat': {
                    'propose_continue': 'STOP'
                },
                'weatherchat': {
                    'propose_continue': 'STOP'
                },
                'usr_name':
                '<no_name>',
                'moviechat_user': {
                    'propose_continue': 'STOP'
                },
                'sportchat': {
                    'propose_continue': 'STOP'
                },
                'last_module':
                'LAUNCHGREETING',
                'tedtalkchat': {
                    'propose_continue': 'STOP'
                },
                'outdoorchat': {
                    'propose_continue': 'STOP'
                },
                'launchgreeting': {
                    'volatile': {
                        'is_name_common': False
                    },
                    'persistent_store': {
                        'is_first_module': True
                    },
                    'propose_continue': 'CONTINUE',
                    'states': {
                        'state': 'greet_new_user'
                    }
                },
                'comfortchat': {
                    'propose_continue': 'STOP'
                },
                'holidaychat': {
                    'propose_continue': 'STOP'
                },
                'dailylife': {
                    'propose_continue': 'STOP'
                },
                'musicchat': {
                    'propose_continue': 'STOP'
                },
                'propose_topic':
                None,
                'last_turn_propose_open_question':
                None,
                'social_open_question_proposing':
                None
            },
            'ua_ref': {
                'user_id':
                'localtest-user-9ccc667a-9ffa-4d72-8ac9-2c903e6ceb4b',
                'map_attributes': {
                    'conversationId':
                    None,
                    'news_user': {
                        'propose_continue': 'STOP'
                    },
                    'previous_modules':
                    ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                    'prev_hash': {
                        'sys_env': 'local'
                    },
                    'user_profile': {
                        'visit': 1,
                        'dominant_turn_count': 0,
                        'total_turn_count': 2,
                        'username': '<no_name>'
                    },
                    'template_manager': {
                        'prev_hash': {
                            'PPsGsg': ['FlAjjQ'],
                            'ocgSRA': ['VS0VHQ'],
                            'KxoF0w': [],
                            '/M0N7w': ['t3gtIQ']
                        },
                        'selector_history': [
                            'error::s_fragments/reprompt/thanks_for_sharing',
                            'error::s_fragments/reprompt/feedback',
                            'error::reprompt'
                        ]
                    },
                    'techsciencechat': {
                        'propose_continue': 'STOP'
                    },
                    'animalchat': {
                        'propose_continue': 'STOP'
                    },
                    'fashionchat': {
                        'propose_continue': 'STOP'
                    },
                    'timechat': {
                        'propose_continue': 'STOP'
                    },
                    'module_selection': {
                        'used_topic': ['LAUNCHGREETING']
                    },
                    'topic_to_replace': [],
                    'cur_selected_modules':
                    'LAUNCHGREETING',
                    'foodchat': {
                        'propose_continue': 'STOP'
                    },
                    'sys_env':
                    'local',
                    'bookchat_user': {
                        'propose_continue': 'STOP'
                    },
                    'socialchat': {
                        'curr_state': 's_init',
                        'propose_continue': 'STOP'
                    },
                    'module_rank': {
                        'MUSICCHAT': 0.0,
                        'GAMECHAT': 0.0,
                        'SPORT': 0.0,
                        'FASHIONCHAT': 0.0,
                        'TECHSCIENCECHAT': 0.0,
                        'TRAVELCHAT': 0.0,
                        'NEWS': 0.0,
                        'BOOKCHAT': 0.0,
                        'ANIMALCHAT': 0.0,
                        'FOODCHAT': 0.0,
                        'MOVIECHAT': 0.0
                    },
                    'travelchat': {
                        'propose_continue': 'STOP'
                    },
                    'gamechat': {
                        'propose_continue': 'STOP'
                    },
                    'weatherchat': {
                        'propose_continue': 'STOP'
                    },
                    'usr_name':
                    '<no_name>',
                    'moviechat_user': {
                        'propose_continue': 'STOP'
                    },
                    'sportchat': {
                        'propose_continue': 'STOP'
                    },
                    'last_module':
                    'LAUNCHGREETING',
                    'tedtalkchat': {
                        'propose_continue': 'STOP'
                    },
                    'outdoorchat': {
                        'propose_continue': 'STOP'
                    },
                    'launchgreeting': {
                        'volatile': {
                            'is_name_common': False
                        },
                        'persistent_store': {
                            'is_first_module': True
                        },
                        'propose_continue': 'CONTINUE',
                        'states': {
                            'state': 'greet_new_user'
                        }
                    },
                    'comfortchat': {
                        'propose_continue': 'STOP'
                    },
                    'holidaychat': {
                        'propose_continue': 'STOP'
                    },
                    'dailylife': {
                        'propose_continue': 'STOP'
                    },
                    'musicchat': {
                        'propose_continue': 'STOP'
                    },
                    'propose_topic':
                    None,
                    'last_turn_propose_open_question':
                    None,
                    'social_open_question_proposing':
                    None
                }
            }
        }
    }, dict(outputs=dict(result=[0, 0, 1, 0]))),
    ({
        'inputs': {
            'input_data': {
                'asr': [],
                'text':
                'no',
                'features': {
                    'npknowledge': {
                        'knowledge': [],
                        'noun_phrase': [],
                        'local_noun_phrase': []
                    },
                    'returnnlp': [{
                        'text': 'no',
                        'dialog_act': ['neg_answer', '0.9891159'],
                        'dialog_act2': ['NA', '0.0'],
                        'intent': {
                            'sys': [],
                            'topic': [],
                            'lexical': ['ans_negative', 'ans_neg']
                        },
                        'sentiment': {
                            'compound': '0.5',
                            'neg': '0.9',
                            'neu': '0.0',
                            'pos': '0.1'
                        },
                        'googlekg': [],
                        'noun_phrase': [],
                        'topic': {
                            'text': 'no',
                            'topicClass': 'Phatic',
                            'confidence': 999.0,
                            'topicKeywords': []
                        },
                        'concept': [],
                        'amazon_topic': None,
                        'dependency_parsing': {
                            'head': [0],
                            'label': ['det']
                        },
                        'pos_tagging': ['INTJ']
                    }],
                    'nounphrase2': {
                        'noun_phrase': [[]],
                        'local_noun_phrase': [[]]
                    },
                    'intent_classify': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['ans_negative', 'ans_neg']
                    },
                    'intent_classify2': [{
                        'sys': [],
                        'topic': [],
                        'lexical': ['ans_negative', 'ans_neg']
                    }],
                    'intent':
                    'general',
                    'concept': [],
                    'sentiment':
                    None,
                    'coreference':
                    None,
                    'text':
                    'no',
                    'ner':
                    None,
                    'dialog_act': [{
                        'text': 'no',
                        'DA': 'neg_answer',
                        'confidence': 0.9891159,
                        'DA2': 'NA',
                        'confidence2': 0.0
                    }],
                    'profanity_check':
                    None,
                    'topic': [{
                        'text': 'no',
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': []
                    }],
                    'asrcorrection':
                    None,
                    'spacynp':
                    None,
                    'amazon_topic':
                    None,
                    'sentiment2':
                    None,
                    'sentence_completion_text':
                    'no',
                    'dependency_parsing': {
                        'dependency_parent': [[0]],
                        'dependency_label': [['det']],
                        'pos_tagging': [['INTJ']]
                    },
                    'googlekg': [],
                    'central_elem': {
                        'senti': 'neg',
                        'regex': {
                            'sys': [],
                            'topic': [],
                            'lexical': ['ans_negative', 'ans_neg']
                        },
                        'DA': 'neg_answer',
                        'DA_score': '0.9891159',
                        'DA2': 'NA',
                        'DA2_score': '0.0',
                        'module': [],
                        'np': [],
                        'text': 'no',
                        'backstory': {
                            'bot_propose_module':
                            '',
                            'confidence':
                            0.5005818009376526,
                            'followup':
                            '',
                            'module':
                            '',
                            'neg':
                            '',
                            'pos':
                            '',
                            'postfix':
                            '',
                            'question':
                            'why do you ask me',
                            'reason':
                            '',
                            'tag':
                            '',
                            'text':
                            'Oh, I was just trying to get to know more about you. '
                        }
                    },
                    'topic2': [{
                        'text': 'no',
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': []
                    }],
                    'sentiment_allennlp': [{
                        'compound': '0.5',
                        'neg': '0.9',
                        'neu': '0.0',
                        'pos': '0.1'
                    }],
                    'segmentation': {
                        'segmented_text': ['no'],
                        'segmented_text_raw': ['no']
                    },
                    'converttext':
                    'no',
                    'topic_module':
                    '',
                    'topic_keywords': [],
                    'knowledge': [],
                    'noun_phrase': []
                },
                'central_elem': {
                    'senti': 'neg',
                    'regex': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['ans_negative', 'ans_neg']
                    },
                    'DA': 'neg_answer',
                    'DA_score': '0.9891159',
                    'DA2': 'NA',
                    'DA2_score': '0.0',
                    'module': [],
                    'np': [],
                    'text': 'no',
                    'backstory': {
                        'bot_propose_module':
                        '',
                        'confidence':
                        0.5005818009376526,
                        'followup':
                        '',
                        'module':
                        '',
                        'neg':
                        '',
                        'pos':
                        '',
                        'postfix':
                        '',
                        'question':
                        'why do you ask me',
                        'reason':
                        '',
                        'tag':
                        '',
                        'text':
                        'Oh, I was just trying to get to know more about you. '
                    }
                },
                'returnnlp': [{
                    'text': 'no',
                    'dialog_act': ['neg_answer', '0.9891159'],
                    'dialog_act2': ['NA', '0.0'],
                    'intent': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['ans_negative', 'ans_neg']
                    },
                    'sentiment': {
                        'compound': '0.5',
                        'neg': '0.9',
                        'neu': '0.0',
                        'pos': '0.1'
                    },
                    'googlekg': [],
                    'noun_phrase': [],
                    'topic': {
                        'text': 'no',
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': []
                    },
                    'concept': [],
                    'amazon_topic': None,
                    'dependency_parsing': {
                        'head': [0],
                        'label': ['det']
                    },
                    'pos_tagging': ['INTJ']
                }],
                'a_b_test':
                'B',
                'system_acknowledgement': {
                    'output_tag': '',
                    'ack': ''
                },
                'bot_ask_open_question':
                None,
                'user_id':
                'localtest-user-99feaa19-c7d4-4266-9db7-69825f0f2026',
                'conversationId':
                None,
                'news_user': {
                    'propose_continue': 'STOP'
                },
                'previous_modules':
                ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                'prev_hash': {
                    'sys_env': 'local'
                },
                'user_profile': {
                    'visit': 1,
                    'dominant_turn_count': 0,
                    'total_turn_count': 2,
                    'username': '<no_name>'
                },
                'template_manager': {
                    'prev_hash': {
                        'PPsGsg': ['0Bsr2Q'],
                        'ocgSRA': ['VS0VHQ'],
                        'KxoF0w': [],
                        '/M0N7w': ['5SE7MQ']
                    },
                    'selector_history': [
                        'error::s_fragments/reprompt/thanks_for_sharing',
                        'error::s_fragments/reprompt/feedback',
                        'error::reprompt'
                    ]
                },
                'techsciencechat': {
                    'propose_continue': 'STOP'
                },
                'animalchat': {
                    'propose_continue': 'STOP'
                },
                'fashionchat': {
                    'propose_continue': 'STOP'
                },
                'timechat': {
                    'propose_continue': 'STOP'
                },
                'module_selection': {
                    'used_topic': ['LAUNCHGREETING']
                },
                'topic_to_replace': [],
                'cur_selected_modules':
                'LAUNCHGREETING',
                'foodchat': {
                    'propose_continue': 'STOP'
                },
                'sys_env':
                'local',
                'bookchat_user': {
                    'propose_continue': 'STOP'
                },
                'socialchat': {
                    'curr_state': 's_init',
                    'propose_continue': 'STOP'
                },
                'module_rank': {
                    'MUSICCHAT': 0.0,
                    'GAMECHAT': 0.0,
                    'SPORT': 0.0,
                    'FASHIONCHAT': 0.0,
                    'TECHSCIENCECHAT': 0.0,
                    'TRAVELCHAT': 0.0,
                    'NEWS': 0.0,
                    'BOOKCHAT': 0.0,
                    'ANIMALCHAT': 0.0,
                    'FOODCHAT': 0.0,
                    'MOVIECHAT': 0.0
                },
                'travelchat': {
                    'propose_continue': 'STOP'
                },
                'gamechat': {
                    'propose_continue': 'STOP'
                },
                'weatherchat': {
                    'propose_continue': 'STOP'
                },
                'usr_name':
                '<no_name>',
                'moviechat_user': {
                    'propose_continue': 'STOP'
                },
                'sportchat': {
                    'propose_continue': 'STOP'
                },
                'last_module':
                'LAUNCHGREETING',
                'tedtalkchat': {
                    'propose_continue': 'STOP'
                },
                'outdoorchat': {
                    'propose_continue': 'STOP'
                },
                'launchgreeting': {
                    'volatile': {
                        'is_name_common': False
                    },
                    'persistent_store': {
                        'is_first_module': True
                    },
                    'propose_continue': 'CONTINUE',
                    'states': {
                        'state': 'greet_new_user'
                    }
                },
                'comfortchat': {
                    'propose_continue': 'STOP'
                },
                'holidaychat': {
                    'propose_continue': 'STOP'
                },
                'dailylife': {
                    'propose_continue': 'STOP'
                },
                'musicchat': {
                    'propose_continue': 'STOP'
                },
                'propose_topic':
                None,
                'last_turn_propose_open_question':
                None,
                'social_open_question_proposing':
                None
            },
            'ua_ref': {
                'user_id':
                'localtest-user-99feaa19-c7d4-4266-9db7-69825f0f2026',
                'map_attributes': {
                    'conversationId':
                    None,
                    'news_user': {
                        'propose_continue': 'STOP'
                    },
                    'previous_modules':
                    ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                    'prev_hash': {
                        'sys_env': 'local'
                    },
                    'user_profile': {
                        'visit': 1,
                        'dominant_turn_count': 0,
                        'total_turn_count': 2,
                        'username': '<no_name>'
                    },
                    'template_manager': {
                        'prev_hash': {
                            'PPsGsg': ['0Bsr2Q'],
                            'ocgSRA': ['VS0VHQ'],
                            'KxoF0w': [],
                            '/M0N7w': ['5SE7MQ']
                        },
                        'selector_history': [
                            'error::s_fragments/reprompt/thanks_for_sharing',
                            'error::s_fragments/reprompt/feedback',
                            'error::reprompt'
                        ]
                    },
                    'techsciencechat': {
                        'propose_continue': 'STOP'
                    },
                    'animalchat': {
                        'propose_continue': 'STOP'
                    },
                    'fashionchat': {
                        'propose_continue': 'STOP'
                    },
                    'timechat': {
                        'propose_continue': 'STOP'
                    },
                    'module_selection': {
                        'used_topic': ['LAUNCHGREETING']
                    },
                    'topic_to_replace': [],
                    'cur_selected_modules':
                    'LAUNCHGREETING',
                    'foodchat': {
                        'propose_continue': 'STOP'
                    },
                    'sys_env':
                    'local',
                    'bookchat_user': {
                        'propose_continue': 'STOP'
                    },
                    'socialchat': {
                        'curr_state': 's_init',
                        'propose_continue': 'STOP'
                    },
                    'module_rank': {
                        'MUSICCHAT': 0.0,
                        'GAMECHAT': 0.0,
                        'SPORT': 0.0,
                        'FASHIONCHAT': 0.0,
                        'TECHSCIENCECHAT': 0.0,
                        'TRAVELCHAT': 0.0,
                        'NEWS': 0.0,
                        'BOOKCHAT': 0.0,
                        'ANIMALCHAT': 0.0,
                        'FOODCHAT': 0.0,
                        'MOVIECHAT': 0.0
                    },
                    'travelchat': {
                        'propose_continue': 'STOP'
                    },
                    'gamechat': {
                        'propose_continue': 'STOP'
                    },
                    'weatherchat': {
                        'propose_continue': 'STOP'
                    },
                    'usr_name':
                    '<no_name>',
                    'moviechat_user': {
                        'propose_continue': 'STOP'
                    },
                    'sportchat': {
                        'propose_continue': 'STOP'
                    },
                    'last_module':
                    'LAUNCHGREETING',
                    'tedtalkchat': {
                        'propose_continue': 'STOP'
                    },
                    'outdoorchat': {
                        'propose_continue': 'STOP'
                    },
                    'launchgreeting': {
                        'volatile': {
                            'is_name_common': False
                        },
                        'persistent_store': {
                            'is_first_module': True
                        },
                        'propose_continue': 'CONTINUE',
                        'states': {
                            'state': 'greet_new_user'
                        }
                    },
                    'comfortchat': {
                        'propose_continue': 'STOP'
                    },
                    'holidaychat': {
                        'propose_continue': 'STOP'
                    },
                    'dailylife': {
                        'propose_continue': 'STOP'
                    },
                    'musicchat': {
                        'propose_continue': 'STOP'
                    },
                    'propose_topic':
                    None,
                    'last_turn_propose_open_question':
                    None,
                    'social_open_question_proposing':
                    None
                }
            }
        }
    }, dict(outputs=dict(result=[0, 0, 0, 1]))),
    ({
        'inputs': {
            'input_data': {
                'asr': [],
                'text':
                'bro',
                'features': {
                    'profanity_check':
                    None,
                    'sentence_completion_text':
                    'that’s bro',
                    'segmentation': {
                        'segmented_text': ['bro'],
                        'segmented_text_raw': ['bro']
                    },
                    'text':
                    'bro',
                    'central_elem': {
                        'senti': 'neu',
                        'regex': {
                            'sys': [],
                            'topic': [],
                            'lexical': []
                        },
                        'DA': 'statement',
                        'DA_score': '0.5253347',
                        'DA2': 'NA',
                        'DA2_score': '0.0',
                        'module': [],
                        'np': ['bro'],
                        'text': 'bro',
                        'backstory': {
                            'bot_propose_module': '',
                            'confidence': 0.6090900301933289,
                            'followup': '',
                            'module': '',
                            'neg': '',
                            'pos': '',
                            'postfix': '',
                            'question': 'You are my friend',
                            'reason': '',
                            'tag': '',
                            'text': 'Thank you, I love making new friends!  '
                        }
                    },
                    'spacynp':
                    None,
                    'amazon_topic':
                    'Phatic',
                    'topic2': [{
                        'text': 'bro',
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': []
                    }],
                    'concept': [{
                        'noun': 'bro',
                        'data': {
                            'nids': '0.5366',
                            'source nids system': '0.2439',
                            'intrusion detection system': '0.2195'
                        }
                    }],
                    'npknowledge': {
                        'knowledge': [],
                        'noun_phrase': ['bro'],
                        'local_noun_phrase': ['bro']
                    },
                    'sentiment':
                    None,
                    'returnnlp': [{
                        'text':
                        'bro',
                        'dialog_act': ['statement', '0.5253347'],
                        'dialog_act2': ['NA', '0.0'],
                        'intent': {
                            'sys': [],
                            'topic': [],
                            'lexical': []
                        },
                        'sentiment': {
                            'compound': '0.5',
                            'neg': '0.05',
                            'neu': '0.9',
                            'pos': '0.05'
                        },
                        'googlekg': [],
                        'noun_phrase': ['bro'],
                        'topic': {
                            'text': 'bro',
                            'topicClass': 'Phatic',
                            'confidence': 999.0,
                            'topicKeywords': []
                        },
                        'concept': [{
                            'noun': 'bro',
                            'data': {
                                'nids': '0.5366',
                                'source nids system': '0.2439',
                                'intrusion detection system': '0.2195'
                            }
                        }],
                        'amazon_topic':
                        'Phatic',
                        'dependency_parsing': {
                            'head': [0],
                            'label': ['flat']
                        },
                        'pos_tagging': ['NOUN']
                    }],
                    'intent_classify2': [{
                        'sys': [],
                        'topic': [],
                        'lexical': []
                    }],
                    'intent_classify': {
                        'sys': [],
                        'topic': [],
                        'lexical': []
                    },
                    'ner':
                    None,
                    'sentiment_allennlp': [{
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    }],
                    'asrcorrection':
                    None,
                    'dependency_parsing': {
                        'dependency_parent': [[0]],
                        'dependency_label': [['flat']],
                        'pos_tagging': [['NOUN']]
                    },
                    'nounphrase2': {
                        'noun_phrase': [['bro']],
                        'local_noun_phrase': [['bro']]
                    },
                    'sentiment2':
                    None,
                    'coreference':
                    None,
                    'googlekg': [],
                    'intent':
                    'general',
                    'topic': [{
                        'text': 'bro',
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': []
                    }],
                    'converttext':
                    'bro',
                    'dialog_act': [{
                        'text': 'bro',
                        'DA': 'statement',
                        'confidence': 0.5253347,
                        'DA2': 'NA',
                        'confidence2': 0.0
                    }],
                    'topic_module':
                    '',
                    'topic_keywords': [],
                    'knowledge': [],
                    'noun_phrase': ['bro']
                },
                'central_elem': {
                    'senti': 'neu',
                    'regex': {
                        'sys': [],
                        'topic': [],
                        'lexical': []
                    },
                    'DA': 'statement',
                    'DA_score': '0.5253347',
                    'DA2': 'NA',
                    'DA2_score': '0.0',
                    'module': [],
                    'np': ['bro'],
                    'text': 'bro',
                    'backstory': {
                        'bot_propose_module': '',
                        'confidence': 0.6090900301933289,
                        'followup': '',
                        'module': '',
                        'neg': '',
                        'pos': '',
                        'postfix': '',
                        'question': 'You are my friend',
                        'reason': '',
                        'tag': '',
                        'text': 'Thank you, I love making new friends!  '
                    }
                },
                'returnnlp': [{
                    'text':
                    'bro',
                    'dialog_act': ['statement', '0.5253347'],
                    'dialog_act2': ['NA', '0.0'],
                    'intent': {
                        'sys': [],
                        'topic': [],
                        'lexical': []
                    },
                    'sentiment': {
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    },
                    'googlekg': [],
                    'noun_phrase': ['bro'],
                    'topic': {
                        'text': 'bro',
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': []
                    },
                    'concept': [{
                        'noun': 'bro',
                        'data': {
                            'nids': '0.5366',
                            'source nids system': '0.2439',
                            'intrusion detection system': '0.2195'
                        }
                    }],
                    'amazon_topic':
                    'Phatic',
                    'dependency_parsing': {
                        'head': [0],
                        'label': ['flat']
                    },
                    'pos_tagging': ['NOUN']
                }],
                'a_b_test':
                'A',
                'system_acknowledgement': {
                    'output_tag': '',
                    'ack': ''
                },
                'bot_ask_open_question':
                None,
                'user_id':
                'localtest-user-5ba829a1-16c6-4bdc-b662-632d039cbc49',
                'conversationId':
                None,
                'news_user': {
                    'propose_continue': 'STOP'
                },
                'previous_modules':
                ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                'prev_hash': {
                    'sys_env': 'local'
                },
                'user_profile': {
                    'visit': 1,
                    'dominant_turn_count': 1,
                    'total_turn_count': 2,
                    'username': '<no_name>'
                },
                'template_manager': {
                    'prev_hash': {
                        'PPsGsg': ['fLkfQw'],
                        'ocgSRA': ['c4IZmw'],
                        'KxoF0w': [],
                        '/M0N7w': ['t3gtIQ']
                    },
                    'selector_history': [
                        'error::s_fragments/reprompt/thanks_for_sharing',
                        'error::s_fragments/reprompt/feedback',
                        'error::reprompt'
                    ]
                },
                'techsciencechat': {
                    'propose_continue': 'STOP'
                },
                'animalchat': {
                    'propose_continue': 'STOP'
                },
                'fashionchat': {
                    'propose_continue': 'STOP'
                },
                'timechat': {
                    'propose_continue': 'STOP'
                },
                'module_selection': {
                    'used_topic': ['LAUNCHGREETING']
                },
                'topic_to_replace': [],
                'cur_selected_modules':
                'LAUNCHGREETING',
                'foodchat': {
                    'propose_continue': 'STOP'
                },
                'sys_env':
                'local',
                'bookchat_user': {
                    'propose_continue': 'STOP'
                },
                'socialchat': {
                    'curr_state': 's_init',
                    'propose_continue': 'STOP'
                },
                'module_rank': {
                    'MUSICCHAT': 0.0,
                    'GAMECHAT': 0.0,
                    'SPORT': 0.0,
                    'FASHIONCHAT': 0.0,
                    'TECHSCIENCECHAT': 0.0,
                    'TRAVELCHAT': 0.0,
                    'NEWS': 0.0,
                    'BOOKCHAT': 0.0,
                    'ANIMALCHAT': 0.0,
                    'FOODCHAT': 0.0,
                    'MOVIECHAT': 0.0
                },
                'travelchat': {
                    'propose_continue': 'STOP'
                },
                'gamechat': {
                    'propose_continue': 'STOP'
                },
                'weatherchat': {
                    'propose_continue': 'STOP'
                },
                'usr_name':
                '<no_name>',
                'moviechat_user': {
                    'propose_continue': 'STOP'
                },
                'sportchat': {
                    'propose_continue': 'STOP'
                },
                'last_module':
                'LAUNCHGREETING',
                'tedtalkchat': {
                    'propose_continue': 'STOP'
                },
                'outdoorchat': {
                    'propose_continue': 'STOP'
                },
                'launchgreeting': {
                    'volatile': {
                        'is_name_common': False
                    },
                    'persistent_store': {
                        'is_first_module': True
                    },
                    'propose_continue': 'CONTINUE',
                    'states': {
                        'state': 'greet_new_user'
                    }
                },
                'comfortchat': {
                    'propose_continue': 'STOP'
                },
                'holidaychat': {
                    'propose_continue': 'STOP'
                },
                'dailylife': {
                    'propose_continue': 'STOP'
                },
                'musicchat': {
                    'propose_continue': 'STOP'
                },
                'propose_topic':
                None,
                'last_turn_propose_open_question':
                None,
                'social_open_question_proposing':
                None
            },
            'ua_ref': {
                'user_id':
                'localtest-user-5ba829a1-16c6-4bdc-b662-632d039cbc49',
                'map_attributes': {
                    'conversationId':
                    None,
                    'news_user': {
                        'propose_continue': 'STOP'
                    },
                    'previous_modules':
                    ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                    'prev_hash': {
                        'sys_env': 'local'
                    },
                    'user_profile': {
                        'visit': 1,
                        'dominant_turn_count': 1,
                        'total_turn_count': 2,
                        'username': '<no_name>'
                    },
                    'template_manager': {
                        'prev_hash': {
                            'PPsGsg': ['fLkfQw'],
                            'ocgSRA': ['c4IZmw'],
                            'KxoF0w': [],
                            '/M0N7w': ['t3gtIQ']
                        },
                        'selector_history': [
                            'error::s_fragments/reprompt/thanks_for_sharing',
                            'error::s_fragments/reprompt/feedback',
                            'error::reprompt'
                        ]
                    },
                    'techsciencechat': {
                        'propose_continue': 'STOP'
                    },
                    'animalchat': {
                        'propose_continue': 'STOP'
                    },
                    'fashionchat': {
                        'propose_continue': 'STOP'
                    },
                    'timechat': {
                        'propose_continue': 'STOP'
                    },
                    'module_selection': {
                        'used_topic': ['LAUNCHGREETING']
                    },
                    'topic_to_replace': [],
                    'cur_selected_modules':
                    'LAUNCHGREETING',
                    'foodchat': {
                        'propose_continue': 'STOP'
                    },
                    'sys_env':
                    'local',
                    'bookchat_user': {
                        'propose_continue': 'STOP'
                    },
                    'socialchat': {
                        'curr_state': 's_init',
                        'propose_continue': 'STOP'
                    },
                    'module_rank': {
                        'MUSICCHAT': 0.0,
                        'GAMECHAT': 0.0,
                        'SPORT': 0.0,
                        'FASHIONCHAT': 0.0,
                        'TECHSCIENCECHAT': 0.0,
                        'TRAVELCHAT': 0.0,
                        'NEWS': 0.0,
                        'BOOKCHAT': 0.0,
                        'ANIMALCHAT': 0.0,
                        'FOODCHAT': 0.0,
                        'MOVIECHAT': 0.0
                    },
                    'travelchat': {
                        'propose_continue': 'STOP'
                    },
                    'gamechat': {
                        'propose_continue': 'STOP'
                    },
                    'weatherchat': {
                        'propose_continue': 'STOP'
                    },
                    'usr_name':
                    '<no_name>',
                    'moviechat_user': {
                        'propose_continue': 'STOP'
                    },
                    'sportchat': {
                        'propose_continue': 'STOP'
                    },
                    'last_module':
                    'LAUNCHGREETING',
                    'tedtalkchat': {
                        'propose_continue': 'STOP'
                    },
                    'outdoorchat': {
                        'propose_continue': 'STOP'
                    },
                    'launchgreeting': {
                        'volatile': {
                            'is_name_common': False
                        },
                        'persistent_store': {
                            'is_first_module': True
                        },
                        'propose_continue': 'CONTINUE',
                        'states': {
                            'state': 'greet_new_user'
                        }
                    },
                    'comfortchat': {
                        'propose_continue': 'STOP'
                    },
                    'holidaychat': {
                        'propose_continue': 'STOP'
                    },
                    'dailylife': {
                        'propose_continue': 'STOP'
                    },
                    'musicchat': {
                        'propose_continue': 'STOP'
                    },
                    'propose_topic':
                    None,
                    'last_turn_propose_open_question':
                    None,
                    'social_open_question_proposing':
                    None
                }
            }
        }
    }, dict(outputs=dict(result=[0, 1, 0, 0], name_uncommon='bro'))),
    ({
        'inputs': {
            'input_data': {
                'asr': [],
                'text':
                'bigfoot',
                'features': {
                    'intent_classify2': [{
                        'sys': [],
                        'topic': [],
                        'lexical': []
                    }],
                    'npknowledge': {
                        'knowledge': [],
                        'noun_phrase': ['bigfoot'],
                        'local_noun_phrase': ['bigfoot']
                    },
                    'returnnlp': [{
                        'text':
                        'bigfoot',
                        'dialog_act': ['statement', '0.78731835'],
                        'dialog_act2': ['NA', '0.0'],
                        'intent': {
                            'sys': [],
                            'topic': [],
                            'lexical': []
                        },
                        'sentiment': {
                            'compound': '0.5',
                            'neg': '0.05',
                            'neu': '0.9',
                            'pos': '0.05'
                        },
                        'googlekg': [],
                        'noun_phrase': ['bigfoot'],
                        'topic': {
                            'text':
                            'bigfoot',
                            'topicClass':
                            'Travel_Geo',
                            'confidence':
                            999.0,
                            'topicKeywords': [{
                                'keyword': 'bigfoot',
                                'confidence': 999.0
                            }]
                        },
                        'concept': [{
                            'noun': 'bigfoot',
                            'data': {
                                'creature': '0.4571',
                                'urban legend': '0.3286',
                                'mysterious phenomenon': '0.2143'
                            }
                        }],
                        'amazon_topic':
                        'Phatic',
                        'dependency_parsing': {
                            'head': [0],
                            'label': ['flat']
                        },
                        'pos_tagging': ['NOUN']
                    }],
                    'intent':
                    'general',
                    'amazon_topic':
                    'Phatic',
                    'ner':
                    None,
                    'intent_classify': {
                        'sys': [],
                        'topic': [],
                        'lexical': []
                    },
                    'googlekg': [],
                    'profanity_check':
                    None,
                    'asrcorrection':
                    None,
                    'sentence_completion_text':
                    'that’s bigfoot',
                    'sentiment':
                    None,
                    'topic2': [{
                        'text':
                        'bigfoot',
                        'topicClass':
                        'Travel_Geo',
                        'confidence':
                        999.0,
                        'topicKeywords': [{
                            'keyword': 'bigfoot',
                            'confidence': 999.0
                        }]
                    }],
                    'concept': [{
                        'noun': 'bigfoot',
                        'data': {
                            'creature': '0.4571',
                            'urban legend': '0.3286',
                            'mysterious phenomenon': '0.2143'
                        }
                    }],
                    'converttext':
                    'bigfoot',
                    'dialog_act': [{
                        'text': 'bigfoot',
                        'DA': 'statement',
                        'confidence': 0.78731835,
                        'DA2': 'NA',
                        'confidence2': 0.0
                    }],
                    'coreference':
                    None,
                    'dependency_parsing': {
                        'dependency_parent': [[0]],
                        'dependency_label': [['flat']],
                        'pos_tagging': [['NOUN']]
                    },
                    'sentiment_allennlp': [{
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    }],
                    'topic': [{
                        'text':
                        'bigfoot',
                        'topicClass':
                        'Travel_Geo',
                        'confidence':
                        999.0,
                        'topicKeywords': [{
                            'keyword': 'bigfoot',
                            'confidence': 999.0
                        }]
                    }],
                    'segmentation': {
                        'segmented_text': ['bigfoot'],
                        'segmented_text_raw': ['bigfoot']
                    },
                    'spacynp': ['bigfoot'],
                    'nounphrase2': {
                        'noun_phrase': [['bigfoot']],
                        'local_noun_phrase': [['bigfoot']]
                    },
                    'sentiment2':
                    None,
                    'text':
                    'bigfoot',
                    'central_elem': {
                        'senti': 'neu',
                        'regex': {
                            'sys': [],
                            'topic': [],
                            'lexical': []
                        },
                        'DA': 'statement',
                        'DA_score': '0.78731835',
                        'DA2': 'NA',
                        'DA2_score': '0.0',
                        'module': [],
                        'np': ['bigfoot'],
                        'text': 'bigfoot',
                        'backstory': {
                            'bot_propose_module':
                            'TECHSCIENCECHAT',
                            'confidence':
                            0.49884501099586487,
                            'followup':
                            '',
                            'module':
                            'TECHSCIENCECHAT',
                            'neg':
                            '',
                            'pos':
                            '',
                            'postfix':
                            '',
                            'question':
                            'do you think there are aliens',
                            'reason':
                            '',
                            'tag':
                            '',
                            'text':
                            'Yes! <break time="300ms"/> I guess I\'ve watched too many sci-fi movies.'
                        }
                    },
                    'topic_module':
                    '',
                    'topic_keywords': [{
                        'keyword': 'bigfoot',
                        'confidence': 999.0
                    }],
                    'knowledge': [],
                    'noun_phrase': ['bigfoot']
                },
                'central_elem': {
                    'senti': 'neu',
                    'regex': {
                        'sys': [],
                        'topic': [],
                        'lexical': []
                    },
                    'DA': 'statement',
                    'DA_score': '0.78731835',
                    'DA2': 'NA',
                    'DA2_score': '0.0',
                    'module': [],
                    'np': ['bigfoot'],
                    'text': 'bigfoot',
                    'backstory': {
                        'bot_propose_module':
                        'TECHSCIENCECHAT',
                        'confidence':
                        0.49884501099586487,
                        'followup':
                        '',
                        'module':
                        'TECHSCIENCECHAT',
                        'neg':
                        '',
                        'pos':
                        '',
                        'postfix':
                        '',
                        'question':
                        'do you think there are aliens',
                        'reason':
                        '',
                        'tag':
                        '',
                        'text':
                        'Yes! <break time="300ms"/> I guess I\'ve watched too many sci-fi movies.'
                    }
                },
                'returnnlp': [{
                    'text':
                    'bigfoot',
                    'dialog_act': ['statement', '0.78731835'],
                    'dialog_act2': ['NA', '0.0'],
                    'intent': {
                        'sys': [],
                        'topic': [],
                        'lexical': []
                    },
                    'sentiment': {
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    },
                    'googlekg': [],
                    'noun_phrase': ['bigfoot'],
                    'topic': {
                        'text':
                        'bigfoot',
                        'topicClass':
                        'Travel_Geo',
                        'confidence':
                        999.0,
                        'topicKeywords': [{
                            'keyword': 'bigfoot',
                            'confidence': 999.0
                        }]
                    },
                    'concept': [{
                        'noun': 'bigfoot',
                        'data': {
                            'creature': '0.4571',
                            'urban legend': '0.3286',
                            'mysterious phenomenon': '0.2143'
                        }
                    }],
                    'amazon_topic':
                    'Phatic',
                    'dependency_parsing': {
                        'head': [0],
                        'label': ['flat']
                    },
                    'pos_tagging': ['NOUN']
                }],
                'a_b_test':
                'A',
                'system_acknowledgement': {
                    'output_tag': '',
                    'ack': ''
                },
                'bot_ask_open_question':
                None,
                'user_id':
                'localtest-user-9834a7ca-c442-418b-9e98-7b226ff3f265',
                'conversationId':
                None,
                'news_user': {
                    'propose_continue': 'STOP'
                },
                'previous_modules':
                ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                'prev_hash': {
                    'sys_env': 'local'
                },
                'user_profile': {
                    'visit': 1,
                    'dominant_turn_count': 1,
                    'total_turn_count': 2,
                    'username': '<no_name>'
                },
                'template_manager': {
                    'prev_hash': {
                        'PPsGsg': ['FlAjjQ'],
                        'ocgSRA': ['3skbGA'],
                        'KxoF0w': [],
                        '/M0N7w': ['aeg3CQ']
                    },
                    'selector_history': [
                        'error::s_fragments/reprompt/thanks_for_sharing',
                        'error::s_fragments/reprompt/feedback',
                        'error::reprompt'
                    ]
                },
                'techsciencechat': {
                    'propose_continue': 'STOP'
                },
                'animalchat': {
                    'propose_continue': 'STOP'
                },
                'fashionchat': {
                    'propose_continue': 'STOP'
                },
                'timechat': {
                    'propose_continue': 'STOP'
                },
                'module_selection': {
                    'used_topic': ['LAUNCHGREETING']
                },
                'topic_to_replace': [],
                'cur_selected_modules':
                'LAUNCHGREETING',
                'foodchat': {
                    'propose_continue': 'STOP'
                },
                'sys_env':
                'local',
                'bookchat_user': {
                    'propose_continue': 'STOP'
                },
                'socialchat': {
                    'curr_state': 's_init',
                    'propose_continue': 'STOP'
                },
                'module_rank': {
                    'MUSICCHAT': 0.0,
                    'GAMECHAT': 0.0,
                    'SPORT': 0.0,
                    'FASHIONCHAT': 0.0,
                    'TECHSCIENCECHAT': 0.0,
                    'TRAVELCHAT': 0.0,
                    'NEWS': 0.0,
                    'BOOKCHAT': 0.0,
                    'ANIMALCHAT': 0.0,
                    'FOODCHAT': 0.0,
                    'MOVIECHAT': 0.0
                },
                'travelchat': {
                    'propose_continue': 'STOP'
                },
                'gamechat': {
                    'propose_continue': 'STOP'
                },
                'weatherchat': {
                    'propose_continue': 'STOP'
                },
                'usr_name':
                '<no_name>',
                'moviechat_user': {
                    'propose_continue': 'STOP'
                },
                'sportchat': {
                    'propose_continue': 'STOP'
                },
                'last_module':
                'LAUNCHGREETING',
                'tedtalkchat': {
                    'propose_continue': 'STOP'
                },
                'outdoorchat': {
                    'propose_continue': 'STOP'
                },
                'launchgreeting': {
                    'volatile': {
                        'is_name_common': False
                    },
                    'persistent_store': {
                        'is_first_module': True
                    },
                    'propose_continue': 'CONTINUE',
                    'states': {
                        'state': 'greet_new_user'
                    }
                },
                'comfortchat': {
                    'propose_continue': 'STOP'
                },
                'holidaychat': {
                    'propose_continue': 'STOP'
                },
                'dailylife': {
                    'propose_continue': 'STOP'
                },
                'musicchat': {
                    'propose_continue': 'STOP'
                },
                'propose_topic':
                None,
                'last_turn_propose_open_question':
                None,
                'social_open_question_proposing':
                None
            },
            'ua_ref': {
                'user_id':
                'localtest-user-9834a7ca-c442-418b-9e98-7b226ff3f265',
                'map_attributes': {
                    'conversationId':
                    None,
                    'news_user': {
                        'propose_continue': 'STOP'
                    },
                    'previous_modules':
                    ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                    'prev_hash': {
                        'sys_env': 'local'
                    },
                    'user_profile': {
                        'visit': 1,
                        'dominant_turn_count': 1,
                        'total_turn_count': 2,
                        'username': '<no_name>'
                    },
                    'template_manager': {
                        'prev_hash': {
                            'PPsGsg': ['FlAjjQ'],
                            'ocgSRA': ['3skbGA'],
                            'KxoF0w': [],
                            '/M0N7w': ['aeg3CQ']
                        },
                        'selector_history': [
                            'error::s_fragments/reprompt/thanks_for_sharing',
                            'error::s_fragments/reprompt/feedback',
                            'error::reprompt'
                        ]
                    },
                    'techsciencechat': {
                        'propose_continue': 'STOP'
                    },
                    'animalchat': {
                        'propose_continue': 'STOP'
                    },
                    'fashionchat': {
                        'propose_continue': 'STOP'
                    },
                    'timechat': {
                        'propose_continue': 'STOP'
                    },
                    'module_selection': {
                        'used_topic': ['LAUNCHGREETING']
                    },
                    'topic_to_replace': [],
                    'cur_selected_modules':
                    'LAUNCHGREETING',
                    'foodchat': {
                        'propose_continue': 'STOP'
                    },
                    'sys_env':
                    'local',
                    'bookchat_user': {
                        'propose_continue': 'STOP'
                    },
                    'socialchat': {
                        'curr_state': 's_init',
                        'propose_continue': 'STOP'
                    },
                    'module_rank': {
                        'MUSICCHAT': 0.0,
                        'GAMECHAT': 0.0,
                        'SPORT': 0.0,
                        'FASHIONCHAT': 0.0,
                        'TECHSCIENCECHAT': 0.0,
                        'TRAVELCHAT': 0.0,
                        'NEWS': 0.0,
                        'BOOKCHAT': 0.0,
                        'ANIMALCHAT': 0.0,
                        'FOODCHAT': 0.0,
                        'MOVIECHAT': 0.0
                    },
                    'travelchat': {
                        'propose_continue': 'STOP'
                    },
                    'gamechat': {
                        'propose_continue': 'STOP'
                    },
                    'weatherchat': {
                        'propose_continue': 'STOP'
                    },
                    'usr_name':
                    '<no_name>',
                    'moviechat_user': {
                        'propose_continue': 'STOP'
                    },
                    'sportchat': {
                        'propose_continue': 'STOP'
                    },
                    'last_module':
                    'LAUNCHGREETING',
                    'tedtalkchat': {
                        'propose_continue': 'STOP'
                    },
                    'outdoorchat': {
                        'propose_continue': 'STOP'
                    },
                    'launchgreeting': {
                        'volatile': {
                            'is_name_common': False
                        },
                        'persistent_store': {
                            'is_first_module': True
                        },
                        'propose_continue': 'CONTINUE',
                        'states': {
                            'state': 'greet_new_user'
                        }
                    },
                    'comfortchat': {
                        'propose_continue': 'STOP'
                    },
                    'holidaychat': {
                        'propose_continue': 'STOP'
                    },
                    'dailylife': {
                        'propose_continue': 'STOP'
                    },
                    'musicchat': {
                        'propose_continue': 'STOP'
                    },
                    'propose_topic':
                    None,
                    'last_turn_propose_open_question':
                    None,
                    'social_open_question_proposing':
                    None
                }
            }
        }
    }, dict(outputs=dict(result=[0, 1, 0, 0], name_uncommon='bigfoot'))),
    # go to question handler and back
    ({
        'inputs': {
            'input_data': {
                'asr': [],
                'text':
                "hi my name is riley what's your name",
                'features': {
                    'coreference':
                    None,
                    'nounphrase2': {
                        'noun_phrase': [[], ['riley'], []],
                        'local_noun_phrase': [[], ['riley'], []]
                    },
                    'concept': [{
                        'noun': 'riley',
                        'data': {
                            'name': '0.4091',
                            'unisex name': '0.3182',
                            'theoretical study': '0.2727'
                        }
                    }],
                    'sentiment':
                    'neu',
                    'converttext':
                    "hi my name is riley what's your name",
                    'dialog_act': [{
                        'text': 'hi',
                        'DA': 'statement',
                        'confidence': 0.22296835,
                        'DA2': 'NA',
                        'confidence2': 0.0
                    }, {
                        'text': 'my name is riley',
                        'DA': 'statement',
                        'confidence': 0.91709507,
                        'DA2': 'NA',
                        'confidence2': 0.0
                    }, {
                        'text': "what's your name",
                        'DA': 'open_question_factual',
                        'confidence': 0.66738737,
                        'DA2': 'NA',
                        'confidence2': 0.0
                    }],
                    'amazon_topic':
                    None,
                    'intent_classify2': [{
                        'sys': [],
                        'topic': [],
                        'lexical': []
                    }, {
                        'sys': [],
                        'topic': [],
                        'lexical': ['info_name']
                    }, {
                        'sys': ['say_bio'],
                        'topic': ['topic_backstory'],
                        'lexical': ['ask_fact', 'ask_name']
                    }],
                    'npknowledge': {
                        'knowledge': [[
                            'Riley', 'Fictional character',
                            '797.1090087890625', '__EMPTY__'
                        ]],
                        'noun_phrase': ['riley'],
                        'local_noun_phrase': ['riley']
                    },
                    'asrcorrection':
                    None,
                    'spacynp': ['riley'],
                    'central_elem': {
                        'senti': 'neu',
                        'regex': {
                            'sys': ['say_bio'],
                            'topic': ['topic_backstory'],
                            'lexical': ['ask_fact', 'ask_name']
                        },
                        'DA': 'open_question_factual',
                        'DA_score': '0.66738737',
                        'DA2': 'NA',
                        'DA2_score': '0.0',
                        'module': [],
                        'np': [],
                        'text': "what's your name",
                        'backstory': {
                            'bot_propose_module':
                            '',
                            'confidence':
                            0.9814661145210266,
                            'followup':
                            '',
                            'module':
                            '',
                            'neg':
                            '',
                            'pos':
                            '',
                            'postfix':
                            '',
                            'question':
                            'What is your name',
                            'reason':
                            '',
                            'tag':
                            '',
                            'text':
                            "I am Alexa.  I can't tell you much more because I'm in a competition right now."
                        }
                    },
                    'topic2': [{
                        'text': 'hi',
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': []
                    }, {
                        'text':
                        'my name is riley',
                        'topicClass':
                        'Phatic',
                        'confidence':
                        999.0,
                        'topicKeywords': [{
                            'keyword': None,
                            'confidence': 999.0
                        }]
                    }, {
                        'text': "what's your name",
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': []
                    }],
                    'profanity_check':
                    None,
                    'sentiment2': [{
                        'neg': '0.0',
                        'neu': '1.0',
                        'pos': '0.0',
                        'compound': '0.0'
                    }, {
                        'neg': '0.0',
                        'neu': '1.0',
                        'pos': '0.0',
                        'compound': '0.0'
                    }, {
                        'neg': '0.0',
                        'neu': '1.0',
                        'pos': '0.0',
                        'compound': '0.0'
                    }],
                    'dependency_parsing': {
                        'dependency_parent': [[0], [2, 4, 4, 0], [0, 3, 1]],
                        'dependency_label':
                        [['discourse'], ['nmod:poss', 'nsubj', 'cop', 'ccomp'],
                         ['xcomp', 'nmod:poss', 'obj']],
                        'pos_tagging': [['INTJ'],
                                        ['PRON', 'NOUN', 'AUX', 'PROPN'],
                                        ['VERB', 'PRON', 'NOUN']]
                    },
                    'intent_classify': {
                        'sys': [],
                        'topic': [],
                        'lexical': []
                    },
                    'sentiment_allennlp': [{
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    }, {
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    }, {
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    }],
                    'returnnlp': [{
                        'text': 'hi',
                        'dialog_act': ['statement', '0.22296835'],
                        'dialog_act2': ['NA', '0.0'],
                        'intent': {
                            'sys': [],
                            'topic': [],
                            'lexical': []
                        },
                        'sentiment': {
                            'compound': '0.5',
                            'neg': '0.05',
                            'neu': '0.9',
                            'pos': '0.05'
                        },
                        'googlekg': [],
                        'noun_phrase': [],
                        'topic': {
                            'text': 'hi',
                            'topicClass': 'Phatic',
                            'confidence': 999.0,
                            'topicKeywords': []
                        },
                        'concept': [],
                        'amazon_topic': None,
                        'dependency_parsing': {
                            'head': [0],
                            'label': ['discourse']
                        },
                        'pos_tagging': ['INTJ']
                    }, {
                        'text':
                        'my name is riley',
                        'dialog_act': ['statement', '0.91709507'],
                        'dialog_act2': ['NA', '0.0'],
                        'intent': {
                            'sys': [],
                            'topic': [],
                            'lexical': ['info_name']
                        },
                        'sentiment': {
                            'compound': '0.5',
                            'neg': '0.05',
                            'neu': '0.9',
                            'pos': '0.05'
                        },
                        'googlekg': [],
                        'noun_phrase': ['riley'],
                        'topic': {
                            'text':
                            'my name is riley',
                            'topicClass':
                            'Phatic',
                            'confidence':
                            999.0,
                            'topicKeywords': [{
                                'keyword': None,
                                'confidence': 999.0
                            }]
                        },
                        'concept': [{
                            'noun': 'riley',
                            'data': {
                                'name': '0.4091',
                                'unisex name': '0.3182',
                                'theoretical study': '0.2727'
                            }
                        }],
                        'amazon_topic':
                        None,
                        'dependency_parsing': {
                            'head': [2, 4, 4, 0],
                            'label': ['nmod:poss', 'nsubj', 'cop', 'ccomp']
                        },
                        'pos_tagging': ['PRON', 'NOUN', 'AUX', 'PROPN']
                    }, {
                        'text':
                        "what's your name",
                        'dialog_act': ['open_question_factual', '0.66738737'],
                        'dialog_act2': ['NA', '0.0'],
                        'intent': {
                            'sys': ['say_bio'],
                            'topic': ['topic_backstory'],
                            'lexical': ['ask_fact', 'ask_name']
                        },
                        'sentiment': {
                            'compound': '0.5',
                            'neg': '0.05',
                            'neu': '0.9',
                            'pos': '0.05'
                        },
                        'googlekg': [],
                        'noun_phrase': [],
                        'topic': {
                            'text': "what's your name",
                            'topicClass': 'Phatic',
                            'confidence': 999.0,
                            'topicKeywords': []
                        },
                        'concept': [],
                        'amazon_topic':
                        None,
                        'dependency_parsing': {
                            'head': [0, 3, 1],
                            'label': ['xcomp', 'nmod:poss', 'obj']
                        },
                        'pos_tagging': ['VERB', 'PRON', 'NOUN']
                    }],
                    'googlekg': [],
                    'segmentation': {
                        'segmented_text':
                        ['hi', 'my name is ner', "what's your name"],
                        'segmented_text_raw':
                        ['hi', 'my name is riley', "what's your name"]
                    },
                    'intent':
                    'general',
                    'ner': [{
                        'text': 'Riley',
                        'begin_offset': 14,
                        'end_offset': 19,
                        'label': 'PERSON'
                    }],
                    'sentence_completion_text':
                    "hi my name is riley what 's your name",
                    'text':
                    "hi my name is riley what's your name",
                    'topic':
                    None,
                    'topic_module':
                    '',
                    'topic_keywords':
                    '',
                    'knowledge': [[
                        'Riley', 'Fictional character', '797.1090087890625',
                        '__EMPTY__'
                    ]],
                    'noun_phrase': ['riley']
                },
                'central_elem': {
                    'senti': 'neu',
                    'regex': {
                        'sys': ['say_bio'],
                        'topic': ['topic_backstory'],
                        'lexical': ['ask_fact', 'ask_name']
                    },
                    'DA': 'open_question_factual',
                    'DA_score': '0.66738737',
                    'DA2': 'NA',
                    'DA2_score': '0.0',
                    'module': [],
                    'np': [],
                    'text': "what's your name",
                    'backstory': {
                        'bot_propose_module':
                        '',
                        'confidence':
                        0.9814661145210266,
                        'followup':
                        '',
                        'module':
                        '',
                        'neg':
                        '',
                        'pos':
                        '',
                        'postfix':
                        '',
                        'question':
                        'What is your name',
                        'reason':
                        '',
                        'tag':
                        '',
                        'text':
                        "I am Alexa.  I can't tell you much more because I'm in a competition right now."
                    }
                },
                'returnnlp': [{
                    'text': 'hi',
                    'dialog_act': ['statement', '0.22296835'],
                    'dialog_act2': ['NA', '0.0'],
                    'intent': {
                        'sys': [],
                        'topic': [],
                        'lexical': []
                    },
                    'sentiment': {
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    },
                    'googlekg': [],
                    'noun_phrase': [],
                    'topic': {
                        'text': 'hi',
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': []
                    },
                    'concept': [],
                    'amazon_topic': None,
                    'dependency_parsing': {
                        'head': [0],
                        'label': ['discourse']
                    },
                    'pos_tagging': ['INTJ']
                }, {
                    'text':
                    'my name is riley',
                    'dialog_act': ['statement', '0.91709507'],
                    'dialog_act2': ['NA', '0.0'],
                    'intent': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['info_name']
                    },
                    'sentiment': {
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    },
                    'googlekg': [],
                    'noun_phrase': ['riley'],
                    'topic': {
                        'text': 'my name is riley',
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': [{
                            'keyword': None,
                            'confidence': 999.0
                        }]
                    },
                    'concept': [{
                        'noun': 'riley',
                        'data': {
                            'name': '0.4091',
                            'unisex name': '0.3182',
                            'theoretical study': '0.2727'
                        }
                    }],
                    'amazon_topic':
                    None,
                    'dependency_parsing': {
                        'head': [2, 4, 4, 0],
                        'label': ['nmod:poss', 'nsubj', 'cop', 'ccomp']
                    },
                    'pos_tagging': ['PRON', 'NOUN', 'AUX', 'PROPN']
                }, {
                    'text':
                    "what's your name",
                    'dialog_act': ['open_question_factual', '0.66738737'],
                    'dialog_act2': ['NA', '0.0'],
                    'intent': {
                        'sys': ['say_bio'],
                        'topic': ['topic_backstory'],
                        'lexical': ['ask_fact', 'ask_name']
                    },
                    'sentiment': {
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    },
                    'googlekg': [],
                    'noun_phrase': [],
                    'topic': {
                        'text': "what's your name",
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': []
                    },
                    'concept': [],
                    'amazon_topic':
                    None,
                    'dependency_parsing': {
                        'head': [0, 3, 1],
                        'label': ['xcomp', 'nmod:poss', 'obj']
                    },
                    'pos_tagging': ['VERB', 'PRON', 'NOUN']
                }],
                'a_b_test':
                'B',
                'system_acknowledgement': {
                    'output_tag': '',
                    'ack': ''
                },
                'bot_ask_open_question':
                None,
                'user_id':
                'localtest-user-3a94bb56-1656-42b0-bbd3-049f784726c5',
                'conversationId':
                None,
                'news_user': {
                    'propose_continue': 'STOP'
                },
                'previous_modules':
                ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                'prev_hash': {
                    'sys_env': 'local'
                },
                'user_profile': {
                    'visit': 1,
                    'dominant_turn_count': 1,
                    'total_turn_count': 2,
                    'username': '<no_name>'
                },
                'template_manager': {
                    'prev_hash': {
                        'PPsGsg': ['FlAjjQ'],
                        'ocgSRA': ['c4IZmw'],
                        'KxoF0w': [],
                        '/M0N7w': ['aeg3CQ']
                    },
                    'selector_history': [
                        'error::s_fragments/reprompt/thanks_for_sharing',
                        'error::s_fragments/reprompt/feedback',
                        'error::reprompt'
                    ]
                },
                'techsciencechat': {
                    'propose_continue': 'STOP'
                },
                'animalchat': {
                    'propose_continue': 'STOP'
                },
                'fashionchat': {
                    'propose_continue': 'STOP'
                },
                'timechat': {
                    'propose_continue': 'STOP'
                },
                'module_selection': {
                    'used_topic': ['LAUNCHGREETING']
                },
                'topic_to_replace': [],
                'cur_selected_modules':
                'LAUNCHGREETING',
                'foodchat': {
                    'propose_continue': 'STOP'
                },
                'sys_env':
                'local',
                'bookchat_user': {
                    'propose_continue': 'STOP'
                },
                'socialchat': {
                    'curr_state': 's_init',
                    'propose_continue': 'STOP'
                },
                'module_rank': {
                    'MUSICCHAT': 0.0,
                    'GAMECHAT': 0.0,
                    'SPORT': 0.0,
                    'FASHIONCHAT': 0.0,
                    'TECHSCIENCECHAT': 0.0,
                    'TRAVELCHAT': 0.0,
                    'NEWS': 0.0,
                    'BOOKCHAT': 0.0,
                    'ANIMALCHAT': 0.0,
                    'FOODCHAT': 0.0,
                    'MOVIECHAT': 0.0
                },
                'travelchat': {
                    'propose_continue': 'STOP'
                },
                'gamechat': {
                    'propose_continue': 'STOP'
                },
                'weatherchat': {
                    'propose_continue': 'STOP'
                },
                'usr_name':
                '<no_name>',
                'moviechat_user': {
                    'propose_continue': 'STOP'
                },
                'sportchat': {
                    'propose_continue': 'STOP'
                },
                'last_module':
                'LAUNCHGREETING',
                'tedtalkchat': {
                    'propose_continue': 'STOP'
                },
                'outdoorchat': {
                    'propose_continue': 'STOP'
                },
                'launchgreeting': {
                    'volatile': {
                        'is_name_common': False
                    },
                    'persistent_store': {
                        'is_first_module': True
                    },
                    'propose_continue': 'CONTINUE',
                    'states': {
                        'state': 'greet_new_user'
                    }
                },
                'comfortchat': {
                    'propose_continue': 'STOP'
                },
                'holidaychat': {
                    'propose_continue': 'STOP'
                },
                'dailylife': {
                    'propose_continue': 'STOP'
                },
                'musicchat': {
                    'propose_continue': 'STOP'
                },
                'propose_topic':
                None,
                'last_turn_propose_open_question':
                None,
                'social_open_question_proposing':
                None
            },
            'ua_ref': {
                'user_id':
                'localtest-user-3a94bb56-1656-42b0-bbd3-049f784726c5',
                'map_attributes': {
                    'conversationId':
                    None,
                    'news_user': {
                        'propose_continue': 'STOP'
                    },
                    'previous_modules':
                    ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                    'prev_hash': {
                        'sys_env': 'local'
                    },
                    'user_profile': {
                        'visit': 1,
                        'dominant_turn_count': 1,
                        'total_turn_count': 2,
                        'username': '<no_name>'
                    },
                    'template_manager': {
                        'prev_hash': {
                            'PPsGsg': ['FlAjjQ'],
                            'ocgSRA': ['c4IZmw'],
                            'KxoF0w': [],
                            '/M0N7w': ['aeg3CQ']
                        },
                        'selector_history': [
                            'error::s_fragments/reprompt/thanks_for_sharing',
                            'error::s_fragments/reprompt/feedback',
                            'error::reprompt'
                        ]
                    },
                    'techsciencechat': {
                        'propose_continue': 'STOP'
                    },
                    'animalchat': {
                        'propose_continue': 'STOP'
                    },
                    'fashionchat': {
                        'propose_continue': 'STOP'
                    },
                    'timechat': {
                        'propose_continue': 'STOP'
                    },
                    'module_selection': {
                        'used_topic': ['LAUNCHGREETING']
                    },
                    'topic_to_replace': [],
                    'cur_selected_modules':
                    'LAUNCHGREETING',
                    'foodchat': {
                        'propose_continue': 'STOP'
                    },
                    'sys_env':
                    'local',
                    'bookchat_user': {
                        'propose_continue': 'STOP'
                    },
                    'socialchat': {
                        'curr_state': 's_init',
                        'propose_continue': 'STOP'
                    },
                    'module_rank': {
                        'MUSICCHAT': 0.0,
                        'GAMECHAT': 0.0,
                        'SPORT': 0.0,
                        'FASHIONCHAT': 0.0,
                        'TECHSCIENCECHAT': 0.0,
                        'TRAVELCHAT': 0.0,
                        'NEWS': 0.0,
                        'BOOKCHAT': 0.0,
                        'ANIMALCHAT': 0.0,
                        'FOODCHAT': 0.0,
                        'MOVIECHAT': 0.0
                    },
                    'travelchat': {
                        'propose_continue': 'STOP'
                    },
                    'gamechat': {
                        'propose_continue': 'STOP'
                    },
                    'weatherchat': {
                        'propose_continue': 'STOP'
                    },
                    'usr_name':
                    '<no_name>',
                    'moviechat_user': {
                        'propose_continue': 'STOP'
                    },
                    'sportchat': {
                        'propose_continue': 'STOP'
                    },
                    'last_module':
                    'LAUNCHGREETING',
                    'tedtalkchat': {
                        'propose_continue': 'STOP'
                    },
                    'outdoorchat': {
                        'propose_continue': 'STOP'
                    },
                    'launchgreeting': {
                        'volatile': {
                            'is_name_common': False
                        },
                        'persistent_store': {
                            'is_first_module': True
                        },
                        'propose_continue': 'CONTINUE',
                        'states': {
                            'state': 'greet_new_user'
                        }
                    },
                    'comfortchat': {
                        'propose_continue': 'STOP'
                    },
                    'holidaychat': {
                        'propose_continue': 'STOP'
                    },
                    'dailylife': {
                        'propose_continue': 'STOP'
                    },
                    'musicchat': {
                        'propose_continue': 'STOP'
                    },
                    'propose_topic':
                    None,
                    'last_turn_propose_open_question':
                    None,
                    'social_open_question_proposing':
                    None
                }
            }
        }
    }, dict(outputs=dict(result=[1, 0, 0, 0], name='riley'))),
    ({
        'inputs': {
            'input_data': {
                'asr': [],
                'text':
                'my name is peanutbutter what is your name',
                'features': {
                    'sentence_completion_text':
                    'my name is peanutbutter what is your name',
                    'coreference':
                    None,
                    'asrcorrection':
                    None,
                    'returnnlp': [{
                        'text':
                        'my name is peanutbutter',
                        'dialog_act': ['statement', '0.95753586'],
                        'dialog_act2': ['NA', '0.0'],
                        'intent': {
                            'sys': [],
                            'topic': [],
                            'lexical': ['info_name']
                        },
                        'sentiment': {
                            'compound': '0.5',
                            'neg': '0.05',
                            'neu': '0.9',
                            'pos': '0.05'
                        },
                        'googlekg': [],
                        'noun_phrase': ['peanutbutter'],
                        'topic': {
                            'text':
                            'my name is peanutbutter',
                            'topicClass':
                            'Phatic',
                            'confidence':
                            999.0,
                            'topicKeywords': [{
                                'keyword': None,
                                'confidence': 999.0
                            }]
                        },
                        'concept': [{
                            'noun': 'peanutbutter',
                            'data': {
                                'shelf stable filling': '0.6364',
                                'food': '0.2727',
                                'food item': '0.0909'
                            }
                        }],
                        'amazon_topic':
                        None,
                        'dependency_parsing': {
                            'head': [2, 4, 4, 0],
                            'label':
                            ['nmod:poss', 'nsubj', 'aux', 'parataxis']
                        },
                        'pos_tagging': ['PRON', 'NOUN', 'AUX', 'ADJ']
                    }, {
                        'text':
                        'what is your name',
                        'dialog_act': ['open_question_factual', '0.57096076'],
                        'dialog_act2': ['NA', '0.0'],
                        'intent': {
                            'sys': ['say_bio'],
                            'topic': ['topic_backstory'],
                            'lexical': ['ask_fact', 'ask_name']
                        },
                        'sentiment': {
                            'compound': '0.5',
                            'neg': '0.05',
                            'neu': '0.9',
                            'pos': '0.05'
                        },
                        'googlekg': [],
                        'noun_phrase': [],
                        'topic': {
                            'text': 'what is your name',
                            'topicClass': 'Phatic',
                            'confidence': 999.0,
                            'topicKeywords': []
                        },
                        'concept': [],
                        'amazon_topic':
                        None,
                        'dependency_parsing': {
                            'head': [4, 4, 4, 0],
                            'label': ['obj', 'cop', 'nmod:poss', 'obj']
                        },
                        'pos_tagging': ['PRON', 'AUX', 'PRON', 'NOUN']
                    }],
                    'profanity_check':
                    None,
                    'dialog_act': [{
                        'text': 'my name is peanutbutter',
                        'DA': 'statement',
                        'confidence': 0.95753586,
                        'DA2': 'NA',
                        'confidence2': 0.0
                    }, {
                        'text': 'what is your name',
                        'DA': 'open_question_factual',
                        'confidence': 0.57096076,
                        'DA2': 'NA',
                        'confidence2': 0.0
                    }],
                    'topic2': [{
                        'text':
                        'my name is peanutbutter',
                        'topicClass':
                        'Phatic',
                        'confidence':
                        999.0,
                        'topicKeywords': [{
                            'keyword': None,
                            'confidence': 999.0
                        }]
                    }, {
                        'text': 'what is your name',
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': []
                    }],
                    'converttext':
                    'my name is peanutbutter what is your name',
                    'concept': [{
                        'noun': 'peanutbutter',
                        'data': {
                            'shelf stable filling': '0.6364',
                            'food': '0.2727',
                            'food item': '0.0909'
                        }
                    }],
                    'sentiment_allennlp': [{
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    }, {
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    }],
                    'spacynp': ['peanutbutter'],
                    'segmentation': {
                        'segmented_text':
                        ['my name is peanutbutter', 'what is your name'],
                        'segmented_text_raw':
                        ['my name is peanutbutter', 'what is your name']
                    },
                    'central_elem': {
                        'senti': 'neu',
                        'regex': {
                            'sys': [],
                            'topic': [],
                            'lexical': ['info_name']
                        },
                        'DA': 'statement',
                        'DA_score': '0.95753586',
                        'DA2': 'NA',
                        'DA2_score': '0.0',
                        'module': ['FOODCHAT'],
                        'np': ['peanutbutter'],
                        'text': 'my name is peanutbutter',
                        'backstory': {
                            'bot_propose_module': '',
                            'confidence': 0.6112542152404785,
                            'followup': '',
                            'module': '',
                            'neg': '',
                            'pos': '',
                            'postfix': '',
                            'question': 'do you like my name',
                            'reason': '',
                            'tag': '',
                            'text': "Yeah, it's a nice name! "
                        }
                    },
                    'googlekg': [],
                    'intent':
                    'general',
                    'text':
                    'my name is peanutbutter what is your name',
                    'amazon_topic':
                    None,
                    'ner': [{
                        'text': 'Peanutbutter',
                        'begin_offset': 11,
                        'end_offset': 23,
                        'label': 'PERSON'
                    }],
                    'sentiment':
                    'neu',
                    'nounphrase2': {
                        'noun_phrase': [['peanutbutter'], []],
                        'local_noun_phrase': [['peanutbutter'], []]
                    },
                    'sentiment2': [{
                        'neg': '0.0',
                        'neu': '1.0',
                        'pos': '0.0',
                        'compound': '0.0'
                    }, {
                        'neg': '0.0',
                        'neu': '1.0',
                        'pos': '0.0',
                        'compound': '0.0'
                    }],
                    'dependency_parsing': {
                        'dependency_parent': [[2, 4, 4, 0], [4, 4, 4, 0]],
                        'dependency_label':
                        [['nmod:poss', 'nsubj', 'aux', 'parataxis'],
                         ['obj', 'cop', 'nmod:poss', 'obj']],
                        'pos_tagging': [['PRON', 'NOUN', 'AUX', 'ADJ'],
                                        ['PRON', 'AUX', 'PRON', 'NOUN']]
                    },
                    'npknowledge': {
                        'knowledge': [],
                        'noun_phrase': ['peanutbutter'],
                        'local_noun_phrase': ['peanutbutter']
                    },
                    'topic': [{
                        'text':
                        'my name is peanutbutter what is your name',
                        'topicClass':
                        'Phatic',
                        'confidence':
                        999.0,
                        'topicKeywords': [{
                            'keyword': None,
                            'confidence': 999.0
                        }]
                    }],
                    'intent_classify': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['info_name']
                    },
                    'intent_classify2': [{
                        'sys': [],
                        'topic': [],
                        'lexical': ['info_name']
                    }, {
                        'sys': ['say_bio'],
                        'topic': ['topic_backstory'],
                        'lexical': ['ask_fact', 'ask_name']
                    }],
                    'topic_module':
                    '',
                    'topic_keywords': [{
                        'keyword': None,
                        'confidence': 999.0
                    }],
                    'knowledge': [],
                    'noun_phrase': ['peanutbutter']
                },
                'central_elem': {
                    'senti': 'neu',
                    'regex': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['info_name']
                    },
                    'DA': 'statement',
                    'DA_score': '0.95753586',
                    'DA2': 'NA',
                    'DA2_score': '0.0',
                    'module': ['FOODCHAT'],
                    'np': ['peanutbutter'],
                    'text': 'my name is peanutbutter',
                    'backstory': {
                        'bot_propose_module': '',
                        'confidence': 0.6112542152404785,
                        'followup': '',
                        'module': '',
                        'neg': '',
                        'pos': '',
                        'postfix': '',
                        'question': 'do you like my name',
                        'reason': '',
                        'tag': '',
                        'text': "Yeah, it's a nice name! "
                    }
                },
                'returnnlp': [{
                    'text':
                    'my name is peanutbutter',
                    'dialog_act': ['statement', '0.95753586'],
                    'dialog_act2': ['NA', '0.0'],
                    'intent': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['info_name']
                    },
                    'sentiment': {
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    },
                    'googlekg': [],
                    'noun_phrase': ['peanutbutter'],
                    'topic': {
                        'text': 'my name is peanutbutter',
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': [{
                            'keyword': None,
                            'confidence': 999.0
                        }]
                    },
                    'concept': [{
                        'noun': 'peanutbutter',
                        'data': {
                            'shelf stable filling': '0.6364',
                            'food': '0.2727',
                            'food item': '0.0909'
                        }
                    }],
                    'amazon_topic':
                    None,
                    'dependency_parsing': {
                        'head': [2, 4, 4, 0],
                        'label': ['nmod:poss', 'nsubj', 'aux', 'parataxis']
                    },
                    'pos_tagging': ['PRON', 'NOUN', 'AUX', 'ADJ']
                }, {
                    'text':
                    'what is your name',
                    'dialog_act': ['open_question_factual', '0.57096076'],
                    'dialog_act2': ['NA', '0.0'],
                    'intent': {
                        'sys': ['say_bio'],
                        'topic': ['topic_backstory'],
                        'lexical': ['ask_fact', 'ask_name']
                    },
                    'sentiment': {
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    },
                    'googlekg': [],
                    'noun_phrase': [],
                    'topic': {
                        'text': 'what is your name',
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': []
                    },
                    'concept': [],
                    'amazon_topic':
                    None,
                    'dependency_parsing': {
                        'head': [4, 4, 4, 0],
                        'label': ['obj', 'cop', 'nmod:poss', 'obj']
                    },
                    'pos_tagging': ['PRON', 'AUX', 'PRON', 'NOUN']
                }],
                'a_b_test':
                'B',
                'system_acknowledgement': {
                    'output_tag': '',
                    'ack': ''
                },
                'bot_ask_open_question':
                None,
                'user_id':
                'localtest-user-2a9ee552-1179-4efd-946f-58fb49b99ef5',
                'conversationId':
                None,
                'news_user': {
                    'propose_continue': 'STOP'
                },
                'previous_modules':
                ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                'prev_hash': {
                    'sys_env': 'local'
                },
                'user_profile': {
                    'visit': 1,
                    'dominant_turn_count': 1,
                    'total_turn_count': 2,
                    'username': '<no_name>'
                },
                'template_manager': {
                    'prev_hash': {
                        'PPsGsg': ['fLkfQw'],
                        'ocgSRA': ['VS0VHQ'],
                        'KxoF0w': [],
                        '/M0N7w': ['+ec58w']
                    },
                    'selector_history': [
                        'error::s_fragments/reprompt/thanks_for_sharing',
                        'error::s_fragments/reprompt/feedback',
                        'error::reprompt'
                    ]
                },
                'techsciencechat': {
                    'propose_continue': 'STOP'
                },
                'animalchat': {
                    'propose_continue': 'STOP'
                },
                'fashionchat': {
                    'propose_continue': 'STOP'
                },
                'timechat': {
                    'propose_continue': 'STOP'
                },
                'module_selection': {
                    'used_topic': ['LAUNCHGREETING']
                },
                'topic_to_replace': [],
                'cur_selected_modules':
                'LAUNCHGREETING',
                'foodchat': {
                    'propose_continue': 'STOP'
                },
                'sys_env':
                'local',
                'bookchat_user': {
                    'propose_continue': 'STOP'
                },
                'socialchat': {
                    'curr_state': 's_init',
                    'propose_continue': 'STOP'
                },
                'module_rank': {
                    'MUSICCHAT': 0.0,
                    'GAMECHAT': 0.0,
                    'SPORT': 0.0,
                    'FASHIONCHAT': 0.0,
                    'TECHSCIENCECHAT': 0.0,
                    'TRAVELCHAT': 0.0,
                    'NEWS': 0.0,
                    'BOOKCHAT': 0.0,
                    'ANIMALCHAT': 0.0,
                    'FOODCHAT': 0.0,
                    'MOVIECHAT': 0.0
                },
                'travelchat': {
                    'propose_continue': 'STOP'
                },
                'gamechat': {
                    'propose_continue': 'STOP'
                },
                'weatherchat': {
                    'propose_continue': 'STOP'
                },
                'usr_name':
                '<no_name>',
                'moviechat_user': {
                    'propose_continue': 'STOP'
                },
                'sportchat': {
                    'propose_continue': 'STOP'
                },
                'last_module':
                'LAUNCHGREETING',
                'tedtalkchat': {
                    'propose_continue': 'STOP'
                },
                'outdoorchat': {
                    'propose_continue': 'STOP'
                },
                'launchgreeting': {
                    'volatile': {
                        'is_name_common': False
                    },
                    'persistent_store': {
                        'is_first_module': True
                    },
                    'propose_continue': 'CONTINUE',
                    'states': {
                        'state': 'greet_new_user'
                    }
                },
                'comfortchat': {
                    'propose_continue': 'STOP'
                },
                'holidaychat': {
                    'propose_continue': 'STOP'
                },
                'dailylife': {
                    'propose_continue': 'STOP'
                },
                'musicchat': {
                    'propose_continue': 'STOP'
                },
                'propose_topic':
                None,
                'last_turn_propose_open_question':
                None,
                'social_open_question_proposing':
                None
            },
            'ua_ref': {
                'user_id':
                'localtest-user-2a9ee552-1179-4efd-946f-58fb49b99ef5',
                'map_attributes': {
                    'conversationId':
                    None,
                    'news_user': {
                        'propose_continue': 'STOP'
                    },
                    'previous_modules':
                    ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                    'prev_hash': {
                        'sys_env': 'local'
                    },
                    'user_profile': {
                        'visit': 1,
                        'dominant_turn_count': 1,
                        'total_turn_count': 2,
                        'username': '<no_name>'
                    },
                    'template_manager': {
                        'prev_hash': {
                            'PPsGsg': ['fLkfQw'],
                            'ocgSRA': ['VS0VHQ'],
                            'KxoF0w': [],
                            '/M0N7w': ['+ec58w']
                        },
                        'selector_history': [
                            'error::s_fragments/reprompt/thanks_for_sharing',
                            'error::s_fragments/reprompt/feedback',
                            'error::reprompt'
                        ]
                    },
                    'techsciencechat': {
                        'propose_continue': 'STOP'
                    },
                    'animalchat': {
                        'propose_continue': 'STOP'
                    },
                    'fashionchat': {
                        'propose_continue': 'STOP'
                    },
                    'timechat': {
                        'propose_continue': 'STOP'
                    },
                    'module_selection': {
                        'used_topic': ['LAUNCHGREETING']
                    },
                    'topic_to_replace': [],
                    'cur_selected_modules':
                    'LAUNCHGREETING',
                    'foodchat': {
                        'propose_continue': 'STOP'
                    },
                    'sys_env':
                    'local',
                    'bookchat_user': {
                        'propose_continue': 'STOP'
                    },
                    'socialchat': {
                        'curr_state': 's_init',
                        'propose_continue': 'STOP'
                    },
                    'module_rank': {
                        'MUSICCHAT': 0.0,
                        'GAMECHAT': 0.0,
                        'SPORT': 0.0,
                        'FASHIONCHAT': 0.0,
                        'TECHSCIENCECHAT': 0.0,
                        'TRAVELCHAT': 0.0,
                        'NEWS': 0.0,
                        'BOOKCHAT': 0.0,
                        'ANIMALCHAT': 0.0,
                        'FOODCHAT': 0.0,
                        'MOVIECHAT': 0.0
                    },
                    'travelchat': {
                        'propose_continue': 'STOP'
                    },
                    'gamechat': {
                        'propose_continue': 'STOP'
                    },
                    'weatherchat': {
                        'propose_continue': 'STOP'
                    },
                    'usr_name':
                    '<no_name>',
                    'moviechat_user': {
                        'propose_continue': 'STOP'
                    },
                    'sportchat': {
                        'propose_continue': 'STOP'
                    },
                    'last_module':
                    'LAUNCHGREETING',
                    'tedtalkchat': {
                        'propose_continue': 'STOP'
                    },
                    'outdoorchat': {
                        'propose_continue': 'STOP'
                    },
                    'launchgreeting': {
                        'volatile': {
                            'is_name_common': False
                        },
                        'persistent_store': {
                            'is_first_module': True
                        },
                        'propose_continue': 'CONTINUE',
                        'states': {
                            'state': 'greet_new_user'
                        }
                    },
                    'comfortchat': {
                        'propose_continue': 'STOP'
                    },
                    'holidaychat': {
                        'propose_continue': 'STOP'
                    },
                    'dailylife': {
                        'propose_continue': 'STOP'
                    },
                    'musicchat': {
                        'propose_continue': 'STOP'
                    },
                    'propose_topic':
                    None,
                    'last_turn_propose_open_question':
                    None,
                    'social_open_question_proposing':
                    None
                }
            }
        }
    }, dict(outputs=dict(result=[0, 1, 0, 0], name_uncommon='peanutbutter'))),
    ({
        'inputs': {
            'input_data': {
                'asr': [],
                'text':
                "olivia what's yours",
                'features': {
                    'converttext':
                    "olivia what's yours",
                    'spacynp':
                    None,
                    'central_elem': {
                        'senti': 'neu',
                        'regex': {
                            'sys': [],
                            'topic': [],
                            'lexical': ['ask_back', 'ask_fact']
                        },
                        'DA': 'open_question_factual',
                        'DA_score': '0.915595',
                        'DA2': 'NA',
                        'DA2_score': '0.0',
                        'module': [],
                        'np': [],
                        'text': "what's yours",
                        'backstory': {
                            'bot_propose_module': '',
                            'confidence': 0.7152905464172363,
                            'followup': '',
                            'module': '',
                            'neg': '',
                            'pos': '',
                            'postfix': '',
                            'question': 'what are you',
                            'reason': '',
                            'tag': '',
                            'text': "I'm an Alexa Prize Social Bot."
                        }
                    },
                    'text':
                    "olivia what's yours",
                    'intent_classify': {
                        'sys': [],
                        'topic': [],
                        'lexical': []
                    },
                    'googlekg': [],
                    'returnnlp': [{
                        'text':
                        'olivia',
                        'dialog_act': ['statement', '0.729123'],
                        'dialog_act2': ['NA', '0.0'],
                        'intent': {
                            'sys': [],
                            'topic': [],
                            'lexical': []
                        },
                        'sentiment': {
                            'compound': '0.5',
                            'neg': '0.05',
                            'neu': '0.9',
                            'pos': '0.05'
                        },
                        'googlekg': [],
                        'noun_phrase': ['olivia'],
                        'topic': {
                            'text':
                            'olivia',
                            'topicClass':
                            'Other',
                            'confidence':
                            999.0,
                            'topicKeywords': [{
                                'keyword': 'olivia',
                                'confidence': 999.0
                            }]
                        },
                        'concept': [{
                            'noun': 'olivia',
                            'data': {
                                'digital communication mode': '0.4',
                                'name': '0.3091',
                                'character': '0.2909'
                            }
                        }],
                        'amazon_topic':
                        None,
                        'dependency_parsing': {
                            'head': [0],
                            'label': ['flat']
                        },
                        'pos_tagging': ['NOUN']
                    }, {
                        'text':
                        "what's yours",
                        'dialog_act': ['open_question_factual', '0.915595'],
                        'dialog_act2': ['NA', '0.0'],
                        'intent': {
                            'sys': [],
                            'topic': [],
                            'lexical': ['ask_back', 'ask_fact']
                        },
                        'sentiment': {
                            'compound': '0.5',
                            'neg': '0.05',
                            'neu': '0.9',
                            'pos': '0.05'
                        },
                        'googlekg': [],
                        'noun_phrase': [],
                        'topic': {
                            'text': "what's yours",
                            'topicClass': 'Phatic',
                            'confidence': 999.0,
                            'topicKeywords': []
                        },
                        'concept': [],
                        'amazon_topic':
                        None,
                        'dependency_parsing': {
                            'head': [0, 1],
                            'label': ['case', 'obj']
                        },
                        'pos_tagging': ['VERB', 'PRON']
                    }],
                    'intent':
                    'general',
                    'dialog_act': [{
                        'text': 'olivia',
                        'DA': 'statement',
                        'confidence': 0.729123,
                        'DA2': 'NA',
                        'confidence2': 0.0
                    }, {
                        'text': "what's yours",
                        'DA': 'open_question_factual',
                        'confidence': 0.915595,
                        'DA2': 'NA',
                        'confidence2': 0.0
                    }],
                    'coreference':
                    None,
                    'segmentation': {
                        'segmented_text': ['olivia', "what's yours"],
                        'segmented_text_raw': ['olivia', "what's yours"]
                    },
                    'amazon_topic':
                    None,
                    'concept': [{
                        'noun': 'olivia',
                        'data': {
                            'digital communication mode': '0.4',
                            'name': '0.3091',
                            'character': '0.2909'
                        }
                    }],
                    'nounphrase2': {
                        'noun_phrase': [['olivia'], []],
                        'local_noun_phrase': [['olivia'], []]
                    },
                    'profanity_check':
                    None,
                    'sentiment':
                    None,
                    'sentiment2':
                    None,
                    'topic':
                    None,
                    'topic2': [{
                        'text':
                        'olivia',
                        'topicClass':
                        'Other',
                        'confidence':
                        999.0,
                        'topicKeywords': [{
                            'keyword': 'olivia',
                            'confidence': 999.0
                        }]
                    }, {
                        'text': "what's yours",
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': []
                    }],
                    'intent_classify2': [{
                        'sys': [],
                        'topic': [],
                        'lexical': []
                    }, {
                        'sys': [],
                        'topic': [],
                        'lexical': ['ask_back', 'ask_fact']
                    }],
                    'dependency_parsing': {
                        'dependency_parent': [[0], [0, 1]],
                        'dependency_label': [['flat'], ['case', 'obj']],
                        'pos_tagging': [['NOUN'], ['VERB', 'PRON']]
                    },
                    'sentence_completion_text':
                    "olivia what 's yours",
                    'npknowledge': {
                        'knowledge': [[
                            'Olivia', 'Fictional character',
                            '1230.229248046875', '__EMPTY__'
                        ]],
                        'noun_phrase': ['olivia'],
                        'local_noun_phrase': ['olivia']
                    },
                    'ner':
                    None,
                    'sentiment_allennlp': [{
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    }, {
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    }],
                    'asrcorrection':
                    None,
                    'topic_module':
                    '',
                    'topic_keywords':
                    '',
                    'knowledge': [[
                        'Olivia', 'Fictional character', '1230.229248046875',
                        '__EMPTY__'
                    ]],
                    'noun_phrase': ['olivia']
                },
                'central_elem': {
                    'senti': 'neu',
                    'regex': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['ask_back', 'ask_fact']
                    },
                    'DA': 'open_question_factual',
                    'DA_score': '0.915595',
                    'DA2': 'NA',
                    'DA2_score': '0.0',
                    'module': [],
                    'np': [],
                    'text': "what's yours",
                    'backstory': {
                        'bot_propose_module': '',
                        'confidence': 0.7152905464172363,
                        'followup': '',
                        'module': '',
                        'neg': '',
                        'pos': '',
                        'postfix': '',
                        'question': 'what are you',
                        'reason': '',
                        'tag': '',
                        'text': "I'm an Alexa Prize Social Bot."
                    }
                },
                'returnnlp': [{
                    'text':
                    'olivia',
                    'dialog_act': ['statement', '0.729123'],
                    'dialog_act2': ['NA', '0.0'],
                    'intent': {
                        'sys': [],
                        'topic': [],
                        'lexical': []
                    },
                    'sentiment': {
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    },
                    'googlekg': [],
                    'noun_phrase': ['olivia'],
                    'topic': {
                        'text':
                        'olivia',
                        'topicClass':
                        'Other',
                        'confidence':
                        999.0,
                        'topicKeywords': [{
                            'keyword': 'olivia',
                            'confidence': 999.0
                        }]
                    },
                    'concept': [{
                        'noun': 'olivia',
                        'data': {
                            'digital communication mode': '0.4',
                            'name': '0.3091',
                            'character': '0.2909'
                        }
                    }],
                    'amazon_topic':
                    None,
                    'dependency_parsing': {
                        'head': [0],
                        'label': ['flat']
                    },
                    'pos_tagging': ['NOUN']
                }, {
                    'text':
                    "what's yours",
                    'dialog_act': ['open_question_factual', '0.915595'],
                    'dialog_act2': ['NA', '0.0'],
                    'intent': {
                        'sys': [],
                        'topic': [],
                        'lexical': ['ask_back', 'ask_fact']
                    },
                    'sentiment': {
                        'compound': '0.5',
                        'neg': '0.05',
                        'neu': '0.9',
                        'pos': '0.05'
                    },
                    'googlekg': [],
                    'noun_phrase': [],
                    'topic': {
                        'text': "what's yours",
                        'topicClass': 'Phatic',
                        'confidence': 999.0,
                        'topicKeywords': []
                    },
                    'concept': [],
                    'amazon_topic':
                    None,
                    'dependency_parsing': {
                        'head': [0, 1],
                        'label': ['case', 'obj']
                    },
                    'pos_tagging': ['VERB', 'PRON']
                }],
                'a_b_test':
                'B',
                'system_acknowledgement': {
                    'output_tag': '',
                    'ack': ''
                },
                'bot_ask_open_question':
                None,
                'user_id':
                'localtest-user-de944078-5dee-4f67-8e99-ebc8cca86f1c',
                'conversationId':
                None,
                'news_user': {
                    'propose_continue': 'STOP'
                },
                'previous_modules':
                ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                'prev_hash': {
                    'sys_env': 'local'
                },
                'user_profile': {
                    'visit': 1,
                    'dominant_turn_count': 1,
                    'gender': 'unknown',
                    'total_turn_count': 2,
                    'username': '<no_name>'
                },
                'template_manager': {
                    'prev_hash': {
                        'PPsGsg': ['0Bsr2Q'],
                        'ocgSRA': ['VS0VHQ'],
                        'KxoF0w': [],
                        '/M0N7w': ['aeg3CQ']
                    },
                    'selector_history': [
                        'error::s_fragments/reprompt/thanks_for_sharing',
                        'error::s_fragments/reprompt/feedback',
                        'error::reprompt'
                    ]
                },
                'techsciencechat': {
                    'propose_continue': 'STOP'
                },
                'animalchat': {
                    'propose_continue': 'STOP'
                },
                'fashionchat': {
                    'propose_continue': 'STOP'
                },
                'module_selection': {
                    'module_rank': {
                        'MUSICCHAT': 0.0,
                        'SPORT': 0.0,
                        'GAMECHAT': 0.0,
                        'FASHIONCHAT': 0.0,
                        'TECHSCIENCECHAT': 0.0,
                        'NEWS': 0.0,
                        'TRAVELCHAT': 0.0,
                        'BOOKCHAT': 0.0,
                        'ANIMALCHAT': 0.0,
                        'FOODCHAT': 0.0,
                        'MOVIECHAT': 0.0
                    },
                    'used_topic_modules': ['LAUNCHGREETING']
                },
                'timechat': {
                    'propose_continue': 'STOP'
                },
                'topic_to_replace': [],
                'cur_selected_modules':
                'LAUNCHGREETING',
                'foodchat': {
                    'propose_continue': 'STOP'
                },
                'sys_env':
                'local',
                'bookchat_user': {
                    'propose_continue': 'STOP'
                },
                'socialchat': {
                    'curr_state': 's_init',
                    'propose_continue': 'STOP'
                },
                'travelchat': {
                    'propose_continue': 'STOP'
                },
                'gamechat': {
                    'propose_continue': 'STOP'
                },
                'weatherchat': {
                    'propose_continue': 'STOP'
                },
                'usr_name':
                '<no_name>',
                'moviechat_user': {
                    'propose_continue': 'STOP'
                },
                'sportchat': {
                    'propose_continue': 'STOP'
                },
                'last_module':
                'LAUNCHGREETING',
                'tedtalkchat': {
                    'propose_continue': 'STOP'
                },
                'outdoorchat': {
                    'propose_continue': 'STOP'
                },
                'launchgreeting': {
                    'volatile': {
                        'is_name_common': False
                    },
                    'persistent_store': {
                        'is_first_module': True
                    },
                    'propose_continue': 'CONTINUE',
                    'states': {
                        'state': 'greet_new_user'
                    }
                },
                'comfortchat': {
                    'propose_continue': 'STOP'
                },
                'holidaychat': {
                    'propose_continue': 'STOP'
                },
                'dailylife': {
                    'propose_continue': 'STOP'
                },
                'musicchat': {
                    'propose_continue': 'STOP'
                },
                'propose_topic':
                None,
                'last_turn_propose_open_question':
                None,
                'social_open_question_proposing':
                None
            },
            'ua_ref': {
                'user_id':
                'localtest-user-de944078-5dee-4f67-8e99-ebc8cca86f1c',
                'map_attributes': {
                    'conversationId':
                    None,
                    'news_user': {
                        'propose_continue': 'STOP'
                    },
                    'previous_modules':
                    ['LAUNCHGREETING', '__EMPTY__', '__EMPTY__'],
                    'prev_hash': {
                        'sys_env': 'local'
                    },
                    'user_profile': {
                        'visit': 1,
                        'dominant_turn_count': 1,
                        'gender': 'unknown',
                        'total_turn_count': 2,
                        'username': '<no_name>'
                    },
                    'template_manager': {
                        'prev_hash': {
                            'PPsGsg': ['0Bsr2Q'],
                            'ocgSRA': ['VS0VHQ'],
                            'KxoF0w': [],
                            '/M0N7w': ['aeg3CQ']
                        },
                        'selector_history': [
                            'error::s_fragments/reprompt/thanks_for_sharing',
                            'error::s_fragments/reprompt/feedback',
                            'error::reprompt'
                        ]
                    },
                    'techsciencechat': {
                        'propose_continue': 'STOP'
                    },
                    'animalchat': {
                        'propose_continue': 'STOP'
                    },
                    'fashionchat': {
                        'propose_continue': 'STOP'
                    },
                    'module_selection': {
                        'module_rank': {
                            'MUSICCHAT': 0.0,
                            'SPORT': 0.0,
                            'GAMECHAT': 0.0,
                            'FASHIONCHAT': 0.0,
                            'TECHSCIENCECHAT': 0.0,
                            'NEWS': 0.0,
                            'TRAVELCHAT': 0.0,
                            'BOOKCHAT': 0.0,
                            'ANIMALCHAT': 0.0,
                            'FOODCHAT': 0.0,
                            'MOVIECHAT': 0.0
                        },
                        'used_topic_modules': ['LAUNCHGREETING']
                    },
                    'timechat': {
                        'propose_continue': 'STOP'
                    },
                    'topic_to_replace': [],
                    'cur_selected_modules':
                    'LAUNCHGREETING',
                    'foodchat': {
                        'propose_continue': 'STOP'
                    },
                    'sys_env':
                    'local',
                    'bookchat_user': {
                        'propose_continue': 'STOP'
                    },
                    'socialchat': {
                        'curr_state': 's_init',
                        'propose_continue': 'STOP'
                    },
                    'travelchat': {
                        'propose_continue': 'STOP'
                    },
                    'gamechat': {
                        'propose_continue': 'STOP'
                    },
                    'weatherchat': {
                        'propose_continue': 'STOP'
                    },
                    'usr_name':
                    '<no_name>',
                    'moviechat_user': {
                        'propose_continue': 'STOP'
                    },
                    'sportchat': {
                        'propose_continue': 'STOP'
                    },
                    'last_module':
                    'LAUNCHGREETING',
                    'tedtalkchat': {
                        'propose_continue': 'STOP'
                    },
                    'outdoorchat': {
                        'propose_continue': 'STOP'
                    },
                    'launchgreeting': {
                        'volatile': {
                            'is_name_common': False
                        },
                        'persistent_store': {
                            'is_first_module': True
                        },
                        'propose_continue': 'CONTINUE',
                        'states': {
                            'state': 'greet_new_user'
                        }
                    },
                    'comfortchat': {
                        'propose_continue': 'STOP'
                    },
                    'holidaychat': {
                        'propose_continue': 'STOP'
                    },
                    'dailylife': {
                        'propose_continue': 'STOP'
                    },
                    'musicchat': {
                        'propose_continue': 'STOP'
                    },
                    'propose_topic':
                    None,
                    'last_turn_propose_open_question':
                    None,
                    'social_open_question_proposing':
                    None
                }
            }
        }
    }, dict(outputs=dict(result=[1, 0, 0, 0], name='olivia')))
]
