import pytest
from types import SimpleNamespace

from response_generator.fsm2 import FSMAttributeAdaptor, Tracker
from template_manager import Template
from user_profiler.user_profile import UserProfile

from response_generator.social_transition.launchgreeting.response_generator import LaunchGreetingModule
from response_generator.social_transition.launchgreeting.states import LGState
from response_generator.social_transition.launchgreeting.utils import utils

from test.launchgreeting.data import not_my_name_with_correction


@pytest.mark.parametrize('inputs, outputs', [
    (input_dict['inputs'], output_dict['outputs'])
    for input_dict, output_dict in not_my_name_with_correction.cases
])
def test_name_correction(inputs, outputs, caplog):

    caplog.set_level('ERROR', 'nlu.dataclass.returnnlp')
    caplog.set_level('ERROR', 'nlu.dataclass.central_element')
    caplog.set_level('ERROR', 'nlu.utils.type_checking')
    caplog.set_level('ERROR', 'pytest.input')
    caplog.set_level('WARNING', 'transitions.core')
    caplog.set_level('WARNING', 'template_manager.template')

    ua_ref, input_data = inputs['ua_ref'], inputs['input_data']

    ua_ref = SimpleNamespace(**ua_ref['map_attributes'])
    state_manager = SimpleNamespace(current_state=SimpleNamespace(bot_ask_open_question=None))

    ua = FSMAttributeAdaptor('launchgreeting', ua_ref)
    fsm = LaunchGreetingModule(
        ua,
        input_data,
        Template.greeting,
        LGState,
        first_state='initial',
        state_manager_last_state=None
    )
    fsm.__post_init__(state_manager)

    tracker = Tracker(input_data, ua, None)
    user_model = UserProfile(tracker.user_attributes.ua_ref)

    intent = utils.is_intent_correcting_name(tracker, fsm.state_tracker, user_model)

    context = f"text = {tracker.input_text}"
    assert intent == outputs['intent'], context
    # if outputs.get('username'):
    #     fsm.execute()
    #     assert fsm.user_model.username == outputs['username']
