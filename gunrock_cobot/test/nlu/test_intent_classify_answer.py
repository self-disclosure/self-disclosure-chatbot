import pytest

from nlu.intent_classifier import IntentClassifier

@pytest.mark.parametrize("text", [
    "i'm not sure",
    "not very sure",
    "i don't know",
    "maybe",
    "i have no idea",
    "that's a tough question",
    "it's a hard one",
    "i have no comment",
    "it's hard to say",
    "depends"
])
def test_ans_unknown(text):
    __has_intent(text, "ans_unknown", True)


@pytest.mark.parametrize("text", [
    "probably the end of it",
])
def test_ans_unknown(text):
    __has_intent(text, "ans_unknown", False)


@pytest.mark.parametrize("text", [
    "okay whatever",
    "i don't care",
    "i don't care about it",
    "no i don't really care",
    "sure i really don't care",
    "so i don't care to talk about it",
    "i don't care you know",
    "no i have not watched it i don't care for super hero movies"
])
def test_ans_dont_care(text):
    __has_intent(text, "ans_dont_care", True)


@pytest.mark.parametrize("text", [
    "i like whatever looks good to me",
])
def test_ans_dont_care_false(text):
    __has_intent(text, "ans_dont_care", False)


@pytest.mark.parametrize("text", [
    "yes",
    "yep",
    "yup",
    "why not",
    "please",
    "please do",
    "of course",
    "true",
    "that's true",
    "i am",
    "i do",
    "okay",
    "i think so",
    "correct",
    "if you want",
    "if you want to",
    "if you do like",
    "one hundred percent",
    "uh-huh"
])
def test_ans_positive(text):
    __has_intent(text, "ans_positive", True)


@pytest.mark.parametrize("text", [
    "please help",
    "please don't",
    "i don't think so",
    "if you like fish",
    "not sure"
])
def test_ans_positive_false(text):
    __has_intent(text, "ans_positive", False)


@pytest.mark.parametrize("text", [
    "no",
    "not really",
    "i'm good",
    "it isn't"
    "my name isn't rick",
    "i never read it",
    "i don't have any",
    "i don't watch star wars",
    "i didn't",
    "I have not",
    "never",
    "i've never watched it",
    "never been there",
    "never listen to a concert",
    "i never knew that",
    "i haven't been to any concerts",
    "i haven't watched it out",
    "i haven't",
    "i can't think of any",
    "i don't remember",
    "i cannot recall",
    "hard to think of any",
    "i forgot",
    "i don't like",
    "not interested in movie",
    "disagree",
    "i don't agree"
])
def test_ans_negative(text):
    __has_intent(text, "ans_negative", True)


@pytest.mark.parametrize("text", [
    "i'm a good guy",
    "it's good",
    "never mind",
    "i remember"
])
def test_ans_negative_false(text):
    __has_intent(text, "ans_negative", False)


@pytest.mark.parametrize("text", [
    "interesting",
    "that's adorable",
    "no that's awesome",
    "it's incredible",
    "that's pretty interesting",
    "that's very interesting",
    "it is amazing",
    "amazing",
    "ha",
    "it's a good one",
    "Sounds interesting",
    "you are amazing",
    "that's surprising",
    "i love it",
    "oh i like that",
    "it sounds very impressive",
    "i think that's fantastic",
    "i think you are amazing",
    "oh yeah that's fantastic",
])
def test_opinion_positive(text):
    __has_intent(text, "opinion_positive", True)


@pytest.mark.parametrize("text", [
    "that's not interesting",
    "it's not very funny",
    "a great guy",
    "i'm good",
    "i don't like it",
    "i didn't like that",
    "i'm not sure if it's amazing"
])
def test_opinion_positive_false(text):
    __has_intent(text, "opinion_positive", False)


@pytest.mark.parametrize("text", [
    "it sucks",
    "i think it's terrible",
    "it's awful"
])
def test_opinion_negative(text):
    __has_intent(text, "opinion_negative", True)


@pytest.mark.parametrize("text", [
    "not bad",
])
def test_opinion_negative_false(text):
    __has_intent(text, "opinion_negative", False)


@pytest.mark.parametrize("text", [
    "that's crazy",
    "that's insane",
    "and it was crazy",
    "i think it's pretty crazy",
    "i knew that was crazy it's cause i'm",
])
def test_opinion_crazy(text):
    __has_intent(text, "opinion_crazy", True)


@pytest.mark.parametrize("text", [
    "crazy rich asians",
    "uh kind being so crazy"
])
def test_opinion_crazy_false(text):
    __has_intent(text, "opinion_crazy", False)


@pytest.mark.parametrize("text", [
    "that's very surprising",
    "i'm surprised"
])
def test_opinion_surprizing(text):
    __has_intent(text, "opinion_surprising", True)


@pytest.mark.parametrize("text", [
    "that's not very surprising",
    "i'm not surprised"
])
def test_opinion_surprizing_false(text):
    __has_intent(text, "opinion_surprising", False)


@pytest.mark.parametrize("text", [
    "go for it",
    "continue",
    "go ahead",
    "keep going",
    "let's hear it",
    "if you like",
    "if you want",
    "read it all finish it"
])
def test_ans_continue(text):
    __has_intent(text, "ans_continue", True)


@pytest.mark.parametrize("text", [
    "yeah i think the same i like those genres",
    "oh my god i like exactly the same kinds of movies",
    "sam",
    "yes i like all of those two",
    "i agree with all of those",
    "that's my favorite author too"
    "i'd say the same",
    "i'm the same with you",
    "me too",
    "i agree with you",
    "so do i",
    "i'm with you",
    "i think so too",
    "you are right",
    "makes sense"
])
def test_ans_same(text):
    __has_intent(text, "ans_same", True)


@pytest.mark.parametrize("text", [
    "i don't agree",
    "that doens't make sense"
])
def test_ans_same(text):
    __has_intent(text, "ans_same", False)


@pytest.mark.parametrize("text", [
    "i like movie",
    "i'm more into movie",
    "my favorite book is the hobbit",
    "i'm into fashion",
    "i'm more into fashion",
    "i like book more",
    "i liked it a lot",
    "that's one of my favorites",
    "book is my favorite",
    "i like book more than movie"
])
def test_ans_like(text):
    __has_intent(text, "ans_like", True)


@pytest.mark.parametrize("text", [
    "i don't like movie",
    "i didn't like the joke",
    "i'm not interested in movie",
    "i feel like it's just a dump movie",
    "it looks like a tree",
    "i'm not into fashion"
])
def test_ans_like_false(text):
    __has_intent(text, "ans_like", False)

@pytest.mark.parametrize("text", [
    "i don't like movie",
    "i don't like it",
    "i dislike movies",
    "i hate sports",
    "i'm not interested in movies",
    "i'm very little interested in sports",
    "i don't love to go outside",
    "not interested in music",
    "i’m not a big movie fan",
    "i do not like fashion",
])
def test_ans_dislike_true(text):
    __has_intent(text, "ans_dislike", True)

@pytest.mark.parametrize("text", [
    "i like movie",
    "i really like it",
    "i don't hate movies",
    "i kinda love sports",
    "i'm interested in movies",
    "i don't detest to go outside",
    "i did not hate it",
])
def test_ans_dislike_false(text):
    __has_intent(text, "ans_dislike", False)


def __has_intent(text: str, intent: str, boolean: bool):
    result = IntentClassifier(text).has_intent(intent)
    assert result is boolean
