import pytest

from nlu.intent_classifier import IntentClassifier


def assert_fn(text: str, is_clarify: bool):
    assert (IntentClassifier(text).is_clarify() is not None) == is_clarify


@pytest.mark.parametrize("text, is_clarify", [
    ("actually explain it another time", False),
    ("after tell you another day", False),
    ("all repeat", True),
    ("amazon what you said", True),
    ("and can you just repeat that question", True),
    ("buy it to me", False),
    ("called you repeat that", True),
    ("can you ask the question again", True),
    ("can you buy it", False),
    ("can you explain", False),
    ("can you please repeat that", True),
    ("can you repeat again", True),
    ("can you repeat and slowly", True),
    ("can you repeat it again", True),
    ("can you repeat it please", True),
    ("can you repeat please", True),
    ("can you repeat that fact", True),
    ("can you repeat that please", True),
    ("can you repeat that question", True),
    ("can you repeat that we were hearing it", True),
    ("can you repeat that", True),
    ("can you repeat those two movie names", True),
    ("can you repeat those", True),
    ("can you repeat those again", True),
    ("can you repeat the question", True),
    ("can you repeat what you just said again sorry i fell asleep", True),
    ("can you repeat what you said", True),
    ("can you repeat", True),
    ("can you say it again", True),
    ("can you say that again that was really funny", True),
    ("can you say that again", True),
    ("could you repeat that please", True),
    ("could you repeat", True),
    ("could you say that again please", True),
    ("ask a question", False),
])
def test_is_repeat_1(text: str, is_clarify: bool): assert_fn(text, is_clarify)

@pytest.mark.parametrize("text, is_clarify", [
    ("explain one", False),
    ("explain what part of what i just said", False),
    ("explain what", False),
    ("funny can you repeat that", True),
    ("grow up what you said", False),
    ("how do you spell like a little bit wasn't really explain", False),
    ("i actually wanna hear about your friends can you explain", False),
    ("i can't explain it right now", False),
    ("i can't explain", False),
    ("i can't hear you", True),
    ("i did not hear what you said", True),
    ("i didn't hear you can you repeat that", True),
    ("i didn't i didn't catch the question can you repeat the question", True),
    ("i didn't know that actually so i like what you said", False),
    ("i didn't understand the last part  level eight", False),
    ("i don't get what you just said", False),
    ("i don't know can you repeat it", True),
    ("i don't really have one of them", False),
    ("i don't understand it", False),
    ("i don't understand what you are", False),
    ("i don't understand your question", False),
    ("i gonna say that's not beautiful when you said that you say it again i can't", False),
    ("i just wish to be one again", False),
    ("i like my favorite can you repeat that", True),
    ("i like them all", False),
    ("i see what you mean", False),
    ("i wanna know more about the korgis", False),
    ("i would like to try again", False),
    ("i'm good what did you say your name was i didn't hear it", False),
    ("i'm sorry can you repeat that again", True),
    ("i'm sorry can you repeat the question", True),
    ("i'm sorry say again", True),
    ("i'm sorry what did you just say i missed that", True),
    ("i'm sorry what was that", True),
    ("it's repeating me", False),
])
def test_is_repeat_2(text: str, is_clarify: bool): assert_fn(text, is_clarify)

@pytest.mark.parametrize("text, is_clarify", [
    ("just a question repeat yes", False),
    ("let's try again", True),
    ("no can you repeat that perfume", True),
    ("no can you repeat that", True),
    ("no i can't say it again sorry", False),
    ("no i probably wouldn't watch it again", False),
    ("no say that again", True),
    ("no what is it repeat", True),
    ("no what you said before that", True),
    ("oh what is that", True),
    ("pardon", True),
    ("what are you asking", True),
    ("i didn't catch that", True),
])
def test_is_repeat_3(text: str, is_clarify: bool): assert_fn(text, is_clarify)

@pytest.mark.parametrize("text, is_clarify", [
    ("please repeat your question", True),
    ("please repeat", True),
    ("repeat after me old ladies are gross and ugly", False),
    ("repeat after me", False),
    ("repeat question", True),
    ("repeat that again", True),
    ("repeat that please", True),
    ("repeat that", True),
    ("repeat the name", True),
    ("repeat your question", True),
    ("repeat", True),
    ("say again", True),
    ("say it again repeat the question", True),
    ("say it again", True),
    ("say that again please", True),
    ("say that again", True),
    ("she has a lot of repeating me", False),
    ("sorry repeat that i don't i don't get what you just said can you repeat yourself", True),
    ("sorry repeat that", True),
    ("sorry what was that", True),
    ("talk about well repeat it", True),
    ("try again", True),
])
def test_is_repeat_4(text: str, is_clarify: bool): assert_fn(text, is_clarify)

@pytest.mark.parametrize("text, is_clarify", [
    ("wait repeat what you said again", True),
    ("well i like the guy who lies cool musical can you repeat that", True),
    ("well she's a it's also hard for her to talk as well because she can't hear herself", False),
    ("what can you repeat that", True),
    ("what did you say  repeat", True),
    ("what did you say again what did you say again", True),
    ("what did you say before about", True),
    ("what did you say", True),
    ("what do i like about what can you repeat that", True),
    ("what do you expect me to say to that", False),
    ("what do you mean by club", False),
    ("what do you mean by how was it", False),
    ("what do you mean by smothered", False),
    ("what do you mean percentage points", False),
    ("what do you mean stop", False),
    ("what do you mean that you played fair and square", False),
    ("what do you mean there's so many", False),
    ("what do you mean what", False),
    ("what do you mean you have an opinion about other things", False),
    ("what do you mean", False),
    ("what do you need me to explain", False),
    ("what i couldn't hear you", True),
    ("what is it", False),
    ("what is that", True),
    ("what repeat that again", True),
    ("what repeat that", True),
    ("what repeat what oh yes yes yes yes i am", False),
    ("what repeat", True),
    ("what do you say", True),
    ("what say that again", True),
    ("what was that opening scene", False),
    ("what was that question", True),
    ("what was that", True),
    ("what was the question", True),
    ("what what did you say", True),
    ("what yellow and what", True),
    ("what", True),
    ("when you say like what do you mean", False),
    ("when you say see what do you mean", False),
    ("why do you say again", True),
    ("wow what was that", True),
])
def test_is_repeat_5(text: str, is_clarify: bool): assert_fn(text, is_clarify)

@pytest.mark.parametrize("text, is_clarify", [
    ("yeah i didn't get you can you repeat the question", True),
    ("yeah i said you repeated me", False),
    ("yeah i would i would watch that again", False),
    ("yeah i'm just curious i know you don't get do go to with liquid but can you repeat what you just said", True),
    ("yeah probably would watch that again", False),
    ("yes explain", False),
    ("yes i would watch it again", False),
    ("you say that again please", True),
    ("you say that again", True),
    ("what's that", True),
    ("no go ahead and repeat", True),
    ("just repeat what you said earlier", True),
    ("repeat that again where should we go at boston", False),
    ("repeat what i say autism good normal bad", False),
    ("repeat is a fun place to go", False),
    ("repeat got my attention", False),
    ("no i'm gonna repeat that", False),
    ("go ahead repeat", True),
    ("repeat your ago", False),
    ("you can go ahead and repeat that", True),
    ("yeah go in repeat it", True),
    ("i'm gonna repeat", False),
    ("go ahead and repeat it", True),
    ("good night can you repeat that", True),
    ("go to the movies and the at the school on repeat", False),
    ("it sounds good but can you repeat it", True),
    ("it's go converse repeat", True),
    ("are you going sorry i didn't hear you can you repeat that", True),
    ("times you gonna ask me to repeat the change topic", False),
    ("hey google repeat", True),
    ("repeat what you have said one hour ago", False),
    ("repeat lost city of the monkey god", False),
    ("what did you say", True),
    ("can you repeat that", True),
    ("huh what did you say", True),
    ("repeat that", True),
    ("repeat it", True),
    ("repeat that again would you say", True),
    ("i'm sorry to interrupt repeat yourself again", True),
])
def test_is_repeat_6(text: str, is_clarify: bool): assert_fn(text, is_clarify)

@pytest.mark.parametrize("text, is_clarify", [
    ("good can you repeat my name", False),
    ("i'm good but can you repeat my name to me", False),
    ("can you repeat my name the gotta my", False),
    ("can you repeat my name the got my", False),
    ("please repeat yourself i couldn't hear you", True),
    ("wait can you repeat yourself", True),
    ("yeah can you repeat yourself", True),
    ("no and it didn't to repeat yourself", False),
    ("i told you you're not repeat yourself so don't", False),
    ("no and i didn't tell you to repeat yourself", False),
    ("i told you not to repeat yourself three times", False),
    ("i told you not to repeat herself three times", False),
    ("hi what can you repeat yourself", True),
    ("say what repeat can you repeat yourself", True),
    ("can you repeat yourself sorry", True),
    ("kind of spoke so fast i missed it so we could you repeat yourself", True),
    ("oh repeat yourself what did you say", True),
    ("you just repeat yourself", False),
    ("repeat yourself", True),
    ("can you please repeat yourself", True),
    ("can you repeat yourself", True),
    ("please repeat yourself", True),
    ("repeat yourself please", True),
    ("no no repeat yourself", True),
    ("could you repeat yourself", True),
    ("can you repeat yourself please", True),
    ("what's the question", True),
    ("what's the time", False),
    ("can you repeat it again", True),
    ("say that one more time", True),
    ("again please", True),
    ("repeat those please", True),
    ("huh what did you say", True),
    ("have what", True),
    ("no what is it", False),
])
def test_is_repeat_7(text: str, is_clarify: bool): assert_fn(text, is_clarify)

