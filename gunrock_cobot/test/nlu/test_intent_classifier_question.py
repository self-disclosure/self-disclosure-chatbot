import pytest

from nlu.intent_classifier import IntentClassifier


def assert_fn(text: str, ask_info: bool):
    lexical_intents = IntentClassifier(text).getIntents().get("lexical", [])
    assert ("ask_info" in  lexical_intents) == ask_info


@pytest.mark.parametrize("text, ask_info", [
    ("but which computer has more gpu", True),
    ("which is better than this", False) # this can be both quesiton and a relative clause.

])
def test_ask_info(text: str, ask_info: bool): assert_fn(text, ask_info)
