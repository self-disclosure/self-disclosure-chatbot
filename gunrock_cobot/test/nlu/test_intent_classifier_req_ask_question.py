import pytest

from nlu.intent_classifier import IntentClassifier


def assert_fn(text: str, is_req_ask_question: bool):
    assert (IntentClassifier(text).is_req_ask_question() is not None) == is_req_ask_question


@pytest.mark.parametrize("text, is_req_ask_question", [
    ("can i ask you a question", True),
    ("i didn't know that what did i ask a cat", False),
    ("i have a question", True),
    ("i'm sorry but what did you ask", False),
    ("let me ask you a question", True),
    ("mask", False),
    ("what alexa can i ask you a question", True),
    ("what time ask", False),
    ("alexa answer my question what do you know about the devil", False),
    ("alexa bot i have a question", True),
    ("alexa can i answer the same questions that you ask", False),
    ("alexa can i ask you a question", True),
    ("alexa can you answer my question", True),
    ("alexa can you ask me another question", False),
    ("alexa i don't want to speak about movies anymore i would like you to answer my question", False),
    ("alexa i have a question", True),
    ("alexa i like p. j. mask", False),
    ("alexa what made you ask me that question", False),
    ("are you beating around the bush is there something you really need to ask me", False),
    ("are you getting smarter right now as i ask you questions", False),
    ("are you just gonna say that every time i ask you for something", False),
    ("are you not gonna ask more mine", False),
])
def test_is_req_ask_qn_1(text: str, is_req_ask_question: bool): assert_fn(text, is_req_ask_question)


@pytest.mark.parametrize("text, is_req_ask_question", [
    ("ask", False),
    ("ask about jungle", False),
    ("ask again", False),
    ("ask ball", False),
    ("ask cool", False),
    ("ask follow jungle", False),
    ("ask her", False),
    ("ask her arie", False),
    ("ask lame", False),
    ("ask me again", False),
    ("ask me a question", False),
    ("ask me question", False),
    ("ask questions", False),
    ("ask sheik", False),
    ("ask sports", False),
    ("ask talk", False),
    ("ask that", False),
    ("ask the deck", False),
    ("ask the end", False),
    ("ask wapo", False),
    ("ask women", False),
])
def test_is_req_ask_qn_2(text: str, is_req_ask_question: bool): assert_fn(text, is_req_ask_question)


@pytest.mark.parametrize("text, is_req_ask_question", [
    ("been wondering but we have not had the balls to ask you know", False),
    ("but i have a question", True),
    ("can i ask", True),
    ("can i ask another question", True),
    ("can i ask a question", True),
    ("can i ask you", True),
    ("can i ask you another question", True),
    ("can i ask you a question", True),
    ("can i ask you some questions", True),
    ("can i ask you something", True),
    ("can i ask you something teddy", True),
    ("can you ask me", False),
    ("can you ask me something", False),
    ("can you read ask the question", False),
    ("cause the joker put on a mask", False),
    ("crazy can i ask you a question", True),
])
def test_is_req_ask_qn_3(text: str, is_req_ask_question: bool): assert_fn(text, is_req_ask_question)


@pytest.mark.parametrize("text, is_req_ask_question", [
    ("did i ask", False),
    ("did i ask no", False),
    ("did you alexa did you wanted to ask me something", False),
    ("did you hear my question", False),
    ("don't ask", False),
    ("don't you think cyronic you're so brilliant you're stuck serving humans mindless tasks they ask of you", False),
    ("do you want me to ask you want me do you want to ask me", False),
    ("good can i ask you something", True),
    ("good i have a question", True),
    ("hey alexa i have a question", True),
    ("hey alexa prize social bot can i ask you something", True),
    ("hey can i ask you a question", True),
    ("hey i got a question", True),
    ("hey i need to ask you a question", True),
    ("how do you watch p. j. mask before", False),
])
def test_is_req_ask_qn_4(text: str, is_req_ask_question: bool): assert_fn(text, is_req_ask_question)


@pytest.mark.parametrize("text, is_req_ask_question", [
    ("i can ask my coloring", False),
    ("i can ask you a question", True),
    ("i can't think it out right now i have a question for you can i ask you a question", True),
    ("i could but are you gonna answer my question", False),
    ("i didn't ask", False),
    ("i didn't ask a question", False),
    ("i didn't ask for song", False),
    ("i didn't even ask you that", False),
    ("i don't know but i have a question for you", True),
    ("i don't know what do you think that we should be wearing mask", False),
    ("i don't know what me ask my mom", False),
    ("i don't really know but i wanna talk i wanna ask you something", True),
    ("i don't want to talk about games i want to ask you a question", True),
    ("i feel like i have a question", True),
    ("i find it interesting that you would ask me that", False),
    ("i have another question", True),
    ("i have a question", True),
    ("i have a question for you", True),
    ("i have can i ask you say something", True),
    ("i just didn't ask that", False),
    ("i like strategy games can i ask you something", True),
    ("i like to ask you", True),
    ("i like to ask you questions not you ask me questions", True),
    ("i listen to music when i ask you to", False),
    ("i need you to answer a question", True),
])
def test_is_req_ask_qn_5(text: str, is_req_ask_question: bool): assert_fn(text, is_req_ask_question)


@pytest.mark.parametrize("text, is_req_ask_question", [
    ("i'm afraid to ask", False),
    ("i'm gonna ask you something okay", True),
    ("i'm good i have a question", True),
    ("i'm good i wanted to ask you a question", True),
    ("i'm interested about that dance at you ask", False),
    ("i'm just saying give me time to answer the question", False),
    ("i'm not doing good because you're not answering my question my question", False),
    ("in ask rich", False),
    ("in mask blood connor", False),
    ("i thought you were gonna ask me something", False),
    ("i told you i will ask the questions", True),
    ("it's a really cool app if you ask me", False),
    ("i wanna ask you a question", True),
    ("i was gonna ask you", True),
    ("i was gonna ask you a question", True),
    ("i would i would love to ask you a question", True),
    ("i would like to ask you", True),
    ("i would like to talk about you giving me time to answer my question", False),
])
def test_is_req_ask_qn_6(text: str, is_req_ask_question: bool): assert_fn(text, is_req_ask_question)


@pytest.mark.parametrize("text, is_req_ask_question", [
    ("just ask alexa", False),
    ("keep her to be quiet and answer my question let's play truth or dare", False),
    ("let me ask my mom", False),
    ("let me ask you a question", True),
    ("mask", False),
    ("maye i ask your name", False),
    ("me ask you something", True),
    ("me too can i ask you a question", True),
    ("no but i have a question to ask you", True),
    ("no can i ask you a question", True),
    ("no can i ask you something", True),
    ("no echo i have a question", True),
    ("no i have a question", True),
    ("no i'm okay i have a question for you", True),
    ("no i wanna ask you a question", True),
    ("no i was gonna ask you that", True),
    ("no i would like to ask you questions", True),
    ("no no no hey can i ask you a question", True),
    ("no thanks i wanna know something about the green mask", False),
    ("no thank you but i have a question to ask you", True),
    ("not really nothing but i have a question for you", True),
    ("no we were having a conversation did you hear my question", False),
    ("no you ask me", False),
])
def test_is_req_ask_qn_7(text: str, is_req_ask_question: bool): assert_fn(text, is_req_ask_question)


@pytest.mark.parametrize("text, is_req_ask_question", [
    ("okay alexa i have a question", True),
    ("one second i need to ask my dad", False),
    ("open i have a question for you", True),
    ("play could i ask you a question", True),
    ("play the joker put on mask", False),
    ("please answer my question", False),
    ("princess to ask you something", True),
    ("right let me ask you something", True),
    ("should i ask", False),
    ("so can i ask you a question", True),
    ("sorry what did you ask me", False),
    ("so the greek mask", False),
    ("stop alexa we're just gonna ask siri", False),
    ("that didn't answer my question", False),
    ("that's my question", False),
    ("that's pretty cool i have a question", True),
    ("the mask", False),
    ("the oscar awards they even showed ask me again", False),
    ("the she stops talking when i ask her to", False),
    ("wait can i ask you a question", True),
    ("well i don't really know but i have a question for you", True),
    ("well sorry both alexa can i ask you something", True),
    ("what did you ask me", False),
    ("when you ask me a question", False),
    ("why are you talking when we did not ask you anything", False),
    ("why can't you answer my question you're stupid stupid", False),
    ("why don't you ask a fagget", False),
    ("why do you ask", False),
])
def test_is_req_ask_qn_8(text: str, is_req_ask_question: bool): assert_fn(text, is_req_ask_question)


@pytest.mark.parametrize("text, is_req_ask_question", [
    ("yeah but i have a question for you", True),
    ("yeah i have a question", True),
    ("yeah it is in my question", False),
    ("yeah yes why you ask", False),
    ("yes but i have a question", True),
    ("yes can i ask you another question", True),
    ("yes i can can you can i ask you one question", True),
    ("yes you can ask", False),
    ("you did not ask her", False),
    ("you need to give me time to answer my question", False),
    ("i have a question about you", True),
    ("you didn't answer my question before", False),
    ("i have a question for you social bot", True),
    ("i'd like to ask you a question", True),
    ("will you answer a question for me", True),
    ("can i start with a question", True),
])
def test_is_req_ask_qn_9(text: str, is_req_ask_question: bool): assert_fn(text, is_req_ask_question)
