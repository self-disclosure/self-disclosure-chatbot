import pytest
from nlu.question_detector import QuestionDetector
from unittest.mock import patch

empty_custom_intent = {"sys": [], "topic": [], "lexical": []}


returnnlp_no_question_at_end = [
    {
        "text": "",
        "dialog_act": ["pos_answer", "1.0"],
        "dialog_act2": ["comment", "0.01"],
        "intent": {"sys": [], "topic": [], "lexical": []},
    }
]

returnnlp_no_question_at_end_2 = [
    {
        "text": "",
        "dialog_act": ["commands", "1.0"],
        "dialog_act2": ["comment", "0.01"],
        "intent": {"sys": [], "topic": [], "lexical": []},
    }
]

returnnlp_no_question_at_end_3 = [
    {
        "text": "yes",
        'dialog_act': ['pos_answer', '0.98037225'],
        'dialog_act2': ['NA', '0.0'],
        'intent': {'lexical': ['ans_positive', 'ans_pos'], 'sys': [], 'topic': []}
    },
    {
        'text': "i'm learning how to use lasers to edge different materials",
        'dialog_act': ['statement', '0.7763253'],
        'dialog_act2': ['NA', '0.0'],
        'intent': {'lexical': ['ask_ability', 'ask_leave'], 'sys': [], 'topic': []}
    }
]

returnnlp_question_at_end_one_segment = [
    {
        "text": "",
        "dialog_act": ["open_question_factual", "0.45"],
        "dialog_act2": ["pos_answer", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": []},
    }
]

returnnlp_question_at_end_two_segments = [
    {
        "text": "",
        "dialog_act": ["statement", "0.02"],
        "dialog_act2": ["pos_answer", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": []},
    },
    {
        "text": "",
        "dialog_act": ["open_question_factual", "0.7"],
        "dialog_act2": ["pos_answer", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": []},
    }
]


returnnlp_question_in_first_da_conf_equals_threshold = [
    {
        "text": "",
        "dialog_act": ["open_question_factual", "0.46"],
        "dialog_act2": ["pos_answer", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": []},
    }
]

returnnlp_question_in_first_da_conf_lower_than_threshold = [
    {
        "text": "",
        "dialog_act": ["open_question_factual", "0.34"],
        "dialog_act2": ["pos_answer", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": []},
    }
]

returnnlp_question_in_second_da_conf_equals_threshold = [
    {
        "text": "",
        "dialog_act": ["pos_answer", "1.0"],
        "dialog_act2": ["open_question_factual", "0.21"],
        "intent": {"sys": [], "topic": [], "lexical": []},
    }
]

returnnlp_question_in_second_da_conf_lower_than_threshold = [
    {
        "text": "",
        "dialog_act": ["pos_answer", "1.0"],
        "dialog_act2": ["open_question_factual", "0.19"],
        "intent": {"sys": [], "topic": [], "lexical": []},
    }
]

returnnlp_intent_ask_yesno = [
    {
        "text": "",
        "dialog_act": ["open_question_factual", "0.45"],
        "dialog_act2": ["pos_answer", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": []},
    },
    {
        "text": "",
        "dialog_act": ["statement", "0.7"],
        "dialog_act2": ["pos_answer", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["ask_yesno"]},
    }
]


returnnlp_intent_command = [
    {
        "text": "",
        "dialog_act": ["open_question_factual", "0.7"],
        "dialog_act2": ["pos_answer", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["command"]},
    }
]

returnnlp_why_not = [
    {
        "text": "why not share my feelings with the social bot",
        "dialog_act": ["open_question_opinion", "0.64"],
        "dialog_act2": ["NA", "0.0"],
         "lexical": ["ask_reason", "ans_positive", "ans_pos"], "topic": ["topic_saycomfort"], "sys": ["need_comfort"]
    },
]

@pytest.mark.parametrize("returnnlp, expected_result", [
    (returnnlp_no_question_at_end, False),
    (returnnlp_no_question_at_end_2, False),
    (returnnlp_no_question_at_end_3, False),
    (returnnlp_question_at_end_one_segment, True),
    (returnnlp_question_at_end_two_segments, True),
    (returnnlp_question_in_first_da_conf_equals_threshold, True),
    (returnnlp_question_in_first_da_conf_lower_than_threshold, False),
    (returnnlp_question_in_second_da_conf_equals_threshold, True),
    (returnnlp_question_in_second_da_conf_lower_than_threshold, False),
    (returnnlp_intent_ask_yesno, True),
    (returnnlp_intent_command, True),
    (returnnlp_why_not, False)
])
def test_question_detector_has_question(returnnlp, expected_result):
    question_detector = QuestionDetector("", returnnlp)
    result = question_detector.has_question()
    assert result == expected_result



returnnlp_intent_ask_yesno = [
    {
        "text": "",
        "dialog_act": ["statement", "0.7"],
        "dialog_act2": ["pos_answer", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["ask_yesno"]},
    },
    {
        "text": "",
        "dialog_act": ["open_question_factual", "0.7"],
        "dialog_act2": ["pos_answer", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["ask_back"]},
    },
]


@pytest.mark.parametrize("returnnlp, expected_result", [
    (returnnlp_intent_ask_yesno, True),
])
def test_intent(returnnlp, expected_result):
    question_detector = QuestionDetector("", returnnlp)
    result = question_detector.is_ask_back_question()
    assert result == expected_result


returnnlp_intent_question_regex = [
    {
        "text": "",
        "dialog_act": ["statement", "0.7"],
        "dialog_act2": ["pos_answer", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },
    {
        "text": "",
        "dialog_act": ["open_question_factual", "0.7"],
        "dialog_act2": ["pos_answer", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["ask_yesno"]},
    },
]

returnnlp_intent_question_regex_false = [
    {
        "text": "",
        "dialog_act": ["statement", "0.7"],
        "dialog_act2": ["pos_answer", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["ask_yesno"]},
    },
    {
        "text": "",
        "dialog_act": ["open_question_factual", "0.7"],
        "dialog_act2": ["pos_answer", "1.0"],
        "intent": {"sys": [], "topic": [""], "lexical": ["qqqaskddd"]},
    },
]

@pytest.mark.parametrize("returnnlp, expected_result", [
    (returnnlp_intent_question_regex, True),
    (returnnlp_intent_question_regex_false, False),
    (returnnlp_no_question_at_end_3, False)

])
def test_is_question_by_regex(returnnlp, expected_result):
    question_detector = QuestionDetector("", returnnlp)
    result = question_detector.is_question_by_regex()
    assert result == expected_result


returnnlp_ask_rating = [
    {
        "text": "what's it rated",
        "dialog_act": ["open_question_factual", "0.7"],
        "dialog_act2": ["pos_answer", "1.0"],
        "intent": {"sys": [], "topic": [""], "lexical": ["qqqaskddd"]},
    },
]

@pytest.mark.parametrize("returnnlp, expected_result", [
    (returnnlp_ask_rating, True),

])
def test_is_follow_up_question(returnnlp, expected_result):
    question_detector = QuestionDetector("", returnnlp)
    result = question_detector.is_follow_up_question()
    assert result == expected_result


returnnlp_question_in_command_form_true = [
    {
        "text": "i want to know how many people died from the corona",
    },
]
returnnlp_question_in_command_form_false = [
    {
        "text": "i want to know more about you",
    },
]


@pytest.mark.parametrize("returnnlp, expected_result", [
    (returnnlp_question_in_command_form_true, True),
    (returnnlp_question_in_command_form_false, False),
])
def test_is_question_in_command_form(returnnlp, expected_result):
    question_detector = QuestionDetector(returnnlp[0]['text'], returnnlp)
    result = question_detector.is_question_in_command_form()
    assert result == expected_result

returnnlp_is_not_why_not_true_seg_0 = [
    {
        "text": "why not",
    },
]
returnnlp_is_not_why_not_false_seg_0 = [
    {
        "text": "why do we do this",
    },
]

# returnnlp_is_not_why_not_true_seg_1 = [
#     {
#         "text": "i want to know how many people died from the corona",
#     },
# ]
# returnnlp_is_not_why_not_false_seg_1 = [
#     {
#         "text": "i want to know more about you",
#     },
# ]
# TODO THIS IS GIVING FALSE PREDICTION, MORE TEST NEEDED! 
# @pytest.mark.parametrize("returnnlp, seg, expected_result", [
#     (returnnlp_is_not_why_not_true_seg_0, -1, True),
#     (returnnlp_is_not_why_not_false_seg_0, -1, False),
# ])
# def test_is_not_why_not(returnnlp, seg, expected_result):
#     question_detector = QuestionDetector("", returnnlp)
#     result = question_detector.is_not_why_not(seg)
#     assert result == expected_result


returnnlp_question_by_dialog_act_seg_0 = [

    {
        "text": "",
        "dialog_act": ["open_question_factual", "0.7"],
        "dialog_act2": ["pos_answer", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },

    {
        "text": "",
        "dialog_act": ["statement", "0.7"],
        "dialog_act2": ["open_question_factual", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },
]
returnnlp_question_by_dialog_act_seg_0_false = [
    {
        "text": "",
        "dialog_act": ["comment", "0.7"],
        "dialog_act2": ["pos_answer", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },

    {
        "text": "",
        "dialog_act": ["statement", "0.7"],
        "dialog_act2": ["open_question_factual", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },
]

returnnlp_question_by_dialog_act_seg_1 = [
    {
        "text": "",
        "dialog_act": ["comment", "0.7"],
        "dialog_act2": ["pos_answer", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },

    {
        "text": "",
        "dialog_act": ["statement", "0.7"],
        "dialog_act2": ["open_question_factual", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },
]
returnnlp_question_by_dialog_act_seg_1_false = [
    {
        "text": "",
        "dialog_act": ["comment", "0.7"],
        "dialog_act2": ["open_question_factual", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },

    {
        "text": "",
        "dialog_act": ["statement", "0.7"],
        "dialog_act2": ["comment", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },
]

@pytest.mark.parametrize("returnnlp, seg, expected_result", [
    (returnnlp_question_by_dialog_act_seg_0, 0, True),
    (returnnlp_question_by_dialog_act_seg_0_false, 0, False),
    (returnnlp_question_by_dialog_act_seg_1, 1, True),
    (returnnlp_question_by_dialog_act_seg_1_false, 1, False),
])
def test_seg_question_by_dialog_act(returnnlp, seg, expected_result):
    question_detector = QuestionDetector("", returnnlp)
    result = question_detector.is_question_by_dialog_act(seg)
    assert result == expected_result

# TODO ADD TEST LATER, no architecture changed.
# returnnlp_question_in_command_form_seg_0 = [
#     {
#         "text": "i want to know how many people died from the corona",
#     },
# ]
# returnnlp_question_in_command_form_seg_0_false = [
#     {
#         "text": "i want to know more about you",
#     },
# ]

# returnnlp_question_in_command_form_seg_1_true = [
#     {
#         "text": "i want to know how many people died from the corona",
#     },
# ]
# returnnlp_question_in_command_form_seg_1_false = [
#     {
#         "text": "i want to know more about you",
#     },
# ]

# @pytest.mark.parametrize("returnnlp", "seg", "expected_result", [
#     (returnnlp_question_by_dialog_act_seg_0, 0, True),
#     (returnnlp_question_by_dialog_act_seg_0_false, 0, False),
#     (returnnlp_question_by_dialog_act_seg_1, 1, True),
#     (returnnlp_question_by_dialog_act_seg_1_false, 1, False),
# ])
# def test_is_question_in_command_form(returnnlp, seg, expected_result):
#     question_detector = QuestionDetector("", returnnlp)
#     result = question_detector.is_question_in_command_form(seg)
#     assert result == expected_result

returnnlp_ask_back_question_seg_0 = [
    {
        "text": "",
        "intent": {"sys": [], "topic": [], "lexical": ["ask_back"]},
    },
    {
        "text": "",
        "intent": {"sys": [], "topic": [], "lexical": [""]},
    },
]
returnnlp_ask_back_question_seg_0_false = [
    {
        "text": "",
        "intent": {"sys": [], "topic": [], "lexical": [""]},
    },
    {
        "text": "",
        "intent": {"sys": [], "topic": [], "lexical": ["ask_back"]},
    },
]

returnnlp_ask_back_question_seg_1_true = [
    {
        "text": "",
        "intent": {"sys": [], "topic": [], "lexical": [""]},
    },
    {
        "text": "",
        "intent": {"sys": [], "topic": [], "lexical": ["ask_back"]},
    },
]
returnnlp_ask_back_question_seg_1_false = [
    {
        "text": "",
        "intent": {"sys": [], "topic": [], "lexical": [""]},
    },
    {
        "text": "",
        "intent": {"sys": [], "topic": [], "lexical": [""]},
    },
]

@pytest.mark.parametrize("returnnlp, seg, expected_result", [
    (returnnlp_ask_back_question_seg_0, 0, True),
    (returnnlp_ask_back_question_seg_0_false, 0, False),
    (returnnlp_ask_back_question_seg_1_true, 1, True),
    (returnnlp_ask_back_question_seg_1_false, 1, False),
])
def test_is_ask_back_question(returnnlp, seg, expected_result):
    question_detector = QuestionDetector("", returnnlp)
    result = question_detector.is_ask_back_question(seg)
    assert result == expected_result

#TODO Regex may be weird for followup question
# returnnlp_follow_up_question_seg_0 = [
#     {
#         "text": "when is this rated",
#     },
#     {
#         "text": "not",
#     },
# ]
# returnnlp_follow_up_question_seg_0_false = [
#     {
#         "text": "not",
#     },
#     {
#         "text": "when is this rated",
#     },
# ]

# returnnlp_follow_up_question_seg_1_true = [
#     {
#         "text": "not",
#     },
#     {
#         "text": "when is this rated",
#     },
# ]
# returnnlp_follow_up_question_seg_1_false = [
#     {
#         "text": "not",
#     },
#     {
#         "text": "great",
#     },
# ]

# @pytest.mark.parametrize("returnnlp, seg, expected_result", [
#     (returnnlp_follow_up_question_seg_0, 0, True),
#     (returnnlp_follow_up_question_seg_0_false, 0, False),
#     (returnnlp_follow_up_question_seg_1_true, 1, True),
#     (returnnlp_follow_up_question_seg_1_false, 1, False),
# ])
# def test_is_follow_up_question(returnnlp, seg, expected_result):
#     question_detector = QuestionDetector("", returnnlp)
#     result = question_detector.is_follow_up_question(seg)
#     assert result == expected_result

returnnlp_factual_question_seg_0 = [
    {
        "text": "",
        "dialog_act": ["comment", "0.7"],
        "dialog_act2": ["open_question_factual", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },

    {
        "text": "",
        "dialog_act": ["statement", "0.7"],
        "dialog_act2": ["comment", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },
]
returnnlp_factual_question_seg_0_false = [
    {
        "text": "",
        "dialog_act": ["comment", "0.7"],
        "dialog_act2": ["NA", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },

    {
        "text": "",
        "dialog_act": ["open_question_factual", "0.7"],
        "dialog_act2": ["comment", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },
]

returnnlp_factual_question_seg_1_true = [
    {
        "text": "",
        "dialog_act": ["comment", "0.7"],
        "dialog_act2": ["NA", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },

    {
        "text": "",
        "dialog_act": ["open_question_factual", "0.7"],
        "dialog_act2": ["comment", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },
]
returnnlp_factual_question_seg_1_false = [
    {
        "text": "",
        "dialog_act": ["comment", "0.7"],
        "dialog_act2": ["NA", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },

    {
        "text": "",
        "dialog_act": ["open_question", "0.7"],
        "dialog_act2": ["comment", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },
]

@pytest.mark.parametrize("returnnlp, seg, expected_result", [
    (returnnlp_factual_question_seg_0, 0, True),
    (returnnlp_factual_question_seg_0_false, 0, False),
    (returnnlp_factual_question_seg_1_true, 1, True),
    (returnnlp_factual_question_seg_1_false, 1, False),
])
def test_is_factual_question(returnnlp, seg, expected_result):
    question_detector = QuestionDetector("", returnnlp)
    result = question_detector.is_factual_question(seg)
    assert result == expected_result

returnnlp_opinion_question_seg_0 = [
    {
        "text": "",
        "dialog_act": ["open_question_opinion", "0.7"],
        "dialog_act2": ["NA", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },

    {
        "text": "",
        "dialog_act": ["open_question_factual", "0.7"],
        "dialog_act2": ["comment", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },
]
returnnlp_opinion_question_seg_0_false = [
    {
        "text": "",
        "dialog_act": ["open_question_factual", "0.7"],
        "dialog_act2": ["NA", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },

    {
        "text": "",
        "dialog_act": ["open_question_opinion", "0.7"],
        "dialog_act2": ["comment", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },
]

returnnlp_opinion_question_seg_1_true = [
    {
        "text": "",
        "dialog_act": ["open_question_factual", "0.7"],
        "dialog_act2": ["NA", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },

    {
        "text": "",
        "dialog_act": ["open_question_opinion", "0.7"],
        "dialog_act2": ["comment", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },
]
returnnlp_opinion_question_seg_1_false = [
    {
        "text": "",
        "dialog_act": ["open_question_opinion", "0.7"],
        "dialog_act2": ["NA", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },

    {
        "text": "",
        "dialog_act": ["open_question_factual", "0.7"],
        "dialog_act2": ["comment", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },
]

@pytest.mark.parametrize("returnnlp, seg, expected_result", [
    (returnnlp_opinion_question_seg_0, 0, True),
    (returnnlp_opinion_question_seg_0_false, 0, False),
    (returnnlp_opinion_question_seg_1_true, 1, True),
    (returnnlp_opinion_question_seg_1_false, 1, False),
])
def test_is_opinion_question(returnnlp, seg, expected_result):
    question_detector = QuestionDetector("", returnnlp)
    result = question_detector.is_opinion_question(seg)
    assert result == expected_result

returnnlp_yes_no_question_seg_0 = [
    {
        "text": "",
        "dialog_act": ["yes_no_question", "0.7"],
        "dialog_act2": ["NA", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },

    {
        "text": "",
        "dialog_act": ["open_question_factual", "0.7"],
        "dialog_act2": ["comment", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },
]
returnnlp_yes_no_question_seg_0_false = [
    {
        "text": "",
        "dialog_act": ["NA", "0.7"],
        "dialog_act2": ["NA", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },

    {
        "text": "",
        "dialog_act": ["open_question_factual", "0.7"],
        "dialog_act2": ["yes_no_question", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },
]

returnnlp_yes_no_question_seg_1_true = [
    {
        "text": "",
        "dialog_act": ["NA", "0.7"],
        "dialog_act2": ["NA", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },

    {
        "text": "",
        "dialog_act": ["open_question_factual", "0.7"],
        "dialog_act2": ["yes_no_question", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },
]
returnnlp_yes_no_question_seg_1_false = [
    {
        "text": "",
        "dialog_act": ["yes_no_question", "0.7"],
        "dialog_act2": ["NA", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },

    {
        "text": "",
        "dialog_act": ["open_question_factual", "0.7"],
        "dialog_act2": ["comment", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },
]

@pytest.mark.parametrize("returnnlp, seg, expected_result", [
    (returnnlp_yes_no_question_seg_0, 0, True),
    (returnnlp_yes_no_question_seg_0_false, 0, False),
    (returnnlp_yes_no_question_seg_1_true, 1, True),
    (returnnlp_yes_no_question_seg_1_false, 1, False),
])
def test_is_yes_no_question(returnnlp, seg, expected_result):
    question_detector = QuestionDetector("", returnnlp)
    result = question_detector.is_yes_no_question(seg)
    assert result == expected_result


returnnlp_get_question_da = [
    {
        "text": "",
        "dialog_act": ["open_question_factual", "0.7"],
        "dialog_act2": ["comment", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },
    {
        "text": "",
        "dialog_act": ["commands", "0.7"],
        "dialog_act2": ["comment", "1.0"],
        "intent": {"sys": [], "topic": [], "lexical": ["abcd"]},
    },
]


@pytest.mark.parametrize("returnnlp, seg, expected_result", [
    (returnnlp_get_question_da, 0, ("comment", 1.0)),
    # (returnnlp_get_question_da, 1, ["commands", "comment"]),
])
def test_is_get_question_da(returnnlp, seg, expected_result):
    question_detector = QuestionDetector("", returnnlp)
    result = question_detector.get_question_da(seg)
    # NOT ADDING ASSERT FOR NOW, this test only ensures the call does not throw an error
