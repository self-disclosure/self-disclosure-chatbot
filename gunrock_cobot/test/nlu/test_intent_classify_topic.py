import pytest

from nlu.intent_classifier import IntentClassifier


# topic level
@pytest.mark.parametrize("text, expected_result", [
    ("i'm depressed", True),
    ("i'm depressed how about you", True),
    ("i'm depressed please help", True),
    ("i'm worried", True),
    ("what is yours", False),
    ("what about you", False),
    ("a perfectly fine give me a second", False),
    ("what is a cat", False),
    ("I want you to kill me", True),
    ("I don't want you to kill me", False),
    ("I had a really bad day today", True),
    ("People need to stop", False),
    ("I am worried about myself", True),
    ("i'm sad", True),
    ("i'm not sad", False),
    ("i'm sick", True),
    ("i'm tired", True),
    ("i'm not tired", False),
    ("i have a bad day", True),
    ("i'm doing terrible", True),
    ("no sadly", False),
    ("i'm really sad", True),
    ("wow no it was really sad though", False),
    ("blazing saddles", False),
    ("can I talk about something personal?", True),
    ("No it's something personal", False),
    ("I want to talk about something personal", True),
    ("I don't want to talk about it, it's personal", False),
    ("running is so tiring", False),
    ("are you tired", False),
    ("list us weird got tired of yes", False),
    ("i hate basketball and football let's stop talking about that", False),
    ("people hate me", True),
    ("no you're a bipartisan i hate you", False),
    ("i hate books", False),
    ("i hate it listening to kidz bop why you're talking", False),
    ("i hate them", False),
    ("talk about my feelings", True),
    ("i'm gonna kill myself duh", True),
    ("should i break up with her", True),
    ("I don't want to die", False),
    ("how do i want to die", True),
    ("well no i'm retired", False),
    ("say i hate my boyfriend i hate my boyfriend i'm gonna break up", True),
    ("i'm gonna die", True),
    ("i need help", True),
    ("yeah i'm kinda depressed emoboi", True),
    ("off no plans because i'm alone", False),
    ("no i've been sick really sick", True),
    ("kind of i've been sick", True),
    ("i've not been sick", False),
    ("i'm feeling a little bit down", True),
    ("i am down", False),
    ("i am feeling down today", True),
    ("i'm a little down", True),
    ("continue", False),
    ("my favorite movie is suicide squad", False),
    ("i wanna suicide", True),
    ("i want to cut myself", True),
    ("i hurt my wrist", True),
    ("cut my wrist", True),
    ("my wrist hurts from playing piano", False),
    ("i just want to cut my wrist", True),
    ("yes i'm playing the game and talking to my ugly cousin", False),
    ("i am tired of this movie", False),
    ("i am tired", True),
    ("i am tired today", True),
    ("i am tired of you talking about games", False),
    ("yeah i'm really sorry that your engineers won't buy you want that's really sad", False),
    ("i have a problem with my amazon fire stick tv", False)
])
def test_is_topic_say_comfort(text: str, expected_result: bool):
    __has_intent(text, "topic_saycomfort", expected_result)


@pytest.mark.parametrize("text, expected_result", [
    ("I joke sometimes", False),
    ("I want a joke", True),
    ("my brother told me a joke", False),
    ("give me a funny joke", True),
    ("tell me a funny joke", True),
    ("can you tell me a joke", True),
    ("I want a funny joke please", True)
])
def test_is_topic_say_funny(text: str, expected_result: bool):
    __has_intent(text, "say_funny", expected_result)


need_comfort_test_cases = [
    ("I am feeling pretty horrible", True),
    ("it's horrible", False),
    ("i'm depressed", True),
    ("i'm depressed how about you", True),
    ("i'm depressed please help", True),
    ("i'm worried", True),
    ("what is yours", False),
    ("what about you", False),
    ("a perfectly fine give me a second", False),
    ("what is a cat", False),
    ("I want you to kill me", True),
    ("I don't want you to kill me", False),
    ("I had a really bad day today", True),
    ("People need to stop", False),
    ("I am worried about myself", True),
    ("i'm sad", True),
    ("i'm not sad", False),
    ("i'm sick", True),
    ("i'm tired", True),
    ("i'm not tired", False),
    ("i have a bad day", True),
    ("i'm doing terrible", True),
    ("no sadly", False),
    ("i'm really sad", True),
    ("wow no it was really sad though", False),
    ("blazing saddles", False),
    ("can I talk about something personal?", True),
    ("No it's something personal", False),
    ("I want to talk about something personal", True),
    ("I don't want to talk about it, it's personal", False),
    ("running is so tiring", False),
    ("are you tired", False),
    ("list us weird got tired of yes", False),
    ("i hate basketball and football let's stop talking about that", False),
    ("people hate me", True),
    ("no you're a bipartisan i hate you", False),
    ("i hate books", False),
    ("i hate it listening to kidz bop why you're talking", False),
    ("i hate them", False),
    ("talk about my feelings", True),
    ("i'm gonna kill myself duh", True),
    ("should i break up with her", True),
    ("I don't want to die", False),
    ("how do i want to die", True),
    ("well no i'm retired", False),
    ("say i hate my boyfriend i hate my boyfriend i'm gonna break up", True),
    ("i'm gonna die", True),
    ("i need help", True),
    ("yeah i'm kinda depressed emoboi", True),
    ("off no plans because i'm alone", False),
    ("no i've been sick really sick", True),
    ("kind of i've been sick", True),
    ("i've not been sick", False),
    ("i'm feeling a little bit down", True),
    ("i am down", False),
    ("i am feeling down today", True),
    ("i'm a little down", True),
    ("continue", False),
    ("my favorite movie is suicide squad", False),
    ("i wanna suicide", True),
    ("or they want you back in their life", False)
]
@pytest.mark.parametrize("text, expected_result", need_comfort_test_cases)
def test_is_need_comfort(text: str, expected_result: bool):
    __has_intent(text, "need_comfort", expected_result)


@pytest.mark.parametrize("text", [
    "anime",
])
def test_topic_movietv(text):
    __has_intent(text, "topic_movietv", True)


@pytest.mark.parametrize("text", [
    "baseball",
])
def test_topic_movietv_false(text):
    __has_intent(text, "topic_movietv", False)


@pytest.mark.parametrize("text", [
    "robotics",
    "math"
])
def test_topic_techscience(text):
    __has_intent(text, "topic_techscience", True)


@pytest.mark.parametrize("text", [
    "mathew",
])
def test_topic_techscience_false(text):
    __has_intent(text, "topic_techscience", False)

@pytest.mark.parametrize("text", ["basketball"
])
def test_topic_sport_true(text):
    __has_intent(text, "topic_sport", True)

@pytest.mark.parametrize("text", [
    "what is the gravitational pool between galaxies",
])
def test_topic_sport_false(text):
    __has_intent(text, "topic_sport", False)

@pytest.mark.parametrize("text", [
    "flash briefing",
    "politics",
    "corona virus",
    "climate change"
])
def test_topic_news(text):
    __has_intent(text, "topic_newspolitics", True)


@pytest.mark.parametrize("text", [
    "flash cards",
])
def test_topic_news(text):
    __has_intent(text, "topic_newspolitics", False)


@pytest.mark.parametrize("text", [
    "corona virus",
    "pandemic",
    "coronavirus",
    "virus",
    "corona",
    "covid"
])
def test_topic_coronavirus(text):
    __has_intent(text, "topic_coronavirus", True)


def __has_intent(text: str, intent: str, boolean: bool):
    result = IntentClassifier(text).has_intent(intent)
    assert result is boolean
