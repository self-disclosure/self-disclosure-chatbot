import pytest
import os

from nlu.intent_classifier import IntentClassifier


def get_profanity_test_cases():
    absolute_path = os.path.dirname(os.path.abspath(__file__))
    filepath = absolute_path + "/profanity_test_case.txt"

    with open(filepath, 'r') as f:
        profanity_test_cases = [line.strip() for line in f]

    return profanity_test_cases


@pytest.mark.parametrize("text", get_profanity_test_cases())
def test_is_profane(text: str):
    assert is_profanity(text) is True


@pytest.mark.parametrize("text", [
    "moby dick",
    "sex and the city"
])
def test_is_profane_false(text: str):
    assert is_profanity(text) is False


def is_profanity(text: str):
    return IntentClassifier(text).is_profanity()
