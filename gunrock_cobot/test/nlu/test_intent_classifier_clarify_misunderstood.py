import pytest

from nlu.intent_classifier import IntentClassifier


def assert_confused(text: str, is_hit: bool):
    search = IntentClassifier(text).checkIntent('clarify_misunderstood') is not None
    assert search == is_hit


@pytest.mark.parametrize("text, is_hit", [
    ("can you explain it this is again", True),
    ("can you explain me what is it about", True),
    ("can you explain to me about fractions", True),
    ("can you explain what the movie was about", True),
    ("can you explain what you mean", True),
    ("can you please explain", True),
    ("explain again", True),
    ("explain more please", True),
    ("explain to me about crossdressers", True),
    ("explain to me about", True),
    ("explain to me what you are", True),
    ("explain to me why she can't do it", True),
    ("explain what you mean", True),
    ("explain yourself", True),
    ("explain", True),
    ("i can explain that at all", False),
    ("i can't explain", False),
    ("i can't really explain it that much", False),
    ("i can't really explain it", False),
    ("i can't really explain", False),
    ("i'm not sure how to explain this", False),
    ("no can you explain to me the joke", True),
    ("no would you like to explain it", True),
    ("please explain", True),
    ("really can you explain that please", True),
    ("yeah can you explain more about it", True),
])
def test_is_confused_1(text: str, is_hit: bool): assert_confused(text, is_hit)


@pytest.mark.parametrize("text, is_hit", [
    ("can't understand that", True),
    ("i don't understand any of that", True),
    ("i don't understand anything that you said me", True),
    ("i don't understand the difference", True),
    ("i don't understand what you just said", True),
    ("i don't understand what you said", True),
    ("i don't understand you", True),
    ("i don't understand your question", True),
    ("my apologies i don't understand yes", True),
    ("my apologies i don't understand", True),
    ("please i don't understand", True),
    ("what i don't understand this", True),
    ("yeah i don't understand people either", False),
    ("yeah i don't understand that", False),
])
def test_is_confused_2(text: str, is_hit: bool): assert_confused(text, is_hit)

