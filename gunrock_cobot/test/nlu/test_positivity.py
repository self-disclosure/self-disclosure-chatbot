import pytest

from nlu.constants import Positivity
from nlu.dataclass import ReturnNLP


from_csv = [
    (
        [{"DA2": "commands", "confidence2": 0.834741, "confidence": 0.98858637, "text": "totally not",
          "DA": "neg_answer"},
         {"DA2": "pos_answer", "confidence2": 0.9234615, "confidence": 0.9363799, "text": "yes", "DA": "commands"}],
        'pos'
    ),
    (
        [{"DA2": "commands", "confidence2": 0.97993386, "confidence": 0.99142087, "text": "no", "DA": "neg_answer"},
         {"DA2": "pos_answer", "confidence2": 0.7134569, "confidence": 0.9777004, "text": "yes", "DA": "commands"}],
        'pos'
    ),
    (
        [{"DA2": "pos_answer", "confidence2": 0.70458543, "confidence": 0.86596024, "text": "wow", "DA": "commands"},
         {"DA2": "NA", "confidence2": 0.0, "confidence": 0.87708396, "text": "i didn't know that either do that but no",
          "DA": "neg_answer"},
         {"DA2": "commands", "confidence2": 0.5664503, "confidence": 0.86495996,
          "text": "i wouldn't like another story", "DA": "neg_answer"}],
        'neg'
    ),
]
from_csv = [(
    dict(returnnlp=[
        dict(
            text=segment['text'],
            dialog_act=[segment['DA'], segment['confidence']],
            dialog_act2=[segment['DA2'], segment['confidence2']],
        ) for segment in dialog_act_raw
    ]),
    dict(positivity=expected)
) for dialog_act_raw, expected in from_csv]


@pytest.mark.parametrize('inputs, outputs', [
    (
        dict(returnnlp=[{
            'dialog_act': ['pos_answer', '0.6304426789283752'],
            'dialog_act2': ['commands', '0.3689870536327362'],
        }]),
        dict(positivity='pos')
    ),
    (
        dict(returnnlp=[{
            'dialog_act': ['pos_answer', '0.6304426789283752'],
            'dialog_act2': ['commands', '0.3689870536327362'],
        }, {
            'dialog_act': ['neg_answer', '0.6304426789283752'],
            'dialog_act2': ['commands', '0.3689870536327362'],
        }]),
        dict(positivity='neg')
    ),
    (
        dict(returnnlp=[{
            'dialog_act': ['statement', '0.6304426789283752'],
            'dialog_act2': ['commands', '0.3689870536327362'],
        }, {
            'dialog_act': ['opinion', '0.6304426789283752'],
            'dialog_act2': ['commands', '0.3689870536327362'],
        }]),
        dict(positivity='neu')
    ),

    (
        dict(returnnlp=[
            {'intent': {"sys": [], "topic": [], "lexical": ["ans_positive"]}}
        ]),
        dict(positivity='pos')
    ),
    (
        dict(returnnlp=[
            {'intent': {"sys": [], "topic": [], "lexical": ["ans_negative"]}}
        ]),
        dict(positivity='neg')
    ),
    (
        dict(returnnlp=[
            {'intent': {"sys": [], "topic": [], "lexical": ["opinion_positive"]}}
        ]),
        dict(positivity='neu')
    ),
    (
        dict(returnnlp=[
            {'intent': {"sys": [], "topic": [], "lexical": ["ans_dislike"]}}
        ]),
        dict(positivity='neg')
    ),

] + from_csv)
def test_answer_positivity(inputs, outputs, caplog):

    caplog.set_level('ERROR')

    rnlp = ReturnNLP.from_list(inputs['returnnlp'])

    positivity = rnlp.answer_positivity

    print(rnlp)

    context = (
        "text={}\n"
        "pos_answer: {}, neg_answer: {}\n"
    ).format(
        rnlp.text,
        rnlp.has_dialog_act('pos_answer'), rnlp.has_dialog_act('neg_answer'),
    )
    assert positivity == Positivity[outputs['positivity']], context


@pytest.mark.parametrize('inputs, outputs', [
    (
        dict(returnnlp=[{
            'dialog_act': ['pos_answer', '0.6304426789283752'],
            'dialog_act2': ['commands', '0.3689870536327362'],
        }]),
        dict(positivity='pos')
    ),
    (
        dict(returnnlp=[{
            'dialog_act': ['pos_answer', '0.6304426789283752'],
            'dialog_act2': ['commands', '0.3689870536327362'],
        }, {
            'dialog_act': ['neg_answer', '0.6304426789283752'],
            'dialog_act2': ['commands', '0.3689870536327362'],
        }]),
        dict(positivity='neg')
    ),
    (
        dict(returnnlp=[{
            'dialog_act': ['statement', '0.6304426789283752'],
            'dialog_act2': ['commands', '0.3689870536327362'],
        }, {
            'dialog_act': ['opinion', '0.6304426789283752'],
            'dialog_act2': ['commands', '0.3689870536327362'],
        }]),
        dict(positivity='neg')
    ),

    (
        dict(returnnlp=[
            {'intent': {"sys": [], "topic": [], "lexical": ["ans_positive"]}}
        ]),
        dict(positivity='neu')
    ),
    (
        dict(returnnlp=[
            {'intent': {"sys": [], "topic": [], "lexical": ["opinion_negative"]}}
        ]),
        dict(positivity='neg')
    ),
    (
        dict(returnnlp=[
            {'intent': {"sys": [], "topic": [], "lexical": ["opinion_positive"]}}
        ]),
        dict(positivity='pos')
    ),
    (
        dict(returnnlp=[
            {'intent': {"sys": [], "topic": [], "lexical": ["ans_dislike"]}}
        ]),
        dict(positivity='neu')
    ),

] + from_csv)
def test_opinion_positivity(inputs, outputs, caplog):

    caplog.set_level('ERROR')

    rnlp = ReturnNLP.from_list(inputs['returnnlp'])

    positivity = rnlp.opinion_positivity

    print(rnlp)

    context = (
        "text={}\n"
        "pos_answer: {}, neg_answer: {}\n"
    ).format(
        rnlp.text,
        rnlp.has_dialog_act('pos_answer'), rnlp.has_dialog_act('neg_answer'),
    )
    assert positivity == Positivity[outputs['positivity']], context
