import copy
import difflib
import itertools
import random
import unittest
from copy import deepcopy
from pprint import pprint, PrettyPrinter
import pytest

from nlu.dataclass.returnnlp import ReturnNLP, ReturnNLPSegment
from nlu.dataclass.central_element import CentralElement
from nlu.dataclass.backstory import Backstory
from nlu.constants import DialogAct as DialogActEnum
from nlu.constants import TopicModule, Positivity


pp = PrettyPrinter(width=180)

GGL_KG_ITEM = {"name": "full house", "entity_name": "Full House", "description": "American sitcom",
        "detailed_description": "Full House is an American television sitcom created by Jeff Franklin for ABC",
        "entity_type": ["TVSeries", "CreativeWork", "Thing"],
        "result_score": "651.743408", "topic_module": "movie"}


class TestBackstory:

    def test_backstory(self):
        response = Backstory.from_dict({
            'confidence': 0.9968717694282532,
            'followup': 'What is your favorite kind of pizza? ',
            'module': 'FOODCHAT',
            'bot_propose_module': 'FOODCHAT',
            'neg': '',
            'pos': '',
            'postfix': '',
            'question': 'what is your favorite food',
            'reason': 'I know, I have really bad taste!',
            'tag': 'food',
            'text': "it's a tough question, i like chicken from panda express."
        })

        assert response.confidence == 0.9968717694282532
        assert response.followup == 'What is your favorite kind of pizza? '
        assert response.module == TopicModule.FOOD
        assert response.bot_propose_module == TopicModule.FOOD
        assert response.neg == ""
        assert response.pos == ""
        assert response.postfix == ""
        assert response.question == "what is your favorite food"
        assert response.reason == 'I know, I have really bad taste!'
        assert response.tag == 'food'
        assert response.text == "it's a tough question, i like chicken from panda express."

    def test_backstory_empty_dict(self):
        response = Backstory.from_dict({})

        self.assert_empty_result(response)

    def test_backstory_empty_value(self):
        response = Backstory.from_dict(
            {
                'confidence': 0,
                'followup': '',
                'module': '',
                'bot_propose_module': '',
                'neg': '',
                'pos': '',
                'postfix': '',
                'question': '',
                'reason': '',
                'tag': '',
                'text': ''
            }
        )

        self.assert_empty_result(response)

    def assert_empty_result(self, response):
        assert response.confidence == 0.0
        assert response.followup == ''
        assert response.module is None
        assert response.bot_propose_module is None
        assert response.neg == ""
        assert response.pos == ""
        assert response.postfix == ""
        assert response.question == ""
        assert response.reason == ''
        assert response.tag == ''
        assert response.text == ""


central_elem = {
    'senti': 'neu',
    'regex': {'sys': [], 'topic': [], 'lexical': []},
    'DA': 'statement',
    'DA_score': '1.0',
    'DA2': 'commands',
    'DA2_score': '0.5',
    'module': [],
    'np': ['john'],
    'key_phrase': ['john'],
    'text': 'john',
    'backstory': {
        'confidence': 0.4724680185317993,
        'text': "Sorry, I'm not supposed to tell you. I'm part of a competition, remember?"
    }
}


central_elem_repr = ("CentralElement(text='john', "
                     "dialog_act=CentralElement.DialogAct(name='statement', "
                     "label=<DialogAct.STATEMENT: 'statement'>, score=1.0), "
                     "dialog_acts=[CentralElement.DialogAct(name='statement', "
                     "label=<DialogAct.STATEMENT: 'statement'>, score=1.0), "
                     "CentralElement.DialogAct(name='commands', label=<DialogAct.COMMANDS: "
                     "'commands'>, score=0.5)], "
                     'custom_intents=CentralElement.CustomIntents(sys=[], topic=[], lexical=[]), '
                     "sentiment='neu', noun_phrase=['john'], key_phrase=['john'], topic_modules=[], "
                     'backstory=Backstory(text="Sorry, I\'m not supposed to tell you. I\'m part of '
                     'a competition, remember?", confidence=0.4724680185317993, followup=\'\', '
                     "module=None, bot_propose_module=None, neg='', pos='', postfix='', "
                     "question='', reason='', tag=''))")

returnnlp = [
    {
        "text": "did you ever watch full house?",
        "dialog_act": ["yes_no_question", "1.0"],
        "dialog_act2": ["commands", "0.01"],
        "intent": {"sys": [], "topic": ["topic_newspolitics", "topic_election"], "lexical": ["ask_yesno"]},
        "sentiment": {"neg": "0.0", "neu": 1.0, "pos": "0.0", "compound": "0.0"},
        "googlekg": [[GGL_KG_ITEM]],
        "noun_phrase": ["full house"],
        "keyphrase": ["full house"],
        "topic": {
            "topicClass": "Movies_TV",
            "confidence": 0.656,
            "text": "did you ever watch full house?",
            "topicKeywords": [{"confidence": 0.486, "keyword": "house"}]
        },
        "concept": [{
            "noun": "full house",
            "data": {"show": "0.5789", "hand": "0.2368", "sitcom": "0.1842"}
        }],
        "dependency_parsing": None
    }
]

returnnlp_repr = (
    "[ReturnNLPSegment(text='did you ever watch full house?', dialog_act=['yes_no_question', 1.0], "
    "dialog_act2=['commands', 0.01], dialog_act_label=(<DialogAct.YES_NO_QUESTION: 'yes_no_question'>, 1.0), "
    "dialog_acts=[ReturnNLPSegment.DialogActItem(label=<DialogAct.YES_NO_QUESTION: 'yes_no_question'>, "
    "confidence=1.0), ReturnNLPSegment.DialogActItem(label=<DialogAct.COMMANDS: 'commands'>, confidence=0.01)], "
    "intent=ReturnNLPSegment.Intent(sys=[], topic=['topic_newspolitics', 'topic_election'], "
    "lexical=['ask_yesno']), sentiment=ReturnNLPSegment.Sentiment(neg=0.0, neu=1.0, pos=0.0, "
    "compound=0.0), "

    "amazonkg=[], "

    "googlekg=[ReturnNLPSegment.GoogleKG(name='full house', entity_name='Full House', "
    "description='American sitcom', detailed_description='Full House is an American "
    "television sitcom created by Jeff Franklin for ABC', result_score=651.743408, "
    "topic_module='movie', entity_type=['TVSeries', 'CreativeWork', 'Thing'])], "
    "googlekg_single_result_for_each_key_phrase=[ReturnNLPSegment.GoogleKG(name='full house', entity_name='Full House', "
    "description='American sitcom', detailed_description='Full House is an American "
    "television sitcom created by Jeff Franklin for ABC', result_score=651.743408, "
    "topic_module='movie', entity_type=['TVSeries', 'CreativeWork', 'Thing'])], "

    "noun_phrase=['full house'], key_phrase=['full house'], "
    "topic=ReturnNLPSegment.Topic(topic_class='Movies_TV', "
    "confidence=0.656, text='did you ever watch full house?', "
    "topic_keywords=[ReturnNLPSegment.Topic.TopicKeywords(keyword='house', confidence=0.486)]), "
    "amazon_topic='', "
    "concept=[ReturnNLPSegment.Concept(noun='full house', "
    "data=[ReturnNLPSegment.Concept.ConceptData(description='show', confidence=0.5789), "
    "ReturnNLPSegment.Concept.ConceptData(description='hand', confidence=0.2368), "
    "ReturnNLPSegment.Concept.ConceptData(description='sitcom', confidence=0.1842)])], "
    "dependency_parsing=ReturnNLPSegment.DependencyParsing(head=[], label=[]), pos_tagging=[], generic_ner=[])]"
)

returnnlp_2 = [
    {
        'text': 'i think thats great',
        'dialog_act': ['opinion', '0.9532783627510071'],
        'dialog_act2': ['comment', '0.03289400413632393'],
        'intent': {'sys': [], 'topic': [], 'lexical': ['ans_pos', 'ans_factopinion']},
        'sentiment': {'compound': '0.5', 'neg': '0.1', 'neu': '0.0', 'pos': '0.9'},
        'googlekg': [], 'noun_phrase': [],
        'topic': {'topicClass': 'Literature', 'confidence': 0.927,
                  'text': 'i think thats great',
                  'topicKeywords': [{'confidence': 0.432, 'keyword': 'great'}]},
        'concept': [], 'amazon_topic': 'Phatic',
        'dependency_parsing': {'head': [2, 0, 2, 3], 'label': ['nsubj', 'acl:relcl', 'obj', 'amod']},
        'pos_tagging': ['PRON', 'VERB', 'ADV', 'ADJ']
    },
    {
        'text': 'i agree. do you have a favourite brand',
        'dialog_act': ['yes_no_question', '0.9613189697265625'],
        'dialog_act2': ['opinion', '0.021165674552321434'],
        'intent': {'sys': [], 'topic': ['topic_backstory'], 'lexical': ['ans_pos']},
        'sentiment': {'compound': '0.5', 'neg': '0.05', 'neu': '0.9', 'pos': '0.05'},
        'googlekg': [], 'noun_phrase': ['a favourite brand'],
        'topic': {'topicClass': 'Other', 'confidence': 999.0,
                  'text': 'i agree. do you have a favourite brand',
                  'topicKeywords': [{'confidence': 999.0, 'keyword': 'brand'}]},
        'concept': [{'noun': 'a favourite brand', 'data': {}}], 'amazon_topic': 'Phatic',
        'dependency_parsing': {'head': [5, 5, 5, 5, 0, 8, 8, 5],
                               'label': ['nsubj', 'aux', 'aux', 'nsubj', 'parataxis', 'det', 'amod', 'obj']},
        'pos_tagging': ['PRON', 'ADV', 'AUX', 'PRON', 'VERB', 'DET', 'ADJ', 'NOUN']
    }
]

returnnlp_3 = [
    {
        'text': 'yes',
        'dialog_act': ['pos_answer', '0.6304426789283752'],
        'dialog_act2': ['commands', '0.3689870536327362'],
        'intent': {'sys': [], 'topic': [], 'lexical': ['ans_pos']},
        'sentiment': {'compound': '0.5', 'neg': '0.1', 'neu': '0.0', 'pos': '0.9'},
        'googlekg': [], 'noun_phrase': [],
        'topic': {'topicClass': 'Phatic', 'confidence': 0.999,
                  'text': 'yes',
                  'topicKeywords': []},
        'concept': [], 'amazon_topic': 'Other',
        'dependency_parsing': {'head': [0], 'label': ['discourse']},
        'pos_tagging': ['INTJ']
    },
]

returnnlp_4 = [
    {
        'text': 'i like to walk my dog',
        'dialog_act': ['opinion', '0.8109769'], 'dialog_act2': ['NA', '0.0'],
        'intent': {'sys': [], 'topic': ['topic_animal'], 'lexical': ['ans_like']},
        'sentiment': {'compound': '0.5', 'neg': '0.1', 'neu': '0.0', 'pos': '0.9'},
        'googlekg': [], 'noun_phrase': ['my dog', 'i like to walk my dog'],
        'topic': {
            'topicClass': 'Pets_Animals', 'confidence': 999.0, 'text': 'i like to walk my dog',
            'topicKeywords': [{'confidence': 999.0, 'keyword': 'dog'}]
        },
        'concept': [{'noun': 'my dog', 'data': {}}, {'noun': 'i like to walk my dog', 'data': {}}],
        'amazon_topic': 'Other', 'dependency_parsing': {
            'head': [2, 0, 4, 2, 6, 4], 'label': ['nsubj', 'acl:relcl', 'mark', 'xcomp', 'nmod:poss', 'obj']
        },
        'pos_tagging': ['PRON', 'VERB', 'PART', 'VERB', 'PRON', 'NOUN']
    }, {
        'text': 'what do you like to do', 'dialog_act': ['open_question_opinion', '0.74289346'],
        'dialog_act2': ['NA', '0.0'], 'intent': {
            'sys': [], 'topic': ['topic_backstory', 'req_topic'],
            'lexical': ['ask_hobby']},
        'sentiment': {'compound': '0.5', 'neg': '0.1', 'neu': '0.0', 'pos': '0.9'},
        'googlekg': [], 'noun_phrase': [],
        'topic': {'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'what do you like to do', 'topicKeywords': []},
        'concept': [], 'amazon_topic': 'Other',
        'dependency_parsing': {
            'head': [4, 4, 4, 0, 6, 4], 'label': ['obj', 'aux', 'nsubj', 'acl:relcl', 'mark', 'xcomp']},
        'pos_tagging': ['PRON', 'AUX', 'PRON', 'VERB', 'PART', 'VERB']
    }]

backstory_dicts = [
    ({
        "bot_propose_module": "TECHSCIENCECHAT",
        "confidence": 1.0000001192092896,
        "followup": "",
        "module": "TECHSCIENCECHAT",
        "neg": "",
        "pos": "",
        "postfix": "",
        "question": "tell me the story of you being created",
        "reason": "",
        "tag": "",
        "text": "you know, when mommy and dad love each other very much, they make a robot!"
    }, Backstory(
        text='you know, when mommy and dad love each other very much, they make a robot!',
        confidence=1.0000001192092896,
        followup='', module=TopicModule.TECHSCI, neg='', pos='', postfix='',
        question='tell me the story of you being created', reason='', tag='', bot_propose_module=TopicModule.TECHSCI)
    )
]


class CentralElemTest(unittest.TestCase):

    def test_instantiate(self):
        ce = CentralElement.from_dict(central_elem)
        pprint(ce)
        assert ce.__repr__() == central_elem_repr

    def test_instantiate_nonetype(self):
        ce = CentralElement.from_dict(None)
        pprint(ce)
        assert ce and isinstance(ce, CentralElement)

    def test_selecting_strategy(self):
        central_features = CentralElement.from_dict(dict(custom_intents={"topic": ["topic_profanity"]}))
        pprint(central_features)
        assert central_features

    def test_ablated(self):
        for _ in range(10):
            with self.subTest():
                elem = copy.deepcopy(central_elem)
                keys = random.sample(central_elem.keys(), k=random.randint(1, 3))
                for key in keys:
                    del elem[key]

                ce = CentralElement.from_dict(elem)
                print(ce)
                assert ce

    def test_has_dialog_act(self):
        ce = CentralElement.from_dict(central_elem)
        conditions = [
            (ce.has_dialog_act('statement'), True),
            (ce.has_dialog_act(DialogActEnum.STATEMENT), True),
            (ce.has_dialog_act(DialogActEnum.ABANDONED), False),
            (ce.has_dialog_act({DialogActEnum.ABANDONED, 'commands'}), True),
            (ce.has_dialog_act('statement', 1.1), False),
        ]

        for idx, (condition, result) in enumerate(conditions):
            with self.subTest():
                print(idx)
                assert condition == result

    def test_backstory(self):
        for d, ce in backstory_dicts:
            with self.subTest():
                backstory = Backstory.from_dict(d)
                print(backstory)
                assert repr(backstory) == repr(ce)


class ReturnNLPTest(unittest.TestCase):

    def test_instantiate(self):

        def driver(rnlp):
            # rnlp = [ReturnNLPSegment.from_dict(r) for r in returnnlp]
            print(f"rnlp=\n{rnlp}")
            expected = returnnlp_repr
            result = rnlp.__repr__()
            s = difflib.SequenceMatcher(None, expected, result)
            matches = [s for s in s.get_matching_blocks()]
            print(matches)
            pp.pprint(list(expected[a: a + n] for a, b, n in matches))

            assert expected == result

        for r in [ReturnNLP(returnnlp), ReturnNLP.from_list(returnnlp)]:
            with self.subTest():
                driver(r)

    def test_instantiate_nonetype(self):
        rnlp = ReturnNLP.from_list(None)
        pp.pprint(rnlp)
        assert rnlp is not None and isinstance(rnlp, ReturnNLP)

    def test_instantiate_some_segment_none(self):
        rnlp = ReturnNLP.from_list(returnnlp + [None])
        pp.pprint(rnlp)
        assert rnlp is not None and isinstance(rnlp, ReturnNLP)

    def test_ablated(self):
        for _ in range(10):
            with self.subTest():
                rnlp = copy.deepcopy(returnnlp)
                for r in rnlp:
                    keys = random.sample(r.keys(), k=random.randint(1, 3))
                    for key in keys:
                        del r[key]

                rnlp = [ReturnNLPSegment.from_dict(r) for r in returnnlp]
                print(rnlp)
                assert rnlp

    def test_float_cast(self):
        rnlp_dict = deepcopy(returnnlp)
        rnlp_dict[0]['dialog_act'][1] = None
        rnlp = ReturnNLP.from_list(rnlp_dict)
        print(rnlp)
        print(rnlp[0].dialog_act)
        assert rnlp

        # rnlp_dict = deepcopy(returnnlp)
        # rnlp_dict[0]['dialog_act'][1] = 'no'
        # rnlp = ReturnNLP.from_list(rnlp_dict)
        # print(rnlp)
        # assert rnlp and False

    # def test_has_dialog_act(self):
    #     rnlp = ReturnNLP.from_list(returnnlp)
    #     rnlp2 = ReturnNLP.from_list(returnnlp_2)
    #     rnlp3 = ReturnNLP.from_list(returnnlp_3)
    #     rnlp4 = ReturnNLP.from_list(returnnlp_4)
    #     conditions = [
    #         (rnlp.has_dialog_act('yes_no_question'), True),
    #         (rnlp.has_dialog_act(DialogActEnum.YES_NO_QUESTION), True),
    #         (rnlp.has_dialog_act('pos_answer'), False),
    #         (rnlp.has_dialog_act([DialogActEnum.POS_ANSWER], threshold=0.0001), False),
    #         (rnlp.has_dialog_act([DialogActEnum.YES_NO_QUESTION, DialogActEnum.COMMANDS], threshold=0.0001), True),

    #         (rnlp.has_dialog_act([DialogActEnum.COMMANDS], threshold=0.0001), False),
    #         (rnlp.has_dialog_act([DialogActEnum.OPENING]), False),
    #         (rnlp.has_dialog_act(['yes_no_question', 'statement', 'opening']), True),
    #         (rnlp.has_dialog_act([DialogActEnum.YES_NO_QUESTION, DialogActEnum.APOLOGY]), True),
    #         (rnlp.has_dialog_act(['pos_answer', 'neg_answer']), False),

    #         (rnlp.has_dialog_act([DialogActEnum.POS_ANSWER, DialogActEnum.NEG_ANSWER]), False),
    #         (rnlp.has_dialog_act(['pos_answer', DialogActEnum.YES_NO_QUESTION]), True),
    #         (rnlp.has_dialog_act(DialogActEnum.YES_NO_QUESTION, threshold=1.1), False),
    #         (rnlp.has_dialog_act(DialogActEnum.YES_NO_QUESTION, index=0), True),

    #         (rnlp2.has_dialog_act(DialogActEnum.YES_NO_QUESTION), True),
    #         (rnlp2.has_dialog_act(DialogActEnum.COMMENT), False),  # temp condition before nlu fixes da2 threshold
    #         (rnlp2[-1].has_dialog_act(DialogActEnum.YES_NO_QUESTION), True),
    #         (rnlp2[-1:].has_dialog_act(DialogActEnum.YES_NO_QUESTION), True),
    #         (rnlp2[-1:].has_dialog_act(DialogActEnum.COMMENT), False),

    #         (rnlp3.has_dialog_act([DialogActEnum.OPEN_QUESTION_FACTUAL, DialogActEnum.COMMANDS]), False),

    #         (rnlp4[0].has_dialog_act({DialogActEnum.OPINION, DialogActEnum.STATEMENT}), True),
    #         (rnlp4[0].has_dialog_act({'opinion', 'statement'}), True)
    #     ]

    #     for idx, (condition, result) in enumerate(conditions):
    #         with self.subTest():
    #             print(idx + 1, condition)
    #             assert condition == result

    def test_convenient_iterators(self):
        rnlp = ReturnNLP(returnnlp)
        expected = {
            'text': ['did you ever watch full house?'],
            'dialog_act_label': [(DialogActEnum.YES_NO_QUESTION, 1.0)],
            'intent': [ReturnNLPSegment.Intent(sys=[],
                                               topic=['topic_newspolitics', 'topic_election'],
                                               lexical=['ask_yesno'])],
            'sys_intent': [[]],
            'topic_intent': [['topic_newspolitics', 'topic_election']],
            'lexical_intent': [['ask_yesno']],
            'sentiment': [ReturnNLPSegment.Sentiment(0.0, 1.0, 0.0, 0.0)],
            'googlekg': [[ReturnNLPSegment.GoogleKG(name='full house',
                                                    entity_name='Full House',
                                                    description='American sitcom',
                                                    detailed_description='Full House is an American television sitcom created by Jeff Franklin for ABC',
                                                    entity_type=['TVSeries', 'CreativeWork', 'Thing'],
                                                    result_score=651.743408,
                                                    topic_module='movie')]],
            'noun_phrase': [['full house']],
            'topic': [ReturnNLPSegment.Topic(topic_class='Movies_TV',
                                             confidence=0.656,
                                             text='did you ever watch full house?',
                                             topic_keywords=[
                                                 ReturnNLPSegment.Topic.TopicKeywords.from_dict(
                                                     dict(keyword='house',
                                                          confidence=0.486)
                                                 )
                                             ])],
            'amaon_topic': "unknown",
            'concept': [[ReturnNLPSegment.Concept(noun='full house',
                                                  data=[
                                                      ReturnNLPSegment.Concept.ConceptData.from_dict(
                                                          dict(description='show',
                                                               confidence=0.5789)),
                                                      ReturnNLPSegment.Concept.ConceptData.from_dict(
                                                          dict(description='hand',
                                                               confidence=0.2368)),
                                                      ReturnNLPSegment.Concept.ConceptData.from_dict(
                                                          dict(description='sitcom',
                                                               confidence=0.1842)),
                                                  ])]],
            'pos_tagging': {},
        }

        conditions = (
            rnlp.text == expected['text'],
            rnlp.dialog_act_label == expected['dialog_act_label'],
            rnlp.intent == expected['intent'],
            rnlp.sys_intent == expected['sys_intent'],
            rnlp.topic_intent == expected['topic_intent'],

            rnlp.lexical_intent == expected['lexical_intent'],
            rnlp.sentiment == expected['sentiment'],
            rnlp.googlekg == expected['googlekg'],
            rnlp.noun_phrase == expected['noun_phrase'],
            rnlp.topic == expected['topic'],

            rnlp.concept == expected['concept'],

            rnlp.flattened_sys_intent == list(itertools.chain.from_iterable(expected['sys_intent'])),
            rnlp.flattened_topic_intent == list(itertools.chain.from_iterable(expected['topic_intent'])),
            rnlp.flattened_lexical_intent == list(itertools.chain.from_iterable(expected['lexical_intent'])),
        )

        print(pp.pformat(rnlp.text), pp.pformat(expected['text']), sep='\n')
        print(pp.pformat(rnlp.sys_intent), pp.pformat(expected['sys_intent']), sep='\n')
        print(pp.pformat(rnlp.sentiment), pp.pformat(expected['sentiment']), sep='\n')
        print(pp.pformat(rnlp.topic), pp.pformat(expected['topic']), sep='\n')
        print(pp.pformat(rnlp.googlekg), pp.pformat(expected['googlekg']), sep='\n')
        print(pp.pformat(rnlp.concept), pp.pformat(expected['concept']), sep='\n')

        for idx, c in enumerate(conditions):
            with self.subTest():
                print(idx + 1, c)
                assert c

    def test_topic_intent_modules(self):
        rnlp = ReturnNLP(returnnlp)
        result = rnlp.topic_intent_modules

        assert result == {TopicModule.NEWS}

    def test_is_incomplete_utterance(self):
        rnlp = ReturnNLP.from_list(returnnlp)
        assert rnlp.is_incomplete_utterance() is False


@pytest.mark.parametrize(('rnlp', 'index'), [
    (ReturnNLP.from_list(None), 1),
    (ReturnNLP.from_list(None), -1),
    (ReturnNLP.from_list(None), slice(0, 1, None)),
    (ReturnNLP.from_list(returnnlp + [None]), 1),
    (ReturnNLP.from_list(returnnlp + [None]), -1),
    (ReturnNLP.from_list(returnnlp + [None]), slice(0, 1)),
])
def test_getitem(rnlp, index):
    result = rnlp[index]
    print(f"result: {type(result)}, \n{result}\n")
    assert result is not None and isinstance(result, (ReturnNLP, ReturnNLPSegment))


@pytest.mark.parametrize("returnnlp, expected_result", [
    ([
        {
            "text": "i'd like to",
            "dialog_act": [DialogActEnum.ABANDONED, "1.0"],
            "dialog_act2": ["None", "None"],
        }
    ], True),
    ([
        {
            "text": "no",
            "dialog_act": [DialogActEnum.NEG_ANSWER, "1.0"],
            "dialog_act2": ["None", "None"],
        },
        {
            "text": "but i'd like to",
            "dialog_act": [DialogActEnum.ABANDONED, "1.0"],
            "dialog_act2": ["None", "None"],
        }
    ], False),
    ([
        {
            "text": "okay",
            "dialog_act": [DialogActEnum.POS_ANSWER, "1.0"],
            "dialog_act2": ["None", "None"],
        }
    ], False),
    ([
        {
            "text": "no but i want to",
            "dialog_act": [DialogActEnum.ABANDONED, "1.0"],
            "dialog_act2": ["None", "None"],
        }
    ], False),
    ([
        {
            "text": "very",
            "dialog_act": [DialogActEnum.ABANDONED, "1.0"],
            "dialog_act2": ["None", "None"],
        }
    ], False),
])
def test_is_incomplete(returnnlp: list, expected_result: bool):
    rnlp = ReturnNLP(returnnlp)
    assert rnlp.is_incomplete_utterance() is expected_result


@pytest.mark.parametrize("returnnlp, expected_result", [
    ([
        {
            "text": "wait",
            "dialog_act": [DialogActEnum.HOLD, "4.0"],
            "dialog_act2": ["None", "None"],
        }
    ], True),
])
def test_is_hesitant(returnnlp: list, expected_result: bool):
    rnlp = ReturnNLP(returnnlp)
    assert rnlp.is_hesitant() is expected_result


@pytest.mark.parametrize("returnnlp, expected_result", [
    ([
         {
             "text": "well",
             "intent": []
         },
         {
             "text": "i don't like action movie",
             "intent": {"sys": [], "topic": [], "lexical": ["ans_dislike"]},
         },
         {
             "text": "i like action movie",
             "intent": {"sys": [], "topic": [], "lexical": ["ans_like"]},
         }
     ],
     "well i like action movie"
    )
])
def test_text_excluding_dislike_segments(returnnlp, expected_result):
    rnlp = ReturnNLP(returnnlp)
    assert rnlp.text_excluding_dislike_segments == "well i like action movie"


class TestReturnNLPSegment:

    @pytest.mark.parametrize("returnnlp_segment, expected_results", [
        (
            {
                "intent": {"sys": [], "topic": ["topic_weather"], "lexical": []},
                'amazon_topic': "Entertainment_Books",
                "concept": [{
                    "noun": "fortnight",
                    "data": {"game": "0.5789"}
                }],
                "googlekg": [[GGL_KG_ITEM]],
                'topic': {'topicClass': 'Fashion', 'confidence': 0.927,
                          'text': 'i think thats great',
                          'topicKeywords': [{'confidence': 0.80, 'keyword': 'great'}]},
                "dialog_act": [DialogActEnum.STATEMENT, "1.0"],
                "dialog_act2": [DialogActEnum.NA, "None"],
            },
            [TopicModule.WEATHER.value, TopicModule.BOOK.value, TopicModule.GAME.value]
        ),
        (
            {
                "intent": {"sys": [], "topic": ["topic_weather"], "lexical": []},
                'amazon_topic': "Entertainment_Books",
                "googlekg": [[{"name": "full house", "entity_name": "Full House", "description": "American sitcom",
        "detailed_description": "Full House is an American television sitcom created by Jeff Franklin for ABC",
        "entity_type": ["TVSeries", "CreativeWork", "Thing"],
        "result_score": "1001", "topic_module": "movie"}]],
                'topic': {'topicClass': 'Fashion', 'confidence': 0.927,
                          'text': 'i think thats great',
                          'topicKeywords': [{'confidence': 0.79, 'keyword': 'great'}]},
                "dialog_act": [DialogActEnum.STATEMENT, "1.0"],
                "dialog_act2": [DialogActEnum.NA, "None"],
            },
            [TopicModule.WEATHER.value, TopicModule.BOOK.value, TopicModule.MOVIE.value]
        ),
    ])
    def test_detect_topic(self, returnnlp_segment, expected_results):
        returnnlp_segment = ReturnNLPSegment.from_dict(returnnlp_segment)
        detected_topic_modules = returnnlp_segment.detect_topic_module_from_system_level(
            consider_amazon_topic_2019=True)

        assert detected_topic_modules == expected_results


class TestSentiment:

    @pytest.mark.parametrize('sentiment, query, expected', [
        (ReturnNLPSegment.Sentiment(pos=0.9, neu=0.5, neg=0.1, compound=0.5), 'pos', True),
        (ReturnNLPSegment.Sentiment(pos=0.9, neu=0.5, neg=0.1, compound=0.5), Positivity.pos, True),
    ])
    def test_sentiment(self, sentiment: ReturnNLPSegment.Sentiment, query, expected: bool):
        value = sentiment == query
        pp.pprint(value)
        assert value is expected
