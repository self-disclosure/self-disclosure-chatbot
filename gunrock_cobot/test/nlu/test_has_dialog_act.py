import pytest

from nlu.constants import DialogAct as DialogActEnum
from nlu.dataclass import ReturnNLP


@pytest.mark.parametrize('inputs, outputs', [
    (dict(_test_case_index=idx, **returnnlp), dict(condition=condition))
    for idx, (returnnlp, conditions) in enumerate([
        (
            dict(
                returnnlp=[
                    dict(
                        text="did you ever watch full house?",
                        dialog_act=["yes_no_question", "1.0"],
                        dialog_act2=["commands", "0.01"],
                    ),
                ]
            ),
            dict(
                conditions=[
                    dict(dialog_act='yes_no_question', expected=True),
                    dict(dialog_act=DialogActEnum.YES_NO_QUESTION, expected=True),
                    dict(dialog_act='pos_answer', expected=False),
                    dict(dialog_act=[DialogActEnum.POS_ANSWER], threshold=0.0001, expected=False),
                    dict(dialog_act=[DialogActEnum.YES_NO_QUESTION, DialogActEnum.COMMANDS], threshold=0.0001,
                         expected=True),

                    dict(dialog_act=[DialogActEnum.COMMANDS], threshold=0.0001, expected=False),
                    dict(dialog_act=[DialogActEnum.OPENING], expected=False),
                    dict(dialog_act=['yes_no_question', 'statement', 'opening'], expected=True),
                    dict(dialog_act=[DialogActEnum.YES_NO_QUESTION, DialogActEnum.APOLOGY], expected=True),
                    dict(dialog_act=['pos_answer', 'neg_answer'], expected=False),

                    dict(dialog_act=[DialogActEnum.POS_ANSWER, DialogActEnum.NEG_ANSWER], expected=False),
                    dict(dialog_act=['pos_answer', DialogActEnum.YES_NO_QUESTION], expected=True),
                    dict(dialog_act=DialogActEnum.YES_NO_QUESTION, threshold=1.1, expected=False),
                    dict(dialog_act=DialogActEnum.YES_NO_QUESTION, index=0, expected=True),
                ]
            )
        ),
        (
            dict(
                returnnlp=[
                    dict(
                        text='i think thats great',
                        dialog_act=['opinion', '0.9532783627510071'],
                        dialog_act2=['comment', '0.03289400413632393'],
                    ),
                    dict(
                        text='i agree. do you have a favourite brand',
                        dialog_act=['yes_no_question', '0.9613189697265625'],
                        dialog_act2=['opinion', '0.021165674552321434'],
                    ),
                ]
            ),
            dict(
                conditions=[
                    dict(dialog_act=DialogActEnum.YES_NO_QUESTION, expected=True),
                    dict(dialog_act=DialogActEnum.COMMENT, expected=False),
                    dict(slice=(-1), dialog_act=DialogActEnum.YES_NO_QUESTION, expected=True),
                    dict(slice=(-1, None), dialog_act=DialogActEnum.YES_NO_QUESTION, expected=True),
                    dict(slice=(-1, None), dialog_act=DialogActEnum.COMMENT, expected=False),
                ]
            )
        ),
        (
            dict(
                returnnlp=[
                    dict(
                        text='yes',
                        dialog_act=['pos_answer', '0.6304426789283752'],
                        dialog_act2=['commands', '0.3689870536327362'],
                    ),
                ]
            ),
            dict(
                conditions=[
                    dict(dialog_act=[DialogActEnum.OPEN_QUESTION_FACTUAL, DialogActEnum.COMMANDS], expected=True),
                ]
            )
        ),
        (
            dict(
                returnnlp=[
                    dict(
                        text='i like to walk my dog',
                        dialog_act=['opinion', '0.8109769'],
                        dialog_act2=['NA', '0.0'],
                    ),
                    dict(
                        text='what do you like to do',
                        dialog_act=['open_question_opinion', '0.74289346'],
                        dialog_act2=['NA', '0.0']
                    ),
                ]
            ),
            dict(
                conditions=[
                    dict(slice=(0), dialog_act={DialogActEnum.OPINION, DialogActEnum.STATEMENT}, expected=True),
                    dict(slice=(0), dialog_act={'opinion', 'statement'}, expected=True)
                ]
            )
        ),
    ]) for condition in conditions['conditions']
])
def test_has_dialog_act(inputs, outputs, caplog):

    caplog.set_level('ERROR', 'nlu.type_check')

    returnnlp = ReturnNLP.from_list(inputs['returnnlp'])
    condition = outputs['condition']
    expected = condition['expected']

    if 'slice' in returnnlp:
        slice_ = returnnlp['slice']
        if isinstance(slice_, tuple) and len(slice_) == 2 and all(slice_):
            rnlp = returnnlp[slice_[0]:slice_[1]]
        elif isinstance(slice_, tuple) and len(slice_) == 2 and slice_[1] is None:
            rnlp = returnnlp[slice_[0]:]
        elif isinstance(slice_, tuple) and len(slice_) == 2 and slice_[0] is None:
            rnlp = returnnlp[:slice_[0]]
        elif isinstance(slice_, int):
            rnlp = returnnlp[slice_]
        else:
            rnlp = returnnlp
    else:
        rnlp = returnnlp

    if 'threshold' in condition:
        has_dialog_act = rnlp.has_dialog_act(dialog_act=condition['dialog_act'], threshold=condition['threshold'])
    else:
        has_dialog_act = rnlp.has_dialog_act(dialog_act=condition['dialog_act'])

    context = (
        "test_index = {}; text = {}; condition = {}"
    ).format(
        inputs['_test_case_index'], returnnlp.text, condition
    )

    assert has_dialog_act == expected, context
