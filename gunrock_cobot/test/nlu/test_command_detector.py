import pytest
from nlu.command_detector import CommandDetector
from unittest.mock import patch
from nlu.constants import DialogAct as DialogActEnum

empty_custom_intent = {"sys": [], "topic": [], "lexical": []}

returnnlp_input = [
    {
        "text": "can we talk about sport",
        "dialog_act": ["open_question_factual", "0.7"],
        "dialog_act2": ["pos_answer", "1.0"],
        "intent": {"sys": [], "topic": [""], "lexical": ["command"]},
    },
]

returnnlp_input_2 = [
    {
        "text": "can we talk about sport",
        "dialog_act": ["open_question_factual", "0.7"],
        "dialog_act2": [DialogActEnum.COMMANDS.value, "1.0"],
        "intent": {"sys": [], "topic": [""], "lexical": ["command"]},
    },
]

returnnlp_input_3 = [
    {
        "text": "talk about sport",
        "dialog_act": ["", "0.7"],
        "dialog_act2": [DialogActEnum.COMMANDS.value, "1.0"],
        "intent": {"sys": [], "topic": [""], "lexical": [""]},
    },
]
@pytest.mark.parametrize("returnnlp, expected_result", [
    (returnnlp_input, True),
    (returnnlp_input_2, True),
    (returnnlp_input_3, True),
])
def test_is_command(returnnlp, expected_result):
    command_detector = CommandDetector("", returnnlp)
    result = command_detector.has_command()
    assert result is expected_result


returnnlp_input_not_command = [
    {
        "text": "yeah",
        "dialog_act": ["open_question_factual", "0.7"],
        "dialog_act2": [DialogActEnum.COMMANDS.value, "1.0"],
        "intent": {"sys": [], "topic": [""], "lexical": ["command"]},
    },
]

returnnlp_input_not_command_2 = [
    {
        "text": "yes",
        "dialog_act2": [DialogActEnum.POS_ANSWER.value, "0.7"],
        "dialog_act": [DialogActEnum.COMMANDS.value, "1.0"],
        "intent": {"sys": [], "topic": [""], "lexical": [""]},
    },
]

returnnlp_input_not_command_3 = [
    {
        "text": "yup",
        "dialog_act2": [DialogActEnum.POS_ANSWER.value, "0.7"],
        "dialog_act": [DialogActEnum.COMMANDS.value, "1.0"],
        "intent": {"sys": [], "topic": [""], "lexical": [""]},
    },
]

returnnlp_command_question = [
    {
        "text": "can you tell me when you were born",
        "dialog_act": ["open_question_factual", "0.7"],
        "dialog_act2": [DialogActEnum.COMMANDS.value, "1.0"],
        "intent": {"sys": [], "topic": [""], "lexical": []},
    },
]
@pytest.mark.parametrize("returnnlp, expected_result", [
    (returnnlp_input_not_command, False),
    (returnnlp_input_not_command_2, False),
    (returnnlp_input_not_command_3, False),
    (returnnlp_command_question, False),

])
def test_is_not_command(returnnlp, expected_result):
    command_detector = CommandDetector("", returnnlp)
    result = command_detector.has_command()
    assert result is expected_result


@pytest.mark.parametrize("text", [
    "can we get off movie please",
    "get off this",
    "i don't wanna talk about this anymore",
    "can we not talk about this",
    "i don't like movie",
    "can we stop talking about this",
    "can we stop talking about about this",  # this is a workaround for coreference bug
    "stop talking about this",
    "i hate movie"
])
def test_is_jumpout_true(text):
    command_detector = CommandDetector(text, [{"dialog_act": ["commands", "0.7"]}])
    result = command_detector.is_request_jumpout("movie")
    assert result is True

@pytest.mark.parametrize("text", [
    "can we get off",
    "get off",
    "i don't wanna talk about",
    "can we not talk about",
    "i don't like",
    "can we stop talking about",
    "i don't like to ",
    "i don't like it",
    "i don't like"
])
def test_is_jumpout_false(text):
    command_detector = CommandDetector(text, [{"dialog_act": ["commands", "0.7"]}])
    result = command_detector.is_request_jumpout("movie")
    assert result is False