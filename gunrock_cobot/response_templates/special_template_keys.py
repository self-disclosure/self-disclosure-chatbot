open_question_ask_hobbies_templates = {
    "greeting::open_questions/general",
    "greeting::open_questions/neg",
    "greeting::open_questions/followup",
    "greeting::open_questions/return_user",
}

open_question_event_templates = {
    "social::open_question/future_events",
    "social::open_question/past_events",
    "social::open_question_follow_up",
}

open_question_general_templates = {
    "shared::t_fragments_v2/error/routing/change_topic",
    "shared::t_fragments_v2/open_question/general",
    "shared::t_fragments_v2/open_question/something_else",
    "shared::t_fragments_v2/open_question/jump_out",
    "social::open_question/future_events",
    "social::open_question/past_events",
    "social::open_question_follow_up",
}

open_question_from_dailylife_tempaltes = {
    "social::friendship/open_question"  # todo
}
