import requests
from urllib.parse import quote
import boto3
from operator import itemgetter
from boto3.dynamodb.conditions import Key, Attr
from itertools import chain


dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
table = dynamodb.Table('KG_Data')


def events(entities: list, keywords:list = None, category:list = None) -> list:
    """
    :param entities: a list of entities. maximum number of entities is 2 for now
    :param keywords: optional list of keywords to match events. good for filtering events
    :param category: optional list of categories "sport", "world", "us", "business", "health", "entertainment", "sci_tech"
    :return: a list of events between one or two entities
    """
    if not entities: return []
    try:
        ents = '+'.join(entities)
        keys = ''
        if keywords:
            keys += '?keywords='+'-'.join(keywords) + '&'
        cats = ''
        if category:
            if not keywords:
                cats += '?'
            cats += 'category=' + '-'.join(category)
        query = ents + keys + cats
        query = quote(query, safe='+?=')
        r = requests.get('http://35.168.206.27/api/v1.0/events/'+query)
        return r.json()
    except Exception as e:
        return []

def related_entity(entities: list, filter:str = None) -> list:
    """
    :param entities: a list of entities. maximum number of entities is 2 for now
    :param filter: optional string label to filter entities. can be person, location, organization
    :return: list of related entities
    """
    if not entities: return []
    try:
        ents = '+'.join(entities)
        filt = ''
        if filter:
            filt = '?filter='+str(filter)
        query = ents + filt
        query = quote(query, safe='+?=')
        # print(query)
        r = requests.get('http://35.168.206.27/api/v1.0/relatedentity/'+query)
        return r.json()
    except Exception as e:
        return []

def entity(entities: list) -> list:
    """
    :param entities: a list of entities. maximum number of entities is 2 for now
    :param filter: optional string label to filter entities. can be person, location, organization
    :return: list of related entities
    """
    if not entities: return []
    try:
        ents = '+'.join(entities)
        query = ents
        query = quote(query, safe='+?=')
        r = requests.get('http://35.168.206.27/api/v1.0/entity/'+query)
        return r.json()
    except Exception as e:
        return []

def keywords(word_list: list) -> list:
    # word_list = [word for line in word_list for word in line.split()]
    word_list = list(set(chain(*map(str.split, word_list))))
    fe = Attr('keywords').contains(word_list[0])
    for word in word_list[1:]:
        fe = fe or Attr('keywords').contains(word)

    response = table.scan(
        Select='ALL_ATTRIBUTES',
        FilterExpression=fe,
        )
    return sorted(response['Items'], key=itemgetter('modified'), reverse=True)




##Example Usage
#Dont forget to include these lines in docker- only necessary for keywords function!!!!
#
#RUN pip3 install boto3
#ENV AWS_ACCESS_KEY_ID AKIAJ2G47XHAFTXN77RA
#ENV AWS_SECRET_ACCESS_KEY eMOLgTSjh1NEeqqDwOMPBdjz6d+EDiDkUPB346vv
#
if __name__ == "__main__":
    # print(entity(['donald trump']))
    # print(related_entity(['lebron james'], filter='organization'))
    # print()
    # print()
    print(events(['trump'], category=['business', 'world']))
    #print(events(['lebron james', 'kawhi'], keywords=['lakers','randle']))
