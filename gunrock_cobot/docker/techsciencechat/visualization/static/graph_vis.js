var dom = document.getElementById("container");
var myChart = echarts.init(dom);
var app = {};
var sample_d = {};
var sample_lst = [];

option = null;
var sample_id = "0af4ff4c583a79e4006d9b2d15b068843dd3784635a6faf70c3818d71da7675c";
$.get('../static/graph_info/conversation_path.json', function (data) {
    sample_d.a = data[sample_id];
    for (var i in data[sample_id]){
        sample_lst.push(data[sample_id][i]);
    }
    
});


myChart.showLoading();
$.get('../static/graph_info/automaton_map.json', function (data) {
    myChart.hideLoading();
    var highlighted_states = sample_lst.map(function (node) { return node.state;});
    var highlighted_transitions = sample_lst.map(function (node) { return node.transition;});
    var nodes = data.states.map(function (node) {
        return {
            attributes: {"topic_class": "techscience"},
            color: node.color,
            label: {
                position: 'right',
                normal: {
                    show: true,
                    color: node.color,
                    fontWeight: "bold",
                    textBorderWidth: 3,
                    textBorderColor: "#ffffff",
                    
                },
            },
            id: node.name,
            x: node.position.x,
            y: node.position.y,
            symbolSize: node.size,
            name: node.name,
            category: "techscience",
            itemStyle: {
                normal: {
                    color: "#ffffff",
                    borderColor: node.color,
                    borderWidth: 1,

                },
                emphasis: {
                    shadowColor: node.color,
                    shadowBlur: 10,

                }
            }
        }
    });

    nodes.forEach(function (node) {
        if (highlighted_states.includes(node.id, 0)){
            node.label.normal.color = "#ffffff";
            node.label.normal.textBorderColor = node.color;
            node.itemStyle.normal.color = node.color;
            node.itemStyle.normal.borderColor = "#ffffff";
            node.itemStyle.emphasis.shadowColor = "#ffffff";
        }

    });



    var i = 0;
    var j = 0;
    var all_lists = [];
    for (i = 0; i < data.transitions.length; i++){
        var link = data.transitions[i];
        var source = link.sources[0];
        var targets = link.targets; 

        for (j = 0; j < targets.length; j++){
            all_lists.push({name:link.name,source: source, target: targets[j]});
        }
    }
    var links = all_lists.map(function (link){
        return {
            name: link.name,
            source: link.source,
            target: link.target,
            color: "#202020",

            itemStyle: {
                emphasis: {
                    color: "#202020",
                    symbolSize: 100,

                },
            },
            label: {

                position: 'right',
                normal: {
                    show: false,
                    formatter: function (data){
                        return data.data.name;},
                },
                value: link.name,
                emphasis: {
                    show: true,
                    color: "#202020",
                    formatter: function (data){
                        return data.data.name;},
                }
                
            },
        }
    });
    links.forEach(function (link) {
        console.log(link.name);
        console.log(link.source);
        console.log(link.target);
        var case1 = highlighted_transitions.includes(link.name, 0);
        var case2 = highlighted_states.includes(link.source, 0);
        var case3 = highlighted_states.includes(link.target, 0);
        if (case1 && case2 && case3){            
            link.lineStyle = {
                width: 1,
                color: "#000000"
            };
        }

    });
    myChart.setOption(option = {
        title: {
            text: 'Topic Modules FSM Visualization',
            subtext: 'Default layout',
            top: 'bottom',
            left: 'right'
        },
        tooltip: {formatter: function (data){return data.data.name;}},
        legend: [{data: ["techscience"]}],
        animationDuration: 2000,
        animationEasingUpdate: 'quinticInOut',
        series: [
            {
                name: 'Topic_Modules',
                type: 'graph',
                layout: 'none',
                data: nodes,
                links: links,
                categories: [{"name": "techscience"}],
                symbolSize: 40,
                roam: true,
                focusNodeAdjacency: true,
                edgeSymbolSize: [4, 10],
                edgeSymbol: ['circle', 'arrow'],
                lineStyle: {
                    curveness: 0.1,
                    length: 10
                },
            }
        ]
    });
});
if (option && typeof option === "object") {
    myChart.setOption(option, true);
}