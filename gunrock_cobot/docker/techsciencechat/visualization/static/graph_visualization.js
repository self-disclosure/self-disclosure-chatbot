var dom = document.getElementById("container");
var myChart = echarts.init(dom);
var app = {};
option = null;
myChart.showLoading();
$.get('../static/graph_info/topic_modules.gexf', function (xml) {
    myChart.hideLoading();

    var graph = echarts.dataTool.gexf.parse(xml);
    console.log(graph.nodes);
    var categories = [{"name": "techscience"}];
    // for (var i = 0; i < 9; i++) {
    //     categories[i] = {
    //         name: '类目' + i
    //     };
    // }
    graph.nodes.forEach(function (node) {
        node.itemStyle = null;
        node.value = node.label;
        node.symbolSize /= 1.5;
        node.label = {
            normal: {
                show: true,
            },
            position: 'right',
        };
        node.category = node.attributes.topic_class;
    });

    graph.links.forEach(function (links) {
        links.itemStyle = null;
        links.label = {
            normal: {
                formatter: links.name,
            }};
    });
    option = {
        title: {
            text: 'Topic Modules FSM Visualization',
            subtext: 'Default layout',
            top: 'bottom',
            left: 'right'
        },
        tooltip: {},
        legend: [{
            // selectedMode: 'single',
            data: categories.map(function (a) {
                return a.name;
            })
        }],
        animationDuration: 1500,
        animationEasingUpdate: 'quinticInOut',
        series : [
            {
                name: 'Topic_Modules',
                type: 'graph',
                layout: 'none',
                data: graph.nodes,
                links: graph.links,
                categories: categories,
                symbolSize: 40,
                roam: true,
                focusNodeAdjacency: true,
                edgeSymbolSize: [4, 10],
                itemStyle: {
                    normal: {
                        borderColor: '#fff',
                        borderWidth: 1,
                        shadowBlur: 10,
                        shadowColor: 'rgba(0, 0, 0, 0.3)'
                    }
                },
                edgeSymbol: ['circle', 'arrow'],
                lineStyle: {
                    color: 'source',
                    curveness: 0.1,
                    length: 10
                },
                emphasis: {
                    lineStyle: {
                        width: 10
                    }
                }
            }
        ]
    };

    myChart.setOption(option);
}, 'xml');;
if (option && typeof option === "object") {
    myChart.setOption(option, true);
}