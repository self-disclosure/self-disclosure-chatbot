from flask import Flask, redirect, url_for, render_template, request
app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def home():
    return render_template('home.html')

@app.route('/test', methods=['GET', 'POST'])
def home2():
    return render_template('home2.html')

@app.route('/docs', methods=['GET', 'POST'])
def states():
    return render_template('states.html')

@app.route('/states/<path:path>')
def states_details(path):
    return render_template("states/" + path)
if __name__ == "__main__":
    app.run(debug=True)