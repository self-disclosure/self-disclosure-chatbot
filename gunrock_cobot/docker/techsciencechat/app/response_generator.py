"""Entry point to topic module's automaton
"""
import logging
import json
from techscience_automaton import TechScienceAutomaton
from types import SimpleNamespace

logging.basicConfig(
    format='[%(levelname)s] %(asctime)s [%(filename)s:%(lineno)d] %(message)s',
)


LOGGER = logging.getLogger(__name__)

REQUIRED_CONTEXT = ['text', 'returnnlp', 'central_elem', 'a_b_test', 'system_acknowledgement']


def get_required_context():
    """Get the required context for techscience module.
    """
    return REQUIRED_CONTEXT


def handle_message(msg):
    """Entry point to module automaton

    Parameters:
        msg:  A dictionary with the following format:
          text: raw utterance from asr
          feature: dictionary of NLU features.  DEPRECATED, see central_elem and returnnlp
          returnnlp: full dictionary of NLU features
          central_elem: dictionary of important NLU features.


    Output:
        response_dict:  A dictionary with the following format:
            response:  Generated response given input text
            template_manager: dictionary of keys from template manager.
            user_attribute: Provided by the system.  Only need to update
                - techsciencechat: dictionary with propose_continue
                - propose_topic

    """
    LOGGER.debug("[TECHSCIENCE] System Input to Techscience - {}".format(json.dumps(msg)))
    LOGGER.info("[TECHSCIENCE] Successfully entered Techscience.")
    # Build Automaton
    automaton = TechScienceAutomaton()


    user_attributes = SimpleNamespace(**{
        "session_id": msg["session_id"],
        "a_b_test": msg["a_b_test"][0],
        "user_profile": msg["user_profile"],
        "template_manager": msg["template_manager"],
        "module_selection": msg["module_selection"],
        "previous_modules": msg["previous_modules"],
        "blender_start_time": msg["blender_start_time"]
    })

    # Automaton Transduce
    result = automaton.transduce(msg, user_attributes)

    # Build response dictionary
    response_dict = dict()
    response_dict["response"] = result["response"]
    response_dict["user_attributes"] = user_attributes.__dict__
    response_dict["user_attributes"]["techsciencechat"] = result["techsciencechat"]

    LOGGER.debug("[TECHSCIENCE] System Output From Techscience - {}".format(json.dumps(response_dict)))
    return response_dict
