"""SystemIntentTransition

Parent State:
  None

Next State:
  s_exit_techsciencechat
  s_intro
  s_ask_more

TODO: Create a SystemIntentState in additoin to transition
"""
from base import BaseTransition

NULL_ANSWER = "Hmm, I haven't thought about that before. "
class SystemIntentTransition(BaseTransition):
    def __init__(self, automaton):
        super().__init__(automaton)
        return

    def __call__(self):
        lexical_intent = self._get_intent()
        # First check exit
        if "exit_techscienceRG" in self.lexical_intents:
            # Add response to the template? TODO
            self._set_transition_state(True)
            return "s_exit_techsciencechat"
        #  
        if "req_scitech" in lexical_intent or "req_topic" in lexical_intent:
            hard_topic = self._detect_topics()
            self._set_topic(hard_topic)
            if hard_topic != "no_topic":
                self._set_transition_state()
                return "s_intro"
        sys_intent =  self.automaton.sys_intents
        cobot_intents_lexical = self.automaton.cobot_intents_lexical
        if sys_intent:
            if len(sys_intent) > 1:
                pass
            if "user_ask_question" in sys_intent:
                return "s_echo"
            if sys_intent[0] == "question" or "ask" in cobot_intents_lexical:
                self.automaton._get_answer_from_qa()
            response = self._get_backstory_response()
            if not response:
                self._add_response_string(NULL_ANSWER)
            if self.automaton.prev_transition != "t_techsci_chitchat" and self.automaton.current_topic != "":
                self.automaton.response += " So, about " + self.automaton.current_topic + " <break time=\"300ms\"/>  "
            else:
                self.automaton.response += " Well, coming back to what we were (talking about|discussing) earlier, "
            return "s_ask_more"

        return "exit_techsciencechat"

    def _set_transition_state(self):
        super()._set_transition_state()
        self.automaton.techsciencechat["fact_counter"] = 0

    def _add_response_string(self, s):
        self.automaton.response += s