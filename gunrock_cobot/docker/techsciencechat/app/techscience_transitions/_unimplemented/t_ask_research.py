"""Ask Research Transition

Parent State:
  s_ask_research

Next State:
  s_user_change_module
  s_interesting_research
  s_exit_techsciencechat_intentional
  s_techsci_chitchat
  s_user_change_topic

"""

class AskResearch(BaseTransition):
    name = "t_ask_research"
    def __init__(self, automaton):
        super(AskResearch).__init__(automaton)
        return

    def __call__(self):
        # Call generic transition 
        default_next_state = super(AskResearch, self).__call__()
        # detected_topic, current_topic
        if default_next_state is not None:
            return default_next_state

        yes_no = self.yes_no
        
        if yes_no > 0: # returned yes no
            self.current_topic = self.detected_topic
            return "s_interesting_research"
        else:
            return "s_techsci_chitchat"


"""Anticipated Responses

    System Response:
    - "Oh yeah, do you wanna know about some research about {topic} that I came across? "
    - "Hey, do you want me to tell you about some research about {topic} that I recently red? "
    - "Say, wanna know about some cool findings about {topic} that I red recently?  

    User Response:
    - Yes/No

"""