"""UserChangeModuleTransition

user Change Module Transition.


Parent State:
  #TODO Never called

Next State:
  s_exit_techsciencechat
  s_change_of_topic


"""


class UserChangeModuleTransition(BaseTransition):
    name = "t_user_change_module"

    def __call__(self):
        lexical_intent = self._get_intent()

        self._set_transition_state()

        if "answer_yes" in lexical_intent:
            # self.techsciencechat[
            #     "propose_topic"] = self.output_module_mappings[self.next_module]
            return "s_exit_techsciencechat"
        else:
            self._set_topic(self.automaton.context_topic)
            return "s_change_of_topic"
