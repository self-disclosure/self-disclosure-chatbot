"""UserChangeTopic

user change the topic of conversation

Parent State:
  s_user_change_topic
Next State:
  s_interesting_fact
  s_change_of_topic
"""

class UserChangeTopic(BaseTransition):
    name = "t_user_change_topic"

    def __call__(self):
        if self._detect_user_change():
            return "s_user_change_module"
        lexical_intent = self._get_intent()
        temp_current_topic = self._get_topic()
        self._suggest_topic()
        self._set_transition_state()

        if "answer_yes" in lexical_intent:
            self._set_topic(self.automaton.current_topic)
            return "s_interesting_fact"
        else:
            self._set_topic(self.automaton.context_topic)
            return "s_change_of_topic"

    def _suggest_topic(self):
        # if self.current_topic in self.broader_topics:
        #     if self.current_topic is "technology" or self.current_topic is "tech":
        #         self.current_topic = random.choice(technology_flairs)
        #     if self.current_topic is "science":
        #         self.current_topic = random.choice(science_flairs)

        # if self.current_topic in self.broader_topics:
        #     if self.current_topic is "technology" or self.current_topic is "tech":
        #         self.current_topic = random.choice(technology_flairs)
        #         self.context_topic = self.current_topic
        #     if self.current_topic is "science":
        #         self.current_topic = random.choice(science_flairs)
        #         self.context_topic = self.current_topic
        return

