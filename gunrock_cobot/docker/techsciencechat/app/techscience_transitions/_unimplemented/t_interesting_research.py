""" Interesting Fact Transition

Previous State:
  - 

Next State
  - s_user_change_module
  - s_interesting_fact
  - s_exit_techsciencechat_intentional
  - s_techsci_chitchat
  - s_user_change_topic


"""

class InterestingResearch(BaseTransition):
    name = "t_interesting_research"
    def __init__(self, automaton):
        super().__init__(automaton)
        return

    def __call__(self):
        if self._detect_user_change():
            return "s_user_change_module"
        current_topic = self._detect_topics()
        lexical_intent = self._get_intent()

        chit_chat = self.find_next_chitchat()
        self._set_transition_state()
        self._set_topic(self.automaton.context_topic, prev_topic="no_topic")


        if chit_chat == "end_chitchat":
            return "s_exit_techsciencechat_intentional"

        self._set_topic(self.automaton.context_topic, prev_topic="no_topic")
        self.reset_fact_counter() #
        return "s_techsci_chitchat"

        self._suggest_topic()
        if self.detect_user_change_topic():
            self._set_topic(current_topic)
            self.reset_fact_counter() #
            return "s_user_change_topic"
        else:
            self.increase_fact_counter() #
            return "s_interesting_fact"

    def _suggest_topic(self):
        # if self.context_topic in self.broader_topics or temp_current_topic in self.broader_topics:
        #     if self.context_topic == "technology" or self.context_topic == "tech" \
        #             or temp_current_topic == "technology" or temp_current_topic == "tech":
        #         self.current_topic = random.choice(technology_flairs).lower()
        #         self.context_topic = self.current_topic
        #     if self.context_topic == "science" or temp_current_topic == "science":
        #         self.current_topic = random.choice(science_flairs).lower()
        #         self.context_topic = self.current_topic
        return
