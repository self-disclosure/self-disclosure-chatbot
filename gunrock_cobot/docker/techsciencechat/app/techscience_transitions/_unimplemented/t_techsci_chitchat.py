"""
if self.intent_classify_topic in self.module_mappings:
    return "s_user_change_module"
temp_current_topic = detect_topic(
    self.knowledge, self.topic_keywords, self.noun_phrase, self.ner, self.text)
lexical_intent = detect_intent(self.text)
self.chitchat_counter = find_next_chitchat(self.chitchat_counter)
if self.chitchat_counter == "end_chitchat":
    return "s_exit_techsciencechat_intentional"
if self.chitchat_counter == "" and temp_current_topic is "no_topic":
    self.techsciencechat["propose_continue"] = "CONTINUE"
    self.techsciencechat["current_topic"] = self.current_topic
    self.techsciencechat["context_topic"] = self.context_topic
    self.techsciencechat["fact_counter"] = self.fact_counter
    self.techsciencechat["research_counter"] = self.research_counter
    self.techsciencechat["chitchat_counter"] = self.chitchat_counter
    return "s_change_of_topic"
elif self.chitchat_counter == "q4" and temp_current_topic is "no_topic":
    self.techsciencechat["propose_continue"] = "CONTINUE"
    self.techsciencechat["current_topic"] = self.current_topic
    self.techsciencechat["fact_counter"] = self.fact_counter
    self.techsciencechat["context_topic"] = self.context_topic
    self.techsciencechat["research_counter"] = self.research_counter
    self.techsciencechat["chitchat_counter"] = self.chitchat_counter
    return "s_ask_research"
elif self.chitchat_counter != "" and temp_current_topic is "no_topic":
    self.techsciencechat["propose_continue"] = "CONTINUE"
    self.techsciencechat["fact_counter"] = self.fact_counter
    self.techsciencechat["research_counter"] = self.research_counter
    self.techsciencechat["current_topic"] = self.current_topic
    self.techsciencechat["context_topic"] = self.context_topic
    self.techsciencechat["chitchat_counter"] = self.chitchat_counter
    return "s_techsci_chitchat"

if self.context_topic in self.broader_topics or temp_current_topic in self.broader_topics:
    if self.context_topic == "technology" or self.context_topic == "tech" \
            or temp_current_topic == "technology" or temp_current_topic == "tech":
        self.current_topic = random.choice(technology_flairs).lower()
        self.context_topic = self.current_topic
    if self.context_topic == "science" or temp_current_topic == "science":
        self.current_topic = random.choice(science_flairs).lower()
        self.context_topic = self.current_topic

if temp_current_topic is not self.context_topic and temp_current_topic is not "no_topic":
    self.context_topic = temp_current_topic
    self.current_topic = temp_current_topic
    self.techsciencechat["propose_continue"] = "CONTINUE"
    self.techsciencechat["current_topic"] = self.current_topic
    self.techsciencechat["fact_counter"] = self.fact_counter
    self.techsciencechat["research_counter"] = self.research_counter
    self.techsciencechat["chitchat_counter"] = self.chitchat_counter
    return "s_user_change_topic"
"""

"""Techscience Chitchat

Techscience Chitchat

Parent State:
  #TODO Never called

Next State:
  s_s_exit_techsciencechat_intentional
  s_ask_research
  s_change_of_topic
  s_techsci_chitchat
  s_user_change_topic


"""


class TechSciChitChatTransition(BaseTransition):
    name = "t_techsci_chitchat"

    def __call__(self):
        if self._detect_user_change():
            return "s_user_change_module"
