"""ResumeTransition
"""
import logging
from techscience_transitions.base import BaseTransition
from techscience_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class ResumeTransition(BaseTransition):
    """Resume Transition is implemented
    """
    name = "t_resume"

    def __init__(self, automaton):
        super(ResumeTransition, self).__init__(automaton)
        # self.sources = ["entry_point"]
        # self.targets += ["s_intro", "s_ask_more","s_error_handling"]
        return

    def __call__(self, init=False):
        # pylint: disable=broad-except
        try:
            LOGGER.info("Using t_resume")
            default_next_state = super(ResumeTransition, self).__call__(init)
            if default_next_state is not None:
                return default_next_state
            LOGGER.info("Entered t_resume")
            propose_topic = len(self.propose_topic) > 0
            if self.yes and propose_topic:
                self.set_attr("current_topic", self.propose_topic[0])
                if self.topic_handler.is_fact(self.current_topic):
                    return "s_interesting_fact"
                elif self.topic_handler.is_topic(self.current_topic):
                    return "s_ask_favorite_thing"
            else:
                new_topics = self.topic_handler.propose_new_topics(None, self.propose_topic, self.context_topic, self.topic_history)
                propose_topic = self.topic_handler.propose_topics_to_list(new_topics)
                self.set_attr("propose_topic", propose_topic)
                
                new_topic_history = self.topic_handler.construct_topic_history(self.current_topic, self.topic_history, {t:0 for t in self.propose_topic})
                self.set_attr("current_topic", None)
                self.set_attr("topic_history", new_topic_history)
                return "s_change_of_topic"
            return "s_change_of_topic"
        except Exception as _e:
            LOGGER.critical("Error:  {}".format(_e))
            return "s_error_handling"
