


import logging
from techscience_transitions.base import BaseTransition
from techscience_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())


class RetrievalTransition(BaseTransition):
    """Init Transition is the general transition.
    """
    name = "t_retrieval"

    def __init__(self, automaton):
        super(RetrievalTransition, self).__init__(automaton)
        self.sources = ["s_retrieval"]
        self.targets += ["s_interesting_fact", "s_ask_favorite_thing","s_intro", "s_asked_more"]
        return

    def __call__(self, init=False):
        # pylint: disable=broad-except
        try:
            LOGGER.info("Using t_transition")
            default_next_state = super(RetrievalTransition, self).__call__(init)
            if default_next_state is not None:
                return default_next_state
            LOGGER.info("Entered t_transition")
            if self.topic_handler.is_fact(self.current_topic):
                return "s_interesting_fact"
            elif self.topic_handler.is_topic(self.current_topic):
                return "s_ask_favorite_thing"
            elif self.topic_handler.is_broad_topic(self.current_topic):
                return "s_intro"
            else:
                return "s_ask_more"
        except Exception as _e:
            LOGGER.critical("Error:  {0}".format(_e), exc_info=True)
            return "s_error_handling"