"""Init Transition

Parent State:
  None

Customized Next State:
  s_intro
  s_ask_more

Anticipated Responses
    Comments:
      From Previous Modules, the user may interest in science and technology,
      or entered by pure mistake.
      There is no previously defined topic.
      So, we can enter s_intro, after getting the nlu components.

"""
import logging
from techscience_transitions.base import BaseTransition
from techscience_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())


class InitTransition(BaseTransition):
    """Init Transition is the general transition.
    """
    name = "t_init"

    def __init__(self, automaton):
        super(InitTransition, self).__init__(automaton)
        self.sources = ["entry_point"]
        self.targets += ["s_intro", "s_ask_more","s_error_handling"]
        return

    def __call__(self, init=True):
        # pylint: disable=broad-except
        try:
            LOGGER.info("Using t_init")
            default_next_state = super(InitTransition, self).__call__(init)
            if default_next_state is not None:
                return default_next_state
            LOGGER.info("Entered t_init")
            self.init_set_topic()

            if self.detect_jumpout_i_like_science:
                LOGGER.debug("Detected Jumpout")
                return "s_exit_techsciencechat"

            if self.get_intro_chitchat():
                # When condition satisfies for intro_chitchat
                return "s_trendy_chitchat"
            
            if self.current_topic is None or self.system_can_handle(self.current_topic):
                return "s_intro"

            return "s_ask_more"
        except Exception as _e:
            LOGGER.critical("Error:  {0}".format(_e), exc_info=True)
            return "s_error_handling"

    def init_set_topic(self):
        """When first entering techscience.
           
        """
        topic_empty = len(self.detected_topics) == 0
        if topic_empty:
            return
        current_topic = self.topic_handler.get_highest_topic(self.detected_topics)
        context_topic = self.topic_handler.construct_context_topics(self.current_topic, \
                                                                         self.detected_topics, \
                                                                         self.context_topic)
        self.set_attr("current_topic", current_topic)
        self.set_attr("context_topic", context_topic)

    def get_transition_response_cases(self):
        # Do you like technology, my favorite + love it ==> custom intro chitchat
        last_system_response = self.get_last_system_response

        # Do you like technology, my favorite + yes i do ==> acknowledgement + fact chitchat about technology
        if self.mentioned_favorite_technology(get_last_system_response): #TODO
            # User expressed love also:  ask user's opinion on technology, default on proposing a technology topic
            if self.user_expressed_love: #TODO
                possible_nxt_turn = ["s_intro", "s_trendy_chitchat"]
                return
            # User Nonchalant:  Opinion Chitchat
            elif self.user_expressed_no_interest:
                # Propose a different interest in technology
                return
            else:
                possible_nxt_turn = ["s_intro", "s_trendy_chitchat", "s_opinion"]
                return
            
            # Default to ask user interest
                
            return

        # Do you like technology, my favorite + sure ==> acknowledgement + cool trend + ask interest
        if self.mentioned_favorite_science:
            return
    
        if self.mentioned_love_topic:
            if self.user_expressed_love: #TODO
                # Ask User Opinion on the subject
                return
            elif self.user_expressed_no_interest:
                # Propose a different topic under
                return
            else:
                # Propose Fact or Trend
                return
            # If 
            return # Default s_intro
        # Expressed love on a topic:
            # - sure
            # - yes/i am into
            # - me too 
            # - yes, (with opinion)
            # - I love **
            #

        if self.mentioned_technology:
            # If user said yes:
            # Ask for a more specific topic, or propose a trendy topic
            return


        if self.mentioned_science:
            # If user said yes, yeah
            return

        if self.mentioned_specific_topic:
            # If User Yes
            # Present Fact on current topic
            return
        return

    def mentioned_favorite_technology(self, txt):
        favorite = self.nlu_processor.text_contains(["my favorite things"], txt)
        tech = self.nlu_processor.text_contains(["do you like technology"], txt)
        return favorite and tech

    def mentioned_favorite_science(self, txt):
        favorite = self.nlu_processor.text_contains(["my favorite things"], txt)
        tech = self.nlu_processor.text_contains(["do you like science"], txt)
        return favorite and tech
        
    def mentioned_specific_topic(self, txt):
        return

    def mentioned_love_topic(self, txt):
        return
