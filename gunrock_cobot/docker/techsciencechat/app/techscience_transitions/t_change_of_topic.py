"""Change of topic transition

Parent State:
  s_change_of_topic

Next State:
  s_interesting_fact
  s_ask_more
  s_opinion

"""
import logging
from techscience_transitions.base import BaseTransition
from techscience_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class ChangeOfTopicTransition(BaseTransition):
    """Implements ChangeOfTopic
    """
    name = "t_change_of_topic"

    def __init__(self, automaton):
        super(ChangeOfTopicTransition, self).__init__(automaton)
        self.sources = ["s_change_of_topic"]
        self.targets += ["s_interesting_fact", "s_change_of_topic", "s_ask_more","s_error_handling"]
        return

    def __call__(self, init=False):
         # pylint: disable=broad-except
        try:
            LOGGER.info("Using t_change_of_topic")
            default_next_state = super(ChangeOfTopicTransition, self).__call__(init)
            # User said alexa, you, and so on
            detect_alexa = self.nlu_processor.detect_alexa

            if default_next_state is not None:
                return default_next_state
            LOGGER.info("Entered t_change_of_topic")
            propose_topic = len(self.propose_topic) > 0
            topic_active = self.current_topic is not None
            LOGGER.debug("Change of topic {0}".format(self.propose_topic))
            LOGGER.debug("Current topic {0}".format(self.current_topic))
            if not propose_topic and not topic_active:
                self._set_transition_state(True)
                ret = "s_exit_techsciencechat"                
            
            elif (not topic_active and propose_topic) or topic_active or (propose_topic and self.yes):
                if topic_active:
                    curr_topic = self.current_topic
                else:
                    curr_topic = self.get_current_topic()
                if propose_topic and self.yes:
                    curr_topic = self.propose_topic[0]
                
                # Note, curr_topic could be None if there is no topic detected
                # We can propose a new topic?
                # Just in case: artificial intelligence
                is_fact = self.topic_handler.is_fact(curr_topic)
                is_topic = self.topic_handler.is_topic(curr_topic)
                LOGGER.debug("Current topic {0}, is_fact {1}, is_topic {2}".format(curr_topic, is_fact, is_topic))
                if self.yes or self.asked_more:
                    self.set_attr("current_topic", curr_topic)
                    self.set_attr("propose_topic", [])
                    self.reset_fact_counter()
                    if is_fact:
                        ret = "s_interesting_fact"
                    elif is_topic:
                        ret = "s_ask_favorite_thing"
                    else:
                        ret = "s_exit_techsciencechat"
                else:
                    # Change topic to trigger exit techscience
                    # new_topics = self.topic_handler.propose_new_topics(None, [], self.context_topic, self.topic_history)
                    new_topic_history = self.topic_handler.construct_topic_history(self.current_topic, self.topic_history, {t:0 for t in self.propose_topic}) #TODO
                    self.set_attr("topic_history", new_topic_history)
                    # self.propose_topic = self.topic_handler.propose_topics_to_list(new_topics)
                    self.set_attr("current_topic", None)
                    self.set_attr("propose_topic", [])

                    self._set_transition_state(True)
                    ret = "s_exit_techsciencechat"
            elif detect_alexa:
                self.set_attr("current_topic", "alexa")
                self.set_attr("propose_topic", [])
                ret = "s_trendy_chitchat"
            else:
                self._set_transition_state(True)
                ret = "s_exit_techsciencechat"   

            return ret
        except Exception as _e:
            LOGGER.critical("Error:  {0}".format(_e), exc_info=True)
            return "s_error_handling"
