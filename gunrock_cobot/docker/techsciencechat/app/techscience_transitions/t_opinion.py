"""Opinion Transition

Parent State:
  s_opinion

Next State:
  s_ask_favorite_thing
  s_change_of_topic

Anticipated Responses

    System response - *Generic opinion about each topic, or no topic.
    User response - May propose ideas, may have a generic idea.

    Next State: s_ask_more, s_ask_favorite_thing, s_change_topic

"""
import logging
from techscience_transitions.base import BaseTransition
from techscience_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class OpinionTransition(BaseTransition):
    """Opinion Transition is a general transition
    """
    name = "t_opinion"

    def __init__(self, automaton):
        super(OpinionTransition, self).__init__(automaton)
        self.sources = ["s_opinion"]
        self.targets += ["s_ask_favorite_thing", "s_change_of_topic", "s_ask_more","s_error_handling"]
        return

    def __call__(self, init=False):
        # pylint: disable=broad-except
        try:
            default_next_state = super(OpinionTransition, self).__call__(init)
            LOGGER.info("Using t_opinion")
            if self.expected_next_state == "s_change_of_topic":
                self.propose_new_topic()
            if default_next_state is not None:
                return default_next_state
            LOGGER.debug("Current topic {}".format(self.current_topic))
            if self.chitchat_continue(self.chitchat_tracker, True):
                next_state = self.map_chitchat_to_state(self.chitchat_tracker)
                LOGGER.debug("Potential Next State: {}".format(next_state))
                if next_state is not None:
                    ret = next_state
                else:
                    ret = "s_opinion"
            else:
                if self.topic_handler.is_topic(self.current_topic):
                    ret = "s_ask_favorite_thing"
                if self.topic_handler.is_fact(self.current_topic):
                    ret = "s_interesting_fact"
                else:
                    ret = "s_trendy_chitchat"
            return ret
        except Exception as _e:
            LOGGER.critical("Error:  {}".format(_e), exc_info=True)
            self.stop_and_set_chitchat()
            return "s_error_handling"
