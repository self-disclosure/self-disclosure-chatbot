"""Intro Transition

Parent State:
  s_intro

Next State:
  s_user_change_module
  s_interesting_research
  s_opinion

Anticipated Responses

    new_topic_starter_unknown
    
      - Wanna tell me more about it or do you want to talk about something else?
        - detect:  user not wanting
        - 
      - Could you teach me about it?  Or would you wanna talk about something else?
        - detect: user said yes, user want to talk about something else
      - Can you tell me what you find interesting about {topic}?  I am very happy to learn more.
        - user not wanting, user said yes, user said sure
    
    intro/broad_topic_starter_active
      User response - {topic_suggestions}, none, user_propose new ideas.

    Next State: s_ask_more, s_ask_favorite_thing

    System response - What sparked your interest in {broad_topic}?
    User response - free-formed response about {broad_topic} + probably a specific topic
    Next - s_opinion, s_ask_favorite_thing

    intro/broad_topic_starter_passive:

"""
import logging
from techscience_transitions.base import BaseTransition
from techscience_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class IntroTransition(BaseTransition):
    """Intro Transition implemented
    """
    name = "t_intro"

    def __init__(self, automaton):
        super(IntroTransition, self).__init__(automaton)
        self.sources = ["s_intro"]
        self.targets += ["s_ask_favorite_thing", "s_change_of_topic", "s_ask_more","s_error_handling"]

        return

    def __call__(self, init=False):
        # pylint: disable=broad-except
        try:
            default_next_state = super(IntroTransition, self).__call__(init)
            LOGGER.info("Using t_intro")
            if default_next_state is not None:
                return default_next_state
            # User saying no to proposed topics
            LOGGER.info("Entered t_intro")
            LOGGER.debug("Expected Next State {}".format(self.expected_next_state))
            LOGGER.debug("User Said Yes {}".format(self.yes))
            # LOGGER.debug("User Said Yes {}".format(self.user_interested))


            change = self.no or self.negative_sentiment
            if change:
              self.set_attr("current_topic", None)
              new_topics = self.topic_handler.propose_new_topics(None, self.propose_topic, self.context_topic,self.topic_history)
              propose_topic = self.topic_handler.propose_topics_to_list(new_topics)
              self.set_attr("propose_topic", propose_topic)
              new_topic_history = self.topic_handler.construct_topic_history(self.current_topic, self.topic_history, {t:0 for t in self.propose_topic})
              LOGGER.debug("[T_INTRO] User Said No to our proposed Topic: Topic History {}".format(new_topic_history))
              self.set_attr("topic_history", new_topic_history)
              ret = "s_change_of_topic"
            else:
            # User picking a topic we can handle
              curr_topic = self.get_current_topic(fact_only=False)
              LOGGER.debug("Found Common Topic: {}".format(curr_topic))
              # Note, curr_topic could be None if there is no topic detected
              if curr_topic is None:
                if len(self.new_topics) > 0:
                  for topic in self.new_topics:
                    if "mentioned" in self.new_topics[topic]:
                      self.new_topics[topic] = self.new_topics[topic].replace("mentioned", "using")
                  ret = "s_ask_more"
                else:
                    new_topics = self.topic_handler.propose_new_topics(None, [], self.context_topic, self.topic_history)
                    curr_topic = self.topic_handler.get_highest_topic(new_topics)

              # We can propose a new topic?

              if self.topic_handler.is_chitchat_topic("opinion", curr_topic):
                self.set_attr("current_topic", curr_topic)
                ret = "s_opinion"
                self.save_topic(self.current_topic)
              elif self.topic_handler.is_topic(curr_topic):
                self.set_attr("current_topic", curr_topic)
                ret = "s_ask_favorite_thing"
                self.save_topic(self.current_topic)
              else:
                if len(self.new_topics) > 0:
                  ret =  "s_ask_more"
                else:
                  self.set_attr("current_topic", "artificial_intelligence")
                  ret = "s_interesting_fact"

            # self.topic_history.append(self.current_topic)
            return ret
        except Exception as _e:
            LOGGER.critical("Error:  {0}".format(_e), exc_info=True)
            return "s_error_handling"
    def save_topic(self, current_topic):
      topic_and_sentiment = {topic:self.detected_topics[topic] for topic in self.propose_topic if topic in self.detected_topics}
      if current_topic not in topic_and_sentiment:
        topic_and_sentiment[current_topic] = 1
      new_topic_history = self.topic_handler.construct_topic_history(current_topic, self.topic_history, topic_and_sentiment)
      self.set_attr("topic_history", new_topic_history)
      LOGGER.debug("New topic History from TINTRO {}".format(new_topic_history))