"""Error Handling Transition

Parent State:
  s_error_handling

Next State:
  s_change_of_topic

"""
import logging
from techscience_transitions.base import BaseTransition
from techscience_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())


class ErrorHandling(BaseTransition):
    """Error handling Transition, hopefully this will never by reached
    """
    name = "t_error_handling"

    def __init__(self, automaton):
        super(ErrorHandling, self).__init__(automaton)
        self.sources = ["s_error_handling"]
        self.targets += ["s_change_of_topic","s_error_handling"]
        return

    def __call__(self, init=False):
        # Call generic transition
        try:
            # pylint: disable=broad-except
            default_next_state = super(ErrorHandling, self).__call__(init)
            LOGGER.info("Using t_error_handling")
            # detected_topic, current_topic
            if default_next_state is not None:
                return default_next_state
            # Note, curr_topic could be None if there is no topic detected
            # We can propose a new topic?
            self.set_attr("current_topic", None)
            self.set_attr("propose_topic", [])
            self._set_transition_state(_exit=True)
            return "s_exit_techsciencechat"
        except Exception as _e:
            LOGGER.debug("Error:  %s".format(_e))
            self._set_transition_state(True)
            return "s_exit_techsciencechat"
