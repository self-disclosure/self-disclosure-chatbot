"""Trendy Chitchat

"""
import logging
from techscience_transitions.base import BaseTransition
from techscience_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class TrendyChitChatTransition(BaseTransition):
    """Opinion Transition is a general transition
    """
    name = "t_trendy_chitchat"

    def __init__(self, automaton):
        super(TrendyChitChatTransition, self).__init__(automaton)
        self.sources = ["s_trendy_chitchat"]
        self.targets += ["s_change_of_topic", "s_ask_more","s_error_handling"]
        return

    def __call__(self, init=False):
        # pylint: disable=broad-except
        try:
            default_next_state = super(TrendyChitChatTransition, self).__call__(init)
            LOGGER.info("Using t_trendy_chitchat")


            if self.expected_next_state is not None:
                LOGGER.debug(self.expected_next_state)
                if default_next_state == "s_exit_techsciencechat":
                    self._set_transition_state(True)
                return self.expected_next_state

            if default_next_state is not None:
                return default_next_state

            if self.chitchat_continue(self.chitchat_tracker, True):
                next_state = self.map_chitchat_to_state(self.chitchat_tracker)
                LOGGER.debug("Potential Next State: {}".format(next_state))
                if next_state is not None:

                    ret = next_state
                else:
                    ret = "s_trendy_chitchat"
            else:
                if self.topic_handler.is_topic(self.current_topic):
                    ret = "s_ask_favorite_thing"
                elif self.topic_handler.is_fact(self.current_topic):
                    ret = "s_interesting_fact"
                else:
                    not_topic = [self.current_topic]
                    self.set_attr("current_topic", None)
                    new_topics = self.topic_handler.propose_new_topics(None, not_topic, self.context_topic, self.topic_history)
                    propose_topic = self.topic_handler.propose_topics_to_list(new_topics)
                    self.set_attr("propose_topic", propose_topic)
                    new_topic_history = self.topic_handler.construct_topic_history(self.current_topic, self.topic_history, {t:0 for t in self.propose_topic})
                    self.set_attr("topic_history", new_topic_history)
                    ret = "s_change_of_topic"
            return ret
        except Exception as _e:
            LOGGER.critical("Error:  {}".format(_e), exc_info=True)
            self.stop_and_set_chitchat()
            return "s_error_handling"
