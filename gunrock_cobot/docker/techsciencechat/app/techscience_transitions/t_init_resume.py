"""ResumeTransition

Resume the state after module is exited.
This state will keep track of the techscience states


Parent State:
  s_user_change_module

Next State:
  s_ask_more
  s_random_fact

Anticipated Responses
    Comments:
      This is called the user entered techscience module a second time.  We do have
      a previous topic and context topic to talk about.
      It does seem weird to immediately prompt the user to the last time we wanted
      to talk about.  We should prompt a chitchat or a interesting news regarding the
      previous known topic
"""
import logging
from techscience_transitions.base import BaseTransition
from techscience_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class InitResumeTransition(BaseTransition):
    """Resume Transition is implemented
    """
    name = "t_init_resume"

    def __init__(self, automaton):
        super(InitResumeTransition, self).__init__(automaton)
        self.sources = ["entry_point"]
        self.targets += ["s_intro", "s_ask_more","s_error_handling"]
        return

    def __call__(self, init=True):
        # pylint: disable=broad-except
        try:
            LOGGER.info("Using t_init_resume")
            default_next_state = super(InitResumeTransition, self).__call__(init)
            
            if default_next_state is not None:
                return default_next_state
            LOGGER.info("Entered t_init_resume")
            if self.detect_jumpout_i_like_science:
                LOGGER.debug("Detected Jumpout")
                return "s_exit_techsciencechat"
            self.resume_set_topic()
            # if self.current_topic is None and self.get_intro_chitchat():
            #     return "s_trendy_chitchat"

            if self.current_topic is None or self.system_can_handle(self.current_topic):
                if len(self.detected_topics) > 0:
                    current_topic = self.get_current_topic(False)
                    LOGGER.debug("Detected Current Topic from USER {0}, detected_topics {1}".format(current_topic, self.detected_topics))
                    self.set_attr("current_topic", current_topic)
                return "s_intro"
            return "s_ask_more"
        except Exception as _e:
            LOGGER.error("Error:  {}".format(_e), exc_info=True)
            return "s_error_handling"


    def resume_set_topic(self):
        """Set topic with current
        """
        if self.current_topic is not None:
            return
        # self.set_attr("current_topic", self.topic_handler.empty_topic_keyword)
        # self.set_attr("propose_topic", self.topic_handler.default_propose_topic)
        # self.set_attr("context_topic", self.topic_handler.default_context)
        # self.set_attr("new_topics", self.nlu_processor.default_new_topics)
        topic_empty = len(self.detected_topics) == 0
        if topic_empty:
            return
        current_topic = self.topic_handler.get_highest_topic(self.detected_topics)
        context_topic = self.topic_handler.construct_context_topics(self.current_topic, \
                                                                         self.detected_topics, \
                                                                         self.context_topic)
        self.set_attr("current_topic", current_topic)
        self.set_attr("context_topic", context_topic)


    def get_transition_response_cases(self):
        # Do you like technology, my favorite + love it ==> custom intro chitchat
        last_system_response = self.get_last_system_response

        # Last time we were talking about technology
        # user said sure:  propose trendy topic
