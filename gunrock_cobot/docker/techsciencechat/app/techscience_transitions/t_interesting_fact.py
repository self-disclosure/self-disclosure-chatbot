""" Interesting Fact Transition

Previous State:
  -

Next State
  - s_user_change_module
  - s_interesting_fact
  - s_exit_techsciencechat_intentional
  - s_techsci_chitchat
  - s_user_change_topic

Anticipated Responses
    Comments:
      The user asked for an interesting fact.  The system responded for a fact and
      asked if the user wants to know more.  Note, this may be delimited by a fact counter.

    System response -
    User response - yes
    Next State:  s_interesting_fact

    System Response - A fact with random thought. I was wandering,
    what is the difference betwee topic1 and topic2.
    User response - I don't know.  This is interesting.
    Next State: s_echo

    User response - no
    Comment - "acknowledge these facts are pretty interesting, but thank the user for listening.
               prompt an opinion".
    Next State: s_random_thought

    OR

    Comment - smoother transition into chitchat
    next_state: chitchat
"""

import logging
from techscience_transitions.base import BaseTransition
from techscience_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class InterestingFact(BaseTransition):
    """Interesting Fact Transition is Implemented
    """
    name = "t_interesting_fact"

    def __init__(self, automaton):
        super(InterestingFact, self).__init__(automaton)
        self.sources = ["s_interesting_fact"]
        self.targets += ["s_interesting_fact", "s_change_of_topic","s_error_handling"]
        return

    def __call__(self, init=False):
        # pylint: disable=broad-except
        try:
            default_next_state = super(InterestingFact, self).__call__(init)
            if self.expected_next_state == "s_change_of_topic":
                self.propose_new_topic()
            if default_next_state is not None:
                return default_next_state
            LOGGER.info("Using t_interesting_fact")
            # Interesting fact
            is_fact = self.topic_handler.is_fact(self.current_topic)
            if not is_fact:
                raise Exception("Not fact topic {}", self.current_topic)

            LOGGER.debug("User Said No {}".format(self.no))
            LOGGER.debug("User Said negative_sentiment {}".format(self.negative_sentiment))
            LOGGER.debug("Curr_Topic {}".format(self.current_topic))

            LOGGER.debug("User Said Yes {}".format(self.yes))

            not_want = self.no or self.negative_sentiment


            if self.chitchat_continue(self.chitchat_tracker, True):
                next_state = self.map_chitchat_to_state(self.chitchat_tracker)
                LOGGER.debug("Potential Next State: {}".format(next_state))
                if next_state is not None:

                    return next_state
                return "s_interesting_fact"

            cont = (self.yes or self.asked_more) and not self.no
            if cont:
                if self.fact_counter_limit:
                    new_topics = self.topic_handler.propose_new_topics(None, [self.current_topic], self.context_topic, self.topic_history)
                    propose_topic = self.topic_handler.propose_topics_to_list(new_topics)
                    self.set_attr("propose_topic", propose_topic)
                    return "s_change_of_topic"
                return "s_interesting_fact"

            if not_want:
                new_topic_history =  self.topic_handler.construct_topic_history(self.current_topic, self.topic_history, {self.current_topic:-1})
                new_topics = self.topic_handler.propose_new_topics(None, [self.current_topic], self.context_topic, self.topic_history)
                propose_topic = self.topic_handler.propose_topics_to_list(new_topics)
                self.set_attr("propose_topic", propose_topic)
                
                new_topic_history.extend(self.topic_handler.construct_topic_history(self.current_topic, self.topic_history, {t:0 for t in self.propose_topic}))
                self.set_attr("current_topic", None)
                self.set_attr("topic_history", new_topic_history)
                return "s_change_of_topic"

            if self.positive_sentiment:
                return "s_interesting_fact"

            if self.topic_handler.is_topic(self.current_topic) and self.chitchat_can_handle(self.current_topic, "opinion"):
                return "s_opinion"
            elif self.topic_handler.is_topic(self.current_topic):
                new_topics = self.topic_handler.propose_new_topics(None, [self.current_topic], self.context_topic,self.topic_history)
                propose_topic = self.topic_handler.propose_topics_to_list(new_topics)
                self.set_attr("propose_topic", propose_topic)
                new_topic_history =  self.topic_handler.construct_topic_history(self.current_topic, self.topic_history, {t:0 for t in self.propose_topic})
                self.set_attr("topic_history", new_topic_history)
                self.set_attr("current_topic", None)
                return "s_change_of_topic"
            self.set_attr("current_topic", None)
            # Propose trendy topic
            return "s_trendy_chitchat"
        except Exception as _e:
            LOGGER.critical("Error:  {}".format(_e), exc_info=True)
            self.stop_and_set_chitchat()
            return "s_error_handling"
