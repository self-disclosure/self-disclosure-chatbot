"""Ask Favorite Thing Transition

Parent State:
  s_ask_favorite_thing

Next State:
  s_user_change_module
  s_interesting_fact
  s_exit_techsciencechat_intentional
  s_techsci_chitchat

Anticipated Responses

    System response - Do you want to hear something cool about {topic}?
    User response - yes/no

    if else:
        - s_opinion
    if yes/no:
        - s_interesting_fact
"""
import logging
from techscience_transitions.base import BaseTransition
from techscience_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class AskFavoriteThing(BaseTransition):
    """Transition after Ask Favorite Thing
    """
    name = "t_ask_favorite_thing"

    def __init__(self, automaton):
        super(AskFavoriteThing, self).__init__(automaton)
        self.sources = ["s_ask_favorite_thing"]
        self.targets += ["s_interesting_fact", "s_change_of_topic", "s_error_handling"]
        return

    def __call__(self, init=False):
        # pylint: disable=broad-except
        try:
            default_next_state = super(AskFavoriteThing, self).__call__(init)
            LOGGER.info("Using t_ask_favorite_thing")
            if default_next_state is not None:
                return default_next_state
            curr_topic = self.get_current_topic()
            if curr_topic is None and self.current_topic is not None:
                curr_topic = self.current_topic
            # Note, curr_topic could be None if there is no topic detected
            # We can propose a new topic?
            if not self.topic_handler.is_fact(curr_topic):
                curr_topic = self.topic_handler.get_fact_from_topic(curr_topic, self.topic_history)
            cont = curr_topic is not None and (self.yes or self.asked_more or self.positive_sentiment)
            LOGGER.debug("User Said Yes {}".format(self.yes))
            LOGGER.debug("User Said Asked More {}".format(self.asked_more))
            LOGGER.debug("User Said Positive Sentiment {}".format(self.positive_sentiment))
            LOGGER.debug("Curr_Topic {}".format(curr_topic))
            if cont:
                self.set_attr("current_topic", curr_topic)
                return "s_interesting_fact"
            
            change = self.no or self.negative_sentiment
            LOGGER.debug("Entered Change {}".format(change))
            if change:
                # Change means to propose a topic other than current topic
                not_topic = [self.current_topic]
                
                new_topics = self.topic_handler.propose_new_topics(curr_topic, not_topic, self.context_topic, self.topic_history)
                propose_topic = self.topic_handler.propose_topics_to_list(new_topics)
                self.set_attr("propose_topic", propose_topic)
                new_topic_history = self.topic_handler.construct_topic_history(self.current_topic, self.topic_history, {self.current_topic: 0})
                new_topic_history.extend(self.topic_handler.construct_topic_history(self.current_topic, self.topic_history, {t:0 for t in self.propose_topic}))
                self.set_attr("topic_history", new_topic_history)
                self.set_attr("current_topic", None)
                return "s_change_of_topic"

            return "s_trendy_chitchat"
        except Exception as _e:
            self.stop_and_set_chitchat()
            LOGGER.critical("Error:  {}".format(_e), exec_info=True)
            return "s_error_handling"
