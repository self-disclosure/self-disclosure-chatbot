"""Base Class for Transition
"""
import random
import logging
from typing import Optional, Dict
from techscience_utils.logging_utils import set_logger_level, get_working_environment
from selecting_strategy.module_selection import GlobalState
from nlu.constants import TopicModule
RANDOM_TRUE = 20  # 20% True for random_yes
BACKSTORY_LVL_1_CONFIDENCE_THRESHOLD = 0.95 # This goes to backstory unconditionally.
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())


class BaseTransition:
    """Transition decides which state to move to
       The decision is based on topic handler and intent handlers
    """
    # pylint: disable=too-many-instance-attributes

    def __init__(self, automaton):
        self.automaton = automaton
        self.sources = []
        self.targets = []
        self.targets.append("s_exit_techsciencechat")
        self.targets.append("s_interesting_fact")
        self.targets.append("s_backstory_response")
        self.targets.append("s_change_of_topic")
        # Note current topic and context topic
        # is the detected topic for current fsm
        # need to combine with previous current_topic and context_topic
        return

    def __call__(self, init=False):
        """Running the Transition Code.  Base case implements common features for all transitions.
        For each transition:
        1. Build the intent from text
        2. Determine if backstory response is definitely needed
        3. Detect user wants to change topic within the module

        args - init

        return
            - "s_exit_techsciencechat"
            - "s_backstory_response"
            - "s_change_of_topic"
            - None
        """
        # Detect topic to set the topics
        self._build_topic_handler()
        self._build_chitchat_handler()

        self.detected_topics = self._detect_topic()
        self.construct_context_topics()
        LOGGER.info("Persistant Topic Proposal {}".format(self.propose_topic))

        self.nlu_processor.detect_sentiment(self.module_sentiment_tracker) #TODO

        if self.chitchat_level_detector:
            return None

        if self.is_global_state_entry:
            """Set the Chitchat To That Specific Turn
            """
            self.set_attr("current_topic", "artificial_intelligence") 
            self.chitchat_tracker["curr_turn"] = 1
            self.chitchat_tracker["trace"] = "a"
            self.chitchat_tracker["is_follow_up"] = 0
            self.chitchat_tracker["curr_state"] = 'facts-artificial_intelligence-1'
            self.set_attr("chitchat_state", 'facts-artificial_intelligence-1')
            return "s_interesting_fact"

        
        if self.question_handler and self.question_handler.question_exists:
            self.stop_and_set_chitchat()
            LOGGER.info("TECHSCIENCE TRANSITION Question Exists")
            if self.question_handler.backstory_exists(0.9):
                LOGGER.info("TECHSCIENCE TRANSITION Use Backstory")
                return "s_qa"
            elif self.question_handler.opinion_question:
                # if we can handle, let opinion handle it
                detect_topic = self.detect_topics("opinion")
                LOGGER.info("TECHSCIENCE TRANSITION Detected Opinion Question {}".format(detect_topic))
                if detect_topic:
                    self.set_attr("current_topic", detect_topic)
                    return "s_opinion"

                # if we cannot handle, let backstory handle it
            self.question_handler.backstory_exists(0.61)
            return "s_qa"

        """Topic Request Level.  User Requested Something
        This can be both a command, and a question asking us to do something
        """

        if self.command_detector and self.command_detector.has_command:
            detect_topic = self.detect_topics()
            jumpout = self.jumpout()
            LOGGER.info("TECHSCIENCE TRANSITION Command Exists Jumpout {}".format(jumpout))
            if jumpout is not None:
                return jumpout
            if self.command_detector.exit_techscience:
                LOGGER.info("TECHSCIENCE TRANSITION Exit Techscience")
                self.stop_and_set_chitchat()
                self._set_transition_state(_exit=True, _unclear=False)
                return "s_exit_techscience_chat"
            elif self.command_detector.req_topic:
                
                LOGGER.info("TECHSCIENCE TRANSITION {}".format(detect_topic))
                if detect_topic:
                    self.stop_and_set_chitchat()
                    self.set_attr("current_topic", detect_topic)
                    if self.topic_handler.is_broad_topic(detect_topic):
                        return "s_intro"
                    elif self.topic_handler.is_fact(detect_topic):
                        self.reset_fact_counter()
                        return "s_interesting_fact"
                else:
                    return "s_qa"
                return "s_change_of_topic"
            elif detect_topic is not None:
                # When detect_topic is > 0, we want to ask if the user want to talk about xx
                # IF the detected_topic is not a opinion topic, we are using ask favorite thing to ask if the user
                # wants to talk about the topic
                LOGGER.debug("Detected Topic {}".format(detect_topic))
                self.set_attr("propose_topic", [detect_topic])
                return "s_ask_favorite_thing"
        if self.automaton.is_resume_mode:
            return "s_resume"
        

        expected_next_state =  self.handle_expected_next_state()
        LOGGER.debug("Enter Expect Next State {}".format(expected_next_state))
        if expected_next_state is not None:
            return expected_next_state
        if self.nlu_processor.detect_alexa:
            self.set_attr("current_topic", "alexa")
            return "s_trendy_chitchat"
        user_preference = self.detect_user_preference




        return None

    def propose_new_topic(self):
        new_topics = self.topic_handler.propose_new_topics(None, [self.current_topic], self.context_topic,self.topic_history)
        propose_topic = self.topic_handler.propose_topics_to_list(new_topics)
        self.set_attr("propose_topic", propose_topic)

    @property
    def fact_counter(self):
        return self.automaton.fact_counter
        
    @property
    def fact_counter_limit(self):
        return self.topic_handler.FACT_LIMIT <= self.fact_counter

    def reset_fact_counter(self):
        self.automaton.reset_fact_counter()

    
    def handle_expected_next_state(self):
        if self.expected_next_state is not None and self.yes:
            # if self.expected_next_state == "s_interesting_fact":
            LOGGER.debug("Propose Topic when handling expected next state {}".format(self.propose_topic))
            if len(self.propose_topic) > 0:
                self.set_attr("current_topic", self.propose_topic[0])
                topic_and_sentiment = {topic: self.detected_topics[topic] for topic in self.propose_topic if topic in self.detected_topics}
                if self.current_topic not in topic_and_sentiment:
                    topic_and_sentiment[self.current_topic] = {"pos":1, "neg":0, "neu":0}
                new_topic_history = self.topic_handler.construct_topic_history(self.current_topic, self.topic_history, topic_and_sentiment)
                self.set_attr("topic_history", new_topic_history)
            return self.expected_next_state
        return self.expected_next_state

    @classmethod
    @property
    def _visdata():
        # pylint: disable=no-method-argument
        return NotImplementedError("_visdata is not implemented")

    @property
    def sentiment_monitor(self):
        # self.topic_handler.detect_sentiment(self.module_sentiment_tracker)
        cnt = 0
        if "neg" in self.module_sentiment_tracker:
            for i in self.module_sentiment_tracker["neg"][::-1]:
                if i > 0:
                    cnt += i
                if i == 0:
                    break
        return cnt

    def set_attr(self, attribute, value):
        setattr(self.automaton, attribute, value)


    @property
    def is_global_state_entry(self):
        LOGGER.debug("Current Global State {0}, Expected: {1}".format(self.automaton.global_state, GlobalState.ASK_ONLINE_SHOPPING))
        return self.automaton.global_state == GlobalState.ASK_ONLINE_SHOPPING
        # return True

    @property
    def chitchat_level_detector(self):
        detector = self.chitchat_handler.get_turn_level_detector(self.chitchat_tracker, self.automaton.chitchat_mapping)
        LOGGER.debug("Got Local Level Detectors")
        detector_result = detector(self.automaton)
        LOGGER.debug("Called Local Level Detectors {}".format(detector_result))
        return detector_result

    @property
    def detect_user_preference(self):
        keyword = self.nlu_processor.user_prefer_keyword
        LOGGER.debug("Detected User Preference {}".format(keyword))
        return keyword

    @property
    def question_handler(self):
        return self.automaton.question_handler

    @property
    def command_detector(self):
        return self.automaton.command_detector

    @property
    def nlu_processor(self):
        return self.automaton.nlu_processor

    @property
    def current_topic(self):
        return self.automaton.current_topic

    @property
    def context_topic(self):
        return self.automaton.context_topic

    @property
    def topic_history(self):
        return self.automaton.topic_history

    @property
    def current_keywords(self):
        return self.automaton.current_keywords

    @property
    def propose_topic(self):
        return self.automaton.propose_topic

    @property
    def chitchat_tracker(self):
        return self.automaton.chitchat_tracker

    @property
    def chitchat_state(self):
        return self.automaton.chitchat_state

    @property
    def new_topics(self)->Dict[str, str]:
        return self.automaton.new_topics

    @property
    def expected_next_state(self):
        return self.automaton.expected_next_state

    @property
    def module_sentiment_tracker(self):
        return self.automaton.module_sentiment_tracker

    # @property
    # def user_request_another_topic(self):
    #     return

    def construct_context_topics(self):
        """Construct Context Topic from detected topics, and current_topic
        """

        if self.current_topic is None and len(self.propose_topic) <= 0:
            return

        if self.current_topic is None:
            current_topic = self.propose_topic[0]
        else:
            current_topic = self.current_topic

        # Construct Context Topic
        new_context_topic = self.topic_handler.construct_context_topics(current_topic, self.detected_topics, self.context_topic)
        self.set_attr("context_topic", new_context_topic)

    # @property
    # def detect_retrieval(self):
    #     #TODO URGENT: OPEN QUESTION FACTUAL DETECTION
    #     """Detect whether EVI is required"""
    #     open_question_dialog_act = self.topic_handler.dialog_act_contains(["open_question_factual"], 0.9)
    #     request_regex = self.nlu_processor.topic_specific_request

    #     LOGGER.debug("Retrieval Regex: question_regex:{0}, request_regex:{1}, open_question_dialogact {2}".format(None, request_regex, open_question_dialog_act))

    #     if self.asked_more:
    #         return False

    #     for keywords in self.detected_topics:
    #         if self.is_broad_topic(keywords):
    #             return False

    #     return open_question_dialog_act



    # @property
    # def detect_high_priority_question_handling(self):
    #     """Detect High Priority Question Handling.
    #     All the questions that can be handled by backstory.
    #     """
        
    #     if self.question_handler and not self.question_handler.is_potential_backstory_question():
    #         # It is not a question
    #         return False

    #     for topics in self.detected_topics:
    #         if self.system_can_handle(topics):
    #             return False
    #     if self.question_handler is None:
    #         potential_answer = None
    #     else:
    #         potential_answer = self.question_handler.handle_question()

    #     # Provided Backstory Answer
    #     if potential_answer.backstory_reason == "":
    #         return False
        
    #     return potential_answer is not None

    @property
    def detect_trendy_chitchat(self):
        propose_topic = self.chitchat_handler.detect_keywords(self.new_topics, self.chitchat_tracker,"trend")
        
        if len(propose_topic) > 0:
            LOGGER.debug("Detected Trendy Chitchat {}".format(propose_topic))
            self.set_attr("propose_topic", propose_topic)
            
            return True
        return False

    def _build_topic_handler(self):
        """Build topic handler for the transition
        """
        self.topic_handler = self.automaton.get_topic_handler()
        return self.topic_handler

    def _build_chitchat_handler(self):
        """Build chitchat handler for the state
        """
        # self.detected_topic = self.automaton.detect_topic()
        # return self.detected_topic
        self.chitchat_handler =  self.automaton.get_chitchat_handler(False)
        return self.chitchat_handler

    # def _detect_intents(self):
    #     """Build the sys_intent, lexical_intent, and topic intent from intent_handler
    #     """
    #     text = self.automaton.text
    #     self.sys_intent = self.intent_handler.detect_sys_intent(text)
    #     self.lexical_intent = self.intent_handler.detect_lexical_intent(text)
    #     self.topic_intent = self.intent_handler.detect_topic_intent(text)

    @property
    def get_last_system_response(self):
        """Get Last System Response TODO
        """
        return ""

    def _detect_topic(self):
        """Retrieve the data from topic_handler to local
           calls detect_topic from topic_handler

           Return
            - proposed_topic - a list of topic current text is using
            - new_context - a list of topics with lower confidence
        """
        proposed_current_topics = self.nlu_processor.detect_predefined_keywords()
        LOGGER.info("[TECHSCIENCECHAT] Detected the topics from user utterance: {}".format(proposed_current_topics))
        new_topics = self.nlu_processor.keyword_detection()
        # Remove proposed_current_topics
        _new_topics = set(new_topics.keys())
        for topic in list(proposed_current_topics.keys()):
            if self.topic_handler.is_broad_topic(topic) and any([topic in keyword and topic != keyword for keyword in _new_topics]):
                del proposed_current_topics[topic]
        # Detect New Keywords
        self.update_new_topics(new_topics)
        LOGGER.info("[TECHSCIENCECHAT] Detected the new keywords from user utterance: {}".format(new_topics))
        LOGGER.debug("[TECHSCIENCECHAT] Current topic after detection: {}".format(self.current_topic))
        return proposed_current_topics

    # def _build_intent_handler(self):
    #     """Build the intent_handler for the current FSM call.
    #     """
    #     self.intent_handler = self.automaton.get_intent_handler()
    #     return self.intent_handler

    def update_new_topics(self, new_topics):
        new_topics = {topic:new_topics[topic] for topic in new_topics if not self.system_can_handle(topic)}
        for topic in new_topics:
            if topic not in self.new_topics:
                self.new_topics[topic] = new_topics[topic]
            elif "mentioned" in self.new_topics[topic]:
                self.new_topics[topic] = self.new_topics[topic].replace("mentioned", "using")
            elif "mentioned" in new_topics[topic]:
                self.new_topics[topic] = new_topics[topic]
             

    @property
    def user_demand_topic(self):
        return self.topic_handler.dialog_act_contains(["commands"], 0.8)

    @property
    def _detect_user_change(self):
        """Detect when user wants to change topic within a module

           In this case we want to detect an active change:
           1. I want to talk about {topic}
           2. I don't want to talk about {topic}
           3. Detect User disinterest (Add this later)
           Returns:
            - True:  If user doesn't want to continue on current topic
            - False: User continue on current topic

        Side: Set propose topic depends on user wants to talk about/not want to talk about topic
        """
        neg_intents = {"ask_leave":False, "get_off_topic":False}
        positive_intents = {"ans_wish":False, "ans_like": False}
        # self.get_intents(neg_intents)
        # self.get_intents(positive_intents)
        user_demand = self.user_demand_topic
        # user_demand = self.topic_handler.dialog_act_contains(["commands"])
        user_leave_topic = self.default_combine_intents(neg_intents)
        user_demand_topic = user_demand
        LOGGER.debug("Check user intents")
        LOGGER.debug("User Demand topic {}".format(user_demand_topic))
        LOGGER.debug("User Leave topic {}".format(user_leave_topic))
        # LOGGER.debug("Dialog Act {}".format(self.topic_handler.dialog_act))
        if len(self.detected_topics) == 0:
            topic_detected = []
        else:
            topic_detected = [self.topic_handler.get_highest_topic(self.detected_topics)] # get the topic with highest weight
        LOGGER.debug("Topic Detected {}".format(topic_detected))
        
        if user_leave_topic:
            LOGGER.info("Detect leave topic {0}".format(user_leave_topic))
            jumpout = self.jumpout()
            # Propose topic with negation
            curr_topic = self.current_topic if self.topic_handler.is_broad_topic(self.current_topic) or self.topic_handler.is_topic(self.current_topic) else None
            new_topics = self.topic_handler.propose_new_topics(curr_topic, topic_detected, self.context_topic,self.topic_history)
            self.set_attr("propose_topic", self.topic_handler.propose_topics_to_list(new_topics))
            new_topic_history = self.topic_handler.construct_topic_history(self.current_topic, self.topic_history, {t:0 for t in self.propose_topic})
            self.set_attr("topic_history", new_topic_history)
            if curr_topic is not None:
                new_topic_history = self.topic_handler.construct_topic_history(self.current_topic, self.topic_history, {curr_topic:0})
                self.set_attr("topic_history", new_topic_history)
            self.set_attr("current_topic", None)
            if len(topic_detected) > 0:
                new_topic_history = self.topic_handler.construct_topic_history(self.current_topic, self.topic_history, {t:-1 for t in topic_detected})
                self.set_attr("topic_history", new_topic_history)
            
            if jumpout is not None:
                return jumpout
            return "s_exit_techsciencechat"
        
        if len(topic_detected) == 0:
            jumpout = self.jumpout(unclear=True)
            if jumpout is not None:
                return jumpout
            return None
        if user_demand_topic:
            LOGGER.info("Detect demand topic {0}".format(user_demand_topic))
            self.set_attr("current_topic", topic_detected[0])
            jumpout = self.jumpout(unclear=False)
            new_topic_history = self.topic_handler.construct_topic_history(self.current_topic, self.topic_history, {t:1 for t in topic_detected})
            self.set_attr("topic_history", new_topic_history)
            self.set_attr("propose_topic", [])
            # if it is fact topic, then direct to interesting_fact
            if self.topic_handler.is_fact(topic_detected[0]):
                return "s_interesting_fact"

            if self.chitchat_can_handle(topic_detected[0], "opinion"):
                return "s_opinion"

            if self.topic_handler.is_topic(topic_detected[0]):
                return "s_ask_favorite_thing"

            if self.topic_handler.is_broad_topic(topic_detected[0]):
                curr_topic = topic_detected[0] if self.topic_handler.is_broad_topic(topic_detected[0]) else None
                new_topics = self.topic_handler.propose_new_topics(curr_topic, [], self.context_topic,self.topic_history)
                self.set_attr("propose_topic", self.topic_handler.propose_topics_to_list(new_topics))
                new_topic_history = self.topic_handler.construct_topic_history(self.current_topic, self.topic_history, {t:0 for t in self.propose_topic})
                self.set_attr("topic_history", new_topic_history)
                self.set_attr("current_topic", None)
                return "s_change_of_topic"
            if jumpout is not None:
                return jumpout

            return "s_ask_more"
    

        return None

    @property
    def detect_jumpout_i_like_science(self):
        last_system_response = self.get_last_system_response
        we_proposed_science = len(self.nlu_processor.text_contains(["do you like science"], last_system_response)) > 0
        user_like_science = len(self.nlu_processor.text_contains(["like science", "love science"])) > 0
        but = len(self.nlu_processor.text_contains(["but", "although"])) > 0
        jumpout = self.jumpout()
        LOGGER.debug("Detected Jumpout from {0} with user-like-science {1}, but {2}".format(self.nlu_processor.user_utterance, user_like_science, but))
        return user_like_science and but and jumpout is not None

    def jumpout(self, unclear=False):
        
        # Add jumpout for coronavirus
        if self.detect_coronavirus:
            self.automaton.set_suggest_keywords("corona virus")
            self._set_transition_state(_exit=True, propose_topic="NEWS", _unclear=True)
            self.stop_and_set_chitchat()
            return "s_exit_techsciencechat"

        jumpout_modules = list(self.nlu_processor.detect_jumpout())

        LOGGER.debug("JUMPOUT_MODULES {}".format(jumpout_modules))
        if len(jumpout_modules) > 0:
            LOGGER.info("[TECHSCIENCE] Detected Jumpout {}".format(jumpout_modules))
            self.stop_and_set_chitchat()
            self._set_transition_state(_exit=True, propose_topic=jumpout_modules[0], _unclear=unclear)
            return "s_exit_techsciencechat"
        return None

    def chitchat_can_handle(self, topic, header):
        """Test if the chitchat is a chitchat state and
           Not in completed_chitchat
        """
        can_handle = self.topic_handler.is_chitchat_topic(header, topic)
        if not can_handle:
            return False

        return self.chitchat_handler.chitchat_can_handle(self.chitchat_tracker, header, topic)

    def _set_transition_state(self, _exit=False, propose_topic=None, _unclear=False):
        """ For every transition state, we will set
            1. propose_continue
            2. context_topic
            3. current_topic

            parameters:
                _exit:  True if exit_techscience_chat
        """
        if propose_topic is None:
            propose_topic = self.automaton.system_propose_topic
        if _unclear:
            self.automaton.set_propose_continue("UNCLEAR")
        elif _exit:
            self.automaton.set_propose_continue("STOP")
        else:
            self.automaton.set_propose_continue("CONTINUE")
        
        self.automaton.set_propose_topic(propose_topic)
    def system_can_handle(self, topic=None):
        """ Return True if system can handle the detected_topic
        """
        if topic is None:
            return False
        topic = self.topic_handler.normalize_topic(topic)
        return self.topic_handler.is_broad_topic(topic) or \
               self.topic_handler.is_topic(topic) or \
               self.topic_handler.is_fact(topic)

    def is_broad_topic(self, topic):
        """The detected topic is a broad topic"""

        return topic is not None and self.topic_handler.is_broad_topic(topic)

    @property
    def asked_more(self):
        """Detect if ask more is in the intent"""
        return self.nlu_processor.user_ask_more or self.nlu_processor.yes


    @property
    def yes(self)->bool:
        """Detect when user says yes.

        """
        return self.nlu_processor.yes
    @property
    def ask_more(self):
        """Detects when user demands more
        """
        # pos = "ask_more" in self.topic_intent
        # command = self.topic_handler.dialog_act_contains(["commands"])
        # return pos and command
        return self.nlu_processor.user_ask_more

    @property
    def positive_sentiment(self):
        """
        """
        return self.nlu_processor.contains_pos_sent

    @property
    def no(self)->bool:
        """User Explicitly answer no
        """
        return self.nlu_processor.no

    @property
    def negative_sentiment(self):
        """User Display negative sentiment
        """
        return self.topic_handler.contains_neg_sent

    # def set_chitchat_state(self, chitchat_state, keyword, header):
    #     self.chitchat_tracker[header][keyword] = chitchat_state

    # def get_chitchat_state(self, keyword, header):
    #     """Get the chitchat state for specific keyword

    #     Parameters:
    #       keyword - topic keyword
    #       header - choose between fact, opinion, trend
    #     """
    #     return self.chitchat_tracker[header][keyword]
    

    def save_chitchat(self):
        """Save Chitchat State
        When abrupt leave chitchat, save the states
        """
        return

    def random_yes(self):
        """Random Variable used to generate randomness
        """
        return random.randrange(100) < self.random_true

    def get_current_topic(self, fact_only=True):
        """From detected topic list, get current_topic and context_topic
        """
        LOGGER.debug("Get Current Topic")
        LOGGER.debug("Propose topic {}".format(self.propose_topic))
        LOGGER.debug("Detected topic {}".format(self.detected_topics))
        LOGGER.debug("Topic History {}".format(self.topic_history))
        return self.topic_handler.get_common_topics(
            self.propose_topic, self.detected_topics, self.context_topic, self.topic_history, fact_only)
    
    # def get_intents(self, intent_map):
    #     for k in intent_map:
    #         if k in self.sys_intent or k in self.topic_intent or k in self.lexical_intent:
    #             intent_map[k] = True

    def default_combine_intents(self, intents, thredhold=1):
        return sum([intents[k] for k in intents]) >= thredhold

    def detect_topics(self, chitchat_header=None)->Optional[str]:
        empty = len(self.detected_topics) == 0
        if empty:
            return None
        topic_detected = self.topic_handler.get_highest_topic(self.detected_topics)
        LOGGER.debug("Detected Topics {}".format(topic_detected))
        if self.chitchat_state != "" and topic_detected in self.chitchat_state:
            return None
        if chitchat_header is not None:
            if self.chitchat_can_handle(topic_detected, chitchat_header):
                return topic_detected
            else:
                return None
        else:
            user_specified_topic = list(self.detected_topics.keys()) #TODO: This case user mentioned topic again.  We can add an acknowldgement
            LOGGER.debug("user_specified_topic {}".format(user_specified_topic))
            return user_specified_topic[0] #TODO URGENT: This is close to topic proposal


    @property
    def detect_command_no_fact(self):
        """Detect when user want something, but can't find entity"""
        # LOGGER.debug("Dialog Act {}".format(self.topic_handler.dialog_act))
        commands = self.topic_handler.dialog_act_contains(["open_question_factual", "commands"], 0.9)
        pos_answer = self.topic_handler.dialog_act_contains(["pos_answer"])
        if pos_answer:
            return False
        # question_regex = "user_ask_question" in self.topic_intent
        # user_request_topic = "req_topics" in self.topic_intent
        
        LOGGER.debug("question_regex {0}, commands: {1}".format(None, commands))

        if self.asked_more:
            return False

        if self.nlu_processor.no:
            # When user said no, so we don't take that as a command for a information
            return False

        if not commands:
            return False

        for keywords in self.detected_topics:
            LOGGER.debug(keywords)
            if self.is_broad_topic(keywords):
                return False

        if self.nlu_processor.detect_alexa:
            return False
        return True
    def write_static_transition(self, result_lst):
        result_lst.append({"name":self.name, "sources": self.sources, "targets": list(set(self.targets))})

    def detect_user_claim_wrong(self):
        return self.nlu_processor.user_claim_wrong
        
    def stop_and_set_chitchat(self, storage=True):
        """Stop and Set chitchat
        """
        if storage:
            self.set_attr("chitchat_state", self.chitchat_handler.null_chitchat_state_string)
        else:
            self.set_attr("chitchat_state", self.chitchat_handler.null_chitchat_state)

    def detect_resume(self):
        """ Improve this
        """
        case1 = self.automaton.last_module != self.automaton.MODULE_NAME
        if not case1:
            return False
        self.stop_and_set_chitchat()
        # Reset Sentiment Monitor
        # self.reset_sentiment_monitor()
        # self.reset_new_topics()
        return self.get_intro_chitchat()

    def reset_new_topics(self):
        self.set_attr("new_topics", self.nlu_processor.default_new_topics)

    def reset_sentiment_monitor(self):
        pos = self.module_sentiment_tracker["pos"]
        neg = self.module_sentiment_tracker["neg"]
        neu = self.module_sentiment_tracker["neu"]

        mean_pos = sum(pos)/len(pos) if len(pos) > 0 else 0
        mean_neg = sum(neg)/len(neg) if len(neg) > 0 else 0
        mean_neu = sum(neu)/len(neu) if len(neu) > 0 else 0

        self.module_sentiment_tracker["pos"] = [mean_pos]
        self.module_sentiment_tracker["neg"] = [mean_neg]
        self.module_sentiment_tracker["neu"] = [mean_neu]

    def chitchat_continue(self, chitchat_tracker, storage=False):
        """Returns True if currently chitchat is continuing
           Note, call this after preprocess_chitchat
        """
        # Check if self.chitchat_state and self.chitchat_tracker is equal
        if chitchat_tracker["curr_state"] != self.chitchat_state:
            return False
        if storage:
            return self.chitchat_state != self.chitchat_handler.null_chitchat_state_string

        return self.chitchat_state != self.chitchat_handler.null_chitchat_state

    def map_chitchat_to_state(self, chitchat_tracker):
        """This should be called if chitchat_continue returns True
        """
        header, topic, _id = self.chitchat_handler.preprocess_chitchat_state(self.chitchat_state)

        if header == "facts":
            self.set_attr("current_topic", topic)
            return "s_interesting_fact"
        if header == "opinion":
            self.set_attr("current_topic", topic)
            return "s_opinion"
        if header == "trend":
            return "s_trendy_chitchat"
        return None


    def get_intro_chitchat(self):
        return False
        # self.chitchat_tracker = self.chitchat_handler.preprocess_chitchat_tracker(self.chitchat_tracker)
        # self.chitchat_state = self.chitchat_handler.get_intro_chitchat_tracker(self.chitchat_tracker)
        # self.chitchat_tracker = self.chitchat_handler.postprocess_chitchat_tracker(self.chitchat_tracker)
        # return self.chitchat_state != self.chitchat_handler.null_chitchat_state_string

    @property
    def detect_coronavirus(self):
        """User wants to talk about coronavirus, this is a submodule of jumpout
        """
        return self.nlu_processor.detect_corona