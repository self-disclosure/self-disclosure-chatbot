"""Ask More Transition

Parent State:
  s_ask_more

Next State:
  s_ask_finish
  s_ask_more

Anticipated Responses

    System response - Do you want to hear something cool about {topic}?
    User response - yes/no

    if no:
        - Propose new topic
        - enter chitchat
        - what do you want to talk about?
    if yes:
        - s_interesting_fact
"""
import logging
from techscience_transitions.base import BaseTransition
from techscience_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class AskMore(BaseTransition):
    """Ask more Transition
    """
    name = "t_ask_more"

    def __init__(self, automaton):
        super(AskMore, self).__init__(automaton)
        self.sources = ["s_ask_more"]
        self.targets += ["s_ask_finish", "s_error_handling"]
        return

    def __call__(self, init=False):
        # pylint: disable=broad-except
        try:
            default_next_state = super(AskMore, self).__call__(init)
            LOGGER.info("Using t_ask_more")
            if default_next_state is not None:
                return default_next_state

            not_want = self.no or self.negative_sentiment
            if not_want:
                new_topics = self.topic_handler.propose_new_topics(None, [], self.context_topic,self.topic_history)
                self.set_attr("propose_topic", self.topic_handler.propose_topics_to_list(new_topics))
                self.set_attr("current_topic", None)
                return "s_change_of_topic"

            return "s_ask_finish"
        except Exception as _e:
            LOGGER.critical("Error:  %s".format(_e))
            return "s_error_handling"
