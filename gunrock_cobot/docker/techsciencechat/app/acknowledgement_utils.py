import logging
from techscience_utils.logging_utils import set_logger_level, get_working_environment

LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

# For remote module
class ModuleLevelAcknowledgementWrapper:
    """Utils to handle System level Acknowledgement
    """
    def __init__(self):
        LOGGER.info("Initialized Module Acknowledgement Handler ")
        self.system_acknowledgement = {}
        return

    def build_acknowledgement_handler(self, ack_field):
        LOGGER.debug("Set System acknowledgement: {}".format(ack_field))
        if ack_field is None:
            ack_field = {}

        self.system_acknowledgement = ack_field
        return self

    def ack_available(self, tags_only=[], _not=[]):
        """Returns True if system acknowledgement is available.
        
        Parameter:
          tags_only - a list of tags to find, if empty, return True for all types of tags
        
        Return:
          True/False
        """
        output_tag = self.system_acknowledgement.get("output_tag", "")
        if len(tags_only) == 0:
            return output_tag != ""
        else:
            for tag in tags_only:
                if tag in _not:
                    return False
                if tag == output_tag:
                    LOGGER.info("Tag Detected {output_tag}".format(output_tag=output_tag))
                    return True
        return False

    @property
    def get_ack(self):
        response = self.system_acknowledgement.get("text", "")
        if response is not None:
            response = ''.join([i if ord(i) < 128 or i == ' ' else ' ' for i in response])
            response = response.strip()
        return response
        

    def set_ack_tag(self, tag):
        """Set Acknowledgement Tag.

        """
        self.system_acknowledgement["input_tag"] = tag

    def get_ack_by_tag(self, tag):
        """Deprecated
        """
        output_tag = self.system_acknowledgement.get("output_tag", None)
        if output_tag is None or output_tag != tag:
            return None
        response = self.get_ack
        LOGGER.debug("Output Tag {0}, response {1}".format(output_tag, response))
        return response

    def build_acknowledgement_handler(self, ack_field):
        LOGGER.debug("Set System acknowledgement: {}".format(ack_field))
        if ack_field is None or len(ack_field) == 0 or ack_field[0] is None:
            self.system_acknowledgement = {}
        else:
            self.system_acknowledgement = ack_field[0]
        return self