"""Backstory response

This case is triggered whenever we fit our criteria to generate
backstory response instead

- parent_transition

"""
import logging

from states.base import BaseState
from techscience_utils.logging_utils import set_logger_level, get_working_environment
from nlu.constants import TopicModule

LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class BackstoryResponse(BaseState):
    """Handle Backstory Responses"""
    name = "s_backstory_response"
    position = dict(x="-418.08344", y="10.8853", z="0.0")

    def __call__(self):
        super(BackstoryResponse, self).__call__()
        LOGGER.info("Entered s_backstory_response")
        if self.question_handler is None:
            return self._get_transition
        backstory_response = self.question_handler.handle_question()
        
        if backstory_response is not None and backstory_response.response != "":
            response = backstory_response.response
            if backstory_response.bot_propose_module:
                propose_topic = backstory_response.bot_propose_module.value
            else:
                propose_topic = None
        else:
            response = self.question_handler.generate_i_dont_know_response()
            propose_topic = None

        if propose_topic and propose_topic != TopicModule.TECHSCI:
    
            self.automaton.techsciencechat["propose_continue"] = "UNCLEAR"
            self.automaton.techsciencechat["propose_topic"] = propose_topic
        
        self._add_response_string(response)
        self.automaton.error_response = False

        return self._get_transition

    @property
    def _get_transition(self):
        return self.get_parent_transition()
