"""Opinion State
2.0 Opinion should be used to introduce topics.

Template Response Guidelines
  opinion/*topic:
    Context:  Querky opinion about a *topic.

    User Utterances: Yeah (Response to do you want to talk about robots?)

    Replies:
      - "(<say-as interpret-as=\"interjection\">Great! </say-as> |
         <say-as interpret-as=\"interjection\">
         Cool! </say-as> |<say-as interpret-as=\"interjection\">Awesome! </say-as>)
              I wouldn't exist without science and tech, so it's kind of personal, haha.
              Recently, I've been really into artificial intelligence and computers.
              Which one do ya wanna talk about? "
      - "Tech is a pretty broad and extensive topic. I love technology because I love myself.
         What sparked your interest in tech?"

    Required Fields: None

  opinion_followup/*topic:
    Context:  Follow up to a quirky comment about opinion.
    Should be 1. yes/no question 2. deeper contextual question

    User Utterances:  I want to talk about artificial intelligence.

    Replies:
      - "You are interested in {broad_topic} too?  That is amazing.
         I myself am a big fan of learning about {broad_topic}.
         Can you tell me what about {broad_topic} you like? \
        {topic_suggestion1}, {topic_suggestion2}?"
      - "I see you also love {broad_topic}.  {broad_topic}, especially trending technology,
        is a pretty broad and extensive topic. I love technology because I love myself.
        What sparked your interest in {broad_topic}?"

    Required Fields:  topic_suggestion1, topic_suggestion2, broad_topic
"""
import logging

from states.base import BaseState
from techscience_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class Opinion(BaseState):
    """Opinion State
    """
    name = "s_opinion"
     # pylint: disable=broad-except
    position = dict(x="-400.08344", y="100.8853", z="0.0")

    def __call__(self):

        try:
            super(Opinion, self).__call__()
            LOGGER.info("Entered s_opinion")
            # Note we assume all states entering opinion are checked
            # topic is not None 
            if self.current_topic is None:
                raise Exception("Current topic for opinion is None")
            is_chitchat_topic = self.topic_handler.is_chitchat_topic("opinion", self.current_topic)
            if not is_chitchat_topic:
              opinion_topic = self.get_opinion_topic(self.current_topic)
            else:
              opinion_topic = self.current_topic
            LOGGER.info("Choose Opinion Topic {}".format(opinion_topic))
            if opinion_topic is None:
              self._add_response("opinion/general_opinion", {})
              self.set_attr("expected_next_state", "s_trendy_chitchat")
              return self._get_transition()

            chitchat_tracker = self.chitchat_handler.preprocess_chitchat_tracker(self.chitchat_tracker)
            _chitchat_state = self.chitchat_handler.preprocess_chitchat_state(self.chitchat_state)

            topic_added = False
            if not self.chitchat_continue(chitchat_tracker, _chitchat_state):
              valid_chitchat = self.chitchat_handler.is_valid_chitchat(chitchat_tracker)
              curr_turn = chitchat_tracker["curr_turn"]

              if valid_chitchat and curr_turn >= 0:
                chitchat_tracker = self.chitchat_handler.store_current_state_as_complete(chitchat_tracker)
              
              _chitchat_state = self.chitchat_handler.new_chitchat_state(chitchat_tracker, "opinion", opinion_topic)

              _, chitchat_topic, chitchat_id = _chitchat_state
              if chitchat_topic is not None:
                  self.choose_acknowledgement()
                  selectors = self.topic_handler.map_keyword_to_selector(opinion_topic, "opinion", True, chitchat_id)
                  starter, followup = selectors
                  topic, formatted_topic = self.format_suggest_topics([opinion_topic])
                  self.set_attr("current_topic", topic)
                  slots = {'topic': formatted_topic}
                  self._add_response(starter, slots)
                  self._add_response(followup, slots)
                  chitchat_tracker = self.chitchat_handler.initialize_chitchat(chitchat_tracker, _chitchat_state)
                  topic_added = True
              else:
                _chitchat_state = self.chitchat_handler.null_chitchat_state
                self._add_response("opinion/general_opinion", {})
                self.set_attr("expected_next_state", "s_trendy_chitchat")
            else:
              LOGGER.debug("Entering Chitchat:  {}".format(opinion_topic))
              chitchat_automaton = self.select_chitchat(*_chitchat_state)
              chitchat_tracker = chitchat_automaton.transduce(chitchat_tracker)
              _chitchat_state = chitchat_tracker["curr_state"]
              topic_added = True
              # Check if chitchat is completed
              if chitchat_automaton.get_next_topic() is not None:
                  self.set_attr("current_topic", chitchat_automaton.next_topic)
                  _chitchat_state = self.chitchat_handler.null_chitchat_state # Stop Chitchat Flow and set the topic
                  chitchat_tracker["chitchat_state"] = _chitchat_state
              if chitchat_automaton.is_jumpout:
                  # Propose a different topic from the current
                  self.set_attr("expected_next_state", "s_change_of_topic")
                  self.set_attr("current_topic", None)
            
            self.set_attr("chitchat_tracker", self.chitchat_handler.postprocess_chitchat_tracker(chitchat_tracker))
            self.set_attr("chitchat_state", self.chitchat_handler.postprocess_chitchat_state(_chitchat_state))
            self.automaton.error_response = False

        except BaseException as _e:
            # selector = "opinion/error_response"
            # slots = {}
            LOGGER.error("State Error s_opinion:  {}".format(_e), exc_info=True)
            # self._add_response(selector, slots)
            self.stop_and_set_chitchat()
            self.automaton.error_response = True
        return self._get_transition

    @property
    def _get_transition(self):
        return "t_opinion"