"""Error State

Logically the conversation should never be here.
So it can be triggered by any transitions, however,
it will resort by to a default state.

"""

import logging

from states.base import BaseState

from techscience_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class ErrorState(BaseState):
    """Handle ErrorState"""
    name = "s_error_handling"
    position = dict(x="-200.08344", y="450.8853", z="0.0")

    def __call__(self):
        super(ErrorState, self).__call__()
        LOGGER.info("Entered s_error_state")
        self._add_response('general_error', {})
        self.automaton.error_response = False

        return self._get_transition

    @property
    def _get_transition(self):
        return "t_error_handling"
