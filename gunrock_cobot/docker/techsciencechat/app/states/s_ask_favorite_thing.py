"""Ask Favorite Thing

Parent Transition:
  - t_resume
  - t_intro

Next_Transition:
  - t_ask_favorite_thing

Important Note:  This is part of strict automaton.
We will always keep a consistent topic until the user decide to change
otherwise.

Template Response Guidelines
  favorite/*topics:
    Context:  The answer to, which one do you want to talk about?
              We can present some idea regarding that specific topic.
              <Oh, yeah, > here's something cool about {topic}.
              Do you want to know more about it?

    User Utterances: {topic_suggestions}, neither, none, user_propose new ideas

    Replies:
      - {topic_acknowledgement} + <Oh, yeah, > here's something cool about {topic}.
        Do you want to hear what I've learned??
      (Generally the above context, in the future.  Can separate question with acknowledgement.)
    Anticipated_response:
      - yes/no


"""
import logging

from states.base import BaseState
from techscience_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())


class AskFavoriteThing(BaseState):
    """Implement AskFavoriteThingState
    """
    name = "s_ask_favorite_thing"
    # pylint: disable=broad-except
    position = dict(x="-212.76357", y="245.29176", z="0.0")

    def __call__(self):
        """self.current_topic is the topic set in the previous transition.
           Assume current_topic here is a valid topic from favorite.
        """
        try:
            super(AskFavoriteThing, self).__call__()
            LOGGER.info("Entered s_ask_favorite_thing")
            if self.current_topic is None:
              if len(self.propose_topic) > 0:
                topic, formatted_topic = self.format_suggest_topics(self.propose_topic)
                selector = "reconfirm_user_request"
                slots = {'topic': formatted_topic}
                self._add_response(selector, slots)
                self.automaton.error_response = False
                return self._get_transition
              else:
                raise Exception("Current Topic is None upon entering Ask Favorite Thing")

            if not self.topic_handler.is_topic(self.current_topic):
              
              self.set_attr("current_topic", self.topic_handler.map_fact_to_topic(self.current_topic, "facts"))
            selector = self.topic_handler.map_keyword_to_selector(
                self.current_topic, "favorite")
            if selector is None:
              # Enter General Chitchat
              raise Exception("Selector is none for topic - {}", self.current_topic)
              
            topic, formatted_topic = self.format_suggest_topics([self.current_topic])
            self.set_attr("current_topic", topic)
            self.set_attr("propose_topic",  [])
            slots = {'topic': formatted_topic}
            self._add_response(selector, slots)
            self.automaton.error_response = False

        except Exception as _e:
            LOGGER.error("Ask Favorite Thing State Error:  {}".format(str(_e)), exc_info=True)
            # selector = "favorite/error_response"
            # self._add_response(selector, {})
            self.automaton.error_response = True
            
        return self._get_transition

    @property
    def _get_transition(self):
        return "t_ask_favorite_thing"
