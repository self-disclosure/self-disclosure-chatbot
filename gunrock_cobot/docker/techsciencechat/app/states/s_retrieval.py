"""Backstory response

This case is triggered whenever we fit our criteria to generate
backstory response instead

- parent_transition

"""
import logging
from nlu.constants import DialogAct
from techscience_utils.retrieval_utils import evi_bot
from states.base import BaseState
from chitchat_automatons.retrieval_driven_chitchat import RetrievalDrivenChitchat
from techscience_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class RetrievalResponse(BaseState):
    """Handle Retrieval Responses"""
    name = "s_retrieval"
    position = dict(x="-418.08344", y="10.8853", z="0.0")

    def __call__(self):
        super(RetrievalResponse, self).__call__()
        LOGGER.info("Entered Retrieval State")

        try:
            response = self.get_cached_fact()
            new_topic = self.extract_using_topics()
            if response is not None:
                # Used the cached fact
                self._add_response("acknowledge_fact_continuation", {"topic": "this topic"})
                self._add_response_string(response)
                self.set_attr("expected_next_state", "s_change_of_topic")
            else:
                evi_response = evi_bot(self.automaton.text)

                evi_response = self.split_up_fact(evi_response)                

                if new_topic is not None:
                    keywords = [new_topic]
                else:
                    # Extract noun_phrase
                    keywords = self.topic_handler.get_topics_from_nounphrase()
                # nounphrase.append(self.topic_handler.extract_nounphrase_from_request())
                keyword, formatted_keyword = self.format_suggest_topics(keywords)
                
                if self.system_can_handle(keyword):
                    self.set_attr("current_topic", keyword)

                if evi_response is None:
                    if len(keywords) == 0:
                        self.evi_dont_know()
                    else:
                        added_response = self.add_evi_nounphrase(formatted_keyword)
                else:
                    if self.topic_handler.dialog_act_contains([DialogAct.COMMANDS]):
                        self._add_response("yes_response_to_request", {})
                    elif self.topic_handler.dialog_act_contains([DialogAct.OPEN_QUESTION_FACTUAL]):
                        self._add_response("yes_response_to_question", {})
                    
                    self._add_response_string(evi_response)

                if self.evi_more_fact():
                    self.set_attr("expected_next_state", "s_retrieval")
                    self._add_response("ask_user_opinion", {"topic": formatted_keyword})

            self.automaton.error_response = False

        except Exception as _e:
            LOGGER.error("State Error s_retrieval:  {}".format(_e), exc_info=True)
            self._add_response("general_dont_know", {})
            self.automaton.error_response = True

        return self._get_transition



    def split_up_fact(self, raw_response):
        if raw_response is None:
            return raw_response
        sentences = raw_response.split(". ")
        if len(sentences) > 1:
            self.set_attr("cached_evi", sentences[1:])
            return sentences[0] + ". "

        return raw_response

    def evi_more_fact(self):

        return len(self.cached_evi) > 0

    def get_cached_fact(self):
        if self.evi_more_fact():
            return self.cached_evi.pop(0) + ". "

        return None

    def evi_dont_know(self):
        ack = self.acknowledgement_handler.get_ack_by_tag("ack_question_idk")
        if ack != "":
            self._add_response_string(ack)
        elif self.topic_handler.dialog_act_contains(["yes_no_question"]):
            self._add_response("yes_no_dont_know", {})
        else:
            self._add_response("general_dont_know", {})
        
    def add_evi_nounphrase(self, nounphrase):
        evi_response = evi_bot("what is {0}".format(nounphrase))
        evi_response = self.split_up_fact(evi_response)
        if evi_response is None:
            self.evi_dont_know()
            return
        if self.topic_handler.dialog_act_contains(["yes_no_question"]):
            self._add_response("yes_response_to_request", {})
        else:
            self._add_response("acknowledgement_present_fact", {"topic": nounphrase})

        self._add_response_string(evi_response)
        self._add_response("hope_answered_question", {})

    @property
    def _get_transition(self):
        return "t_retrieval"
