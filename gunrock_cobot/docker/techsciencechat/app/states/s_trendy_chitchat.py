"""Trendy Chitchat

For now: triggered when asked, what's trendy.

Resort to change of topic when trendy is empty.

"""
import logging

from states.base import BaseState
from techscience_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class TrendyChitChat(BaseState):
    """Trendy State
    """
    name = "s_trendy_chitchat"
     # pylint: disable=broad-except
    position = dict(x="-400.08344", y="100.8853", z="0.0")

    def __call__(self):
        super(TrendyChitChat, self).__call__()

        topic_added = False
        LOGGER.info(self.chitchat_tracker)
        try:
            LOGGER.info("Entered s_trendy")
            # Note we assume all states entering opinion are checked
            chitchat_tracker = self.chitchat_handler.preprocess_chitchat_tracker(self.chitchat_tracker)
            _chitchat_state = self.chitchat_handler.preprocess_chitchat_state(self.chitchat_state)
            slots = {}
            if not self.chitchat_continue(chitchat_tracker, _chitchat_state):
                valid_chitchat = self.chitchat_handler.is_valid_chitchat(chitchat_tracker)
                curr_turn = chitchat_tracker["curr_turn"]
                if valid_chitchat and curr_turn >= 0:
                    # This is T Resume
                    
                    self.set_attr("chitchat_tracker", self.chitchat_handler.store_current_state_as_complete(chitchat_tracker))
                if not valid_chitchat:
                    # Start a new chitchat
                    chitchat_state = self.get_trendy_chitchat(chitchat_tracker)
                    chitchat_tracker = self.chitchat_handler.initialize_chitchat(chitchat_tracker, chitchat_state)
                else:
                    chitchat_state = _chitchat_state
                _, chitchat_topic, chitchat_id = chitchat_state
                if chitchat_topic is not  None:
                    _chitchat_state = ("trend", chitchat_topic, chitchat_id)
                    chitchat_tracker["curr_state"] = _chitchat_state
                    chitchat_tracker["curr_turn"] = 0
            if self.chitchat_continue(chitchat_tracker, _chitchat_state):
                LOGGER.debug("Entering Chitchat State {}".format(_chitchat_state))
                chitchat_automaton = self.select_chitchat(*_chitchat_state)
                chitchat_tracker = chitchat_automaton.transduce(chitchat_tracker)
                _chitchat_state = chitchat_tracker["curr_state"]
                # Check if chitchat is completed
                topic_added = True
                if chitchat_automaton.get_next_topic() is not None:
                    self.set_attr("current_topic", chitchat_automaton.next_topic)
                    new_topic_history = self.topic_handler.construct_topic_history(self.current_topic, self.topic_history, {self.current_topic: -1})
                    self.set_attr("topic_history", new_topic_history)
                    _chitchat_state = self.chitchat_handler.null_chitchat_state
            self.set_attr("chitchat_state", self.chitchat_handler.postprocess_chitchat_state(_chitchat_state))
            self.set_attr("chitchat_tracker", self.chitchat_handler.postprocess_chitchat_tracker(chitchat_tracker))
            self.set_attr("error_response", False)

        except BaseException as _e:
            # selector = "trend_chitchat/error_response"
            # slots = {}
            topic_added = True
            self.stop_and_set_chitchat()
            LOGGER.debug("State Error s_trendy_chitchat:  {0}".format(_e), exc_info=True)
            self.set_attr("expected_next_state", "s_exit_techsciencechat")
            self.set_attr("error_response", True)
        
        if not topic_added:
            self._add_response("exit_techscience_chat", slots)
            self.stop_and_set_chitchat()
            self._set_transition_state(True)
            return "t_init_resume"
        
        return self._get_transition

    @property
    def _get_transition(self):
        return "t_trendy_chitchat"

    def get_trendy_chitchat(self, chitchat_tracker):
        """Find a potential topic through context topic.
        """
        chitchat_state = self.chitchat_handler.new_chitchat_state(chitchat_tracker, "trend", self.current_topic, propose_topic=self.propose_topic)
        keyword = chitchat_state[1]
        new_topic_history = self.topic_handler.construct_topic_history(self.current_topic, self.topic_history, {keyword: 0})
        self.set_attr("topic_history", new_topic_history)
        return chitchat_state