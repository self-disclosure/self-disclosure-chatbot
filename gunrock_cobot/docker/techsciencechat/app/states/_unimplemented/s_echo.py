"""Echo state for FSM

Parent Transition:
  - t_sys_intent

Next Transition:
  - t_context
    (or t_change_of_topic)


    def s_echo(self):
        # next_transition = self.t_context.get("next_transition", "t_init")
        next_transition = self.t_context.get(
            "next_transition", "t_change_of_topic")
        return next_transition

"""


from states.base import BaseState

class Echo(BaseState):
    name = "s_echo"

    def __init__(self, automaton):
        super().__init__(automaton)
    def __call__(self):
        return self._get_transition()


    def _get_transition(self):
        next_transition = self.t_context.get(
            "next_transition", "t_change_of_topic")
        return next_transition
