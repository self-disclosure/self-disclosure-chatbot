"""Ask Research State

Parent Transition
  - t_techsci_chitchat

Next Transition
  - t_ask_research

Template Responses
  research/ask_research

"""

from states.base import BaseState

class AskResearch(BaseState):
    name = "s_ask_research"

    def __init__(self, automaton):
        super().__init__(automaton)
    
    def __call__(self):
        slots = {'topic': self.current_topic}
        self._add_response('research/ask_research', slots)
        return self._get_transition()

    def _get_transition(self):
        return "t_ask_research"

"""
Template Response Guidelines
  research/*topics:
    Context:  The answer to, which one do you want to talk about?  We can present
              some idea regarding that specific topic.  <Oh, yeah, > here's something cool about {topic}.  Do you want to know more about it?
  
    User Utterances: {topic_suggestions}, neither, none, user_propose new ideas

    Replies: 
      - Have you heard the recent research advancements regarding {topic}?
      (Generally the above context, in the future.  Can separate question with acknowledgement.)
    
    - "Oh yeah, do you wanna know about some research about {topic} that I came across? "
    - "Hey, do you want me to tell you about some research about {topic} that I recently red? "
    - "Say, wanna know about some cool findings about {topic} that I red recently?  

    Anticipated_response: 
      - yes/no

    Required fields:
      - topic
  
Old Template Responses
"""