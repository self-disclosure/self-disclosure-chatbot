"""UserChangeModule

When the user temporarily decides to talk about something else.  

Parent Transition:
  - t_intro
  - t_ask_favorite_thing
  - t_interesting_fact
  - t_ask_research
  - t_interesting_research
  - t_techsci_chitchat
  - t_opinion

System Responses

Next_Transition:
  - t_resume
"""

from states.base import BaseState
from utils.data_topic import MODULE_TO_RESPONSE, TOPIC_TO_MODULES

RESPONSE_STRING = "Do you want to talk about {topic} instead of science and technology?"

class UserChangeModule(BaseState):
    default_topic = "other topics"
    name = "s_user_change_module"

    def __init__(self, automaton):
        super().__init__(automaton)

    def __call__(self):
        other_topic = self.get_other_module_utterance()
        self._add_response_string(RESPONSE_STRING.format(other_topic))
        self._set_next_state()
        return self._get_transition()


    def _get_transition(self):
        return "t_resume"

    def _set_next_state(self):
        """ Note this state changes the user attribute since this state may jump out module"""

        self.automaton.techsciencechat["propose_continue"] = "UNCLEAR"
        self.automaton.techsciencechat["propose_topic"] = TOPIC_TO_MODULE[
            self.intent_classify_topic]
            
        self.automaton.techsciencechat["current_topic"] = self.automaton.current_topic
        self.automaton.techsciencechat["context_topic"] = self.automaton.context_topic

        self.automaton.techsciencechat["fact_counter"] = self.automaton.fact_counter
        self.automaton.techsciencechat["chitchat_counter"] = self.automaton.chitchat_counter


"""

Template Response Guidelines
  change_of_topic/broad_topic_starter_passive:
    Context:  User manually asked to change the topic.  Or express doubt about talking about technology.
              We can either ask: what do you want to talk about?  Or suggest a topic.
  
    User Utterances: Can we talk about something else?

    Replies:
      - "Of course!  What do you want to talk about?"

    Anticipated_response: 
      - yes/no TODO not added yet
      - {topic_suggestions}, neither, none, user_propose new ideas.
  
Old Template Responses
"""