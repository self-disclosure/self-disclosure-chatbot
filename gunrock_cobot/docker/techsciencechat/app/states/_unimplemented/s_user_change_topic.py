"""
    def s_user_change_topic(self):
        self.response += template_techsci.utterance(selector='user_change_topic/default',
                                                    slots={
                                                        'topic': self.current_topic},
                                                    user_attributes_ref=self.user_attributes_hash)
        self.context_topic = self.current_topic
        return "t_user_change_topic"
"""

"""
Template Response Guidelines
  Comment:  Every transition should have a detector for #TODO can we talk about something else?
            I don't want to talk about topic anymore.
            I don't want to talk about topic... and so on.
            
  user_change_topic/user_wants_to_change_topic:
    Context:  User manually asked to change the topic.  Or express doubt about talking about technology.
              We can either ask: what do you want to talk about?  Or suggest a topic.
  
    User Utterances: Can we talk about something else?

    Replies:
      - "Of course!  What do you want to talk about?"

    Anticipated_response: 
      - yes/no TODO not added yet
      - {topic_suggestions}, neither, none, user_propose new ideas.
  
  change_of_topic/change_topic_passive
    Context: User is interested in something else.  We detect a desire to change topic.  
  
Old Template Responses
"""