"""InterestingResearch for the FSM

Parent Transition:
  - t_ask_research
  - t_opinion
  - t_intro
  - t_interesting_research

Template Responses:
    favorite/know_nothing:
      - " Aw, that's okay. We are all learners here. I didn't know much about {topic} until a couple of weeks ago myself.
       Wanna know more about {topic}? "
    favorite/
      - default
      - artificial_intelligence_topic
      - engineering_topic
      - biology_topic
      - physics_topic
      - chemistry_topic
      - astronomy_topic
      - medicine_topic
      - computers_topic
      - general_topic

     
Next_Transition:
  - t_interesting_research

"""

from states.base import BaseState
# from utils.topic_utils import topic_exists_in_opinion

NO_FACT_RESPONSE = "Oh boy! All this thinking has made me forget a few things. " \
                                 "Do you mind talking about something else for now?"

class InterestingResearch(BaseState):
    name = "s_interesting_research"

    def __init__(self, automaton):
        super(InterestingResearch, self).__init__(automaton)


    def __call__(self):
        # Get response from retrieve_kg_topic
        slots = {'topic': self.current_topic}
        selector = self.topic_handler.map_topic_to_selector(self.current_topic, "facts")
        slots = {'topic': self.current_topic}
        self.add_ack_if_possible()
        self._add_response('facts/start', slots)
        self._add_response(selector, slots)
        self._add_response('facts/end', slots)
        return self._get_transition()

        return self._get_transition(NO_FACT_RESPONSE)

    def retrieve_remote_research(self):
        retrieved_response = self.retrieve_kg_topic()
        if not retrieved_response:
            retrieved_response = self.retrieve_evi("Research in")
        if retrieved_response:
            self._add_response_string(retrieved_response)
        # return
    def get_transition(self):
        return "t_interesting_research"

"""

Template Response Guidelines
  {topic}/facts:
    - A list of facts
    Context:
      - The user said yes on do you want to know more about *
      - We will add more acknowledgements.
      *Note:  I don't want to add ask_more here.  TODO.

"""