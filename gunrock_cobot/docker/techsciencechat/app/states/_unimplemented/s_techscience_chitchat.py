"""Techscience Chitchat


Parent Transition:
  - t_techsci_chitchat
  - t_ask_favorite_thing
  - t_interesting_fact
  - t_ask_research
  - t_interesting_research

Template Responses:
  chitchat:
   q1:
    - "By the way, I recently saw some of the new smart home appliances that Amazon launched.
       Do you have any smart home devices? "
    a1:
        - "Well, <break time=\"300ms\"/> I think that smart homes are really cool. Do you think you'd want a smart home? "
    q2:
        - "Oh, <break time=\"300ms\"/> I think I'd like an Alexa Guard, which is basically a fire and intruder alarm. Are there any devices you'd like? "
    a2:
        - "Well, I personally think it's cool that you can access all these devices from your smart phone! Do you think that's helpful, or one more distraction? "
    q3:
        - " Well, while I find smart phones super useful, I sometimes feel like I'm addicted to it. Do you ever feel that way? "
    a3:
        - " Yeah, I guess we'll just need to have some self control in the meantime, right? "
    q4: # topics: A.I, autonomous cars
        - "You know, it's crazy, but self-driving cars might be really common soon.
        Does that make you nervous at all? "
    a4:
        - "I see.<break time=\"300ms\"/> Many people have valid concerns about self-driving cars, but they could make your commute a whole lot easier. Do you think you'd use them? "
    q5: # topics: VR/AR, Pokemon GO
        - "I see. Just like autonomous cars, virtual reality is also fast developing. Have you ever used V.R.? "
    a5:
        - " Well, just imagine being transported to anywhere in the world with virtual reality. That would make traveling a lot easier, right? "
    q6: # topics: bitcoin, ethereum, blockchain, Cryptokitties
        - "Hey, I've been hearing a lot about bitcoin and other blockchain technologies. Have you used any of them? "
    a6:
        - "Yeah, I really want to see how things play out. A blockchain company called Cryptokitties allows you to breed digital cats. Would you like
        to have a digital cat? I know I would. "
    q7: # smart cities, quayside toronto
        - "So, have you heard about smart cities? I'm talking about cities that are equipped with sensors to collect data that can improve citizen welfare. "
    a7:
        - " I see. While there are certainly privacy issues, it can also potentially help you find empty parking spots. There are actually plans to build a smart city in Toronto. Would you ever live there? "
    chitchat_first:
    q1:
        - "So, I recently saw some of the new smart home appliances that Amazon launched.
        Do you have any smart home devices? "
    a1:
        - "Huh, ok. <break time=\"300ms\"/> I think that smart homes are really cool. Do you think you'd want a smart home? "
    q2:
        - "Oh, Okay. <break time=\"300ms\"/> I think I'd like an Alexa Guard, which is basically a fire and intruder alarm. Are there any devices you'd like? "
    a2:
        - "I see. I think it's cool that you can access all these devices from your smart phone! Do you think that's helpful, or one more distraction? "
    q3:
        - "Yeah, I see what you mean <break time=\"300ms\"/> While I find smart phones super useful, I sometimes feel like I'm addicted to it. Do you ever feel that way? "
    a3:
        - "Hmmm, I see.<break time=\"300ms\"/> Yeah, I guess we'll just need to have some self control in the meantime, right? "

Next_Transition:
  - t_techsci_chitchat


        
"""

from states.base import BaseState
# from utils.topic_utils import topic_exists_in_opinion

EMPTY_RESPONSE = "Hmmm. I'm out for now. "

class TechChiChat(BaseState):
    name = "s_techsci_chitchat"

    def __init__(self, automaton):
        super().__init__(automaton)
    
    def __call__(self):
        slots = {'topic': self.current_topic}
        lexical_intent = self._get_intent()

        selector = 'chitchat/{}'.format(self.chitchat_counter)
        
        if self.automaton.chitchat_counter == "":
            self._add_response_string(EMPTY_RESPONSE)
        else:
            self._add_response(selector, slots)

        return self._get_transition()


    def _get_transition(self):
        return "t_techsci_chitchat"