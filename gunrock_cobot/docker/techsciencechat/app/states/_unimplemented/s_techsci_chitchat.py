"""Techsci Chitchat

Chitchat is the final state for rule-based FSM.  When user doesn't want to talk more about
hard facts or hard research, chitchat alleviates the tension.


    def s_techsci_chitchat(self):
        lexical_intent = detect_intent(self.text)
        if self.chitchat_counter == "":
            self.response += "Hmmm. I'm out for now. "
        else:
            self.response += template_techsci.utterance(selector='chitchat/{}'.format(self.chitchat_counter),
                                                        slots={
                                                            'topic': self.current_topic},
                                                        user_attributes_ref=self.user_attributes_hash)
            # if "answer_yes" in lexical_intent and self.chitchat_counter not in ["q1", "q4", "a4"]:
            #    ack = " (Oh yeah|Yes)! I agree! "
            #    self.response = ack + self.response
        return "t_techsci_chitchat"
"""