"""Ask Finish Conversation

Parent Transition:
  - t_ask_finish

Template Responses:
  Context:
  default:
    - " I see. Well, I already feel wiser. Thank you,  for sharing this with me. "
    - " Oh, I see! It feels good to have learnt something. Thank you,  for making me wiser."
    - " Ah, that's so interesting. I had never thought about it.
        Thank you, for educating me."
    - " Ah! It makes perfect sense. Thank you,
        I can't wait to share this information with my friends."
  know_nothing:
    - " Oh, I see. Hmmm, I think I should read up on that. "
    - " Ah! I guess we I'll look it up later. It's better to be caught up with these things. "
    - " Oh! Then I guess I'll just read about it later. Thank you for letting me know. "

Next_Transition:
  - t_change_of_topic

"""
import logging

from states.base import BaseState
from techscience_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())


class AskFinish(BaseState):
    """Implement Ask Finish
    """
    # pylint: disable=broad-except
    name = "s_ask_finish"
    position = dict(x="-100", y="200.8853", z="0.0")

    def __call__(self):
        try:
            super(AskFinish, self).__call__()
            LOGGER.info("Entered s_ask_finish")
            # self._add_response('ask_finish/default', {})
            self.choose_acknowledgement()
            for topic in self.new_topics:
              if "using" in self.new_topics[topic]:
                _topic, formatted_topic = self.format_suggest_topics([self.current_topic])
                self.set_attr("current_topic", _topic)
                slots = {'topic': formatted_topic, "user_mentioned_topic": topic}
                self.new_topics[topic] = self.new_topics[topic].replace("using", "used")
                self._add_response('ask_finish/default', slots)            
                return self._get_transition
            slots = {}
            self._add_response('ask_finish/know_nothing', slots)           
            self.automaton.error_response = False
            return self._get_transition

        except Exception as _e:
            LOGGER.debug("Ask Finish State Error:  {}".format(str(_e)), exc_info=True)
            self._add_response('ask_finish/error_response', {})
            self.automaton.error_response = True

        return self._get_transition

    @property
    def _get_transition(self):
        return "t_ask_finish"
