"""Exit Techscience Chat

This is when the system detects exiting techscience chitchat.
1. When the user doesn't seem to draw much interest in this topic.
2. The system prompts a change of topic.  (Use user's previous utterance to 
   change a different topic.)

    Parent Transition:

    Intentional:
        - t_ask_favorite_thing
        - t_interesting_fact
        - t_ask_research
        - t_interesting_research

    Next_Transition:
  - t_resume

Template Response Guidelines
  This state is triggered whenever the use actively desire to leave a module.
  In this case, we should tell the system to leave the module

  
Old Template Responses
"""
import logging

from states.base import BaseState

from techscience_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class ExitTechScienceChat(BaseState):
    """ExitTechscience State
    """
    # pylint: disable=broad-except
    name = "s_exit_techsciencechat"
    position = dict(x="50.76357", y="400.29176", z="0.0")

    def __call__(self):
        super(ExitTechScienceChat, self).__call__()
        LOGGER.info("Entered s_exit_techsciencechat")
        slots = {}
        selector = "exit_techscience_chat"
        raw_propose_topic = self.automaton.get_propose_topic
        propose_topic = self.topic_handler.module_to_response(raw_propose_topic)
        LOGGER.debug("Propose Topic {}".format(propose_topic))
        if self.automaton.techsciencechat.get("propose_continue") == "UNCLEAR":
          if propose_topic is None:
            propose_topic = "something else"
          elif "news" in propose_topic and self.automaton.get_keywords_from_response == "corona virus":
            propose_topic = "news regarding corona virus"
          selector = "exit_techscience_unclear"
          slots = {"topic":propose_topic}
        elif propose_topic is not None:
          self.choose_acknowledgement()

        self._add_response(selector, slots)
        self.automaton.error_response = False

        return self._get_transition

    @property
    def _get_transition(self):
        return "t_init_resume"
