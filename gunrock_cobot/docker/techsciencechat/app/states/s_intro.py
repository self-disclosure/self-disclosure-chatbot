"""S_Intro State for the FSM

Parent Transition:
  - t_init

Next_Transition:
  - t_intro


Template Response Guidelines
  intro/new_topic_starter_unknown:
    Context:  The user entered topic techscience chat, with a specific topic

    User Utterances: The user should be adamant when talking about {topic}

    Potential Acknowledgements:
    - "Um", "Yeah", "Ooh lala"
    Some agreement but with an address for concern

    Replies:
    - Just a teeny tiny problem, to tell you the truth, I don't really know that much about {topic} yet.
    - so... I actually don't really know much about {topic}.
    - actually, I've spent quite some time learning about {topic}

    Followup:
    - Wanna tell me more about it or do you want to talk about something else?
    - Could you teach me about it?  Or would you wanna talk about something else?
    - Can you tell me what you find interesting about {topic}?  I am very happy to learn more.

    Required Fields: topic

    Anticipated_response:
    - I want to talk about {new_topic}.
    - I want to talk about something else.
    - no
    - {new_topic} is  xxx.
    - I found {new_topic} cool.  (Some fact about topic).

  intro/broad_topic_starter_passive:
    Context:  The user said yes to a project we proposed.  This is proposed by selecting strategy.  We
              want to direct to a list of topic we can handle.

    User Utterances:  Sure, yes, of course.  

    Potential Acknowledgements:
    - Anything positive.

    Replies:
      - " Finally, someone who is up my alley."
      - " Legend says, knowledge is the power that light up my world.  And OUR world. High five! I am Totally Kidding,
         I don't have hands."
      - "I am so glad you are into science and technology. I love technology like I love myself. hehe"

    Followup:
      - " Recently, I've been really into {topic_suggestions}.  I am still trying to wrap my mind around them.  ooh ask me something about computers, let's see if I know them."
      - " I can't wait for technology to be advanced enough so everyone in the world can benefit from it.  {topic_suggestions}, they are all so amazing to think about.  I can't wait to leanr more  Haha, I lost my train of thoughts, what do you want to talk about again?"
      - " I am super into {topic_suggestions}. Which one do ya wanna talk about?"
 

    Required Fields:  topic_suggestions, broad_topic

    Anticipated_responses:
      - {topic_suggestions}, neither, none, user_propose new ideas. s_ask_favorite_thing
      - {user_generic_response} --> may detect unheard topic --> ask_more

    New Ideas:
      - "You mean, you are talking TO artificial intelligence?  Get it?" (Custom response)


  intro/broad_topic_starter_active:
    Context: The user mentioned *topic we can handle.  We should address that and continue on

    User Utterances:  I want to talk about technology.

    Replies:
    - "<say-as interpret-as=\"interjection\">Awesome! </say-as> I (love talking |am excited to talk)
      about {topic}.
      Tell me what you find interesting about {topic}. Let's see if I know it. "
    - "You are interested in {broad_topic} too?  That's amazing.
       I am so glad there are a lot of people in the world
      interested in {broad_topic}.  <say-as interpret-as=\"interjection\">
      Ah! </say-as>
      can you tell me what you find interesting about {topic}.
      I am willing to learn."
    - "<say-as interpret-as=\"interjection\">Great! </say-as> {topic}
      is <emphasis level = \"strong\">so</emphasis> interesting.
      Let me think a little."

    Required Fields: broad_topic, topic

    Anticipated_responses:
      - generic response about *topic

  system_error_response:
    Context:  The system is throwing some error.  So we have to respond something.

    User Utterance:  Anything above

    Replies:
      - "{broad_topic} is a large area.  Someone with good grasp in {broad_topic}
         must be very smart and curious.
         I like to ponder the implication of our realm of knowledge.
         Sorry, I am turning into Sheldon Cooper."

"""
import logging
import random
from states.base import BaseState
from techscience_utils.logging_utils import set_logger_level, get_working_environment
from techscience_utils.retrieval_utils import evi_bot

LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class Intro(BaseState):
    """Intro State
    """
  # pylint: disable=broad-except
    name = "s_intro"
    position = dict(x="-250.82776", y="100.6904", z="0.0")

    

    def __call__(self):
        super(Intro, self).__call__()
        try:
            LOGGER.info("Entered s_intro")
            self._get_selector_from_topic()
            LOGGER.debug("S_INTRO PROPOSE TOPICS {}".format(self.propose_topic))
            self.automaton.error_response = False

        except Exception as _e:
            LOGGER.error("State Error s_intro:  {}".format(_e), exc_info=True)
            # selector = "intro/error_response"
            # broad_topic = self.get_broad_topic(self.current_topic)
            # slots = {"broad_topic": broad_topic}
            self.automaton.error_response = True
        return self._get_transition

    @property
    def _get_transition(self):
        return "t_intro"

    def _get_selector_from_topic(self):
        """Intro is when a user first enter the automaton.  Note the current_topic
           is already decided to be either level 1 or level 2 topic or None.

        Return
          - selector
            - intro/broad_topic_starter_passive
            - intro/broad_topic_starter_active
        """
        # first_time = self.is_first_time_user
        if self.no_detected_topic(): # No current topic
          broad_topic = None
        elif self.system_can_handle():
          broad_topic = self.get_broad_topic(self.current_topic)
        else:
          broad_topic = "technology"

        self.propose_topic_list = []

        if self.current_topic == "ipads" and self.trendy_route(self.current_topic):
          return
        # Note we can get the previous topic from last text (so detect topics full TODO)
        last_response = self.get_last_system_response
        sys_fav = self.mentioned_favorite_technology(last_response)
        user_favorite = self.nlu_processor.user_favorite
        propose_topic_dict = self.topic_handler.propose_new_topics(broad_topic, [], self.context_topic, self.topic_history)
        self.propose_topic_list = self.topic_handler.propose_topics_to_list(propose_topic_dict)
        topic, formatted_topic = self.format_suggest_topics(self.propose_topic_list)
        self.propose_topics_string = formatted_topic
       
        if sys_fav:
          # Express acknowledgement on common interests
          self.map_question_to_module = {"s_opinion": self.opinion_route, "s_interesting_fact": self.facts_route}
          self.choose_acknowledgement()
          keys = list(self.map_question_to_module.keys())
          LOGGER.info(" {} ".format(keys))
          self.set_attr("expected_next_state", random.choice(keys))
          LOGGER.info(" {} ".format(self.expected_next_state))
          success = self.map_question_to_module[self.expected_next_state]()
          LOGGER.info("Expected Next State: {}".format(self.expected_next_state))
          if success:
            return
        sys_mentioned_specific_topic = self.mentioned_specific_topic(last_response)
        LOGGER.info("[TECHSCIENCE INTRO] Selected topic by system proposal {}".format(sys_mentioned_specific_topic))
        if sys_mentioned_specific_topic is not None:
          topic, formatted_topic = self.format_suggest_topics([sys_mentioned_specific_topic])
          self.propose_topics_string = formatted_topic
          if topic == "ipads" and self.trendy_route(topic):
            return
          LOGGER.debug("Before transition {}".format(topic))
          if not self.topic_handler.is_fact(topic):
            fact_topic = self.topic_handler.find_fact_for_topic(topic, self.topic_history)
            topic, formatted_topic = self.format_suggest_topics([fact_topic])
            self.propose_topics_string = formatted_topic
          LOGGER.debug("After transition {}".format(topic))
          if self.topic_handler.is_fact(topic) and self.facts_route():
            self.propose_topic_list = [topic]
            self.set_attr("expected_next_state", "s_interesting_fact")
            return

        if len(self.automaton.text.split(" ")) == 1:
          if self.topic_handler.is_chitchat_topic("opinion", self.current_topic):
            if self.opinion_route():
              self.set_attr("expected_next_state", "s_opinion")
              return

          if self.topic_handler.is_fact(self.current_topic):
            if self.facts_route():
              self.set_attr("expected_next_state", "s_interesting_fact")
              return
        if len(self.automaton.new_topics) > 0:
          new_t = list(self.automaton.new_topics.keys())[0]
        else:
          new_t = ""

        if self.nlu_processor.user_like and self.evi_available(new_t):
          if self.facts_route():
            self.set_attr("expected_next_state", "s_retrieval")
            return


        # user_agree_nonchalant = self.topic_handler.detect_nonchalant

        # if user_agree_nonchalant:
        #   return

        # if sys_fav and self.user_agree_neutral:
        #   # user said yes or yes i do
        #   self.map_question_to_module = {"s_opinion": self.opinion_route, "s_interesting_fact": self.facts_route}
        #   self.choose_acknowledgement()
        #   keys = list(self.map_question_to_module.keys())
        #   LOGGER.info(" {} ".format(keys))
        #   self.expected_next_state = random.choice(keys)
        #   LOGGER.info(" {} ".format(self.expected_next_state))
        #   self.map_question_to_module[self.expected_next_state]()
        #   return


        new_topic_history = self.topic_handler.construct_topic_history(self.current_topic, self.topic_history, {t:0 for t in self.propose_topic_list})
        LOGGER.debug("S_INTRO NEW TOPICS {}".format(self.new_topics))
        self.set_attr("topic_history", new_topic_history)
        if self.no_detected_topic() and len(self.new_topics) == 0:
            self.set_attr("current_topic", None)
            selector = 'intro/broad_topic_starter_passive'
            
            slots = {"topic_suggestions": self.propose_topics_string}
            self.set_attr("propose_topic", self.propose_topic_list)

        elif self.is_broad_topic() and len(self.new_topics) < 1:
            selector = 'intro/broad_topic_starter_active'
            slots = {
                "topic_suggestions": self.propose_topics_string,
                "broad_topic": self.current_topic}
            self.set_attr("propose_topic", self.propose_topic_list)
            self.set_attr("current_topic", None)            
        # elif len(self.new_topics) > 0:
        #     selector = 'intro/new_topic_starter_unknown' # TECHSCIENCE: TODO
        #     # Get the topic
        #     new_topic, formatted_new_topic = self.format_suggest_topics(list(self.new_topics.keys()))
        #     self.set_attr("propose_topic", [])
        #     self.set_attr("current_topic", topic)
        #     slots = {"topic": formatted_new_topic, "broad_topic": broad_topic}
        #     self.new_topics[new_topic] = self.new_topics[new_topic].replace("unused", "mentioned")
        else:
            self.set_attr("current_topic", None)
            selector = 'intro/broad_topic_starter_passive'
            
            slots = {"topic_suggestions": self.propose_topics_string}
            self.set_attr("propose_topic", self.propose_topic_list)

        self._add_full_response(selector, slots)
        LOGGER.info("Used selector: {}".format(selector))
        return 

    def opinion_route(self):
      try:
        # Express why I like about technology
        self._add_response('intro/favorite_part_technology', {})
        # Ask user what's their opinion on artificial intelligence 
        self._add_response('intro/ask_user_opinion_technology', {})
        self.set_attr("current_topic", "technology")
        return True
      except:
        return False

    def facts_route(self):
      try:
        # Express why I like about technology
        self._add_response('intro/propose_new_research', {"topic":self.propose_topics_string})
        new_topic_history = self.topic_handler.construct_topic_history(self.current_topic, self.topic_history, {t:1 for t in self.propose_topic_list})
        self.set_attr("topic_history", new_topic_history)
        self.set_attr("propose_topic", self.propose_topic_list)
        # Ask user what's their opinion on artificial intelligence 
        return True
      except Exception as e:
        LOGGER.debug("TECHSCIENCE MINOR BUG IN facts route {}".format(e))
        return False

    def trendy_route(self, topic=None, start=False):
      new_topic_history = self.topic_handler.construct_topic_history(self.current_topic, self.topic_history, {t:1 for t in self.propose_topic_list})
      # Set chitchat state and chitchat tracker
      # if start:
      #   chitchat_state = ["trend", "ipads", 2]
      # else:
      chitchat_state = ["trend", "ipads", 2]
      self.set_attr("topic_history", new_topic_history)
      
      chitchat_tracker = self.chitchat_handler.preprocess_chitchat_tracker(self.chitchat_tracker)
      chitchat_tracker = self.chitchat_handler.initialize_chitchat(chitchat_tracker, chitchat_state)
      if chitchat_state[1] is not  None:
          self.set_attr("expected_next_state", "s_trendy_chitchat")
          chitchat_tracker["curr_state"] = chitchat_state
          chitchat_tracker["curr_turn"] = 0
          chitchat_automaton = self.select_chitchat(*chitchat_state)
          chitchat_tracker = chitchat_automaton.transduce(chitchat_tracker)
          chitchat_state = chitchat_tracker["curr_state"]
          self.set_attr("chitchat_state", self.chitchat_handler.postprocess_chitchat_state(chitchat_state))
          self.set_attr("chitchat_tracker", self.chitchat_handler.postprocess_chitchat_tracker(chitchat_tracker))
          return True
      
      return False
      
    def evi_available(self, topic):
      evi_response = evi_bot("what is {0}".format(topic))
      return evi_response is not None

