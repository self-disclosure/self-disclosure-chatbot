"""S_Resume

Resume State should address the user, ask if they want to talk about the topic again.

"""
import logging

from states.base import BaseState
from techscience_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class ResumeState(BaseState):
    """Intro State
    """
  # pylint: disable=broad-except
    name = "s_resume"
    position = dict(x="-250.82776", y="100.6904", z="0.0")

    def __call__(self):
        super(ResumeState, self).__call__()
        try:
            LOGGER.info("Entered s_resume")
            # What should we do to resume topic?
            # Propose topic from history

            # current topic
            if self.current_topic is not None:
                # if self.chitchat_continue:
                #     return #TODO IMPLEMENT CHITCHAT RESUME
                if self.topic_handler.is_chitchat_topic("facts", self.current_topic):
                    selector = "techscience_user_conversation_continue"
                    slots = {"topic": self.current_topic}
                else:
                    selector = "techscience_user_conversation_continue"
                    slots = {"topic": self.current_topic}
                # we were talking about {} last time.  Do you want to continue out conversation?
                self.set_attr("propose_topic", [self.current_topic])
                self.set_attr("current_topic", None)
            else:
                LOGGER.debug("SRESUME {}".format(self.topic_history))
                # I remember you like {topic}, are you interested in topic 2?
                topic_history = self.topic_history[:]
                topic_history.sort(key=lambda x: x[1])
                LOGGER.debug("Sorted Topic History {}".format(self.topic_history))
                LOGGER.debug("Context Topic {}".format(self.context_topic))
                
                top_topic = topic_history[0]
                if top_topic[1] > 0:
                    selector = "techscience_user_like_resume_propose"
                    propose_topic = self.context_topic.get(top_topic[0])
                    if propose_topic is None:
                        propose_topic = self.topic_utils.propose_on_default(top_topic[0])
                    slots = {"last_topic": top_topic[0]}
                else:
                    selector = "techscience_user_mentioned_resume_propose"
                    slots = {"last_topic": top_topic[0]}
                self.set_attr("propose_topic", [top_topic[0]])
            # Lead into a new conversation or continue previous topic

            self._add_response(selector, slots)
            
            self.automaton.error_response = False

        except Exception as _e:
            LOGGER.debug("State Error s_resumme:  {}".format(_e), exec_info=True)
            # selector = "resume/error_response"
            self.automaton.error_response = True

        return self._get_transition

    @property
    def _get_transition(self):
        return "t_resume"
