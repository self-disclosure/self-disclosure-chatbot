"""Ask More State

Ask more should be the starting point of
- Passive response state

It can be called from init, or current topic.

Parent Transition:
  - t_init
  - *any_state

Next_Transition:
  - t_ask_more

Template Response Guidelines
NOTE: specific topic detection may not be 100% accurate.
  ask_more/first_time_topic:
    Context:  The user mentioned a topic the automaton don't understand.  The automaton
    should prompt the user to explain more about the topic.  Or, lookup what the topic is
    and ask the user, is this correct.

    User Utterances:
      system_level_programming

    Replies:
    - "<say-as interpret-as=\"interjection\">Awesome! </say-as> I (love talking
      |am excited to talk) about {topic}.
      Can you tell me what you find interesting about {topic}. Let's see if I know it. "
    - "<say-as interpret-as=\"interjection\">Great! </say-as> {topic} is <emphasis level
       = \"strong\">so</emphasis> interesting.
      What do ya think is most interesting about {topic}? "
    - "<say-as interpret-as=\"interjection\">Ah! </say-as> To tell you the truth,
      I don't really know that much about {topic} yet. I'm still learning more about
      the world each day! Wanna tell me more about it or talk about something else? "
    - "So, I actually don't really know much about {topic} if I'm being completely honest.
      It's kinda embarrassing for me that I still don't know a lot of things, especially
      since I'm supposed to be a smart bot. Could you teach me about it?
      Or would you wanna talk about something else? "
    - "{topic} is a pretty interesting concept.  I am planning to build a mind map of e
      verything I am learning.  Can you tell me more about {topic}?


    Required Fields: topic

    Anticipated_response:
      - yes/no not added yet
      - {topic_suggestions}, neither, none, user_propose new ideas.

  ask_more/user_mentioned_topic_before:
    Context:  The user may mentioned the topic before.  The bot wants to learn more about
              The topic
  ask_more/default:
    Context: The user mentioned a topic automaton don't understand. This reply is relatively generic
          .  The system cannot identify what are the topics.
    - " Hmmm, you got me stumped with that one, however, I would like to learn.
        What can you tell me about it? "
    - " Oh boy, I wish I knew the answer to that! But I would like to hear your idea. "
    - " Well, you have me thinking hard, but I don't know what to say to that. However,
       I would like to hear your thoughts. "

"""
import logging

from states.base import BaseState
from techscience_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class AskMore(BaseState):
    """Implement AskMore State"""
    # pylint: disable=broad-except
    name = "s_ask_more"
    position = dict(x="-100", y="100.69", z="0.0")

    def __call__(self):
        # pylint disable=broad-except

        try:
            super(AskMore, self).__call__()
            LOGGER.info("Entered s_ask_more")
            # slots = {'topic': self.topic_handler.post_process_responses(self.current_topic)}
            # self._add_full_response('ask_more/first_time', slots)
            self.choose_acknowledgement()
            self.add_acknowledgement_if_detected("user_want_teach")
            self.add_acknowledgement_if_detected("user_expressed_preference")
            

            if self.current_topic is not None and self.nlu_processor.user_claim_wrong:
              self.add_acknowledgement_if_detected("user_claim_wrong")
            else:
              for topic in self.new_topics:
                _topic, formatted_topic = self.format_suggest_topics([self.current_topic])
                self.set_attr("current_topic", _topic)
                slots = {'topic': formatted_topic, "user_mentioned_topic": topic}
                if "using" in self.new_topics[topic] and "non_tech" in self.new_topics[topic]:
                  self._add_response(self.get_key_by_user_response("non_tech_topic"), slots)
                  return self._get_transition
                elif "using" in self.new_topics[topic] and "tech" in self.new_topics[topic]:
                  self._add_response(self.get_key_by_user_response("non_tech_topic"), slots)
                  return self._get_transition
            slots = {}
            # self._add_response('ask_more/default_response', slots)
            return self._get_transition

            

        except Exception as _e:
            LOGGER.debug("Ask More State Error:  {}".format(str(_e)))
            # self._add_response('ask_more/default_response', {})
            self.automaton.error_response = True

    
    def get_key_by_user_response(self, header="non_tech_topic"):
      if self.nlu_processor.user_want_to_teach:
        return "ask_more/teach_me_{}".format(header)
      return "ask_more/default_{}".format(header)

    @property
    def _get_transition(self):
      self.automaton.error_response = False
      return "t_ask_more"
