"""Backstory response

This case is triggered whenever we fit our criteria to generate
backstory response instead

- parent_transition

"""
import logging

from states.base import BaseState
from techscience_utils.logging_utils import set_logger_level, get_working_environment
from nlu.constants import TopicModule

LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class QAState(BaseState):
    """Handle Backstory Responses"""
    name = "s_qa"
    position = dict(x="-418.08344", y="10.8853", z="0.0")

    def __call__(self):
        super(QAState, self).__call__()
        if self.question_handler is None:
            return self._get_transition
        backstory_response = self.question_handler.system_backstory_response
        is_i_dont_know = False
        LOGGER.debug("Retained Backstory Response {}".format(backstory_response))
        if backstory_response is not None and backstory_response.response != "":
            is_i_dont_know = backstory_response.tag == "ack_question_idk"
            response = backstory_response.response
            if backstory_response.bot_propose_module:
                propose_topic = backstory_response.bot_propose_module.value
            else:
                propose_topic = None
        else:
            response = self.question_handler.generate_i_dont_know_response()
            is_i_dont_know = True
            propose_topic = None

        if propose_topic and propose_topic != TopicModule.TECHSCI:
    
            self.automaton.techsciencechat["propose_continue"] = "UNCLEAR"
            self.automaton.techsciencechat["propose_topic"] = propose_topic
        
        self._add_response_string(response)
        self.automaton.error_response = False
        #TODO URGENT: Add Exit for I don't know response
        LOGGER.debug("I DON'T KNOW {}".format(is_i_dont_know))

        if is_i_dont_know:
            # Propose Topic
            # Move to change of topic
            propose_topic_dict = self.topic_handler.propose_new_topics(None, [], self.context_topic, self.topic_history)
            self.propose_topic_list = self.topic_handler.propose_topics_to_list(propose_topic_dict)
            topic, formatted_topic = self.format_suggest_topics(self.propose_topic_list)
            selector = "qa/propose_new_topic"
            slots = {"topic_suggestions": formatted_topic}
            self._add_full_response(selector, slots)
            return "t_change_of_topic"
        # 
        return self._get_transition

    @property
    def _get_transition(self):
        return self.get_parent_transition()
