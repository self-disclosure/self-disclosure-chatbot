"""Base class for State

In topic module's finite state machine.  A state is called
after a transition.

States only need to add response.

For every state, where is only one next_transition.

"""
import logging

import template_manager
from techscience_utils.data_topic import MODULE_TO_RESPONSE
from techscience_utils.logging_utils import set_logger_level, get_working_environment
from typing import List, Optional, Dict, Tuple

TEMPLATE_TECHSCI = template_manager.Templates.techscience
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

# from techscience_utils.retrieval_utils import query_tag, retrieve_kg_topic
# from evi import EVI_bot


class BaseState:
    """The Base State for the automaton.
    """

    def __init__(self, automaton):
        self.automaton = automaton

    def __call__(self):
        self._build_topic_handler()
        self._build_intent_handler()
        self._build_acknowledgement_handler()
        # self._detect_intents()

        # set expected_state as None
        self.set_attr("expected_next_state", None)
        return



    def _add_full_response(self, selector, slots, control=dict(ack=True, content=True, question=True)):
        # Add acknowledgements
        if control["ack"]:
            self.choose_acknowledgement()
            # ack_selector = "{0}/acknowledgement".format(selector)
            # self._add_response(ack_selector, slots)

        # Add Content
        if control["content"]:
            content_selector = "{0}/content".format(selector)
            self._add_response(content_selector, slots)

        # Add Followup
        if control["question"]:
            question_selector = "{0}/question".format(selector)
            self._add_response(question_selector, slots)

    def _add_response(self, selector, slots):

        self._add_response_string(TEMPLATE_TECHSCI.utterance(
            selector=selector,
            slots=slots,
            user_attributes_ref=self.automaton.user_attributes_ref))

    def choose_acknowledgement(self):
        """Choose Acknowledgement From User Response"""
        # Only for techscience entry.
        # We expressed favorite technology + user also expressed like technology.
        last_response = self.get_last_system_response
        sys_fav = self.mentioned_favorite_technology(last_response) or self.mentioned_specific_topic(last_response)
        user_favorite = self.nlu_processor.user_favorite
        user_agree = self.nlu_processor.user_agree
        LOGGER.debug("Select an acknowledgement")
        if sys_fav and (user_favorite or user_agree):
            LOGGER.debug("Selected common interest acknowledgement")
            common_acknowledgement = "acknowledgement_common_interest"
            self._add_response(common_acknowledgement, {})
            return True

        if self.user_happiness:
            LOGGER.debug("Selected happiness acknowledgement")
            common_acknowledgement = "acknowledgement_user_expressed_happiness"
            self._add_response(common_acknowledgement, {})
            return True

        if self.user_expressed_opinion:
            LOGGER.debug("Selected opinion acknowledgement")
            common_acknowledgement = "acknowledgement_user_expressed_opinion"
            self._add_response(common_acknowledgement, {})
            return True

        if self.nlu_processor.user_agree:
            LOGGER.debug("Select a positive acknowledgement to acknowledgement")
            common_acknowledgement = "acknowledgement_user_expressed_agreement"
            self._add_response(common_acknowledgement, {})
            return True

        propose_topic = self.user_like_exit
        if propose_topic is not None:
            LOGGER.debug("Select a positive acknowledgement to acknowledgement")
            common_acknowledgement = "acknowledgement_user_like_science"
            self._add_response(common_acknowledgement, {"propose_topic": propose_topic})
            return True

        # Use System Provided Acknowledgement
        # if self.acknowledgement_handler.ack_available(_not=["ack_question_idk"]):
        #     self._add_response_string(self.acknowledgement_handler.get_ack)
        #     return True
        # self._add_response_string()
        return False

    @property
    def fact_counter_limit(self):
        return self.topic_handler.FACT_LIMIT <= self.fact_counter

    @property
    def fact_counter(self):
        return self.automaton.fact_counter

    @property
    def user_like_exit(self):
        last_system_response = self.get_last_system_response
        user_like_science = len(self.nlu_processor.text_contains(["like science", "love science"])) > 0
        but = len(self.nlu_processor.text_contains(["but", "although"])) > 0
        #TODO This aims to provide a smooth transition from techscience to other modules
        raw_propose_topic = self.automaton.get_propose_topic
        propose_topic = self.topic_handler.module_to_response(raw_propose_topic)
        LOGGER.debug("Detected Jumpout from {0} \
        with user-like-science {1}, propose_topic {2}".\
        format(self.nlu_processor.user_utterance, user_like_science, propose_topic))
        if user_like_science:
            return propose_topic
        return None

    def set_input_tags(self, input_tag):
      self.acknowledgement_handler.set_input_tags(input_tag)

    def increase_fact_counter(self):
        self.automaton.increase_fact_counter()

    def reset_fact_counter(self):
        self.automaton.reset_fact_counter()



    @property
    def user_happiness(self):
        return self.nlu_processor.user_expressed_happiness


    # def _detect_intents(self):
    #     """Build the sys_intent, lexical_intent, and topic_intnet from intent_handler
    #     """
    #     text = self.automaton.text
    #     self.sys_intent = self.intent_handler.detect_sys_intent(text)
    #     self.lexical_intent = self.intent_handler.detect_lexical_intent(text)
    #     self.topic_intent = self.intent_handler.detect_topic_intent(text)

    def mentioned_favorite_technology(self, txt):
        favorite = self.nlu_processor.text_contains(["my favorite things"], txt)
        tech = self.nlu_processor.text_contains(["do you like technology"], txt)
        return favorite and tech

    def mentioned_specific_topic(self, txt):
        a_i = self.nlu_processor.text_contains(["Artificial Intelligence", "AI"], txt)
        tech = self.nlu_processor.text_contains(["love talking"], txt)
        if a_i:
            return "artificial_intelligence"

        ipads = self.nlu_processor.text_contains(["ipad"], txt)
        draw = self.nlu_processor.text_contains(["draw"], txt)
        if ipads and draw:
            return "ipads"
        return None

    def add_acknowledgement_if_detected(self, intent_key):
        if "user_want_teach" in self.topic_intent and intent_key == "user_want_teach":
            common_acknowledgement = "acknowledgement_user_want_to_teach"
            self._add_response(common_acknowledgement, {})

        if intent_key == "user_expressed_preference" and self.nlu_processor.user_prefer_keyword and "mentioned" in self.new_topics.get(self.nlu_processor.user_prefer_keyword, ""):
            common_acknowledgement = "acknowledgement_user_expressed_preference"
            self._add_response(common_acknowledgement, {"keyword": self.nlu_processor.user_prefer_keyword})
        if intent_key == "user_claim_wrong" and self.nlu_processor.user_claim_wrong:
            common_acknowledgement = "acknowledgement_user_claim_wrong"
            self._add_response(common_acknowledgement, {})
        return



    @property
    def get_last_system_response(self):
        """Get Last System Response TODO
        """
        return self.automaton.last_response

    @property
    def nlu_processor(self):
        return self.automaton.nlu_processor

    @property
    def question_handler(self):
        return self.automaton.question_handler

    @property
    def current_topic(self):
        return self.automaton.current_topic

    @property
    def context_topic(self):
        return self.automaton.context_topic

    @property
    def topic_history(self):
        return self.automaton.topic_history

    @property
    def current_keywords(self):
        return self.automaton.current_keywords

    @property
    def propose_topic(self):
        return self.automaton.propose_topic

    @property
    def chitchat_tracker(self):
        return self.automaton.chitchat_tracker

    @property
    def chitchat_state(self):
        return self.automaton.chitchat_state

    @property
    def new_topics(self):
        return self.automaton.new_topics

    @property
    def cached_evi(self):
        return self.automaton.cached_evi

    @property
    def expected_next_state(self):
        return self.automaton.expected_next_state

    def set_attr(self, attribute, value):
        setattr(self.automaton, attribute, value)

    def system_can_handle(self, topic=None):
        """ Return True if system can handle the detected_topic
        """
        return self.topic_handler.is_broad_topic(topic) or \
               self.topic_handler.is_topic(topic) or \
               self.topic_handler.is_fact(topic)    

    @property
    def _get_transition(self):
        """Get the predefined transition for each state.
           Each state is designed to have one transition.
        """
        raise NotImplementedError("get_transition is not implemented")

    @classmethod
    @property
    def _visdata(cls):
        # return {"full_name": cls.__name__, "color": [], "size": 27,
        # "position":[]}
        return

    def get_other_module_utterance(self):
        """Propose a different topic to jump to
        """
        return MODULE_TO_RESPONSE[self.automaton.intent_classify_topic]

    def _add_response_string(self, _s):
        self.automaton.response += _s

    def _add_response_if_empty(self, _s):
        empty = len(self.automaton.response) == 0
        if empty:
            self._add_response_string(_s)

    def _get_intent(self):
        self.lexical_intent = self.automaton.detect_intents()
        return self.lexical_intent

    def _build_acknowledgement_handler(self):
        """Build acknowledgement Handler for the state
        """

        self.acknowledgement_handler = self.automaton.get_acknowledgement_handler(True)
        return self.acknowledgement_handler

    def _build_topic_handler(self):
        """Build topic handler for the state
        """
        self.topic_handler = self.automaton.get_topic_handler(False)
        return self.topic_handler

    def _build_intent_handler(self):
        """Build topic handler for the state
        """
        self.intent_handler = self.automaton.get_intent_handler(False)
        return self.intent_handler

    @property
    def chitchat_handler(self):
        return self.automaton.get_chitchat_handler(False)

    @property
    def is_first_time_user(self):
        """Return True if the user is first time user
        """
        return True

    def is_broad_topic(self):
        """The detected topic is a broad topic"""

        return self.current_topic is not None and self.topic_handler.is_broad_topic(
            self.current_topic)

    def get_broad_topic(self, topic):
        """ Given the current topic, get the broad topic from
            users.
            topic_handler.map_to_broad_topic will always return a broad_topic

        """
        return self.topic_handler.map_to_broad_topic(topic)

    def no_detected_topic(self):
        """Return whether topic handler detect specific topic from
           user's utterance
        """
        return self.current_topic is None and len(self.new_topics) < 1

    def _set_transition_state(self, _exit=False, propose_topic=None, _unclear=False):
        """ For every transition state, we will set
            1. propose_continue
            2. context_topic
            3. current_topic

            parameters:
                _exit:  True if exit_techscience_chat
        """
        if propose_topic is None:
            propose_topic = self.automaton.system_propose_topic
        if _unclear:
            self.automaton.set_propose_continue("UNCLEAR")
        elif _exit:
            self.automaton.set_propose_continue("STOP")
        else:
            self.automaton.set_propose_continue("CONTINUE")

    # def _set_topic(self):
    #     """ Set the current topic and context topic
    #     """

    #     self.automaton.current_topic = self.current_topic
    #     self.automaton.context_topic = self.context_topic
    #     self.automaton.topic_history = self.topic_history
    #     self.automaton.tech_propose_topic = self.propose_topic
    #     self.automaton.chitchat_tracker = self.chitchat_tracker
    #     self.automaton.chitchat_state = self.chitchat_state
    #     self.automaton.expected_next_state = self.expected_next_state

    def get_parent_transition(self):
        """Get the previous transition that calls this state
        """
        return self.automaton.current_transition

    def format_suggest_topics(self, suggest_topics:List[str])->Optional[Tuple[str, str]]:
        """Format suggest topics takes in a list of
           suggest topics and generate the response string.
           TODO: Test and Distribute the function
        """
        # pylint: disable=no-self-use
        if len(suggest_topics) == 0:
            return None, "this"
        if len(suggest_topics) > 1:
            LOGGER.warning("More than one suggested topic proposed.  Will pick the first one!")
        topic = suggest_topics[0]
        
        formatted_topic = self.topic_handler.post_process_responses(topic)
        if topic is None or formatted_topic is None:
            formatted_topic = "this"
        return topic, formatted_topic

    def write_static_state(self, result_lst):
        result_lst.append({"name": self.name, \
                           "size": 60.0, \
                           "position": self.position,
                           "color": "#8b0000"})

    def select_chitchat(self, header, topic, curr_id):
        """Given topic/fact or header.  Select the corresponding chitchat automaton
           Parameters:
             - curr_id - current id for chitchat
             - topic  - fact topic or opinion topic (already verified)
             - header - choose between (opinion, fact, trend)
           Return:
             - chitchat_automaton
        """
        LOGGER.debug("{}".format(self.automaton.chitchat_mapping))
        LOGGER.debug("[TECHSCIENCE STATES BASE]Selecting Chitchat {0}, {1}, {2}".format(header, topic, curr_id))
        automaton = self.automaton.get_chitchat_automaton(header, topic, curr_id)
        return automaton

    # def get_chitchat_state(self, keyword, header):
    #     """Get the chitchat state for specific keyword

    #     Parameters:
    #       keyword - topic keyword
    #       header - choose between fact, opinion, trend
    #     """
    #     return self.chitchat_tracker[header][keyword]

    def chitchat_continue(self, chitchat_tracker, chitchat_state):
        """Returns True if currently chitchat is continuing
           Note, call this after preprocess_chitchat
        """
        # Check if self.chitchat_state and self.chitchat_tracker is equal
        if chitchat_tracker["curr_state"] != chitchat_state:
            LOGGER.debug("Potential Error: Chitchat Tracker and Chitchat state not equal. {0}, {1}".format(chitchat_tracker["curr_state"], chitchat_state))
            return False
        if chitchat_tracker["curr_turn"] < 0:
            return False
        return chitchat_state != self.chitchat_handler.null_chitchat_state



    def stop_and_set_chitchat(self, storage=True):
        """Stop and Set chitchat
        """
        if storage:
            self.set_attr("chitchat_state",  self.chitchat_handler.null_chitchat_state_string)
        else:
            self.set_attr("chitchat_state",  self.chitchat_handler.null_chitchat_state)

    @property
    def user_expressed_opinion(self):
        return self.nlu_processor.user_expressed_opinion
        # OPINION_THRESHOLD = 5
        # dialog_act_contains = self.topic_handler.dialog_act_contains(["opinion"])
        # regex_contains = "user_expressed_opinion" in self.topic_intent
        # regex_no_opinion = "user_no_opinion" in self.topic_intent
        # if len(self.automaton.text.split(" ")) < OPINION_THRESHOLD:
        #     return False
        # if regex_contains or dialog_act_contains:
        #     return True
        # return False

    def get_opinion_topic(self, current_topic):
        is_opinion = self.topic_handler.is_chitchat_topic("opinion", current_topic)
        if is_opinion:
            return current_topic

        for chitchat_state in self.chitchat_tracker["available_ids"]:
            header, topic, _id = chitchat_state
            if header == "opinion":
                return topic

        return None

    def extract_using_topics(self)->Optional[str]:
        for topic in self.new_topics:
            if "using" in self.new_topics[topic]:
                return topic
        return None