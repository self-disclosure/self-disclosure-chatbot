"""Interesting Fact State

Parent Transition:
  - t_resume
  - t_opinion

Template Responses:
    favorite/know_nothing:
      - " Aw, that's okay. We are all learners here. I didn't know much about
          {topic} until a couple of weeks ago myself.
       Wanna know more about {topic}? "
    favorite/
      - default
      - artificial_intelligence_topic
      - engineering_topic
      - biology_topic
      - physics_topic
      - chemistry_topic
      - astronomy_topic
      - medicine_topic
      - computers_topic
      - general_topic


Next_Transition:
  - t_interesting_fact

Template Response Guidelines
  {topic}/facts:
    - A list of facts
    Context:
      - The user said yes on do you want to know more about *
      - We will add more acknowledgements.
      *Note:  I don't want to add ask_more here.

"""
import logging
import random

from states.base import BaseState
from techscience_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class InterestingFact(BaseState):
    """InterestingFact State
    """
    # pylint: disable=broad-except
    name = "s_interesting_fact"
    position = dict(x="-300.08344", y="346.8853", z="0.0")

    def __call__(self):
        try:
            super(InterestingFact, self).__call__()
            LOGGER.info("Entered interesting_fact_state")

            fact_topic =self.topic_handler.find_fact_for_topic(self.current_topic, self.topic_history)

            if fact_topic is None:
                response = self.question_handler.dont_know_answer
                self._add_response_string(response + " say, are you interested in other science topics??")
                self.set_attr("expected_next_state", "s_change_of_topic")
                return self._get_transition()

            topic_added = False
            is_chitchat_topic = self.topic_handler.is_chitchat_topic("facts", fact_topic)

            # Special Case, Acknowledge User Don't have IPad
            if self.detect_user_dont_have_ipad():
                topic_added = True

                selector = self.topic_handler.map_keyword_to_selector(
                    fact_topic, "facts")
                topic, formatted_topic = self.format_suggest_topics([fact_topic])
                self.set_attr("current_topic", topic)
                slots = {'topic': formatted_topic}
                self.choose_acknowledgement()
                self._add_response('facts/continue', slots)
            
            # check if current_topic is a chitchat topic
            elif is_chitchat_topic:
                chitchat_tracker = self.chitchat_handler.preprocess_chitchat_tracker(self.chitchat_tracker)
                _chitchat_state = self.chitchat_handler.preprocess_chitchat_state(self.chitchat_state)
                if self.chitchat_continue(chitchat_tracker, _chitchat_state):
                    # At this level, chitchat state should be the same as self.chitchat_state
                    chitchat_automaton = self.select_chitchat(*_chitchat_state)
                    chitchat_tracker = chitchat_automaton.transduce(chitchat_tracker)
                    _chitchat_state = chitchat_tracker["curr_state"]
                    topic_added = True
                    # Check if chitchat is completed
                    if chitchat_automaton.get_next_topic() is not None:
                        self.set_attr("current_topic", chitchat_automaton.next_topic)
                        _chitchat_state = self.chitchat_handler.null_chitchat_state # Stop Chitchat Flow and set the topic
                        chitchat_tracker["chitchat_state"] = _chitchat_state

                    if chitchat_automaton.is_jumpout:
                        self.set_attr("expected_next_state", "s_change_of_topic")
                    
                else:
                    # This should be called after Chitchat Resume
                    # Chitchat Resume
                    # At this point, self.chitchat state is set to 0
                    valid_chitchat = self.chitchat_handler.is_valid_chitchat(chitchat_tracker)
                    curr_turn = chitchat_tracker["curr_turn"]
                    if valid_chitchat and curr_turn >= 0:
                        # Implement t_resume.  which is reset chitchat tracker
                        chitchat_tracker = self.chitchat_handler.store_current_state_as_complete(chitchat_tracker)
                    if not valid_chitchat:
                        # Start a new chitchat
                        _chitchat_state = self.chitchat_handler.new_chitchat_state(chitchat_tracker, "facts", fact_topic)
                        
                    chitchat_tracker = self.chitchat_handler.initialize_chitchat(chitchat_tracker, _chitchat_state)
                    _, chitchat_topic, chitchat_id = _chitchat_state
                    if chitchat_topic is not None:
                        selectors = self.topic_handler.map_keyword_to_selector(fact_topic, "facts", True, chitchat_id)
                        starter, followup = selectors
                        topic, formatted_topic = self.format_suggest_topics([fact_topic])
                        self.set_attr("current_topic", topic)
                        slots = {'topic': formatted_topic}
                        if self.choose_acknowledgement():
                            self._add_response('facts/start', slots)
                        self._add_response(starter, slots)
                        self._add_response(followup, slots)
                        topic_added = True
                    else:
                        _chitchat_state = self.chitchat_handler.null_chitchat_state
                self.set_attr("chitchat_state", self.chitchat_handler.postprocess_chitchat_state(_chitchat_state))
                self.set_attr("chitchat_tracker", self.chitchat_handler.postprocess_chitchat_tracker(chitchat_tracker))

            if not topic_added:

                selector = self.topic_handler.map_keyword_to_selector(
                    fact_topic, "facts")
                topic, formatted_topic = self.format_suggest_topics([fact_topic])
                self.set_attr("current_topic", topic)
                slots = {'topic': formatted_topic}
                self.choose_acknowledgement()
                self._add_response('facts/start', slots)
                self._add_response(selector, slots)
                
                LOGGER.debug("New Fact Counter {}".format(self.fact_counter))
                self.increase_fact_counter()
                if not self.fact_counter_limit:# facts not end:
                    self._add_response('facts/continue', slots)
                else:
                    self._add_response('facts/end', slots)
            self.automaton.error_response = False
            self.set_attr("propose_topic", self.topic_handler.default_propose_topic)




        except Exception as _e:
            # selector = "facts/error_response"
            LOGGER.error("State Error s_interesting_fact:  {}".format(_e), exc_info=True)
            # error_topic = self.current_topic 
            # if error_topic is None:
            #     error_topic = "science stuff"
            # self._add_response(selector, {"topic": error_topic})
            self.stop_and_set_chitchat()
            self.automaton.error_response = True

        return self._get_transition

    @property
    def _get_transition(self):
        return "t_interesting_fact"


    def detect_user_dont_have_ipad(self):
        """Temporary Fix to address user don't have ipad
        """
        return self.nlu_processor.detect_user_dont_have_ipad