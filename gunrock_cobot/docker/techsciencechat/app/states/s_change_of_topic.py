"""Change and Topic and User Change of Topic for the FSM

Parent Transition:
  - t_user_change_topic
  - t_user_change_module
  - t_techsci_chitchat
  - t_opinion

Template Response Guidelines
  change_of_topic/change_topic_passive:
    Context: User is disinterested in the current topic.  We propose a new topic.
    User Utterances: "No"

    Replies:
      - "That's cool, do you want to talk about something else?  Like {new_topic}"
    Anticipated_response:
      - yes/no not added yet
      - No, I want to talk about *.

  change_of_topic/change_topic_active
    Context: User is interested in something else.  We detect a desire to change topic.

Old Template Responses
Next_Transition:
  - t_change_of_topic

"""
import logging

from states.base import BaseState
from techscience_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())


class ChangeOfTopic(BaseState):
    """Change of Topic State
    """
    name = "s_change_of_topic"
    position = dict(x="-508.08344", y="206.8853", z="0.0")

    # pylint: disable=broad-except
    def __call__(self):
        try:
          LOGGER.info("Entering s_change_of_topic")
          super(ChangeOfTopic, self).__call__()
          LOGGER.info("Entered s_change_of_topic")
          topic_active = self.current_topic is not None and \
                        (self.topic_handler.is_fact(self.current_topic) \
                         or self.topic_handler.is_topic(self.current_topic))        
          proposed_topic = len(self.propose_topic) > 0
          new_topic = self.extract_using_topics()
          if not topic_active and proposed_topic:
              selector = "change_of_topic/change_topic_passive"
              topic, formatted_topic = self.format_suggest_topics(self.propose_topic)
              slots = {"topic_suggestions": formatted_topic}
              self._add_full_response(selector, slots)

          elif self.fact_counter_limit:
            LOGGER.debug("Proposing New Topics in fact counter limit {}".format(self.propose_topic))
            selector = "change_of_topic/change_topic_suggestion"
            topic1, formatted_topic = self.format_suggest_topics([self.current_topic])
            topic2, formatted_topic2 = self.format_suggest_topics(self.propose_topic)
            slots = {"topic1": formatted_topic, "topic2":formatted_topic2}
            self._add_full_response(selector, slots)          
          elif topic_active:
            self.set_attr("propose_topic", [])
            selector = "change_of_topic/change_topic_active"
            topic, formatted_topic = self.format_suggest_topics([self.current_topic])
            slots = {"topic": formatted_topic}
            self._add_full_response(selector, slots)
          elif new_topic:
            selector = "change_of_topic/keyword_to_topic"
            propose_topic = self.topic_handler.propose_topic_from_keyword(new_topic, context_topic=self.context_topic, topic_history=self.topic_history)
            self.set_attr("propose_topic", [propose_topic])
            topic, formatted_topic = self.format_suggest_topics(self.propose_topic)
            slots = {"topic": formatted_topic, "keyword":new_topic}
            self.set_attr("current_topic", None)
            self._add_response(selector, slots)
          else:
            LOGGER.debug("topic_active: {0}, propose_topic: {1}".format(self.current_topic, self.propose_topic))
            selector = 'change_of_topic/exit_techscience'
            self._set_transition_state(True)
            slots = {"topic": {}}
            self._add_response(selector, slots)
          
          self.automaton.error_response = False

        except Exception as _e:
            LOGGER.error("State Error s_change_of_topic:  {}".format(_e), exc_info=True)
            # selector = 'change_of_topic/error_response'
            slots = {"topic": {}}
            # self._add_response(selector, slots)
            self.automaton.error_response = True
        return self._get_transition()

    def _get_transition(self):
        return "t_change_of_topic"

    def contains_new_topic_transition(self):
      return