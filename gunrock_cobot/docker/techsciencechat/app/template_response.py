import hashlib
import yaml
from typing import List

class TemplateGenerator:

    def __init__(self):
        try:
            from response_templates import intent_general
            data = intent_general.data
        except ImportError:
            pass

    def _read(self) -> dict:
        with open('bookchat_templates.yaml') as f:
            return yaml.load(f)

    def _write(self, data: dict):
        with open('bookchat_templates_data.py', 'w') as f:
            f.write("data = ")
            f.write(str(data))

    def generate(self):
        def nested(d: dict, u: dict, path: List[str]):
            for k, v in u.items():
                if isinstance(v, dict):
                    path.append(k)
                    d[k] = nested(d.get(k, {}), v, path)
                    path = []
                elif isinstance(v, list):
                    d[k] = [self._generate_utterance_entry(vi) for vi in v] # path
            return d

        data = self._read()
        return nested(data, data, [])

    def _generate_utterance_entry(self, text: str): # path: List[str]
        return {
            'text': text,
            #'hash': hashlib.blake2b(('/'.join(path) + '/' + text).encode()).hexdigest()
        }


if __name__ == '__main__':
    tg = TemplateGenerator()
    tg._write(tg.generate())
