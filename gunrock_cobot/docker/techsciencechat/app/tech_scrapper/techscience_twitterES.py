import time
from pprint import pprint
from elasticsearch import Elasticsearch, RequestsHttpConnection
from requests_aws4auth import AWS4Auth


key_list = ['running', 'today', 'fox: nascar', 'greenleaf own', 'canada news', 'golf', 'us open', 'imdb',
            'quentin sommerville', 'sports', 'medium', 'mtv video music awards', 'tiff', 'amazing', 'royal family',
            'cycling', 'wnba playoffs', 'reuters sports', 'back to school', 'business', 'ben zauzmer',
            'hollywood reporter', 'nations league', 'peopletv', 'moto gp', 'fifa awards', 'variety', 'hypebeast',
            'weird', 'haha', 'space news', 'personal story', 'photography', 'national geographic', 'pcmag',
            'united nations', 'tech', 'festivals', 'the indian express', 'pop culture', 'trending',
            'kerala floods 2018', 'irish news', 'sports news', 'nj.com', 'wnba', 'baseball', 'wwe', 'personal stories',
            'hockey', 'social action', 'india news', 'media', 'major league soccer', 'emmy awards 2018', 'wrestling',
            'doctor who on bbc america', 'buzzfeed news', 'football', 'nascar', 'movies', 'wmur tv', 'kerala floods',
            'gq magazine', 'bbc world service', 'the boston globe', 'bailey parker', 'mlb', 'stitch', 'kcci news',
            'health', 'instyle', 'washington post', 'culture', 'moto2', 'style', 'mic', 'racing', 'nifty', 'nowthis',
            'cute', 'vice news', 'ncaa basketball', 'formula 1', 'athletics', '7 news adelaide', 'fun', 'cronkite news',
            'celebrity', 'teen vogue', 'la liga', 'animals', 'e! entertainment', 'anchor', 'tech insider',
            "harper's bazaar", 'cma country music', 'surfing', 'indy car', 'mtv video music awards 2018', 'sky news',
            'billboard', 'tennis', 'urban legends', 'bbc news africa', 'fashion', 'politics', 'axios', 'geek.com',
            'kcranews', 'fallon tonight', 'technology', 'afl', 'nfl', 'business insider', 'pga tour', 'pbs newshour',
            'post opinions', 'us elections', 'champions league', 'basketball', 'fema', 'insider', 'ufc', 'art',
            'boxing', 'europa league', 'ncaa football', 'iheartradio mmvas', 'mashable', 'developing',
            'reuters business', 'tictoc by bloomberg', 'world news', 'claws', 'black ballad', 'music', 'tidal',
            'us news', 'league cup', 'globalnews.ca', 'cnbc', 'history', 'premier league', 'nbc chicago',
            'los angeles times', 'the new york times', 'evan auerbach', 'television', 'gaming', 'holidays', 'taekwondo',
            'weather', 'nhl', 'sportscenter live', 'books', 'cool', 'australian news', 'topic', 'makers',
            'hashtag roundup', 'food insider', 'bbc north america', 'arts', 'am to dm by buzzfeed news', 'food',
            'esports', 'nba', 'bloomberg opinion', 'cnet', 'pga', 'asian games', 'super cup', 'serie a', 'mls',
            'uk news', 'gymnastics', 'reuters world', 'travel', 'kokh fox 25', 'science', 'texas tribune', 'soccer',
            'in memoriam', 'cosmopolitan', 'the weather network', 'reuters top news', 'opinion', 'gamespot',
            'foo fighters', 'much', 'paul reiser', 'hurricane florence']

def category(type):
    host = 'search-elastictm-kj4q2xold4x3clsht4xm6v3374.us-east-1.es.amazonaws.com'
    awsauth = AWS4Auth('AKIAIBDWK2ZAQZUTRGRQ',
                       'jCCV5uIeuK/D45f9I2+BH0u1sb/4t368yvQ6nAa4', 'us-east-1', 'es')
    es = Elasticsearch(
        hosts=[{'host': host, 'port': 443}],
        http_auth=awsauth,
        use_ssl=True,
        verify_certs=True,
        connection_class=RequestsHttpConnection
    )
    res = es.search(index="moments", body={
        "size": 10,
        "query": {
            "match": {
                "source": {
                    'query': type
                }
            },
        },
        "sort": [
            {"epoch": "desc"},
            {"like": "desc"},
        ],
    }
    )

    resp = []
    for item in res['hits']['hits']:
        resp.append(item['_source'])

    # from pprint import pprint
    # pprint(res)
    return resp


def moment_search(s):
    host = 'search-elastictm-kj4q2xold4x3clsht4xm6v3374.us-east-1.es.amazonaws.com'
    awsauth = AWS4Auth('AKIAIBDWK2ZAQZUTRGRQ',
                       'jCCV5uIeuK/D45f9I2+BH0u1sb/4t368yvQ6nAa4', 'us-east-1', 'es')
    es = Elasticsearch(
        hosts=[{'host': host, 'port': 443}],
        http_auth=awsauth,
        use_ssl=True,
        verify_certs=True,
        connection_class=RequestsHttpConnection
    )
    # if isinstance(, list)
    res = es.search(index="moments", body={
        "size": 10,
        "query": {
            "match": {
                "desc": {
                    'query': s
                }
            },
        },
        "sort": [
            {"epoch": "desc"},
            {"like": "desc"},
        ],
    }
    )

    resp = []
    for item in res['hits']['hits']:
        resp.append(item['_source'])

    if not resp:
        res = es.search(index="moments", body={
            "size": 10,
            "query": {
                "match": {
                    "title": {
                        'query': s
                    }
                },
            },
            "sort": [
                {"epoch": "desc"},
                {"like": "desc"},
            ],
        }
        )
        for item in res['hits']['hits']:
            resp.append(item['_source'])
    return resp


# if __name__ == "__main__":
#     pprint(moment_search("science"))
#     # pprint(category('animals'))
#     pass
