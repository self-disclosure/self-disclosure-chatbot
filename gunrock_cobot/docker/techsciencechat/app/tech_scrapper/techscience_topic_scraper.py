import praw
import pandas as pd
from pprint import pprint
import datetime as dt
import prawcore
import json

COBOT_API_KEY='xgTjk23SRM9Q9VFrUEFOjav0Bn4ot21W8uFLEieT'
# REDDIT_CLIENT_ID = "0OyokrmI54QUAA"
# REDDIT_CLIENT_SECRET = "3f4Zcet4eQwMrIA7czaEMBl-6ZA"
REDDIT_CLIENT_ID = "wsS9EUzhLLDfUw"
REDDIT_CLIENT_SECRET = "7wLb-CPHQyeZSrznBgT-edbDqU8"


class TopicScraper(object):
    def __init__(self):
        self.topics_of_interest = ["futurology", "timetravel", "nasa"]
        self.limit = 3000
        self.topics_data = None
        self.topics_dict = {"tag": [],
                            "title": [],
                            "score": [],
                            "id": [],
                            "url": [],
                            "comms_num": [],
                            "created": [],
                            "body": []}

    def scrape_topics(self):
        reddit = praw.Reddit(client_id=REDDIT_CLIENT_ID,
                             client_secret=REDDIT_CLIENT_SECRET,
                             user_agent='extractor',
                             timeout=2
                             )
        for topic in self.topics_of_interest:
            self.topic_top_subreddits(reddit, topic)
            self.topic_hot_subreddits(reddit, topic)
            self.topic_new_subreddits(reddit, topic)
            self.subredditsdataframe()

    def topic_top_subreddits(self, reddit, topic):
        subreddit = reddit.subreddit(topic).top(limit=self.limit)
        for submission in subreddit:
            self.topics_dict["tag"].append(topic)
            self.topics_dict["title"].append(submission.title)
            self.topics_dict["score"].append(submission.score)
            self.topics_dict["id"].append(submission.id)
            self.topics_dict["url"].append(submission.url)
            self.topics_dict["comms_num"].append(submission.num_comments)
            self.topics_dict["created"].append(submission.created)
            self.topics_dict["body"].append(submission.selftext)

    def topic_hot_subreddits(self, reddit, topic):
        subreddit = reddit.subreddit(topic).top(limit=self.limit)
        for submission in subreddit:
            self.topics_dict["tag"].append(topic)
            self.topics_dict["title"].append(submission.title)
            self.topics_dict["score"].append(submission.score)
            self.topics_dict["id"].append(submission.id)
            self.topics_dict["url"].append(submission.url)
            self.topics_dict["comms_num"].append(submission.num_comments)
            self.topics_dict["created"].append(submission.created)
            self.topics_dict["body"].append(submission.selftext)

    def topic_new_subreddits(self, reddit, topic):
        subreddit = reddit.subreddit(topic).top(limit=self.limit)
        for submission in subreddit:
            self.topics_dict["tag"].append(topic)
            self.topics_dict["title"].append(submission.title)
            self.topics_dict["score"].append(submission.score)
            self.topics_dict["id"].append(submission.id)
            self.topics_dict["url"].append(submission.url)
            self.topics_dict["comms_num"].append(submission.num_comments)
            self.topics_dict["created"].append(submission.created)
            self.topics_dict["body"].append(submission.selftext)

    def get_date(self, created):
        return dt.datetime.fromtimestamp(created)

    def subredditsdataframe(self):
        self.topics_data = pd.DataFrame(self.topics_dict)
        self.topics_data = self.topics_data.assign(timestamp=self.topics_data["created"].apply(self.get_date))
        json_data = self.topics_data.to_dict(orient='records')
        with open('scitech_topics.json', 'w') as json_file:
            json.dump(json_data, json_file, indent=4, sort_keys=True, default=str)


if __name__ == "__main__":
    rtopic = TopicScraper()
    rtopic.scrape_topics()
    pass

