import praw
import pandas as pd
from pprint import pprint
import datetime as dt
import prawcore
import json

COBOT_API_KEY='xgTjk23SRM9Q9VFrUEFOjav0Bn4ot21W8uFLEieT'
# REDDIT_CLIENT_ID = "0OyokrmI54QUAA"
# REDDIT_CLIENT_SECRET = "3f4Zcet4eQwMrIA7czaEMBl-6ZA"
REDDIT_CLIENT_ID = "wsS9EUzhLLDfUw"
REDDIT_CLIENT_SECRET = "7wLb-CPHQyeZSrznBgT-edbDqU8"


class ScrapeScienceReddit(object):
    def __init__(self):
        self.science_flairs = set()
        self.limit = 8000
        self.topics_dict = {"tag": [],
                            "title": [],
                            "score": [],
                            "id": [],
                            "url": [],
                            "comms_num": [],
                            "created": [],
                            "body": []}
        self.topics_data = None

    def reddit_sci(self):
        reddit = praw.Reddit(client_id=REDDIT_CLIENT_ID,
                             client_secret=REDDIT_CLIENT_SECRET,
                             user_agent='extractor',
                             timeout=2
                             )
        self.get_science_flairs(reddit)
        self.science_top_subreddits(reddit)
        self.science_hot_subreddits(reddit)
        self.science_new_subreddits(reddit)
        self.subredditsdataframe()

    def subredditsdataframe(self):
        self.topics_data = pd.DataFrame(self.topics_dict)
        self.topics_data = self.topics_data.assign(timestamp=self.topics_data["created"].apply(self.get_date))
        json_data = self.topics_data.to_dict(orient='records')
        with open('science.json', 'w') as json_file:
            json.dump(json_data, json_file, indent=4, sort_keys=True, default=str)

    def get_date(self, created):
        return dt.datetime.fromtimestamp(created)

    # Dumb function to retrive flairs for subreddit.
    # We could potentially add more flairs and definitely find a better way to do this.
    def get_science_flairs(self, reddit):
        subreddit = reddit.subreddit('science').hot()
        for sub in subreddit:
            if sub.link_flair_text:
                self.science_flairs.add(sub.link_flair_text)
        subreddit = reddit.subreddit('science').top()
        for sub in subreddit:
            if sub.link_flair_text:
                self.science_flairs.add(sub.link_flair_text)
        subreddit = reddit.subreddit('science').new()
        for sub in subreddit:
            if sub.link_flair_text:
                self.science_flairs.add(sub.link_flair_text)

    def science_top_subreddits(self, reddit):
        subreddit = reddit.subreddit('science').top(limit=self.limit)
        for submission in subreddit:
            self.topics_dict["tag"].append(submission.link_flair_text)
            if submission.link_flair_text not in self.science_flairs:
                self.science_flairs.add(submission.link_flair_text)
            self.topics_dict["title"].append(submission.title)
            self.topics_dict["score"].append(submission.score)
            self.topics_dict["id"].append(submission.id)
            self.topics_dict["url"].append(submission.url)
            self.topics_dict["comms_num"].append(submission.num_comments)
            self.topics_dict["created"].append(submission.created)
            self.topics_dict["body"].append(submission.selftext)

    def science_hot_subreddits(self, reddit):
        subreddit = reddit.subreddit('science').hot(limit=self.limit)
        for submission in subreddit:
            self.topics_dict["tag"].append(submission.link_flair_text)
            if submission.link_flair_text not in self.science_flairs:
                self.science_flairs.add(submission.link_flair_text)
            self.topics_dict["title"].append(submission.title)
            self.topics_dict["score"].append(submission.score)
            self.topics_dict["id"].append(submission.id)
            self.topics_dict["url"].append(submission.url)
            self.topics_dict["comms_num"].append(submission.num_comments)
            self.topics_dict["created"].append(submission.created)
            self.topics_dict["body"].append(submission.selftext)

    def science_new_subreddits(self, reddit):
        subreddit = reddit.subreddit('science').hot(limit=self.limit)
        for submission in subreddit:
            self.topics_dict["tag"].append(submission.link_flair_text)
            if submission.link_flair_text not in self.science_flairs:
                self.science_flairs.add(submission.link_flair_text)
            self.topics_dict["title"].append(submission.title)
            self.topics_dict["score"].append(submission.score)
            self.topics_dict["id"].append(submission.id)
            self.topics_dict["url"].append(submission.url)
            self.topics_dict["comms_num"].append(submission.num_comments)
            self.topics_dict["created"].append(submission.created)
            self.topics_dict["body"].append(submission.selftext)


########################################################################################################################
class ScrapeTechnologyReddit(object):
    def __init__(self):
        self.technology_flairs = set()
        self.limit = 8000
        self.topics_dict = {"tag": [],
                            "title": [],
                            "score": [],
                            "id": [],
                            "url": [],
                            "comms_num": [],
                            "created": [],
                            "body": []}
        self.topics_data = None

    def reddit_tech(self):
        reddit = praw.Reddit(client_id=REDDIT_CLIENT_ID,
                             client_secret=REDDIT_CLIENT_SECRET,
                             user_agent='extractor',
                             timeout=2
                             )
        self.get_technology_flairs(reddit)
        self.technology_top_subreddits(reddit)
        self.technology_hot_subreddits(reddit)
        self.technology_new_subreddits(reddit)
        self.subredditsdataframe()

    def subredditsdataframe(self):
        self.topics_data = pd.DataFrame(self.topics_dict)
        self.topics_data = self.topics_data.assign(timestamp=self.topics_data["created"].apply(self.get_date))
        json_data = self.topics_data.to_dict(orient='records')
        with open('technology.json', 'w') as json_file:
            json.dump(json_data, json_file, indent=4, sort_keys=True, default=str)

    def get_date(self, created):
        return dt.datetime.fromtimestamp(created)

    # Dumb function to retrive flairs for subreddit.
    # We could potentially add more flairs and definitely find a better way to do this.
    def get_technology_flairs(self, reddit):
        subreddit = reddit.subreddit('technology').hot()
        for sub in subreddit:
            if sub.link_flair_text:
                self.technology_flairs.add(sub.link_flair_text)
        subreddit = reddit.subreddit('technology').top()
        for sub in subreddit:
            if sub.link_flair_text:
                self.technology_flairs.add(sub.link_flair_text)
        subreddit = reddit.subreddit('technology').new()
        for sub in subreddit:
            if sub.link_flair_text:
                self.technology_flairs.add(sub.link_flair_text)

    def technology_top_subreddits(self, reddit):
        subreddit = reddit.subreddit('technology').top(limit=self.limit)
        for submission in subreddit:
            self.topics_dict["tag"].append(submission.link_flair_text)
            if submission.link_flair_text not in self.technology_flairs:
                self.technology_flairs.add(submission.link_flair_text)
            self.topics_dict["title"].append(submission.title)
            self.topics_dict["score"].append(submission.score)
            self.topics_dict["id"].append(submission.id)
            self.topics_dict["url"].append(submission.url)
            self.topics_dict["comms_num"].append(submission.num_comments)
            self.topics_dict["created"].append(submission.created)
            self.topics_dict["body"].append(submission.selftext)

    def technology_hot_subreddits(self, reddit):
        subreddit = reddit.subreddit('technology').hot(limit=self.limit)
        for submission in subreddit:
            self.topics_dict["tag"].append(submission.link_flair_text)
            if submission.link_flair_text not in self.technology_flairs:
                self.technology_flairs.add(submission.link_flair_text)
            self.topics_dict["title"].append(submission.title)
            self.topics_dict["score"].append(submission.score)
            self.topics_dict["id"].append(submission.id)
            self.topics_dict["url"].append(submission.url)
            self.topics_dict["comms_num"].append(submission.num_comments)
            self.topics_dict["created"].append(submission.created)
            self.topics_dict["body"].append(submission.selftext)

    def technology_new_subreddits(self, reddit):
        subreddit = reddit.subreddit('technology').hot(limit=self.limit)
        for submission in subreddit:
            self.topics_dict["tag"].append(submission.link_flair_text)
            if submission.link_flair_text not in self.technology_flairs:
                self.technology_flairs.add(submission.link_flair_text)
            self.topics_dict["title"].append(submission.title)
            self.topics_dict["score"].append(submission.score)
            self.topics_dict["id"].append(submission.id)
            self.topics_dict["url"].append(submission.url)
            self.topics_dict["comms_num"].append(submission.num_comments)
            self.topics_dict["created"].append(submission.created)
            self.topics_dict["body"].append(submission.selftext)


if __name__ == "__main__":
    rscience = ScrapeScienceReddit()
    rscience.reddit_sci()
    rtech = ScrapeTechnologyReddit()
    rtech.reddit_tech()
