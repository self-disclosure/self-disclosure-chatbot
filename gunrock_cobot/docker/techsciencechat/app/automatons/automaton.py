"""Automaton Template to build FSM

"""

import logging
from types import SimpleNamespace
from utils.error_utils import ERROR_MESSAGE
from nlu.util_nlu import ReturnNLPSegment, CentralElement

# TODO Implement Transitions and States before call this
# from transitions.base import BaseTransition as Transition
# from states.base import BaseState as State
# TODO: Add get_log utils
import template_manager
import os
import json

from response_templates import template_data
t_e = template_manager.Templates.error

logging.basicConfig(
    format='[%(levelname)s] %(asctime)s [%(filename)s:%(lineno)d] %(message)s',
)


logging.getLogger().setLevel(logging.INFO)


class Automaton:
    """

    Automaton Response Dictionary

    response: <str>
    prev_hash:  hash from user_attributes
    [MODULE_NAME]chat:
      not_first_time: <bool>
      propose_continue: <str>
      propose_topic: <str>
      current_transition: <str>
      current_topic: <str>
      context_topic: [<str>]
      fact_counter: <int>
      chichat_counter: ""
    propose_topic: <str>

    """
    MODULE_NAME = "DEFAULT FSM"

    def __init__(self, debug=False, force_create=False):
        self.__bind_states()
        self.__bind_transitions()

        if debug:
            self._create_visualization(force_create)

    def __bind_states(self):
        self.states_mapping = {subclasses.name: subclasses(
            self) for subclasses in State.__subclasses__()}

    def __bind_transitions(self):
        self.transitions_mapping = {subclasses.name: subclasses(
            self) for subclasses in Transition.__subclasses__()}
    
    def _create_visualization(self, force_create):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        logpath = os.path.join(dir_path, "automaton_logs", LOGFILE)
        if force_create or not os.path.exists(logpath):
            states = {state: self.states_mapping[state]._visdata for state in self.states_mapping}
            transitions = {transition: self.transitions_mapping[transition]._visdata for transition in self.transitions_mapping}
            with open(logpath, 'w') as log:
                json.dump({"states": states, "transitions": transitions}, log)
    

    def transduce(self, input_data):
        """
        Transduce through the state/transition in techscience module

        Parameters:
          input_data - dictionary of input data from the system

        Return:
          tech_science_response_dictionary.  ^ See Definition above
        """

        # Add input_data format check
        self._get_text(input_data)

        # Retrieve metadata for topic_detection
        self._retrieve_nlu( input_data)

        # Define the metadata for finite state machine
        self._define_metadata()

        # set current_transition
        current_transition = self._set_current_transition(input_data)

        self._generate_base_response()
        self._retrieve_user_attributes(input_data)

        # self.set_intent_classify_topic(input_data)

        try:
            next_state = self.transitions_mapping[current_transition]()
            next_transition = self.states_mapping[next_state]()

            # save transition for next user utterance
            self.techsciencechat["current_transition"] = next_transition
            self.response_dict[
                "response"] = construct_dynamic_response(self.response)
            self.response_dict["template_manager"] = dict(
                self.template_manager_hash.template_manager)
            self.response_dict["techsciencechat"] = self.techsciencechat
            self.response_dict["propose_topic"] = self.propose_topic

            return self.response_dict

        except BaseException:
            return self.response_dict

    def _get_text(self, input_data):
        """Get the plain text from input_data.

        Parameters:
           input_data: raw input dictionary from system module

        Set Instance Variables:
           self.text - processed input utterance
           self.input_data - raw input_data for later use #TODO Can remove

        Output:
           None
        """
        self.text = preprocess_text(input_data["text"][0])
        self.template_manager_hash = SimpleNamespace(
            **{"template_manager": input_data["template_manager"]})


    def _retrieve_nlu(self, input_data):
        """Retrieve NLU metadata

        Parameters:
           input_data:  raw input dictionary from system module

        Set Instance Variables:
           self.central_e - Dataclass central element
           self.returnnlp - Datastructure returnnlp

        Output:
           None
        """

        self.central_e = CentralElement.from_dict(input_data["central_elem"])
        self.returnnlp = [ReturnNLPSegment.from_dict(
            nlp) for nlp in input_data["returnnlp"]]

    def _define_metadata(self):
        """Set the default response metadata.

        Parameters:
           None

        Set Instance Variables:
           self.techsciencechat:
             - not_first_time:  True  (Always because the key will be set after entering automaton)
             - propose_continue:  CONTINUE/STOP/UNCLEAR

           self.response
           self.propose_topic

        """

        self.techsciencechat = {"not_first_time": True,
                                "propose_continue": "CONTINUE",
                                }
        self.response = ""
        self.propose_topic = []

    def _set_current_transition(self, input_data):
        """ Set the current transition for every call on automaton.

        Parameters:
           input_data:  raw input dictionary from system module

        Current transition is set to one of the following three values:
        1. t_init:  first entering techscience module
        2. techsciencechat["current_transition"]: set by previous call to automaton

        Return:
          current_transition
        """
        current_transition = "t_init"  # default transition

        if "techsciencechat" not in input_data:
            return current_transition
        techscience_chat = input_data["techsciencechat"]

        if "current_transition" in techscience_chat:
            self.prev_transition = techscience_chat["current_transition"]
            current_transition = techscience_chat["current_transition"]
        return current_transition

    def _retrieve_user_attributes(self, input_data):
        """Retrieve User Attributes from input_data.

        Parameters:
           input_data:  raw input dictionary from system module

           self.context_topic - List of broader topic from current data
           self.current_topic - Current topic to talk about
           self.topic_history - keytopic already talked about
           self.fact_counter - status to keep track of num facts.  (Prevent Redudancy)
           self.research_counter - status to keep track of research facts
           self.chitchat_counter - status to keep track of chitchat
           self.next_module - proposed next module
        """
        if "context_topic" in input_data["techsciencechat"]:
            self.context_topic = input_data["techsciencechat"]["context_topic"]
        else:
            self.context_topic = []

        if "current_topic" in input_data["techsciencechat"]:
            self.current_topic = input_data["techsciencechat"]["current_topic"]
        else:
            self.current_topic = None

        if "fact_counter" in input_data["techsciencechat"]:
            self.fact_counter = input_data["techsciencechat"]["fact_counter"]
        else:
            self.fact_counter = 0

        if "research_counter" in input_data["techsciencechat"]:
            self.research_counter = input_data[
                "techsciencechat"]["research_counter"]
        else:
            self.research_counter = 0

        if "chitchat_counter" in input_data["techsciencechat"]:
            self.chitchat_counter = input_data[
                "techsciencechat"]["chitchat_counter"]
        else:
            self.chitchat_counter = ""

        if "next_module" in input_data["techsciencechat"]:
            self.next_module = input_data["techsciencechat"]["next_module"]
        else:
            self.next_module = None

    def _generate_base_response(self):
        """Return the default template for response dict

        """
        response = t_e.utterance(selector='topic_modules/default',
                                 slots="techscience",
                                 user_attributes_ref=self.template_manager_hash)
        self.response_dict = {"response": response,
                              "template_manager": dict(self.template_manager_hash.template_manager),
                              "techsciencechat": {
                                  "not_first_time": True,
                                  "propose_continue": "STOP",
                                  "propose_topic": [],
                                  "current_transition": "",
                                  "current_topic": None,
                                  "context_topic": [],
                                  "topic_history": [],
                                  "research_counter": 0,
                                  "fact_counter": 0,
                                  "chitchat_counter": ""
                              },
                              "propose_topic": None,
                              }

    def bulid_intent_handler(self):
        """Call your own customized intent_handler
        """
        raise NotImplementedError

    def bulid_topic_handler(self):
        """Call cusotmized topic_handler
        """
        raise NotImplementedError