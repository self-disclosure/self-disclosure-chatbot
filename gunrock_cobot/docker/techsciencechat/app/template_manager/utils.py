import base64
import json
import logging
import operator
import zlib
from functools import reduce
from typing import Union, TYPE_CHECKING

if TYPE_CHECKING:
    from template_manager.template import Template


logger = logging.getLogger(__name__)


# MARK: - Hashing Template Entry

def hash_utterance(template: 'Template', selector: str, utterance: Union[str, dict]):
    if isinstance(utterance, dict):
        utterance = json.dumps(utterance, sort_keys=True, default=str)
    value = '/'.join(list(filter(None, [template.name, selector, utterance]))).encode()
    uid = zlib.adler32(value).to_bytes(4, byteorder='big')
    return base64.b64encode(uid).decode()[:-2]


# MARK: - For traversing nested dictionary

def traverse(tree: dict, selector: str):
    logging.info(
        "traverse with tree: {}, selector: {}".format(tree, selector))
    selector = selector.split('/')
    return reduce(operator.getitem, selector, tree)


def remove_duplicates(text: str, mode: str):
    if mode not in {'bigram'}:
        return text
