import logging
import warnings
from types import SimpleNamespace
from typing import List, Dict, Tuple, Union

from template_manager.template import Template
from template_manager.tua import TemplateUserAttributes


# MARK: - Logger setup
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setFormatter(logging.Formatter('[TM] %(message)s'))
logger.addHandler(ch)


class TemplateManager:

    def __init__(self, template: 'Template', user_attributes_ref):
        """
        Create an instance of TemplateManager for all interaction with the template system.

        :param template:    name of template. See list of templates for name
        :param user_attributes_ref:     passuser_attributes from cobot
        """

        # Compatibility checking
        # TODO: better check than not Template?
        if not isinstance(template, Template):
            template = Template[template.name]

        self.template = template
        if not user_attributes_ref:
            user_attributes_ref = SimpleNamespace()
        self.user_attributes = TemplateUserAttributes(user_attributes_ref)

    def speak(
        self, selector: str, slots: Dict[str, str], embedded_info: dict = None,
        *, _append_selector_history=True
    ) -> str:
        """
        The main function of template manager. Use this function to get an utterance from shared_template.
        ### IMPORTANT: ###
        Please also look at response_templates/shared_template.yml to see what which template you want
        and what slots you need to provide.

        :param selector:            a string or List[str] that specifies which template you want to use.
                                    e.g. to select from opinion -> agree: use "opinion/agree"

        :param slots:               a dict used to fill in the slots in the templates.
                                    Formatting is done using str Formatter()
                                    e.g. for topic_choice: {'topic_a': 'movies', 'topic_b': 'sports'}
                                    ### IMPORTANT: ###
                                    Include all slots that the template group needs.
                                    Match your dict keys with the template name.

        :param embedded_info:       Optional dict to be passed by reference. Will be extended with info stored
                                    with the utterance if valid dict object is passed.

        :return:                    A completely formatted utterance string that you can return to cobot;
        """

        return self.template.speak(
            selector, slots, self.user_attributes, embedded_info, _append_selector_history=_append_selector_history
        )

    def utterance(self, selector: Union[List[str], str], slots: Dict[str, str]) -> Union[str, dict]:
        warnings.warn("Use speak() instead", DeprecationWarning, stacklevel=2)

        def _str_dict_selector_mock(entry: Union[str, dict], embedded_info: dict = None) -> Union[str, dict]:
            if type(entry) is not str and type(entry) is not dict:
                raise ValueError(f"Entry type invalid: {type(entry)}; {entry}")
            return entry

        template_n_mock = Template[self.template.name]
        template_n_mock._str_dict_selector = _str_dict_selector_mock
        tm = TemplateManager(template_n_mock, self.user_attributes.ua_ref)

        if isinstance(selector, list):
            selector = '/'.join(selector)

        return tm.speak(selector, slots)

    def has_utterance(self, unformatted_utterance: str, selector: str) -> bool:
        warnings.warn("Deprecated, will be removed.", DeprecationWarning, stacklevel=2)
        return self.template.has_utterance(unformatted_utterance, selector)

    def has_selector(self, selector: str) -> bool:
        return self.template.has_selector(selector)

    def get_child_keys(self, selector: str) -> List[str]:
        """
        Return the keys under a particular selector.
        """
        # warnings.warn("Deprecated. Will be removed.", DeprecationWarning, stacklevel=2)
        return self.template.get_child_keys(selector)

    def count_utterances(self, selector: str) -> int:
        return self.template.count_utterances(selector)

    def count_used_utterances(self, selector: str) -> int:
        return self.template.count_used_utterances(selector, self.user_attributes)
