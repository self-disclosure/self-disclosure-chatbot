"""Main Function for techscience_automaton

Define TechscienceAutomaton, which is passed in the transitions and states
as a metadata holder and status keeper.

"""

import logging
from types import SimpleNamespace
import json
import os
from question_handling.quetion_handler import QuestionResponse, QuestionResponseTag
from techscience_utils.techscience_question_handler import TechscienceQuestionHandler
from techscience_utils.techscience_command_detector import TechscienceCommandDetector
from techscience_utils.data_topic import ACTIVATED_CHITCHAT
from techscience_utils.chitchat_utils import ChitChatHandler
from techscience_utils.misc_utils import preprocess_text, construct_dynamic_response
from techscience_utils.nlu_process_utils import NLU_Processor
from techscience_utils.intent_utils import TechScienceIntentHandler
from techscience_utils.topic_utils import TechScienceTopicHandler
from techscience_utils.acknowledgement_utils import TechscienceAcknowledgementHandler
from techscience_utils.logging_utils import set_logger_level, get_working_environment
from nlu.dataclass import ReturnNLP, CentralElement
from techscience_transitions.base import BaseTransition as Transition
from states.base import BaseState as State
from chitchat_automatons.base import ChitChatAutomaton as ChitChat
from selecting_strategy.module_selection import ModuleSelector
from nlu.constants import TopicModule
import template_manager
from typing import List

LOGGER = logging.getLogger(__name__)

ERROR_TEMPLATE = template_manager.Templates.error
LOGFILE = "automaton_map.json"

logging.basicConfig(
    format='[%(levelname)s] %(asctime)s [%(filename)s:%(lineno)d] %(message)s',
)


set_logger_level(get_working_environment())

class TechScienceAutomaton:
    """

    Techscience Response Dictionary

    response: <str>
    prev_hash:  hash from user_attributes
    techsciencechat:
      not_first_time: <bool>
      propose_continue: <str>
      propose_topic: <str>
      current_transition: <str>
      current_topic: <str>
      context_topic: {}
    propose_topic: <str>

    """
    # pylint: disable=no-member
    # pylint: disable=attribute-defined-outside-init
    # pylint: disable=too-many-instance-attributes
    # pylint: disable=no-self-use

    MODULE_NAME = "TECHSCIENCECHAT"
    BACKSTORY_CONFIDENCE_THRESHOLD = 0.77

    SENTIMENT_THRESHOLD = 10
    def __init__(self):
        self.__bind_states()
        self.__bind_transitions()
        self.__register_chitchat()
        self.chitchat_handler = self.get_chitchat_handler()
        self.build_intent_handler()
        self.build_topic_handler()
        self.build_nlu_processor()
        self.build_acknowledgement_handler()

    def __bind_states(self):
        self.states_mapping = {subclasses.name: subclasses(
            self) for subclasses in State.__subclasses__()}

    def __bind_transitions(self):
        self.transitions_mapping = {subclasses.name: subclasses(
            self) for subclasses in Transition.__subclasses__()}

    def __register_chitchat(self):
        self.chitchat_mapping = {}
        for subclasses in ChitChat.__subclasses__():
            if subclasses.header in ACTIVATED_CHITCHAT:
                topics = ACTIVATED_CHITCHAT[subclasses.header]
                if subclasses.topic in topics:
                    ids = topics[subclasses.topic]
                    self.chitchat_mapping[(subclasses.header, subclasses.topic, subclasses.c_id)] = subclasses(self)

    def build_intent_handler(self):
        """Get Intent Handler"""
        self.intent_handler = TechScienceIntentHandler()

    def build_topic_handler(self):
        """Get Topic Handler"""
        self.topic_handler = TechScienceTopicHandler()

    def build_nlu_processor(self):
        self.nlu_processor = NLU_Processor()


    def build_acknowledgement_handler(self):
        self.acknowledgement_handler = TechscienceAcknowledgementHandler()

    def build_static_graph(self, output_file):
        """ This function is only called in local state"""
        json_dict = {}
        states = [{"name": "entry_point", \
                           "size": 5.0, \
                           "position": dict(x="-250.82776", y="50.6904", z="0.0"),
                           "color": "#000000"}]
        transitions = []
        for s in self.states_mapping:
            self.states_mapping[s].write_static_state(states)
        for t in self.transitions_mapping:
            self.transitions_mapping[t].write_static_transition(transitions)
        
        json_dict["states"] = states
        json_dict["transitions"] = transitions

        if os.path.exists(output_file):
            os.remove(output_file)
        with open(output_file, "w") as j:
            json.dump(json_dict, j)

    def build_question_handler(self):
        if len(self.returnnlp_raw) == 0:
            self.question_handler = None
            self.command_detector = None
        else:
            self.question_handler = TechscienceQuestionHandler(self.nlu_processor, \
                                                    self.ack_field_raw, \
                                                    self.user_attributes_ref)
            self.command_detector = TechscienceCommandDetector(self.nlu_processor)


    def transduce(self, input_data, user_attributes: SimpleNamespace = SimpleNamespace(**{})):
        """
        Transduce through the state/transition in techscience module

        Parameters:
          input_data - dictionary of input data from the system

        Return:
          tech_science_response_dictionary.  ^ See Definition above
        """
        # pylint: disable=broad-except
        # Add input_data format check
        self._get_text(input_data)
        self._generate_base_response()
        self.user_attributes_ref = user_attributes
        self.module_selector = ModuleSelector(self.user_attributes_ref)
        
        try:
            # Retrieve metadata for topic_detection
            self._retrieve_nlu(input_data)
            self.ack_field = input_data.get("system_acknowledgement", [])

            if isinstance(self.ack_field, list) and len(self.ack_field) > 0:
                self.ack_field_raw = self.ack_field[0]

            # Define the metadata for finite state machine
            self._define_metadata()
            self._retrieve_user_attributes(input_data)

            # set current_transition
            self.current_transition = self._set_current_transition(input_data)
            self.get_topic_handler()
            self.get_intent_handler()

            
            self.chitchat_tracker = self.chitchat_handler.get_available_chitchats(self.chitchat_tracker, self.chitchat_mapping)
            self.build_question_handler()
            self.next_state = self.transitions_mapping[self.current_transition]()
            self.next_transition = self.states_mapping[self.next_state]()
            self.chitchat_handler.remove_available_chitchats(self.chitchat_tracker)


            self.stored_session_id = self.session_id
            self._save_user_attributes()
            
            # save transition for next user utterance
            self.techsciencechat["prev_state"] = self.next_state
            self.techsciencechat["prev_transition"] = self.current_transition
            self.techsciencechat["current_transition"] = self.next_transition
            self.response_dict["response"] = construct_dynamic_response(self.response)

            self.response_dict["techsciencechat"] = self.techsciencechat
            self.module_selector.propose_topic = self.response_dict.get("propose_topic", None)
            self.module_selector.add_propose_keyword(self.response_dict.get("suggest_keywords", None))

            return self.response_dict

        except Exception as _e:
            self.response_dict[
                "response"] = ""
            LOGGER.error("Transduce Error {}".format(_e), exc_info=True)
            LOGGER.error("For Debugging Purpose {}".format(input_data))
            return self.response_dict

    def get_sys_acknowledgement(self, input_data):
        system_acknowledgement = input_data.get("system_acknowledgement", "")
        return system_acknowledgement

    @property
    def global_state(self):
        return self.module_selector.global_state

    def add_new_chitchat_tracker(self):
        """Add a new chitchat tracker for all the facts, opinion, and trend chitchats
        """
        def _add_fields(d=dict()):
            d["curr_state"] = ""
            d["curr_turn"] = -1
            d["completed_ids"] = []
            d["curr_node"] = ""
            d["unfinished_ids"] = {} # Format will be id: last_turn
            d["is_followup"] = 0 # Check if the current state is a followup
            d["trace"] = ""
            return d

        # Add Available IDS
        chitchat_tracker = _add_fields()
        return chitchat_tracker

    def get_topic_handler(self, rebuild=True):
        """Build topic handler for each transduce
        """
        if rebuild:
            self.topic_handler.build_topic_handler(
                self.returnnlp, self.central_elem)
        return self.topic_handler

    def get_acknowledgement_handler(self, rebuild=True):
        """Build the acknowledgement handler for each transduce
        """
        if rebuild:
            self.acknowledgement_handler = self.acknowledgement_handler.build_acknowledgement_handler(self.ack_field)
        return self.acknowledgement_handler

    def get_chitchat_handler(self, rebuild=True):
        """Build topic handler for each transduce
        """
        if rebuild or self.chitchat_handler is None:
            self.chitchat_handler = ChitChatHandler()
        return self.chitchat_handler

    def get_intent_handler(self, rebuild=True):
        """Get intent handler for transduce"""
        if rebuild:
            self.intent_handler.build_intent_handler(self.returnnlp)
        return self.intent_handler

    def _get_text(self, input_data):
        """Get the plain text and simple information from input_data.

        Parameters:
           input_data: raw input dictionary from system module

        Set Instance Variables:
           self.text - processed input utterance
           self.input_data - raw input_data for later use

        Output:
           None
        """
        text = input_data.get("text", [""])
        self.text = preprocess_text(self.get_first_element(text))
        
        self.last_module = input_data.get("last_module", "")

        a_b_test = input_data.get("a_b_test", ["A"])
        self.a_b_test = self.get_first_element(a_b_test)

        last_response = input_data.get("last_response", [""])
        self.last_response = self.get_first_element(last_response)

        session_id = input_data.get("session_id", [""])
        self.session_id = self.get_first_element(session_id)

        self.suggested_keywords = input_data.get("suggest_keywords")
        if self.suggested_keywords is None:
            self.suggested_keywords = ""
        
        self._user_name = input_data.get("usr_name", "")

    def set_propose_topic(self, propose_topic):
        self.response_dict["propose_topic"] = propose_topic
        self.techsciencechat["propose_topic"] = propose_topic

    @property
    def get_propose_topic(self):
        return self.techsciencechat.get("propose_topic")

    def set_suggest_keywords(self, keywords):
        self.response_dict["suggest_keywords"] = keywords
        LOGGER.debug("Set Suggest_Keywords: {}".format(self.response_dict))

    def set_propose_continue(self, state):
        self.techsciencechat["propose_continue"] = state
        LOGGER.debug("Set propose_continue: {}".format(self.response_dict))

    @property
    def get_keywords_from_response(self):
        LOGGER.debug("Get Suggest_Keywords: {}".format(self.response_dict))
        return self.response_dict.get("suggest_keywords")
    
    @property
    def user_name(self):
        if self._user_name == "<no_name>":
            return None
        return self._user_name

    def get_first_element(self, value):        
        if isinstance(value, list) and len(value) > 0:
            return value[0]
        elif isinstance(value, list):
            return ""
        else:
            return str(value)


    def _retrieve_nlu(self, input_data):
        """Retrieve NLU metadata

        Parameters:
           input_data:  raw input dictionary from system module

        Set Instance Variables:
           self.central_elem - Dataclass central element
           self.returnnlp - Datastructure returnnlp

        Output:
           None
        """
        central_elem_raw = input_data.get("central_elem", {})
        if isinstance(central_elem_raw, list) and len(central_elem_raw) > 0:
            central_elem_raw = central_elem_raw[0]
        try:
            self.central_elem = CentralElement.from_dict(central_elem_raw)
        except:
            self.central_elem = None
        self.central_elem_raw = central_elem_raw
        returnnlp = input_data.get("returnnlp", [])
        if isinstance(returnnlp, list) and len(returnnlp) > 0 and isinstance(returnnlp[0], list):
            returnnlp = returnnlp[0]
        elif not isinstance(returnnlp, list):
            returnnlp = []
        if None in returnnlp:
            LOGGER.warning("[TECHSCIENCE] Returnnlp contains a None Value. {}".format(returnnlp))
        LOGGER.debug("ReturnNLP {}".format(returnnlp))
        self.returnnlp_raw = [i for i in returnnlp if i is not None]
        self.returnnlp = ReturnNLP.from_list(self.returnnlp_raw)

        self.nlu_processor.build_nlu_handler(self.returnnlp_raw, self.central_elem_raw, input_data.get("text"))

        LOGGER.info("Retrieve NLU Data Successful")


    def _define_metadata(self):
        """Set the default response metadata.

        Parameters:
           None

        Set Instance Variables:
           self.techsciencechat:
             - not_first_time:  True  (Always because the key will be set after entering automaton)
             - propose_continue:  CONTINUE/STOP/UNCLEAR

           self.response
           self.propose_topic

        """

        self.techsciencechat = {"propose_continue": "CONTINUE"}
        self.response = ""
        self.system_propose_topic = None

    def _set_current_transition(self, input_data):
        """ Set the current transition for every call on automaton.

        Parameters:
           input_data:  raw input dictionary from system module

        Current transition is set to one of the following three values:
        1. t_init:  first entering techscience module
        2. techsciencechat["current_transition"]: set by previous call to automaton

        Return:
          current_transition
        """
        if "techsciencechat" not in input_data:
            return "t_init_resume"
        techscience_chat = input_data.get("techsciencechat", {})
        current_transition = techscience_chat.get("current_transition", "t_init_resume")
        if self.is_first_time_user(input_data):
            # returning user not resume mode
            if self.is_resume_mode:
                self._reset_user_attributes()
        
        self.prev_transition = techscience_chat.get("prev_transition")            
        return current_transition

    @property
    def is_resume_mode(self):
        last_topic = self.module_selector.last_topic_module
        LOGGER.debug("techscience last topic {}".format(last_topic))
        return last_topic != TopicModule.TECHSCI.value and len(self.topic_history) > 0

    def _reset_user_attributes(self):
        """We write all the data to redis.  Reset the corresponding parameter history to empty.

        topic_history
        context_topic
        new_topics
        """
        self.topic_history = self.topic_handler.default_history #TODO
        self.context_topic = self.topic_handler.default_context #TODO
        self.new_topics = self.nlu_processor.default_new_topics #TODO
        self.current_topic = self.topic_handler.empty_topic_keyword #TODO
        self.module_sentiment_tracker = self.nlu_processor.default_module_sentiment_tracker #TODO

    def check_user_attribute_format(self):
        """Check all the fields passed into techscience is the correct type
        """
        def check_type(field2, field1):
            if type(field1) != type(field2):
                return False
            if isinstance(field1, list):
                same_type = all([check_type(field1, field2) for i in range(min(len(field1), len(field2)))])
                return same_type and len(set([type(i) for i in field1])) <= 1 and len(set([type(i) for i in field1])) <= 2
            if isinstance(field1, dict):
                type1 = set([type(i) for i in field1])
                type2 = set([type(j) for j in field2])
                # Temporary patch TODO
                if len(field1) == 0 or len(field2) == 0:
                    return True
                key_same = len(type1) == len(type2) and (len(type1) == 0 or list(type1)[0] == list(type2)[0])
                val1 = field1.values()
                val2 = field2.values()
                if len(val1) == 0 and len(val2) == 0:
                    return True
                elif len(val1) == 0:
                    ref_val = list(val2)[0]
                else:
                    ref_val = list(val1)[0]
                return key_same and all([check_type(ref_val, val) for val in val2]) and all([check_type(ref_val, val) for val in val1])
            return True

        if not check_type(self.context_topic, self.topic_handler.default_context):
            self.context_topic = copy.deepcopy(self.topic_handler.default_context)
            LOGGER.warning("[TECHSCIENCECHAT] {} is not correct format! Could cause potential error.".format("context_topic"))
            return
        if not check_type(self.current_topic, self.topic_handler.empty_topic_keyword):
            self.current_topic = copy.deepcopy(self.topic_handler.empty_topic_keyword)
            LOGGER.warning("[TECHSCIENCECHAT] {} is not correct format! Could cause potential error.".format("current_topic"))
            return
        if not check_type(self.current_keywords, self.topic_handler.empty_keyword_contexts):
            self.current_keywords = copy.deepcopy(self.topic_handler.empty_keyword_contexts)
            LOGGER.warning("[TECHSCIENCECHAT] {} is not correct format! Could cause potential error.".format("current_keywords"))
            return
        if not check_type(self.topic_history, self.topic_handler.default_history):
            self.topic_history = copy.deepcopy(self.topic_handler.default_history)
            LOGGER.warning("[TECHSCIENCECHAT] {} is not correct format! Could cause potential error.".format("topic_history"))
            return
        if not check_type(self.propose_topic, self.topic_handler.default_propose_topic):
            self.propose_topic = copy.deepcopy(self.topic_handler.default_propose_topic)
            LOGGER.warning("[TECHSCIENCECHAT] {} is not correct format! Could cause potential error.".format("propose_topic"))
            return
        if not check_type(self.chitchat_state, self.chitchat_handler.null_chitchat_state_string):
            self.chitchat_state = copy.deepcopy(self.chitchat_handler.null_chitchat_state_string)
            LOGGER.warning("[TECHSCIENCECHAT] {} is not correct format! Could cause potential error.".format("chitchat_state"))
            return
        if not check_type(self.chitchat_tracker,  self.add_new_chitchat_tracker()):
            self.chitchat_tracker = copy.deepcopy(self.add_new_chitchat_tracker())
            LOGGER.warning("[TECHSCIENCECHAT] {} is not correct format! Could cause potential error.".format("chitchat_tracker"))
            return
        if not check_type(self.new_topics,  self.nlu_processor.default_new_topics):
            self.new_topics = copy.deepcopy(self.topic_handler.default_new_topics)
            LOGGER.warning("[TECHSCIENCECHAT] {} is not correct format! Could cause potential error.".format("new_topics"))
            return
        if not check_type(self.module_sentiment_tracker,  self.nlu_processor.default_module_sentiment_tracker):
            self.module_sentiment_tracker = copy.deepcopy(self.nlu_processor.default_module_sentiment_tracker)
            LOGGER.warning("[TECHSCIENCECHAT] {} is not correct format! Could cause potential error.".format("module_sentiment_tracker"))
            return

        if not check_type(self.cached_evi,  self.default_evi_cache):
            self.cached_evi = copy.deepcopy(self.default_evi_cache)
            LOGGER.warning("[TECHSCIENCECHAT] {} is not correct format! Could cause potential error.".format("cached_evi"))
            return
        return

    @property
    def default_evi_cache(self)->List[str]:
        return []

    def _retrieve_user_attributes(self, input_data):
        """Retrieve User Attributes from input_data.

        Parameters:
           input_data:  raw input dictionary from system module

           self.context_topic - List of broader topic from current data
           self.current_topic - Current topic to talk about
           self.topic_history - keytopic already talked about
           self.next_module - proposed next module
        """
        def add_if_not_exist(key, default, formatter=None):
            """Add key as instance variable"""
            if key in input_data["techsciencechat"]:
                if formatter is None:
                    setattr(self, key, input_data["techsciencechat"][key])
                else:
                    setattr(self, key, formatter(input_data["techsciencechat"][key]))
            else:
                setattr(self, key, default)

        chitchat_tracker = self.add_new_chitchat_tracker()
        add_if_not_exist("context_topic", self.topic_handler.default_context)
        add_if_not_exist("current_topic", self.topic_handler.empty_topic_keyword)
        add_if_not_exist("current_keywords", self.topic_handler.empty_keyword_contexts)
        add_if_not_exist("topic_history", self.topic_handler.default_history)
        add_if_not_exist("propose_topic", self.topic_handler.default_propose_topic)
        add_if_not_exist("fact_counter", self.topic_handler.default_fact_counter)

        # Chitchat Handler
        add_if_not_exist("chitchat_state", self.chitchat_handler.null_chitchat_state_string)
        add_if_not_exist("chitchat_tracker", chitchat_tracker, lambda x: self.chitchat_handler.format_tracker(x))
        
        # NLU Process Handler
        add_if_not_exist("new_topics", self.nlu_processor.default_new_topics)
        add_if_not_exist("module_sentiment_tracker", self.nlu_processor.default_module_sentiment_tracker)

        add_if_not_exist("expected_next_state", None)
        add_if_not_exist("expected_next_detector", self.nlu_processor.default_expected_next_detector) #TODO
        add_if_not_exist("next_module", None)
        add_if_not_exist("cached_evi", self.default_evi_cache)

        # For DEBUG ONLY
        add_if_not_exist("error_response", False)
        add_if_not_exist("stored_session_id", None)

    def increase_fact_counter(self):
        self.fact_counter += 1

    def reset_fact_counter(self):
        self.fact_counter = self.topic_handler.default_fact_counter
    
    def _save_user_attributes(self):
        """Save keys into techscience_chat attributes
        """
        self.techsciencechat["context_topic"] = self.context_topic
        self.techsciencechat["current_topic"] = self.current_topic
        self.techsciencechat["fact_counter"] = self.fact_counter
        self.techsciencechat["topic_history"] = self.topic_history
        self.techsciencechat["current_keywords"] = self.current_keywords
        self.techsciencechat["propose_topic"] = self.propose_topic

        self.techsciencechat["chitchat_tracker"] = self.chitchat_tracker
        self.techsciencechat["chitchat_state"] = self.chitchat_state

        
        

        self.techsciencechat["new_topics"] = self.new_topics
        self.techsciencechat["module_sentiment_tracker"] = self.module_sentiment_tracker
        self.techsciencechat["cached_evi"] = self.cached_evi

        self.techsciencechat["expected_next_state"] = self.expected_next_state
        self.techsciencechat["next_module"] = self.next_module


        self.techsciencechat["error_response"] = self.error_response
        self.techsciencechat["stored_session_id"] = self.stored_session_id

    def _generate_base_response(self):
        """Return the default template for response dict

        """
        response = ""
        self.response_dict = {
            "response": response,
            "techsciencechat": {
                "propose_continue": "STOP"},
            "propose_topic": None,

        }

    def is_first_time_user(self, input_data):
        """Return true if first time user.
        If no session id stored, then it is first time user

        If current session id is different from before, then it is also considered as first entry
        """
        stored_session_id = getattr(self, "stored_session_id") 
        LOGGER.debug("[TECHSCIENCE] is_first time check {}".format(stored_session_id))
        if stored_session_id is None:
            return True # session id never stored, this is true first time user
        return False 

    def get_answer_from_backstory(self):
        """Retrieve backstory from central_elements"""
        return self.central_elem.backstory

    def get_chitchat_automaton(self, header, topic, _id):
        """Retrieve the corresponding automaton given keys
        
        Parameter:
          - header: opinion, trend, chitchat
          - topic: activated topic
          - _id: id for the topic
        
        Return: Corresponding Chitchat Automaton
        """
        return self.chitchat_mapping[(header, topic, _id)]

    
