import logging
from enum import auto, Enum
from typing import Any, Dict, List, Union
from types import SimpleNamespace

try:
    from cobot_python_sdk.user_attributes import UserAttributes
    has_cobot_user_attributes = True
except ImportError:
    has_cobot_user_attributes = False


class LoggerAdapter(logging.LoggerAdapter):
    def __init__(self, prefix: str, subprefix: str, logger):
        super(LoggerAdapter, self).__init__(logger, {})
        self.prefix = prefix
        self.subprefix = subprefix

    def process(self, msg, kwargs):
        return f"[{self.prefix}] <{self.subprefix}>: {msg}", kwargs


# MARK: - UserAttribute

class UserAttributeAdaptor:

    class Mode(Enum):
        DIRECT, DICT = auto(), auto()

        def __eq__(self, o):
            if isinstance(o, str):
                return self.name == o.upper()
            else:
                return super().__eq__(o)

    def __init__(
        self, module: str,
        user_attributes_ref: Union['UserAttributes', SimpleNamespace, Dict[str, Any]],
        *,
        auto_add_missing_keys=False
    ):
        """
        :params module (str): name of the module, must be as one of the root key if user_attributes_ref is dict.
        :params user_attributes_ref:
            if type is dict, must be in the format of {<module_name>: {modules_user_attributes}}
            must contain the following keys: [<module_name>, 'template_manager', 'propose_topic'],
            optional keys: ['visit']
        """
        self.logger = LoggerAdapter(self.__class__.__name__, module, logging.getLogger(__name__))

        self.module = module
        if not user_attributes_ref:
            self.logger.warning(f"user_attributes_ref is invalid: {type(user_attributes_ref)}")
            raise ValueError

        self._ua_ref = user_attributes_ref

        if not self.has_required_key(module, auto_add_missing_keys):
            raise KeyError

    def __repr__(self):
        return self._ua_ref.__repr__()

    def has_required_key(self, key: Union[str, List[str]], auto_add=False):
        if isinstance(key, str):
            key = [key]

        if auto_add:
            for k in key:
                if not hasattr(self.ua_ref, k):
                    if self.mode is UserAttributeAdaptor.Mode.DICT:
                        self._ua_ref[k] = {}
                    elif self.mode is UserAttributeAdaptor.Mode.DIRECT:
                        setattr(self.ua_ref, k, {})
                    else:
                        return False
            return True
        else:
            missing_keys = [k for k in key if not hasattr(self.ua_ref, k)]
            if any(missing_keys):
                self.logger.warning(
                    f"user_attributes_ref {type(self.ua_ref)} doesn't contain required keys: {missing_keys}; "
                    f"{self.ua_ref}", stack_info=True)
                return False
            else:
                return True

    @property
    def mode(self) -> 'UserAttributeAdaptor.Mode':
        if has_cobot_user_attributes and isinstance(self._ua_ref, UserAttributes):
            return UserAttributeAdaptor.Mode.DIRECT
        elif isinstance(self._ua_ref, SimpleNamespace):
            return UserAttributeAdaptor.Mode.DIRECT
        elif isinstance(self._ua_ref, dict):
            return UserAttributeAdaptor.Mode.DICT
        else:
            return UserAttributeAdaptor.Mode.DIRECT

    @property
    def ua_ref(self):
        """
        Attempt to generate a compatible SimpleNamespace ua_ref
        """
        if self.mode is UserAttributeAdaptor.Mode.DICT:
            return SimpleNamespace(**self._ua_ref)
        else:
            return self._ua_ref

    @property
    def _storage(self) -> dict:
        """
        Mirror to ua_ref's root 'store' key
        """
        if self.mode is UserAttributeAdaptor.Mode.DIRECT:
            if not (self._ua_ref and
                    hasattr(self._ua_ref, self.module) and
                    getattr(self._ua_ref, self.module) is not None):
                setattr(self._ua_ref, self.module, dict())
            return getattr(self._ua_ref, self.module)
        else:
            return self._ua_ref

    def __getitem__(self, key):
        return self._storage[key]

    def __setitem__(self, key, value):
        self._storage[key] = value

    def get(self, key, default=None):
        try:
            return self.__getitem__(key)
        except KeyError:
            return default

    def _get_raw(self, key: str):
        if self.mode is UserAttributeAdaptor.Mode.DIRECT:
            try:
                return getattr(self._ua_ref, key)
            except AttributeError as e:
                self.logger.warning(e)
                return None
        else:
            return self._ua_ref.get(key)

    def _set_raw(self, key: str, value: Any):
        setattr(self._ua_ref, key, value)
