from enum import Enum
from typing import List, Optional
import re
from nlu.command_detector import CommandDetector
from techscience_utils.logging_utils import set_logger_level, get_working_environment
from techscience_utils.data_topic import BROAD_TO_SPECIFIC
"""Copied from Movie
"""

import logging
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class TechscienceCommandDetector:

    def __init__(self, nlu_processor):
        self.nlu_processor = nlu_processor
        self.common_command_detector = CommandDetector(self.nlu_processor.user_utterance, self.nlu_processor.returnnlp)

    @property
    def exit_techscience(self):
        broad_topics = list(BROAD_TO_SPECIFIC.keys())
        system_detected_jumpout = any([self.common_command_detector.is_request_jumpout(b) for b in broad_topics])

        techscience_based_jumpout = "get_off_topic" in self.nlu_processor.sys_intent
        return techscience_based_jumpout or system_detected_jumpout

    @property
    def has_command(self):
        return self.common_command_detector.has_command_complete()

    @property
    def req_topic(self):
        LOGGER.debug("Request Topic System Intent {0}\n, Lexical Intent {1}\n, Topic Intent {2}".format(self.nlu_processor.sys_intent,\
        self.nlu_processor.lexical_intent, self.nlu_processor.topic_intent))
        return "req_scitech" in self.nlu_processor.topic_intent or "ask_info" in self.nlu_processor.lexical_intent \
        or "ans_wish" in self.nlu_processor.lexical_intent or "req_topic_jump" in self.nlu_processor.sys_intent or "req_topic" in self.nlu_processor.topic_intent