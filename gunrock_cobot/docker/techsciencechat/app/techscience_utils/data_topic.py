"""The topics can be categorized into three levels:
1. Broader topics
  : Broader topic has a much generalized semantics.
    - template contain facts, opinion, ask_favorite
2. System Handle Topics
    - template contain facts, opinion
    - broader topics 

"""
PRIORITY = ["artificial_intelligence", "ipads", "computers", "engineering"]

DYNAMIC_CHITCHATS = ["retrieval"]

INTRO_CHITCHATS = [("trend", "ipads", 1), ("trend", "alexa", 1)]

ACTIVATED_CHITCHAT = {"opinion": {"ipads":[1,], "artificial_intelligence":[1,],}, \
                     "facts": {"computers": [1,3], "artificial_intelligence":[1,2,6,10], "ipads": [1,]}, \
                     "trend": {"cybertruck":[1,], "smartphone":[1,], "ipads":[1,2], "medicine":[1,], "alexa":[1,]}}

BROAD_TO_SPECIFIC = {"science": set(["physics", "astronomy", "medicine", "chemistry", "biology"]),
                     "technology": set(["computers", "artificial_intelligence", "engineering"]),
                    }

TOPIC_TO_FACT = {"artificial_intelligence": set(["alexa", "artificial_intelligence", "robot", "machine_learning", "computer_vision"]),
                 "computers": set(["computers","blockchain", "alexa", "apple", "bitcoin", "google", "microsoft", "robot", "virtual_reality"]),
                 "astronomy": set(["black_hole", "aliens", "space", "star", "universe"]),
                 "medicine": set(["medicine"]),
                 "chemistry": set(["chemistry"]),
                 "biology": set(["biology"]),
                 "physics": set(["classic_physics"]),
                 "cryptocurrency": set(["bitcoin", "ethereum"]),
                 "engineering": set(["google", "microsoft", "ipads"]),
}

PROPOSAL_MAPPING = {"artificial_intelligence": ["computers", "alexa", "robot"]}
TRENDY_TOPICS = ["cyber_truck", "smart_phones", "ipads"]

"""TOPIC_MAPPING NORMALIZE EACH TOPIC TO THE KEYWORD WE CAN HANDLE.
"""
KEYWORD_MAPPING = {"artificial_intelligence": set(["artificial intelligence", "ai", "a.i", "a.i", "a. i", "intelligence", "a i", "a. i"]),
            "robot": set(["robotics", "robots", "robot"]),
            "astronomy":set(["astronomy"]),
            "space":set(["space"]),
            "bitcoin": set(["bitcoins", "bitcoin"]),
            "biology":set(["bio", "biology"]),
            "chemistry":set(["chem", "chemistry"]),
            "computers":set(["computer","c.s", "computer science", "computer engineering", "c s"]), # Refactor?  CS vs CS as hobby
            "engineering":set(["engineering"]),
            "medicine":set(["medicine"]),
            "amazon":set(["amazon","amazon s"]),
            "phones":set(["phones"]),
            "cyber_truck": set(["cyber truck"]),
            "smart_phones": set(["smart phones"]),
            "physics":set(["physics"]),
            "science":set(["science", "sci"]),
            "virtual_reality":set(["v.r"]),
            "aliens": set(["alien", "e.t"]),
            "black_hole": set(["black hole"]),
            "star": set(["stars"]),
            "ipads": set(["ipad", "ipads", "i pads"]),
            "technology": set(["technology"]),
            "blockchain": set(["blockchains"]),
            "cryptocurrency": set(["crypto currency"]),
            "machine_learning": set(["machine learning", "m l", "m.l", "m.l"]),
            "computer_vision": set(["computer_vision", "computer vision", "c v"])
            }

RESPONSE_VARIATION = {"artificial_intelligence": set(["artificial intelligence", "a i"]),
            "robot": set(["robotics", "robots", "robot"]),
            "astronomy":set(["astronomy"]),
            "biology":set(["biology"]),
            "chemistry":set(["chemistry"]),
            "computers":set(["computers", "computer science", "computer engineering"]), # Refactor?  CS vs CS as hobby
            "engineering":set(["engineering"]),
            "general":set(["general"]),
            "medicine":set(["medicine"]),
            "physics":set(["physics"]),
            "science":set(["science"]),
            "virtual_reality":set(["v.r", "virtual reality"]),
            "aliens": set(["alien", "e.t"]),
            "black_hole": set(["black hole"]),
            "star": set(["stars"]),
            "technology": set(["technology"]),
            "blockchain": set(["blockchains"]),
            "cryptocurrency": set(["crypto currency"]),
            "machine_learning": set(["machine learning", "m l", "m.l", "m.l"]),
            "computer_vision": set(["computer vision", "c.v"])
            }


#TODO: Refactor topic lists 

TOPIC_LIST = ["science", "technology", "tech", "space", "apple", "google", "ai", "a.i", "a. i", 
              "internet", "black hole", "blockchain", "bitcoin", "virtual reality", "artificial intelligence", "computer",
              "robot", "robots", "astronomy", "biology", "engineering", "physics", "chemistry", "medicine",
              "robotics", "nanotech", "biotech", "energy", "politics", "security", "software", "hardware", "medicines",
              "psychology", "social science", "anthropology", "nanoscience", "computer science", "epidemiology",
              "paleontology", "neuroscience", "animal science", "nano tech", "nano technology", "computers"]


####################################################################
####################################################################

MODULE_TO_RESPONSE = {'topic_movietv': 'movies and T.V. shows',
                        'topic_newspolitics': 'the news',
                        'topic_weather': 'the weather',
                        'topic_music': 'music',
                        'topic_book': 'books',
                        'topic_sport': 'sports',
                        'topic_holiday': 'holidays',
                        'topic_food': 'food',
                        'topic_travel': 'travel',
                        'topic_animal': 'animals',
                        'topic_game': 'games'}
TOPIC_TO_MODULES = {'topic_movietv': 'MOVIECHAT',
                                'topic_newspolitics': 'NEWS',
                                'topic_weather': 'WEATHER',
                                'topic_music': 'MUSICCHAT',
                                'topic_book': 'BOOKCHAT',
                                'topic_sport': 'SPORT',
                                'topic_holiday': 'HOLIDAYCHAT',
                                'topic_food': 'FOODCHAT',
                                'topic_travel': 'TRAVELCHAT',
                                'topic_animal': 'ANIMALCHAT',
                                'topic_game': 'GAMECHAT'}

BLACKLIST = ["ass", "asshole", "anal", "anus", "arsehole", "arse", "bitch", "bangbros", "bastards",
            "bitch", "tits", "butt", "blow job", "boob", "bra", "cock", "cum", "cunt", "dick", "shit",
            "sex", "erotic", "fuck", "fxxk", "f\*\*k", "f\*\*c", "f\*ck", "fcuk", "gang bang", "gangbang",
            "genital", "damn", "horny", "jerk off", "jerkoff", "kill", "loser", "masturbate", "naked",
            "nasty", "negro", "nigger", "nigga", "orgy", "pussy", "penis", "perve", "rape", "fucked",
            "fucking",  "racist", "sexy", "strip club", "vagina", "faggot", "fag", "drug", "weed",
            "kill", "murder", "incel", "prostitute", "slut", "whore", "cuck", "cuckold", "shooting", "nazis",
            "nazism", "nazi", "sgw", "balls", "thot", "virgin", "cumshot", "cumming", "dildo", "vibrator", "sexual",
            "douche", "douche", "bukkake", "douchebag", "porn", "porngraph", "porngraphy", "died", "killed",
            "killing", "murdered", "murdering", "kills", "murders", "clit", "clitoris", "mushroom tatoo",
            "dammit", "wtf", "breasts", "tits", "boobies", "orgasm"]