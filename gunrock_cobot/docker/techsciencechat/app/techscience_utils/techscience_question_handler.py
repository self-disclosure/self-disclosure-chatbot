"""Techscience Question Handler
"""

# in your topic level question handler (take movie as example)
from nlu.question_detector import QuestionDetector
from question_handling.quetion_handler import QuestionHandler, QuestionResponse, QuestionResponseTag
from typing import Optional, List
from types import SimpleNamespace

from techscience_utils.logging_utils import set_logger_level, get_working_environment
import logging
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())
# class TechscienceQuestion(Enum):
#     BACKSTORY_QUESTION = "backstory_question"
#     FACTUAL_QUESTION = "factual_question"
#     REQUEST_QUESTION = "request_question"
#     OPINION_QUESTION = "open_question"
#     FOLLOWUP_QUESTION = "followup_question"

class TechscienceQuestionDetector(QuestionDetector):
    def __init__(self, complete_user_utterance: str, returnnlp: Optional[List[dict]], nlu_processor=None):
        super(TechscienceQuestionDetector, self).__init__(complete_user_utterance, returnnlp)
        self.nlu_processor = nlu_processor

    def has_question_seg(self, seg_i):
        LOGGER.debug("Segmentation Regex {}".format(set(self.nlu_processor.returnnlp[seg_i].intent.lexical)))
        return (self.is_not_command(seg_i) and self.is_not_why_not(seg_i) and
                (self.is_question_by_regex(seg_i) or self.is_question_by_dialog_act(seg_i))) or \
            self.is_question_in_command_form(seg_i)

    def yes_but_question_detection(self) -> bool:
        # Finding the question after the word, but
        # find the segment that starts with but
        seg_i = self.nlu_processor.get_seg_from_text("but")
        LOGGER.debug("DEBUG Segmentation {0} has the text {1}".format(seg_i, "but"))
        return self.has_question_seg(seg_i)

    def get_question_segment_text(self):
        # todo: improve it to check more segments
        return self.nlu_processor.clean_utterance(self.returnnlp[-1].text)

class TechscienceQuestionHandler(QuestionHandler):
    def __init__(self, nlu_processor, system_acknowledgement, user_attribute_ref):
        super(TechscienceQuestionHandler, self).__init__(nlu_processor.user_utterance, nlu_processor.returnnlp_raw, \
        system_ack=system_acknowledgement, user_attributes_ref=user_attribute_ref)
        self.nlu_processor = nlu_processor
        # self.system_question_handler = None
        self.system_backstory_response = None

        self.complete_user_utterance = self.nlu_processor.user_utterance
        self.returnnlp = self.nlu_processor.returnnlp
        self.question_detector = TechscienceQuestionDetector(self.nlu_processor.user_utterance, self.returnnlp, self.nlu_processor)

        self.question_segment_text = self.question_detector.get_question_segment_text()
        self.backstory_threshold = 1.0
        self.system_ack = system_acknowledgement
        self.user_attribute_ref = user_attribute_ref if user_attribute_ref else SimpleNamespace()

    # def get_question_handler(self, backstory_threshold=1.0):
    #     return QuestionHandler(self.nlu_processor.user_utterance, 
    #         self.nlu_processor.returnnlp_raw, # or input_data['returnnlp'][0] for remote module
    #     backstory_threshold,
    #     self.system_acknowledgement,
    #     self.user_attribute_ref # the same thing you pass to template maanger
    #     )
    
    def get_backstory_answer(self) -> Optional[QuestionResponse]:
        if self.system_backstory_response:
            return self.system_backstory_response.response
        return None



    @property
    def dont_know_answer(self):
        return self.generate_i_dont_know_response()



        

    @property
    def question_exists(self):
        LOGGER.debug("Testing Whether Question Exists {}".format(self.complete_user_utterance))
        
        if self.question_detector:
            LOGGER.debug("Has Question {}".format(self.question_detector.has_question()))
            self.nlu_processor.print_dialog_acts()
            if self.question_detector.has_question():
                return True
            return self.question_detector.yes_but_question_detection()
        else:
            return any([v.startswith("ask") for v in self.nlu_processor.lexical_intent])

    def backstory_exists(self, confidence_thres):
        if self.system_backstory_response and confidence_thres == self.backstory_threshold:
            return self.system_backstory_response
        self.backstory_threshold = confidence_thres

        potential_backstory_answer = self.handle_question()
        if not potential_backstory_answer:
            return False

        if potential_backstory_answer.tag == "ack_question_idk":
            return False
        self.system_backstory_response = potential_backstory_answer
        return True

    
    @property
    def factual_question(self):
        if self.question_detector:
            return self.question_detector.is_factual_question()
        else:
            return "ask_fact" in self.nlu_processor.lexical_intent
    @property
    def opinion_question(self):
        if self.question_detector:
            return self.question_detector.is_opinion_question()
        else:
            return "ask_opinion" in self.nlu_processor.lexical_intent

    @property
    def yes_no_question(self):
        if self.question_detector:
            return self.question_detector.is_yes_no_question()
        else:
            return "ask_yesno" in self.nlu_processor.lexical_intent