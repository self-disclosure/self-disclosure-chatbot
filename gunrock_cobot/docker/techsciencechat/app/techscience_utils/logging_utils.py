import os
import logging

def get_working_environment():
    if os.environ.get('STAGE') == 'PROD' or os.environ.get('STAGE') == 'BETA':
        return os.environ.get('STAGE').lower()
    else:
        return "local"
def set_logger_level(working_environment):
    if working_environment == "local":
        logging.getLogger().setLevel(logging.DEBUG)
    else:
        logging.getLogger().setLevel(logging.INFO)