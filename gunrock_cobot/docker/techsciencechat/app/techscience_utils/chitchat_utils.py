"""Utilities to process chitchat
"""
import logging
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from techscience_utils.data_topic import ACTIVATED_CHITCHAT, INTRO_CHITCHATS, DYNAMIC_CHITCHATS
from techscience_utils.logging_utils import set_logger_level, get_working_environment
from chitchat_automatons.base import ChitChatAutomaton

import numpy as np
from typing import Dict


LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())
class ChitChatHandler:
    def __init__(self):
        self._get_available_chitchats()
        return

    def get_available_chitchats(self, chitchat_tracker:str, chitchat_mapping:Dict[tuple, ChitChatAutomaton])->Dict:
        """Set Chitchat's available IDs.
           Define available_ids as dictionary:
             - {(state_tuple): {keywords:[], history_score:int}}
        """
        LOGGER.debug("Getting available chitchats")
        available_ids = {}
        # Filter chitchat_tuple by available_ids
        for chitchat_state in chitchat_mapping:
            header, topic, c_id = chitchat_state
            chitchat_automaton = chitchat_mapping[chitchat_state]
            if header in self.available_chitchat \
                and topic in self.available_chitchat[header] and c_id in self.available_chitchat[header][topic]:
                available_ids[chitchat_state] = dict(keywords = chitchat_automaton.keywords,
                                                     history_score = chitchat_automaton.history_score)
        chitchat_tracker["available_ids"] = available_ids
        if "available_ids" in chitchat_tracker:
            LOGGER.debug("Retrieved chitchat_tracker available ids successful")
            LOGGER.debug("Chitchat Tracker {}".format(chitchat_tracker))
        return chitchat_tracker

    def remove_available_chitchats(self, chitchat_tracker):
        """Remove available chitchats from chitchat tracker
        """
        if "available_ids" in chitchat_tracker:
            del chitchat_tracker["available_ids"]

    def _get_available_chitchats(self):
        self.available_chitchat = ACTIVATED_CHITCHAT

    def get_current_turn(self, chitchat_tracker):
        return chitchat_tracker["curr_turn"]
        
    def get_next_turn(self, chitchat_tracker):
        """Increment Chitchat counter to the next turn.
        Used in:
          chitchat_automaton

        Parameter:
          chitchat_tracker:  from master automaton
        Return:
          current turn number
        """
        if not chitchat_tracker["is_followup"]:
            chitchat_tracker["curr_turn"] += 1
        return self.get_current_turn(chitchat_tracker)

    def preprocess_chitchat_state(self, chitchat_state):
        return self._str_to_tup(chitchat_state)

    def postprocess_chitchat_state(self, chitchat_state):
        return self._tup_to_str(chitchat_state)

    def preprocess_chitchat_tracker(self, chitchat_tracker):
        """Chitchat Tracker is raw data from master automaton

        Parameter:
          chitchat_tracker - raw data from master automaton

        Return:
          processed chitchat tracker
        """

        _tracker = dict()

        _tracker["curr_state"] = self._str_to_tup(chitchat_tracker["curr_state"])
        _tracker["curr_turn"] = chitchat_tracker["curr_turn"]
        _tracker["curr_node"] = None if chitchat_tracker["curr_node"] == "" else chitchat_tracker["curr_node"]
        _tracker["completed_ids"] = [self._str_to_tup(s) for s in chitchat_tracker["completed_ids"]]
        _tracker["unfinished_ids"] = {self._str_to_tup(s):chitchat_tracker["unfinished_ids"][s] for s in chitchat_tracker["unfinished_ids"]}
        _tracker["is_followup"] = chitchat_tracker["is_followup"]
        _tracker["trace"] = chitchat_tracker["trace"]

        if "available_ids" in chitchat_tracker:
            _tracker["available_ids"] = chitchat_tracker["available_ids"]
        else:
            _tracker["available_ids"] = []
        return _tracker

    def postprocess_chitchat_tracker(self, chitchat_tracker):
        """Postprocess Chitchat Tracker for techsciencechat

        Parameter:
          chitchat_tracker - data from chitchat automaton

        Return:
          chitchat tracker format in techsciencechat
        """

        _tracker = dict()

        _tracker["curr_state"] = self._tup_to_str(chitchat_tracker["curr_state"])
        _tracker["curr_turn"] = chitchat_tracker["curr_turn"]
        _tracker["curr_node"] = "" if chitchat_tracker["curr_node"] is None else chitchat_tracker["curr_node"]
        _tracker["completed_ids"] = [self._tup_to_str(s) for s in chitchat_tracker["completed_ids"]]
        _tracker["unfinished_ids"] = {self._tup_to_str(s):chitchat_tracker["unfinished_ids"][s] for s in chitchat_tracker["unfinished_ids"]}
        _tracker["is_followup"] = chitchat_tracker["is_followup"]
        _tracker["trace"] = chitchat_tracker["trace"]
        return _tracker

    def _str_to_tup(self, s):
        if isinstance(s, tuple):
            return s
        if s == self.null_chitchat_state_string:
            return self.null_chitchat_state
        header, keyword, _id = s.split("-")
        return tuple([header, keyword, int(_id)])

    def _tup_to_str(self, tup):
        if isinstance(tup, str):
            return tup
        if tup == self.null_chitchat_state:
            return self.null_chitchat_state_string
        return "-".join([str(t) for t in tup])

    def is_activated_chitchat(self, header, topic, c_id):
        """Check if chitchat is activated

        Used in:
          chitchat_automaton
        """
        LOGGER.debug("Checking if the chitchat is activated {}".format([header, topic, c_id]))
        if header not in ACTIVATED_CHITCHAT:
            return False
        header_val =  ACTIVATED_CHITCHAT[header]
        if topic not in header_val:
            return False
        if c_id not in header_val[topic]:
            return False
        return True

    def chitchat_can_handle(self, chitchat_tracker, header, topic):
        """Return True if chitchat state is valid.  This should be called after preprocess
        
        Used in:
          Master Automaton: states
        """
        for state in chitchat_tracker["available_ids"]:
            
            if header == state[0] and topic == state[1]:
                return True
        
        return False

    def is_valid_chitchat(self, chitchat_tracker):
        """Return True if chitchat state is valid.  This should be called after preprocess
        
        Used in:
          Master Automaton: states
        """
        
  
        null_chitchat_state = self.null_chitchat_state
        chitchat_state = chitchat_tracker["curr_state"]
        if chitchat_state == null_chitchat_state:
            return False

        completed_chitchats = chitchat_tracker["completed_ids"]
        available_chitchats = chitchat_tracker["available_ids"]
        # In Available ID
        return chitchat_state not in completed_chitchats and chitchat_state in available_chitchats

    def new_chitchat_state(self, chitchat_tracker, header, topic, context_topic={}, propose_topic=[], get_all=False):
        """Retrieve the new chitchat_state for the current header and topic
           Chitchat tracker is postprocessed.

        Parameter:
          chitchat_tracker: processed chitchat tracker
          header: opinion, trend, fact
          topic: could be None #TODO: HANDLE THIS
        
        """
        LOGGER.debug("CHITCHAT TRACKER {}".format(chitchat_tracker))
        completed_chitchats = chitchat_tracker["completed_ids"]
        available_chitchats = chitchat_tracker["available_ids"]

    
        potential_chitchats = [tuple(t) for t in available_chitchats if t not in completed_chitchats if t[0] == header or header is None]
        potential_chitchats.sort(key=lambda x: available_chitchats[x]["history_score"], reverse=True)

        # For now, we will just choose the first one.  Later, we can choose
        # by keyword or better score TODO
        # Filter by topic if topic is not None

        keyword_chitchats = []
        if topic is not None:
            potential_chitchats = [(header, t, _id) for header, t, _id in potential_chitchats if t == topic]
        
        if len(propose_topic) > 0:
            keyword_chitchats = [(header, t, _id) for header, t, _id in potential_chitchats if t in propose_topic]
            keyword_chitchats.sort(key=lambda x: available_chitchats[x]["history_score"], reverse=True)

        LOGGER.debug("{}".format(keyword_chitchats))
        # Get list of positive context topics
        if len(context_topic) > 0:
            if topic in context_topic:
                contexts = [context_topic[topic][t] for t in context_topic[topic] if context_topic[topic][t] >= 0]
            else:
                contexts = [context_topic[topic][t] for topic in context_topic for t in context_topic[topic] if context_topic[topic][t] >= 0]


            potential_chitchats.sort(key=lambda x: common_words(available_chitchats[x]["keywords"], contexts))
        potential_chitchats = keyword_chitchats + potential_chitchats
        LOGGER.debug("{}".format(potential_chitchats))
        if get_all:
            return potential_chitchats
        if len(potential_chitchats) > 0:
            return potential_chitchats[0]
        else:
            return self.null_chitchat_state


    def store_current_state_as_complete(self, chitchat_tracker, storage=False):
        """Chitchat Tracker is a valid chitchat.  Store chitchat 
           In Complete.

           parameter:
             chitchat_tracker: processed chitchat Tracker
           output:
             chitchat_tracker
        """

        # Store chitchat_tracker["curr_state"] into completed ids
        if storage:
            curr_state = self._tup_to_str(chitchat_tracker["curr_state"])
            chitchat_tracker["curr_state"] = self.null_chitchat_state_string
        else:
            curr_state = chitchat_tracker["curr_state"]
            chitchat_tracker["curr_state"] = self.null_chitchat_state
        
        if curr_state[0] not in DYNAMIC_CHITCHATS:
            chitchat_tracker["completed_ids"].append(curr_state)
        chitchat_tracker["curr_turn"] = -1
        return chitchat_tracker

    def initialize_chitchat(self, chitchat_tracker, chitchat_state, master=True):
        chitchat_tracker["curr_state"] = chitchat_state
        if master:
            chitchat_tracker["curr_turn"] = 0
        else:
            chitchat_tracker["curr_turn"] = -1
        return chitchat_tracker

    @property
    def null_chitchat_state(self):
        return (None, None, -1)

    @property
    def null_chitchat_state_string(self):
        return ""

    def format_tracker(self, chitchat_tracker):
        if "curr_state" not in chitchat_tracker:
            chitchat_tracker["curr_state"] = self.null_chitchat_state_string
        if "curr_turn" not in chitchat_tracker:
            chitchat_tracker["curr_turn"] = -1
        if "completed_ids" not in chitchat_tracker:
            chitchat_tracker["completed_ids"] = []
        if "unfinished_ids" not in chitchat_tracker:
            chitchat_tracker["unfinished_ids"] = {}

        if "curr_node" not in chitchat_tracker:
            chitchat_tracker["curr_node"] = ""

        if "trace" not in chitchat_tracker:
            chitchat_tracker["trace"] = ""
        if "is_followup" not in chitchat_tracker:
            chitchat_tracker["is_followup"] = 0
        return chitchat_tracker
    
    def detect_keywords(self, new_topics:Dict[str, str], chitchat_tracker:dict, header:str):
        if "available_ids" not in chitchat_tracker:
            return []
        completed_ids = []
        if "completed_ids" in chitchat_tracker:
            completed_ids = chitchat_tracker["completed_ids"]
        completed_topics = [ self._str_to_tup(t)[1] for t in completed_ids if self._str_to_tup(t)[0] == header]
        ids = chitchat_tracker["available_ids"]
        LOGGER.debug("CHITCHAT Filtered Keyword Match {}".format(new_topics))
        filtered = [states[1] for states in ids if states[0] == header and common_words(ids[states]["keywords"], list(new_topics.keys()), 100) and states[1] not in completed_topics]
        LOGGER.debug("CHITCHAT Filtered Keyword Match {}".format(filtered))
        return filtered

    def get_node(self, chitchat_tracker):
        return chitchat_tracker["curr_node"]

    def get_intro_chitchat_tracker(self, chitchat_tracker):
        chitchat_states = self.new_chitchat_state(chitchat_tracker, None, None, get_all=True)
        LOGGER.debug("CHITCHAT States {}".format(chitchat_states))
        LOGGER.debug("CHITCHAT Intro {}".format(INTRO_CHITCHATS))
        for c in chitchat_states:
            if c in INTRO_CHITCHATS:
                chitchat_tracker["curr_state"] = c
                chitchat_tracker["curr_turn"] = 0
                return self._tup_to_str(c)

        return self.null_chitchat_state_string

    def get_turn_level_detector(self, chitchat_tracker, chitchat_mapping):
        chitchat_state = chitchat_tracker.get("curr_state")
        chitchat_turn = chitchat_tracker.get("curr_turn", 0)
        default_detector = lambda automaton:False
        # Turn it into tuple
        header, keyword, _id = self._str_to_tup(chitchat_state)

        if header is None:
            return default_detector

        chitchat_automaton = chitchat_mapping[(header, keyword, _id)]
        return chitchat_automaton.turn_level_detector.get(chitchat_turn+1, default_detector)

def common_words(list1, list2, threshold=90):

    if len(list1) == 0 or len(list2) == 0:
        return False
    commons = []
    for l in list1:
        commons.extend(process.extract(l, list2, limit=len(list2)))

    filtered = [t[1] for t in commons if t[1] >= threshold]
    if len(filtered) == 0:
        return False
    return np.mean(filtered) > 0

