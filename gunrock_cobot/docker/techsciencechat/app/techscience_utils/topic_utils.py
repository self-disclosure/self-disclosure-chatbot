"""Contains utilities files for topic detection in automaton.

There are two levels of topic detection.

1. Given input_data, topic_detection seeks to detect keys in techscience_template.yml (response template)
2. Normalize all the topics (a.i. to Artificial Intelligence, tech to Technology)
3. Keeps track of the topic we cannot handle yet.

"""
import re
import json
import random
import logging
import os
import copy
from typing import List, Dict, Optional, Tuple, Any
from nlu.constants import Positivity

from techscience_utils.data_topic import PROPOSAL_MAPPING, BROAD_TO_SPECIFIC, TOPIC_TO_FACT, KEYWORD_MAPPING, RESPONSE_VARIATION, ACTIVATED_CHITCHAT, PRIORITY, TOPIC_TO_MODULES, MODULE_TO_RESPONSE
from response_templates import template_data
from techscience_utils.logging_utils import set_logger_level, get_working_environment

LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())
dir_path = os.path.dirname(os.path.realpath(__file__))
keyword_path = os.path.join(dir_path, "keyword_weights.json")

def positivity_to_dict(positivity:dict)->dict:
    if isinstance(positivity, dict):
        return positivity
    if isinstance(positivity, int):
        if positivity > 0:
            return {"pos": positivity, "neg":0, "neu":0}
        elif positivity < 0:
            return {"pos": 0, "neg": abs(positivity), "neu":0}
        else:
            return {"pos": 0, "neg": 0, "neu":0}
    return {"pos": 0, "neg": 0, "neu":0}

def add_positivity(pos1, pos2):
    res = {}
    for k in pos1:
        if k in pos2:
            v2 = pos2[k]
        else:
            v2 = 0
        res[k] = v2 + pos1[k]
    return res

class TopicHandler:
    """Given returnnlp and central_elem data from the nlu.
       Topic Handler extracts all the relevant topic the user may be interested in.
    """
    FACT_LIMIT = 3

    def _process_topic(self, topic):
        """Extract the topic class, keywords, confidence, and topic_text from NLU element
           For now, we
        """
        # if not isinstance(topic, list):
        #     topic = []

        self.topic_class = []
        self.topic_confidence = []
        self.topic_text = []
        # self.topic_keywords = {k.keyword: k.confidence for t in topic for k in t.topic_keywords}
        # self.topic_words = list(self.topic_keywords.keys())
        self.topic_keywords = {}
        # for t in topic:
        #     for k in t.topic_keywords:
        #         if t.topic_class == "SciTech":
        #             self.topic_keywords[t.text][k.keyword] = k.confidence
        # if len(self.topic_keywords) > 0:
        #     self.curr_topic_keyword = self.topic_keywords[self.topic_text[0]]
        #     self.topic_words = list(self.curr_topic_keyword.keys())
        #     self.topic_words.sort(
        #         key=lambda x: self.curr_topic_keyword[x],
        #         reverse=True)
        # else:
        self.curr_topic_keyword = {}
        self.topic_words = []

    @property
    def default_history(self)->List[Tuple[str, float]]:
        return []

    @property
    def default_context(self)->Dict[str, Dict[str, float]]:
        return {}

    @property
    def default_propose_topic(self)->List[str]:
        return []

    @property
    def empty_topic_keyword(self)->Optional[str]:
        return None

    @property
    def empty_keyword_contexts(self)->Dict[str, float]:
        return {}

    @property
    def default_fact_counter(self)->int:
        return 0


    def process_concept(self, concept):
        self.concept = concept

    def process_nounphrase(self, noun_phrase):
        self.noun_phrase = noun_phrase

    def process_sentiment(self, sentiment):
        self.sentiment = sentiment

    def find_fact_for_topic(self, topic, topic_history):
        """ find fact for topic
        """
        # if system can handle, find a fact that hasn't been read
        if self.is_fact(topic):
            return topic
        return self.get_fact_from_topic(topic, topic_history)

    def _process_nlu_elements(self, returnnlp, central_elem):
        """Retrieve topic, concept, noun_phrase from returnnlp
           and dialog_act from central_elem
        """
        self.central_elem = central_elem
        self.returnnlp = returnnlp
        # self.dialog_act = central_elem.dialog_act
        
        # returnnlp_elements = [
        #     (seg.text,
        #      seg.topic,
        #      seg.concept,
        #      seg.sentiment,
        #      seg.noun_phrase,
        #      seg.googlekg) for seg in returnnlp]
        # text, topic, concept, sentiment, noun_phrase, google_kg = list(
        #     map(list, zip(*returnnlp_elements)))
        self._process_topic(self.returnnlp.topic)
        self.process_concept(self.returnnlp.concept)
        self.text = self.returnnlp.text
        self.process_nounphrase(self.returnnlp.noun_phrase)
        # self.process_google_kg(google_kg)
        self.process_sentiment(self.returnnlp.sentiment)

    def process_google_kg(self, kg):
        self.google_kg = kg

    def process_sentiment(self, sentiment):
        self.sentiment = sentiment


class TechScienceTopicHandler(TopicHandler):
    """Topic is categorized into three levels

       Level 1 Topic:  Broad Concept.  The goal here is the user is interested in techscience.
         - Detect two changes:  Does the user know what he wants to talk about?  If so, follow the user.
         -                      Else, design an engaging topic

       Level 2 Topic:  System Defined Topic.

       Level 3 Topic:  Topic System doesn't have.
         - Interface with News
         - Old Chitchat Interface
         - Old Retrieval Interface.
    """
    TOPIC_CONFIDENCE_THRESHOLD = 0.9
    DEFAULT_CURRENT_TOPIC = "tech"
    module_name = "techscience"
    LENGTH_THRESHOLD = 5
    def __init__(self):
        super(TechScienceTopicHandler, self).__init__()
        self.normalized_map = {}
        self.new_topics = []
        self._process_template()

    def build_topic_handler(self, returnnlp, central_elem):
        super(
            TechScienceTopicHandler,
            self)._process_nlu_elements(
            returnnlp,
            central_elem)
        return self

    def _process_template(self):
        """Process response template's topic response
        - For now, only works for
          - opinion
          - favorite
          - topics
        """
        data = template_data.data["techscience"]
        self.favorite_list = [topic for topic in data["favorite"]]
        self.opinion_list = [topic for topic in data["opinion"]]
        self.topics_list = [topic for topic in data["facts"]]

    def normalize_topic(self, noun_phrase:str, add_to_new:bool=True)->Optional[str]:
        """Normalize Topic takes in one raw nounphrase and map it to topic
           we can handle

        Parameters:
           - noun_phrase:  raw noun_phrase
        Return:
           - normalized topic if can be normalized, none otherwise
        
        Add topic to self.normalized_map:  {normalized_topic: raw_text}
        Add noun_phrase to self.new_topics if not handled
        TODO: Normalize Topic
        """

        # In order to keep consistent: check already normalized topic
        for normalized in self.normalized_map:
            if self.normalized_map[normalized] == noun_phrase:
                return normalized

        extended_keyword = {t: set([t]) for t in TOPIC_TO_FACT if t not in KEYWORD_MAPPING}
        extended_keyword.update(KEYWORD_MAPPING)
        extended_fact = {f: set([f]) for t in TOPIC_TO_FACT for f in TOPIC_TO_FACT[t] if f not in KEYWORD_MAPPING}
        extended_keyword.update(extended_fact)
        for m in extended_keyword:
            if noun_phrase == m:
                return m
            if noun_phrase in extended_keyword[m]:
                self.normalized_map[m] = noun_phrase
                return m
        if noun_phrase not in self.new_topics and add_to_new:
            self.new_topics.append(noun_phrase)
            return None


    def _load_weights(self):
        with open(keyword_path, "r") as r:
            self.weights = json.loads(r.read())

    def post_process_responses(self, topic):
        """Post processing some templates.  For now fix the problem with various a.i.
           Adding more diversity to the responses

           Parameters:
             - topic
           Return:
             - varied responses
        """
        if topic in RESPONSE_VARIATION:
            return random.choice(list(RESPONSE_VARIATION[topic]))

        for k in RESPONSE_VARIATION:
            if topic in RESPONSE_VARIATION[k]:
                return random.choice(list(RESPONSE_VARIATION[k]))
        if topic is not None:
            return ''.join([i if ord(i) < 128 else ' ' for i in topic])
        return ""


    def get_highest_topic(self, detected_topics):
        """Pick the topic with the highest weights

        Parameters:
          - detected_topics:  results from self.detect_topic
        Return:
          - topic with highest weight
        """
        return max(list(detected_topics), key=lambda x: detected_topics[x])


    def detect_topic(self, history=-1):
        """Detects the topic of a user utterance.

        Detect topic is completely independent from previous utterance.

        Returns:
            proposed_current_topics:  A dictionary of proposed topic to discuss
        """
        # context_topics = []  # extracted context topic
        proposed_noun_phrase = self.get_topics_from_nounphrase(history)
        proposed_concept = self.get_topics_from_conceptnet(history)
        proposed_raw_text = self.get_topics_from_raw_text()
        def add_to_d(d, k):
            if k in d:
                d[k] += 1
            else:
                d[k] = 1
        good_phrase = lambda x: x != "" and "<UNK>" not in x
        # Construct the proposed current topics
        proposed_current_topics = {}
        for phrase in proposed_noun_phrase:
            if not good_phrase(phrase):
                continue
            normalized = self.normalize_topic(phrase)
            if normalized is not None:
                add_to_d(proposed_current_topics, normalized)

        for phrase in proposed_concept:
            if not good_phrase(phrase):
                continue
            normalized = self.normalize_topic(phrase)
            if normalized is not None:
                add_to_d(proposed_current_topics, normalized)

        for phrase in proposed_raw_text:
            if not good_phrase(phrase):
                continue
            normalized = self.normalize_topic(phrase, False)
            if normalized is not None:
                add_to_d(proposed_current_topics, normalized)

        return proposed_current_topics

    def get_topics_from_raw_text(self, history=0):
        """Detect topic from raw text

        Parameter:
          history: how long to look at the history user utterance.  
                   0 to look at the only the current utterance
        Return:
          proposed: a list of topics detected.
        """        
        proposed = []
        if len(self.text) > 0:
            if history == -1:
                history = len(self.text)
            sentence = self.text[:history + 1]
            for text in sentence:
                for t in text.split(" "):
                    proposed.append(t)
        return proposed

    def get_topics_from_conceptnet(self, history=0):
        """Detect topic from conceptnet

        Parameter:
          history: how long to look at the history user utterance.  
                   0 to look at the only the current utterance
        Return:
          proposed: a list of topics detected.
        """
        proposed = []
        if len(self.concept) > 0:
            if history == -1:
                history = len(self.noun_phrase)
            concepts = self.concept[:history + 1]
            for concept_lst in concepts:
                for c in concept_lst:
                    topic = c.noun
                    proposed.append(topic)
        return proposed

    def get_topics_from_nounphrase(self, history=0):
        """Detect topic with nounphrase.
           
        Parameter:
          history: how long to look at the history user utterance.  
                   0 to look at the only the current utterance
        Return:
          proposed: a list of nounphrases detected.
        """
        proposed = []
        if history == -1:
            history = len(self.noun_phrase)
        if len(self.noun_phrase) > 0:
            nounphrase = self.noun_phrase[:history + 1]
            for current_nounphrase in nounphrase:
                for np in current_nounphrase:
                    proposed.append(np)
        return proposed
                                
    def _map_keyword_to_selector_no_chitchat(self, keyword, header):
        """Map topic to corresponding selector, not chitchat.

        Parameter:
            keyword - normalized keyword
            header - fact, opinion, favorite
        Return:
            selector for the corresponding keyword
        """
        # Map the topic to opinion or fact if must
        if header == "opinion":
            l = self.opinion_list
        elif header == "favorite":
            l = self.favorite_list
        elif header == "facts":
            l = self.topics_list
        else:
            raise NameError("Header not found {0}", header)
        if keyword not in l:
            raise NameError("Keyword {0} not found in {1}", keyword, header)
        else:
            if header == "facts":
                return "{0}/{1}/{2}".format(header, keyword, "facts")
            return "{0}/{1}".format(header, keyword)

    def _map_keyword_to_selector_chitchat(self, keyword, header, ids):
        """Map topic to corresponding selector, not chitchat.

        Parameter:
            keyword - normalized keyword
            header - fact, opinion, trendy
        Return:
            selector for the corresponding keyword
        """
        if header == "opinion":
            # Map the topic to opinion or fact if must
            starter =  "{0}/{1}/f{2}".format(header, keyword, ids)
            followup = "{0}/{1}/q{2}".format(header, keyword, ids)        
        else:
            # Map the topic to opinion or fact if must
            starter =  "{0}/{1}/{2}/f{3}".format(header, keyword, "chitchat_facts", ids)
            followup = "{0}/{1}/{2}/q{3}".format(header, keyword, "chitchat_facts", ids)
        return (starter, followup)

    def map_keyword_to_selector(self, keyword, header, chitchat=False, ids=None):
        """Map topic to corresponding selector, not chitchat.

        Parameter:
            keyword - normalized keyword
            header - fact, opinion, topic
        Return:
            selector for the corresponding keyword
        """
        if header == "opinion":
            chitchat = True
        if chitchat:
            return self._map_keyword_to_selector_chitchat(keyword, header, ids)
        return self._map_keyword_to_selector_no_chitchat(keyword, header)


    def is_broad_topic(self, keyword:str)->bool:
        """Returns true if keyword is a broad topic
        """
        return keyword in BROAD_TO_SPECIFIC

    def is_topic(self, keyword:str)->bool:
        """Returns true if keyword is a regular topic
        """ 
        return keyword in TOPIC_TO_FACT

    def is_fact(self, keyword:str)->bool:
        """Returns true if keyword is a fact topic
        """
        return keyword in [i for t in TOPIC_TO_FACT for i in TOPIC_TO_FACT[t]]

    def is_chitchat_topic(self, header, keyword, ids=None):
        """Return True if the topic can be handled by chitchat.
           Registered in Chitchat Activated.

           Parameters:
             - Header:  choose between 'trend', 'opinion', and 'facts'
             - Keyword:  the keyword detected
             - ids: specific chitchat id, if None, check if topic can be handled in general

           Return:
             - True if it is a chitchat topic False otherwise
        """
        activated = ACTIVATED_CHITCHAT[header]
        if keyword not in activated:
            return False
        if ids is None:
            return len(activated[keyword]) > 0
        return ids in activated[keyword]

    def map_fact_to_topic(self, fact, header):
        """Given a fact, map to corresponding topic

           Parameters:
             - fact:  fact keyword
             - header: opinion, fact, favorite

           Return:
             - the topic fact is under.

        """
        if fact in TOPIC_TO_FACT:
            return fact
        for t in TOPIC_TO_FACT:
            if fact in TOPIC_TO_FACT[t]:
                return t
        raise NameError("Fact Topic not normalized {}".format(fact))

    def map_to_broad_topic(self, topic):
        """Given a topic detected, map to a broader topic
           The goal of this mapping is to lead a conversation.

        Args:
            topic:  current_topic generated from detect_topic

        Return:
            broad_topic:  broad topic to discuss about
        """
        if topic in BROAD_TO_SPECIFIC:
            return topic
        for broad in BROAD_TO_SPECIFIC:
            for t in BROAD_TO_SPECIFIC[broad]:
                if topic == t:
                    return broad
                if topic in TOPIC_TO_FACT[t]:
                    return broad
        raise NameError("Topic not categorized {}", topic)


    def get_common_topics(self, propose_topics:Dict[str, Positivity],
                          detected_topics, context_topic, topic_history, fact_only=True)->str:
        """Get the common topics from propose_topics, detected_topics, topic_history

        Parameters:
          propose_topics: A list of topics proposed by our system
          detected_topics:  A dictionary of topics detected from user
          topic_history:  dictionary of topic history
        Return:
          proposing a topic while appending to topic_history
        """
        # Highest Priority;  We proposed the topic and we detected user mention
        # the topic
        common_topics = set(propose_topics).intersection(set(detected_topics.keys()))
        if len(common_topics) > 0:
            propose = list(common_topics)[0]
            # topic_history[propose] = 1
            return propose

        # Second Priority:  We proposed the topic, but user proposed another topic, we go with users
        # Later we should build the connection between the user's topic and our
        # topic with google kg
        if len(detected_topics) > 0:
            highest = self.get_highest_topic(detected_topics)
            if not fact_only or self.is_fact(highest):
                # topic_history[highest] = 1
                return highest
        
        proposed = self.propose_topics_to_list({p: {"pos":0, "neg":0, "neu":0} for p in propose_topics}, limit=1)
        if len(proposed) > 0:
            proposed_topic = proposed[0]
            if not fact_only or self.is_fact(proposed_topic):
                # topic_history[proposed_topic] = 0
                return proposed_topic
        return None

    @property
    def contains_pos_sent(self):
        """Return True when positive sentiment is overwhelming negative sentiment
        """
        score = 0
        for sentiment in self.sentiment:
            score += float(sentiment.pos)
            score -= abs(float(sentiment.neg))
        return score > 0.25

    @property
    def contains_neg_sent(self):
        """Return True when positive sentiment is overwhelming negative sentiment
        """
        score = 0
        for sentiment in self.sentiment:
            score -= abs(float(sentiment.pos))
            score += abs(float(sentiment.neg))
        return score > 0.5
    # def suggest_from_broad(self, broad_topic, not_topics):
    #     if broad_topic in BROAD_TO_SPECIFIC:
    #         topic = BROAD_TO_SPECIFIC[broad_topic]
    #     else:
    #         topic = TOPIC_TO_FACT.keys()
    #     potential_topics = set()
    #     # expand topic to fact topics
    #     for t in topic:
    #         potential_topics = potential_topics.union(set(TOPIC_TO_FACT[t]))

    #     potential_topics = list(potential_topics - set(not_topics))
        
    #     return potential_topics

    def propose_topic_from_keyword(self, keyword:str, broad_topic:Optional[str]=None, \
                                 not_topics:List[str]=[], context_topic:Dict[str, Dict[str, dict]]={}, \
                                 topic_history:List[Tuple[str, float]]=[], curr_topic:Optional[str]=None, \
                                 topic_only:bool=True)->str:
        
        result_proposal = self.propose_new_topics(broad_topic, not_topics, context_topic, topic_history)

        # For every topic in context topic, if keyword is part of context topic, then propose that topic before others
        for topic in context_topic:
            for k in context_topic[topic]:
                if k == keyword and context_topic[topic][k]["pos"] > context_topic[topic][k]["neg"]:
                    return topic
        proposed_topics = self.propose_topics_to_list(result_proposal)
        if len(proposed_topics) > 0:
            return proposed_topics[0]
        return "artificial_intelligence"

    def propose_new_topics(self, broad_topic:Optional[str], \
                                 not_topics:List[str], context_topic:Dict[str, Dict[str, dict]], \
                                 topic_history:List[Tuple[str, float]], curr_topic:Optional[str]=None, \
                                 topic_only:bool=True)->Dict[str, float]:
        """Propose new topics not the same as not_topics

        Parameters:
          - broad_topic:  if not none, suggest from broad_topic
          - not_topics:  list of topics not to cover
          - context_topics:  list of topic keeping track
          - topic_history; dictionary of topics already talked about
          - topic_only:  if true, only propose topics instead of fact topics

        Returns:
          - a dictionary of new topics
        """
        LOGGER.debug("Proposing New Topics broad_topic:{0}\n \
                      not_topics: {1}\n \
                      context_topic: {2}\n\
                      topic_history: {3} \n \
                      curr_topic: {4} \n \
                      topic_only: {5} \n".format(broad_topic, not_topics, context_topic, topic_history, curr_topic, topic_only))
        
        if broad_topic in BROAD_TO_SPECIFIC:
            potential_topics = BROAD_TO_SPECIFIC[broad_topic]
        else:
            potential_topics = TOPIC_TO_FACT.keys()

        LOGGER.debug("[TOPIC UTILS] Potential Topics {}".format(potential_topics))
        _topic_history_items = [topic_info[0] for topic_info in topic_history]
        if not topic_only:
            potential_topics = [f  for t in potential_topics for f in TOPIC_TO_FACT[t]]
        propose_topics = {}
        for t in potential_topics:
            if t in not_topics:
                continue
            elif t in _topic_history_items:
                continue
            else:
                propose_topics[t] = 0
        LOGGER.debug("[TOPIC UTILS] Raw Propose Topics {}".format(propose_topics))

        if curr_topic is not None and curr_topic in context_topic:
            context_occurrence = context_topic[curr_topic]
        else:
            context_occurrence = {case: {"pos":0, "neg":0, "neu":0} for t in context_topic for case in context_topic[t]}
            for key in context_topic:
                c = context_topic[key]
                for i in c:
                    context_occurrence[i] = add_positivity(c[i], context_occurrence[i])
        # Add weight to the topics
        for p in propose_topics:
            if p in context_occurrence and p in propose_topics:
                positivity = context_occurrence[p]
                propose_topics[p] = positivity["pos"] - positivity["neg"] + propose_topics[p]
        LOGGER.info("Proposed Topic {}".format(propose_topics))
        # if len(propose_topics) == 0:
        #     return {"artificial_intelligence":1} #PATCH: FIX LATER
        return propose_topics

    def propose_topics_to_list(self, propose_topics:Dict[str, float], limit=1)->List[str]:
        """Given a list of propose topics, get the limited number of topics.
        """
        if len(propose_topics) <= limit:
            return list(propose_topics.keys())
        sorted_topics = {}
        for k in propose_topics:
            s = propose_topics[k]
            if s not in sorted_topics:
                sorted_topics[s] = set()
            sorted_topics[s].add(k)
        _keys = list(sorted_topics.keys())
        _keys.sort(reverse=True)
        output = []
        
        for score in _keys:
            sets = sorted_topics[score]
            for t in PRIORITY:
                if t in sets:
                    output.append(t)
                if len(output) >= limit:
                    return output
            for s in sets:
                if s not in output:
                    output.append(s)
                if len(output) >= limit:
                    return output
        return output
        
        

    def get_fact_from_topic(self, curr_topic, topic_history)->Optional[str]:
        if self.is_broad_topic(curr_topic):
            for topic in BROAD_TO_SPECIFIC[curr_topic]:
                if topic not in topic_history:
                    curr_topic = topic
                    break
                if topic_history[fact_topic] > 0:
                    curr_topic = topic
                    break
        if self.is_topic(curr_topic):
            
            for fact_topic in TOPIC_TO_FACT[curr_topic]:
                if fact_topic not in topic_history:
                    return fact_topic
                if topic_history[fact_topic] > 0:
                    return fact_topic
        else:
            LOGGER.warning("curr_topic is not from the system.  Resort to no topic instead")
            return None
        
    def save_new_topics(self, new_topics):
        new_topics.extend(self.new_topics)

    def construct_context_topics(self, current_topic:str, detected_topics:Dict[str, dict], current_context:Dict[str, Dict[str, dict]]):
        """Returns a reconstructed context topic 
           
           Parameter:
             - current_topic:  Currently talking about this topic.  Note this topic is either a fact topic or topic.
               We use this to track information.
             - detected_topics:  A dictionary of topics detected.  
             - current_context:  Current context topics
           Return:
             - changed context topic dictionary
           TODO: Test
        """

        if current_topic not in current_context:

            current_context[current_topic] = {key: positivity_to_dict(detected_topics[key]) for key in detected_topics}
        else:

            current_topic_context = current_context[current_topic]
            for k in detected_topics:
                d = positivity_to_dict(detected_topics[k])
                if k in current_topic_context:
                    current_topic_context[k] = add_positivity(current_topic_context[k], d)
                else:
                    current_topic_context[k] = positivity_to_dict(detected_topics[k])
        return copy.deepcopy(current_context)

    def construct_topic_history(self, new_topic:Optional[str], topic_history:List[Tuple[str, float]], topic_and_sentiment:Dict[str, float]):
        """Construct or update topic_history

          Parameter:
          - topic_history: (1 if user like, 0 if no opinion, -1 if user dislike)
          - topic_and_sentiment: Dictionary of Topic and corresponding sentiment
        """
        LOGGER.debug("Construct Topic History new_topic:{0}\n \
                      topic_history: {1}\n \
                      topic_and_sentiment: {2}"\
                      .format(new_topic, topic_history, topic_and_sentiment))

        new_topic_history = []
        un_updated_topic_history = []
        for topic, sentiment in topic_history:
            if topic == new_topic and new_topic is not None:
                new_topic_history.append((new_topic, 0))
            elif topic in topic_and_sentiment:
                new_sentiment = sentiment + topic_and_sentiment[topic]
                new_topic_history.append((topic, new_sentiment))
            else:
                un_updated_topic_history.append((topic, sentiment))

        LOGGER.debug("Topic and Sentiment {}".format(topic_and_sentiment))
        for topic in topic_and_sentiment:
            new_topic_history.append((topic, topic_and_sentiment[topic]))
        new_topic_history.extend(un_updated_topic_history)
            
        return new_topic_history


    def dialog_act_contains(self, dialog_acts, threshold=0.4):
        """Return True if dialog_act contains one of the element from list

           Parameter:
           - dialog_acts: a list of dialog acts 
        """
        return self.returnnlp.has_dialog_act(dialog_acts)


    def keyword_detection(self, system_only=False):
        """Given a user utterance, (processed from returnnlp). Detect Keywords under certain category.

        Supporting:
          - IOS, Apple
          - Android
        """
        keywords = self.keyword_detection(KEYWORD_MAPPING)
        if system_only:
            return keywords

        new_words = self.new_keyword_detection()
        return list(set(keywords).union(new_topics))

    def keyword_detection(self, keyword_mapping):
        """
        """
        proposed_nounphrase = self.get_topics_from_nounphrase()
        result = []
        r = self.create_regex_for_predefined_keywords(keyword_mapping)
        for np in proposed_nounphrase:
            noun_phrase = self.normalize_topic(np, KEYWORD_MAPPING)
            if noun_phrase is not None and re.search(r, noun_phrase):
                result.append(noun_phrase)
        return result

    def find_trendy_topic(self, current_topic, context_topic, topic_history):
        return

    def create_regex_for_predefined_keywords(self, keyword_mapping):
        regex = ["\\b{0}\\b".format(v) for v in keyword_mapping]        
        TOPIC_REGEX = r"|".join(regex)
        return TOPIC_REGEX

    # def upgraded_topic_filter(self, t):
    #     relevant_weights = [d for d in self.weights["keyword_detector"] if d["source"] == "topic" and d["description"] == t.topic_class and d["confidence"] > t.confidence]
    #     return sum([r["weight"] for r in relevant_weights]) > 0

    def upgraded_concept_net_filter(self, t):
        relevant_weights = [d for d in self.weights["keyword_detector"] if d["source"] == "concept_net" and d["description"] == t.description and d["confidence"] > t.confidence]
        return sum([r["weight"] for r in relevant_weights]) > 0

    def propose_on_default(self, last_topic):
        default_proposal = ["artificial_intelligence"]
        return PROPOSAL_MAPPING.get(last_topic, default_proposal)[0]

    def new_keyword_detection(self):
        result = []
        # processed_topic = [topic for topic in self.topic if self.upgraded_topic_filter(topic)]
        process_concepted = [l.noun for c in self.concept for l in c for d in l.data if self.upgraded_concept_net_filter(d)]
        # processed_topic_keywords = [keyword.keyword for t in processed_topic for keyword in t.topic_keywords]
        # result.extend(processed_topic_keywords)

        result.extend(process_concepted)
        return result

    def detect_sentiment(self, sentiment_tracker):
        """Update the sentiment by corresponding keys
        """
        for seg in self.sentiment:
            sentiment_tracker["pos"].append(abs(float(seg.pos)))
            sentiment_tracker["neg"].append(abs(float(seg.neg)))
            sentiment_tracker["neu"].append(abs(float(seg.neu)))

    def extract_nounphrase_from_request(self):
        # potential_topics = ["tell me about", "tell me more about", ""]
        nounphrase = self.text[0].split("about")[-1]
        return nounphrase

    def module_to_response(self, module:str):
        for key in TOPIC_TO_MODULES:
            if TOPIC_TO_MODULES[key] == module:
                return MODULE_TO_RESPONSE.get(key)
        return None
