_req_topic = ["((can\\b|will\\b).*(talk|tell|discuss|chat).* about|talk|favorite)", "(what about)", "(tell me about)", "(i want to know more about)"]
R_REQ_TOPIC = r"|".join(_req_topic)

_req_research = ["((^who\\b|^what\\b).* (enhancement|progress|research|development|advancement) .*)"]
R_REQ_RESEARCH = r"|".join(_req_research)

_req_scitech = ["(^will\\b|^can\\b|.*(talk|chat|discuss|tell).*(science|technology|artificial intelligence|a.i.|robot))"]
R_REQ_SCITECH = r"|".join(_req_scitech)

# Regex for Lexical Level Intents
_ask_recommend = ["(\\bgive.* (\\brecommendation\\b|\\bsuggestion\\b))",\
                  "(\\byou\\b (\\bsuggest\\b|\\brecommend\\b).* \\bme\\b .*)",\
                  "(\\byou\\b (\\bsuggest\\b|\\brecommend\\b))", \
                  "((^what|\\btell\\b \\bme\\b).* (\\bgood\\b|\\bpopular\\b).* )",\
                  "((\\bdo\\b \\byou\\b|\\bcan\\b \\bi\\b) \\bhave\\b suggestions)",\
                  "(\\bwhat\\b|\\bwho\\b|\\bwhen\\b|\\bwhy\\b) .* should \\bi\\b"
                  ]
R_ASK_RECOMMEND = r"|".join(_ask_recommend)

_ask_question = ["(^who\\b|^what\\b|^when\\b|^when\\b|^where\\b|^why\\b|^how\\b)"]
R_ASK_QUESTION = r"|".join(_ask_question)


# Regex for System Level Intents
_exit_techscience = ["(.*(stop|don't|do not) want to (talk|discuss|tell.* me).*)", \
                     "(\\bi\\b (don't|do not) like (technology|techscience|tech))",\
                     "(.* we (should talk|talk) about something else)"]
R_EXIT_TECHSCIENCE = r"|".join(_exit_techscience)
# R_EXIT_TECHSCIENCE = r"(.*(stop|don't|do not).*(talk|discuss|tell.* me).*)|(\bi\b.*(hate|don't|do not|never).*)|((talk|discuss|tell.* me).* about.* something else)|((stop|quit).*(talk|tell|chat|inform).*(about|on).*(science|technology|tech).*)|(.* (hate|not interested|dislike)).*(science|this|technology|tech)"


_answer_yes = ["\\byes\\b", "\\bya\\b", ".*(^(?!.*that's).*okay.*)", "\\bcool\\b", "\\binteresting\\b", "\\bcontinue\\b", "\\babsolutely\\b",\
              "\\bok\\b", "\\byeah\\b", ".*(^(?!.*not).*sure.*)", ".*sounds good to me.*", "alrighty", ".*(^(?!.*not).*great.*)",]

_answer_no =["\\bno\\b", "\\bnope\\b", "\\bnone\\b", "\\bnothing\\b", "\\bnot\\b", "\\bdunno\\b", "\\bnope\\b", "stop", ".*(don't|do not|now).*(know|like|have|enjoy|want)"]

_ask_more = ["tell me more", "more please", "wanna hear more", "listen to more", "i can try"]

_pos_sentiment = [".*(^(?!.*not).*hilarious.*)", ".*(^(?!.*not).*funny.*)", "(\\bhaha\\b)", "(^(?!.*don).* (like|love).*)", "((\\bgood\\b|\\bfine\\b) job)"]
# _pos_sentiment = _answer_yes
_user_dont_know = ["don\'t know", "am not sure"]
# _neg_sentiment = [".*(^(?!.*not).*hilarious.*)", ".*(^(?!.*not).*funny.*)", "(\\bhaha\\b)", "(^(?!.*don).* (like|love).*)"]

_user_claimed_wrong = ["that's (incorrect|wrong|false|not true)", "that is (incorrect|wrong|false|not true)"]

_agree = ["i agree"]
R_ANSWER_YES = r"|".join(_answer_yes)
R_ANSWER_NO = r"|".join(_answer_no)
R_ASK_MORE = r"|".join(_ask_more)
R_POS_SENT = r"|".join(_pos_sentiment)
R_USER_DONT_KNOW =  r"|".join(_user_dont_know)
R_USER_CLAIMED_WRONG =  r"|".join(_user_claimed_wrong)

R_USER_AGREE = r"|".join(_agree)
