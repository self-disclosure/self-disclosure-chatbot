import re
import json
from cobot_common.service_client import get_client
import praw
import time
import redis
import random
import logging
from techscience_utils.data_topic import BLACKLIST
import boto3
import hashlib
from boto3.dynamodb.conditions import Key, Attr

from itertools import chain

# TODO: Link with decoupling
COBOT_API_KEY = 'EwecQ1fkEd4YMVUOHHpeSam7zbef53gipQsxAUki'
REDDIT_CLIENT_ID = "lKmLeMmSSLns6A"
REDDIT_CLIENT_SECRET = "Fi6V_pCzKqE-4TRVqcmpMJdET2s"
REDIS_DYNAMO_KEY = "gunrock:remote:techscience:dynamo"

# TODO: Dynamo Table
dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
table = dynamodb.Table('SciTech')

rds = redis.StrictRedis(host='52.87.136.90', port=16517,
                        socket_timeout=3,
                        socket_connect_timeout=1,
                        retry_on_timeout=True,
                        db=0, password="alexaprize", decode_responses=True)


def evi_bot(utterance):
    try:
        client = get_client(api_key=COBOT_API_KEY)
        r = client.get_answer(question=utterance, timeout_in_millis=1000)
        if r["response"] == "" or re.search(r"skill://", r["response"]):
            return None
        return r["response"] + " "
    except Exception as e:
        logging.info("[TECHSCIENCECHAT_MODULE] Timeout in EVI_bot {}".format(e))
        return None

def retrieve_reddit(utterance, subreddits, limit):
    # currently using gt's client info
    reddit = praw.Reddit(client_id=REDDIT_CLIENT_ID,
                         client_secret=REDDIT_CLIENT_SECRET,
                         user_agent='extractor',
                         timeout=2
                         #username='gunrock_cobot', # not necessary for read-only
                         #password='ucdavis123'
                        )
    subreddits = '+'.join(subreddits)
    subreddit = reddit.subreddit(subreddits)
    submissions = list(subreddit.search(utterance, limit=limit)) # higher limit is slower; delete limit arg for top 100 results
    if len(submissions)>0:
        return submissions
    return []


def get_interesting_reddit_posts(text, limit):
    submissions = retrieve_reddit(text, ['todayilearned'], limit)
    if submissions != None:
        results = [submissions[i].title.replace("TIL", "").lstrip() for i in range(len(submissions))]
        results = [results[i] + '.' if results[i][-1]!='.' and results[i][-1]!='!' and results[i][-1]!='?' else results[i] for i in range(len(results))]
        #Sensitive Words Pattern
        final_results = []
        for result in results:
            if not any(re.search(x, result) for x in BLACKLIST):
                final_results.append(result)
        if len(final_results) > 0:
            return final_results
    return []

######################################################################
############################ Reddit Retrieval ########################
######################################################################

def topic_mapper(topic):
    mappings = {"artificial intelligence": "ai",
                "a.i.": "ai",
                "a. i.": "ai",
                "robot": "robotics",
                "robots": "robotics",
                "planets": "space",
                "cosmology": "space",
                "planet": "space",
                "black hole": "space",
                "black holes": "space",
                "bitcoins": "bitcoin",
                "medicines": "medicine",
                "computer": "computer science",
                "computers": "computer science",
                "nano tech": "nanotech",
                "internet": "software",
                "nano technology": "nanotech"}
    if topic not in mappings:
        return topic
    if topic in mappings:
        return mappings[topic]
    return topic


def query_tag(tag):
    """
    Returns table data for database/graph ID. Dynamo partitioned on graph
    id
    :param id: utf8 hash
    :return: data for hash
    """
    tag = topic_mapper(tag)
    try:
        response = table.query(
            IndexName='tag',
            KeyConditionExpression=Key('tag').eq(tag),
            ScanIndexForward=False,
        )
        if response:
            response = response['Items']
    except:
        response = None
    final_response = QA(response)
    return final_response


def query_keywords(word_list: list, threadDict: dict = {}, full=False) -> list:
        # full = True
        SCANLIMIT = 20
        start = time.time()
        # word_list = [word for line in word_list for word in line.split()]
        word_list = list(set(chain(*map(str.split, word_list))))
        fe = Attr('keywords').contains(word_list[0].lower())
        for word in word_list[1:]:
            fe = fe | Attr('keywords').contains(word.lower())
        response = table.scan(
            FilterExpression=fe,
        )
        resp = response['Items']
        i = 1
        while 'LastEvaluatedKey' in response:
            response = table.scan(ExclusiveStartKey=response['LastEvaluatedKey'], FilterExpression=fe)
            resp.extend(response['Items'])
            if i == SCANLIMIT:
                break
            i += 1
        scoredict = {}
        for i in range(1, len(word_list) + 1):
            scoredict[i] = []
        for r in resp:
            score = 0
            scoredict[score] = []
            for w in word_list:
                if w in r['keywords']:
                    score += 1
            scoredict[score].append(r)
        final = []
        for s in sorted(scoredict.keys(), reverse=True):
            scorelist = scoredict[s]
            scorelist = sorted(scorelist, key=itemgetter('modified'), reverse=True)
            final.extend(scorelist)
        final2 = []
        for each in final:
            if each['toLabel'] == 'PERSON' and each['fromLabel'] == 'PERSON':
                final2.append(each)
        if not final2:
            final2 = final
        final2 = QA(final2)
        stop = time.time()
        threadDict['keywords'] = final2
        return final2


def QA(response):
    SUBLIMITTITLE = 40
    SENTENCEMAX = 300
    SENTENCEMIN = 30
    l = []
    for each in response:
        try:
            revisedsents = []
            sents = each['title']
            s, n = re.subn(r'[^\w\s\.\']', '', sents)
            if n<SUBLIMITTITLE and len(s) < SENTENCEMAX and len(s) > SENTENCEMIN:
                revisedsents.append(s)
            else:
                continue #optional could stop and not put in the body
            each['title'] = revisedsents[0]
        except KeyError:
            pass
        l.append(each)
    return l

# Refactored
def retrieve_kg_topic(response):
    final_list = list()
    count = 0
    if not response:
        return ""
    for resp in response:
        if not any(re.search(x, resp["title"]) for x in BLACKLIST):
            if "title" in resp:
                if resp["title"][len(resp["title"]) - 1] != '.':
                    period_title = resp["title"] + '.'
                    final_list.append(period_title)
                else:
                    final_list.append(resp["title"])
    return_resp = str(random.choice(final_list)).replace(" mit ", " M.I.T ")
    while get_redis(REDIS_DYNAMO_KEY, return_resp) is not None and count < 150:
        return_resp = str(random.choice(final_list))
        count += 1
    set_redis_with_expire(REDIS_DYNAMO_KEY, return_resp, return_resp, expire = 3 * 30 * 86400)
    return return_resp 

def set_redis_with_expire(prefix, input_str, output, expire=24 * 60 * 60):
    logging.info(
        '[REDIS] set redis with expire input: {} output: {}, expire {}'.format(
            input_str, output, expire))
    hinput = hashlib.md5(input_str.encode()).hexdigest()
    return rds.set(prefix + ':' + hinput, json.dumps(output),
                   nx=True, ex=expire)


def get_redis(prefix, input_str):
    logging.info('[REDIS] get redis  input: {}'.format(input_str))
    hinput = hashlib.md5(input_str.encode()).hexdigest()
    results = rds.get(prefix + ':' + hinput)
    if results is None:
        return None
    return json.loads(results)