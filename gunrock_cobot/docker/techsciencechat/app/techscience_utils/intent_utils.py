"""Contains utilities files for intent detection and handling in automaton.

"""
import itertools
import re
from techscience_utils.data_topic import TOPIC_LIST
from techscience_utils.misc_utils import detect_question
from techscience_utils.error_utils import NotImplementedError
from nlu.intentmap_scheme import sys_re_patterns, topic_re_patterns, lexical_re_patterns

# [SYSTEM RESPONSE]
S_REGEX_FAVORITE = "One of my favorite topics is blockchains due to its ability to decentralize systems and increase trustworthiness."
S_REGEX_TECH = "I have always been inclined towards knowing more about artificial intelligence."
S_REGEX_SCIENTIST = "Although numerous scientists have helped us through their research, Nikola Tesla will always be my favorite."
S_DEFAULT = "I would like to reserve my opinion on this for now. However, we could talk about something else. " \
           "What do you wish to talk about? "

S_EXIT_TECHSCIENCE = "Ok."
S_START_KG = "Google Assistant was recently able to make phone calls and book appointments for people. You should check that out. "

# Regex for Custom Intents
_req_topic = ["((can\\b|will\\b).*(talk|tell|discuss|chat).* about|favorite)"]
R_REQ_TOPIC = r"|".join(_req_topic)

_req_research = ["((^who\\b|^what\\b).* (enhancement|progress|research|development|advancement) .*)"]
R_REQ_RESEARCH = r"|".join(_req_research)

_req_scitech = ["(^will\\b|^can\\b| (talk|chat|discuss|tell) .* about .* (science|technology|artificial intelligence|a.i.|robot))", "(tell me about |tell me more )"]
R_REQ_SCITECH = r"|".join(_req_scitech)

# Regex for Lexical Level Intents
_ask_recommend = ["(\\bgive.* (\\brecommendation\\b|\\bsuggestion\\b))",\
                  "(\\byou\\b (\\bsuggest\\b|\\brecommend\\b).* \\bme\\b .*)",\
                  "(\\byou\\b (\\bsuggest\\b|\\brecommend\\b))", \
                  "((^what|\\btell\\b \\bme\\b).* (\\bgood\\b|\\bpopular\\b).* )",\
                  "((\\bdo\\b \\byou\\b|\\bcan\\b \\bi\\b) \\bhave\\b suggestions)",\
                  "(\\bwhat\\b|\\bwho\\b|\\bwhen\\b|\\bwhy\\b) .* should \\bi\\b"
                  ]
R_ASK_RECOMMEND = r"|".join(_ask_recommend)

_ask_question = ["(^who\\b|^what\\b|^when\\b|^when\\b|^where\\b|^why\\b|how * (is|was|are|) the \\b)"]
R_ASK_QUESTION = r"|".join(_ask_question)

R_ASK_OPINION = r"(who\b|what\b|when\b|where\b|why\b|which\b|how\b|can|know|curious|interest|hear|tell|give|let|do).* (you|your).* (think|feel|like|love|thought|support|hate|believe|opinion|stance|view|say|favorite)"

# Regex for System Level Intents
_exit_techscience = ["(.*(stop|don't|do not) want to (talk|discuss|tell.* me).*)", \
                     "(\\bi\\b (don't|do not) like (technology|techscience|tech))",\
                     "(.* we (should talk|talk) about something else)"]
R_EXIT_TECHSCIENCE = r"|".join(_exit_techscience)
# R_EXIT_TECHSCIENCE = r"(.*(stop|don't|do not).*(talk|discuss|tell.* me).*)|(\bi\b.*(hate|don't|do not|never).*)|((talk|discuss|tell.* me).* about.* something else)|((stop|quit).*(talk|tell|chat|inform).*(about|on).*(science|technology|tech).*)|(.* (hate|not interested|dislike)).*(science|this|technology|tech)"
_express_interest = ["i am interested", "very interested"]
R_EXPRESS_INTEREST = r"|".join(_express_interest)

_answer_yes = ["\\byes\\b", "\\bya\\b", ".*(^(?!.*that's).*okay.*)", "\\bcool\\b", "\\binteresting\\b", "\\bcontinue\\b", "\\babsolutely\\b",\
              "\\bok\\b", "\\byeah\\b", ".*(^(?!.*not).*sure.*)", ".*sounds good to me.*", "alrighty", ".*(^(?!.*not).*great.*)",]

_answer_no =["\\bno\\b", "\\bnope\\b", "\\bnone\\b", "\\bnothing\\b", "\\bnot\\b", "\\bdunno\\b", "\\bnope\\b", "stop", ".*(don't|do not|now).*(know|like|have|enjoy|want)"]

_ask_more = ["tell me more", "more please", "wanna hear more", "keep talking", "listen to more"]

_pos_sentiment = [".*(^(?!.*not).*hilarious.*)", ".*(^(?!.*not).*funny.*)", "(\\bhaha\\b)", "(^(?!.*don).* (like|love).*)"]
# _pos_sentiment = _answer_yes
_user_dont_know = ["don\'t know", "am not sure"]
# _neg_sentiment = [".*(^(?!.*not).*hilarious.*)", ".*(^(?!.*not).*funny.*)", "(\\bhaha\\b)", "(^(?!.*don).* (like|love).*)"]

_user_like = ["do (like|love|enjoy|prefer)", "i (like|love|enjoy|prefer)"]

_user_expressed_happiness = ["(makes|make) me happy", "i am happy"]

_user_agree = ["i do too", "i agree"]

_ask_leave = ["what else"]

_user_want_teach = ["teach you"]

R_ASK_LEAVE = r"|".join(_ask_leave)

R_ANSWER_YES = r"|".join(_answer_yes)
R_ANSWER_NO = r"|".join(_answer_no)
R_ASK_MORE = r"|".join(_ask_more)
R_POS_SENT = r"|".join(_pos_sentiment)
R_USER_EXPRESSED_HAPPINESS = r"|".join(_user_expressed_happiness)
R_USER_AGREE = r"|".join(_user_agree)
R_USER_TEACH = r"|".join(_user_want_teach)


question_detection = [
    "(who\b|what\b|when\b|where\b|why\b|how\b|which\b|^do\b|^does\b|^did\b|^was\b|^is\b|^are\b|^were\b|^will\b|^would\b)\s*(was|is|were|are|will|would|did|do|does|can|could|you|he|she|him|her|they|we|i)",
    "(do|don't|did|didn't)\s*(you)\s*(know|understand|hear|think|like)",
    "(^who|^what|^where|^why|^how|^which|^should|^will|^would)",
    # r"(\btell me\b|\bgive me\b)"
    "(^can\\b|^may\\b|^should|^would|^could)",
]

R_QUESTION_DETECTION = r"|".join(question_detection)
answer_nonchalant = []
# Deprecated REGEX
R_ANSWER_UNCERTAIN = r"(\bi\b|\bi'm\b).* (not.* sure|unsure|uncertain|(don't|do not).* know|(don't|do not).* have)"
R_USER_ASK_QUESTION = r"((can|\\wmay) .* (ask|tell) .* question)|((i have a question))"

# Regex for Handle Intents
R_REGEX_FAVORITE = r"you.* favorite.* you.* (like|love).* "
R_REGEX_TECH = r"favorite.* (science|tech|technology)"
R_REGEX_SCIENTIST = r"favorite.* (tech|person|scientist|personality)"

R_USER_DONT_KNOW =  r"|".join(_user_dont_know)

R_USER_LIKE = r"|".join(_user_like)
class IntentHandler:
    """Base Class for Handling All Intents
    """

    def _extract_predefined_intentmap(self):
        """Extract the keyvalues from existing intent maps from nlu module
           Preset the Intentmap to None
        """
        self.custom_sys_intent_map = {k:None for k in sys_re_patterns}
        self.custom_topic_intent_map = {k:None for k in topic_re_patterns}
        self.custom_lexical_intent_map = {k:None for k in lexical_re_patterns}

    def _extract_imported_intent(self, returnnlp):
        self.sys_intent = list(itertools.chain(*returnnlp.sys_intent))
        self.topic_intent = list(itertools.chain(*returnnlp.topic_intent))
        self.lexical_intent = list(itertools.chain(*returnnlp.lexical_intent))
        # intents = [[intent.sys, intent.lexical, intent.topic] for intent in self.imported_intent]
        # self.sys_intent, self.lexical_intent, self.topic_intent = list(map(list, zip(*intents)))

    def detect_sys_intent(self, text=None):
        """Detect Intents given text using REGEX patterns.

        Args:
        text: input_text from user utterance

        Return:
        list of intents

        Raises:
        IOError:  An error occurred oaccessing the bigtable.Table object
        """
        if text is None:
            return self.sys_intent
        
        # Add your own system intent detector.  Optional
        raise NotImplementedError

    def detect_topic_intent(self, text):
        if text is None:
            return self.topic_intent
        
        # Add your own system intent detector.  Optional
        raise NotImplementedError

    def detect_lexical_intent(self, text):
        if text is None:
            return self.lexical_intent
        
        # Add your own system intent detector.  Optional
        raise NotImplementedError




class TechScienceIntentHandler(IntentHandler):
    custom_sys_intent_map = {"get_off_topic":R_EXIT_TECHSCIENCE}
    custom_lexical_intent_map = {"ask_recommend":R_ASK_RECOMMEND,
                                 "suggest_question": R_ASK_QUESTION}
    custom_topic_intent_map = {}
    topic_specific_intent_map = {"req_topics": R_REQ_TOPIC,
                                 "ans_like": R_USER_LIKE,
                                 "req_research": R_REQ_RESEARCH,
                                 "user_expressed_interest": R_EXPRESS_INTEREST,
                                 "req_recommend":R_ASK_RECOMMEND,
                                 "user_expressed_happiness": R_USER_EXPRESSED_HAPPINESS,
                                 "user_ask_question":R_QUESTION_DETECTION,
                                 "req_scitech": R_REQ_SCITECH,
                                 "ans_yes": R_ANSWER_YES,
                                 "ask_more": R_ASK_MORE,
                                 "ans_no": R_ANSWER_NO,
                                 "pos_sent": R_POS_SENT,
                                 "user_want_teach": R_USER_TEACH,
                                 "user_agree":R_USER_AGREE,
                                 "ask_leave":R_ASK_LEAVE,
                                 "ask_opinion": R_ASK_OPINION,
                                 "user_dont_know": R_USER_DONT_KNOW}

    def __init__(self):
        super(TechScienceIntentHandler, self).__init__()

    def build_intent_handler(self, returnnlp):
        self._extract_imported_intent(returnnlp)

        self._extract_predefined_intentmap()
        self._add_custom_intent_map()
        return self

    def _add_custom_intent_map(self):
        for intent in TechScienceIntentHandler.custom_sys_intent_map:
            if intent not in self.custom_sys_intent_map or self.custom_sys_intent_map[intent] is None:
                self.custom_sys_intent_map[intent] = TechScienceIntentHandler.custom_sys_intent_map[intent]
        for intent in TechScienceIntentHandler.custom_lexical_intent_map:
            if intent not in self.custom_lexical_intent_map or self.custom_lexical_intent_map[intent] is None:
                self.custom_lexical_intent_map[intent] = TechScienceIntentHandler.custom_lexical_intent_map[intent]
        
        self.topic_specific_intent_map = TechScienceIntentHandler.topic_specific_intent_map


    def detect_topic_intent(self, text=None):
        """Detect Lexical Intents for Techscience

        Args:
        text: input_text from user utterance

        Return:
        list of intents

        """
        if text is None:
            return self.topic_intent
        
        topic_specific_intent_map = TechScienceIntentHandler.topic_specific_intent_map
        topic_intent = self.topic_intent[:]
        # Add your own system intent detector.  Optional
        for _topic_i in self.topic_specific_intent_map:
            r = self.topic_specific_intent_map[_topic_i]
            if r is not None and re.search(r, text) and _topic_i not in self.topic_intent:
                topic_intent.append(_topic_i)
        return topic_intent

    def detect_lexical_intent(self, text=None):
        """Detect Lexical Intents for Techscience

        Args:
        text: input_text from user utterance

        Return:
        list of intents

        """
        if text is None:
            return self.lexical_intent
        
        lexical_intent = self.lexical_intent[:]
        # Add your own system intent detector.  Optional
        for _lex_i in self.custom_lexical_intent_map:
            r = self.custom_lexical_intent_map[_lex_i]
            if r is not None and re.search(r, text) and _lex_i not in self.lexical_intent:
                lexical_intent.append(_lex_i)
        return lexical_intent

    def detect_sys_intent(self, text=None):
        """Detect System Intents for Techscience

        Two relevant topics in this case are exit techscience.

        Args:
        text: input_text from user utterance

        Return:
        list of intents

        """
        if text is None:
            return self.sys_intent

        sys_intent = self.sys_intent[:]
        # Add your own system intent detector.  Optional
        for _sys_i in self.custom_sys_intent_map:
            r = self.custom_sys_intent_map[_sys_i]
            if r is not None and re.search(r, text) and _sys_i not in self.sys_intent:
               sys_intent.append(_sys_i)
        return sys_intent



############################################################################################################
############################################################################################################

def detect_intent(text):
    """Detect Intents given text using REGEX Patterns

    Args:
        text: input_text from user utterance

    Returns:
        A list of intents

    Raises:
        IOError: An error occurred accessing the bigtable.Table object.
 
    TODO: instead of if elif hierarchy, try all regex and add to dictionary successful matches
    DEPRECATED
    """
    # System level: when this is returned to state response function, start new dialogue flow
    sys_intents = []
    text = text.lower()
    if re.search(R_ASK_RECOMMEND, text):
        sys_intents.append("ask_recommend")
    elif re.search(R_REQ_TOPIC, text):
        sys_intents.append("req_topic")
    elif re.search(R_REQ_RESEARCH, text):
        sys_intents.append("req_research")

    # Lexical level: narrow to broad
    elif re.search(R_ANSWER_YES, text):
        sys_intents.append("answer_yes")
    elif re.search(R_ANSWER_NO, text):
        sys_intents.append("answer_no")
    elif re.search(R_ANSWER_UNCERTAIN, text): # asking question at beginning of utterance
        sys_intents.append("answer_uncertain")
    elif re.search(R_ASK_QUESTION, text): # asking question at beginning of utterance 
        sys_intents.append("ask_question")
    elif re.search(R_REQ_SCITECH, text):
        sys_intents.append("req_scitech")
    # TODO: Refactor
    elif re.search(r"(talk|chat).* about|(\bit|is).* a", text) and [text for topic in TOPIC_LIST if topic in text]:
        sys_intents.append("req_scitech")
    return sys_intents

def detect_sys_intent(text, dialog_act):
    sys_intents = []
    text = text.lower()
    if re.search(R_EXIT_TECHSCIENCE, text):
        sys_intents.append("exit_techscienceRG")
    elif re.search(R_EXIT_TECHSCIENCE2, text):
        sys_intents.append("exit_techscienceRG")
    if re.search(R_USER_ASK_QUESTION, text):
        sys_intents.append("user_ask_question")

    if len(sys_intents) == 0:
        return None
    return sys_intents

def handle_sys_intent(input, intent):
    if intent == "exit_techsciencechat":
        return {
            "next": "s_exittechscienceRG",
            "response": S_EXIT_TECHSCIENCE,
            "context": input["currentstate"],
            "syscode": "exit" # cobot can potentially use this to overwrite reponse or whatever
        }
    elif intent == "ask_recommend":
        return {
            # TODO: should make handle_sys_intent a method of TechScienceAutomaton so I can call it's response functions
            "next": "s_startKG",
            "response": S_START_KG,
        }
    return None


def handle_intent(text, intent):
    if intent == "ask_opinion" or intent == "ask_question":
        if re.search(R_REGEX_FAVORITE, text):
            return S_REGEX_FAVORITE
        elif re.search(R_REGEX_TECH, text):
            return S_REGEX_TECH
        elif re.search(R_REGEX_SCIENTIST, text):
            return S_REGEX_SCIENTIST
        else:
            evidata = EVI_bot(text)
            if evidata:
                return evidata
            # else:
            #     redditdata = retrieve_bot(text)
            #     if redditdata:
            #         return redditdata
            # return "Hm. You have me stumped. "

    return S_DEFAULT