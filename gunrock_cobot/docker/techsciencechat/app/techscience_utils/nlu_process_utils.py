"""NLU Module Processor 2.0

This file is for test only on nlu data class.
"""
import random
import logging
import re
import json
from typing import List, Dict, Tuple
from fuzzywuzzy import fuzz 
import itertools
from nlu.dataclass import ReturnNLP, CentralElement
# from techscience_utils.data_topic import BROAD_TO_SPECIFIC, TOPIC_TO_FACT, KEYWORD_MAPPING, RESPONSE_VARIATION, ACTIVATED_CHITCHAT, PRIORITY
from response_templates import template_data
from techscience_utils.logging_utils import set_logger_level, get_working_environment
from nlu.intentmap_scheme import sys_re_patterns, topic_re_patterns, lexical_re_patterns
from nlu.constants import DialogAct as DialogActEnum
from nlu.constants import Positivity, TopicModule
from techscience_utils.data_topic import KEYWORD_MAPPING, BROAD_TO_SPECIFIC, TOPIC_TO_MODULES
from techscience_utils.regex_data import (R_EXIT_TECHSCIENCE, R_ASK_RECOMMEND, R_ASK_QUESTION, \
                             R_REQ_TOPIC, R_REQ_RESEARCH, R_REQ_SCITECH, R_ANSWER_YES, R_ASK_MORE, \
                                 R_ANSWER_NO, R_POS_SENT, R_USER_DONT_KNOW, R_USER_AGREE)

import os 
dir_path = os.path.dirname(os.path.realpath(__file__))

custom_sys_intent_map = {"get_off_topic":R_EXIT_TECHSCIENCE}

custom_lexical_intent_map = {"ask_recommend":R_ASK_RECOMMEND,
                                 "suggest_question": R_ASK_QUESTION}
custom_topic_intent_map = {}
topic_specific_intent_map = {"req_topic": R_REQ_TOPIC,
                             "user_agree": R_USER_AGREE,
                                 "req_research": R_REQ_RESEARCH,
                                 "req_recommend":R_ASK_RECOMMEND,
                                 "req_scitech": R_REQ_SCITECH,
                                 "ans_yes": R_ANSWER_YES,
                                 "ask_more": R_ASK_MORE,
                                 "ans_no": R_ANSWER_NO,
                                 "pos_sent": R_POS_SENT,
                                 "user_dont_know": R_USER_DONT_KNOW}
keyword_path = os.path.join(dir_path, "keyword_weights.json")
GENERIC_LIST = ["thing", "it", "that", "shouldn't"] #TODO: ADD NOUN FILTER TO REMOVE SHOULDNT
EXTRA_LIST = ["about science", "about technology"]
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class NLU_Processor:
    def __init__(self):
        self._load_weights()

    def _load_weights(self):
        """Load Weights from Keyword Paths
        """
        if not os.path.exists(keyword_path):
            self.weights = None
        with open(keyword_path, "r") as r:
            self.weights = json.loads(r.read())

    @property
    def default_new_topics(self)->Dict[str, str]:
        return {}

    @property
    def user_favorite(self):
        return "ans_like" in self.topic_intent

    @property
    def topic_specific_request(self):
        return "req_scitech" in self.topic_intent

    @property
    def user_like(self):
        return "user_like" in self.topic_intent

    @property
    def user_dont_know(self):
        return "user_dont_know" in self.topic_intent or "ans_unknown" in self.lexical_intent

    @property
    def user_agree(self):
        return "user_agree" in self.topic_intent or "ans_same" in self.lexical_intent

    @property
    def user_interested(self):
        return "user_expressed_interest" in self.topic_intent or "ask_more" in self.topic_intent

    @property
    def default_expected_next_detector(self)->Dict:
        return {}

    @property
    def user_expressed_happiness(self):
        return "user_expressed_happiness" in self.topic_intent

    @property
    def user_utterance(self):
        return self.clean_utterance()


    def clean_utterance(self, utterance=None):
        if utterance == None:
            utterance = self.raw_user_utterance
        for topic in KEYWORD_MAPPING:
            for extra_words in KEYWORD_MAPPING[topic]:
                utterance = utterance.replace(extra_words, topic)
        return utterance

    @property
    def default_module_sentiment_tracker(self)->Dict[str, List[float]]:
        return {"pos":[], "neg":[], "neu":[]}

    def build_nlu_handler(self, returnnlp_raw, central_elem_raw, user_text):
        """Returnnlp and Central Element Dictionary directly from InputData
        """
        self._process_nlu_elements(returnnlp_raw, central_elem_raw)
        self._extract_imported_intent()
        self._extract_predefined_intentmap()
        self._add_custom_intent_map()
        self.raw_user_utterance = user_text[0]
        self.topic_intent = self._detect_topic_intent(self.user_utterance)

        return self

    def print_dialog_acts(self):
        for i, seg in enumerate(self.returnnlp):
            LOGGER.debug("Seg {0}, Sentence {1}: \n  Dialog Acts {1}".format(i, seg.text, seg.dialog_acts))

    def _process_nlu_elements(self, returnnlp_raw, central_elem_raw):
        """Retrieve topic, concept, noun_phrase from returnnlp
           and dialog_act from central_elem
        """
        self.returnnlp = ReturnNLP.from_list(returnnlp_raw)
        self.returnnlp_raw = returnnlp_raw
        setattr(self.returnnlp, "num_segments", len(self.returnnlp))

    def _extract_predefined_intentmap(self):
        """Extract the keyvalues from existing intent maps from nlu module
           Preset the Intentmap to None
        """
        self.custom_sys_intent_map = {k:None for k in sys_re_patterns}
        self.custom_topic_intent_map = {k:None for k in topic_re_patterns}
        self.custom_lexical_intent_map = {k:None for k in lexical_re_patterns}

    def _extract_imported_intent(self):
        if self.is_nlu_valid:
            self.sys_intent = list(itertools.chain(*self.returnnlp.sys_intent))
            self.lexical_intent = list(itertools.chain(*self.returnnlp.lexical_intent))
            self.topic_intent = list(itertools.chain(*self.returnnlp.topic_intent))
        else:
            self.sys_intent = []
            self.lexical_intent = [] 
            self.topic_intent = []

    def _add_custom_intent_map(self):
        """This Populates the intentmap for this class with static intent
           map
        """
        for intent in custom_sys_intent_map:
            if intent not in self.custom_sys_intent_map or self.custom_sys_intent_map[intent] is None:
                self.custom_sys_intent_map[intent] = custom_sys_intent_map[intent]
        for intent in custom_lexical_intent_map:
            if intent not in self.custom_lexical_intent_map or self.custom_lexical_intent_map[intent] is None:
                self.custom_lexical_intent_map[intent] = custom_lexical_intent_map[intent]
        
        self.topic_specific_intent_map = topic_specific_intent_map

    def _detect_topic_intent(self, text=None):
        """Detect Lexical Intents for Techscience

        Args:
        text: input_text from user utterance

        Return:
        list of intents

        """
        if text is None:
            return self.topic_intent
        
        topic_intent = self.topic_intent[:]
        # Add your own system intent detector.  Optional
        for _topic_i in self.topic_specific_intent_map:
            r = self.topic_specific_intent_map[_topic_i]
            LOGGER.debug("Testing {0}, on {1}, result {2}, {3}".format(r, text, re.search(r, text), _topic_i))
            if r is not None and re.search(r, text) and _topic_i not in self.topic_intent:
                topic_intent.append(_topic_i)
        return topic_intent

    def _detect_lexical_intent(self, text=None):
        """Detect Lexical Intents for Techscience

        Args:
        text: input_text from user utterance

        Return:
        list of intents

        """
        if text is None:
            return self.lexical_intent
        
        lexical_intent = self.lexical_intent[:]
        # Add your own system intent detector.  Optional
        for _lex_i in self.custom_lexical_intent_map:
            r = self.custom_lexical_intent_map[_lex_i]
            if r is not None and re.search(r, text) and _lex_i not in self.lexical_intent:
                lexical_intent.append(_lex_i)
        return lexical_intent

    def _detect_sys_intent(self, text=None):
        """Detect System Intents for Techscience

        Two relevant topics in this case are exit techscience.

        Args:
        text: input_text from user utterance

        Return:
        list of intents

        """
        if text is None:
            return self.sys_intent

        sys_intent = self.sys_intent[:]
        # Add your own system intent detector.  Optional
        for _sys_i in self.custom_sys_intent_map:
            r = self.custom_sys_intent_map[_sys_i]
            if r is not None and re.search(r, text) and _sys_i not in self.sys_intent:
               sys_intent.append(_sys_i)
        return sys_intent

    @property
    def is_nlu_valid(self):
        """Check if NLU Elements are Valid
        """
        return self.returnnlp is not None

    def detect_predefined_keywords(self, keyword_mapping=KEYWORD_MAPPING)->Dict[str, Dict[str, float]]:
        """Detect Topic Only from corresponding keyword mapping

        Return:
          - detected_topics: Dict[str, float] (each keyword with a positive or negative association)
        """

        # For Each Segment: Check whether the predefined keyword is detected.
        if self.is_nlu_valid:
            keyword_from_raw = self.get_topics_from_raw_text(keyword_mapping)
            segmented_user_positivity = self.get_user_positivity_from_segments()
            return {k:segmented_user_positivity[i] for i, k in enumerate(keyword_from_raw) if k is not None}
        else:
            detected_keyword = self.get_topics_from_raw_text(keyword_mapping, self.raw_user_utterance)
            local_positivity = self.detect_raw_positivity(self.raw_user_utterance)

            if len(detected_keyword) == 0 or detected_keyword[0] is None:
                return {}
            return {detected_keyword[0]: local_positivity}

    def detect_raw_positivity(self, text:str)->Dict[str, float]:
        sentiment = copy.deepcopy(self.default_module_sentiment_tracker)
        if "pos_sent" in self.topic_intent:
            sentiment["pos"] = 1
        return sentiment

    def get_topics_from_raw_text(self, keywords: Dict[str, set], text: str="")->List[List[str]]:
        """Detect topic from raw text

        self.text is a list of segmented texts extracted from returnnlp
        Parameter:
            keywords: key_word_mapping to search
        Return:
          a list of topics detected.

        """ 
        
        def extract_keyword_fuzz(text, keywords: Dict[str, set]):
            ratios = {}
            for k in keywords:
                ratio = fuzz.WRatio(k, text)
                for v in keywords[k]:
                    if extract_keyword_single(text, v):
                        ratio = 100
                    else:
                        ratio = max(ratio, fuzz.WRatio(v, text))
                ratios[k] = ratio

            filtered = [r for r in ratios if ratios[r] >= 99]
            if len(filtered) == 0:
                return None
            return max(filtered, key=lambda x: ratios[x])

        def extract_keyword_single(text, keyword):
            search = re.search(r"\b{}\b".format(keyword), text)
            return search

        if self.is_nlu_valid:
            _text = self.returnnlp.text
        else:
            _text = [text]

        results = []
        for k in _text:
            results.append(extract_keyword_fuzz(k, keywords))
        return results

    @property
    def detect_user_dont_have_ipad(self):
        return re.search(r"don't have( an|) ipad", self.raw_user_utterance)

    # Check if keywords is a broad topic, if it is, also check the pos_tagging
    def get_user_positivity_from_segments(self)->List[Dict[str, float]]:
        positivity = []
        num_segments = self.returnnlp.num_segments
        LOGGER.debug("Dialog Acts {}".format([seg.dialog_acts for seg in self.returnnlp]))
        for i in range(num_segments):
            positive: Tuple[bool, ...] = (
                'ans_pos' in self.returnnlp.lexical_intent[i],
                'ans_like' in  self.returnnlp.lexical_intent[i],
                self.returnnlp.has_dialog_act(DialogActEnum.POS_ANSWER, index=i),
            )
            negative: Tuple[bool, ...] = (
                'ans_neg' in self.returnnlp.lexical_intent[i],
                self.returnnlp.has_dialog_act(DialogActEnum.NEG_ANSWER, index=i)
            )
            pos = {"pos": 0, "neg": 0, "neu": 0}
            positivity.append(pos)
            if any(positive):
                pos["pos"] = 1
            elif any(negative):
                pos["neg"] = 1
            else:
                pos["neu"] = 1
        return positivity

    def user_ask_fact(self, topic):
        if self.is_nlu_valid:
            return self.returnnlp.has_dialog_act(DialogActEnum.OPEN_QUESTION_FACTUAL)
        else:
            return "what is {}".format(topic) in self.raw_user_utterance

    @property
    def user_ask_opinion(self):
        return self.returnnlp.has_dialog_act([DialogActEnum.OPEN_QUESTION_OPINION]) or "ask_opinion" in self.lexical_intent

    @property
    def user_ask_more(self):
        return "req_more" in self.sys_intent or "ask_more" in self.topic_intent

    @property
    def detect_corona(self):
        if self.is_nlu_valid:
            return "topic_coronavirus" in itertools.chain(*self.returnnlp.topic_intent)
        CORONA_VIRUS = ["corona"]
        old_version = self.text_contains(CORONA_VIRUS)
        if isinstance(old_version, bool):
            return old_version
        if isinstance(old_version, list):
            return len(old_version) > 0
        return old_version is not None

    @property
    def detect_alexa(self):
        alexa = ["you", "i want to talk about you"]
        old_version = self.text_contains(alexa)
        if isinstance(old_version, bool):
            return old_version
        if isinstance(old_version, list):
            return len(old_version) > 0
        return old_version is not None

    @property
    def user_expressed_opinion(self):
        OPINION_THRESHOLD = 5
        user_preference = self.detect_user_expressed_preference
        if len(self.raw_user_utterance.split(" ")) < OPINION_THRESHOLD:
            return False
        if self.is_nlu_valid:
            opinion = self.returnnlp.has_dialog_act("opinion")
            return opinion and not user_preference
        LOGGER.warning("[TECHSCIENCE] returnnlp is None.  Using Local Detector Instead")
        return "user_expressed_opinion" in self.topic_intent and not user_preference

    def keyword_detection(self)->Dict[str, str]:
        if self.is_nlu_valid:
            return self.upgraded_keyword_detection()
        return {}
    

    def upgraded_keyword_detection(self)->Dict[str, str]:
        results = {}
        set_of_handled = set([s for keyword in KEYWORD_MAPPING for s in KEYWORD_MAPPING[keyword]])
        for topic in self.returnnlp.topic:
            processed_topic_keywords = [keyword.keyword for keyword in topic.topic_keywords if keyword.keyword != '']
            if self.upgraded_topic_filter(topic):
                results.update({k: "tech_unused" for k in processed_topic_keywords if k not in set_of_handled and k not in KEYWORD_MAPPING})
            else:
                results.update({k: "non_tech_unused" for k in processed_topic_keywords if k not in set_of_handled and k not in KEYWORD_MAPPING})

        for c in self.returnnlp.concept:
            for l in c:
                for d in l.data:
                    if l.noun in set_of_handled or l.noun in KEYWORD_MAPPING:
                        continue
                    if self.upgraded_concept_net_filter(d):
                        results[l.noun] = "tech_unused"
                    else:
                        results[l.noun] = "non_tech_unused"
        
        nouns = self.filter_keyword_noun_only(results)
        def is_generic(noun):
            return noun in GENERIC_LIST

        nouns = {noun:nouns[noun] for noun in nouns if not is_generic(noun)}
        def replace_with_noun_phrase(noun):
            noun_phrases =  list(itertools.chain(*self.returnnlp.noun_phrase))
            if noun in BROAD_TO_SPECIFIC:
                for l in noun_phrases:
                    if noun in l.split(" ") and l not in EXTRA_LIST:
                        return l
            if noun == "science" or noun == "technology":
                return None
            return noun
        ret = {}
        for k in nouns:
            s = replace_with_noun_phrase(k)
            if s is not None:
                ret[s] = nouns[k]
        return ret


    def upgraded_topic_filter(self, t:str)->bool:
        relevant_weights = [d for d in self.weights["keyword_detector"] if d["source"] == "topic" and d["description"] == t.topic_class and d["confidence"] > t.confidence]
        return sum([r["weight"] for r in relevant_weights]) > 0

    def upgraded_concept_net_filter(self, t:str)->bool:
        relevant_weights = [d for d in self.weights["keyword_detector"] if d["source"] == "concept_net" and d["description"] == t.description and d["confidence"] > t.confidence]
        return sum([r["weight"] for r in relevant_weights]) > 0

    def filter_keyword_noun_only(self, result):
        """For New topic detection, if the keyword is single term.  Make sure it is a noun
        """
        # Map text to postaggin
        pos_tagging = self.get_pos_tagging_from_returnnlp()
        if len(pos_tagging) != len(self.returnnlp.text):
            return {}
        key_map = {}
        for i, segment in enumerate(self.returnnlp.text):
            text = segment.split(" ")
            seg_pos = pos_tagging[i]
            if len(text) == len(seg_pos):
                for j, t in enumerate(text):
                    key_map[t] = seg_pos[j]
            
        


        
        return {r:result[r] for r in result if r in key_map and key_map[r] == "NOUN"}



    def get_pos_tagging_from_returnnlp(self):
        return [data.pos_tagging for data in self.returnnlp.data]

    def get_seg_from_text(self, text):
        for i, seg in enumerate(self.returnnlp):
            if text in seg.text.split(" "):
                return i
        return -1


    @property
    def user_dont_care(self):
        return "ask_leave" in self.lexical_intent or "ans_dont_care" in self.lexical_intent

    def detect_sentiment(self, sentiment_tracker:Dict[str, List[float]]):
        # yes
        # no
        yes_counts = sentiment_tracker.get("yes")
        no_counts = sentiment_tracker.get("no")
        yes = self.user_utterance.lower().strip() == "yes"
        no = self.user_utterance.lower().strip() == "no"
        if yes_counts and len(yes_counts) > 0 and yes:
            sentiment_tracker["no"] = [0]
            sentiment_tracker["yes"][0] += 1
        if no_counts and len(no_counts) > 0 and no:
            sentiment_tracker["yes"] = [0]
            sentiment_tracker["no"][0] += 1
    

    # def old_keyword_detection(self):
    #     """Given a user utterance, (processed from returnnlp). Detect Keywords under certain category.

    #     Supporting:
    #       - IOS, Apple
    #       - Android

    #     Return:
    #       - list of keywords detected
    #     """
    #     result = []
    #     proposed_nounphrase = self.get_topics_from_nounphrase()
    #     r = self.create_regex_for_predefined_keywords(KEYWORD_MAPPING)
    #     for np in proposed_nounphrase:
    #         noun_phrase = self.normalize_topic(np, KEYWORD_MAPPING)
    #         if noun_phrase is not None and re.search(r, noun_phrase):
    #             result.append(noun_phrase)
    #     result.extend([text for text in proposed_nounphrase if re.search(r, text)])
    #     # Process self.topic, self.topic is useful when confidence=999.0
    #     processed_topic = [topic for topic in self.topic if self.topic_filter(topic)]
    #     processed_topic_keywords = [keyword.keyword for t in processed_topic for keyword in t.topic_keywords]
    #     result.extend(processed_topic_keywords)
    #     process_concepted = [l.noun for c in self.concept for l in c if self.concept_net_filter(l)]
    #     result.extend(process_concepted)
    #     raw_regex = self.create_regex_for_raw_variations(KEYWORD_MAPPING)
    #     for t in self.text:
    #         res = re.search(raw_regex, t)
    #         if res:
    #             result.append(t)
    #     return result

    # def googlekg_filter(self, elem):
    #     if elem.description in ["Chemical element"]:
    #         return True
    #     return elem.result_score > 30

    # def concept_net_filter(self, l):
    #     if len(l.data) <= 0:
    #         return False
    #     for data in l.data:
    #         if data.description in ["programming language","metal","subject", "science", "discipline", "electronic device"]:
    #             if data.confidence > 0.5:
    #                 return True
    #     return False





    # def topic_filter(self, t):
    #     if t.topic_class in ["SciTech", "Math"]:
    #         return True
    #     if t.confidence <= .5:
    #         return False

    #     if t.topic_class in ["Celebrities","Phatic","Food_Drink", "Other","Religion", "Movies_TV", "Music", "Games", "Art_Event", "Travel_Geo", "Politics", "Pets_Animals"]:
    #         return False
    #     return True


    # def create_regex_for_predefined_keywords(self, keyword_mapping):
    #     regex = ["\\b{0}\\b".format(v) for v in keyword_mapping]        
    #     TOPIC_REGEX = r"|".join(regex)
    #     return TOPIC_REGEX

    # def create_regex_for_raw_variations(self, keyword_mapping):
    #     regex = []

    #     for k in keyword_mapping:
    #         regex.extend(["\\b{0}\\b".format(v.replace(".", "\\.")) for v in keyword_mapping[k]])        
    #     TOPIC_REGEX = r"|".join([r.replace("\\.\\b", "\\b\\.")for r in regex])
    #     return TOPIC_REGEX

    @property
    def user_want_to_teach(self):
        return "user_want_teach" in self.topic_intent

    @property
    def contains_pos_sent(self):
        """Return True when positive sentiment is overwhelming negative sentiment
           Deprecated, use ReturnNLP TODO
        """
        ans_pos = "ans_pos" in self.lexical_intent and "ans_neg" not in self.lexical_intent
        ans_like = "ans_like" in self.lexical_intent
        pos_sent = "pos_sent" in self.topic_intent

        return self.yes or ans_pos or ans_like or pos_sent


    @property
    def yes(self):
        """Deprecated, use ReturnNLP yes TODO
        """
        if self.is_nlu_valid:
            return self.returnnlp.answer_positivity == Positivity.pos
        LOGGER.warning("[TECHSCIENCE] returnnlp is None.  Using Local Detector Instead")
        return "ans_yes" in self.topic_intent

    @property
    def no(self)->bool:
        """Deprecated, use System Level TODO
        """

        if self.is_nlu_valid:
            return self.returnnlp.answer_positivity == Positivity.neg
        LOGGER.warning("[TECHSCIENCE] returnnlp is None.  Using Local Detector Instead")
        return "ans_no" in self.topic_intent

    @property
    def user_prefer_keyword(self):
        """If user mentioned he/she likes something
        """

        # User expressed opinion
        user_expressed_opinion = self.returnnlp.has_dialog_act("opinion")
        goal_pos_tagging = ["PRON", "VERB", "NOUN"] #TODO Expand this
        preference_keyword = ["like", "prefer", "enjoy", "love"]
        # search for a structure from pos_tagging
        for i, pos_tagging in enumerate(self.get_pos_tagging_from_returnnlp()):
            for j in range(len(pos_tagging)-len(goal_pos_tagging) + 1):
                if pos_tagging[j] == goal_pos_tagging[0]:
                    if pos_tagging[j:j+len(goal_pos_tagging)] == goal_pos_tagging \
                    and self._helper_get_text(i,j + goal_pos_tagging.index("VERB")) in preference_keyword:
                        index_keyword = j + goal_pos_tagging.index("NOUN")
                        return self._helper_get_text(i, index_keyword)

        return None

    @property
    def detect_user_expressed_preference(self):
        user_expressed_opinion = self.returnnlp.has_dialog_act("opinion")
        preference_keyword = ["like", "prefer", "enjoy", "love", "interested in", "interest in"]
        return len(self.text_contains(preference_keyword)) > 0 and user_expressed_opinion

    def _helper_get_text(self, i, j):
        """Get the word of jth index in ith segment
        """
        texts = self.returnnlp.text[i].split(" ")
        if len(texts) <= j:
            return None
        return texts[j]

    @property
    def user_ask_for_recommendation(self):
        return "ask_recommend" in self.lexical_intent

    @property
    def user_claim_wrong(self):
        if self.is_nlu_valid:
            user_expressed_comment = self.returnnlp.has_dialog_act("comment")
            if not user_expressed_comment:
                return False
            incorrect_keywords = ["wrong", "incorrect"]
            goal_pos_tagging = ["ADV", "ADJ"]
            for i, pos_tagging in enumerate(self.get_pos_tagging_from_returnnlp()):
                for j in range(len(pos_tagging)-len(goal_pos_tagging) + 1):
                    if pos_tagging[j] == goal_pos_tagging[0]:
                        if pos_tagging[j:j+len(goal_pos_tagging)] == goal_pos_tagging \
                        and self._helper_get_text(i,j + goal_pos_tagging.index("ADJ")) in incorrect_keywords:
                            return True
                return False

        return "user_claim_wrong" in self.topic_intent

    def text_contains(self, keywords, txt=None):
        result = []
        if not txt:
            txt = self.raw_user_utterance
        for k in keywords:
            if k in txt:
                result.append(k)
        return result

    def detect_jumpout(self)->List[str]:
        if self.is_nlu_valid:
            return [i.value for i in self.returnnlp.topic_intent_modules if i != TopicModule.TECHSCI]
        return []

    # def detect_nonchalant(self):
    #     # Length of user text is low
        # only detect sure for now

        if len(self.text) == 0:
            return False

        return self.text[0].split(" ") < 2 and self.detect_regex(r"sure", self.text[0].lower())
