ERROR_MESSAGE = "<say-as interpret-as='interjection'>Uh oh \
                  </say-as><break time='.05s'/> <prosody rate='80%'> \
                 I think you want to talk about something else. </prosody>",

class NotImplementedError(Exception):
    pass