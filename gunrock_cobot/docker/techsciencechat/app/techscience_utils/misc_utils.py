
"""Contains utilities files for topic detection in automaton.

"""
from difflib import SequenceMatcher
from typing import List
import random

def preprocess_text(txt):
    return txt.lower().strip()

def similar(a, b):
    """Find similarity score between two strings

    Args:
        a: 
        b: 

    Returns:
        A value 
    """
    return SequenceMatcher(None, a, b).ratio()

class Timeout():
    """Timeout class using ALARM signal."""
    class Timeout(Exception):
        pass

    def __init__(self, sec):
        self.sec = sec

    def __enter__(self):
        signal.signal(signal.SIGALRM, self.raise_timeout)
        signal.alarm(self.sec)

    def __exit__(self, *args):
        signal.alarm(0)    # disable alarm

    def raise_timeout(self, *args):
        raise Timeout.Timeout()

def construct_dynamic_response(response):
    all_choices = ""
    flag = False
    if response is not None and response != "":
        for char in response:
            if char == '(':
                flag = True
            elif char == ')':
                flag = False
            if flag and char != '(':
                all_choices += char
            elif not flag and char == ')':
                selected_response = random.choice(all_choices.split('|'))
                replaceable = '(' + all_choices + char
                response = response.replace(replaceable, selected_response)
                all_choices = ""
        response[0].capitalize()
    return response

def detect_question(dialog_act):
    for each in dialog_act:
        if 'DA' in each:
            if "question" in each['DA']:
                return True
    return False

def retrieve_scitech(utterance):
    retrieve = retrv.TopicRetrievalResponseGenerator()
    response_dict = retrieve.execute(utterance)
    if response_dict:
        return response_dict['response']
    return ""

def find_next_chitchat(counter):
    if counter == "":
        return "q1"

    qno = int(counter[1])
    if counter[0] == "q" and qno <= 7:
        return "a" + str(qno)
    elif counter[0] == "a" and qno < 7:
        return "q" + str(qno + 1)
    else:
        return "end_chitchat"

def find_next_chitchat_first(counter):
    if counter == "":
        return "q1"

    qno = int(counter[1])
    if counter[0] == "q" and qno <= 3:
        return "a" + str(qno)
    elif counter[0] == "a" and qno < 3:
        return "q" + str(qno + 1)
    else:
        return ""

def utterance(key: List[str], get_all: bool=False):
    """
    :param key: a list of str that represents the nested dict keys.
                e.g. {key1: {key2: {key3: text}}}, you do ['key1', 'key2', 'key3']
    :param get_all:
    :return: either a list, or an instance of, the utterance, and the hash of the utterance
    terry - not using hash right now because my template format has some qa pairs that don't work with hash
    """
    # return "what what", "hash"
    try:
        from techscience_templates_data import data
        data = data[key[0]]
        for i in key[1:]:
            data = data[i]

        data = [d['text'] for d in data]

        if get_all:
            return data  # type: List[Tuple[str, str]]
        else:
            return random.choice(data)  # type: Tuple[str, str]

    except Exception as e:
        # To keep cobot running, catch everything!
        return "Oh no, I think my creators misspelled something in my code. Would you mind asking something else?"
