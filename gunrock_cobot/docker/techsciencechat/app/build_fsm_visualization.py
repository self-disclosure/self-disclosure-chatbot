"""
this file is designed to build fsm
"""
import os
from techscience_automaton import TechScienceAutomaton
LOGFILE = "automaton_map.json"

modules = os.path.join(os.getenv("COBOT_HOME"), "gunrock_cobot", "docker")

STATIC_STATE_LOG = os.path.join(modules, "techsciencechat", "app", "automaton_logs", LOGFILE)

graph_path = os.path.join(modules, "techsciencechat", "visualization", "static", "graph_info")


def build_dynamic_graph(cleaned_json):
    """Given a log generated from integration test.  Draw out the 
       Path and conversation taken from that log.

       # Source: Daily CSV
       # Source: Integration Test Results
    """
    return

def build_static_graph():
    automaton = TechScienceAutomaton()
    automaton.build_static_graph(STATIC_STATE_LOG)

    os.rename(STATIC_STATE_LOG, os.path.join(graph_path, LOGFILE))
    return automaton

def main():
    return

if __name__ == '__main__':
    main()