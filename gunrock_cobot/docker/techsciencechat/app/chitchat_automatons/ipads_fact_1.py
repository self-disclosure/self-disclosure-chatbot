"""

Fact: 1
- Steve Jobs mentioned the design of a tablet as early as 1983.  
His exact words were. What we want to do is we want to put an incredibly great computer in a book 
that you can carry around with you and learn how to use in 20 minutes."

Followup:
  - I really miss Steve Jobs.  Do you thnk IPad achieved his vision thirty years ago?

User-yes, User-pos
  - "I agree.  I actually think IPads are useful, and the starting sequence for first time usage is pretty simple."

User-no
  - "Yeah probably. Though Apple does have a lot to improve on, I still think the vision is achieved given the number of non-tech
  people using the IPad"

(Common Question):
  - I actually find this hilarious.  When introducing IPhone, Steve Jobs made a clear distaint against stylus.  And now
    the newest product apple introduces is apple pencil.  I wonder what Steve Jobs think about Apple Pencil.

User-pos
  - "Yeah, he probably will like it.  I mean, back then, stylus is a terrible inconvenience.  That's why touchscreen was a
     major selling point of iphone to replace stylus.  Today, apple pencil are irreplacable by just fingers"
 

  - "Sigh.  He is probably won't like it.  I heard he is a very strict person regarding project design.  Though he is a visionary."

(Common Question)

  - Have you tried using apple pencil before?  How do you like it?

User-Pos/User-Yes
  - "That's pretty great.  If I may ask,  What do you use apple pencil for?"

User-No
  - "You are probably right.  I don't have personal experience with apple pencil.  So I won't make comments about it.
     What do you think apple pencil is most useful for?"

End
  - "Thank you for your input.  I'll try to get in touch with Apple's customer support.  I hope their custom service is great
     for chatbots. Haha"
"""

from chitchat_automatons.base import ChitChatAutomaton

class IPADS_FACT_1(ChitChatAutomaton):
  """Sample Chitchat Automaton
  """
  header = "facts"
  topic = "ipads"
  c_id = 1
  history_score = 4.36
  keywords = ["ipads", "apple pencil"]
  potential_next_topics = (None, None, -1) # Disabled
  chitchat_length = 4
  next_topic = "computers"
  turn_level_detector = {1: lambda automaton: any([IPADS_FACT_1._user_said_yes(automaton), IPADS_FACT_1._user_dont_care(automaton), IPADS_FACT_1._user_dont_have_apple_pencil(automaton)]), \
                          2: lambda automaton: any([IPADS_FACT_1._user_said_yes(automaton), IPADS_FACT_1._user_dont_care(automaton), IPADS_FACT_1._user_dont_have_apple_pencil(automaton)]),
                          3: lambda automaton: any([IPADS_FACT_1._user_said_yes(automaton),  IPADS_FACT_1._user_dont_care(automaton), IPADS_FACT_1._user_dont_have_apple_pencil(automaton)]),
                          4: lambda automaton: any([IPADS_FACT_1._user_said_yes(automaton),  IPADS_FACT_1._user_dont_care(automaton), IPADS_FACT_1._user_dont_have_apple_pencil(automaton)])}
  
  def _build_distinguisher(self):
      """Build custom distinguisher for each states in this class
      """
      # distinguisher_map = {1: lambda : self._sentiment_yesno_distinuish(),
      #                      2: lambda : self._no_choice(),
      #                      3: lambda: self._sentiment_yesno_distinuish(),
      #                      4: lambda : self._sentiment_yesno_distinuish(),
      #                      }


      
      distinguisher_map = {1: lambda chitchat_tracker : self.first_turn_distinguish(chitchat_tracker), 
                           2: lambda chitchat_tracker : self.second_turn_distinguish(chitchat_tracker),
                           3: lambda chitchat_tracker : self.third_turn_distinguish(chitchat_tracker),
                           4: lambda chitchat_tracker : self.fourth_turn_distinguish(chitchat_tracker)}

      self.distinguisher_map = distinguisher_map

  @staticmethod
  def _user_said_yes(automaton):
      return automaton.nlu_processor.yes and not automaton.nlu_processor.no

  @staticmethod
  def _user_dont_care(automaton):
      return automaton.nlu_processor.user_dont_care or len(automaton.nlu_processor.text_contains(["don't have", "ipad"])) == 2

  @staticmethod
  def _user_dont_have_apple_pencil(automaton):
      return len(automaton.nlu_processor.text_contains(["don't have", "apple pencil"])) == 2

  def first_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    if IPADS_FACT_1._user_dont_care(self.master_automaton):
      ack = self.generate_acknowledgement(" Oh, I apologize.  I assumed a lot of people have ipads so I thought talking about it may make you to like me.  ")
      self._add_response_string(ack)
      self.set_chitchat_jumpout(chitchat_tracker)
      return
    if IPADS_FACT_1._user_said_yes(self.master_automaton):
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)
      return

    else:
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {}, chitchat_tracker)
      return
  
  def second_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    if IPADS_FACT_1._user_dont_care(self.master_automaton) or IPADS_FACT_1._user_dont_have_apple_pencil(self.master_automaton):
      ack = self.generate_acknowledgement(" Oh, I apologize.  I assumed a lot of people have ipads so I thought talking about it may make you to like me.  ")
      self._add_response_string(ack)
      self.set_chitchat_jumpout(chitchat_tracker)
      return
    if IPADS_FACT_1._user_said_yes(self.master_automaton):
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      
      self.add_response("", {}, chitchat_tracker)
      return
    else:
      ack = self.generate_acknowledgement(" Oh, I apologize.  I assumed a lot of people have ipads so I thought talking about it may make you to like me.  ")
      self._add_response_string(ack)
      self.set_chitchat_jumpout(chitchat_tracker)

  def third_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    if IPADS_FACT_1._user_dont_care(self.master_automaton) or IPADS_FACT_1._user_dont_have_apple_pencil(self.master_automaton):
      ack = self.generate_acknowledgement(" Oh, I apologize.  I assumed a lot of people have ipads so I thought talking about it may make you to like me.  ")
      self._add_response_string(ack)
      return
    if IPADS_FACT_1._user_said_yes(self.master_automaton):
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)
      return
    else:
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {}, chitchat_tracker)
      return

  def fourth_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    if IPADS_FACT_1._user_dont_care(self.master_automaton) or IPADS_FACT_1._user_dont_have_apple_pencil(self.master_automaton):
      ack = self.generate_acknowledgement(" Oh, I apologize.  I assumed a lot of people have ipads so I thought talking about it may make you to like me.  ")
      self._add_response_string(ack)
      return
    if IPADS_FACT_1._user_said_yes(self.master_automaton):
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)
      return
    else:
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {}, chitchat_tracker)
      return

  def _no_choice(self):
    return ""

  def _sentiment_yesno_distinuish(self):
      """Custom defined sentiment distinguishment for current chitchat
      """
      # if self.detect_ask_recommend:
      #   return "r"
      if self.yes:
        return "p"
      if self.positive_sentiment:
        return "p"
      return "n"