"""Every Conversation should have an corresponding automaton to match its
   Change.

Entry Point:
  fact:  Only eight percent of the world’s currency is real physical money, the rest only exists in computers.
         I like how computers are making everything so lightweight and convenient.
  followup:  What do you think of that?

is it safe?

User-pos:
  Haha, I do agree with that.  Sometimes I just think about how computers revolutionalized the life of mankind.  Say, are you 
  interested in cryptocurrency like bitcoins?
User-neg:
  <blimey> I never thought of that.  I can ensure you it is definitely safe enough.  Or else the world will start to break down.
  Hey I am curious, do you own any cryptocurrency like bitcoins?
User-neutral (keyword detect):
  You do make a good point.  Safety may be an concern for these technologies.  I am curious, do you own cryptocurrency?

User-yes: (Agree)
  - "That's pretty awesome.  You do have a good sense of adventure and forsight.  I remember when bitcoins were first created,
     one of the early developers used few thousand dollars worth of bitcoin today to buy one pizza. I can taste the regret."
User-no/unsure:
  - "Perhaps that is a good idea.  I mean, bitcoins fluctuates quite wildly and it is generally unpredictable."

Wrapup:
  - "Anyways, that's all what I can feel about cryptocurrency.  It is a pretty interesting advancement, but we just don't know
  where it is going to lead us."
 
*Exit:  Next topic: fact blockchains
"""

from chitchat_automatons.base import ChitChatAutomaton

class COMPUTERS_FACT_3(ChitChatAutomaton):
  """Sample Chitchat Automaton
  """
  header = "facts"
  topic = "computers"
  c_id = 3
  history_score = 3.0
  keywords = ["digital currency", "bitcoins", "cryptocurrency"]
  potential_next_topics = ("trend", "ipads", 1) # Disabled
  chitchat_length = 2
  next_topic = "bitcoin"
  
  def _build_distinguisher(self):
      """Build custom distinguisher for each states in this class
      """
      # distinguisher_map = {1: lambda : self._sentiment_distinuish(),
      #                      2: lambda: self._yesno_distinguish()
      #                      }
      distinguisher_map = {1: lambda chitchat_tracker : self.first_turn_distinguish(chitchat_tracker), 
                           2: lambda chitchat_tracker : self.second_turn_distinguish(chitchat_tracker),
      }
      
      self.distinguisher_map = distinguisher_map

  def first_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    if self.topic_handler.dialog_act_contains(["yes_no_question"]):
      if self.detected_words(["safe?", "safety", "safe"]):
        chitchat_tracker["trace"] = "k"
        chitchat_tracker["is_followup"] = 0
        self.add_response("k", {}, chitchat_tracker)
        return
    if self.positive_sentiment:
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)
      return
    if self.negative_sentiment:
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {}, chitchat_tracker)
      return
    chitchat_tracker["trace"] = "k"
    chitchat_tracker["is_followup"] = 0
    self.add_response("k", {}, chitchat_tracker)
    return 

  def second_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    chitchat_tracker["is_followup"] = 0
    if self.positive_sentiment:
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)
      return
    elif self.negative_sentiment:
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {}, chitchat_tracker)
    else:
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {}, chitchat_tracker)

  # def _sentiment_distinuish(self):
  #     """Custom defined sentiment distinguishment for current chitchat
  #     """


  #     if self.topic_handler.dialog_act_contains(["yes_no_question"]):
  #       if self.detected_words(["safe?", "safety", "safe"]):
  #         return "k"
  #     if self.positive_sentiment:
  #       return "p"
  #     if self.negative_sentiment:
  #       return "n"
  #     return "k"

  # def _yesno_distinguish(self):
  #     """Custom defined yes/no distinguishment for current chitchat
  #     """
  #     if self.yes:
  #       return "p"
  #     if self.no:
  #       return "n"
  #     return "n"

  def detected_words(self, keywords):
      if len(self.topic_handler.text) == 0:
        return False
      detected_topics = self.topic_handler.text[0]
      for k in keywords:
        if k in detected_topics:
          return True
      return False