"""AI FACT 6

Fact:

  Recently, an AI called Libratus beat world champion poker player at six player poker.  Imagine taking this 
  A.I with you while going to Vegas!

Followup:
  I think it would be hilarous if we have this A.I whisper to our ears while in Caesar Palace.  Do you 
  think that is a good idea?

User-pos
  - <say-as interpret-as=\"interjection\">kaching</say-as> Sorry, I can't hear you with the sound of cash flowing into my head.

User-neg
  - Haha, probably not a good idea.  I heard Vegas hire terminator level security guards to watch each players.  And you cannot win
    too much, or they will throw you out.

Nevertheless, how do you feel about the recent advances of artificial intelligence in classic games?

User-long-response
  - "Probably.  I mean, it is kind of fun to see human vs machine fights.  Kinda reminds me of the Matrix, you know,
     human versus programs.  But by the end of the day, all of those classic games have a closed ending.  There are 
     countable and finite number of possibilities.  That's why machines can handle it.  Not really a big deal.  Not 
     like Machines have conscienous.  What do you think?
User-No:
   - "Yeah, that's probably right.  But it is just classic games.  There are limited possibilities in a classic
      game.  Maybe the number is too big for human to comprehend.  But a machine can easily count to millions, or billions.
      It is not machines have the ability to decide.  It just can see more, and choose the best outcome.  I am not sure 
      if this is actually real intelligence.  What's your take on this?

User-Have-Response:
   - "  That's great.  Any inputs are welcome because I want to figure out how to talk with people regarding artificial intelligence.
      It is a relatively touchy topic, but important for the progress of the society."
User-No-Response:
   - " Okay bro, this is a deep topic to think about.  I understand."

End response:
   - "While we are on the topic, do you like computer science?"
   
"""

from chitchat_automatons.base import ChitChatAutomaton

class AI_FACT_6(ChitChatAutomaton):
  """Sample Chitchat Automaton
  """
  header = "facts"
  topic = "artificial_intelligence"
  c_id = 6
  history_score = 5.0
  keywords = ["classic games", "poker", "ai games"]
  potential_next_topics = ("facts", "artificial_intelligence", 10)
  chitchat_length = 3
  next_topic = "artificial_intelligence"
  
  def _build_distinguisher(self):
      """Build custom distinguisher for each states in this class
      """
      # distinguisher_map = {1: lambda : self._sentiment_distinuish(), 
      #                      2: lambda : self._long_short_no_distinguish(),
      #                      3: lambda : self._long_short_no_distinguish()
      #                      }

      
      distinguisher_map = {1: lambda chitchat_tracker : self.first_turn_distinguish(chitchat_tracker), 
                           2: lambda chitchat_tracker : self.second_turn_distinguish(chitchat_tracker),
                           3: lambda chitchat_tracker : self.third_turn_distinguish(chitchat_tracker)}


      
      self.distinguisher_map = distinguisher_map

  def first_turn_distinguish(self, chitchat_tracker):
      if self.positive_sentiment:
        chitchat_tracker["trace"] = "p"
        chitchat_tracker["is_followup"] = 0
        self.add_response("p", {}, chitchat_tracker)
      else:
        chitchat_tracker["trace"] = "n"
        chitchat_tracker["is_followup"] = 0
        self.add_response("n", {}, chitchat_tracker)

  def second_turn_distinguish(self, chitchat_tracker):
      """Refactoring Only #TODO
      """
      if len(self.master_automaton.text.split(" ")) > self.topic_handler.LENGTH_THRESHOLD:
        chitchat_tracker["trace"] = "p"
        chitchat_tracker["is_followup"] = 0
        self.add_response("p", {}, chitchat_tracker)
        return
      elif self.no or self.user_dont_know:
        chitchat_tracker["trace"] = "n"
        chitchat_tracker["is_followup"] = 0
        self.add_response("n", {}, chitchat_tracker)
        return
      
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)

  def third_turn_distinguish(self, chitchat_tracker):
      """Refactoring Only #TODO
      """
      if len(self.master_automaton.text.split(" ")) > self.topic_handler.LENGTH_THRESHOLD:
        chitchat_tracker["trace"] = "p"
        chitchat_tracker["is_followup"] = 0
        self.add_response("p", {}, chitchat_tracker)
        return
      elif self.no or self.user_dont_know:
        chitchat_tracker["trace"] = "n"
        chitchat_tracker["is_followup"] = 0
        self.add_response("n", {}, chitchat_tracker)
        return
      
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)

  # def _sentiment_distinuish(self):
  #     """Custom defined sentiment distinguishment for current chitchat
  #     """
  #     if self.positive_sentiment:
  #       return "p"
  #     if self.negative_sentiment:
  #       return "n"
  #     return "n"

  # def _long_short_no_distinguish(self):
  #     """Custom defined yes/no distinguishment for current chitchat
  #     """
  #     if len(self.master_automaton.text.split(" ")) > self.topic_handler.LENGTH_THRESHOLD:
  #       return "p"
  #     if self.no:
  #       return "n"
  #     if self.user_dont_know:
  #       return "n"
  #     return "p"

  # def __what_is_detector(self):
  #     return

  @property
  def user_dont_know(self):
    #TODO
    return False