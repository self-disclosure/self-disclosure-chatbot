"""User Driven Dialog

*sometimes:
- oh, then, do you follow the latest tech gadget trends?  I know some updates with {} if you are interested.



Default Start:
  - Do you have a favorite subject?

User stated preference:
  - What do you like about {}?

Respond:
  - {custom acknowledgement}, I like that about science too.  I heard many people like to know {people also ask}. 
  - {custom acknowledgement}, I want to ask.  Do you know {}?
  
"""

class UserDrivenChitchat(ChitChatAutomaton):
  """Sample Chitchat Automaton
  """
  header = "user_driven"
  topic = "all"
  c_id = 0
  history_score = 0.0

  def __init__(self, automaton):
      super(RetrievalDrivenChitchat, self).__init__(automaton)
      # Register State Pools
      self.state_pools = self.register_states()

  def transduce(self, chitchat_tracker):
      """RetrievalDrivenChitchat

      Parameters:
        - chitchat tracker
      Return:
        - updated chitchat tracker
      """

      
      return chitchat_tracker

  def entry_trigger(self):
    return
