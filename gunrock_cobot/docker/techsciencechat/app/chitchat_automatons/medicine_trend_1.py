"""Medicine Trendy

Fact 1: Hey, have you heard about the legendary story behind Phineas Gage?
Followup: I think it is one of the coolest case in the history of medicine.

User-i-have-heard:
Right?  I mean, it is amazing how the guy survived an iron rod  going through his face and into his brain 

User-no:
It’s so crazy! So, what happened was that Phineas Gage was an American railroad construction worker in the 19th century.  He was born around 1823, I think.  One day, there was an explosion  and an iron rod jabbed through his face.  I mean, ouch.  And it went right through his head.

Common:
What's really cool is that even with an one inch wide iron rod  in his skull,  he was still alive!.  When the doctors came, he was still conscious, but vomiting.

Common:
And within the next few days, the doctors need to cut out more of his brain because it was infected.  And remove his left eye.  Like, this is the 19th century before anesthesia was invented. Before antibiotics was a thing.  And he stayed alive, functioning and everything!.  he was in a coma for a while though, but he continued his life for another 12 years or something.

Common:
But later, his personality totally changed.  Basically he turned from a really nice guy to a really annoying piece of work. One example I read was that he became really aggressive. This is the legendary "American Crowbar case,"  pretty cool huh?

Common:
What's even funnier is he donated his iron rod to Harvard Medical School, then reclaimed it back, and nicknamed it "my iron bar" like a little kid with his security blanket.

End:
That's all the story I know about medicine for now.
"""
from chitchat_automatons.base import ChitChatAutomaton

class MEDICINE_TREND_1(ChitChatAutomaton):
  """Sample Chitchat Automaton
  """
  header = "trend"
  topic = "medicine"
  c_id = 1
  history_score = 5.00
  keywords = ["medical history"]
  potential_next_topics = (None, None, -1)
  chitchat_length = 6
  next_topic = "medicine"
  
  def _build_distinguisher(self):
      """Build custom distinguisher for each states in this class
      """
      # distinguisher_map = {1: lambda : self._no_choice(),
      #                      2: lambda : self._user_heard(), 
      #                      3: lambda : self._no_choice(),
      #                      4: lambda : self._no_choice(),
      #                      5: lambda : self._no_choice(),
      #                      6: lambda : self._no_choice(),
      #                      }
      distinguisher_map = {1: lambda chitchat_tracker : self.first_turn_distinguish(chitchat_tracker), 
                           2: lambda chitchat_tracker : self.second_turn_distinguish(chitchat_tracker),
                           3: lambda chitchat_tracker : self.third_turn_distinguish(chitchat_tracker),
                           4: lambda chitchat_tracker : self.fourth_turn_distinguish(chitchat_tracker),
                           5: lambda chitchat_tracker : self.fifth_turn_distinguish(chitchat_tracker),
                           6: lambda chitchat_tracker : self.sixth_turn_distinguish(chitchat_tracker),
                           }

      
      self.distinguisher_map = distinguisher_map

  def first_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    chitchat_tracker["is_followup"] = 0
    self.add_response("", {}, chitchat_tracker)
  
  def second_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    if self.yes:
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)
      return
    else:
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {}, chitchat_tracker)


  def third_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    chitchat_tracker["is_followup"] = 0
    self.add_response("", {}, chitchat_tracker)

  def fourth_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    chitchat_tracker["is_followup"] = 0
    self.add_response("", {}, chitchat_tracker)

  def fifth_turn_distinguish(self, chitchat_tracker):
    chitchat_tracker["is_followup"] = 0
    self.add_response("", {}, chitchat_tracker)

  def sixth_turn_distinguish(self, chitchat_tracker):
    chitchat_tracker["is_followup"] = 0
    self.add_response("", {}, chitchat_tracker)

  def _user_heard(self):
      """Custom defined sentiment distinguishment for current chitchat
      """
      if self.yes:
        return "p"
      if self.user_dont_know:
        return "n"
      return "n"