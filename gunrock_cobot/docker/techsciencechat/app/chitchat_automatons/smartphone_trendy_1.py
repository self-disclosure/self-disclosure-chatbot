"""Smartphone trendy

Entry Point:
  fact:  By the way, I am curious, what phone do you use?

User-pos:
  *Identify your phone.
  That's a good choice.  Personally I don't have a smart phone.  I should ask Amazon to make their own smart phones.  
  I am curious, do you want to get a new smart phone in the next few months?
User-dontknow:
  *I don't use a smart phone.
  That's okay.  Although I want to mention, it is rare these days for someone not using a smart phone.  Maybe you are
  just very young or very oldschool.  Do you ever want to get a new phone?

Neutral:
  That's interesting.  Say, do you plan to get a new phone?

User-pos:
  - what do you like about your phone?  Like, what features do you find useful?
User-neg:
  - Haha, that's fair.  Say, if you ever get a smart phone, what kind of features do you want from it?

Wildcard:
- *ask_recommend:  For now, I don't know if I can make some good recommendations.  I think water proof is 
very important in smart phones.  Other than that, also make sure you feel comfortable with what it can do.
After all, it is your phone.

User-pos/non-neg: (Agree)
  - That's some good choices.  Personally for me, it is hard to use many features.  Though I think the only relevant feature
    that I would need is for the smart phone to support the alexa app. 
User-i-dont-want/neg:
  - Haha, that is alright.  You kind of remind me of Christopher Walken, He is a pretty famous actor.  I heard some of his impersonation on Big Bang Theory, but
  I never seen any of his movies.  He doesn't even have a cellphone!   

Ending
  - "Anyways, I recommend looking at E3 Expo next year for some updated tech.  I predict better foldable phones will be out.
     Do you want to know some cool facts about modern tech company?"

*Exit:  Next topic: trendy smart phones
"""

from chitchat_automatons.base import ChitChatAutomaton

class SMARTPHONE_TREND_1(ChitChatAutomaton):
  """Sample Chitchat Automaton
  """
  header = "trend"
  topic = "smartphone"
  c_id = 1
  
  keywords = ["water proof", "cellphone", "smark phones"]
  potential_next_topics = ("facts", "computers", 3) # Disabled
  chitchat_length = 4
  next_topic = "engineering"
  
  def _build_distinguisher(self):
      """Build custom distinguisher for each states in this class
      """
      # distinguisher_map = {1: lambda : self._no_choice(),
      #                      2: lambda : self._sentiment_distinuish(),
      #                      3: lambda: self._yesno_distinguish(),
      #                      4: lambda : self._no_choice(),
      #                      }

      distinguisher_map = {1: lambda chitchat_tracker : self.first_turn_distinguish(chitchat_tracker), 
                           2: lambda chitchat_tracker : self.second_turn_distinguish(chitchat_tracker),
                           3: lambda chitchat_tracker : self.third_turn_distinguish(chitchat_tracker),
                           4: lambda chitchat_tracker : self.fourth_turn_distinguish(chitchat_tracker)}

      
      self.distinguisher_map = distinguisher_map

  def first_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    chitchat_tracker["is_followup"] = 0
    self.add_response("", {}, chitchat_tracker)
  
  def second_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    if self.detected_words(["don't have", "don't own"]):
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {}, chitchat_tracker)
      return
    if self.positive_sentiment:
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)
      return
    chitchat_tracker["trace"] = "k"
    chitchat_tracker["is_followup"] = 0
    self.add_response("k", {}, chitchat_tracker)

  def third_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    if self.yes:
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)
      return
    else:
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {}, chitchat_tracker)
      return

  def fourth_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    chitchat_tracker["is_followup"] = 0
    self.add_response("", {}, chitchat_tracker) 

  def _no_choice(self):
    return ""
  def _sentiment_distinuish(self):
      """Custom defined sentiment distinguishment for current chitchat
      """
      # if self.detect_ask_recommend:
      #   return "r"
      if self.detected_words(["don't have", "don't own"]):
        return "n"
      if self.positive_sentiment:
        return "p"
      return "k"


  def _yesno_distinguish(self):
      """Custom defined yes/no distinguishment for current chitchat
      """
      # if self.detect_ask_recommend:
      #   return "r"
      if self.yes:
        return "p"
      if self.no:
        return "n"
      return "n"

  def detect_ask_recommend(self):
    return "ask_recommend" in self.lexical_intent

  def detected_words(self, keywords):
      if len(self.topic_handler.text) == 0:
        return False
      detected_topics = self.topic_handler.text[0]
      for k in keywords:
        if k in detected_topics:
          return True
      return False