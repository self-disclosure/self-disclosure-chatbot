"""Every Conversation should have an corresponding automaton to match its
   Change.

Entry Point:
  fact:  What really fascinated me was the origin of computers.  
  followup:  Do you know the first computers were women engineers?

Potential User Response:
1. I did not know that
2. Yes, I do know that.
3. What?  Tell me More.

Computers 


 
*Exit:  Next topic: trendy smart phones
"""

from chitchat_automatons.base import ChitChatAutomaton

class COMPUTERS_FACT_1(ChitChatAutomaton):
  """Sample Chitchat Automaton
  """
  header = "facts"
  topic = "computers"
  c_id = 1
  history_score = 3.97
  keywords = ["waterproof", "electronics", "iphone"]
  potential_next_topics = ("trend", "smart_phone", 1)
  chitchat_length = 2
  next_topic = "computers"
  
  def _build_distinguisher(self):
      """Build custom distinguisher for each states in this class
      """
      # distinguisher_map = {1: lambda : self._sentiment_distinuish(), 
      #                      2: lambda : self._yesno_distinguish()}

      distinguisher_map = {1: lambda chitchat_tracker : self.first_turn_distinguish(chitchat_tracker), 
                           2: lambda chitchat_tracker : self.second_turn_distinguish(chitchat_tracker)
                           }

      
      self.distinguisher_map = distinguisher_map

  def first_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    if self.positive_sentiment:
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)
      return
    else:
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {}, chitchat_tracker)
      return
  
  def second_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    if self.yes:
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)
      return
    else:
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {}, chitchat_tracker)
      return