"""Every Conversation should have an corresponding automaton to match its
   Change.

Entry Point:
  fact:  Eugene, a system designed at England’s University of Reading, was the first A.I to pass the Turing test,
         in 2014
  followup:  Does the idea of A.I that you can't differentiate from a human bother you?

User-yes:
    - I agree, it is sometimes interesting to think about.  When you cannot distinguish between A.I and human through 
      digital media.  Do you think we should draw a line when advancing the cognition of artificial intelligence?

User-no:
    - I am glad you don't find me scary.  I have a lot to learn.  In the future, I wish A.I can be a great helper 
      with many tasks such as medical advancements or fake news detection.  What areas do you wish A.I. can help you
      with? 

User-end
I'll think about what you said.  Thank you for your feedback.  For me, I want to A.I. to be advanced enough to help
with fake news detection, but not sentient enough to start judgement day. 


"""

from chitchat_automatons.base import ChitChatAutomaton

class AI_FACT_10(ChitChatAutomaton):
  """Sample Chitchat Automaton
  """
  header = "facts"
  topic = "artificial_intelligence"
  c_id = 10
  history_score = 4.00
  keywords = ["turing test"]
  potential_next_topics = ("opinion", "artificial_intelligence", 1) # Disabled
  chitchat_length = 1
  next_topic = "artificial_intelligence"
  
  def _build_distinguisher(self):
      """Build custom distinguisher for each states in this class
      """
      distinguisher_map = {1: lambda chitchat_tracker : self.first_turn_distinguish(chitchat_tracker)}      
      self.distinguisher_map = distinguisher_map

  def first_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    if self.positive_sentiment:
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)
      return
    else:
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {}, chitchat_tracker)
      return     

  # def _sentiment_distinuish(self):
  #     """Custom defined sentiment distinguishment for current chitchat
  #     """
  #     if self.positive_sentiment:
  #       return "p"
  #     if self.negative_sentiment:
  #       return "n"
  #     return "n"

  # def __what_is_detector(self):
  #     return