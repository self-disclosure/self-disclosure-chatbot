"""Cybertruck trendy

Q: So, what do you think about Tesla's new Cybertruck?
A: I don’t really know much about it.
Q: Oh, ok. The Cybertruck is Tesla’s new electric pickup truck. It’s pretty crazy looking. Do you think you’d buy an electric pickup?
A: Sure, I’d love to have an electric car.
Q: Cool. And actually, some people say that overall the Cybertruck is cheaper than a Ford F150. Have you seen a picture of a Cybertruck?
A: No, I haven’t
Q: They’re pretty weird looking. They kinda look like a truck version of a DeLorean from Back to the Future.
"""

"""Every Conversation should have an corresponding automaton to match its
   Change.

Entry Point:
  fact:  So, what do you think about Tesla's new Cybertruck?

User-pos:
  That's fair. The truck actually makes me miss Lightning McQueen. <kachow> Say, do you want to try driving it?
User-dontknow:
  Oh, ok. The Cybertruck is Tesla’s new electric pickup truck. It’s pretty crazy looking. Do you think you’d buy an electric pickup?
User-neg:
  Hehehe, <break>   I mean, dude, I really feel bad for tesla.  <bam>, there goes the company's stock price.  But, there are a lot of people
  preordering the car, do you think you would want it?

User-yes: (Agree)
  - "Cool. I would want it if I could drive a car.  And actually, some people say that overall the Cybertruck is cheaper than a Ford F150. Have you seen a picture of a Cybertruck?"
User-no:
  - "Haha, that's okay.  The car does seem cool, but it is also wise to see how it performs before buying yet.  Say, have you seen an picture of the car?"

User-yes/User-no
  - "I think it looks pretty cool. They kinda look like a truck version of a DeLorean from Back to the Future."

End:
  - "I really like talking with you about this.  Now I have some clear picture what people actually want in an car"
 
*Next ChitChat Intro:
(What type of car do you drive?)
"""

from chitchat_automatons.base import ChitChatAutomaton

class CYBERTRUCK_TRENDY_1(ChitChatAutomaton):
  """Sample Chitchat Automaton
  """
  header = "trend"
  topic = "cybertruck"
  c_id = 1
  history_score = 3.25
  keywords = ["cybertruck", "automotive technology", "drive"]
  potential_next_topics = ("trend", "medicine", 1) # Disabled
  chitchat_length = 4
  next_topic = "engineering" # Change this to automotive Technology

  turn_level_detector = {1: lambda automaton: any([CYBERTRUCK_TRENDY_1.user_dont_know(automaton)]),\
                         2: lambda automaton: any([]),
                         3: lambda automaton: any([]),
                         4: lambda automaton: any([]),
                         
                         }

  @staticmethod
  def user_said_local(automaton, turn=0):
    return automaton.nlu_processor.user_dont_know

  
  def _build_distinguisher(self):
      """Build custom distinguisher for each states in this class
      """
      # distinguisher_map = {1: lambda : self._no_choice(),
      #                      2: lambda: self._sentiment_distinuish(),
      #                      3: lambda: self._yesno_distinguish(),
      #                      4: lambda : self._no_choice(),
      #                      }
      
      distinguisher_map = {1: lambda chitchat_tracker : self.first_turn_distinguish(chitchat_tracker), 
                           2: lambda chitchat_tracker : self.second_turn_distinguish(chitchat_tracker),
                           3: lambda chitchat_tracker : self.third_turn_distinguish(chitchat_tracker),
                           4: lambda chitchat_tracker : self.fourth_turn_distinguish(chitchat_tracker)}

      self.distinguisher_map = distinguisher_map

  def first_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    chitchat_tracker["is_followup"] = 0
    self.add_response("", {}, chitchat_tracker)
  
  def second_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    if CYBERTRUCK_TRENDY_1.user_dont_know:
      chitchat_tracker["trace"] = "d"
      chitchat_tracker["is_followup"] = 0
      self.add_response("d", {}, chitchat_tracker)
      return
    if self.positive_sentiment:
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)
      return
    if self.negative_sentiment:
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {}, chitchat_tracker)
      return
    chitchat_tracker["trace"] = "p"
    chitchat_tracker["is_followup"] = 0
    self.add_response("p", {}, chitchat_tracker)
    return


  def third_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    if self.yes:
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)
      return
    else:
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {}, chitchat_tracker)
      return

  def fourth_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    chitchat_tracker["is_followup"] = 0
    self.add_response("", {}, chitchat_tracker)
