"""What type of Car do you drive, chitchat.

By the way, do you drive? If so, what type of car do you drive?
I have a Prius
How long have you had you Prius?
Like 6 years.
Have you thought about getting a new car anytime soon?
Yeah, I have.
Any ideas what you might buy? What’s your dream car?
Defintely a Tesla.
Nice! A Tesla is a pretty expensive car. Is there any particular reason you want a Tesla?

Entry Point:
  fact:  By the way, do you drive?  If so, what type of car do you drive?

*Identify keyword from car database.
User-yes/I drive car **.
  - How long have you had your car?
User-yes/No car:
  - 
User-no, I don't drive:
  - 

User-yes: (Agree)
  - "Good choice.  Just imagine having a phone that is not waterproof, and next to a body of water.
     burrr, thinking of it gives me chills."
User-no/unsure:
  - "I heard Huawei P30 Pro, Samsung Galaxy Note 9, and IPhone XR are the best rated water proof smartphones according
         to tech authority.  The rating is based on waterproof IP.  That's all what I can remember."

Topic_End

*Wildcard:
What is {water_proof_ip}:
- "I am glad you asked.  Water proof IP, IP standands for Ingress Protection. Rated 0 is no protection, meaning
         the phone is easily damaged by water, 10 means it can overcome high pressured water jets.  That's all I know for now.
         Though I do think it is nerve wracking to have a phone that can be easily damaged by water."
 
*Exit:  Next topic: trendy smart phones
"""