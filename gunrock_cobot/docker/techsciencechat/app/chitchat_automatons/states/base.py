"""Chitchat Automaton States
"""
import logging

import template_manager
from techscience_utils.data_topic import MODULE_TO_RESPONSE
from techscience_utils.logging_utils import set_logger_level, get_working_environment

TEMPLATE_TECHSCI = template_manager.Templates.techscience
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())


class BaseState:
    """The Base State for the automaton.
    """

    def __init__(self, automaton, chitchat_tracker):
        self.chitchat_tracker = chitchat_tracker
        self.automaton = automaton
        return

    def __call__(self):
        return None

    @property
    def utterance(self):
        return self.automaton.master_automaton.text