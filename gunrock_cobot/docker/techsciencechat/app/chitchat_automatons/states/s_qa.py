"""This is a sample state in chitchat automaton

"""
import logging

import template_manager
from techscience_utils.data_topic import MODULE_TO_RESPONSE
from techscience_utils.logging_utils import set_logger_level, get_working_environment
from techscience_utils.retrieval_utils import retrieve_reddit, evi_bot, get_interesting_reddit_posts
from backstory_client import get_backstory_response_text

TEMPLATE_TECHSCI = template_manager.Templates.techscience
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

REDDIT_LIMIT = 2

from chitchat_automatons.states.base import BaseState

class QA(BaseState):
    name = "s_qa"
    def __call__(self):
        LOGGER.debug("Entering State: {}".format("s_qa"))

        # If Opinion Question:
        if self.is_opinion:
            # Backstory
            response = self.get_backstory_response(0.70)

            if response is None:
                response = self.get_reddit_post()
            if response is None:
                response = self.template_response("no_opinion")      
        else:
            # Retrieve KG Topic
            response = self.get_google_kg()
            # Get From EVI
            if response is None:
                response = self.get_from_evi()
            # Get backstory with high threshold
            if response is None:
                response = self.get_backstory_response()

            # No Fact
            if response is None:
                response = self.template_response("no_fact")
        return "t_qa"

    @property
    def is_opinion(self):
        regex_opinion = "ask_opinion" in self.automaton.topic_intent
        dialog_act_opinion = self.automaton.topic_handler.dialog_act_contains(["open_question_opinion"])
        return regex_opinion or dialog_act_opinion

    def get_backstory_response(self, threshold):
        ab_test = self.ab_test
        backstory = get_backstory_response_text(self.utterance, \
            ab_test, threshold)
        return backstory

    @property
    def ab_test(self):
        return self.automaton.master_automaton.ab_test
    def get_reddit_post(self):
        reddit_limit = get_interesting_reddit_posts(self.keyword, REDDIT_LIMIT)
        if len(reddit_limit) > 0:
            return reddit_limit[0]
        return None

    def get_google_kg(self):
        return None

    def template_response(self, tag):
        selector = "s_qa/{}".format(tag)
        return self.automaton._add_response(selector, {})

    def get_from_evi(self):
        return  evi_bot(self.utterance)