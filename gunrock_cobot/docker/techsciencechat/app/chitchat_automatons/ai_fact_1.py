"""Every Conversation should have an corresponding automaton to match its
   Change.  Convert to user privacy

Entry Point:
  fact:  Artificial intelligence today are involved with a lot of online product recommendation.
  
  followup:  Do you like some of the products recommended by the shopping sites?

# Anticipate a user keyword

# Else:

# Acknowledge:
- 
User-yes:
  I agree.  I think having online product recommendation is a win win situation.  I want a good product that fits my need, the company wants to sell their product.
User-no:
  Haha, perhaps there is something we could improve upon

Common Point:
  - Actually, the better question to ask is, do you think online product recommendations poses an intrusion of privacy?

User-yes: (Agree)
  - "Hmm, you do make a good point.  Online recommendation is pretty helpful.  My suggestion is to let the users know what data about them are actually collected.  
     This can give people a sense of control."
User-neg-sentiment:  
  - "Perhaps not, I also heard today artificial intelligence is not as all powerful as publicized.  I am pretty sure the majority of the work is just a large 
     data collection or data mining"

Common Point:
  - I am curious.  Do you like artificial intelligence in your life?
"""
from techscience_utils.logging_utils import set_logger_level, get_working_environment
from chitchat_automatons.base import ChitChatAutomaton
import logging

LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class AI_FACT_1(ChitChatAutomaton):
  """Sample Chitchat Automaton
  """
  header = "facts"
  topic = "artificial_intelligence"
  c_id = 1
  history_score = 5.0
  keywords = ["data mining", "user privacy"]
  potential_next_topics = ("opinion", "artificial_intelligence", 1)
  chitchat_length = 2
  next_topic = "artificial_intelligence"

  turn_level_detector = {1: lambda automaton: any([AI_FACT_1._user_ask_more_information(automaton),\
                                                   AI_FACT_1._user_ask_opinion(automaton),\
                                                   AI_FACT_1._positive_connotation(automaton),\
                                                   AI_FACT_1._user_dont_care(automaton),\
                                                   AI_FACT_1._negative_connotation(automaton),\
                            ]),
                         2: lambda automaton: any([AI_FACT_1.user_said_online(automaton),\
                                                   AI_FACT_1.user_said_local(automaton),\
                                                   len(AI_FACT_1.detected_suggestions(automaton)) > 0,\
                                                   AI_FACT_1._positive_connotation(automaton),\
                                                   AI_FACT_1._negative_connotation(automaton),\
                            ]),
                         
                         }
  
  def _build_distinguisher(self):
      """Build custom distinguisher for each states in this class
      """
      distinguisher_map = {1: lambda chitchat_tracker : self.first_turn_distinguish(chitchat_tracker), 
                           2: lambda chitchat_tracker : self.second_turn_distinguish(chitchat_tracker)}
      
      self.distinguisher_map = distinguisher_map

  def first_turn_distinguish(self, chitchat_tracker):
    acknowledgement = ""
    # Case 1: User expressed complement, we can followup and add p
    if AI_FACT_1._user_ask_opinion(self.master_automaton, 1) or AI_FACT_1._user_dont_care(self.master_automaton, 1):
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {"acknowledgement": acknowledgement}, chitchat_tracker)
      return

    if AI_FACT_1._user_ask_more_information(self.master_automaton, 1):
      chitchat_tracker["trace"] = "m"
      chitchat_tracker["is_followup"] = 0
      self.add_response("m", {}, chitchat_tracker)
      return 

    if AI_FACT_1._positive_connotation(self.master_automaton, 1):
      self.add_acknowledgement(["user_positive_opinion"])
      chitchat_tracker["trace"] = "a"
      chitchat_tracker["is_followup"] = 0
      self.add_response("a", {}, chitchat_tracker)
      return 

    # # Case 2: User don't know, User don't care. 
    # if AI_FACT_1._user_dont_care(self.master_automaton, 1):
    #   self.set_chitchat_jumpout(chitchat_tracker, True)
    #   # Jump Out and ask what are you interested in? TODO
    #   return 
    # Case 3: User expressed negative connotation
    elif AI_FACT_1._negative_connotation(self.master_automaton, 1):
      self.add_acknowledgement(["user_complaint"])
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {"acknowledgement": acknowledgement}, chitchat_tracker)
    else:
      # Defaults to my opinion
      chitchat_tracker["trace"] = "p"
      self.add_response("p", {"acknowledgement": acknowledgement}, chitchat_tracker)
      chitchat_tracker["is_followup"] = 0
      return 

  def add_acknowledgement(self, tags):
    # Thank you for your complement
    if self.acknowledgement_handler.ack_available(tags_only=tags):
      self._add_response_string(self.acknowledgement_handler.get_ack)
    return

  @staticmethod
  def user_said_local(automaton, turn=0):
    LOGGER.debug("Checking User Said Local")
    return automaton.nlu_processor.no or len(automaton.nlu_processor.text_contains(["grocery", "stores"])) > 0

  @staticmethod
  def user_said_online(automaton, turn=0):
    LOGGER.debug("Checking User Said Online")
    return automaton.nlu_processor.yes or len(automaton.nlu_processor.text_contains(["online"])) > 0

  def generate_custom_yes_no_answer(self, user_utterance):
    LOGGER.debug("User Utterance to generate response {}".format(user_utterance))
    if len(self.master_automaton.nlu_processor.text_contains(["can you"], user_utterance)) > 0:
      return "yes i can, "
    return ""

  def second_turn_distinguish(self, chitchat_tracker):
    prev_utterance = chitchat_tracker.get("trace")
    detected_suggestions = AI_FACT_1.detected_suggestions(self.master_automaton)
    LOGGER.debug(prev_utterance)

    yes_no_question = self.master_automaton.question_handler.yes_no_question
    acknowledgement = ""
    # If it is yes no question, we can answer it.
    if yes_no_question:
      acknowledgement = self.generate_custom_yes_no_answer(self.master_automaton.nlu_processor.user_utterance)
    # If other question, we answer it in another turn 
    if prev_utterance == "a" and not chitchat_tracker["is_followup"]:
      if AI_FACT_1.user_said_online(self.master_automaton):
        chitchat_tracker["trace"] = "a"
        chitchat_tracker["is_followup"] = 1
        self.add_response("a", {"acknowledgement": acknowledgement}, chitchat_tracker)
        return
      elif AI_FACT_1.user_said_local(self.master_automaton):
        chitchat_tracker["trace"] = "an"
        self.add_response("an", {"acknowledgement": acknowledgement}, chitchat_tracker)
      else:
        chitchat_tracker["trace"] = "p"
        self.add_response("p", {"acknowledgement": acknowledgement}, chitchat_tracker)
      chitchat_tracker["is_followup"] = 0
      return

    if prev_utterance == "n" and len(detected_suggestions) > 0:
      chitchat_tracker["trace"] = "l"
      chitchat_tracker["is_followup"] = 0
      self.add_response("l", {"user_suggestions": detected_suggestions[0]}, chitchat_tracker) #TODO, fintune detected_suggestions
      return

    elif prev_utterance == "a" and len(detected_suggestions) > 0:
      chitchat_tracker["trace"] = "l"
      chitchat_tracker["is_followup"] = 0
      self.add_response("l", {"user_suggestions": detected_suggestions[0]}, chitchat_tracker) #TODO, fintune detected_suggestions
      return

    elif AI_FACT_1.user_said_no(self.master_automaton) or AI_FACT_1._user_dont_care(self.master_automaton):
      chitchat_tracker["trace"] = "no"
      chitchat_tracker["is_followup"] = 0
      self.add_response("no", {"acknowledgement": "", "user_suggestions": "it"}, chitchat_tracker)
      return
  
    elif AI_FACT_1._negative_connotation(self.master_automaton):
      self.add_acknowledgement(["user_complaint"])
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {}, chitchat_tracker)
      return
    else:
      chitchat_tracker["is_followup"] = 0
      self.add_acknowledgement([])
      chitchat_tracker["trace"] = "p"
      self.add_response("p", {"acknowledgement": acknowledgement}, chitchat_tracker)

  @staticmethod
  def detected_suggestions(automaton, turn=0):
    """Improve User Detected Suggestions
    """
    SHOPPING_SITES = ["instacart", "walmart", "safeway", "costco", "amazon", "target"]

    # First case, get one word noun
    sites = automaton.nlu_processor.text_contains(SHOPPING_SITES)
    return sites

  @staticmethod
  def user_said_no(automaton, turn=0):
    return automaton.nlu_processor.no

  @staticmethod
  def _user_ask_more_information(automaton, turn=0):
    LOGGER.debug("Triggered User Ask More in AIFACT1")
    return automaton.nlu_processor.text_contains(["how"]) #follow up question detector is a little problemmatic, I'll use it after more testing
  
  @staticmethod
  def _user_ask_opinion(automaton, turn=0):
    LOGGER.debug("Triggered User Opinion in AIFACT1")
    return automaton.nlu_processor.user_ask_opinion

  @staticmethod
  def _user_dont_care(automaton, turn=0):
    return automaton.nlu_processor.user_dont_care


  @staticmethod
  def _positive_connotation(automaton, turn=0):
    # Detect When User has a positive connotation
    postivity_raw_data = automaton.nlu_processor.get_user_positivity_from_segments()
    # combine positivity

    # Answer Positivity
    if automaton.nlu_processor.yes:
      return True
    elif automaton.nlu_processor.no or automaton.topic_handler.contains_neg_sent:
      return False

    pos = 0
    neg = 0
    neu = 0
    for d in postivity_raw_data:
      pos += d["pos"]
      neg += d["neg"]
      neu += d["neu"]
    if pos > neg and pos > neu:
      return True
    return False

  @staticmethod
  def _negative_connotation(automaton, turn=0):
    if automaton.nlu_processor.no or automaton.topic_handler.contains_neg_sent:
      return True
    elif automaton.nlu_processor.yes:
      return False
    # Detect When User has a positive connotation
    postivity_raw_data = automaton.nlu_processor.get_user_positivity_from_segments()
    # combine positivity
    pos = 0
    neg = 0
    neu = 0
    for d in postivity_raw_data:
      pos += d["pos"]
      neg += d["neg"]
      neu += d["neu"]
    if pos < neg and neg > neu:
      return True
    return False