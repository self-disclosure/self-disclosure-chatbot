"""Trendy Chitchat Around IPads 2

"""

from chitchat_automatons.base import ChitChatAutomaton

class IPAD_TREND_2(ChitChatAutomaton):
  """Sample Chitchat Automaton
  """
  header = "trend"
  topic = "ipads"
  c_id = 2
  history_score = 4.00
  keywords = []
  potential_next_topics = ("facts", "ipads", 1) # Disabled
  chitchat_length = 2
  next_topic = "ipads"

  turn_level_detector = {1: lambda automaton: any([IPAD_TREND_2._user_said_yes(automaton), IPAD_TREND_2._user_said_no(automaton)]), \
                          2: lambda automaton: any([IPAD_TREND_2.user_ask_for_recommendation(automaton)])}
  
  def _build_distinguisher(self):
      """Build custom distinguisher for each states in this class
      """


      distinguisher_map = {1: lambda chitchat_tracker : self.first_turn_distinguish(chitchat_tracker), 
                           2: lambda chitchat_tracker : self.second_turn_distinguish(chitchat_tracker),
                           }

      self.distinguisher_map = distinguisher_map

  @staticmethod
  def _user_said_yes(automaton):
      return automaton.nlu_processor.yes and not automaton.nlu_processor.no
    
  @staticmethod
  def _user_said_no(automaton):
      return automaton.nlu_processor.no and not automaton.nlu_processor.yes

  @staticmethod
  def user_ask_for_recommendation(automaton):
      return automaton.nlu_processor.user_ask_for_recommendation

  @staticmethod
  def user_draw_on_ipad(automaton):
      return len(automaton.nlu_processor.text_contains(["drawing on", "ipad"])) == 2

  def first_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    if IPAD_TREND_2._user_said_yes(self.master_automaton) or IPAD_TREND_2.user_draw_on_ipad(self.master_automaton):
      self.add_response("p", {}, chitchat_tracker)
    elif IPAD_TREND_2._user_said_yes(self.master_automaton):
      self.add_response("n", {}, chitchat_tracker)
    else:
      self.add_response("p", {}, chitchat_tracker)
    
  
  def second_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    if IPAD_TREND_2.user_ask_for_recommendation(self.master_automaton):
      chitchat_tracker["trace"] = "r"
      chitchat_tracker["is_followup"] = 0
      self.add_response("r", {}, chitchat_tracker)
    else:
      self.add_response("e", {}, chitchat_tracker)

    return

