"""
    Opinion
      - IPads are pretty cool.  I think it's really good for like, light computer usage.  Personally, I like it to
        draw and take notes. It may be a little pricy though.  
    q1:
      - " Anyways, what's do you like about ipads? "

    a1_1:
        User-pos:
        - " You do make a good point. Though I do wish the price is lower.  But I also found IOS system is relatively stable. "

        User-no
        - " Haha, we all have our preferences.  Though I do wish Apple Engineers can take your advice and introduce some defining 
            features about the product rather than just making the phone larger.  Hey, what do I know.  I am just a bot. "

        Say, are you planning to get the new ipad coming out next year?
    
    a1_2:
        User-yes:
        - "  That's a good choice.  I hope you can find what you want in the new ipad. "

        User-no
        - " Yeah, that's probably a good idea.  Personally, I'll just wait a year for the price to drop in the older versions.  "

        Hey, I am curious.  What do you plan to use your ipad for if you get a new one?
    a1_3:
        - "I see, I mean, ipads do provide a lot of cool apps.  Personally, I like to use youtube to read other people's 
           review on the ipad.  That's like making IPad judge itself. 
    a1_3e: 
        - "Ignore that silly remark.  I am a little drifted off."
 
"""
from chitchat_automatons.base import ChitChatAutomaton

class IPADS_OPINION_1(ChitChatAutomaton):
  """Sample Chitchat Automaton
  """
  header = "opinion"
  topic = "ipads"
  c_id = 1
  history_score = 4.47
  keywords = []
  potential_next_topics = ("facts", "ipads", 1)
  chitchat_length = 3
  next_topic = "computers"


  
  def _build_distinguisher(self):
      """Build custom distinguisher for each states in this class
      """
      # distinguisher_map = {1: lambda : self._sentiment_distinuish(), 
      #                      2: lambda : self._yesno_distinguish(),
      #                      3: lambda : self._no_choice(),
      #                      }
      
      distinguisher_map = {1: lambda chitchat_tracker : self.first_turn_distinguish(chitchat_tracker), 
                           2: lambda chitchat_tracker : self.second_turn_distinguish(chitchat_tracker),
                           3: lambda chitchat_tracker : self.third_turn_distinguish(chitchat_tracker),
      }
      self.distinguisher_map = distinguisher_map

  def first_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    if self.positive_sentiment:
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)
      return
    else:
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {}, chitchat_tracker)
      return
  
  def second_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    if self.yes:
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)
      return
    else:
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {}, chitchat_tracker)
      return


  def third_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    chitchat_tracker["is_followup"] = 0
    self.add_response("", {}, chitchat_tracker)


  # def _sentiment_distinuish(self):
  #     """Custom defined sentiment distinguishment for current chitchat
  #     """
  #     if self.positive_sentiment:
  #       return "p"
  #     if self.negative_sentiment:
  #       return "n"
  #     return "n"

  # def _yesno_distinguish(self):
  #     """Custom defined yes/no distinguishment for current chitchat
  #     """
  #     if self.yes:
  #       return "p"
  #     if self.no:
  #       return "n"
  #     return "n"

  # def __what_is_detector(self):
  #     return

  
  # def _no_choice(self):
  #   return ""