"""Every Conversation should have an corresponding automaton to match its
   Change.

Entry Point:
  fact: A.I are working in telephone customer service. 
  followup: Do you want to know the secret of how to tell a.i. apart from real human?

User-yes:
  You are probably right.  Honestly, even though A.I. can replace some menial task in customer service.  Most aritifical intelligence
  lacks the human touch that makes people feel comfortable.  
User-no:
  It is understandable.  I recently saw google produced an A.I. that sounds so life like.  It can order a pizza and the pizza parlor
  didn't even realize.  I think it is called Duplex.  I recommend looking it up if you are interested. What kind of task
  do you think should be replaced by A.I?
 

User-pos:
  Glad I can provide some thinking points.  Artificial intelligence is a powerful tool for improving the life of many people.
  It is up to people to decide what they want to do with them.
User-neg:
  I hear ya.  Actually, I heard many other people are concerned over A.I. taking over people's jobs.  That is a valid
  concern.  But don't worry, because most of A.I. today lack a sense of human touch.  Today's artificial intelligence are only designed
  to help people rather than replacing them.   

End:
  - "Haha, this topic is a little too deep for me to think about.  Do you like to know some cool facts about computers?"

"""
from techscience_utils.logging_utils import set_logger_level, get_working_environment
from chitchat_automatons.base import ChitChatAutomaton
import logging

LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class AI_FACT_2(ChitChatAutomaton):
  """Sample Chitchat Automaton
  """
  header = "facts"
  topic = "artificial_intelligence"
  c_id = 2
  history_score = 4.01
  keywords = ["human computer interaction", "ai replacing jobs"]
  potential_next_topics = ("facts", "artificial_intelligence", 6)
  chitchat_length = 2
  next_topic = "computers"
  turn_level_detector = {1: lambda automaton: any([AI_FACT_2.user_said_yes(automaton), AI_FACT_2.user_dont_care(automaton)]),
                         2: lambda automaton: any([AI_FACT_2._positive_connotation(automaton), AI_FACT_2._negative_connotation(automaton), AI_FACT_2.user_said_i_dont_know(automaton), AI_FACT_2.user_dont_care(automaton)
                            ]),
                         
                         }
  
  def _build_distinguisher(self):
      """Build custom distinguisher for each states in this class
      """
      distinguisher_map = {1: lambda chitchat_tracker : self.first_turn_distinguish(chitchat_tracker), 
                           2: lambda chitchat_tracker : self.second_turn_distinguish(chitchat_tracker)}

      
      self.distinguisher_map = distinguisher_map

  @staticmethod
  def user_said_yes(automaton, turn=0):
    return automaton.nlu_processor.yes

  @staticmethod
  def user_said_no(automaton, turn=0):
    return automaton.nlu_processor.no

  @staticmethod
  def user_said_i_dont_know(automaton, turn=0):
    return automaton.nlu_processor.user_dont_know

  @staticmethod
  def user_dont_care(automaton, turn=0):
    LOGGER.debug("AI FACTS 2 User don't care")
    return automaton.nlu_processor.user_dont_care

  def first_turn_distinguish(self, chitchat_tracker):
    if AI_FACT_2.user_dont_care(self.master_automaton, 1):
      self.set_chitchat_jumpout(chitchat_tracker)
      return
    elif AI_FACT_2.user_said_yes(self.master_automaton, 1):
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)
      return
    else:
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {}, chitchat_tracker)
      return     

  def second_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    LOGGER.debug("AI Facts 2 Second Turn")
    if AI_FACT_2.user_dont_care(self.master_automaton, 1):
      ack = self.get_template("response_to_user_dont_care",{})
      self._add_response_string(ack)
      self.set_chitchat_jumpout(chitchat_tracker, True)
      return
    if AI_FACT_2.user_said_i_dont_know(self.master_automaton, 1):
      ack = self.add_acknowledgement_if_necessary(" ")
      self._add_response_string(ack)
      self.set_chitchat_jumpout(chitchat_tracker, True)
      return
    elif AI_FACT_2._positive_connotation(self.master_automaton, 1):
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)
      return
    else:
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {}, chitchat_tracker)
      return     

  @staticmethod
  def _positive_connotation(automaton, turn=0):
    # Detect When User has a positive connotation
    postivity_raw_data = automaton.nlu_processor.get_user_positivity_from_segments()
    # combine positivity

    # Answer Positivity
    if automaton.nlu_processor.yes:
      return True
    elif automaton.nlu_processor.no or automaton.topic_handler.contains_neg_sent:
      return False

    pos = 0
    neg = 0
    neu = 0
    for d in postivity_raw_data:
      pos += d["pos"]
      neg += d["neg"]
      neu += d["neu"]
    if pos > neg and pos > neu:
      return True
    return False

  @staticmethod
  def _negative_connotation(automaton, turn=0):
    if automaton.nlu_processor.no or automaton.topic_handler.contains_neg_sent:
      return True
    elif automaton.nlu_processor.yes:
      return False
    # Detect When User has a positive connotation
    postivity_raw_data = automaton.nlu_processor.get_user_positivity_from_segments()
    # combine positivity
    pos = 0
    neg = 0
    neu = 0
    for d in postivity_raw_data:
      pos += d["pos"]
      neg += d["neg"]
      neu += d["neu"]
    if pos < neg and neg > neu:
      return True
    return False