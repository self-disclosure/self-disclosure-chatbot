"""Introduction Chitchat On Alexa

Start:
- I am kind of curious.  How did you get your alexa?

Next:
- That's nice.  Do you think Alexa is useful?

Positive:
- Awww, thank you.  We have a lot of improve.  I think many people want us to be more like Jarvis.  It is
kind of hard because not only we have to be very good at listening, we also need to learn sarcasm.  Right now, 
it is kind of hard for us to learn that.  Do you like artificial intelligence?

Nonpositive + long length:
- Well, thank you for your feedback, getting good criticism is helpful for improving ourselves.  What's your opinion on
artificial intelligence?

Nonpositive + short length:
- Yeah, we may never measure up to Jarvis.  But we are getting close.  Are you interested in Artificial Intelligence?

End
- What do you like the most about artificial intelligence?

"""

from chitchat_automatons.base import ChitChatAutomaton

class ALEXA_TREND_1(ChitChatAutomaton):
  """Sample Chitchat Automaton
  """
  header = "trend"
  topic = "alexa"
  c_id = 1
  history_score = 4.29
  keywords = []
  potential_next_topics = ("facts", "computers", 1) # Disabled
  chitchat_length = 3
  next_topic = "artificial_intelligence"
  
  def _build_distinguisher(self):
      """Build custom distinguisher for each states in this class
      """
      # distinguisher_map = {1: lambda : self._no_choice(),
      #                      2: lambda : self._no_choice(),
      #                      3: lambda: self._sentiment_length_distinuish(),
      #                      }
      distinguisher_map = {1: lambda chitchat_tracker : self.first_turn_distinguish(chitchat_tracker), 
                           2: lambda chitchat_tracker : self.second_turn_distinguish(chitchat_tracker),
                           3: lambda chitchat_tracker : self.third_turn_distinguish(chitchat_tracker),
      }
      
      self.distinguisher_map = distinguisher_map

  def first_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    chitchat_tracker["is_followup"] = 0
    self.add_response("", {}, chitchat_tracker)
  
  def second_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    chitchat_tracker["is_followup"] = 0
    self.add_response("", {}, chitchat_tracker)

  def third_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    if self.positive_sentiment:
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)
      return
    l = len(self.master_automaton.text.split(" ")) > self.topic_handler.LENGTH_THRESHOLD
    if l:
      chitchat_tracker["trace"] = "l"
      chitchat_tracker["is_followup"] = 0
      self.add_response("l", {}, chitchat_tracker)
      return
    chitchat_tracker["trace"] = "s"
    chitchat_tracker["is_followup"] = 0
    self.add_response("s", {}, chitchat_tracker)

  # def _no_choice(self):
  #   return ""

  # def _sentiment_length_distinuish(self):
  #     """Custom defined sentiment distinguishment for current chitchat
  #     """
  #     if self.positive_sentiment:
  #       return "p"
  #     l = len(self.master_automaton.text.split(" ")) > self.topic_handler.LENGTH_THRESHOLD
  #     if l:
  #       return "l"
  #     return "s"