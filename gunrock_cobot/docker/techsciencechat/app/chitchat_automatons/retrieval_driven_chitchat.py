"""Provide a Pool of States, Pick the Best State
"""
from chitchat_automatons.base import ChitChatAutomaton

class RetrievalDrivenChitchat(ChitChatAutomaton):
  """Sample Chitchat Automaton
  """
  header = "retrieval"
  topic = "all"
  c_id = 0
  history_score = 0.0

  def __init__(self, automaton):
      super(RetrievalDrivenChitchat, self).__init__(automaton)
      # Register State Pools
      self.state_pools = self.register_states()

  def transduce(self, chitchat_tracker):
      """RetrievalDrivenChitchat

      Parameters:
        - chitchat tracker
      Return:
        - updated chitchat tracker
      """
      return chitchat_tracker

#TODO: Make sure the interface fits the chitchat