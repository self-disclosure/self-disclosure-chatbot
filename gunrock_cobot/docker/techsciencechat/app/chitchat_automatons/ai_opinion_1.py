"""
    f1:
      - " I've also heard that some people are afraid of what A.I can do.
          <break time = \"150ms\"></break> I mean <break time = \"150ms\"></break> look at me. 
          I am afraid of speaking too loud and lower the IQ of the entire street."
    q1:
      - " Sooo... Do you like talking with the chatbots? "

    User-Positive:
      - " Awww, thank you for not disliking me.  Personally I am trying by best to improve myself.
          The thing is, it is hard to understand people sometimes.  We are trying our best to cope. "
    User-Negative:
      - " Haha, that's fair.  We do have a lot to improve.  I mean, we are trying.  And no, I am not
          recording what you are saying.  So don't worry about the spying thing.  The gist is technology
          is advanced enough that we can understand some of languages on the spot. "
    
    Common Next:
      - "Right, my opinion on artificial intelligence.  Hmmm, honestly I believe A.I is just a tool to make the
         world a better place.  Just like any tools, its impact depends on how people who use it, rather than
         artificial intelligence itself.  What do you think?"

    User-Agree
      - "Right?  The thing is, A.I. is a world changing technology.  Definitely not advanced enough to be called 
         intelligence, am I right?  But useful enough to change the world."
    User-No-Agree
      - "Ah, that's fair also.  The world is rapidly changing.  It is exciting, scary, but definitely moving forward.
         Something about that really excites me."

    Common Next:
      - "I see, I see.  Honestly, I am just overwhelmed by the excitement the future holds.  We are explorers of the unknown
         world.  It will be better, as long we we work to make it better.  Don't you think?"
 

End:  Thank you for talking with me about this.  I really like this conversation. At least I don't feel so bummed out, haha.
"""
from techscience_utils.logging_utils import set_logger_level, get_working_environment
from chitchat_automatons.base import ChitChatAutomaton
import logging

LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())
class AI_OPINION_1(ChitChatAutomaton):
  """Sample Chitchat Automaton
  """
  header = "opinion"
  topic = "artificial_intelligence"
  c_id = 1
  history_score = 4.0
  keywords = ["chatbots"]
  potential_next_topics = ("trend", "alexa", 1)
  chitchat_length = 4
  next_topic = "artificial_intelligence"

  turn_level_detector = {1: lambda automaton: any([AI_OPINION_1._positive_connotation(automaton),\
  AI_OPINION_1.user_dont_care(automaton)
                            ]),
                         2: lambda automaton: any([AI_OPINION_1.user_dont_care(automaton)
                            ]),
                         3: lambda automaton: any([AI_OPINION_1.user_dont_care(automaton)
                            ]),
                         4: lambda automaton: any([AI_OPINION_1.user_dont_care(automaton)
                            ])
                         
                         }
  
  def _build_distinguisher(self):
      """Build custom distinguisher for each states in this class
      """
      # distinguisher_map = {1: lambda : self._sentiment_distinuish(),
      #                      2:  lambda : self._no_choice(),
      #                      3: lambda : self._yesno_distinguish(),
      #                      4:  lambda : self._no_choice(),
      #                      }
      distinguisher_map = {1: lambda chitchat_tracker : self.first_turn_distinguish(chitchat_tracker), 
                           2: lambda chitchat_tracker : self.second_turn_distinguish(chitchat_tracker),
                           3: lambda chitchat_tracker : self.third_turn_distinguish(chitchat_tracker),
                           4: lambda chitchat_tracker : self.fourth_turn_distinguish(chitchat_tracker)}

      self.distinguisher_map = distinguisher_map

  def first_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    if AI_OPINION_1._positive_connotation(self.master_automaton):
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)
      return
    elif AI_OPINION_1.user_dont_care(self.master_automaton, 1):
      ack = self.generate_acknowledgement(" Sorry, I got a little carried away with the blabbering.  I don't know why, but I get too nervous when talking about myself. ")
      self._add_response_string(ack)
      self.set_chitchat_jumpout(chitchat_tracker, True)
      # Jump Out and ask what are you interested in? TODO
      return 
    else:
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {}, chitchat_tracker)
      return
  
  def second_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    acknowledgement = ""
    if AI_OPINION_1.user_dont_care(self.master_automaton, 1):
      ack = self.generate_acknowledgement(" Sorry, I got a little carried away with the blabbering.  I don't know why, but I get too nervous when talking about myself. ")
      self._add_response_string(ack)
      self.set_chitchat_jumpout(chitchat_tracker, True)
      # Jump Out and ask what are you interested in? TODO
      return     
    if AI_OPINION_1.user_compliment(self.master_automaton):
      acknowledgement = self.get_template("acknowledgement_user_compliment",{})
    LOGGER.debug("Adding Acknowledgement on second turn {}".format(acknowledgement))
    chitchat_tracker["is_followup"] = 0
    self.add_response("", {"acknowledgement": acknowledgement}, chitchat_tracker)

  def third_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    if AI_OPINION_1.user_dont_care(self.master_automaton, 1):
      ack = self.get_template("response_to_user_dont_care",{})
      ack = ack + self.generate_acknowledgement("  I don't know why, but I get too nervous when talking about myself. ")
      self._add_response_string(ack)
      self.set_chitchat_jumpout(chitchat_tracker, True)
      # Jump Out and ask what are you interested in? TODO
      return
    if AI_OPINION_1.user_said_yes(self.master_automaton):
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)
      return
    else:
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {}, chitchat_tracker)
      return

  def fourth_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    if AI_OPINION_1.user_dont_care(self.master_automaton, 1):
      ack = self.generate_acknowledgement(" Sorry, I got a little carried away with the blabbering.  I don't know why, but I get too nervous when talking about myself. ")
      self._add_response_string(ack)
      self.set_chitchat_jumpout(chitchat_tracker, True)
      # Jump Out and ask what are you interested in? TODO
      return
    chitchat_tracker["is_followup"] = 0
    self.add_response("", {}, chitchat_tracker)

  @staticmethod
  def user_compliment(automaton, turn=0):
    return "pos_sent" in automaton.nlu_processor.topic_intent and len(automaton.nlu_processor.user_utterance.split(" ")) > 3 #TODO: Add a function to detect long answers

  @staticmethod
  def _positive_connotation(automaton, turn=0):
    # Detect When User has a positive connotation
    postivity_raw_data = automaton.nlu_processor.get_user_positivity_from_segments()
    # combine positivity
    LOGGER.debug("Positivity Detected Topic Intent {0}, Lexical Intent {1} System Intent {2}".format(automaton.nlu_processor.topic_intent, automaton.nlu_processor.lexical_intent, automaton.nlu_processor.sys_intent))
    # Answer Positivity
    if automaton.nlu_processor.yes:
      return True
    elif automaton.nlu_processor.no or automaton.topic_handler.contains_neg_sent:
      return False

    pos = 0
    neg = 0
    neu = 0
    for d in postivity_raw_data:
      pos += d["pos"]
      neg += d["neg"]
      neu += d["neu"]
    if pos > neg and pos > neu:
      return True
    return False

  @staticmethod
  def _negative_connotation(automaton, turn=0):
    if automaton.nlu_processor.no or automaton.topic_handler.contains_neg_sent:
      return True
    elif automaton.nlu_processor.yes:
      return False
    # Detect When User has a positive connotation
    postivity_raw_data = automaton.nlu_processor.get_user_positivity_from_segments()
    # combine positivity
    pos = 0
    neg = 0
    neu = 0
    for d in postivity_raw_data:
      pos += d["pos"]
      neg += d["neg"]
      neu += d["neu"]
    if pos < neg and neg > neu:
      return True
    return False
  
  @staticmethod
  def user_said_yes(automaton, turn=0):
    return automaton.nlu_processor.yes

  @staticmethod
  def user_dont_care(automaton, turn=0):
    return automaton.nlu_processor.user_dont_care