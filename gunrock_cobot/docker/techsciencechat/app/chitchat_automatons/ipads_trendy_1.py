"""Trendy Chitchat Around IPads

By the way, have you heard about recent updates on the new feature updates from Apple?

User-Yes
- Hahaha, those features are pretty important.  I read in the news.  A toddler
in Washingotn DC locked his father out of his IPad, for 48 years.  I really hope the feature updates can address that issue.

User-No
- It's not some big news.  Basically the newest IOS update is introducing a 
systemwise Dark Mode.  There are some security updates, but most of the features won't really have an impact.

From what I've heard, next year, Apple is going to unveil 6 iphones next year.
Rumors also mentioned Apple is going to unveil new ipad model next year.

User-Positive
- I see your point

User-Negative/Nonchalant:
- That's cool.

Common Point:
- Hey, sometimes I wonder.  IPhones are promoting its larger screensize.  IPads promote smaller size.  Are there going to be a day when IPhone is going
to be larger than IPad?  Hahaha.

Common Point:
- Don't worry.  It is just a weird thought.  Are you interested in some cool facts on artificial intelligence?

"""

from chitchat_automatons.base import ChitChatAutomaton

class IPAD_TREND_1(ChitChatAutomaton):
  """Sample Chitchat Automaton
  """
  header = "trend"
  topic = "ipads"
  c_id = 1
  history_score = 4.00
  keywords = []
  potential_next_topics = ("opinion", "ipads", 1) # Disabled
  chitchat_length = 3
  next_topic = "artificial_intelligence"


  
  def _build_distinguisher(self):
      """Build custom distinguisher for each states in this class
      """
      turn_level_detector = {1: lambda automaton: any([False,]), \
                             2: lambda automaton: any([IPAD_TREND_1.user_said_yes(automaton)]),
                             3: lambda automaton: any([IPAD_TREND_1.user_positive_sent(automaton)])}
      distinguisher_map = {1: lambda chitchat_tracker : self.first_turn_distinguish(chitchat_tracker), 
                           2: lambda chitchat_tracker : self.second_turn_distinguish(chitchat_tracker),
                           3: lambda chitchat_tracker : self.third_turn_distinguish(chitchat_tracker),
                           }

      self.distinguisher_map = distinguisher_map

  def first_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    chitchat_tracker["is_followup"] = 0
    self.add_response("", {}, chitchat_tracker)
  
  def second_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    if IPAD_TREND_1.user_said_yes(self.master_automaton):
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)
      return
    else:
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {}, chitchat_tracker)
      return

  def third_turn_distinguish(self, chitchat_tracker):
    """Refactoring Only #TODO
    """
    if IPAD_TREND_1.user_positive_sent(self.master_automaton):
      chitchat_tracker["trace"] = "p"
      chitchat_tracker["is_followup"] = 0
      self.add_response("p", {}, chitchat_tracker)
      return
    else:
      chitchat_tracker["trace"] = "n"
      chitchat_tracker["is_followup"] = 0
      self.add_response("n", {}, chitchat_tracker)
      return



