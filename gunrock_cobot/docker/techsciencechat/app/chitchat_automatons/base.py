"""Base class for chitchat automatons
"""
import logging
import template_manager
from techscience_utils.logging_utils import set_logger_level, get_working_environment
from chitchat_automatons.states.base import BaseState as ChitchatState
from chitchat_automatons.transitions.base import BaseTransition as ChitchatTransition
from typing import Dict

TEMPLATE_TECHSCI = template_manager.Templates.techscience
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

class ChitChatAutomaton:
    history_score = 3.0
    turn_level_detector = {}
    
    def __init__(self, automaton):
        self.master_automaton = automaton
        self.chitchat_handler = self.master_automaton.get_chitchat_handler()
        self.is_complete = False
        self.is_jumpout = False
        return

    def _build_nlu_handler(self):
        """Build the intent_handler for the current FSM call.
        """
        self.nlu_processor = self.master_automaton.nlu_processor
        self.topic_handler = self.master_automaton.get_topic_handler()
        text = self.master_automaton.text
        # self.intent_handler = self.master_automaton.get_intent_handler(False)
        # self.sys_intent = self.intent_handler.detect_sys_intent(text)
        # self.lexical_intent = self.intent_handler.detect_lexical_intent(text)
        # self.topic_intent = self.intent_handler.detect_topic_intent(text)

    def transduce(self, chitchat_tracker):
        """Simple Automaton For Chitchat

        Parameters:
          - chitchat tracker: processed chitchat tracker from master automaton
        
        Return:
          - updated chitchat tracker
        """
        self._build_nlu_handler()
        self._build_distinguisher()
        self.acknowledgement_handler = self.master_automaton.get_acknowledgement_handler(True)
        # Get the current turn number, note the turn number is at previous state
        # So we will first check if the turn is at the limit, if so, we can jump to 
        # A different automaton if automaton is activated
        # If not, we can return a fact transition
        _header, _topic, _cid = chitchat_tracker.get("curr_state", (None, None, None))
        if self.c_id != _cid:
            raise EnvironmentError("Wrong Chitchat: correct_chitchat:{0} actual_chitchat{1}".format(_cid, self.c_id))

        # LOGGER.error("[TECHSCIENCE] Chitchat, Entered into Chitchat Base {}".format(chitchat_tracker.get("curr_state")))

        # If activated at a node, set chitchat tracker based on the transition
        # transition_node = self.chitchat_handler.get_node(chitchat_tracker)
        # chitchat_tracker = self.transit(transition_node, chitchat_tracker)

        # chitchat_tracker = self.check_next_state(chitchat_tracker)
        # # If activated at a node, transit to corresponding state
        # state_node = self.chitchat_handler.get_node(chitchat_tracker)
        # LOGGER.debug("Check Chitchat_tracker state: {}".format(chitchat_tracker))
        # if state_node:
        #     LOGGER.debug("Calling Chitchat State: {}".format(state_node))
        #     if self.call_state(state_node, chitchat_tracker):
        #         return chitchat_tracker


    
        # Get the turn number
        prepare_for_next_topic = False
        turn_number = self.chitchat_handler.get_current_turn(chitchat_tracker)

        # LOGGER.error("[TECHSCIENCE] Chitchat Turn Number {}".format(turn_number))
        # Check if turn is at end
        if turn_number >= self.chitchat_length:
            # add to completed
            current_state = (self.header, self.topic, self.c_id)
            completed_ids = chitchat_tracker.get("completed_ids", [])

            # Check if current state is already added to completed chitchats
            if current_state in completed_ids:
                selector = self.get_selector("e", turn_number)
                self.set_chitchat_jumpout(chitchat_tracker)
                self._add_response(selector, {})
                return chitchat_tracker
            else:
                prepare_for_next_topic = True
                completed_ids.append(current_state)
            chitchat_tracker["completed_ids"] = completed_ids
        else:
              # if next is activate, get next transduce
              # if not, use fact transition
          # If not, move to the next
            turn_number = self.chitchat_handler.get_next_turn(chitchat_tracker)

        self.distinguisher_map.get(turn_number, self.default_chitchat_exit)(chitchat_tracker)
        #  add to response
        if turn_number >= self.chitchat_length and chitchat_tracker["is_followup"] == 0:
            LOGGER.debug("CHITCHAT TRACKER: Setting the next automaton...")
            # get next automaton 
            activated = self.chitchat_handler.is_activated_chitchat(*self.potential_next_topics)
            completed = self.potential_next_topics in chitchat_tracker["completed_ids"]
            LOGGER.debug("CHITCHAT TRACKER: Potential Next Topics {0} is Activated {1} and not Completed {2}"\
            .format(self.potential_next_topics, activated, completed))
            if activated and not completed:
                chitchat_tracker["completed_ids"].append((self.header, self.topic, self.c_id))
                next_automaton = self.get_next_automaton(*self.potential_next_topics)
                chitchat_tracker = next_automaton.get_initial_state(chitchat_tracker)
                LOGGER.debug("Potential Next Topic: {}".format(self.potential_next_topics))
                LOGGER.debug("Get New Chitchat_tracker Selector: {}".format(chitchat_tracker))
            if not activated:
                chitchat_tracker["completed_ids"].append((self.header, self.topic, self.c_id))


        return chitchat_tracker

    def default_chitchat_exit(self, chitchat_tracker=None):
        #TODO
        return



    def add_response(self, selector_apx, slots, chitchat_tracker):
        selector = self.get_selector(selector_apx, chitchat_tracker.get("curr_turn"))
        self._add_response(selector, slots)

    def _add_response_string(self, string):
        self.master_automaton.response += string
        

    def get_initial_state(self, chitchat_tracker):
        """Initialze the State for the chitchat
        """
        chitchat_state = (self.header, self.topic, self.c_id)
        return self.chitchat_handler.initialize_chitchat(chitchat_tracker, chitchat_state, False)

    def get_selector(self, selector_apx, turn_number):
        selector_string = "{header}_chitchat/{topic}/a{c_id}_{turn_num}{apx}"
        selector = selector_string.format(header=self.header, 
                                          topic=self.topic, 
                                          c_id=self.c_id,
                                          turn_num=turn_number, apx=selector_apx)
        LOGGER.debug("Chitchat Selector: {}".format(selector))
        return selector

    def get_next_automaton(self, header, topic, _id):
        return self.master_automaton.get_chitchat_automaton(header, topic, _id)
    
    def _add_response(self, selector, slots):
        self._add_response_string(self.get_template(selector, slots))

    def get_template(self, selector, slots):
        return TEMPLATE_TECHSCI.utterance(
            selector=selector,
            slots=slots,
            user_attributes_ref=self.master_automaton.user_attributes_ref)

    def get_next_topic(self):
        """Interface for master automaton.
           If chitchat is completed, get_next_topic will jump to the next
           topic to chat about, else None
        """
        if self.is_complete:
            return self.next_topic
        return None

    @property
    def yes(self)->bool:
        """Detect when user says yes.

        """
        return self.nlu_processor.yes

    @property
    def ask_more(self):
        """Detects when user demands more
        """
        return self.nlu_processor.user_ask_more

    @property
    def positive_sentiment(self):
        """
        """
        # ans_pos = "ans_pos" in self.lexical_intent
        # ans_like = "ans_like" in self.lexical_intent
        # pos_sent = "pos_sent" in self.topic_intent

        return self.nlu_processor.contains_pos_sent
        # return ans_pos or ans_like or pos_sent or t_pos_sent

    @staticmethod
    def user_said_yes(automaton):
        return automaton.nlu_processor.yes

    @staticmethod
    def user_positive_sent(automaton):
        return automaton.nlu_processor.contains_pos_sent

    def set_chitchat_jumpout(self, chitchat_tracker, jumpout=False):
        self.is_complete = True
        self.is_jumpout = jumpout
        chitchat_tracker["curr_turn"] = -1
        chitchat_tracker["curr_state"] = self.chitchat_handler.null_chitchat_state
        return 

    @property
    def no(self)->bool:
        """User Explicitly answer no
        """
        return self.nlu_processor.no

    @property
    def negative_sentiment(self):
        """User Display negative sentiment
        """
        # return self.nlu_processor.contains_neg_sent TODO
        return False
        
    def _no_choice(self):
        return ""

    @property
    def user_dont_know(self):
        return self.nlu_processor.user_dont_know
        # return "user_dont_know" in self.topic_intent or "ans_unknown" in self.lexical_intent

    def call_state(self, curr_node, chitchat_tracker):
        """Current Node 
        """
        for subclasses in ChitchatState.__subclasses__():
            if subclasses.name == curr_node:
                LOGGER.debug("Calling State: {}".format(curr_node))
                next_state = subclasses(self, chitchat_tracker)()
                chitchat_tracker["curr_node"] = next_state
                return True
        return False

    def transit(self, curr_node, chitchat_tracker):
        # Get state from node
        # if state doesn't exist, then return None
        # Else set state by transition
        next_state = None
        for subclasses in ChitchatTransition.__subclasses__():
            if subclasses.name == curr_node:
                next_state = subclasses(self, chitchat_tracker)()
                break
        chitchat_tracker["curr_node"] = next_state
        return chitchat_tracker

    def check_next_state(self, chitchat_tracker):
        """If detect freeformed question, move to defined state
        Deterministic choose from rule
        """
        # if self.detect_question:
        #     chitchat_tracker["curr_node"] = "s_qa"
        return chitchat_tracker

    def register_states(self):
        return {}

    def jumpout_chitchat(self):
        """Check if jump out of current chitchat is needed.
        We can add one 
        """
        # Detect User Complaint
            # Specific Detect Are you listening to me?

            # Specificly Detect, Are you stupid?

            # Specifically Detect, you sound so **.

        
        return

    def generate_acknowledgement(self, original_response):
        """Add Acknowledgement if Necessary
        TODO: Check for long repsonse
        """
        if self.acknowledgement_handler.ack_available():
            return " {0} {1} ".format(self.acknowledgement_handler.get_ack, original_response)

        new_response = self.jumpout_chitchat()
        if new_response is not None:
            return new_response
        return original_response