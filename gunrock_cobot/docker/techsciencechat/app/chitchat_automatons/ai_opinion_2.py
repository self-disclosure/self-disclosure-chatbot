"""Opinion Chitchat
    f1:
      - " Even though I feel good about artificial intelligence today, I do have some concerns with deep fake technology.
          The part that bugs me the most is deep fake, a computer program, can create videos of people speaking words they never spoke. 
      "
    q1:
      - " What's your opinion on that? "

    Common:
      - " Right?  But the good news is, scientists are working on a way to detect deep fake videos.  I heard they made some good progress on
          that.  But for now, I won't trust online videos that easily.  "

    Common2:
      - " On a brigther note, I also think deep fake would be a great tool to change the ending of the movies you don't like. Think about it,
          you can rewrite ending to Game of Thrones ANYWAY you want. "

"""