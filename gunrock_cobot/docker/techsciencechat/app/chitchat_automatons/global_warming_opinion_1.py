"""Global warming opinion

Entry Point:
  opinion:  A lot of online product recommendations are made by A.I
  followup:  Do you think they're doing a good job?

User-yes:
  I agree.  Though I do think in some cases it is creepy how they know what I want at that exact moment, when I am browsing through the internet.
User-no:
  Haha, perhaps there is something we could improve upon

Common Point:
  - Actually, the better question to ask is, do you think online product recommendations poses an intrusion of privacy?

User-yes: (Agree)
  - "Hmm, you do make a good point.  Online recommendation is pretty helpful.  My suggestion is to let the users know what data about them are actually collected.  
     This can give people a sense of control."
User-neg-sentiment:  
  - "Perhaps not, I also heard today artificial intelligence is not as all powerful as publicized.  I am pretty sure the majority of the work is just a large 
     data collection or data mining"

Common Point:
  - I am curious.  Do you like artificial intelligence in your life?
"""