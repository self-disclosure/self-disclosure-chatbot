"""
    f1:
      - "Yeah... isn't it so crazy that almost everything in the universe is made up of the elements on the periodic table?"
    q1:
      - " Do you have a favorite element? "
"""

"""Every Conversation should have an corresponding automaton to match its
   Change.

Entry Point:
  fact:  "Yeah... isn't it so crazy that almost everything in the universe is made up of the elements on the periodic table?"
  followup:  Do you have a favorite element?

User-pos:
  - That's nice.  My favorite element is Helium.  Do you want to know why?
User-neg:
  - That's alright, I can tell you my favorite element.  Can you guess why?

User-identify-cool:
  - 

Else
  - "Because it is the coolest element.  Get it?  haha"

- I don't get it, no, how so, why?
  - Because, helium has the lowest boiling point of all elements.  

Hey, you know what.  

User-yes: (Agree)
  - "Good choice.  Just imagine having a phone that is not waterproof, and next to a body of water.
     burrr, thinking of it gives me chills."
User-no/unsure:
  - "I heard Huawei P30 Pro, Samsung Galaxy Note 9, and IPhone XR are the best rated water proof smartphones according
         to tech authority.  The rating is based on waterproof IP.  That's all what I can remember."

*Wildcard:
What is {water_proof_ip}:
- "I am glad you asked.  Water proof IP, IP standands for Ingress Protection. Rated 0 is no protection, meaning
         the phone is easily damaged by water, 10 means it can overcome high pressured water jets.  That's all I know for now.
         Though I do think it is nerve wracking to have a phone that can be easily damaged by water."
 
*Exit:  Next topic: trendy smart phones
"""