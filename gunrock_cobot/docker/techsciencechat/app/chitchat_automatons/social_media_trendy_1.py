"""Cybertruck trendy

Q: Hey, by the way, do you have an instagram?
A: I don’t really know much about it.
Q: Oh, ok. The Cybertruck is Tesla’s new electric pickup truck. It’s pretty crazy looking. Do you think you’d buy an electric pickup?
A: Sure, I’d love to have an electric car.
Q: Cool. And actually, some people say that overall the Cybertruck is cheaper than a Ford F150. Have you seen a picture of a Cybertruck?
A: No, I haven’t
Q: They’re pretty weird looking. They kinda look like a truck version of a DeLorean from Back to the Future.
"""

"""Every Conversation should have an corresponding automaton to match its
   Change.

Entry Point:
  fact:  Hey, by the way, do you have an instagram?

User-pos:
  That's fair. The truck actually makes me miss Lightning McQueen. <kachow> Say, do you want to try driving it?
User-dontknow:
  Oh, ok. The Cybertruck is Tesla’s new electric pickup truck. It’s pretty crazy looking. Do you think you’d buy an electric pickup?
User-neg:
  Hehehe, <break>   I mean, dude, I really feel bad for tesla.  <bam>, there goes the company's stock price.  But, there are a lot of people
  preordering the car, do you think you would want it?

User-yes: (Agree)
  - "Cool. I would want it if I could drive a car.  And actually, some people say that overall the Cybertruck is cheaper than a Ford F150. Have you seen a picture of a Cybertruck?"
User-no:
  - "Haha, that's okay.  The car does seem cool, but it is also wise to see how it performs before buying yet.  Say, have you seen an picture of the car?"

User-yes/User-no
  - "I think it looks pretty cool. They kinda look like a truck version of a DeLorean from Back to the Future."

End:
  - "I really like talking with you about this.  Now I have some clear picture what people actually want in an car"
 
*Next ChitChat Intro:
(What type of car do you drive?)
"""