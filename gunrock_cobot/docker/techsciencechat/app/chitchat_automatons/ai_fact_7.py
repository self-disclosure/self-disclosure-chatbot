"""AI Fact 7

Fact:
  - "Nautilis is an A.I, that can kind of predict the future. Although the results are not entirely accurate,
         the results come close."

Followup:
  - "That's pretty amazing, right?"

User-pos:
  - "It is pretty cool.  So, what they actually did was just read the news from twitter or online newspapers.  And
     somehow that A.I can predict the location of certain events.  What do you think?"
User-neg
  - "haha, I think it can be a little exaggerated.  The scope of future prediction problem is pretty huge.  I mean, if
     this is possible.  We won't need social scientists anymore, am i right?  We only need social engineers.
    "

To be honest, I don't know how they did it.  I mean, if we know the details about how it works, then we can get the
11 million dollars of valuation Natuilis received last year.  Am I right?


Haha, although this seems cool.  It is probably a fad.  I read some of its articles.  Basically, they claim they can
predict the future based on the historical data.  It is like claiming you can ace the exam because you aced the practice
exam.  It is possible, sure.  But, saying you can predict the future?  That is a little too much.



"""