"""Every Conversation should have an corresponding automaton to match its
   Change.

Entry Point:
    facts:
        - " Virtual Assitant, or Chatbots are very popular today.  There are millions of chatbots already created on Facebook, Amazon, Google,
        and other large technology companies.  By the year 2029, some predict Chatbots will be indistinguishable from humans.          
         "
    followup:
        - " Do you want to know how this chatbot is made? "
    a3_1
      - I am so glad you are interested.  Let me tell you a little about myself. There are a lot of advancements in artificial intelligence
        research.  Scientists at OpenAI and Google can build systems that read and maybe understand text.  We grew from that type of technology.  We want to talk as much as possible with 
        people as possible.  Just like dogs learning to heed human commands.  With enough speech, we can generall find a good pattern to converse with humans.  Do you like chatting with us?
        What do you like the most it?  Or What do you hate the most about? So we can work on it and improve.

    a3_2p + Long Length:
      - Wow, that's a lot of feedbacks.  Thank you for all of your suggestions.  Personally, I believe the most important thing for us is to learn.  Learning makes me happy.  

    a3_n:
      - "Aw, that's okay.  I am glad to talk with you either way.  So, back to the concept of robots talking with humans.  What do you expect from your artificial intelligence assistant?

    a3_3
    - I see.  Actually, a lot of the problems with improving a.i. assistnats lie with engineering.  Then, another realm of problems lie with design.  We hope while learning how to talk
      with you guys. We will provide a good companionship if you don't want to talk with other people.  And don't worry.  We do listen, but we don't remember in long term.  So feel free
      to talk with us.  We will try to learn how to understand the language.  Do you have more questions about how this chatbot works?

    a3_4p:
    -  "That's a good question, I'll think about how to answer them. Say, you do seem like an inquisitive person.  Do you want to talk more about artificial intelligence or computers?"

    a3_4n:
    - "Haha, that's okay.  Say, you do seem like an inquisitive person.  Do you want to talk more about artificial intelligence?
"""