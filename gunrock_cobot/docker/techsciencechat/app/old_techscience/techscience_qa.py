import requests
import json
import redis
import logging
import hashlib
import re
from backstory_client import get_backstory_response

from cobot_common.service_client import get_client

COBOT_API_KEY = 'xgTjk23SRM9Q9VFrUEFOjav0Bn4ot21W8uFLEieT'
EVI_KEY = "gunrock:api:evi"

rds = redis.StrictRedis(host='52.87.136.90', port=16517,
                        socket_timeout=3,
                        socket_connect_timeout=1,
                        retry_on_timeout=True,
                        db=0, password="alexaprize", decode_responses=True)


# QA resources:
# Hardcoded logic
# Backstory
# EVI
# TODO: Define arbitrary order for calling these resources based on state
# TODO: Give flexibility, can return just response or also next state
# TODO: Option to toggle which response generators to use
class QABot:
    def __init__(self):
        pass
        # self.logger = app_logger
        # self.__bind_states()
        # self.__bind_transitions()

    def get_answer(self, utterance, a_b_test):
        # right now just pipeline is just Backstory -> EVI
        backstory_response = get_backstory_response(utterance, a_b_test)

        if backstory_response['confidence'] > 0.89:
            return backstory_response['text']
        else:
            evi_response = self.get_evi_response(utterance)
            if evi_response and re.search(
                    r"an opinion on that|I didn’t get that.|Sorry, I didn’t catch that|"
                    r"Alexa Original|'favorite' is usually defined|is a plural form of|Hi there|Hi|"
                    r"Sorry, I can’t find the answer to the question I heard.|I don't have an opinion on that.|"
                    r"You could ask me about music or geography.|You can ask me anything you like.|"
                    r"I can answer questions about people, places and more.|^As a|I didn't get that|"
                    r"'Mind' is usually defined|The Place I Belong, the movie released|"
                    r"'Talking' is usually defined|usually defined as plural of what|sound is a vibration|"
                    r"something that would happen|Say is a town in southwest Niger|tough to explain", evi_response):
                evi_response = None

            if evi_response:
                return evi_response
            elif backstory_response['confidence'] > 0.55:
                return backstory_response['text']
        return None


    def get_evi_response(self, utterance):
        evi_cache_res = get_redis(EVI_KEY, utterance.lower())
        if evi_cache_res is not None:
            return evi_cache_res
        try:
            client = get_client(api_key=COBOT_API_KEY)
            r = client.get_answer(question=utterance, timeout_in_millis=1000)
            if (r["response"] == ""
                    or r["response"].startswith('skill://')
                    or r["response"] in evi_filter_list):
                return None
            res = r["response"] + " "
            set_redis_with_expire(EVI_KEY, utterance.lower(), res,
                                  expire=3 * 30 * 86400)
            return res
        except Exception as e:
            print("[techscience_qa.get_evi_response] Exception: {}".format(
                repr(e)))
            return None


evi_filter_list = {
    "Sorry, I can’t find the answer to the question I heard.",
    "I don't have an opinion on that.",
    "You could ask me about music or geography.",
    "You can ask me anything you like.",
    "I can answer questions about people, places and more.",
}


def set_redis_with_expire(prefix, input_str, output, expire=24 * 60 * 60):
    logging.info(
        '[REDIS] set redis with expire input: {} output: {}, expire {}'.format(
            input_str, output, expire))
    hinput = hashlib.md5(input_str.encode()).hexdigest()
    return rds.set(prefix + ':' + hinput, json.dumps(output),
                   nx=True, ex=expire)


def get_redis(prefix, input_str):
    logging.info('[REDIS] get redis  input: {}'.format(input_str))
    hinput = hashlib.md5(input_str.encode()).hexdigest()
    results = rds.get(prefix + ':' + hinput)
    if results is None:
        return None
    return json.loads(results)
