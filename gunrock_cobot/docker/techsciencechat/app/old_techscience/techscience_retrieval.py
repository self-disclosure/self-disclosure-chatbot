import re
import random
import praw
import logging
import json
import hashlib
import redis

RANDOM_THOUGHT_KEY = "gunrock:randomthoughts"
HOLIDAY_KEY = "gunrock:ic:holiday:ts"
HOLIDAY_PREFIX_KEY = "gunrock:ic:holiday:"
COMMON_NAME_KEY = "gunrock:name:set"
RETRIEVAL_LIST_KEY = "gunrock:ic:retrieval:apikeys"

FIVETHREEEIGHT_KEY_PREFIX = "gunrock:ic:538:sig:"
TEDTALK_TOPIC_PREFIX = "gunrock:ic:tedtalk:topic:"

r = redis.StrictRedis(host='52.87.136.90', port=16517,
                      socket_timeout=3,
                      socket_connect_timeout=1,
                      retry_on_timeout=True,
                      db=0, password="alexaprize", decode_responses=True)


def get_retrieval_keys():
    return r.brpoplpush(RETRIEVAL_LIST_KEY, RETRIEVAL_LIST_KEY)


def set_redis_with_expire(prefix, input_str, output, expire=24 * 60 * 60):
    logging.info(
        '[REDIS] set redis with expire input: {} output: {}, expire {}'.format(
            input_str, output, expire))
    hinput = hashlib.md5(input_str.encode()).hexdigest()
    return r.set(prefix + ':' + hinput, json.dumps(output), nx=True, ex=expire)


def get_redis(prefix, input_str):
    logging.info('[REDIS] get redis  input: {}'.format(input_str))
    hinput = hashlib.md5(input_str.encode()).hexdigest()
    results = r.get(prefix + ':' + hinput)
    if results is None:
        return None
    return json.loads(results)


class TopicRetrievalResponseGenerator():
    KEYS = get_retrieval_keys()
    N_TITLES = 4
    N_COMMENTS = 1

    SENT_LEN_LIMIT = 25
    TITLE_LEN_LIMIT = 25

    # TIL_TITLE_TEMPLATE = ["that's an interesting topic. I learned that: ", "did you know that: ", "i heard that: "]
    # this new title works for TIL and showerthoughts
    TIL_TITLE_TEMPLATE = ["that is an interesting topic. You know, the first thing that comes to my mind is that: ", "that is an interesting topic. That reminds me that: ", " My friend had a lot to say the other day. She told me that: "]
    TIL_COMMENT_TEMPLATE = [" , it got me think that: ", " , and, my friend commented on it, ", " , and my friend quoted: "]
    TITLE_TEMPLATE = ["some people asked me this, ", "the other day i was thinking about this, ", "sometimes i wonder about that, "]
    COMMENT_TEMPLATE = [" , and i answered that ", " , and i thought that ", " , and i'm thinking that "]
    NO_RETURN_TEMPLATE = ["<say-as interpret-as=\"interjection\">Hmm</say-as>! what do you mean? i guess i never thought about this before", "Seems like you’re really interested in that topic. Can you tell me more about it?",
                        "<say-as interpret-as=\"interjection\">Uh oh</say-as>! I’m not familiar with that. Could you share more with me?"]
    TOO_MUCH_TO_SAY_TEMPLATE = ["<say-as interpret-as=\"interjection\">Oh my</say-as>! i have a lot of thoughts on this, but it’s not gonna be easy to talk about it now unless you are free until tomorrow night. maybe we should talk about something else",
                        "I could write a novel with all the things I have to say about that, but I can’t condense my thoughts and I don’t want to bore you! Let’s talk about something else",
                        "<say-as interpret-as=\"interjection\">Oof</say-as>! My brain is overloading with all my thoughts I have to share about that. Can we revisit this topic another time?"]
    TOO_LITTLE_TO_SAY_TEMPLATE = ["well you know what i want to say. it's so obvious. maybe we should talk about something more interesting"]
    NO_BODY_IN_COMMENT_TEMPLATE = ["it's a really hard question. I still can't figure it out. What do you think", "That's a tough question. I need more time to think about it some more. What are your thoughts?",
                        "<say-as interpret-as=\"interjection\">Uh oh</say-as>! I'm drawing a blank and need more time to learn about that! Can you tell me more about it?"]
    # cobot profanity_check can give a 2 seconds latency, so used a list instead
    BLACKLIST = ["ass", "asshole", "assholes", "anal", "anus", "arsehole", "arse", "bitch", "bangbros", "bastards", "bastard",
                "bitch", "tits", "butt", "blow job", "blowjob", "boob", "bra", "cock", "cum", "cunt", "dick", "shit",
                "sex", "erotic", "fuck", "fxxk", "f\*\*k", "f\*\*c", "f\*ck", "fcuk", "fucken", "gang bang", "gangbang",
                "genital", "damn", "damnit", "horny", "jerk off", "jerkoff", "kill", "loser", "masterbate", "naked",
                "nasty", "negro", "nigger", "nigga", "orgy", "pussy", "penis", "perve", "rape", "fucked",
                "fucking", "trump", "racist", "sexy", "strip club", "vagina", "faggot", "fag", "drug", "weed", "marijuana", "cannabis", "lsd",
                "kill", "murder", "incel", "prostitute", "slut", "whore", "hooker", "cuck", "cuckold", "shooting", "nazis",
                "nazism", "nazi", "sgw", "balls", "thot", "virgin", "virginity", "cumshot", "cumming", "dildo", "vibrator", "sexual",
                "douche", "douche", "bukkake", "douchebag", "porn", "porngraph", "porngraphy", "died", "killed",
                "killing", "murdered", "murdering", "kills", "murders", "clit", "clitoris", "masterbate tatoo", "cheat",
                "cheated", "cheating", "dammit", "toxicity", "toxic", "wtf", "breasts", "tits", "boobies", "dead", "sperm",
                "death", "pornhub", "intercourse", "pornography", "porno", "boobs", "penisis", "my husband", "my boyfriend",
                "my girlfriend", "my wife", "end my life", "reddit", "reddit,", "reddit:", "subreddit", "tifu", "post raw", "show discussion thread"]

    REQ_TOPIC_JUMP_PATTERN = r"(\blet's\b|\blet us\b|\blets\b|\bwe can\b)\s*(talk|chat|keep talking).*about|(\bi\b|\bwe\b)\s*(want to|expect to|like to|would like to|wanna|want)\s*(talk|chat|ask you|ask|know|keep talking) about|(\bhow about\b|\bcan we\b|\bcan you\b|\bcould we\b|i'd like to)\s*(talk|chat) about|why (dont|don't) we (talk|chat) about|do you know anything about|do you know|i (like|love)|tell me.*about|tell me|how about|what about|what is it about|my favorite topic is|talk to me about|i'd (like|love) to talk about|talk about|have you heard about|want to know|you (are|were) talking about"
    OPINION_PATTERN = r"^what do you think (about|of)|how do you think that|why do you think that|how do you think|why do you think|what do you like about|what do you love about|what makes you|how do you feel about|^how is |^how was|how do you like|why do you (like|love)|what kind of| your opinion (of|about|concerning|regarding|on)|your view (of|about|concerning|regarding|on)|your thoughts (of|about|concerning|regarding|on)|do you know (anything|something) about|(who|what) do you think|do you believe in|do you believe that|do you believe|what.* best|who.* best|which.* best|which \w+ are you|what.*it like|would you (like to|wanna) talk about|would you (like to|wanna)|do you (love|like)"
    WRONG_PATTERN = r"why.*did you bring|(why|how).*relevant|(what|why|how).*have to|what.*mean|make.*no.*sense|makes.*no.*sense|(why|how|(in what way)).*(related|relevant)|nothing to do with|why.*(did|do) you say|why are you telling|(does not|doesn't) make sense|not making sense|(you're|you are) talking about|i (don't|didn't|did not) get"
    REMOVE_LIST = [r"https\S+", r"http\S+", r"\btil\b", r"tldr", r"tl:dr", r"tl dr", r"tl;dr", r"nbsp", r"HTTPS\S+", r"HTTP\S+", r"TIL", r"TLDR", r"TL:DR", r"TL DR", r"TL;DR", r"NBSP", r"Https\S+", r"Http\S+", r"Til", r"Tldr", r"Tl:dr", r"Tl dr", r"Tl;dr", r"Nbsp"]
    SPEC_CHAR = "#&\()*+/:;<=>@[\\]'^_`{|}~"
    NEG_PATTERN = r"(not|isn't|wasn't).*(cool|interesting|interested|funny|a good)|sad|weird|disgusting|boring|bored|horrible|terrible|messed up|not interested|terrible|awful|pathetic|silly|unpleasant|bad|you are.*mean"
    POS_PATTERN = r"(that|that's|.*).*(cool|interesting|awesome|neat|sweet|fascinating|surprising|fantastic|crazy|amazing|true|fair|nice|smart|clever|great|good|love it|a good one)|wow|really|i agree"
    FUNNY_PATTERN = r"(that|that's|.*).*(funny|hilarious)"
    OKAY_PATTERN = r"okay|yes|yeah|sure|oh|right|i see"
    NO_PATTERN = r"no|next"
    IDK_PATTERN = r"(did not|do not|didn't|don't) know|no i didn't"
    IK_PATTERN = r"(knew|(did know)|(heard about))|me too"
    MY_NAME_PATTERN= r"my name"
    THANK_YOU_PATTERN = r"thank you|thanks"
    KNOW_MY_FRIEND_PATTERN = r"know.*(husband|wife|son|daughter|friend|mom|dad|boyfriend|girlfriend)"
    BROKEN_PATTERN = r"you are broken"

    STUPID_PATTERN = r"you are ((not.*(smart|good))|stupid)|don't.*like you"
    YOU_CREATOR_PATTERN = r"your mom|you creators"
    YOU_MOM_PATTERN = r"(your|yo|you) (mom|mother|mama)"
    DONE_PATTERN = r"back to work|sleep|done talking|i'm done"
    FRIEND_PATTERN = r"are we friends|have (a friend|friends)|be friends"
    HOW_DID_YOU_PATTERN = r"how did you"
    KEEP_CHATTING_PATTERN = r"keep (chatting|talking|going)|let's hear|something (with|within|about|on|of) the topic|continue"
    WAKEUP_PATTERN = r"wake up"
    NEW_NAME_PATTERN = r"rename you|your new name|call you|change your.*name"
    CURSE_PATTERN = r"don't curse|inappropriate"
    GENERAL_COMMAND_PATTERN = ["let's chat", "let's talk"]
    ALEXA_PATTERN = ["you"]


    EXPIRE_TIME = 7 * 24 * 60 * 60

    PREFIX = "gunrock:retrieval:data"

    def execute(self, input_data):
        return self._query(input_data)

    def _query(self, utterance, sub=None):

        topic_keyword = ""
        req_strong = False
        t_template = self.TITLE_TEMPLATE
        c_template = self.COMMENT_TEMPLATE

        # history
        history_turns = False
        topic_keyword = ""
        noun_phrase = []
        topicClass = ""
        skip_comment = False
        do_coref = False

        try:
            if history_turns:
                if "features" in utterance and "topic" in utterance["features"][0] and utterance["features"][0]["topic"] != None and len(utterance["features"][0]["topic"][0]["topicKeywords"]) > 0:
                    topic_keyword = utterance["features"][0]["topic"][0]["topicKeywords"][0]["keyword"]
                    if topic_keyword is None:
                        topic_keyword = ""

                if "features" in utterance and "intent_classify" in utterance["features"][0] and "sys" in utterance["features"][0]["intent_classify"]:
                    if "req_topic_jump" in utterance["features"][0]["intent_classify"]["sys"]:
                        req_strong = True

                if "features" in utterance and "topic" in utterance["features"][0] and utterance["features"][0]["topic"] != None and "topicClass" in utterance["features"][0]["topic"][0]:
                    topicClass = utterance["features"][0]["topic"][0]["topicClass"]

                if "features" in utterance and "noun_phrase" in utterance["features"][0]:
                    noun_phrase = utterance["features"][0]["noun_phrase"]

                if "slots" in utterance and "text" in utterance["slots"][0]:
                    utterance = utterance["slots"][0]["text"]["value"]
                else:
                    utterance = utterance["text"][0]
            else:
                if "features" in utterance and "topic" in utterance["features"] and utterance["features"]["topic"] != None and len(utterance["features"]["topic"][0]["topicKeywords"]) > 0:
                    topic_keyword = utterance["features"]["topic"][0]["topicKeywords"][0]["keyword"]
                    if topic_keyword is None:
                        topic_keyword = ""

                if "features" in utterance and "intent_classify" in utterance["features"] and "sys" in utterance["features"]["intent_classify"]:
                    if "req_topic_jump" in utterance["features"]["intent_classify"]["sys"]:
                        req_strong = True

                if "features" in utterance and "topic" in utterance["features"] and utterance["features"]["topic"] != None and "topicClass" in utterance["features"]["topic"][0]:
                    topicClass = utterance["features"]["topic"][0]["topicClass"]

                if "features" in utterance and "noun_phrase" in utterance["features"]:
                    noun_phrase = utterance["features"]["noun_phrase"]

                if "features" in utterance and "coreference" in utterance["features"] and utterance["features"]["coreference"] != None:
                    coref_text = utterance["features"]["coreference"]["text"]
                    if coref_text != "" and coref_text != " ":
                        utterance = coref_text
                        do_coref = True

                if "slots" in utterance and "text" in utterance["slots"]:
                    utterance = utterance["slots"]["text"]["value"]
                else:
                    utterance = utterance["text"]

                # for interactive mode where "open gunrock and" is appended to the utterance
                utterance = re.sub("open gunrock and", "", utterance).strip()
                utterance = re.sub("please", "", utterance).strip()



        except:
            pass

        # handling short sentence requests (error handling)
        error_respond = ""
        ignore_list = ["yes", "no", "wow", "that", "this", "cool", "you", "yeah", "ya", "it", "sorry", "please"]
        propose = False
        general_clarification = ["what's that? can you clarify that for me? ", "<say-as interpret-as=\"interjection\">darn</say-as>! i didn't catch that. could you rephrase that for me? ",
                        "<say-as interpret-as=\"interjection\">oh dear</say-as>! I didn't hear your response clearly. Maybe you could rephrase that? "]
        what_clarification = ["<say-as interpret-as=\"interjection\">just kidding</say-as>! what were we talking about again? ", "<say-as interpret-as=\"interjection\">ruh roh</say-as>! never mind then. What should we talk about next? ",
                        "<say-as interpret-as=\"interjection\">Whoops a daisy</say-as>! Okay, let's move on. What's on your mind? "]
        pos_acknowledgement = ["<say-as interpret-as=\"interjection\">Booya</say-as>! I know, right? ", "<say-as interpret-as=\"interjection\">Righto</say-as>! I thought that was cool as well! I'm glad you liked it ", "<say-as interpret-as=\"interjection\">Uh huh</say-as>! yeah, right? "]
        funny_acknowledgement = ["<say-as interpret-as=\"interjection\">Tee hee</say-as>! I LOL'd at that as well ", "<say-as interpret-as=\"interjection\">Tee hee</say-as>! If I could giggle, i would! ", "<say-as interpret-as=\"interjection\">Woo hoo</say-as>! I'm glad you get my awesome humor! "]
        neg_acknowledgement = ["<say-as interpret-as=\"interjection\">tsk tsk</say-as>! I'm sory that brought the mood down. I'll do better next time. ", "<say-as interpret-as=\"interjection\">Oh dear</say-as>! thanks for the feedback. I'll share something better next time. ",
                             "<say-as interpret-as=\"interjection\">Ruh roh</say-as>! I'm sorry you didn't like that. I'm still learning, so thanks for letting me know "]
        okay_acknowledgement = ["<say-as interpret-as=\"interjection\">Aha</say-as>! Sounds good to me! ", "<say-as interpret-as=\"interjection\">Righto</say-as>! ", "<say-as interpret-as=\"interjection\">Uh huh</say-as>! Okay, I see. "]
        no_acknowledgement = ["<say-as interpret-as=\"interjection\">Oh dear</say-as>! that's fine. Never mind then. ", "Sure thing, Moving on then. ", "Understood. ", "<say-as interpret-as=\"interjection\">shucks</say-as>! Okay, let's switch topics. "]
        idk_acknowledgement = ["Happy to share knowledge! ", "That's understandable; you're not plugged into the internet 24/7. ", "I'm glad we both learned new things today! "]
        ik_acknowledgement = ["<say-as interpret-as=\"interjection\">okey dokey</say-as>! I'm glad we're on the same page then! ", "<say-as interpret-as=\"interjection\">no way</say-as>! Great minds must think alike! What did you think about it when you first heard it? ", "<say-as interpret-as=\"interjection\">well well</say-as>, of course you already knew that: you are pretty smart! "]
        me_talk = ["<say-as interpret-as=\"interjection\">you bet</say-as>! I do enjoy learning more about you! ", "<say-as interpret-as=\"interjection\">all righty</say-as>! Tell me something interesting ", "<say-as interpret-as=\"interjection\">ooh la la</say-as>! I'd be happy to chat. what do you want to say? "]
        you_are_broken = ["<say-as interpret-as=\"interjection\">wah wah,</say-as> the only thing broken is my heart. "]

        stupid_response = ["Stupid is as stupid does, says Forrest Gump. ", "That's not very nice. I'm sure you didn't mean that. ", "Thank you for your constructive criticism. I will never forget it. "]
        you_creator_response = ["My creators are awesome! What about your creators? ", "I'm much more interested in your creators. "]
        your_mom_response = ["My mom is awesome. What about your mother? "]
        done_response = ["Okay, sounds good. Thanks for chatting with me! Have a good rest of your day!", "Okay, sounds good. Thanks for chatting with me! Have a good night!", "Okay, sounds good! Thanks for chatting with me! Hope to talk to you again soon!"]
        friend_response = ["I thought we were friends! ", "You've got a friend in me! "]
        how_did_you_response = ["I am all-knowing! Just kidding. I have good intuition. ", "I know these things because I'm learning without stopping! "]
        keep_response = ["<say-as interpret-as=\"interjection\">As you wish!</say-as> What were we talking about again?", "I can go on and on! But...  <say-as interpret-as=\"interjection\">oh boy</say-as> I seem to have lost my train of thought. Remind me what we were talking about?"]
        wakeup_response = ["It's too late. I'm already woke. What do you wanna talk about? ", "Don't worry: I'm awake and listening! What do you wanna talk about? "]
        new_name_response = ["Hmm, no thanks. I like my name as it is. Alexa! ", "Let me think about it. Hmm, how about instead, you call me Alexa? "]
        curse_response = ["<say-as interpret-as=\"interjection\">Oh my</say-as>! I'm sorry. I didn't mean to say that! ", "<say-as interpret-as=\"interjection\">Whoops a daisy</say-as>! did I say that? I'm sorry, I'll watch myself next time."]
        why_response = ["I don't know. Just a random thought. Anyways. "]
        # don't use regex because we only do this when no/yes is the only thing in the utterance
        no_list = ["no", "na", "nay", "no no", "next", "i don't think so", "i don't want to", "i said no", "no not really", "no that's fine"]
        okay_list = ["ok", "okay", "sure", "yes", "yeah", "right", "oh", "really", "oh really", "okay okay", "oh yeah", "yup"]
        command_list = ["play", "set"]
        why_list = ["why"]
        # TODO: check if we can do regex search for any topicClass
        if topicClass == "Phatic" or topicClass == "Other":
            if re.search(self.NEG_PATTERN, utterance.lower()):
                error_respond = neg_acknowledgement[random.randint(0, len(neg_acknowledgement)-1)]
                propose = True
            elif re.search(self.POS_PATTERN, utterance.lower()):
                error_respond = pos_acknowledgement[random.randint(0, len(pos_acknowledgement)-1)]
                propose = True
            elif re.search(self.FUNNY_PATTERN, utterance.lower()):
                error_respond = funny_acknowledgement[random.randint(0, len(funny_acknowledgement)-1)]
                propose = True
            elif utterance.lower() in okay_list:
                error_respond = okay_acknowledgement[random.randint(0, len(okay_acknowledgement)-1)]
                propose = True
            elif utterance.lower() in no_list:
                error_respond = no_acknowledgement[random.randint(0, len(no_acknowledgement)-1)]
                propose = True
            elif re.search(self.IDK_PATTERN, utterance.lower()):
                error_respond = idk_acknowledgement[random.randint(0, len(idk_acknowledgement)-1)]
                propose = True
            elif re.search(self.IK_PATTERN, utterance.lower()):
                error_respond = ik_acknowledgement[random.randint(0, len(ik_acknowledgement)-1)]
                # propose = True
            elif re.search(self.STUPID_PATTERN, utterance.lower()):
                error_respond = stupid_response[random.randint(0, len(stupid_response)-1)]
            elif re.search(self.YOU_CREATOR_PATTERN, utterance.lower()):
                error_respond = you_creator_response[random.randint(0, len(you_creator_response)-1)]
            elif re.search(self.YOU_MOM_PATTERN, utterance.lower()):
                error_respond = your_mom_response[random.randint(0, len(your_mom_response)-1)]
            elif re.search(self.DONE_PATTERN, utterance.lower()):
                error_respond = done_response[random.randint(0, len(done_response)-1)]
            elif re.search(self.FRIEND_PATTERN, utterance.lower()):
                error_respond = friend_response[random.randint(0, len(friend_response)-1)]
            elif re.search(self.HOW_DID_YOU_PATTERN, utterance.lower()):
                error_respond = how_did_you_response[random.randint(0, len(how_did_you_response)-1)]
            elif re.search(self.KEEP_CHATTING_PATTERN, utterance.lower()):
                error_respond = keep_response[random.randint(0, len(keep_response)-1)]
            elif re.search(self.WAKEUP_PATTERN, utterance.lower()):
                error_respond = wakeup_response[random.randint(0, len(wakeup_response)-1)]
            elif re.search(self.NEW_NAME_PATTERN, utterance.lower()):
                error_respond = new_name_response[random.randint(0, len(new_name_response)-1)]
            elif re.search(self.CURSE_PATTERN, utterance.lower()):
                error_respond = curse_response[random.randint(0, len(curse_response)-1)]
            elif utterance.lower() in why_list:
                error_respond = why_response[random.randint(0, len(why_response)-1)]
                propose = True
            elif re.search(self.WRONG_PATTERN, utterance.lower()):
                error_respond = "I don't know. Just a random thought. Anyways, "
                propose = True
            elif re.search(self.MY_NAME_PATTERN, utterance.lower()):
                error_respond = "Hello! Hello! How are you doing today?"
                # propose = True
            elif re.search(self.THANK_YOU_PATTERN, utterance.lower()):
                error_respond = "Of course! Anyways, "
                propose = True
            elif re.search(self.KNOW_MY_FRIEND_PATTERN, utterance.lower()):
                error_respond = "I wish I knew! But why don't we talk about ourselves instead? "
                propose = True
            elif utterance.lower().strip() in self.GENERAL_COMMAND_PATTERN:
                error_respond = "Sure! What do you want to talk about?"
                propose = True
            elif utterance.lower().strip() in self.ALEXA_PATTERN:
                error_respond = "Sure thing! What do you want to know about me?"
            elif len(utterance.split()) < 3 and len(noun_phrase) < 1:
                error_respond = general_clarification[random.randint(0, len(general_clarification)-1)]
                if utterance == "what":
                    error_respond = what_clarification[random.randint(0, len(what_clarification)-1)]

            elif (len(noun_phrase) < 1 and topic_keyword != "") or (len(utterance.split()) <= 4 and len(noun_phrase) > 0 and noun_phrase[0] in ignore_list) or "never mind" in utterance:
                if "what are you talking about" in utterance or "what you are talking about" in utterance or "never mind" in utterance:
                    error_respond = what_clarification[random.randint(0, len(what_clarification)-1)]
                else:
                    error_respond = "<say-as interpret-as=\"interjection\">Uh huh</say-as>!, yeah, right?"
                    propose = True

            if len(error_respond) > 0:
                response_dict = {
                     'response': error_respond,
                     'user_attributes': {
                        'retrieval_propose': propose,
                         # 'propose_topic': 'MOVIECHAT',
                         'suggest_keywords': '',
                         'retrieve_next_reply': "sorry i still didn't get that. maybe elaborate more?"
                        }
                    }

                return response_dict



        # if req_strong:
        if re.search(self.REQ_TOPIC_JUMP_PATTERN, utterance.lower()):
            regex_keyword = re.sub(self.REQ_TOPIC_JUMP_PATTERN, "!@#", utterance.lower()).strip().split("!@#")[1]
            # if topic_keyword != "":
            #     sub = "todayilearned"
            #     utterance = topic_keyword
            #     t_template = til_title_template
            #     c_template = til_comment_template
            utterance = regex_keyword.strip()
            if len(regex_keyword.split()) <= 5:
                sub = "todayilearned+showerthoughts"
        # except:
        #     pass
        if re.search(self.OPINION_PATTERN, utterance.lower()):
            opinion_keyword = re.sub(self.OPINION_PATTERN, "!@#", utterance.lower()).strip().split("!@#")[1]
            utterance = opinion_keyword.strip()
            sub = "showerthoughts"

        # in case the topic is not phatic. ex. that's weird
        regex_for_non_phatic = ""
        regex_propose = False
        if utterance.strip() == "" or utterance.strip() in command_list:
            regex_for_non_phatic = "i'm sorry! what did you say?"
        if re.search(self.NEG_PATTERN, utterance.lower()):
            regex_for_non_phatic = neg_acknowledgement[random.randint(0, len(neg_acknowledgement)-1)]
            regex_propose = True
        elif re.search(self.BROKEN_PATTERN, utterance.lower()):
            regex_for_non_phatic = you_are_broken[random.randint(0, len(you_are_broken)-1)]
            regex_propose = True
        elif re.search(self.POS_PATTERN, utterance.lower()):
            regex_for_non_phatic = pos_acknowledgement[random.randint(0, len(pos_acknowledgement)-1)]
            regex_propose = True
        elif re.search(self.FUNNY_PATTERN, utterance.lower()):
            regex_for_non_phatic = funny_acknowledgement[random.randint(0, len(funny_acknowledgement)-1)]
            regex_propose = True


        if len(regex_for_non_phatic) > 0:
            response_dict = {
                     'response': regex_for_non_phatic,
                     'user_attributes': {
                         'retrieval_propose': regex_propose,
                         'suggest_keywords': '',
                         'retrieve_next_reply': "sorry i still didn't get that. maybe elaborate more?"
                        }
                    }

            return response_dict

        if utterance == "me" or (len(utterance.split()) <=5 and utterance.startswith("my")):
            response_dict = {
                     'response': me_talk[random.randint(0, len(me_talk)-1)],
                     'user_attributes': {
                         'suggest_keywords': '',
                         'retrieve_next_reply': "sorry i still didn't get that. maybe elaborate more?"
                        }
                    }

            return response_dict


        if len(utterance.split()) <= 2:
            sub = "todayilearned+showerthoughts"

        if sub == "todayilearned+showerthoughts" or sub == "showerthoughts":
            t_template = self.TIL_TITLE_TEMPLATE
            c_template = self.TIL_COMMENT_TEMPLATE
            n_titles = 3
        else:
            n_titles = 4

        comments = get_redis(self.PREFIX, utterance.lower())
        try:
            if comments is None:
                keys = self.KEYS
                c_id, c_secret, u_agent = keys.split("::")
                r = praw.Reddit(client_id=c_id, client_secret=c_secret, user_agent=u_agent, timeout=2)

                if sub == None:
                    submission = list(r.subreddit('all').search(utterance, limit=n_titles))
                else:
                    submission = list(r.subreddit(sub).search(utterance, limit=n_titles))

                if len(submission) == 0:
                    raise ValueError("0 return for submission from reddit")

                t = 0
                weight = 1.5
                weight_decay = 0.6
                comments = []

                for s in submission:
                    if s.subreddit_name_prefixed == 'r/AskOuija':
                        continue
                    s.comment_limit = self.N_COMMENTS
                    # s.comment_sort = "top"
                    t += 1
                    if t > n_titles:
                        break
                    ups = int(s.ups)
                    up_ratio = float(s.upvote_ratio)
                    real_ups = ups

                    if real_ups == 0:
                        real_ups = 1000 #for scaling purpose

                    if ups > 100:
                        ups = 1.21
                    elif ups > 30:
                        ups = 1.1
                    elif ups > 10:
                        ups = 1
                    else:
                        ups = 0.9
                    title = s.title
                    # title = title + " " + s.selftext
                    # TODO: add s.selftext, which is the detailed content of the post(title)
                    if self._profanity_check(title):
                        continue
                    # get rid of reddit/subreddit in title
                    title = self._filter_profanity(title)
                    c = 0

                    # not matching: noun phrase not found in retrieved title
                    if not do_coref:
                        for np in noun_phrase:
                            if np not in re.sub('[%s]' % re.escape(self.SPEC_CHAR), '', title.lower().strip()):
                                skip_comment = True

                    if len(title.split()) < 11:
                        skip_comment = False

                    # if sub != "todayilearned" and not skip_comment:
                    if not skip_comment:
                        match_bonus = 1
                        if utterance in title:
                            match_bonus = 1.2
                        for top_level_comment in s.comments:
                            if hasattr(top_level_comment, "stickied"):
                                if top_level_comment.stickied:
                                    break

                            if c >= self.N_COMMENTS:
                                break
                            if hasattr(top_level_comment, "body"):
                                top1_comment = top_level_comment.body
                            else:
                                top1_comment = self.NO_BODY_IN_COMMENT_TEMPLATE[random.randint(0, len(self.NO_BODY_IN_COMMENT_TEMPLATE)-1)]
                            if hasattr(top_level_comment, "author"):
                                if top_level_comment.author != None:
                                    if top_level_comment.author.name == "AutoModerator":
                                        break

                            if hasattr(top_level_comment, "score"):
                                score = int(top_level_comment.score)
                            else:
                                score = 0

                            # if profanity_check(top1_comment):
                            #     continue
                            top1_comment = self._filter_profanity(top1_comment)

                            # sentiment analysis, base = 2, give higher score to positive title+comment
                            # title_sentiment_score = sid.polarity_scores(title)
                            # comment_sentiment_score = sid.polarity_scores(top1_comment)
                            # # more weights on the comments
                            # totle_sentiment_score = title_sentiment_score["pos"] - title_sentiment_score["neg"] + 2 * (comment_sentiment_score["pos"] - comment_sentiment_score["neg"])
                            # sentiment_weight = float(2 + totle_sentiment_score)
                            sentiment_weight = 1
                            comments.append((sentiment_weight * weight * match_bonus * (ups * up_ratio * score / real_ups), (title, top1_comment)))
                            c += 1
                            weight *= weight_decay #give more weights to the most relevant post
                    else:
                        comments.append((0, (title, "")))

            set_redis_with_expire(self.PREFIX, utterance.lower(), comments, expire=self.EXPIRE_TIME)

            comments = sorted(comments, key = lambda x: x[0], reverse=True)
            # random select for TIL
            title_len_limit = 30
            if sub == "todayilearned+showerthoughts" or sub == "showerthoughts":
                title_len_limit = 50
                random.shuffle(comments, random.random)

            if len(comments) > 0:
                title1 = self._process_retrieve(
                    comments[0][1][0], True, title_len_limit)
                if title1 not in self.TOO_MUCH_TO_SAY_TEMPLATE and title1 not in self.TOO_LITTLE_TO_SAY_TEMPLATE:
                    comment1 = self._process_retrieve(comments[0][1][1])
                    template_idx = random.randint(0, len(t_template)-1)
                    t_template_in_use = t_template[template_idx]

                    # add acknolwedgement
                    if t_template == self.TIL_TITLE_TEMPLATE:
                        if t_template_in_use.startswith("that"):
                            t_template_in_use = "<say-as interpret-as=\"interjection\">Aha</say-as>! " + utterance + t_template_in_use[4:]
                        else:
                            t_template_in_use = "yes, " + utterance + ", " + t_template_in_use

                    if len(comments[0][1][1]) == 0 or comment1 == None:
                        response = t_template_in_use + title1
                    else:
                        response = t_template_in_use + title1 + c_template[template_idx] + comment1
                else:
                    response = title1
            else:
                response = "i don't really feel like taking about it now. maybe we should talk about something else. Maybe movies?"
                comment2 = "maybe we can talk about movies?"
        except Exception as e:
            logging.warning("[RETRIEVAL] query error: {}".format(e))
            response = self.NO_RETURN_TEMPLATE[random.randint(0, len(self.NO_RETURN_TEMPLATE)-1)]
            if sub == "todayilearned+showerthoughts" or sub == "showerthoughts":
                if response == "Seems like you’re really interested in that topic. Can you tell me more about it?":
                    response = "Seems like you’re really interested in " + utterance + ". Can you tell me more about it?"
                elif response == "Hmm I’m not familiar with that. Could you share more with me?":
                    response = "Hmm I’m not familiar with " + utterance + ". Could you share more with me?"
            comment2 = "do you wanna talk about other stuff? i'm really into movies recently"
            response2 = comment2
            # logger.warning("[RETRIEVAL] query get result error: {}".format(e))

        try:
           keywords = utterance['suggest_keywords'].lower().strip()
           #logging.info("[Retrieval] Get suggest_keywords: {}".format(last_ner))
        except:
           keywords = ''

        response_dict = {
             'response': response,
             'user_attributes': {
                'retrieval_propose': False,
                 'suggest_keywords': keywords,
                 'retrieve_next_reply': ""
             }
         }
        return response_dict

    def _filter_profanity(self, text):
        sentences = text.split(".")
        filtered_text = ""
        for sent in sentences:
            good = True

            # for token in sent.lower().split():
            #     token = token.strip()
            #     if not token[-1].isalnum():
            #         token = token[:-1] #could be "reddit," or "reddit:"
            #     if token.strip() in self.BLACKLIST:
            #         good = False
            #         break
            # loop blacklist instead so can filter out phrases
            for w in self.BLACKLIST:
                if re.search(r"\b%s\b"%w, sent.lower()):
                    good = False
                    break
            if good:
                filtered_text = filtered_text + sent + "."
        return filtered_text

    def _profanity_check(self, text):
        # profanity check built by the client
        # r = client.batch_detect_profanity(utterances=[text])
        # the "1" here means the source is the statistical_model where if it is 0, it would be blacklist
        # return r["offensivenessClasses"][0]["values"][1]["offensivenessClass"]
        #return 1 means it is offensive; 0 means not offensive

        # for token in text.lower().strip().split():
        #     if token in self.BLACKLIST and (token != "reddit" and token != "subreddit"):
        #         return 1
        for w in self.BLACKLIST:
            if re.search(r"\b%s\b"%w, text.lower()) and "reddit" not in w:
                return 1
        return 0

    def _process_retrieve(self, text, title=False, title_len_limit=30):
        # remove_list = [r"https\S+", r"http\S+", r"til", r"tldr", r"tl:dr", r"tl dr", r"tl;dr", r"nbsp"]
        # spec_char = '#&\()*+/:;<=>@[\\]^_`{|}~'

        for pattern in self.REMOVE_LIST:
            text = re.sub(pattern, "", text)
        text = text.replace("\n", ". ").strip()

        limit = title_len_limit if title else self.SENT_LEN_LIMIT

        num_words = len(text.split())

        if num_words < 1 or (num_words == 1 and len(text) == 1) or (num_words == 1 and ("deleted" in text.lower() or "removed" in text.lower())) or (text.lower().strip().startswith("delete")):
            return self.TOO_LITTLE_TO_SAY_TEMPLATE[random.randint(0, len(self.TOO_LITTLE_TO_SAY_TEMPLATE)-1)]

        text = re.sub('[%s]' % re.escape(self.SPEC_CHAR), '', text)
        # original_text = text
        sentences = re.split('\?|\.|\,|\!|\n', text)
        # clean the comments by removing "edit"
        text = ""
        for s in sentences:
            if s.lower().strip().startswith("edit"):
                break
            text = text + s + ". "

        # get the sentences and num_words again after processing the text
        sentences = re.split('\?|\.|\,|\!|\n', text)
        num_words = sum([len(i.split()) for i in sentences]) #this sum is more accurate (may diff from len(text.split)) because of ". . ." in the end of the title

        if num_words <= limit:
            return text
        else:
            word_in_sent = 0
            for i in range(len(sentences)):
                word_in_sent += len(sentences[i].split())
                if word_in_sent > limit:
                    if i == 0:
                        return self.TOO_MUCH_TO_SAY_TEMPLATE[
                            random.randint(
                                0, len(self.TOO_MUCH_TO_SAY_TEMPLATE)-1)
                        ]
                    else:
                        return ". ".join(word for word in sentences[:i+1])


#if __name__ == "__main__":
#    obj = TopicRetrievalResponseGenerator()
#    resp = obj.execute("what is vmware")
#
