import logging
import signal
import re
from requests.exceptions import ReadTimeout
from cobot_common.service_client import get_client

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
COBOT_API_KEY = 'xgTjk23SRM9Q9VFrUEFOjav0Bn4ot21W8uFLEieT'


class Timeout():
    """Timeout class using ALARM signal."""
    class Timeout(Exception):
        pass

    def __init__(self, sec):
        self.sec = sec

    def __enter__(self):
        signal.signal(signal.SIGALRM, self.raise_timeout)
        signal.alarm(self.sec)

    def __exit__(self, *args):
        signal.alarm(0)    # disable alarm

    def raise_timeout(self, *args):
        raise Timeout.Timeout()


def EVI_bot(utterance, threadDict: dict = {}):
    try:
        with Timeout(1):
            client = get_client(api_key=COBOT_API_KEY)
            r = client.get_answer(question=utterance, timeout_in_millis=1000)
            logger.warning(r)
            if r["response"] == "" or re.search(r"skill://", r["response"]):
                return None
            threadDict['evi'] = r["response"]
            return r["response"] + " "
    except ReadTimeout as e:
        logger.error("[TECHSCIENCE] evi bot read error:{}".format(e))
        threadDict['evi'] = None
        logger.warning("Timeout in EVI_bot")
        return None


if __name__ == "__main__":
    EVI_bot('what is your opinion on science')
