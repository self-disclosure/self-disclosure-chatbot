from techscience_automaton import *
from techscience_utils import *
import logging
logger = logging.getLogger(__name__)

required_context = ['text']


def get_required_context():
    return required_context


def handle_message(msg):
    msg["text"][0] = msg["text"][0].lower().strip()

    if msg.get("techsciencechat") is None: # check if None
        msg["techsciencechat"] = {}
    current_state = msg["techsciencechat"].get("current_transition")

    if current_state == None:
        msg["techsciencechat"]["current_transition"] = "t_init"

    # # hack: reset everything techsciencechat; asr shouldn't return qwe but best just comment this out before pushing
    # if re.search(r"reset techsciencechat qwe", msg["text"][0]):
    #     response_dict = {
    #         'response': "techsciencechat has been reset!",
    #         'user_attributes': {
    #             'techsciencechat': {}
    #         }
    #     }
    #     return response_dict

    RG = TechScienceAutomaton()
    results = RG.transduce(msg)
    response_dict = dict()
    response_dict["response"] = results["response"]
    response_dict["user_attributes"] = {
        "techsciencechat": msg.get("techsciencechat", {}),
        "template_manager": results["template_manager"],
        "propose_topic": msg.get("propose_topic", results["propose_topic"])
    }

    for k, v in results["techsciencechat"].items():
        response_dict["user_attributes"]["techsciencechat"][k] = v
    '''
        except Exception as e:
            # To keep cobot running, catch everything!
            return {
                "response": "Oh no, I think my creators misspelled something in my code. Would you mind asking something else?"
            }
        '''

    return response_dict
