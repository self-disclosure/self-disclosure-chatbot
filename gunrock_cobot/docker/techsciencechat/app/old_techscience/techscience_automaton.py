import logging
logging.basicConfig(
    format='[%(levelname)s] %(asctime)s [%(filename)s:%(lineno)d] %(message)s',
)
from techscience_utils import *
from techscience_qa import *
import template_manager
from types import SimpleNamespace

template_techsci = template_manager.Templates.techscience
template_error = template_manager.Templates.error

logging.getLogger().setLevel(logging.INFO)


class TechScienceAutomaton:

    def __init__(self):
        self.__bind_states()
        self.__bind_transitions()
        self.response = ""
        self.current_topic = ""
        self.context_topic = ""
        self.broader_topics = ["science", "technology", "tech"]
        self.fact_counter = 0
        self.research_counter = 0
        self.prev_transition = ""
        self.chitchat_counter = ""
        self.next_module = ""
        self.module_mappings = {'topic_movietv': 'movies and T.V. shows',
                                'topic_newspolitics': 'the news',
                                'topic_weather': 'the weather',
                                'topic_music': 'music',
                                'topic_book': 'books',
                                'topic_sport': 'sports',
                                # 'topic_entertainment': 'news and entertainment',
                                'topic_holiday': 'holidays',
                                'topic_food': 'food',
                                'topic_travel': 'travel',
                                'topic_animal': 'animals',
                                'topic_game': 'games'}
        self.output_module_mappings = {'topic_movietv': 'MOVIECHAT',
                                       'topic_newspolitics': 'NEWS',
                                       'topic_weather': 'WEATHER',
                                       'topic_music': 'MUSICCHAT',
                                       'topic_book': 'BOOKCHAT',
                                       'topic_sport': 'SPORT',
                                       # 'topic_entertainment': 'NEWS',
                                       'topic_holiday': 'HOLIDAYCHAT',
                                       'topic_food': 'FOODCHAT',
                                       'topic_travel': 'TRAVELCHAT',
                                       'topic_animal': 'ANIMALCHAT',
                                       'topic_game': 'GAMECHAT'}

    # bind each state to corresponding callback function that generates
    # response
    def __bind_states(self):
        self.states_mapping = {
            "s_echo": self.s_echo,
            "s_intro": self.s_intro,
            "s_opinion": self.s_opinion,
            "s_ask_favorite_thing": self.s_ask_favorite_thing,
            "s_interesting_fact": self.s_interesting_fact,
            # "s_first_chitchat": self.s_first_chitchat,
            "s_ask_research": self.s_ask_research,
            "s_interesting_research": self.s_interesting_research,
            "s_change_of_topic": self.s_change_of_topic,
            "s_user_change_topic": self.s_user_change_topic,
            "s_techsci_chitchat": self.s_techsci_chitchat,
            "s_ask_more": self.s_ask_more,
            "s_ask_finish": self.s_ask_finish,
            "s_user_change_module": self.s_user_change_module,
            "s_exit_techsciencechat_intentional": self.s_exit_techsciencechat_intentional,
            "s_exit_techsciencechat": self.s_exit_techsciencechat
        }

    # bind each state to corresponding callback function that determines the
    # next state
    def __bind_transitions(self):
        self.transitions_mapping = {
            "t_init": self.t_init,
            "t_sys_intent": self.t_sys_intent,
            "t_intro": self.t_intro,
            "t_opinion": self.t_opinion,
            "t_ask_favorite_thing": self.t_ask_favorite_thing,
            # "t_first_chitchat": self.t_first_chitchat,
            "t_interesting_fact": self.t_interesting_fact,
            "t_ask_research": self.t_ask_research,
            "t_interesting_research": self.t_interesting_research,
            "t_change_of_topic": self.t_change_of_topic,
            "t_user_change_topic": self.t_user_change_topic,
            "t_techsci_chitchat": self.t_techsci_chitchat,
            "t_ask_finish": self.t_ask_finish,
            "t_user_change_module": self.t_user_change_module,
            "t_resume": self.t_resume  # resume after techscience not continuously selected

        }

    # get a response based on current state by calling the corresponding
    # callback functions
    def transduce(self, input_data):
        # Refactored
        class UserAttributes:

            def __init__(self, prev_hash, usr_name):
                self.prev_hash = prev_hash  # type: dict
                self.usr_name = usr_name

        self.text = input_data["text"][0]
        self.this_module = "TECHSCIENCECHAT"
        if (input_data["features"][0]).get("coreference"): # EVI Bot Input Refactored
            self.coref_text = input_data["features"][0][ # Refactored
                "coreference"].get("text", input_data["text"][0]) # Refactored
            self.text = self.coref_text # Refactored
        else:
            self.coref_text = self.text
        self.ner = input_data["features"][0]["ner"] # Detect topic Refactored
        self.knowledge = input_data["features"][0]["npknowledge"]["knowledge"] # Detect topic refactored
        self.topic_keywords = input_data["features"][0]["topic_keywords"] # Detect topic, refactored
        self.noun_phrase = input_data["features"][0]["noun_phrase"] # Detect topic, noun_phrase
        
        self.sentiment = input_data["features"][0]["sentiment"] # Not used

        self.cobot_intents_lexical = input_data[ # Cobot_Intents_Lexical Refactored
            "features"][0]["intent_classify"]["lexical"] 
        self.cobot_intents_system = input_data[
            "features"][0]["intent_classify"]["sys"] # Not used
    
        self.dialog_act = input_data["features"][0]["dialog_act"] # Refactored

        self.user_attributes = input_data["techsciencechat"]  # prev techscience user attributes
        self.used_topics = input_data["module_selection"]["used_topic"] # Not Used
        self.backstory_response = input_data["features"][0]["central_elem"]["backstory"] # Refactored
        self.ask_flag = any(intent in ask_intents for intent in self.cobot_intents_lexical) # Not Used
        self.template_manager_hash = SimpleNamespace(**{"template_manager": input_data["template_manager"]})
        self.last_module = input_data["last_module"] # Not Used

        self.response_dict = dict()
    
        if input_data["features"][0]["intent_classify"]["topic"]: # Refactored
            self.intent_classify_topic = input_data[ # *
                "features"][0]["intent_classify"]["topic"][0] # *
        else: # *
            self.intent_classify_topic = "topic_techscience" # *

        if "context_topic" in input_data["techsciencechat"]: # Refactored
            self.context_topic = input_data["techsciencechat"]["context_topic"]
        if "current_topic" in input_data["techsciencechat"]:
            self.current_topic = input_data["techsciencechat"]["current_topic"]
        if "fact_counter" in input_data["techsciencechat"]:
            self.fact_counter = input_data["techsciencechat"]["fact_counter"]
        if "research_counter" in input_data["techsciencechat"]:
            self.research_counter = input_data[
                "techsciencechat"]["research_counter"]
        if "chitchat_counter" in input_data["techsciencechat"]:
            self.chitchat_counter = input_data[
                "techsciencechat"]["chitchat_counter"]
        if "next_module" in input_data["techsciencechat"]:
            self.next_module = input_data["techsciencechat"]["next_module"] # *
        if self.ask_flag: # Not Used
            # transition and state decides how to use this
            self.ask_result = EVI_bot(self.coref_text) # Not Used
        self.input_data = input_data # Refactored

        # attributes to store in callback functions
        self.techsciencechat = { # Refactored
            "not_first_time": True,
            "propose_continue": "CONTINUE",
            "propose_topic": None
            # "prev_hash": ""
        }  # info stored here will be stored in user attr
        # self.response = "Uh oh. I'm getting confused. Can you help me lead the conversation back on track?"
        self.response = ""
        self.t_context = {} # TODO; where is this set.

        if input_data["techsciencechat"]["current_transition"]:
            self.prev_transition = input_data[
                "techsciencechat"]["current_transition"]

        self.sys_intents = detect_sys_intent(self.text, self.dialog_act)
        if self.sys_intents:
            current_transition = "t_sys_intent"
        else:
            current_transition = input_data[
                "techsciencechat"]["current_transition"]

        # next_state = self.transitions_mapping[current_transition]()  # t state returns s state
        # next_transition = self.states_mapping[next_state]()  # s state
        # returns t state

        try:
            if current_transition == "t_sys_intent":
                next_state = self.transitions_mapping[
                    current_transition]()  # t state returns s state
                if next_state == "s_ask_more":
                    next_state = self.transitions_mapping[
                        current_transition]()  # t state returns s state
                    if next_state == "s_ask_more":
                        next_state = self.transitions_mapping[
                            self.prev_transition]()
                else:
                    next_state = self.transitions_mapping[current_transition]()
                next_transition = self.states_mapping[
                    next_state]()  # s state returns t state
        except Exception as e: # Refactored
            return {
                "response": "<say-as interpret-as='interjection'>Uh oh </say-as><break time='.05s'/> <prosody rate='80%'> I think you want to talk about something else. </prosody>",
                "template_manager": dict(self.template_manager_hash.template_manager),
                "techsciencechat": {
                    "not_first_time": False,
                    "propose_continue": "STOP",
                    "propose_topic": None,
                    "current_transition": "",
                    "current_topic": "",
                    "context_topic": "",
                    "fact_counter": 0,
                    "chitchat_counter": ""
                },
                "propose_topic": None,
            }

        # save transition for next user utterance
        self.techsciencechat["current_transition"] = next_transition
        self.response_dict[
            "response"] = construct_dynamic_response(self.response)
        self.response_dict["template_manager"] = dict(self.template_manager_hash.template_manager)
        self.response_dict["techsciencechat"] = self.techsciencechat
        self.response_dict["propose_topic"] = self.techsciencechat[
            "propose_topic"]

        return self.response_dict


    # =========================================================
    # Refactored
    def t_sys_intent(self):
        lexical_intent = detect_intent(self.text)
        if "exit_techscienceRG" in self.sys_intents:
            return "s_exit_techsciencechat"
        if "req_scitech" in lexical_intent or "req_topic" in lexical_intent:
            hard_topic = detect_topic(
                self.knowledge, self.topic_keywords, self.noun_phrase, self.ner, self.text)
            if hard_topic != "no_topic":
                self.techsciencechat["propose_continue"] = "CONTINUE"
                self.techsciencechat[
                    "current_topic"], self.current_topic = hard_topic, hard_topic
                self.techsciencechat[
                    "context_topic"], self.context_topic = hard_topic, hard_topic
                self.techsciencechat["fact_counter"] = 0
                self.techsciencechat[
                    "chitchat_counter"] = self.chitchat_counter
                return "s_intro"
        if self.sys_intents:
            if len(self.sys_intents) > 1:
                pass
            if "user_ask_question" in self.sys_intents:
                self.response = "Sure! Ask away!"
                return "s_echo"
            if self.sys_intents[0] == "question" or "ask" in self.cobot_intents_lexical:
                qa = QABot()
                self.response = qa.get_answer(self.text, self.input_data['a_b_test'])
                if not self.response:
                    if self.backstory_response:
                        curr_confidence = self.backstory_response['confidence']
                        curr_response = self.backstory_response['text']
                        if curr_response and curr_confidence > 0.65:
                            self.response = curr_response
                if not self.response:
                    self.response = "Hmm, I haven't thought about that before. "
                if self.prev_transition != "t_techsci_chitchat" and self.current_topic != "":
                    self.response += " So, about " + self.current_topic + " <break time=\"300ms\"/>  "
                else:
                    self.response += " Well, coming back to what we were (talking about|discussing) earlier, "
                return "s_ask_more"
                # return "s_echo"
        return "s_exit_techsciencechat"  # should not reach here

    # Refactored
    def s_exit_techsciencechat(self):
        self.response = "Ok!"
        self.techsciencechat["propose_continue"] = "STOP"
        if self.techsciencechat["propose_topic"] is None:
            self.techsciencechat["propose_topic"] = None
        else:
            self.response_dict["propose_topic"] = self.techsciencechat[
                "propose_topic"]
            self.techsciencechat["propose_topic"] = None
        self.techsciencechat["current_topic"] = ""
        self.techsciencechat["context_topic"] = ""
        self.techsciencechat["fact_counter"] = 0
        self.techsciencechat["chitchat_counter"] = ""
        return "t_resume"

    # Refactored
    def s_exit_techsciencechat_intentional(self):
        self.response = "Okay! That was a lot of talk on science and technology! "
        self.techsciencechat["propose_continue"] = "STOP"
        if self.techsciencechat["propose_topic"] in self.module_mappings:
            self.techsciencechat["propose_topic"] = None
        self.techsciencechat["current_topic"] = ""
        self.techsciencechat["context_topic"] = ""
        self.techsciencechat["fact_counter"] = 0
        self.techsciencechat["chitchat_counter"] = ""
        return "t_resume"

    # Todo: Enable resuming a topic if module is exited
    def t_resume(self):
        temp_current_topic = detect_topic(
            self.knowledge, self.topic_keywords, self.noun_phrase, self.ner, self.text)
        if temp_current_topic is not "no_topic":
            self.current_topic, self.context_topic = temp_current_topic, temp_current_topic
        else:
            self.current_topic, self.context_topic = "technology", "technology"
        lexical_intent = detect_intent(self.text)
        self.techsciencechat["propose_continue"] = "CONTINUE"
        self.techsciencechat["current_topic"] = self.current_topic
        self.techsciencechat["context_topic"] = self.context_topic
        self.techsciencechat["fact_counter"] = 0
        self.techsciencechat["chitchat_counter"] = ""
        return "s_ask_favorite_thing"

    def s_echo(self):
        # next_transition = self.t_context.get("next_transition", "t_init")
        next_transition = self.t_context.get(
            "next_transition", "t_change_of_topic")
        return next_transition

    def t_init(self):
        self.current_topic = detect_topic(
            self.knowledge, self.topic_keywords, self.noun_phrase, self.ner, self.text)
        self.context_topic = self.current_topic
        lexical_intent = detect_intent(self.text)
        if "exit_techscienceRG" in lexical_intent:
            self.response = template_techsci.utterance(selector='intro/no_topic',
                                                       slots={
                                                           'topic': self.current_topic},
                                                       user_attributes_ref=self.template_manager_hash)
            self.techsciencechat["propose_continue"] = "STOP"
            self.techsciencechat["current_topic"] = None
            self.techsciencechat["context_topic"] = None
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            return "s_exit_techsciencechat"
        if self.current_topic is "no_topic":
            self.current_topic = "technology"
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = "technology"
            self.techsciencechat["context_topic"] = "technology"
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
        else:
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.current_topic
            self.techsciencechat["context_topic"] = self.context_topic
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
        return "s_intro"

    # Refactored
    def s_intro(self):
        # if self.user_attribtues_hash.prev_hash["transition_args"]:
        #    self.current_topic = self.user_attributes_hash.prev_hash["transition_args"]
        if self.current_topic in self.broader_topics:
            self.response += template_techsci.utterance(selector='intro/in_topic_scitech_first_time',
                                                        slots={
                                                            'topic': self.current_topic},
                                                        user_attributes_ref=self.template_manager_hash)
        else:
            self.response += template_techsci.utterance(selector='intro/in_other_topics',
                                                        slots={
                                                            'topic': self.current_topic},
                                                        user_attributes_ref=self.template_manager_hash)
        return "t_intro"

    def t_intro(self):
        if self.intent_classify_topic in self.module_mappings:
            return "s_user_change_module"
        self.current_topic = detect_topic(
            self.knowledge, self.topic_keywords, self.noun_phrase, self.ner, self.text)
        lexical_intent = detect_intent(self.text)
        if self.current_topic in self.broader_topics:
            if self.current_topic is "technology" or self.current_topic is "tech":
                self.current_topic = random.choice(technology_flairs)
            if self.current_topic is "science":
                self.current_topic = random.choice(science_flairs)

        if "ask_question" in lexical_intent and "req_research" in lexical_intent:
            self.current_topic = self.context_topic
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.context_topic
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            return "s_interesting_research"
        elif self.current_topic is "no_topic" and ("req_research" not in lexical_intent or "ask_question" not in lexical_intent):
            self.current_topic = self.context_topic
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.context_topic
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            return "s_opinion"
        else:
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.current_topic
            self.techsciencechat["context_topic"] = self.current_topic
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            return "s_opinion"

    def s_opinion(self):
        key_selector = self.current_topic.replace(" ", "_")

        if topic_exists_in_opinion(key_selector):
            self.response += template_techsci.utterance(selector='opinion/{}'.format(key_selector + "_topic"),
                                                        slots={
                                                            'topic': self.current_topic},
                                                        user_attributes_ref=self.template_manager_hash)
        else:
            self.response += template_techsci.utterance(selector='opinion/general_topic',
                                                        slots={
                                                            'topic': self.current_topic},
                                                        user_attributes_ref=self.template_manager_hash)
        return "t_opinion"

    def t_opinion(self):

        if self.intent_classify_topic in self.module_mappings:
            return "s_user_change_module"

        if self.backstory_response:
            curr_confidence = self.backstory_response['confidence']
            curr_response = self.backstory_response['text']
            if curr_response and curr_confidence > 0.75:
                self.response = curr_response
                return "s_change_of_topic"

        self.current_topic = detect_topic(
            self.knowledge, self.topic_keywords, self.noun_phrase, self.ner, self.text)
        lexical_intent = detect_intent(self.text)

        if self.current_topic in self.broader_topics:
            if self.current_topic is "technology" or self.current_topic is "tech":
                self.current_topic = random.choice(technology_flairs)
                self.context_topic = self.current_topic
            if self.current_topic is "science":
                self.current_topic = random.choice(science_flairs)
                self.context_topic = self.current_topic

        if "ask_question" in lexical_intent or "req_research" in lexical_intent:
            self.current_topic = self.context_topic
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.context_topic
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            return "s_interesting_research"
        elif self.current_topic is "no_topic":
            self.current_topic = self.context_topic
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.context_topic
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            return "s_ask_favorite_thing"
        else:
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.current_topic
            self.techsciencechat["context_topic"] = self.current_topic
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            return "s_ask_favorite_thing"

    def s_ask_favorite_thing(self):
        key_selector = self.current_topic.replace(" ", "_")
        lexical_intent = detect_intent(self.text)
        if "answer_no" in lexical_intent and self.current_topic != "artificial intelligence":
            self.response += template_techsci.utterance(selector='favorite/know_nothing',
                                                        slots={
                                                            'topic': self.current_topic},
                                                        user_attributes_ref=self.template_manager_hash)
            return "t_ask_favorite_thing"

        if topic_exists_in_favorite(key_selector):
            if "answer_yes" in lexical_intent:
                ack = " (Yeah|Yes), I agree with you. "
                self.response += ack + template_techsci.utterance(selector='favorite/{}'.format(key_selector + "_topic"),
                                                                  slots={
                                                                      'topic': self.current_topic},
                                                                  user_attributes_ref=self.template_manager_hash)
            else:
                self.response += template_techsci.utterance(selector='favorite/{}'.format(key_selector + "_topic"),
                                                            slots={
                                                                'topic': self.current_topic},
                                                            user_attributes_ref=self.template_manager_hash)
        else:
            self.response += template_techsci.utterance(selector='favorite/general_topic',
                                                        slots={
                                                            'topic': self.current_topic},
                                                        user_attributes_ref=self.template_manager_hash)
        return "t_ask_favorite_thing"

    def t_ask_favorite_thing(self):
        if self.intent_classify_topic in self.module_mappings:
            return "s_user_change_module"
        self.current_topic = detect_topic(
            self.knowledge, self.topic_keywords, self.noun_phrase, self.ner, self.text)
        lexical_intent = detect_intent(self.text)
        self.techsciencechat["fact_counter"] = self.fact_counter
        self.techsciencechat["research_counter"] = self.research_counter
        self.techsciencechat["chitchat_counter"] = self.chitchat_counter

        if self.current_topic is "no_topic":
            self.current_topic = self.context_topic
            self.techsciencechat["current_topic"] = self.context_topic

        if self.context_topic in self.broader_topics:
            if self.context_topic == "technology" or self.context_topic == "tech":
                temp_current_topic = random.choice(technology_flairs).lower()
                self.context_topic = temp_current_topic
            if self.context_topic == "science":
                temp_current_topic = random.choice(science_flairs).lower()
                self.context_topic = temp_current_topic

        if "answer_yes" in lexical_intent:
            self.current_topic = self.context_topic
            self.techsciencechat["context_topic"] = self.context_topic
            self.techsciencechat["current_topic"] = self.context_topic
            return "s_interesting_fact"
        elif "answer_no" in lexical_intent:
            self.chitchat_counter = find_next_chitchat(self.chitchat_counter)
            if self.chitchat_counter == "end_chitchat":
                return "s_exit_techsciencechat_intentional"
            self.current_topic = self.context_topic
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.context_topic
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            return "s_techsci_chitchat"
        return "s_interesting_fact"

    def s_interesting_fact(self):
        key_selector = self.current_topic.replace(" ", "_")
        lexical_intent = detect_intent(self.text)
        if topic_exists(key_selector):
            if self.current_topic == "artificial intelligence":
                self.response += template_techsci.utterance(selector='facts/start',
                                                            slots={
                                                                'topic': self.current_topic},
                                                            user_attributes_ref=self.template_manager_hash) \
                    + template_techsci.utterance(selector='topics/{}/facts'.format(key_selector),
                                                 slots={
                                                     'topic': self.current_topic},
                                                 user_attributes_ref=self.template_manager_hash)
            else:
                self.response += template_techsci.utterance(selector='facts/start',
                                                            slots={
                                                                'topic': self.current_topic},
                                                            user_attributes_ref=self.template_manager_hash) \
                    + template_techsci.utterance(selector='topics/{}/facts'.format(key_selector),
                                                 slots={
                                                     'topic': self.current_topic},
                                                 user_attributes_ref=self.template_manager_hash)
                if self.fact_counter <= 1:
                    self.response += template_techsci.utterance(selector='facts/end',
                                                                slots={
                                                                    'topic': self.current_topic},
                                                                user_attributes_ref=self.template_manager_hash)
            # if "answer_yes" in lexical_intent:
            #    ack = " (Oh yes|Oh yeah)! I agree with you. "
            #    self.response = ack + self.response
        else:
            self.response += retrieve_kg_topic(query_tag(self.current_topic))
            # if not self.response:
            #     self.response += retrieve_kg_topic(
            #         query_keywords(self.current_topic))
            if not self.response:
                self.response += retrieve_scitech(
                    "what is " + self.current_topic)
            if not self.response:
                self.response += EVI_bot("What is " + self.current_topic)
            if self.response:
                if self.fact_counter <= 1:
                    self.response += template_techsci.utterance(selector='facts/end',
                                                                slots={
                                                                    'topic': self.current_topic},
                                                                user_attributes_ref=self.template_manager_hash)
            # if self.knowledge:
            #    ack = self.knowledge[0][0] + " ,you say. (Interesting|Nice|Hmmm). "
            #    self.response = ack + self.response
            else:
                self.response += "Oh boy! All this thinking has made me forget a few things. " \
                                 "However, there is some research in science that might interest you. " \
                                 "Do you wish to know some of that? "  # Todo: Add hardcoded facts
        return "t_interesting_fact"

    def t_interesting_fact(self):
        if self.intent_classify_topic in self.module_mappings:
            return "s_user_change_module"
        lexical_intent = detect_intent(self.text)
        temp_current_topic = detect_topic(
            self.knowledge, self.topic_keywords, self.noun_phrase, self.ner, self.text)
            
        if "answer_yes" in lexical_intent and self.fact_counter < 2:
            self.techsciencechat["fact_counter"] = self.fact_counter + 1
            return "s_interesting_fact"
        elif "answer_no" in lexical_intent or self.fact_counter >= 2:
            self.chitchat_counter = find_next_chitchat(self.chitchat_counter)
            if self.chitchat_counter == "end_chitchat":
                return "s_exit_techsciencechat_intentional"
            self.current_topic = self.context_topic
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.context_topic
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["fact_counter"] = 0
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            return "s_techsci_chitchat"

        if self.context_topic in self.broader_topics or temp_current_topic in self.broader_topics:
            if self.context_topic == "technology" or self.context_topic == "tech" \
                    or temp_current_topic == "technology" or temp_current_topic == "tech":
                self.current_topic = random.choice(technology_flairs).lower()
                self.context_topic = self.current_topic
            if self.context_topic == "science" or temp_current_topic == "science":
                self.current_topic = random.choice(science_flairs).lower()
                self.context_topic = self.current_topic

        if temp_current_topic is "no_topic" and "answer_yes" in lexical_intent and self.fact_counter < 2:
            self.current_topic = self.context_topic
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.context_topic
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["fact_counter"] = self.fact_counter + 1
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            return "s_interesting_fact"
        elif self.current_topic is not temp_current_topic and temp_current_topic is not "no_topic":
            self.current_topic = temp_current_topic
            self.context_topic = temp_current_topic
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["context_topic"] = self.context_topic
            self.techsciencechat["current_topic"] = self.current_topic
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["fact_counter"] = 0
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            return "s_user_change_topic"
        else:
            self.context_topic = self.current_topic
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.current_topic
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["fact_counter"] = self.fact_counter + 1
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            return "s_interesting_fact"

    def s_ask_research(self):
        self.response += template_techsci.utterance(selector='research/ask_research',
                                                    slots={
                                                        'topic': self.current_topic},
                                                    user_attributes_ref=self.template_manager_hash)
        return "t_ask_research"

    def t_ask_research(self):
        if self.intent_classify_topic in self.module_mappings:
            return "s_user_change_module"
        lexical_intent = detect_intent(self.text)
        temp_current_topic = detect_topic(
            self.knowledge, self.topic_keywords, self.noun_phrase, self.ner, self.text)
        if "answer_yes" in lexical_intent:
            return "s_interesting_research"
        elif "answer_no" in lexical_intent:
            if self.chitchat_counter is "":
                self.chitchat_counter = find_next_chitchat(
                    self.chitchat_counter)
            if self.chitchat_counter == "end_chitchat":
                return "s_exit_techsciencechat_intentional"
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            return "s_techsci_chitchat"

        if self.context_topic in self.broader_topics or temp_current_topic in self.broader_topics:
            if self.context_topic == "technology" or self.context_topic == "tech" \
                    or temp_current_topic == "technology" or temp_current_topic == "tech":
                self.current_topic = random.choice(technology_flairs).lower()
                self.context_topic = self.current_topic
            if self.context_topic == "science" or temp_current_topic == "science":
                self.current_topic = random.choice(science_flairs).lower()
                self.context_topic = self.current_topic

        if temp_current_topic is "no_topic" and "answer_yes" in lexical_intent:
            self.current_topic = self.context_topic
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.context_topic
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            return "s_interesting_research"
        elif self.current_topic is not temp_current_topic and temp_current_topic is not "no_topic":
            self.current_topic = temp_current_topic
            self.context_topic = temp_current_topic
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["context_topic"] = self.context_topic
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["current_topic"] = self.current_topic
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            return "s_user_change_topic"
        else:
            self.context_topic = self.current_topic
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.current_topic
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            return "s_interesting_research"

    def s_interesting_research(self):
        self.response += retrieve_kg_topic(query_tag(self.current_topic))
        if not self.response:
            self.response += retrieve_kg_topic(
                query_keywords(self.current_topic))
        if self.response:
            self.response = template_techsci.utterance(selector='research/start',
                                                       slots={
                                                           'topic': self.current_topic},
                                                       user_attributes_ref=self.template_manager_hash) + self.response
            if self.research_counter <= 0:
                self.response += template_techsci.utterance(selector='research/end',
                                                            slots={
                                                                'topic': self.current_topic},
                                                            user_attributes_ref=self.template_manager_hash)
        else:
            self.response += EVI_bot("Research in " + self.current_topic)
            if self.response:
                self.response = template_techsci.utterance(selector='research/start',
                                                           slots={
                                                               'topic': self.current_topic},
                                                           user_attributes_ref=self.template_manager_hash) + self.response
                if self.research_counter <= 0:
                    self.response = template_techsci.utterance(selector='research/end',
                                                               slots={
                                                                   'topic': self.current_topic},
                                                               user_attributes_ref=self.template_manager_hash)
            if not self.response:
                self.response += "Oh boy! All this thinking has made me forget a few things. " \
                                 "Do you mind talking about something else for now?"  # Todo: Add hardcoded facts
        return "t_interesting_research"

    def t_interesting_research(self):
        if self.intent_classify_topic in self.module_mappings:
            return "s_user_change_module"
        temp_current_topic = detect_topic(
            self.knowledge, self.topic_keywords, self.noun_phrase, self.ner, self.text)
        lexical_intent = detect_intent(self.text)
        self.chitchat_counter = find_next_chitchat(self.chitchat_counter)
        if self.chitchat_counter == "end_chitchat":
            return "s_exit_techsciencechat_intentional"

        if self.context_topic in self.broader_topics or temp_current_topic in self.broader_topics:
            if self.context_topic == "technology" or self.context_topic == "tech" \
                    or temp_current_topic == "technology" or temp_current_topic == "tech":
                self.current_topic = random.choice(technology_flairs).lower()
                self.context_topic = self.current_topic
            if self.context_topic == "science" or temp_current_topic == "science":
                self.current_topic = random.choice(science_flairs).lower()
                self.context_topic = self.current_topic

        if temp_current_topic is "no_topic" and "answer_yes" in lexical_intent and self.research_counter <= 1:
            self.current_topic = self.context_topic
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.context_topic
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat[
                "research_counter"] = self.research_counter + 1
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            return "s_interesting_research"
        elif self.current_topic is not temp_current_topic and temp_current_topic is not "no_topic":
            self.current_topic = temp_current_topic
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.current_topic
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat["research_counter"] = 0
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            return "s_user_change_topic"
        else:
            self.context_topic = self.current_topic
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.current_topic
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat[
                "research_counter"] = self.research_counter + 1
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
        if temp_current_topic is not self.context_topic and "answer_yes" not in lexical_intent:
            return "s_techsci_chitchat"
        elif self.research_counter > 1:
            self.techsciencechat["research_counter"] = 0
            return "s_techsci_chitchat"
        elif "answer_no" in lexical_intent and (temp_current_topic is "no_topic" or temp_current_topic is not self.context_topic):
            self.techsciencechat["research_counter"] = 0
            return "s_techsci_chitchat"

    def s_techsci_chitchat(self):
        lexical_intent = detect_intent(self.text)
        if self.chitchat_counter == "":
            self.response += "Hmmm. I'm out for now. "
        else:
            self.response += template_techsci.utterance(selector='chitchat/{}'.format(self.chitchat_counter),
                                                        slots={
                                                            'topic': self.current_topic},
                                                        user_attributes_ref=self.template_manager_hash)
            # if "answer_yes" in lexical_intent and self.chitchat_counter not in ["q1", "q4", "a4"]:
            #    ack = " (Oh yeah|Yes)! I agree! "
            #    self.response = ack + self.response
        return "t_techsci_chitchat"

    def t_techsci_chitchat(self):
        if self.intent_classify_topic in self.module_mappings:
            return "s_user_change_module"
        temp_current_topic = detect_topic(
            self.knowledge, self.topic_keywords, self.noun_phrase, self.ner, self.text)
        lexical_intent = detect_intent(self.text)
        self.chitchat_counter = find_next_chitchat(self.chitchat_counter)
        if self.chitchat_counter == "end_chitchat":
            return "s_exit_techsciencechat_intentional"
        if self.chitchat_counter == "" and temp_current_topic is "no_topic":
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.current_topic
            self.techsciencechat["context_topic"] = self.context_topic
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            return "s_change_of_topic"
        elif self.chitchat_counter == "q4" and temp_current_topic is "no_topic":
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.current_topic
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat["context_topic"] = self.context_topic
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            return "s_ask_research"
        elif self.chitchat_counter != "" and temp_current_topic is "no_topic":
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["current_topic"] = self.current_topic
            self.techsciencechat["context_topic"] = self.context_topic
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            return "s_techsci_chitchat"

        if self.context_topic in self.broader_topics or temp_current_topic in self.broader_topics:
            if self.context_topic == "technology" or self.context_topic == "tech" \
                    or temp_current_topic == "technology" or temp_current_topic == "tech":
                self.current_topic = random.choice(technology_flairs).lower()
                self.context_topic = self.current_topic
            if self.context_topic == "science" or temp_current_topic == "science":
                self.current_topic = random.choice(science_flairs).lower()
                self.context_topic = self.current_topic

        if temp_current_topic is not self.context_topic and temp_current_topic is not "no_topic":
            self.context_topic = temp_current_topic
            self.current_topic = temp_current_topic
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.current_topic
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            return "s_user_change_topic"

    def s_ask_more(self):
        self.response += template_techsci.utterance(selector='ask_more/default',
                                                    slots={
                                                        'topic': self.current_topic},
                                                    user_attributes_ref=self.template_manager_hash)
        return "t_ask_finish"

    def t_ask_finish(self):
        if self.intent_classify_topic in self.module_mappings:
            return "s_user_change_module"
        temp_current_topic = detect_topic(
            self.knowledge, self.topic_keywords, self.noun_phrase, self.ner, self.text)
        lexical_intent = detect_intent(self.text)

        if self.context_topic in self.broader_topics or temp_current_topic in self.broader_topics:
            if self.context_topic == "technology" or self.context_topic == "tech" \
                    or temp_current_topic == "technology" or temp_current_topic == "tech":
                self.current_topic = random.choice(technology_flairs).lower()
                self.context_topic = self.current_topic
            if self.context_topic == "science" or temp_current_topic == "science":
                self.current_topic = random.choice(science_flairs).lower()
                self.context_topic = self.current_topic

        self.techsciencechat["propose_continue"] = "CONTINUE"
        self.techsciencechat["chitchat_counter"] = self.chitchat_counter
        if self.current_topic is not temp_current_topic and temp_current_topic is not "no_topic":
            self.current_topic = temp_current_topic
            self.context_topic = temp_current_topic
            self.techsciencechat["context_topic"] = self.context_topic
            self.techsciencechat["current_topic"] = self.current_topic
            return "s_user_change_topic"
        else:
            self.context_topic = self.current_topic
            self.techsciencechat["current_topic"] = self.current_topic
            return "s_ask_finish"
        return "s_ask_finish"

    def s_ask_finish(self):
        lexical_intent = detect_intent(self.text)
        if "answer_no" in lexical_intent or "answer_uncertain" in lexical_intent:
            self.response += template_techsci.utterance(selector='ask_finish/know_nothing',
                                                        slots={
                                                            'topic': self.current_topic},
                                                        user_attributes_ref=self.template_manager_hash)
        else:
            self.response += template_techsci.utterance(selector='ask_finish/default',
                                                        slots={
                                                            'topic': self.current_topic},
                                                        user_attributes_ref=self.template_manager_hash)
        return "t_change_of_topic"

    # Refactored
    def s_change_of_topic(self):
        # Conversation extender and gives us a clearer idea of the next
        # self.current_topic.
        self.response += template_techsci.utterance(selector='change_of_topic/default',
                                                    slots={
                                                        'topic': self.current_topic},
                                                    user_attributes_ref=self.template_manager_hash)
        return "t_change_of_topic"

    def t_change_of_topic(self):
        if self.intent_classify_topic in self.module_mappings:
            return "s_user_change_module"
        temp_current_topic = detect_topic(
            self.knowledge, self.topic_keywords, self.noun_phrase, self.ner, self.text)
        lexical_intent = detect_intent(self.text)
        if temp_current_topic is not "no_topic":
            self.current_topic = temp_current_topic
            self.context_topic = temp_current_topic
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.current_topic
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["context_topic"] = self.context_topic
            return "s_interesting_fact"
        elif "answer_no" in lexical_intent:
            return "s_exit_techsciencechat"

        if self.context_topic in self.broader_topics or temp_current_topic in self.broader_topics:
            if self.context_topic == "technology" or self.context_topic == "tech" \
                    or temp_current_topic == "technology" or temp_current_topic == "tech":
                self.current_topic = random.choice(technology_flairs).lower()
                self.context_topic = self.current_topic
            if self.context_topic == "science" or temp_current_topic == "science":
                self.current_topic = random.choice(science_flairs).lower()
                self.context_topic = self.current_topic

        if self.current_topic is "no_topic":
            self.current_topic = self.context_topic
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat["current_topic"] = self.context_topic
            self.techsciencechat["research_counter"] = self.research_counter
            return "s_interesting_fact"
        elif self.current_topic is not "no_topic":
            self.context_topic = self.current_topic
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat["current_topic"] = self.current_topic
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["context_topic"] = self.context_topic
            return "s_interesting_fact"
        else:
            return "s_exit_techsciencechat"

    def s_user_change_topic(self):
        self.response += template_techsci.utterance(selector='user_change_topic/default',
                                                    slots={
                                                        'topic': self.current_topic},
                                                    user_attributes_ref=self.template_manager_hash)
        self.context_topic = self.current_topic
        return "t_user_change_topic"

    def t_user_change_topic(self):
        if self.intent_classify_topic in self.module_mappings:
            return "s_user_change_module"
        temp_current_topic = detect_topic(
            self.knowledge, self.topic_keywords, self.noun_phrase, self.ner, self.text)
        lexical_intent = detect_intent(self.text)

        if self.context_topic in self.broader_topics or temp_current_topic in self.broader_topics:
            if self.context_topic == "technology" or self.context_topic == "tech" \
                    or temp_current_topic == "technology" or temp_current_topic == "tech":
                self.current_topic = random.choice(technology_flairs).lower()
                self.context_topic = self.current_topic
            if self.context_topic == "science" or temp_current_topic == "science":
                self.current_topic = random.choice(science_flairs).lower()
                self.context_topic = self.current_topic

        if "answer_yes" in lexical_intent:
            self.context_topic = self.current_topic
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.current_topic
            self.techsciencechat["context_topic"] = self.context_topic
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            self.techsciencechat["research_counter"] = self.research_counter
            return "s_interesting_fact"
        elif "answer_no" in lexical_intent:
            self.current_topic = self.context_topic
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.current_topic
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            self.techsciencechat["research_counter"] = self.research_counter
            return "s_change_of_topic"
        return "s_interesting_fact"

    def s_user_change_module(self):
        self.response += "Do you want to talk about " + \
            self.module_mappings[self.intent_classify_topic] + \
            " instead of science and technology?"
        self.techsciencechat["propose_continue"] = "UNCLEAR"
        self.techsciencechat["propose_topic"] = self.output_module_mappings[
            self.intent_classify_topic]
        self.techsciencechat["current_topic"] = self.current_topic
        self.techsciencechat["context_topic"] = self.context_topic
        self.techsciencechat["fact_counter"] = self.fact_counter
        self.techsciencechat["chitchat_counter"] = self.chitchat_counter
        self.response_dict["propose_topic"] = self.output_module_mappings[
            self.intent_classify_topic]
        return "t_resume"

    def t_user_change_module(self):
        lexical_intent = detect_intent(self.text)
        if "answer_yes" in lexical_intent and self.next_module is not "":
            self.techsciencechat[
                "propose_topic"] = self.output_module_mappings[self.next_module]
            self.response_dict["propose_topic"] = self.output_module_mappings[
                self.next_module]
            return "s_exit_techsciencechat"
        elif "answer_no" in lexical_intent:
            self.current_topic = self.context_topic
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.current_topic
            self.techsciencechat["context_topic"] = self.current_topic
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat["research_counter"] = self.research_counter
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            return "s_change_of_topic"
        else:
            self.current_topic = self.context_topic
            self.techsciencechat["propose_continue"] = "CONTINUE"
            self.techsciencechat["current_topic"] = self.current_topic
            self.techsciencechat["context_topic"] = self.current_topic
            self.techsciencechat["fact_counter"] = self.fact_counter
            self.techsciencechat["chitchat_counter"] = self.chitchat_counter
            self.techsciencechat["research_counter"] = self.research_counter
            return "s_change_of_topic"
        return "s_exit_techsciencechat"
