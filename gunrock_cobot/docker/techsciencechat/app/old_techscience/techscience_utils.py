import re
import random
import signal
import requests
import logging
import praw
import operator
import json
import boto3
import time
import redis
import hashlib
import techscience_retrieval as retrv

from cobot_common.service_client import get_client
from difflib import SequenceMatcher
from typing import Union, List, Dict, Tuple
from response_templates import template_data
from boto3.dynamodb.conditions import Key, Attr
from operator import itemgetter
from itertools import chain


COBOT_API_KEY='xgTjk23SRM9Q9VFrUEFOjav0Bn4ot21W8uFLEieT'
REDDIT_CLIENT_ID = "lKmLeMmSSLns6A"
REDDIT_CLIENT_SECRET = "Fi6V_pCzKqE-4TRVqcmpMJdET2s"
REDIS_DYNAMO_KEY = "gunrock:remote:techscience:dynamo"

dynamodb = boto3.resource('dynamodb', region_name='us-east-1')

table = dynamodb.Table('SciTech')

rds = redis.StrictRedis(host='52.87.136.90', port=16517,
                        socket_timeout=3,
                        socket_connect_timeout=1,
                        retry_on_timeout=True,
                        db=0, password="alexaprize", decode_responses=True)


def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()


topic_list = ["science", "technology", "tech", "space", "apple", "google", "ai", "a.i.", "a. i.", 
              "internet", "black hole", "blockchain", "bitcoin", "virtual reality", "artificial intelligence", "computer",
              "robot", "robots", "astronomy", "biology", "engineering", "physics", "chemistry", "medicine",
              "robotics", "nanotech", "biotech", "energy", "politics", "security", "software", "hardware", "medicines",
              "psychology", "social science", "anthropology", "nanoscience", "computer science", "epidemiology",
              "paleontology", "neuroscience", "animal science", "nano tech", "nano technology", "computers"]

broader_topics = ["science", "technology", "tech"]

blacklist = ["ass", "asshole", "anal", "anus", "arsehole", "arse", "bitch", "bangbros", "bastards",
            "bitch", "tits", "butt", "blow job", "boob", "bra", "cock", "cum", "cunt", "dick", "shit",
            "sex", "erotic", "fuck", "fxxk", "f\*\*k", "f\*\*c", "f\*ck", "fcuk", "gang bang", "gangbang",
            "genital", "damn", "horny", "jerk off", "jerkoff", "kill", "loser", "masturbate", "naked",
            "nasty", "negro", "nigger", "nigga", "orgy", "pussy", "penis", "perve", "rape", "fucked",
            "fucking",  "racist", "sexy", "strip club", "vagina", "faggot", "fag", "drug", "weed",
            "kill", "murder", "incel", "prostitute", "slut", "whore", "cuck", "cuckold", "shooting", "nazis",
            "nazism", "nazi", "sgw", "balls", "thot", "virgin", "cumshot", "cumming", "dildo", "vibrator", "sexual",
            "douche", "douche", "bukkake", "douchebag", "porn", "porngraph", "porngraphy", "died", "killed",
            "killing", "murdered", "murdering", "kills", "murders", "clit", "clitoris", "mushroom tatoo",
            "dammit", "wtf", "breasts", "tits", "boobies", "orgasm"]

technology_flairs = ['Software', 'Robotics', 'Hardware', 'Nanotech', 'Biotech', 'Energy', 'AI', 'Space']

science_flairs = ['Engineering', 'Astronomy', 'Health', 'Psychology', 'Social Science', 'Anthropology',
                  'Artificial Intelligence AMA', 'Nanoscience', 'Computer Science', 'Epidemiology', 'Paleontology',
                  'Biology', 'Environment', 'Physics', 'Chemistry', 'Economics', 'Neuroscience', 'Medicine',
                  'Animal Science']


class Timeout():
    """Timeout class using ALARM signal."""
    class Timeout(Exception):
        pass

    def __init__(self, sec):
        self.sec = sec

    def __enter__(self):
        signal.signal(signal.SIGALRM, self.raise_timeout)
        signal.alarm(self.sec)

    def __exit__(self, *args):
        signal.alarm(0)    # disable alarm

    def raise_timeout(self, *args):
        raise Timeout.Timeout()


def topic_mapper(topic):
    mappings = {"artificial intelligence": "ai",
                "a.i.": "ai",
                "a. i.": "ai",
                "robot": "robotics",
                "robots": "robotics",
                "planets": "space",
                "cosmology": "space",
                "planet": "space",
                "black hole": "space",
                "black holes": "space",
                "bitcoins": "bitcoin",
                "medicines": "medicine",
                "computer": "computer science",
                "computers": "computer science",
                "nano tech": "nanotech",
                "internet": "software",
                "nano technology": "nanotech"}
    if topic not in mappings:
        return topic
    if topic in mappings:
        return mappings[topic]
    return topic


def query_tag(tag):
    """
    Returns table data for database/graph ID. Dynamo partitioned on graph
    id
    :param id: utf8 hash
    :return: data for hash
    """
    tag = topic_mapper(tag)
    try:
        response = table.query(
            IndexName='tag',
            KeyConditionExpression=Key('tag').eq(tag),
            ScanIndexForward=False,
        )
        if response:
            response = response['Items']
    except:
        response = []
    final_response = QA(response)
    return final_response


def query_keywords(word_list: list, threadDict: dict = {}, full=False) -> list:
        # full = True
        SCANLIMIT = 20
        start = time.time()
        # word_list = [word for line in word_list for word in line.split()]
        word_list = list(set(chain(*map(str.split, word_list))))
        fe = Attr('keywords').contains(word_list[0].lower())
        for word in word_list[1:]:
            fe = fe | Attr('keywords').contains(word.lower())
        response = table.scan(
            FilterExpression=fe,
        )
        resp = response['Items']
        i = 1
        while 'LastEvaluatedKey' in response:
            response = table.scan(ExclusiveStartKey=response['LastEvaluatedKey'], FilterExpression=fe)
            resp.extend(response['Items'])
            if i == SCANLIMIT:
                break
            i += 1
        scoredict = {}
        for i in range(1, len(word_list) + 1):
            scoredict[i] = []
        for r in resp:
            score = 0
            scoredict[score] = []
            for w in word_list:
                if w in r['keywords']:
                    score += 1
            scoredict[score].append(r)
        final = []
        for s in sorted(scoredict.keys(), reverse=True):
            scorelist = scoredict[s]
            scorelist = sorted(scorelist, key=itemgetter('modified'), reverse=True)
            final.extend(scorelist)
        final2 = []
        for each in final:
            if each['toLabel'] == 'PERSON' and each['fromLabel'] == 'PERSON':
                final2.append(each)
        if not final2:
            final2 = final
        final2 = QA(final2)
        stop = time.time()
        threadDict['keywords'] = final2
        return final2


def QA(response):
    SUBLIMITTITLE = 40
    SENTENCEMAX = 300
    SENTENCEMIN = 30
    l = []
    for each in response:
        try:
            revisedsents = []
            sents = each['title']
            s, n = re.subn(r'[^\w\s\.\']', '', sents)
            if n<SUBLIMITTITLE and len(s) < SENTENCEMAX and len(s) > SENTENCEMIN:
                revisedsents.append(s)
            else:
                continue #optional could stop and not put in the body
            each['title'] = revisedsents[0]
        except KeyError:
            pass
        l.append(each)
    return l


def retrieve_kg_topic(response):
    final_list = list()
    count = 0
    if not response:
        return ""
    for resp in response:
        if not any(re.search(x, resp["title"]) for x in blacklist):
            if "title" in resp:
                if resp["title"][len(resp["title"]) - 1] != '.':
                    period_title = resp["title"] + '.'
                    final_list.append(period_title)
                else:
                    final_list.append(resp["title"])
    return_resp = str(random.choice(final_list)).replace(" mit ", " M.I.T ")
    while get_redis(REDIS_DYNAMO_KEY, return_resp) is not None and count < 150:
        return_resp = str(random.choice(final_list))
        count += 1
    set_redis_with_expire(REDIS_DYNAMO_KEY, return_resp, return_resp, expire = 3 * 30 * 86400)
    return return_resp 

def get_interesting_reddit_posts(text, limit):
    submissions = retrieve_reddit(text, ['todayilearned'], limit)
    if submissions != None:
        results = [submissions[i].title.replace("TIL", "").lstrip() for i in range(len(submissions))]
        results = [results[i] + '.' if results[i][-1]!='.' and results[i][-1]!='!' and results[i][-1]!='?' else results[i] for i in range(len(results))]
        #Sensitive Words Pattern
        final_results = []
        for result in results:
            if not any(re.search(x, result) for x in blacklist):
                final_results.append(result)
        if len(final_results) > 0:
            return final_results
    return None


def detect_sys_intent(text, dialog_act):
    sys_intents = []
    text = text.lower()
    if re.search(r"(stop|don't|do not).* (talk|discuss|tell.* me).* \bi\b.* (hate|don't|do not|never).* |(talk|discuss|tell.* me).* about.* something else", text):
        sys_intents.append("exit_techscienceRG")
    elif re.search(r"((stop|quit).* (talk|tell|chat|inform).* (about|on).* (science|technology|tech)).*|(.* (hate|not interested|dislike)).* (science|this|technology|tech)", text):
        sys_intents.append("exit_techscienceRG")
    if re.search(r"((can|may) .* (ask|tell) .* question)|((i have a question))", text):
        sys_intents.append("user_ask_question")
    if detect_question(dialog_act):
        sys_intents.append("question")
    if len(sys_intents) == 0:
        return None
    return sys_intents


def detect_question(dialog_act):
    for each in dialog_act:
        if 'DA' in each:
            if "question" in each['DA']:
                return True
    return False

ask_intents = {
    "ask_yesno", "ask_preference", "ask_opinion", "ask_advice", "ask_ability", "ask_hobby",
    "ask_recommend", "ask_reason", "ask_self", "ask_fact", "ask_freq", "ask_dist", "ask_loc",
    "ask_count", "ask_degree", "ask_time", "ask_person", "ask_name",
}

ans_intents = {
    "ans_unknown", "ans_neg", "ans_pos", "ans_like", "ans_wish", "ans_factopinion",
}


def detect_intent(text):
    """
    Detect intents
    TODO: instead of if elif hierarchy, try all regex and add to dictionary successful matches
    """
    # System level: when this is returned to state response function, start new dialogue flow
    sys_intents = list()
    text = text.lower()
    if re.search(r"(give.* (recommendation|suggestion)|(suggest|recommend).* me .* (suggest|recommend)|(^what|tell me).* (good|popular).* )", text):
        sys_intents.append("ask_recommend")
    elif re.search(r"(can\b|will\b).*(talk|tell|discuss|chat).* about|talk|favorite ", text):
        sys_intents.append("req_topic")
    elif re.search(r"(^who\b|^what\b).* (enhancement|progress|research|development|advancement) .*", text):
        sys_intents.append("req_research")

    # Lexical level: narrow to broad
    elif re.search(r"\byes\b|\bya\b|\bokay\b|\bcool\b|\binteresting\b|\bcontinue\b|\bwhatever\b|\babsolutely\b|\bOK\b|\byeah\b|\bsure\b", text):
        sys_intents.append("answer_yes")
    elif re.search(r"\bno\b|\bnone\b|\bnothing\b|\bnot\b|\bdunno\b|\bnope\b.*(don't|do not|now).*(know|like|have|enjoy|want)", text):
        sys_intents.append("answer_no")
    elif re.search(r"(\bi\b|\bi'm\b).* (not.* sure|unsure|uncertain|(don't|do not).* know|(don't|do not).* have)", text): # asking question at beginning of utterance
        sys_intents.append("answer_uncertain")
    elif re.search(r"^who\b|^what\b|^when\b|^when\b|^where\b|^why\b|^how\b", text): # asking question at beginning of utterance
        sys_intents.append("ask_question")
    elif re.search(r"^will\b|^can\b|.*(talk|chat|discuss|tell).*(science|technology|artificial intelligence|a.i.|robot)", text):
        sys_intents.append("req_scitech")
    elif re.search(r"(talk|chat).* about|(\bit|is).* a", text) and [text for topic in topic_list if topic in text]:
        sys_intents.append("req_scitech")
    return sys_intents


def handle_sys_intent(input, intent):
    if intent == "exit_techsciencechat":
        return {
            "next": "s_exittechscienceRG",
            "response": "Ok. ",
            "context": input["currentstate"],
            "syscode": "exit" # cobot can potentially use this to overwrite reponse or whatever
        }
    elif intent == "ask_recommend":
        return {
            # TODO: should make handle_sys_intent a method of TechScienceAutomaton so I can call it's response functions
            "next": "s_startKG",
            "response": "Google Assistant was recently able to make phone calls and book appointments for people. You should check that out. ",
        }
    return None


def handle_intent(text, intent):
    if intent == "ask_opinion" or intent == "ask_question":
        if re.search(r"you.* favorite.* you.* (like|love).* ", text):
            return "One of my favorite topics is blockchains due to its ability to decentralize systems and increase trustworthiness."
        elif re.search(r"favorite.* (science|tech|technology)", text):
            return "I have always been inclined towards knowing more about artificial intelligence."
        elif re.search(r"favorite.* (tech|person|scientist|personality)", text):
            return "Although numerous scientists have helped us through their research, Nikola Tesla will always be my favorite."
        else:
            evidata = EVI_bot(text)
            if evidata:
                return evidata
            # else:
            #     redditdata = retrieve_bot(text)
            #     if redditdata:
            #         return redditdata
            # return "Hm. You have me stumped. "

    return "I would like to reserve my opinion on this for now. However, we could talk about something else. " \
           "What do you wish to talk about? "


# def retrieve_bot(utterance):
#     r = requests.post('http://ec2-54-242-153-127.compute-1.amazonaws.com:8085/embedding', data = utterance)
#     if r != None and r.text != None:
#         data = r.json()
#         if float(data["sim_score"]) > 0.75:
#             return data["comment"] + " "
#     return None

def retrieve_scitech(utterance):
    retrieve = retrv.TopicRetrievalResponseGenerator()
    response_dict = retrieve.execute(utterance)
    if response_dict:
        return response_dict['response']
    return ""


def EVI_bot(utterance):
    try:
        client = get_client(api_key=COBOT_API_KEY)
        r = client.get_answer(question=utterance, timeout_in_millis=1000)
        if r["response"] == "" or re.search(r"skill://", r["response"]):
            return None
        return r["response"] + " "
    except Exception as e:
        logging.info("[TECHSCIENCECHAT_MODULE] Timeout in EVI_bot {}".format(e))
        return None


def retrieve_reddit(utterance, subreddits, limit):
    # cuurently using gt's client info
    try:
        reddit = praw.Reddit(client_id=REDDIT_CLIENT_ID,
                             client_secret=REDDIT_CLIENT_SECRET,
                             user_agent='extractor',
                             timeout=2
                             # username='gunrock_cobot', # not necessary for read-only
                             # password='ucdavis123'
                             )
        subreddits = '+'.join(subreddits)
        subreddit = reddit.subreddit(subreddits)
        # higher limit is slower; delete limit arg for top 100 results
        submissions = list(subreddit.search(utterance, limit=limit))
        if len(submissions) > 0:
            return submissions
        return None
    except Exception as e:
        logging.error("Fail to retrieve reddit. error msg: {}. user utterance: {}".format(e, utterance))
        return None


'''
Deprecated dbpedia retrieval. Needs a better selection strategy from the bunch of responses.
def retrieve_dbpedia(topic):
    data = [
        ('query', topic),
        ('numResults', '4'),
    ]
    try:
        response = requests.post('http://cobot-loadb-1rhfajrzotei8-1490558674.us-east-1.elb.amazonaws.com/getData',
                                 data=data)
        return response
    except:
        return ""
'''


def construct_dynamic_response(response):
    all_choices = ""
    flag = False
    if response is not None:
        for char in response:
            if char == '(':
                flag = True
            elif char == ')':
                flag = False
            if flag and char != '(':
                all_choices += char
            elif not flag and char == ')':
                selected_response = random.choice(all_choices.split('|'))
                replaceable = '(' + all_choices + char
                response = response.replace(replaceable, selected_response)
                all_choices = ""
        response[0].capitalize()
    return response


def map_topics(extracted_topic_list):
    new_list = list()
    ai_topics = ["artificial intelligence", "ai", "a.i.", "a. i."]
    for i in range(len(extracted_topic_list)):
        if extracted_topic_list[i] in ai_topics:
            new_list.append("artificial intelligence")
        else:
            new_list.append(extracted_topic_list[i])
    return new_list


def detect_topic(knowledge, topic_keywords, noun_phrase, ner, input_data):
    possible_topics = set()
    if ner:
        possible_topics.add(ner[0]["text"])
    if topic_keywords:
        possible_topics.add(topic_keywords[0]["keyword"])
    if noun_phrase:
        for noun in noun_phrase:
            possible_topics.add(noun)
    allowed_list = [each for each in possible_topics if each in topic_list]
    if allowed_list:
        new_allowed_list = map_topics(allowed_list)
        if len(new_allowed_list) == 1:
            return new_allowed_list[0]
        elif len(new_allowed_list) > 1:
            single_out = [topic for topic in new_allowed_list if topic not in broader_topics]
            if single_out:
                return single_out[0]
            else:
                return "science"
    else:
        input_list = input_data.split()
        allowed_list = [each for each in input_list if each in topic_list]
        if allowed_list:
            if len(allowed_list) == 1:
                return allowed_list[0]
            elif len(allowed_list) > 1:
                single_out = [topic for topic in allowed_list if topic not in broader_topics]
                if single_out:
                    return single_out[0]
                else:
                    return "science"
    return "no_topic"


def topic_exists_in_opinion(topic):
    data = template_data.data
    topic = topic.replace(" ", "_")
    if topic + "_topic" not in data["techscience"]["opinion"]:
        return False
    else:
        return True


def topic_exists_in_favorite(topic):
    data = template_data.data
    topic = topic.replace(" ", "_")
    if topic + "_topic" not in data["techscience"]["favorite"]:
        return False
    else:
        return True


def topic_exists(topic):
    data = template_data.data
    if topic not in data["techscience"]["topics"]:
        return False
    else:
        return True


def find_next_chitchat(counter):
    if counter == "":
        return "q1"

    qno = int(counter[1])
    if counter[0] == "q" and qno <= 7:
        return "a" + str(qno)
    elif counter[0] == "a" and qno < 7:
        return "q" + str(qno + 1)
    else:
        return "end_chitchat"

def find_next_chitchat_first(counter):
    if counter == "":
        return "q1"

    qno = int(counter[1])
    if counter[0] == "q" and qno <= 3:
        return "a" + str(qno)
    elif counter[0] == "a" and qno < 3:
        return "q" + str(qno + 1)
    else:
        return ""

def set_redis_with_expire(prefix, input_str, output, expire=24 * 60 * 60):
    logging.info(
        '[REDIS] set redis with expire input: {} output: {}, expire {}'.format(
            input_str, output, expire))
    hinput = hashlib.md5(input_str.encode()).hexdigest()
    return rds.set(prefix + ':' + hinput, json.dumps(output),
                   nx=True, ex=expire)


def get_redis(prefix, input_str):
    logging.info('[REDIS] get redis  input: {}'.format(input_str))
    hinput = hashlib.md5(input_str.encode()).hexdigest()
    results = rds.get(prefix + ':' + hinput)
    if results is None:
        return None
    return json.loads(results)

def utterance(key: List[str], get_all: bool=False):
    """
    :param key: a list of str that represents the nested dict keys.
                e.g. {key1: {key2: {key3: text}}}, you do ['key1', 'key2', 'key3']
    :param get_all:
    :return: either a list, or an instance of, the utterance, and the hash of the utterance
    terry - not using hash right now because my template format has some qa pairs that don't work with hash
    """
    # return "what what", "hash"
    try:
        from techscience_templates_data import data
        data = data[key[0]]
        for i in key[1:]:
            data = data[i]

        data = [d['text'] for d in data]

        if get_all:
            return data  # type: List[Tuple[str, str]]
        else:
            return random.choice(data)  # type: Tuple[str, str]

    except Exception as e:
        # To keep cobot running, catch everything!
        return "Oh no, I think my creators misspelled something in my code. Would you mind asking something else?"
