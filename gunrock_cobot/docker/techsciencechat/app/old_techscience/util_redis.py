import redis
import logging

import praw
import random
from datetime import datetime, timedelta
from fuzzywuzzy import fuzz
import hashlib
import json
from user_profiler.user_profile import Gender
from typing import List


########################################################################################

#  IMPORTANT: DO NOT MODIFY THIS FILE IF IT IS IN REMOTE MODULE (ie. under docker folder)

#  IF you want to change this file, please modify the one directly under gunrock_cobot folder
#  and run "python3 util_redis.py" to copy the file to all remote modules

########################################################################################

class RedisHelper:
    SERVER_IP = '52.87.136.90'

    COMMON_NAME_KEY = "gunrock:name:set"
    NAME_KEY_PREFIX = "gunrock:name:"
    NAME_FIELD_GENDER = "gender"

    HOLIDAY_KEY = "gunrock:ic:holiday:ts"
    HOLIDAY_PREFIX_KEY = "gunrock:ic:holiday:"

    PREFIX_ASR = "gunrock:asrcorrection:data"
    PREFIX_NPKNOWLEDGE = "gunrock:npknowledge:data"

    PREFIX_RETRIEVAL = "gunrock:retrieval:data"
    RETRIEVAL_LIST_KEY = "gunrock:ic:retrieval:apikeys"

    RANDOM_THOUGHT_KEY = "gunrock:randomthoughts"

    PREFIX_MUSIC_ARTIST = 'module:music:artist:'
    MUSIC_GENRE_KEY = 'gunrock:module:music:genre'
    PREFIX_MUSIC_USER = 'gunrock:module:music:user:'

    def __init__(self):
        self.r = redis.StrictRedis(
            host=self.SERVER_IP,
            port=16517,
            db=0,
            password="alexaprize",
            charset='utf-8',
            decode_responses=True)

    def get_client(self):
        return self.r

    def get(self, prefix, input_str):
        logging.info('[REDIS] get redis input: {}'.format(input_str))
        hinput = hashlib.md5(input_str.encode()).hexdigest()
        results = self.r.get(prefix + ':' + hinput)
        if results is None:
            return None
        return json.loads(results)

    def set_with_expire(self, prefix, input_str, output, expire=24 * 60 * 60):
        logging.info(
            '[REDIS] set redis with expire input: {} output: {}, expire {}'.format(
                input_str, output, expire))
        hinput = hashlib.md5(input_str.encode()).hexdigest()
        return self.r.set(prefix + ':' + hinput, json.dumps(output), nx=True, ex=expire)

    def is_common_name(self, name):
        return self.is_common_english_name(name) or self.is_common_english_indian_european_name(name)

    def is_common_english_name(self, name):
        return self.r.sismember(self.COMMON_NAME_KEY, name.lower())

    def is_common_english_indian_european_name(self, name):
        return self.r.exists(self.NAME_KEY_PREFIX + name.lower())

    def get_predicted_gender(self, name):
        gender = self.r.hget(self.NAME_KEY_PREFIX + name.lower(), self.NAME_FIELD_GENDER)

        if not gender:
            gender = '3'

        gender_dict = {'0': Gender.FEMALE, '1': Gender.MALE, '3': None}

        return gender_dict[gender]

    # [start] reddit key
    def list_reddit_keys(self):
        return self.r.smembers(self.RETRIEVAL_LIST_KEY)

    def get_reddit_keys_length(self):
        return self.r.llen(self.RETRIEVAL_LIST_KEY)

    def get_reddit_key(self):
        """get the first one from a list of reddit keys and round robin"""
        return self.r.brpoplpush(self.RETRIEVAL_LIST_KEY, self.RETRIEVAL_LIST_KEY)
    # [end] reddit key


    # [start] holiday
    def scan_related_holiday(self, keyword, threshold=70):
        key = None
        for holiday in self.r.zscan_iter(self.HOLIDAY_KEY):
            if fuzz.token_sort_ratio(holiday[0], keyword) > threshold:
                key = holiday[0]
                break
        if key is None:
            return None
        return self.r.hgetall(self.HOLIDAY_PREFIX_KEY + key)

    def get_future_holiday(self, category=None, limit=5):
        # FIXME: safe way to make sure it include today
        second_before_today = int(
            (datetime.utcnow() - timedelta(hours=24)).strftime("%s")) - 1
        keys = self.r.zrangebyscore(
            self.HOLIDAY_KEY, second_before_today, 'inf', num=limit, start=0)
        ret = []
        for key in keys:
            holiday_data = self.r.hgetall(self.HOLIDAY_PREFIX_KEY + key)
            if category is None or (
                    "Category" in holiday_data and category == holiday_data["Category"]):
                ret.append(holiday_data)
        return ret

    def get_all_holiday(self, category=None, limit=5):
        # FIXME: safe way to make sure it include today
        keys = self.r.zrangebyscore(self.HOLIDAY_KEY, 0, 'inf', num=limit, start=0)
        ret = []
        for key in keys:
            holiday_data = self.r.hgetall(self.HOLIDAY_PREFIX_KEY + key)

            if category is None or (
                    "Category" in holiday_data and category == holiday_data["Category"]):
                ret.append(holiday_data)
        return ret
    # [end] holiday

    # [start] nlu
    def get_asr(self, phonetics_type):
        return self.get(self.PREFIX_ASR, phonetics_type)

    def get_npknowledge(self, input_str):
        return self.get(self.PREFIX_NPKNOWLEDGE, input_str)
    # [end] nlu

    # [start] response generator
    def get_retrieval(self, input_str):
        return self.get(self.PREFIX_RETRIEVAL, input_str)

    def get_random_thoughts(self, keyword):
        logging.info(
            "[Utils] Get random thoughts with input word: {} and redis key {}".format(
                keyword, self.RANDOM_THOUGHT_KEY))
        resp = self.r.hget(self.RANDOM_THOUGHT_KEY, keyword.lower().strip())
        if resp is None or resp == '':
            return None
        return resp

    # [end] response generator


    # [start] music
    def is_known_artist(self, artist):
        return self.r.exists(self.PREFIX_MUSIC_ARTIST + artist)

    def is_known_genre(self, genre):
        return self.r.sismember(self.MUSIC_GENRE_KEY, genre)

    def is_returning_user_in_music_module(self, user_id):
        return self.r.exists(self.PREFIX_MUSIC_USER + user_id)

    def get_user_music_profile(self, user_id):
        return self.r.get(self.PREFIX_MUSIC_USER + user_id)

    def set_user_music_profile(self, user_id, user):
        return self.r.set(self.PREFIX_MUSIC_USER + user_id, json.dumps(user))
    # [end] music
