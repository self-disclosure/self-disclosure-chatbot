"""For every csv file in test_cases, generated_unittest_csv
Run Automaton to ensure no breaks
"""
import os
import random
import unittest
from tests.daily_conv_unittest.daily_unittest_utils import DailyStatisticsCSV
from techscience_automaton import TechScienceAutomaton

class TestDailyUnitTest(unittest.TestCase):
    """ Test TopicUtils will focus on testing Topic Handler
        The goal of topic handler is to take in a text and
        1. Extract the NLU features that identify the topic user wants to talk about
        2. Map the user input to the corresponding resonse templates
     """

    def test_simple_nlu(self):
        """
        """
        test_cases = DailyStatisticsCSV()
        generator_iterator = test_cases.generator()
        for conv_id, generator in generator_iterator:
            automaton = TechScienceAutomaton()
            for input_data in generator:
                automaton.transduce(input_data)
                break
            


if __name__ == '__main__':
    unittest.main()