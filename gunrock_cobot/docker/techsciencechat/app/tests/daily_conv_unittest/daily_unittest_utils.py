
import pandas as pd
import os
import json
class DailyStatisticsCSV:
    def __init__(self, data_dir="../test_cases/generated_unittest_csv"):
        self.files = [os.path.join(data_dir, f) for f in os.listdir(data_dir)]

    def generator(self):
        for files in self.files:
            d = self.extract_from_csv(files)
            for conv_id in d:
                dialog = d[conv_id]
                yield conv_id, self._conv_generator(dialog)
                
    def _conv_generator(self, dialog):
        for l in dialog:
            yield l

    def remove_all_single_quotes(self, s):
        return s.replace("{'", "{\"").replace("'}", "\"}")\
                                          .replace("',", "\",").replace("':", "\":")\
                                          .replace(" '", " \"").replace("['", "[\"").replace("']", "\"]")
    def extract_from_csv(self, filename):
        df = pd.read_csv(filename).set_index("conversation_id")
        extract_fields = ["text", "a_b_test", "last_module", "returnnlp", "central_elem"]
        result = {}
        for i, row in df.iterrows():
            if i not in result:
                result[i] = []
            row_dict = dict(row)
            for k in {**row_dict}:
                if k not in extract_fields:
                    del row_dict[k]
                else:
                    if k == "central_elem":
                        row_dict[k] = json.loads(self.remove_all_single_quotes(row_dict[k]))
                        continue
                    if k == "returnnlp":
                        row_dict[k] = json.loads(row_dict[k])
            row_dict["template_manager"] = {}
            row_dict["techsciencechat"] = {}
            result[i].append(row_dict)

            
            break
            # print(group["a_b_test"])
            # print(group["returnnlp"])
            # print(group["central_elem"])
            # print("\n")
        # For each conversation id, we need to generate the input_data
        return result

    