"""Intro Techscience"""

# fav_tech_intro = {1: {'text': ['yes i like technology', 'no'], 'features': [{'intent_classify': {'sys': [], 'topic': [], 'lexical': []}, 'text': 'yes i like technology', 'spacynp': ['technology'], 'nounphrase2': {'noun_phrase': [], 'local_noun_phrase': []}, 'googlekg': [['technology', 'technology', 'University in Zürich, Switzerland', '97.959839', '__EMPTY__']], 'central_elem': {'senti': None, 'regex': {'sys': [], 'topic': [], 'lexical': []}, 'DA': None, 'DA_score': None, 'module': [], 'np': [], 'text': '', 'backstory': {'text': 'Hum, I have never thought about that. ', 'confidence': 0}}, 'dependency_parsing': {'dependency_parent': [], 'dependency_label': [], 'pos_tagging': []}, 'sentence_completion_text': 'yes i like technology', 'amazon_topic': 'Science_and_Technology', 'coreference': None, 'intent': 'general', 'converttext': 'yes i like technology', 'ner': None, 'concept': None, 'topic2': None, 'sentiment': 'pos', 'topic': [{'topicClass': 'SciTech', 'confidence': 999.0, 'text': 'yes i like technology', 'topicKeywords': [{'confidence': 999.0, 'keyword': 'technology'}]}], 'npknowledge': {'knowledge': ['technology', 'technology', 'University in Zürich, Switzerland', '97.959839', '__EMPTY__'], 'noun_phrase': [], 'local_noun_phrase': []}, 'returnnlp': None, 'dialog_act': None, 'intent_classify2': [{'sys': [], 'topic': [], 'lexical': []}], 'sentiment2': None, 'sentiment_allennlp': None, 'segmentation': {'segmented_text': [], 'segmented_text_raw': ['']}, 'asrcorrection': None, 'topic_module': '', 'topic_keywords': [{'confidence': 999.0, 'keyword': 'technology'}], 'knowledge': ['technology', 'technology', 'University in Zürich, Switzerland', '97.959839', '__EMPTY__'], 'noun_phrase': []}, None], 'returnnlp': [None, [{'sentiment': {'neg': 0.9993, 'compound': 0, 'pos': 0.0007, 'neu': 0}, 'googlekg': [], 'pos_tagging': ['INTJ'], 'concept': [], 'amazon_topic': 'Other', 'dependency_parsing': {'head': [0], 'label': ['det']}, 'topic': {'topicKeywords': [], 'text': 'no', 'topicClass': 'Phatic', 'confidence': 999}, 'text': 'no', 'noun_phrase': [], 'dialog_act': ['neg_answer', '0.4601633548736572'], 'intent': {'sys': ['regex_neg_answer'], 'topic': [], 'lexical': ['ans_neg']}}]], 'central_elem': [{'senti': None, 'regex': {'sys': [], 'topic': [], 'lexical': []}, 'DA': None, 'DA_score': None, 'module': [], 'np': [], 'text': '', 'backstory': {'text': 'Hum, I have never thought about that. ', 'confidence': 0}}, {'backstory': {'question': 'why do you ask me', 'confidence': 0.5005818009376526, 'text': 'Oh, I was just trying to get to know more about you. '}, 'regex': {'sys': ['regex_neg_answer'], 'topic': [], 'lexical': ['ans_neg']}, 'np': [], 'DA_score': '0.4601633548736572', 'senti': 'neg', 'module': [], 'text': 'no', 'DA': 'neg_answer'}], 'a_b_test': ['B', 'B'], 'last_response': ['<prosody rate = "60%">so</prosody> <break time = "300ms"></break> that reminds me: <break time="150ms" /> are you interested in science? I love talking about Artificial Intelligence.. <break time="150ms" /> How about you?', 'Nice! I\'m glad you\'re having a good day! <break time="150ms" /> What do you like to do for fun?'], 'suggest_keywords': None, 'last_module': 'SOCIAL', 'template_manager': {'prev_hash': {'gN4J9Q': ['/WcXSg', 'CyUXZA'], 'yUMMjg': ['DtwXiw'], 'PPsGsg': ['zforzw'], 'COcM3A': ['ak1JUw'], 'rJ8LnQ': [], 'uLUL9Q': [], 'is0KPQ': []}, 'selector_history': ['social::backstory/no_match']}, 'techsciencechat': {'propose_continue': 'STOP'}, 'usr_name': 'sam', 'module_selection': {'module_rank': {'MUSICCHAT': 0, 'GAMECHAT': 0, 'SPORT': 0, 'FASHIONCHAT': 0, 'TECHSCIENCECHAT': 0, 'TRAVELCHAT': 0, 'NEWS': 0, 'BOOKCHAT': 0, 'ANIMALCHAT': 0, 'FOODCHAT': 0, 'MOVIECHAT': 0}, 'used_topic': ['LAUNCHGREETING', 'SOCIAL', 'TECHSCIENCECHAT']}, 'system_acknowledgement': None},
#                   2: {'text': ['i like technology because it makes me happy', 'yes i like technology'], 'features': [{'ner': None, 'asrcorrection': None, 'intent': 'general', 'spacynp': ['technology'], 'intent_classify': {'sys': [], 'topic': ['topic_techscience'], 'lexical': ['ans_pos', 'ans_like']}, 'coreference': {'text': 'i like technology because i like technology makes me happy', 'replace': {'it': ['technology', 'i like technology']}, 'kg': [[['technology', 'University in Zürich, Switzerland', '97.959839', '__EMPTY__']]], 'replace_ne': 'i like technology', 'segmented_text': ['i like technology because i like technology makes me happy']}, 'dialog_act': [{'text': 'i like technology because it makes me happy', 'DA': 'opinion', 'confidence': 0.9844413995742798}], 'returnnlp': [{'text': 'i like technology because i like technology makes me happy', 'dialog_act': ['opinion', '0.9844413995742798'], 'intent': {'sys': [], 'topic': ['topic_techscience'], 'lexical': ['ans_pos', 'ans_like']}, 'sentiment': {'compound': 0, 'neg': 0.0016, 'neu': 0, 'pos': 0.9984}, 'googlekg': [['technology', 'University in Zürich, Switzerland', '97.959839', '__EMPTY__']], 'noun_phrase': ['technology'], 'topic': {'topicClass': 'SciTech', 'confidence': 999.0, 'text': 'i like technology because it makes me happy', 'topicKeywords': [{'confidence': 999.0, 'keyword': 'technology'}]}, 'concept': [{'noun': 'technology', 'data': {'factor': '0.4559', 'topic': '0.2873', 'area': '0.2568'}}], 'amazon_topic': 'Other', 'dependency_parsing': {'head': [2, 0, 2, 6, 6, 2, 6, 6], 'label': ['nsubj', 'acl:relcl', 'xcomp', 'mark', 'nsubj', 'xcomp', 'obj', 'obj']}, 'pos_tagging': ['PRON', 'VERB', 'NOUN', 'SCONJ', 'PRON', 'VERB', 'PRON', 'ADJ']}], 'topic': [{'topicClass': 'SciTech', 'confidence': 999.0, 'text': 'i like technology because it makes me happy', 'topicKeywords': [{'confidence': 999.0, 'keyword': 'technology'}]}], 'nounphrase2': {'noun_phrase': [['technology']], 'local_noun_phrase': [['technology']]}, 'sentiment_allennlp': [{'compound': 0, 'neg': 0.0016, 'neu': 0, 'pos': 0.9984}], 'text': 'i like technology because it makes me happy', 'intent_classify2': [{'sys': [], 'topic': ['topic_techscience'], 'lexical': ['ans_pos', 'ans_like']}], 'sentence_completion_text': 'i like technology because it makes me happy', 'segmentation': {'segmented_text': ['i like technology because it makes me happy'], 'segmented_text_raw': ['i like technology because it makes me happy']}, 'amazon_topic': 'Other', 'topic2': [{'topicClass': 'SciTech', 'confidence': 999.0, 'text': 'i like technology because it makes me happy', 'topicKeywords': [{'confidence': 999.0, 'keyword': 'technology'}]}], 'concept': [{'noun': 'technology', 'data': {'factor': '0.4559', 'topic': '0.2873', 'area': '0.2568'}}], 'central_elem': {'senti': 'pos', 'regex': {'sys': [], 'topic': ['topic_techscience'], 'lexical': ['ans_pos', 'ans_like']}, 'DA': 'opinion', 'DA_score': '0.9844413995742798', 'module': ['TECHSCIENCECHAT'], 'np': ['technology'], 'text': 'i like technology because i like technology makes me happy', 'backstory': {'confidence': 0.5469762086868286, 'followup': '', 'module': 'TECHSCIENCECHAT', 'neg': '', 'pos': '', 'postfix': '', 'question': 'Why do you like science and technology', 'reason': '', 'tag': '', 'text': "without science and technology, i wouldn't be. i'm a big fan of ai. for obvious reasons."}}, 'googlekg': [[['technology', 'University in Zürich, Switzerland', '97.959839', '__EMPTY__']]], 'sentiment': 'compound', 'dependency_parsing': {'dependency_parent': [[2, 0, 2, 6, 6, 2, 6, 6]], 'dependency_label': [['nsubj', 'acl:relcl', 'xcomp', 'mark', 'nsubj', 'xcomp', 'obj', 'obj']], 'pos_tagging': [['PRON', 'VERB', 'NOUN', 'SCONJ', 'PRON', 'VERB', 'PRON', 'ADJ']]}, 'npknowledge': {'knowledge': [['technology', 'University in Zürich, Switzerland', '97.959839', '__EMPTY__']], 'noun_phrase': ['technology'], 'local_noun_phrase': ['technology']}, 'converttext': 'i like technology because it makes me happy', 'sentiment2': [{'neg': '0.0', 'neu': '0.446', 'pos': '0.554', 'compound': '0.7351'}], 'topic_module': '', 'topic_keywords': [{'confidence': 999.0, 'keyword': 'technology'}], 'knowledge': [['technology', 'University in Zürich, Switzerland', '97.959839', '__EMPTY__']], 'noun_phrase': ['technology']}, None], 'returnnlp': [[{'text': 'i like technology because i like technology makes me happy', 'dialog_act': ['opinion', '0.9844413995742798'], 'intent': {'sys': [], 'topic': ['topic_techscience'], 'lexical': ['ans_pos', 'ans_like']}, 'sentiment': {'compound': 0, 'neg': 0.0016, 'neu': 0, 'pos': 0.9984}, 'googlekg': [['technology', 'University in Zürich, Switzerland', '97.959839', '__EMPTY__']], 'noun_phrase': ['technology'], 'topic': {'topicClass': 'SciTech', 'confidence': 999.0, 'text': 'i like technology because it makes me happy', 'topicKeywords': [{'confidence': 999.0, 'keyword': 'technology'}]}, 'concept': [{'noun': 'technology', 'data': {'factor': '0.4559', 'topic': '0.2873', 'area': '0.2568'}}], 'amazon_topic': 'Other', 'dependency_parsing': {'head': [2, 0, 2, 6, 6, 2, 6, 6], 'label': ['nsubj', 'acl:relcl', 'xcomp', 'mark', 'nsubj', 'xcomp', 'obj', 'obj']}, 'pos_tagging': ['PRON', 'VERB', 'NOUN', 'SCONJ', 'PRON', 'VERB', 'PRON', 'ADJ']}], [{'sentiment': {'neg': 0.005, 'compound': 0, 'pos': 0.005, 'neu': 0.99}, 'googlekg': [], 'pos_tagging': ['INTJ'], 'concept': [], 'amazon_topic': 'Science_and_Technology', 'dependency_parsing': {'head': [0], 'label': ['discourse']}, 'topic': {'topicKeywords': [], 'text': 'yes', 'topicClass': 'Phatic', 'confidence': 999}, 'text': 'yes', 'noun_phrase': [], 'dialog_act': ['pos_answer', '0.7661693096160889'], 'intent': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}}, {'sentiment': {'neg': 0.0007, 'compound': 0, 'pos': 0.9993, 'neu': 0}, 'googlekg': [['technology', 'University in Zürich, Switzerland', '97.959839', '__EMPTY__'], ['i like technology', 'Song by The Something Experience', '131.497772', 'music']], 'pos_tagging': ['PRON', 'VERB', 'NOUN'], 'concept': [{'data': {'factor': '0.4559', 'area': '0.2568', 'topic': '0.2873'}, 'noun': 'technology'}, {'data': {}, 'noun': 'i like technology'}], 'amazon_topic': 'Science_and_Technology', 'dependency_parsing': {'head': [2, 0, 2], 'label': ['nsubj', 'acl:relcl', 'obj']}, 'topic': {'topicKeywords': [{'keyword': 'technology', 'confidence': 999}], 'text': 'i like technology', 'topicClass': 'SciTech', 'confidence': 999}, 'text': 'i like technology', 'noun_phrase': ['technology', 'i like technology'], 'dialog_act': ['opinion', '0.588045060634613'], 'intent': {'sys': [], 'topic': ['topic_techscience'], 'lexical': ['ans_like']}}]], 'central_elem': [{'senti': 'pos', 'regex': {'sys': [], 'topic': ['topic_techscience'], 'lexical': ['ans_pos', 'ans_like']}, 'DA': 'opinion', 'DA_score': '0.9844413995742798', 'module': ['TECHSCIENCECHAT'], 'np': ['technology'], 'text': 'i like technology because i like technology makes me happy', 'backstory': {'confidence': 0.5469762086868286, 'followup': '', 'module': 'TECHSCIENCECHAT', 'neg': '', 'pos': '', 'postfix': '', 'question': 'Why do you like science and technology', 'reason': '', 'tag': '', 'text': "without science and technology, i wouldn't be. i'm a big fan of ai. for obvious reasons."}}, {'backstory': {'text': "i'm a big fan of the a.i. technology, for obvious reasons.", 'question': 'What’s your favorite technology', 'confidence': 0.5715954899787903, 'module': 'TECHSCIENCECHAT'}, 'regex': {'sys': [], 'topic': ['topic_techscience'], 'lexical': ['ans_like']}, 'np': ['technology', 'i like technology'], 'DA_score': '0.588045060634613', 'senti': 'pos', 'module': ['TECHSCIENCECHAT'], 'text': 'i like technology', 'DA': 'opinion'}], 'a_b_test': ['B', 'B'], 'last_response': ["<say-as interpret-as='interjection'>Ack</say-as>, <break time = '150ms/>I missed that. I feel bad when I get it wrong! It's frustrating.. Do you mind talking about something else?", '<say-as interpret-as="interjection">Ooh,</say-as> that reminds me: <break time="150ms" /> are you interested in technology? I love talking about Artificial Intelligence. <break time="150ms" /> How about you?'], 'suggest_keywords': None, 'last_module': 'SOCIAL', 'template_manager': {'prev_hash': {'gN4J9Q': ['/WcXSg', 'CyUXZA'], 'zcUS6w': ['Kx0iTQ'], 'VpQIVw': [], 'MsoPew': [], 'yUMMjg': ['Ds8bMg'], 'PPsGsg': ['FlAjjQ'], 'COcM3A': ['BDtHmA'], 'rJ8LnQ': [], 'uLUL9Q': [], 'uvgSpQ': ['Q/xA+Q'], 'is0KPQ': []}, 'selector_history': []}, 'techsciencechat': {'module_sentiment_tracker': {'pos': [0.005, 0.9993, 0.0007], 'neg': [0.005, 0.0007, 0.9993], 'neu': [0.99, 0, 0]}, 'chitchat_tracker': {'unfinished_ids': {}, 'curr_turn': -1, 'completed_ids': []}, 'current_topic': 'technology', 'current_transition': 't_intro', 'propose_topic': [], 'not_first_time': True, 'topic_history': {'artificial_intelligence': 1}, 'prev_state': 's_intro', 'propose_continue': 'CONTINUE', 'context_topic': {'technology': {'technology': 2}}, 'new_topics': ['i like technology']}, 'usr_name': 'sam', 'module_selection': {'module_rank': {'MUSICCHAT': 0, 'GAMECHAT': 0, 'SPORT': 0, 'FASHIONCHAT': 0, 'TECHSCIENCECHAT': 0, 'TRAVELCHAT': 0, 'NEWS': 0, 'BOOKCHAT': 0, 'ANIMALCHAT': 0, 'FOODCHAT': 0, 'MOVIECHAT': 0}, 'used_topic': ['LAUNCHGREETING', 'SOCIAL', 'TECHSCIENCECHAT']}, 'system_acknowledgement': None}

# }
# neutral_tech_intro = {1: {'text': ['yes', 'no'], 'features': [{'googlekg': [[]], 'topic': [{'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'yes', 'topicKeywords': []}], 'ner': None, 'coreference': None, 'topic2': [{'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'yes', 'topicKeywords': []}], 'converttext': 'yes', 'dialog_act': [{'text': 'yes', 'DA': 'pos_answer', 'confidence': 0.7661692500114441}], 'returnnlp': [{'text': 'yes', 'dialog_act': ['pos_answer', '0.7661692500114441'], 'intent': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}, 'sentiment': {'compound': 0, 'neg': 0.005, 'neu': 0.99, 'pos': 0.005}, 'googlekg': [], 'noun_phrase': [], 'topic': {'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'yes', 'topicKeywords': []}, 'concept': [], 'amazon_topic': 'Other', 'dependency_parsing': {'head': [0], 'label': ['discourse']}, 'pos_tagging': ['INTJ']}], 'intent_classify': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}, 'spacynp': None, 'amazon_topic': 'Other', 'concept': [], 'sentiment2': [{'neg': '0.0', 'neu': '0.0', 'pos': '1.0', 'compound': '0.4019'}], 'sentence_completion_text': 'yes', 'intent_classify2': [{'sys': [], 'topic': [], 'lexical': ['ans_pos']}], 'central_elem': {'senti': 'pos', 'regex': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}, 'DA': 'pos_answer', 'DA_score': '0.7661692500114441', 'module': [], 'np': [], 'text': 'yes', 'backstory': {'confidence': 0.4728533625602722, 'followup': '', 'module': '', 'neg': '', 'pos': '', 'postfix': '', 'question': 'why do you ask me', 'reason': '', 'tag': '', 'text': 'Oh, I was just trying to get to know more about you. '}}, 'asrcorrection': None, 'sentiment_allennlp': [{'compound': 0, 'neg': 0.005, 'neu': 0.99, 'pos': 0.005}], 'dependency_parsing': {'dependency_parent': [[0]], 'dependency_label': [['discourse']], 'pos_tagging': [['INTJ']]}, 'sentiment': 'pos', 'intent': 'general', 'segmentation': {'segmented_text': ['yes'], 'segmented_text_raw': ['yes']}, 'npknowledge': {'knowledge': [], 'noun_phrase': [], 'local_noun_phrase': []}, 'text': 'yes', 'nounphrase2': {'noun_phrase': [[]], 'local_noun_phrase': [[]]}, 'topic_module': '', 'topic_keywords': [], 'knowledge': [], 'noun_phrase': []}, None], 'returnnlp': [[{'text': 'yes', 'dialog_act': ['pos_answer', '0.7661692500114441'], 'intent': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}, 'sentiment': {'compound': 0, 'neg': 0.005, 'neu': 0.99, 'pos': 0.005}, 'googlekg': [], 'noun_phrase': [], 'topic': {'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'yes', 'topicKeywords': []}, 'concept': [], 'amazon_topic': 'Other', 'dependency_parsing': {'head': [0], 'label': ['discourse']}, 'pos_tagging': ['INTJ']}], [{'sentiment': {'neg': 0.9993, 'compound': 0, 'pos': 0.0007, 'neu': 0}, 'googlekg': [], 'pos_tagging': ['INTJ'], 'concept': [], 'amazon_topic': 'Other', 'dependency_parsing': {'head': [0], 'label': ['det']}, 'topic': {'topicKeywords': [], 'text': 'no', 'topicClass': 'Phatic', 'confidence': 999}, 'text': 'no', 'noun_phrase': [], 'dialog_act': ['neg_answer', '0.4601633548736572'], 'intent': {'sys': ['regex_neg_answer'], 'topic': [], 'lexical': ['ans_neg']}}]], 'central_elem': [{'senti': 'pos', 'regex': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}, 'DA': 'pos_answer', 'DA_score': '0.7661692500114441', 'module': [], 'np': [], 'text': 'yes', 'backstory': {'confidence': 0.4728533625602722, 'followup': '', 'module': '', 'neg': '', 'pos': '', 'postfix': '', 'question': 'why do you ask me', 'reason': '', 'tag': '', 'text': 'Oh, I was just trying to get to know more about you. '}}, {'backstory': {'question': 'why do you ask me', 'confidence': 0.5005818009376526, 'text': 'Oh, I was just trying to get to know more about you. '}, 'regex': {'sys': ['regex_neg_answer'], 'topic': [], 'lexical': ['ans_neg']}, 'np': [], 'DA_score': '0.4601633548736572', 'senti': 'neg', 'module': [], 'text': 'no', 'DA': 'neg_answer'}], 'a_b_test': ['A', 'A'], 'last_response': ['<prosody rate = "60%">so</prosody><break time = "200ms"></break> um, I\'ve been meaning to ask you: <break time="150ms" /> do you like technology? <break time="150ms" /> It\'s one of my favorite things to talk about.', 'Nice! I\'m glad you\'re having a good day! <break time="150ms" /> What do you like to do for fun? I really like to watch movies.'], 'suggest_keywords': None, 'last_module': 'SOCIAL', 'template_manager': {'prev_hash': {'gN4J9Q': ['CyUXZA'], 'yUMMjg': ['kc8ifQ'], 'PPsGsg': ['zforzw'], 'COcM3A': ['LilGCw'], 'rJ8LnQ': [], 'uLUL9Q': [], 'is0KPQ': []}, 'selector_history': []}, 'techsciencechat': {'propose_continue': 'STOP'}, 'usr_name': 'sam', 'module_selection': {'module_rank': {'MUSICCHAT': 0, 'GAMECHAT': 0, 'SPORT': 0, 'FASHIONCHAT': 0, 'TECHSCIENCECHAT': 0, 'TRAVELCHAT': 0, 'NEWS': 0, 'BOOKCHAT': 0, 'ANIMALCHAT': 0, 'FOODCHAT': 0, 'MOVIECHAT': 0}, 'used_topic': ['LAUNCHGREETING', 'SOCIAL', 'TECHSCIENCECHAT']}, 'system_acknowledgement': None}
# }

import json
import random
import unittest
from techscience_automaton import TechScienceAutomaton
import logging
import sys
import copy

with open("../test_cases/intro_techscience_test.json") as r:
    _test_data = json.loads(r.read())

# logging.disable(logging.WARNING)


class TestSimpleConversation(unittest.TestCase):
    """ Test TopicUtils will focus on testing Topic Handler
        The goal of topic handler is to take in a text and
        1. Extract the NLU features that identify the topic user wants to talk about
        2. Map the user input to the corresponding resonse templates
     """
    def get_automaton(self):
        return TechScienceAutomaton()

    def get_attribute(self, response, attribute):
        user_attributes = response.get("techsciencechat", {})
        return user_attributes.get(attribute, None)

    def _test_helper(self, actual_state, expected_states):
        self.assertIn(actual_state, set(expected_states), msg="Should be {0}, but actually {1}".format(expected_states, actual_state))

    def test_broad_specific_entry(self):
        """
        """
        logging.disable(logging.DEBUG)
        logging.disable(logging.INFO)
        logging.disable(logging.CRITICAL)
        logging.disable(logging.WARNING)
        data = _test_data["test_broad_new_topic"]
        state_order = ["s_intro", "s_retrieval", "s_retrieval", "s_change_of_topic"]
        response = None
        automaton = self.get_automaton()
        for k in data:
            print(k)
            input_data_1 = self.get_random_next_input(k, data)

            if response:
                input_data_1["techsciencechat"] = copy.deepcopy(response["techsciencechat"])
                input_data_1["session_id"] = [session_id]
            print("User Utterance: {}\n\n".format(input_data_1.get("text")[0]))
            response = automaton.transduce(input_data_1)
            self.helper_context_topic(self.get_attribute(response, "context_topic"), automaton.topic_handler)
            print(self.get_attribute(response, "new_topics"))
            session_id = input_data_1["session_id"][0]
            prev_state, prev_transition = self.get_transition_state(response)
            print("Used State {}".format(prev_state))
            self.assertEqual(prev_state, state_order[int(k)-1], msg="State Not Equal: Expected State - {0} Actual State - {1}".format(state_order[int(k)-1], prev_state))
            print("System Response {}\n\n\n".format(response.get("response")))
            session_id = input_data_1["session_id"][0]
        return

    def test_handle_incorrect_fact(self):
        """Incorrect Fact
        """
        logging.disable(logging.DEBUG)
        logging.disable(logging.INFO)
        logging.disable(logging.CRITICAL)
        logging.disable(logging.WARNING)
        data = _test_data["test_incorrect_fact"]
        response = None
        automaton = self.get_automaton()
        for k in data:
            print(k)
            input_data_1 = self.get_random_next_input(k, data)

            if response:
                input_data_1["techsciencechat"] = copy.deepcopy(response["techsciencechat"])
                input_data_1["session_id"] = [session_id]
            print("User Utterance: {}\n\n".format(input_data_1.get("text")[0]))
            response = automaton.transduce(input_data_1)
            self.helper_context_topic(self.get_attribute(response, "context_topic"), automaton.topic_handler)
            print(self.get_attribute(response, "new_topics"))
            session_id = input_data_1["session_id"][0]
            prev_state, prev_transition = self.get_transition_state(response)
            print("Used State {}".format(prev_state))
            print("System Acknowledgement {}".format(input_data_1.get("system_acknowledgement")))
            print("System Response {}\n\n\n".format(response.get("response")))
            session_id = input_data_1["session_id"][0]

    def test_ipads(self):
        """Incorrect Fact
        """
        logging.disable(logging.DEBUG)
        logging.disable(logging.INFO)
        logging.disable(logging.CRITICAL)
        logging.disable(logging.WARNING)
        data = _test_data["test_techscience_ipads"]
        response = None
        automaton = self.get_automaton()
        for k in data:
            print(k)
            input_data_1 = self.get_random_next_input(k, data)

            if response:
                input_data_1["techsciencechat"] = copy.deepcopy(response["techsciencechat"])
                input_data_1["session_id"] = [session_id]
            print("User Utterance: {}\n\n".format(input_data_1.get("text")[0]))
            response = automaton.transduce(input_data_1)
            self.helper_context_topic(self.get_attribute(response, "context_topic"), automaton.topic_handler)
            print(self.get_attribute(response, "new_topics"))
            session_id = input_data_1["session_id"][0]
            prev_state, prev_transition = self.get_transition_state(response)
            print("Used State {}".format(prev_state))
            print("System Acknowledgement {}".format(input_data_1.get("system_acknowledgement")))
            print("System Response {}\n\n\n".format(response.get("response")))
            session_id = input_data_1["session_id"][0]


    def test_broad_specific_teach_me(self):
        """
        """
        logging.disable(logging.DEBUG)
        logging.disable(logging.INFO)
        logging.disable(logging.CRITICAL)
        logging.disable(logging.WARNING)
        data = _test_data["test_broad_new_topic_teach_me"]
        state_order = ["s_intro", "s_ask_more", "s_ask_finish"]
        response = None
        automaton = self.get_automaton()
        for k in data:
            print(k)
            input_data_1 = self.get_random_next_input(k, data)

            if response:
                input_data_1["techsciencechat"] = copy.deepcopy(response["techsciencechat"])
                input_data_1["session_id"] = [session_id]
            print("User Utterance: {}\n\n".format(input_data_1.get("text")[0]))
            response = automaton.transduce(input_data_1)
            self.helper_context_topic(self.get_attribute(response, "context_topic"), automaton.topic_handler)
            print(self.get_attribute(response, "new_topics"))
            session_id = input_data_1["session_id"][0]
            prev_state, prev_transition = self.get_transition_state(response)
            print("Used State {}".format(prev_state))
            self.assertEqual(prev_state, state_order[int(k)-1], msg="State Not Equal: Expected State - {0} Actual State - {1}".format(state_order[int(k)-1], prev_state))
            print("System Response {}\n\n\n".format(response.get("response")))
            session_id = input_data_1["session_id"][0]
        return

    def test_environmental_science(self):
        """
        """
        logging.disable(logging.DEBUG)
        logging.disable(logging.INFO)
        logging.disable(logging.CRITICAL)
        logging.disable(logging.WARNING)
        data = _test_data["test_environmental_science"]
        state_order = ["s_intro", "s_retrieval"]
        response = None
        automaton = self.get_automaton()
        for k in data:
            print(k)
            input_data_1 = self.get_random_next_input(k, data)

            if response:
                input_data_1["techsciencechat"] = copy.deepcopy(response["techsciencechat"])
                input_data_1["session_id"] = [session_id]
            print("User Utterance: {}\n\n".format(input_data_1.get("text")[0]))
            response = automaton.transduce(input_data_1)
            self.helper_context_topic(self.get_attribute(response, "context_topic"), automaton.topic_handler)
            print(self.get_attribute(response, "new_topics"))
            session_id = input_data_1["session_id"][0]
            prev_state, prev_transition = self.get_transition_state(response)
            print("Used State {}".format(prev_state))
            self.assertEqual(prev_state, state_order[int(k)-1], msg="State Not Equal: Expected State - {0} Actual State - {1}".format(state_order[int(k)-1], prev_state))
            print("System Response {}\n\n\n".format(response.get("response")))
            session_id = input_data_1["session_id"][0]
        return

    def test_passive_new_keyword_entry(self):
        """
        """
        logging.disable(logging.DEBUG)
        # logging.disable(logging.INFO)
        logging.disable(logging.CRITICAL)
        logging.disable(logging.WARNING)
        data = _test_data["test_passive_pasttime_entry"]
        response = None
        automaton = self.get_automaton()
        for k in data:
            print(k)
            input_data_1 = self.get_random_next_input(k, data)

            if response:
                input_data_1["techsciencechat"] = copy.deepcopy(response["techsciencechat"])
                input_data_1["session_id"] = [session_id]
            print("User Utterance: {}\n\n".format(input_data_1.get("text")[0]))
            response = automaton.transduce(input_data_1)
            self.helper_context_topic(self.get_attribute(response, "context_topic"), automaton.topic_handler)
            print(self.get_attribute(response, "new_topics"))
            session_id = input_data_1["session_id"][0]
            prev_state, prev_transition = self.get_transition_state(response)
            print("Used State {}".format(prev_state))
            print("System Response {}\n\n\n".format(response.get("response")))
            session_id = input_data_1["session_id"][0]



    def test_returning_user_consistency(self):
        """This is an artifical test to make sure returning user with different fields doesn't crash
        """
        logging.disable(logging.DEBUG)
        logging.disable(logging.INFO)
        logging.disable(logging.CRITICAL)
        logging.disable(logging.WARNING)
        data = _test_data["test_returning_user_consistency"]
        automaton = self.get_automaton()
        input_data_1 = self.get_random_next_input("1", data)  
        for k in input_data_1["techsciencechat"]:
            input_data_1["techsciencechat"][k] = None # Set each field to None
        response = automaton.transduce(input_data_1)
        self.helper_context_topic(self.get_attribute(response, "context_topic"), automaton.topic_handler)
        session_id = input_data_1["session_id"][0]
        prev_state, prev_transition = self.get_transition_state(response)
        
        return

    def helper_context_topic(self, context_topic, topic_handler):
        # Check Context Topic's structure
        if len(context_topic) > 0:
            for topic in context_topic:
                self.assertEqual(type(topic), str, msg="Context Topic Key must be String {}".format(context_topic))
                self.assertTrue(topic_handler.is_fact(topic) or topic_handler.is_topic(topic) or topic_handler.is_broad_topic(topic), msg="key of context topic must be a topic handled by the system!")
                result_context = context_topic[topic]
                if len(result_context) > 0:
                    for kv in result_context:
                        self.assertEqual(type(kv), str, msg="Context Topic value  must be string {}".format(kv))
                        self.assertEqual(type(result_context[kv]), dict, msg="Context value  must be positivity value {}".format(kv))


    def test_user_preference(self):
        """
        """
        logging.disable(logging.DEBUG)
        # logging.disable(logging.INFO)
        # logging.disable(logging.CRITICAL)
        # logging.disable(logging.WARNING)
        data = _test_data["test_user_preference_interrupt"]
        response = None
        automaton = self.get_automaton()
        for k in data:
            print(k)
            input_data_1 = self.get_random_next_input(k, data)

            if response:
                input_data_1["techsciencechat"] = copy.deepcopy(response["techsciencechat"])
                input_data_1["session_id"] = [session_id]
            print("User Utterance: {}\n\n".format(input_data_1.get("text")[0]))
            response = automaton.transduce(input_data_1)
            self.helper_context_topic(self.get_attribute(response, "context_topic"), automaton.topic_handler)
            print(self.get_attribute(response, "new_topics"))
            session_id = input_data_1["session_id"][0]
            prev_state, prev_transition = self.get_transition_state(response)
            print("Used State {}".format(prev_state))
            print("System Acknowledgement {}".format(input_data_1.get("system_acknowledgement")))
            print("System Response {}\n\n\n".format(response.get("response")))
            session_id = input_data_1["session_id"][0]

    def test_field_persistence(self):
        """Test the automaton generated fields are propogaged through automaton, transition, and state
        """

        logging.disable(logging.DEBUG)
        logging.disable(logging.INFO)
        logging.disable(logging.CRITICAL)
        logging.disable(logging.WARNING)
        # Any test case is fine for this case
        data = _test_data["test_user_not_interested"]
        automaton = self.get_automaton()
        input_data_1 = self.get_random_next_input("1", data)        
        response = automaton.transduce(input_data_1)
        self.helper_context_topic(self.get_attribute(response, "context_topic"), automaton.topic_handler)
        unvisited = set()
        for k in automaton.states_mapping:
            state = automaton.states_mapping[k]
            if hasattr(state, 'current_topic'):
                self.assertIs(state.current_topic, automaton.current_topic)
                self.assertIs(state.context_topic, automaton.context_topic)
                self.assertIs(state.current_keywords, automaton.current_keywords)
                self.assertIs(state.topic_history, automaton.topic_history)
                self.assertIs(state.propose_topic, automaton.propose_topic)
                

                self.assertEqual(state.chitchat_tracker, automaton.chitchat_tracker)
                self.assertEqual(state.chitchat_state, automaton.chitchat_state)
                self.assertIs(state.expected_next_state, automaton.expected_next_state)
                self.assertIs(state.new_topics, automaton.new_topics)
            else:
                unvisited.add(k)
        for k in automaton.transitions_mapping:
            transition = automaton.transitions_mapping[k]
            if hasattr(transition, 'current_topic'):

                self.assertIs(transition.current_topic, automaton.current_topic)
                self.assertIs(transition.context_topic, automaton.context_topic)
                self.assertIs(transition.current_keywords, automaton.current_keywords)
                self.assertIs(transition.topic_history, automaton.topic_history)
                self.assertIs(transition.propose_topic, automaton.propose_topic)

                self.assertEqual(state.chitchat_tracker, automaton.chitchat_tracker)
                self.assertEqual(state.chitchat_state, automaton.chitchat_state)
                self.assertIs(state.expected_next_state, automaton.expected_next_state)
                self.assertIs(state.new_topics, automaton.new_topics)
            else:
                unvisited.add(k)

        # Test All of states have the same user attribute values
        print("Unvisited State and Transitions {}".format(unvisited))

    def test_user_not_interested(self):
        data = _test_data["test_user_not_interested"]
        # logging.disable(logging.DEBUG)
        # logging.disable(logging.INFO)
        # logging.disable(logging.CRITICAL)
        # logging.disable(logging.WARNING)
        # i really like technology
        automaton = self.get_automaton()
        input_data_1 = self.get_random_next_input("1", data)        
        response = automaton.transduce(input_data_1)
        self.helper_context_topic(self.get_attribute(response, "context_topic"), automaton.topic_handler)
        keywords = automaton.nlu_processor.detect_predefined_keywords()
        self.assertIn("technology", keywords)
        session_id = input_data_1["session_id"][0]
        prev_state, prev_transition = self.get_transition_state(response)
        propose_topic = self.get_attribute(response, "propose_topic")
        propose_topic[0] = "astronomy"
        session_id_1 = self.get_attribute(response, "stored_session_id")
        self._test_helper(prev_state, set(["s_intro"]))
        next_transition = "t" + prev_state[1:]
        self.assertEqual(session_id_1, session_id, msg="Stored Session ID {0} \n is different from Input Session ID {1}".format(session_id_1, session_id))
        print(response.get("response"))
        print("================================\n\n\n\n")
        # yes i am interested
        input_data_2 = self.get_random_next_input("2", data)
        input_data_2["techsciencechat"] = copy.deepcopy(response["techsciencechat"])
        response = automaton.transduce(input_data_2)
        self.helper_context_topic(self.get_attribute(response, "context_topic"), automaton.topic_handler)
        current_topic = self.get_attribute(response, "current_topic")
        self.assertEqual(current_topic, propose_topic[0], msg="Proposed topic {0} must persist.  Instead it changed to {1}".format(propose_topic[0], current_topic  ))
        prev_state, prev_transition = self.get_transition_state(response)
        self._test_helper(prev_transition, set([next_transition]))
        self._test_helper(prev_state, set(["s_ask_favorite_thing", "s_opinion"]))
        session_id_2 = self.get_attribute(response, "stored_session_id")
        next_transition = "t" + prev_state[1:]
        self.assertEqual(session_id_1, session_id_2)
        # print(self.get_attribute(response, "current_topic"))
        print(response.get("response"))
        print("================================\n\n\n\n")
        # that's cool
        input_data_3 = self.get_random_next_input("3", data)
        input_data_3["techsciencechat"] = copy.deepcopy(response["techsciencechat"])
        response = automaton.transduce(input_data_3)
        # print(automaton.new_topics)
        self.helper_context_topic(self.get_attribute(response, "context_topic"), automaton.topic_handler)
        prev_state, prev_transition = self.get_transition_state(response)
        self._test_helper(prev_transition, set([next_transition]))
        self._test_helper(prev_state, set(["s_interesting_fact"]))
        session_id_3 = self.get_attribute(response, "stored_session_id")
        self.assertEqual(session_id_1, session_id_3)
        next_transition = "t" + prev_state[1:]
        print(response.get("response"))
        print("================================\n\n\n\n")

        # no
        input_data_4 = self.get_random_next_input("4", data)        
        input_data_4["techsciencechat"] = copy.deepcopy(response["techsciencechat"])
        response = automaton.transduce(input_data_4)
        self.helper_context_topic(self.get_attribute(response, "context_topic"), automaton.topic_handler)
        prev_state, prev_transition = self.get_transition_state(response)

        self._test_helper(prev_transition, set([next_transition]))
        self._test_helper(prev_state, set(["s_change_of_topic"]))
        print(response.get("response"))

    def test_simple_new_topic(self):
        """Conversation Flow from t_init -> s_intro -> t_intro -> s_ask_more -> t_ask_more

        1. Test Construct Context Topic's Use
        2. Test Ask More Entry

        """
        data = _test_data["test_simple_new_topic"]

        automaton = self.get_automaton()
        input_data_1 = self.get_random_next_input("1", data)
        response = automaton.transduce(input_data_1)
        context_topic = self.get_attribute(response, "context_topic")
        self.helper_context_topic(context_topic, automaton.topic_handler)
        propose_topic = self.get_attribute(response, "propose_topic")[0]
        # Check construct_context_topic is run correctly
        
        
        input_data_2 = self.get_random_next_input("2", data)
        input_data_2["techsciencechat"] = copy.deepcopy(response["techsciencechat"])
        response = automaton.transduce(input_data_2)
        context_topic = self.get_attribute(response, "context_topic")
        self.helper_context_topic(context_topic, automaton.topic_handler)
        # print(context_topic)
        print(self.get_attribute(response, "current_topic"))
        self.assertEqual("machine_learning",self.get_attribute(response, "current_topic"))
        self.assertIn(propose_topic, context_topic)

        # input_data_3 = self.get_random_next_input("3", data)
        # input_data_3["techsciencechat"] = copy.deepcopy(response["techsciencechat"])
        # response = automaton.transduce(input_data_3)
        # context_topic = self.get_attribute(response, "context_topic")
        # print(context_topic)

    # def test_neutral_intro_flow(self):
    #     """Conversation Flow from t_init -> s_intro -> t_intro -> s_opinion -> t_opinion
    #     """
        
    #     automaton = self.get_automaton()
    #     response = automaton.transduce(neutral_tech_intro[1])
    #     print("s_intro response: {}".format(response["response"]))
    #     # fav_tech_intro[2]["techsciencechat"] = response["techsciencechat"]
    #     # response = automaton.transduce(neutral_tech_intro[2])
    #     # print(response["response"])

    def get_transition_state(self, response):
        user_attributes = response.get("techsciencechat", {})
        return user_attributes.get("prev_state"), user_attributes.get("prev_transition")

    def get_random_next_input(self, index, source):
        return random.choice(source[index])


   


        
if __name__ == '__main__':
    unittest.main()