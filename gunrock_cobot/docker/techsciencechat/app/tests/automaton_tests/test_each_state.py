"""Test Each Automaton States Individually

3. s_ask_finish, t_ask_finish
3. s_ask_more, t_ask_more
4. s_ask_research, t_ask_research
5. s_backstory_response, t_backstory_response
6. s_change_of_topic, t_change_of_topic
7. s_echo, t_echo

"""

"""
This Test focus on the basic flow of our automaton
"""




import random
import unittest
from utils.mock_data import DailyStatistics
from nlu.util_nlu import ReturnNLPSegment, CentralElement
from techscience_automaton import TechScienceAutomaton
import logging
from tests.automaton_tests.utils_test_case_extractor import test_transition_starts, ask_more_ask_finish, backstory_input
logging.disable(logging.DEBUG)
logging.disable(logging.INFO)

class TestEachState(unittest.TestCase):
    """ Each state should generate a respose.  Based on the response generated, the state should trigger
        the next transition.
    """

    def get_automaton(self):

        return TechScienceAutomaton()

    # def get_most_frequent_states(self):
    #     tested_transitions = []
    #     tested_states = []
    #     tested_next_transition = []
    #     responses = []
    #     automaton = self.get_automaton()
    #     test_cases = DailyStatistics("../test_cases")
    #     iterator = test_cases.generator()
    #     # ret = [ReturnNLPSegment.from_dict(seg) for seg in returnnlp]
    #     # c = CentralElement.from_dict(central_elem)

    #     for text, response, ret, central_elem in iterator:
    #         msg = {"text":text, "template_manager":{},
    #                "feature":{}, "returnnlp":ret,
    #                "central_elem":central_elem,
    #                "techsciencechat":{"PROPOSE_CONTINUE": "STOP"}, "propose_topic": None}
    #         response = automaton.transduce(msg)
    #         res = response["response"]
    #         tested_transitions.append(automaton.current_transition)
    #         tested_states.append(automaton.next_state)
    #         tested_next_transition.append(automaton.next_transition)
    #         responses.append(res)
    #     return

    def get_conversation_with_state(self, state):
        l = []
        for t in test_transition_starts:
            for msg, s in test_transition_starts[t]:
                if s == state:
                    l.append(msg)
        return l

    # def test_intro_active_ask(self, state):
    #     return

    def test_ask_more(self):
        dup = []
        num_calls = 10
        ask_more = self.get_conversation_with_state("s_ask_more")
        for _ in range(num_calls):
            automaton = self.get_automaton()
            for msg in ask_more:
                response = automaton.transduce(msg)
                dup.append(response["response"])
        print("Duplicate Measures [0 - 1] (higher the more diverse): ask_more {:.5f}".format(
            float(len(set(dup))) / len(dup)))

    def test_ask_favorite_thing(self):
        """This tests ask_favorite thing will lead to interesting_fact
        """

        dup = []
        num_calls = 10
        ask_favorite_thing = self.get_conversation_with_state(
            "s_ask_favorite_thing")
        for _ in range(num_calls):
            automaton = self.get_automaton()
            for msg in ask_favorite_thing:
                response = automaton.transduce(msg)
                dup.append(response["response"])
        print("Duplicate Measures [0 - 1] (higher the more diverse): ask_favorite_thing {:.2f}".format(
            len(set(dup)) / len(dup)))
        return

    # def test_ask_finish(self):
    #     dup = []
    #     num_calls = 10
    #     for _ in range(num_calls):
    #         automaton = self.get_automaton()
    #         response = automaton.transduce(ask_more_ask_finish)
    #         dup.append(response["response"])
    #     print("Duplicate Measures [0 - 1] (higher the more diverse): ask_finish {:.2f}".format(
    #         len(set(dup)) / len(dup)))

    # def test_change_of_topic(self):
    #     dup = []
    #     num_calls = 10
    #     change_of_topic = self.get_conversation_with_state("s_change_of_topic")
    #     for _ in range(num_calls):
    #         automaton = self.get_automaton()
    #         for msg in change_of_topic:
    #             response = automaton.transduce(msg)
    #             dup.append(response["response"])
    #     print("Duplicate Measures [0 - 1] (higher the more diverse): change_of_topic {:.2f}".format(
    #         len(set(dup)) / len(dup)))

    # def test_opinion(self):
    #     dup = []
    #     num_calls = 10
    #     opinion = self.get_conversation_with_state("s_opinion")
    #     for _ in range(num_calls):
    #         automaton = self.get_automaton()
    #         for msg in opinion:
    #             response = automaton.transduce(msg)
    #             dup.append(response["response"])
    #     print("Duplicate Measures [0 - 1] (higher the more diverse): opinion {:.2f}".format(
    #         len(set(dup)) / len(dup)))

    # def test_intro(self):
    #     dup = []
    #     num_calls = 40
    #     intro = self.get_conversation_with_state("s_intro")
    #     for _ in range(num_calls):
    #         automaton = self.get_automaton()
    #         for msg in intro:
    #             response = automaton.transduce(msg)
    #             dup.append(response["response"])
    #     print("Duplicate Measures [0 - 1] (higher the more diverse): intro {:.2f}".format(
    #         len(set(dup)) / len(dup)))

    # def test_backstory_response(self):
    #     automaton = self.get_automaton()
        
    #     original_transition = backstory_input["techsciencechat"]["current_transition"]
    #     response = automaton.transduce(backstory_input)

    #     self.assertEquals(
    #         response["techsciencechat"]["current_transition"],
    #         original_transition)
    #     return


if __name__ == '__main__':
    unittest.main()
