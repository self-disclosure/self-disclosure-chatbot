"""Testing Retrieval States Automaton
    TODO: Add Chitchat functionality later.
"""
import logging
import copy
import json
import random
import unittest
from techscience_automaton import TechScienceAutomaton

# logging.disable(logging.DEBUG)
# logging.disable(logging.INFO)
# logging.disable(logging.CRITICAL)
# logging.disable(logging.WARNING)

# sample_chitchat_out = {'text': ['tell me about blockchain', 'tell me about artificial intelligence'], 'features': [{'dependency_parsing': {'dependency_parent': [[0, 1, 4, 1]], 'dependency_label': [['ccomp', 'obj', 'case', 'obl']], 'pos_tagging': [['VERB', 'PRON', 'ADP', 'NOUN']]}, 'profanity_check': [0], 'ner': None, 'intent_classify': {'sys': ['req_topic_jump'], 'topic': [], 'lexical': ['ask_info']}, 'npknowledge': {'knowledge': [['Blockchain', 'Event', '12.17116', '__EMPTY__']], 'noun_phrase': ['blockchain'], 'local_noun_phrase': ['blockchain']}, 'concept': [{'noun': 'blockchain', 'data': {'technology': '0.6', 'big bitcoin organization': '0.2', 'website': '0.2'}}], 'spacynp': ['blockchain'], 'intent': 'general', 'nounphrase2': {'noun_phrase': [['blockchain']], 'local_noun_phrase': [['blockchain']]}, 'sentence_completion_text': 'tell me about blockchain', 'topic': [{'topicClass': 'Movies_TV', 'confidence': 0.313, 'text': 'tell me about blockchain', 'topicKeywords': [{'confidence': 0.405, 'keyword': 'me'}]}], 'segmentation': {'segmented_text': ['tell me about blockchain'], 'segmented_text_raw': ['tell me about blockchain']}, 'coreference': None, 'sentiment': 'neu', 'intent_classify2': [{'sys': ['req_topic_jump'], 'topic': [], 'lexical': ['ask_info']}], 'googlekg': [[['Blockchain', 'Event', '12.17116', '__EMPTY__']]], 'central_elem': {'senti': 'neu', 'regex': {'sys': ['req_topic_jump'], 'topic': [], 'lexical': ['ask_info']}, 'DA': 'commands', 'DA_score': '0.9904952645301819', 'module': ['TECHSCIENCECHAT'], 'np': ['blockchain'], 'text': 'tell me about blockchain', 'backstory': {'confidence': 0.5240171551704407, 'followup': '', 'module': '', 'neg': '', 'pos': '', 'postfix': '', 'question': 'what is cortical computation', 'reason': '', 'tag': '', 'text': 'I think it\'s the study of how brains compute <break time="300ms"/> but I don\'t know a lot about it, to be honest. '}}, 'topic2': [{'topicClass': 'Movies_TV', 'confidence': 0.313, 'text': 'tell me about blockchain', 'topicKeywords': [{'confidence': 0.405, 'keyword': 'me'}]}], 'text': 'tell me about blockchain', 'dialog_act': [{'text': 'tell me about blockchain', 'DA': 'commands', 'confidence': 0.9904952645301819}], 'returnnlp': [{'text': 'tell me about blockchain', 'dialog_act': ['commands', '0.9904952645301819'], 'intent': {'sys': ['req_topic_jump'], 'topic': [], 'lexical': ['ask_info']}, 'sentiment': {'neg': '0.0', 'neu': '1.0', 'pos': '0.0', 'compound': '0.0'}, 'googlekg': [['Blockchain', 'Event', '12.17116', '__EMPTY__']], 'noun_phrase': ['blockchain'], 'topic': {'topicClass': 'Movies_TV', 'confidence': 0.313, 'text': 'tell me about blockchain', 'topicKeywords': [{'confidence': 0.405, 'keyword': 'me'}]}, 'concept': [{'noun': 'blockchain', 'data': {'technology': '0.6', 'big bitcoin organization': '0.2', 'website': '0.2'}}], 'dependency_parsing': {'head': [0, 1, 4, 1], 'label': ['ccomp', 'obj', 'case', 'obl']}, 'pos_tagging': ['VERB', 'PRON', 'ADP', 'NOUN']}], 'converttext': 'tell me about blockchain', 'asrcorrection': None, 'sentiment2': [{'neg': '0.0', 'neu': '1.0', 'pos': '0.0', 'compound': '0.0'}], 'topic_module': '', 'topic_keywords': [], 'knowledge': [['Blockchain', 'Event', '12.17116', '__EMPTY__']], 'noun_phrase': ['blockchain']}, None], 'returnnlp': [[{'text': 'tell me about blockchain', 'dialog_act': ['commands', '0.9904952645301819'], 'intent': {'sys': ['req_topic_jump'], 'topic': [], 'lexical': ['ask_info']}, 'sentiment': {'neg': '0.0', 'neu': '1.0', 'pos': '0.0', 'compound': '0.0'}, 'googlekg': [['Blockchain', 'Event', '12.17116', '__EMPTY__']], 'noun_phrase': ['blockchain'], 'topic': {'topicClass': 'Movies_TV', 'confidence': 0.313, 'text': 'tell me about blockchain', 'topicKeywords': [{'confidence': 0.405, 'keyword': 'me'}]}, 'concept': [{'noun': 'blockchain', 'data': {'technology': '0.6', 'big bitcoin organization': '0.2', 'website': '0.2'}}], 'dependency_parsing': {'head': [0, 1, 4, 1], 'label': ['ccomp', 'obj', 'case', 'obl']}, 'pos_tagging': ['VERB', 'PRON', 'ADP', 'NOUN']}], [{'sentiment': {'neg': '0.0', 'pos': '0.437', 'compound': '0.4767', 'neu': '0.563'}, 'googlekg': [['artificial intelligence', '2001 film', '678.277649', 'movie']], 'pos_tagging': ['VERB', 'PRON', 'ADP', 'ADJ', 'NOUN'], 'concept': [{'data': {'field': '0.3136', 'topic': '0.3105', 'technology': '0.376'}, 'noun': 'artificial intelligence'}], 'dependency_parsing': {'head': [0, 1, 5, 5, 1], 'label': ['ccomp', 'obj', 'case', 'amod', 'obl']}, 'topic': {'topicKeywords': [{'keyword': 'intelligence', 'confidence': 999}], 'text': 'tell me about artificial intelligence', 'topicClass': 'SciTech', 'confidence': 999}, 'text': 'tell me about artificial intelligence', 'noun_phrase': ['artificial intelligence'], 'dialog_act': ['commands', '0.8898943066596985'], 'intent': {'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': ['ask_info']}}]], 'central_elem': [{'senti': 'neu', 'regex': {'sys': ['req_topic_jump'], 'topic': [], 'lexical': ['ask_info']}, 'DA': 'commands', 'DA_score': '0.9904952645301819', 'module': ['TECHSCIENCECHAT'], 'np': ['blockchain'], 'text': 'tell me about blockchain', 'backstory': {'confidence': 0.5240171551704407, 'followup': '', 'module': '', 'neg': '', 'pos': '', 'postfix': '', 'question': 'what is cortical computation', 'reason': '', 'tag': '', 'text': 'I think it\'s the study of how brains compute <break time="300ms"/> but I don\'t know a lot about it, to be honest. '}}, {'backstory': {'text': "i don't think it's a competition for any leaders. i believe any research will help everyone in advance, rather than push anyone back.", 'question': 'Who is leading artificial intelligence', 'confidence': 0.7664469480514526, 'module': 'TECHSCIENCECHAT'}, 'regex': {'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': ['ask_info']}, 'np': ['artificial intelligence'], 'DA_score': '0.8898943066596985', 'senti': 'neu', 'module': ['TECHSCIENCECHAT', 'MOVIECHAT', 'TECHSCIENCECHAT'], 'text': 'tell me about artificial intelligence', 'DA': 'commands'}], 'a_b_test': ['B', 'B'], 'suggest_keywords': None, 'last_module': 'TECHSCIENCECHAT', 'template_manager': {'prev_hash': {'PPsGsg': ['7WwlVw'], 'gN4J9Q': ['CyUXZA'], 'rJ8LnQ': [], 'is0KPQ': [], 'mPIK5Q': ['W6QUYQ']}}, 'techsciencechat': {'chitchat_tracker': {'trend': {}, 'opinion': {}, 'fact': {'artificial_intelligence': {'new_ids': [1, 2], 'unfinished_ids': {}, 'curr_turn': 0, 'curr_id': 1, 'completed_ids': [], 'next_id': -1}}}, 'current_topic': 'artificial_intelligence', 'current_transition': 't_interesting_fact', 'tech_propose_topic': [], 'not_first_time': True, 'topic_history': {}, 'prev_state': 's_interesting_fact', 'propose_continue': 'CONTINUE', 'context_topic': {}}, 'usr_name': 'samuel', 'module_selection': {'used_topic': ['LAUNCHGREETING', 'TECHSCIENCECHAT']}}
with open("../test_cases/retrieval_techscience_test.json") as r:
    _test_data = json.loads(r.read())

class TestRetrievalNewWord(unittest.TestCase):
    """ Test TopicUtils will focus on testing Topic Handler
        The goal of topic handler is to take in a text and
        1. Extract the NLU features that identify the topic user wants to talk about
        2. Map the user input to the corresponding resonse templates
     """

    def get_automaton(self):
        return TechScienceAutomaton()

    def get_attribute(self, response, attribute):
        user_attributes = response.get("techsciencechat", {})
        return user_attributes.get(attribute, None)

    def _test_helper(self, actual_state, expected_states):
        self.assertIn(actual_state, set(expected_states), msg="Should be {0}, but actually {1}".format(expected_states, actual_state))  

    def get_transition_state(self, response):
        user_attributes = response.get("techsciencechat", {})
        return user_attributes.get("prev_state"), user_attributes.get("prev_transition")


    def get_random_next_input(self, index, source):
        return random.choice(source[index])

    def helper_context_topic(self, context_topic, topic_handler):
        # Check Context Topic's structure
        if len(context_topic) > 0:
            for topic in context_topic:
                self.assertEqual(type(topic), str, msg="Context Topic Key must be String {}".format(context_topic))
                self.assertTrue(topic_handler.is_fact(topic) or topic_handler.is_topic(topic) or topic_handler.is_broad_topic(topic), msg="key of context topic must be a topic handled by the system!")
                result_context = context_topic[topic]
                if len(result_context) > 0:
                    for kv in result_context:
                        self.assertEqual(type(kv), str, msg="Context Topic value  must be string {}".format(kv))
                        self.assertEqual(type(result_context[kv]), dict, msg="Context value  must be positivity value {}".format(kv))


    def _test_block(self, turn, automaton, data, session_id=None, prev_response=None):
        input_data_1 = self.get_random_next_input(turn, data)
        if prev_response:
            input_data_1["techsciencechat"] = copy.deepcopy(prev_response["techsciencechat"])
            input_data_1["session_id"] = [session_id]
        print("User Utterance: {}\n\n".format(input_data_1.get("text")[0]))
        response = automaton.transduce(input_data_1)
        print(self.get_attribute(response, "new_topics"))
        session_id = input_data_1["session_id"][0]
        prev_state, prev_transition = self.get_transition_state(response)
        print("Used State {}".format(prev_state))
        print("System Response {}\n\n\n".format(response.get("response")))
        return response, session_id

    def test_space(self):
        """Conversation Flow from t_init -> s_intro -> t_intro -> s_ask_more -> t_ask_more

        1. Test Construct Context Topic's Use
        2. Test Ask More Entry

        """
        logging.disable(logging.DEBUG)
        # logging.disable(logging.INFO)
        # logging.disable(logging.CRITICAL)
        # logging.disable(logging.WARNING)
        data = _test_data["test_techscience_space"]
        automaton = self.get_automaton()
        session_id = None
        prev_response = None
        for k in data:
            print(k)
            prev_response, session_id = self._test_block(str(k), automaton, data, session_id, prev_response)
if __name__ == '__main__':
    unittest.main()
