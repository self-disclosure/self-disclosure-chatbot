"""User should be able to leave conversation at any given point
"""


import random
import unittest
from _techscience_automaton import TechScienceAutomaton
from tests.automaton_tests.utils_test_case_extractor import leave_return


class TestLeaveConversation(unittest.TestCase):
    """ Each state should generate a respose.  Based on the response generated, the state should trigger
        the next transition.
    """

    def get_automaton(self):
        return TechScienceAutomaton()

    def test_trigger_exit(self):
        """Init follows two path:
        s_askmore or s_intro
        """

        # yes, sure, yeah, of course --> s_intro
        leave, ret = leave_return
        automaton = self.get_automaton()
        response = automaton.transduce(leave)
        self.assertEquals(
            automaton.next_state,
            "s_exit_techsciencechat",
            msg=automaton.next_state)
        self.assertTrue(self.check_propose_continue(response))
        response = automaton.transduce(ret)
        self.assertEquals(
            automaton.current_transition,
            "t_init_resume",
            msg=automaton.current_transition)

    def check_propose_continue(self, response):
        return response["techsciencechat"]["propose_continue"] == "STOP"


if __name__ == '__main__':
    unittest.main()
