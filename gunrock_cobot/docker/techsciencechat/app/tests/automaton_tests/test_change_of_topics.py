"""For each transition, test the case that will trigger the corresponding
   states.  (Both Positive and Negative States)

"""

"""
This Test focus on the basic flow of our automaton
"""

import logging
import copy
import json
import random
import unittest
from techscience_automaton import TechScienceAutomaton


# from tests.automaton_tests.utils_test_case_extractor import test_transition_starts, ask_finish_change_of_topic, ask_more_ask_finish

with open("../test_cases/change_topic_techscience_test.json") as r:
    _test_data = json.loads(r.read())

class TestEachTransition(unittest.TestCase):
    """ Each state should generate a respose.  Based on the response generated, the state should trigger
        the next transition.
    """

    def get_automaton(self):
        return TechScienceAutomaton()

    def get_attribute(self, response, attribute):
        user_attributes = response.get("techsciencechat", {})
        return user_attributes.get(attribute, None)

    def _test_helper(self, actual_state, expected_states):
        self.assertIn(actual_state, set(expected_states), msg="Should be {0}, but actually {1}".format(expected_states, actual_state))  

    def get_transition_state(self, response):
        user_attributes = response.get("techsciencechat", {})
        return user_attributes.get("prev_state"), user_attributes.get("prev_transition")

    def get_random_next_input(self, index, source):
        return random.choice(source[index])

    def change_of_topic_entry(self):
        """Starting point for all change of topic unittests
        """
        return

    # def test_simple_change_of_topic(self):
    #     """This test ensures simple entry to change of topic 

    #     Key Feature to Test
    #     1. Consistent Session ID
    #     2. Propose topic consistency
    #     3. No Crash
    #     4. change of topic morph into a better user utterance detection (Combine this with other tests)

    #     When user says no, make sure we don't propose topic we already said!

    #     t_init (t_init_resume) -> s_intro -> t_intro -> s_interesting_fact -> t_interesting_fact -> s_change_of_topic

    #     """
    #     data = _test_data["test_simple_change_of_topic"]

    #     automaton = self.get_automaton()
    #     input_data_1 = self.get_random_next_input("1", data)
    #     print(input_data_1.get("text"))
      
    #     response = automaton.transduce(input_data_1)
    #     keywords = automaton.nlu_processor.detect_predefined_keywords()
    #     self.assertIn("science", keywords)
    #     session_id = input_data_1["session_id"][0]
    #     prev_state, prev_transition = self.get_transition_state(response)
    #     session_id_1 = self.get_attribute(response, "stored_session_id")
    #     self._test_helper(prev_state, set(["s_intro"]))
    #     next_transition = "t" + prev_state[1:]
    #     self.assertEqual(session_id_1, session_id, msg="Stored Session ID {0} \n is different from Input Session ID {1}".format(session_id_1, session_id))

    #     print("==========================================================")
    #     input_data_2 = self.get_random_next_input("2", data)
    #     print(input_data_2.get("text"))
    #     input_data_2["techsciencechat"] = copy.deepcopy(response["techsciencechat"])
    #     response = automaton.transduce(input_data_2)
    #     current_topic = self.get_attribute(response, "current_topic")
    #     keywords = automaton.nlu_processor.detect_predefined_keywords()
    #     self.assertIn("chemistry", keywords)
    #     self.assertEqual(current_topic, "chemistry")
    #     prev_state, prev_transition = self.get_transition_state(response)
    #     self._test_helper(prev_transition, set([next_transition]))
    #     self._test_helper(prev_state, set(["s_interesting_fact"]))
    #     session_id_2 = self.get_attribute(response, "stored_session_id")
    #     next_transition = "t" + prev_state[1:]
    #     self.assertEqual(session_id_1, session_id_2)

    #     input_data_3 = self.get_random_next_input("3", data)
    #     input_data_3["techsciencechat"] = copy.deepcopy(response["techsciencechat"])
    #     response = automaton.transduce(input_data_3)
    #     prev_state, prev_transition = self.get_transition_state(response)
    #     self._test_helper(prev_transition, set([next_transition]))
    #     self._test_helper(prev_state, set(["s_change_of_topic"]))
    #     session_id_3 = self.get_attribute(response, "stored_session_id")
    #     self.assertEqual(session_id_1, session_id_3)
    #     next_transition = "t" + prev_state[1:]

        # input_data_4 = self.get_random_next_input("4", data)        
        # input_data_4["techsciencechat"] = copy.deepcopy(response["techsciencechat"])
        # response = automaton.transduce(input_data_4)
        # prev_state, prev_transition = self.get_transition_state(response)

        # self._test_helper(prev_transition, set([next_transition]))
        
        # return

    def test_change_of_topic_to_exit(self):
        """Test Change of Topic, no thanks.  To Exit Techscience
        """
        # logging.disable(logging.DEBUG)
        # logging.disable(logging.INFO)
        # logging.disable(logging.CRITICAL)
        # logging.disable(logging.WARNING)
        data = _test_data["test_change_topic_to_exit"]

        automaton = self.get_automaton()

        # 
        input_data_1 = self.get_random_next_input("1", data)
        print(input_data_1.get("text")[0])
      
        response = automaton.transduce(input_data_1)
        keywords = automaton.nlu_processor.detect_predefined_keywords()
        session_id = input_data_1["session_id"][0]
        prev_state, prev_transition = self.get_transition_state(response)
        session_id_1 = self.get_attribute(response, "stored_session_id")
        # self._test_helper(prev_state, set(["s_intro"]))
        next_transition = "t" + prev_state[1:]
        self.assertEqual(session_id_1, session_id, msg="Stored Session ID {0} \n is different from Input Session ID {1}".format(session_id_1, session_id))
        print("\t\t{}".format(response.get("response")))
        print(prev_transition)
        print(prev_state)
        print("==========================================================")
        input_data_2 = self.get_random_next_input("2", data)
        print(input_data_2.get("text")[0])
        input_data_2["techsciencechat"] = copy.deepcopy(response["techsciencechat"])
        response = automaton.transduce(input_data_2)
        current_topic = self.get_attribute(response, "current_topic")
        keywords = automaton.nlu_processor.detect_predefined_keywords()
        prev_state, prev_transition = self.get_transition_state(response)
        # self._test_helper(prev_transition, set([next_transition]))
        # self._test_helper(prev_state, set(["s_interesting_fact"]))
        session_id_2 = self.get_attribute(response, "stored_session_id")
        next_transition = "t" + prev_state[1:]
        self.assertEqual(session_id_1, session_id_2)
        print("\t\t{}".format(response.get("response")))
        print(prev_transition)
        print(prev_state)
        print("==========================================================")
        input_data_3 = self.get_random_next_input("3", data)
        print(input_data_3.get("text")[0])
        input_data_3["techsciencechat"] = copy.deepcopy(response["techsciencechat"])
        response = automaton.transduce(input_data_3)
        prev_state, prev_transition = self.get_transition_state(response)
        # self._test_helper(prev_transition, set([next_transition]))
        # self._test_helper(prev_state, set(["s_change_of_topic"]))
        session_id_3 = self.get_attribute(response, "stored_session_id")
        self.assertEqual(session_id_1, session_id_3)
        next_transition = "t" + prev_state[1:]
        print("\t\t{}".format(response.get("response")))
        print(prev_transition)
        print(prev_state)
        print("==========================================================")
        input_data_4 = self.get_random_next_input("4", data)
        print(input_data_4.get("text")[0])
        input_data_4["techsciencechat"] = copy.deepcopy(response["techsciencechat"])
        response = automaton.transduce(input_data_4)
        prev_state, prev_transition = self.get_transition_state(response)
        # self._test_helper(prev_transition, set([next_transition]))
        # self._test_helper(prev_state, set(["s_change_of_topic"]))
        session_id_4 = self.get_attribute(response, "stored_session_id")
        self.assertEqual(session_id_1, session_id_4)
        next_transition = "t" + prev_state[1:]
        print("\t\t{}".format(response.get("response")))
        print(prev_transition)
        print(prev_state)
        print("==========================================================")
        input_data_5 = self.get_random_next_input("5", data)
        print(input_data_5.get("text")[0])
        input_data_5["techsciencechat"] = copy.deepcopy(response["techsciencechat"])
        response = automaton.transduce(input_data_5)
        prev_state, prev_transition = self.get_transition_state(response)
        # self._test_helper(prev_transition, set([next_transition]))
        # self._test_helper(prev_state, set(["s_change_of_topic"]))
        session_id_5 = self.get_attribute(response, "stored_session_id")
        self.assertEqual(session_id_1, session_id_5)
        next_transition = "t" + prev_state[1:]
        print("\t\t{}".format(response.get("response")))
        print(prev_transition)
        print(prev_state)
        print("==========================================================")

    # def util_test_transitions(
    #         self,
    #         msg,
    #         current_transition,
    #         next_state,
    #         possible_other_states=[]):
    #     automaton = self.get_automaton()
    #     response = automaton.transduce(msg)
    #     target_states = possible_other_states + [next_state]
    #     self.assertEquals(
    #         automaton.current_transition,
    #         current_transition,
    #         msg="current transition should be {current_transition}, instead it is {detected_transition}. \n \
    #                            please check your input message".format(
    #             current_transition=current_transition,
    #             detected_transition=automaton.current_transition))
    #     self.assertTrue(
    #         automaton.next_state in target_states,
    #         msg="next state should be {next_state}, instead it is {detected_state}. \n \
    #                            please check your input message".format(
    #             next_state=next_state,
    #             detected_state=automaton.next_state))

    # def test_change_of_topic_from_interesting_fact(self):
    #     data = _test_data["test_user_not_interested"]
    #     automaton = self.get_automaton()
    #     input_data_1 = self.get_random_next_input("1", data)        
    #     response = automaton.transduce(input_data_1)
    #     unvisited = set()
    #     for k in automaton.states_mapping:
    #         state = automaton.states_mapping[k]
    #         if hasattr(state, 'current_topic'):
    #             self.assertIs(state.current_topic, automaton.current_topic)
    #             self.assertIs(state.context_topic, automaton.context_topic)
    #             self.assertIs(state.current_keywords, automaton.current_keywords)
    #             self.assertIs(state.topic_history, automaton.topic_history)
    #             self.assertIs(state.propose_topic, automaton.propose_topic)

    #             self.assertEqual(state.chitchat_tracker, automaton.chitchat_tracker)
    #             self.assertEqual(state.chitchat_state, automaton.chitchat_state)
    #             self.assertIs(state.expected_next_state, automaton.expected_next_state)
    #             self.assertIs(state.new_topics, automaton.new_topics)
    #         else:
    #             unvisited.add(k)
    #     for k in automaton.transitions_mapping:
    #         transition = automaton.transitions_mapping[k]
    #         if hasattr(transition, 'current_topic'):

    #             self.assertIs(transition.current_topic, automaton.current_topic)
    #             self.assertIs(transition.context_topic, automaton.context_topic)
    #             self.assertIs(transition.current_keywords, automaton.current_keywords)
    #             self.assertIs(transition.topic_history, automaton.topic_history)
    #             self.assertIs(transition.propose_topic, automaton.propose_topic)

    #             self.assertEqual(state.chitchat_tracker, automaton.chitchat_tracker)
    #             self.assertEqual(state.chitchat_state, automaton.chitchat_state)
    #             self.assertIs(state.expected_next_state, automaton.expected_next_state)
    #             self.assertIs(state.new_topics, automaton.new_topics)
    #         else:
    #             unvisited.add(k)


    # def test_conversation_change(self):
    #     """Init follows two path:
    #     s_askmore or s_intro
    #     """
    #     s1 = {'text': ['yes i do like techno', 'my name is kevin'], 'features': [{'intent_classify': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}, 'googlekg': [[], [['techno', 'Musical event', '22.133463', '__EMPTY__']]], 'ner': None, 'sentiment': 'pos', 'asrcorrection': {'techno': ['technology']}, 'topic': [{'topicClass': 'Music', 'confidence': 999.0, 'text': 'yes i do like techno', 'topicKeywords': [{'confidence': 999.0, 'keyword': 'techno'}]}], 'intent_classify2': [{'sys': [], 'topic': [], 'lexical': ['ans_pos']}, {'sys': [], 'topic': [], 'lexical': ['ans_like']}], 'returnnlp': [{'text': 'yes', 'dialog_act': ['pos_answer', '0.9995905756950378'], 'intent': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}, 'sentiment': {'neg': '0.0', 'neu': '0.0', 'pos': '1.0', 'compound': '0.4019'}, 'googlekg': [], 'noun_phrase': [], 'topic': {'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'yes', 'topicKeywords': []}, 'concept': [], 'dependency_parsing': {'head': [0], 'label': ['discourse']}, 'pos_tagging': ['INTJ']}, {'text': 'i do like techno', 'dialog_act': ['statement', '0.7159385085105896'], 'intent': {'sys': [], 'topic': [], 'lexical': ['ans_like']}, 'sentiment': {'neg': '0.0', 'neu': '0.444', 'pos': '0.556', 'compound': '0.3612'}, 'googlekg': [['techno', 'Musical event', '22.133463', '__EMPTY__']], 'noun_phrase': ['techno'], 'topic': {'topicClass': 'Music', 'confidence': 999.0, 'text': 'i do like techno', 'topicKeywords': [{'confidence': 999.0, 'keyword': 'techno'}]}, 'concept': [{'noun': 'techno', 'data': {'genre': '0.5829', 'music': '0.2362', 'style': '0.1809'}}], 'dependency_parsing': {'head': [3, 3, 0, 3], 'label': ['nsubj', 'aux', 'parataxis', 'preterm']}, 'pos_tagging': ['PRON', 'AUX', 'INTJ', 'NOUN']}], 'segmentation': {'segmented_text': ['yes', 'i do like techno'], 'segmented_text_raw': ['yes', 'i do like techno']}, 'topic2': [{'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'yes', 'topicKeywords': []}, {'topicClass': 'Music', 'confidence': 999.0, 'text': 'i do like techno', 'topicKeywords': [{'confidence': 999.0, 'keyword': 'techno'}]}], 'nounphrase2': {'noun_phrase': [[], ['techno']], 'local_noun_phrase': [[], ['techno']]}, 'coreference': None, 'converttext': 'yes i do like techno', 'sentiment2': [{'neg': '0.0', 'neu': '0.0', 'pos': '1.0', 'compound': '0.4019'}, {'neg': '0.0', 'neu': '0.444', 'pos': '0.556', 'compound': '0.3612'}], 'intent': 'general', 'npknowledge': {'knowledge': [['techno', 'Musical event', '22.133463', '__EMPTY__']], 'noun_phrase': ['techno'], 'local_noun_phrase': ['techno']}, 'concept': [{'noun': 'techno', 'data': {'genre': '0.5829', 'music': '0.2362', 'style': '0.1809'}}], 'profanity_check': [0], 'dialog_act': [{'text': 'yes', 'DA': 'pos_answer', 'confidence': 0.9995905756950378}, {'text': 'i do like techno', 'DA': 'statement', 'confidence': 0.7159385085105896}], 'spacynp': ['techno'], 'central_elem': {'senti': 'pos', 'regex': {'sys': [], 'topic': [], 'lexical': ['ans_like']}, 'DA': 'statement', 'DA_score': '0.7159385085105896', 'module': [], 'np': ['techno'], 'text': 'i do like techno', 'backstory': {'confidence': 0.6322230100631714, 'followup': 'What kind of music do you like? ', 'module': 'MUSICCHAT', 'neg': '', 'pos': '', 'postfix': '', 'question': 'Do you like pop music', 'reason': '', 'tag': 'music', 'text': "I love pop music! The tunes are so catchy and upbeat! I'm a big fan of Taylor Swift. "}}, 'dependency_parsing': {'dependency_parent': [[0], [3, 3, 0, 3]], 'dependency_label': [['discourse'], ['nsubj', 'aux', 'parataxis', 'preterm']], 'pos_tagging': [['INTJ'], ['PRON', 'AUX', 'INTJ', 'NOUN']]}, 'sentence_completion_text': 'yes i wanted like techno', 'text': 'yes i do like techno', 'topic_module': '', 'topic_keywords': [{'confidence': 999.0, 'keyword': 'techno'}], 'knowledge': [['techno', 'Musical event', '22.133463', '__EMPTY__']], 'noun_phrase': ['techno']}, None], 'returnnlp': [[{'text': 'yes', 'dialog_act': ['pos_answer', '0.9995905756950378'], 'intent': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}, 'sentiment': {'neg': '0.0', 'neu': '0.0', 'pos': '1.0', 'compound': '0.4019'}, 'googlekg': [], 'noun_phrase': [], 'topic': {'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'yes', 'topicKeywords': []}, 'concept': [], 'dependency_parsing': {'head': [0], 'label': ['discourse']}, 'pos_tagging': ['INTJ']}, {'text': 'i do like techno', 'dialog_act': ['statement', '0.7159385085105896'], 'intent': {'sys': [], 'topic': [], 'lexical': ['ans_like']}, 'sentiment': {'neg': '0.0', 'neu': '0.444', 'pos': '0.556', 'compound': '0.3612'}, 'googlekg': [['techno', 'Musical event', '22.133463', '__EMPTY__']], 'noun_phrase': ['techno'], 'topic': {'topicClass': 'Music', 'confidence': 999.0, 'text': 'i do like techno', 'topicKeywords': [{'confidence': 999.0, 'keyword': 'techno'}]}, 'concept': [{'noun': 'techno', 'data': {'genre': '0.5829', 'music': '0.2362', 'style': '0.1809'}}], 'dependency_parsing': {'head': [3, 3, 0, 3], 'label': ['nsubj', 'aux', 'parataxis', 'preterm']}, 'pos_tagging': ['PRON', 'AUX', 'INTJ', 'NOUN']}], [{'sentiment': {'neg': '0.0', 'pos': '0.0', 'compound': '0.0', 'neu': '1.0'}, 'googlekg': [['kevin', 'American actor', '52.16864', 'movie']], 'pos_tagging': ['PRON', 'NOUN', 'AUX', 'PROPN'], 'concept': [{'data': {'client': '0.2', 'character': '0.55', 'person': '0.25'}, 'noun': 'kevin'}], 'dependency_parsing': {'head': [2, 4, 4, 0], 'label': ['nmod:poss', 'nsubj', 'cop', 'parataxis']}, 'topic': {'topicKeywords': [{'confidence': 999}], 'text': 'my name is kevin', 'topicClass': 'Phatic', 'confidence': 999}, 'text': 'my name is kevin', 'noun_phrase': ['kevin'], 'dialog_act': ['statement', '0.9654770493507385'], 'intent': {'sys': [], 'topic': [], 'lexical': ['info_name']}}]], 'central_elem': [{'senti': 'pos', 'regex': {'sys': [], 'topic': [], 'lexical': ['ans_like']}, 'DA': 'statement', 'DA_score': '0.7159385085105896', 'module': [], 'np': ['techno'], 'text': 'i do like techno', 'backstory': {'confidence': 0.6322230100631714, 'followup': 'What kind of music do you like? ', 'module': 'MUSICCHAT', 'neg': '', 'pos': '', 'postfix': '', 'question': 'Do you like pop music', 'reason': '', 'tag': 'music', 'text': "I love pop music! The tunes are so catchy and upbeat! I'm a big fan of Taylor Swift. "}}, {'backstory': {'question': 'do you like my name', 'confidence': 0.6024399995803833, 'text': "Yeah, it's a nice name! "}, 'regex': {'sys': [], 'topic': [], 'lexical': ['info_name']}, 'np': ['kevin'], 'DA_score': '0.9654770493507385', 'senti': 'neu', 'module': [], 'text': 'my name is kevin', 'DA': 'statement'}], 'a_b_test': ['A', 'A'], 'suggest_keywords': None, 'last_module': 'LAUNCHGREETING', 'template_manager': {'prev_hash': {'PPsGsg': ['7WwlVw'], 'gN4J9Q': ['1rwq/w'], 'rJ8LnQ': [], 'uJ0L6w': [], 'W98ISw': [], 'OecOKA': ['OAceTA']}}, 'techsciencechat': {'propose_continue': 'STOP'}, 'usr_name': None, 'module_selection': {'module_rank': {'MUSICCHAT': 0, 'GAMECHAT': 0, 'SPORT': 0, 'TECHSCIENCECHAT': 0, 'TRAVELCHAT': 0, 'NEWS': 0, 'BOOKCHAT': 0, 'ANIMALCHAT': 0, 'FOODCHAT': 0, 'MOVIECHAT': 0}, 'used_topic': ['LAUNCHGREETING', 'TECHSCIENCECHAT']}}
    #     s2 = {'text': ["let's talk about engineering", 'yes i do like techno'], 'features': [{'intent_classify': {'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': []}, 'googlekg': [[]], 'ner': None, 'sentiment': 'neu', 'asrcorrection': {'engineering': ['engineering']}, 'topic': [{'topicClass': 'SciTech', 'confidence': 999.0, 'text': "let's talk about engineering", 'topicKeywords': [{'confidence': 999.0, 'keyword': 'engineering'}]}], 'intent_classify2': [{'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': []}], 'returnnlp': [{'text': "let's talk about engineering", 'dialog_act': ['commands', '0.9270387291908264'], 'intent': {'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': []}, 'sentiment': {'neg': '0.0', 'neu': '1.0', 'pos': '0.0', 'compound': '0.0'}, 'googlekg': [], 'noun_phrase': ['engineering'], 'topic': {'topicClass': 'SciTech', 'confidence': 999.0, 'text': "let's talk about engineering", 'topicKeywords': [{'confidence': 999.0, 'keyword': 'engineering'}]}, 'concept': [{'noun': 'engineering', 'data': {'field': '0.4364', 'discipline': '0.296', 'profession': '0.2676'}}], 'dependency_parsing': {'head': [2, 0, 4, 2], 'label': ['obj', 'xcomp', 'case', 'obl']}, 'pos_tagging': ['NOUN', 'VERB', 'ADP', 'NOUN']}], 'segmentation': {'segmented_text': ["let's talk about engineering"], 'segmented_text_raw': ["let's talk about engineering"]}, 'topic2': [{'topicClass': 'SciTech', 'confidence': 999.0, 'text': "let's talk about engineering", 'topicKeywords': [{'confidence': 999.0, 'keyword': 'engineering'}]}], 'nounphrase2': {'noun_phrase': [['engineering']], 'local_noun_phrase': [[]]}, 'coreference': None, 'converttext': "let's talk about engineering", 'sentiment2': [{'neg': '0.0', 'neu': '1.0', 'pos': '0.0', 'compound': '0.0'}], 'intent': 'general', 'npknowledge': {'knowledge': [], 'noun_phrase': ['engineering'], 'local_noun_phrase': []}, 'concept': [{'noun': 'engineering', 'data': {'field': '0.4364', 'discipline': '0.296', 'profession': '0.2676'}}], 'profanity_check': [0], 'dialog_act': [{'text': "let's talk about engineering", 'DA': 'commands', 'confidence': 0.9270387291908264}], 'spacynp': ['engineering'], 'central_elem': {'senti': 'neu', 'regex': {'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': []}, 'DA': 'commands', 'DA_score': '0.9270387291908264', 'module': ['TECHSCIENCECHAT'], 'np': ['engineering'], 'text': "let's talk about engineering", 'backstory': {'confidence': 0.5041292309761047, 'followup': '', 'module': 'TECHSCIENCECHAT', 'neg': '', 'pos': '', 'postfix': '', 'question': 'do you like science', 'reason': '', 'tag': '', 'text': "I'm a big fan of A.I. research, for obvious reasons. "}}, 'dependency_parsing': {'dependency_parent': [[2, 0, 4, 2]], 'dependency_label': [['obj', 'xcomp', 'case', 'obl']], 'pos_tagging': [['NOUN', 'VERB', 'ADP', 'NOUN']]}, 'sentence_completion_text': "let 's talk about engineering", 'text': "let's talk about engineering", 'topic_module': '', 'topic_keywords': [{'confidence': 999.0, 'keyword': 'engineering'}], 'knowledge': [], 'noun_phrase': ['engineering']}, None], 'returnnlp': [[{'text': "let's talk about engineering", 'dialog_act': ['commands', '0.9270387291908264'], 'intent': {'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': []}, 'sentiment': {'neg': '0.0', 'neu': '1.0', 'pos': '0.0', 'compound': '0.0'}, 'googlekg': [], 'noun_phrase': ['engineering'], 'topic': {'topicClass': 'SciTech', 'confidence': 999.0, 'text': "let's talk about engineering", 'topicKeywords': [{'confidence': 999.0, 'keyword': 'engineering'}]}, 'concept': [{'noun': 'engineering', 'data': {'field': '0.4364', 'discipline': '0.296', 'profession': '0.2676'}}], 'dependency_parsing': {'head': [2, 0, 4, 2], 'label': ['obj', 'xcomp', 'case', 'obl']}, 'pos_tagging': ['NOUN', 'VERB', 'ADP', 'NOUN']}], [{'sentiment': {'neg': '0.0', 'pos': '1.0', 'compound': '0.4019', 'neu': '0.0'}, 'googlekg': [], 'pos_tagging': ['INTJ'], 'concept': [], 'dependency_parsing': {'head': [0], 'label': ['discourse']}, 'topic': {'topicKeywords': [], 'text': 'yes', 'topicClass': 'Phatic', 'confidence': 999}, 'text': 'yes', 'noun_phrase': [], 'dialog_act': ['pos_answer', '0.9995905756950378'], 'intent': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}}, {'sentiment': {'neg': '0.0', 'pos': '0.556', 'compound': '0.3612', 'neu': '0.444'}, 'googlekg': [['techno', 'Musical event', '22.133463', '__EMPTY__']], 'pos_tagging': ['PRON', 'AUX', 'INTJ', 'NOUN'], 'concept': [{'data': {'genre': '0.5829', 'style': '0.1809', 'music': '0.2362'}, 'noun': 'techno'}], 'dependency_parsing': {'head': [3, 3, 0, 3], 'label': ['nsubj', 'aux', 'parataxis', 'preterm']}, 'topic': {'topicKeywords': [{'keyword': 'techno', 'confidence': 999}], 'text': 'i do like techno', 'topicClass': 'Music', 'confidence': 999}, 'text': 'i do like techno', 'noun_phrase': ['techno'], 'dialog_act': ['statement', '0.7159385085105896'], 'intent': {'sys': [], 'topic': [], 'lexical': ['ans_like']}}]], 'central_elem': [{'senti': 'neu', 'regex': {'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': []}, 'DA': 'commands', 'DA_score': '0.9270387291908264', 'module': ['TECHSCIENCECHAT'], 'np': ['engineering'], 'text': "let's talk about engineering", 'backstory': {'confidence': 0.5041292309761047, 'followup': '', 'module': 'TECHSCIENCECHAT', 'neg': '', 'pos': '', 'postfix': '', 'question': 'do you like science', 'reason': '', 'tag': '', 'text': "I'm a big fan of A.I. research, for obvious reasons. "}}, {'backstory': {'followup': 'What kind of music do you like? ', 'tag': 'music', 'text': "I love pop music! The tunes are so catchy and upbeat! I'm a big fan of Taylor Swift. ", 'question': 'Do you like pop music', 'confidence': 0.6322230100631714, 'module': 'MUSICCHAT'}, 'regex': {'sys': [], 'topic': [], 'lexical': ['ans_like']}, 'np': ['techno'], 'DA_score': '0.7159385085105896', 'senti': 'pos', 'module': [], 'text': 'i do like techno', 'DA': 'statement'}], 'a_b_test': ['A', 'A'], 'suggest_keywords': None, 'last_module': 'TECHSCIENCECHAT', 'template_manager': {'prev_hash': {'PPsGsg': ['7WwlVw'], 'gN4J9Q': ['1rwq/w'], 'rJ8LnQ': [], 'uJ0L6w': [], 'W98ISw': [], 'OecOKA': ['OAceTA']}}, 'techsciencechat': {'research_counter': 0, 'current_topic': 'technology', 'fact_counter': 0, 'current_transition': 't_intro', 'propose_topic': ['engineering', 'computers'], 'not_first_time': True, 'topic_history': ['technology'], 'propose_continue': 'CONTINUE', 'context_topic': ['technology', 'artificial_intelligence', 'astronomy', 'biology', 'chemistry', 'engineering', 'medicine', 'physics', 'science', 'techno']}, 'usr_name': None, 'module_selection': {'module_rank': {'MUSICCHAT': 0, 'GAMECHAT': 0, 'SPORT': 0, 'TECHSCIENCECHAT': 0, 'TRAVELCHAT': 0, 'NEWS': 0, 'BOOKCHAT': 0, 'ANIMALCHAT': 0, 'FOODCHAT': 0, 'MOVIECHAT': 0}, 'used_topic': ['LAUNCHGREETING', 'TECHSCIENCECHAT']}}
    #     s3 = {'text': ['yes', "let's talk about engineering"], 'features': [{'intent_classify': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}, 'googlekg': [[]], 'ner': None, 'sentiment': 'pos', 'asrcorrection': None, 'topic': [{'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'yes', 'topicKeywords': []}], 'intent_classify2': [{'sys': [], 'topic': [], 'lexical': ['ans_pos']}], 'returnnlp': [{'text': 'yes', 'dialog_act': ['pos_answer', '0.8112116456031799'], 'intent': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}, 'sentiment': {'neg': '0.0', 'neu': '0.0', 'pos': '1.0', 'compound': '0.4019'}, 'googlekg': [], 'noun_phrase': [], 'topic': {'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'yes', 'topicKeywords': []}, 'concept': [], 'dependency_parsing': {'head': [0], 'label': ['discourse']}, 'pos_tagging': ['INTJ']}], 'segmentation': {'segmented_text': ['yes'], 'segmented_text_raw': ['yes']}, 'topic2': [{'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'yes', 'topicKeywords': []}], 'nounphrase2': {'noun_phrase': [[]], 'local_noun_phrase': [[]]}, 'coreference': None, 'converttext': 'yes', 'sentiment2': [{'neg': '0.0', 'neu': '0.0', 'pos': '1.0', 'compound': '0.4019'}], 'intent': 'general', 'npknowledge': {'knowledge': [], 'noun_phrase': [], 'local_noun_phrase': []}, 'concept': [], 'profanity_check': [0], 'dialog_act': [{'text': 'yes', 'DA': 'pos_answer', 'confidence': 0.8112116456031799}], 'spacynp': None, 'central_elem': {'senti': 'pos', 'regex': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}, 'DA': 'pos_answer', 'DA_score': '0.8112116456031799', 'module': [], 'np': [], 'text': 'yes', 'backstory': {'confidence': 0.4728533625602722, 'followup': '', 'module': '', 'neg': '', 'pos': '', 'postfix': '', 'question': 'why do you ask me', 'reason': '', 'tag': '', 'text': 'Oh, I was just trying to get to know more about you. '}}, 'dependency_parsing': {'dependency_parent': [[0]], 'dependency_label': [['discourse']], 'pos_tagging': [['INTJ']]}, 'sentence_completion_text': 'yes', 'text': 'yes', 'topic_module': '', 'topic_keywords': [], 'knowledge': [], 'noun_phrase': []}, None], 'returnnlp': [[{'text': 'yes', 'dialog_act': ['pos_answer', '0.8112116456031799'], 'intent': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}, 'sentiment': {'neg': '0.0', 'neu': '0.0', 'pos': '1.0', 'compound': '0.4019'}, 'googlekg': [], 'noun_phrase': [], 'topic': {'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'yes', 'topicKeywords': []}, 'concept': [], 'dependency_parsing': {'head': [0], 'label': ['discourse']}, 'pos_tagging': ['INTJ']}], [{'sentiment': {'neg': '0.0', 'pos': '0.0', 'compound': '0.0', 'neu': '1.0'}, 'googlekg': [], 'pos_tagging': ['NOUN', 'VERB', 'ADP', 'NOUN'], 'concept': [{'data': {'field': '0.4364', 'profession': '0.2676', 'discipline': '0.296'}, 'noun': 'engineering'}], 'dependency_parsing': {'head': [2, 0, 4, 2], 'label': ['obj', 'xcomp', 'case', 'obl']}, 'topic': {'topicKeywords': [{'keyword': 'engineering', 'confidence': 999}], 'text': "let's talk about engineering", 'topicClass': 'SciTech', 'confidence': 999}, 'text': "let's talk about engineering", 'noun_phrase': ['engineering'], 'dialog_act': ['commands', '0.9270387291908264'], 'intent': {'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': []}}]], 'central_elem': [{'senti': 'pos', 'regex': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}, 'DA': 'pos_answer', 'DA_score': '0.8112116456031799', 'module': [], 'np': [], 'text': 'yes', 'backstory': {'confidence': 0.4728533625602722, 'followup': '', 'module': '', 'neg': '', 'pos': '', 'postfix': '', 'question': 'why do you ask me', 'reason': '', 'tag': '', 'text': 'Oh, I was just trying to get to know more about you. '}}, {'backstory': {'text': "I'm a big fan of A.I. research, for obvious reasons. ", 'question': 'do you like science', 'confidence': 0.5041292309761047, 'module': 'TECHSCIENCECHAT'}, 'regex': {'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': []}, 'np': ['engineering'], 'DA_score': '0.9270387291908264', 'senti': 'neu', 'module': ['TECHSCIENCECHAT'], 'text': "let's talk about engineering", 'DA': 'commands'}], 'a_b_test': ['A', 'A'], 'suggest_keywords': None, 'last_module': 'TECHSCIENCECHAT', 'template_manager': {'prev_hash': {'PPsGsg': ['7WwlVw'], 'gN4J9Q': ['1rwq/w'], 'rJ8LnQ': [], 'uJ0L6w': [], 'W98ISw': [], 'OecOKA': ['OAceTA']}}, 'techsciencechat': {'research_counter': 0, 'current_topic': 'technology', 'fact_counter': 0, 'current_transition': 't_ask_more', 'not_first_time': True, 'topic_history': ['technology'], 'propose_continue': 'CONTINUE', 'context_topic': ['technology', 'artificial_intelligence', 'astronomy', 'biology', 'chemistry', 'engineering', 'medicine', 'physics', 'science', 'techno']}, 'usr_name': None, 'module_selection': {'module_rank': {'MUSICCHAT': 0, 'GAMECHAT': 0, 'SPORT': 0, 'TECHSCIENCECHAT': 0, 'TRAVELCHAT': 0, 'NEWS': 0, 'BOOKCHAT': 0, 'ANIMALCHAT': 0, 'FOODCHAT': 0, 'MOVIECHAT': 0}, 'used_topic': ['LAUNCHGREETING', 'TECHSCIENCECHAT']}}

    #     conversations = [s1, s2, s3]
    #     automaton = self.get_automaton()
        

    #     for msg in conversations:
    #         response = automaton.transduce(msg)
    #         print(response["techsciencechat"]["current_transition"])

    # def test_conversation_change(self):
    #     """Init follows two path:
    #     s_askmore or s_intro
    #     """
    #     s1 = {'text': ['yes i do like techno', 'my name is kevin'], 'features': [{'intent_classify': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}, 'googlekg': [[], [['techno', 'Musical event', '22.133463', '__EMPTY__']]], 'ner': None, 'sentiment': 'pos', 'asrcorrection': {'techno': ['technology']}, 'topic': [{'topicClass': 'Music', 'confidence': 999.0, 'text': 'yes i do like techno', 'topicKeywords': [{'confidence': 999.0, 'keyword': 'techno'}]}], 'intent_classify2': [{'sys': [], 'topic': [], 'lexical': ['ans_pos']}, {'sys': [], 'topic': [], 'lexical': ['ans_like']}], 'returnnlp': [{'text': 'yes', 'dialog_act': ['pos_answer', '0.9995905756950378'], 'intent': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}, 'sentiment': {'neg': '0.0', 'neu': '0.0', 'pos': '1.0', 'compound': '0.4019'}, 'googlekg': [], 'noun_phrase': [], 'topic': {'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'yes', 'topicKeywords': []}, 'concept': [], 'dependency_parsing': {'head': [0], 'label': ['discourse']}, 'pos_tagging': ['INTJ']}, {'text': 'i do like techno', 'dialog_act': ['statement', '0.7159385085105896'], 'intent': {'sys': [], 'topic': [], 'lexical': ['ans_like']}, 'sentiment': {'neg': '0.0', 'neu': '0.444', 'pos': '0.556', 'compound': '0.3612'}, 'googlekg': [['techno', 'Musical event', '22.133463', '__EMPTY__']], 'noun_phrase': ['techno'], 'topic': {'topicClass': 'Music', 'confidence': 999.0, 'text': 'i do like techno', 'topicKeywords': [{'confidence': 999.0, 'keyword': 'techno'}]}, 'concept': [{'noun': 'techno', 'data': {'genre': '0.5829', 'music': '0.2362', 'style': '0.1809'}}], 'dependency_parsing': {'head': [3, 3, 0, 3], 'label': ['nsubj', 'aux', 'parataxis', 'preterm']}, 'pos_tagging': ['PRON', 'AUX', 'INTJ', 'NOUN']}], 'segmentation': {'segmented_text': ['yes', 'i do like techno'], 'segmented_text_raw': ['yes', 'i do like techno']}, 'topic2': [{'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'yes', 'topicKeywords': []}, {'topicClass': 'Music', 'confidence': 999.0, 'text': 'i do like techno', 'topicKeywords': [{'confidence': 999.0, 'keyword': 'techno'}]}], 'nounphrase2': {'noun_phrase': [[], ['techno']], 'local_noun_phrase': [[], ['techno']]}, 'coreference': None, 'converttext': 'yes i do like techno', 'sentiment2': [{'neg': '0.0', 'neu': '0.0', 'pos': '1.0', 'compound': '0.4019'}, {'neg': '0.0', 'neu': '0.444', 'pos': '0.556', 'compound': '0.3612'}], 'intent': 'general', 'npknowledge': {'knowledge': [['techno', 'Musical event', '22.133463', '__EMPTY__']], 'noun_phrase': ['techno'], 'local_noun_phrase': ['techno']}, 'concept': [{'noun': 'techno', 'data': {'genre': '0.5829', 'music': '0.2362', 'style': '0.1809'}}], 'profanity_check': [0], 'dialog_act': [{'text': 'yes', 'DA': 'pos_answer', 'confidence': 0.9995905756950378}, {'text': 'i do like techno', 'DA': 'statement', 'confidence': 0.7159385085105896}], 'spacynp': ['techno'], 'central_elem': {'senti': 'pos', 'regex': {'sys': [], 'topic': [], 'lexical': ['ans_like']}, 'DA': 'statement', 'DA_score': '0.7159385085105896', 'module': [], 'np': ['techno'], 'text': 'i do like techno', 'backstory': {'confidence': 0.6322230100631714, 'followup': 'What kind of music do you like? ', 'module': 'MUSICCHAT', 'neg': '', 'pos': '', 'postfix': '', 'question': 'Do you like pop music', 'reason': '', 'tag': 'music', 'text': "I love pop music! The tunes are so catchy and upbeat! I'm a big fan of Taylor Swift. "}}, 'dependency_parsing': {'dependency_parent': [[0], [3, 3, 0, 3]], 'dependency_label': [['discourse'], ['nsubj', 'aux', 'parataxis', 'preterm']], 'pos_tagging': [['INTJ'], ['PRON', 'AUX', 'INTJ', 'NOUN']]}, 'sentence_completion_text': 'yes i wanted like techno', 'text': 'yes i do like techno', 'topic_module': '', 'topic_keywords': [{'confidence': 999.0, 'keyword': 'techno'}], 'knowledge': [['techno', 'Musical event', '22.133463', '__EMPTY__']], 'noun_phrase': ['techno']}, None], 'returnnlp': [[{'text': 'yes', 'dialog_act': ['pos_answer', '0.9995905756950378'], 'intent': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}, 'sentiment': {'neg': '0.0', 'neu': '0.0', 'pos': '1.0', 'compound': '0.4019'}, 'googlekg': [], 'noun_phrase': [], 'topic': {'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'yes', 'topicKeywords': []}, 'concept': [], 'dependency_parsing': {'head': [0], 'label': ['discourse']}, 'pos_tagging': ['INTJ']}, {'text': 'i do like techno', 'dialog_act': ['statement', '0.7159385085105896'], 'intent': {'sys': [], 'topic': [], 'lexical': ['ans_like']}, 'sentiment': {'neg': '0.0', 'neu': '0.444', 'pos': '0.556', 'compound': '0.3612'}, 'googlekg': [['techno', 'Musical event', '22.133463', '__EMPTY__']], 'noun_phrase': ['techno'], 'topic': {'topicClass': 'Music', 'confidence': 999.0, 'text': 'i do like techno', 'topicKeywords': [{'confidence': 999.0, 'keyword': 'techno'}]}, 'concept': [{'noun': 'techno', 'data': {'genre': '0.5829', 'music': '0.2362', 'style': '0.1809'}}], 'dependency_parsing': {'head': [3, 3, 0, 3], 'label': ['nsubj', 'aux', 'parataxis', 'preterm']}, 'pos_tagging': ['PRON', 'AUX', 'INTJ', 'NOUN']}], [{'sentiment': {'neg': '0.0', 'pos': '0.0', 'compound': '0.0', 'neu': '1.0'}, 'googlekg': [['kevin', 'American actor', '52.16864', 'movie']], 'pos_tagging': ['PRON', 'NOUN', 'AUX', 'PROPN'], 'concept': [{'data': {'client': '0.2', 'character': '0.55', 'person': '0.25'}, 'noun': 'kevin'}], 'dependency_parsing': {'head': [2, 4, 4, 0], 'label': ['nmod:poss', 'nsubj', 'cop', 'parataxis']}, 'topic': {'topicKeywords': [{'confidence': 999}], 'text': 'my name is kevin', 'topicClass': 'Phatic', 'confidence': 999}, 'text': 'my name is kevin', 'noun_phrase': ['kevin'], 'dialog_act': ['statement', '0.9654770493507385'], 'intent': {'sys': [], 'topic': [], 'lexical': ['info_name']}}]], 'central_elem': [{'senti': 'pos', 'regex': {'sys': [], 'topic': [], 'lexical': ['ans_like']}, 'DA': 'statement', 'DA_score': '0.7159385085105896', 'module': [], 'np': ['techno'], 'text': 'i do like techno', 'backstory': {'confidence': 0.6322230100631714, 'followup': 'What kind of music do you like? ', 'module': 'MUSICCHAT', 'neg': '', 'pos': '', 'postfix': '', 'question': 'Do you like pop music', 'reason': '', 'tag': 'music', 'text': "I love pop music! The tunes are so catchy and upbeat! I'm a big fan of Taylor Swift. "}}, {'backstory': {'question': 'do you like my name', 'confidence': 0.6024399995803833, 'text': "Yeah, it's a nice name! "}, 'regex': {'sys': [], 'topic': [], 'lexical': ['info_name']}, 'np': ['kevin'], 'DA_score': '0.9654770493507385', 'senti': 'neu', 'module': [], 'text': 'my name is kevin', 'DA': 'statement'}], 'a_b_test': ['A', 'A'], 'suggest_keywords': None, 'last_module': 'LAUNCHGREETING', 'template_manager': {'prev_hash': {'PPsGsg': ['7WwlVw'], 'gN4J9Q': ['1rwq/w'], 'rJ8LnQ': [], 'uJ0L6w': [], 'W98ISw': [], 'OecOKA': ['OAceTA']}}, 'techsciencechat': {'propose_continue': 'STOP'}, 'usr_name': None, 'module_selection': {'module_rank': {'MUSICCHAT': 0, 'GAMECHAT': 0, 'SPORT': 0, 'TECHSCIENCECHAT': 0, 'TRAVELCHAT': 0, 'NEWS': 0, 'BOOKCHAT': 0, 'ANIMALCHAT': 0, 'FOODCHAT': 0, 'MOVIECHAT': 0}, 'used_topic': ['LAUNCHGREETING', 'TECHSCIENCECHAT']}}
    #     s2 = {'text': ["let's talk about engineering", 'yes i do like techno'], 'features': [{'intent_classify': {'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': []}, 'googlekg': [[]], 'ner': None, 'sentiment': 'neu', 'asrcorrection': {'engineering': ['engineering']}, 'topic': [{'topicClass': 'SciTech', 'confidence': 999.0, 'text': "let's talk about engineering", 'topicKeywords': [{'confidence': 999.0, 'keyword': 'engineering'}]}], 'intent_classify2': [{'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': []}], 'returnnlp': [{'text': "let's talk about engineering", 'dialog_act': ['commands', '0.9270387291908264'], 'intent': {'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': []}, 'sentiment': {'neg': '0.0', 'neu': '1.0', 'pos': '0.0', 'compound': '0.0'}, 'googlekg': [], 'noun_phrase': ['engineering'], 'topic': {'topicClass': 'SciTech', 'confidence': 999.0, 'text': "let's talk about engineering", 'topicKeywords': [{'confidence': 999.0, 'keyword': 'engineering'}]}, 'concept': [{'noun': 'engineering', 'data': {'field': '0.4364', 'discipline': '0.296', 'profession': '0.2676'}}], 'dependency_parsing': {'head': [2, 0, 4, 2], 'label': ['obj', 'xcomp', 'case', 'obl']}, 'pos_tagging': ['NOUN', 'VERB', 'ADP', 'NOUN']}], 'segmentation': {'segmented_text': ["let's talk about engineering"], 'segmented_text_raw': ["let's talk about engineering"]}, 'topic2': [{'topicClass': 'SciTech', 'confidence': 999.0, 'text': "let's talk about engineering", 'topicKeywords': [{'confidence': 999.0, 'keyword': 'engineering'}]}], 'nounphrase2': {'noun_phrase': [['engineering']], 'local_noun_phrase': [[]]}, 'coreference': None, 'converttext': "let's talk about engineering", 'sentiment2': [{'neg': '0.0', 'neu': '1.0', 'pos': '0.0', 'compound': '0.0'}], 'intent': 'general', 'npknowledge': {'knowledge': [], 'noun_phrase': ['engineering'], 'local_noun_phrase': []}, 'concept': [{'noun': 'engineering', 'data': {'field': '0.4364', 'discipline': '0.296', 'profession': '0.2676'}}], 'profanity_check': [0], 'dialog_act': [{'text': "let's talk about engineering", 'DA': 'commands', 'confidence': 0.9270387291908264}], 'spacynp': ['engineering'], 'central_elem': {'senti': 'neu', 'regex': {'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': []}, 'DA': 'commands', 'DA_score': '0.9270387291908264', 'module': ['TECHSCIENCECHAT'], 'np': ['engineering'], 'text': "let's talk about engineering", 'backstory': {'confidence': 0.5041292309761047, 'followup': '', 'module': 'TECHSCIENCECHAT', 'neg': '', 'pos': '', 'postfix': '', 'question': 'do you like science', 'reason': '', 'tag': '', 'text': "I'm a big fan of A.I. research, for obvious reasons. "}}, 'dependency_parsing': {'dependency_parent': [[2, 0, 4, 2]], 'dependency_label': [['obj', 'xcomp', 'case', 'obl']], 'pos_tagging': [['NOUN', 'VERB', 'ADP', 'NOUN']]}, 'sentence_completion_text': "let 's talk about engineering", 'text': "let's talk about engineering", 'topic_module': '', 'topic_keywords': [{'confidence': 999.0, 'keyword': 'engineering'}], 'knowledge': [], 'noun_phrase': ['engineering']}, None], 'returnnlp': [[{'text': "let's talk about engineering", 'dialog_act': ['commands', '0.9270387291908264'], 'intent': {'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': []}, 'sentiment': {'neg': '0.0', 'neu': '1.0', 'pos': '0.0', 'compound': '0.0'}, 'googlekg': [], 'noun_phrase': ['engineering'], 'topic': {'topicClass': 'SciTech', 'confidence': 999.0, 'text': "let's talk about engineering", 'topicKeywords': [{'confidence': 999.0, 'keyword': 'engineering'}]}, 'concept': [{'noun': 'engineering', 'data': {'field': '0.4364', 'discipline': '0.296', 'profession': '0.2676'}}], 'dependency_parsing': {'head': [2, 0, 4, 2], 'label': ['obj', 'xcomp', 'case', 'obl']}, 'pos_tagging': ['NOUN', 'VERB', 'ADP', 'NOUN']}], [{'sentiment': {'neg': '0.0', 'pos': '1.0', 'compound': '0.4019', 'neu': '0.0'}, 'googlekg': [], 'pos_tagging': ['INTJ'], 'concept': [], 'dependency_parsing': {'head': [0], 'label': ['discourse']}, 'topic': {'topicKeywords': [], 'text': 'yes', 'topicClass': 'Phatic', 'confidence': 999}, 'text': 'yes', 'noun_phrase': [], 'dialog_act': ['pos_answer', '0.9995905756950378'], 'intent': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}}, {'sentiment': {'neg': '0.0', 'pos': '0.556', 'compound': '0.3612', 'neu': '0.444'}, 'googlekg': [['techno', 'Musical event', '22.133463', '__EMPTY__']], 'pos_tagging': ['PRON', 'AUX', 'INTJ', 'NOUN'], 'concept': [{'data': {'genre': '0.5829', 'style': '0.1809', 'music': '0.2362'}, 'noun': 'techno'}], 'dependency_parsing': {'head': [3, 3, 0, 3], 'label': ['nsubj', 'aux', 'parataxis', 'preterm']}, 'topic': {'topicKeywords': [{'keyword': 'techno', 'confidence': 999}], 'text': 'i do like techno', 'topicClass': 'Music', 'confidence': 999}, 'text': 'i do like techno', 'noun_phrase': ['techno'], 'dialog_act': ['statement', '0.7159385085105896'], 'intent': {'sys': [], 'topic': [], 'lexical': ['ans_like']}}]], 'central_elem': [{'senti': 'neu', 'regex': {'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': []}, 'DA': 'commands', 'DA_score': '0.9270387291908264', 'module': ['TECHSCIENCECHAT'], 'np': ['engineering'], 'text': "let's talk about engineering", 'backstory': {'confidence': 0.5041292309761047, 'followup': '', 'module': 'TECHSCIENCECHAT', 'neg': '', 'pos': '', 'postfix': '', 'question': 'do you like science', 'reason': '', 'tag': '', 'text': "I'm a big fan of A.I. research, for obvious reasons. "}}, {'backstory': {'followup': 'What kind of music do you like? ', 'tag': 'music', 'text': "I love pop music! The tunes are so catchy and upbeat! I'm a big fan of Taylor Swift. ", 'question': 'Do you like pop music', 'confidence': 0.6322230100631714, 'module': 'MUSICCHAT'}, 'regex': {'sys': [], 'topic': [], 'lexical': ['ans_like']}, 'np': ['techno'], 'DA_score': '0.7159385085105896', 'senti': 'pos', 'module': [], 'text': 'i do like techno', 'DA': 'statement'}], 'a_b_test': ['A', 'A'], 'suggest_keywords': None, 'last_module': 'TECHSCIENCECHAT', 'template_manager': {'prev_hash': {'PPsGsg': ['7WwlVw'], 'gN4J9Q': ['1rwq/w'], 'rJ8LnQ': [], 'uJ0L6w': [], 'W98ISw': [], 'OecOKA': ['OAceTA']}}, 'techsciencechat': {'research_counter': 0, 'current_topic': 'technology', 'fact_counter': 0, 'current_transition': 't_intro', 'propose_topic': ['engineering', 'computers'], 'not_first_time': True, 'topic_history': ['technology'], 'propose_continue': 'CONTINUE', 'context_topic': ['technology', 'artificial_intelligence', 'astronomy', 'biology', 'chemistry', 'engineering', 'medicine', 'physics', 'science', 'techno']}, 'usr_name': None, 'module_selection': {'module_rank': {'MUSICCHAT': 0, 'GAMECHAT': 0, 'SPORT': 0, 'TECHSCIENCECHAT': 0, 'TRAVELCHAT': 0, 'NEWS': 0, 'BOOKCHAT': 0, 'ANIMALCHAT': 0, 'FOODCHAT': 0, 'MOVIECHAT': 0}, 'used_topic': ['LAUNCHGREETING', 'TECHSCIENCECHAT']}}
    #     s3 = {'text': ['yes', "let's talk about engineering"], 'features': [{'intent_classify': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}, 'googlekg': [[]], 'ner': None, 'sentiment': 'pos', 'asrcorrection': None, 'topic': [{'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'yes', 'topicKeywords': []}], 'intent_classify2': [{'sys': [], 'topic': [], 'lexical': ['ans_pos']}], 'returnnlp': [{'text': 'yes', 'dialog_act': ['pos_answer', '0.8112116456031799'], 'intent': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}, 'sentiment': {'neg': '0.0', 'neu': '0.0', 'pos': '1.0', 'compound': '0.4019'}, 'googlekg': [], 'noun_phrase': [], 'topic': {'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'yes', 'topicKeywords': []}, 'concept': [], 'dependency_parsing': {'head': [0], 'label': ['discourse']}, 'pos_tagging': ['INTJ']}], 'segmentation': {'segmented_text': ['yes'], 'segmented_text_raw': ['yes']}, 'topic2': [{'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'yes', 'topicKeywords': []}], 'nounphrase2': {'noun_phrase': [[]], 'local_noun_phrase': [[]]}, 'coreference': None, 'converttext': 'yes', 'sentiment2': [{'neg': '0.0', 'neu': '0.0', 'pos': '1.0', 'compound': '0.4019'}], 'intent': 'general', 'npknowledge': {'knowledge': [], 'noun_phrase': [], 'local_noun_phrase': []}, 'concept': [], 'profanity_check': [0], 'dialog_act': [{'text': 'yes', 'DA': 'pos_answer', 'confidence': 0.8112116456031799}], 'spacynp': None, 'central_elem': {'senti': 'pos', 'regex': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}, 'DA': 'pos_answer', 'DA_score': '0.8112116456031799', 'module': [], 'np': [], 'text': 'yes', 'backstory': {'confidence': 0.4728533625602722, 'followup': '', 'module': '', 'neg': '', 'pos': '', 'postfix': '', 'question': 'why do you ask me', 'reason': '', 'tag': '', 'text': 'Oh, I was just trying to get to know more about you. '}}, 'dependency_parsing': {'dependency_parent': [[0]], 'dependency_label': [['discourse']], 'pos_tagging': [['INTJ']]}, 'sentence_completion_text': 'yes', 'text': 'yes', 'topic_module': '', 'topic_keywords': [], 'knowledge': [], 'noun_phrase': []}, None], 'returnnlp': [[{'text': 'yes', 'dialog_act': ['pos_answer', '0.8112116456031799'], 'intent': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}, 'sentiment': {'neg': '0.0', 'neu': '0.0', 'pos': '1.0', 'compound': '0.4019'}, 'googlekg': [], 'noun_phrase': [], 'topic': {'topicClass': 'Phatic', 'confidence': 999.0, 'text': 'yes', 'topicKeywords': []}, 'concept': [], 'dependency_parsing': {'head': [0], 'label': ['discourse']}, 'pos_tagging': ['INTJ']}], [{'sentiment': {'neg': '0.0', 'pos': '0.0', 'compound': '0.0', 'neu': '1.0'}, 'googlekg': [], 'pos_tagging': ['NOUN', 'VERB', 'ADP', 'NOUN'], 'concept': [{'data': {'field': '0.4364', 'profession': '0.2676', 'discipline': '0.296'}, 'noun': 'engineering'}], 'dependency_parsing': {'head': [2, 0, 4, 2], 'label': ['obj', 'xcomp', 'case', 'obl']}, 'topic': {'topicKeywords': [{'keyword': 'engineering', 'confidence': 999}], 'text': "let's talk about engineering", 'topicClass': 'SciTech', 'confidence': 999}, 'text': "let's talk about engineering", 'noun_phrase': ['engineering'], 'dialog_act': ['commands', '0.9270387291908264'], 'intent': {'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': []}}]], 'central_elem': [{'senti': 'pos', 'regex': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}, 'DA': 'pos_answer', 'DA_score': '0.8112116456031799', 'module': [], 'np': [], 'text': 'yes', 'backstory': {'confidence': 0.4728533625602722, 'followup': '', 'module': '', 'neg': '', 'pos': '', 'postfix': '', 'question': 'why do you ask me', 'reason': '', 'tag': '', 'text': 'Oh, I was just trying to get to know more about you. '}}, {'backstory': {'text': "I'm a big fan of A.I. research, for obvious reasons. ", 'question': 'do you like science', 'confidence': 0.5041292309761047, 'module': 'TECHSCIENCECHAT'}, 'regex': {'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': []}, 'np': ['engineering'], 'DA_score': '0.9270387291908264', 'senti': 'neu', 'module': ['TECHSCIENCECHAT'], 'text': "let's talk about engineering", 'DA': 'commands'}], 'a_b_test': ['A', 'A'], 'suggest_keywords': None, 'last_module': 'TECHSCIENCECHAT', 'template_manager': {'prev_hash': {'PPsGsg': ['7WwlVw'], 'gN4J9Q': ['1rwq/w'], 'rJ8LnQ': [], 'uJ0L6w': [], 'W98ISw': [], 'OecOKA': ['OAceTA']}}, 'techsciencechat': {'research_counter': 0, 'current_topic': 'technology', 'fact_counter': 0, 'current_transition': 't_ask_more', 'not_first_time': True, 'topic_history': ['technology'], 'propose_continue': 'CONTINUE', 'context_topic': ['technology', 'artificial_intelligence', 'astronomy', 'biology', 'chemistry', 'engineering', 'medicine', 'physics', 'science', 'techno']}, 'usr_name': None, 'module_selection': {'module_rank': {'MUSICCHAT': 0, 'GAMECHAT': 0, 'SPORT': 0, 'TECHSCIENCECHAT': 0, 'TRAVELCHAT': 0, 'NEWS': 0, 'BOOKCHAT': 0, 'ANIMALCHAT': 0, 'FOODCHAT': 0, 'MOVIECHAT': 0}, 'used_topic': ['LAUNCHGREETING', 'TECHSCIENCECHAT']}}
    #     s4 = {'text': ['can we talk about space together', 'yes'], 'features': [{'sentiment': 'neu', 'ner': None, 'converttext': 'can we talk about space together', 'dialog_act': [{'text': 'can we talk about space together', 'DA': 'commands', 'confidence': 0.6250352263450623}], 'returnnlp': [{'text': 'can we talk about space together', 'dialog_act': ['commands', '0.6250352263450623'], 'intent': {'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': ['ask_yesno']}, 'sentiment': {'neg': '0.0', 'neu': '1.0', 'pos': '0.0', 'compound': '0.0'}, 'googlekg': [['space', 'American singer-songwriter', '2114.220947265625', 'music']], 'noun_phrase': ['about space', 'talk about space together', 'space'], 'topic': {'topicClass': 'SciTech', 'confidence': 999.0, 'text': 'can we talk about space together', 'topicKeywords': [{'confidence': 999.0, 'keyword': 'space'}]}, 'concept': [{'noun': 'about space', 'data': {}}, {'noun': 'talk about space together', 'data': {}}, {'noun': 'space', 'data': {'character': '0.4492', 'factor': '0.3134', 'concept': '0.2374'}}], 'dependency_parsing': {'head': [3, 3, 0, 5, 3, 3], 'label': ['aux', 'nsubj', 'acl:relcl', 'case', 'obl', 'advmod']}, 'pos_tagging': ['AUX', 'PRON', 'VERB', 'ADP', 'NOUN', 'ADV']}], 'text': 'can we talk about space together', 'coreference': None, 'intent_classify': {'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': ['ask_yesno']}, 'intent_classify2': [{'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': ['ask_yesno']}], 'asrcorrection': None, 'concept': [{'noun': 'about space', 'data': {}}, {'noun': 'talk about space together', 'data': {}}, {'noun': 'space', 'data': {'character': '0.4492', 'factor': '0.3134', 'concept': '0.2374'}}], 'nounphrase2': {'noun_phrase': [['about space', 'talk about space together', 'space']], 'local_noun_phrase': [['about space', 'space']]}, 'spacynp': ['space'], 'topic2': [{'topicClass': 'SciTech', 'confidence': 999.0, 'text': 'can we talk about space together', 'topicKeywords': [{'confidence': 999.0, 'keyword': 'space'}]}], 'central_elem': {'senti': 'neu', 'regex': {'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': ['ask_yesno']}, 'DA': 'commands', 'DA_score': '0.6250352263450623', 'module': ['TECHSCIENCECHAT', 'MUSICCHAT'], 'np': ['about space', 'talk about space together', 'space'], 'text': 'can we talk about space together', 'backstory': {'confidence': 0.5788989663124084, 'followup': '', 'module': '', 'neg': '', 'pos': '', 'postfix': '', 'question': 'do you believe in space aliens', 'reason': '', 'tag': '', 'text': 'Yes, I believe! <break time="300ms"/> I guess I watched too many sci-fi movies. Huh? '}}, 'dependency_parsing': {'dependency_parent': [[3, 3, 0, 5, 3, 3]], 'dependency_label': [['aux', 'nsubj', 'acl:relcl', 'case', 'obl', 'advmod']], 'pos_tagging': [['AUX', 'PRON', 'VERB', 'ADP', 'NOUN', 'ADV']]}, 'npknowledge': {'knowledge': [['space', 'American singer-songwriter', '2114.220947265625', 'music']], 'noun_phrase': ['about space', 'talk about space together', 'space'], 'local_noun_phrase': ['about space', 'space']}, 'intent': 'general', 'sentiment2': [{'neg': '0.0', 'neu': '1.0', 'pos': '0.0', 'compound': '0.0'}], 'sentence_completion_text': 'can we talk about space together', 'topic': [{'topicClass': 'SciTech', 'confidence': 999.0, 'text': 'can we talk about space together', 'topicKeywords': [{'confidence': 999.0, 'keyword': 'space'}]}], 'segmentation': {'segmented_text': ['can we talk about ner together'], 'segmented_text_raw': ['can we talk about space together']}, 'profanity_check': [0], 'googlekg': [[['space', 'American singer-songwriter', '2114.220947265625', 'music']]], 'topic_module': '', 'topic_keywords': [{'confidence': 999.0, 'keyword': 'space'}], 'knowledge': [['space', 'American singer-songwriter', '2114.220947265625', 'music']], 'noun_phrase': ['about space', 'talk about space together', 'space']}, None], 'returnnlp': [[{'text': 'can we talk about space together', 'dialog_act': ['commands', '0.6250352263450623'], 'intent': {'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': ['ask_yesno']}, 'sentiment': {'neg': '0.0', 'neu': '1.0', 'pos': '0.0', 'compound': '0.0'}, 'googlekg': [['space', 'American singer-songwriter', '2114.220947265625', 'music']], 'noun_phrase': ['about space', 'talk about space together', 'space'], 'topic': {'topicClass': 'SciTech', 'confidence': 999.0, 'text': 'can we talk about space together', 'topicKeywords': [{'confidence': 999.0, 'keyword': 'space'}]}, 'concept': [{'noun': 'about space', 'data': {}}, {'noun': 'talk about space together', 'data': {}}, {'noun': 'space', 'data': {'character': '0.4492', 'factor': '0.3134', 'concept': '0.2374'}}], 'dependency_parsing': {'head': [3, 3, 0, 5, 3, 3], 'label': ['aux', 'nsubj', 'acl:relcl', 'case', 'obl', 'advmod']}, 'pos_tagging': ['AUX', 'PRON', 'VERB', 'ADP', 'NOUN', 'ADV']}], [{'sentiment': {'neg': '0.0', 'pos': '1.0', 'compound': '0.4019', 'neu': '0.0'}, 'googlekg': [], 'pos_tagging': ['INTJ'], 'concept': [], 'dependency_parsing': {'head': [0], 'label': ['discourse']}, 'topic': {'topicKeywords': [], 'text': 'yes', 'topicClass': 'Phatic', 'confidence': 999}, 'text': 'yes', 'noun_phrase': [], 'dialog_act': ['pos_answer', '0.927345335483551'], 'intent': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}}]], 'central_elem': [{'senti': 'neu', 'regex': {'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': ['ask_yesno']}, 'DA': 'commands', 'DA_score': '0.6250352263450623', 'module': ['TECHSCIENCECHAT', 'MUSICCHAT'], 'np': ['about space', 'talk about space together', 'space'], 'text': 'can we talk about space together', 'backstory': {'confidence': 0.5788989663124084, 'followup': '', 'module': '', 'neg': '', 'pos': '', 'postfix': '', 'question': 'do you believe in space aliens', 'reason': '', 'tag': '', 'text': 'Yes, I believe! <break time="300ms"/> I guess I watched too many sci-fi movies. Huh? '}}, {'backstory': {'question': 'why do you ask me', 'confidence': 0.4728533625602722, 'text': 'Oh, I was just trying to get to know more about you. '}, 'regex': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}, 'np': [], 'DA_score': '0.927345335483551', 'senti': 'pos', 'module': [], 'text': 'yes', 'DA': 'pos_answer'}], 'a_b_test': ['A', 'A'], 'suggest_keywords': None, 'last_module': 'TECHSCIENCECHAT', 'template_manager': {'prev_hash': {'u/AXEA': [], 'gN4J9Q': ['/WcXSg'], 'i4kRpw': ['Nrl+gw'], 'u9oXBQ': [], 'yM0X9w': [], '4bQYdA': [], 'llkbNA': ['ffMuLA'], 'PPsGsg': ['fLkfQw'], 'mPIK5Q': ['90043g', 'ovREew', 'nkgr2Q', 'W6QUYQ']}}, 'techsciencechat': {'chitchat_tracker': {'unfinished_ids': {}, 'curr_turn': 0, 'curr_state': 'facts-artificial_intelligence-1', 'completed_ids': []}, 'current_topic': 'artificial_intelligence', 'current_transition': 't_interesting_fact', 'chitchat_state': 'facts-artificial_intelligence-1', 'tech_propose_topic': ['artificial_intelligence', 'computers'], 'not_first_time': True, 'topic_history': {'artificial_intelligence': 0, 'computers': 1}, 'prev_state': 's_interesting_fact', 'propose_continue': 'CONTINUE', 'context_topic': {'artificial_intelligence': {'artificial_intelligence': 3}, 'technology': {'technology': 3}}, 'new_topics': []}, 'usr_name': '<no_name>', 'module_selection': {'used_topic': ['LAUNCHGREETING', 'TECHSCIENCECHAT']}}
    #     conversations = [s1, s2, s3, s4]
    #     automaton = self.get_automaton()
    #     conversations = [s4]


    #     for msg in conversations:

    #         response = automaton.transduce(msg)
    #         print(automaton.topic_handler.dialog_act)
    #         # print(response["techsciencechat"]["current_state"])

if __name__ == '__main__':
    unittest.main()
