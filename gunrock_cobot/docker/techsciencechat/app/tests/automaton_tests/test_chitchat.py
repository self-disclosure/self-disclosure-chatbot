"""Testing Chitchat Automaton
    TODO: Add Chitchat functionality later.
"""
import logging
import copy
import json
import random
import unittest
from techscience_automaton import TechScienceAutomaton

# logging.disable(logging.DEBUG)
# logging.disable(logging.INFO)
# logging.disable(logging.CRITICAL)
# logging.disable(logging.WARNING)

# sample_chitchat_out = {'text': ['tell me about blockchain', 'tell me about artificial intelligence'], 'features': [{'dependency_parsing': {'dependency_parent': [[0, 1, 4, 1]], 'dependency_label': [['ccomp', 'obj', 'case', 'obl']], 'pos_tagging': [['VERB', 'PRON', 'ADP', 'NOUN']]}, 'profanity_check': [0], 'ner': None, 'intent_classify': {'sys': ['req_topic_jump'], 'topic': [], 'lexical': ['ask_info']}, 'npknowledge': {'knowledge': [['Blockchain', 'Event', '12.17116', '__EMPTY__']], 'noun_phrase': ['blockchain'], 'local_noun_phrase': ['blockchain']}, 'concept': [{'noun': 'blockchain', 'data': {'technology': '0.6', 'big bitcoin organization': '0.2', 'website': '0.2'}}], 'spacynp': ['blockchain'], 'intent': 'general', 'nounphrase2': {'noun_phrase': [['blockchain']], 'local_noun_phrase': [['blockchain']]}, 'sentence_completion_text': 'tell me about blockchain', 'topic': [{'topicClass': 'Movies_TV', 'confidence': 0.313, 'text': 'tell me about blockchain', 'topicKeywords': [{'confidence': 0.405, 'keyword': 'me'}]}], 'segmentation': {'segmented_text': ['tell me about blockchain'], 'segmented_text_raw': ['tell me about blockchain']}, 'coreference': None, 'sentiment': 'neu', 'intent_classify2': [{'sys': ['req_topic_jump'], 'topic': [], 'lexical': ['ask_info']}], 'googlekg': [[['Blockchain', 'Event', '12.17116', '__EMPTY__']]], 'central_elem': {'senti': 'neu', 'regex': {'sys': ['req_topic_jump'], 'topic': [], 'lexical': ['ask_info']}, 'DA': 'commands', 'DA_score': '0.9904952645301819', 'module': ['TECHSCIENCECHAT'], 'np': ['blockchain'], 'text': 'tell me about blockchain', 'backstory': {'confidence': 0.5240171551704407, 'followup': '', 'module': '', 'neg': '', 'pos': '', 'postfix': '', 'question': 'what is cortical computation', 'reason': '', 'tag': '', 'text': 'I think it\'s the study of how brains compute <break time="300ms"/> but I don\'t know a lot about it, to be honest. '}}, 'topic2': [{'topicClass': 'Movies_TV', 'confidence': 0.313, 'text': 'tell me about blockchain', 'topicKeywords': [{'confidence': 0.405, 'keyword': 'me'}]}], 'text': 'tell me about blockchain', 'dialog_act': [{'text': 'tell me about blockchain', 'DA': 'commands', 'confidence': 0.9904952645301819}], 'returnnlp': [{'text': 'tell me about blockchain', 'dialog_act': ['commands', '0.9904952645301819'], 'intent': {'sys': ['req_topic_jump'], 'topic': [], 'lexical': ['ask_info']}, 'sentiment': {'neg': '0.0', 'neu': '1.0', 'pos': '0.0', 'compound': '0.0'}, 'googlekg': [['Blockchain', 'Event', '12.17116', '__EMPTY__']], 'noun_phrase': ['blockchain'], 'topic': {'topicClass': 'Movies_TV', 'confidence': 0.313, 'text': 'tell me about blockchain', 'topicKeywords': [{'confidence': 0.405, 'keyword': 'me'}]}, 'concept': [{'noun': 'blockchain', 'data': {'technology': '0.6', 'big bitcoin organization': '0.2', 'website': '0.2'}}], 'dependency_parsing': {'head': [0, 1, 4, 1], 'label': ['ccomp', 'obj', 'case', 'obl']}, 'pos_tagging': ['VERB', 'PRON', 'ADP', 'NOUN']}], 'converttext': 'tell me about blockchain', 'asrcorrection': None, 'sentiment2': [{'neg': '0.0', 'neu': '1.0', 'pos': '0.0', 'compound': '0.0'}], 'topic_module': '', 'topic_keywords': [], 'knowledge': [['Blockchain', 'Event', '12.17116', '__EMPTY__']], 'noun_phrase': ['blockchain']}, None], 'returnnlp': [[{'text': 'tell me about blockchain', 'dialog_act': ['commands', '0.9904952645301819'], 'intent': {'sys': ['req_topic_jump'], 'topic': [], 'lexical': ['ask_info']}, 'sentiment': {'neg': '0.0', 'neu': '1.0', 'pos': '0.0', 'compound': '0.0'}, 'googlekg': [['Blockchain', 'Event', '12.17116', '__EMPTY__']], 'noun_phrase': ['blockchain'], 'topic': {'topicClass': 'Movies_TV', 'confidence': 0.313, 'text': 'tell me about blockchain', 'topicKeywords': [{'confidence': 0.405, 'keyword': 'me'}]}, 'concept': [{'noun': 'blockchain', 'data': {'technology': '0.6', 'big bitcoin organization': '0.2', 'website': '0.2'}}], 'dependency_parsing': {'head': [0, 1, 4, 1], 'label': ['ccomp', 'obj', 'case', 'obl']}, 'pos_tagging': ['VERB', 'PRON', 'ADP', 'NOUN']}], [{'sentiment': {'neg': '0.0', 'pos': '0.437', 'compound': '0.4767', 'neu': '0.563'}, 'googlekg': [['artificial intelligence', '2001 film', '678.277649', 'movie']], 'pos_tagging': ['VERB', 'PRON', 'ADP', 'ADJ', 'NOUN'], 'concept': [{'data': {'field': '0.3136', 'topic': '0.3105', 'technology': '0.376'}, 'noun': 'artificial intelligence'}], 'dependency_parsing': {'head': [0, 1, 5, 5, 1], 'label': ['ccomp', 'obj', 'case', 'amod', 'obl']}, 'topic': {'topicKeywords': [{'keyword': 'intelligence', 'confidence': 999}], 'text': 'tell me about artificial intelligence', 'topicClass': 'SciTech', 'confidence': 999}, 'text': 'tell me about artificial intelligence', 'noun_phrase': ['artificial intelligence'], 'dialog_act': ['commands', '0.8898943066596985'], 'intent': {'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': ['ask_info']}}]], 'central_elem': [{'senti': 'neu', 'regex': {'sys': ['req_topic_jump'], 'topic': [], 'lexical': ['ask_info']}, 'DA': 'commands', 'DA_score': '0.9904952645301819', 'module': ['TECHSCIENCECHAT'], 'np': ['blockchain'], 'text': 'tell me about blockchain', 'backstory': {'confidence': 0.5240171551704407, 'followup': '', 'module': '', 'neg': '', 'pos': '', 'postfix': '', 'question': 'what is cortical computation', 'reason': '', 'tag': '', 'text': 'I think it\'s the study of how brains compute <break time="300ms"/> but I don\'t know a lot about it, to be honest. '}}, {'backstory': {'text': "i don't think it's a competition for any leaders. i believe any research will help everyone in advance, rather than push anyone back.", 'question': 'Who is leading artificial intelligence', 'confidence': 0.7664469480514526, 'module': 'TECHSCIENCECHAT'}, 'regex': {'sys': ['req_topic_jump'], 'topic': ['topic_techscience'], 'lexical': ['ask_info']}, 'np': ['artificial intelligence'], 'DA_score': '0.8898943066596985', 'senti': 'neu', 'module': ['TECHSCIENCECHAT', 'MOVIECHAT', 'TECHSCIENCECHAT'], 'text': 'tell me about artificial intelligence', 'DA': 'commands'}], 'a_b_test': ['B', 'B'], 'suggest_keywords': None, 'last_module': 'TECHSCIENCECHAT', 'template_manager': {'prev_hash': {'PPsGsg': ['7WwlVw'], 'gN4J9Q': ['CyUXZA'], 'rJ8LnQ': [], 'is0KPQ': [], 'mPIK5Q': ['W6QUYQ']}}, 'techsciencechat': {'chitchat_tracker': {'trend': {}, 'opinion': {}, 'fact': {'artificial_intelligence': {'new_ids': [1, 2], 'unfinished_ids': {}, 'curr_turn': 0, 'curr_id': 1, 'completed_ids': [], 'next_id': -1}}}, 'current_topic': 'artificial_intelligence', 'current_transition': 't_interesting_fact', 'tech_propose_topic': [], 'not_first_time': True, 'topic_history': {}, 'prev_state': 's_interesting_fact', 'propose_continue': 'CONTINUE', 'context_topic': {}}, 'usr_name': 'samuel', 'module_selection': {'used_topic': ['LAUNCHGREETING', 'TECHSCIENCECHAT']}}
with open("../test_cases/test_chitchats.json") as r:
    _test_data = json.loads(r.read())

class TestSimpleChitchat(unittest.TestCase):
    """ Test TopicUtils will focus on testing Topic Handler
        The goal of topic handler is to take in a text and
        1. Extract the NLU features that identify the topic user wants to talk about
        2. Map the user input to the corresponding resonse templates
     """

    def get_automaton(self):
        return TechScienceAutomaton()

    def get_attribute(self, response, attribute):
        user_attributes = response.get("techsciencechat", {})
        return user_attributes.get(attribute, None)

    def _test_helper(self, actual_state, expected_states):
        self.assertIn(actual_state, set(expected_states), msg="Should be {0}, but actually {1}".format(expected_states, actual_state))  

    def get_transition_state(self, response):
        user_attributes = response.get("techsciencechat", {})
        return user_attributes.get("prev_state"), user_attributes.get("prev_transition")

    def get_random_next_input(self, index, source):
        return random.choice(source[index])


    def entry_into_chitchat(self, key, entry_id):
        data = _test_data[key]

        automaton = self.get_automaton()
        i = 1
        prev_user_attribute = None
        while i < len(data):
            input_data = self.get_random_next_input(str(i), data)

            if prev_user_attribute is not None:
                input_data["techsciencechat"] = copy.deepcopy(prev_user_attribute)
            response = automaton.transduce(input_data)
            prev_user_attribute = response.get("techsciencechat")
            if str(i) == entry_id:
                return prev_user_attribute
            i+= 1
        
        return None
    
    def test_basic_chitchat_entry(self):
        """We are testing on ai_fact_6 in this case
        """
        # logging.disable(logging.DEBUG)
        # logging.disable(logging.INFO)
        # logging.disable(logging.CRITICAL)
        # logging.disable(logging.WARNING)

        data = _test_data["test_ai_fact_6"]
        automaton = self.get_automaton()
        user_attributes = self.entry_into_chitchat("test_ai_fact_6", "1")
        
        input_data_2 = self.get_random_next_input("2", data) # Entry from s_interesting_facts
        print("User Utterance: {}\n\n".format(input_data_2.get("text")))
        input_data_2["techsciencechat"] = user_attributes
        session_id = input_data_2.get("session_id", "")
        response = automaton.transduce(input_data_2)
        print("System Response: {}\n\n\n".format(response.get("response")))
        prev_state, prev_transition = self.get_transition_state(response)
        self.assertEqual(prev_state, "s_interesting_fact", msg="Should Enter into Interesting Fact State")
        
        self.chitchatstate_test_helper(response, "facts-artificial_intelligence-6", 0)

        input_data_3 = self.get_random_next_input("3", data) # Entry from s_interesting_facts
        print("User Utterance: {}\n\n".format(input_data_3.get("text")))
        input_data_3["techsciencechat"] = response.get("techsciencechat", {})
        input_data_3["session_id"] = session_id
        self.assertNotEqual(input_data_3["techsciencechat"], {})
        response = automaton.transduce(input_data_3)
        print("System Response: {}\n\n\n".format(response.get("response")))
        prev_state, prev_transition = self.get_transition_state(response)
        self.assertEqual(prev_state, "s_interesting_fact", msg="Should Enter into Interesting Fact State")
        self.assertEqual(prev_transition, "t_interesting_fact", msg="Should Enter into Interesting Fact Transition")
        self.chitchatstate_test_helper(response, "facts-artificial_intelligence-6", 1)

        input_data_4 = self.get_random_next_input("4", data) # Entry from s_interesting_facts
        input_data_4["techsciencechat"] = response.get("techsciencechat", {})
        print("User Utterance: {}\n\n".format(input_data_4.get("text")))
        self.assertNotEqual(input_data_4["techsciencechat"], {})
        input_data_4["session_id"] = session_id
        response = automaton.transduce(input_data_4)
        print("System Response: {}\n\n\n".format(response.get("response")))
        prev_state, prev_transition = self.get_transition_state(response)
        self.assertEqual(prev_state, "s_interesting_fact", msg="Should Enter into Interesting Fact State")
        self.assertEqual(prev_transition, "t_interesting_fact", msg="Should Enter into Interesting Fact Transition")
        self.chitchatstate_test_helper(response, "facts-artificial_intelligence-6", 2)

        input_data_5 = self.get_random_next_input("5", data) # Entry from s_interesting_facts
        input_data_5["techsciencechat"] = response.get("techsciencechat", {})
        input_data_5["session_id"] = session_id
        self.assertNotEqual(input_data_5["techsciencechat"], {})
        print("User Utterance: {}\n\n".format(input_data_5.get("text")))
        response = automaton.transduce(input_data_5)
        print("System Response: {}\n\n\n".format(response.get("response")))
        prev_state, prev_transition = self.get_transition_state(response)
        self.assertEqual(prev_state, "s_interesting_fact", msg="Should Enter into Interesting Fact State")
        self.assertEqual(prev_transition, "t_interesting_fact", msg="Should Enter into Interesting Fact Transition")
        self.chitchatstate_test_helper(response, "facts-artificial_intelligence-6", 3)

    def test_chitchat_handle_complaint(self):
        """Chitchat_Compliant
        """
        for tests in ["test_ai_fact_6_annoying_jumpout", "test_ai_fact_6_creepy_jumpout", "test_ai_fact_6_stupid_jumpout"]:

            data = _test_data["test_ai_fact_6_annoying_jumpout"]
            automaton = self.get_automaton()
            user_attributes = self.entry_into_chitchat("test_ai_fact_6", "1")
            input_data_2 = self.get_random_next_input("2", data) # Entry from s_interesting_facts
            print("User Utterance: {}\n\n".format(input_data_2.get("text")))
            session_id = input_data_2.get("session_id", "")
            input_data_2["techsciencechat"] = user_attributes
            response = automaton.transduce(input_data_2)
            print("System Response: {}\n\n\n".format(response.get("response")))
            prev_state, prev_transition = self.get_transition_state(response)
            self.assertEqual(prev_state, "s_interesting_fact", msg="Should Enter into Interesting Fact State")
            
            self.chitchatstate_test_helper(response, "facts-artificial_intelligence-6", 0)

            input_data_3 = self.get_random_next_input("3", data) # Entry from s_interesting_facts
            input_data_3["techsciencechat"] = response.get("techsciencechat", {})
            print("User Utterance: {}\n\n".format(input_data_3.get("text")))
            input_data_3["session_id"] = session_id
            self.assertNotEqual(input_data_3["techsciencechat"], {})
            response = automaton.transduce(input_data_3)
            print("System Response: {}\n\n\n".format(response.get("response")))
            prev_state, prev_transition = self.get_transition_state(response)
            self.assertEqual(prev_state, "s_interesting_fact", msg="Should Enter into Interesting Fact State")
            self.assertEqual(prev_transition, "t_interesting_fact", msg="Should Enter into Interesting Fact Transition")
            self.chitchatstate_test_helper(response, "facts-artificial_intelligence-6", 1)

            # input_data_4 = self.get_random_next_input("4", data) # Entry from s_interesting_facts
            # input_data_4["techsciencechat"] = response.get("techsciencechat", {})
            # self.assertNotEqual(input_data_4["techsciencechat"], {})
            # response = automaton.transduce(input_data_4)
            # prev_state, prev_transition = self.get_transition_state(response)
            # self.assertEqual(prev_state, "s_interesting_fact", msg="Should Enter into Interesting Fact State")
            # self.assertEqual(prev_transition, "t_interesting_fact", msg="Should Enter into Interesting Fact Transition")
            # self.chitchatstate_test_helper(response, "facts-artificial_intelligence-6", 2)
    

    # def test_chitchat_transition(self):
    #     """Transition from facts-artificial_intelligence-1 to opinion_artificial_intelligence-1
    #     """
    #     data = _test_data["test_ai_fact_6"]
    #     automaton = self.get_automaton()
    #     user_attributes = self.entry_into_chitchat("test_ai_fact_6", "5")
    #     input_data_2 = self.get_random_next_input("6", data) # Entry from s_interesting_facts
    #     input_data_2["techsciencechat"] = user_attributes
    #     response = automaton.transduce(input_data_2)
    #     prev_state, prev_transition = self.get_transition_state(response)
    #     self.assertEqual(prev_state, "s_opinion", msg="Should Enter Opinion")

    def chitchatstate_test_helper(self, response, correct_state, i):
        actual_state = self.get_attribute(response, "chitchat_state")
        # Note actual state is a string
        self.assertTrue(isinstance(actual_state, str), msg="curr_state should be string")
        self.assertEqual(actual_state, correct_state, msg="Actual State {0} doesn't match correct state {1}".format(actual_state, correct_state))
        
        # Get the chitchat_metadata
        actual_chitchat_tracker = self.get_attribute(response, "chitchat_tracker")
        self.chitchat_tracker_format_test(actual_chitchat_tracker)
        self.assertEqual(actual_chitchat_tracker["curr_turn"], i, msg="Curr Turn should be {}".format(i))
        self.assertEqual(actual_chitchat_tracker["curr_state"], actual_state, msg="curr_state from chitchat tracker should be consistent")
       
       
       
        return

    def chitchat_tracker_format_test(self, chitchat_tracker):
        self.assertTrue(isinstance(chitchat_tracker, dict))
        _tracker_keys = set(chitchat_tracker.keys())

        self.assertIn("curr_state", _tracker_keys, msg="curr_state not in tracker")
        self.assertTrue(isinstance(chitchat_tracker["curr_state"], str), msg="curr_state should be string")
        self.assertIn("curr_turn", _tracker_keys, msg="curr_turn not in tracker")
        self.assertTrue(isinstance(chitchat_tracker["curr_turn"], int), msg="curr_turn should be integer")
        self.assertIn("curr_node", _tracker_keys, msg="curr_node not in tracker")
        self.assertTrue(isinstance(chitchat_tracker["curr_node"], str), msg="curr_node should be string")
        self.assertIn("completed_ids", _tracker_keys, msg="completed_ids not in tracker")
        self.assertTrue(isinstance(chitchat_tracker["completed_ids"], list), msg="completed_ids should be a list")

        self.assertIn("unfinished_ids", _tracker_keys, msg="unfinished_ids not in tracker")
        self.assertTrue(isinstance(chitchat_tracker["unfinished_ids"], dict), msg="unfinished_ids should be a dict")


    def test_chitchat_full_flow(self):
        # The test that encompasses all the flows between all chitchats
        # s_intro -> s_ask_favorite_things -> s_interesting_fact -> ... (chitchat cycle) -> s_change_of_topic
        # s_interesting_fact (another chitchat cycle)...->s_change_of_topic->s_exit_chitchat
        # logging.disable(logging.DEBUG)
        # logging.disable(logging.INFO)
        # logging.disable(logging.CRITICAL)
        # logging.disable(logging.WARNING)
        data = _test_data["test_all_chitchat"]
        response = None
        automaton = self.get_automaton()
        for k in data:
            if k == 4:
                break
            input_data_1 = self.get_random_next_input(k, data)

            if response:
                input_data_1["techsciencechat"] = copy.deepcopy(response["techsciencechat"])
                input_data_1["session_id"] = [session_id]
            print("User Utterance: {}\n\n".format(input_data_1.get("text")[0]))
            response = automaton.transduce(input_data_1)
            session_id = input_data_1["session_id"][0]
            prev_state, prev_transition = self.get_transition_state(response)
            print("Used State {}".format(prev_state))
            print("System Response {}\n\n\n".format(response.get("response")))
            print("Chitchat State - {0}, Turn Number {1}".format(response.get("techsciencechat", {}).get("chitchat_state"), \
                                                          response.get("techsciencechat", {}).get("chitchat_tracker", {}).get("curr_turn")))
            session_id = input_data_1["session_id"][0]
        

    def test_should_jumpout(self):
        return 
        
    # def test_simple_nlu(self):
    #     """
    #     """
    #     test_cases = DailyStatistics("../tests/test_cases")
    #     iterator = test_cases.generator()
    #     for ret, central_elem in iterator:
    #         # returnnlp = clean_returnnlp(returnnlp)
    #         #     # central_elem = clean_ce(central_elem)
    #         returnnlp = [ReturnNLPSegment.from_dict(seg) for seg in ret]
    #         central_elem = CentralElement.from_dict(central_elem)
    #         if "TECHSCIENCECHAT" in central_elem.topic_modules:
    #             tech_handler = TechScienceTopicHandler(returnnlp, central_elem)
    #             current_topic = tech_handler.detect_topic()
    #     #     break

    # def test_opinion_chitchat(self):
    #     data = _test_data["sample_chitchat_opinion"]

    # def test_fact_chitchat(self):
    #     data = _test_data["sample_chitchat_opinion"]

    # def test_trendy_chitchat(self):
    #     data = _test_data["sample_chitchat_opinion"]
        


if __name__ == '__main__':
    unittest.main()
