"""For each transition, test the case that will trigger the corresponding
   states.  (Both Positive and Negative States)

"""

"""
This Test focus on the basic flow of our automaton
"""




import random
import logging
import unittest
from techscience_automaton import TechScienceAutomaton
from response_templates import template_data
from tests.automaton_tests.utils_test_case_extractor import test_transition_starts, ask_finish_change_of_topic, ask_more_ask_finish

logging.disable(logging.DEBUG)
logging.disable(logging.INFO)

class TestEachTransition(unittest.TestCase):
    """ Each state should generate a respose.  Based on the response generated, the state should trigger
        the next transition.
    """

    def get_automaton(self):

        return TechScienceAutomaton()

    def get_error_responses(self, d, error_responses=[]):
        for k in d:
            sub_dict = d[k]
            if "error" in k:
                if isinstance(sub_dict, dict):
                    self.get_error_responses(sub_dict, error_responses)
                if isinstance(sub_dict, list):
                    error_responses.extend(sub_dict)
    
        
    def util_test_transitions(
            self,
            msg,
            current_transition,
            next_state,
            possible_other_states=[]):
        automaton = self.get_automaton()
        error_responses = []
        data_dict = template_data.data["techscience"]
        self.get_error_responses(data_dict, error_responses)
        errors = set([e["entry"] for e in error_responses])
        response = automaton.transduce(msg)

        target_states = possible_other_states + [next_state]
        self.assertEquals(
            automaton.current_transition,
            current_transition,
            msg="current transition should be {current_transition}, instead it is {detected_transition}. \n \
                               please check your input message".format(
                current_transition=current_transition,
                detected_transition=automaton.current_transition))
        self.assertTrue(
            automaton.next_state in target_states,
            msg="next state should be {next_state}, instead it is {detected_state}. \n \
                               please check your input message".format(
                next_state=next_state,
                detected_state=automaton.next_state))

        self.assertNotIn(response["response"],
            errors,
            msg="Error response detected. {0}".format(response["response"]))

    def test_init(self):
        """Init follows two path:
        s_askmore or s_intro
        """
        cases = test_transition_starts["t_init"]
        for msg, s in cases:
            self.util_test_transitions(msg, "t_init", s)

    def test_intro(self):
        """t_intro triggers
        - s_ask_more:  When the user mentioned a topic we cannot handle
        - s_ask_favorite_thing: When user mentioned a topic we can handle
        - s_opinion: response to a freeformed response
        """
        cases = test_transition_starts["t_intro"]
        for msg, s in cases:
            self.util_test_transitions(msg, "t_intro", s)
    
    def test_chitchat_entry(self):
        """t_chitchat_entry
        """
        automaton = self.get_automaton()
        print(automaton.chitchat_mapping)
        return

    # def test_ask_finish(self):
    #     """ t_ask finish
    #     """
    #     self.util_test_transitions(
    #         ask_finish_change_of_topic,
    #         "t_ask_finish",
    #         "s_change_of_topic",
    #         ["s_opinion"])
    #     return

    # def test_ask_more(self):
    #     """ t ask_more
    #     """
    #     self.util_test_transitions(
    #         ask_more_ask_finish,
    #         "t_ask_more",
    #         "s_ask_finish")

    #     return

    # def test_opinion(self):
    #     """Opinion is a catch all state that anticipates:
    #     1. s_change_of_topic
    #     2. s_ask_more
    #     3. s_ask_favorite thing
    #     """
    #     cases = test_transition_starts["t_opinion"]
    #     for msg, s in cases:
    #         self.util_test_transitions(msg, "t_opinion", s)

    # def test_change_of_topic(self):
    #     cases = test_transition_starts["t_change_of_topic"]
    #     for msg, s in cases:
    #         self.util_test_transitions(msg, "t_change_of_topic", s)

    # def test_ask_favorite_thing(self):
    #     """This comes after ask favorite thing
    #         - s_interesting_fact: when user answers yes
    #         - s_change_of_topic: when user answers no
    #     """
    #     cases = test_transition_starts["t_ask_favorite_thing"]
    #     for msg, s in cases:
    #         self.util_test_transitions(msg, "t_ask_favorite_thing", s)

    #     return

    # def test_interesting_fact(self):
    #     """This comes after presenting a fact
    #        1. With a random chance, jump to opinion
    #        2. s_interesting fact if yes
    #        3. s_change_of_topic if no
    #     """
    #     cases = test_transition_starts["t_ask_favorite_thing"]
    #     for msg, s in cases:
    #         self.util_test_transitions(
    #             msg, "t_ask_favorite_thing", s, ["s_opinion"])


if __name__ == '__main__':
    unittest.main()
