"""Extract specific test cases
"""

import template_manager
from template_manager import Template

t_e = Template.error


class TestCaseGenerator:
    def __init__(self):
        self._load_template()
        return

    def _load_template(self):
        # For every response, get the path to the response

        def __get_from_dict(d):
            if isinstance(d, list):
                return {u["entry"]: [] for u in d}
            ret = dict()
            for k in d:
                d_ = __get_from_dict(d[k])
                for v in d_:
                    d_[v].append(k)
                ret = {**ret, **d_}

            return ret
        self.data = __get_from_dict(t_e._data)
        return self.data

    def is_error(self, response):
        return response in self.data

    def get_error_details(self, response):
        if self.is_error(response):
            return self.data[response]
        return [response]

    def _get_from_dict(self):
        return

# if __name__ == '__main__':
#     ErrorDetectorHandler()
