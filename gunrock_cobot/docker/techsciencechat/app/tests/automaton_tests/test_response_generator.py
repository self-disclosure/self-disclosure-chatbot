"""
This Test focus on the entry point of our conversation.
"""
import random
import unittest
from response_generator import handle_message
from utils.mock_data import returnnlp, DailyStatistics, ret_
from tests.automaton_tests.util_error_detector import ErrorDetectorHandler


class TestResponseGenerator(unittest.TestCase):
    """ Extract Simple Conversation From Json file.
        Input into our _response_generator
    """

    def test_simple_response(self):
        """Test response will not generate an error message
        """
        test_cases = DailyStatistics("../test_cases")
        iterator = test_cases.generator()
        error_handler = ErrorDetectorHandler()
        for text, response, ret, central_elem in iterator:
            msg = {
                "text": text,
                "template_manager": {},
                "feature": {},
                "returnnlp": ret,
                "central_elem": central_elem,
                "techsciencechat": {
                    "PROPOSE_CONTINUE": "STOP"},
                "propose_topic": None}
            response = handle_message(msg)
            res = response["response"]
            self.assertFalse(
                error_handler.is_error(res),
                msg=res +
                "\n" +
                " ".join(
                    error_handler.get_error_details(res)))


if __name__ == '__main__':
    unittest.main()
