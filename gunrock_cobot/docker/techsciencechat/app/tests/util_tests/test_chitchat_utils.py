"""
This File Tests for Chitchat State Tracking using Chitchat Utils

"""
import random
import unittest
import logging
from techscience_utils.chitchat_utils import ChitChatHandler
from techscience_automaton import TechScienceAutomaton
from techscience_utils.data_topic import BROAD_TO_SPECIFIC, TOPIC_TO_FACT, KEYWORD_MAPPING
import template_manager
logging.disable(logging.DEBUG)
logging.disable(logging.INFO)
TEMPLATE_TECHSCI = template_manager.Templates.techscience

class TestChitChat(unittest.TestCase):
    """Test ChitChatUtils will focus on testing Chitchat Handler

       Chitchat handler is a wrapper around chitchat_tracker
       and chitchat_state.
    """

    ################## Utilities ###########################
    def get_chitchat_util(self):
        chitchat_handler = ChitChatHandler()
        return chitchat_handler

    def get_master_automaton(self):
        return TechScienceAutomaton()

    def get_chitchat_tracker(self, init, completed, in_chitchat):
        """Sample Chitchat Tracker"""
        chitchat_handler = self.get_chitchat_util()
        master_automaton = self.get_master_automaton()
        chitchat_tracker = master_automaton.add_new_chitchat_tracker()
        
        if in_chitchat:
            # State must be a valid state
            chitchat_tracker["curr_state"] = "facts-artificial_intelligence-1"
            chitchat_tracker["curr_turn"] = 1
            return chitchat_tracker

        return chitchat_tracker

    def chitchat_tracker_correct_format(self, chitchat_tracker, storage=False):

        def should_be_string(val):
            self.assertIsInstance(val, str, msg="{0} should be type string instead of {1}".format(val, type(val)))

        def should_be_tuple(val):
            self.assertIsInstance(val, tuple, msg="{0} should be type tuple instead of {1}".format(val, type(val)))

        curr_state = chitchat_tracker["curr_state"]
        completed_ids = chitchat_tracker["completed_ids"]
        unfinished_ids = chitchat_tracker["unfinished_ids"]

        if storage:
            should_be_func = should_be_string
        else:
            should_be_func = should_be_tuple
        should_be_func(curr_state)
        for c in completed_ids:
            should_be_func(c)
        for t in unfinished_ids:
            should_be_func(t)
                                                  

    ################## Chitchat Utils #########################
    def test_chitchat_valid(self):
        chitchat_handler = self.get_chitchat_util()
        master_automaton = self.get_master_automaton()

        # No Chitchat Around
        no_chitchat = self.get_chitchat_tracker(True, False, False)
        chitchat_tracker = chitchat_handler.preprocess_chitchat_tracker(no_chitchat)
        is_valid = chitchat_handler.is_valid_chitchat(chitchat_tracker)
        self.assertFalse(is_valid, msg="Error:  {} valid".format(no_chitchat["curr_state"]))
        chitchat_tracker = chitchat_handler.postprocess_chitchat_tracker(chitchat_tracker)
        # In a Chitchat
        in_chitchat = self.get_chitchat_tracker(True, False, True)
        self.chitchat_tracker_correct_format(chitchat_tracker, True)
        chitchat_tracker = chitchat_handler.preprocess_chitchat_tracker(in_chitchat)
        self.chitchat_tracker_correct_format(chitchat_tracker, False)
        chitchat_tracker = chitchat_handler.get_available_chitchats(chitchat_tracker, master_automaton.chitchat_mapping)
        is_valid = chitchat_handler.is_valid_chitchat(chitchat_tracker)
        self.chitchat_tracker_correct_format(chitchat_tracker, False)
        chitchat_tracker = chitchat_handler.postprocess_chitchat_tracker(chitchat_tracker)
        self.chitchat_tracker_correct_format(chitchat_tracker, True)
        self.assertTrue(is_valid, msg="Error:  {} not valid".format(in_chitchat["curr_state"]))
        
        return

    def test_get_available_chitchats(self):
        chitchat_handler = self.get_chitchat_util()
        master_automaton = self.get_master_automaton()
        sample_chitchat_tracker = master_automaton.add_new_chitchat_tracker()
        chitchat_mapping = master_automaton.chitchat_mapping

        chitchat_tracker = chitchat_handler.get_available_chitchats(sample_chitchat_tracker, chitchat_mapping)
        
        for state in chitchat_tracker["available_ids"]:
            activated = chitchat_handler.is_activated_chitchat(*state)
            self.assertTrue(activated, msg="Error:  {} not activated".format(state))
        # At this step, chitchat state is not valid
        chitchat_tracker = chitchat_handler.preprocess_chitchat_tracker(chitchat_tracker)
        valid = chitchat_handler.is_valid_chitchat(chitchat_tracker)
        self.assertFalse(valid, msg="Error:  {} should not be valid ".format(sample_chitchat_tracker["curr_state"]))


    ################## Chitchat Initiation ####################
    def test_new_chitchat_state(self):
        chitchat_handler = self.get_chitchat_util()
        master_automaton = self.get_master_automaton()
        all_available_chitchats = chitchat_handler.available_chitchat
        # No Chitchat Around
        no_chitchat = self.get_chitchat_tracker(True, False, False)
        chitchat_tracker = chitchat_handler.preprocess_chitchat_tracker(no_chitchat)
        chitchat_tracker = chitchat_handler.get_available_chitchats(chitchat_tracker, master_automaton.chitchat_mapping)
        for header in all_available_chitchats:
            for topic in all_available_chitchats[header]:
                proposed_state = chitchat_handler.new_chitchat_state(chitchat_tracker, header, topic)
                chitchat_tracker = chitchat_handler.initialize_chitchat(chitchat_tracker, proposed_state)
                is_valid = chitchat_handler.is_valid_chitchat(chitchat_tracker)
                self.assertTrue(is_valid, msg="Error:  {} not valid_state".format(proposed_state))
        
        return
    ################## Chitchat Continue ######################
    def test_get_next_turn(self):
        """Test get current turn and get next turn
        """
        chitchat_handler = self.get_chitchat_util()
        master_automaton = self.get_master_automaton()
        all_available_chitchats = chitchat_handler.available_chitchat
        # No Chitchat Around
        no_chitchat = self.get_chitchat_tracker(True, False, False)
        chitchat_tracker = chitchat_handler.preprocess_chitchat_tracker(no_chitchat)
        chitchat_tracker = chitchat_handler.get_available_chitchats(chitchat_tracker, master_automaton.chitchat_mapping)
        for header in all_available_chitchats:
            for topic in all_available_chitchats[header]:
                proposed_state = chitchat_handler.new_chitchat_state(chitchat_tracker, header, topic)
                chitchat_tracker = chitchat_handler.initialize_chitchat(chitchat_tracker, proposed_state)
                turn = chitchat_handler.get_current_turn(chitchat_tracker)
                next_turn = chitchat_handler.get_next_turn(chitchat_tracker)
                self.assertEqual(turn+1, next_turn, msg="Turn not incremented")

    ################## Chitchat Resume ########################



    ################## Chitchat End ###########################
    def test_store_chitchat_state(self):
        chitchat_handler = self.get_chitchat_util()
        master_automaton = self.get_master_automaton()
        all_available_chitchats = chitchat_handler.available_chitchat

        in_chitchat = self.get_chitchat_tracker(True, False, True)


        self.chitchat_tracker_correct_format(in_chitchat, True)
        chitchat_tracker = chitchat_handler.preprocess_chitchat_tracker(in_chitchat)
        self.chitchat_tracker_correct_format(chitchat_tracker, False)
        chitchat_tracker = chitchat_handler.get_available_chitchats(chitchat_tracker, master_automaton.chitchat_mapping)
        is_valid = chitchat_handler.is_valid_chitchat(chitchat_tracker)
        self.assertTrue(is_valid, msg="Error:  {} not valid".format(in_chitchat["curr_state"]))
        chitchat_handler.store_current_state_as_complete(chitchat_tracker, False)
        is_valid = chitchat_handler.is_valid_chitchat(chitchat_tracker)
        self.assertFalse(is_valid, msg="Error:  {} should not be valid".format(chitchat_tracker["curr_state"]))
        self.chitchat_tracker_correct_format(chitchat_tracker, False)
        chitchat_tracker = chitchat_handler.postprocess_chitchat_tracker(chitchat_tracker)
        self.chitchat_tracker_correct_format(chitchat_tracker, True)

        self.chitchat_tracker_correct_format(in_chitchat, True)
        chitchat_tracker = chitchat_handler.preprocess_chitchat_tracker(in_chitchat)
        self.chitchat_tracker_correct_format(chitchat_tracker, False)
        chitchat_tracker = chitchat_handler.get_available_chitchats(chitchat_tracker, master_automaton.chitchat_mapping)
        is_valid = chitchat_handler.is_valid_chitchat(chitchat_tracker)
        self.assertTrue(is_valid, msg="Error:  {} not valid".format(in_chitchat["curr_state"]))
        chitchat_handler.store_current_state_as_complete(chitchat_tracker, True)
        self.chitchat_tracker_correct_format(chitchat_tracker, True)

if __name__ == '__main__':
    unittest.main()