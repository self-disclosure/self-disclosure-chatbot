"""
This File Tests for Chitchat State Tracking using Chitchat Utils

"""
import random
import unittest

from utils.retrieval_utils import evi_bot, get_interesting_reddit_posts, \
 QA, retrieve_kg_topic, query_tag
# from techscience_automaton import TechScienceAutomaton
# from utils.data_topic import BROAD_TO_SPECIFIC, TOPIC_TO_FACT, KEYWORD_MAPPING
# import template_manager

# TEMPLATE_TECHSCI = template_manager.Templates.techscience

class TestRetrievalUtils(unittest.TestCase):
    """Test ChitChatUtils will focus on testing Chitchat Handler

       Chitchat handler is a wrapper around chitchat_tracker
       and chitchat_state.
    """

    ################## Utilities ###########################
    def test_evi_bot(self):
        text = evi_bot("what is arthritis")
        print(text)
        self.assertIsNotNone(text, "Evi BOT is not working.")

    def test_reddit_bot(self):
        text = get_interesting_reddit_posts("about aerodynamics", 2)
        print(text)

    def test_qa(self):
        nanotech = query_tag("nanotech")
        response = retrieve_kg_topic(nanotech)
        print(response)
        for tags in nanotech:
            print(tags.keys())
            print(tags["title"])
            print(tags["score"])
            print(tags["tag"])
            print("========================")

    def test_retrieve_kg_topic(self):
        return retrieve_kg_topic([{}])

if __name__ == '__main__':
    unittest.main()