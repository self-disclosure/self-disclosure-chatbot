"""
This file tests utilities for intent retrieval and intent handling
"""
import random
import unittest
from utils.intent_utils import TechScienceIntentHandler
from utils.mock_data import returnnlp
from nlu.util_nlu import ReturnNLPSegment, CentralElement

class TestIntentUtils(unittest.TestCase):
    """  """

    def test_handle_topic_specific_intent(self):
        """ Test handle topic specific intent 
            Case tested:
                - req_topic
                - req_research
                - req_recommend # Tested in lexical intent
                - req_scitech
        """
        returnnlp_data = [ReturnNLPSegment.from_dict(r) for r in returnnlp]
        intents_handler = TechScienceIntentHandler().build_intent_handler(returnnlp_data)

        req_topic_pos = ["can we chat about something cool",

        ]

        req_topic_neg = [

        ]

        key_val = {"req_topic":(req_topic_pos, req_topic_neg)}
        self._handle_intent(intents_handler.detect_topic_intent, key_val)


        req_research_pos = ["who made progress on neural networks",

        ]

        req_research_neg = []

        key_val = {"req_research":(req_research_pos, req_research_neg)}
        self._handle_intent(intents_handler.detect_topic_intent, key_val)


        req_sci_tech_pos = ["will you discuss ai"]

        req_sci_tech_neg = []
        key_val = {"req_scitech":(req_sci_tech_pos, req_sci_tech_neg)}
        self._handle_intent(intents_handler.detect_topic_intent, key_val)

        ans_yes_pos = ["yes"]
        ans_yes_neg = ["no"]
        key_val = {"ans_yes": (ans_yes_pos, ans_yes_neg)}
        self._handle_intent(intents_handler.detect_topic_intent, key_val)


        user_dn_pos = ["i don't know", "i am not sure"]
        user_dn_neg = ["i am sure", "yes"]
        key_val = {"user_dont_know": (user_dn_pos, user_dn_neg)}
        self._handle_intent(intents_handler.detect_topic_intent, key_val)

        # Positive Sentiment
        pos_sent = ["it's hilarious", "hilarious", "haha", "i like it", "i love it"]
        not_pos_sent = ["no", "sigh", "that's not funny", "i don't like it", "that's not hilarious"]

        # # Answer yes
        ans_yes = ["yes", "okay", "alrighty", "great", "yes", "sure", "sounds good to me"]
        not_ans_yes = ["no", "don't want it", "no, that's okay", "haha stop"]

        not_ans_no = ["yes", "okay", "alrighty", "great", "yes", "sure", "sounds good to me"]
        ans_no = ["no", "don't want it", "no, that's okay", "haha stop"]

        ask_more = ["no but i wanna hear more"]
        not_ask_more = ["no", "don't want it", "no, that's okay", "haha stop"]

      
        user_ans_like = ["i do like"]
        not_ans_like = ["i don't like"]
        
        user_happy = ["i am happy", "this makes me happy"]
        user_not_happy = ["i don't care"]

        key_val = {"pos_sent":(pos_sent, not_pos_sent),
                   "ans_yes":(ans_yes, not_ans_yes),
                   "ask_more": (ask_more, not_ask_more),
                   "ans_no": (ans_no, not_ans_no),
                   "user_expressed_happiness": (user_happy, user_not_happy),
                   "ans_like": (user_ans_like, not_ans_like)}
        self._handle_intent(intents_handler.detect_topic_intent, key_val)
        return

    def test_handle_sys_intent(self):
        """ test handle sys intent and handle intent 
            For our case sys intent only covers exit techscience
        """
        
        returnnlp_data = [ReturnNLPSegment.from_dict(r) for r in returnnlp]
        intents_handler = TechScienceIntentHandler().build_intent_handler(returnnlp_data)

        exit_techscience = ["i don't want to talk about techscience",\
                            
                            "no, i don't like technology",\
                            "can we talk about something else"]

        not_exit_techscience = ["I like techscience", \
                                "let's talk more", \
                                "who is the most interesting person in biology",
                                "i don't know you should talk to me about that",
                                "yes",\
                                "maybe",\
                                "sure"]
    
        key_val = {"get_off_topic":(exit_techscience, not_exit_techscience)}
        self._handle_intent(intents_handler.detect_sys_intent, key_val)


    def test_handle_lexical_intent(self):
        """ Test Handle Lexical Intent
            Cases Tested:
                - ask_recommend
                - suggest_question
        """

        returnnlp_data = [ReturnNLPSegment.from_dict(r) for r in returnnlp]
        intents_handler = TechScienceIntentHandler().build_intent_handler(returnnlp_data)


        # Test scripts for ask_recommend
        ask_recommend_text = ["what do you recommend for technology information",
                              "do you have suggestions",\
                              "what computer should i get"]

        not_ask_recommend_text = ["i don't want to talk about techscience",\
                                "no, i don't like technology",\
                                "can we talk about something else"
                                "i like techscience", \
                                "let's talk more", \
                                "who is the most interesting person in biology",
                                "yes",\
                                "maybe",\
                                "sure"]

        # Test scripts for suggest_question
        ask_question = ["what do you recommend for technology information",
                              "what can I do for this problem",\
                              "what computer should I get",
                              "how do I do this"
                              "what's your favorite food"
                        ]

        not_ask_question = ["i don't want to talk about techscience",\
                                "no, i don't like technology",\
                                "can we talk about something else"
                                "i like techscience", \
                                "let's talk more", \
                                "yes",\
                                "maybe",\
                                "sure"]
  
        
        key_val = {"ask_recommend":(ask_recommend_text, not_ask_recommend_text), \
                   "suggest_question":(ask_question, not_ask_question),
                #    "ans_like": (user_ans_like, not_ans_like)
                   }

        self._handle_intent(intents_handler.detect_lexical_intent, key_val)


    def _handle_intent(self, intent_detector, key_val):
        """ Generic Test Handle Intent

        Parameter:  
          - intent_detector:  intent detector method to be tested
          - key_val: a dictionary of {intent: tuple(list_of_positive_case, list_of_negative_case)}
        """
        


        for intent in key_val:
            pos_text, neg_text = key_val[intent]
            for txt in pos_text:
                ret_intents = intent_detector(txt)
                self.assertIn(intent, ret_intents, msg=txt)
            
            for txt in neg_text:
                ret_intents = intent_detector(txt)
                self.assertNotIn(intent, ret_intents,msg=txt)



        
        

if __name__ == '__main__':
    unittest.main()