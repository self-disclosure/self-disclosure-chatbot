import random
import unittest
from techscience_utils.nlu_process_utils import NLU_Processor
from nlu.dataclass.returnnlp import ReturnNLP
from techscience_utils.data_topic import BROAD_TO_SPECIFIC, TOPIC_TO_FACT, KEYWORD_MAPPING
import template_manager

import json
import pandas as pd
import os

TEMPLATE_TECHSCI = template_manager.Templates.techscience
PATH = "../test_cases/labeled_yesno_keyword.csv"

class TestNLUProcess(unittest.TestCase):
    """ Test TopicUtils will focus on testing Topic Handler
        The goal of topic handler is to take in a text and 
        1. Extract the NLU features that identify the topic user wants to talk about
        2. Map the user input to the corresponding resonse templates
     """

    def get_NLUProcessor(self, returnnlp, central):
        nlu_processor = NLU_Processor()
        return nlu_processor.build_nlu_handler(returnnlp, central)

    def get_data(self, key):
        df = pd.read_csv(PATH)
        for i, row in df.iterrows():
            yield row["text"], row[key], self._clean_nlp(row["returnnlp"])

    def _clean_nlp(self, returnnlp):
        raw_nlp = json.loads(returnnlp)
        ret = [clean_raw_dict(r) for r in raw_nlp]
        result = ReturnNLP(ret)
        return result

    def test_keyword_exists(self):
        data = self.get_data("keyword")
        num_tests = 0
        num_incorrect = 0
        for text, result, returnnlp in data:
            processor = self.get_NLUProcessor(returnnlp, None)
            prediction = processor.keyword_detection()
            have_keyword = len(prediction) > 0
            actual = (have_keyword and result==1) or (not have_keyword and result==0)
            num_tests += 1
            try:
                self.assertTrue(actual, msg="{0} predicted {1}, actual {2}".format(text, prediction, result))
            except AssertionError:
                num_incorrect += 1
                


        print("Keyword Detection Accuracy:  {0}".format(float(num_tests - num_incorrect)/num_tests))

    def test_yes(self):
        data = self.get_data("yes")
        num_tests = 0
        num_incorrect = 0
        for text, result, returnnlp in data:
            processor = self.get_NLUProcessor(returnnlp, None)
            prediction = processor.yes
            actual = result == 1
            num_tests += 1
            try:
                self.assertEqual(actual, prediction, msg="{0} predicted {1}, actual {2}".format(text, prediction, result))
            except AssertionError:
                num_incorrect += 1
        print("AnsYes Accuracy:  {0}".format(float(num_tests - num_incorrect)/num_tests))

    def test_no(self):
        data = self.get_data("No")
        num_tests = 0
        num_incorrect = 0
        for text, result, returnnlp in data:
            processor = self.get_NLUProcessor(returnnlp, None)
            prediction = processor.no
            actual = result == 1
            num_tests += 1
            try:
                self.assertEqual(actual, prediction, msg="{0} predicted {1}, actual {2}".format(text, prediction, result))
            except AssertionError:
                num_incorrect += 1
        print("AnsNo Accuracy:  {0}".format(float(num_tests - num_incorrect)/num_tests))

    def test_is_long_sentence(self):
        """TODO"""
        # LENGTH
        return

    def test_user_dont_know(self):
        return

    def test_user_asked_yes_no(self):
        return

    # def test_dialog_act(self):
    #     """System should be able to handle all topics in KEYWORD_MAPPING
    #     """
    #     topic_handler = TechScienceTopicHandler()
    #     data = {'text': ["what's new in blockchain", ""], 'features': [{'segmentation': {'segmented_text': ["what's new in blockchain"], 'segmented_text_raw': ["what's new in blockchain"]}, 'dependency_parsing': {'dependency_parent': [[0, 1, 4, 1]], 'dependency_label': [['obj', 'amod', 'case', 'obl']], 'pos_tagging': [['ADV', 'ADJ', 'ADP', 'PROPN']]}, 'intent': 'general', 'coreference': None, 'ner': None, 'intent_classify2': [{'sys': [], 'topic': ['topic_qa'], 'lexical': []}], 'npknowledge': {'knowledge': [['Blockchain', 'Event', '12.17116', '__EMPTY__']], 'noun_phrase': ['blockchain'], 'local_noun_phrase': ['blockchain']}, 'sentiment': 'neu', 'sentence_completion_text': "what 's new in blockchain", 'spacynp': ['blockchain'], 'asrcorrection': None, 'sentiment2': [{'neg': '0.0', 'neu': '1.0', 'pos': '0.0', 'compound': '0.0'}], 'returnnlp': [{'text': "what's new in blockchain", 'dialog_act': ['open_question_factual', '0.715646505355835'], 'intent': {'sys': [], 'topic': ['topic_qa'], 'lexical': []}, 'sentiment': {'neg': '0.0', 'neu': '1.0', 'pos': '0.0', 'compound': '0.0'}, 'googlekg': [['Blockchain', 'Event', '12.17116', '__EMPTY__']], 'noun_phrase': ['blockchain'], 'topic': {'topicClass': 'Phatic', 'confidence': 0.316, 'text': "what's new in blockchain", 'topicKeywords': []}, 'concept': [{'noun': 'blockchain', 'data': {'technology': '0.6', 'big bitcoin organization': '0.2', 'website': '0.2'}}], 'dependency_parsing': {'head': [0, 1, 4, 1], 'label': ['obj', 'amod', 'case', 'obl']}, 'pos_tagging': ['ADV', 'ADJ', 'ADP', 'PROPN']}], 'dialog_act': [{'text': "what's new in blockchain", 'DA': 'open_question_factual', 'confidence': 0.715646505355835}], 'googlekg': [[['Blockchain', 'Event', '12.17116', '__EMPTY__']]], 'intent_classify': {'sys': [], 'topic': ['topic_qa'], 'lexical': []}, 'text': "what's new in blockchain", 'nounphrase2': {'noun_phrase': [['blockchain']], 'local_noun_phrase': [['blockchain']]}, 'topic2': [{'topicClass': 'Phatic', 'confidence': 0.316, 'text': "what's new in blockchain", 'topicKeywords': []}], 'converttext': "what's new in blockchain", 'central_elem': {'senti': 'neu', 'regex': {'sys': [], 'topic': ['topic_qa'], 'lexical': []}, 'DA': 'open_question_factual', 'DA_score': '0.715646505355835', 'module': ['TECHSCIENCECHAT'], 'np': ['blockchain'], 'text': "what's new in blockchain", 'backstory': {'confidence': 0.5696151852607727, 'followup': '', 'module': '', 'neg': '', 'pos': '', 'postfix': '', 'question': 'what is cortical computation', 'reason': '', 'tag': '', 'text': 'I think it\'s the study of how brains compute <break time="300ms"/> but I don\'t know a lot about it, to be honest. '}}, 'topic': [{'topicClass': 'Phatic', 'confidence': 0.316, 'text': "what's new in blockchain", 'topicKeywords': []}], 'concept': [{'noun': 'blockchain', 'data': {'technology': '0.6', 'big bitcoin organization': '0.2', 'website': '0.2'}}], 'profanity_check': [0], 'topic_module': '', 'topic_keywords': [], 'knowledge': [['Blockchain', 'Event', '12.17116', '__EMPTY__']], 'noun_phrase': ['blockchain']}, None], 'returnnlp': [[{'text': "what's new in blockchain", 'dialog_act': ['open_question_factual', '0.715646505355835'], 'intent': {'sys': [], 'topic': ['topic_qa'], 'lexical': []}, 'sentiment': {'neg': '0.0', 'neu': '1.0', 'pos': '0.0', 'compound': '0.0'}, 'googlekg': [['Blockchain', 'Event', '12.17116', '__EMPTY__']], 'noun_phrase': ['blockchain'], 'topic': {'topicClass': 'Phatic', 'confidence': 0.316, 'text': "what's new in blockchain", 'topicKeywords': []}, 'concept': [{'noun': 'blockchain', 'data': {'technology': '0.6', 'big bitcoin organization': '0.2', 'website': '0.2'}}], 'dependency_parsing': {'head': [0, 1, 4, 1], 'label': ['obj', 'amod', 'case', 'obl']}, 'pos_tagging': ['ADV', 'ADJ', 'ADP', 'PROPN']}], [{'sentiment': {'neg': '0.0', 'pos': '0.0', 'compound': '0.0', 'neu': '1.0'}, 'googlekg': [['Blockchain', 'Event', '12.17116', '__EMPTY__']], 'pos_tagging': ['ADV', 'ADJ', 'ADP', 'PROPN'], 'dependency_parsing': {'head': [0, 1, 4, 1], 'label': ['obj', 'amod', 'case', 'obl']}, 'topic': {'topicKeywords': [], 'text': "what's new in blockchain", 'topicClass': 'Phatic', 'confidence': 0.316}, 'text': "what's new in blockchain", 'noun_phrase': ['blockchain'], 'dialog_act': ['open_question_factual', '0.4414178133010864'], 'intent': {'sys': [], 'topic': ['topic_qa'], 'lexical': []}}]], 'central_elem': [{'senti': 'neu', 'regex': {'sys': [], 'topic': ['topic_qa'], 'lexical': []}, 'DA': 'open_question_factual', 'DA_score': '0.715646505355835', 'module': ['TECHSCIENCECHAT'], 'np': ['blockchain'], 'text': "what's new in blockchain", 'backstory': {'confidence': 0.5696151852607727, 'followup': '', 'module': '', 'neg': '', 'pos': '', 'postfix': '', 'question': 'what is cortical computation', 'reason': '', 'tag': '', 'text': 'I think it\'s the study of how brains compute <break time="300ms"/> but I don\'t know a lot about it, to be honest. '}}, {'backstory': {'question': 'what is cortical computation', 'confidence': 0.5696151852607727, 'text': 'I think it\'s the study of how brains compute <break time="300ms"/> but I don\'t know a lot about it, to be honest. '}, 'regex': {'sys': [], 'topic': ['topic_qa'], 'lexical': []}, 'np': ['blockchain'], 'DA_score': '0.4414178133010864', 'senti': 'neu', 'module': [], 'text': "what's new in blockchain", 'DA': 'open_question_factual'}], 'a_b_test': ['B', 'B'], 'suggest_keywords': None, 'last_module': 'LAUNCHGREETING', 'template_manager': {'prev_hash': {'PPsGsg': ['0QIdFQ'], 'COcM3A': ['BDtHmA'], 'gN4J9Q': ['CyUXZA'], 'rJ8LnQ': [], 'is0KPQ': []}}, 'techsciencechat': {'propose_continue': 'STOP'}, 'usr_name': 'johnny', 'module_selection': {'module_rank': {'MUSICCHAT': 0, 'GAMECHAT': 0, 'SPORT': 0, 'TECHSCIENCECHAT': 0, 'TRAVELCHAT': 0, 'NEWS': 0, 'BOOKCHAT': 0, 'ANIMALCHAT': 0, 'FOODCHAT': 0, 'MOVIECHAT': 0}, 'used_topic': ['LAUNCHGREETING', 'TECHSCIENCECHAT']}}
    #     returnnlp = data["returnnlp"][0]
    #     ret = [ReturnNLPSegment.from_dict(seg) for seg in returnnlp]
    #     central_e = CentralElement.from_dict(data["central_elem"][0])
    #     tech_handler = TechScienceTopicHandler().build_topic_handler(ret, central_e)
        
    #     self.assertTrue(tech_handler.dialog_act_contains(["open_question_factual"]))

    # def test_user_not_want(self):
    #     """Exploratory Test on user doesn't want to talk about topic
    #     """
    #     topic_handler = TechScienceTopicHandler()
        data = {'text': ["i don't want to talk about artificial intelligence", 'yes'], 'features': [{'concept': [{'noun': 'artificial intelligence', 'data': {'technology': '0.376', 'field': '0.3136', 'topic': '0.3105'}}], 'central_elem': {'senti': 'neg', 'regex': {'sys': [], 'topic': ['topic_techscience'], 'lexical': ['ans_neg', 'ask_leave']}, 'DA': 'neg_answer', 'DA_score': '0.31597116589546204', 'module': ['TECHSCIENCECHAT', 'MOVIECHAT', 'TECHSCIENCECHAT'], 'np': ['artificial intelligence'], 'text': "i don't want to talk about artificial intelligence", 'backstory': {'confidence': 0.7149051427841187, 'followup': '', 'module': 'TECHSCIENCECHAT', 'neg': '', 'pos': '', 'postfix': '', 'question': 'Who is leading artificial intelligence', 'reason': '', 'tag': '', 'text': 'I don’t see it as a competition for there to be any leaders. I believe that research of any kind gets everyone ahead, rather than push anyone back. '}}, 'converttext': "i don't want to talk about artificial intelligence", 'text': "i don't want to talk about artificial intelligence", 'sentence_completion_text': "i do n't want to talk about artificial intelligence", 'dialog_act': [{'text': "i don't want to talk about artificial intelligence", 'DA': 'neg_answer', 'confidence': 0.31597116589546204}], 'topic2': [{'topicClass': 'SciTech', 'confidence': 999.0, 'text': "i don't want to talk about artificial intelligence", 'topicKeywords': [{'confidence': 999.0, 'keyword': 'intelligence'}]}], 'coreference': None, 'intent': 'general', 'dependency_parsing': {'dependency_parent': [[3, 3, 0, 5, 3, 8, 8, 5]], 'dependency_label': [['nsubj', 'advmod', 'parataxis', 'mark', 'xcomp', 'case', 'nmod', 'obl']], 'pos_tagging': [['PRON', 'ADV', 'VERB', 'PART', 'VERB', 'ADP', 'ADJ', 'NOUN']]}, 'npknowledge': {'knowledge': [['artificial intelligence', '2001 film', '678.277649', 'movie']], 'noun_phrase': ['artificial intelligence'], 'local_noun_phrase': ['artificial intelligence']}, 'returnnlp': [{'text': "i don't want to talk about artificial intelligence", 'dialog_act': ['neg_answer', '0.31597116589546204'], 'intent': {'sys': [], 'topic': ['topic_techscience'], 'lexical': ['ans_neg', 'ask_leave']}, 'sentiment': {'neg': '0.118', 'neu': '0.581', 'pos': '0.3', 'compound': '0.4363'}, 'googlekg': [['artificial intelligence', '2001 film', '678.277649', 'movie']], 'noun_phrase': ['artificial intelligence'], 'topic': {'topicClass': 'SciTech', 'confidence': 999.0, 'text': "i don't want to talk about artificial intelligence", 'topicKeywords': [{'confidence': 999.0, 'keyword': 'intelligence'}]}, 'concept': [{'noun': 'artificial intelligence', 'data': {'technology': '0.376', 'field': '0.3136', 'topic': '0.3105'}}], 'dependency_parsing': {'head': [3, 3, 0, 5, 3, 8, 8, 5], 'label': ['nsubj', 'advmod', 'parataxis', 'mark', 'xcomp', 'case', 'nmod', 'obl']}, 'pos_tagging': ['PRON', 'ADV', 'VERB', 'PART', 'VERB', 'ADP', 'ADJ', 'NOUN']}], 'intent_classify': {'sys': [], 'topic': ['topic_techscience'], 'lexical': ['ans_neg', 'ask_leave']}, 'segmentation': {'segmented_text': ["i don't want to talk about ner"], 'segmented_text_raw': ["i don't want to talk about artificial intelligence"]}, 'nounphrase2': {'noun_phrase': [['artificial intelligence']], 'local_noun_phrase': [['artificial intelligence']]}, 'asrcorrection': None, 'ner': None, 'googlekg': [[['artificial intelligence', '2001 film', '678.277649', 'movie']]], 'sentiment': 'neu', 'spacynp': ['artificial intelligence'], 'intent_classify2': [{'sys': [], 'topic': ['topic_techscience'], 'lexical': ['ans_neg', 'ask_leave']}], 'sentiment2': [{'neg': '0.118', 'neu': '0.581', 'pos': '0.3', 'compound': '0.4363'}], 'topic': [{'topicClass': 'SciTech', 'confidence': 999.0, 'text': "i don't want to talk about artificial intelligence", 'topicKeywords': [{'confidence': 999.0, 'keyword': 'intelligence'}]}], 'profanity_check': [0], 'topic_module': '', 'topic_keywords': [{'confidence': 999.0, 'keyword': 'intelligence'}], 'knowledge': [['artificial intelligence', '2001 film', '678.277649', 'movie']], 'noun_phrase': ['artificial intelligence']}, None], 'returnnlp': [[{'text': "i don't want to talk about artificial intelligence", 'dialog_act': ['neg_answer', '0.31597116589546204'], 'intent': {'sys': [], 'topic': ['topic_techscience'], 'lexical': ['ans_neg', 'ask_leave']}, 'sentiment': {'neg': '0.118', 'neu': '0.581', 'pos': '0.3', 'compound': '0.4363'}, 'googlekg': [['artificial intelligence', '2001 film', '678.277649', 'movie']], 'noun_phrase': ['artificial intelligence'], 'topic': {'topicClass': 'SciTech', 'confidence': 999.0, 'text': "i don't want to talk about artificial intelligence", 'topicKeywords': [{'confidence': 999.0, 'keyword': 'intelligence'}]}, 'concept': [{'noun': 'artificial intelligence', 'data': {'technology': '0.376', 'field': '0.3136', 'topic': '0.3105'}}], 'dependency_parsing': {'head': [3, 3, 0, 5, 3, 8, 8, 5], 'label': ['nsubj', 'advmod', 'parataxis', 'mark', 'xcomp', 'case', 'nmod', 'obl']}, 'pos_tagging': ['PRON', 'ADV', 'VERB', 'PART', 'VERB', 'ADP', 'ADJ', 'NOUN']}], [{'sentiment': {'neg': '0.0', 'pos': '1.0', 'compound': '0.4019', 'neu': '0.0'}, 'googlekg': [], 'pos_tagging': ['INTJ'], 'concept': [], 'dependency_parsing': {'head': [0], 'label': ['discourse']}, 'topic': {'topicKeywords': [], 'text': 'yes', 'topicClass': 'Phatic', 'confidence': 999}, 'text': 'yes', 'noun_phrase': [], 'dialog_act': ['pos_answer', '0.9454925656318665'], 'intent': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}}]], 'central_elem': [{'senti': 'neg', 'regex': {'sys': [], 'topic': ['topic_techscience'], 'lexical': ['ans_neg', 'ask_leave']}, 'DA': 'neg_answer', 'DA_score': '0.31597116589546204', 'module': ['TECHSCIENCECHAT', 'MOVIECHAT', 'TECHSCIENCECHAT'], 'np': ['artificial intelligence'], 'text': "i don't want to talk about artificial intelligence", 'backstory': {'confidence': 0.7149051427841187, 'followup': '', 'module': 'TECHSCIENCECHAT', 'neg': '', 'pos': '', 'postfix': '', 'question': 'Who is leading artificial intelligence', 'reason': '', 'tag': '', 'text': 'I don’t see it as a competition for there to be any leaders. I believe that research of any kind gets everyone ahead, rather than push anyone back. '}}, {'backstory': {'question': 'why do you ask me', 'confidence': 0.4728533625602722, 'text': 'Oh, I was just trying to get to know more about you. '}, 'regex': {'sys': [], 'topic': [], 'lexical': ['ans_pos']}, 'np': [], 'DA_score': '0.9454925656318665', 'senti': 'pos', 'module': [], 'text': 'yes', 'DA': 'pos_answer'}], 'a_b_test': ['B', 'B'], 'suggest_keywords': None, 'last_module': 'TECHSCIENCECHAT', 'template_manager': {'prev_hash': {'PPsGsg': ['0QIdFQ'], 'JrYPRA': [], 'gN4J9Q': ['1rwq/w'], 'rJ8LnQ': [], 'OecOKA': ['pBUfog'], 'is0KPQ': [], 'mPIK5Q': ['90043g']}}, 'techsciencechat': {'research_counter': 0, 'fact_counter': 0, 'current_transition': 't_interesting_fact', 'tech_propose_topic': ['alexa', 'artificial_intelligence'], 'not_first_time': True, 'topic_history': ['artificial_intelligence', 'alexa'], 'prev_state': 's_interesting_fact', 'propose_continue': 'CONTINUE', 'context_topic': []}, 'usr_name': 'allison', 'module_selection': {'module_rank': {'MUSICCHAT': 0, 'GAMECHAT': 0, 'SPORT': 0, 'TECHSCIENCECHAT': 0, 'TRAVELCHAT': 0, 'NEWS': 0, 'BOOKCHAT': 0, 'ANIMALCHAT': 0, 'FOODCHAT': 0, 'MOVIECHAT': 0}, 'used_topic': ['LAUNCHGREETING', 'TECHSCIENCECHAT']}}
    #     returnnlp = data["returnnlp"][0]
    #     ret = [ReturnNLPSegment.from_dict(seg) for seg in returnnlp]
    #     central_e = CentralElement.from_dict(data["central_elem"][0])
    #     tech_handler = TechScienceTopicHandler().build_topic_handler(ret, central_e)
    #     for c in ret:
    #         print(c)
    #         print("\n\n")

    def test_get_topics_from_raw_text(self):
        sample_keywords = {"artificial_intelligence": set(["a.i", "a.i.", "a. i. ", "a . i."])}
        raw_nlu_processor = self.get_NLUProcessor(None, None)
        extracted = raw_nlu_processor.get_topics_from_raw_text(sample_keywords, "a.i")
        self.assertEqual(extracted[0], "artificial_intelligence")

        extracted = raw_nlu_processor.get_topics_from_raw_text(sample_keywords, "a.i.")
        self.assertEqual(extracted[0], "artificial_intelligence")

        extracted = raw_nlu_processor.get_topics_from_raw_text(sample_keywords, "artificial_intelligence")
        self.assertEqual(extracted[0], "artificial_intelligence")

        extracted = raw_nlu_processor.get_topics_from_raw_text(sample_keywords, "a. i. ")
        self.assertEqual(extracted[0], "artificial_intelligence")

        extracted = raw_nlu_processor.get_topics_from_raw_text(sample_keywords, "butternut squash")
        self.assertIsNone(extracted[0])

        # Test Predefined Keywords with Valid NLU, note this is only testing the regex part, so only text is relevant
        data = [{'text': 'are you an a.i.', \
                 'dialog_act': ['yes_no_question', 0.9957030415534973], \
                 'googlekg': [], 'concept': [{'data': {}, 'noun': 'an a.i.'}], \
                 'amazon_topic': 'Other', \
                 'dependency_parsing': {'pos_tagging': [['AUX', 'PRON', 'DET', 'NOUN']], 'dependency_label': [['cop', 'nsubj', 'det', 'obj']], 'dependency_parent': [[4.0, 4.0, 4.0, 0.0]]}, \
                 'pos_tagging': [['AUX', 'PRON', 'DET', 'NOUN']], \
                 'intent': {'lexical': ['ask_yesno'], 'topic': ['topic_backstory'], 'sys': []}, \
                 'sentiment': {'neg': '0.0', 'pos': '0.0', 'compound': '0.0', 'neu': '1.0'}, \
                 'topic': {'confidence': 999.0, 'topicKeywords': [{'confidence': 999.0, 'keyword': 'a.i.'}], 'text': 'are you an a.i.', 'topicClass': 'SciTech'}, \
                 'dialog_act2': ['dev_command', 0.0005971826612949371]}]        
        nlu_processor = self.get_NLUProcessor(ReturnNLP.from_list(data), None)
        output = nlu_processor.get_topics_from_raw_text(sample_keywords)
        print(output)
        return

def clean_raw_dict(seg):
    if isinstance(seg, dict):
        if "M" in seg:
            return clean_raw_dict(seg["M"])
        elif "L" in seg:
            return [clean_raw_dict(l) for l in seg["L"]]
        elif "S" in seg:
            return seg["S"]
        elif "N" in seg:
            return float(seg["N"])
        else:
            for s in seg:
                seg[s] = clean_raw_dict(seg[s])
    return seg

if __name__ == '__main__':
    unittest.main()
