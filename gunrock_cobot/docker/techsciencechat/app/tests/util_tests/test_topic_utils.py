"""
This file tests utilities for topic retrieval
https://docs.python.org/3/library/unittest.html
"""
import random
import unittest
from techscience_utils.topic_utils import TechScienceTopicHandler
from techscience_utils.mock_data import returnnlp, DailyStatistics, ret_, topic_detection_test_cases
from nlu.util_nlu import ReturnNLPSegment, CentralElement
from techscience_utils.data_topic import BROAD_TO_SPECIFIC, TOPIC_TO_FACT, KEYWORD_MAPPING, RESPONSE_VARIATION
import template_manager

TEMPLATE_TECHSCI = template_manager.Templates.techscience

class TestTopicUtils(unittest.TestCase):
    """ Test TopicUtils will focus on testing Topic Handler
        The goal of topic handler is to take in a text and 
        1. Extract the NLU features that identify the topic user wants to talk about
        2. Map the user input to the corresponding resonse templates
     """

    def test_map_topic_to_selector(self):
        """Test techscience_handler.map_keyword_to_selector

        Test all the facts in TOPIC_TO_FACT maps to correct fact
        Test all the opinions and favorite of each topic can be mapped
        """
        tech_handler = TechScienceTopicHandler()
        for topic in TOPIC_TO_FACT:
            opinion_selector = tech_handler.map_keyword_to_selector(topic, "opinion", True, 1)
            favorite_selector = tech_handler.map_keyword_to_selector(topic, "favorite")
            msg_o="{}_opinion".format(topic)
            msg_f="{}_favorite".format(topic)
            self.assertIsNotNone(opinion_selector, msg_o)
            self.assertIsNotNone(favorite_selector, msg_f)
            TEMPLATE_TECHSCI.utterance(
            selector=opinion_selector[0],
            slots={"topic": topic}, user_attributes_ref={})
            TEMPLATE_TECHSCI.utterance(
            selector=opinion_selector[1],
            slots={"topic": topic}, user_attributes_ref={})
            TEMPLATE_TECHSCI.utterance(
            selector=favorite_selector,
            slots={"topic": topic}, user_attributes_ref={})
            for fact in TOPIC_TO_FACT[topic]:
                fact_selector = tech_handler.map_keyword_to_selector(fact, "facts")
                msg_c="{}_fact".format(topic)
                self.assertIsNotNone(fact_selector, msg=msg_c)
                TEMPLATE_TECHSCI.utterance(selector=fact_selector, slots={"topic": fact}, user_attributes_ref={})

    def test_map_fact_to_topic(self):
        """Test map_fact_to_topic -> Optional[]
        """
        return

    def test_map_to_broad_topic(self, topic):
        return

    def test_map_fact_from_topic(self):
        return

    def test_normalize_topic(self):
        """Test tech_handler.normalize_topic

        - normalize topic takes in one raw nounphrase and map it to topic we can handle
        - normalized topic if can be normalized, none otherwise

        @tech_handler
          - normalized_map
          - normalize_topic

        
        """
        tech_handler = TechScienceTopicHandler()
        return

    def test_post_process_templates(self):
        """Test tech_handler.post_process_reponses

        - input must be either topic or a variation of topic
        - return must be a string variation of topic

        @tech_handler
          - normalized_map
          - normalize_topic

        TODO: CALL POST_PROCESS_TEMPLATE BEFORE EVERY ADD_RESPONSE
        """
        tech_handler = TechScienceTopicHandler()
        result = {}
        for w in TOPIC_TO_FACT:
            result[w] = tech_handler.post_process_responses(w)
            for f in TOPIC_TO_FACT[w]:
                result[f] = tech_handler.post_process_responses(f)

        for r in result:
            self.assertIsNotNone(result[r], msg="variation not handled: {0}".format(r))
            self.assertIn(result[r], RESPONSE_VARIATION[r], msg="variation not handled correctly: {0}".format(r))
        
    def test_suggest_topic(self):
        """Test tech_handler.post_process_reponses
          - suggest_from_broad
          - suggest_topic
        """
        tech_handler = TechScienceTopicHandler()
        
        for _ in range(100):
            topic = tech_handler.suggest_topic([], False)
            suggested_topic = tech_handler.suggest_from_broad("science", [topic])
            self.assertNotIn(topic, suggested_topic)
    
    def test_topic_exists(self):
        """Test tech_handler.is_topic,  tech_handler.is_broad_topic,  tech_handler.is_fact
           tech_hander.is_chitchat_topic 
        """
        for topic in topic_detection_test_cases:
            data = topic_detection_test_cases[topic]
            returnnlp = data["returnnlp"][0]
            ret = [ReturnNLPSegment.from_dict(seg) for seg in returnnlp]
            central_e = CentralElement.from_dict(data["central_elem"][0])
            tech_handler = TechScienceTopicHandler().build_topic_handler(ret, central_e)
            
            detected_topics = tech_handler.detect_topic()
            # topic = tech_handler.get_highest_topic(detected_topics)
            print("raw text:  %s", tech_handler.text)
            print("detected:  %s", detected_topics)
            # print("highest topic:  %s", topic)
        
        

    def test_topic_can_handle(self):
        """System should be able to handle all topics in KEYWORD_MAPPING
        """
        empty_topic_handler = TechScienceTopicHandler()
        for keyword in BROAD_TO_SPECIFIC:
            self.assertTrue(empty_topic_handler.is_broad_topic(keyword))
        for keyword in TOPIC_TO_FACT:
            self.assertTrue(empty_topic_handler.is_topic(keyword))
        for keyword in TOPIC_TO_FACT:
            for fact in TOPIC_TO_FACT[keyword]:
                self.assertTrue(empty_topic_handler.is_fact(fact))

    def test_map_to_broad_topic(self):
        """System should be able to handle all topics in KEYWORD_MAPPING
        """
        empty_topic_handler = TechScienceTopicHandler()
        for broad in BROAD_TO_SPECIFIC:
            topics = BROAD_TO_SPECIFIC[broad]
            proposed_topics = set()
            for t in topics:
                a = empty_topic_handler.map_to_broad_topic(t)
                proposed_topics.add(a)
                for fact in TOPIC_TO_FACT[t]:
                    a = empty_topic_handler.map_to_broad_topic(fact)
                    proposed_topics.add(a)
            
            self.assertIn(broad, proposed_topics)

    def test_construct_context_topics(self):
        """Test topic_handler.construct_context_topics, 
        """

        
        # 
        return

    def test_construct_topic_history(self):
        return

    def test_get_common_topics(self):
        """
        """
        return

    def test_propose_new_topics(self):
        """test topic_handler.propose_topics_to_list and topic_hander.propose_new_topics
        """
        return

    # def test_no_repeated_topics(self):
    #     """Propose New topics have no repeats
    #     """
    #     topic_handler = TechScienceTopicHandler()
    #     # None Broad topic
    #     not_topics = []
    #     context_topic = {}
    #     topic_history = {}
    #     proposed_topics = set()
    #     while len(proposed_topics) < len(TOPIC_TO_FACT):
    #         new_topics = topic_handler.propose_new_topics(None, not_topics, context_topic, topic_history)
    #         topics = list(new_topics.keys())
    #         not_topics.extend(topics[:2])
    #         for t in topics:
    #             self.assertNotIn(t, proposed_topics, "Error: repeated_topics %s".format(t))
    #             proposed_topics.add(t)

    def test_no_repeated_fact_topics(self):
        """Propose New topics have no repeats
        """
        topic_handler = TechScienceTopicHandler()
        # None Broad topic
        not_topics = []
        context_topic = {}
        topic_history = {}
        proposed_topics = set()
        facts = set([k for t in TOPIC_TO_FACT for k in TOPIC_TO_FACT[t]])
        while len(proposed_topics) < len(facts):
            new_topics = topic_handler.propose_new_topics(None, not_topics, context_topic, topic_history, False)
            topics = list(new_topics.keys())
            not_topics.extend(topics[:2])
            for t in topics:
                self.assertNotIn(t, proposed_topics, "Error: repeated_topics %s".format(t))
                proposed_topics.add(t)


if __name__ == '__main__':
    unittest.main()