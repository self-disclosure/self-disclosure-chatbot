#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""this client use online protocall for news querying"""

import requests
import redis
import datetime
from requests.auth import AuthBase
import random
import logging
logger = logging.getLogger(__name__)


from_date = (datetime.datetime.now() - datetime.timedelta(days=7)).date().isoformat()

# class NewsAuth(AuthBase):
#     # Provided by newsapi: https://newsapi.org/docs/authentication
#     def __init__(self, api_key):
#         self.api_key = api_key
#
#     def __call__(self, request):
#         request.headers.update(self.get_auth_headers(self.api_key))
#         return request
#
#     def get_auth_headers(self, api_key):
#         return {
#             'Content-Type': 'Application/JSON',
#             'Authorization': api_key
#         }

class NewsAuth(AuthBase):
    # Provided by newsapi: https://newsapi.org/docs/authentication
    THRESHOLD = 950
    PREFIX = "gunrock:ic:news:api:"
    NEWS_SET_KEY = "gunrock:ic:news:setapi"
    def __init__(self):
        r = redis.StrictRedis(host='52.87.136.90', port=16517,
                              socket_timeout=3,
                              socket_connect_timeout=1,
                              retry_on_timeout=True,
                              db=0, password="alexaprize", decode_responses=True)

        self.api_key = self._get_api_key(r)

    def __call__(self, request):
        request.headers.update(self.get_auth_headers(self.api_key))
        return request

    def get_auth_headers(self, api_key):
        return {
            'Content-Type': 'Application/JSON',
            'Authorization': api_key
        }

    def _get_api_key(self, rds):
        keys = rds.smembers(self.NEWS_SET_KEY)
        for key in keys:
            api_count = rds.get(self.PREFIX + key)
            if int(api_count) < self.THRESHOLD:
                rds.incr(self.PREFIX + key)
                logging.info("[NEWS] use api key {} with number of call: {}".format(key, api_count))
                return key
            logging.warning("[NEWS] api key {} removed with number of call: {} hit limit".format(key, api_count))
            r.srem(self.NEWS_SET_KEY, key)
        logging.warning("[NEWS] no api key can use now")
        return None


class OnlineNewsClient(object):
    def __init__(self, topic=None, sources=None, domains=None, from_parameter=from_date, to=None, language='en',
                       sort_by=None, page=None, page_size=60, country='us', category=None):
        # sortBy = relevancy, popularity, publishedAt
        # weblink and api

        # self.all_api_keys = [
        #     'fd1aa21ae5a1467cbbc66926f57805c2',
        #     '501b4053c2e040ad8fff9700acbf77ba',
        #     '82f43011d60b4db799ebd9b614f15c51',
        #     'bb63f65cfb114a63a60ed5f615656e25',
        #     '8b1f7f2ca1a146c4a835785da6dfc805',
        #     'bb5845281e5641babfa1b6d874ed4c8c',
        #     '7f5dfd19a3cf4e819b1a60f5ed29994e',
        #     '0f82b251f1324e38a1f7c7ccd8fc5e00',
        # ]

        self.url = 'https://newsapi.org/v2/'.rstrip('/')
        self.auth = NewsAuth()

        #api_key=random.choice(self.all_api_keys)

        # query parameter
        self.topic = topic
        self.sources = sources
        self.language = language
        self.country = country
        self.category = category
        self.page_size = page_size
        self.page = page
        self.domains = domains
        self.from_parameter = from_parameter
        self.to = to
        self.sort_by = sort_by

        self.categories = ['business', 'entertainment', 'general', 'health', 'science', 'sports', 'technology']
        self.all_source = "cnn,time,the-wall-street-journal,the-washington-post," \
                          "the-new-york-times,the-huffington-post,bbc-news,nbc-news," \
                          "new-scientist,the-economist,wired,bloomberg," \
                          "espn,fox-sports,bbc-sport,ign,abc-news,bbc-sport,fox-news,nfl-news," \
                          "the-washington-post,cbs-news,nhl-news,msnbc,cnbc"

    def get_headlines(self, topic, category, source=None):
        """get_headlines can query without a topic
            source cannot mix with country or category"""

        if topic:
            topic = "+".join(topic.split(" "))

        # Define Payload
        payload = {}
        payload['q'] = self.topic
        payload['category'] = self.category
        payload['sources'] = self.all_source
        payload['country'] = None

        if topic:
            topic = "+".join(topic.split(" "))
            payload['q'] = topic

        if category:
            payload['category'] = category
            payload['sources'] = None
            payload['country'] = self.country  # us

        if source:
            payload['category'] = None
            payload['sources'] = source
            payload['country'] = None

        payload['language'] = self.language
        payload['pageSize'] = self.page_size
        payload['page'] = self.page

        # Send Request
        r = requests.get(self.url + '/top-headlines', auth=self.auth, timeout=30, params=payload)
        logger.info("get headline url: {}".format(r.url))
        return r.json()

    def get_everything(self, topic, source=None):
        """call this function with a concrete topic"""

        # Define Payload
        payload = {}
        payload['q'] = self.topic

        payload['sources'] = self.all_source
        if topic:
            topic = "+".join(topic.split(" "))
            payload['q'] = topic
        if source:
            payload['sources'] = source

        payload['domains'] = self.domains
        payload['from'] = self.from_parameter
        payload['to'] = self.to
        payload['language'] = self.language
        payload['sortBy'] = self.sort_by
        payload['page'] = self.page
        payload['pageSize'] = self.page_size

        # Send Request
        r = requests.get(self.url + '/everything', auth=self.auth, timeout=30, params=payload)
        logger.info("get everything url: {}".format(r.url))
        return r.json()

    def get_sources(self):

        # Define Payload
        payload = {}
        payload['category'] = self.category
        payload['language'] = self.language
        payload['country'] = self.country

        # Send Request
        r = requests.get(self.url + '/sources', auth=self.auth, timeout=30, params=payload)
        return r.json()

'''
client = OnlineNewsClient()
news = client.get_everything(topic='tennis')
if news:
    print(news['articles'][0]['title'].strip())
    print(news['articles'][0]['description'].strip())
'''
