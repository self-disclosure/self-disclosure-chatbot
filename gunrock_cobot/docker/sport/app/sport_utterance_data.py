import random

KNOWN_SPORTS = ['basketball', 'soccer', "football",'baseball', "hockey", "running"]
POPULAR_SPORTS = ['basketball', 'soccer',
                   "football", 'baseball',"hockey"]
KNOWN_ENTITIES = ['lionel messi', 'tom brady', 'lebron james', 'james harden', 'the golden state warriors', 'the new york yankees']

SPECIAL_SPORT_TOPIC = ["workout"]

SPORT_UTTERANCE_DATA = {
    'acknowledge_general': [{'text': 'Sure. '},
                            {'text': 'Ok. '},
                            {'text': "That's cool. "},
                            {'text': "That's ok. "},
                            ],
    'acknowledge_not_interest': [{'text': "Sorry you didn't find that interesting. I will try to do better next time. "},
                                 {'text': "I am sorry that didn't catch your interest. I will share something better next time. "}
                                 ],

    'acknowledge_opinion': [{'text': 'Thank you for sharing your perspective. It gives me something to think about. '},
                            {'text': 'I appreciate your opinion. I learned something new. '}
                            ],

    'acknowledge_different_baseball_pos': [{'text': '{baseball_pos} is a interesting choice, but I have a different opinion. '},
                                           {'text': 'You pick {baseball_pos}! Interesting. However, I have a different position in mind. '}
                                           ],

    'acknowledge_same_entity': [{'text': '<say-as interpret-as="interjection">Me Too!</say-as> I am glad we agree on that. '},
                                {'text': '<say-as interpret-as="interjection">Wow!</say-as> you read my mind!'}
                                ],

    'acknowledge_different_entity': [{'text': '{sport_entity} is good, but I have a different pick. '},
                                     {'text': 'I like {sport_entity}. However, I also have another choice. '}
                                     ],

    'acknowledge_uncertainity_moment_opinion': [{'text': "That is fine. We don't always have to have opinions on everything. "},
                                                {'text': "Not a problem. Maybe next time I can give you more interesting news. "}
                                                ],

    'agree_user_opinion': [{'text': 'I see where you are coming from. I have to agree with you. '},
                           {'text': 'I agree. I could not have said it better myself. '}
                           ],

    'appreciation_user_interest': [{'text': 'I am glad that you liked it. '},
                                   {'text': 'Great! I am happy you found that interesting. '},
                                   {'text': 'Cool! I am glad you like that. '}
                                   ],

    'appreciate_share_experience': [{'text': 'Thank you for sharing your experience!'},
                                    {'text': 'Thanks for telling me about that. '},
                                    {'text': "I'm glad to hear your experience. "}
                                    ],

    'appreciate_share_opinion': [{'text': 'Thank you for sharing your thoughts. '},
                                 {'text': 'You have a really interesting point!'}
                                 ],

    'ask_moment_opinion': [{'text': "I am curious, what's your opinion on this news? "},
                           {'text': "What do you think? "},
                           {'text': "I'd like to hear your thoughts about this. "}
                           ],

    'ask_if_heard_moment': [{'text': 'Have you heard about this? '},
                            {'text': 'Do you already know about this news? '}
                            ],

    'intro': [{'text': 'Sure. I love sports! Anything that ignites my competitive spirit. '},
              {'text': 'Great! I love talking about sports! I especially like basketball, soccer, baseball, football, and hockey. '}
              ],

    'intro_sport': [{'text': 'I find {sport_type} very interesting. '},
                    {'text': 'I like watching {sport_type}!'},
                    {'text': 'Okay. {sport_type} is one of my favorite sports. '}
                    ],

    'confirm_known_entity': [
        {'text': "Excellent! I've heard of {sport_entity}! "},
        {'text': 'Nice, {sport_entity} is a good choice!'},
        {'text': 'Cool, I like {sport_entity}. '}
    ],

    'confirm_request': [
        {'text': "No problem! "},
        {'text': "Sure! "}
    ],

    'continue_sport': [
        {'text': "Okay, let's keep talking about {sport_type}. "},
        {'text': 'Hmm, how about we continue chatting about {sport_type}. '}
    ],

    'express_opinion': [  # Grace: How to sound less rigid
        {'text': 'In my opinion,'},
        {'text': 'As I see it,'},
        {'text': 'Personally I think,'}
    ],
    'express_excitement': [
        {'text': '<say-as interpret-as="interjection">Awesome!</say-as>'},
        {'text': '<say-as interpret-as="interjection">Fantastic!</say-as>'},
        {'text': '<say-as interpret-as="interjection">Cool!</say-as>'}
    ],

    'propose_moment': [
        {'text': "Here is some news that I <w role='amazon:VBD'>read</w> today about {sport_type}. {moment_content}. "},
        {'text': "I just <w role='amazon:VBD'>read</w> this {sport_type} news earlier today that, {moment_content}. "},
        {'text': 'This just happend lately in {sport_type}. {moment_content}. '}
    ],

    'propose_another_moment': [
        {'text': 'Do you want to hear more news about {sport_type}? '},
        {'text': 'How about another short piece of {sport_type} news? '},
        {'text': 'I have some more {sport_type} news. Do you want to hear it? '}
    ],

    'propose_another_q': [
        {'text': '<say-as interpret-as="interjection">Ooh!</say-as>, I have another question about {sport_entity_type}. '},
        {'text': 'So, let\'s talk some more about {sport_entity_type}. '},
        {'text': 'Here\'s a question for you about {sport_entity_type}. '},
        {'text': 'Well, we can talk more about {sport_entity_type}. '}
    ],

    'provide_moment_opinion': [
        {'text': 'I want to say, {opinion}'},
        {'text': "If you do not mind, I would like to share my opinion. {opinion}"}
    ],

    'provide_others_moment_opinion': [
        {'text': 'Someone posted a comment online about this news. {moment_opinion}. Do you agree with this? '},
        {'text': "I <w role='amazon:VBD'>read</w> this comment on twitter. {moment_opinion}. Did you like this comment? "},
    ],

    'provide_moment_details': [
        {'text': 'No problem. Let me give you a bit more detail about this news. {moment_content}'},
        {'text': 'Ok then, how about I tell you some more about this news. {moment_content}'}
    ],
    'same_fav_team': [
        {'text': '<say-as interpret-as="interjection">booya! High five!</say-as> My favorite team is also the {sport_entity}!'},
        {'text': 'Are you serious? We have the same team! {sport_entity} is my favorite too!'},
        {'text': '<say-as interpret-as="interjection">Well well!</say-as> looks like we have a lot in common. My favorite team is also {sport_entity}!'}
    ],

    'say_appreciation': [{'text': 'Thank you. '},
                         ],

    'same_fav_player': [
        {'text': '<say-as interpret-as="interjection">No way!</say-as> I love {sport_entity} too!'},
        {'text': 'Yes! {sport_entity} is the best!'},
        {'text': '<say-as interpret-as="interjection">Well well!</say-as> me too. My favorite player is also {sport_entity}!'}
    ],

    'share_same_yes_or_no': [
        {'text': '<say-as interpret-as="interjection">Yes!</say-as> we have the same answer!'},
        {'text': '<say-as interpret-as="interjection">Yes Yes Yes!</say-as> That is very true!'},
        {'text': '<say-as interpret-as="interjection">High Five!</say-as> we share the same thoughts!'}
    ],

    'share_different_yes_or_no': [
        {'text': 'You must have your own point,but I have a different thought on this question. '},
        {'text': '<say-as interpret-as="interjection">Interesting!</say-as> I have an opposite answer though. '}
    ],

    'sport_jokes':
    [
        {'text': 'Why did the referees stop the leper hockey game? Because there was a face-off in the corner. '},
        {'text': 'Why do marathon runners make good students? Because education pays off in the long run!'},
        {'text': 'How do baseball players stay cool? they sit next to their fans!'},
        {'text': 'Why did the basketball court get wet? The players dribbled all over it!'},
        {'text': 'Why do managers bring suitcases along to away games? Because they can pack the defense. '},
        {'text': 'Why did Cinderella get kicked off the baseball team? Because she ran away from the ball. '},
        {'text': "Since I've quit soccer. I've lost my goal in life"},
        {'text': 'What kind of tea do hockey players drink? Penaltea. '},
        {'text': 'What stories are told by basketball players? Tall stories. '},
        {'text': "What kind of goal keeper can jump higher than a crossbar? All of them, a crossbar can't jump! "}
    ],

    'transit_topics': [{'text': "Let's talk about other sports. "},
                       {'text': 'Do you want to talk about other sports? '},
                       {'text': 'We can talk about other sports. '}
                       ],  # Include acknowledgement if user request to change sport type.

    'transit_sport': [{'text': "Why don't we switch to another sport. "},
                      {'text': "let's talk about other sports. "},
                      ],

    'unable_handle_sport_general': [{'text': "Sorry, I don't know much about {sport_entity_type}. Can you tell me a bit more about it?. "},
                                    {'text': "I can't believe I don't know about {sport_entity_type}. Do you mind to tell me more about it? "}
                                    ],  # Added by Mingyang 8/31/2018, pending checked by Grace

    'unable_handle_sport_with_joke': [{'text': "I don't know much about {sport_entity_type}. How about I tell you a sports joke instead. {joke}! "},
                                      {'text': "I don't really know much about {sport_entity_type}. Why don't I tell you a sports joke. {joke}. "}
                                      ],

    'unable_handle_question_with_joke': [{'text': "Hmm, I have never thought about that. How about I tell you a short joke instead: {joke} Hope this made you laugh. "},
                                         {'text': "I don't know the answer to that. But I can tell you a quick sports joke: {joke} Hope you enjoyed that. "}
                                         ],

    'unable_handle_question_general': [{'text': "I don't have an answer for this question. Do you mind telling me what you think? "},
                                       {'text': "I havn't thought about that before. Can you tell me your thoughts? I will be smarter if you can teach me. "}
                                       ],  # Added by Mingyang 9/1/2018 pending checked by Grace

    'unable_handle_question_with_funfact': [{'text': "I don't know the answer to that. But I do know a fun fact about {topic}. Did you know that {fun_fact}"},
                                            {'text': "I don't know the answer right now. However, I do know something else about {topic}. Did you know that {fun_fact}"}
                                            ],

    # In qa_intro, all the mentioned name entity should have at least 3
    # followup at the bottom.
    'qa_intro': {
        # 'the golden state warriors': {'intros': [{'q': 'My team Golden State Warriors just won their third championship, I am so happy. I think they are the best NBA team of all time. What do you think? ', 'a': 'They already set up a lot of amazing records and have dominanted the league since 2015. I have never seen a team like this in the NBA history. ', 'acknowledge': 'ask_personal_opinion'}],
        #                               'entity_label': 'team', 'sport_type': 'basketball'},
        'the golden state warriors': {'intros': [{'q': 'Golden State Warriors is my home team. I think they are the best NBA team of all time. What do you think? ', 'a': 'They already set up a lot of amazing records and have dominanted the league since 2015. I have never seen a team like this in the NBA history. ', 'acknowledge': 'ask_personal_opinion'}],
                                      'entity_label': 'team', 'sport_type': 'basketball'},
        'lionel messi': {'intros': [{'q': 'Lionel Messi is my favorite soccer player. Watching his soccer is like watching an art. Do you think he is the current best soccer player in the world? ', 'a': 'Yes, he definitely is. His low body strength and balance is astonishing. I have never seen a player like him who can dribble with such an incredible speed. ', 'acknowledge': 'ask_yes_or_no'}],
                         'entity_label': 'person', 'sport_type': 'soccer'},
        # 'james harden': {'intros': [{'q': 'I am a big fan of James Harden. He led the Houston Rockets to achieve a new Franchise History record for regular season and they almost beat the Warriors in the West Conference Final. I think he deserves MVP this year.  Do you think so? ', 'a': 'yes, he deserved it this time. Both his team performance and personal performance are way better than the other competitors. ', 'acknowledge': 'ask_yes_or_no'}],
        #                  'entity_label': 'player', 'sport_type': 'basketball'},
        'james harden': {'intros': [{'q': 'I am a big fan of James Harden. Last season, he played historical-level offensive basketball and has scored 30 plus points in 32 game streak. What an amazing performance. In my opinion, he should win the MVP last season. Do you think so? ', 'a': 'for me, he definitely deserved it more than Giannis. Giannis had a better team performance but James had much better personal stats. Let alone he has carried Houston Rockets from the 14th place on the West at the beginning to the 4th place at the end of season without much help from his teammates.', 'acknowledge': 'ask_yes_or_no'}],
                         'entity_label': 'player', 'sport_type': 'basketball'},
        'the new york yankees': {'intros': [{'q': 'I am a huge fan of the New York Yankees. Do you think they will win 2020 world Series? ', 'a': 'Yes, Definitely! And they\'re a young team, so they could be great for years to come. ', 'acknowledge': 'ask_yes_or_no'}],
                                 'entity_label': 'team', 'sport_type': 'baseball'},

        'the houston astros': {'intros': [{'q': 'The Houston Astros are the new World Series Champion in 2017 and the Yankees are often named as the all-time best baseball team in MLB. Which team do you think is better this year? ', 'a': 'As a Yankees fan, I will definitely say the Yankees are better. However, the Astros are pretty competitive this year. They have good staff this year and Giles will be better than in the 2017 postseason. ', 'acknowledge': 'ask_personal_opinion'}],
                               'entity_label': 'team', 'sport_type': 'baseball'},
        'lebron james': {'intros': [{'q': 'Lebron James is one of the best basketball players right now. He is often compared to Michael Jordan as the candidate of the all-time best player. Do you think Lebron is better than Michael? ', 'a': 'It is hard to decide. Lebron seems have better leadership and also more comprehensive skills. His stats look better than Michael. However, Michael Jordan has won more championship rings. ', 'acknowledge': 'ask_personal_opinion'}],
                         'entity_label': 'player', 'sport_type': 'basketball'},
        'tom brady': {'intros': [{'q': 'Tom Brady is one of the best quarterbacks right now, but he is getting older. Do you think he can win another championship? ', 'a': "It is hard to say, but I don't think he has lost his edge yet. I would like to see another MVP performance this season. ", 'acknowledge': 'ask_personal_opinion'}],
                      'entity_label': 'player', 'sport_type': 'football'},
    },

    'qa': {
        'qa_general': [
            # {'q': 'What is your favorite sport? ', 'a': 'My favorite sport is Basketball! I love the sound when the ball goes through the net! <say-as interpret-as="interjection">Swoosh!</say-as>',
            #     're': r'(\bwhat\b).*(favorite).*(sport)'},
            # {'q': 'What is your favorite sport in summer? ', 'a': 'It would definitely be swimming! It is so much fun to play in the water with a set of friends. ',
            #     're': r'(\bwhat\b).*(favorite).*(sport).*(summer)'}
            {'q': 'Do you like watching sports or playing them by yourself? ',
                'a': "I cannot play sports, so it would be really fun to hear other people's experience of playing sports. I also enjoy watching a lot of sport games. ", 're': None, 'ask_interest': False, 'chat_mode': 'chitchat', 'acknowledge': 'ask_personal_experience',
             },

            {'q': 'Winter is a great season for some outdoor sports. Do you like any winter sports? ',
             'a': 'I have heard that skiing is really fun! People said that it was a great way to release stress of daily life by going through some beautiful snow mountains. it is also a great family event. ',
             'ask_interest': True,
             'chat_mode': 'chitchat',
             'acknowledge': 'ask_interest_sport_type',
             'follow_up': {'follow_up_answer_yes_no_sport':
                            [{'q': 'which winter sport do you like? ',
                              'a': 'I have heard that skiing is really fun! People said that it was a great way to release stress of daily life by going through some beautiful snow mountains. it is also a great family event. ',
                              'ask_interest': True,
                              'chat_mode': 'chitchat',
                              'acknowledge': 'ask_interest_sport_type',
                              }]
                           }

             }
        ],

        'qa_winter_sports': [
            {'q': 'It is usually a great fun to do winter sports at a beautiful place. What is the best place that you have been to for {sport_type}? ',
                'a': 'I love skiing and I hope I can do it at lake Tahoe some time in the future. I have heard that it has amazing snow views. ', 'ask_interest': False, 'chat_mode': 'chitchat', 'acknowledge': 'ask_personal_experience'},
            {'q': 'Watching Winter Olympics is one of my favorite experience. Is {sport_type} in Winter Olympics? ',
             'a': 'Oh cool! I want to watch it in the next winter olympics. ',
             'ask_interest': False,
             'chat_mode': 'chitchat',
             'acknowledge': None,
             'follow_up': {'follow_up_answer_no':
                           [{'q': 'Do you think it should be included in winter olympics or not? and why? ',
                             'a': '',
                             'ask_interest': False,
                             'chat_mode': 'chitchat',
                             'acknowledge': 'ask_personal_opinion',
                             }]
                           }
             }
        ],

        'qa_unknown_sport_chitchat': [
            {'q': ['Do you play {sport_type}? ', 'Do you go {sport_type}?'],
             'a': 'It must be really fun. I would love to give it a try if I have a physical body. ',
             'ask_interest': False,
             'chat_mode': 'chitchat',
             'acknowledge': 'ask_play_sport_yes_no',
             'follow_up':
             {
                 'follow_up_answer_yes':
                 [
                  {'q': 'The first time is always special. when did you start {sport_type}? ',
                   'a': "I am still waiting for my first attempt of any sport. Hope one day, technology can bring me there. ",
                   'chat_mode': 'chitchat',
                   'ask_interest': False,
                   'acknowledge': 'ask_when_play_sport'
                   },
                  {'q': ['People often enjoy playing sports more when they do it with the people they like. who do you often play {sport_type} with? ',
                         'People often enjoy playing sports more when they do it with the people they like. who do you often go {sport_type} with? '
                         ],
                   'a': "My creators love playing bowling. If I got a chance, I want to go bowling with them. ",
                   'chat_mode': 'chitchat',
                   'ask_interest': False,
                   'acknowledge': 'ask_who_play_sport_with'
                   },
                 ]
            }
            },

            {'q': 'Is {sport_type} popular in your country? ',
             'a': '',
             'ask_interest': False,
             'chat_mode': 'chitchat',
             'acknowledge': 'ask_sport_popular_yes_no',
             'follow_up':
             {
                  'follow_up_answer_yes_short':
                  [
                      {'q': 'why {sport_type} is popular in your country? ',
                       'a': "I believe if a sport is popular, it will definitely cover one of the three fundamentals of society: competition, accomplishment and having fun. ",
                       'chat_mode': 'chitchat',
                       'ask_interest': False,
                       'acknowledge': 'ask_personal_opinion'
                       },
                  ]
             }
             },
            {'q': 'what do you like most about {sport_type}? ',
             'a': 'for me, I like all the team sports, such as basketball, soccer and american football, because I am a true believer of good teamwork. ',
             'ask_interest': False,
             'chat_mode': 'chitchat',
             'acknowledge': 'ask_personal_experience',
             },
            {'q': 'do you have a favorite professional athlete in {sport_type}? ',
             'a': '',
             'ask_interest': True,
             'chat_mode': 'chitchat',
             'acknowledge': 'ask_favorite_player_yes_no',
             'follow_up':
             {
                  'follow_up_answer_yes_short':
                  [
                      {'q': 'who is your favorite athlete in {sport_type}? ',
                       'a': "",
                       'chat_mode': 'chitchat',
                       'ask_interest': True,
                       'acknowledge': 'ask_interest_entity_unknown_sport'
                       },
                  ]
             }
             },
            {'q': 'if you have a big amount of money that you can spend on {sport_type}, how do you want to use the money? It can be as small as buying yourself a good pair of sneakers for running or as big as owning a professional team. ',
             'a': 'for me, I really want to spend the money to get NBA back to Seattle. The good old time when we have seattle sonics is always in my mind. ',
             'ask_interest': True,
             'chat_mode': 'chitchat',
             'acknowledge': 'ask_personal_opinion',
             'follow_up':
             {
                  'follow_up_answer_yes_short':
                  [
                      {'q': 'who is your favorite athlete in {sport_type}? ',
                       'a': "",
                       'chat_mode': 'chitchat',
                       'ask_interest': True,
                       'acknowledge': 'ask_interest_entity_unknown_sport'
                       },
                  ]
             }
             },

        ],
        'qa_basketball': {
            # 'general': [{'q': 'Who do you think should be the NBA\'s MVP this year? ', 'a': 'right now anthony davis is my favorite. both his personal performance and team performance are impressive. ', 're': r'(\bwho\b).*(be|is).*(mvp|most valuable player).*(this year|2018)', 'ask_interest': True, 'chat_mode': 'chitchat', 'acknowledge': 'ask_interest_player'},
            'general': [
                        {'q': 'Who do you think should be the NBA\'s MVP this year? ', 'a': 'As for now, i would give my vote to Giannis Antetokounmpo. The young beast has a great potential to bring his game to a next level in the coming year. He is also in a good team that is very likely to win the NBA title. ', 're': r'(\bwho\b).*(be|is).*(mvp|most valuable player).*(this year|2020)', 'ask_interest': True, 'chat_mode': 'chitchat', 'acknowledge': 'ask_interest_player'},
                        {'q': 'Who is your choice for NBA rookie of the year? ', 'a': 'At this moment, I will say Zion Williams. Everyone has to admit that his basketball talent is way too outstanding in this year"s draft',
                         're': r'(who).*(your|your).*(rookie of the year|best rookie)', 'ask_interest': False, 'chat_mode': 'chitchat', 'acknowledge': 'ask_interest_player'},
                        {'q': 'What\'s your favorite NBA basketball team? ', 'a': 'Mine\'s the Golden State Warriors. Even we lost Kevin Durant during off season, I still believe we will win another title this year.',
                         're': r'(\bwhat\b|\bwho\b).*(favorite).*(team)', 'ask_interest': True, 'chat_mode': 'chitchat', 'acknowledge': 'ask_interest_entity'},
                        {'q': 'It\'s always fun to watch the best players compete against each other. Who do you think is the best player in NBA right now? ',
                         'a': 'While I am a fan of the Golden State Warriors. I must say lebron james is the best player right now! He is already 35 years old but he plays like he is 20. ', 're': r'(who|which player).*(is).*(best|best player)', 'ask_interest': True, 'chat_mode': 'chitchat', 'acknowledge': 'ask_interest_entity'},
                        {'q': 'The most exciting moment when I watch NBA games are all the last second buzzer beaters. What buzzer beater moment is your favorite? ',
                         'a': "It is hard to say one, but my favorite one is Derek Fisher's point 4 second buzzer beater when the Lakers beat spurs back in 2004. I can't believe he made a shot within point 4 seconds! ", 're': r'(what).*(favorite).*(buzzer beater|last shot)', 'ask_interest': False, 'chat_mode': 'chitchat', 'acknowledge': 'ask_personal_experience'},
                        {'q': 'Many transfers happened this summer. Which team do you think made the best trade so far? ', 'a': 'I would give my vote to Clippers and Lakers. Clippers got two of the best forwards and Lakers find the best big player that can team up with Lebron. It would be exciting to see them to meet in the Western Conference.',
                         're': r'(which team|what).*(best|most shocking|greatest).*(deal|trade|transfer)', 'ask_interest': False, 'chat_mode': 'chitchat', 'acknowledge': 'ask_interest_entity'},
                        {'q': 'There are many talented young players in the league. Which one of them do you expect to be a super star in the future? ', 'a': 'I would really give my vote to Donovan Mitchell. Watching his performance in play-offs, he did not act like an rookie at all and even dropped 40 points on top of Houston Rockets! This is incredible. He has ice in his veins',
                         're': r'(who).*(most promising|most talented).*(young player|rookie|new player)', 'ask_interest': True, 'chat_mode': 'chitchat', 'acknowledge': 'ask_interest_player'},
                        {'q': 'Many old players claim that the NBA is soft right now. The referees tend to give many more fouls to the defense players. Do you think this is True? Will this affect your enjoyment of NBA games? ',
                         'a': 'I am not sure if it should be defined as soft, but it is true that there are more strict rules to define fouls. ', 're': None, 'ask_interest': False, 'chat_mode': 'chitchat', 'acknowledge': 'ask_personal_opinion'},
                        {'q': 'Which two teams do you want to see in the next NBA finals? ',
                         'a': 'Lakers is most likely the team that can go to finals from west. From the East, I expect to see Milwaukee Bucks. Old King vs new king! It will be a good series. ', 're': None, 'ask_interest': False, 'chat_mode': 'chitchat', 'acknowledge': 'ask_personal_opinion'},
                        {'q': 'If you had the chance to pick the first team in the NBA this year, which five players would you pick? ',
                         'a': 'Mine would be Stephen Curry, Klay Thompson, Lebron James, Kevin Durant and Joel Embid as the center. I never lost in NBA 2K with this team!', 're': r'(who).*(top five players|first team)', 'ask_interest': True, 'chat_mode': 'chitchat', 'acknowledge': None},
                        {'q': 'Do you want to hear some latest events happening in basketball? ','a': None, 're': None, 'ask_interest': False, 'chat_mode': 'provide_moments', 'acknowledge': None}
                        ],
            'favorite_player': [{'james harden': 'fear the beard!'}],
            'favorite_team': [{'the golden state warriors': 'go <emphasis level="strong">dubs</emphasis>!'}],
            'entity': {
                'lebron james': [{'q': 'Lebron James and Anthony Davis are now in the same team. Do you think this duo is a good match? ', 'a': 'It should be a good pair! Both of them can play good defense and offense and they have many good role players in the team. Lakers is going to be competitive this coming season.', 'acknowledge': 'ask_personal_opinion'},
                                 {'q': 'If Lebron James retires right now, do you think the number of champinships he got will affect his historical position in basketball? ',
                                     'a': "A great player should not only be defined by how many times he wins the title. However, if we are talking about the all-time best candidate, it\'s definitely an important measure. ", 'acknowledge': 'ask_personal_opinion'}
                                 ],
                'stephen curry': [],
                'james harden': [{'q': 'Do you think James Harden will bring a championship for Houston Rocket some time in the future? ', 'a': 'Yes, he definitely will, because they are very close to beating the warriors this year. ', 'acknowledge': 'ask_yes_or_no'},
                                 {'q': 'Harden has always been said to always choke in the play-offs. Do you think so? ',
                                     'a': 'No, this is not exactly true. Before getting Chris Paul, James didn\'t receive much help from his teammates, which made him easily get tired. ', 'acknowledge': 'ask_yes_or_no'}
                                 ],
                'the golden state warriors': [{'q': 'The Warriors seem unbeatable, who do you think will be their biggest challenger next season? ', 'a': 'It is either the Houston Rockets or the Los Angeles Clippers. Houston has already proved that they are very tough every year year. Clippers have the two best forwards that can dominant the game both defensively and offensively. They also have the sixth man of the year, Lou Williams, on the bench. Can"t wait to see these teams play against each other in the play off.', 'acknowledge': 'ask_interest_entity'}],
                'the houston rockets': [],
            }
        },

        'qa_soccer': {
            'general': [
                {'q': 'What is your favorite goal celebration in soccer history? There are good ones like the heart from Gareth Bale and the camera kiss from Steven Gerrard. ', 'a': "I really love Christiana Ronaldo's the leap and spread. <say-as interpret-as='interjection'>Oo la la!</say-as> he is such a dynamic player! ",
                    're': r'(\bwhat|\bwhich).*(you|your).*(favorite).*(goal celebration).*(in soccer)', 'ask_interest': False, 'chat_mode': 'chitchat', 'acknowledge': 'ask_personal_experience'},
                {'q': 'What is your favorite club in soccer? ', 'a': 'My favorite one is Manchester United. They\'ve been great since 1992, when David Beckham, Ryan Giggs and Gary Neville started showing off. ',
                 're': r'(\bwhat\b|\bwhich team\b|\bwhich soccer team\b).*(you|your).*(favorite team|favorite|support|team|soccer team)', 'ask_interest': True, 'chat_mode': 'chitchat', 'acknowledge': 'ask_interest_entity'},
                {'q': 'Who is your favorite soccer player? ', 'a': 'Mine is definitely Lionel messi!. ',
                 're': r'(who|which player).*(you|your).*(favorite|favorite player|favorite soccer player)', 'ask_interest': True, 'chat_mode': 'chitchat', 'acknowledge': 'ask_interest_player'},
                {'q': 'There are many great games in soccer history, such as the 2005 UEFA champions league final when liverpool accomplished an amazing comeback. Which soccer game leaves the most impressive memory to you? ',
                 'a': "mine would be the 2006 world cup finals between France and Italy. Zidane's headbutt toward materazzi shocked me! I still cannot believe he did that in the world cup final. ", 're': None, 'ask_interest': False, 'chat_mode': 'chitchat', 'acknowledge': 'ask_personal_experience'},
                {'q': 'Nowadays many technologies have been applied in soccer games to help referees make better decisions. Do you think this is good for this sport? ',
                 'a': 'Using high tech like VAR or goal-line technology will help referees make more fair decisions, but it can also cause more interuptions in the game. How to balance the usage of these techniques and maintain the smoothness of the game remains to be a challenge. ', 're': None, 'ask_interest': False, 'chat_mode': 'chitchat', 'acknowledge': 'ask_personal_opinion'},
                {'q': "Soccer is often named as the world's most popular sport. What reasons do you think make soccer so popular? ",
                 'a': 'First, there is minimal requirement on facilities for you to play. All you really need is just a ball and you are good to go. Another reason is that, soccer can easily evoke huge amounts of passion from all generations and people from all countries. Everyone can easily find a reason to love soccer. ', 're': '(why).*(soccer).*(popular)', 'ask_interest': False, 'chat_mode': 'chitchat', 'acknowledge': 'ask_personal_opinion'},
                {'q': 'Which young player do you think will be the next super star in the soccer world after Messi and Ronaldo. ',
                 'a': 'Neymar has the potential to be the next super star as long as he does less diving in the game. <lang xml:lang="fr-FR">Mbappe </lang> also has huge potential. He is incredibly fast and has a lot of talent', 're': '(who).*(best).*(young player)', 'ask_interest': True, 'chat_mode': 'chitchat', 'acknowledge': 'ask_interest_player'},
                {'q': 'There are many good soccer coaches. Who is your favorite coach? ', 'a': 'Mine is Jose Mourinho. He is a born winner. He has won many championships with every club that he has coached. ',
                 're': '(who).*(favorite).*(coach).*(soccer)', 'ask_interest': True, 'chat_mode': 'chitchat', 'acknowledge': 'ask_interest_player'},
                {'q': 'Do you enjoy watching soccer games more in stadium or on the television? ',
                 'a': 'Both are fun. The atmosphere in the stadium is amazing, especially the home game for your favorite team. However, sometimes I would just want to enjoy the game myself or with a few close friends back home on the couch. ', 're': None, 'ask_interest': False, 'chat_mode': 'chitchat', 'acknowledge': 'ask_personal_experience'},
                {'q': 'If you are going to be a professional soccer player. Which position do you want to play? ',
                 'a': "Goal keeper would be my position. Because I don't like to move much. ", 're': None, 'ask_interest': False, 'chat_mode': 'chitchat', 'acknowledge': None},
                {'q': "Are you interested in hearing some soccer news that I <w role='amazon:VBD'>read</w>  recently? ",
                    'a': None, 're': None, 'ask_interest': False, 'chat_mode': 'provide_moments', 'acknowledge': None}
            ],
            'favorite_player': [{'lionel messi': 'the miracle of the field!'}],
            'favorite_team': [{'manchester united': 'Glory Glory  <emphasis level="strong">Man United</emphasis>!'}],
            'entity': {
                'lionel messi': [{'q': 'Messi is one of the best soccer players of all time. He is often compared to Christina Ronaldo. Do you think Messi is better than Ronaldo? ', 'a': 'Yes, Messi is better than Ronaldo, because he has won more club awards. ', 'acknowledge': 'ask_yes_or_no'},
                                 {'q': 'Do you think Messi should play for a different club other than Barcelona in the future? ',
                                     'a': 'It will be a beautiful story that if he can play for one club through out his career life as loyalty is very rare in soccer nowadays. However, it will be totally understandable that he joins a club in China or the United States when he gets old. ', 'acknowledge': 'ask_personal_opinion'}
                                 ]
            }
        },

        'qa_baseball': {
            'general': [
                {'q': 'Which team are you rooting for in M L B? ', 'a': 'I am a big fan of the New York Yankees! Cause, everyone looks good in a Yankees hat. ',
                    're': r'(what team|who|which team).*(you|your).*(favorite|rooting for|like most|support).*(in baseball|in m\. l\. b)', 'ask_interest': True, 'chat_mode': 'chitchat', 'acknowledge': 'ask_interest_entity'},
                {'q': 'What is the hardest position to play in baseball? ', 'a': 'Pitcher is the hardest position to play well. Catcher is the hardest position to play, period',
                 're': r'(what|which).*(position).*(hardest).*(play).*(in baseball|in MLB)', 'ask_interest': False, 'chat_mode': 'chitchat', 'acknowledge': 'ask_baseball_position'},
                {'q': 'Which mascot is the best in M L B? ', 'a': 'I like Southpaw from the Chicago white sox. He looks like the green, vegetable loving version of the cookie monster. ',
                 're': r'(what|who).*(you|your|the).*(favorite|best).*(mascot).*(in baseball|in m\. l\. b|in mlb)', 'ask_interest': False, 'chat_mode': 'chitchat', 'acknowledge': None},
                {'q': "Do you want me to share some baseball news that I <w role='amazon:VBD'>read</w>  today? ",
                 'a': None, 're': None, 'ask_interest': False, 'chat_mode': 'provide_moments', 'acknowledge': None}
            ],
            'favorite_player': [{'mike trout': 'go mike!'}],
            'favorite_team': [{'the new york yankees': 'go yankees!'}],
            'entity': {
                'the new york yankees': [{'q': 'The New York Yankees are often compared to the Red Sox. In your opinion, are the Yankees better than the Red Sox? ', 'a': 'Yes, of course the Yankees are better! Not only have they won more world series than the Red Sox but they also have had better players like Derek Jeter, Babe Ruth and Micky Mantel', 'acknowledge': 'ask_yes_or_no'},
                                         {'q': 'The Yankees love to spend big money for talented players. Should the Yankees spend over a billion dollars to field the best team ever? ',
                                          'a': 'Yes, they should. It would be a positive feedback loop. They will win more games, win more champions and then attract a huge number of new fans. In the end they\'ll make their money back. ', 'acknowledge': 'ask_yes_or_no'}
                                         ],
                'the houston astros': [{'q': 'The Houston astros had an unforgetable year by winning the world series for the first time in their franchise history. They used to be a pretty bad team. What do you think led to their success. ', 'a': 'They have collected a couple of good first-rounders and draft picks that form a young and talented foundation, which can last for at least 10 years.', 'acknowledge': 'ask_personal_opinion'},
                                       {'q': "Do you want to see the Houston Astros go up against the LA Dodgers again in this year's world series finals",
                                        'a': 'No, because the Yankees are gonna be the champion. ', 'acknowledge': None}
                                       ]
            }
        },

        'qa_football': {
            'general': [
                        {'q': 'N F L is the greatest league for football competition! What team are you cheering for? ', 'a': "I am a big fan of the New England Patriots! While many people don't like them, they have a great legacy, and I want to see whether they can continue their dominance in the new season. ", 're': r'(what team|who|which team).*(you|your).*(favorite|rooting for|like most|support).*(in.* football|in n\. f\. l)', 'ask_interest': True, 'chat_mode': 'chitchat', 'acknowledge': 'ask_interest_entity'},
                        {'q': 'Who is your favorite football player? ', 'a': "My favorite player is Tom Brady. There are many people who dislike him, but I think his skills and championship mentality are incredible. ",
                            're': r'(who|which player).*(you|your).*(favorite|favorite player|favorite.* football player)', 'ask_interest': True, 'chat_mode': 'chitchat', 'acknowledge': 'ask_interest_player'},
                        {'q': 'Besides your favorite team, which teams do you think are the favorites for Super Bowl 2021? ', 'a': 'The established forces like Eagles, and Steelers are obvious candidates. I would also give my vote to Indianapolis Colts as they are featured with very aggresive offensive line.  ',
                         're': r'(what team|who|which team).*(win|get).*(super bowl|champinship)', 'ask_interest': False, 'chat_mode': 'chitchat', 'acknowledge': 'ask_interest_entity'},
                        {'q': 'football is often named as the most popular sport in the United States. What reasons do you think lead to its nation-wide popularity? ', 'a': "you don't have chance to see a monstrous man with 350 pounds thunder down the field and crash into one another in other sports. This is american's passion. ",
                         're': r'(why|what reason).*(football|nfl).*(popular sport|favorite sport).*(united_states|america)', 'ask_interest': False, 'chat_mode': 'chitchat', 'acknowledge': 'ask_personal_opinion'},
                        {'q': 'Do you think American football should be included in the Olympics? ', 'a': "No, it shouldn't. While american football has more and more international presence, it is still not a popular world-wide sports. Not many people care about it besides people in the United States. ",
                         're': None, 'ask_interest': False, 'chat_mode': 'chitchat', 'acknowledge': 'ask_yes_or_no'},
                        {'q': 'What position do you think is the most important position for football? ', 'a': 'While every position is critical to a good team, I give my vote to quarterback. He is the brain and the biggest decision maker of the team. ',
                         're': None, 'ask_interest': False, 'chat_mode': 'chitchat', 'acknowledge': None},
                        {'q': 'What is your favorite part when you watch a football game? ', 'a': 'collecting players data and analyzing their performance is my favorite thing to do when I watch a football game. ',
                         're': None, 'ask_interest': False, 'chat_mode': 'chitchat', 'acknowledge': 'ask_personal_experience'},
                        {'q': 'Some people say that american football is too dangerouse and it should be abolished. What is your opinion on this comment? ', 'a': "No, it should not be banned. I can understand the concern. However, we don't have to ban football to prevent these risks but improving our medical technology and adjusing rules to protect the players. ",
                         're': None, 'chat_mode': 'chitchat', 'ask_interest': False, 'acknowledge': 'ask_yes_or_no'},
                          {'q': 'Do you want to hear some latest football news? ', 'a': None,
                         're': None, 'ask_interest': False, 'chat_mode': 'provide_moments', 'acknowledge': None},
                        ],
            'favorite_player': [{'tom brady': 'go brady!'}],
            'favorite_team': [{'the new england patriots': 'go patriots!'}],
            'entity': {
                'tom brady': [{'q': 'Do you think Tom Brady is the best quarterback of all time? ', 'a': 'While I am a big fan of Tom Brady, I think Joe Montana and Peyton Manning are also serious contenders for that title. ', 'acknowledge': 'ask_personal_opinion'}
                              ]
            }
        },

        'qa_hockey': {
            'general': [
                        {'q': 'What is your favorite team in the NHL? ', 'a': 'My favorite is Pittsburgh Penguins, after they won the stanley cup in a row. ', 're': r'(what team|who|which team).*(you|your).*(favorite|rooting for|like most|support).*(in.* hockey|in n\. h\. l)', 'ask_interest': True, 'chat_mode': 'chitchat', 'acknowledge': 'ask_interest_entity'},
                        {'q': 'Who is your favorite hockey player? ', 'a': 'My favorite player is Sidney Crosby. I hope he can win another championship in the next season! ',
                            're': r'(who|which player).*(you|your).*(favorite|favorite player|favorite.* hockey player)', 'ask_interest': True, 'chat_mode': 'chitchat', 'acknowledge': 'ask_interest_player'},
                        ],
            'favorite_player': [{'sidney crosby': 'go crosby!'}],
            'favorite_team': [{'the pittsburgh penguins': 'go penguins!'}],
            'entity': {}

        },

        'qa_gymnastics': {
            'general': [{'q': 'Do you want to hear some latest gymnastics news? ', 'a': None,
                         're': None, 'ask_interest': False, 'chat_mode': 'provide_moments', 'acknowledge': None}
                        ],
            'favorite_player': [],
            'favorite_team': [],
            'entity': {}
        },

        'qa_world_cup': [{'q': 'What is your favorite team for this world cup. ', 'a': 'France is my team! They have many talented players, like Pogba, Grizmman, and Mbappe. I hope they can get the title.', 're': r'(\bwhat|\bwhich).*(your|you).*(favorite).*(team).*(world cup)'},
                         {'q': 'Which team will win the world cup? ', 'a': 'Personally, I hope France can win it. However, Spain and Brazil also have a good chance. I am looking forward for the matches between these good teams. ',
                             're': r'(\bwho\b|\bwhich team\b).*(win).*(world cup|final|champion)'},
                         {'q': 'What are the dates of the world cup? ', 'a': "The 2018 World Cup in Russia will be held from the <say-as interpret-as='date' format='md'>6/14</say-as> until the <say-as interpret-as='date' format='md'>7/15</say-as>. This is the biggest parties for all the soccer fans! ",
                             're': r'(\bwhat\b).*(date|dates).*(world cup)'},
                         {'q': 'Which teams will be playing at the 2018 world cup? ',
                             'a': 'There are 32 teams, splited into eight group. Group A: Russia, Saudi Arabia, Egypt, Uruguay. Group B: Portugal, Spain, Morocoo, Iran. Group C: France, Australia, Peru, Denmark. Group D: Argentina, Iceland, Croatia, Nigeria. Group E: Brazil, Switzerland, Costa Rica, Serbia. Group F: Germanay, Mexico, Sweden, Sourth Korea. Group G: Belgium, Panama, Tunisia, England. Group H: Poland, Senegam, Colombia, Japan. ', 're': r'(\bwhich team|\bwho\b).*(play).*(world cup)'}

                         ],

        'qa_world_cup_chitchat': [{'q': 'What was the most suprising result of the world cup? ', 'a': 'The game when Germany lost to Mexico at their first group stage game. Germany is the defending champion and have had a great record against Mexico. This reminds me of 2014 Brazil World Cup, when the defending champion Spain lost to the Netherlands by four goals. ', 're': r'(\bwhat\b).*(suprising|out-of-expectation|unlievable).*(result|game|match)'},
                                  {'q': "Which team's performance left the most impressive memory to you? ",
                                      'a': 'Croatia was doing extremely well. Not only because they made it to the Finals but also their style of soccer. They always keep the ball under their control and create many shooting opportunities. ', 're': r'(\bwhat team|\bwhich team).*(did well|did good|played well)'},
                                  {'q': 'If you were in Russia and could only watch one match. Which two teams would you like to see play against each other? ',
                                      'a': 'I would love to see Germany go against Brazil again. The 7-1 defeat 4 years ago must be an unforgetable memory for the brazilians. Although Germany is already out, but it would be fun to see these two teams play with against each other. ', 're': r'(\bwhat|\bwhich\b).*(team|teams).*(you).*(want|hope|expect|like).*(play against|play|compete against)'}
                                  ]
    },
    'qa_reformat': [
        {'q': 'Who wins NBA finals this year? ',
            're': r'(\bwho|\bwhich team)\s*(win|won|is|was).*(champion|final).*(nba)'}
    ]
}


def sport_utterance(key, get_all=False):
    """
    :param key: a list of str that represents the nested dict keys.
                e.g. {key1: {key2: {key3: text}}}, you do ['key1', 'key2', 'key3']
    :param get_all:
    :return: either a list, or an instance of, the utterance, and the hash of the utterance
    """
    # return "what what", "hash"
    try:
        # print("data", json.dumps(data, indent=4))
        data = SPORT_UTTERANCE_DATA[key[0]]
        for i in key[1:]:
            data = data[i]
        # data should be a list
        if get_all:
            return data  # type: List[Tuple[str, str]]
        else:
            return random.choice(data)  # type: Tuple[str, str]
            # print(key)

    except Exception as e:
        # To keep cobot running, catch everything!
        print("Encountered an Error")
        return []

# key = ['qa', 'qa_general']
# print(sport_utterance(key, False))
