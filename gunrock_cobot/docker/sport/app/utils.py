import json
import logging
import re
import socket
import requests
from datetime import datetime, timedelta
import os
import hashlib
from fuzzywuzzy import fuzz
import random
import praw

#import intentmap_scheme
#intentmap_scheme = None
import redis

import template_manager

template = template_manager.Templates.sys_rg
template_social = template_manager.Templates.social
template_transition = template_manager.Templates.transition

# Reddit API KEY
REDDIT_CLIENT_ID = "lKmLeMmSSLns6A"
REDDIT_CLIENT_SECRET = "Fi6V_pCzKqE-4TRVqcmpMJdET2s"

WORKING_ENV = "local"
if os.environ.get('STAGE') == 'PROD' or os.environ.get('STAGE') == 'BETA':
    WORKING_ENV = os.environ.get('STAGE').lower()
    logging.getLogger().setLevel(logging.INFO)
else:
    WORKING_ENV = "local"
    logging.getLogger().setLevel(logging.DEBUG)

EVI_PROFANITY_RESPONSE = 'rather not answer that'

HOST_NAME = socket.gethostname()

RANDOM_THOUGHT_KEY = "gunrock:randomthoughts"
HOLIDAY_KEY = "gunrock:ic:holiday:ts"
HOLIDAY_PREFIX_KEY = "gunrock:ic:holiday:"
COMMON_NAME_KEY = "gunrock:name:set"
RETRIEVAL_LIST_KEY = "gunrock:ic:retrieval:apikeys"

FIVETHREEEIGHT_KEY_PREFIX = "gunrock:ic:538:sig:"
TEDTALK_TOPIC_PREFIX = "gunrock:ic:tedtalk:topic:"

r = redis.StrictRedis(host='52.87.136.90', port=16517,
                      socket_timeout=3,
                      socket_connect_timeout=1,
                      retry_on_timeout=True,
                      db=0, password="alexaprize", decode_responses=True)

amazon_intent_map = {
    'LaunchRequestIntent': 'LAUNCHGREETING'
}

custom_intent_sys_map = {
    # System level intents
    'adjust_speak': 'ADJUSTSPEAK',
    'terminate': 'TERMINATE',
    'change_topic': 'SOCIAL',
    'clarify': 'CLARIFICATION',
    'say_comfort': 'SAY_COMFORT',
    'say_thanks': 'SAY_THANKS',
    'say_bio': 'SOCIAL',
    'req_more': 'REQ_MORE',
    'req_time': 'REQ_TIME',
    'req_playgame': 'PLAY_GAME',
    'req_playmusic': 'PLAY_MUSIC',
    'req_task': 'REQ_TASK',
    'req_loc': 'REQ_LOC'
}

custom_intent_topic_map = {
    # Modular Level Intent
    # topic_controversial
    'topic_phatic': 'PHATIC',
    'topic_controversial': 'CONTROVERSIALOPION',
    'topic_profanity': 'CONTROVERSIALOPION',
    'topic_backstory': 'SOCIAL',
    'topic_movietv': 'MOVIECHAT',
    'topic_newspolitics': 'NEWS',
    'topic_health': 'SAY_COMFORT',
    'topic_weather': 'WEATHER',
    'topic_qa': 'SOCIAL',
    # Note that this is just for temporary categorization setting to
    # rely on a single retrieval model;
    # Later, more customization is needed to design the dialog flow
    'topic_music': 'MUSICCHAT',
    'topic_book': 'BOOKCHAT',
    # 'topic_sport': 'RETRIEVAL', Uncomment it when you push back the topic_sport
    'topic_sport': 'SPORT',  # Changed by Mingyang Zhou
    'topic_entertainment': 'MOVIECHAT',
    'topic_holiday': 'HOLIDAYCHAT',
    'topic_recommendtedtalk': 'RECOMMENDATIONTCHAT',
    # 'topic_food': 'RETRIEVAL',
    'topic_food': 'FOODCHAT',
    'topic_techscience': 'TECHSCIENCECHAT',  # disable it until version ready
    'topic_psyphi': 'PSYPHICHAT',
    'topic_travel': 'TRAVELCHAT',
    'topic_finance': 'RETRIEVAL',
    'topic_history': 'RETRIEVAL',
    'topic_animal': 'ANIMALCHAT',
    'topic_game': 'GAMECHAT',
    'topic_storymeme': 'STORYFUNFACT',
    'topic_others': 'RETRIEVAL',
    'req_topic': 'SOCIAL',
    'topic_saycomfort': 'SAY_COMFORT',
    'say_funny': 'SAY_FUNNY'
}

concept_map = {
    "movie": "MOVIECHAT",
    "movies": "MOVIECHAT",
    "book": "BOOKCHAT",
    "singer": "MUSICCHAT",
    "band": "MUSICCHAT",
    "animals": "ANIMALCHAT",
    "animal": "ANIMALCHAT",
    "dog": "ANIMALCHAT",
    "bird": "ANIMALCHAT",
    "cat": "ANIMALCHAT",
    "sport": "SPORT",
    "insect": "ANIMALCHAT",
    "game": "GAMECHAT",
    "games": "GAMECHAT",
    "technology": "TECHSCIENCECHAT",
    "science": "TECHSCIENCECHAT",
    "european country": "TRAVELCHAT",
    "nation": "TRAVELCHAT",
    "asian country": "TRAVELCHAT",
    "food": "FOODCHAT"
}

google_knowledge_map = {
    "movie": "MOVIECHAT",
    "book": "BOOKCHAT",
    "game": "GAMECHAT",
    "sports": "SPORT",
    "music": "MUSICCHAT",
    "travel": "TRAVELCHAT",
    "news": "NEWS",
    "animals": "ANIMALCHAT",
    "food": "FOODCHAT"
}

TOPIC_MAP = {
    'Art_Event': '',
    'Business': 'NEWS',
    'Celebrities': '',
    'Drug': '',
    'Education': 'NEWS',
    'Entertainment': '',
    'Fashion': 'RETRIEVAL',
    'Food_Drink': 'FOODCHAT',
    'Games': '',
    'Health': '',
    'History': '',
    'Joke': 'SAY_FUNNY',
    'Literature': '',
    'Math': 'SOCIAL',
    'Movies_TV': '',
    'Music': 'MUSICCHAT',
    'News': 'NEWS',
    'Other': '',
    'Pets_Animals': 'ANIMALCHAT',  # TODO: put to pet animal
    'Phatic': '',
    'Politics': '',
    'Psychology': '',
    'Relationship': '',
    'Religion': '',  # TODO: add a religion module
    'SciTech': '',
    'Sex_Profanity': '',
    'Shopping': '',
    'Sports': '',
    'Travel_Geo': '',
    'Weather_Time': ''
}

dialogflow_modules = [
    "MOVIECHAT",
    "BOOKCHAT",
    "TECHSCIENCECHAT",
    "TRAVELCHAT",
    "GAMECHAT",
    "SPORT",
    "PSYPHICHAT",
    "MUSICCHAT",
    "HOLIDAYCHAT",
    "NEWS",
    "ANIMALCHAT",
    "WEATHER",
    "SAY_COMFORT",
    "SOCIAL",
    "RECOMMENDATIONTCHAT",
    "FOODCHAT",
]
pos_key = "pos"
neg_key = "neg"
neu_key = "neu"
other_key = "others"
# TODO: detect sentiment output
sentiment_attitude_map = {
    "pos": pos_key,
    "neg": neg_key,
    "neu": neu_key,
    "compound": pos_key,
    "unknown": other_key
}

#CompiledPtn = intentmap_scheme.CompiledPattern()


def get_retrieval_keys():
    return r.brpoplpush(RETRIEVAL_LIST_KEY, RETRIEVAL_LIST_KEY)


# def get_keyword_modules(
#         regex_topics,
#         knowledge_topics,
#         topic_classifier_topics,
#         concept):
#     logging.info(
#         "[Utils] get topics from regex: {}, knowlege: {}, topic classifier: {}".format(
#             regex_topics,
#             knowledge_topics,
#             topic_classifier_topics))
#     ret = []
#     logging.info(
#         "[Utils] get keyword module from regx: {}".format(regex_topics))
#     ret.extend([custom_intent_topic_map[topic] for topic in regex_topics])
#     if knowledge_topics is not None and knowledge_topics != '' and knowledge_topics in google_knowledge_map:
#         logging.info(
#             "[Utils] get keyword module from knowledge: {}".format(knowledge_topics))
#         ret.append(google_knowledge_map[knowledge_topics])
#     if concept is not None and len(concept) > 0:
#         logging.info(
#             "[Utils] get keyword module from concept: {}".format(concept))
#         for c in concept:
#             # NOTE: faster for now cuz we have a smaller map
#             for ck in concept_map.keys():
#                 if ck in c['data']:
#                     ret.append(concept_map[ck])
#                     continue
#     if topic_classifier_topics is not None and topic_classifier_topics not in [
#             '']:
#         logging.info("[Utils] get keyword module from topic: {}".format(
#             topic_classifier_topics))
#         ret.append(topic_classifier_topics)
#     logging.info("[Utils] return module: {}".format(ret))
#     return ret


# def is_common_name(name):
#     return r.sismember(COMMON_NAME_KEY, name.lower())


# def scan_related_holiday(keyword, threshold=70):
#     key = None
#     for holiday in r.zscan_iter(HOLIDAY_KEY):
#         if fuzz.token_sort_ratio(holiday[0], keyword) > threshold:
#             key = holiday[0]
#             break
#     if key is None:
#         return None
#     return r.hgetall(HOLIDAY_PREFIX_KEY + key)


# def get_future_holiday(category=None, limit=5):
#     # FIXME: safe way to make sure it include today
#     second_before_today = int(
#         (datetime.utcnow() - timedelta(hours=24 + 10)).strftime("%s")) - 1
#     keys = r.zrangebyscore(
#         HOLIDAY_KEY, second_before_today, 'inf', num=limit, start=0)
#     ret = []
#     for key in keys:
#         holiday_data = r.hgetall(HOLIDAY_PREFIX_KEY + key)
#         if category is None or (
#                 "Category" in holiday_data and category == holiday_data["Category"]):
#             ret.append(holiday_data)
#     return ret


# def get_all_holiday(category=None, limit=5):
#     # FIXME: safe way to make sure it include today
#     keys = r.zrangebyscore(HOLIDAY_KEY, 0, 'inf', num=limit, start=0)
#     ret = []
#     for key in keys:
#         holiday_data = r.hgetall(HOLIDAY_PREFIX_KEY + key)
#         if category is None or (
#                 "Category" in holiday_data and category == holiday_data["Category"]):
#             ret.append(holiday_data)
#     return ret


# def get_tedtalk_topics():
#     return [key[len(TEDTALK_TOPIC_PREFIX):]
#             for key in r.keys(TEDTALK_TOPIC_PREFIX + "*")]


# def get_tedtalk_content(category='robots', limit=3):
#     key = TEDTALK_TOPIC_PREFIX + category
#     if not r.exists(key):
#         logging.warning(
#             "[Utils] tedtalk topic category: {} not exist".format(category))
#         return None
#     titles = r.hkeys(key)
#     res = r.hmget(key, random.sample(titles, limit))
#     return [json.loads(r) for r in res]


# def get_tetalk_content_by_title(title, category='robots'):
#     key = TEDTALK_TOPIC_PREFIX + category
#     res = r.hget(key, title)
#     if res is None:
#         return None
#     return json.loads(res)


# def get_tedtalk_by_utt(utterance):
#     threshold = 0
#     # st = time()
#     try:
#         headers = {
#             'Content-Type': 'application/json',
#         }
#         data = {
#             'text': utterance
#         }

#         resp = requests.post(
#             'http://ec2-35-171-245-158.compute-1.amazonaws.com:8090/talk',
#             headers=headers,
#             data=json.dumps(data),
#             timeout=0.2)

#         ret = resp.json()
#         # end = time()
#         # print('Time {0}'.format(end - st))
#         # NOTE: http code
#         # print(resp.status_code)
#         if resp.status_code < 300:
#             # TODO: handle low confidence score
#             if ret['confidence'] > threshold:
#                 return ret
#             else:
#                 return None

#     except Exception as e:
#         logging.error(
#             '[DEBATE] post sentence embedding with data err: {}'.format(e), exc_info=True)
#         return None


# def get_compiled_pattern():
#     return CompiledPtn


# def get_538_significant_digits(date=None):
#     # date can be 2018-06-21 or none
#     if date is None:
#         key = FIVETHREEEIGHT_KEY_PREFIX + 'latest'
#     else:
#         key = FIVETHREEEIGHT_KEY_PREFIX + date
#     logging.info(
#         "[Utils] Get 538 significant digits with redis key {}".format(key))
#     if r.exists(key) is False:
#         logging.warning(
#             "[Utils] 538 significnat digits redis key {} not found".format(key))
#         return None

#     resp = r.hgetall(key)
#     if resp is None or resp == '':
#         return None
#     return resp


# def get_randomthoughts_by_key(keyword):
#     logging.info(
#         "[Utils] Get random thoughts with input word: {} and redis key {}".format(
#             keyword, RANDOM_THOUGHT_KEY))
#     resp = r.hget(RANDOM_THOUGHT_KEY, keyword.lower().strip())
#     if resp is None or resp == '':
#         return None
#     return resp


# def get_topic_class(topic_struct):
#     if topic_struct is None or len(topic_struct) < 1:
#         return ''
#     if 'topicClass' in topic_struct[0]:
#         return topic_struct[0]['topicClass']
#     return ''


# def get_topic(topic_struct, threshold=0.4):
#     logging.info(
#         "[Utils] Get topic analyzer output: {}, threshold: {}".format(
#             topic_struct, threshold))
#     if topic_struct is None or len(topic_struct) < 1:
#         return '', []
#     topic_class = get_topic_class(topic_struct)
#     if topic_struct[0]['confidence'] < threshold:
#         return '', []
#     if 'topicKeywords' in topic_struct[0]:
#         topic_keywords = topic_struct[0]['topicKeywords']
#         if topic_keywords:
#             if topic_keywords[0]['confidence'] < threshold:
#                 return '', []
#     return TOPIC_MAP[topic_class], topic_keywords


# def get_feature_by_key(state, key):
#     logging.debug(
#         "[Utils] Get feature with key: {}".format(key, state))
#     features_dict = json.loads(state.to_json())["features"]
#     return features_dict.get(
#         key, None) if features_dict[key] is not None else None


# def get_sentiment(state, lexical):
#     # TODO: define specific sentiment
#     senti = get_feature_by_key(state, 'sentiment')
#     if senti is "pos" or senti is "neg":
#         return senti
#     if senti is "compound":
#         return "pos"
#     if 'ans_like' in lexical or 'ans_pos' in lexical:
#         return 'pos'
#     if 'ans_neg' in lexical:
#         return 'neg'
#     return "unknown"


# def get_sentiment_key(sentiment, lexical):
#     # TODO: fine tune
#     logging.info(
#         "[Utils] Get sentiment key with sentiment: {}, lexical: {}".format(
#             sentiment, lexical))
#     if 'ans_like' in lexical or 'ans_pos' in lexical:
#         return sentiment_attitude_map["pos"]
#     if 'ans_neg' in lexical:
#         return sentiment_attitude_map["neg"]
#     if 'ans_unknown' in lexical:
#         return sentiment_attitude_map["unknown"]

#     if sentiment in sentiment_attitude_map:
#         return sentiment_attitude_map[sentiment]
#     return other_key

#     # return sentiment_attitude_map[sentiment] -- will give TypeError:
#     # unhashable type: 'dict'


# def unique(sequence):
#     seen = set()
#     return [x for x in sequence if not (x in seen or seen.add(x))]


# def check_intersect(a, b):
#     a_set = set(a)
#     b_set = set(b)
#     if (a_set & b_set):
#         return True
#     else:
#         return False


# # TODO: deprecated
# def get_poptopics():
#     now_ts = int(datetime.now().timestamp())
#     lastweek_ts = now_ts - 604800  # 1 week in seconds
#     hot_topics = []
#     try:
#         hot_topics = r.zrangebyscore("gunrock:topic:pop", lastweek_ts, now_ts)
#     except Exception as e:
#         logging.error(
#             "[Utils] Err connect to redis with zrange key gunrock:topic:pop".format(e), exc_info=True)
#         hot_topics = ["nba", "nba playoff", "royal wedding"]
#     return hot_topics


# def get_coreference_topics(c_knowledge):
#     c_knowledge_list = []
#     try:
#         logging.info(
#             "[Utils] get coref topics: {}".format(c_knowledge))
#         for kg in c_knowledge:
#             if kg:
#                 logging.info(
#                     "[Utils] get coref by found kg in ckg: {}".format(kg))
#                 if kg[-1] != "__EMPTY__" and isinstance(kg[-1], str):
#                     c_knowledge_list.append(google_knowledge_map[kg[-1]])
#                 elif kg[0][-1] != "__EMPTY__":
#                     c_knowledge_list.append(google_knowledge_map[kg[0][-1]])
#         return c_knowledge_list
#     except Exception as e:
#         logging.error(
#             "[Utils] get coref topics error: ".format(e), exc_info=True)
#         return []


# def topic_detector(
#         custom_intents,
#         knowledge_topics,
#         topic_classifier_topics,
#         concept):
#     topic_list = []
#     # regular expression
#     if "topic_weather" in custom_intents['topic']:
#         topic_list.append(custom_intent_topic_map['topic_weather'])
#     if "topic_holiday" in custom_intents['topic']:
#         topic_list.append(custom_intent_topic_map['topic_holiday'])
#     if "topic_game" in custom_intents['topic']:
#         topic_list.append(custom_intent_topic_map['topic_game'])
#     if "topic_movietv" in custom_intents['topic']:
#         topic_list.append(custom_intent_topic_map['topic_movietv'])
#     if "topic_book" in custom_intents['topic']:
#         topic_list.append(custom_intent_topic_map['topic_book'])
#     if "topic_newspolitics" in custom_intents['topic']:
#         topic_list.append(custom_intent_topic_map['topic_newspolitics'])
#     if "topic_techscience" in custom_intents['topic']:
#         topic_list.append(custom_intent_topic_map['topic_techscience'])
#     if "topic_music" in custom_intents['topic']:
#         topic_list.append(custom_intent_topic_map['topic_music'])
#     if "topic_sport" in custom_intents['topic']:
#         topic_list.append(custom_intent_topic_map['topic_sport'])
#     if "topic_animal" in custom_intents['topic']:
#         topic_list.append(custom_intent_topic_map['topic_animal'])
#     if "topic_psyphi" in custom_intents['topic']:
#         topic_list.append(custom_intent_topic_map['topic_psyphi'])
#     if "topic_travel" in custom_intents['topic']:
#         topic_list.append(custom_intent_topic_map['topic_travel'])
#     if "topic_recommendtedtalk" in custom_intents['topic']:
#         topic_list.append(custom_intent_topic_map['topic_recommendtedtalk'])
#     if "topic_food" in custom_intents['topic']:
#         topic_list.append(custom_intent_topic_map['topic_food'])

#     # Google knowledge
#     if knowledge_topics is not None and knowledge_topics != '' and knowledge_topics in google_knowledge_map:
#         topic_list.append(google_knowledge_map[knowledge_topics])

#     # Concept Net
#     if concept is not None and len(concept) > 0:
#         for c in concept:
#             # NOTE: faster for now cuz we have a smaller map
#             for ck in concept_map.keys():
#                 if ck in c['data']:
#                     topic_list.append(concept_map[ck])
#                     continue

#     if "say_funny" in custom_intents['topic']:
#         topic_list.append(custom_intent_topic_map['say_funny'])
#     if "topic_storymeme" in custom_intents['topic']:
#         topic_list.append(custom_intent_topic_map['topic_storymeme'])
#     if "topic_entertainment" in custom_intents['topic']:
#         topic_list.append(custom_intent_topic_map['topic_entertainment'])

#     # Amazon topic analyzer
#     if topic_classifier_topics is not None and topic_classifier_topics not in ['']:
#         topic_list.append(topic_classifier_topics)

#     return topic_list


# def special_match(curr_utt, module_list, last_module):
#     if re.search(r"\bcurry\b", curr_utt) and last_module == 'SPORT':
#         return last_module
#     elif module_list and module_list[0] == 'WEATHER' and last_module == 'NEWS':
#         return 'WEATHER'
#     return False


# def read_googlekg(knowledge_list):
#     knowledge = None
#     if knowledge_list is not None and len(knowledge_list) != 0 \
#             and len(knowledge_list[0]) == 4:
#         if float(knowledge_list[0][2]) > 500:
#             knowledge = knowledge_list[0][3]
#     return knowledge


# def extract_central_intent(nlu):
#     # first level sentences
#     first = []
#     for index, elem in enumerate(nlu):
#         if elem['senti'] != 'neg' and (elem['DA'] in ['commands', 'open_question', 'yes_no_question'] or
#                                        "req_topic_jump" in elem['regex']['sys']) and elem['module']:
#             first.append(elem)

#     # second level
#     second = []
#     for index, elem in enumerate(nlu):
#         if elem['DA'] in ['commands', 'open_question', 'yes_no_question'] and index not in first:
#             second.append(elem)

#     # third level
#     third = []
#     for index, elem in enumerate(nlu):
#         if (elem['module'] or check_intersect(['topic_backstory', 'topic_qa'], elem['regex']['topic'])) \
#                 and elem['senti'] != 'neg' and index not in first + second:
#             third.append(elem)

#     if first:
#         return first[-1]
#     if second:
#         return second[-1]
#     if third:
#         return third[-1]
#     return nlu[-1]


# def read_returnnlp(returnnlp):
#     logging.info('[UTILS][NLP] return nlp: {}'.format(returnnlp))
#     if not returnnlp:
#         logging.info('[UTILS][NLP] return nlp is none')
#         return [{'senti': None,
#                 'regex': {'sys': [],
#                           'topic': [],
#                           'lexical': []},
#                 'DA': None,
#                 'module': [],
#                 'np': [],
#                 'text': None
#                 }]
#     nlu = []
#     for sentence in returnnlp:
#         conf = 0
#         sentiment = other_key
#         for tag in sentence['sentiment']:
#             if float(sentence['sentiment'][tag]) > conf:
#                 conf = float(sentence['sentiment'][tag])
#                 sentiment = tag
#         senti = get_sentiment_key(sentiment, sentence['intent']['lexical'])
#         t_module = ''
#         t_keywords = []
#         if sentence.get('topic') is not None:
#             t_module, t_keywords = get_topic(
#                 [sentence.get('topic')], threshold=0.8)
#         elem = {
#             'senti': senti,
#             'regex': sentence.get('intent'),
#             'DA': sentence.get('dialog_act')[0] if sentence.get('dialog_act') else None,
#             'module': topic_detector(sentence.get('intent'), read_googlekg(sentence.get('googlekg')),
#                                      t_module, sentence.get('concept')),
#             'np': sentence.get('noun_phrase'),
#             'text': sentence.get('text')
#         }
#         nlu.append(elem)
#     logging.info('[UTILS][NLP] get nlu: {}'.format(nlu))
#     return nlu


# def getCustomModules(
#         curr_utt,
#         returnnlp,
#         amazon_intents,
#         user_attributes,
#         current_state
# ):
#     logging.info('[UTILS][GETMODULE] get custom modules...')
#     last_module = user_attributes.last_module
#     logging.info('[UTILS][GETMODULE] last module: {}'.format(last_module))

#     nlu = read_returnnlp(returnnlp)
#     central_elem = extract_central_intent(nlu)

#     # UPDATE MODULE RANK
#     cur_modules = []
#     for elem in nlu:
#         if elem['senti'] != 'neg':
#             cur_modules += elem['module']
#     cur_modules = unique(cur_modules)
#     if user_attributes.module_rank is not None:
#         for module in user_attributes.module_rank:
#             if module in cur_modules:
#                 user_attributes.module_rank[module] += 1
#             else:
#                 user_attributes.module_rank[module] = user_attributes.module_rank[module]*0.9

#     senti = central_elem['senti']
#     topic_module_list = central_elem['module']
#     DA = central_elem['DA']
#     custom_intents = central_elem['regex']
#     np = central_elem['np']
#     # add backstory confidence in central elem
#     backstory = get_backstory_response_with_score(central_elem['text'], user_attributes)
#     central_elem['backstory'] = backstory

#     # add central elem into current state
#     current_state.features["central_elem"] = central_elem

#     # bot = [act[0] for act in dialog_act[-1].get('response_da')]
#     # user = [act.get('DA') for act in dialog_act[:-1]]

#     # check whether there is default Amazon intents
#     module_names = [amazon_intent_map[intent]
#                     for intent in amazon_intent_map.keys() if intent in amazon_intents]
#     logging.info(
#         '[UTILS][GETMODULE] get amazon module: {}'.format(module_names))

#     logging.info(
#         '[UTILS][GETMODULE] get keyword module: {}'.format(topic_module_list))
#     if WORKING_ENV == "local":
#         print("[DEBUG] central sentence: {}".format(central_elem['text']))
#         print("[DEBUG] topic module list: {}".format(topic_module_list))
#         print("[DEBUG] dialog act: {}".format(DA))
#         print("[DEBUG] senti: {}".format(senti))
#         from pprint import pprint
#         print("[DEBUG] bacstory:")
#         pprint(backstory, width=120)
#         print("[DEBUG] module rank:")
#         pprint(user_attributes.module_rank, width=120)
#         # setattr(user_attributes, "usr_name", None)

#     # proceed if no default Amazon intents
#     if len(module_names) == 0:
#         logging.info("[Utils] no amazon module name found")

#         if "topic_profanity" in custom_intents['topic']:
#             logging.info("[SS Rule]: sensitive words")
#             module_names.append(custom_intent_topic_map['topic_controversial'])
#             update_propose_continue(user_attributes)
#         # DISABLE BEFORE THE FINAL
#         elif "terminate" in custom_intents['sys']:
#             logging.info("[SS Rule]: terminate chat")
#             module_names.append(custom_intent_sys_map['terminate'])
#         elif "req_play_game" in custom_intents['sys'] and "GAMECHAT" in topic_module_list and len(curr_utt.split()) < 10:
#             logging.info("[SS Rule]: request play game")
#             module_names.append(custom_intent_sys_map['req_playgame'])
#             update_propose_continue(user_attributes)
#         elif "req_play_music" in custom_intents['sys'] and 'MUSICCHAT' in topic_module_list and len(curr_utt.split()) < 10:
#             logging.info("[SS Rule]: request play music")
#             module_names.append(custom_intent_sys_map['req_playmusic'])
#             update_propose_continue(user_attributes)
#         elif "req_task" in custom_intents['sys'] and not topic_module_list and len(curr_utt.split()) < 10:
#             logging.info("[SS Rule]: request other task")
#             module_names.append(custom_intent_sys_map['req_task'])
#             update_propose_continue(user_attributes)

#         elif special_match(curr_utt, topic_module_list, last_module):
#             logging.info("[SS Rule]: special logic for edge cases")
#             module_names.append(special_match(curr_utt, topic_module_list, last_module))

#         # jump intent
#         elif "change_topic" in custom_intents['sys']:
#             logging.info("[SS Rule]: change topic")
#             if len(topic_module_list) == 0:
#                 module_names.append('SOCIAL')
#             else:
#                 module_names.append(topic_module_list[0])
#                 if topic_module_list[0] != last_module:
#                     update_propose_continue(user_attributes)
#         elif "select_wrong" in custom_intents['sys']:
#             logging.info("[SS Rule]: select wrong modules")
#             module_names.append('SOCIAL')
#         # QUICK FIX FOR QUESTION
#         elif (DA in ["commands", "open_question", "yes_no_question"] or "req_topic_jump" in custom_intents['sys'] or
#               re.search(r"^(whats|what is|what are)", curr_utt)) and topic_module_list:
#             logging.info("[SS Rule]: jump to specific topics")
#             if last_module in topic_module_list:
#                 module_names.append(last_module)
#             else:
#                 module_names.append(topic_module_list[0])
#                 update_propose_continue(user_attributes)
#         elif "req_topic_jump_single" in custom_intents['sys'] and topic_module_list and len(
#                 curr_utt.split()) < 3 and last_module not in ["NEWS", "PSYPHICHAT", "RECOMMENDATIONTCHAT"]:
#             logging.info(
#                 "[SS Rule]: jump to specific topics by single word requests")
#             if curr_utt.split()[-1] == 'news':
#                 module_names.append('NEWS')
#             elif last_module in topic_module_list:
#                 module_names.append(last_module)
#             else:
#                 module_names.append(topic_module_list[0])
#                 update_propose_continue(user_attributes)

#         # one turn service
#         elif "adjust_speak" in custom_intents['sys'] and not topic_module_list:
#             logging.info("[SS Rule]: complain speak style")
#             module_names.append(custom_intent_sys_map['adjust_speak'])
#             update_propose_continue(user_attributes)
#         elif "clarify" in custom_intents['sys'] and not topic_module_list and \
#                 last_module != custom_intent_sys_map['clarify']:
#             logging.info("[SS Rule]: ask clarity")
#             module_names.append(custom_intent_sys_map['clarify'])
#         elif "say_bio" in custom_intents['sys'] and senti != "neg":
#             logging.info("[SS Rule]: self intro")
#             module_names.append('SOCIAL')
#         elif "req_location" in custom_intents['sys']:
#             logging.info("[SS Rule]: request location information")
#             module_names.append(custom_intent_sys_map['req_location'])
#             update_propose_continue(user_attributes)
#         elif "req_time" in custom_intents['sys']:
#             logging.info("[SS Rule]: request for time")
#             module_names.append(custom_intent_sys_map['req_time'])
#             update_propose_continue(user_attributes)
#         # elif ('topic_controversial' in custom_intents['topic']) \
#         #         and check_intersect(['ask_opinion', 'ask_preference', 'ask_yesno', 'ask_advice', 'ask_ability'],
#         #                             custom_intents['lexical']):
#         #     logging.info("[SS Rule]: controversial topic intent")
#         #     module_names.append(custom_intent_topic_map['topic_controversial'])
#         #     update_propose_continue(user_attributes)

#         # <--- Special Opening Logic ---> #
#         elif user_attributes.launchgreeting is not None and user_attributes.launchgreeting.get(
#                 "propose_continue") != 'STOP':
#             logging.info("[SS Rule]: stay in launch greeting")
#             module_names.append("LAUNCHGREETING")
#         elif "info_name" in custom_intents['lexical']:
#             logging.info("[SS Rule]: name correction")
#             module_names.append("LAUNCHGREETING")

#         # <--- Special Last Module Logic Rules ---> #
#         elif "req_more" in custom_intents['sys'] and last_module in ['STORYFUNFACT', 'SAY_FUNNY']:
#             logging.info(
#                 "[SS Rule]: special rule for giving more jokes/stories")
#             module_names.append(last_module)
#         elif user_attributes.previous_modules[1] in dialogflow_modules and last_module in ['STORYFUNFACT', 'SAY_FUNNY', custom_intent_topic_map['topic_controversial']]:
#             logging.info(
#                 "[SS Rule]: grounding for specific jumpout")
#             user_attributes.socialchat['curr_state'] = 's_40'
#             module_names.append('SOCIAL')
#         elif select_propose_continue(user_attributes):
#             logging.info("[SS Rule]: select modules propose to continue")
#             module_names.append(select_propose_continue(user_attributes))

#         # <--- Topic Level Rule Base ---> #
#         elif len(topic_module_list) > 0 and senti != 'neg':
#             if last_module in topic_module_list:
#                 module_names.append(last_module)
#             else:
#                 module_names.append(topic_module_list[0])
#                 update_propose_continue(user_attributes)
#         elif "topic_phatic" in custom_intents['topic']:
#             logging.info("[SS Rule]: phatic topic")
#             module_names.append(custom_intent_topic_map['topic_phatic'])
#             update_propose_continue(user_attributes)
#         elif "topic_saycomfort" in custom_intents['topic']:
#             logging.info("[SS Rule]: say_comfort without sentiment")
#             module_names.append(custom_intent_topic_map['topic_saycomfort'])
#         elif backstory['confidence'] > 0.8:
#             logging.info("[SS Rule]: high conf goes to backstory")
#             module_names.append('SOCIAL')
#             user_attributes.socialchat['curr_state'] = 's_00'
#         elif DA in ["commands", "open_question", "yes_no_question"] and not topic_module_list and not re.search(
#             r"\b(it|that|this|those|one|them|him|his|her|their|he|what|you|she|our|us|another)\b", curr_utt) and \
#                 len(curr_utt.split()) > 3 and len(np) > 0:
#             module_names.append("RETRIEVAL")
#             update_propose_continue(user_attributes)
#         elif DA in ["open_question", "yes_no_question"] or check_intersect(['topic_backstory', 'topic_qa'], custom_intents['topic']):
#             logging.info("[SS Rule]: questions to social at the end")
#             module_names.append('SOCIAL')
#         # elif last_module == 'LAUNCHGREETING' and senti == 'neg':
#         #     logging.info(
#         #         "[SS Rule]: none of modules in launch greeting interest user")
#         #     template_manager.get_new_module(user_attributes, 3)
#         #     module_names.append('SOCIAL')
#         elif check_intersect(["topic_others", "topic_finance"], custom_intents['topic']):
#             logging.info("[SS Rule]: topics cannot be handled go to retrieval")
#             module_names.append('RETRIEVAL')
#         elif "req_topic" in custom_intents['sys']:
#             logging.info("[SS Rule]: provide a topic for the user")
#             module_names.append('SOCIAL')

#         # <--- Lexical Level Rule Base ---> #
#         # TODO: replace this part using lexical intent to design error handling
#         # templates
#         elif check_intersect(["ask_hobby", 'ask_yesno', 'ask_ability'], custom_intents['lexical']):
#             logging.info("[SS Rule]: ask random opinion stuff intent")
#             module_names.append('SOCIAL')
#             user_attributes.socialchat['curr_state'] = 's_00'  # backstory
#         elif check_intersect(["ask_fact", "ask_dist", "ask_loc", "ask_degree",
#                               "ask_freq", "ask_count", "ask_time", "ask_person", "ask_advice"],
#                              custom_intents['lexical']):
#             logging.info("[SS Rule]: ask random factual stuff intent")
#             module_names.append('SOCIAL')  # EVI
#             user_attributes.socialchat['curr_state'] = 's_10'
#         elif DA in ["thanking", "appreciation"]:
#             logging.info("[SS Rule]: say thank you")
#             module_names.append(custom_intent_sys_map['say_thanks'])

#         # topic proposed by social lower than topic classifier
#         elif user_attributes.propose_topic is not None and senti != 'neg':
#             logging.info("[SS Rule]: select the proposed modules")
#             module_names.append(user_attributes.propose_topic)
#         elif last_module is not None and last_module in dialogflow_modules and senti != "neg" \
#                 and drop_propose_stop(user_attributes, last_module):
#             logging.info(
#                 "[SS Rule]: special dialog flow priority policy to last module: {}".format(last_module))
#             module_names.append(last_module)
#         elif last_module in ['PSYPHICHAT'] and drop_propose_stop(user_attributes, last_module):
#             logging.info(
#                 "[SS Rule]: special dialog flow priority policy without sentiment for module: {}".format(last_module))
#             module_names.append(last_module)

#         elif last_module in ['CHANGETOPIC', 'LAUNCHGREETING', 'SOCIAL', 'PSYPHICHAT', 'SPORT', 'BOOKCHAT', 'MOVIECHAT',
#                              'TECHSCIENCECHAT', 'ANIMALCHAT', 'HOLIDAYCHAT', 'MUSICCHAT', 'NEWS', 'GAMECHAT',
#                              'TRAVELCHAT', 'RETRIEVAL'] and senti in ["neg"]:
#             logging.info(
#                 "[SS Rule]: negtive feedback for propose, go back to Social")
#             module_names.append("SOCIAL")
#         else:
#             logging.info("[SS Rule]: uncertain")
#             module_names.append('RETRIEVAL')

#     if not module_names:
#         module_names.append('RETRIEVAL')
#     # Some necessary post processing
#     module_names = unique(module_names)
#     check_propose_continue(user_attributes, module_names[0])
#     setattr(user_attributes, "propose_topic", None)
#     if WORKING_ENV == "local":
#         from pprint import pprint
#         print("[DEBUG] module names: {}".format(module_names))
#         print("[DEBUG] user attribute:")
#         pprint(user_attributes.serialize_to_json(), width=180)
#     template_manager.add_discussed_module(module_names[0], user_attributes)
#     setattr(user_attributes, 'last_module', module_names[0])
#     # update previous three modules
#     if user_attributes.previous_modules is None:
#         setattr(user_attributes, 'previous_modules', [
#                 '__EMPTY__', '__EMPTY__', '__EMPTY__'])
#     user_attributes.previous_modules.pop()
#     user_attributes.previous_modules.insert(0, module_names[0])

#     # remove certain modules from propose list:
#     if module_names[0] in ['PSYPHICHAT']:
#         user_attributes.module_rank[module_names[0]] = -1

#     return module_names


# def concatenate_propose(
#         user_attributes,
#         selected_module,
#         final_response,
#         ab_setting):
#     # NOTE: special handling evi profanity check
#     if EVI_PROFANITY_RESPONSE in final_response:
#         final_response = 'I\'d rather not answer that.'
#         module, utterance = template_manager.get_new_module(user_attributes)
#         final_response += template_manager.Templates.social.utterance(
#             selector='switch_topic', slots={
#                 'topic': utterance}, user_attributes_ref=user_attributes)
#         setattr(user_attributes, 'propose_topic', module)
#         return final_response

#     if selected_module in [
#             'PSYPHICHAT',
#             'SPORT',
#             'BOOKCHAT',
#             'MOVIECHAT',
#             'GAMECHAT',
#             'TRAVELCHAT',
#             'TECHSCIENCECHAT',
#             'ANIMALCHAT',
#             'HOLIDAYCHAT',
#             'MUSICCHAT',
#             'NEWS',
#             'FOODCHAT',
#     ] and not drop_propose_stop(
#             user_attributes,
#             selected_module):
#         if user_attributes.propose_topic is None:
#             module, tag = template_manager.get_new_module(user_attributes)
#             setattr(user_attributes, "propose_topic", module)
#             if tag != 'EMPTY':
#                 # try to get specific transition
#                 transition = template_transition.utterance(selector='transition_56/{}/{}'.format(
#                         selected_module, module), slots={}, user_attributes_ref=user_attributes)
#                 if transition:
#                     final_response += " " + transition
#                 else:
#                     topic_specific_key = 'topic_specific'
#                     if ab_setting is not None and ab_setting != 'a':
#                         topic_specific_key = topic_specific_key + '_' + ab_setting
#                     final_response += " " + template_social.utterance(selector='{}/{}'.format(
#                         topic_specific_key, module), slots={}, user_attributes_ref=user_attributes)
#             else:
#                 final_response += " What would you like to talk about next?"

#     elif selected_module == 'RETRIEVAL' and user_attributes.retrieval_propose is True:
#         module, tag = template_manager.get_new_module(user_attributes)
#         setattr(user_attributes, "propose_topic", module)
#         if tag != 'EMPTY':
#             final_response += " " + template_social.utterance(
#                 selector='topic_specific_c/{}'.format(module),
#                 slots={},
#                 user_attributes_ref=user_attributes)
#         else:
#             final_response += " What would you like to talk about next?"
#     return final_response


# def build_module_states_map(user_attributes):
#     module_states_map = {
#         'MOVIECHAT': user_attributes.moviechat_user,
#         'BOOKCHAT': user_attributes.bookchat_user,
#         'TRAVELCHAT': user_attributes.travelchat,
#         'GAMECHAT': user_attributes.gamechat,
#         'NEWS': user_attributes.news_user,
#         'ANIMALCHAT': user_attributes.animalchat,
#         'MUSICCHAT': user_attributes.musicchat,
#         'SOCIAL': user_attributes.socialchat,
#         'WEATHER': user_attributes.weatherchat,
#         'REQ_TIME': user_attributes.timechat,
#         'HOLIDAYCHAT': user_attributes.holidaychat,
#         'SAY_COMFORT': user_attributes.comfortchat,
#         'PSYPHICHAT': user_attributes.psyphichat,
#         'SPORT': user_attributes.sportchat,
#         'TECHSCIENCECHAT': user_attributes.techsciencechat,
#         'RECOMMENDATIONTCHAT': user_attributes.tedtalkchat,
#         'FOODCHAT': user_attributes.foodchat,
#         'LAUNCHGREETING': user_attributes.launchgreeting
#     }
#     return module_states_map


# module_state_map = {
#     'MOVIECHAT': 'moviechat_user',
#     'BOOKCHAT': 'bookchat_user',
#     'TRAVELCHAT': 'travelchat',
#     'GAMECHAT': 'gamechat',
#     'NEWS': 'news_user',
#     'ANIMALCHAT': 'animalchat',
#     'MUSICCHAT': 'musicchat',
#     'SOCIAL': 'socialchat',
#     'WEATHER': 'weatherchat',
#     'HOLIDAYCHAT': 'holidaychat',
#     'REQ_TIME': 'timechat',
#     'SAY_COMFORT': 'comfortchat',
#     'PSYPHICHAT': 'psyphichat',
#     'SPORT': 'sportchat',
#     'TECHSCIENCECHAT': 'techsciencechat',
#     'RECOMMENDATIONTCHAT': 'tedtalkchat',
#     'FOODCHAT': 'foodchat',
#     'LAUNCHGREETING': 'launchgreeting'
# }


# def update_propose_continue(user_attributes):
#     module_states_map = build_module_states_map(user_attributes)
#     for module in module_states_map:
#         if module_states_map[module] is not None:
#             module_states_map[module]['propose_continue'] = 'STOP'
#         else:
#             setattr(user_attributes, module_state_map[module], {
#                     'propose_continue': 'STOP'})
#     user_attributes.socialchat['curr_state'] = 's_init'


# def select_propose_continue(user_attributes):
#     module_states_map = build_module_states_map(user_attributes)
#     for module in module_states_map:
#         if module_states_map[module] is not None:
#             if module_states_map[module].get('propose_continue') == 'CONTINUE':
#                 logging.info(
#                     "[Utils] selected module: {}, continue".format(module))
#                 return module
#     return None


# def drop_propose_stop(user_attributes, last_module):
#     module_states_map = build_module_states_map(user_attributes)
#     if module_states_map.get(last_module) is not None:
#         if module_states_map.get(last_module).get(
#                 "propose_continue") != "STOP":
#             return True
#         else:
#             return False
#     return True


# def check_propose_continue(user_attributes, selected_module):
#     module_states_map = build_module_states_map(user_attributes)
#     if selected_module in module_states_map:
#         for module in module_states_map:
#             if module != selected_module and module_states_map[module] is not None:
#                 module_states_map[module]['propose_continue'] = 'STOP'


# class intentClassifier:
#     """intent classifier is used to compile and match the input string"""
#     input = ""

#     def __init__(self, input):
#         self.input = input.lower()

#     def preprocess(self, user_attributes_ref):
#         key = ['preprocess']
#         # TODO: template manager
#         if self.checkIntent("topic_profanity"):
#             logging.info(
#                 '[Utils] ASR preprocess with profane results: {}'.format(
#                     self.input))
#             presps = [
#                 "<say-as interpret-as=\"interjection\">jiminy cricket </say-as><break time=\"200ms\"></break>I'd rather talk about something else",
#                 "<say-as interpret-as=\"interjection\">oh boy </say-as><break time=\"150ms\"></break>This is kinda making me uncomfortable.<break time=\"200ms\"></break>I'd rather talk about something else."]
#             return random.choice(presps)
#         if self.checkIntent("hesitate"):
#             key.append('hesitate')
#             return template.utterance(selector=key, slots={},
#                                       user_attributes_ref=None)
#         if self.checkIntent("complaint"):
#             key.append('complaint')
#             # TODO: move to constant
#             if re.search(
#                 r"\bstop\b|\bexit\b|\bquit\b|\bend\b|\bover\b|i'm done|exit social|out of social|leave social",
#                     self.input):
#                 key.append('stop')
#             elif re.search(r"misunderstood|not listening|are you.* listening|quiet|shut your mouth|"
#                            r"i'm done|you.* deaf|repeatitous", self.input):
#                 key.append('listen_to_me')
#             elif re.search(r"i give up|come on|shut.* up|shut.* off|(?<!government).shut.* down|done listening|"
#                            r"done talking|that.* enough", self.input):
#                 key.append('shut_up')
#             elif re.search(r"terrible conversationalist|you.* retarded|you don't know|you.* more work|forget.* it|"
#                            r"you.* forgetful|already said|creepy|you.* disappointing|socially awkward|go to hell|"
#                            r"kick your ass|mentioned|no .* sense|not.* answer|doesn't .* sense", self.input):
#                 key.append('terrible')
#             elif re.search(r"are you sick|^stupid|hate you|angry.* you|you.* hoe|you.* trash|you.* crap|you.* garbage|"
#                            r"you're a loser|you.* filthy|what the hell|what the heck|piss|offended me|offensive"
#                            r"what.*wrong with you", self.input):
#                 key.append('hate_you')
#             elif re.search(r"my name wrong", self.input):
#                 key.append('misspell_name')
#             else:
#                 key.append('default')
#             return template.utterance(selector=key, slots={},
#                                       user_attributes_ref=user_attributes_ref)
#         # if self.checkIntent("not_complete"):
#         #     key.append('not_complete')
#         #     # TODO: move to constant
#         #     return template_manager.get_utterance(template, selector=key, slots={},
#         #                                          user_attributes_ref=None)

#     def getIntents(self):
#         """ Detect and categorize intents - rule based """
#         intent_output = {'sys': [], 'topic': [], 'lexical': []}
#         for intent in intentmap_scheme.sys_re_patterns.keys():
#             if CompiledPtn.get_sys_regex(intent).search(self.input):
#                 intent_output['sys'].append(intent)
#         for intent in intentmap_scheme.topic_re_patterns.keys():
#             if CompiledPtn.get_topic_regex(intent).search(self.input):
#                 intent_output['topic'].append(intent)
#         for intent in intentmap_scheme.lexical_re_patterns.keys():
#             if CompiledPtn.get_lexical_regex(intent).search(self.input):
#                 intent_output['lexical'].append(intent)
#         return intent_output

#     def checkIntent(self, intent=""):
#         'check the input text with respect to the given intent'
#         if intent in intentmap_scheme.sys_re_patterns.keys():
#             return CompiledPtn.get_sys_regex(intent).search(self.input)
#         if intent in intentmap_scheme.topic_re_patterns.keys():
#             return CompiledPtn.get_topic_regex(intent).search(self.input)
#         if intent in intentmap_scheme.lexical_re_patterns.keys():
#             return CompiledPtn.get_lexical_regex(intent).search(self.input)


def set_logger_level(logging):
    working_environment = get_working_environment()
    if working_environment == "local":
        logging.getLogger().setLevel(logging.DEBUG)
    else:
        logging.getLogger().setLevel(logging.INFO)

def get_working_environment():
    if os.environ.get('STAGE') == 'PROD' or os.environ.get('STAGE') == 'BETA':
        return os.environ.get('STAGE').lower()
    else:
        return "local"


def set_redis_with_expire(prefix, input_str, output, expire=24 * 60 * 60):
    logging.info(
        '[REDIS] set redis with expire input: {} output: {}, expire {}'.format(
            input_str, output, expire))
    hinput = hashlib.md5(input_str.encode()).hexdigest()
    return r.set(prefix + ':' + hinput, json.dumps(output), nx=True, ex=expire)


def get_redis(prefix, input_str):
    logging.info('[REDIS] get redis  input: {}'.format(input_str))
    hinput = hashlib.md5(input_str.encode()).hexdigest()
    results = r.get(prefix + ':' + hinput)
    if results is None:
        return None
    return json.loads(results)


def retrieve_reddit(utterance, subreddits, limit):
    # cuurently using gt's client info
    try:
        reddit = praw.Reddit(client_id=REDDIT_CLIENT_ID,
                             client_secret=REDDIT_CLIENT_SECRET,
                             user_agent='extractor',
                             timeout=2
                             # username='gunrock_cobot', # not necessary for read-only
                             # password='ucdavis123'
                             )
        subreddits = '+'.join(subreddits)
        subreddit = reddit.subreddit(subreddits)
        # higher limit is slower; delete limit arg for top 100 results
        submissions = list(subreddit.search(utterance, limit=limit))
        if len(submissions) > 0:
            return submissions
        return None
    except Exception as e:
        logging.error("Fail to retrieve reddit. error msg: {}. user utterance: {}".format(e, utterance))
        return None


def get_interesting_reddit_posts(text, limit):
    SENSITIVE_WORDS_LIST = ['kill', 'murder', 'sex', 'porn',
                            'bitch', 'dead', 'torture', 'die', 'sucks', 'damn', 'death']
    submissions = retrieve_reddit(text, ['todayilearned'], limit)
    if submissions is not None:
        results = [submissions[i].title.replace(
            "TIL", "").lstrip() for i in range(len(submissions))]
        results = [results[i] + '.' if results[i][-1] != '.' and results[i][-1] !=
                   '!' and results[i][-1] != '?' else results[i] for i in range(len(results))]
        # Sensitive Words Pattern
        final_results = []
        for result in results:
            if not any(re.search(x, result.lower()) for x in SENSITIVE_WORDS_LIST):
                final_results.append(result)
        return final_results
    return None


if __name__ == '__main__':
    print(get_tedtalk_topics())
    print(get_tedtalk_content(category='asia'))
    print("test get content by titie:")
    print(
        get_tetalk_content_by_title(
            'How the US should use its superpower status',
            category='asia'))
    # print(get_backstory_response_with_score("what food do you like"))
    # print(get_all_holiday(category="Technology", limit=20))

    # print(scan_related_holiday('french fries day', 70))

    # print(scan_related_holiday('kwanzaa day', 70))

    # print(get_redis("gunrock:test", "test1"))
    # print(set_redis_with_expire("gunrock:test",
    #                            "test1", {"test1": ['test2', 'test3']}, 3))
    # print(get_redis("gunrock:test", "test1"))
    import time

    time.sleep(3)
    # print(get_redis("gunrock:test", "test1"))
    # print(get_538_significant_digits(date='2018-06-21'))

    # return all the future holidays
    # print(get_future_holiday(limit=1))
    # get by category
    # print(get_future_holiday(category="Technology"))
    # # not cat found return nothing
    # print(get_future_holiday("error test"))
    # print(is_common_name("Kevin"))
    # print(is_common_name("yes"))
