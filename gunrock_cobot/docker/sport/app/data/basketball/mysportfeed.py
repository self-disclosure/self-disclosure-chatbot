import requests
import base64
import json

USERNAME = "zmy9108"
PASSWORD = "opengunrock"
#pull_regular_season_player_url = 'https://api.mysportsfeeds.com/v1.2/pull/nba/2017-2018-regular/cumulative_player_stats.json?playerstats=2PA/G,2PM/G,2P%,3PA/G,3PM/G,3P%,FTA/G,FT%,REB/G,AST/G,PTS/G,TOV/G,STL/G,BS/G'
#pull_playoff_player_url = ''
#pull_regular_season_team_url = ''
pull_active_player_url = 'https://api.mysportsfeeds.com/v1.2/pull/nba/2017-2018-regular/active_players.json'
pull_overall_team_url = 'https://api.mysportsfeeds.com/v1.2/pull/nba/2017-2018-regular/overall_team_standings.json'

def send_request(pull_url):
    # Request
    try:
        response = requests.get(
            url=pull_url,
            params={
                "fordate": "20161121"
            },
            headers={
                "Authorization": "Basic " + base64.b64encode('{}:{}'.format(USERNAME,PASSWORD).encode('utf-8')).decode('ascii')
            }
        )
        print('Response HTTP Status Code: {status_code}'.format(
            status_code=response.status_code))
        """
        print('Response HTTP Response Body: {content}'.format(
            content=response.content))
        """
        return response.content
    except requests.exceptions.RequestException:
        print('HTTP Request failed')
        return None

def full_player_name(player_data):
	r"""
		Return the full name of the player
	"""
	first_name = player_data['player']['FirstName']
	last_name = player_data['player']['LastName']
	full_name = ' '.join([first_name,last_name])
	return full_name.lower()

def full_team_name(team_data):
	r"""
		Return the full name of the player
	"""
	city_name = team_data['team']['City']
	team_name = team_data['team']['Name']
	full_name = ' '.join([city_name,team_name])
	return full_name.lower()

#Get a list of Active Players
regular_active_players_response = send_request(pull_active_player_url)
regular_active_players_data = json.loads(str(regular_active_players_response,encoding='utf-8'))
regular_active_players_list = regular_active_players_data['activeplayers']['playerentry']
player_names = [full_player_name(x) for x in regular_active_players_list]
print(player_names[:10])


#Get a list of Teams
regular_teams_response = send_request(pull_overall_team_url)
regular_teams_data = json.loads(str(regular_teams_response,encoding='utf-8'))
regular_teams_list = regular_teams_data['overallteamstandings']['teamstandingsentry']
team_names = [full_team_name(x) for x in regular_teams_list]
print(team_names[:10])

entity_list = []
#Build the Entity List:
for name in player_names:
	entity_list.append((name,{"label":"player"}))
for name in team_names:
	entity_list.append((name,{"label":"team"}))

#build entity_list_dict from entity_list
entity_dict = dict(x for x in entity_list)

with open("entity_list.json",'w') as fp:
	json.dump(entity_dict,fp)
"""
active_players = json.loads(str(active_players_response,encoding='utf-8'))
#Print the number of active players
print(len(active_players['cumulativeplayerstats']['playerstatsentry']))

active_players_list = active_players['cumulativeplayerstats']['playerstatsentry']
"""