import requests
import json
import datetime

team_results_data = requests.get('http://worldcup.sfg.io/teams/results')
#team_results_group_data = requests.get('http://worldcup.sfg.io/teams/group_results')
#team_data = requests.get('http://worldcup.sfg.io/teams/')
match_data = requests.get('http://worldcup.sfg.io/matches')
#match_today_data = requests.get('http://worldcup.sfg.io/matches/today')


#print(type(team_data.json()))
#team_data_json = team_data.json()
team_results_data_json = team_results_data.json()
#team_results_group_data_json = team_results_group_data.json()
match_data_json = match_data.json()
#match_today_data_json = match_today_data.json()
"""
#Dump the Team Data
with open('team.json','w') as fp:
	json.dump(team_data_json,fp)
"""
#Dump the Team_results_data with respect to time. 
#Dump the Team_reults_group_data with respect to time. 

now = datetime.datetime.now()
current_year = now.year
current_month = now.month
current_day = now.day

if now.hour >= 12:
	half_day = 0
else:
	half_day = 1
time_suffix = "{}_{}_{}_{}".format(current_month,current_day,current_year,half_day)

#Dump the team results
with open("team_results_{}.json".format(time_suffix),'w') as fp:
	json.dump(team_results_data_json,fp)
"""
#Dump the team group results
with open("team_results_group_{}.json".format(time_suffix),'w') as fp:
	json.dump(team_results_group_data_json,fp)
"""

#Dump the match data
with open("match_data_{}.json".format(time_suffix),'w') as fp:
	json.dump(match_data_json,fp)
"""
#Dump the match today data
with open("match_today_data_{}.json".format(time_suffix),'w') as fp:
	json.dump(match_today_data_json,fp)
"""