import re
from typing import List, Optional
from nlu.dataclass import ReturnNLP
from nlu.constants import DialogAct as DialogActEnum


class OpinionDetector:
    POS_REGEX = r"^(?!.*not ).*(amazing|funny|hilarious|cute|interesting|cool|exciting|wonderful|awesome|surprising|alarming)"

    def __init__(self, returnnlp: Optional[List[dict]]):
        self.returnnlp = ReturnNLP.from_list(returnnlp) if returnnlp else None

    def extract_positive_opinion_keyword(self):
        """
        :return: the last matched opinion keyword
        """

        if not self.returnnlp:
            return None

        for i in range(len(self.returnnlp)):
            if self.returnnlp.has_dialog_act([DialogActEnum.POS_ANSWER, DialogActEnum.COMMENT, DialogActEnum.OPINION, DialogActEnum.APPRECIATION], i):
                lexical_intents = self.returnnlp.lexical_intent[i]
                segmented_user_utterance = self.returnnlp.text[i]

                match = OpinionDetector.is_positive_opinion(segmented_user_utterance, lexical_intents)
                if match:
                    matched_groups = match.groups()
                    if matched_groups:
                        matched_groups = list(filter(None, matched_groups))
                        if matched_groups:
                            return matched_groups[-1].lower()
        return None

    @staticmethod
    def is_positive_opinion(text, lexical_intents):
        if 'ans_neg' not in lexical_intents:
            return re.search(OpinionDetector.POS_REGEX, text)
        return None
