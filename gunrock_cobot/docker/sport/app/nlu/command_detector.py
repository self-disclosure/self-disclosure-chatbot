import re
from typing import List, Dict, Any, Optional
from nlu.intentmap_scheme import lexical_re_patterns
from nlu.constants import DialogAct as DialogActEnum
from nlu.dataclass.returnnlp import ReturnNLP, ReturnNLPSegment
import logging
import itertools

class CommandDetector:

    def __init__(self, complete_user_utterance: str, returnnlp: Optional[List[dict]]):
        self.complete_user_utterance = complete_user_utterance
        self.returnnlp = ReturnNLP.from_list(returnnlp)

    def has_command(self):
        COMMAND_BLACK_LIST = ["yes", "yeah", "yup", "certainly", "no"]

        last_segment = self.returnnlp[-1]  # todo: maybe check other segments as well?
        if last_segment.text in COMMAND_BLACK_LIST:
            return False

        for lexical_intents in last_segment.intent.lexical:
            if "command" in lexical_intents:
                return True

        return self.is_command_by_dialog_act()

    def has_command_complete(self, all=True):
        """Checking has command in all segment.
        We can refactor has_command as has_command_complete(all=False)
        """
        COMMAND_BLACK_LIST = ["yes", "yeah", "yup", "certainly", "no"]
        if not all:
            segments = [self.returnnlp[-1]]
        else:
            segments = self.returnnlp

        def check_seg_command(seg):
            black_listed = seg.text in COMMAND_BLACK_LIST
            lexical = "command" in itertools.chain(*seg.intent.lexical)
            dialog_act =  self.is_command_by_dialog_act(seg=seg)
            return not black_listed and (lexical or dialog_act)

        return any([check_seg_command(seg) for seg in segments])

    def has_command_in_question_form_req_topic(self):
        return self.returnnlp[-1].has_intent("command_in_question_form_req_topic")

    def has_command_in_question_form_no_req_topic(self):
        return self.returnnlp[-1].has_intent("command_in_question_form_no_req_topic")

    def is_request_change_topic(self):
        return self.returnnlp[-1].has_intent(["change_topic", "req_topic_jump", "get_off_topic"])

    def is_request_jumpout(self, topic_name: str = "TOPIC NAME"):
        """
        Return True if it is a request_jumpout_command
        """

        jumpout_pattern = "|".join([
            rf"((can|could) (we|you) (please )?|((\blet's\b|\blet us\b|\blets\b)\s))?((get off|(stop|not) talk(ing)? about|i (don't|do not) (want|wanna) (to )?talk( about)?|i'm tired of talking about|^i hate)( the)?(( \w+)*( this| it| that| {topic_name})( please| anymore)?$))",
            rf"i don't like {topic_name}"
        ])

        if not self.has_command():
            return False

        match_regex = re.search(jumpout_pattern, self.complete_user_utterance) is not None
        return match_regex

    def is_request_tellmore(self):
      """
      Return Ture if user request the bot tell me more
      """
      if self.has_command() and self.returnnlp[-1].has_intent("req_more"):
          return True
      return False


    def is_command_by_dialog_act(self, seg=None):
        if seg is None:
            seg = self.returnnlp[-1]

        return seg.has_dialog_act(DialogActEnum.COMMANDS) and \
               not seg.has_dialog_act([DialogActEnum.OPEN_QUESTION,
                                                      DialogActEnum.OPEN_QUESTION_FACTUAL,
                                                      DialogActEnum.OPEN_QUESTION_OPINION,
                                                      DialogActEnum.YES_NO_QUESTION], threshold=0.5)

