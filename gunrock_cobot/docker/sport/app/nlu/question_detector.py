import logging

from typing import List, Dict, Any, Optional
from nlu.dataclass.returnnlp import ReturnNLP, ReturnNLPSegment
from nlu.dataclass.central_element import CentralElement
from nlu.constants import DialogAct as DialogActEnum
from nlu.command_detector import CommandDetector
from backstory_client import get_backstory_response
from nlu.dataclass.backstory import Backstory
import re
from nlu.intentmap_scheme import IS_QUESTION_IN_COMMAND_FORM


class QuestionDetector:
    def __init__(self, complete_user_utterance: str, returnnlp: Optional[List[dict]]):
        self.complete_user_utterance = complete_user_utterance
        self.returnnlp = ReturnNLP.from_list(returnnlp)
        self.question_intents = {"ask_yesno", "ask_preference", "ask_opinion", "ask_advice",
                            "ask_hobby", "ask_recommend", "ask_info",
                            "ask_back", "ask_reason"}

    def has_question(self):
        # Currently assume question is in the last segment
        return (self.is_not_why_not() and
                (self.is_question_by_regex() or self.is_question_by_dialog_act())) or \
            self.is_question_in_command_form() or \
            self.end_with_question_mark()

    def end_with_question_mark(self):
        text = self.get_question_segment_text().strip()
        if text:
            logging.info(f"end_with_question_mark: {text[-1]}")
            return text[-1] == "?"
        return False

    def has_question_seg(self, seg):
        return (self.is_not_why_not(seg=seg) and
                (self.is_question_by_regex(seg=seg) or self.is_question_by_dialog_act(seg=seg))) or \
        self.is_question_in_command_form(seg)

    def is_question_by_regex(self, seg=-1):
        
        detected_question_intents = set(self.returnnlp[seg].intent.lexical)
        intents_matching_ask_prefix = self.question_intents.intersection(detected_question_intents)
        match_question_by_regex = len(intents_matching_ask_prefix) != 0

        return match_question_by_regex

    def is_not_why_not(self, seg=-1):
        match = re.match(r"why not", self.returnnlp[seg].text)
        return match is None

    def is_not_command(self, seg=-1):
        command_detector = CommandDetector(self.complete_user_utterance, self.returnnlp)

        return not command_detector.has_command()

    def is_question_by_dialog_act(self, seg=-1):
        return self.returnnlp[seg].has_dialog_act([
            DialogActEnum.OPEN_QUESTION,
            DialogActEnum.OPEN_QUESTION_FACTUAL,
            DialogActEnum.OPEN_QUESTION_OPINION,
            DialogActEnum.YES_NO_QUESTION], threshold=(0.35, 0.2))

    def is_question_in_command_form(self, seg=-1):
        match = re.search(IS_QUESTION_IN_COMMAND_FORM, self.complete_user_utterance)
        return match is not None

    def get_question_segment_text(self, seg=-1):
        # todo: improve it to check more segments
        return self.returnnlp[seg].text

    def is_ask_back_question(self, seg=-1):
        return "ask_back" in self.returnnlp[seg].intent.lexical

    def is_follow_up_question(self, seg=-1):
        # todo: add more
        result = re.search(r"^when$|^where$|^what's .* rated$", self.returnnlp[seg].text)
        return result is not None

    def is_potential_backstory_question(self):
        # backstory covers all types of question. Can potentially be improved.
        return self.has_question()

    def is_backstory_question_from_regex(self, seg=-1):
        return "topic_backstory" in self.returnnlp[seg].intent.topic

    def is_factual_question(self, seg=-1):
        return self.returnnlp[seg].has_dialog_act(DialogActEnum.OPEN_QUESTION_FACTUAL)

    def is_opinion_question(self, seg=-1):
        return self.returnnlp[seg].has_dialog_act(DialogActEnum.OPEN_QUESTION_OPINION)

    def is_yes_no_question(self, seg=-1):
        return self.returnnlp[seg].has_dialog_act(DialogActEnum.YES_NO_QUESTION)

    def get_question_da(self, seg):
        return self.returnnlp[seg].dialog_act_label

    def backstory_high_confidence(self, conf_threshold:float=0.9, seg=-1) -> bool:
        response_dict = get_backstory_response(self.get_question_segment_text(seg))
        backstory_response = Backstory.from_dict(response_dict)
        return backstory_response.confidence > conf_threshold and backstory_response.text