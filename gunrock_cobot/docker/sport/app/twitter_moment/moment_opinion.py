
import boto3
from boto3.dynamodb.conditions import Key, Attr
import time
dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
table = dynamodb.Table('Moments')
from pprint import pprint
import json
import redis
from textblob import TextBlob
import re

from difflib import SequenceMatcher

KEYWORD_PREFIX = "gunrock:moments:tags:"
REDIS_R = redis.StrictRedis(host='52.87.136.90', port=16517, db=0, password="alexaprize", decode_responses=True)

FILTER_PATTERNS = [r'\S*twitter.com\S*',r'http\S*',r'\n',r'\S*#\S*','\xa0']
REMOVE_PATTERNS = [r'\S*@\S*']

OPINION_THRESHOLD = 0.7
SIMILARITY_THRESHOLD = 0.6

def get_sport_moment_data():
    """
    Type is the moment type like Today, SPorts, News, Entertainment. One of 5 tags.
    :param type: can be news, sports, today, entertainment, fun must be lowercase!
    :return: list of items by date. Up to you to truncate and sort by likes
    """
    type_tag='sports'
    response = table.query(
        # ProjectionExpression= "id,name,score,sub,title,opinion,related",
        IndexName='type',
        KeyConditionExpression=Key('type').eq(type_tag),
        ScanIndexForward=False
    )
    resp = response['Items']
    #print(type(resp))
    return resp

def get_moment_source_lookup(source):
    """
    Source is the tag that is occasionally attached to moment like politics
    :param source: any tag from the get keywords function
    :return: list of events sorted by date
    """
    response = table.query(
        # ProjectionExpression= "id,name,score,sub,title,opinion,related",
        IndexName='source',
        KeyConditionExpression=Key('source').eq(source),
        ScanIndexForward=False
    )
    resp = response['Items']

    
    result = []
    for item in resp:
        if item['type'] == 'sports':
            result.append(item)
    
    return result

def filter_tweets(raw_tweet):
    #result = re.sub(r'\S*twitter.com\S*','',raw_tweet).strip()
    result = raw_tweet
    for filter_pattern in FILTER_PATTERNS:
        result = re.sub(filter_pattern,' ',result)

    for remove_pattern in REMOVE_PATTERNS:
        if re.search(remove_pattern,result):
            return None
    return result

def sentence_similar(a, b):
    return SequenceMatcher(None, a, b).ratio()

def select_moment_opinions(moment):
    r'''
    Pick the opinions from the tweets around a moment. We first reformat the text by removing url, hashtags and linebreakers. 
    Then, we filter out the moments with @, as we don't have a good way to process these twitter accounts. Some of them sound really awkward. 
    Finally, we will check the opinion_score using textblob's sentiment classifier. Based on a small set of data, we think the Threshold of 0.7 is good. 
    We will conduct experiment to further evaluate the sentiment classifier on picking approrpiate contents. 
    We also filter out the tweets sharing a high similarity to the title. So afar we have the similarity Threshold 0.5
    In the end of the function we will return a list of opinions that can be used to interact with users. 
    Input:
        moment: The dictionary object that we get from Dynamodb on the sport moment. 
    Output:
        list: Return a list of filterd high-quality opinion comments on the moment
    '''
    #TODO: Include a filter of black list
    try:
        opinion_tweets_list = []
        for moment_tweet in moment['tweets']:
            tweet = filter_tweets(moment_tweet['tweet_text'])
            if tweet:
                subjectivity_score =abs(TextBlob(tweet).sentiment[0])+TextBlob(tweet).sentiment[1]
                if subjectivity_score >= OPINION_THRESHOLD and sentence_similar(tweet_title,tweet) <= SIMILARITY_THRESHOLD: 
                    opinion_tweets_list.append(tweet)
    except:
        return []
    return opinion_tweets_list

if __name__ == "__main__":
    moments = get_moment_source_lookup('mlb')
    #print(len(moments))
    
    #for test_moment in moments:
        #print(test_moment['desc'])
    
    test_moment = moments[0]
    pprint(test_moment)

    #first_moment_tweet = test_moment['tweets'][0]['tweet_text']
    #print(first_moment_tweet)

    #print(TextBlob(first_moment_tweet).sentiment)

    tweet_title = test_moment['title']
    print("The title of the moment is: {}".format(tweet_title))

    opinions = select_moment_opinions(test_moment)
    print(opinions)
