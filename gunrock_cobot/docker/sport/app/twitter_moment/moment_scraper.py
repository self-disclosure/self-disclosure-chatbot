# -*- coding: utf-8 -*-
from requests import get
from bs4 import BeautifulSoup
import os
import re
import json
from datetime import datetime
import pytz

start_url = "https://twitter.com/i/moments?lang=en"
save_path = "."
domain = "https://twitter.com/{}"
current_time = datetime.now(tz=pytz.utc).strftime('%Y-%m-%d')


def go_category():
    category_links = {}
    try:
        response = get(start_url)
        html_soup = BeautifulSoup(response.text, "html.parser")
        category_container = html_soup.find_all(name="ul", attrs={"class": "MomentGuideNavigation-categories"})

        for utags in category_container:
            for litag in utags.find_all("li"):
                if litag.text and litag.a["href"]:
                    category_links[litag.text.strip()] = litag.a["href"].strip()
        
        for k, v in category_links.items():
            print(k, v)
            get_moments(k, domain.format(v))
    except Exception as e:
        print("GO CATEGORY: {}".format(e))
    pass


def get_moments(name, url):
    moments = []
    #try:
    response = get(url)
    html_soup = BeautifulSoup(response.text, "html.parser")
    link_container = html_soup.find_all(name="div", attrs={"class": "MomentCapsuleSummary-details"})
    links = [link.a["href"].strip() for link in link_container]
    for l in links:
        try:
            moment = get_moment(l)
            moments.append(moment)
        except:
            pass
    
    #except Exception as e:
        #print("GET MOMENT LINK {}: {}".format(url, e))
    

    print("SCRAPE MOMENTS: {}".format(len(moments)))
    try:
        os.mkdir(save_path + "/" + name)
    except:
        pass

    f = open(save_path + "/" + name + "/{}_{}.json".format(name, current_time), "w")
    json.dump(moments, f)
    f.close()


def get_moment(url):
    title = "\\N"
    source = "\\N"
    desc = "\\N"
    like = "\\N"
    tweets = []
    try:
        tweets = []
        response = get(url)
        html_soup = BeautifulSoup(response.text, "html.parser")
        title_container = html_soup.find_all(name="h1", attrs={"class": "MomentCapsuleCover-title u-dir"})
        title = " ".join([t.text.strip() for t in title_container if t.text])
        # print(title)
        subtitle_container = html_soup.find_all(name="div", attrs={"class": "MomentCapsuleSubtitle"})
        source_container = subtitle_container[0].find_all(name="span", attrs={"class": re.compile("(MomentCapsuleSubtitle-category u-dir|MomentUserByline-fullname)")})
        source = " ".join([s.text.strip() for s in source_container if s.text])
        # print(source)
        desc_container = html_soup.find_all(name="div", attrs={"class": "MomentCapsuleCover-description u-dir"})
        desc = " ".join([d.text.strip() for d in desc_container if d.text])
        # print(desc)
        like_container = html_soup.find_all(name="strong", attrs={"class": "MomentCapsuleLikesFacepile-countNum"})
        like = like_container[0].text.strip() if len(like_container) and like_container[0].text else "\\N"
        # print(like)
        tweet_stream_container = html_soup.find_all(name="div", attrs={"class": re.compile("MomentCapsuleItem MomentCapsuleItem--with(Media|Text) js-stream-item")})
        for tweet in tweet_stream_container:
            try:
                tweeter_container = tweet.find_all(name="span", attrs={"class": "MomentUserByline-fullname"})
                tweeter = " ".join(t.text.strip() for t in tweeter_container if t.text)
                tweet_text = tweet.p.text.strip()
                likes_container = tweet.find_all(name="span", attrs={"class": "ProfileTweet-actionCountForPresentation"})
                likes = likes_container[0].text.strip() if len(likes_container) and likes_container[0].text else "\\N"
                tweets.append((tweeter, tweet_text, likes))
            except:
                pass
        # print(tweets)
    except Exception as e:
        print("GET MOMENT {}: {}".format(url, e))
    return {
        "title": title,
        "source": source,
        "like": like,
        "desc": desc,
        "tweets": tweets
    }


if __name__ == '__main__':
    go_category()