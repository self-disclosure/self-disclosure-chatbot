import random
import re
import datetime
import json
import logging
from backstory_client import get_backstory_response_text
logging.basicConfig(
    # What does this Mean
    format='[%(levelname)s] %(asctime)s [%(filename)s:%(lineno)d] %(message)s',

)
import requests
import signal
import template_manager

# define template_sports to generate response
template_sports = template_manager.Templates.sports

from sport_utterance_data import KNOWN_SPORTS, KNOWN_ENTITIES,sport_utterance, POPULAR_SPORTS, SPECIAL_SPORT_TOPIC
from sport_utils import *
from sport_acknowledge import Sport_Acknowledge
from selecting_strategy.module_selection import ModuleSelector
from sports_question_handeler import SportsQuestionHandeler

from utils import set_logger_level

set_logger_level(logging)

# Get the API_KEY for Evi
COBOT_API_KEY = 'xgTjk23SRM9Q9VFrUEFOjav0Bn4ot21W8uFLEieT'

# Get the WORLD CUP Data Dirx
WORLD_CUP_DIR = 'data/soccer/worldcup'
NOW = datetime.datetime.now()
if NOW.hour > 12:
    half_day = 0
else:
    half_day = 1
TIME_SUFFIX = "{}_{}_{}_{}".format(NOW.month, NOW.day, NOW.year, half_day)

# TODO: Handle when there is no news for sport_type
DEFAULT_NO_UPDATE_RESPONSE_FOR_SPORT = "Have you heard any interesting updates on {sport_type}?"

# Get the News Client
# NEWS_BOT = RNews_Client(addr='52.87.136.90')

# Initialize Thereshold Value for Error Status
SUGGEST_ERROR_THRESHOLD = 2
#define the unknown_sports_templates_upperbound, default is 3
UNKNOWN_SPORTS_TEMPLATES_UPPERBOUND=3

DO_JOKE = 1.0

rds = redis.StrictRedis(host='52.87.136.90', port=16517,
                        socket_timeout=3,
                        socket_connect_timeout=1,
                        retry_on_timeout=True,
                        db=0, password="alexaprize", decode_responses=True)


class Timeout():
    """Timeout class using ALARM signal. """
    class Timeout(Exception):
        pass

    def __init__(self, sec):
        self.sec = sec

    def __enter__(self):
        signal.signal(signal.SIGALRM, self.raise_timeout)
        signal.alarm(self.sec)

    def __exit__(self, *args):
        signal.alarm(0)    # disable alarm

    def raise_timeout(self, *args):
        raise Timeout.Timeout()

# ============= Define Sport Response ==============


class Sport_Response(object):

    def __init__(self, current_state, user_attributes, user_utterance=None, user_intent=None, context=None, sys_info=None):
        self.current_state = current_state
        self.user_attributes = user_attributes
        self.module_selector = ModuleSelector(self.user_attributes)
        self.user_utterance = user_utterance
        self.user_intent = user_intent
        self.context = context
        self.sys_info = sys_info
        # self.user_profile = user_profile

        # Initialize Acknowledge Mechanism
        self.acknowledge_system = Sport_Acknowledge(user_utterance, user_attributes, user_intent, context, sys_info)

        self.__bind__response()

    def __bind__response(self):
        self.response_mapping = {
            #"s_sport_ini_chitchat": self.s_sport_ini_chitchat,
            #"s_sport_ini_know_entity": self.s_sport_ini_know_entity,
            "s_sport_chitchat_q": self.s_sport_chitchat_q,
            "s_sport_chitchat_a": self.s_sport_chitchat_a,
            "s_sport_favorite_sport_confirmation": self.s_sport_favorite_sport_confirmation,
            "s_sport_confirm_sport_continue": self.s_sport_confirm_sport_continue,
            "s_sport_answer_question": self.s_sport_answer_question,
            "s_sport_provide_news": self.s_sport_provide_news,
            "s_sport_provide_funfacts": self.s_sport_provide_funfacts,
            "s_sport_provide_moments": self.s_sport_provide_moments,
            "s_sport_give_moment_opinion": self.s_sport_give_moment_opinion,
            "s_sport_provide_moments_details": self.s_sport_provide_moments_details,
            "s_sport_ask_interest_in_moment": self.s_sport_ask_interest_in_moment,
            "s_sport_exit": self.s_sport_exit
        }

    def generate_response(self):
        current_response = self.response_mapping[self.current_state]()
        logging.debug("[SPORTS_MODULE] expect state after fail_question_answer: {}".format(current_response.get("expect_next_state", None)))
        if self.context["fail_question_answer"]:
            # Modify the previous_state
            self.previous_state = "s_sport_answer_question"
            self.context["previous_state"] = "s_sport_answer_question"
            if current_response.get("expect_next_state", None):
                self.current_state = current_response["expect_next_state"]
            else:
                #TODO: Need to have better transition here.
                self.current_state = "s_sport_confirm_sport_continue"
            
            # Clear the selected_question
            self.context["selected_question"] = {}

            # Generate the response
            new_response = self.response_mapping[self.current_state]()
            new_response["response"] = ' '.join(
                [current_response["response"], new_response["response"]])
            # Add a key called: modified_current_state
            new_response["modified_current_state"] = self.current_state
            #logging.debug
            return new_response

        return current_response
    #####################################Shared Function for All States#################################
    def __retrieve_sport_type_content__(self, current_sport, expect_next_state, num_error_status):
        #initialize main_body_response:
        main_body_response = None
        #First Priority: Attempt to present random news from Twitter
        try:
            selected_news = search_moment_by_sport_type(
                current_sport, limit=5)
        except:
            selected_news = []
        # Remove all the talked selected_news
        for news in selected_news:
            if hash(news) in self.context["talked_web_content"].get(current_sport, []):
                selected_news.remove(news)
        if selected_news:
            selected_post = selected_news[0]
            main_body_response = "I <w role='amazon:VBD'>read</w> some news about {sport_entity}. {news} Do you find this interesting?".format(sport_entity=current_sport, news=selected_post)

            # No need to ground with any start response.

            if len(selected_news) > 1:
                expect_next_state = "s_sport_provide_news"
            else:
                # Changed by Mingyang Zhou
                #expect_next_state = "s_sport_ini_chitchat"
                expect_next_state = "s_sport_confirm_sport_continue"

                # Update num_error_status
            num_error_status = 0
            self.context["selected_news"] = selected_news
            self.context['talked_web_content'][current_sport] = self.context["talked_web_content"].get(current_sport, []) + [hash(selected_post)]

            return main_body_response, expect_next_state, num_error_status

        #Second Priority: Present random funfact about a sport if we cant retrieve one
        try:
            with Timeout(2):
                # ground the current sport when the current sport
                # is in the GROUND_SPORT_LIST
                if current_sport in GROUND_SPORT_LIST:
                    search_key_word = " ".join(
                        [current_sport, "game"])
                else:
                    search_key_word = current_sport
                reddit_post = get_interesting_reddit_posts(
                    search_key_word, 2)
                # remove all the talked content
                for post in reddit_post:
                    if hash(post) in self.context["talked_web_content"].get(current_sport, []):
                        reddit_post.remove(post)
        except Timeout.Timeout:
            logging.debug("[SPORTS_MODULE] did not successfully retrieve content on {}".format(current_sport))
            reddit_post = None
        #If we found a reddit_post
        if reddit_post:
            selected_funfacts = reddit_post
            selected_post = reddit_post[0]
            main_body_response = "I found something interesting about {sport_type}. {fun_fact}. Isn't this interesting?".format(
                sport_type=current_sport, fun_fact=selected_post)
            # Select expect next_state
            if len(selected_funfacts) == 2:
                expect_next_state = "s_sport_provide_funfacts"
            else:
                expect_next_state = "s_sport_confirm_sport_continue"
            # Update num_error_status
            num_error_status = 0
            
            #Update the context
            self.context["selected_funfacts"] = selected_funfacts
            self.context['talked_web_content'][current_sport] = self.context["talked_web_content"].get(current_sport, []) + [hash(selected_post)]
        return main_body_response, expect_next_state, num_error_status

    def __retrieve_entity_type_content__(self, current_entity, current_sport, expect_next_state, num_error_status, random_quit_thred=0, raise2stype=False):
        #initialize main_body_response:
        main_body_response = None
        #retrieve the reddit content
        try:
            with Timeout(2):
                reddit_post = get_interesting_reddit_posts(
                    current_entity["name"], 2)
        except Timeout.Timeout:
            main_body_response = template_sports.utterance(selector='grounding/unable_handle_sport_general', slots={'sport_entity_type': current_entity['name']},
                                                           user_attributes_ref=self.user_attributes)
            num_error_status += 1

        if reddit_post:
            selected_funfacts = reddit_post
            selected_post = reddit_post[0]
            main_body_response = "I found something interesting on {sport_entity}. {fun_fact} Isn't this interesting?".format(
                sport_entity=current_entity["name"], fun_fact=selected_post)
            
            self.context["selected_funfacts"] = selected_funfacts
            self.context['talked_web_content'][current_entity["name"]] = self.context["talked_web_content"].get(current_entity["name"], []) + [hash(selected_post)]
            ######################################################################################################################################
            # Select expect next_state
            if len(reddit_post) == 2:
                expect_next_state = "s_sport_provide_funfacts"
            elif current_sport:
                total_current_sport_templates = template_sports.count_utterances(selector="qa_templates/sport_type/qa_{}/general".format(current_sport)) if current_sport in KNOWN_SPORTS else UNKNOWN_SPORTS_TEMPLATES_UPPERBOUND
                if self.context["asked_questions"].get(current_sport, 0) == total_current_sport_templates:
                    # 50% chance back to general & 50% chance go to favorite sport continue
                    if random.uniform(0, 1) > random_quit_thred:
                        #expect next state to be  s_sport_confirm_sport_continue
                        expect_next_state = 's_sport_confirm_sport_continue'
                    else:
                        expect_next_state = 's_sport_chitchat_q'
                        #change chitchat_mode to general
                        self.context["chitchat_mode"] = "general"
                else:
                    expect_next_state = 's_sport_chitchat_q'
                    #update the chitchat_mode back to higher hierarchy
                    self.context["chitchat_mode"] = "sport_type"
                    raise2stype = True
            else:
                expect_next_state = "s_sport_confirm_sport_continue"
                #expect_next_state = "s_sport_ini_chitchat"
            # Update num_error_status
            num_error_status = 0
        else:
            main_body_response = template_sports.utterance(selector='grounding/unable_handle_sport_general', slots={'sport_entity_type': current_entity['name']},
                                                           user_attributes_ref=self.user_attributes)
            # Define the Expect Next State
            if current_sport:
                total_current_sport_templates = template_sports.count_utterances(selector="qa_templates/sport_type/qa_{}/general".format(current_sport)) if current_sport in KNOWN_SPORTS else UNKNOWN_SPORTS_TEMPLATES_UPPERBOUND
                if self.context["asked_questions"].get(current_sport, 0) == total_current_sport_templates:
                    # 50% chance back to general & 50% chance go to favorite sport continue
                    if random.uniform(0, 1) > random_quit_thred:
                        #expect next state to be  s_sport_confirm_sport_continue
                        expect_next_state = 's_sport_confirm_sport_continue'
                    else:
                        expect_next_state = 's_sport_chitchat_q'
                        #change chitchat_mode to general
                        self.context["chitchat_mode"] = "general"
                else:
                    expect_next_state = 's_sport_chitchat_q'
                    #update the chitchat_mode back to higher hierarchy
                    self.context["chitchat_mode"] = "sport_type"
                    raise2stype = True
            else:
                expect_next_state = "s_sport_confirm_sport_continue"
            
            # propose_continue = "STOP"
            num_error_status += 1  # Update the num error_status
        
        return main_body_response, expect_next_state, num_error_status, raise2stype

    def update_expect_state_outsideQA(self, current_sport, current_entity, chitchat_mode, expect_next_state, raise2stype=False, random_quit_thred=0.0):
        if chitchat_mode == "entity" and expect_next_state in ["s_sport_chitchat_q", "s_sport_chitchat_a", None]:
            if current_entity["name"] in KNOWN_ENTITIES:
                #separately discussed cases for KNOWN_ENTITIES and UNKOWN_ENTITIES
                total_current_entity_templates = template_sports.count_utterances(selector="qa_templates/sport_entity/qa_{}".format(current_entity["name"]))
                if self.context["asked_questions"].get(current_entity["name"], 0) < total_current_entity_templates:
                    expect_next_state = 's_sport_chitchat_q'
                    return expect_next_state, raise2stype
            
            #Other cases, we want to switch back to sport_type dialog flow
            if current_sport:
                #checkout whether we want to jumpout
                total_current_sport_templates = template_sports.count_utterances(selector="qa_templates/sport_type/qa_{}/general".format(current_sport)) if current_sport in KNOWN_SPORTS else UNKNOWN_SPORTS_TEMPLATES_UPPERBOUND
                if self.context["asked_questions"].get(current_sport, 0) == total_current_sport_templates:
                    # 50% chance back to general & 50% chance go to favorite sport continue
                    if random.uniform(0, 1) > random_quit_thred:
                        #expect next state to be  s_sport_confirm_sport_continue
                        expect_next_state = 's_sport_confirm_sport_continue'
                    else:
                        expect_next_state = 's_sport_chitchat_q'
                        #change chitchat_mode to general
                        self.context["chitchat_mode"] = "general"
                else:
                    expect_next_state = 's_sport_chitchat_q'
                    #update the chitchat_mode back to higher hierarchy
                    self.context["chitchat_mode"] = "sport_type"
                    raise2stype = True

        elif chitchat_mode == "sport_type" and expect_next_state in ["s_sport_chitchat_q", "s_sport_chitchat_a", None]:
            total_current_sport_templates = template_sports.count_utterances(selector="qa_templates/sport_type/qa_{}/general".format(current_sport)) if current_sport in KNOWN_SPORTS else UNKNOWN_SPORTS_TEMPLATES_UPPERBOUND
            if self.context["asked_questions"].get(current_sport, 0) == total_current_sport_templates and not self.context["followup_questions"]:
                # 50% chance back to general & 50% chance go to favorite sport continue
                if random.uniform(0, 1) > random_quit_thred:
                    #expect next state to be  s_sport_confirm_sport_continue
                    expect_next_state = 's_sport_confirm_sport_continue'
                else:
                    expect_next_state = 's_sport_chitchat_q'
                    #change chitchat_mode to general
                    self.context["chitchat_mode"] = "general"
            else:
                expect_next_state = 's_sport_chitchat_q'


        return expect_next_state, raise2stype
####################################################################################################################################################
    def s_sport_chitchat_q(self):
        r"""
        Will enter this state when the user suggest a entity to talk about or we are smoothly transit from s_sport_followup_chitcaht_answer.
        The name entity, or sport type won't be changed under this state.
        """
        # Get the three information from
        user_sport_intents = self.user_intent['sport_intents']
        user_sport_context = self.context

        current_entity = user_sport_context['current_entity']
        current_sport = user_sport_context['current_sport']
        asked_questions = user_sport_context['asked_questions']
        propose_continue = user_sport_context['propose_continue']
        followup_questions = user_sport_context["followup_questions"]
        general_sport_questions = user_sport_context['general_sport_questions']
        talked_web_content = user_sport_context["talked_web_content"] #talked_web_content is a list of talked content

        # selected_chitchat_id = None
        # selected_chitchat_key = None
        # selected_question = user_sport_context["selected_question"]
        if user_sport_context["previous_state"] == "s_sport_chitchat_q":
            selected_question = user_sport_context["selected_question"]
        else:
            selected_question = {}

        num_error_status = 0
        raise2stype = False

        # Initialize the Expect Next State
        expect_next_state = None

        # Initialize start_response and main_body_response
        start_response = ""
        main_body_response = ""
        
        chitchat_mode = user_sport_context["chitchat_mode"]
        ####################################Modify the Starting Response########################################################
        acknowledge_key = selected_question.get("acknowledge", None)
        start_response = self.acknowledge_system.get_acknowledge(acknowledge_key)
        #update the selected_question
        selected_question = {}
        ####################################Build the Flow to Sample Questions##################################################
        
        #1 Check followup questions. 
        #logging.debug("[SPORTS_MODULE] followup_questions: {}".format(followup_questions))
        if followup_questions:
            selected_question = followup_questions[-1]
            #update the followup_questions 
            self.context["followup_questions"].pop()

            #determine the expect_next_state
            if not selected_question.get("a", "") and len(self.context['followup_questions']) == 0:
                #change expectation state for follow_up questions
                if chitchat_mode == "sport_type":
                    assert current_sport is not None, "current_sport cannot be none if we are in the sport_type chitchat"
                    total_current_sport_templates = template_sports.count_utterances(selector="qa_templates/sport_type/qa_{}/general".format(current_sport)) if current_sport in KNOWN_SPORTS else UNKNOWN_SPORTS_TEMPLATES_UPPERBOUND
                    if asked_questions.get(current_sport, 0) == total_current_sport_templates:
                        # 50% chance back to general & 50% chance go to favorite sport continue
                        if random.uniform(0, 1) > 0.0:
                            #expect next state to be  s_sport_confirm_sport_continue
                            expect_next_state = 's_sport_confirm_sport_continue'
                        else:
                            expect_next_state = 's_sport_chitchat_q'
                            #change chitchat_mode to general
                            self.context["chitchat_mode"] = "general"
                    else:
                        expect_next_state = 's_sport_chitchat_q'
            
            elif not selected_question.get("a", ""):
                expect_next_state = 's_sport_chitchat_q'
            
            #TODO: generate main_body_response with different chat_mode: general, entity
            if chitchat_mode == "sport_type":
                assert current_sport is not None, "current_sport cannot be none if we are in the sport_type chitchat"

                if selected_question.get('acknowledge', None) in ['ask_play_sport_yes_no', 'ask_who_play_sport_with']:
                    #Optimize the 
                    if re.search(r'ing', current_sport):
                        main_body_response = selected_question['q'][1].format(sport_type=current_sport)
                    else:
                        main_body_response = selected_question['q'][0].format(sport_type=current_sport)        
                else:
                    main_body_response = random.choice(selected_question['q']).format(sport_type=current_sport)
        
        #2.1 chitchat_mode is sport_entity
        elif chitchat_mode == "entity":
            assert  current_entity["name"] is not None, "current_entity name cannot be none"
            # If Known Entity where we have templates 
            if current_entity["name"] in KNOWN_ENTITIES:
                #Using templates to chat about  the named entity
                total_current_entity_templates = template_sports.count_utterances(selector="qa_templates/sport_entity/qa_{}".format(current_entity["name"]))
                assert asked_questions.get(current_entity["name"], 0) < total_current_entity_templates, "asked_questions should be less than total entity_emplates"
                selected_question = template_sports.utterance(selector="qa_templates/sport_entity/qa_{}".format(current_entity["name"]), slots={}, user_attributes_ref=self.user_attributes)
                #update the asked_questions
                self.context["asked_questions"][current_entity["name"]] = asked_questions.get(current_entity["name"], 0) + 1
            else:
                #Retrieve the content for the  entities that is not Known
                current_entity = ground_sport_entity(current_entity, current_sport)
                current_entity["name"] = current_entity["name"].strip()
                #retrieve the content for the entity
                main_body_response, expect_next_state, num_error_status, raise2stype = self.__retrieve_entity_type_content__(current_entity, current_sport, expect_next_state, num_error_status)
                
                if main_body_response:
                        #reset the start_response
                        start_response = ""
                        response_dict = {
                                        "response": ' '.join([start_response, main_body_response]),
                                        "propose_continue": propose_continue,
                                        "expect_next_state": expect_next_state,
                                        "num_error_status": num_error_status
                                        }
                        if raise2stype:
                            response_dict["2sport_type"] = raise2stype
                        return response_dict

            ##################################################################################################################

            assert selected_question, "selected_question must not be empty"
            
            #update the expect_next_state
            if not selected_question.get("a", "") and asked_questions.get(current_entity["name"], 0) == total_current_entity_templates:
                if current_sport:
                    #checkout whether we want to jumpout
                    total_current_sport_templates = template_sports.count_utterances(selector="qa_templates/sport_type/qa_{}/general".format(current_sport)) if current_sport in KNOWN_SPORTS else UNKNOWN_SPORTS_TEMPLATES_UPPERBOUND
                    if asked_questions.get(current_sport, 0) == total_current_sport_templates:
                        # 50% chance back to general & 50% chance go to favorite sport continue
                        if random.uniform(0, 1) > 0.0:
                            #expect next state to be  s_sport_confirm_sport_continue
                            expect_next_state = 's_sport_confirm_sport_continue'
                        else:
                            expect_next_state = 's_sport_chitchat_q'
                            #change chitchat_mode to general
                            self.context["chitchat_mode"] = "general"
                    else:
                        expect_next_state = 's_sport_chitchat_q'
                        #update the chitchat_mode back to higher hierarchy
                        self.context["chitchat_mode"] = "sport_type"
                        raise2stype = True
            elif not selected_question.get("a", ""):
                #We still have templates
                expect_next_state = 's_sport_chitchat_q'
            ####################################################
            
            #generate the main_response, no format is needed
            main_body_response = random.choice(selected_question['q'])

        #2.2 chitchat_mode is sport_type
        elif chitchat_mode == "sport_type":
            #TODO: Implement Resume Mode to extract content for talked sports with no extra chitchat templates
            assert current_sport is not None, "current_sport cannot be none if we are in the sport_type chitchat"
            if current_sport in KNOWN_SPORTS:
                """
                sports with customized templates, 
                """
                total_current_sport_templates = template_sports.count_utterances(selector="qa_templates/sport_type/qa_{}/general".format(current_sport))
                assert asked_questions.get(current_sport, 0) < total_current_sport_templates, "the asked_questions should be less than {}".format(total_current_sport_templates)
                selected_question = template_sports.utterance(selector="qa_templates/sport_type/qa_{}/general".format(current_sport), slots={}, user_attributes_ref = self.user_attributes)
                #update the asked_questions
                self.context["asked_questions"][current_sport] = asked_questions.get(current_sport, 0) + 1
            else:
                #questions on unknown sport_type, randomly pick from demonstrating a fact or use some general templates to handle
                ################################################Retrieve Content and Present#########################################
                if random.uniform(0,1) < 0.5 and not talked_web_content.get(current_sport, []): #default value: 0.5
                    # #present random funfact about a sport if we cant retrieve one
                    main_body_response, expect_next_state, num_error_status = self.__retrieve_sport_type_content__(current_sport, expect_next_state, num_error_status)
                    
                    if main_body_response:
                        #reset the start_response
                        start_response = ""
                        response_dict = {
                                        "response": ' '.join([start_response, main_body_response]),
                                        "propose_continue": propose_continue,
                                        "expect_next_state": expect_next_state,
                                        "num_error_status": num_error_status
                                        }
                        return response_dict
                ####################################################################################################################################

                #pick an unkown sports templates
                total_current_sport_templates = UNKNOWN_SPORTS_TEMPLATES_UPPERBOUND
                #sample three questions from unknown sports template
                assert asked_questions.get(current_sport, 0) < total_current_sport_templates, "the asked questions of an unknown sport should be less than {}".format(total_current_sport_templates)
                
                selected_question = template_sports.utterance(selector="qa_templates/sport_type/qa_unknown/general", slots={"sport_type": current_sport}, user_attributes_ref = self.user_attributes)
                self.context["asked_questions"][current_sport] = asked_questions.get(current_sport, 0) + 1
                
            
            #if selected_question:
            assert selected_question, "selected_question must not be empty."
            #determine the expect_next_state
            if not selected_question.get("a", "") and asked_questions.get(current_sport, 0) == total_current_sport_templates:
                # 50% chance back to general & 50% chance go to favorite sport continue
                if random.uniform(0, 1) > 0.0:
                    #expect next state to be  s_sport_confirm_sport_continue
                    expect_next_state = 's_sport_confirm_sport_continue'
                else:
                    expect_next_state = 's_sport_chitchat_q'
                    #change chitchat_mode to general
                    self.context["chitchat_mode"] = "general"
            elif not selected_question.get("a", ""):
                expect_next_state = 's_sport_chitchat_q'
            
            #Define the main_body_response and append any additional start_response to connect different states
            #main_body_response = random.choice(selected_question['q']).format(sport_type=current_sport)
            if selected_question.get('acknowledge', None) in ['ask_play_sport_yes_no', 'ask_who_play_sport_with']:
                #Optimize the 
                if re.search(r'ing', current_sport):
                    main_body_response = selected_question['q'][1].format(sport_type=current_sport)
                else:
                    main_body_response = selected_question['q'][0].format(sport_type=current_sport)       
            else:
                main_body_response = random.choice(selected_question['q']).format(sport_type=current_sport)

            #Add transition to start_response
            if self.context["asked_questions"].get(current_sport, 0) > 1:
                if random.uniform(0,1) < 0.7: #default value is 0.7
                    start_response = ' '.join([start_response, template_sports.utterance(selector='grounding/continue_sport', slots={'sport_type': current_sport}, user_attributes_ref=self.user_attributes)])
            elif self.context["previous_state"] in ["s_sport_provide_news", "s_sport_provide_funfacts", "s_sport_answer_question"] or self.context["2sport_type"]:
                start_response = ' '.join([start_response, template_sports.utterance(selector='grounding/continue_sport', slots={'sport_type': current_sport}, user_attributes_ref=self.user_attributes)])


        
        #update the context
        if selected_question:
            self.context["selected_question"] = selected_question
        
        #update the current_sport
        if current_sport:
            self.context["current_sport"] = current_sport
            if current_sport not in self.context["talked_sports"]:
                self.context["talked_sports"].append(self.context["current_sport"])

        
        response_dict = {
        "response": ' '.join([start_response, main_body_response]),
        "propose_continue": propose_continue,
        "expect_next_state": expect_next_state,
        "num_error_status": num_error_status
        }
        if raise2stype:
            #if 2sport_type is True
            response_dict["2sport_type"] = True
        return response_dict
    
    def s_sport_chitchat_a(self):
        r"""
        Will enter this state when the system intends to answer the questions.
        """

        # Get the three information from
        user_sport_intents = self.user_intent['sport_intents']
        user_sport_context = self.context

        current_entity = user_sport_context['current_entity']
        current_sport = user_sport_context['current_sport']
        asked_questions = user_sport_context['asked_questions']
        propose_continue = user_sport_context['propose_continue']
        followup_questions = user_sport_context["followup_questions"]
        general_sport_questions = user_sport_context['general_sport_questions']

        # selected_chitchat_id = None
        # selected_chitchat_key = None
        selected_chitchat_id = user_sport_context["selected_chitchat_id"]
        selected_chitchat_key = user_sport_context["selected_chitchat_key"]
        selected_question = user_sport_context["selected_question"]
        num_error_status = 0
        raise2stype = False #indicate whether we convert the chitchat mode from entity to sport

        # Initialize the Expect Next State
        expect_next_state = None

        # Initialize start_response and main_body_response
        start_response = ""
        main_body_response = ""


        chitchat_mode = user_sport_context["chitchat_mode"]

        logging.debug("[SPORTS_MODULE] followup_questions: {}".format(followup_questions))
        ####################################Modify the Starting Response########################################################
        acknowledge_key = selected_question.get("acknowledge", None)
        start_response = self.acknowledge_system.get_acknowledge(acknowledge_key)

        ####################################Starting the Conversation########################################################
        assert selected_question.get("a", ""), "answer in the selected question should not be empty"

        #Get the main Body Response
        main_body_response = random.choice(selected_question['a']).format(sport_entity=current_entity.get("name", ""), sport_type=current_sport)
        
        # Determine the expect state
        #TODO: build the logic for 'general', and 'entity'
        if followup_questions:
            if not selected_question.get("a", "") and len(self.context['followup_questions']) == 0:
                #change expectation state for follow_up questions
                if chitchat_mode == "sport_type":
                    assert current_sport is not None, "current_sport cannot be none if we are in the sport_type chitchat"
                    total_current_sport_templates = template_sports.count_utterances(selector="qa_templates/sport_type/qa_{}/general".format(current_sport)) if current_sport in KNOWN_SPORTS else UNKNOWN_SPORTS_TEMPLATES_UPPERBOUND
                    if asked_questions.get(current_sport, 0) == total_current_sport_templates:
                        # 50% chance back to general & 50% chance go to favorite sport continue
                        if random.uniform(0, 1) > 0.0:
                            #expect next state to be  s_sport_confirm_sport_continue
                            expect_next_state = 's_sport_confirm_sport_continue'
                        else:
                            expect_next_state = 's_sport_chitchat_q'
                            #change chitchat_mode to general
                            self.context["chitchat_mode"] = "general"
                    else:
                        expect_next_state = 's_sport_chitchat_q'
            
            elif not selected_question.get("a", ""):
                expect_next_state = 's_sport_chitchat_q'
        elif chitchat_mode == "entity":
            total_current_entity_templates = template_sports.count_utterances(selector="qa_templates/sport_entity/qa_{}".format(current_entity["name"]))
            if asked_questions.get(current_entity["name"], 0) == total_current_entity_templates:
                if current_sport:
                    #checkout whether we want to jumpout
                    total_current_sport_templates = template_sports.count_utterances(selector="qa_templates/sport_type/qa_{}/general".format(current_sport)) if current_sport in KNOWN_SPORTS else UNKNOWN_SPORTS_TEMPLATES_UPPERBOUND
                    if asked_questions.get(current_sport, 0) == total_current_sport_templates:
                        # 50% chance back to general & 50% chance go to favorite sport continue
                        if random.uniform(0, 1) > 0.0:
                            #expect next state to be  s_sport_confirm_sport_continue
                            expect_next_state = 's_sport_confirm_sport_continue'
                        else:
                            expect_next_state = 's_sport_chitchat_q'
                            #change chitchat_mode to general
                            self.context["chitchat_mode"] = "general"
                    else:
                        expect_next_state = 's_sport_chitchat_q'
                        #update the chitchat_mode back to higher hierarchy
                        self.context["chitchat_mode"] = "sport_type"
                        raise2stype = True

        elif chitchat_mode == "sport_type":
            #TODO: build the logic for sport in UNKNOWN_SPORTS
            total_current_sport_templates = template_sports.count_utterances(selector="qa_templates/sport_type/qa_{}/general".format(current_sport)) if current_sport in KNOWN_SPORTS else UNKNOWN_SPORTS_TEMPLATES_UPPERBOUND
            if asked_questions.get(current_sport, 0) == total_current_sport_templates:
                # 50% chance back to general & 50% chance go to favorite sport continue
                if random.uniform(0, 1) > 0:
                    #expect next state to be  s_sport_confirm_sport_continue
                    expect_next_state = 's_sport_confirm_sport_continue'
                else:
                    expect_next_state = 's_sport_chitchat_q'
                    #change chitchat_mode to general
                    self.context["chitchat_mode"] = "general"


        
        response_dict = {
        "response": ' '.join([start_response, main_body_response]),
        "propose_continue": propose_continue,
        "num_error_status": num_error_status,
        "expect_next_state": expect_next_state,
        }
        if raise2stype:
            #if 2sport_type is True
            response_dict["2sport_type"] = True
        return response_dict

    def s_sport_favorite_sport_confirmation(self):
        user_sport_intents = self.user_intent['sport_intents']
        user_sport_context = self.context
        num_error_status = user_sport_context['num_error_status']
        propose_continue = user_sport_context['propose_continue']



        if user_sport_context["previous_state"] == 's_sport_favorite_sport_confirmation':
            starter_quote = ""
            selected_quote = "I didn't catch the sport that you have mentioned. Can you say it again?"
            num_error_status += 1
        elif user_sport_context["previous_state"] == 's_sport_none':
            starter_quote = template_sports.utterance(selector='grounding/start_sport_topic', slots={},
                                                      user_attributes_ref=self.user_attributes)
            selected_quote = template_sports.utterance(selector='grounding/ask_favorite_sport', slots={},
                                                       user_attributes_ref=self.user_attributes)
            num_error_status = 0
        elif user_sport_context["previous_state"] == "s_sport_confirm_sport_continue":
            starter_quote = ""
            selected_quote = template_sports.utterance(selector='grounding/confirm_next_favorite_sport', slots={},
                                                       user_attributes_ref=self.user_attributes)
            num_error_status = 0
        else:
            starter_quote = template_sports.utterance(selector='grounding/appologize_for_missing_interest', slots={},
                                                      user_attributes_ref=self.user_attributes)
            selected_quote = template_sports.utterance(selector='grounding/ask_user_interested_sport', slots={},
                                                       user_attributes_ref=self.user_attributes)
            num_error_status = 0
        
        #Add a special acknowledegment for workout. TEMPORORY FIX
        for topic in SPECIAL_SPORT_TOPIC:
            if re.search(topic, self.user_utterance):
                starter_quote = "I am glad that you like {}. ".format(topic) + starter_quote
                break

        response_dict = {
            "response": ' '.join([starter_quote, selected_quote]),
            "propose_continue": propose_continue,
            "num_error_status": num_error_status
        }
        return response_dict
    
    def s_sport_answer_question(self):
        # Get the user sport intent
        user_sport_intents = self.user_intent['sport_intents']
        user_sport_context = self.context
        user_utterance = self.user_utterance

        #Initialize the Question Handeler
        sport_question_handeler = SportsQuestionHandeler(self.user_utterance, self.sys_info, self.user_attributes)

        # Initialize num Error Status:
        num_error_status = user_sport_context['num_error_status']
        propose_continue = user_sport_context['propose_continue']
        asked_questions = user_sport_context["asked_questions"]
        raise2stype = False

        current_sport = user_sport_context["current_sport"]
        current_entity = user_sport_context["current_entity"]
        if user_sport_context["expect_next_state"] in ["s_sport_provide_news", "s_sport_provide_funfacts"]:
            expect_next_state = user_sport_context["expect_next_state"]
        else:
            expect_next_state = None

        # Check if detected sports and detected entity
        if user_sport_context['detected_entities']['sport_ner'] or user_sport_context['detected_entities']['sys_ner']:
            if user_sport_context['detected_entities']['sport_ner']:
                self.context['current_sport'] = user_sport_context[
                    'detected_entities']['sport_ner'][0]['sport_type']
                current_sport = self.context['current_sport']
                if self.context["chitchat_mode"] == "general":
                        self.context["chitchat_mode"] = "sport_type"
            else:
                if user_sport_context['detected_entities']['sys_ner'][0]['sport_type']:
                    self.context['current_sport'] = user_sport_context[
                        'detected_entities']['sys_ner'][0]['sport_type']
                    current_sport = self.context['current_sport']
                    if self.context["chitchat_mode"] == "general":
                        self.context["chitchat_mode"] = "sport_type"
        

        elif user_sport_context['detected_sports']:
            # Such that in the next state, we may select this new sport to talk
            # about.
            self.context['current_sport'] = user_sport_context['detected_sports'][0]
            current_sport = self.context['current_sport']
            if self.context["chitchat_mode"] == "general":
                self.context["chitchat_mode"] = "sport_type"
        ###########################################################################################
        #change expect state in special cases.
        expect_next_state, raise2stype = self.update_expect_state_outsideQA(current_sport, current_entity, self.context["chitchat_mode"], expect_next_state)

        ###########################################################################################
        self.user_utterance = ground_question_with_sport_type(
            self.user_utterance, current_sport,
            user_sport_context['detected_sports'])
        
        sport_question_handeler_response = sport_question_handeler.handle_question()

        if sport_question_handeler_response and sport_question_handeler_response.response and sport_question_handeler_response.tag!= "ack_question_idk":
            main_response = sport_question_handeler_response.response
            if sport_question_handeler_response.bot_propose_module:
                if sport_question_handeler_response.bot_propose_module.value != "SPORT":
                    self.context["propose_topic"] =  sport_question_handeler_response.bot_propose_module.value
                    propose_continue =  "STOP"
                #TODO: Record the backstory_reasoning
            return self.get_response_dict(main_response, num_error_status, propose_continue, current_sport, expect_next_state, raise2stype=raise2stype)


        num_error_status += 1

        if user_sport_context['detected_entities']['sys_ner'] or user_sport_context['detected_entities']['sport_ner']:
            topic_keyword = self.get_topic_keyword(user_sport_context)
            reddit_post = get_interesting_reddit_post_with_timeout(
                topic_keyword, 2)

            if reddit_post:
                selected_post = reddit_post[0]
                selected_quote = template_sports.utterance(selector='grounding/unable_handle_question_with_funfact', slots={"topic": topic_keyword, "fun_fact": selected_post},
                                                           user_attributes_ref=self.user_attributes)

                return self.get_response_dict(selected_quote, num_error_status, propose_continue, current_sport, expect_next_state, raise2stype=raise2stype)
        elif sport_question_handeler_response.tag == "ack_question_idk":
            main_response = sport_question_handeler_response.response
            if sport_question_handeler_response.bot_propose_module:
                if sport_question_handeler_response.bot_propose_module.value != "SPORT":
                    self.context["propose_topic"] =  sport_question_handeler_response.bot_propose_module.value
                    propose_continue =  "STOP"
                #TODO: Record the backstory_reasoning
            self.context["fail_question_answer"] = True
            return self.get_response_dict(main_response, num_error_status, propose_continue, current_sport, expect_next_state, raise2stype=raise2stype)


        selected_quote = unable_answer_question_error_handle(user_utterance, user_sport_context, self.sys_info, self.user_attributes)
        self.context["fail_question_answer"] = True

        return self.get_response_dict(selected_quote, num_error_status, propose_continue, current_sport, expect_next_state, raise2stype=raise2stype)

    def get_response_dict(self, response, num_error_status, propose_continue, current_sport, expect_next_state=None, raise2stype=False):
        response_dict = {
            "response": response,
            "num_error_status": num_error_status,
            "propose_continue": propose_continue,
            "expect_next_state": expect_next_state,
        }

        if current_sport:
            response_dict['sport_type'] = current_sport
        if raise2stype:
            response_dict['2sport_type'] = True
        return response_dict

    def get_topic_keyword(self, user_sport_context):
        if user_sport_context['detected_entities']['sport_ner']:
            topic_keyword = user_sport_context[
                'detected_entities']['sport_ner'][0]['name']
        else:
            topic_keyword = user_sport_context[
                'detected_entities']['sys_ner'][0]['name']
        return topic_keyword

    # TODO: Implement this State
    def s_sport_provide_news(self):
        # Get the user sport intent
        user_sport_intents = self.user_intent['sport_intents']
        user_sport_contexts = self.context

        # Initialize num Error Status:
        current_entity = user_sport_contexts['current_entity']
        current_sport = user_sport_contexts['current_sport']
        num_error_status = user_sport_contexts['num_error_status']
        raise2stype = False

        # Initialize propose_continue
        propose_continue = user_sport_contexts['propose_continue']

        # Determine the reply content
        selected_news = user_sport_contexts['selected_news']
        selected_post = selected_news[1]
        selected_topic = None
        if current_entity["name"]:
            selected_topic = current_entity["name"]
        else:
            selected_topic = current_sport
        start_response = ""

        if selected_topic:
            main_body_response = "Here is another news about {topic}. {news}. Do you find this news interesting to you?".format(topic=selected_topic, news=selected_post)
            
            #update talked_web_content
            self.context["talked_web_content"][selected_topic].append(hash(selected_post))
            

            #####################################################Update the context####################################################################
            #expect_next_state = "s_sport_ini_chitchat"
            if user_sport_context["chitchat_mode"]  == "entity":
                if current_sport:
                    #there is a known current_sport
                    total_current_sport_templates = template_sports.count_utterances(selector="qa_templates/sport_type/qa_{}/general".format(current_sport)) if current_sport in KNOWN_SPORTS else UNKNOWN_SPORTS_TEMPLATES_UPPERBOUND

                    if user_sport_contexts["asked_questions"].get(current_sport, 0) < total_current_sport_templates or user_sport_contexts["followup_questions"]:
                        expect_next_state = "s_sport_chitchat_q"
                        self.context["chitchat_mode"] = "sport_type"
                        raise2stype = False
                    else:
                        expect_next_state = "s_sport_confirm_sport_continue"
                else:
                    expect_next_state = "s_sport_confirm_sport_continue"

            elif user_sport_context["chitchat_mode"] == "sport_type":
                total_current_sport_templates = template_sports.count_utterances(selector="qa_templates/sport_type/qa_{}/general".format(current_sport)) if current_sport in KNOWN_SPORTS else UNKNOWN_SPORTS_TEMPLATES_UPPERBOUND
                
                if user_sport_contexts["asked_questions"].get(current_sport, 0) < total_current_sport_templates or user_sport_contexts["followup_questions"]:
                    expect_next_state = "s_sport_chitchat_q"
                else:
                    expect_next_state = "s_sport_confirm_sport_continue"
            ###########################################################################################################################################
            num_error_status = 0
        else:
            main_body_response = "My programmer made a mistake. Let me propose other sport topics. "
            expect_next_state = "s_sport_chitchat_q"
            num_error_status += 1

        response_dict = {
            "response": ' '.join([start_response, main_body_response]),
            "propose_continue": propose_continue,
            "expect_next_state": expect_next_state,
            "num_error_status": num_error_status
        }
        if raise2stype:
            response_dict["2sport_type"] = True

        return response_dict

    def s_sport_provide_funfacts(self):
        # Get the user sport intent
        user_sport_intents = self.user_intent['sport_intents']
        user_sport_contexts = self.context

        # Initialize num Error Status:
        current_entity = user_sport_contexts['current_entity']
        current_sport = user_sport_contexts['current_sport']
        num_error_status = user_sport_contexts['num_error_status']
        raise2stype = False

        # Initialize propose_continue
        propose_continue = user_sport_contexts['propose_continue']

        # Determine the reply content
        selected_funfacts = user_sport_contexts['selected_funfacts']
        selected_post = selected_funfacts[1]
        selected_topic = None
        if current_entity["name"]:
            selected_topic = current_entity["name"]
        else:
            selected_topic = current_sport
        start_response = ""

        if selected_topic:
            main_body_response = "I <w role='amazon:VBD'>read</w> another fun fact about {topic}. {fun_fact} Do you find that interesting?".format(
                topic=selected_topic, fun_fact=selected_post)
            
            #Logics to update expect_next_state
            ##################################################Update Expect State##############################################
            #expect_next_state = "s_sport_ini_chitchat"
            if user_sport_contexts["chitchat_mode"]  == "entity":
                if current_sport:
                    #there is a known current_sport
                    total_current_sport_templates = template_sports.count_utterances(selector="qa_templates/sport_type/qa_{}/general".format(current_sport)) if current_sport in KNOWN_SPORTS else UNKNOWN_SPORTS_TEMPLATES_UPPERBOUND

                    if user_sport_contexts["asked_questions"].get(current_sport, 0) < total_current_sport_templates or user_sport_contexts["followup_questions"]:
                        expect_next_state = "s_sport_chitchat_q"
                        self.context["chitchat_mode"] = "sport_type"
                        raise2stype = False
                    else:
                        expect_next_state = "s_sport_confirm_sport_continue"
                else:
                    expect_next_state = "s_sport_confirm_sport_continue"

            elif user_sport_contexts["chitchat_mode"] == "sport_type":
                total_current_sport_templates = template_sports.count_utterances(selector="qa_templates/sport_type/qa_{}/general".format(current_sport)) if current_sport in KNOWN_SPORTS else UNKNOWN_SPORTS_TEMPLATES_UPPERBOUND
                
                if user_sport_contexts["asked_questions"].get(current_sport, 0) < total_current_sport_templates or user_sport_contexts["followup_questions"]:
                    expect_next_state = "s_sport_chitchat_q"
                else:
                    expect_next_state = "s_sport_confirm_sport_continue"
            ######################################################################################################################    
            num_error_status = 0
        else:
            main_body_response = "I've confused myself again. Let me propose other things that I can talk about in sports. "
            #expect_next_state = "s_sport_ini_chitchat"
            expect_next_State = "s_sport_confirm_sport_continue"
            num_error_status += 1

        response_dict = {
            "response": ' '.join([start_response, main_body_response]),
            "propose_continue": propose_continue,
            "expect_next_state": expect_next_state,
            "num_error_status": num_error_status
        }

        if raise2stype:
            response_dict["2sport_type"] = True
        return response_dict

    # Updated on 8/28/2018, by Mingyang Zhou
    def s_sport_provide_moments(self):
        r"""
            TODO: Allow Users to directly ask for news regarding sports types
        """
        # Get the user sport intent
        user_sport_intents = self.user_intent['sport_intents']
        user_sport_contexts = self.context

        # Initialize num Error Status:
        current_entity = user_sport_contexts['current_entity']
        current_sport = user_sport_contexts['current_sport']
        num_error_status = user_sport_contexts['num_error_status']

        # Initialize propose_continue
        propose_continue = user_sport_contexts['propose_continue']

        # Initalize the talked moments
        talked_moments = user_sport_contexts['talked_moments']
        
        # logging.debug(
        #     "[SPORTS_MODULE] start moment crawling")
        

        # Determine the reply content

        moments = search_moment_by_sport_type_full(current_sport)
        
        # logging.debug(
        #     "[SPORTS_MODULE] length crawled for moments: {}".format(len(moments)))
        
        # Remove any moments in the remaining moments
        remaining_moments = []
        for m in moments:
            if m['title'] not in talked_moments:
                remaining_moments.append(m)
        # selected_post = random.choice(remaining_moments)
        if len(remaining_moments) > 0:
            selected_post = remaining_moments[0]
        else:
            # TODO: Handle case when provide moments fail.  Use previous talked
            # moments
            selected_post = {"title": current_sport, "desc": DEFAULT_NO_UPDATE_RESPONSE_FOR_SPORT.format(
                sport_type=current_sport)}
       
        selected_moment = {'title': selected_post['title'], 'content': selected_post[
            'desc'], 'opinion': []}

        # for single_opinion in selected_post.get('opinions', []):
        #     # logging.info("[SPORTS_MODULE] the current opinion is: {}".format(single_opinion))
        #     if len(single_opinion.split()) <= 30:
        #         selected_moment['opinion'].append(single_opinion)

        # Generate Response
        if user_sport_intents['sport_i_requestnews']:
            start_response_key = "confirm_request"
        else:
            start_response_key = "express_excitement"

        # start_response = sport_utterance(start_response_key)['text']
        start_response = template_sports.utterance(selector='grounding/{}'.format(start_response_key), slots={},
                                                   user_attributes_ref=self.user_attributes)
        main_body_response = template_sports.utterance(selector='grounding/propose_moment', slots={'sport_type': current_sport, 'moment_content': selected_moment['title']},
                                                       user_attributes_ref=self.user_attributes)
        # logging.info("[SPORTS_MODULE] main_body_response is: {}".format(main_body_response))

        # Confirm if users have heard about this news
        # post_grounding_key = ["ask_if_heard_moment"]
        post_grounding_utterance = template_sports.utterance(selector='grounding/ask_if_heard_moment', slots={},
                                                             user_attributes_ref=self.user_attributes)
        main_body_response = ' '.join(
            [main_body_response, post_grounding_utterance])

        selected_opinion = selected_moment['opinion']
        if selected_opinion:
            expect_next_state = 's_sport_give_moment_opinion'
        elif len(remaining_moments) > 1:
            expect_next_state = 's_sport_provide_moments'
        else:
            #expect_next_state = 's_sport_ini_chitchat'
            expect_next_state = 's_sport_chitchat_q'

        # Update the Context
        talked_moments.append(selected_moment['title'])

        response_dict = {
            "response": ' '.join([start_response, main_body_response]),
            "propose_continue": propose_continue,
            "selected_moment": selected_moment,
            "talked_moments": talked_moments,
            "expect_next_state": expect_next_state,
            "num_error_status": num_error_status
        }

        return response_dict

    def s_sport_give_moment_opinion(self):
        user_sport_intents = self.user_intent['sport_intents']
        user_sport_contexts = self.context

        current_sport = user_sport_contexts['current_sport']
        # Initialize Num Error Status
        num_error_status = user_sport_contexts['num_error_status']

        # Initialize propose_continue
        propose_continue = user_sport_contexts['propose_continue']

        # Extract the selected_moment
        selected_moment = user_sport_contexts['selected_moment']

        # Initialize the start response
        start_response = ""

        # Get the previous state
        previous_state = user_sport_contexts['previous_state']
        talked_moments = user_sport_contexts['talked_moments']

        # Determine the reply content
        moments = search_moment_by_sport_type_full(current_sport)

        # Remove any moments in the remaining moments
        remaining_moments = []
        for m in moments:
            if m['title'] not in talked_moments:
                remaining_moments.append(m)

        ##################Generate the Main Response################
        selected_opinion = random.choice(selected_moment['opinion'])
        if previous_state == 's_sport_provide_moments':
            start_response = template_sports.utterance(selector='grounding/express_excitement', slots={},
                                                       user_attributes_ref=self.user_attributes)
            main_body_response = template_sports.utterance(selector='grounding/provide_others_moment_opinion', slots={'moment_opinion': selected_opinion},
                                                           user_attributes_ref=self.user_attributes)
            if remaining_moments:
                expect_next_state = 's_sport_ask_interest_in_moment'
            else:
                #expect_next_state = 's_sport_ini_chitchat'
                expect_next_state = 's_sport_chitchat_q'

        elif user_sport_intents['sport_i_answeruncertain']:
            start_response = template_sports.utterance(selector='grounding/acknowledge_uncertainity_moment_opinion', slots={},
                                                       user_attributes_ref=self.user_attributes)
            if remaining_moments:
                main_body_response = template_sports.utterance(selector='grounding/propose_another_moment', slots={'sport_type': current_sport},
                                                               user_attributes_ref=self.user_attributes)
                expect_next_state = 's_sport_provide_moments'
            else:
                main_body_response = ''
                #expect_next_state = 's_sport_ini_chitchat'
                expect_next_state = 's_sport_chitchat_q'
        else:
            start_response = template_sports.utterance(selector='grounding/acknowledge_opinion', slots={},
                                                       user_attributes_ref=self.user_attributes)
            main_body_response = template_sports.utterance(selector='grounding/provide_others_moment_opinion', slots={'moment_opinion': selected_opinion},
                                                           user_attributes_ref=self.user_attributes)

            if remaining_moments:
                expect_next_state = 's_sport_ask_interest_in_moment'
            else:
                #expect_next_state = 's_sport_ini_chitchat'
                expect_next_state = 's_sport_chitchat_q'

        response_dict = {
            "response": ' '.join([start_response, main_body_response]),
            "propose_continue": propose_continue,
            "expect_next_state": expect_next_state,
            "num_error_status": num_error_status
        }

        return response_dict

    def s_sport_provide_moments_details(self):
        r"""
            Provide the details about the moment that we provide to users.
        """
        # Get the user sport intent
        user_sport_intents = self.user_intent['sport_intents']
        user_sport_contexts = self.context

        # Initialize num Error Status:
        current_entity = user_sport_contexts['current_entity']
        current_sport = user_sport_contexts['current_sport']
        num_error_status = user_sport_contexts['num_error_status']

        # Initialize propose_continue
        propose_continue = user_sport_contexts['propose_continue']

        '''
        # Determine the reply content
        moments = search_moment_by_sport_type_full(current_sport)
        '''

        # Initialize selected_moment and talked_moments
        selected_moment = user_sport_contexts['selected_moment']
        talked_moments = user_sport_contexts['talked_moments']

        # Determine the reply content
        moments = search_moment_by_sport_type_full(current_sport)
        # Get the remaining Moments
        remaining_moments = []
        for m in moments:
            if m['title'] not in talked_moments:
                remaining_moments.append(m)

        # Initialize start response
        start_response = ''

        # logging.info("[SPORTS_MODULE] start_response is: {}".format(start_response))
        # main_response_key = ["provide_moment_details"]
        # main_body_response = sport_utterance(main_response_key)['text'].format(
        #     moment_content=selected_moment['content'])
        main_body_response = template_sports.utterance(selector='grounding/provide_moment_details', slots={'moment_content': selected_moment['content']},
                                                       user_attributes_ref=self.user_attributes)
        # logging.info("[SPORTS_MODULE] main_body_response is: {}".format(main_body_response))

        selected_opinion = selected_moment['opinion']
        if selected_opinion:
            # post_grounding_key = ["ask_moment_opinion"]
            post_grounding_utterance = template_sports.utterance(selector='grounding/ask_moment_opinion', slots={},
                                                                 user_attributes_ref=self.user_attributes)
            main_body_response = ' '.join(
                [main_body_response, post_grounding_utterance])

            expect_next_state = 's_sport_give_moment_opinion'

        elif len(remaining_moments) > 1:
            # post_grounding_key = ["propose_another_moment"]
            post_grounding_utterance = template_sports.utterance(selector='grounding/propose_another_moment', slots={'sport_type': current_sport},
                                                                 user_attributes_ref=self.user_attributes)
            main_body_response = ' '.join(
                [main_body_response, post_grounding_utterance])
            expect_next_state = 's_sport_provide_moments'
        else:
            #expect_next_state = 's_sport_ini_chitchat'
            expect_next_state = 's_sport_chitchat_q'

        # Update the Context
        talked_moments.append(selected_moment['title'])

        response_dict = {
            "response": ' '.join([start_response, main_body_response]),
            "propose_continue": propose_continue,
            "expect_next_state": expect_next_state,
            "num_error_status": num_error_status
        }

        return response_dict

    def s_sport_ask_interest_in_moment(self):
        r'''
        Confirm people's interest in hearing about another news
        '''
        user_sport_intents = self.user_intent['sport_intents']
        user_sport_contexts = self.context

        current_sport = user_sport_contexts['current_sport']
        # Initialize Num Error Status
        num_error_status = user_sport_contexts['num_error_status']

        # Initialize propose_continue
        propose_continue = user_sport_contexts['propose_continue']

        # Initialize the start response
        start_response = ""

        # Get the previous state
        previous_state = user_sport_contexts['previous_state']
        talked_moments = user_sport_contexts['talked_moments']

        # Determine the reply content
        moments = search_moment_by_sport_type_full(current_sport)

        # Remove any moments in the remaining moments
        remaining_moments = []
        for m in moments:
            if m['title'] not in talked_moments:
                remaining_moments.append(m)

        ##################Generate the Main Response################
        if user_sport_intents['sport_i_notinterest'] and len(self.user_utterance.split()) < 4:
            start_response_key = "acknowledge_not_interest"
        elif user_sport_intents['sport_i_answeruncertain']:
            start_response_key = "acknowledge_general"
        else:
            if user_sport_intents['sport_i_answeryes']:
                start_response_key = "appreciation_user_interest"
            else:
                start_response_key = "acknowledge_not_interest"

        # start_response = sport_utterance(start_response_key)['text']
        start_response = post_grounding_utterance = template_sports.utterance(selector='grounding/{}'.format(start_response_key), slots={},
                                                                              user_attributes_ref=self.user_attributes)

        if len(remaining_moments) > 1:
            # main_response_key = ["propose_another_moment"]
            # main_body_response = sport_utterance(
            #     main_response_key)['text'].format(sport_type=current_sport)
            main_body_response = template_sports.utterance(selector='grounding/propose_another_moment', slots={'sport_type': current_sport},
                                                           user_attributes_ref=self.user_attributes)
            expect_next_state = 's_sport_provide_moments'
        else:
            main_body_response = ""
            #expect_next_state = "s_sport_ini_chitchat"
            expect_next_state = "s_sport_chitchat_q"

        response_dict = {
            "response": ' '.join([start_response, main_body_response]),
            "propose_continue": propose_continue,
            "expect_next_state": expect_next_state,
            "num_error_status": num_error_status
        }

        return response_dict

    def s_sport_exit(self):
        # Get the user sport intent
        user_sport_intents = self.user_intent['sport_intents']
        user_sport_context = self.context
        propose_continue = user_sport_context['propose_continue']
        num_error_status = 0

        if user_sport_context['unclear']:
            # If propose_topic
            if user_sport_context['propose_topic']:
                detected_other_topic = MODULE_KEYWORD_MAP[
                    user_sport_context['propose_topic']]
                # selected_quote = 'It seems that you want to talk about {other_topic} intead of sports. Do you want to talk about {other_topic}?'.format(
                #     other_topic=detected_other_topic)
                if SPECIAL_SUBTOPIC.get(user_sport_context['propose_topic'], None) is not None:
                    #logging.debug("[SPORTS_MODULE] special subtopic is detected")
                    for x in SPECIAL_SUBTOPIC[user_sport_context['propose_topic']]:
                        if re.search(x, self.user_utterance):
                            selected_quote = template_sports.utterance(selector='grounding/exit_sport_module_topic', slots={'other_topic': x},
                                                           user_attributes_ref=self.user_attributes)
                            propose_continue = "UNCLEAR"

                            self.module_selector.add_propose_keyword({
                                "text": x
                            })

                            response_dict = {
                                            "response": selected_quote,
                                            "num_error_status": num_error_status,
                                            "propose_continue": propose_continue,
                                        }
                            return response_dict


                selected_quote = template_sports.utterance(selector='grounding/exit_sport_module_topic', slots={'other_topic': detected_other_topic},
                                                           user_attributes_ref=self.user_attributes)
            else:
                selected_quote = template_sports.utterance(selector='grounding/exit_sport_module_no_topic', slots={},
                                                           user_attributes_ref=self.user_attributes)
            propose_continue = "UNCLEAR"
        elif user_sport_context['previous_state'] == 's_sport_favorite_sport_confirmation' and user_sport_context["num_error_status"] > 0:
            selected_quote  = "I am sorry for keep missing your favorite sport. Maybe we can talk about sports later. "
            propose_continue = "STOP"
        else:
            selected_quote = "No Problem! "
            propose_continue = "STOP"

        response_dict = {
            "response": selected_quote,
            "num_error_status": num_error_status,
            "propose_continue": propose_continue,
        }
        return response_dict


    def s_sport_confirm_sport_continue(self):
        user_sport_intents = self.user_intent['sport_intents']
        user_sport_contexts = self.context
        current_sport = user_sport_contexts['current_sport']
        num_error_status = 0
        propose_continue = "CONTINUE"
        selected_question = user_sport_contexts["selected_question"]
        
        #logging.debug("[SPORTS_MODULE] the selected_question in s_sport_confirm_sport_continue is: {}".format(selected_question))
        #start_utterance = ""
        acknowledge_key = selected_question.get("acknowledge", None)
        start_utterance = self.acknowledge_system.get_acknowledge(acknowledge_key)
        #logging.debug("[SPORTS_MODULE] the acknowledge is: {}".format(start_response))

        if user_sport_intents['sport_i_notinterest']:
            start_utterance = "Sure!"
            main_utterance = start_utterance + ' ' + template_sports.utterance(selector='grounding/confirm_continue_sport_nointerest', slots={},
                                                                                                      user_attributes_ref=self.user_attributes)
        elif current_sport:
            assert current_sport is not None, "[SPORT_MODULE] BUG: The current sport under this case should not be None"
            start_utterance = ' '.join([start_utterance, template_sports.utterance(selector='grounding/run_out_of_sport_type_templates', slots={'sport_type': current_sport},user_attributes_ref=self.user_attributes)])
            main_utterance = start_utterance + ' ' + template_sports.utterance(selector='grounding/confirm_continue_sport', slots={'sport_type': current_sport},
                                                                                                      user_attributes_ref=self.user_attributes)
        elif user_sport_contexts["resume_mode_start"]:
            talked_sports = user_sport_contexts["talked_sports"]
            start_utterance = ' '.join([start_utterance, template_sports.utterance(selector='grounding/resume_start_sport_general', slots={}, user_attributes_ref=self.user_attributes)])
            logging.debug("[SPORTS_MODULE] talked sports: {}".format(talked_sports))
            if talked_sports:
                current_sport  = talked_sports[-1]
                main_utterance = start_utterance + ' ' + template_sports.utterance(selector='grounding/confirm_continue_sport', slots={'sport_type': current_sport}, user_attributes_ref=self.user_attributes)
            else:
                main_utterance = start_utterance + ' ' + template_sports.utterance(selector='grounding/confirm_continue_sport_nointerest', slots={}, user_attributes_ref=self.user_attributes)

        else:
            main_utterance = start_utterance + ' ' + template_sports.utterance(selector='grounding/confirm_continue_sport_nointerest', slots={},
                                                                                                      user_attributes_ref=self.user_attributes)

        response_dict = {
            "response": main_utterance,
            "num_error_status": num_error_status,
            "sport_type": None,
            "propose_continue": propose_continue
        }

        return response_dict
