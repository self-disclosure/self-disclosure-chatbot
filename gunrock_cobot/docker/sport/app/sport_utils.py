import praw
import redis
import string
from get_news import RNews_Client
import signal
import time
import re
from kgapi import *
from nltk import sent_tokenize
import requests
import json
import random
import logging
import os
import boto3
from boto3.dynamodb.conditions import Key, Attr
from pprint import pprint
from twitterES import moment_search
#from service_client import get_client

# For Mining Opinions
from textblob import TextBlob
from difflib import SequenceMatcher
from sport_utterance_data import sport_utterance
import template_manager
from service_client import get_client
from utils import set_logger_level
from backstory_client import get_backstory_response_text
from nlu.constants import DialogAct as DialogActEnum
from nlg_post_process.profanity_classifier import BotResponseProfanityClassifier
from nlu.command_detector import CommandDetector #detect command

set_logger_level(logging)

# define template_sports to generate response
template_sports = template_manager.Templates.sports

# Connect to the Moments table on dynamodb
dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
table = dynamodb.Table('Moments')

# For Moments
KEYWORD_PREFIX = "gunrock:moments:tags:"
REDIS_R = redis.StrictRedis(host='52.87.136.90', port=16517,
                            db=0, password="alexaprize", decode_responses=True)
SPORT_MOMENT_TAG_MAP = {
    'basketball': ['nba', 'ncaa basketball', 'basketball'],
    'football': ['nfl', 'ncaa football'],
    'hockey': ['nhl', 'hockey'],
    'baseball': ['mlb', 'baseball'],
    'tennis': ['tennis'],
    'wrestling': ['wrestling', 'wwe'],
    'soccer': ['premier league', 'major league soccer', 'football', 'fifa 19', 'uefa nations league', 'la liga'],
    'golf': ['pga', 'golf'],
    'gymnastics': ['gymnastics']
}

# For Moments Opinions
MOMENT_OPINION_FILTER_PATTERNS = [
    r'\S*twitter.com\S*', r'http\S*', r'\n', r'\S*#\S*']
MOMENT_OPINION_REMOVE_PATTERNS = [r'\S*@\S*']

MOMENT_OPINION_SCORE_THRESHOLD = 0.7
MOMENT_OPINION_SIMILARITY_THRESHOLD = 0.5

logging.basicConfig(
    format='[%(levelname)s] %(asctime)s [%(filename)s:%(lineno)d] %(message)s',
)

COBOT_API_KEY = 'EwecQ1fkEd4YMVUOHHpeSam7zbef53gipQsxAUki'  # EVI COBOT API Key
# Reddit API KEY
REDDIT_CLIENT_ID = "lKmLeMmSSLns6A"
REDDIT_CLIENT_SECRET = "Fi6V_pCzKqE-4TRVqcmpMJdET2s"

# SENSITIVE_WORDS_LIST = ["\b#!*@\b", "\b#!@'\b", "\b#!@'faced\b", "\b$*#@!\b", "\b$hit\b", "\b%*&@!\b", "\ba55\b", "\ba55hole\b", "\ba55holes\b", "\ba$$\b", "\ba$$hole\b", "\ba$$holes\b", "\ba$%\b", "\ba**hole\b", "\ba**holes\b", "\ba--\b", "\ba-hole\b", "\babo\b", "\banal\b", "\banus\b", "\bapeshit\b", "\barse\b", "\barsehole\b", "\bass\b", "\bassfucker\b", "\basshole\b", "\bassholes\b", "\basswipe\b", "\bb#tches\b", "\bb%$2h\b", "\bb*#@hes\b", "\bb*****s\b", "\bb***h\b", "\bb**ch\b", "\bb*tch\b", "\bb*tches\b", "\bb.i.t.c.h.\b", "\bb@#$h\b", "\bb____\b", "\bb____es\b", "\bballache\b", "\bballs\b", "\bbangbros\b", "\bbastard\b", "\bbastards\b", "\bbatshit\b", "\bbaw-bag\b", "\bbean-flicker\b", "\bbeaner\b", "\bbeat his meat\b", "\bbeat my meat\b", "\bbeat the meat\b", "\bbeat their meat\b", "\bbeat your meat\b", "\bbeaten his meat\b", "\bbeaten my meat\b", "\bbeaten the meat\b", "\bbeaten their meat\b", "\bbeaten your meat\b", "\bbeating his meat\b", "\bbeating my meat\b", "\bbeating the meat\b", "\bbeating their meat\b", "\bbeating your meat\b", "\bbeats his meat\b", "\bbeats my meat\b", "\bbeats the meat\b", "\bbeats their meat\b", "\bbeats your meat\b", "\bbellend\b", "\bbi**h\b", "\bbi-atch\b", "\bbiiitch\b", "\bbiiittch\b", "\bbitch\b", "\bbitche$\b", "\bbitches\b", "\bbitchin'\b", "\bbitching\b", "\bbitchtits\b", "\bbitchy\b", "\bbloodclaat\b", "\bblow job\b", "\bblow-job\b", "\bblowjob\b", "\bblueballs\b", "\bboned\b", "\bboob\b", "\bboobies\b", "\bboobs\b", "\bbra\b", "\bbrainfuck\b", "\bbreasts\b", "\bbrown-hatter\b", "\bbugger\b", "\bbugger-up\b", "\bbuggered\b", "\bbuggery\b", "\bbukkake\b", "\bbulldyke\b", "\bbulldykes\b", "\bbulls#*t\b", "\bbulls**t\b", "\bbullsh*t\b", "\bbullsh--in'\b", "\bbullshit\b", "\bbullshitter\b", "\bbullshittin\b", "\bbullshitting\b", "\bbullshittin\u2019\b", "\bbum-boy\b", "\bbum-chum\b", "\bbumblefuck\b", "\bbumfuck\b", "\bbumsucker\b", "\bbumtag\b", "\bbutt\b", "\bcabron\b", "\bcannabis\b", "\bcheat\b", "\bcheated\b", "\bcheating\b", "\bchick-with-a-dick\b", "\bchickenshit\b", "\bchinc\b", "\bching-chong\b", "\bchinky\b", "\bchoad\b", "\bchollo\b", "\bcholo\b", "\bclit\b", "\bclitface\b", "\bclitoris\b", "\bclunge\b", "\bclusterfuck\b", "\bcocaine\b", "\bcock\b", "\bcock-up\b", "\bcocksuck\b", "\bcocksucker\b", "\bcocksuckers\b", "\bcocksuckin\b", "\bcocksucking\b", "\bcocksuckin\u2019\b", "\bcockteaser\b", "\bcoolie\b", "\bcuck\b", "\bcuckold\b", "\bcum\b", "\bcumming\b", "\bcumshot\b", "\bcunt\b", "\bcunt-face\b", "\bcunt-hair\b", "\bcunt-hole\b", "\bcunted\b", "\bcuntface\b", "\bcuntfaced\b", "\bcunthead\b", "\bcunts\b", "\bcunty\b", "\bdago\b", "\bdagoes\b", "\bdammit\b", "\bdamn\b", "\bdamnit\b", "\bdarkie\b", "\bdarky\b", "\bdead\b", "\bdeath\b", "\bdick\b", "\bdickbag\b", "\bdickbeaters\b", "\bdickface\b", "\bdickhead\b", "\bdickhole\b", "\bdickjuice\b", "\bdickmilk\b", "\bdickmonger\b", "\bdickslap\b", "\bdicksplash\b", "\bdicksplat\b", "\bdicksucker\b", "\bdicksucking\b", "\bdickwad\b", "\bdickweasel\b", "\bdickweed\b", "\bdickwod\b", "\bdiddyride\b", "\bdied\b", "\bdildo\b", "\bdildos\b", "\bdipsh*t\b", "\bdipsh*ts\b", "\bdipshit\b", "\bdirtbox\b", "\bdonkey-dick\b", "\bdoochbag\b", "\bdothead\b", "\bdouche\b", "\bdouche bag\b", "\bdouchebag\b", "\bdouchebags\b", "\bdrug\b", "\bdumbfuck\b", "\bdumfuck\b", "\bend my life\b", "\berotic\b", "\beye-fuck\b", "\beyefuck\b", "\beyefucked\b", "\beyefucking\b", "\bf#@k\b", "\bf***in\b", "\bf***ing\b", "\bf***in\u2019\b", "\bf***k\b", "\bf**c\b", "\bf**k\b", "\bf**ked\b", "\bf**kin\b", "\bf**king\b", "\bf**kin\u2019\b", "\bf*ck\b", "\bf*ckin\b", "\bf*cking\b", "\bf*ckin\u2019\b", "\bf*cks\b", "\bf---ing\b", "\bf--k\b", "\bf--kin\b", "\bf--king\b", "\bf--kin\u2019\b", "\bf-ck\b", "\bf-ckin\b", "\bf-cking\b", "\bf-ckin\u2019\b", "\bf...\b", "\bf..k\b", "\bf@#k\b", "\bf@ck\b", "\bf\\*\\*c\b", "\bf\\*\\*k\b", "\bf\\*ck\b", "\bface-fuck\b", "\bfacefuck\b", "\bfacesitting\b", "\bfag\b", "\bfaget\b", "\bfaggot\b", "\bfags\b", "\bfannybatter\b", "\bfat-slag\b", "\bfck\b", "\bfcking\b", "\bfckn\b", "\bfcuk\b", "\bfelch\b", "\bfelcher\b", "\bfeltch\b", "\bfizuk\b", "\bfluck\b", "\bfu**er\b", "\bfu*k\b", "\bfuc\b", "\bfucc\b", "\bfuck\b", "\bfuck!\b", "\bfuck'n\b", "\bfucka\b", "\bfuckable\b", "\bfuckbuddy\b", "\bfucked\b", "\bfucken\b", "\bfucker\b", "\bfuckers\b", "\bfuckery\b", "\bfuckhead\b", "\bfuckheads\b", "\bfuckin\b", "\bfucking\b", "\bfuckin\u2019\b", "\bfuckity\b", "\bfuckload\b", "\bfuckmachine\b", "\bfucks\b", "\bfuckstick\b", "\bfucktard\b", "\bfuckton\b", "\bfuckwit\b", "\bfudge-packer\b", "\bfudgepacker\b", "\bfuk\b", "\bfuk'n\b", "\bfukin\u2019\b", "\bfukkk\b", "\bfukload\b", "\bfuktard\b", "\bfur-burger\b", "\bfxxk\b", "\bgang bang\b", "\bgangbang\b", "\bgay-boy\b", "\bgaybob\b", "\bgaydo\b", "\bgaytard\b", "\bgaywad\b", "\bgenital\b", "\bgippo\b", "\bgism\b", "\bgod-damn\b", "\bgoddamm\b", "\bgoddamn\b", "\bgolliwog\b", "\bgolliwogg\b", "\bgyppo\b", "\bhad sex\b", "\bhalfrican\b", "\bhand-job\b", "\bhand-shandy\b", "\bhandjob\b", "\bhard-on\b", "\bhatefuck\b",
#                         "\bhave sex\b", "\bhaving sex\b", "\bhebe\b", "\bheeb\b", "\bhomodunshit\b", "\bhooker\b", "\bhorny\b", "\bhorseshit\b", "\bhymie\b", "\bincel\b", "\binjun\b", "\bintercourse\b", "\bjack-off\b", "\bjap\b", "\bjerk0ff\b", "\bjerk off\b", "\bjerk-off\b", "\bjerking off\b", "\bjerkoff\b", "\bjew-down\b", "\bjewboy\b", "\bjewed\b", "\bjewing\b", "\bjigaboo\b", "\bjism\b", "\bjizz\b", "\bjungle-bunny\b", "\bjunglebunny\b", "\bkike\b", "\bkill\b", "\bkill you\b", "\bkill your\b", "\bkill yourself\b", "\bkill yourselves\b", "\bkilled\b", "\bkilling\b", "\bkills\b", "\bklusterfuck\b", "\bklusterfuk\b", "\bkooch\b", "\bkootch\b", "\bkyke\b", "\blardass\b", "\bleg-over\b", "\blesbo\b", "\blezza\b", "\blezzer\b", "\bloser\b", "\blove-tunnel\b", "\blsd\b", "\bmacaca\b", "\bmanmuck\b", "\bmardarse\b", "\bmarijuana\b", "\bmarmite-miner\b", "\bmarmite-motorway\b", "\bmasterbate\b", "\bmasterbated\b", "\bmasterbates\b", "\bmasterbating\b", "\bmasturbate\b", "\bmasturbated\b", "\bmasturbating\b", "\bmasturbation\b", "\bmexcrement\b", "\bmilf\b", "\bmindfuck\b", "\bminge\b", "\bmoffie\b", "\bmosshead\b", "\bmothafucka\b", "\bmotherf****r\b", "\bmotherf**ker\b", "\bmotherf*cket\b", "\bmotherfuck\b", "\bmotherfucker\b", "\bmotherfuckers\b", "\bmotherfuckin\b", "\bmotherfucking\b", "\bmotherfuckin\u2019\b", "\bmuff-diver\b", "\bmuffdiver\b", "\bmurder\b", "\bmurdered\b", "\bmurdering\b", "\bmurders\b", "\bmuthaf#@ckin\b", "\bmuthaf***as\b", "\bmuthaf**ka\b", "\bmuthafucka\b", "\bmuthafuckas\b", "\bmuthafucker\b", "\bmuthafuckin\b", "\bmuthafucking\b", "\bmuthafuckin\u2019\b", "\bmuthafukas\b", "\bmuthaphuckkin\b", "\bmuthaphukkin\b", "\bmuzzie\b", "\bmy boyfriend\b", "\bmy girlfriend\b", "\bmy husband\b", "\bmy wife\b", "\bn!@@a\b", "\bn****s\b", "\bn***a\b", "\bn***as\b", "\bn***az\b", "\bn***g\b", "\bn*gga\b", "\bn*ggas\b", "\bn*ggaz\b", "\bn----'s\b", "\bN.I.*.*.E.R.\b", "\bn.i.g.g.a.\b", "\bn.i.g.g.a.z.\b", "\bn____\b", "\bnaked\b", "\bnappyhead\b", "\bnasty\b", "\bnazi\b", "\bnazisanazism\b", "\bneedle-dick\b", "\bnegress\b", "\bnegro\b", "\bni**a\b", "\bni**as\b", "\bni@@az\b", "\bnig\b", "\bnig-nog\b", "\bnigaboo\b", "\bnigga\b", "\bniggas\b", "\bniggaz\b", "\bnigger\b", "\bniggers\b", "\bniggs\b", "\bniglet\b", "\bnut-sack\b", "\bnutsack\b", "\borgy\b", "\bp***y\b", "\bp*ssy\b", "\bpaki\b", "\bpaki-bashing\b", "\bpanooch\b", "\bpenis\b", "\bpenisbanger\b", "\bpenises\b", "\bpenisis\b", "\bpenispuffer\b", "\bperve\b", "\bphuck\b", "\bphuk\b", "\bphuq\b", "\bpiccaninny\b", "\bpickaninny\b", "\bpikey\b", "\bpillow-biter\b", "\bpolack\b", "\bpolesmoker\b", "\bpooftah\b", "\bpoonani\b", "\bpoonany\b", "\bpoontang\b", "\bpopery\b", "\bpopish\b", "\bporch-monkey\b", "\bporchmonkey\b", "\bpork-sword\b", "\bporn\b", "\bporngraph\b", "\bporngraphy\b", "\bpornhub\b", "\bporno\b", "\bpornography\b", "\bporns\b", "\bpost raw\b", "\bproddy\b", "\bprostitute\b", "\bpu$$y\b", "\bpu**y\b", "\bpu--y\b", "\bpunani\b", "\bpunanny\b", "\bpunany\b", "\bpurple-headed-warrior\b", "\bpussy\b", "\bPussy-Gang\b", "\bpussylicking\b", "\bpussywhipped\b", "\bPzychobitch\b", "\bqueef\b", "\bqueerbait\b", "\bqueerhole\b", "\bracist\b", "\brag-head\b", "\braghead\b", "\brape\b", "\brim-job\b", "\brimjob\b", "\brimming\b", "\bruski\b", "\brusski\b", "\brussky\b", "\bs#*t\b", "\bs**t\b", "\bsambo\b", "\bschlong\b", "\bschool shooting\b", "\bsex\b", "\bsex life\b", "\bsexual\b", "\bsexy\b", "\bsgw\b", "\bsh$t\b", "\bsh*!\b", "\bsh**\b", "\bsh*t\b", "\bshat\b", "\bshebnon\b", "\bshit\b", "\bshit!\b", "\bshit-ton\b", "\bshitbird\b", "\bshitbiscuit\b", "\bshitburger\b", "\bshitcan\b", "\bshiteater\b", "\bshitface\b", "\bshitfaced\b", "\bshithead\b", "\bshitheaded\b", "\bshitheads\b", "\bshitload\b", "\bshitlord\b", "\bshitmobile\b", "\bshits\b", "\bshitstain\b", "\bshitstorm\b", "\bshitter\b", "\bshitting\b", "\bshittin\u2019\b", "\bshitty\b", "\bshiz\b", "\bshizz\b", "\bshlong\b", "\bshooting\b", "\bskull-fuck\b", "\bskullfuck\b", "\bslopehead\b", "\bslut\b", "\bsmelly-bridge\b", "\bsonofabitch\b", "\bsonovabitch\b", "\bspam-javelin\b", "\bspearchucker\b", "\bsperm\b", "\bsplooge\b", "\bstarfucker\b", "\bstench-trench\b", "\bstiffy\b", "\bstrfkr\b", "\bstrip club\b", "\bstripper\b", "\bsumbitch\b", "\bsupernigger\b", "\btaco-bumper\b", "\btadger\b", "\btaig\b", "\btallywhacker\b", "\btarbrush\b", "\bthot\b", "\bthundercunt\b", "\btifu\b", "\btit\b", "\btits\b", "\btitties\b", "\btodger\b", "\btossa\b", "\btossbag\b", "\btosser\b", "\btowel-head\b", "\btowelhead\b", "\btoxic\b", "\btoxicity\b", "\btranny\b", "\btrump\b", "\btwat\b", "\btwatlips\b", "\btwats\b", "\btwink\b", "\btwot\b", "\btwunt\b", "\bvadge\b", "\bvaginas\b", "\bvagjuice\b", "\bvajazzle\b", "\bvibrator\b", "\bvirgin\b", "\bvirginity\b", "\bvjayjay\b", "\bwack-off\b", "\bwank\b", "\bwanker\b", "\bwant sex\b", "\bweed\b", "\bwetbac\b", "\bwetback\b", "\bwetbacks\b", "\bwetblack\b", "\bwetblacks\b", "\bwhore\b", "\bwigga\b", "\bwigger\b", "\bwilly-woofter\b", "\bwog\b", "\bwogs\b", "\bwolly-woofter\b", "\bwoofter\b", "\bwtf\b", "\byid\b", "rape"]



FILTERED_NAME = ['alexa', 'echo']

MAIN_SPORT_LIST = ["soccer", "cricket", "basketball", "baseball", "tennis", "volleyball", "field Thockey",
                   "football", "table tennis", "golf", "hockey", "rugby", "badminton", "rugby league",
                   "boxing", "dragon boat racing", "gymnastics", "team handball", "bowling", "swimming",
                   "work out", "fitness", "surfing", "fishing", "wrestling", "cycling", "skateboard", "horse racing",
                   "badminton", "handball", "weight lifting", "ski", "water polo", "taekwondo", "rafting", "snorkeling",
                   "skiing", "snowboarding", "ice skating", "marathon", "triathlon", "yoga", "kickboxing", "climbing",
                   "horse riding", "biking" "curling", "karate", "cheerleading", "jogging", "horseback riding",
                   "ping pong", "jai alai", "hiking", "rock climbing", "iron man challenge", "fencing", "run",
                   "track and field", "softball", "track", "shoot", "shooting", "bike", "frisbee", "dance", "dodge ball", "lacrosse", 
                   "swim", "martial arts", "run", "running","dancing"]

BOT_FAV_PLAYERS = {"basektball": {"name": "lebron james", "special_ack": "He is always the king! "},
                   "soccer": {"name": "lionel messi", "special_ack": "He is the miracle of the field! "},
                   "baseball": {"name": "mike trout", "special_ack": "go mike! "},
                   "football": {"name": "tom brady", "special_ack": "Brady! Brady! Brady! "},
                   "hockey": {"name": "sidney crosby", "special_ack": "go crosby! "}
                  }
BOT_FAV_TEAMS = {"basektball": {"name": "the golden state warriors", "special_ack": "go <emphasis level='strong'>dubs</emphasis>! "},
                "soccer": {"name": 'manchester united', "special_ack": "Glory Glory  <emphasis level='strong'>Man United</emphasis>! "},
                "baseball": {"name": "the new york yankees", "special_ack": "go yankees! "},
                "football": {"name": "the new england patriots", "special_ack": "go patriots "},
                "hockey": {"name": "the pittsburgh penguins", "special_ack": "go penguins! "}
                  }


WINTER_SPORT_LIST = ["skiing", "snowboarding",
                     "ice skating", "sledding", "curling"]

GROUND_SPORT_LIST = ["cricket", "boxing"]

LEAGUE_SPORT_MAP = {"nfl": "football", "nhl": "hockey",
                    "mlb": "baseball", "nba": "basketball",
                    "n. f. l.": "football", "n. h. l.": "hockey",
                    "m. l. b.": "baseball", "n. b. a.": "basketball"}

SYS_OTHER_TOPIC_LIST = ['topic_movietv', 'topic_health', 'topic_weather', 'topic_music', 'topic_book', 'topic_holiday', 'topic_food',
                        'topic_techscience', 'topic_finance', 'topic_history', 'topic_game', 'topic_others', 'topic_newspolitics', 'topic_game']  # 'topic_game','topic_animal', 'topic_travel' disabled for now

# Disable MOVIECHAT,MUSICHAT for now, as it is really easy to be triggered
SYS_OTHER_MODULE_LIST = ['WEATHER', 'BOOKCHAT',
                         'TECHSCIENCECHAT', 'GAMECHAT', 'FOODCHAT', 'HOLIDAYCHAT']

SOCCER_TERMS = ['world cup']

TOPIC_KEYWORD_MAP = {'topic_movietv': 'movies',
                     'topic_weather': 'weather',
                     'topic_music': 'music',
                     'topic_book': 'books',
                     'topic_game': 'games',
                     'topic_techscience': 'technology and science',
                     #'topic_travel':'travel',
                     'topic_animal': 'animals',
                     'topic_newspolitics': 'news',
                     'topic_food': 'food'}

SPECIAL_SUBTOPIC = {
    "NEWS": ["corona virus", "covid-19", "covid nineteen", "covid ninteen"]
}

MODULE_KEYWORD_MAP = {
    'MOVIECHAT': 'movies',
    'WEATHER': 'weather',
    'MUSICCHAT': 'music',
    'BOOKCHAT': 'books',
    'GAMECHAT': 'games',
    'TECHSCIENCECHAT': 'technology and science',
    'ANIMALCHAT': 'animals',
    'NEWS': 'news',
    'FOODCHAT': 'food',
    'HOLIDAYCHAT': 'holiday',
}

KEYWORD_MODULE_MAP = {
    'movies': 'MOVIECHAT',
    'weather': 'WEATHER',
    'music': 'MUSICCHAT',
    'books': 'BOOKCHAT',
    'technology and science': 'TECHSCIENCECHAT',  # disable it until version ready
    #'topic_travel': 'TRAVELCHAT',
    'animals': 'ANIMALCHAT',
    'games': 'GAMECHAT',
    'news': 'NEWS',
    'food': 'FOODCHAT',
    'holiday': 'HOLIDAYCHAT',
}

EXCEPTION_KEY_WORDS = ['game', 'games', 'pick', 'television',
                       'tv', 'defense', 'offense', 'outfield', 'john henry', 'good move',
                       'trade', 'money', 'cowboys', 'bee', 'hit',
                       'water polo', 'team', 'seventy sixer', '70 sixer', 'sixer', 'horse', 'jaguars']

EXCEPTION_GOOGLE_KNOWLEDGE_KEY = [
    'planet', 'all', 'american football', 'seventy sixer', 'field goal', 'team', 'head coach', 'professional wrestling']

# exclude the wrong detected ner from NLU pipeline
WRONG_NER = ['whoever barcelona']
WRONG_JUMP_OUT = {"FOODCHAT": ['fun', 'canadian', 'corona', 'stephen curry', 'curry'],
                  "ANIMALCHAT": ['big bear', 'brewers', 'arizona'],
                  'BOOKCHAT': ['brian james']}


class Timeout():
    """Timeout class using ALARM signal."""
    class Timeout(Exception):
        pass

    def __init__(self, sec):
        self.sec = sec

    def __enter__(self):
        signal.signal(signal.SIGALRM, self.raise_timeout)
        signal.alarm(self.sec)

    def __exit__(self, *args):
        signal.alarm(0)    # disable alarm

    def raise_timeout(self, *args):
        raise Timeout.Timeout()


RETRIEVAL_LIST_KEY = "gunrock:ic:retrieval:apikeys"
try:
    with Timeout(1):
        redis_get_key = redis.StrictRedis(
            host='52.87.136.90', port=16517, db=0, password="alexaprize", decode_responses=True)
        # profanity check
        # client = get_client(api_key='xgTjk23SRM9Q9VFrUEFOjav0Bn4ot21W8uFLEieT', timeout_in_millis=1000)
        retrieve_online = True
except Timeout.Timeout:
    retrieve_online = False
    print("retrieve_online initialization problem")

n_titles = 4
n_comments = 1

sent_len_limit = 50
title_len_limit = 30

# zhou doesnt like title templates. looking for usage and removing
title_template = ["some people asked me this, ",
                  "i was thinking about this the other day, ", "this discussion seems relevant: "]
comment_template = [" , and i answered that ",
                    " , and i thought that ", " , and i'm thinking that "]

# cobot profanity_check can give a 2 seconds latency, so used a list instead
blacklist = ["ass", "asshole", "anal", "anus", "arsehole", "arse", "bitch", "bangbros", "bastards,",
             "bitch", "tits", "butt", "blow job", "boob", "bra", "cock", "cum", "cunt", "dick", "shit",
             "sex", "erotic", "fuck", "fxxk", "f\*\*k", "f\*\*c", "f\*ck", "fcuk", "gang bang", "gangbang",
             "genital", "damn", "horny", "jerk off", "jerkoff", "kill", "loser", "masturbate", "naked",
             "nasty", "negro", "nigger", "nigga", "orgy", "pussy", "penis", "perve", "rape", "fucked",
             "fucking", "trump", "racist", "sexy", "strip club", "vagina", "faggot", "fag", "drug", "weed",
             "kill", "murder", "incel", "prostitute", "slut", "whore", "cuck", "cuckold", "shooting", "nazis",
             "nazism", "nazi", "sgw", "balls", "thot", "virgin", "cumshot", "cumming", "dildo", "vibrator", "sexual",
             "douche", "douche", "bukkake", "douchebag", "porn", "porngraph", "porngraphy", "died", "killed",
             "killing", "murdered", "murdering", "kills", "murders", "clit", "clitoris", "mushroom tattoo", "cheat",
             "cheated", "cheating", "dammit", "toxicity", "toxic"]

######################################################Functions for Refactoring#####################################################
def detect_same_fav_entity(current_entity):
    same_entity = None
    same_entity_label = None
    for sport in BOT_FAV_TEAMS.keys():
        bot_current_team = BOT_FAV_TEAMS[sport]["name"]
        bot_current_player = BOT_FAV_PLAYERS[sport]["name"]
        
        if re.search(current_entity['name'], bot_current_team):
            #found the same favorite team
            same_entity = bot_current_team
            same_entity_label = "team"
            return (same_entity, same_entity_label)
        
        if re.search(current_entity['name'], bot_current_player):
            #found the same favorite team
            same_entity = bot_current_player
            same_entity_label = "player"
            return (same_entity, same_entity_label)
    return (same_entity, same_entity_label)

def ground_sport_entity(current_entity, current_sport):
    #First Ground the Current_Entity["name"]
    if current_sport and load_sport_teams(current_sport):
        # try to ground the name entity
        found_team = False
        team_names = load_sport_teams(current_sport)
        for team in team_names:
            if re.search(current_entity["name"], team):
                current_entity["name"] = team
                found_team = True
                break
        # If didn't find a match in current sport
        if not found_team:
            team_names = load_all_sport_teams()
            for team in team_names:
                if re.search(current_entity["name"], team):
                    current_entity["name"] = team
    else:
        team_names = load_all_sport_teams()
        for team in team_names:
            if re.search(current_entity["name"], team):
                current_entity["name"] = team
    return current_entity
#####################################################################################################################################     
# detect question from dialog act


def detect_question_DA(dialog_act):
    r"""
    dialog_act is the dialog_act from Dian, which is a dictionary that contains the text for the sentence segment and the associated dialog act tag
    """
    if (dialog_act["DA"] == "yes_no_question" or dialog_act["DA"] == "open_question_factual" or dialog_act["DA"] == "open_question_opinion" or dialog_act["DA"] == "open_question") and float(dialog_act['confidence']) > 0.5:
        return (dialog_act['text'], True, float(dialog_act['confidence']))
    # elif amz_dialog_act == "Information_RequestIntent" or amz_dialog_act == "Opinion_RequestIntent":
    #     return (dialog_act['text'], True)
    else:
        return (None, False, 0)


def detect_question_DA_update(returnnlp_util, segment_index):
    if returnnlp_util.has_dialog_act([
        DialogActEnum.OPEN_QUESTION,
        DialogActEnum.OPEN_QUESTION_FACTUAL,
        DialogActEnum.OPEN_QUESTION_OPINION,
        DialogActEnum.YES_NO_QUESTION
    ], index=segment_index):
        return True
    else:
        return False


def detect_abandoned_answer_DA(dialog_act, sys_info):
    r"""
    Decode whether users have abandoned dialog_act
    """
    if dialog_act["DA"] == "abandoned" and float(dialog_act["confidence"]) > 0.9 and not sys_info['sys_noun_phrase']:
        return True
    return False


def detect_abandoned_answer_DA_update(returnnlp_util, segment_index, sys_info):
    """
    detect whether  users have abandoned  dialog_act
    """
    if returnnlp_util.has_dialog_act(DialogActEnum.ABANDONED) and not sys_info['sys_noun_phrase']:
        return True
    return False

# Sport Chat


def detect_sport_general_chitchat_qa(asked_questions, selected_chitchat_key):
    talked_questions = access_nested_dict(
        asked_questions, selected_chitchat_key)
    if talked_questions:
        if len(talked_questions) < len(sport_utterance(selected_chitchat_key, get_all=True)):
            return True
    else:
        return True
    return False


def detect_sport_type(utterance):
    for sport in MAIN_SPORT_LIST:
        sport_re = r"\b{sport}".format(sport=sport)
        if re.search(sport_re, utterance.lower()):
            return sport
    return None


def detect_sport_label(utterance):
    for label in ['player', 'team']:
        if re.search(label, utterance.lower()):
            return label
    return None

def detect_command(input_utterance, input_returnnlp):
    """
    Detect whether a command (not a device command) is given from the user
    """
    command_detector = CommandDetector(input_utterance, input_returnnlp)
   
    return command_detector.has_command()

def detect_same_yes_or_no(user_sport_intent, system_response):
    answer_yes_or_no = False
    answer_same = False

    if user_sport_intent['sport_i_answeryes'] or user_sport_intent['sport_i_answerno']:
        answer_yes_or_no = True
        if user_sport_intent['sport_i_answeryes'] and re.search('yes', system_response.lower()):
            answer_same = True
        elif user_sport_intent['sport_i_answerno'] and re.search('no', system_response.lower()):
            answer_same = True

    return (answer_yes_or_no, answer_same)


def detect_ignored_sport(utterance, ignore_pattern):
    r"""
    return a list of detected ignored sport type in utterance
    """
    result = []
    for pattern in ignore_pattern:
        searched_word = re.search(pattern, utterance)
        if searched_word:
            result.append(searched_word.group())
    return result

# Detect sports teams
def detect_teams(input_returnnlp, concept_thred = 0.1, current_sport=None, segment_index=0):
    """
    Detect whether a team is mentioned by the user
    """
    teams = []
    #for segment in input_returnnlp:
    segment = input_returnnlp[segment_index]
    #get the concept
    segment_concepts = segment['concept']
    if segment_concepts is None:
      segment_concepts = []
    for concept in segment_concepts:
        if float(concept['data'].get('professional team','0')) > concept_thred:
            teams.append({"team": concept["noun"], "sport_type": current_sport})
    
    #load major league sports team and check if there are intersection
    major_teams_sports = ["basketball", "baseball", "football", "hockey"]
    if current_sport in major_teams_sports:
        # check if there is intersection between t and text
        #logging.debug("[SPORTS_MODULE] the detect_team, current_sport: {}".format(current_sport))
        #load the corresponding txt
        with open("teams/{}.txt".format(current_sport)) as f:
            loaded_teams = f.readlines()
        #for segment in input_returnnlp:
        segment = input_returnnlp[segment_index]
        current_segment_text = segment["text"]
        for t in loaded_teams:
            t = t.lower()
            #logging.debug("[SPORTS_MODULE] the team from loaded_teams: {}".format(t))
            team_word_set = set(t.split())
            current_segment_word_set = set(current_segment_text.split())
            #check if there is intersection between t and current_segment
            intersection = team_word_set.intersection(current_segment_word_set)
            if intersection:
                teams.append({"team": t, "sport_type": current_sport})
    else:
        for sport in major_teams_sports:
            with open("teams/{}.txt".format(sport)) as f:
                loaded_teams = f.readlines()
            #for segment in input_returnnlp:
            segment = input_returnnlp[segment_index]
            current_segment_text = segment["text"]
            #logging.debug("[SPORTS_MODULE] current segmentation text is: {}".format(current_segment_text))
            if type(current_segment_text) == list:
              current_segment_text = current_segment_text[0]
              logging.debug("[SPORTS_MODULE] current segmentation text is: {}".format(current_segment_text))
            for t in loaded_teams:
                t.lower()
                team_word_set = set(t.split())
                current_segment_word_set = set(current_segment_text.split())
                #check if there is intersection between t and current_segment
                intersection = team_word_set.intersection(current_segment_word_set)
                if intersection:
                    teams.append({"team": t, "sport_type": sport})


    
    return teams

# Create a function to detect human_name
def detect_names(input_returnnlp, concept_thred = 0.1):
    """
    Detect whether a name is mentioned by the user
    """
    names = []
    for segment in input_returnnlp:
        #get the concept
        segment_concepts = segment['concept']
        try:
          for concept in segment_concepts:
              if float(concept['data'].get('name', '0')) > concept_thred or float(concept['data'].get('person', '0')) > concept_thred or float(concept['data'].get('player', '0')) > concept_thred:
                  names.append(concept["noun"])
        except:
          return []
    return names

# Create a function to filter out certain
def filter_entity_terms(entity):
    r"""
        return True if the entity should be filtered
    """
    result = False
    if entity in LEAGUE_SPORT_MAP.keys():
        result = True
    return result


def football_soccer_disambiguater(utterance):
    r"""
    Return True if we should transit football to soccer
    """
    for word in SOCCER_TERMS:
        if re.search(word, utterance.lower()):
            return True
    return False


def get_sys_sport_entity(utterance, sys_info, returnnlp_util=None):
    r'''
    Return the best Quality Name Entity
    '''
    sys_ner = []
    sys_topic_ner = []
    sys_knowledge_ner = []
    knowledge_sport_detected = False

    # get shte noun_phrases
    sys_noun_phrase = sys_info['sys_noun_phrase']

    # if sys_info['sys_ner']:
    #     sys_ner = [x['text'].lower() for x in sys_info['sys_ner'] if x[
    #         'text'].lower() not in MAIN_SPORT_LIST and x[
    #         'text'].lower() not in WRONG_NER]

    # if sys_info['sys_topic']:
    #     if sys_info['sys_topic'][0].get('topicClass', None) == "Sports":
    #         sys_topic_ner = [x['keyword'].lower()
    #                          for x in sys_info['sys_topic'][0]['topicKeywords'] if x[
    #             'keyword'].lower() not in MAIN_SPORT_LIST]

    if returnnlp_util:
        for knowledge in returnnlp_util:
            if knowledge:
                if knowledge.topic_module == 'sports' and (knowledge.result_score > 50):
                    for word in sys_noun_phrase:
                        word = word.lower()
                        if (word not in MAIN_SPORT_LIST) and (word not in EXCEPTION_GOOGLE_KNOWLEDGE_KEY) and re.search(word, knowledge.name.lower()):
                            sys_knowledge_ner.append([knowledge.name,  knowledge.description, knowledge.result_score,  knowledge.topic_module])
                            knowledge_sport_detected = True
                            break


    # Just for checking purpose
    # if sys_knowledge_ner:
    #     logging.info("[SPORT MODULE] the detected google knowledge ner is : {}".format(
    #         sys_knowledge_ner[0]))

    result_ners = []
    if sys_topic_ner or sys_knowledge_ner:
        if sys_knowledge_ner and sys_topic_ner:
            for skn in sys_knowledge_ner:
                for stn in sys_topic_ner:
                    if re.search(stn, skn[0].lower()) or re.search(skn[0].lower(), stn):
                        selected_ner = {}
                        selected_ner['name'] = skn[0] if len(skn[0]) > len(
                            stn) else stn  # keep the longer one
                        selected_ner['label'] = detect_sport_label(skn[1])
                        selected_ner['sport_type'] = detect_sport_type(skn[1])

                        if selected_ner['sport_type'] == 'football' and football_soccer_disambiguater(utterance):
                            selected_ner['sport_type'] = 'soccer'

                        if not filter_entity_terms(selected_ner['name']):
                            result_ners.append(selected_ner.copy())

        if sys_knowledge_ner and sys_ner:
            for skn in sys_knowledge_ner:
                for sn in sys_ner:
                    # Merge the result of entity, and amazon ner together
                    if re.search(sn, skn[0].lower()) or re.search(skn[0].lower(), sn):
                        selected_ner = {}
                        selected_ner['name'] = skn[0] if len(skn[0]) > len(
                            sn) else sn  # keep the longer one
                        selected_ner['label'] = detect_sport_label(skn[1])
                        selected_ner['sport_type'] = detect_sport_type(skn[1])

                        if selected_ner['sport_type'] == 'football' and football_soccer_disambiguater(utterance):
                            selected_ner['sport_type'] = 'soccer'

                        if not filter_entity_terms(selected_ner['name']):
                            result_ners.append(selected_ner.copy())

        if sys_topic_ner and sys_ner:
            for stn in sys_topic_ner:
                for sn in sys_ner:
                    if re.search(sn, stn):
                        selected_ner = {}
                        selected_ner['name'] = stn
                        selected_ner['label'] = None
                        selected_ner['sport_type'] = None
                        if not filter_entity_terms(selected_ner['name']):
                            result_ners.append(selected_ner.copy())
        if sys_knowledge_ner:
            for skn in sys_knowledge_ner:
                selected_ner = {}
                selected_ner['name'] = skn[0]
                selected_ner['label'] = detect_sport_label(skn[1])
                selected_ner['sport_type'] = detect_sport_type(skn[1])

                if selected_ner['sport_type'] == 'football' and football_soccer_disambiguater(utterance):
                    selected_ner['sport_type'] = 'soccer'

                if not filter_entity_terms(selected_ner['name']):
                    result_ners.append(selected_ner.copy())
        '''
        elif sys_ner and 'topic_sport' in sys_info['sys_intent']['topic']:
            selected_ner = {}
            selected_ner['name'] = sys_ner[0]
            selected_ner['label'] = None
            selected_ner['sport_type'] = None
        '''
    #logging.info("[SPORTS] All the detected ners: {}".format(result_ners))
    return result_ners


def ground_question_with_sport_type(utterance, current_sport, detected_sports):
    r"""
    Ground questions that has keyword player, game and team with sport type if no sport type is detected
    """
    result = utterance

    grounding_keyword_list = ["player",
                              "team", "game", "match"]

    word_list = utterance.split()

    if not detected_sports and current_sport:
        for key_word in grounding_keyword_list:
            if key_word in word_list:
                # find key_word index
                key_word_index = word_list.index(key_word)
                # insert the sport type before index
                word_list.insert(key_word_index, current_sport)

                return " ".join(word_list)
    return result


def detect_sport_googlekg(returnnlp_googlekg):
    result = False
    for x in returnnlp_googlekg:
        if x:
            if x.topic_module == 'sports':
                return True
    return False


def get_sys_other_topic(sys_info, detected_sports, utterance,  returnnlp_googlekg):
    r"""
    Return the other topic detected by system while no sport intent is detected
    """
    detect_sport = False
    all_sys_detected_topic = []

    for y in sys_info['sys_intent_2']:
        all_sys_detected_topic += y['topic']

    if 'topic_sport' in all_sys_detected_topic or detect_sport_googlekg(returnnlp_googlekg) or detected_sports or "SPORT" in sys_info["sys_central_elem"]["module"] or detect_teams(sys_info["returnnlp"][0]):
        detect_sport = True

    if not detect_sport:
        # check if the sys_intent list contains other topics
        for topic in sys_info['sys_intent']['topic']:
            if topic in SYS_OTHER_TOPIC_LIST:
                # Check if one of the topic we can talk is detected here
                if TOPIC_KEYWORD_MAP.get(topic, None):
                    return TOPIC_KEYWORD_MAP[topic]
                else:
                    return True

        # check if the sys_topic topic module contains other topics
        for module in sys_info['sys_central_elem']['module']:
            if module in SYS_OTHER_MODULE_LIST:
                if not any(re.search(x, utterance) for x in WRONG_JUMP_OUT.get(module, [])):
                    return MODULE_KEYWORD_MAP.get(module, True)
                #logging.debug("[SPORTS_MODULE] wrong jumpout: {}".format(WRONG_JUMP_OUT.get(module, [])))

        # if sys_info['sys_module'] in SYS_OTHER_MODULE_LIST:
        #     return MODULE_KEYWORD_MAP.get(sys_info['sys_module'], True)

        # If topic is not detected

        # for knowledge in sys_info['sys_knowledge']:
        #     if knowledge:
        #         if knowledge[3] != 'sports' and knowledge[3] != '__EMPTY__' and float(knowledge[2]) > 500 and (knowledge[1].lower() not in EXCEPTION_GOOGLE_KNOWLEDGE_KEY):
        #             return True
    return None


def check_utterance_coherence(sys_info, detected_entities, user_context):
    r"""
    Check if the keywords responded from users is shared with the previous utterance
    """
    previous_response = user_context['previous_system_response']

    # A bit data processing
    if previous_response:
        re.sub(r'_', " ", previous_response)

        if detected_entities['sys_ner']:
            for ner in detected_entities['sys_ner']:
                #logging.info("[SPORTS_MODULE] detected_entities: {}".format(ner))
                if re.search(ner['name'], previous_response.lower()):
                    return True
        elif sys_info['sys_noun_phrase']:
            for np in sys_info['sys_noun_phrase']:
                if re.search(np, previous_response.lower()):
                    return True

    return False


def check_talked_entities(test_entity, talked_entities):
    r"""
    Return True, if none of the repeated entities are found in talked entities. Vice Versa, Return False.
    """
    result = True
    for x in talked_entities:
        if re.search(test_entity, x.lower()):
            return False
    return result
# Sport Response F
# Detect Sport Type in utterance

# Define a function to create a dictionary_item


def create_qa_dict_from_list(qa_list, data):
    r'''
        Create a nested dictionary item from a list, where the list is formatted as:
        [key_1,key_2,..., key_3, value]
        qa_list: List of keys that you want to include in the nested dictionary
        data: the data you want to store in the last key of the dictionary
        return:
            a nested dictionary, with data as its ending data
    '''
    result_dict = {}
    for i, key in enumerate(reversed(qa_list)):
        if i == 0:
            result_dict[key] = data
        else:
            data = result_dict.copy()
            result_dict = {}
            result_dict[key] = data

    return result_dict


def access_nested_dict(nested_dict, key):
    r'''
    Give a list of keys for a nested data, return the data. If the key does not exist return None
    '''
    try:
        data = nested_dict[key[0]]
        for k in key[1:]:
            data = data[k]
        return data
    except Exception as e:
        return None


def update_nesteddict_with_new_key(nested_dict, key, value):
    r'''
    determine which part of the key is missing from the nested_dict
    '''
    data = nested_dict.get(key[0], None)
    if data:
        update_nesteddict_with_new_key(nested_dict[key[0]], key[1:], value)
    else:
        nested_dict[key[0]] = create_qa_dict_from_list(key, value)[key[0]]


def update_nesteddict_with_existing_key(nested_dict, key, updated_value):
    r'''
        Update the existing subset key at the right spot. The updated value should be updated at the end of the keys
    '''
    data = nested_dict[key[0]]
    if isinstance(data, dict):
        update_nesteddict_with_existing_key(
            nested_dict[key[0]], key[1:], updated_value)
    else:
        nested_dict[key[0]].append(updated_value)

#############################################Ground Sport Team Function###


def load_sport_teams(sport_type):
    r"""
    Load the list of teams belonging to a certain sport type
    """
    result = None
    # Define the path to the sport team data
    sport_team_path = 'teams'
    # Check whether sport_type has corresponding files
    team_file_name = '/'.join([sport_team_path, "{}.txt".format(sport_type)])
    if os.path.isfile(team_file_name):
        with open(team_file_name, 'r') as f:
            team_names = f.readlines()
        # clean the data and store it in the result
        result = [x.strip().lower() for x in team_names]

    return result


def load_all_sport_teams():
    r"""
    Load all the existing teams and create a big list of them
    """
    sport_team_path = 'teams'

    result = []
    for file in os.listdir(sport_team_path):
        if file.endswith(".txt"):
            with open('/'.join([sport_team_path, file]), 'r') as f:
                current_teams = f.readlines()
            result += [x.strip().lower() for x in current_teams]
    return result


#############################################API Functions################
def retrieve_reddit(utterance, subreddits, limit):
    # cuurently using gt's client info
    try:
        reddit = praw.Reddit(client_id=REDDIT_CLIENT_ID,
                             client_secret=REDDIT_CLIENT_SECRET,
                             user_agent='extractor',
                             timeout=2
                             # username='gunrock_cobot', # not necessary for read-only
                             # password='ucdavis123'
                             )
        subreddits = '+'.join(subreddits)
        subreddit = reddit.subreddit(subreddits)
        # higher limit is slower; delete limit arg for top 100 results
        submissions = list(subreddit.search(utterance, limit=limit))
        if len(submissions) > 0:
            return submissions
        return None
    except Exception as e:
        logging.error("Fail to retrieve reddit. error msg: {}. user utterance: {}".format(e, utterance))
        return None


def get_interesting_reddit_post_with_timeout(text, limit):
    try:
        with Timeout(2):
            reddit_post = get_interesting_reddit_posts(text, limit)

    except Timeout.Timeout:
        logging.info("get interesting reddit post timeout")
        return None

    return reddit_post


def get_interesting_reddit_posts(text, limit):
    submissions = retrieve_reddit(text, ['todayilearned'], limit)
    if submissions != None:
        results = [submissions[i].title.replace(
            "TIL", "").lstrip() for i in range(len(submissions))]
        results = [results[i] + '.' if results[i][-1] != '.' and results[i][-1] !=
                   '!' and results[i][-1] != '?' else results[i] for i in range(len(results))]
        # Sensitive Words Pattern
        final_results = []
        for result in results:
            # found_profanity = False
            # for w in SENSITIVE_WORDS_LIST:
            #     try:
            #         regex_w = re.compile(w)
            #         if re.search(regex_w, result.lower()):
            #             logging.debug(
            #                 "[SPORTS_REDDIT]profanity word found is: {}".format(w))
            #             found_profanity = True
            #             break
            #     except:
            #         logging.debug(
            #             "[SPORTS_REDDIT]issues with the word: {}".format(w))
            # if not found_profanity:
            #     final_results.append(result)
            found_profanity = BotResponseProfanityClassifier.is_profane(
                result)
            result = result.lower()
            #logging.debug("[SPORTS_MODULE] found reddit result is: {}".format(result))
            found_exact_text = re.search(text, result)
            #logging.debug("[SPORTS_MODULE] search query is: {}".format(text.strip()))

            #logging.debug("[SPORTS_MODULE] search query is: {}".format(text=="utah jazz"))
            #logging.debug("[SPORTS_MODULE] found exact same text is: {}".format(re.search(text, result)))
            if not found_profanity and found_exact_text:
                final_results.append(result)

        return final_results
    return []

# Define the Client for NewsBot
# Get the News Client


def get_popular_sport_topics():
    news_bot = RNews_Client(addr='52.87.136.90')
    pop_themes = news_bot.get_pop_theme_with_timeout(
        'sports', top=5, timeout=2)
    return pop_themes


# results = get_interesting_reddit_posts('sandwich', limit=2)
# print(results)


def get_KG_news(entity, keyword=[], category=[]):
    r"""
Extract News from KG provided from Kevin
Input:
    entity: A list of entity names,
    keyword: A list of keywords to limit the search
"""
    news = kgapi_events(entity, keyword, category)
    if news:
        return news
    else:
        return None


def post_process_KG_news(content):
    content = content.lower()
    A = re.findall(r"\.{3,}", content)
    B = re.findall(r"[(].*[)]", content)
    if A:
        content = content.replace(A[0], '.')
    if B:
        content = content.replace(B[0], '')
    return content


def get_retrieval_response(utterance, sub=None):
    keys = redis_get_key.brpoplpush(RETRIEVAL_LIST_KEY, RETRIEVAL_LIST_KEY)
    c_id, c_secret, u_agent = keys.split("::")
    r = praw.Reddit(client_id=c_id, client_secret=c_secret, user_agent=u_agent, timeout=2)

    try:
        if sub == None:
            submission = r.subreddit('all').search(utterance, limit=n_titles)
        else:
            submission = r.subreddit(sub).search(utterance, limit=n_titles)

        t = 0
        weight = 1.5
        weight_decay = 0.6
        comments = []
        for s in submission:
            s.comment_limit = n_comments
            s.comment_sort = "top"
            t += 1
            if t > n_titles:
                break
            ups = int(s.ups)
            up_ratio = float(s.upvote_ratio)
            real_ups = ups

            if real_ups == 0:
                real_ups = 1000  # for scaling purpose

            if ups > 100:
                ups = 1.21
            elif ups > 30:
                ups = 1.1
            elif ups > 10:
                ups = 1
            else:
                ups = 0.9
            title = s.title
            # title = title + " " + s.selftext
            # TODO: add s.selftext, which is the detailed content of the
            # post(title)
            if profanity_check(title):
                continue
            c = 0
            for top_level_comment in s.comments:
                if c >= n_comments:
                    break
                top1_comment = top_level_comment.body
                if top_level_comment.author != None:
                    if top_level_comment.author.name == "AutoModerator":
                        break
                # print(top1_comment)
                # if profanity_check(top1_comment):
                #     continue
                top1_comment = filter_profanity(top1_comment)
                score = int(top_level_comment.score)

                # sentiment analysis, base = 2, give higher score to positive title+comment
                # title_sentiment_score = sid.polarity_scores(title)
                # comment_sentiment_score = sid.polarity_scores(top1_comment)
                # # more weights on the comments
                # totle_sentiment_score = title_sentiment_score["pos"] - title_sentiment_score["neg"] + 2 * (comment_sentiment_score["pos"] - comment_sentiment_score["neg"])
                # sentiment_weight = float(2 + totle_sentiment_score)
                sentiment_weight = 1
                comments.append((sentiment_weight * weight * (ups *
                                                              up_ratio * score / real_ups), (title, top1_comment)))
                c += 1
                weight *= weight_decay  # give more weights to the most relevant post

        comments = sorted(comments, key=lambda x: x[0], reverse=True)
        # print(comments)
        if len(comments) > 0:
            title1 = process_retrieve(comments[0][1][0], True)
            if title1 != "well i have a lot of thoughts on this, but it's not gonna be easy to talk about it now unless you are free until tomorrow night. maybe we should talk about something else":
                comment1 = process_retrieve(comments[0][1][1])
            #     template_idx = random.randint(0, len(title_template)-1)
            #     #removed the title that zhou doesn't like and the comment has to go since it cannot be mainted to be grammically correct or
            #     #of relevant value. ie the response 'its so obvious'
            #     response = title1
                response = title1 + '. ' + comment1
            else:
                response = title1
        else:
            response = None

    except:
        response = None

    return response


def filter_profanity(text):
    sentences = text.split(".")
    filtered_text = ""
    for sent in sentences:
        good = True
        for w in blacklist:
            if re.search(r"\b%s\b" % w, sent.lower()):
                good = False
                break
        if good:
            filtered_text = filtered_text + sent + "."
    return filtered_text


def profanity_check(text):
    # profanity check built by the client
    # r = client.batch_detect_profanity(utterances=[text])
    # the "1" here means the source is the statistical_model where if it is 0, it would be blacklist
    # return r["offensivenessClasses"][0]["values"][1]["offensivenessClass"]
    # return 1 means it is offensive; 0 means not offensive

    for w in blacklist:
        if re.search(r"\b%s\b" % w, text.lower()):
            return 1
    return 0


def process_retrieve(text, title=False):
    text = re.sub(r"https\S+", "", text)
    text = re.sub(r"http\S+", "", text)
    text = text.replace("\n", ". ").strip()

    limit = title_len_limit if title else sent_len_limit

    num_words = len(text.split())
    spec_char = '#&\()*+-/:;<=>@[\\]^_`{|}~'
    if num_words <= 1:
        # fixed because zhou hates the previous template.
        return ". What do you think? I haven't made up my mind yet."
    if num_words <= sent_len_limit:
        text = re.sub('[%s]' % re.escape(spec_char), '', text)
        return text
    else:
        # print(text)
        sentences = re.split('\?|\.|\,|\!|\n', text)
        sentences = [re.sub('[%s]' % re.escape(spec_char), '', s)
                     for s in sentences]
        word_in_sent = 0
        for i in range(len(sentences)):
            word_in_sent += len(sentences[i].split())
            if word_in_sent > sent_len_limit:
                if i == 0:
                    return ". There are too many different opinions on this for me to decide. " \
                           "But I'm curious, can you tell me yours? "
                else:
                    return ". ".join(word for word in sentences[:i + 1])

# Functions to Extract the Sport Moment


def get_sport_moment_data():
    """
    Type is the moment type like Today, SPorts, News, Entertainment. One of 5 tags.
    :param type: can be news, sports, today, entertainment, fun must be lowercase!
    :return: list of items by date. Up to you to truncate and sort by likes
    """
    type_tag = 'sports'
    response = table.query(
        # ProjectionExpression= "id,name,score,sub,title,opinion,related",
        IndexName='type',
        KeyConditionExpression=Key('type').eq(type_tag),
        ScanIndexForward=False
    )
    resp = response['Items']
    # print(type(resp))
    return resp


def get_moment_source_lookup(source):
    """
    Source is the tag that is occasionally attached to moment like politics
    :param source: any tag from the get keywords function
    :return: list of events sorted by date
    """
    # response = table.query(
    #     ProjectionExpression="title, #desc, #source, #type, opinions",
    #     ExpressionAttributeNames={"#desc": "desc",
    #                               "#source": "source",
    #                               "#type": "type", },
    #     IndexName='source',
    #     KeyConditionExpression=Key('source').eq(source),
    #     ScanIndexForward=False
    # )
    result = []
    try:
        response = table.query(
            ProjectionExpression="title, #desc, #source, #type",
            ExpressionAttributeNames={"#desc": "desc",
                                      "#source": "source",
                                      "#type": "type", },
            IndexName='source',
            KeyConditionExpression=Key('source').eq(source),
            ScanIndexForward=False
        )
        resp = response['Items']

        for item in resp:
            if item['type'] == 'sports':
                result.append(item)
    except Exception as e:
        logging.error(
            "[SPORTSMODULE][ERROR] While querying the moments table {}".format(e), exc_info=True)

    return result


def get_moment_title_lookup(np, limit=2):
    """
    Scans Db for a list of people or words in the title of the moment
    :param np: list of people or words
    :return: list ordered by date of moments
    """
    SCANLIMIT = 40
    start = time.time()
    if len(np) > 1:
        #fe = Attr('title').contains(np[0].lower()) & Attr('title').contains(np[1].lower())
        fe = Attr('title').contains(np[0].lower())
        for word in np[1:]:
            fe = fe & Attr('title').contains(word.lower())

    elif np:

        fe = Attr('title').contains(np[0].lower())
    else:
        return []

    response = table.scan(
        FilterExpression=fe,
    )

    resp = response['Items']
    i = 1
    while 'LastEvaluatedKey' in response:
        response = table.scan(ExclusiveStartKey=response[
                              'LastEvaluatedKey'], FilterExpression=fe)
        resp.extend(response['Items'])
        stop = time.time()
        elapse = stop - start
        if len(resp) > 5 or elapse > 1:
            break
        i += 1
    stop = time.time()

    # Filter the resp by specifying the type
    for r in resp:
        if r['type'] != 'sports':
            resp.remove(r)
    '''
    if len(resp) > limit:
        resp = resp[:limit]
    '''

    return resp


def get_moment_desc_lookup(np, limit=2):
    """
    Scans Db for a list of people or words in the title of the moment
    :param np: list of people or words
    :return: list ordered by date of moments
    """
    SCANLIMIT = 40
    start = time.time()
    if len(np) > 1:
        #fe = Attr('title').contains(np[0].lower()) & Attr('title').contains(np[1].lower())
        fe = Attr('desc').contains(np[0].lower())
        for word in np[1:]:
            fe = fe & Attr('desc').contains(word.lower())

    elif np:

        fe = Attr('desc').contains(np[0].lower())
    else:
        return []

    response = table.scan(
        FilterExpression=fe,
    )

    resp = response['Items']
    i = 1
    while 'LastEvaluatedKey' in response:
        response = table.scan(ExclusiveStartKey=response[
                              'LastEvaluatedKey'], FilterExpression=fe)
        resp.extend(response['Items'])
        stop = time.time()
        elapse = stop - start
        if len(resp) > 5 or elapse > 1:
            break
        i += 1
    # Filter the resp by specifying the type
    for r in resp:
        if r['type'] != 'sports':
            resp.remove(r)

    stop = time.time()
    '''
    if len(resp) > limit:
        resp = resp[:limit]
    '''
    return resp


def get_moment_keywords():
    """
    This gives all keys that are in the database for the source tag. THis is the tag like politics
    :return: super useful list of source tags
    """
    v = REDIS_R.hget(KEYWORD_PREFIX, 'keys')
    return json.loads(v)


def format_moment_contents(moment_responses):
    """
    Format the list of moment_responses dictionary type to a list of strings that we
    can use as the utterance response.
    return moments: list of the moments we use for utterance
    """
    processed_moments = []
    for m in moment_responses:
        m_title = m['title']  # str(m['title'],'utf-8')
        m_description = m['desc']  # str(m['desc'],'utf-8')
        m_result = '. '.join([m_title, m_description])
        processed_moments.append(m_result)
    processed_moments = list(set(processed_moments))
    return processed_moments


def search_moment_by_keywords(keyword, limit=2):
    '''
        Search moments with keyword from both title and content.
        Return a list of processed moments that can be directly applied to
        utterances.
    '''
    try:
        # Search Moment with Keywords
        moments = get_moment_title_lookup([keyword])
        if not moments:
            moments = get_moment_desc_lookup([keyword])
        # # Back Up Option
        # if not moments:
        #     moments = moment_search(keyword)
        processed_moments = format_moment_contents(moments)
        if len(processed_moments) > limit:
            processed_moments = processed_moments[:limit]
    except:
        return []
    return processed_moments


def search_moment_by_sport_type(sport_type, limit=2):
    try:
        corresponding_moment_tags = SPORT_MOMENT_TAG_MAP.get(sport_type, [])
        moments = []
        # print(corresponding_moment_tags)
        for tag in corresponding_moment_tags:
            moments += get_moment_source_lookup(tag)
        processed_moments = format_moment_contents(moments)
        if len(processed_moments) > limit:
            processed_moments = processed_moments[:limit]
    except:
        return []

    return processed_moments


def search_moment_by_sport_type_full(sport_type, limit=10):
    '''
        Search Moments with sport type and return a list of full moment objects.
    '''
    # TODO: Sort the moments with respect to time stamp
    try:
        corresponding_moment_tags = SPORT_MOMENT_TAG_MAP.get(sport_type, [])
        moments = []
        # print(corresponding_moment_tags)
        for tag in corresponding_moment_tags:
            moments += get_moment_source_lookup(tag)
        #processed_moments = format_moment_contents(moments)
        if len(moments) > limit:
            moments = moments[:limit]
    except:
        return []

    return moments
############################################Extract Moment Opinions#######


def opinion_mining_filter(raw_tweet):
    r'''
    Process the moment opinion content. Return a processed content, which will remove
    url, hashtags and get rid all the posts with "@" in it.
    '''
    #result = re.sub(r'\S*twitter.com\S*','',raw_tweet).strip()
    result = raw_tweet
    for filter_pattern in MOMENT_OPINION_FILTER_PATTERNS:
        result = re.sub(filter_pattern, ' ', result)

    for remove_pattern in MOMENT_OPINION_REMOVE_PATTERNS:
        if re.search(remove_pattern, result):
            return None
    return result


def sentence_similar(a, b):
    return SequenceMatcher(None, a, b).ratio()


def select_moment_opinions(moment):
    r'''
    Pick the opinions from the tweets around a moment. We first reformat the text by removing url, hashtags and linebreakers.
    Then, we filter out the moments with @, as we don't have a good way to process these twitter accounts. Some of them sound really awkward.
    Finally, we will check the opinion_score using textblob's sentiment classifier. Based on a small set of data, we think the Threshold of 0.7 is good.
    We will conduct experiment to further evaluate the sentiment classifier on picking approrpiate contents.
    We also filter out the tweets sharing a high similarity to the title. So afar we have the similarity Threshold 0.5
    In the end of the function we will return a list of opinions that can be used to interact with users.
    '''
    # TODO: Include a filter of black list
    try:
        opinion_tweets_list = []
        for moment_tweet in moment['tweets']:
            tweet = opinion_mining_filter_tweets(moment_tweet['tweet_text'])
            if tweet:
                subjectivity_score = abs(TextBlob(tweet).sentiment[
                                         0]) + TextBlob(tweet).sentiment[1]
                if subjectivity_score >= MOMENT_OPINION_SCORE_THRESHOLD and sentence_similar(tweet_title, tweet) <= MOMENT_OPINION_SIMILARITY_THRESHOLD:
                    opinion_tweets_list.append(tweet)
    except:
        return []

    return opinion_tweets_list

# Generate Question Error_Handle_Response##################


def user_utterance_paraphrase(user_utterance):
        # Replacement_Rules
    replacement_rules = [(r"\bare you\b", "am i"), (r"\bi was\b", "you were"), (r"\bi am\b", "you are"), (r"\bwere you\b", "was i"),
                         (r"\bi\b", "you"), (r"\bmy\b",
                                             "your"), (r"\bmyself\b", "yourself"),
                         (r"\byou\b", "i"), (r"\byour\b", "my"),
                         (r"\bme\b", "you")]

    paraphrase_response = user_utterance
    # Replace a couple of things
    for rule in replacement_rules:
        if re.search(rule[0], user_utterance):
            paraphrase_response = re.sub(rule[0], rule[1], paraphrase_response)
            break

    return paraphrase_response


def unable_answer_question_error_handle(user_utterance, user_context, sys_info, user_attributes):
    r"""
    Generate different acknowledgement of questions
    (1) If the length of the question is less than 10 words, paraphrase; otherwise paraphrase the longest noun_phrase
    (2) If the question is a backstory question, strategy, keep it as a secret for now.
    (3) If the question is an yes_or_no question
    (4) elif the question is asking opinion, then say that you have never thought about it before. ask his thought.
    (5) if the question is a fact question, then admit you don't know the answer and tell user that you will get back to him later.
    """
    # initialize error_handle_response
    error_handle_response = ""
    detected_noun_phrase = sys_info.get('sys_noun_phrase', [])
    detected_noun_phrase.sort(key=lambda s: len(s.split()))
    detected_noun_phrase.reverse()
    # initilaize returnnlp
    # returnnlp = sys_info['features']['returnnlp']
    # initialize the question type
    detected_question_types = user_context["detected_question_types"]

    # paraphrase the question when the length of the question is less than 10
    # elif there is noun_phrase, paraphrse that noun_phrase
    if len(user_utterance.split()) < 10:
        error_handle_response = user_utterance_paraphrase(user_utterance) + '?'
    elif detected_noun_phrase:
        error_handle_respoinse = user_utterance_paraphrase(
            detected_noun_phrase[0]) + '?'

    backstory_tag_list = ["topic_backstory", "ask_hobby", "ask_preference"]

    # Check if the question is a backstory question
    if any(x in detected_question_types for x in backstory_tag_list):
        unable_handle_backstory_question_response = template_sports.utterance(selector='grounding/unable_handle_backstory_question', slots={},
                                                                              user_attributes_ref=user_attributes)
        error_handle_response = ' '.join(
            [error_handle_response, unable_handle_backstory_question_response])

    elif "yes_no_question" in detected_question_types and (re.search(r"\byou\b", user_utterance) or re.search(r"\byour\b", user_utterance)):
        # Try to just say no I am not
        if re.search(r"^do", user_utterance):
            unable_handle_yes_or_no_question_response = "no, i don't."
        elif re.search(r"^can", user_utterance):
            unable_handle_yes_or_no_question_response = "sorry, i can't."
        elif re.search(r"^would", user_utterance):
            unable_handle_yes_or_no_question_response = "no, i wouldn't sorry."
        elif re.search(r"^did", user_utterance):
            unable_handle_yes_or_no_question_response = "no, i did't."
        elif re.search(r"^could", user_utterance):
            unable_handle_yes_or_no_question_response = "sorry, i couldn't."
        else:
            unable_handle_yes_or_no_question_response = "sorry, but no."

        error_handle_response = ' '.join(
            [error_handle_response, unable_handle_yes_or_no_question_response])
    elif "open_question_opinion" in detected_question_types:
        unable_handle_opinion_question_response = template_sports.utterance(selector='grounding/unable_handle_opinion_question', slots={},
                                                                            user_attributes_ref=user_attributes)
        error_handle_response = ' '.join(
            [error_handle_response, unable_handle_opinion_question_response])
    else:
        unable_handle_question_general_response = template_sports.utterance(selector='grounding/unable_handle_question_general', slots={},
                                                                            user_attributes_ref=user_attributes)
        error_handle_response = ' '.join(
            [error_handle_response, unable_handle_question_general_response])

    # Add a break in the end
    error_handle_response = ' '.join(
        [error_handle_response, "<break time='600ms'/>"])

    return error_handle_response


def get_backstory_or_evi_response(utterance, a_b_test):
    backstory = get_backstory_response_text(utterance, a_b_test, 0.8)
    if backstory:
        return backstory

    return get_evi_response(utterance)


def get_evi_response(utterance):
    evi_client = get_client(api_key=COBOT_API_KEY)
    try:
        r = evi_client.get_answer(
            question=utterance, timeout_in_millis=1000)
    except:
        logging.info("Fail to get evi from evi API.")
        return None

    if r["response"] == "" or r["response"].startswith('skill://') or re.search(r"an opinion on that", r['response']):
        return None

    response = postprocessing_evi_response(r["response"])
    logging.debug("Evi response: {}".format(response))

    return response


def postprocessing_evi_response(evi_response):
    r"""
    Some hard reformat of evi_response
    """

    # 1. Remove the "for more information response"
    evi_response = evi_response.lower()

    if re.search("wikihow", evi_response):
        evi_response = re.sub(r"for more details.*", "", evi_response)
        evi_response = re.sub(
            r"here's something i found on wikihow", "", evi_response)

    filter_list = ["alexa", "'that' is usually defined as"]

    # get rid of the response that include alexa
    for x in filter_list:
        if re.search(x, evi_response):
            return None

    # get rid of extremely short response from alexa
    if len(evi_response.split()) < 3 or len(evi_response.split()) > 50:
        return None

    return evi_response


'''
if __name__ == "__main__":
    # EVI_client = get_client(api_key=COBOT_API_KEY)
    # r = EVI_client.get_answer(
    #     question="best wrestling player", timeout_in_millis=1000)
    # print(r["response"])

    #DONT FORGET TO ADD THESE LINES INTO YOUR DOCKER!!!!
    # ENV AWS_ACCESS_KEY_ID AKIAJ2G47XHAFTXN77RA
    # ENV AWS_SECRET_ACCESS_KEY eMOLgTSjh1NEeqqDwOMPBdjz6d+EDiDkUPB346vv
    # ENV AWS_DEFAULT_REGION us-east-1

    #Examples
    #rint(len(get_sport_moment_data()))
    pprint(search_moment_by_sport_type_full('football'))
    #pprint(get_moment_title_lookup(['alexander zverev'])) #Let's start with full name search.
    #print(title_lookup(['chris pine']))
    #pprint(get_moment_source_lookup('nba'))
    #print(get_moment_keywords())
    #raw_moments = get_moment_title_lookup(['luke shaw'])
    #pprint(raw_moments)
    #processed_moments = format_moment_contents(raw_moments)
    #print(processed_moments)
    #processed_moments = search_moment_by_keywords('luke shaw')
    #print(processed_moments)

    #moments = search_moment_by_sport_type_full('football')
    #pprint(moments[0])
'''

    
