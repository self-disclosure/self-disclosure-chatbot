import requests
from urllib.parse import quote

def kgapi_events(entities: list, keywords:list = None, category:list = None) -> list:
    """
    :param entities: a list of entities. maximum number of entities is 2 for now
    :param keywords: optional list of keywords to match events. good for filtering events
    :return: a list of events between one or two entities
    """
    if not entities: return []
    try:
        ents = '+'.join(entities)
        keys = ''
        if keywords:
            keys += '?keywords='+'-'.join(keywords) + '&'
        cats = ''
        if category:
            if not keywords:
                cats += '?'
            cats += 'category=' + '-'.join(category)
        query = ents + keys + cats
        query = quote(query, safe='+?=')
        print(query)
        r = requests.get('http://35.168.206.27/api/v1.0/events/'+query)
        return r.json()
    except Exception as e:
        return []

def kgapi_entity(entities: list, filter:str = None) -> list:
    """
    :param entities: a list of entities. maximum number of entities is 2 for now
    :param filter: optional string label to filter entities. can be person, location, organization
    :return: list of related entities
    """
    if not entities: return []
    try:
        ents = '+'.join(entities)
        filt = ''
        if filter:
            filt = '?filter='+str(filter)
        query = ents + filt
        query = quote(query, safe='+?=')
        # print(query)
        r = requests.get('http://35.168.206.27/api/v1.0/entity/'+query)
        return r.json()
    except Exception as e:
        return []
'''
##Example Usage
if __name__ == "__main__":
    print(entity(['lebron james'], filter='organization'))
    print()
    print()
    print(events(['lebron james', 'kawhi'], keywords=['cris','carter']))
'''