
from sport_utterance_data import *
import re
from sport_utils import *
import template_manager
# define template_sports to generate response
template_sports = template_manager.Templates.sports
UNKNOWN_SPORTS_TEMPLATES_UPPERBOUND = 3


class Sport_Acknowledge(object):

    def __init__(self, user_utterance, user_attributes, user_intent, context, sys_info):
        self.user_utterance = user_utterance
        self.user_attributes = user_attributes
        self.user_intent = user_intent
        self.context = context
        self.sys_info = sys_info
        self.selected_chitchat_key = context['selected_chitchat_key']
        self.selected_chitchat_id = context['selected_chitchat_id']
        if context['selected_question']:
            self.selected_question = context['selected_question']
        else:
            self.selected_question = None

        # Bind the Acknowledge Function.
        self.__bind__acknowledge()

    def __bind__acknowledge(self):
        r'''
        The key should match the key defined in sport_utterance_data
        '''
        self.acknowledge_func = {
            "ask_interest_entity": self.acknowledge_interest_entity,
            "ask_interest_player": self.acknowledge_interest_player,
            "ask_baseball_position": self.acknowledge_baseball_position,
            "ask_personal_experience": self.acknowledge_personal_experience,
            "ask_personal_opinion": self.acknowledge_personal_opinion,
            'ask_yes_or_no': self.acknowledge_yes_or_no,
            'ask_interest_sport_type': self.acknowledge_interest_sport_type,
            'ask_play_sport_yes_no': self.acknowledge_play_sport_yes_no,
            'ask_when_play_sport': self.acknowledge_when_play_sport,
            'ask_who_play_sport_with': self.acknowledge_who_play_sport_with,
            'ask_sport_popular_yes_no': self.acknowledge_sport_popular_yes_no,
            'ask_favorite_player_yes_no': self.acknowledge_favorite_player_yes_no,
            'ask_interest_entity_unknown_sport': self.acknowledge_interest_entity_unknown_sport,
            'ask_jog_reason': self.acknowledge_ask_jog_reason,
            'ask_jog_path': self.acknowledge_ask_jog_path,
            'ask_jog_distance': self.acknowledge_ask_jog_distance,
        }


    def get_acknowledge(self, acknowledge_type):
        # if acknowledge_type == "none":
        #     return ""
        # special acknowledge for 
        acknowledge_special, sys_ack_bool = self.acknowledge_special_instance()

        if acknowledge_type:
            acknowledge_qa = self.acknowledge_func[acknowledge_type]()
        else:
            acknowledge_qa = ""
        
        #merge the response
        if acknowledge_special and acknowledge_qa and not sys_ack_bool:
            if self.user_intent["sport_intents"]["sport_i_suggestentity"] and acknowledge_type in ["ask_interest_player", "ask_interest_entity"]:
                acknowledge_response = acknowledge_qa
            else:
                acknowledge_response = ' '.join([acknowledge_special, acknowledge_qa])
        elif acknowledge_special:
            acknowledge_response = acknowledge_special
        elif acknowledge_qa:
            acknowledge_response = acknowledge_qa
        else:
            acknowledge_response = ""

        return acknowledge_response

    def acknowledge_special_instance(self):
        """
        New function to handle acknowledgement for general cases, such as acknowledgement on intent or a certin previous context condition is met. 
        """
        #Return system acknowledgement for certain output_tag
        #logging.debug("[SPORTS_MODULE] system_acknowledgement output_tag: {}".format(self.sys_info["system_acknowledgement"].get("output_tag", "")))
        if self.sys_info["system_acknowledgement"].get("output_tag", "") and (self.sys_info["system_acknowledgement"].get("output_tag", "") not in ["ack_question_idk", "ack_hobbies", "ack_general"]):
            acknowledge_special = self.sys_info["system_acknowledgement"]["ack"]
            return acknowledge_special, True


        if self.context["previous_state"] == "s_sport_favorite_sport_confirmation" and self.context["num_error_status"] > 0:
            acknowledge_special = template_sports.utterance(selector='grounding/appologize_for_missing_interest_too_many', slots={},user_attributes_ref=self.user_attributes)
            return acknowledge_special, False
        
        #acknowledgement due to intent
        if self.user_intent["sport_intents"]["sport_i_suggestentity"]:
            acknowledge_special = self.acknowledge_suggestsportentity()
        elif self.user_intent["sport_intents"]["sport_i_suggestsporttype"]:
            logging.debug("[SPORTS_MODULE] enter the logic of generating suggestsporttype ack")
            acknowledge_special = self.acknowledge_suggestsporttype()
        else:
            acknowledge_special = ""
        return acknowledge_special, False

    def acknowledge_suggestsporttype(self):
        r"""
        Acknowledge on suggest sport type
        """
        current_sport = self.context["current_sport"]
        talked_sports = self.context["talked_sports"]
        asked_questions = self.context["asked_questions"]
        total_current_sport_templates = template_sports.count_utterances(selector="qa_templates/sport_type/qa_{}/general".format(current_sport)) if current_sport in KNOWN_SPORTS else UNKNOWN_SPORTS_TEMPLATES_UPPERBOUND
        #acknowledge the case when we are running out of tempaltes for a sport when user still request for it.
        if asked_questions.get(current_sport, 0) == total_current_sport_templates:
        #TODO: incldue the case when KNOWN_SPORTS is running out of tempaltes
            acknowledge_special = template_sports.utterance(selector='grounding/run_out_of_sport_type_templates', slots={'sport_type': current_sport}, user_attributes_ref=self.user_attributes)
        elif current_sport in KNOWN_SPORTS:
            acknowledge_special = template_sports.utterance(selector='grounding/intro_sport', slots={'sport_type': current_sport}, user_attributes_ref=self.user_attributes)
        else:
            acknowledge_special = template_sports.utterance(selector='grounding/intro_unknown_sport', slots={'sport_type': current_sport}, user_attributes_ref=self.user_attributes)

        return acknowledge_special
    def acknowledge_suggestsportentity(self):
        r"""
        Acknowledge on suggested sport entity. 
        1. Acknowledge same favorite player and same favorite team
        2. Acknowledge the entity name
        """
        current_entity = self.context["current_entity"]
        current_sport = self.context["current_sport"]
        assert current_entity, "current_entity cannot be None"
        
        logging.debug("[SPORTS_MODULE] current entity in ack_suggestentity is: {}".format(current_entity))
        #1. same favorite player or same favorite team is detected
        if detect_same_fav_entity(current_entity)[0] is not None:
            same_fav_entity = detect_same_fav_entity(current_entity)
            #found same fav_entity
            if same_fav_entity[1] == "team":
                acknowledge_special = template_sports.utterance(selector='grounding/same_fav_team', slots={'sport_entity': current_entity["name"]}, user_attributes_ref=self.user_attributes)
            else:
                acknowledge_special = template_sports.utterance(selector='grounding/same_fav_player', slots={'sport_entity': current_entity["name"]}, user_attributes_ref=self.user_attributes)
        else:
            acknowledge_special =  template_sports.utterance(selector='grounding/confirm_known_entity', slots={'sport_entity': current_entity["name"]}, user_attributes_ref=self.user_attributes)
        return acknowledge_special





    def acknowledge_paraphrase(self):
        # Replacement_Rules
        replacement_rules = [(r"\bare you\b", "am i"), (r"\bi was\b", "you were"), (r"\bi am\b", "you are"), (r"\bwere you\b", "was i"),
                         (r"\bi\b", "you"), (r"\bmy\b",
                                             "your"), (r"\bmyself\b", "yourself"),
                         (r"\byou\b", "i"), (r"\byour\b", "my"), 
                         (r"\bme\b", "you")]

        paraphrase_response = self.user_utterance
        # Replace a couple of things
        for rule in replacement_rules:
            if re.search(rule[0], self.user_utterance):
                paraphrase_response = re.sub(rule[0], rule[1], paraphrase_response)
                break
        return paraphrase_response

    def acknowledge_interest_entity(self):
        r'''
        Acknowledge For the question that asked about user's interest entity, for example
        what team are you cheering for. If not interupted to some other higher responses, 3 cases.
        (1) User answered with a name entity that is the same as ours.
        (2) User answered with a name entity that is not the same as ours
        (2) User does not have one or he is uncertain or he answered something else.
        '''
        if self.selected_question:
            selected_question = self.selected_question
        else:
            selected_question = sport_utterance(self.selected_chitchat_key, get_all=True)[
                self.selected_chitchat_id]

        selected_question_answer = random.choice(selected_question['a']).lower()
        
        detected_teams = detect_teams(self.sys_info['returnnlp'][0])

        if self.context['detected_entities']['sys_ner'] or self.context['detected_entities']['sport_ner']:
            detected_entities = self.context['detected_entities']['sport_ner'][:] if self.context[
                'detected_entities']['sport_ner'] else self.context['detected_entities']['sys_ner'][:]

            if any(re.search(x['name'], selected_question_answer) for x in detected_entities):
                # Same Entity
                acknowledge_key = ["acknowledge_same_entity"]
                acknowledge_response = sport_utterance(acknowledge_key)['text']
            else:
                # Different Entity
                acknowledge_key = ["acknowledge_different_entity"]
                acknowledge_response = sport_utterance(
                    acknowledge_key)['text'].format(sport_entity=detected_entities[0]['name'])
        elif detected_teams:
            acknowledge_response = template_sports.utterance(selector="grounding/acknowledge_fav_team", slots={"team": detected_teams[0]}, user_attributes_ref = self.user_attributes)
        else:
            acknowledge_key = ["acknowledge_general"]
            acknowledge_response = sport_utterance(acknowledge_key)['text']

        return acknowledge_response

    def acknowledge_interest_player(self):
        r'''
        Acknowledge For the question that asked about user's interest player, for example
        wwho is your favorite player, 4 cases.
        (1) User answered with a name entity that is the same as ours.
        (2) User answered with a name entity that is not the same as ours
        (3) If User mention a name, acknowledge the name.
        (4) User does not have one or he is uncertain or he answered something else.
        '''
        if self.selected_question:
            selected_question = self.selected_question
        else:
            selected_question = sport_utterance(self.selected_chitchat_key, get_all=True)[
                self.selected_chitchat_id]

        selected_question_answer = random.choice(selected_question['a']).lower()
        

        detected_names = detect_names(self.sys_info['returnnlp'][0])
        if self.context['detected_entities']['sys_ner'] or self.context['detected_entities']['sport_ner']:
            detected_entities = self.context['detected_entities']['sport_ner'][:] if self.context[
                'detected_entities']['sport_ner'] else self.context['detected_entities']['sys_ner'][:]

            if any(re.search(x['name'], selected_question_answer) for x in detected_entities):
                # Same Entity
                acknowledge_key = ["acknowledge_same_entity"]
                acknowledge_response = sport_utterance(acknowledge_key)['text']
            else:
                # Different Entity
                acknowledge_key = ["acknowledge_different_entity"]
                acknowledge_response = sport_utterance(
                    acknowledge_key)['text'].format(sport_entity=detected_entities[0]['name'])
        
        elif detected_names:
            acknowledge_response = template_sports.utterance(selector="grounding/acknowledge_fav_player", slots={"player": detected_names[0]},
                                                             user_attributes_ref= self.user_attributes)
        else:
            acknowledge_key = ["acknowledge_general"]
            acknowledge_response = sport_utterance(acknowledge_key)['text']

        return acknowledge_response

    def acknowledge_baseball_position(self):
        r"""
        Acknowledge For the question regarding the baseball positions. (For example what is the hardest position to play in baseball).
        If not interupted to some other higher responses, 3 cases.
        (1) User answered a baseball position that is the same as ours.
        (2) User answered a baseball position that is different from us.
        (3) User does not have an opinion or he is uncertain or he answered something else
        """
        if self.selected_question:
            selected_question = self.selected_question
        else:
            selected_question = sport_utterance(self.selected_chitchat_key, get_all=True)[
                self.selected_chitchat_id]

        selected_question_answer = random.choice(selected_question['a']).lower()

        # Define a list of baseball position
        baseball_positions = ['pitcher', 'catcher', 'first baseman', 'second baseman', 'third baseman',
                              'shortstop', 'left fielder', 'center fielder', 'right fielder', 'striker', 'strike']

        # Detrect baseball_positions
        detected_positions = [
            x for x in baseball_positions if re.search(x, self.user_utterance)]

        if detected_positions:
            if any(re.search(x,  selected_question_answer) for x in detected_positions):
                # Same Choice of Position
                acknowledge_key = ["acknowledge_same_entity"]
                acknowledge_response = sport_utterance(acknowledge_key)['text']
            else:
                # Different Entity
                acknowledge_key = ["acknowledge_different_baseball_pos"]
                acknowledge_response = sport_utterance(
                    acknowledge_key)['text'].format(baseball_pos=detected_positions[0])
        else:
            acknowledge_key = ["acknowledge_general"]
            acknowledge_response = sport_utterance(acknowledge_key)['text']

        return acknowledge_response

    def acknowledge_personal_experience(self):
        r'''
        Acknowledge For the question that asked about user's personal experience, for example
        what is the best buzzer beater?
        (1) User answered with some lengthy content, we will appreciate for their sharing
        (2) User answered I don't know or some unuseful short answers, we use general acknowledgement.
        '''
        # Initialize the Sport Intent
        user_sport_intent = self.user_intent['sport_intents']
        user_utterance = self.user_utterance

        if len(user_utterance.split()) > 3 and not user_sport_intent['sport_i_answeruncertain']:
            acknowledge_key = ["appreciate_share_experience"]
            acknowledge_response = sport_utterance(acknowledge_key)['text']

        else:
            acknowledge_key = ["acknowledge_general"]
            acknowledge_response = sport_utterance(acknowledge_key)['text']

        return acknowledge_response

    def acknowledge_personal_opinion(self):
        r'''
        Acknowledge For the question that asked about user's personal opinion, for example
        Do you think the NBA is soft right now?
        (1) User answered with some lengthy content, we will appreciate for their sharing.
        (2) User answered I don't know or some unuseful short answers, we use general acknowledgement.
        '''
        # Initialize the Sport Intent
        user_sport_intent = self.user_intent['sport_intents']
        user_utterance = self.user_utterance
        user_sport_context = self.context

        if self.selected_question:
            selected_question = self.selected_question
        else:
            selected_question = sport_utterance(self.selected_chitchat_key, get_all=True)[
                self.selected_chitchat_id]

        selected_question_answer = random.choice(selected_question['a']).lower()

        if len(user_utterance.split()) > 6 and not user_sport_intent['sport_i_answeruncertain']:
            acknowledge_key = ["appreciate_share_opinion"]
            acknowledge_response = sport_utterance(acknowledge_key)['text']
        elif len(user_utterance.split()) <= 6 and not user_sport_intent['sport_i_answeruncertain'] and not user_sport_intent['sport_i_answeryes']:
            # paraphrase
            paraphrased_response = self.acknowledge_paraphrase()

            acknowledge_response = template_sports.utterance(selector="grounding/acknowledge_paraphrase_share_opinion", slots={"paraphrase": paraphrased_response},
                                                             user_attributes_ref=self.user_attributes)
        else:
            acknowledge_key = ["acknowledge_general"]
            acknowledge_response = sport_utterance(acknowledge_key)['text']

        return acknowledge_response

    def acknowledge_yes_or_no(self):
        r'''
        Acknowledge For the question that asked user yes or no.
        (1) User answered the same answer then we should express delightfulness to see that we share something in common.
        (2) User answered different answer then we should acknowledge their answer and politely indicate that we have a different answer.
        (3) Otherwise, we will have general acknowledgement and do self expressness
        '''
        # Initialize the Sport Intent
        user_sport_intent = self.user_intent['sport_intents']
        user_utterance = self.user_utterance

        if self.selected_question:
            selected_question = self.selected_question
        else:
            selected_question = sport_utterance(self.selected_chitchat_key, get_all=True)[
                self.selected_chitchat_id]

        selected_question_answer = random.choice(selected_question['a']).lower()

        answer_yes_or_no, answer_same = detect_same_yes_or_no(
            user_sport_intent, selected_question_answer)

        if answer_yes_or_no and len(user_utterance.split()) < 5:
            # acknowledge_key = ["appreciate_share_opinion"]
            # cknowledge_response = sport_utterance(acknowledge_key)['text']
            if answer_same:
                acknowledge_key = ["share_same_yes_or_no"]
                acknowledge_response = sport_utterance(acknowledge_key)['text']
            else:
                acknowledge_key = ["share_different_yes_or_no"]
                acknowledge_response = sport_utterance(acknowledge_key)['text']
        else:
            acknowledge_key = ["acknowledge_general"]
            acknowledge_response = sport_utterance(acknowledge_key)['text']

        return acknowledge_response

    def acknowledge_interest_sport_type(self):
        # Initialize the Sport Intent
        user_sport_intent = self.user_intent['sport_intents']
        user_utterance = self.user_utterance
        user_sport_context = self.context
        current_sport = user_sport_context['current_sport']

        if self.selected_question:
            selected_question = self.selected_question
        else:
            selected_question = sport_utterance(self.selected_chitchat_key, get_all=True)[
                self.selected_chitchat_id]

        selected_question_answer = random.choice(selected_question['a']).lower()

        if current_sport and (not user_sport_intent['sport_i_notinterestsporttype']):
            if re.search(current_sport, user_utterance):
                acknowledge_key = "grounding/acknowledge_interest_sport_type_same"
            else:
                acknowledge_key = "grounding/acknowledge_interest_sport_type_different"

            acknowledge_response = template_sports.utterance(selector=acknowledge_key, slots={'sport_type': current_sport},
                                                             user_attributes_ref=self.user_attributes)
        else:
            acknowledge_response = template_sports.utterance(selector="grounding/acknowledge_general", slots={},
                                                             user_attributes_ref=self.user_attributes)
        return acknowledge_response

    def acknowledge_play_sport_yes_no(self):
        # Initialize the Sport Intent
        user_sport_intent = self.user_intent['sport_intents']
        user_utterance = self.user_utterance
        user_sport_context = self.context
        current_sport = user_sport_context['current_sport']

        if self.selected_question:
            selected_question = self.selected_question
        else:
            selected_question = sport_utterance(self.selected_chitchat_key, get_all=True)[
                self.selected_chitchat_id]

        selected_question_answer = random.choice(selected_question['a']).lower()

        if user_sport_intent["sport_i_answeryes"]:
            acknowledge_response = ""
        elif user_sport_intent["sport_i_answerno"]:
            acknowledge_response = template_sports.utterance(selector="grounding/acknowledge_play_sport_no", slots={},
                                                             user_attributes_ref=self.user_attributes)
        else:
            acknowledge_response = template_sports.utterance(selector="grounding/acknowledge_general", slots={},
                                                             user_attributes_ref=self.user_attributes)

        return acknowledge_response

    def acknowledge_play_sport_yes_no(self):
        # Initialize the Sport Intent
        user_sport_intent = self.user_intent['sport_intents']
        user_utterance = self.user_utterance
        user_sport_context = self.context
        current_sport = user_sport_context['current_sport']

        if self.selected_question:
            selected_question = self.selected_question
        else:
            selected_question = sport_utterance(self.selected_chitchat_key, get_all=True)[
                self.selected_chitchat_id]

        selected_question_answer = random.choice(selected_question['a']).lower()

        if user_sport_intent["sport_i_answeryes"]:
            acknowledge_response = ""
        elif user_sport_intent["sport_i_answerno"]:
            acknowledge_response = template_sports.utterance(selector="grounding/acknowledge_play_sport_no", slots={},
                                                             user_attributes_ref=self.user_attributes)
        else:
            acknowledge_response = template_sports.utterance(selector="grounding/acknowledge_general", slots={},
                                                             user_attributes_ref=self.user_attributes)

        return acknowledge_response

    def acknowledge_when_play_sport(self):
        user_sport_intent = self.user_intent['sport_intents']
        user_utterance = self.user_utterance
        user_sport_context = self.context
        current_sport = user_sport_context['current_sport']

        if self.selected_question:
            selected_question = self.selected_question
        else:
            selected_question = sport_utterance(self.selected_chitchat_key, get_all=True)[
                self.selected_chitchat_id]

        #selected_question_answer = selected_question['a'].lower()

        not_remember_pattern = [
            r"(can't|don't|do not|can not)\s*(remember|know)"
        ]

        if any(re.search(x, user_utterance) for x in not_remember_pattern) or user_sport_intent['sport_i_answerno']:
            acknowledge_response = template_sports.utterance(selector="grounding/acknowledge_general", slots={},
                                                             user_attributes_ref=self.user_attributes)
        elif len(user_utterance.split()) > 6:
            acknowledge_response = template_sports.utterance(selector="grounding/acknowledge_who_play_sport_with_long", slots={},
                                                             user_attributes_ref=self.user_attributes)
        else:
            # paraphrase
            paraphrased_response = self.acknowledge_paraphrase()

            acknowledge_response = template_sports.utterance(selector="grounding/acknowledge_paraphrase", slots={"paraphrase": paraphrased_response},
                                                             user_attributes_ref=self.user_attributes)

        return acknowledge_response

    def acknowledge_who_play_sport_with(self):
        user_sport_intent = self.user_intent['sport_intents']
        user_utterance = self.user_utterance
        user_sport_context = self.context
        current_sport = user_sport_context['current_sport']

        if self.selected_question:
            selected_question = self.selected_question
        else:
            selected_question = sport_utterance(self.selected_chitchat_key, get_all=True)[
                self.selected_chitchat_id]

        #selected_question_answer = selected_question['a'].lower()

        not_remember_pattern = [
            r"(can't|don't|do not|can not)\s*(remember|know)"
        ]

        if any(re.search(x, user_utterance) for x in not_remember_pattern) or user_sport_intent['sport_i_answerno']:
            acknowledge_response = template_sports.utterance(selector="grounding/acknowledge_general", slots={},
                                                             user_attributes_ref=self.user_attributes)
        elif len(user_utterance.split()) > 6:
            acknowledge_response = template_sports.utterance(selector="grounding/appreciate_share_experience", slots={},
                                                             user_attributes_ref=self.user_attributes)
        else:
            # paraphrase
            paraphrased_response = self.acknowledge_paraphrase()

            acknowledge_response = template_sports.utterance(selector="grounding/acknowledge_paraphrase", slots={"paraphrase": paraphrased_response},
                                                             user_attributes_ref=self.user_attributes)

        return acknowledge_response

    def acknowledge_sport_popular_yes_no(self):
        user_sport_intent = self.user_intent['sport_intents']
        user_utterance = self.user_utterance
        user_sport_context = self.context
        current_sport = user_sport_context['current_sport']

        if self.selected_question:
            selected_question = self.selected_question
        else:
            selected_question = sport_utterance(self.selected_chitchat_key, get_all=True)[
                self.selected_chitchat_id]

        #selected_question_answer = selected_question['a'].lower()

        if user_sport_intent["sport_i_answeryes"] and len(user_utterance.split()) < 6:
            acknowledge_response = template_sports.utterance(
                selector="grounding/acknowledge_sport_popular_yes", slots={}, user_attributes_ref=self.user_attributes)
        elif user_sport_intent["sport_i_answerno"] and len(user_utterance.split()) < 6:
            acknowledge_response = template_sports.utterance(
                selector="grounding/acknowledge_sport_popular_no", slots={}, user_attributes_ref=self.user_attributes)
        elif len(user_utterance.split()) > 6:
            acknowledge_response = template_sports.utterance(
                selector="grounding/acknowledge_opinion", slots={}, user_attributes_ref=self.user_attributes)
        else:
            acknowledge_response = template_sports.utterance(
                selector="grounding/acknowledge_general", slots={}, user_attributes_ref=self.user_attributes)

        return acknowledge_response

    def acknowledge_favorite_player_yes_no(self):
        user_sport_intent = self.user_intent['sport_intents']
        user_utterance = self.user_utterance
        user_sport_context = self.context
        current_sport = user_sport_context['current_sport']

        if self.selected_question:
            selected_question = self.selected_question
        else:
            selected_question = sport_utterance(self.selected_chitchat_key, get_all=True)[
                self.selected_chitchat_id]

        #selected_question_answer = selected_question['a'].lower()

        if user_sport_intent["sport_i_answeryes"] and len(user_utterance.split()) < 6:
            acknowledge_response = template_sports.utterance(
                selector="grounding/express_excitement", slots={}, user_attributes_ref=self.user_attributes)
        elif len(user_utterance.split()) < 6:
            acknowledge_response = template_sports.utterance(
                selector="grounding/acknowledge_general", slots={}, user_attributes_ref=self.user_attributes)
        else:
            acknowledge_response = template_sports.utterance(selector="grounding/appreciate_share_experience", slots={},
                                                             user_attributes_ref=self.user_attributes)

        return acknowledge_response

    def acknowledge_interest_entity_unknown_sport(self):
        user_sport_intent = self.user_intent['sport_intents']
        user_utterance = self.user_utterance
        user_sport_context = self.context
        current_sport = user_sport_context['current_sport']

        if self.selected_question:
            selected_question = self.selected_question
        else:
            selected_question = sport_utterance(self.selected_chitchat_key, get_all=True)[
                self.selected_chitchat_id]

        #selected_question_answer = selected_question['a'].lower()

        if len(user_utterance.split()) < 3 and not user_sport_intent['sport_i_answeruncertain']:
             # paraphrase
            paraphrased_response = self.acknowledge_paraphrase()

            acknowledge_response = template_sports.utterance(selector="grounding/acknowledge_paraphrase", slots={"paraphrase": paraphrased_response},
                                                             user_attributes_ref=self.user_attributes)
        else:
            acknowledge_response = template_sports.utterance(
                selector="grounding/acknowledge_general", slots={}, user_attributes_ref=self.user_attributes)

        return acknowledge_response

    def acknowledge_ask_jog_reason(self):
        r'''
        Acknowledge For the question that asked about user's motivation to run
        (1) User answered with some lengthy content, we will appreciate for their sharing.
        (2) We will just general acknowledge it. 
        '''
        # Initialize the Sport Intent
        user_sport_intent = self.user_intent['sport_intents']
        user_utterance = self.user_utterance

        if len(user_utterance.split()) > 3 and not user_sport_intent['sport_i_answeruncertain']:
            acknowledge_response = template_sports.utterance(selector="grounding/acknowledge_jog_reason_general", slots={},
                                                             user_attributes_ref=self.user_attributes)
        else:
            acknowledge_response = template_sports.utterance(selector="grounding/acknowledge_general", slots={}, user_attributes_ref=self.user_attributes)

        return acknowledge_response

    def acknowledge_ask_jog_path(self):
        r'''
        Acknowledge For the question that asked about user's jogging habbit
        (1) User answered with the keyword neighborhood, 
        (2) User answer with the keyrwodk nature,
        (3) Lengthy answer without keywords
        (2) We will just general acknowledge it. 
        '''
        # Initialize the Sport Intent
        user_sport_intent = self.user_intent['sport_intents']
        user_utterance = self.user_utterance
        
        neighborhood_pattern = r"neighborhood|my house|my place|neighbor|neighbour"
        nature_pattern = r"nature|natural|mountain|scenic"

        if re.search(neighborhood_pattern, user_utterance) and not (user_sport_intent['sport_i_answeruncertain'] or user_sport_intent['sport_i_answerno'] or user_sport_intent['sport_i_notinterest']):
            acknowledge_response = template_sports.utterance(selector="grounding/acknowledge_jog_neighborhood_path", slots={}, user_attributes_ref=self.user_attributes)
        elif re.search(nature_pattern, user_utterance) and not (user_sport_intent['sport_i_answeruncertain'] or user_sport_intent['sport_i_answerno'] or user_sport_intent['sport_i_notinterest']):
            acknowledge_response = template_sports.utterance(selector="grounding/acknowledge_jog_nature_path", slots={}, user_attributes_ref=self.user_attributes) 
        elif len(user_utterance.split()) > 3 and not user_sport_intent['sport_i_answeruncertain']:
            acknowledge_response =  acknowledge_response = template_sports.utterance(selector="grounding/acknowledge_jog_path_general", slots={}, user_attributes_ref=self.user_attributes) 
        else:
            acknowledge_response = template_sports.utterance(selector="grounding/acknowledge_general", slots={}, user_attributes_ref=self.user_attributes)

        return acknowledge_response

    def acknowledge_ask_jog_distance(self):
        r'''
        Acknowledge For the question that asked about user how far they run
        (1) If distance keywords and numbers are both detected, then we will repeat that pair
        (2) If just distance keywords detected and not very long (< 6), paraphrase and acknowledge
        (2) general
        '''
        # Initialize the Sport Intent
        user_sport_intent = self.user_intent['sport_intents']
        user_utterance = self.user_utterance
        
        #Check if we can detect numbers or key distance words
        #distance_key_words = ["mile","kilometer"]
        
        detected_distance = None
        if self.sys_info['sys_ner'] is not None:
            for sys_ner in self.sys_info['sys_ner']:
                if sys_ner.get('label', "") == 'QUANTITY':
                    detected_distance = sys_ner['text']
                    break

        if detected_distance:
            acknowledge_response = template_sports.utterance(selector="grounding/acknowledge_jog_distance", slots={'paraphrase': detected_distance}, user_attributes_ref=self.user_attributes)
        else:
            acknowledge_response = template_sports.utterance(selector="grounding/acknowledge_general", slots={}, user_attributes_ref=self.user_attributes)

        return acknowledge_response