import logging
logging.basicConfig(
    format='[%(levelname)s] %(asctime)s [%(filename)s:%(lineno)d] %(message)s',
)
# from sport_transition import Sport_Transition,WorldCup_Transition
# from sport_response import Sport_Response,WorldCup_Response
from sport_transition import Sport_Transition
from sport_response import Sport_Response
from sport_utils import *
from sport_utterance_data import sport_utterance, KNOWN_SPORTS
import re
import json
import datetime
import os
import requests
from types import SimpleNamespace
from nlu.dataclass import CentralElement, ReturnNLP, ReturnNLPSegment
from selecting_strategy.module_selection import ModuleSelector
from user_profiler.user_profile import UserProfile
from nlu.question_detector import QuestionDetector

logging.getLogger().setLevel(logging.INFO)

SUGGEST_PATTERNS = [r"(\blet's\b|\blet us\b)\s*(talk|chat)",
                    r"(\bi\b)\s(want to|expect to|would like to|wanna|really wanna|really want|used to play|enjoy|really enjoy)\s*(talk|chat|watch)",
                    r"(\bhow about we\b|\bcan we\b|\bcould we\b)\s*(talk|chat)",
                    r"(\bi)\s(also like|only like|like|love|am fan|fan|watch|prefer|follow|play)",
                    r"(\bi am\b|i'm)\s*(interested|passionate)",
                    r"(my favorite)\s*(sport|player|team)\s*(is|was)",
                    r"(tell me about|how about|what about)",
                    ]

NO_INTEREST_PATTERNS = [r"(\bi\b).*(don't|do not|never).*(interested|a fan|fan|like|love|talk about|want|care|watch|play|follow|into|having|have|know anything|know much|keep up with)",
                        r"(don't|not|never).*(interested|a fan|fan|like|love|talk about|want|care|watch|play|follow|into|having|have|(know .* means)|know much|wanna)",
                        r"(\bother|\botherthings|\banything else|\bsomething else).*(you).*(tell|talk|chat)",
                        r"\b(talk|chat).*(other|other things|something else|some other things|something)",
                        r"((\bi)\s*(hate|dislike))|i would rather die than",
                        r"(is|are)\s*(boring)",
                        r"(nothing about)|^no\b|^zero\b|\bsucks",
                        ]

QUESTION_PATTERNS = [
    r"(who\b|what\b|when\b|where\b|why\b|how\b|which\b|^do\b|^does\b|^did\b|^was\b|^is\b|^are\b|^were\b|^will\b|^would\b)\s*(was|is|were|are|will|would|did|do|does|can|could|you|he|she|him|her|they|we|i)",
    r"(do|don't|did|didn't)\s*(you)\s*(know|understand|hear|think|like)",
    r"(^who|^what|^where|^why|^how|^which|^should|^will|^would)",
    # r"(\btell me\b|\bgive me\b)"
    r"(^can|^may|^should|^would|^could|^is|^are|^were)",
]

REQUEST_INFO_QUESTION_PATTERNS = [
    r"(do you|can you)\s*(have anything|know anything|talk|want to talk|know)\s*(about|on)",
    r"(can we|how about we)\s*(talk|chat)\s*(about)",
    r"tell me about"
]

REQUEST_PATTERNS = [r"(\btell me\b|\bgive me\b)",
                    r"(\bi\b)\s*(want|want to)"]

EXCEPTION_QUESTION = [r"\bwhy not$",
                      ]

QUIT_MODULE_PATTERN = [r"^finished$|^cancel$|^stop$"]

IGNORE_PATTERN = [r"(?<=\binstead of\s)(\w+)", r"(?<=\bother than\s)(\w+)"]

REQUEST_PROVIDE_TOPIC = [r"what (would you|do you) (like|want) to talk"]

'''
with open('data/sport_data.json') as f:
    SPORT_DATA = json.load(f)
'''
from sport_utterance_data import SPORT_UTTERANCE_DATA  # noqa: F401

# Define the Data Dir for World Cup
WORLD_CUP_DIR = 'data/soccer/worldcup'
# Definet the Time for Now
NOW = datetime.datetime.now()
if NOW.hour > 12:
    half_day = 0
else:
    half_day = 1
TIME_SUFFIX = "{}_{}_{}_{}".format(NOW.month, NOW.day, NOW.year, half_day)

# Define the Sport Chatbot Object to Generate Response


class Sport_Chatbot(object):

    def __init__(self, Utterance, user_attributes, Sys_Info, Sport_Info):
        # Initialize the sport intents for users
        self.__bind_user_sport_intents()  # Initialize the User Intent to Track
        self.__bind_sport_context()  # Initialize the Sport Context to Track
        # Current Utterance
        # Utterance is already in lower case
        self.user_utterance = re.sub("open gunrock and", "", Utterance)
        # logging.debug("[SPORTS_MODULE] user utterance type: {}".format(type(self.user_utterance)))
        # if type(self.user_utterance) == list:
        #     self.user_utterance = self.user_utterance[0]
        self.user_attributes = user_attributes
        self.module_selection = ModuleSelector(self.user_attributes)
        self.user_profile = UserProfile(self.user_attributes)
        self.sys_info = Sys_Info
        self.sport_info = Sport_Info
        self.returnnlp_util = ReturnNLP(Sys_Info['returnnlp'][0])
        self.question_detector = QuestionDetector(self.user_utterance, Sys_Info['returnnlp'][0])
        #logging.debug("[SPORTS_MODULE] user favorite sport: {}".format(self.user_profile.topic_module_profiles.sport.fav_sport_type))
        #logging.debug("[SPORTS_MODULE] returnnl util entities: {}".format(self.returnnlp_util.detect_entities()))
        #Try detect names
        # if detect_names(Sys_Info['returnnlp'][0]):
        #     logging.debug("[SPORTS_MODULE] detected names are: {}".format(detect_names(Sys_Info['returnnlp'][0])))
        # Update the User_Profile Information
        # self.user_profile = Sport_Info['sport_user']

        # Update the context information
        if Sport_Info['sport_context'].get('conversation_id', None) == Sport_Info['session_id']:
            self.context["previous_state"] = Sport_Info[
                'sport_context']['previous_state']
            self.context["previous_system_response"] = Sport_Info[
                'sport_context']['previous_system_response']
            self.context["current_sport"] = Sport_Info[
                'sport_context'].get('previous_sport', None)
            if Sport_Info['sport_context']['previous_entity'].get("name", None):
                self.context["current_entity"]["name"] = Sport_Info[
                    'sport_context']['previous_entity'].get("name", None)
            if Sport_Info['sport_context']['previous_entity'].get("label", None):
                self.context["current_entity"]["label"] = Sport_Info[
                    'sport_context']['previous_entity'].get("label", None)
            self.context["talked_sports"] = Sport_Info[
                'sport_context']['talked_sports'][:]
            self.context["talked_entities"] = Sport_Info[
                'sport_context']['talked_entities'][:]
            self.context["suggest_entity_rejection"] = int(
                Sport_Info['sport_context']['suggest_entity_rejection'])
            self.context["num_error_status"] = int(Sport_Info['sport_context'][
                                                   'num_error_status'])  # Update the num error status
            self.context["expect_next_state"] = Sport_Info['sport_context'].get(
                'expect_next_state', None)  # Upcoming Expected Next State
            self.context["conversation_id"] = Sport_Info['session_id']
            # Update the asked_questions
            # Copy the Content from One Dictionary to another.
            self.context["asked_questions"] = Sport_Info[
                'sport_context'].get('asked_questions', {})
            self.context["not_interested_sports"] = Sport_Info[
                'sport_context'].get('not_interested_sports', [])
            self.context["selected_funfacts"] = Sport_Info[
                'sport_context'].get('selected_funfacts', [])
            self.context["selected_news"] = Sport_Info[
                'sport_context'].get('selected_news', [])
            self.context["talked_moments"] = Sport_Info['sport_context'].get(
                'talked_moments', [])  # Get the talked moments
            self.context["selected_moment"] = Sport_Info[
                'sport_context'].get('selected_moment', None)
            # Get Talked Content
            self.context["talked_web_content"] =  Sport_Info['sport_context'].get('talked_web_content', {})
            # Get followup questions
            self.context["followup_questions"] = Sport_Info[
                'sport_context'].get("followup_questions", [])
            # Get general sport questions template
            self.context["general_sport_questions"] = Sport_Info[
                'sport_context'].get("general_sport_questions", [])

            # Get selected_question
            self.context["selected_question"] = Sport_Info[
                'sport_context'].get('selected_question', {})

            # Get chitchat_mode
            self.context["chitchat_mode"] = Sport_Info['sport_context'].get('chitchat_mode', 'general')
            # get entity
            self.context["2sport_type"] = Sport_Info['sport_context'].get('2sport_type', False)
            self.context['favorite_sports'] = Sport_Info['sport_context'].get('favorite_sports', [])
        else:
            # logging.info("[SPORTS_MODULE] reinitialize everything")
            # Update Converstaion ID
            self.context["conversation_id"] = Sport_Info['session_id']
            self.context["expect_next_state"] = "s_sport_favorite_sport_confirmation"
            #Keep the context for users. 
            self.context["talked_web_content"] =  Sport_Info['sport_context'].get('talked_web_content', {})
            self.context["talked_sports"] = Sport_Info['sport_context'].get('talked_sports', [])
            self.context["talked_entities"] = Sport_Info['sport_context'].get('talked_entities', [])
            self.context['asked_questions'] = Sport_Info['sport_context'].get('asked_questions', {})
            self.context['not_interested_sports'] = Sport_Info['sport_context'].get('not_interested_sports', [])
            self.context['talked_moments'] = Sport_Info['sport_context'].get('talked_moments', [])
            self.context['favorite_sports'] = Sport_Info['sport_context'].get('favorite_sports', [])

        # logging.info("[SPORT MODULE] the selected_chitchat_id: {}".format(
        #     self.context["selected_chitchat_id"]))


        # Update the previous state if propose_continue is STOP
        if Sport_Info['sport_context'] is not None:
            # Check if previous propose_continue is "STOP"
            # if Sport_Info['sport_context'].get('propose_continue') == "STOP" or Sport_Info['sport_context'].get('propose_continue') == "UNCLEAR":
            #     self.context["previous_state"] = None
            if self.module_selection.last_topic_module != 'SPORT' and self.context['previous_state'] is not None:
                self.context["previous_state"] = None
                # Restart the followup_questions and seleted_chitchat_id
                self.context["followup_questions"] = []
                self.context["selected_question"] = {}
                # clear the expect_next_state too
                self.context["expect_next_state"] = None
                self.context["chitchat_mode"] = "general"
                #Resume Mode Start is only activated one turn
                self.context["resume_mode_start"] = True
                self.context["current_sport"] = None
                self.context["current_entity"] = {"name": None, "label": None}

        #Using global state to decide whether we want to change the initialized state
        if self.module_selection.global_state.value == "ask_fav_sport":
            self.context["previous_state"] = "s_sport_favorite_sport_confirmation"

        # clear selected_question if previous_state is not ini_chitchat,
        # followup_chitchat_ask or answer_question
        if self.context["previous_state"] not in ["s_sport_chitchat_q", "s_sport_answer_question"]:
            self.context["selected_question"] = {}

        # logging.info("[SPORT MODULE] The previous state is: {}".format(
        #     self.context['previous_state']))

        # Update the Intents, Sport_Conversation_Attributes
        self.__update__()
        
        #logging.debug("[SPORTS_MODULE] current_sport after update is: {}".format(self.context["current_sport"]))

        # Create User Intent Info
        self.user_intent = {"sys_intents": Sys_Info[
            "sys_intent"], "sport_intents": self.user_sport_intents}

        # logging
        logging.info("[SPORT_MODULE] detected_sports: {}".
                     format(self.context['detected_sports']))
        logging.info("[SPORT MODULE] The previous state is: {}".format(
            self.context['previous_state']))
        logging.info("[SPORT MODULE] The curent user utterance is: {}".format(
            self.user_utterance))
        logging.info("[SPORT MODULE] The folowup_questions are: {}".format(
            self.context["followup_questions"]))
        logging.info("[SPORT MODULE] The general_sport_questions are: {}".format(
            self.context["general_sport_questions"]))
        logging.info("[SPORT MODULE] The selected_question is: {}".format(
            self.context["selected_question"]))
        # logging.info("[SPORT MODULE] Whether nointerest_sport detected: {}".format(self.user_intent['sport_intents']['sport_i_nointerestsport']))
        # logging.info("[SPORT MODULE] The no interest in sport:
        # {}".format(self.user_sport_intents['sport_i_notinterestsport']))

    def __bind_user_sport_intents(self):
        self.user_sport_intents = {
            "sport_i_followupentity": False,
            "sport_i_notinterest": False,
            "sport_i_notinterestsporttype": False,  # Added by Mingyang Zhou
            "sport_i_notinterestsport": False,  # Added by Mingyang Zhou
            # Added by Mingyang Zhou, user indicate want to talk about sport
            # inside sport module
            "sport_i_requestsport": False,
            #"sport_i_notinterestentity":False, # Added by Mingyang Zhou
            "sport_i_suggestentity": False,
            "sport_i_suggestsporttype": False,
            "sport_i_askquestion": False,
            "sport_i_askopinion": False,
            "sport_i_requestnews": False,  # Added by Mingyang Zhou
            ##################Lexiical Level Intent#################
            "sport_i_askback": False,
            "sport_i_answerno": False,
            "sport_i_answeryes": False,
            "sport_i_answeruncertain": False,
            "sport_i_nointerestworldcup": False,
            "sport_i_answerabandoned": False,
            "sport_i_answerabandoned_not_question": False,
            'sport_i_opinionneg': False, 
            "sport_i_requesttopic": False, 
        }

    def __bind_sport_context(self):
        r"""
        Initialize a dictionary type to store context information
        """
        self.context = {
            "previous_state": None,
            # Added by Mingyang Zhou 9/13, record the previous system response
            "previous_system_response": None,
            "current_sport": None,
            "current_entity": {"name": None, "label": None},
            "suggest_entity_rejection": 0,
            "num_error_status": 0,
            "expect_next_state": None,
            "selected_chitchat_key": [],  # Added by Mingyang Zhou
            "selected_chitchat_id": None,
            "conversation_id": None,  # Initialize Converstaion ID
            "propose_continue": 'CONTINUE',
            "selected_funfacts": [],  # Added by Mingyang Zhou
            "selected_news": [],
            # Added by Mingyang Zhou, list of hash ids for all the contents we
            # have talked before, 10/11/2018
            "selected_moment": None,
            # The user attributes passed for Austin's template manager, added
            # by Mingyang 9/10/2018
            # Added by Mingyang Zhou to keep up with the followup questions in
            # sports, 10/14/2018
            "followup_questions": [],
            # Added by Mingyang Zhou to keep the reusable questions for unknown
            # sport type, 10/17/2018
            "general_sport_questions": [],
            # Define a term called selected_question to handle the conflict
            # between general sport_questions and folowup_question
            "selected_question": {},
            # Added by Mingyang Zhou, to manage the chitchat hierarchy, 4/1/2020
            "chitchat_mode": "general", #Three chitchat_mode: general; sport_type; entity; special_envent (TODO)
            "2sport_type": False, #A variable indicating whether we have jumped to sport_type_chitchat_mode from a lower hierarchy
            "resume_mode_start": False,
            ##########context included at current utterance#####
            "detected_sports": [],
            "detected_entities": {"sys_ner": [], "sport_ner": []},
            "unclear": False,
            "propose_topic": None,
            "followup_entity": False,
            "detected_question_types": [],
            "fail_question_answer": False,
            #########contex for user############################
            "talked_web_content": {},
            "talked_sports": [],
            "talked_entities": [],
            "asked_questions": {},  # Initialize by Mingyang
            "not_interested_sports": [],
            "talked_moments": [],  # Added by Mingyang Zhou, list of titles of talked moments
            "favorite_sports": [],

        }

    def __update_segment__(self, sentence_segment, sentence_segment_index=0):
        r"""
            Update User Intent, and a set of attributes associated with sports converstaion like: current name_entity, current_sport;
        """
        # Detect the Sport in Current sentence segment
        self.detect_sport(sentence_segment)
        # Detect the Entities in Current Sentence Segment
        if sentence_segment_index == 1:
            logging.debug('[SPORTS_MODULE] detected_entities before the function: {}'.format(sentence_segment.get('detected_entities', [])))
        self.detect_entity(sentence_segment, sentence_segment_index)
        # Detect the Intent in Sport Module
        self.detect_intent(sentence_segment, sentence_segment_index)
        # logging detect intent
        segment_detected_intents = []
        for intent in list(sentence_segment['user_sport_intents'].keys()):
            if sentence_segment['user_sport_intents'][intent]:
                segment_detected_intents.append(intent)
        logging.debug('[SPORTS_MODULE] The deteceted intents in current segment is: {}'.format(
            segment_detected_intents))
        # logging.debug('[SPORTS_MODULE] The detected entities in current segment is: {}'.format(
        #     sentence_segment['detected_entities']))
        # logging.info('[SPORTS_MODULE] The user sport suggestentity is: {}'.format(
        #     sentence_segment['user_sport_intents']))

        # Check it we want to followup with a entity that user is interested to
        # talk about
        user_sport_context = self.context
        selected_chitchat_key = user_sport_context['selected_chitchat_key']
        selected_chitchat_id = user_sport_context['selected_chitchat_id']
        # Use User Sport Intents to determine the t=the update
        user_sport_intents = sentence_segment['user_sport_intents']
        # logging.info("[SPORTS_MODULE] sentence_segment intents:
        # {}".format(user_sport_intents))

        # Update the current entity information if sport_i_suggestentity is
        # True
        # logging.info("[SPORTS_MODULE] bot suggestentity {}".format(
        #     user_sport_intents['sport_i_suggestentity']))

        if user_sport_intents['sport_i_suggestentity'] == True:

            # Update the current_entity
            if sentence_segment['detected_entities']['sport_ner']:
                self.context['current_entity']['name'] = sentence_segment[
                    'detected_entities']['sport_ner'][0]['name']
                self.context['current_entity']['label'] = sentence_segment[
                    'detected_entities']['sport_ner'][0]['label']
                self.context['current_sport'] = sentence_segment[
                    'detected_entities']['sport_ner'][0]['sport_type']
            elif sentence_segment['detected_entities']['sys_ner']:
                self.context['current_entity']['name'] = sentence_segment[
                    'detected_entities']['sys_ner'][0]['name']

                if sentence_segment['detected_entities']['sys_ner'][0]['label']:
                    self.context['current_entity']['label'] = sentence_segment[
                        'detected_entities']['sys_ner'][0]['label']
                else:
                    self.context['current_entity']['label'] = None

                if sentence_segment['detected_entities']['sys_ner'][0]['sport_type']:
                    self.context['current_sport'] = sentence_segment['detected_entities']['sys_ner'][0]['sport_type']  # UPDATE CURRENT SPORT
                # else:
                #     self.context['current_sport'] = None
            # else:
            #     self.context['current_sport'] = None
            
            #update the chitchat_mode
            if check_talked_entities(self.context['current_entity']["name"], self.context['talked_entities']):
                self.context["chitchat_mode"] = "entity"
                # Clear the follow_up questions and selected_question
                self.context["followup_questions"] = []
                self.context["selected_question"] = {}
        # Update the Sport Typsport_i_notinterestsport
        elif user_sport_intents['sport_i_suggestsporttype'] == True or self.context['previous_state'] == 's_sport_favorite_sport_confirmation':
            if sentence_segment["detected_sports"]:
                #Change the logic to priotise the popular sports
                found_popular_sport = False
                if len(sentence_segment["detected_sports"]) > 1:
                    for x in KNOWN_SPORTS:
                        if x in sentence_segment["detected_sports"]:
                            self.context['current_sport'] = x
                            found_popular_sport = True
                            break
                #In all other cases randomly sample the current sport     
                if not found_popular_sport:
                    self.context['current_sport'] = sentence_segment["detected_sports"][0]
                #update the chitchat_mode
                self.context["chitchat_mode"] = "sport_type"

                #logging.debug("[SPORTS_MODULE] current_sport is: {}".format(self.context['current_sport']))
                # Update self.context['current_entity']
                self.context['current_entity'] = {"name": None, "label": None}

                # Clear the follow_up questions and selected_questions   
                self.context["followup_questions"] = []
                self.context["selected_question"] = {}
                #update favorite_sport
                self.context["favorite_sports"].append(self.context['current_sport'])

            

        # Update the not interested sport type lists
        if user_sport_intents['sport_i_notinterestsporttype']:
            # Update the user profile with the not interested sports
            for sport in sentence_segment['detected_sports']:
                if sport not in self.context['not_interested_sports']:
                    self.context['not_interested_sports'].append(sport)

            if self.context['current_sport']:
                self.context['not_interested_sports'].append(
                    self.context['current_sport'])

            # Clear the follow_up questions and selected_chitchat_id
            self.context["followup_questions"] = []
            self.context["selected_question"] = {}

            #update the chitchat_mode
            self.chitchat_mode = "general"

        elif user_sport_intents['sport_i_notinterest'] and self.context['previous_state'] in ["s_sport_chitchat_q", "s_sport_chitchat_a"]:
            if self.context['current_sport']:
                self.context['not_interested_sports'].append(
                    self.context['current_sport'])
                

                
            self.context["followup_questions"] = []
            self.context["selected_question"] = {}

            #change the chitchat_mode
            self.chitchat_mode = "general"

        # Update the current sport if the user request news
        if self.user_sport_intents['sport_i_requestnews']:
            if sentence_segment['detected_sports']:
                self.context['current_sport'] = sentence_segment[
                    'detected_sports'][0]

            elif re.search('sports', self.user_utterance) or re.search('sport', self.user_utterance):
                # If user request sport news. Random assign a sport type.
                # TODO: Remove the sport type that user is not interested in.
                self.context['current_sport'] = random.choice(
                    list(SPORT_MOMENT_TAG_MAP.keys()))

            # Clear the follow_up questions and selected_chitchat_id
            self.context["followup_questions"] = []
            self.context["selected_question"] = {}

            # Needs to be determined
            # self.chitchat_mode = "general"

        # Udpate the user_utterance if user_detect_question
        if user_sport_intents['sport_i_askquestion']:
            # self.user_utterance = detect_question_DA(
            #     self.sys_info['sys_DA'])[0]  # Replace the user_utterance with the segmented text
            # also replace the coreference words
            if self.sys_info['sys_coreference']:
                sentence_segment['text'] = self.sys_info[
                    'sys_coreference']['text']
            if type(sentence_segment['text']) == list:
                logging.debug("[SPORTS_MODULE] current sentence segment text is: {}".format(sentence_segment['text']))
                self.user_utterance = sentence_segment['text'][0]
            else:
                self.user_utterance = sentence_segment['text']
            # logging.info("[SPORTS_MODULE] user_utterance for sports module is
            # : {}".format(self.user_utterance))

            # Extract the types of questions detected in the module
            # update the question_type,
            self.context["detected_question_types"] = []

            self.context["detected_question_types"].append(
                sentence_segment['dialog_act'][0])

            for topic in self.sys_info["sys_intent"].get("topic", []):
                self.context["detected_question_types"].append(topic)

            for lexical in self.sys_info["sys_intent"].get("lexical", []):
                ["detected_question_types"].append(lexical)

        # Update the Unclear Status
        detected_other_topic = get_sys_other_topic(self.sys_info, sentence_segment['detected_sports'], self.user_utterance, self.returnnlp_util.googlekg[sentence_segment_index])

        if detected_other_topic and not check_utterance_coherence(self.sys_info, sentence_segment['detected_entities'], user_sport_context):
            logging.debug("[SPORT_MODULE] Detected Other Topics:{}".format(
                detected_other_topic))
            self.context['unclear'] = True
            # update the proposed topic if the return result from
            # get_sys_other_topic is a string
            if type(detected_other_topic) == str:
                self.context['propose_topic'] = KEYWORD_MODULE_MAP[
                    detected_other_topic]

            # If any exception happens then don't break out
            # logging.info(
            #     "[SPORT MODULE] Current Utterance before we check exception words: {}".format(self.user_utterance))
            for exception_word in EXCEPTION_KEY_WORDS:
                # logging.info(
                #     "[SPORT MODULE] The exception_word is: {}".format(exception_word))
                # It is better to do user_utterance
                if re.search(exception_word, self.user_utterance):
                    self.context['unclear'] = False
                    break
        elif user_sport_intents['sport_i_requestnews']:
            if not (re.search('sport', self.user_utterance) or self.context['current_sport'] in list(SPORT_MOMENT_TAG_MAP.keys())):
                self.context['unclear'] = True
                self.context['propose_topic'] = "NEWS"

        # Determine whether added the detected entities to
        # context['detected_entities']
        if not user_sport_intents['sport_i_notinterestsporttype'] and not user_sport_intents['sport_i_notinterest']:
            if sentence_segment['detected_entities']['sport_ner']:
                self.context['detected_entities'][
                    'sport_ner'] += sentence_segment['detected_entities']['sport_ner'].copy()

            if sentence_segment['detected_entities']['sys_ner']:
                self.context['detected_entities'][
                    'sys_ner'] += sentence_segment['detected_entities']['sys_ner'].copy()

            if sentence_segment['detected_sports']:
                self.context[
                    'detected_sports'] += sentence_segment['detected_sports'].copy()
        # if not self.user_sport_intents['sport_i_askquestion']

    def __update__(self):
        r"""Update the Context Information Based on the previous utterances and
        current response
        """
        # Update the context with respect to each sentence segment
        sys_returnnlp = self.sys_info['returnnlp'][0]
        if len(sys_returnnlp) > 0:
            for i, sentence_segment in enumerate(sys_returnnlp):
                # Initialize sentence_segment with a default intent map
                sentence_segment['user_sport_intents'] = {
                    "sport_i_followupentity": False,
                    "sport_i_notinterest": False,
                    "sport_i_notinterestsporttype": False,  # Added by Mingyang Zhou
                    "sport_i_notinterestsport": False,  # Added by Mingyang Zhou
                    #"sport_i_notinterestentity":False, # Added by Mingyang Zhou
                    "sport_i_suggestentity": False,
                    "sport_i_suggestsporttype": False,
                    "sport_i_askquestion": False,
                    "sport_i_askopinion": False,
                    "sport_i_requestnews": False,  # Added by Mingyang Zhou
                    ##################Lexiical Level Intent#################
                    "sport_i_askback": False,
                    "sport_i_answerno": False,
                    "sport_i_answeryes": False,
                    "sport_i_answeruncertain": False,
                    "sport_i_nointerestworldcup": False,
                    "sport_i_answerabandoned": False,
                    "sport_i_opinionneg": False,
                }
                self.__update_segment__(
                    sentence_segment, sentence_segment_index=i)
        else:
            sentence_segment = {}
            sentence_segment['text'] = self.user_utterance
            # sentence_segment['DA'] = ''
            sentence_segment['user_sport_intents'] = {
                "sport_i_followupentity": False,
                "sport_i_notinterest": False,
                "sport_i_notinterestsporttype": False,  # Added by Mingyang Zhou
                "sport_i_notinterestsport": False,  # Added by Mingyang Zhou
                #"sport_i_notinterestentity":False, # Added by Mingyang Zhou
                "sport_i_suggestentity": False,
                "sport_i_suggestsporttype": False,
                "sport_i_askquestion": False,
                "sport_i_askopinion": False,
                "sport_i_requestnews": False,  # Added by Mingyang Zhou
                ##################Lexiical Level Intent#################
                "sport_i_askback": False,
                "sport_i_answerno": False,
                "sport_i_answeryes": False,
                "sport_i_answeruncertain": False,
                "sport_i_nointerestworldcup": False,
                "sport_i_opinionneg": False,
            }
            self.__update_segment__(sentence_segment)
        

        # Process the final update
        user_sport_context = self.context
        selected_chitchat_key = user_sport_context['selected_chitchat_key']
        selected_chitchat_id = user_sport_context['selected_chitchat_id']
        selected_question = user_sport_context['selected_question']

        if selected_chitchat_id is not None or selected_question:
            if selected_question:
                bot_ask_interest = selected_question.get('ask_interest', None)
            else:
                bot_ask_interest = sport_utterance(selected_chitchat_key, get_all=True)[
                    selected_chitchat_id].get('ask_interest', False)
            # logging.info(
            #     "[SPORTS_MODULE] bot_ask_interest {}".format(bot_ask_interest))
            if bot_ask_interest:
                if self.context['detected_entities']['sport_ner']:
                    self.context['current_entity']['name'] = self.context['detected_entities']['sport_ner'][0]['name']
                    self.context['current_entity']['label'] = self.context['detected_entities']['sport_ner'][0]['label']
                    if self.context['detected_entities']['sport_ner'][0]['sport_type']:
                        self.context['current_sport'] = self.context['detected_entities']['sport_ner'][0]['sport_type']
                    # Update the followup_entity
                    self.context['followup_entity'] = True
                    
                    if self.context["current_entity"]["name"] not in self.context["talked_entities"]:
                        self.user_sport_intents['sport_i_suggestentity'] = True
                        #update the chitchat_mode
                        self.context["chitchat_mode"] = "entity"
                        #disable the selected question
                        self.context["selected_question"] = {}
                    else:
                        self.user_sport_intents['sport_i_suggestentity'] = False

                elif self.context['detected_entities']['sys_ner']:
                    self.context['current_entity']['name'] = self.context[
                        'detected_entities']['sys_ner'][0]['name']
                    self.context['current_entity']['label'] = self.context[
                        'detected_entities']['sys_ner'][0]['label']
                    if self.context['detected_entities']['sys_ner'][0]['sport_type']:
                        #logging.debug("[SPORTS_MODULE]  update the sport type from sys_ner: {}".format(self.context['detected_entities']['sys_ner'][0]['sport_type']))
                        self.context['current_sport'] = self.context['detected_entities']['sys_ner'][0]['sport_type']
                    # Update the followup_entity
                    self.context['followup_entity'] = True

                    if self.context["current_entity"]["name"] not in self.context["talked_entities"]:
                        self.user_sport_intents['sport_i_suggestentity'] = True
                        #update the chitchat_mode
                        self.context["chitchat_mode"] = "entity"
                        #disable the selected question
                        self.context["selected_question"] = {}
                    else:
                        self.user_sport_intents['sport_i_suggestentity'] = False
                elif self.context["detected_sports"]:
                    self.context["current_sport"] = self.context[
                        "detected_sports"][0]
                    if self.context["current_sport"] not in self.context["talked_sports"]:
                        # Optimize the Suggest Sport Type to be True
                        self.user_sport_intents[
                            'sport_i_suggestsporttype'] = True
                    else:
                        self.user_sport_intents[
                            'sport_i_suggestsporttype'] = False

        # # Update the Intents Under Special Cases
        # if self.user_sport_intents['sport_i_askback'] and self.context["previous_state"] == "s_sport_followup_chitchat_ask":
        #     # Set the sport_i_askquestion as False
        #     self.user_sport_intents['sport_i_askquestion'] = False

        # If previous_state is ask favorite sport and no detected_sports
        if self.context["previous_state"] == "s_sport_favorite_sport_confirmation" and not self.context["detected_sports"]:
            # convert current sport to None
            self.context["current_sport"] = None

        # Update the followup questions
        self.update_followup_question_context()

        # Update Sport type and Sport Entity label
        self.previous_state = self.context['previous_state']
        # logging.info("[SPORTS_MODULE] detected_intents:
        # {}".format(self.user_sport_intents))

    def detect_sport(self, sentence_segment):
        r"""
        detect any sport type from the MAIN_SPORT_LIST that appears in the sentence segment. Update the sentence_segment.detected_sports[]
        """
        special_sport_mapping = {"run": "running",
                                 "shoot": "shooting",
                                 "bike": "biking",
                                 "dance": "dancing",
                                 "ski": "skiing",
                                 "swim": "swimming",
                                 "cycling": "biking"
                                 }

        sentence_segment['detected_sports'] = []
        for sport in MAIN_SPORT_LIST:
            sport_re = r"\b{sport}".format(sport=sport)
            if re.search(sport_re, sentence_segment['text']) and sport not in detect_ignored_sport(sentence_segment['text'], IGNORE_PATTERN):
                if special_sport_mapping.get(sport, None) is not None:
                    # fixing the grammatical mistake of some sport type
                    sentence_segment['detected_sports'].append(
                        special_sport_mapping[sport])
                else:
                    sentence_segment['detected_sports'].append(sport)

        for league in LEAGUE_SPORT_MAP.keys():  # Detect the sport type
            if re.search(league, sentence_segment['text']):
                sentence_segment['detected_sports'].append(
                    LEAGUE_SPORT_MAP[league])
        # sort the sentence_segment
        sentence_segment['detected_sports'].sort(key=lambda s: len(s.split()))
        sentence_segment['detected_sports'].reverse()
        # logging.info("[SPORTS] the sorted detected sports list is: {}".format(
        #     sentence_segment['detected_sports']))

    def detect_entity(self, sentence_segment, sentence_segment_index=0):
        r"""
        detect any name entity that matches our database in the sentence segment
            1. sys_ner: a list of string type, that represents the ners detected by the system.
            2. sport_ner: a list of string type, that represents the sport ners detected by the system.
        Update the sentence_segment['detected_entities']
        """
        sentence_segment['detected_entities'] = {"sys_ner": [], "sport_ner": []}
        #logging.debug("[SPORTS_MODULE] returnnlp_util.googlekg: {}".format(self.returnnlp_util.googlekg[sentence_segment_index]))
        sentence_segment['detected_entities']["sys_ner"] = get_sys_sport_entity(sentence_segment['text'], self.sys_info, self.returnnlp_util.googlekg[sentence_segment_index])
        #logging.debug('[SPORTS_MODULE] detected_entities_sys_ner: {}'.format(sentence_segment['detected_entities']["sys_ner"]))

        # Update the sport_ner
        if sentence_segment['detected_entities']["sys_ner"]:
            for candidate_ner in sentence_segment['detected_entities']["sys_ner"]:
                # logging.info('[SPORTS_MODULE]
                # detected_entities_sys_ner:{}'.format(sentence_segment['detected_entities']["sys_ner"]))
                for sport_entity in SPORT_UTTERANCE_DATA['qa_intro'].keys():
                    if re.search(candidate_ner['name'].lower().strip(), sport_entity.lower().strip()):
                        # In Sport NER, we will have the detected Known
                        # Entities
                        sentence_segment['detected_entities']["sport_ner"].append({"name": sport_entity, "label": SPORT_UTTERANCE_DATA['qa_intro'][
                            sport_entity]['entity_label'], "sport_type": SPORT_UTTERANCE_DATA['qa_intro'][sport_entity]['sport_type']})
                # update the detected_sport
                if candidate_ner['sport_type']:
                    sentence_segment['detected_sports'].append(
                        candidate_ner['sport_type'])


        for sport_entity in SPORT_UTTERANCE_DATA['qa_intro'].keys():
            if re.search(sport_entity.lower().strip(), sentence_segment['text']):
                sentence_segment['detected_entities']["sport_ner"].append({"name": sport_entity, "label": SPORT_UTTERANCE_DATA['qa_intro'][
                    sport_entity]['entity_label'], "sport_type": SPORT_UTTERANCE_DATA['qa_intro'][sport_entity]['sport_type']})

        #use detected teams to update the sport_ner
        detected_teams = detect_teams(self.sys_info['returnnlp'][0], current_sport=self.context["current_sport"], segment_index=sentence_segment_index)
        if detected_teams:
            for team in detected_teams:
                sentence_segment['detected_entities']['sys_ner'].append({"name": team["team"], "label": "team", "sport_type": team["sport_type"]})

    def detect_intent(self, sentence_segment, sentence_segment_index=0):
        """
            Define regex rules to detect sport submodule intents.
            Apply intent detection to the sentence segment
        """
        if any(re.search(t, sentence_segment['text']) for t in QUIT_MODULE_PATTERN):
            self.user_sport_intents["sport_i_notinterestsport"] = True
            sentence_segment['user_sport_intents'][
                "sport_i_notinterestsport"] = True
        elif self.context["previous_state"] == "s_sport_favorite_sport_confirmation" and re.search(r"none|nothing", sentence_segment['text']):
            self.user_sport_intents["sport_i_notinterestsport"] = True
            sentence_segment['user_sport_intents'][
                "sport_i_notinterestsport"] = True


        if any(re.search(t, sentence_segment['text']) for t in NO_INTEREST_PATTERNS) and not re.search(r"^why don't\b", sentence_segment['text']) and len(sentence_segment['text'].split()) > 2:
            logging.info(["[SPORT] NO Interest Pattern is detected"])

            self.user_sport_intents["sport_i_notinterest"] = True
            sentence_segment['user_sport_intents'][
                "sport_i_notinterest"] = True

            # Detect the sport type
            if sentence_segment["detected_sports"]:
                self.user_sport_intents["sport_i_notinterestsporttype"] = True
                sentence_segment['user_sport_intents'][
                    "sport_i_notinterestsporttype"] = True

            if re.search(r'(sport|sports)', sentence_segment['text']):
                self.user_sport_intents["sport_i_notinterestsport"] = True
                sentence_segment['user_sport_intents'][
                    "sport_i_notinterestsport"] = True
        else:
            # if sentence_segment["detected_sports"] and len(sentence_segment['text'].split()) < 4:
            #     for x in sentence_segment["detected_sports"]:
            #         if x and x != self.context["current_sport"]:
            #             self.user_sport_intents[
            #                 "sport_i_suggestsporttype"] = True
            #             sentence_segment['user_sport_intents'][
            #                 "sport_i_suggestsporttype"] = True
            #             break
            if sentence_segment["detected_sports"] and self.context["current_sport"] not in sentence_segment["detected_sports"] and len(sentence_segment['text'].split()) < 5:
                # If people give a positive statement and a different sport
                # type is detected
                self.user_sport_intents["sport_i_suggestsporttype"] = True
                sentence_segment['user_sport_intents'][
                    "sport_i_suggestsporttype"] = True

            if sentence_segment["detected_sports"] and (self.context["previous_state"] is None or self.context["previous_state"] == "s_sport_confirm_sport_continue"):
                self.user_sport_intents["sport_i_suggestsporttype"] = True
                sentence_segment['user_sport_intents'][
                    "sport_i_suggestsporttype"] = True
            # The length condition is to restrict the cases only apply to the
            # entity
            if (sentence_segment["detected_entities"]["sport_ner"] or sentence_segment["detected_entities"]["sys_ner"]) and len(sentence_segment['text'].split()) < 5:
                if self.context["previous_state"] != "s_sport_chitchat_q":
                    self.user_sport_intents["sport_i_suggestentity"] = True
                    sentence_segment['user_sport_intents'][
                        "sport_i_suggestentity"] = True

                elif sentence_segment["detected_sports"] and any(x != self.context["current_sport"] for x in sentence_segment["detected_sports"]):
                    self.user_sport_intents["sport_i_suggestentity"] = True
                    sentence_segment['user_sport_intents'][
                        "sport_i_suggestentity"] = True
        
        # detect suggestsporttype or suggestentity intent
        if any(re.search(t, sentence_segment['text']) for t in SUGGEST_PATTERNS) or detect_command(self.user_utterance, self.sys_info['returnnlp'][0]) and not sentence_segment['user_sport_intents']['sport_i_notinterest']:
            if sentence_segment["detected_entities"]["sport_ner"] or sentence_segment["detected_entities"]["sys_ner"]:
                self.user_sport_intents["sport_i_suggestentity"] = True
                sentence_segment['user_sport_intents'][
                    "sport_i_suggestentity"] = True

            if sentence_segment["detected_sports"] and self.context["current_sport"] not in sentence_segment["detected_sports"]:
                self.user_sport_intents["sport_i_suggestsporttype"] = True
                sentence_segment['user_sport_intents'][
                    "sport_i_suggestsporttype"] = True

            if re.search(r"(other sport|other sports|another sport)", sentence_segment['text']):
                self.user_sport_intents["sport_i_notinterestsporttype"] = True
                self.user_sport_intents["sport_i_notinterest"] = True
                sentence_segment['user_sport_intents'][
                    "sport_i_notinterestsporttype"] = True
                sentence_segment['user_sport_intents'][
                    "sport_i_notinterest"] = True

            if re.search(r"(talk about sport|talk about sports|talk sports)", sentence_segment['text']) and self.sys_info['last_module'] == 'SPORT':
                self.user_sport_intents["sport_i_requestsport"] = True
                sentence_segment['user_sport_intents'][
                    'sport_i_requestsport'] = True

        if self.context['previous_state'] == 's_sport_favorite_sport_confirmation' and sentence_segment["detected_sports"] and not sentence_segment['user_sport_intents']['sport_i_notinterest']:
            self.user_sport_intents["sport_i_suggestsporttype"] = True
            sentence_segment['user_sport_intents']["sport_i_suggestsporttype"] = True


        # News Transit to Sports
        proposed_sport_type_from_other_module = self.module_selection.get_top_propose_keyword().SPORTS.get("SPORT_TYPE", None)
        if proposed_sport_type_from_other_module:
            self.user_sport_intents["sport_i_suggestsporttype"] = True
            sentence_segment['user_sport_intents'][
                    "sport_i_suggestsporttype"] = True
            sentence_segment['user_sport_intents']["sport_i_suggestsporttype"] = True
            self.context["current_sport"] = proposed_sport_type_from_other_module
            sentence_segment['detected_sports'].append(proposed_sport_type_from_other_module)
        
        # detect ask_question intent
        # if
        # re.search(r"(who\b|what\b|when\b|where\b|why\b|how\b|which\b|^do\b|^does\b|^did\b|^was\b|^is\b|^are\b|^were\b|^will\b|^would\b)\s*(was|is|were|are|will|would|did|do|does|can|could|you|he|she|him|her|they|we)",
        # self.user_utterance):

        # if ((any(re.search(q, sentence_segment['text']) for q in
        # QUESTION_PATTERNS)) and detect_question_DA(sentence_segment)[1]) or
        # (detect_question_DA(sentence_segment)[1] and
        # detect_question_DA(sentence_segment)[2] > 0.9):
        if detect_question_DA_update(self.returnnlp_util, sentence_segment_index) and not detect_command(self.user_utterance, self.sys_info['returnnlp'][0]):
            # Special Logics to deal with the case when user request
            # information through a question
            if any(re.search(q, sentence_segment['text']) for q in REQUEST_INFO_QUESTION_PATTERNS) and re.search('sport|sports', sentence_segment['text']):
                pass
            elif any(re.search(q, sentence_segment['text']) for q in REQUEST_INFO_QUESTION_PATTERNS) and (sentence_segment["detected_entities"]["sport_ner"] or sentence_segment["detected_entities"]["sys_ner"]):
                self.user_sport_intents["sport_i_suggestentity"] = True
                sentence_segment['user_sport_intents'][
                    "sport_i_suggestentity"] = True
            elif any(re.search(q, sentence_segment['text']) for q in REQUEST_INFO_QUESTION_PATTERNS) and sentence_segment["detected_sports"]:
                self.user_sport_intents["sport_i_suggestsporttype"] = True
                sentence_segment['user_sport_intents'][
                    "sport_i_suggestsporttype"] = True
            else:
                self.user_sport_intents["sport_i_askquestion"] = True
                sentence_segment['user_sport_intents'][
                    "sport_i_askquestion"] = True
                # Detect whether it ask the opinion
                if re.search(r"(who\b|what\b|when\b|where\b|why\b|which\b|how\b|can|know|curious|interest|hear|tell|give|let|do).* (you|your).* (think|feel|like|love|thought|support|hate|believe|opinion|stance|view|say|favorite)", self.user_utterance):
                    self.user_sport_intents['sport_i_askopinion'] = True
                    sentence_segment['user_sport_intents'][
                        'sport_i_askopinion'] = True

        # Detect the Lexical Level Intents
        #logging.info("[SPORTS] the sentence_segment_text before detecting answerno: {}".format(sentence_segment['text']))
        # detect answer_no intent
        if re.search(r"^not|^no\b|^nope\b|(don't|do not) (like|enjoy|want)|(^i)\s(don't|can't|will not|didn't) (like|enjoy|want)", sentence_segment['text']) or any (x in sentence_segment['intent']['lexical'] for x in ['ans_negative', 'ans_neg']) or sentence_segment['dialog_act'][0] == "neg_answer":
            self.user_sport_intents['sport_i_answerno'] = True
            sentence_segment['user_sport_intents']['sport_i_answerno'] = True

        # detect answer_yes intent
        if re.search(r"yes|yeah|sure|\bi\b (\bdo\b|\bam\b|\bwant to\b)", sentence_segment['text']):
            self.user_sport_intents['sport_i_answeryes'] = True
            sentence_segment['user_sport_intents']['sport_i_answeryes'] = True

        # detect answer_uncertain intent
        if re.search(r"(\bi\b|\bi'm\b).* (not.* sure|unsure|uncertain|(don't|do not).* know|(don't|do not).* have)", sentence_segment['text']):
            self.user_sport_intents['sport_i_answeruncertain'] = True
            sentence_segment['user_sport_intents'][
                'sport_i_answeruncertain'] = True
        elif re.search(r"(not.* sure|(don't|do not).* know|(don't|do not).* have)", sentence_segment['text']):
            self.user_sport_intents['sport_i_answeruncertain'] = True
            sentence_segment['user_sport_intents'][
                'sport_i_answeruncertain'] = True

        # detect answer_ask_back intent
        if re.search(r"how about you|what about you|^what do you think$", sentence_segment['text']):
            self.user_sport_intents['sport_i_askback'] = True
            sentence_segment['user_sport_intents']['sport_i_askback'] = True
            sentence_segment['user_sport_intents'][
                'sport_i_askquestion'] = False
            self.user_sport_intents['sport_i_askquestion'] = False

        # detect request news intent
        if re.search('news', sentence_segment['text']):
            if any(re.search(t, sentence_segment['text']) for t in REQUEST_PATTERNS) or any(re.search(q, sentence_segment['text']) for q in QUESTION_PATTERNS):
                self.user_sport_intents['sport_i_requestnews'] = True
                sentence_segment['user_sport_intents'][
                    'sport_i_requestnews'] = True

        # Detect Abandoned Pattern
        # if detect_abandoned_answer_DA(sentence_segment, self.sys_info) and
        # self.context["previous_state"] != "s_sport_followup_chitchat_answer":
        # if detect_abandoned_answer_DA_update(self.returnnlp_util, sentence_segment_index, self.sys_info):
        #     self.user_sport_intents['sport_i_answerabandoned'] = True
        #     sentence_segment['user_sport_intents'][
        #         'sport_i_answerabandoned'] = True
        #     if not self.user_sport_intents['sport_i_askquestion']:
        #         # Add this flag to use appropriate response
        #         self.user_sport_intents[
        #             'sport_i_answerabandoned_not_question'] = True

        #         self.user_sport_intents['sport_i_askquestion'] = True
        #         sentence_segment['user_sport_intents'][
        #             'sport_i_askquestion'] = True

        # Detect opinion Neg
        #logging.debug("[SPORTS_MODULE] sentence_segment keys: {}".format(sentence_segment.keys()))
        if 'opinion_negative' in sentence_segment['intent']['lexical']:
            sentence_segment['user_sport_intents']['sport_i_opinionneg'] = True
            self.user_sport_intents['sport_i_opinionneg'] = True


    def update_followup_question_context(self):
        r"""
        Determine whether we should include the specific followup in the context_list
        """
        # The special followup key:['follow up reasoning', 'answer_yes']
        try:
            # check the follow_up keys
            # if ("selected_question" not in self.context["conversation_context"]
            #         or "follow_up" not in self.context["conversation_context"]["selected_question"]):
            #     return
            selected_chitchat_id = self.context["selected_chitchat_id"]
            selected_chitchat_key = self.context["selected_chitchat_key"]
            #general_sport_questions = self.context["general_sport_questions"]
            selected_question = self.context["selected_question"]

            if not selected_chitchat_id and not selected_question:
                return
            if not selected_question:
                selected_question = sport_utterance(selected_chitchat_key, get_all=True)[
                    selected_chitchat_id]
            
            logging.debug("[SPORTS_MODULE] selected question for follow_up keys: {}".format(selected_question.get("follow_up", {}).keys()))
            if selected_question.get("follow_up", None) is None:
                return

            for x in list(selected_question["follow_up"].keys()):
                # logging.info("[SPORTS_MODULE] follow_up keys x is: {}".format(x))
                if x == "follow_up_answer_yes_no_sport" and not self.context["detected_sports"] and self.user_sport_intents["sport_i_answeryes"]:
                    self.context[
                        "followup_questions"] += selected_question["follow_up"][x]

                    # Change the expect_next_state to chit_chat_ask
                    if self.context["expect_next_state"] == "s_sport_confirm_sport_continue":
                        self.context["expect_next_state"] = "s_sport_chitchat_q"

                    logging.info(
                        "[SPORT] Special Followup, follow_up_answer_yes_no_sport,  is captured")

                if x == "follow_up_answer_no" and self.user_sport_intents["sport_i_answerno"]:
                    self.context[
                        "followup_questions"] += selected_question["follow_up"][x]

                    # Change the expect_next_state to chitchat_ask
                    if self.context["expect_next_state"] == "s_sport_confirm_sport_continue":
                        self.context["expect_next_state"] = "s_sport_chitchat_q"
                    logging.info(
                        "[SPORT] Special Followup, follow_up_answer_no,  is captured")

                if x == "follow_up_answer_yes" and self.user_sport_intents["sport_i_answeryes"]:

                    self.context[
                        "followup_questions"] += selected_question["follow_up"][x]

                    # Change the expect_next_state to chitchat_ask
                    if self.context["expect_next_state"] == "s_sport_confirm_sport_continue":
                        self.context["expect_next_state"] = "s_sport_chitchat_q"
                    logging.info(
                        "[SPORT] Special Followup, follow_up_answer_yes,  is captured")

                if x == "follow_up_answer_yes_short" and self.user_sport_intents["sport_i_answeryes"] and len(self.user_utterance.split()) < 6:

                    self.context[
                        "followup_questions"] += selected_question["follow_up"][x]

                    # Change the expect_next_state to chitchat_ask
                    if self.context["expect_next_state"] == "s_sport_confirm_sport_continue":
                        self.context["expect_next_state"] = "s_sport_chitchat_q"
                    logging.info(
                        "[SPORT] Special Followup, follow_up_answer_yes_short,  is captured")

                if x == "general_followup":
                    self.context["followup_questions"] += selected_question["follow_up"][x]
                    if self.context["expect_next_state"] == "s_sport_confirm_sport_continue":
                        self.context["expect_next_state"] = "s_sport_chitchat_q"
                    logging.debug(
                        "[SPORT] General followup is captured")


        except Exception as e:
            logging.error(
                "[SPORT] Something Wrong happens in followup questions. err: {}".format(e), exc_info=True)
            pass

    def transit_state(self):
        # Create a Transition Objective to Handle Transition
            # This Step ensures that no matter how we enter the state, we will
            # be guided to the initalization of the Sport System
        if self.previous_state:
            # Indicates that it is previously in the world cup subsystem
            if re.search(r'worldcup', self.previous_state):
                self.previous_state = None
        sport_transition_machine = Sport_Transition(
            self.previous_state, user_utterance=self.user_utterance, user_intent=self.user_intent, context=self.context)

        state_dict = sport_transition_machine.get_current_state()

        # Update the context information
        # if self.context["previous_state"] == "s_sport_ini_chitchat":
        #     self.context["suggest_entity_rejection"] = sport_transition_machine.context[
        #         "suggest_entity_rejection"]
        if self.context["previous_state"] == "s_sport_favorite_sport_confirmation":
            self.context["num_error_status"] = sport_transition_machine.context[
                "num_error_status"]  # Update the num error_status

        self.current_state = state_dict["current_state"]
        logging.info("[SPORTS_MODULE] current sport module state is: {}".format(
            self.current_state))

    def get_response(self):
        # Get the Corresponding Response Function.
        # Initialize a Sport Reponse Objective
        # sport_response_machine = Sport_Response(self.current_state,user_utterance=self.user_utterance,user_intent=self.user_intent,context=self.context)
        # Create a Transition Objective to Handle Transition
        sport_response_machine = Sport_Response(
            self.current_state, user_attributes=self.user_attributes, user_utterance=self.user_utterance, user_intent=self.user_intent, context=self.context, sys_info=self.sys_info)

        sport_response = sport_response_machine.generate_response()
        
        logging.debug("[SPORTS_MODULE] the sports module response is: {}".format(sport_response["response"]))
        # Check if "sport_type" is provided, and update current_sport if the
        # answer is Yes.
        if sport_response.get("sport_type", False) != False:
            self.context["current_sport"] = sport_response["sport_type"]
            if self.context["current_sport"]:
                if self.context["current_sport"] not in self.context["talked_sports"]:
                    self.context["talked_sports"].append(
                        self.context["current_sport"])
        elif self.context["current_sport"]: 
            if self.context["current_sport"] not in self.context["talked_sports"]:
                self.context["talked_sports"].append(self.context["current_sport"])


        if sport_response.get("name_entity", False) != False:
            self.context["current_entity"][
                'name'] = sport_response['name_entity']['name']
            self.context["current_entity"][
                'label'] = sport_response['name_entity']['label']
            if self.context["current_entity"]['name']:
                if self.context["current_entity"]['name'] not in self.context["talked_entities"]:
                    self.context["talked_entities"].append(
                        self.context["current_entity"]["name"])
        elif self.context["current_entity"]['name'] and check_talked_entities(self.context["current_entity"]["name"], self.context["talked_entities"]):
            self.context["talked_entities"].append(self.context["current_entity"]["name"])

        # Check it "num_error_status" is provided, and update the context
        if sport_response.get("num_error_status", None) is not None:
            self.context["num_error_status"] = sport_response[
                "num_error_status"]
        else:
            self.context["num_error_status"] = 0

        # Check if "expect_next_stats" is provided, and update the context
        if sport_response.get('expect_next_state', False) != False:
            self.context['expect_next_state'] = sport_response[
                'expect_next_state']
        else:
            self.context['expect_next_state'] = None

        if sport_response.get('selected_funfacts', None) is not None:
            self.context['selected_funfacts'] = sport_response[
                'selected_funfacts']

        if sport_response.get('selected_news', None) is not None:
            self.context['selected_news'] = sport_response['selected_news']

        if sport_response.get('talked_moments'):
            self.context['talked_moments'] = sport_response[
                'talked_moments'][:]

        if sport_response.get('selected_moment'):
            self.context['selected_moment'] = sport_response['selected_moment']

            # class UserAttributes:

            #     def __init__(self, prev_hash):
            #         self.prev_hash = prev_hash

            # ua = UserAttributes({})
        if sport_response.get("modified_current_state", None) is not None:
            self.current_state = sport_response["modified_current_state"]
        
        if sport_response.get("2sport_type", False):
            self.context["2sport_type"] = True
        else:
            self.context["2sport_type"] = False
        
        response_dict = {
            'response': sport_response['response'],
            'user_attributes':
            {
                'sportchat': {
                    'previous_state': self.current_state,
                    'previous_system_response': sport_response['response'],
                    'previous_sport': self.context["current_sport"],
                    'previous_entity': self.context["current_entity"],
                    'talked_sports': self.context["talked_sports"],
                    'talked_entities': self.context["talked_entities"],
                    'asked_questions': self.context["asked_questions"],
                    'suggest_entity_rejection': str(self.context["suggest_entity_rejection"]),
                    'num_error_status': str(self.context['num_error_status']),
                    'expect_next_state': self.context['expect_next_state'],
                    'conversation_id': self.context['conversation_id'],
                    'propose_continue': sport_response['propose_continue'],
                    # Changed by Mingyang Zhou
                    'not_interested_sports': self.context['not_interested_sports'],
                    # The candidate funfacts from Reddit Dataset
                    'selected_funfacts': self.context['selected_funfacts'],
                    'selected_news': self.context['selected_news'],
                    # The titles of all the talked moments, Added by Mingyang
                    # Zhou
                    'talked_moments': self.context['talked_moments'],
                    # Store the selected moment to provide corresponding moment
                    # opinions.
                    'selected_moment': self.context['selected_moment'],
                    'talked_web_content': self.context['talked_web_content'],
                    'followup_questions': self.context['followup_questions'],
                    'general_sport_questions': self.context['general_sport_questions'],
                    'selected_question': self.context['selected_question'],
                    "chitchat_mode": self.context["chitchat_mode"],
                    "2sport_type": self.context["2sport_type"],
                    "favorite_sports": self.context["favorite_sports"]
                },
            },
            # 'prev_hash': ua.prev_hash
        }

        # if propose_topic is not None, then we propose topic
        if self.context['propose_topic']:
            self.module_selection.propose_topic = self.context['propose_topic']
        # Update User Profile
        if self.context["favorite_sports"]:
            self.user_profile.topic_module_profiles.sport._storage['fav_sport_type'] = self.context["favorite_sports"][0]
        if self.context["talked_sports"]:
            self.user_profile.topic_module_profiles.sport._storage['talked_sport_types'] = self.context["talked_sports"].copy()
        
        return response_dict
