import logging
import random
from typing import Dict, List, Tuple, Set, Optional, Union, Any
from enum import Enum

from user_attribute_adaptor import UserAttributeAdaptor
from user_profiler.user_profile import UserProfile
from nlu.dataclass.returnnlp import ReturnNLP, ReturnNLPSegment
from nlu.constants import TopicModule
from user_profiler.user_profile import UserProfile

logger = logging.getLogger(__name__)

MODULE_RANK_IGNORE_THRESHOLD = 0.15
NEG_SENTIMENT_THRESHOLD = 0.75
from dataclasses import dataclass, field, asdict, astuple


class ProposeContinue(Enum):
    CONTINUE = "CONTINUE"
    UNCLEAR = "UNCLEAR"
    STOP = "STOP"


topics_available_for_proposal = {
    TopicModule.FASHION.value,
    TopicModule.ANIMAL.value,
    TopicModule.BOOK.value,
    TopicModule.SPORTS.value,
    TopicModule.MUSIC.value,
    TopicModule.TRAVEL.value,
    TopicModule.FOOD.value,
    TopicModule.GAME.value,
    TopicModule.NEWS.value,
    TopicModule.TECHSCI.value,
    TopicModule.MOVIE.value,
}

# default topic priority
topic_priority_3 = [
    TopicModule.ANIMAL.value,
    TopicModule.MOVIE.value,
    TopicModule.MUSIC.value,
    TopicModule.SPORTS.value,
    TopicModule.GAME.value,
    TopicModule.FOOD.value,
    TopicModule.FASHION.value,
    TopicModule.BOOK.value,
    TopicModule.NEWS.value,
    TopicModule.TECHSCI.value,
    TopicModule.TRAVEL.value,
]

# female topic priority
topic_priority_2 = [
    TopicModule.MOVIE.value,
    TopicModule.MUSIC.value,
    TopicModule.ANIMAL.value,
    TopicModule.FASHION.value,
    TopicModule.GAME.value,
    TopicModule.FOOD.value,
    TopicModule.BOOK.value,
    TopicModule.SPORTS.value,
    TopicModule.NEWS.value,
    TopicModule.TRAVEL.value,
    TopicModule.TECHSCI.value,
]

# male topic priority
topic_priority_1 = [
    TopicModule.MOVIE.value,
    TopicModule.GAME.value,
    TopicModule.SPORTS.value,
    TopicModule.MUSIC.value,
    TopicModule.ANIMAL.value,
    TopicModule.TECHSCI.value,
    TopicModule.FOOD.value,
    TopicModule.NEWS.value,
    TopicModule.BOOK.value,
    TopicModule.FASHION.value,
    TopicModule.TRAVEL.value,
]

personal_topic_map = {
    "male": topic_priority_1,
    "female": topic_priority_2,
    "unknown": topic_priority_3
}

topic_name_map = {
    'SPORT': {'sports'},
    'TECHSCIENCECHAT': {'technology', 'science'},
    'MOVIECHAT': {'movies'},
    'BOOKCHAT': {'books'},
    'TRAVELCHAT': {'traveling'},
    'GAMECHAT': {'games', 'video games'},
    'NEWS': {'the news'},
    'ANIMALCHAT': {'animals'},
    'MUSICCHAT': {'music'},
    'WEATHER': {'the weather'},
    'HOLIDAYCHAT': {'holidays'},
    'FOODCHAT': {'food'},
    'FASHIONCHAT': {'fashion'}
}


module_and_user_attributes_key_map = {
    'MOVIECHAT': 'moviechat_user',
    'BOOKCHAT': 'bookchat_user',
    'TRAVELCHAT': 'travelchat',
    'GAMECHAT': 'gamechat',
    'ELECTIONCHAT': 'electionchat',
    'NEWS': 'news_user',
    'ANIMALCHAT': 'animalchat',
    'MUSICCHAT': 'musicchat',
    'SOCIAL': 'socialchat',
    'WEATHER': 'weatherchat',
    'HOLIDAYCHAT': 'holidaychat',
    'REQ_TIME': 'timechat',
    'SAY_COMFORT': 'comfortchat',
    'SPORT': 'sportchat',
    'TECHSCIENCECHAT': 'techsciencechat',
    'RECOMMENDATIONTCHAT': 'tedtalkchat',
    'FOODCHAT': 'foodchat',
    'FASHIONCHAT': 'fashionchat',
    'LAUNCHGREETING': 'launchgreeting',
    'DAILYLIFE': 'dailylife',
    'OUTDOORCHAT': 'outdoorchat',
    'SAY_FUNNY': 'sayfunny',
    'RETRIEVAL': 'retrieval',
    'SELFDISCLOSURE': 'selfdisclosure',

    # self-disclosure-study
    'COVIDCHAT': 'covidchat',
}


class GlobalState(Enum):
    NA = "na"
    ASK_RECENT_MOVIE = "ask_recent_movie"

    ASK_RECENT_BOOK = "ask_recent_book"

    ASK_FAV_MUSIC_GENRE = "ask_fav_music_genre"

    ASK_RECENT_PLAYED_GAME = "ask_recent_played_game"

    ASK_FAV_SPORT = "ask_fav_sport"

    ASK_FAV_ANIMAL = "ask_fav_animal"
    ASK_HAVE_PET = "ask_have_pet"

    ASK_ONLINE_SHOPPING = "ask_online_shopping"

    ASK_LAST_VISITED_PLACE = "ask_last_visited_place"

    ASK_FAV_FOOD = "ask_fav_food"
    ASK_FAV_CUISINE = "ask_fav_cuisine"

    ASK_CLOTHES = "ask_clothes"
    ASK_COMFORT_VS_PRACTICAL = "ask_comfort_vs_practical"

    FRIENDSHIP_OPEN_QUESTION = "ask_friendship_open_question"
    FAMILY_OPEN_QUESTION = "ask_family_open_question"


global_state_map = {
    # TopicModule.MOVIE.value: [GlobalState.ASK_RECENT_MOVIE], TODO: waiting for kaihui
    TopicModule.MUSIC.value: [GlobalState.ASK_FAV_MUSIC_GENRE],
    TopicModule.GAME.value: [GlobalState.ASK_RECENT_PLAYED_GAME],
    TopicModule.SPORTS.value: [GlobalState.ASK_FAV_SPORT],
    TopicModule.ANIMAL.value: [GlobalState.ASK_HAVE_PET, GlobalState.ASK_FAV_ANIMAL],
    # TopicModule.TECHSCI.value: [GlobalState.ASK_ONLINE_SHOPPING],
    TopicModule.FOOD.value: [GlobalState.ASK_FAV_FOOD, GlobalState.ASK_FAV_CUISINE],
    TopicModule.TRAVEL.value: [GlobalState.ASK_LAST_VISITED_PLACE],
    TopicModule.FASHION.value: [GlobalState.ASK_CLOTHES],
}

discussed_module_global_state_map = {
    # TopicModule.MOVIE.value: [GlobalState.ASK_RECENT_MOVIE], TODO: waiting for kaihui
    TopicModule.MUSIC.value: [GlobalState.ASK_FAV_MUSIC_GENRE],
    TopicModule.GAME.value: [GlobalState.ASK_RECENT_PLAYED_GAME],
    TopicModule.SPORTS.value: [GlobalState.ASK_FAV_SPORT],
    TopicModule.ANIMAL.value: [GlobalState.ASK_FAV_ANIMAL],
    # TopicModule.TECHSCI.value: [GlobalState.ASK_ONLINE_SHOPPING],
    TopicModule.FOOD.value: [GlobalState.ASK_FAV_FOOD],
    TopicModule.TRAVEL.value: [GlobalState.ASK_LAST_VISITED_PLACE],
    # TopicModule.FASHION.value: [GlobalState.ASK_CLOTHES],
}


@dataclass
class ProposeKeywordEntity:
    text: str = ""
    entity_type: List[str] = field(default_factory=list)

    # Deprecated
    SPORTS: dict = field(default_factory=dict)   # deprecated,for backward compatibility only
    NEWS: List[str] = field(default_factory=list)  # deprecated,for backward compatibility only

    @classmethod
    def from_dict(cls, d: Dict[str, Any]):
        return cls(
            text=d.get("text", ""),
            entity_type=d.get("entity_type", ""),
            SPORTS=d.get("SPORT", {}),
            NEWS=d.get("NEWS", [])
        )

    @classmethod
    def from_preferred_entity(cls, preferred_entity: UserProfile.PreferrdEntityTopic):
        return cls(
            text=preferred_entity.entity_detected,
            entity_type=preferred_entity.entity_type,
        )

class ModuleSelector(UserAttributeAdaptor):
    interruption_modules = [TopicModule.ADJUST_SPEAK.value,
                            TopicModule.CLARIFICATION.value,
                            TopicModule.TERMINATE.value]

    def __init__(self, user_attributes_ref):
        super().__init__('module_selection', user_attributes_ref, auto_add_missing_keys=True)

    @staticmethod
    def get_topic_module_natural_name(module_name):
        return random.sample(topic_name_map[module_name], 1)[0]

    def get_personal_topic_proposal_order(self):
        user_profile = UserProfile(self.ua_ref)
        gender = user_profile.gender
        personal_topics = personal_topic_map[gender]
        return personal_topics

    @property
    def module_rank(self) -> Dict[str, float]:
        if 'module_rank' not in self._storage:
            self['module_rank'] = self.create_initial_module_rank()

        return self['module_rank']

    @module_rank.setter
    def module_rank(self, value: Dict[str, float]):
        if not isinstance(value, dict):
            logger.warning(f"module_rank is being set with an invalid value: {value}. Ignoring.")
            return

        self['module_rank'] = value

    def init_module_rank(self):
        self.module_rank = self.create_initial_module_rank()

    @staticmethod
    def create_initial_module_rank():
        module_rank = {}
        for topic in topics_available_for_proposal:
            module_rank[topic] = 0.0
        return module_rank

    def update_module_rank(self, module_candidates, returnnlp_raw: List[dict]):
        logger.info(f"update_module_rank: module_candidates = {module_candidates} ")

        returnnlp = ReturnNLP.from_list(returnnlp_raw)

        cur_modules = []

        for returnnlp_segment in returnnlp:
            if returnnlp_segment.sentiment.neg < NEG_SENTIMENT_THRESHOLD:
                for module in returnnlp_segment.detect_topic_module_from_system_level():
                    if module_candidates[0] in ['LAUNCHGREETING']:
                        continue
                    elif module_candidates[0] in ['ANIMALCHAT', 'FOODCHAT']:
                        if module not in ['ANIMALCHAT', 'FOODCHAT']:
                            cur_modules.append(module)
                    elif module != module_candidates[0]:
                        cur_modules.append(module)

        cur_modules = self.unique(cur_modules)
        for module in self.module_rank:
            if module in cur_modules:
                self.module_rank[module] = self.module_rank[module] + 1
            else:
                self.module_rank[module] = self.module_rank[module] * 0.7

    def init_module_user_attributes(self):
        for key, value in module_and_user_attributes_key_map.items():
            setattr(self.ua_ref, value, {})

    def get_module_user_attributes(self, module_name: str):
        logging.info("get_module_user_attributes")
        module_user_attribute = None
        module_key_name = module_and_user_attributes_key_map.get(module_name, None)
        logging.info(f"module_key_name: {module_key_name}")

        if module_key_name:
            module_user_attribute = getattr(self.ua_ref, module_key_name, None)
            logging.info(f"module_key_name: {module_user_attribute}")

            if not module_user_attribute:
                setattr(self.ua_ref, module_key_name, {})
                module_user_attribute = getattr(self.ua_ref, module_key_name, None)

        return module_user_attribute

    def get_propose_continue_module(self) -> Optional[str]:
        for module in module_and_user_attributes_key_map:
            if self.get_propose_continue(module) == 'CONTINUE':
                return module
        return None

    def get_propose_continue(self, module_name):
        module_user_attributes = self.get_module_user_attributes(module_name)
        return module_user_attributes.get('propose_continue', 'STOP') if module_user_attributes else 'STOP'

    def set_propose_continue(self, module_name, state: str):
        module_user_attributes = self.get_module_user_attributes(module_name)
        module_user_attributes['propose_continue'] = state

    @property
    def current_selected_module(self) -> Optional[TopicModule]:
        if 'current_selected_module' not in self._storage:
            self['current_selected_module'] = TopicModule.NA.value

        return TopicModule(self['current_selected_module'])

    @current_selected_module.setter
    def current_selected_module(self, module: TopicModule):
        self['current_selected_module'] = module.value

    # TODO: change this to property
    @property
    def last_topic_module(self):
        """
        :return: the last used main topic module before current turn.
        """
        previous_modules = self.get_previous_modules()
        if previous_modules:
            return previous_modules[0]

        return None

    @property
    def last_module(self):
        previous_modules_raw = self._get_previous_modules_raw()
        return previous_modules_raw[0] if previous_modules_raw else None

    def get_previous_modules(self):
        previous_modules = self._get_previous_modules_raw()

        # Filtered out irrelevant modules like launch, social or terminate
        topic_modules = [topic for topic in previous_modules if topic not in self.interruption_modules]
        return topic_modules

    def _get_previous_modules_raw(self):
        return self._get_raw("previous_modules")

    @property
    def propose_topic(self) -> str:
        if 'propose_topic' not in self._storage or self['propose_topic'] is None:
            proposed_topic_raw = self._get_raw("propose_topic")
            self['propose_topic'] = proposed_topic_raw

        return self['propose_topic']

    @propose_topic.setter
    def propose_topic(self, topic_module: Optional[Union[TopicModule, str]]):
        if isinstance(topic_module, TopicModule):
            self['propose_topic'] = topic_module.value
            self._set_raw('propose_topic', topic_module.value)
        else:
            self['propose_topic'] = topic_module
            self._set_raw('propose_topic', topic_module)

    @property
    def propose_keywords(self) -> List[dict]:
        value = []
        if 'propose_keywords' not in self._storage or self['propose_keywords'] is None:
            if self._get_raw('suggest_keywords'):
                self['propose_keywords'] = self._get_raw('suggest_keywords')
            else:
                self['propose_keywords'] = []

        return self['propose_keywords']

    @propose_keywords.setter
    def propose_keywords(self, keywords: List[dict]):
        if keywords is None:
            propose_keywords = []
        else:
            propose_keywords = keywords
        self['propose_keywords'] = propose_keywords
        self._set_raw('suggest_keywords', propose_keywords)

    def add_propose_keyword(self, keyword: Union[str, Dict, UserProfile.PreferrdEntityTopic, ProposeKeywordEntity]):
        logger.info(f"[add_propose_keyword] {keyword}, type: {type(keyword)}")
        if isinstance(keyword, str):
            entity = {
                    "text": keyword
                }
            self.propose_keywords.append(entity)
        elif isinstance(keyword, Dict):
            entity = keyword
            self.propose_keywords.append(entity)
        elif isinstance(keyword, UserProfile.PreferrdEntityTopic):
            entity = ProposeKeywordEntity.from_preferred_entity(keyword).__dict__
            self.propose_keywords.append(entity)

        logger.info(f"[propose_keyword] {self.propose_keywords}")
        self._set_raw('suggest_keywords',  self.propose_keywords)

    def get_top_propose_keyword(self) -> ProposeKeywordEntity:
        if self.propose_keywords:
            return ProposeKeywordEntity.from_dict(self.propose_keywords[0])
        else:
            return ProposeKeywordEntity.from_dict(dict())

    def get_propose_keywords_text_only(self):
        return [ProposeKeywordEntity.from_dict(item).text for item in self.propose_keywords if ProposeKeywordEntity.from_dict(item).text]

    @property
    def global_state(self) -> GlobalState:
        if 'global_state' not in self._storage:
            self['global_state'] = 'na'

        return GlobalState(self['global_state'])

    @global_state.setter
    def global_state(self, global_state: GlobalState):
        self['global_state'] = global_state.value

    @property
    def used_topic_modules(self) -> List[str]:
        """
        :return: Used topic modules, including the ones being discussed and the ones being proposed
        """
        if 'used_topic_modules' not in self._storage:
            self['used_topic_modules'] = []
        return self['used_topic_modules']

    @used_topic_modules.setter
    def used_topic_modules(self, value: List[str]):
        if value is None:
            value = []
        self['used_topic_modules'] = value

    def add_used_topic_module(self, module: str):
        logger.info(f"add_used_topic_module: {module}")

        if module not in self.used_topic_modules:
            self.used_topic_modules.append(module)

            # Reset used topic modules if running out of all modules
            available_personal_topics = self.get_available_topics()
            if len(available_personal_topics) == 0:
                # all modules have been discussed, set all_modules_discussed in user attribute
                setattr(self.ua_ref, "all_modules_discussed", True)
                self.set_used_module_reset_number()
                self.used_topic_modules = []

            return True
        else:
            return False
    
    def set_used_module_reset_number(self):
        logger.info("[Module Selection] reset used_module_list")
        # set used_module_reset_number
        used_module_reset_number = getattr(self.ua_ref, "used_module_reset_number", None)
        if not used_module_reset_number:
            used_module_reset_number = 0
        used_module_reset_number += 1
        setattr(self.ua_ref, "used_module_reset_number", used_module_reset_number)
        # if reset the module for more than twice, clean disliked_topic_module_list
        if used_module_reset_number > 1:
            self.disliked_topic_modules = []
        
 
    @property
    def disliked_topic_modules(self) -> List[str]:
        """
        :return: Disliked topic modules, including the ones being proposed and user said no
        """
        if 'disliked_topic_modules' not in self._storage:
            self['disliked_topic_modules'] = []
        return self['disliked_topic_modules']

    @disliked_topic_modules.setter
    def disliked_topic_modules(self, value: List[str]):
        if value is None:
            value = []
        self['disliked_topic_modules'] = value

    def add_disliked_topic_module(self, module: str):
        logger.info(f"add_disliked_topic_module: {module}")

        if module not in self.disliked_topic_modules:
            self.disliked_topic_modules.append(module)

            # Reset used topic modules if running out of all modules
            available_personal_topics = self.get_available_topics()
            if len(available_personal_topics) == 0:
                self.set_used_module_reset_number()
                self.used_topic_modules = []
                available_personal_topics = self.get_available_topics()
                if len(available_personal_topics) == 0:
                    self.disliked_topic_modules = []
            return True
        else:
            return False

    @property
    def out_of_template_topic_modules(self) -> List[str]:
        """
        :return:  topic modules that run out of templates
        """
        if 'out_of_template_topic_modules' not in self._storage:
            self['out_of_template_topic_modules'] = []
        return self['out_of_template_topic_modules']

    @out_of_template_topic_modules.setter
    def out_of_template_topic_modules(self, value: List[str]):
        if value is None:
            value = []
        self['out_of_template_topic_modules'] = value

    def add_out_of_template_topic_module(self, module: str):
        logger.info(f"add_out_of_template_topic_module: {module}")

        if module not in self.out_of_template_topic_modules:
            self.out_of_template_topic_modules.append(module)

            # Reset used topic modules if running out of all modules
            available_personal_topics = self.get_available_topics()
            if len(available_personal_topics) == 0:
                self.set_used_module_reset_number()
                self.used_topic_modules = []
                available_personal_topics = self.get_available_topics()
                if len(available_personal_topics) == 0:
                    self.disliked_topic_modules = []
                    available_personal_topics = self.get_available_topics()
                    if len(available_personal_topics) == 0:
                        self.out_of_template_topic_modules = []
            return True
        else:
            return False

    def get_available_topics(self):
        """
        :return: topics not proposed or discussed yet, sorted by topic priority
        """
        personal_topics = self.get_personal_topic_proposal_order()

        # filter available_topics with used_topic
        topics = [topic for topic in personal_topics
                  if (topic not in self.used_topic_modules and topic not in self.disliked_topic_modules and topic not in self.out_of_template_topic_modules)]
        return topics

    @property
    def preferred_entity_topics(self) -> List[UserProfile.PreferrdEntityTopic]:
        user_profile = UserProfile(self.ua_ref)
        return user_profile.preferred_entity_topics

    def has_available_preferred_topics(self):
        return len(self.get_available_preferred_topics()) > 0

    def get_available_preferred_topics(self) -> List[str]:
        available_entity_topics = self.get_available_preferred_entity_topics()
        available_preferred_topics = self.unique([elem.module.value for elem in available_entity_topics])
        return available_preferred_topics

    def get_available_preferred_entity_topics(self) -> List[UserProfile.PreferrdEntityTopic]:
        available_topics = self.get_available_topics()
        preferred_entity_topics = self.preferred_entity_topics
        modules = self.unique([elem.module.value for elem in preferred_entity_topics])
        available_modules = set(modules).intersection(set(available_topics))
        available_entity_topics = [elem for elem in preferred_entity_topics if elem.module.value in available_modules]
        return available_entity_topics

    def get_sorted_available_preferred_topics(self) -> List[str]:
        """
        :return: available preferred topics sorted by personal topic proposal order
        """
        available_preferred_topics = self.get_available_preferred_topics()

        if not available_preferred_topics:
            return []

        personal_topic_order = self.get_personal_topic_proposal_order()
        result = []
        for topic in personal_topic_order:
            if topic in available_preferred_topics:
                result.append(topic)

        return result

    def get_next_modules(self, amount=1) -> List[Tuple[str, str]]:
        """
        :param amount: number of modules to be returned
        :return: Next proposable modules. If all topics have been used, loop from the beginning
        """

        if amount > len(self.get_personal_topic_proposal_order()):
            amount = len(self.get_personal_topic_proposal_order())

        # First try get from available preferred topic
        next_modules = self.get_sorted_available_preferred_topics()

        # if not enough, try get from other available topics
        if len(next_modules) < amount:
            other_modules = []

            other_modules.extend(x for x in self.get_available_topics()
                                 if x not in next_modules)
            # if still not enough, get more
            if len(next_modules) + len(other_modules) < amount:
                other_modules.extend(x for x in self.get_personal_topic_proposal_order()
                                     if x not in next_modules + other_modules)
            # sort other topics by module rank
            ranks = self.module_rank
            other_modules.sort(
                key=lambda t: ranks[t]
                if (ranks[t] > MODULE_RANK_IGNORE_THRESHOLD)
                else float('-inf'), reverse=True)

            next_modules.extend(other_modules)

            # TODO: might need to improve which modules to be ranked if amount is large

        # get modules and their utterances
        modules = next_modules[:amount]
        utts = [random.sample(topic_name_map[m], k=1)[0] for m in modules]

        return list(zip(modules, utts))

    @staticmethod
    def unique(sequence):
        seen = set()
        return [x for x in sequence if not (x in seen or seen.add(x))]
