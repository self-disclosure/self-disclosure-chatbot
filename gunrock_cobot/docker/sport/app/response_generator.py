import json
import logging
import random
from types import SimpleNamespace

import requests
import re
import sys
import time
from sport_chatbot import Sport_Chatbot
from utils import set_logger_level

from nlu.dataclass import CentralElement, ReturnNLP, ReturnNLPSegment
from nlu.constants import DialogAct as DialogActEnum

set_logger_level(logging)


class SportExpert(object):
    """it gets any utterance and return a response about sport"""

    def __init__(self, utterance):
        self.utterance = utterance
        pass

    def return_response(self):
        return "Hi, I am Sport Expert. Let's talk about sports."


# required_context = ['intent', 'slots', 'text', 'ner']
required_context = ['intent', 'slots']


def get_required_context():
    return required_context


def handle_message(msg):
    #Attempt the new ReturnNLP util
    # returnnlp = ReturnNLP(msg['returnnlp'][0])
    # logging.info("[SPORTS] check if dialog act opinion is there: {}".format(returnnlp.has_dialog_act(DialogActEnum.OPINION)))

    # your response generator model should operate on the text or other context information here
    # 1. Get the Input Utterance
    Utterance = msg['slots'][0]['text']['value'].lower().strip()
    # 2. Get Previous Context and User Intent
    # Retrieval Previous Module to determine how we enter the state.
    previous_module = msg['previous_modules'][0]  # msg['last_module']

    previous_modules = msg['previous_modules']
    logging.info(
        "[SPORTS] The previous_modules are: {}".format(previous_modules))
    # logging.info("[SPORTS_MODULE] The last module in the previous utterance is: {}".format(
    #     previous_module))
    # 4. Get the System Level Intent

    sys_intent = msg['features'][0]['intent_classify']

    # Added by Mingyang Zhou,9/21/2018
    sys_intent_2 = msg['features'][0]['intent_classify2']

    # Added by Mingyang Zhou, 10/04/2018
    sys_central_elem = msg['central_elem'][0]

    # 5. Get the System Level NERs
    try:
        sys_ner = msg['features'][0]['ner']
    except Exception:
        sys_ner = []
    #logging.debug("[SPORTS_MODULE] The NER at Current Utterance are: {}".format(sys_ner))

    # Get the content around topic

    try:
        sys_topic = msg['features'][0]['topic']
    except Exception:
        sys_topic = []
    '''
    try:
        sys_topic = []
        for x in msg['features'][0]['intent_classify2']:
            sys_topic += x['topic']
    except:
        sys_topic = []
    logging.info("[SPORTS_MODULE] Detected sys_topic: {}".format(sys_topic))
    '''

    # Get the content around the knowlede
    #sys_knowledge = msg['features'][0]['googlekg'][0] # list of knowledge
    #logging.info("[SPORTS] feature sys_knowledge is: {}".format(sys_knowledge))

    # Get the sys module selected by the system
    sys_module = msg['features'][0]['topic_module']

    # Get the dialog act and sentence segmentation information
    sys_DA = msg['features'][0]['dialog_act']
    
    #logging.info("[SPORTS_MODULE] system dialog act: {}".format(sys_DA))
    # sys_amazon_DA = msg['features'][0]['amz_dialog_act']
    sys_acknowledgement = msg['system_acknowledgement'][0]
    logging.debug("[SPORTS_MODULE] system_acknowledgement: {}".format(sys_acknowledgement))

    # Get the noun phrase
    sys_noun_phrase = msg['features'][0]['noun_phrase']

    # Get the coreference
    sys_coreference = msg['features'][0]['coreference']

    a_b_test = msg['a_b_test']

    asr_correction = msg['features'][0]['asrcorrection']
    #logging.debug("[SPORTS_MODULE] asr_correction is: {}".format(asr_correction))
    if asr_correction is None:
        asr_correction = {}

    # Define a dictionary to store the system level info
    # msg features is for retrieval class. it needs full features
    
    sys_returnnlp = msg['returnnlp']
    System_Info = {"last_module": previous_module, "sys_intent": sys_intent, "sys_intent_2": sys_intent_2, "sys_ner": sys_ner,
                   "sys_topic": sys_topic, 'sys_module': sys_module, 'msg': msg,
                   "sys_DA": sys_DA, "sys_noun_phrase": sys_noun_phrase, "sys_coreference": sys_coreference,
                   "sys_central_elem": sys_central_elem, "a_b_test": a_b_test, "returnnlp": sys_returnnlp, 'system_acknowledgement': sys_acknowledgement, 'asr_correction': asr_correction}

    # 6. Get THe Sports Context
    sport_context = msg['sportchat']

    # 7. Get the User Attribute
    sport_user = msg['sport_user']

    # 8. Get the Session ID
    session_id = msg['session_id'][0]
    logging.info(
        "[SPORTS_MODULE] The current session id is: {}".format(session_id))
    # Define a dictionary to store the sport module level info
    Sport_Info = {"sport_context": sport_context,
                  "sport_user": sport_user, "session_id": session_id}

    # TODO: Define a SportChatBot Class to Provide Response

    user_attributes = SimpleNamespace(**{
        "session_id": msg["session_id"],
        "a_b_test": msg["a_b_test"][0],
        "user_profile": msg["user_profile"],
        "template_manager": msg["template_manager"],
        "module_selection": msg["module_selection"],
        "previous_modules": msg["previous_modules"],
        "blender_start_time": msg["blender_start_time"]
    })

    # Initialize the Sport_Bot
    Sport_Bot = Sport_Chatbot(Utterance, user_attributes, System_Info, Sport_Info)

    # TODO: Update the Sport State
    # Sport_Bot.self_update()

    # Transite to an appropriate state to handle the Utterance.
    Sport_Bot.transit_state()

    # Generate Response
    response_dict = Sport_Bot.get_response()

    output_dict = {}
    output_dict['user_attributes'] = user_attributes.__dict__
    logging.info(f"output_dict user attributes: {user_attributes.__dict__}")

    output_dict['response'] = response_dict['response']
    output_dict['user_attributes']['sportchat'] = response_dict['user_attributes']['sportchat']

    '''
    response_dict = {
        "response":'I love sport',
        "propose_continue": "CONTINUE"

    }
    '''
    return output_dict
