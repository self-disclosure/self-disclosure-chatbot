import random
import logging
from sport_utils import *
from sport_utterance_data import KNOWN_SPORTS
import template_manager
template_sports = template_manager.Templates.sports
UNKNOWN_SPORTS_TEMPLATES_UPPERBOUND=3

class Sport_Transition(object):

    def __init__(self, previous_state, user_utterance=None, user_intent=None, context=None):
        self.user_utterance = user_utterance
        # Insert User Intent
        self.user_intent = user_intent
        # Inser the Sport Context
        self.context = context

        if previous_state == "s_sport_exit" or previous_state is None:
            self.previous_state = "s_sport_none"
            self.context['previous_state'] = self.previous_state
        else:
            self.previous_state = previous_state
        # Insert User Utterance

        self.__bind__translation()

    def __bind__translation(self):
        self.transition_mapping = {
            "s_sport_none": self.s_sport_none_transition,
            "s_sport_chitchat_q": self.s_sport_chitchat_q_transition,
            "s_sport_chitchat_a": self.s_sport_chitchat_a_transition,
            "s_sport_favorite_sport_confirmation": self.s_sport_favorite_sport_confirmation_transition,
            "s_sport_answer_question": self.s_sport_answer_question_transition,
            "s_sport_provide_news": self.s_sport_provide_news_transition,
            "s_sport_provide_funfacts": self.s_sport_provide_funfacts_transition,
            "s_sport_exit": self.s_sport_exit_transition,
            "s_sport_provide_moments": self.s_sport_provide_moments_transition,
            "s_sport_provide_moments_details": self.s_sport_provide_moments_details_transition,
            "s_sport_ask_interest_in_moment": self.s_sport_ask_interest_in_moment_transition,
            "s_sport_give_moment_opinion": self.s_sport_give_moment_opinion_transition,
            "s_sport_confirm_sport_continue": self.s_sport_confirm_sport_continue_transition,
        }

    def get_current_state(self):
        # Get the current state
        #logging.debug("[SPORTS_MODULE] previous state is: {}".format(self.previous_state))
        current_state = self.transition_mapping[self.previous_state]()
        state_dict = {
            "current_state": current_state
        }
        return state_dict

    def suggest_sporttype_transition(self):
        current_sport = self.context["current_sport"]
        total_current_sport_templates = template_sports.count_utterances(selector="qa_templates/sport_type/qa_{}/general".format(current_sport)) if current_sport in KNOWN_SPORTS else UNKNOWN_SPORTS_TEMPLATES_UPPERBOUND
        if self.context["asked_questions"].get(current_sport, 0) < total_current_sport_templates or self.context["followup_questions"]:
            current_state = "s_sport_chitchat_q"
        else:
            current_state  =  "s_sport_confirm_sport_continue"
        return current_state
##########################################################################
    # Define Different Transition Function

    def s_sport_none_transition(self):
        user_sport_context = self.context
        user_sport_intent = self.user_intent['sport_intents']
        if user_sport_intent['sport_i_notinterestsport'] or self.context['unclear']:
            r"""
            Transit to exit state
            """
            current_state = 's_sport_exit'
        elif user_sport_intent['sport_i_requestnews']:
            r"""
            Transit to provide_moments
            """
            current_state = "s_sport_provide_moments"
        elif user_sport_intent['sport_i_askquestion']:
            r"""
            Transit to sport_answer_question state
            """
            current_state = 's_sport_answer_question'
        elif user_sport_intent['sport_i_suggestentity'] and user_sport_context["chitchat_mode"]=="entity":
            r"""
            Transit to sport ini_know_entity
            """
            current_sport = user_sport_context["current_sport"]

            current_state = "s_sport_chitchat_q"  # none->followup_chitchat_ask

        elif user_sport_intent['sport_i_suggestsporttype']:
            r"""
                Transit back to sport_ini_chitchat
            """
            #two cases if we have templates for the sport vs if we don't have templates for this sport.
            current_state = self.suggest_sporttype_transition()

        elif self.context["expect_next_state"]:
            r"""
            Transit to expect_next_state
            """
            current_state = self.context["expect_next_state"]
        else:
            r"""
            Transit to sport ini_chitchat
            """
            if user_sport_context["current_sport"]:
                current_sport = user_sport_context["current_sport"]
                total_current_sport_templates = template_sports.count_utterances(selector="qa_templates/sport_type/qa_{}/general".format(current_sport)) if current_sport in KNOWN_SPORTS else UNKNOWN_SPORTS_TEMPLATES_UPPERBOUND
                if user_sport_context["asked_questions"].get(current_sport, 0) < total_current_sport_templates or user_sport_context["followup_questions"]:
                    current_state = "s_sport_chitchat_q"
                    if user_sport_context["chitchat_mode"] == "entity":
                        self.context["chitchat_mode"] = "sport_type"
                        self.context["2sport_type"] = True
                else:
                    current_state  =  "s_sport_confirm_sport_continue"
            else:
                current_state = "s_sport_confirm_sport_continue"

        return current_state

    def s_sport_chitchat_q_transition(self):
        user_sport_intent = self.user_intent['sport_intents']
        user_sport_context = self.context

        current_entity = user_sport_context['current_entity']
        talked_entities = user_sport_context['talked_entities']

        # Priotize suggest nameentity
        if user_sport_intent['sport_i_notinterestsport'] or self.context['unclear']:
            r"""
            Transit to exit state
            """
            current_state = 's_sport_exit'
            self.context["suggest_entity_rejection"] = 0

        elif user_sport_intent['sport_i_requestnews']:
            r"""
            Transit to provide_moments
            TODO: Need to Recheck the Provide Moments Issue
            """
            current_state = "s_sport_provide_moments"

        elif user_sport_intent["sport_i_requestsport"]:
            r"""
            Transit to favorite sport state
            TODO: Check this logic merged from ini_chitchat
            """
            current_state = "s_sport_favorite_sport_confirmation"

        elif user_sport_intent['sport_i_askquestion']:
            r"""
            Transit to sport ask_question state, if user's intent is to ask us a question.
            """
            current_state = 's_sport_answer_question'

        elif user_sport_intent['sport_i_suggestentity'] and user_sport_context["chitchat_mode"]=="entity":
            r"""
            Transit to sport ini know entity
            """
            #current_state = 's_sport_ini_know_entity'
            current_state = 's_sport_chitchat_q'
            self.context["suggest_entity_rejection"] = 0

        elif user_sport_intent['sport_i_suggestsporttype']:
            r"""
            Transit to sport ini_chitchat if the user express no interest in current sports.
            """
            #logging.info("[SPORTS_MODULE] transit from ask to ini_chitchat")
            # followup_chitchat_ask->sport_ini_chitchat
            current_state = self.suggest_sporttype_transition()
            self.context["suggest_entity_rejection"] = 0
        
        elif user_sport_intent['sport_i_notinterest']:
            if user_sport_context["chitchat_mode"] == "entity":
                if user_sport_context["current_sport"]:
                    current_sport = user_sport_context["current_sport"]
                    total_current_sport_templates = template_sports.count_utterances(selector="qa_templates/sport_type/qa_{}/general".format(current_sport)) if current_sport in KNOWN_SPORTS else UNKNOWN_SPORTS_TEMPLATES_UPPERBOUND
                    if user_sport_context["asked_questions"].get(current_sport, 0) < total_current_sport_templates or user_sport_context["followup_questions"]:
                        current_state = "s_sport_chitchat_q"
                        self.context["chitchat_mode"] = "sport_type"
                        self.context["2sport_type"] = True
                    else:
                        current_state  =  "s_sport_confirm_sport_continue"
            else:
                current_state = 's_sport_confirm_sport_continue'
        elif user_sport_context['followup_entity'] and check_talked_entities(current_entity["name"], talked_entities):
            r"""
            Transit to sport ini know entity, this change assures that no interest gests higher weightto determine the next state
            """
            #current_state = 's_sport_ini_know_entity'
            current_state = 's_sport_chitchat_q'
            self.context["suggest_entity_rejection"] = 0

        elif user_sport_context['expect_next_state']:
            r"""
            If expect next state is defined then transfer to the expect next state
            """
            if user_sport_intent['sport_i_answerno'] and user_sport_context['expect_next_state'] == "s_sport_provide_moments":
                #logging.info("[SPORTS_MODULE] transit from ask to ini_chitchat because answer_no?")
                #current_state = 's_sport_ini_chitchat'
                current_state = "s_sport_chitchat_q"
            else:
                #logging.info("[SPORTS_MODULE] transit from ask to ini_chitchat because wrong expect_next_state?")
                current_state = user_sport_context['expect_next_state']
            self.context["suggest_entity_rejection"] = 0
        else:
            r"""
            Transit to follow up chitrchat answer, if no above request is raised.
            """
            #current_state = 's_sport_followup_chitchat_answer'  # followup_chitchat_ask->sport_ini_chitchat
            current_state = 's_sport_chitchat_a'
            self.context["suggest_entity_rejection"] = 0

        return current_state

    def s_sport_chitchat_a_transition(self):
        user_sport_context = self.context
        user_sport_intent = self.user_intent['sport_intents']
        # Priotize suggest nameentity
        if user_sport_intent['sport_i_notinterestsport'] or self.context['unclear']:
            r"""
            Transit to exit state
            """
            current_state = 's_sport_exit'

        elif user_sport_intent['sport_i_requestnews']:
            r"""
            Transit to provide_moments
            """
            current_state = "s_sport_provide_moments"

        elif user_sport_intent['sport_i_askquestion']:
            r"""
            Transit to answer_question state, if the user intent is to aks a question.
            """
            current_state = 's_sport_answer_question'

        elif user_sport_intent['sport_i_suggestentity'] and user_sport_context["chitchat_mode"]=="entity":
            r"""
            Transit to sport ini know entity
            """
            current_state = 's_sport_chitchat_q'


        elif user_sport_intent['sport_i_suggestsporttype']:
            r"""
            Transit to sport_ini_chitchat state or the user suggest a new sport type but not a specific name entity.
            """
            current_state = self.suggest_sporttype_transition()

        elif user_sport_intent['sport_i_notinterest']:
            r"""
            Transit to sport_confirm_sport_continue when people indicate no interest in current sport
            """
            current_state = 's_sport_confirm_sport_continue'

        elif self.context['expect_next_state']:
            # Went to the expect next state
            current_state = self.context['expect_next_state']
        else:
            current_state = 's_sport_chitchat_q'

        return current_state

    def s_sport_favorite_sport_confirmation_transition(self):
        user_sport_intent = self.user_intent['sport_intents']
        user_sport_context = self.context
        # For Now, I can only think of this one state that it should head to ~
        # TODO: Not direct to Exit but provide some other relevant Sport
        # Information
        # (self.context['previous_state'] == "s_sport_favorite_sport_confirmation" and self.context['num_error_status'] > 0)
        if user_sport_intent['sport_i_notinterestsport'] or self.context['unclear']:
            r"""
            Transit to exit state
            """
            current_state = 's_sport_exit'
            # Update the num_error_status
            self.context['num_error_status'] = 0

        elif user_sport_intent['sport_i_requestnews']:
            r"""
            Transit to provide_moments
            """
            current_state = "s_sport_provide_moments"

        elif user_sport_intent['sport_i_askquestion']:
            r"""
            Transit to the answer_question state, if the user directly raise the question about something else
            """
            current_state = 's_sport_answer_question'
            # Update the num_error_status
            self.context['num_error_status'] = 0

        elif user_sport_intent['sport_i_suggestentity'] and user_sport_context["chitchat_mode"]=="entity":
            r"""
            Transit to sport ini know entity
            """
            current_state = 's_sport_chitchat_q'
            self.context['num_error_status'] = 0

        elif user_sport_intent['sport_i_suggestsporttype']:
            r"""
            Transit to the s_sport_ini_chitchat state if the
            """
            current_state = self.suggest_sporttype_transition()
            # Update the num_error_status
            self.context['num_error_status'] = 0
        elif user_sport_intent['sport_i_notinterest']:
            r"""
            Just quit sport
            """
            current_state = 's_sport_exit'
            self.context["num_error_status"] = 0
        elif not self.context['detected_sports'] and self.context['num_error_status'] == 0:
            r"""
            Stay with the current state to handle the case when thre is no sport type detected.
            """
            current_state = 's_sport_favorite_sport_confirmation'
        else:
            r"""
            Transit to sport_ini_chitchat if there is a sport type detected and the user did not suggest to talk about a different entity.
            """
            current_state = 's_sport_exit'
            #self.context['num_error_status'] = 0
        return current_state

    def s_sport_answer_question_transition(self):
        user_sport_intent = self.user_intent['sport_intents']
        user_sport_context = self.context
        if user_sport_intent['sport_i_notinterestsport'] or self.context['unclear']:
            r"""
            Transit to exit state
            """
            current_state = 's_sport_exit'

        elif user_sport_intent['sport_i_askquestion']:
            r"""
            Transit to answer_question state, if the user intent is to ask a question.
            """
            current_state = 's_sport_answer_question'

        elif user_sport_intent['sport_i_requestnews']:
            r"""
            Transit to provide_moments
            """
            current_state = "s_sport_provide_moments"

        elif user_sport_intent['sport_i_suggestentity'] and user_sport_context["chitchat_mode"]=="entity":
            r"""
            Transit to sport ini know entity
            """
            current_state = 's_sport_chitchat_q'
        elif user_sport_intent['sport_i_suggestsporttype']:
            current_state = self.suggest_sporttype_transition()
        elif self.context['expect_next_state']:
            r"""
            If expect next state is defined then transfer to the expect next state
            """
            #logging.info("[SPORTS_MODULE] transit from ask to ini_chitchat because wrong expect_next_state?")
            current_state = self.context['expect_next_state']
        else:
            r"""
            Transit to answer followup chitchat ask
            """
            current_state = 's_sport_confirm_sport_continue'

        return current_state

    def s_sport_provide_funfacts_transition(self):
        user_sport_intent = self.user_intent['sport_intents']
        user_sport_context = self.context
        if user_sport_intent['sport_i_notinterestsport'] or self.context['unclear']:
            r"""
            Transit to exit state
            """
            current_state = 's_sport_exit'

        elif user_sport_intent['sport_i_requestnews']:
            r"""
            Transit to provide_moments
            """
            current_state = "s_sport_provide_moments"

        elif user_sport_intent['sport_i_askquestion']:
            r"""
            Transit to answer_question state, if the user intent is to ask a question.
            """
            current_state = 's_sport_answer_question'
        elif user_sport_intent['sport_i_suggestentity'] and user_sport_context["chitchat_mode"]=="entity":
            r"""
            Transit to sport ini know entity
            """
            current_state = 's_sport_chitchat_q'

        elif user_sport_intent['sport_i_suggestsporttype']:
            r"""
            Transit to sport_ini_chitchat state or the user suggest a new sport type but not a specific name entity.
            """
            current_state = self.suggest_sporttype_transition()
        elif user_sport_intent['sport_i_notinterest']:
            r"""
            return to s_sport_chitchat_q or 
            """
            logging.debug("[SPORTS_MODULE] current_sport is: {}".format(user_sport_context["current_sport"]))
            if user_sport_context["current_sport"]:
                current_sport = user_sport_context["current_sport"]
                total_current_sport_templates = template_sports.count_utterances(selector="qa_templates/sport_type/qa_{}/general".format(current_sport)) if current_sport in KNOWN_SPORTS else UNKNOWN_SPORTS_TEMPLATES_UPPERBOUND
                if user_sport_context["asked_questions"].get(current_sport, 0) < total_current_sport_templates or user_sport_context["followup_questions"]:
                    current_state = "s_sport_chitchat_q"
                    if user_sport_context["chitchat_mode"] == "entity":
                        self.context["chitchat_mode"] = "sport_type"
                        self.context["2sport_type"] = True
                else:
                    current_state  =  "s_sport_confirm_sport_continue"
            else:
                current_state = "s_sport_confirm_sport_continue"
        else:
            r"""
            stay with provide news, if above cases are not hitted
            """
            current_state = self.context[
                'expect_next_state']  # TODO: May be I need to Modify the design of this state

        return current_state

    def s_sport_provide_news_transition(self):
        user_sport_intent = self.user_intent['sport_intents']
        user_sprot_context = self.context
        if user_sport_intent['sport_i_notinterestsport'] or self.context['unclear']:
            r"""
            Transit to exit state
            """
            current_state = 's_sport_exit'

        elif user_sport_intent['sport_i_requestnews']:
            r"""
            Transit to provide_moments
            """
            current_state = "s_sport_provide_moments"

        elif user_sport_intent['sport_i_askquestion']:
            r"""
            Transit to answer_question state, if the user intent is to ask a question.
            """
            current_state = 's_sport_answer_question'

        elif user_sport_intent['sport_i_suggestentity'] and user_sport_context["chitchat_mode"]=="entity":
            r"""
            Transit to sport ini know entity
            """
            current_state = 's_sport_chitchat_q'

        elif user_sport_intent['sport_i_suggestsporttype']:
            r"""
            Transit to sport_ini_chitchat state or the user suggest a new sport type but not a specific name entity.
            """
            current_state = self.suggest_sporttype_transition()
        elif user_sport_intent['sport_i_notinterest']:
            r"""
            return to sport_chitchat_q or confirm_continue
            """
            if user_sport_context["current_sport"]:
                current_sport = user_sport_context["current_sport"]
                total_current_sport_templates = template_sports.count_utterances(selector="qa_templates/sport_type/qa_{}/general".format(current_sport)) if current_sport in KNOWN_SPORTS else UNKNOWN_SPORTS_TEMPLATES_UPPERBOUND
                if user_sport_context["asked_questions"].get(current_sport, 0) < total_current_sport_templates or user_sport_context["followup_questions"]:
                    current_state = "s_sport_chitchat_q"
                    if user_sport_context["chitchat_mode"] == "entity":
                        self.context["chitchat_mode"] = "sport_type"
                        self.context["2sport_type"] = True
                else:
                    current_state  =  "s_sport_confirm_sport_continue"
            else:
                current_state = "s_sport_confirm_sport_continue"

        else:
            r"""
            stay with provide news, if above cases are not hitted
            """
            current_state = self.context[
                'expect_next_state']  # TODO: May be I need to Modify the design of this state

        return current_state

    def s_sport_provide_moments_transition(self):
        r'''
        Besides the Higher Priority Transition, it will ask if users have heard about the news in the end. 
        (1) If user says Yes, and we have some opinions. Go to 's_sport_give_moment_opinion' and extract an opinion. 
        (2) If user says Yes, and we don't have opinions. Go to 's_sport_ask_interest_moment' and confirm if users want to hear another moment or not. 
        (3) If user says No. Go to 's_sport_provide_moments_details' and give users some more details of the news
        '''
        user_sport_intent = self.user_intent['sport_intents']
        user_sport_context = self.context
        if user_sport_intent['sport_i_notinterestsport'] or self.context['unclear']:
            r"""
            Transit to exit state
            """
            current_state = 's_sport_exit'

        elif user_sport_intent['sport_i_requestnews']:
            r"""
            Transit to provide_moments
            """
            current_state = "s_sport_provide_moments"

        elif user_sport_intent['sport_i_askquestion']:
            r"""
            Transit to answer_question state, if the user intent is to ask a question.
            """
            current_state = 's_sport_answer_question'

        elif user_sport_intent['sport_i_suggestentity'] and user_sport_context["chitchat_mode"]=="entity":
            r"""
            Transit to sport ini know entity
            """
            current_state = 's_sport_chitchat_q'

        elif user_sport_intent['sport_i_suggestsporttype']:
            r"""
            Transit to sport_ini_chitchat state or the user suggest a new sport type but not a specific name entity.
            """
            current_state = self.suggest_sporttype_transition()

        elif user_sport_intent['sport_i_notinterest']:
            r"""
            Transit to confirm interest for another moment state
            """
            current_state = 's_sport_ask_interest_in_moment'
        else:
            r"""
            Transfer to ini_chitchat if user don't want one more moment otherwise provide another moment
            """
            if user_sport_intent['sport_i_answerno']:
                current_state = 's_sport_provide_moments_details'
            else:
                current_state = self.context['expect_next_state']

        return current_state

    def s_sport_give_moment_opinion_transition(self):
        r'''
        Besides the Higher Priority Transition, it will ask if users want to hear about more news in the end or ask if users agree with the comment.
        Transfer to confirm user interest in another moment afterwards. 
        '''
        user_sport_intent = self.user_intent['sport_intents']
        user_sport_context = self.context
        if user_sport_intent['sport_i_notinterestsport'] or self.context['unclear']:
            r"""
            Transit to exit state
            """
            current_state = 's_sport_exit'

        elif user_sport_intent['sport_i_requestnews']:
            r"""
            Transit to provide_moments
            """
            current_state = "s_sport_provide_moments"

        elif user_sport_intent['sport_i_askquestion']:
            r"""
            Transit to answer_question state, if the user intent is to ask a question.
            """
            current_state = 's_sport_answer_question'
        elif user_sport_intent['sport_i_suggestentity'] and user_sport_context["chitchat_mode"]=="entity":
            r"""
            Transit to sport ini know entity
            """
            current_state = 's_sport_chitchat_q'

        elif user_sport_intent['sport_i_suggestsporttype']:
            r"""
            Transit to sport_ini_chitchat state or the user suggest a new sport type but not a specific name entity.
            """
            current_state = self.suggest_sporttype_transition()
        elif user_sport_intent['sport_i_notinterest']:
            current_state = 's_sport_ask_interest_in_moment'
        else:
            r"""
            Transfer to ini_chitchat if user don't want one more moment otherwise provide another moment
            """
            if self.context['expect_next_state'] == 's_sport_provide_moments':
                if user_sport_intent['sport_i_answerno']:
                    current_state = 's_sport_chitchat_q'
                else:
                    current_state = 's_sport_provide_moments'
            else:
                current_state = self.context['expect_next_state']

        return current_state

    def s_sport_provide_moments_details_transition(self):
        r'''
        Besides the Higher Priority Transition, it will ask if users' thoughts regarding the news or ask users if he wants to hear another news. 
        (1) If we ask users for their opinion, and they comment that they don't have an opinion, transfer to "s_sport_ask_interest_in_moment"
        (2) If we ask users if they want to hear another news and they said no, transfer to "s_sport_chitchat_q"
        (3) If we ask users for their opinion, and they provide something. Then, transfer to "s_sport_give_moment_opinion"
        (4) If we ask users if they want to hear another news and they said yes, transfer to "s_sport_provide_moments"
        '''
        user_sport_intent = self.user_intent['sport_intents']
        user_sport_context = self.context
        if user_sport_intent['sport_i_notinterestsport'] or self.context['unclear']:
            r"""
            Transit to exit state
            """
            current_state = 's_sport_exit'

        elif user_sport_intent['sport_i_requestnews']:
            r"""
            Transit to provide_moments
            """
            current_state = "s_sport_provide_moments"

        elif user_sport_intent['sport_i_askquestion']:
            r"""
            Transit to answer_question state, if the user intent is to ask a question.
            """
            current_state = 's_sport_answer_question'

        elif user_sport_intent['sport_i_suggestentity'] and user_sport_context["chitchat_mode"]=="entity":
            r"""
            Transit to sport ini know entity
            """
            current_state = 's_sport_chitchat_q'

        elif user_sport_intent['sport_i_suggestsporttype']:
            r"""
            Transit to sport_ini_chitchat state or the user suggest a new sport type but not a specific name entity.
            """
            current_state = self.suggest_sporttype_transition()

        elif user_sport_intent['sport_i_notinterest']:
            r"""
            Transit to confirm interest for another moment state
            """
            current_state = 's_sport_ask_interest_in_moment'
        else:
            if self.context['expect_next_state'] == 's_sport_provide_moments' and user_sport_intent['sport_i_answerno']:
                current_state = 's_sport_chitchat_q'
            else:
                current_state = self.context['expect_next_state']

        return current_state

    def s_sport_ask_interest_in_moment_transition(self):
        r'''
        Besides the Higher Priority Transition, it will ask if users is interested to learn about another moment news in the end. 
        (1) If User says No, then we go to s_sport_ini_chitchat
        (2) If User says Yes, then we go to s_sport_provide_moments
        '''
        user_sport_intent = self.user_intent['sport_intents']
        user_sport_context = self.context
        if user_sport_intent['sport_i_notinterestsport'] or self.context['unclear']:
            r"""
            Transit to exit state
            """
            current_state = 's_sport_exit'

        elif user_sport_intent['sport_i_requestnews']:
            r"""
            Transit to provide_moments
            """
            current_state = "s_sport_provide_moments"

        elif user_sport_intent['sport_i_askquestion']:
            r"""
            Transit to answer_question state, if the user intent is to ask a question.
            """
            current_state = 's_sport_answer_question'

        elif user_sport_intent['sport_i_suggestentity'] and user_sport_context["chitchat_mode"]=="entity":
            r"""
            Transit to sport ini know entity
            """
            current_state = 's_sport_chitchat_q'

        elif user_sport_intent['sport_i_notinterest'] or user_sport_intent['sport_i_suggestsporttype']:
            r"""
            Transit to sport_ini_chitchat state or the user suggest a new sport type but not a specific name entity.
            """
            current_state = self.suggest_sporttype_transition()
        else:
            if self.context['expect_next_state'] == 's_sport_provide_moments' and user_sport_intent['sport_i_answerno']:
                current_state = 's_sport_chitchat_q'
            else:
                current_state = self.context['expect_next_state']

        return current_state

    def s_sport_exit_transition(self):
        pass

    def s_sport_confirm_sport_continue_transition(self):
        """
           Beside the high priority transition:
           (1) If sport type is detected and the intent is not not_interested, transit to ini_chitchat
        """
        user_sport_intent = self.user_intent['sport_intents']
        user_sport_context = self.context
        if user_sport_intent['sport_i_notinterestsport'] or self.context['unclear']:
            r"""
            Transit to exit state
            """
            current_state = 's_sport_exit'

        elif user_sport_intent['sport_i_requestnews']:
            r"""
            Transit to provide_moments
            """
            current_state = "s_sport_provide_moments"

        elif user_sport_intent['sport_i_askquestion']:
            r"""
            Transit to answer_question state, if the user intent is to ask a question.
            """
            current_state = 's_sport_answer_question'

        elif user_sport_intent['sport_i_suggestentity'] and user_sport_context["chitchat_mode"]=="entity":
            r"""
            Transit to sport ini know entity
            """
            current_state = 's_sport_chitchat_q'
        elif user_sport_intent['sport_i_suggestsporttype']:
            r"""
            Transit to sport_ini_chitchat state or the user suggest a new sport type but not a specific name entity.
            """
            current_state = self.suggest_sporttype_transition()

        elif user_sport_intent['sport_i_answerno']:
            r"""
            Transit to exit 
            """
            current_state = 's_sport_exit'
        else:
            r"""
            Transit to favorite sport confirmation
            """
            current_state = 's_sport_favorite_sport_confirmation'

        return current_state


###########################World Cup Subsystem############################


class WorldCup_Transition(Sport_Transition):

    def __init__(self, previous_state, user_utterance=None, user_intent=None, context=None):
        # Initilaize the Upper Class
        super(WorldCup_Transition, self).__init__(
            previous_state, user_utterance, user_intent, context)

        # Include the transition mapping defined for WorldCup_Transition
        self.__bind_transition_worldcup()

    def __bind_transition_worldcup(self):
        # self.transition_mapping[""]
        self.transition_mapping[
            "s_sport_worldcup_ini_chitchat"] = self.s_sport_worldcup_ini_chitchat_transition
        self.transition_mapping[
            "s_sport_worldcup_propose_topic"] = self.s_sport_worldcup_propose_topic_transition
        self.transition_mapping[
            "s_sport_worldcup_chitchat"] = self.s_sport_worldcup_chitchat_transition

##########################################################################
    # Overwrite sport_none_transition
    def s_sport_none_transition(self):
        user_sport_intent = self.user_intent['sport_intents']
        if user_sport_intent['sport_i_notinterestsport'] or self.context['unclear']:
            r"""
            Transit to exit state
            """
            current_state = 's_sport_exit'
        elif user_sport_intent['sport_i_askquestion']:
            r"""
            Transit to sport_answer_question state
            """
            current_state = 's_sport_answer_question'
        else:
            r"""
            Transit to sport-ini_chitchat, if no specific name entity is suggested or just general start of sports.
            """
            current_state = "s_sport_worldcup_ini_chitchat"  # none->followup_chitchat_ask

        return current_state

    def s_sport_worldcup_ini_chitchat_transition(self):
        user_sport_intent = self.user_intent['sport_intents']
        if user_sport_intent['sport_i_notinterestsport'] or self.context['unclear']:
            r"""
            Transit to exit state
            """
            current_state = 's_sport_exit'
        elif user_sport_intent['sport_i_askquestion']:
            r"""
            Transit to sport_answer_question state
            """
            current_state = 's_sport_answer_question'
        else:
            r"""
            Transit to tea_summary state
            """
            #current_state = 's_sport_worldcup_team_summary'
            current_state = 's_sport_worldcup_propose_topic'
        return current_state

    def s_sport_worldcup_propose_topic_transition(self):
        user_sport_intent = self.user_intent['sport_intents']
        user_sport_context = self.context
        if user_sport_intent['sport_i_notinterestsport'] or self.context['unclear']:
            r"""
            Transit to exit state
            """
            current_state = 's_sport_exit'
        elif user_sport_intent['sport_i_askquestion']:
            r"""
            Transit to sport_answer_question state
            """
            current_state = 's_sport_answer_question'
        elif user_sport_intent['sport_i_answerno'] or user_sport_intent['sport_i_notinterest']:
            r"""
            Stay with sport_worldcup_propose_topic state
            """
            current_state = 's_sport_worldcup_propose_topic'
        else:
            r"""
            Move to the corresponding topic
            """
            # TODO: Propose the next state based on what havn't being talked
            # about
            current_state = user_sport_context['expect_next_state']

        return current_state

    # Redefine World Cup Ask Questions.
    def s_sport_answer_question_transition(self):
        user_sport_intent = self.user_intent['sport_intents']
        if user_sport_intent['sport_i_notinterestsport'] or self.context['unclear']:
            r"""
            Transit to exit state
            """
            current_state = 's_sport_exit'
        elif user_sport_intent['sport_i_askquestion']:
            r"""
            Transit to answer_question state, if the user intent is to aks a question.
            """
            current_state = 's_sport_answer_question'
        else:
            r"""
            Transit to propose some WorldCup
            """
            current_state = 's_sport_worldcup_propose_topic'

        return current_state

    # Define a new state to conduct Chitchat
    def s_sport_worldcup_chitchat_transition(self):
        user_sport_intent = self.user_intent['sport_intents']
        if user_sport_intent['sport_i_notinterestsport'] or self.context['unclear']:
            r"""
            Transit to exit state
            """
            current_state = 's_sport_exit'
        elif user_sport_intent['sport_i_askquestion']:
            r"""
            Transit to answer_question state, if the user intent is to aks a question.
            """
            current_state = 's_sport_answer_question'
        else:
            r"""
            Transit to propose some WorldCup topics
            """
            current_state = 's_sport_worldcup_propose_topic'
        return current_state
