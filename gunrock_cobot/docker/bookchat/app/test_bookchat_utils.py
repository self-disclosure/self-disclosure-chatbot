import pytest
from bookchat_utils import no_book_name_included


@pytest.mark.parametrize("text", ["i like to read and dance",
                                  "i like to dance and read",
                                  "i like to read book",
                                  "read books",
                                  "read book",
                                  "yeah",
                                  "yes",
                                  "nothing"])
def test_no_book_name_included_true(text):
    assert no_book_name_included(text) is True


@pytest.mark.parametrize("text", ["yeah i read harry potter",
                                  "yes i just read harry potter",
                                  "harry potter"])
def test_no_book_name_included_false(text):
    assert no_book_name_included(text) is False
