import xmltodict
import urllib
import urllib3
import json
import hashlib
import redis
import random
from util_redis import RedisHelper
import logging
from fuzzywuzzy import fuzz
from collections import OrderedDict
urllib3.disable_warnings()

from new_bookchat_utils.logging_utils import set_logger_level, get_working_environment

LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())
# TODO: make api calls more safe. e.g. use syntax: example_dict.get('key1', {}).get('key2')
class GoodreadsClient():
    BASE_URL = "https://www.goodreads.com/"
    GOODREADS_CLIENT_KEYS = ["NSBwbOa42jsk2l9m9xWRw", "Vf18CgXKdwVz8pzh9nMDg", "OwXNTUG9U00OJiiZMofp5w"]
    GOODREADS_CLIENT_SECRETS = ["XVTgCDNQUdGyOl7eWeSMr9qs7kbsdGL45SmpSRLi5A",
                                "Q4esRZJEy2KGNNsZM9s5UR0G6ne1tLZnwhtKavPJlYE",
                                "QrDsQBaJM1gy0KFwDxCzPiIj3Snen0C5RwFzSjoY"]

    def __init__(self):
        # index = random.randrange(0, len(self.GOODREADS_CLIENT_KEYS))
        # self.client_key = self.GOODREADS_CLIENT_KEYS[index]
        # self.client_secret = self.GOODREADS_CLIENT_SECRETS[index]
        self.client_key = "cQyWfJ56Iz6XM0F7HIGEgw"

    '''Returns the top 20 books for bookname'''
    def search_books_with_bookname(self, bookname):
        data = self.search_books_with_bookname_cached(bookname)

        if data and data["search"]["results"]:
            results = data["search"]["results"]["work"]
            num_results = len(results)
            total_results = data["search"]["total-results"]
            return results, num_results, total_results
        return None, -1, -1

    def search_books_with_bookname_cached(self, bookname):
        GOODREADS_BOOK_SEARCH_PREFIX = "gunrock:module:bookchat:goodreads_book_search"
        key = bookname
        cache = RedisHelper().get(GOODREADS_BOOK_SEARCH_PREFIX, key)

        if cache is not None:
            LOGGING.debug("[goodreads_book_search] cache hit")
            return cache
        LOGGING.debug("[goodreads_book_search] cache miss")

        value = self._request_book_search_with_book_name(bookname)

        RedisHelper().set_with_expire(GOODREADS_BOOK_SEARCH_PREFIX, key, value, 14 * 24 * 60 * 60)

        return value

    def _request_book_search_with_book_name(self, book_name):
        path = "search/index.xml"
        payload = {"key": self.client_key, "q": book_name, "search[field]": "title"}
        return self.request_get(path, payload, "xml")

    '''Returns the top 20 books for author'''
    def search_books_with_author(self, author):
        data = self._request_book_search_with_author(author)
        books = [self._parse_author_book(book, author) for book in data["author"]['books']['book']]
        # books = [book for book in books if book.get("author") == author]
        books_sorted = sorted(books, key=lambda x: x['rating_count'], reverse=True)
        return books_sorted

    def _convert_ordered_dict_to_val(self, ordered_dict):
        if isinstance(ordered_dict, OrderedDict):
            t = ordered_dict.get("@type")
            val = ordered_dict.get("#text")
            if t is None or val is None:
                return None
            if t == "integer":
                return int(val)
        return ordered_dict

    def _parse_author_book(self, entry, author):
        ret = {'type': 'book'}
        def _get_field(odict, key):
            if isinstance(odict, dict):
                ret = odict.get(key, None)
                return '' if ret is None else self._convert_ordered_dict_to_val(ret)
            else:
                return ''
        ret["id"] = _get_field(entry, 'id')
        ret['title'] = _get_field(entry, 'title_without_series').strip()
        ret["full_title"] = _get_field(entry, "title").strip()
        ret['year'] = _get_field(entry, 'publication_year').strip()

        ret['readers'] = _get_field(entry, 'text_reviews_count')

        ret['rating_count'] = _get_field(entry, 'ratings_count')

        ret['average_rating'] = _get_field(entry, 'average_rating')

        ret['author'] = _get_field(entry, 'author')
        ret["description"] = _get_field(entry, "description").strip()
        return ret

    def _parse(self, entry):
        ret = {'type': 'book', 'goodread_id': '', 'author_id': '', 'author': ''}
        def _get_field(odict, key):
            ret = odict.get(key, None)
            return '' if ret is None else ret

        ret['title'] = _get_field(entry, 'original_title').strip()
        ret['year'] = _get_field(entry, 'original_publication_year').strip()
        ret["description"] = _get_field(entry, "description").strip()
        readers = _get_field(entry, 'reviews_count').strip()
        ret['readers'] = int(readers) if readers else 1

        rcount = _get_field(entry, 'ratings_count').strip()
        ret['rating_count'] = int(rcount) if rcount else 1

        
        

        rsum = _get_field(entry, 'ratings_sum').strip()
        logging.debug("Rating Count {0}, RSum {1}".format(ret['rating_count'], float(rsum)))
        ret['rating'] = (float(rsum) if rsum else 0.) / max(float(ret.get('rating_count', 0.1)), 1)

        best_book = entry.get('best_book', None)
        if best_book is None:
            return ret

        ret['goodread_id'] = _get_field(best_book, 'id').strip()
        if not ret['title']:
            ret['title'] = _get_field(best_book, 'title').strip()

        author = best_book.get('author', None)
        if author is None:
            return ret

        ret['author_id'] = _get_field(author, 'id').strip()
        ret['author'] = _get_field(author, 'name').strip()

        return ret

    def search_books_in_series(self, series_id):
        path = f'series/show/{series_id}.xml'
        payload = {"key": self.client_key}
        resp = self.request_get(path, payload, "xml")

        books = resp['series']['series_works']['series_work']
        books = [self._parse(book['work']) for book in books]

        books_sorted = sorted(books, key=lambda x: x['rating_count'], reverse=True)
        return books_sorted

    def _request_book_search_with_author(self, author):
        path = "author/list.xml"
        default_idx = 1
        payload = {"key": self.client_key, "id": author, "page":default_idx}
        return self.request_get(path, payload, "xml")

    def request_book_with_id(self, bookid):
        path = "book/show/" + bookid + ".xml"
        payload = {"key": self.client_key}
        data = self.request_get(path, payload, "xml")

        if data and data["book"]:
            return data["book"]
        return None

    def request_author_with_id(self, authorid):
        path = "author/show/" + authorid
        payload = {"format": "xml", "key": self.client_key}
        data = self.request_get(path, payload, "xml")
        if data  and data["author"]:
            return data["author"]
        return None

    def request_get(self, path, params, format):
        import requests
        encoded_query = urllib.parse.urlencode(params)
        final_url = self.BASE_URL + path + "?" + encoded_query
        results = requests.get(url=final_url, timeout=(0.3, 0.6))
        # http = urllib3.PoolManager()
        # r = http.request('GET', self.BASE_URL + path, fields=params)

        # if r.status != 200:
        #     return None
        if format == "xml":
            data = xmltodict.parse(results.content)
        # elif format == "json":
        #     return None
        return data['GoodreadsResponse']


class Book():
    def __init__(self, bookinfo):
        self.title = bookinfo["title"]
        self.bookId = bookinfo["id"]
        if type(bookinfo["authors"]["author"]) == list:
             self.author = bookinfo["authors"]["author"][0]["name"]
             self.authorId = bookinfo["authors"]["author"][0]["id"]
        else:
            self.author = bookinfo["authors"]["author"]["name"]
            self.authorId = bookinfo["authors"]["author"]["id"]
        self.description = bookinfo["description"]
        if bookinfo["series_works"] != None:
            self.series = bookinfo.get("series_works", {})["series_work"]["series"]["title"]
        self.rating = bookinfo["average_rating"]
        self.similarBooks = self.getSimilarBooks(bookinfo)

    def getDescription(self):
        '''Parse description'''
        pass

    def getSimilarBooks(self, bookinfo):
        books = []
        if bookinfo.get("similar_books", None) != None:
            for book in bookinfo["similar_books"]["book"]:
                books.append({
                    "id": book["id"],
                    "title": book["title"],
                    "title_without_series": book["title_without_series"],
                    "average_rating": book["average_rating"],
                    "author": book["authors"]["author"]["name"]
                })
        return books


class Author():
    def __init__(self, authorinfo):
        self.name = authorinfo["name"]
        self.about = authorinfo["about"]
        self.books = self.getAuthorBooks(authorinfo)

    def getAbout(self):
        '''Parse about'''
        pass

    def getAuthorBooks(self, authorinfo):
        books = []
        for book in authorinfo["books"]["book"]:
            books.append({
                "id": book["id"][r"#text"],
                "title": book["title"],
                "title_without_series": book["title_without_series"],
                "average_rating": book["average_rating"],
                "description": book["description"]
            })
        return books
