"""Temporary Location to put all the deprecated functions
The python code never used in bookchat
"""
regex_potential_book_name_pattern =[
    r"have you.* read the book ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)$|(\w+ \w+ \w+ \w+)$|(\w+ \w+ \w+ \w+ \w+))",
    r"have you.* read ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)$|(\w+ \w+ \w+ \w+)$|(\w+ \w+ \w+ \w+ \w+))",
    r"my.* favorite.* book is ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)$|(\w+ \w+ \w+ \w+)$|(\w+ \w+ \w+ \w+ \w+))",
    r"you.* know the book ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)$|(\w+ \w+ \w+ \w+)$|(\w+ \w+ \w+ \w+ \w+))",
    r"about the book ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)$|(\w+ \w+ \w+ \w+)$|(\w+ \w+ \w+ \w+ \w+))",
    r"talk(ing|) about ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)$|(\w+ \w+ \w+ \w+)$|(\w+ \w+ \w+ \w+ \w+))",
    r"tell me about ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)$|(\w+ \w+ \w+ \w+)$|(\w+ \w+ \w+ \w+ \w+))",
    r"like the book ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)$|(\w+ \w+ \w+ \w+)$|(\w+ \w+ \w+ \w+ \w+))",
    r"(1|one|book).* i read was ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)$|(\w+ \w+ \w+ \w+)$|(\w+ \w+ \w+ \w+ \w+))",
    r"i am.* reading ((\w+)$|(\w+ \w+)$|(\w+ \w+ \w+)$|(\w+ \w+ \w+ \w+)$|(\w+ \w+ \w+ \w+ \w+))",
    r"(^(\w+)|^(\w+ \w+)|^(\w+ \w+ \w+)|^(\w+ \w+ \w+ \w+)|^(\w+ \w+ \w+ \w+ \w+)) by",
]

ask_intents = {
    "ask_yesno", "ask_preference", "ask_opinion", "ask_advice", "ask_ability", "ask_hobby",
    "ask_recommend", "ask_reason", "ask_self", "ask_fact", "ask_freq", "ask_dist", "ask_loc",
    "ask_count", "ask_degree", "ask_time", "ask_person", "ask_name", "topic_qa", "topic_backstory"
}

ans_intents = {
    "ans_unknown", "ans_neg", "ans_pos", "ans_like", "ans_wish", "ans_factopinion",
}

def book_name_replace(book_name_query):
    corrected_book_name = None
    if re.search(r"\bthe book it\b|\bit by stephen king\b", book_name_query):
        corrected_book_name = "0450411435"

    if corrected_book_name is not None:
        logging.debug("[book_utils][book_name_replace] regex corrected book name: {}".format(corrected_book_name))
        return corrected_book_name

    return book_name_query

def normalize_book_name(book_name):
    # for roman numerals; don't replace i, v, or x because they are ambiguous, e.g. V for Vendetta, X Men
    replace_words = {
        "ii": "two", "iii": "three", "iv": "four", "vi": "six",
        "vii": "seven", "viii": "eight", "ix": "nine", "xi": "eleven",
        "xii": "twelve", "xiii": "thirteen",
    }
    res = []
    # remove non alphanumeric chars except for '; convert to lower case
    book_name = ''.join(ch if (ch.isalnum() or ch == "'")
                     else " " for ch in book_name.lower())
    for word in book_name.split():
        try:
            # convert numbers
            word = int(word)
            word = num2words(word)
        except:
            # convert roman numerals
            if word in replace_words:
                word = replace_words[word]
        res.append(word)
    return " ".join(res)