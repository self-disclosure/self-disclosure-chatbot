import json
import re
from book_automaton import *
from bookchat_utils import *
from types import SimpleNamespace
import logging
from new_bookchat_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

required_context = ['text']

def get_required_context():
    return required_context

def handle_message(msg, app_logger):
    #try:
    #print("msg", json.dumps(msg, indent=4))
    LOGGER.debug("Raw Input to Book {}".format(msg))


    msg["text"][0] = msg["text"][0].lower().strip()
    if msg.get("bookchat_user") is None: # check if None
        msg["bookchat_user"] = {}
    current_state = msg["bookchat_user"].get("current_transition")
    if current_state == None:
        msg["bookchat_user"]["current_transition"] = "t_init"

    user_attributes = SimpleNamespace(**{
        "session_id": msg["session_id"],
        "a_b_test": msg["a_b_test"][0],
        "user_profile": msg["user_profile"],
        "template_manager": msg["template_manager"],
        "module_selection": msg["module_selection"],
        "previous_modules": msg["previous_modules"],
        "blender_start_time": msg["blender_start_time"]
    })

    RG = BookAutomaton(app_logger)
    results = RG.transduce(msg, user_attributes)
    # some info that we might want to save
    ''' for reference
    response_dict = {
        'response': "hi this is bookchat",
        'user_attributes': {
            'bookchat_user': {
                'last_book_mentioned': {},
                'last_author_mentioned': {},
                'liked_books': {},
                'liked_genre': {},
                'liked_authors': {},
                'indices': {}, 
                'current_state': None
            }
        },
        'context_manager': {
            'bookchat_context': {
                'last_state': None,
                'current_state': 't_init',            
            }
        }
    }
    '''

    # parse results and return response plus information to store in user attributes and current cobot state
    response_dict = {}
    response_dict["response"] = results["response"]
    response_dict["user_attributes"] = user_attributes.__dict__

    response_dict["user_attributes"]["bookchat_user"] = msg.get("bookchat_user", {})
    # if there are new fields or info for existing fields have changed, update bookchat_user
    for k,v in results["bookchat_user"].items():
        response_dict["user_attributes"]["bookchat_user"][k] = v

    response_dict["context_manager"] = {"bookchat_context":{}}
    for k,v in results["bookchat_context"].items():
        response_dict["context_manager"]["bookchat_context"][k] = v


    return response_dict