"""New Entities Detection Refactoring
"""


def detect_author_new(nlu_processor):
    return

def detect_book_name(user_utterance, last_response=""):
    if no_book_name_included(user_utterance):
        return None, None, False

    book_infos = extracted_book_names_from_new_ner(last_response, user_utterance)
    LOGGER.debug("[BOOKCHAT] Result from ner: {}".format(book_infos))
    processed_book_name_results, author_only = post_process_book_ner(book_infos)

    # temporarily just to make the code compatible, as old version required a query name here
    if processed_book_name_results:
        book_name_query = processed_book_name_results[0].get("bookname", "")
    else:
        book_name_query = ""

    return processed_book_name_results, book_name_query, author_only

def extracted_book_names_from_new_ner(last_bot_response, user_utterance):
    # input to ner_server
    input_ = {
        "last_bot_response": last_bot_response,
        "user_utterance": user_utterance
    }
    headers = {
            'Content-Type': 'application/json'
    }
    query_json = json.dumps(input_)
    LOGGER.debug("[BOOKCHAT Entity Detection] Input to NER Server {}".format(query_json))
    try:
        response = requests.post(url=NER_URL,
                                 headers=headers,
                                 data=query_json,
                                 timeout=100).json()
        LOGGER.debug("[BOOKCHAT Entity Detection] Successful extraction from new NER {}".format(response), exc_info=True)
    except Exception as _e:
        LOGGER.debug("[BOOKCHAT Entity Detection] Extract from new NER {}".format(user_utterance), exc_info=True)
        return {}

    # Process response, get a list of booknames
    return response

def post_process_book_ner(book_ner_result):
    books = book_ner_result.get("books", [])
    people = book_ner_result.get("people", [])
    book_name_results = []
    author_only = True
    if len(books) > 0:
        author_only = False
        for entry in books:
            if entry["type"] == "book":
                if is_not_good_book_entry(entry):
                    continue
                book_info = convert_ner_server_format_to_fsm_format(entry)
                book_name_results.append(book_info)
            if entry["type"] == "bookSeries":
                # pick the best book from good reads
                book_info = get_book_series_from_entry(entry)
                book_name_results.append(book_info)

    # If books is None, but detected author, get the most popular book.
    if len(people) > 0:
        if len(people) > 1:
            LOGGER.warning("[BOOKCHAT Entity Detection] Detected more than One Author {}".format(people))
        author = people[0]
        book_info = get_book_series_from_author(author)
        if book_info is not None:
            book_name_results.append(book_info)
    return book_name_results, author_only