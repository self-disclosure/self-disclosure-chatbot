
def book_chat_regex():
    return {"yes_book_grounding": YES_BOOK_GROUNDING,
            "ans_yes": SPECIAL_YES_CASE,
            "user_agree": USER_AGREE_BOOK,
            "req_book": USER_REQUEST_BOOK,
            "user_dislike": USER_DISLIKE,
            "ask_recommend": ASK_RECOMMEND,
            "have_read": HAVE_READ,
            "ask_for_description": ASK_DESCRIPTION,
            "exit_book_module": EXIT_BOOK_MODULE,
            "no_book_grounding": NO_BOOK_GROUNDING}

YES_BOOK_GROUNDING = r"that('s| is) the (one|1)"

NO_BOOK_GROUNDING = r"not the book"

SPECIAL_YES_CASE = r"definitely" #TODO: Add this to System

USER_AGREE_BOOK = r"my favorite .* too"

ASK_DESCRIPTION = r"(describe the book)|(book description)"

ASK_RECOMMEND = r"(?=.*(book|one|1|novel|(anything|something) to read))(((recommend|suggest).* me)|(you.* (recommend|suggest)))"

USER_DISLIKE = r"(haven't|never|don't|not) (like|\bread).* (book|novel)?"

USER_REQUEST_BOOK = r"(talk|chat) about.* book"

EXIT_BOOK_MODULE = r"(i|we) (hate|dislike|don't like) reading|can't .* and read a book|not (going to|gonna) talk about books|(stop|don't|do not).* (talk|discuss|tell.* me).* book|(\bi\b|\bwe\b).* (hate|don't|do not|never).* read.* book|(talk|discuss|tell.* me).* about.* something (else|beside)|(change|different|next|new) topic|get off book|i (hate|don't like|do not like) book(s|)|(can we|let).* not talk about book|(not talk|talking about books)|(no more books)"

HAVE_READ = r"(i've|i have) read"