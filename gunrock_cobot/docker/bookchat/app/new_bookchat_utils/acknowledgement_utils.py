
from acknowledgement.acknowledgement_utils import ModuleLevelAcknowledgementWrapper
import logging
from new_bookchat_utils.logging_utils import set_logger_level, get_working_environment

LOGGER = logging.getLogger(__name__)

# logging.basicConfig(
#     format='[%(levelname)s] %(asctime)s [%(filename)s:%(lineno)d] %(message)s',
# )


set_logger_level(get_working_environment())

# Temporary Regex Location before using Intent Utils
import re

R_HAVE_YOU_READ = r"|".join(["have you read", "have you read", "did you read"])

class BookchatAcknowledgementHandler(ModuleLevelAcknowledgementWrapper):
    """Utils to handle TechscienceAkcnowledgementHandler"""


    def __init__(self, question_detector):
        super(BookchatAcknowledgementHandler, self).__init__()
        self.question_detector = question_detector
        self.key_to_acknowledgement = {
        "s_book_grounding": lambda args : self.add_book_grounding_ack(args)
        }

    def add_acknowledgement(self, response, key, args):
        return self.key_to_acknowledgement[key](args)

    def add_book_grounding_ack(self, args):
        # book_name = args.get("book_name", "that book")
        is_yes_no = self.question_detector.is_yes_no_question()
        have_you_read = re.search(R_HAVE_YOU_READ, self.question_detector.get_question_segment_text()) is not None
        LOGGER.debug("[BOOKCHAT Acknowledgement Handling] {0}, {1}".format(is_yes_no, have_you_read))
        if  is_yes_no and have_you_read:
            return "Yes, I did! "
        return " "