import logging
import re
import json
from typing import List, Dict, Tuple, Union
from fuzzywuzzy import fuzz 
import itertools
from nlu.constants import TopicModule
from difflib import SequenceMatcher 
from new_bookchat_utils.logging_utils import set_logger_level, get_working_environment
from nlu.constants import Positivity, DialogAct as DialogActEnum
from new_bookchat_utils.custom_bookchat_intent_regex import book_chat_regex
from typing import List, Optional
from bookchat_utils import detect_book_name

LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

all_genres = {
    "action",
    "adventure",
    "animation",
    "biography",
    "classics",
    "comedy",
    "comics",
    "crime",
    "documentary",
    "drama",
    "family",
    "fantasy",
    "fable",
    "fiction",
    "nonfiction",
    "graphic novels",
    "history",
    "horror",
    "manga",
    "music",
    "mystery",
    "philosophy",
    "poetry",
    "romance",
    "self help",
    "science fiction",
    "thriller",
    "war",
    "western",
    "young adult",
}

bookchat_regex = book_chat_regex()

class NLU_Processor:
    def __init__(self, automaton):
        self.author_entry = None
        self.automaton = automaton
        self.book_entry = None
        self.raw_book_entry = ([], None, None, [])
        return

    def build_nlu_handler(self, returnnlp, user_text):
        """Returnnlp and Central Element Dictionary directly from InputData
        """
        self._process_nlu_elements(returnnlp)
        self._extract_imported_intent()
        # self._extract_predefined_intentmap()
        # self._add_custom_intent_map()
        self.user_utterance = user_text[0]
        LOGGER.debug("Bookchat User Utterance {}".format(self.user_utterance))
        return self

    def _process_nlu_elements(self, returnnlp):
        """Retrieve topic, concept, noun_phrase from returnnlp
           and dialog_act
        """
        self.returnnlp = returnnlp
        setattr(self.returnnlp, "num_segments", len(self.returnnlp)) #HACK FOR NOW, CHANGE LATER

    def _extract_imported_intent(self):
        if self.is_nlu_valid:
            self.sys_intent = list(itertools.chain(*self.returnnlp.sys_intent))
            self.lexical_intent = list(itertools.chain(*self.returnnlp.lexical_intent))
            self.topic_intent = list(itertools.chain(*self.returnnlp.topic_intent))
        else:
            self.sys_intent = []
            self.lexical_intent = [] 
            self.topic_intent = []
    @property
    def is_nlu_valid(self):
        """Check if NLU Elements are Valid
        """
        return self.returnnlp is not None

    @property
    def user_ask_recommend(self):
        system_ask_recommend = False

        if self.is_nlu_valid:
            system_ask_recommend = "ask_recommend" in self.lexical_intent
        
        local_recommend = self.detect_regex("ask_recommend", self.user_utterance)
        return system_ask_recommend or local_recommend

    @property
    def misheard(self):
        return self.ans_no([r"misheard"])

    def detect_jumpout(self)->List[str]:
        if self.is_nlu_valid:
            return [i.value for i in self.returnnlp.topic_intent_modules if i != TopicModule.BOOK]
        return []

    @property
    def user_ans_unknown(self):
        return re.search(r"(can't|can( |)not) (remember|think of (any|one|1|it|.*book|name))", self.user_utterance)

    @property
    def user_said_yes(self)->bool:
        return self.ans_yes(["ans_yes"])

    @property
    def ans_tell_me_more(self) -> bool:
        return "req_more" in self.sys_intent or self.detect_regex("ask_for_description", self.user_utterance)

    @property
    def user_ans_neg(self)->bool:
        return self.ans_no([])

    def ans_yes(self, custom_regex:Union[List[str], str])->bool:
        
        if self.detect_regex(custom_regex, self.user_utterance):
            return True
        
        if self.is_nlu_valid:
            return self.returnnlp.answer_positivity == Positivity.pos
        LOGGER.warning("[TECHSCIENCE] returnnlp is None.  Using Local Detector Instead")
        return False #TODO: Build Local regex detector!

    @property
    def detect_corona(self):
        if self.is_nlu_valid:
            return "topic_coronavirus" in itertools.chain(*self.returnnlp.topic_intent)
        search = re.search(r"\b(corona virus|pandemic|coronavirus|virus|corona|covid|nineteen)\b", self.user_utterance)
        LOGGER.debug("[BOOKCHAT] {}".format(search))
        return search is not None

    def detect_regex(self, regex_r:Union[List[str], str], text:str=None)->bool:
        if text is None:
            text = self.user_utterance
        if len(regex_r) == 0:
            return False
        if isinstance(regex_r, str):
            regex_r = [regex_r]
        
        return any(
            [reg in bookchat_regex and re.search(bookchat_regex[reg], text) is not None for reg in regex_r])
            
    def user_dislike(self, seg, custom_regex:Union[List[str], str]=[])->bool:

        
        if self.is_nlu_valid:
            if self.detect_regex(custom_regex, self.returnnlp[seg].text):
                return True
            neg_dialog_act = self.returnnlp.has_dialog_act(DialogActEnum.NEG_ANSWER,index=seg)
            if neg_dialog_act:
                return True

            return self.returnnlp.has_intent_seg(seg, {'ans_negative', 'ans_dislike'})
        LOGGER.warning("[TECHSCIENCE] returnnlp is None.  Using Local Detector Instead")
        return False #TODO: Build Local regex detector!

    def ans_no(self, custom_regex:Union[List[str], str])->bool:
        
        if self.detect_regex(custom_regex, self.user_utterance):
            return True
        
        if self.is_nlu_valid:
            return self.returnnlp.answer_positivity == Positivity.neg
        LOGGER.warning("[TECHSCIENCE] returnnlp is None.  Using Local Detector Instead")
        return False #TODO: Build Local regex detector!

    @property
    def command_exit_book_module(self):
        return self.detect_regex("exit_book_module", self.user_utterance)

    @property
    def req_book(self):
        return self.detect_regex("req_book", self.user_utterance)

    def user_agree(self, regex_r:Union[List[str], str])->bool:
        positive = self.ans_yes(regex_r)

        LOGGER.debug("[BOOKCHAT NLU] Postiive {0}, Lexical Intents {1}, Regex {2}".format(positive, self.lexical_intent, regex_r))
        if self.is_nlu_valid:
            """Note this case is a more strict case of user_agree
            TODO: Fine tune user agree
            """
            return positive
        return positive

    def get_entities(self):
        entities = self.returnnlp.detect_entities(consider_entertainment_ner=True)
        LOGGER.debug("NLU {}".format(entities))
        return entities

    def get_pos_of_word_seg(self, entities:Union[List[str], str]) -> Dict:
        """Given a list of entities, return the corresponding positivity of a word
        at that location
        """
        LOGGER.debug("[BOOKCHAT NLU] Entities {}".format(entities))
        result_entities = {}
        for seg_i in range(self.returnnlp.num_segments):
            text = self.returnnlp[seg_i].text
            dialog_act = self.returnnlp[seg_i].dialog_acts
            for entity in entities:
                if self._common_substring(entity.lower(), text):
                    LOGGER.debug("[BOOKCHAT NLU] Segment: {0}, entity {1}, Text {2}".format(seg_i, entity, text))
                    result_entities[entity] = seg_i
        LOGGER.debug("[BOOKCHAT NLU] Result Entities {}".format(result_entities))
        return result_entities

    def _common_substring(self, s1, s2):
        seqMatch = SequenceMatcher(None,s1,s1) 
        match = seqMatch.find_longest_match(0, len(s1), 0, len(s1))
        return match.size > 0

    @property
    def user_ans_same(self):
        LOGGER.debug("[NLU_PROCESSING_UTILS] {}".format(self.lexical_intent))
        return "ans_same" in self.lexical_intent or self.user_said_yes

    @property
    def user_prefer(self):
        LOGGER.debug("[NLU_PROCESSING_UTILS] {}".format(self.lexical_intent))
        return "ans_like" in self.lexical_intent or self.user_said_yes

    @property
    def detect_genre(self):
        try:
            detected_genres = []
            entities = self.get_entities()
            LOGGER.debug("[BOOKCHAT_MODULE] Detecting Entities {}".format(entities))
            for genre in all_genres:
                if re.search(genre, self.user_utterance):
                    detected_genres.append(genre)
            return detected_genres
        except:
            LOGGER.debug("[BOOKCHAT_MODULE] Exception in detecting genre")
            return []

    def detect_author(self, force=False):
        if self.author_entry is None or force:
            self.author_entry = self._detect_author()
            return
        
        return self.author_entry

    def _detect_author(self):
        try:
            entity_names = []
            result, final_book_query, author_only, raw_book_names = detect_book_name(self.automaton)
            self.raw_book_entry = (result, final_book_query, author_only, raw_book_names)
            LOGGER.debug(" BOOKCHAHT DETECTION Detect Author {}".format(result))
            if result and len(result) > 0:
                return [r.get("bookauthor") for r in result if r.get("bookauthor") is not None]
            else:

                return []
        except:
            return []

    def detect_book(self, force=False, get_raw=False):
        if self.book_entry is None or force or self.raw_book_entry[1] is None:
            self.book_entry, self.raw_book_entry = self._detect_book()

        if get_raw:
            return self.raw_book_entry
        
        return self.book_entry

    def detect_fiction(self):
        return re.search(r"fiction",self.user_utterance) and not re.search(r"non.*fiction",self.user_utterance)

    def detect_non_fiction(self):
        return re.search(r"non.*fiction",self.user_utterance)
    def _detect_book(self):
        try:
            entity_names = []
            raw_book_entry = detect_book_name(self.automaton)
            results, final_book_query, author_only, raw_book_names = raw_book_entry
            LOGGER.debug("Raw results {}".format(results))
            if type(results) == list and len(results) > 0:
                results = results[0]

            if type(results) == dict:
                entity_names.append(results["bookname"])

            LOGGER.debug(" BOOKCHAHT DETECTION entity_names_1 {}".format(entity_names))
            if len(entity_names) == 0:
                return [], raw_book_entry
            return entity_names, raw_book_entry
        except Exception as e:
            LOGGER.debug("[BOOKCHAT_MODULE] Exception in detect_entity_1 {}".format(e))
            return [], self.raw_book_entry

    def get_raw_book_name(self):
        return self.raw_book_entry[-1]

    @property
    def user_have_read(self):
        return self.detect_regex("have_read", self.user_utterance)
