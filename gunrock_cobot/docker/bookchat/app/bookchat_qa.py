import re

from cobot_common.service_client import get_client
from nlu.question_detector import QuestionDetector
from question_handling.quetion_handler import QuestionHandler, QuestionResponse, QuestionResponseTag
from typing import Optional, List
from types import SimpleNamespace
from new_bookchat_utils.logging_utils import set_logger_level, get_working_environment
import logging
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())
COBOT_API_KEY = 'xgTjk23SRM9Q9VFrUEFOjav0Bn4ot21W8uFLEieT'

# QA resources:
# Hardcoded logic
# Backstory
# EVI
# TODO: Define arbitrary order for calling these resources based on state
# TODO: Give flexibility, can return just reponse or also next state
# TODO: Option to toggle which response generators to use
class BookQuestionDetector(QuestionDetector):
    def __init__(self, complete_user_utterance: str, returnnlp: Optional[List[dict]], nlu_processor=None):
        super(BookQuestionDetector, self).__init__(complete_user_utterance, returnnlp)
        self.nlu_processor = nlu_processor



class BookQuestionHandler(QuestionHandler):
    def __init__(self, automaton, question_detector):
        self.nlu_processor = automaton.nlu_processor
        self.system_backstory_response = None

        self.complete_user_utterance = self.nlu_processor.user_utterance
        self.returnnlp = self.nlu_processor.returnnlp
        self.question_detector = question_detector

        self.question_segment_text = self.question_detector.get_question_segment_text()
        self.backstory_threshold = 1.0
        self.system_ack = system_acknowledgement
        self.user_attribute_ref = user_attribute_ref if user_attribute_ref else SimpleNamespace()

class QABot:
    def __init__(self, automaton):
        self.automaton = automaton
        LOGGER = automaton.logger
        self.text = automaton.text
        self.dialog_act = automaton.dialog_act
        # self.amz_dialog_act = automaton.amz_dialog_act
        # self.ner = automaton.ner
        # self.asr_correction = automaton.asr_correction
        # self.knowledge = automaton.knowledge
        # self.topic_keywords = automaton.topic_keywords
        # self.sentiment = automaton.sentiment
        self.central_elem = automaton.central_elem
        self.subdialog_context_tags = automaton.user_attributes.get("subdialog_context",{}).get("tags", [])
        # self.features = automaton.features
        self.question_detector = self.automaton.question_detector
        # self.question_handler = self.automaton.question_handler

    # def get_answer_only_with_question(self, question, transition=None):
    #     self.question_type = "modified_question"
    #     self.question_text = question
    #     if self.question_type is not None:
    #         self.automaton.bookchat_user["tags"].append(
    #             "book_user_qa_answer_only_with_question")
    #         res = self.specific_handling_logic_answer_only()
    #         if res:
    #             return res
    #         res = self.general_handling_logic_answer_only()
    #         if res:
    #             return res
    #     return None

    def get_answer_only(self, enable_none=False, transition=None):
        self.question_type, self.question_text = self.determine_potential_question()
        LOGGER.info("[BOOK QA] question_type: {0}, question_text {1}".format(self.question_type, self.question_text))
        if self.question_type is not None:
            self.automaton.bookchat_user["tags"].append(
                "book_user_qa_answer_only")
            res = self.specific_handling_logic_answer_only()
            if res:
                return res
            res = self.general_handling_logic_answer_only(enable_none)
            if res:
                return res
        return None

    def get_answer_only_new(self, enable_none=False, transition=None):
        self.question_type, self.question_text = self.determine_potential_question()
        LOGGER.info("[BOOK QA] question_type: {0}, question_text {1}".format(self.question_type, self.question_text))
        if self.question_type is not None:
            self.automaton.bookchat_user["tags"].append(
                "book_user_qa_answer_only")

            # Transition based followup question

            # Question Handler
            res = self.question_handler.handle_question()
            res = self.specific_handling_logic_answer_only()
            if res:
                return res
            res = self.general_handling_logic_answer_only(enable_none)
            if res:
                return res
        return None

    def get_backstory_response_if_high(self):
        response = self.question_handler.get_high_level_backstory()
        return

    def specific_handling_logic_answer_only(self):
        if re.search(
            r"(talk|chat|know) about.* book(s|)$",
                self.question_text):
            return "Sure! Just let me know if you want to talk about a specific book. "
        elif re.search(r"ask you.* (question|something)", self.question_text):
            return "Yes, you can ask me questions anytime. "
        elif re.search(r"you (\bread).* book", self.question_text) and self.automaton.user_attributes["current_subdialog"] == "BookDialog":
            return "Yes, I've read it. "
        return None

    def general_handling_logic_answer_only(self, enable_none):
        backstory_response, backstory_confidence = self.get_backstory_response_central_elem()
        if backstory_response and backstory_confidence > 0.9: # Highest Level Backstory Confidence
            LOGGER.info("[BOOKCHAT QA] Backstory high - question: {0}, response: {1}".format(
                             self.question_text, backstory_response))
            return backstory_response

        evi_response = self.get_evi_response()
        if evi_response:
            LOGGER.info("[BOOKCHAT QA] EVI - question: {0}, response: {1}".format(
                             self.question_text, evi_response))
            return evi_response

        if backstory_response and backstory_confidence > 0.75: # Second Level Backstory Confidence
            LOGGER.info("[BOOKCHAT QA] Backstory low - question: {0}, response: {1}".format(
                             self.question_text, backstory_response))
            return backstory_response

        '''
        if len(self.text.strip().split()) < 7:
            return self.automaton.utt(
                ["qa_responses", "default_no_ask_continue"], {"user_question": self.reword_question()})
        '''
        if enable_none:
            return None
        return self.automaton.utt(["qa_responses", "short_no_ans"])

    # def handle_potential_question(self, resume_state, transition=None):
    #     self.resume_state = resume_state
    #     self.question_type, self.question_text = self.determine_potential_question()
    #     if self.question_type is not None:
    #         self.automaton.bookchat_user["tags"].append("book_user_qa")
    #         res = self.specific_handling_logic()
    #         if res:
    #             return res
    #         res = self.general_handling_logic()
    #         if res:
    #             return res
    #     return None

    def retrieve_potential_question(self):
        # first try dialog act
        self.dialog_act_confidence = -1

        if self.question_detector and self.question_detector.has_question():
            
            da, conf = self.question_detector.get_question_da(-1)
            text = self.question_detector.get_question_segment_text()
            LOGGER.debug("[BOOKCHAT QUESTION DETECTOR] Question Detected {0}, {1}".format(da.value, text))
            return da.value, text
        if self.dialog_act:
            for entry in self.dialog_act:
                if "response_da" in entry:  # skip
                    continue
                elif (entry["DA"] == "open_question" or entry["DA"] == "yes_no_question" or entry["DA"] == "open_question_factual" or entry["DA"] == "open_question_opinion") and entry["confidence"] > 0.65:
                    self.dialog_act_confidence = entry["confidence"]
                    return entry["DA"], entry["text"]

        return None, None

    def determine_potential_question(self):
        question_type, question_text = self.retrieve_potential_question()
        question_type, question_text = self.general_post_processing(question_type, question_text)
        if self.automaton.user_attributes["current_subdialog"] == "BookDialog":
            return self.book_dialog_processing(question_type, question_text)
        return question_type, question_text

    # def specific_handling_logic(self):
    #     if re.search(r"(talk|chat|know) about.* book(s|)$", self.question_text):
    #         self.automaton.response = "I love reading all sorts of books. What book would you like to talk about? "
    #         self.automaton.t_context["next_transition"] = "t_ask_book"
    #         return "s_echo"
    #     elif re.search(r"ask you.* (question|something)", self.question_text):
    #         self.automaton.response = "Yes, you can ask me questions. I'll answer them if I can. "
    #         self.automaton.t_context["next_transition"] = self.resume_state
    #         return "s_echo"
    #     return None

    # def general_handling_logic(self):
    #     self.automaton.t_context["next_transition"] = self.resume_state

    #     backstory_response, backstory_confidence = self.get_backstory_response_central_elem()
    #     if backstory_response and backstory_confidence > 0.9: # Highest Level Backstory Confidence
    #         LOGGER.info("[BOOKCHAT QA] Backstory high - question: {0}, response: {1}".format(
    #                          self.question_text, backstory_response))
    #         self.automaton.response = backstory_response
    #         return "s_echo"

    #     evi_response = self.get_evi_response()
    #     if evi_response:
    #         LOGGER.info("[BOOKCHAT QA] EVI - question: {0}, response: {1}".format(
    #                          self.question_text, evi_response))
    #         self.automaton.response = evi_response)
    #         return "s_echo"

    #     if backstory_response and backstory_confidence > 0.65: # Third Level Backstory Confidence
    #         LOGGER.info("[BOOKCHAT QA] Backstory low - question: {0}, response: {1}".format(
    #                          self.question_text, backstory_response))
    #         self.automaton.response = backstory_response
    #         return "s_echo"

    #     self.automaton.response = self.automaton.utt(
    #         ["qa_responses", "default"], {"user_question": self.reword_question()})
    #     self.automaton.bookchat_user["flow_interruption"] = "i_ask_continue_book"
    #     return "s_echo"

    # def reword_question(self):
    #     replace_words = {"you": "I", "your": "my"}
    #     reworded_question = []
    #     for word in self.question_text.strip().split():
    #         if word in replace_words:
    #             word = replace_words[word]
    #         reworded_question.append(word)
    #     return " ".join(reworded_question)

    def get_backstory_response_central_elem(self):
        backstory = self.central_elem["backstory"]
        if "text" in backstory and "confidence" in backstory:
            return backstory["text"] + " ", backstory["confidence"]
        return None, None
    '''
    def get_backstory_response(self, utterance):
        try:
            headers = {
                'Content-Type': 'application/json',
            }
            data = {
                'text': utterance
            }
            resp = requests.post(
                'http://ec2-54-175-156-102.compute-1.amazonaws.com:8085/module',
                headers=headers,
                data=json.dumps(data),
                timeout=1)
            ret = resp.json()
            # NOTE: http code
            if resp.status_code < 500:
                return ret['text'] + " ", ret['confidence']
        except Exception as e:
            LOGGER.info(
                '[book_qa.get_backstory_response] warning: post sentence embedding backsotry with data err: {}'.format(e))

        return None, None
    '''

    def get_evi_response(self):
        try:
            client = get_client(api_key=COBOT_API_KEY)
            r = client.get_answer(question=self.text, timeout_in_millis=1000)
            if r["response"] == "" or r["response"].startswith('skill://') or any(re.search(x, r["response"]) for x in evi_filter_list or len(r["response"]) > 300):
                return None
            return r["response"] + " "
        except Exception as e:
            LOGGER.info(
                "[book_qa.get_evi_response] Exception: {}".format(repr(e)))

        return None

    def general_post_processing(self, question_type, question_text):
        if question_text is None:
            return question_type, question_text
        elif re.search(r"^(what|how) about you", question_text):
            return None, None
        elif re.search(r"^(when|where) (is|was|did)\b", question_text) and self.dialog_act_confidence > 0.9:
            return question_type, question_text
        elif re.search(r"^when\b|^where\b", question_text):
            return None, None
        return question_type, question_text

    def book_dialog_processing(self, question_type, question_text):
        if question_text is None:
            return question_type, question_text
        #TODO URGENT
        # self.nlu_processor.user_ask_preference and 
        elif "q1_question" in self.subdialog_context_tags and "or" in question_text:
            return None, None
        return question_type, question_text

ask_intents = {
    "ask_yesno", "ask_preference", "ask_opinion", "ask_advice", "ask_ability", "ask_hobby",
    "ask_recommend", "ask_reason", "ask_self", "ask_fact", "ask_freq", "ask_dist", "ask_loc",
    "ask_count", "ask_degree", "ask_time", "ask_person", "ask_name", "topic_qa", "topic_backstory"
}

evi_filter_list = {
    r"Sorry, I can’t find the answer to the question I heard",
    r"I don't have an opinion on that",
    r"You could ask me about music or geography",
    r"You can ask me anything you like",
    r"I can answer questions about people, places and more",
    r"I didn't get that",
    r"As a noun|is usually defined as|As an adjective|As a verb",
    r"To experience Audible",
}
