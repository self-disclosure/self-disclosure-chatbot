import pytest
from bookchat_utils import extracted_book_names_from_new_ner

class TestBookNer:

    book_extraction_test_data = [
        ("judy blume tales of the fourth grade nothing it connects to me", "Anyways, I think reading a book can change how you view the world. What book has had a big impact on your life?", "Tales of a Fourth Grade Nothing"),
        ("The Institute",
         "Anyways, I think reading a book can change how you view the world. What book has had a big impact on your life?",
         ""),
        ("The Institute",
         "Cool! I love to read when I'm not chatting. <break time='150ms'/> Have you read any good books recently?",
         "")

    ]
    @pytest.mark.parametrize("user_utterance, last_bot_response, expected_result", book_extraction_test_data)
    def test_extract_book_name(self, user_utterance, last_bot_response, expected_result):
        result = extracted_book_names_from_new_ner(last_bot_response, user_utterance)
        print(result)
