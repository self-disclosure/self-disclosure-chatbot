from goodreads_utils import *
from bookchat_utils import *
import json

book_client = GoodreadsClient()


def test_request_book_serach_with_book_name():
    book_names = ["harry potter", "sapiens", "talking to strangers", "yes sapiens"]

    for book_name in book_names:
        result = book_client._request_book_search_with_book_name(book_name)
        assert_results(result)


def assert_results(result):
    try:
        results = result["search"]["results"]["work"]
    except KeyError:
        assert False

    assert len(results) <= 20