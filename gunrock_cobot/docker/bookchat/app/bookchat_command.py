import re
import logging
from new_bookchat_utils.logging_utils import set_logger_level, get_working_environment
from nlu.command_detector import CommandDetector
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())
from selecting_strategy.module_selection import topic_name_map as modules_mapping


class BookCommandDetector(CommandDetector):
    def __init__(self, automaton):
        self.automaton = automaton
        user_utterance = automaton.text
        returnnlp_raw = automaton.returnnlp_raw
        super(BookCommandDetector, self).__init__(user_utterance, returnnlp_raw)
        self.nlu_processor = automaton.nlu_processor

    def handle_jumpout(self):
        if self.automaton.detect_coronavirus:
            return "s_ask_exit"

        jumpout_modules = list(self.nlu_processor.detect_jumpout())

        LOGGER.debug("[BOOKCHAT] JUMPOUT_MODULES {}".format(jumpout_modules))
        if len(jumpout_modules) > 0:
            LOGGER.info("[BOOKCHAT] Detected Jumpout {}".format(jumpout_modules))
            output = modules_mapping.get(jumpout_modules[0], ["something else"])
            if len(output) > 0:
                output_topic = list(output)[0]
            else:
                output_topic = "something else"
            self.automaton.t_context["propose_continue"] = "UNCLEAR"
            self.automaton.module_selector.propose_topic = jumpout_modules[0]
            self.automaton.t_context["propose_topic"] = self.automaton.utt(["exit_book_unclear"], {"topic":output_topic})
            
            return "s_ask_exit"
        return None

    @property
    def command_exit_book_module(self):
        return self.nlu_processor.command_exit_book_module

    @property
    def command_exit_book(self):
        return False # User request a different book.  Can we talk about a different book? TODO

    @property
    def command_user_preference(self):
        return self.nlu_processor.user_prefer and len(self.nlu_processor.detect_genre) > 0


    def handle_potential_command(self, resume_state):
        self.resume_state = resume_state
        if self.has_command:
            jumpout = self.handle_jumpout() and len(self.jumpout_blacklist) == 0
            LOGGER.info("[BOOKCHAT COMMAND DETECTOR] Command Exists Jumpout {}".format(jumpout))
            if jumpout:
                self.automaton.t_context["propose_continue"] = "UNCLEAR"
                return "s_ask_exit"
            if self.command_exit_book_module:
                LOGGER.info("[BOOKCHAT COMMAND DETECTOR] Exit Book") #TODO REfine this
                self.automaton.t_context["propose_continue"] = "STOP"
                return "s_ask_exit"
            if self.command_exit_book:
                self.automaton.context.current_context["author_only"] = False
                return "s_ask_book"
            if self.nlu_processor.req_book:
                self.automaton.context.current_context["author_only"] = False
                return "s_ask_book"

            if self.nlu_processor.user_prefer and len(self.nlu_processor.detect_genre) > 0:
                LOGGER.debug("Detected User Prefer genre {}".format(self.nlu_processor.detect_genre))
                return "s_ask_book"
        return None

    def is_filtered_command(self):
        if self.automaton.switch_to_module and re.search(r"(talk|chat) about.* book", self.command_text):
            return True
        return False

    @property
    def jumpout_blacklist(self):
        genre = self.nlu_processor.detect_genre
        LOGGER.info("[BOOKCHAT COMMAND DETECTOR] Jumpout Blacklist Jumpout {}".format(genre))
        return genre
        
    def specific_handling_logic(self):
        if self.command_type == "propose_module_switch":
            candidate_module = self.automaton.user_attributes.get("candidate_module")
            if candidate_module is not None and candidate_module == self.command_text:
                self.automaton.t_context["propose_topic"] = "Hmm. Do you want to talk about {} instead of books? ".format(modules_mapping[self.command_text])
                self.automaton.t_context["propose_continue"] = "UNCLEAR"
                self.automaton.module_selector.propose_topic = self.command_text
                return "s_ask_exit"
            self.automaton.bookchat_user["candidate_module"] = self.command_text
            return None
        else:
            if re.search(r, self.command_text):
                self.automaton.response = "I love reading all sort of books. What book would you like to talk about? "
                self.automaton.t_context["next_transition"] = "t_ask_book"
                return "s_echo"
        return None
