import time
import logging
import re
from types import SimpleNamespace
from util_redis import RedisHelper
from nlg_post_process.profanity_classifier import BotResponseProfanityClassifier
from user_profiler.user_profile import UserProfile
from nlu.intentmap_scheme import MAX_1_WORDS, MAX_2_WORDS
from nlu.stopwords import stopwords

def trace_latency(func):
    import time

    def inner_func(*args, **kwargs):
        try:
            start = time.time()
            output = func(*args, **kwargs)  # Call the original function with its arguments.
            end = time.time()

            logging.info(f"[Trace latency] function: {func.__qualname__}, latency: {(end - start) * 1000}")

            return output

        except Exception as e:
            logging.error(f"[Trace latency] Fail to execute function: {func.__qualname__}, message: {e}")

    return inner_func


class BlenderResponseGenerator:
    """
    Use blender to handle question handling and acknowledgement.
    """

    def __init__(self, user_attributes_ref):
        self.user_attribute_ref = user_attributes_ref if user_attributes_ref else SimpleNamespace()
        self.session_id = getattr(self.user_attribute_ref, "session_id", "")
        self.redis_client = RedisHelper()
        self.user_profile = UserProfile(self.user_attribute_ref)
        self.blender_start_time = getattr(self.user_attribute_ref, "blender_start_time", None)
        logging.info(f"[Blender] start time: {self.blender_start_time}")

    @trace_latency
    def generate_response(self) -> str:
        response = self.fetch()
        logging.info("[Blender] ack and qa response = {}, key = {}".format(response, self._get_key()))

        if response:
            if not self.should_filter_out_inappropriate_blender_response(response) and \
                    not BotResponseProfanityClassifier.is_profane(response):
                response += ". "
                return response

        return ""

    def fetch(self):
        response = ""
        if not self.session_id:
            return ""

        if self.blender_start_time:
            timeout = 2500  # 2500 msec
            while self._get_duration_in_msec() < timeout:
                response = self.redis_client.get(RedisHelper.PREFIX_BLENDER_QUESTION_HANDLING, self._get_key())
                if response:
                    logging.info(f"[Blender] fetch: get result with global duration (msec): {self._get_duration_in_msec()}")
                    return response

                time.sleep(0.05)
            logging.info(f"[Blender] fetch: get no result with global duration (msec): {self._get_duration_in_msec()}")
        else:
            for i in range(6):
                response = self.redis_client.get(RedisHelper.PREFIX_BLENDER_QUESTION_HANDLING, self._get_key())
                if response:
                    logging.info(f"[Blender] fetch: get result with local duration (msec): {0.05 * i * 1000}")
                    return response

                time.sleep(0.05)
            logging.info(f"[Blender] fetch: get no result with local duration (msec): {300}")

        return response

    def _get_duration_in_msec(self):
        return (time.time() - self.blender_start_time) * 1000

    def _get_key(self):
        key = "{}_{}".format(self.session_id, self.user_profile.total_turn_count)
        return key

    @staticmethod
    def should_filter_out_inappropriate_blender_response(response) -> bool:
        SIBLING = r"((brother|sister|sibling)(|s))"
        KID = r"(kid(|s)|son(|s)|daughter(|s))"
        PARENT = r"(mom|dad|(grand|)parent(|s))"
        FAMILY = rf"(famil(y|ies)|{SIBLING}|{KID}|{PARENT})"  # famil(y|ies)|
        PET = rf"((dog|cat|pet)(|s))"
        THING = "(body|face|car)"

        response_filter = "|".join([
            r".*\b(my name is)\b.*",
            r".*\b(i'm a)\b\b",
            r".*\b(i'm not)\b",
            r".*\b(i don't know you).*\b",
            r".*\b(i would love to do that)\b",
            r".*\b(snuf film)",
            r".*\b(mad)\b",
            r".*\b((i'm going to)|(i have been to))\b.*",
            r".*\bi live in\b.*",
            rf".*\bi{MAX_1_WORDS}moving\b.*",
            rf".*\bmy ((ex-|)(husband|wife|boyfriend|girlfriend))\b",
            rf".*\bmy ({FAMILY}|{PET}|{THING})\b",
            rf".*i.*(have|had){MAX_2_WORDS}({FAMILY}|{PET}|{THING}).*",
            rf".*i have been married",
            rf".*\bi have had\b.*",
            rf".*\bmy instagram\b",
        ])

        return re.match(response_filter, response.lower()) is not None


