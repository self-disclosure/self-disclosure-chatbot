import logging
import traceback
from typing import Any, Callable, Optional, Union


logger = logging.getLogger('nlu.typecheck')


class TypeCheckError(Exception):

    def __init__(self, obj: Any, expected_type: type, stack_info: traceback.StackSummary):
        stack_itr = reversed(stack_info)
        type_checking_stack = next(stack_itr, None)
        caller_stack = next(stack_itr, None)

        if type_checking_stack and caller_stack:
            message = (
                f'[TYPECHECK] <{type_checking_stack.name}> failed: '
                f"Object {obj} {type(obj)} is not of expected type: {expected_type}. "
                f'Traceback: "{caller_stack.filename}", '
                f'at line {caller_stack.lineno}: {caller_stack.line}. '
            )
        else:
            message = f"[TYPECHECK] Object {obj} {type(obj)} is not of expected type: {expected_type}."
        super().__init__(message)


class TypeCastError(Exception):
    def __init__(self, error: Exception, stack_info: traceback.StackSummary):
        stack_itr = reversed(stack_info)
        type_checking_stack = next(stack_itr, None)
        caller_stack = next(stack_itr, None)

        if type_checking_stack and caller_stack:
            message = (
                f'[TYPECHECK] <{type_checking_stack.name}> failed: '
                f'{error}'
                f'Traceback: "{caller_stack.filename}", '
                f'at line {caller_stack.lineno}: {caller_stack.line}. '
            )
        else:
            message = f"{error}"
        super().__init__(message)


def is_type(d: Any, type_: type, warnings: str = 'default'):
    if not isinstance(d, type_):
        logger.warning(TypeCheckError(d, type_, traceback.extract_stack()))
        # if warnings:
        #     logger.warning(TypeCheckError(d), exc_info=True,
        #                    stack_info=False if warnings in {'default', 'no_stack'} else True)
        return False
    return True


def ensure_type(d: Any, type_: type, default: Optional[Union[Any, Callable[[], Any]]], warnings: str = 'default'):
    if not isinstance(d, type_):
        logger.warning(TypeCheckError(d, type_, traceback.extract_stack()))
        # if warnings:
        #     logger.warning(TypeCheckError(d), exc_info=True,
        #                    stack_info=False if warnings in {'default', 'no_stack'} else True)
        return default() if callable(default) else default
    else:
        return d


def safe_cast(value: Any, type_: Union[type, Callable[[Any], Any]],
              default: Optional[Union[Any, Callable[[], Any]]], warnings: str = 'default'):
    try:
        if isinstance(value, str) and value == 'None':
            return default() if callable(default) else default
        return type_(value)
    except (TypeError, ValueError) as e:
        logger.warning(TypeCastError(e, traceback.extract_stack()))
        # if warnings:
        #     logger.warning(e, exc_info=True,
        #                    stack_info=False if warnings in {'default', 'no_stack'} else True)
        return default() if callable(default) else default
