from bookchat_utils import *

class ContextManager:
    def __init__(self, user_id):
        self.user_id = user_id
        # context
        self._load_context()
        
    def _pack_context(self):
        return {
            "books": self.books,
            "authors": self.authors,
            "chitchat": self.chitchat,
            "current_context": self.current_context,
        }
    
    def _unpack_context(self, context):
        if type(context) == dict:
            self.books = context.get("books", {})
            self.authors = context.get("authors", {})
            self.chitchat = context.get("chitchat", {})
            self.current_context = context.get("current_context", {})
        else:
            self.books = {}
            self.authors = {}
            self.chitchat = {}
            self.current_context = {}          
    
    def _load_context(self):
        user_context = get_redis_with_userid(USER_PREFIX, self.user_id)
        self._unpack_context(user_context)
    
    def save_context(self):
        user_context = self._pack_context()
        set_redis_with_userid(USER_PREFIX, self.user_id, user_context)
    
    