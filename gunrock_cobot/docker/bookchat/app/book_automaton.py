import re
import random
import logging

import inspect
from timeit import default_timer as timer

from bookchat_utils import *
from bookchat_qa import *
from bookchat_command import *
from context_manager import *
from book_dialog import *
from chitchat_dialog import *
from nlu.dataclass.returnnlp import ReturnNLP
from nlu.dataclass.central_element import CentralElement
from nlu.question_detector import QuestionDetector
from nlu.constants import TopicModule
from new_bookchat_utils.logging_utils import set_logger_level, get_working_environment
from question_handling.quetion_handler import QuestionHandler, QuestionResponse, QuestionResponseTag
from new_bookchat_utils.acknowledgement_utils import BookchatAcknowledgementHandler
from new_bookchat_utils.nlu_process_utils import NLU_Processor
from selecting_strategy.module_selection import ModuleSelector
from types import SimpleNamespace
from nlu.constants import TopicModule
import template_manager
from response_templates import template_data
template_books = template_manager.Templates.books

wrong_book_regex = r"(wrong|incorrect|not a).* (book|novel|poem|comic|story|version|\bone\b)|(wasn't|not).* (what|book|novel|poem|comic|story|version|\bone\b).* (\bi\b|\bi'm\b|name)|got.* wrong|not.* (right|talking about that|(this|that) book)|you.* (mistake|error|dumb|stupid)"
not_book_regex = r"(did(n't| not)|was(n't| not)).* (talk|propose|suggest|mention|say|mean).* book|(isn't|not|wasn't) a book"
dont_talk_book = r"i.* do(n't| not).* (talk|chat|discuss) about.* (this|book|anymore)|not (talk|chat|discuss).* (this|book|anymore)"

LOGGER = logging.getLogger(__name__)

ERROR_TEMPLATE = template_manager.Templates.error

# logging.basicConfig(
#     format='[%(levelname)s] %(asctime)s [%(filename)s:%(lineno)d] %(message)s',
# )

BACKSTORY_CONFIDENCE_THRESHOLD = 0.9

set_logger_level(get_working_environment())

class BookAutomaton:
    def __init__(self, app_logger):
        self.logger = LOGGER
        self.last_response = ""
        self.__bind_states()
        self.__bind_transitions()
        self.__bind_interruptions()

        self.response = ""

    # bind each state to corresponding callback function that generates response
    def __bind_states(self):
        self.states_mapping = {
            "s_intro": self.s_intro,
            "s_echo": self.s_echo, # special state that just returns the response from the transition callback function
            "s_interesting_comment": self.s_interesting_comment,
            "s_ask_book": self.s_ask_book,
            "s_ask_exit": self.s_ask_exit,
            "s_chitchat": self.s_chitchat,
            "s_book_chitchat": self.s_book_chitchat,
            "s_book_grounding": self.s_book_grounding,
            "s_subdialog": self.s_subdialog,
        }

    # bind each state to corresponding callback function that determines
    # the next state
    def __bind_transitions(self):
        self.transitions_mapping = {
            "t_init": self.t_init,
            "t_sys_intent": self.t_sys_intent,
            #"t_intro": self.t_intro,
            "t_resume": self.t_resume, # resume after bookchat not continuously selected
            "t_interesting_comment": self.t_interesting_comment,
            "t_ask_book": self.t_ask_book,
            "t_ask_exit": self.t_ask_exit,
            "t_chitchat": self.t_chitchat,
            "t_book_chitchat": self.t_book_chitchat,
            "t_book_grounding": self.t_book_grounding,
            "t_handle_bible_intent": self.t_handle_bible_intent,
            "t_subdialog": self.t_subdialog,
        }

    def __bind_interruptions(self):
        self.interruptions_mapping = {
            "i_ask_continue_book": self.i_ask_continue_book,
        }

    def get_first_element(self, value):        
        if isinstance(value, list) and len(value) > 0:
            return value[0]
        elif isinstance(value, list):
            return ""
        else:
            return str(value)

    def set_expect_opinion(self):
        self.bookchat_user["expect_opinion"] = True

    def _get_text(self, input_data):
        LOGGER.debug("Book Input Data {}".format(input_data))
        text = input_data.get("text", [""])
        self.text = self.get_first_element(text)
        
        self.last_module = input_data.get("last_module", "")

        a_b_test = input_data.get("a_b_test", ["A"])
        self.a_b_test = self.get_first_element(a_b_test)

        last_response = input_data.get("last_response", [""])
        self.last_response = last_response[0] if len(last_response) > 0 else ""

        session_id = input_data.get("session_id", [""])
        self.session_id = self.get_first_element(session_id)

        self.suggest_keywords = self.module_selector.get_top_propose_keyword().text
        
        self._user_name = input_data.get("usr_name", "<no_name>")
        self.ack_field = input_data.get("system_acknowledgement", [{'output_tag': '', 'ack': ''}])
        if isinstance(self.ack_field, list) and len(self.ack_field) > 0:
            self.ack_field_raw = self.ack_field[0]
        else:
            self.ack_field_raw = {'output_tag': '', 'ack': ''}
        return

    @property
    def user_name(self):
        if self._user_name == "<no_name>":
            return None
        return self._user_name

    def build_question_handler(self):
        if len(self.returnnlp_raw) == 0:
            self.question_detector = None
            self.question_handler = None
        else:
            self.question_detector = QuestionDetector(self.text, self.returnnlp)
            self.question_handler = QuestionHandler(self.text, \
                               self.returnnlp_raw, \
                               BACKSTORY_CONFIDENCE_THRESHOLD, \
                               self.ack_field_raw, \
                               self.user_attributes_ref)

    def get_acknowledgement_handler(self, rebuild=True):
        """Build the acknowledgement handler for each transduce
        """
        self.acknowledgement_handler = BookchatAcknowledgementHandler(self.question_detector)
        if rebuild:
            self.acknowledgement_handler = self.acknowledgement_handler.build_acknowledgement_handler(self.ack_field)
        return self.acknowledgement_handler

    def _retrieve_nlu(self, input_data):
        """Retrieve NLU metadata

        Parameters:
           input_data:  raw input dictionary from system module

        Set Instance Variables:
           self.central_elem - Dataclass central element
           self.returnnlp - Datastructure returnnlp

        Output:
           None
        """
        returnnlp = input_data.get("returnnlp", [])
        if isinstance(returnnlp, list) and len(returnnlp) > 0 and isinstance(returnnlp[0], list):
            returnnlp = returnnlp[0]
        elif not isinstance(returnnlp, list):
            returnnlp = []
        if None in returnnlp:
            LOGGER.warning("[BOOKCHAT] Returnnlp contains a None Value. {}".format(returnnlp))
        self.returnnlp_raw = [i for i in returnnlp if i is not None]
        self.returnnlp = ReturnNLP.from_list(self.returnnlp_raw)
        self.nlu_processor = NLU_Processor(self).build_nlu_handler(self.returnnlp, input_data.get("text"))
        LOGGER.info("Retrieve NLU Data Successful")

    @property
    def is_resume_mode(self):
        last_topic = self.module_selector.last_topic_module
        LOGGER.debug("Book last topic {}".format(last_topic))
        return last_topic != TopicModule.BOOK.value


    # get a response based on current state
    # by calling the corresponding callback functions
    def transduce(self, input_data, user_attributes):
        # extract features from input_data
        text_data = input_data.get("text", ["", ""])
        self.text = text_data[0]
        # self.returnnlp = ReturnNLP(input_data['returnnlp'][0])

        # if (input_data["features"][0]).get("coreference"):
        #     self.coref_text = input_data["features"][0]["coreference"].get("text", input_data["text"][0])
        # else:
        #     self.coref_text = self.text

        LOGGER.info("[BOOKCHAT AUTOMATON] For debugging purposes Session: {}".format(input_data.get("session_id")))

        self.dialog_act = input_data["features"][0]["dialog_act"] #TODO REFACTOR
        # self.amz_dialog_act = input_data["features"][0].get("amz_dialog_act")
        self.ner = input_data["features"][0]["ner"] #TODO REMOVE
        # self.asr_correction = input_data["features"][0]["asrcorrection"]
        # self.noun_phrase = input_data["features"][0]["noun_phrase"]
        self.knowledge = input_data["features"][0]["knowledge"] #TODO REMOVE
        # self.topic_keywords = input_data["features"][0]["topic_keywords"]
        # self.sentiment = input_data["features"][0]["sentiment"]
        self.central_elem = input_data["features"][0]["central_elem"] #TODO REMOVE
        # self.features = input_data["features"][0]


        self.user_attributes = input_data["bookchat_user"] # prev bookchat user attributes

        self.user_attributes_ref = user_attributes  # the same user attributes as the one local modules use
        self.module_selector = ModuleSelector(self.user_attributes_ref)

        self.context_manager = input_data["bookchat_context"] # prev cobot state info
        self.sys_resp_type = input_data["resp_type"][1]
        self.user_id = input_data["user_id"][0]
        # self.session_id = input_data["session_id"][0]
        self.current_loop = self.user_attributes.get("current_loop",{"turns_left":0})
        self.input_data = input_data
        
        # self.last_response = text_data[-1]
        self._get_text(input_data)

        self._retrieve_nlu(input_data)
        self.build_question_handler()
        self.get_acknowledgement_handler()
        self.context = ContextManager(self.user_id)
        self.logger.info("context.books: {}".format(self.context.books))
        self.logger.info("context.current_context: {}".format(self.context.current_context))
        self.prefix = ""
        # attributes to store in callback functions
        self.bookchat_user = {
            "not_first_time": True,
            "propose_continue": "CONTINUE",
            "candidate_module": None,
            "tags": [],
            "no_counter": input_data["bookchat_user"].get("no_counter", 0),
            "mentioned_books": {},
        } # info stored here will be stored in user attr
        self.bookchat_context = {
            "last_module": True
        } # info stored here will be stored in cobot current state
        self.response = "" # store response to return here
        self.t_context = {} # t states can temporarily store info here for s states to use

        self.switch_to_module = False #TODO, remove this later
        self.sys_intents = detect_sys_intent(self.question_detector, self.nlu_processor)
        self.bookchat_user["prev_state"] = self.user_attributes.get("prev_state", "")
        # set some attributes
        self.bookchat_user["current_state"] = self.user_attributes.get("current_state", "")

        # if a bookchat was not selected consecutively
        sys_continue_flow_types = {"hesitant", "incomplete", "clarify", "asr_error", "req_allow_ask_question", "req_allow_tell_sth"}
        if self.is_resume_mode and input_data["bookchat_user"].get("no_first_time", False): # if a bookchat was not selected consecutively
        # Entered Resume Mode
            if input_data["bookchat_user"].get("current_transition") == "t_chitchat":
                if "current_context" in self.context.chitchat and "interrupted" in self.context.chitchat["current_context"]:
                    self.context.chitchat["current_context"]["interrupted"] = True
            self.context.current_context = {
                "book_infos": [],
                "author_only": False,
                "book_name_query": []}            
            current_transition = "t_resume"
        else:
            current_transition = input_data["bookchat_user"]["current_transition"]
        self.add_no()
        if current_transition == "t_init":
            self.switch_to_module = True
        # system level intent overrides everything
        if self.sys_intents:
            if current_transition == "t_chitchat":
                if "current_context" in self.context.chitchat and "interrupted" in self.context.chitchat["current_context"]:
                    self.context.chitchat["current_context"]["interrupted"] = True
            current_transition = "t_sys_intent"

        if self.bookchat_user.get("no_counter") >= 3:
            current_transition = "t_ask_exit"

        flow_interruption_state = self.user_attributes.get("flow_interruption")
        if flow_interruption_state and current_transition != "t_sys_intent":
            current_transition = self.interruptions_mapping[flow_interruption_state](current_transition)
        self.bookchat_user["flow_interruption"] = False

        self.reset_flags(current_transition)
        self.current_t_transition = current_transition
        start = timer()
        self.logger.info("[book_automaton.transduce] Entering: {}".format(current_transition))
        next_state = self.transitions_mapping[current_transition]() # t state returns s state
        self.logger.info("[book_automaton.transduce] Exiting: {0}, Time elapsed: {1}".format(current_transition, timer() - start))
        start = timer()
        self.logger.info("[book_automaton.transduce] Entering: {}".format(next_state))
        next_transition = self.states_mapping[next_state]() # s state returns t state
        self.logger.info("[book_automaton.transduce] Exiting: {0}, Time elapsed: {1}".format(next_state, timer() - start))
        self.context.save_context()
        self.bookchat_user["prev_state"] = next_state
        self.bookchat_user["current_transition"] = next_transition # save transition for next user utterance
        self.bookchat_user["current_loop"] = self.current_loop
        self.logger.debug("Final Result")
        self.logger.debug(self.response)
        if self.prefix is None:
            self.prefix = ""
        elif len(self.prefix) == 0:
            self.prefix = self.ack_field_raw.get("ack", "")
        self.response = self.prefix + self.response
        if len(self.response) > 650: # hard filter for really long utterances
            self.logger.info("[BOOKCHAT_MODULE] Exception overwrote long response: {}".format(self.response))
            self.response = ""
        elif len(self.response) == 0:
            self.logger.info("[BOOKCHAT_MODULE] Checking if book resmpose is empty {}".format(self.response))
        self.module_selector.add_propose_keyword(self.bookchat_user.get("suggest_keywords", ""))

        return {
            "response": constructDynamicResponse(self.response),
            "bookchat_user": self.bookchat_user,
            "bookchat_context": self.bookchat_context,
        }

    # wrapper for Austin's template manager
    def utt(self, selector, slots={}):
        return template_books.utterance(selector=selector, slots=slots, user_attributes_ref=self.user_attributes_ref)

    def reset_flags(self, current_transition):
        # if current_transition != "t_book_chitchat":
        #     self.user_attributes["book_chitchat_last_idx"] = None
        #     self.bookchat_user["book_chitchat_last_idx"] = None
        if current_transition != "t_ask_book":
            self.user_attributes["ask_book_repeat"] = True
            self.bookchat_user["ask_book_repeat"] = True
        if current_transition != "t_subdialog" and self.user_attributes.get("current_subdialog") != "ChitchatDialog":
            self.user_attributes["current_subdialog"] = None
            self.user_attributes["subdialog_context"] = {}
            self.bookchat_user["current_subdialog"] = None
            self.bookchat_user["subdialog_context"] = {}
        if current_transition != "t_chitchat" and self.user_attributes.get("current_subdialog") != "BookDialog":
            self.user_attributes["current_subdialog"] = None
            self.user_attributes["subdialog_context"] = {}
            self.bookchat_user["current_subdialog"] = None
            self.bookchat_user["subdialog_context"] = {}

    #=========================================================
    # Special functions
    def try_book_dialog(self, strength=0):
        book_infos, final_book_query, author_only, raw_book_names = self.nlu_processor.detect_book(get_raw=True)
        LOGGER.debug("Entering Try Book Dialog {}".format(book_infos))

        propose_book = self.context.current_context.get("propose_book")
        propose_book_name = None
        if propose_book is not None:
            propose_book_name = propose_book.get("bookname")
        if propose_book_name and (propose_book_name.lower() in self.text.lower() or self.nlu_processor.user_ans_same):
            book_infos = [propose_book]
            author_only = False

        self.context.current_context["author_only"] = author_only
        # Handle special book context
        chitchat_context = self.context.current_context.get("chitchat_context", [])
        if book_infos and book_infos[0]["bookname"] == "special_bible_intent" and self.user_attributes.get("current_subdialog") == "ChitchatDialog" and "q2_question" in self.user_attributes.get("subdialog_context", {}).get("tags", {}):
            pass
        elif book_infos and book_infos[0]["bookname"] == "special_bible_intent":
            return self.handle_bible_intent()

        elif "book_not_read" in chitchat_context:
            #TODO: We should remove the book context from the chitchat
            # end_book_dialog
            self.prefix = " I assumed you red the book.  I must got a little carried away. "
            self.bookchat_user["current_subdialog"] = "ChitchatDialog"
            set_chitchat_dialog_init_context(self.context, self.user_attributes.get("subdialog_context"))
            self.subdialog = ChitchatDialog(self)
            self.subdialog.set_chitchat_context()
            return "s_chitchat"
        elif book_infos and len(book_infos) > 0:
            self.logger.info(f"[try_book_dialog] book infos: {book_infos}")
            no_prefix = False
            segments = self.nlu_processor.get_pos_of_word_seg([b.get("bookname") for b in book_infos])
            if len(book_infos) == 1:
                book_info = book_infos[0]
                i = segments.get(book_info.get("bookname", ""), -1)
                if self.nlu_processor.user_dislike(seg=i):
                    self.logger.debug("[BOOKCHAT AUTOMATON] User Doesn't like {}".format(book_info))
                    self.prefix = self.ack_field_raw.get("ack", "That's understandable. ")
                    return None
                current_book_id = self.context.current_context.get("book_info", {}).get("bookid")
                if str(book_info["bookid"]) == str(current_book_id):
                    return None
                if propose_book_name:
                    if propose_book_name.lower() in self.text.lower() or self.nlu_processor.user_ans_same:
                        self.prefix = self.utt(["acknowledge_same_book"])
                        no_prefix = True
                    else:
                        return None
                if author_only:
                    self.context.current_context = {
                        "book_infos": book_infos,
                        "author_only": author_only,
                        "book_name_query": final_book_query}
                    return "s_ask_book"

                return self.start_book_dialog(book_info, no_prefix=no_prefix)
            book_infos = book_infos[:3]
            
            self.context.current_context = {
                "book_infos": book_infos,
                "author_only": author_only,
                "book_name_query": final_book_query}
            self.current_loop = {"turns_left": len(book_infos)}
            return "s_book_grounding"
        elif propose_book is not None and self.nlu_processor.user_said_yes:
            LOGGER.debug("USER SAID YES TO OUR PROPOSE BOOK {}".format(propose_book))
            return self.start_book_dialog(propose_book)

        elif self.nlu_processor.user_have_read:
            self.prefix = self.utt(["no_book_info"], {"bookname": "that book"})
            return "s_echo"

        return None

    def start_book_dialog(self, book_info, author_entry=False, no_prefix=False):
        LOGGER.debug("initiate_book_dialog:".format(book_info["bookname"]))
        self.bookchat_user["current_subdialog"] = "BookDialog"
        self.user_attributes["current_subdialog"] = "BookDialog"
        set_book_dialog_init_context(self.context, book_info)
        self.subdialog = BookDialog(self, no_prefix)
        self.subdialog.set_chitchat_context()
        return "s_subdialog"

    def end_of_subdialog(self):
        current_subdialog = self.user_attributes["current_subdialog"]
        if current_subdialog == "BookDialog":
            return self.end_book_dialog()

    def end_book_dialog(self):
        # self.prefix = "I don't have more to say about {}. ".format(self.context.current_context["book_info"]["bookname"])
        LOGGER.debug("initiate_chitchat_dialog {}".format(self.context))
        self.bookchat_user["current_subdialog"] = "ChitchatDialog"
        set_chitchat_dialog_init_context(self.context, self.user_attributes.get("subdialog_context"))
        self.subdialog = ChitchatDialog(self)
        self.subdialog.set_chitchat_context()
        return self.s_chitchat()

    def start_chitchat_dialog(self):
        LOGGER.debug("initiate_chitchat_dialog")
        self.bookchat_user["current_subdialog"] = "ChitchatDialog"
        set_chitchat_dialog_init_context(self.context, self.user_attributes.get("subdialog_context"))
        self.subdialog = ChitchatDialog(self)
        self.subdialog.set_chitchat_context()
        return "s_chitchat"

    def end_chitchat_dialog(self):
        self.context.current_context = {}
        return self.s_ask_book()

    def handle_bible_intent(self):
        idx = self.user_attributes.get("bible_idx", 0)
        self.t_context["next_transition"] = "t_handle_bible_intent"
        quotes = utterance(["bible_quotes"], True)
        self.response = quotes[idx] + utterance(["bible_ask_continue"])
        self.bookchat_user["bible_idx"] = idx + 1
        if self.bookchat_user["bible_idx"] >= len(quotes):
            self.bookchat_user["bible_idx"] = 0
        return "s_echo"

    # def handle_potential_question_old(self, resume_state=None, transition=None):
    #     if resume_state is None:
    #         resume_state = self.current_t_transition
    #     self.qa_bot = QABot(self)
    #     res = self.qa_bot.handle_potential_question(resume_state, transition)
    #     if res: return res

    #     return None

    def handle_potential_question(self, resume_state=None, transition=None):
        self.qa_bot = QABot(self)
        res = self.qa_bot.get_answer_only(transition)
        if res is not None and self.current_t_transition == "t_ask_book":
            self.prefix = res + "<break time='0.15s'/> "
            return "s_ask_book"
        if res is not None:
            self.prefix = res + "<break time='0.15s'/> "
        return None

    def handle_potential_command(self, resume_state=None, enable=True):
        if not enable:
            return None
        if resume_state is None:
            resume_state = self.current_t_transition
        self.command_bot = BookCommandDetector(self)
        res = self.command_bot.handle_potential_command(resume_state)
        if res: return res

        return None

    #=========================================================
    # Callback functions
    def t_sys_intent(self):
        # Question Detector and Handler Before SysIntent
        # TODO: what to do if there is more than one sys intent?
        if len(self.sys_intents) > 1:
            pass
        if self.sys_intents[0] == "exit_bookchat":
            return "s_ask_exit"

        if self.sys_intents[0] == "exit_bookchat_corona":
            return "s_ask_exit"

        elif self.sys_intents[0] == "question_handler":
            # Enter QA Here TODO
            self.qa_bot = QABot(self)
            res = self.qa_bot.get_answer_only(enable_none=True)
            if res:
                self.response = res
            else:
                self.response = self.question_handler.generate_i_dont_know_response()
            
            self.response += "do you want to talk about a specific book? "
            self.t_context["next_transition"] = "t_ask_book"
            return "s_echo"
            # preprocessed_response = self.question_handler.handle_question()
            # if preprocessed_response.tag != QuestionResponseTag.IDK.value:
            #     self.response = preprocessed_response.response
            #     return "s_echo"
        elif self.sys_intents[0] == "which_book":
            bookname = self.context.current_context.get("book_info", {}).get("bookname")
            if bookname:
                self.response = "I believe we are talking about {bookname}. ".format(bookname=bookname)
            else:
                self.response = " I don't remember.  My memory has been affected since I have to talk with so many people these days. "
            return "s_echo"
        elif self.sys_intents[0] == "fav_books":
            self.qa_bot = QABot(self)
            res = self.qa_bot.get_answer_only(enable_none=True)
            if res:
                self.response = res + "<break time='0.5s'/> How about you? What book do you like? "
            else:
                self.response = "I love The Great Gatsby. It's really thought provoking, and I can see why some people call it the great American novel.<break time='0.5s'/> How about you? What book do you like? "
            self.t_context["next_transition"] = "t_ask_book"
            return "s_echo"
        elif self.sys_intents[0] == "ask_recommend":
            self.qa_bot = QABot(self)
            res = self.qa_bot.get_answer_only(enable_none=True)
            if res:
                self.response = res + "What book do you want to talk more about? "
            else:
                self.response = "Here's what I would recommend: <break time='0.5s'/> Harry Potter, and A Game of Thrones are some books that I really enjoyed recently. What book do you want to talk more about? "
            self.t_context["next_transition"] = "t_ask_book"
            return "s_echo"
        elif self.sys_intents[0] == "know_books":
            self.prefix = "I know a lot of books already. "
            return "s_ask_book"
        elif self.sys_intents[0] == "change_book":
            self.prefix = "Sure! We can talk about another book. "
            return "s_ask_book"
        elif self.sys_intents[0] == "guess_book":
            self.t_context["next_transition"] = "t_ask_book"
            self.response = "Hahaha, I wouldn't have any idea. What's the book? "
            return "s_echo"

        return "s_ask_exit" # should not reach here

    def t_ask_exit(self):
        return "s_ask_exit"

    @property
    def detect_coronavirus(self):
        return "exit_bookchat_corona" in self.sys_intents

    def s_ask_exit(self):
        # Add a special case for corona_exit
        self.bookchat_user["current_state"] = self.get_caller_function_name()

        self.bookchat_user["ask_book_repeat"] = False

        if self.detect_coronavirus:
            self.bookchat_user["propose_continue"] = "UNCLEAR"
            self.bookchat_user["suggest_keywords"] = "corona virus"
            self.module_selector.propose_topic = TopicModule.NEWS

            self.response = " Are you interested in news regarding corona virus? "
            return "t_resume"

        if "propose_topic" in self.t_context:
            self.response = self.t_context["propose_topic"]            

        if "propose_continue" in self.t_context:
            self.bookchat_user["propose_continue"] = self.t_context["propose_continue"]
        else:
            self.bookchat_user["propose_continue"] = "STOP"
            self.response = " That's okay. "
        return "t_resume"

    def _set_transition_state(self, _exit=False, propose_topic=None, _unclear=False):
        if propose_topic is not None:
            self.bookchat_user["propose_topic"] = propose_topic
        if _unclear:
            self.bookchat_user["propose_continue"] = "UNCLEAR"
        elif _exit:
            self.bookchat_user["propose_continue"] = "STOP"
        else:
            self.bookchat_user["propose_continue"] = "CONTINUE"
        

    def s_echo(self):
        self.bookchat_user["current_state"] = self.get_caller_function_name()

        next_transition = self.t_context.get("next_transition", "t_resume")
        return next_transition

    def t_resume(self):
        self.bookchat_user["ask_book_repeat"] = False
        self.context.current_context["author_only"] = False
        if len(self.text.strip().split()) >= 5:
            res = self.try_book_dialog(strength=2)
        else:
            res = self.try_book_dialog(strength=1)
        if res: return res

        res = self.handle_potential_question()
        if res: return res

        res = self.handle_potential_command()
        if res: return res

        return "s_ask_book"

    def t_handle_bible_intent(self):
        if self.nlu_processor.user_said_yes:
            return self.handle_bible_intent()

        res = self.try_book_dialog()
        if res: return res

        res = self.handle_potential_question("t_resume")
        if res: return res

        res = self.handle_potential_command("t_resume")
        if res: return res

        return "s_ask_book"

    def t_init(self):
        # assumptions about how bookchat is usually triggered initially
        # (1) Topic selection: e.g. Let's talk about books
        # (2) Question: e.g. What are you reading
        # (3) Statement: e.g. My favorite book is harry potter

        # (3) Statement; for now just send to intro anyways

        self.user_attributes["ask_book_repeat"] = False
        self.bookchat_user["ask_book_repeat"] = False
        
        res = self.try_book_dialog(strength=2)
        if res: return res

        res = self.handle_potential_question(transition="init")
        if res: return res

        res = self.handle_potential_command()
        if res: return res



        # (1) Topic selection
        return "s_intro"

    def s_intro(self):
        self.bookchat_user["current_state"] = self.get_caller_function_name()

        if self.prefix == "":
            self.response = self.utt(["init", "one_turn_intro"])
        else:
            self.response = self.utt(["init", "one_turn_intro_short"])
        return "t_ask_book"

    def add_no(self):
        if self.text == "no":
            self.bookchat_user["no_counter"] = self.bookchat_user.get("no_counter", 0) + 1
        else:
            self.bookchat_user["no_counter"] = 0

    '''
    def t_intro(self):
        # handles 3 cases for user response to intro statement
        # (1) Question: e.g. What are you reading?
        # (2) Statement: e.g. My favorite book is harry potter
        # (3) Other: e.g. Ok/Cool/I love books too i.e. something where we can lead the dialogue

        # (2) Statement
        res = self.try_book_dialog()
        if res: return res

        res = self.handle_potential_question()
        if res: return res

        res = self.handle_potential_command()
        if res: return res

        # (3) Other
        self.response = utterance(["init", "continuation"])
        self.t_context["next_transition"] = "t_ask_book"
        return "s_echo"
    '''

    def s_ask_book(self):
        prev_state = self.bookchat_user.get("current_state")
        self.bookchat_user["current_state"] = self.get_caller_function_name()
        author_only = self.context.current_context.get("author_only", False)
        # Adding a side line for the book you like to read
        if prev_state == "s_intro" and self.nlu_processor.user_said_yes and not author_only:
            # If genre is detected
            detected_genre = detect_genre(self.text)
            self.logger.debug("[BOOKCHAT] Detected Genre: {}".format(detected_genre))

            if len(detected_genre) > 0:
                utt = self.utt(["ask_book_recommend_with_genre"], {"genre":detected_genre[0]})
            else:
                utt = self.utt(["ask_book_recommend"])
            # No genre is detected
            self.response = utt
            self.prefix = ""
            return "t_ask_book"
        
        if author_only:
            book_infos = self.context.current_context.get("book_infos", [])
            self.logger.debug("[BOOKCHAT UTILS: Ask BOOK] Entered Author Only {}".format(book_infos))
            if len(book_infos) > 0:
                book_info = book_infos[0]
                LOGGER.debug("Book infos {}".format(book_info))
                book_name = book_info.get("bookname")
                author_name = book_info.get("bookauthor")
                self.context.current_context["author_only"] = False
                if book_name and author_name:
                    context = {"author": author_name, "book_name": book_name}
                    self.response = self.utt(["ask_book_by_author"], context)
                    self.context.current_context["propose_book"] = book_info
                    return "t_ask_book"
        ask_book_repeat = self.bookchat_user.get("ask_book_repeat", False)
        if ask_book_repeat:
            self.logger.debug("[BOOKCHAT Ask Book] ask_book_repeat {}".format(ask_book_repeat))
            self.bookchat_user["ask_book_repeat"] = False
            return "t_ask_book"
            
            # I really like {} too.  My favorite book by {} is {}.  What's your favorite {} book?
        
        idx = self.user_attributes.get("book_q_idx", 1)
        data = template_data.data["books"]["ask_book"]
        num_questions =len(data)
        # Debug idx, num_question_etc
        self.logger.debug("[BOOKCHAT UTILS: Ask BOOK] {}".format(data))
        if idx >= num_questions or "no_ask_book_questions_left" in self.user_attributes:
            if "no_ask_book_questions_left" not in self.user_attributes: # first time reaching the end
                self.bookchat_user["no_ask_book_questions_left"] = True
                self.prefix = ""
                self.response = "So. Umm. we have talked about books a lot. Let's talk about something else! "
                self.bookchat_user["propose_continue"] = "STOP"
                return "t_resume"
            self.response = self.utt(["ask_book_default"])
            return "t_ask_book"
        
        ask_book = "(Anyways|By the way), " + data["q{}".format(idx)][0]["entry"]
        self.bookchat_user["book_q_idx"] = idx + 1
        self.logger.debug("[BOOKCHAT UTILS: Ask BOOK] Ask Book Reply {}".format(ask_book))
        # say_interest_book = utterance(["say_interest_book"])
        self.response = ask_book
        return "t_ask_book"

    def t_ask_book(self):
        # I haven't been reading
        LOGGER.debug("Testing Haven't been Reading")
        if self.bookchat_user["current_state"] == "s_intro" and re.search(r"\bno\b|\bi\b (have|did)(n't| not)\b|\bnope\b|\bnot (really|read)\b|(do(n't| not)|not).* (talk|chat|discuss).* (it|that|book)", self.text):
            if re.search(r"have you|(what|how) about you", self.text):
                self.prefix = "Actually, I am wondering what to read. "
            else:
                self.prefix = "So you haven't. Maybe you've been busy. Um, "
            self.bookchat_user["ask_book_repeat"] = False
            return self.start_chitchat_dialog()


        LOGGER.debug("Testing user_dislike")
        # Don't like book
        if self.nlu_processor.ans_no("user_dislike"):
            self.bookchat_user["ask_book_repeat"] = True
            self.prefix = "That's okay! "
            return self.start_chitchat_dialog()

        # Don't remember book
        LOGGER.debug("Testing user_ans_unknown")
        if self.nlu_processor.user_ans_unknown:
            self.bookchat_user["ask_book_repeat"] = True
            self.prefix = "It's okay if you're not sure! "
            return self.start_chitchat_dialog()

        LOGGER.debug("Testing misheard")
        if self.nlu_processor.misheard:
            self.bookchat_user["ask_book_repeat"] = True
            self.prefix = "My apologies. what are some other books you have read recently?"
            return "s_ask_book"

        LOGGER.debug("User states don't talk about that book.")
        if self.user_attributes.get("ask_book_repeat", True) == True:
            if re.search(r"(do(n't| not)|not).* (talk|chat|discuss|like).* (it|that|book)", self.text):
                self.prefix = "Okay, we can discuss another book. "
                self.bookchat_user["ask_book_repeat"] = True
                return "s_ask_book"

        LOGGER.debug("User states not_book_regex")
        if self.user_attributes.get("ask_book_repeat", True) == False:
            if re.search(not_book_regex, self.text):
                self.bookchat_user["ask_book_repeat"] = True
                self.prefix = "Oh, you weren't talking about a specific book. Silly me. "
                return self.start_chitchat_dialog()

        # when user answers something like "no", "yes", "okay", "no no", ask them to repeat once
        # Detect bookname here:
        # 1. text length is short
        # 2. all word in the list
        # This came from we asked the user the book name, user respond bookname
        LOGGER.debug("Text not detected.")
        LOGGER.debug(self.bookchat_user)
        if len(self.text.strip().split()) <= 3 and all(word in title_filter_list for word in self.text.strip().split()) and self.bookchat_user["current_state"] != "s_intro":
            propose_book = self.context.current_context.get("propose_book")
            LOGGER.debug("Under Short Response propose_book {}".format(propose_book))
            if propose_book is not None and self.nlu_processor.ans_no(r"don't have"):
                self.context.current_context["propose_book"] = None
                self.context.current_context["ask_book_repeat"] = False
                return self.start_chitchat_dialog()
            if self.user_attributes.get("ask_book_repeat", True):
                LOGGER.debug("Entered Ask Book Repeat")
                self.bookchat_user["ask_book_repeat"] = False
                self.t_context["next_transition"] = "t_ask_book"
                self.response = self.utt(["confirm_book_name"])
                return "s_echo"
            # yes to have you read any good books recently?
            if self.bookchat_user["current_state"] == "s_intro" and self.nlu_processor.user_said_yes:
                self.prefix = self.utt(["ask_user_read_good_book"])
                LOGGER.debug("Entry from S Intro")
                return "s_ask_book"

            if self.bookchat_user["prev_state"] == "s_chitchat" and self.nlu_processor.user_said_yes and self.bookchat_user.get("q2_transition", False):
                LOGGER.debug("Entry from q2")
                self.bookchat_user["q2_transition"] = False
                self.response = self.utt(["chitchat_ask_book"])
                self.t_context["next_transition"] = "t_ask_book"
                self.bookchat_user["ask_book_repeat"] = True
                return "s_echo"
            return self.start_chitchat_dialog()

        res = self.try_book_dialog(strength=2)
        if res: return res

        res = self.handle_potential_question("t_resume")
        if res: return res

        res = self.handle_potential_command("t_resume")
        if res: return res

        propose_book = self.context.current_context.get("propose_book")
        LOGGER.debug("Propose Book {0}, user don't have {1}".format(propose_book, self.text))
        if propose_book is not None and self.nlu_processor.ans_no("user_dont_have"):
            LOGGER.debug("User don't have a book")
            self.context.current_context["propose_book"] = None
            self.context.current_context["ask_book_repeat"] = False
            return self.start_chitchat_dialog()

        # yes to have you read any good books recently?
        if self.bookchat_user["current_state"] == "s_intro" and self.nlu_processor.user_said_yes:
            self.prefix = self.utt(["ask_user_read_good_book"])
            LOGGER.debug("Entry from S Intro")
            return "s_ask_book"


        if self.user_attributes.get("ask_book_repeat", True):
            self.bookchat_user["ask_book_repeat"] = False
            self.t_context["next_transition"] = "t_ask_book"
            self.response = self.utt(["confirm_book_name"])
            return "s_echo"
        LOGGER.debug("Tags {}".format(self.context.current_context))
        tags = self.context.current_context.get("tags", [])
        chitchat_context = self.context.current_context.get("chitchat_context", [])
        if "c_no_questions_left" in chitchat_context or "c_no_questions_left" in tags:
            return "s_ask_exit"
        # self.bookchat_user["ask_book_repeat"] = True
        self.prefix = "Ah, I don't know any books by that name. "
        return self.start_chitchat_dialog()

    # gives up to 3 interesting comments before breaking the loop
    def s_interesting_comment(self):
        self.bookchat_user["current_state"] = self.get_caller_function_name()

        # give comment from reddit, ask what do you think
        idx = self.current_loop["idx"]
        recognition = ""
        user_redis_data = get_redis_with_userid(USER_PREFIX, self.user_id)
        if idx == 0:
            recognition = utterance(["know_book"]).format(book_title=user_redis_data["current_book_info"]["bookname_full"])
        say_fact = utterance(["say_fact"]).format(fact=user_redis_data["comments"][idx])
        ask_interest = utterance(["ask_interest"])
        self.current_loop["idx"] = idx + 1
        self.current_loop["turns_left"] = self.current_loop["turns_left"] - 1
        self.response = recognition + say_fact + ask_interest
        return "t_interesting_comment"

    def t_interesting_comment(self):
        if re.search(r"where.* you.* (hear|get.* info)|fake|(not|doubt|don't|is that).* true|\blie|(don't|do not).* believe|(that|it).* (stupid|dumb)", self.text):
            self.response = "I learned that from reddit. Maybe it's not true. ",
            self.t_context["next_transition"] = "t_interesting_comment"
            return "s_echo"
        elif re.search(r"(not|wasn't).* about.* (book|novel|poem|comic|story)|(wrong|incorrect|irrelevant|not.* related).* (info|fact)", self.text):
            self.t_context["from_interesting_comment"] = True
            self.t_context["ack_mistake"] = "Oops, looks like I goofed up. "
            self.current_loop = {"turns_left": 2}
            return "s_book_chitchat"
        elif re.search(wrong_book_regex, self.text):
            self.prefix = "Ops, let me ask you something else! "
            return self.start_chitchat_dialog()
        LOGGER.debug("Enter Try Book Dialog From Interesting Comment")
        res = self.try_book_dialog()
        if res: return res

        res = self.handle_potential_question()
        if res: return res

        res = self.handle_potential_command()
        if res: return res

        if self.current_loop["turns_left"]<=0:
            return self.start_chitchat_dialog()

        return "s_interesting_comment"

    def s_book_grounding(self):
        self.bookchat_user["current_state"] = self.get_caller_function_name()
        mentioned_books = self.bookchat_user.get("mentioned_books", {}).get("user_mentioned", [])
        book_infos = self.context.current_context["book_infos"]
        book_name_query = self.context.current_context["book_name_query"]
        author_only = self.context.current_context.get("author_only", False)
        book_infos = [book for book in book_infos if book.get("bookname") not in mentioned_books]
        LOGGER.debug("Turns Left {}".format(self.current_loop["turns_left"]))
        idx = len(book_infos) - self.current_loop["turns_left"]
        book_info = book_infos[idx]
        self.logger.debug("Book Infos {0}, Index {1}".format(book_infos, idx))
        self.response = ""


        try:
            params = {
                "book_title": book_info["bookname"],
                "author_name": book_info["bookauthor"],
            }
            if author_only:
                key = "ground_book_author"
            else:
                key = "ground_book"
            book_title = book_info.get("bookname", None)
            if book_title:
                mentioned_books.append(book_title)
            self.bookchat_user["mentioned_books"] = mentioned_books
            # Add Acknowledgement to Yes/No Question First.  (Then ground the book with name)
            self.response = self.acknowledgement_handler.add_acknowledgement(self.response, key="s_book_grounding", args={})
            if idx == 0 and len(book_infos) > 1 and not author_only: # Beginnging talking about the book, multiple info with book
            

                self.response += self.utt(
                    ["ground_book_intro"], {"num_books": len(book_infos)}) + self.utt(["ground_book"], params)
            elif idx == 0: # Beginnging talking about the book
                self.response += self.utt([key], params)
            else:
                self.response += self.utt([key], params)
        except BaseException as _e:
            params = {"book_title": book_info["bookname"]}
            if idx == 0 and len(book_infos) > 1:
                self.response += self.utt(
                    ["ground_book_intro"], {"num_books": len(book_infos)}) + self.utt(["ground_book_alt"], params)
            elif idx == 0:
                self.response += self.utt(["ground_book_alt"], params)
            else:
                self.response += self.utt(["ground_book_alt"], params)
            self.logger.error("[BOOKCHAT] Book Grounding Erro::  {}".format(_e), exc_info=True)
        self.current_loop["turns_left"] -= 1
        return "t_book_grounding"

    def t_book_grounding(self):
        if self.nlu_processor.ans_no("no_book_grounding"):
            self.prefix = "Ah, my mistake. Let me ask you something else. "

            return self.start_chitchat_dialog()

        elif self.nlu_processor.ans_yes("yes_book_grounding"):
            book_infos = self.context.current_context["book_infos"]
            idx = len(book_infos) - self.current_loop["turns_left"] - 1
            book_info = book_infos[idx]
            #facts = get_imdb_trivia_new(book_info["book_id"], limit=self.cfgs.MOVIE_FACTS_TURNS_MAX)
            # if facts == None:
            #    facts = get_imdb_trivia(book_info["book_id"], limit=self.cfgs.MOVIE_FACTS_TURNS_MAX)
            # return self.start_book_dialog_old(book_info, facts)
            return self.start_book_dialog(book_info)
        # elif self.nlu_processor.ans_tell_me_more:
        #     # Handle when user want to ask information about books
        #     self.prefix = " I am not good at describing books.  Probably because my creator failed all non science classes in school. "
        #     return "s_ask_book" #TODO Improve this flow
        LOGGER.debug("Enter Try Book Dialog From Book Grounding")
        res = self.try_book_dialog()
        if res: return res

        res = self.handle_potential_question()
        if res: return res

        res = self.handle_potential_command()
        if res: return res

        if self.current_loop["turns_left"] <= 0:
            self.prefix = "Ah, I don't think I know the book you're talking about. "
            return self.start_chitchat_dialog()

        return "s_book_grounding"

    # when entering chitchat mode, pass in the number of chitchat turns, and a returning point
    # store chitchat info for future use
    def s_chitchat(self):
        LOGGER.debug("Chitchat Current Context {}".format(self.context.current_context))
        self.bookchat_user["current_state"] = self.get_caller_function_name()
        LOGGER.debug("Chitchat Subdialog Context {}".format(self.user_attributes.get("subdialog_context")))
        output = self.subdialog.get_response(self.input_data)
        self.prefix = None # We customize blender inside chitchat

        # Potential Followup
        followup = self.subdialog.map_to_to_book_dialog(output.get("entity"))
        self.bookchat_user["subdialog_context"] = {
            "current_state": self.context.current_context["current_state"],
            "old_state": self.context.current_context["old_state"],
            "transition_history": self.context.current_context["transition_history"],
            "tags": self.context.current_context["tags"],
            "q_key": self.context.current_context.get("q_key"),
        }
        if followup:
            f, next_transition = followup
            self.bookchat_user["subdialog_context"]["chitchat_dialog_activated"] = False
            self.bookchat_user["subdialog_context"]["q_key"] = self.context.current_context.get("q_key")
            self.context.current_context = {}
            if next_transition == "start_book_dialog":
                self.prefix = output["response"]
                book_infos, final_book_query, author_only, raw_book_names = self.nlu_processor.detect_book(get_raw=True)
                book_name = output.get("entity")
                # Here book infos should be book entities
                infos = [book_info for book_info in book_infos if book_info.get("bookname", "") == book_name]
                LOGGER.debug("Book Infos {}".format(infos))
                if len(infos) > 0:
                    self.start_book_dialog(infos[0], no_prefix=True)
                    return self.s_subdialog()
            else:
                self.response = output["response"] + f
                return next_transition
        


        self.logger.debug("Chitchat Enter Output {} ".format(output))

        # after acking the previous question but jump out into different dialog flow
        if output["exit_code"] == "s_exit":
        #     return self.end_chitchat_dialog()
            if output["next_t_transition"] != "t_book_grounding":
                self.prefix = output["response"]
                return self.end_chitchat_dialog()
            self.response = output["response"]
            self.context.current_context = {}
            return output["next_t_transition"]
        # after entering chitchat after questions are already exhausted
        elif self.context.current_context["current_state"] == "s_exit":
            self.bookchat_user["q2_transition"] =  self.context.current_context.get("q2_transition", False)
            self.response = output["response"]
            return self.end_chitchat_dialog()
        self.response = output["response"]
        return output["next_t_transition"]

    def t_chitchat(self):
        self.subdialog = ChitchatDialog(self)
        self.subdialog.set_chitchat_context()
        self.context.current_context["interrupted"] = True
        

        if self.subdialog.detect_chitchat_only_response():
            self.context.current_context["interrupted"] = False
            return "s_chitchat"
        # save_chitchat_dialog_current_context(self.context)
        LOGGER.debug("Enter Try Book Dialog From Chitchat")
        res = self.try_book_dialog()
        if res: return res

        res = self.handle_potential_question()
        if res: return res

        res = self.handle_potential_command()
        if res: return res

        self.context.current_context["interrupted"] = False
        self.context.chitchat = {}
        return "s_chitchat"

    def s_book_chitchat(self):
        self.bookchat_user["current_state"] = self.get_caller_function_name()

        self.current_loop["turns_left"] -= 1
        last_idx = self.user_attributes.get("book_chitchat_last_idx")
        questions = utterance(["book_chitchat_question"], True)
        prefix = ""
        if self.t_context.get("from_interesting_comment"): # if wrong book detected e.g. from say_interesting_comment
            prefix = self.t_context.get("ack_mistake", "")+utterance(["say_interest_unknown_book"])
        elif not last_idx:
            if "prefix" in self.current_loop:
                prefix = self.current_loop["prefix"]+utterance(["say_interest_unknown_book"])
            else:
                prefix = utterance(["not_know_book"])+utterance(["say_interest_unknown_book"])
        else:
            prefix = utterance(["short_response"])
            if last_idx < len(questions):
                questions.pop(last_idx)
        q_inds = range(len(questions))
        idx = random.choice(q_inds)
        self.bookchat_user["book_chitchat_last_idx"] = idx
        self.response = prefix + questions[idx]
        return "t_book_chitchat"

    def t_book_chitchat(self):
        if re.search(wrong_book_regex, self.text):
            return self.start_chitchat_dialog()
        LOGGER.debug("Enter Try Book Dialog From t_book_chitchat")
        res = self.try_book_dialog()
        if res: return res

        res = self.handle_potential_question()
        if res: return res

        res = self.handle_potential_command()
        if res: return res

        if self.current_loop["turns_left"]<=0:
            return self.start_chitchat_dialog()

        return "s_book_chitchat"

    def s_subdialog(self):
        self.bookchat_user["current_state"] = self.get_caller_function_name()
        self.prefix = None # Customize Blender inside of the dialog
        self.response = self.subdialog.get_response(self.input_data)
        self.bookchat_user["subdialog_context"] = {
            "current_state": self.context.current_context["current_state"],
            "old_state": self.context.current_context["old_state"],
            "transition_history": self.context.current_context["transition_history"],
            "tags": self.context.current_context["tags"],
        }
        #self.bookchat_user["tags"].append("")

        if self.context.current_context["current_state"] == "s_exit":
            return self.end_of_subdialog()

        return "t_subdialog"

    def t_subdialog(self):
        if self.user_attributes["current_subdialog"] == "BookDialog":
            self.subdialog = BookDialog(self)
            self.subdialog.set_chitchat_context()

        if re.search(wrong_book_regex, self.text) and "q1_question" in self.user_attributes.get("subdialog_context", {}).get("tags", {}):
            self.prefix = "Oops, my bad. Let's talk about another book. "
            return "s_ask_book"
        elif re.search(dont_talk_book, self.text) and self.user_attributes["current_subdialog"] == "BookDialog":
            self.prefix = "Okay. Let's talk about another book. "
            return "s_ask_book"
        if self.subdialog.detect_chitchat_only_response():
            self.context.current_context["interrupted"] = False
            return "s_subdialog"


        res = self.try_book_dialog()
        if res: return res

        res = self.handle_potential_question()
        if res: return res

        res = self.handle_potential_command()
        if res: return res

        if self.is_resume_mode:
            return "s_ask_book"

        return "s_subdialog"

    def i_ask_continue_book(self, current_transition):
        if self.nlu_processor.user_ans_neg:
            return "t_ask_exit"

        return current_transition

    def get_caller_function_name(self):
        return inspect.stack()[1][3]

