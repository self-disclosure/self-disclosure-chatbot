import re
from fuzzywuzzy import fuzz

from bookchat_detection_utils import *


def get_ans_func(id):
    id = str(id)
    '''
    mapping = {
        "1": "1",
        "2": "1"
    }
    if id in mapping:
        id = mapping[id]
    '''
    method_name = "chitchat_{}_ans".format(id)
    possibles = globals().copy()
    possibles.update(locals())
    method = possibles.get(method_name)
    return method

