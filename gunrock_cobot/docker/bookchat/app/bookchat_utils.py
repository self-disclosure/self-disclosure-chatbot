import json
import re
import random
import logging
import hashlib
import traceback
import signal
from threading import Thread
import requests
import praw
import redis
from fuzzywuzzy import fuzz
from num2words import num2words
from collections import OrderedDict
from new_bookchat_utils.logging_utils import set_logger_level, get_working_environment

from goodreads_utils import GoodreadsClient
from cobot_common.service_client import get_client
from typing import Union, List, Dict, Tuple
from nlg_post_process.profanity_classifier import BotResponseProfanityClassifier
from error_handling_utils import trace_latency

LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())


USER_PREFIX = "gunrock:module:bookchat:user_id"
BOOK_ID_PREFIX = "gunrock:module:bookchat:book_id"
BOOK_NAME_PREFIX = "gunrock:module:bookchat:book_name_2"
COBOT_API_KEY='xgTjk23SRM9Q9VFrUEFOjav0Bn4ot21W8uFLEieT'
GOODREADS_CLIENT_KEY = "Vf18CgXKdwVz8pzh9nMDg"
GOODREADS_CLIENT_SECRET = "Q4esRZJEy2KGNNsZM9s5UR0G6ne1tLZnwhtKavPJlYE"
GOODREADS_CLIENT_KEYS = ["NSBwbOa42jsk2l9m9xWRw", "Vf18CgXKdwVz8pzh9nMDg", "OwXNTUG9U00OJiiZMofp5w"]
GOODREADS_CLIENT_SECRETS = ["XVTgCDNQUdGyOl7eWeSMr9qs7kbsdGL45SmpSRLi5A", "Q4esRZJEy2KGNNsZM9s5UR0G6ne1tLZnwhtKavPJlYE", "QrDsQBaJM1gy0KFwDxCzPiIj3Snen0C5RwFzSjoY"]
REDDIT_CLIENT_ID = "lKmLeMmSSLns6A"
REDDIT_CLIENT_SECRET = "Fi6V_pCzKqE-4TRVqcmpMJdET2s"
RETRIEVAL_LIST_KEY = "gunrock:ic:retrieval:apikeys"
NER_URL = "http://52.20.135.52:5015/book_NER"
title_filter_list = {"yes", "no", "okay", "well", "what", "that's"}
BOOK_BACKSTORY_FIRST_LEVEL = 0.9

# beat her - pride and prejudice filter
blacklist = [r"\bass", r"\basshole", r"\banal\b", r"\banus", r"\barsehole", r"\barse\b", r"\bbitch", r"\bbangbros", r"\bbastard"
                 r"\btit(s|ties|)\b", r"\bbutt(s|)\b", r"\bblow( |)job\b", r"\bboob", r"\bbra(s|)\b", r"\bcock(s|)\b", r"\bcum\b", r"\bcunt(s|)\b", r"\bdick(s|)\b", r"\bshit(s|)\b", r"\bcocaine\b",
                 r"\bsex", "erotic", "fuck", "fxxk", r"f\*\*k", r"f\*\*c", r"f\*ck", r"fcuk", r"f\*\*\*", "fcking", "fck", "fucken", "fucked", "fucks", "gang bang", "gangbang",
                 "genital", "damn", "damnit", "horny", "jerk off", "jerkoff", r"\bkill", "loser", "masterbate", "masterbates", "masterbated", "naked",
                 "nasty", "negro", "nigger", "nigga", "orgy", "pussy", "penis", "perve", r"\brape", "fucked",
                 "fucking", "trump", "racist", "sexy", "strip club", "vagina", "faggot", "fag", "drug", "weed", "marijuana", "cannabis", "lsd",
                 "murder", "incel", "prostitute", "slut", "whore", "hooker", "cuck", "cuckold", "shooting", "nazis",
                 "nazism", "nazi", "sgw", r"\bballs", "thot", "virgin", "virginity", "cumshot", "cumming", "dildo", "vibrator", "sexual",
                 "douche", "douche", "bukkake", "douchebag", r"\bporn", "porngraph", "porngraphy", "died", "killed",
                 "killing", "murdered", "murdering", "murders", r"\bclit", "clitoris", "masterbate tatoo", "cheat",
                 "cheated", "cheating", "dammit", "toxicity", "toxic", "wtf", "breasts", "boobies", r"\bdead", "sperm",
                 "death", "pornhub", "intercourse", "pornography", "porno", "boobs", "penisis", "my husband", "my boyfriend",
                 "my girlfriend", "my wife", "end my life", "reddit", "reddit,", "reddit:", "subreddit", "tifu", "post raw", "show discussion thread",
                 "penises", "dildos", "vaginas", "school shooting", "breasts", "showerthoughts", "todayilearned", "mushroom tatoo", r"commit.* suicide"]  # add "\r" after testing


# Redis python client
r = redis.StrictRedis(host='52.87.136.90', port=16517, db=0, password="alexaprize", charset='utf-8',decode_responses=True)

def get_retrieval_keys():
    return r.brpoplpush(RETRIEVAL_LIST_KEY, RETRIEVAL_LIST_KEY)

def detect_sys_intent(question_detector, nlu_processor):
    sys_intents = []
    text = nlu_processor.user_utterance
    if nlu_processor.command_exit_book_module:
        sys_intents.append("exit_bookchat")

    elif re.search(r"which book", text):
        sys_intents.append("which_book") #temporary addition to followup questions in book

    elif nlu_processor.detect_corona:
        sys_intents.append("exit_bookchat_corona")

    elif re.search(r"tell me.* news|play.* music", text):
        sys_intents.append("exit_bookchat")
    elif nlu_processor.user_ask_recommend:
        sys_intents.append("ask_recommend")
    elif question_detector is not None and question_detector.backstory_high_confidence(BOOK_BACKSTORY_FIRST_LEVEL):
        # If the question is a backstory question
        sys_intents.append("question_handler")
    elif re.search(r"(what|tell me).* your favorite \b(?!part\b).*?\b book|(what|tell me).* book you (like|enjoy)", text):
        sys_intents.append("fav_books")

    elif re.search(r"book.* you.* (know|talk|chat)|(what|tell me).* book.* you.* read", text):
        sys_intents.append("know_books")
    elif re.search(r"(talk|chat) about.* (another|different) book|change book", text):
        sys_intents.append("change_book")
    elif re.search(r"guess.* my.* book|(want|can).* you.* guess", text):
        sys_intents.append("guess_book")
    LOGGER.debug("Sys_intents: {}".format(sys_intents))
    return sys_intents

class Timeout():
    """Timeout class using ALARM signal."""
    class Timeout(Exception):
        pass

    def __init__(self, sec):
        self.sec = sec

    def __enter__(self):
        signal.signal(signal.SIGALRM, self.raise_timeout)
        signal.alarm(self.sec)

    def __exit__(self, *args):
        signal.alarm(0)    # disable alarm

    def raise_timeout(self, *args):
        raise Timeout.Timeout()


def detect_book_name(automaton):
    text = automaton.text
    last_response = automaton.last_response
    if no_book_name_included(text):
        return [], None, False, []

    if match_special_case_for_bible(text):
        results = [
            {
            "bookname": "special_bible_intent",
            "bookname_full": "special_bible_intent",
            "bookid": 0,
            "bookauthor": "",
            "bookauthorid": 0
            }
        ]
        return results, "bible", False, []

    book_infos = extracted_book_names_from_new_ner(last_response, text)
    LOGGER.debug("[BOOKCHAT] Result from ner: {}".format(book_infos))
    processed_book_name_results, author_only, raw_book_names = post_process_book_ner(book_infos)

    # temporarily just to make the code compatible, as old version required a query name here
    if processed_book_name_results:
        book_name_query = processed_book_name_results[0].get("bookname", "")
    else:
        book_name_query = ""

    # Special case for raw_book_names
    if len(raw_book_names) > 0 and len(processed_book_name_results) > 0:
        rbn = raw_book_names[0]
        pbnr = processed_book_name_results[0]
        author_only = rbn == pbnr.get("bookauthor", "").lower()

    return processed_book_name_results, book_name_query, author_only, raw_book_names

def match_special_case_for_bible(text):
    match = re.search(r"bible|scripture|(old|new).* testament|genesis", text)
    LOGGER.debug("[BOOKCHAT] match_special_case_for_bible {}".format(match))
    return match is not None


@trace_latency
def extracted_book_names_from_new_ner(last_bot_response, user_utterance):
    if last_bot_response is None:
        last_bot_response = ""
    # input to ner_server
    input_ = {
        "last_bot_response": last_bot_response,
        "user_utterance": user_utterance
    }
    headers = {
            'Content-Type': 'application/json'
    }
    query_json = json.dumps(input_)
    LOGGER.debug("[BOOKCHAT] Input to NER Server {}".format(query_json))
    try:
        response = requests.post(url=NER_URL,
                                 headers=headers,
                                 data=query_json,
                                 timeout=(0.3, 0.5)).json()
        LOGGER.debug("[BOOKCHAT] Successful extraction from new NER {}".format(response), exc_info=True)
    except Exception as _e:
        LOGGER.debug("[BOOKCHAT] Extract from new NER {}".format(user_utterance), exc_info=True)
        return {}

    # Process response, get a list of booknames
    return response

def _convert_ordered_dict_to_val(ordered_dict):
    LOGGER.debug(ordered_dict)
    if isinstance(ordered_dict, OrderedDict):
        t = ordered_dict.get("@type")
        val = ordered_dict.get("#text")
        if t is None or val is None:
            return None
        if t == "integer":
            return int(val)
    return ordered_dict

def post_process_book_ner(book_ner_result):
    books = book_ner_result.get("books", [])
    people = book_ner_result.get("people", [])
    book_name_results = []
    books_raw = book_ner_result.get("books_raw", [])
    author_only = True
    if len(books) > 0:
        author_only = False
        for entry in books:
            if entry["type"] == "book":
                if is_not_good_book_entry(entry):
                    continue
                book_info = convert_ner_server_format_to_fsm_format(entry)
                book_name_results.append(book_info)
            if entry["type"] == "bookSeries":
                # pick the best book from good reads
                book_info = get_book_series_from_entry(entry)
                book_name_results.append(book_info)

    # If books is None, but detected author, get the most popular book.
    elif len(people) > 0:
        if len(people) > 1:
            logging.warning("[BOOKCHAT] Detected more than One Author {}".format(people))
        author = people[0]
        book_info = get_book_series_from_author(author)
        if book_info is not None:
            book_name_results.append(book_info)
    return book_name_results, author_only, books_raw

def get_book_series_from_author(entry):
    client = GoodreadsClient()
    LOGGER.debug("[BOOKCHAT] Author Entry: {}".format(entry))
    author_id = entry.get("author_id")
    author = entry.get("author")
    books = client.search_books_with_author(author_id)
    num_results = len(books)
    if num_results > 0:
        best_book = books[0]
        book_title = _convert_ordered_dict_to_val(best_book.get("title"))
        # clean book title
        book_title = ''.join([i for i in book_title if i.isalpha() or i == " "])
        goodread_id = _convert_ordered_dict_to_val(best_book.get("id"))
        if best_book is not None and book_title is not None:
            description = best_book.get("description")

            book_info = {
                "bookname": book_title,
                "bookname_full": book_title,
                "bookid": goodread_id,
                "bookauthor": author,
                "bookauthorid": author_id,
                "description": description
            }
            LOGGER.debug("[BOOKCHAT] Book Info: {}".format(book_info))

            return book_info
    return None

def get_book_series_from_entry(entry):
    client = GoodreadsClient()
    LOGGER.debug("[BOOKCHAT] Series Entry: {}".format(entry))
    series_id = entry.get("series_id")
    books = client.search_books_in_series(series_id)
    LOGGER.debug("[BOOKCHAT] Book Series: {}".format(books))

    most_popular_book = max(books, key=lambda x: x.get("rating_count", 0))
    book_title = most_popular_book.get("title", "").replace(":", "")

    book_info = {
        "bookname": book_title,
        "bookname_full": book_title,
        "bookid": most_popular_book.get("goodread_id", None),
        "bookauthor": most_popular_book.get("author", ""),
        "bookauthorid": most_popular_book.get("author_id", None),
        "description": most_popular_book.get("description", None)
    }
    LOGGER.debug("[BOOKCHAT] Book Info: {}".format(book_info))

    return book_info

# for result from our own ner server
def is_not_good_book_entry(entry):
    if BotResponseProfanityClassifier.is_profane(entry.get("title", "")):
        return True
    return False

def convert_ner_server_format_to_fsm_format(book_from_ner):
    book_title = book_from_ner.get("title","").replace(":", "")

    movie_info = {
        "bookname": book_title,
        "bookname_full": book_title,
        "bookid": book_from_ner.get("goodread_id", None),
        "bookauthor": book_from_ner.get("author", ""),
        "bookauthorid": book_from_ner.get("author_id", None)
    }

    return movie_info

def no_book_name_included(text):
    regex_list = [
        r"^(yes|yeah)$",
        r"^nothing$",
        r"read and",
        r"read( book(s)?)?$",
        r"good question$",
        r"^((the | this |that |about |a |)(book(s|)|serie(s|)|trilogy)|the|this|that('s|)|about|a|new(s|)|yes|no|nope|okay|sure|(i |)love|i think|well|what('s|)|it('s|)|tv|i|(i |)read|read|(the book |)before|something|anymore|anything|thank you|a lot|(a |)name|talk|chat|him)$"
    ]

    for regex in regex_list:
        match = re.search(regex, text)
        if match:
            return True
    return False

@trace_latency
def retrieve_reddit(utterance, subreddits, limit):
    REDDIT_SEARCH_PREFIX = "gunrock:module:moviechat:reddit_search_prefix" # share with movie
    utterance = utterance.lower()
    subreddits.sort()
    subreddits = '+'.join(subreddits)
    key = utterance + "_" + subreddits + "_" + str(limit)
    LOGGER.debug("[retrieve_reddit] key {}".format(key))
    cache = tmdb_get_redis(REDDIT_SEARCH_PREFIX, key)

    if cache is not None:
        LOGGER.debug("[retrieve_reddit] cache hit")
        return cache
    LOGGER.debug("[retrieve_reddit] cache miss")

    keys = get_retrieval_keys()
    c_id, c_secret, u_agent = keys.split("::")

    reddit = praw.Reddit(client_id=c_id,
                         client_secret=c_secret,
                         user_agent=u_agent,
                         timeout=2
                         #username='gunrock_cobot', # not necessary for read-only
                         #password='ucdavis123'
                        )
    subreddit = reddit.subreddit(subreddits)
    results = list(subreddit.search(utterance, limit=limit)) # higher limit is slower; delete limit arg for top 100 results
    value = parse_retrieve_reddit_results(results)
    if len(value)==0:
        value = None
    # currently use expire period of three months; don't expire until after
    # finals
    tmdb_set_redis(REDDIT_SEARCH_PREFIX, key, value)
    return value

def parse_retrieve_reddit_results(results):
    output = []
    for res in results:
        data = {
            "title": res.title,
            "subreddit": res.subreddit.display_name,
            "score": res.score,
            "num_comments": res.num_comments,
            "created_utc": res.created_utc,
        }
        output.append(data)
    return output

# note: order of keywords when searching does affect results
# each keywords(s) in keywords must appear in the result, not necessarily in the same order
def get_interesting_reddit_posts(keywords, limit):
    try:
        with Timeout(1):
            num_query = 5
            text = " ".join(keywords)
            submissions = retrieve_reddit(text, ['todayilearned'], num_query)
            if submissions is not None:
                results = []
                LOGGER.info("[get_interesting_reddit_posts] number of facts retrieved - initial {}".format(len(submissions)))
                # highest score posts first
                submissions.sort(reverse=True, key=lambda x: x["score"])
                for submission in submissions:
                    # title post processing
                    title = submission["title"].replace("TIL", "").strip()
                    if title[-1]!='.' and title[-1]!='!' and title[-1]!='?':
                        title += "."
                    # filters
                    title_lower = title.lower()
                    if submission["score"] < 100:
                        LOGGER.debug("[get_interesting_reddit_posts] filtered post by score")
                    elif len(title) > 300:
                        LOGGER.debug("[get_interesting_reddit_posts] filtered post by length")
                    elif any(not re.search(r"\b"+keyword.lower()+r"\b", title_lower) for keyword in keywords):
                        LOGGER.debug("[get_interesting_reddit_posts] filtered post by keywords")
                    elif any(re.search(x, title_lower) for x in blacklist):
                        LOGGER.debug("[get_interesting_reddit_posts] filtered post by blacklist")
                    elif any(fuzz.token_set_ratio(title_lower, res.lower()) > 70 for res in results):
                        LOGGER.debug("[get_interesting_reddit_posts] filtered post by duplicate")
                    else:
                        results.append(title)
                #Sensitive Words Pattern
                # sort based on score
                LOGGER.info("[get_interesting_reddit_posts] number of facts retrieved - final {}".format(len(results)))
                if len(results) > 0:
                    return results[:limit]
            else:
                LOGGER.info("[get_interesting_reddit_posts] retrieve_reddit returned None")
                return None
    except Timeout.Timeout:
        LOGGER.info("[BOOKCHAT_MODULE] Timeout in get_interesting_reddit_posts {}".format(traceback.format_exc()))
        return None
    except Exception as e:
        LOGGER.info("[BOOKCHAT_MODULE] Exception in get_interesting_reddit_posts {}".format(traceback.format_exc()))
        return None

def get_reddit_with_book_info(book_info, limit):
    # use book name, author, or series name
    keywords = [book_info["bookname"].strip()]
    if len(keywords[0].split()) == 1:
        keywords.append(book_info["bookauthor"].strip())

    facts = get_interesting_reddit_posts(keywords, limit)
    return facts

# def detect_book_and_get_facts(text, ner, knowledge, strength, limit):
#     try:
#         with Timeout(1):
#             book_info = detect_book_name(text, ner, knowledge, strength=strength)
#             posts = None
#             if book_info != None:
#                 #LOGGER.debug("book_info", book_info)
#                 if book_info["bookname"] == "special_bible_intent":
#                     return book_info, None
#                 query_string = book_info["bookname"].strip()
#                 if len(query_string.split()) == 1:
#                     query_string = query_string + " " + book_info["bookauthor"]
#                 #LOGGER.debug("query_string", query_string)
#                 posts = get_interesting_reddit_posts(book_info["bookid"], query_string, limit)
#             return book_info, posts
#     except Timeout.Timeout:
#         LOGGER.debug("[BOOKCHAT_MODULE] Timeout in detect_book_and_get_facts {}".format(traceback.format_exc()))
#         return None, None
#     except Exception as e:
#         LOGGER.debug("[BOOKCHAT_MODULE] Exception in detect_book_and_get_facts {}".format(traceback.format_exc()))
#         return None, None


def get_similar_books_with_bookid(bookid):
    client = GoodreadsClient()
    results = client.request_book_with_id(bookid)
    book = Book(results)
    return book.similarBooks

def get_same_author_books_with_authorid(authorid):
    client = GoodreadsClient()
    results = client.request_author_with_id(authorid)
    author = Author(results)
    return author.books




def detect_intent(text):
    """
    Detect intents
    TODO: instead of if elif hierarchy, try all regex and add to dictionary successful matches
    """
    # System level: when this is returned to state response function, start new dialogue flow
    if re.search(r"(give.* (book|me).* (recommendation|suggestion)|(suggest|recommend).* me.* book|you.* (suggest|recommend)|(^what|tell me).* (good|popular).* book)", text):
        return "ask_recommend"
    elif re.search(r"(stop|don't|do not).* (talk|discuss|tell.* me).* book|\bi\b (hate|don't|do not|never).* (read|like.* book|know any book)|(talk|discuss|tell.* me).* about.* something else|i hate book(s|)", text):
        return "exit_bookRG"
    elif re.search(r"(talk|chat).* about|have you read|favorite book", text):
        return "req_topic"

    # Lexical level: narrow to broad
    elif re.search(r"yes|yeah|sure|\bi\b (\bdo\b|\bam\b|\bwant to\b)", text):
        return "answer_yes"
    elif re.search(r"\bno\b|\bnope\b|(don't|do not) (like|enjoy|want)", text):
        return "answer_no"
    elif re.search(r"(\bi\b|\bi'm\b).* (not.* sure|unsure|uncertain|(don't|do not).* know|(don't|do not).* have)", text): # asking question at beginning of utterance
        return "answer_uncertain"
    elif re.search(r"^who\b|^what\b|^when\b|^when\b|^where\b|^why\b|^how\b", text): # asking question at beginning of utterance
        return "ask_question"
    elif re.search(r"(^who\b|^what\b|^when\b|^where\b|^why\b|^which\b|^how\b|can|know|curious|interest|hear|tell|give|let|do).* you.* (think|feel|like|love|thought|support|hate|believe|opinion|stance|view|say|know|favorite).*", text):
        return "ask_opinion"

# reference
'''
def detect_intent_book(text):
    """
    Detect more granular intent for the book module if necessary
    Note that this is to complement the intents passed from the system
    """
    if re.search("different book", text):
        return "change_book"
    elif re.search(r"music|turn|\bplay\b|connect|direct |^open|directory", text):
        return "req_others"
    elif re.search("repeat|again", text):
        return "repeat"
    elif re.search("talk.*about|tell me.*about|chat about|tell me a |recommend|let'?s do|let'?s talk|how about book|^books$", text):
        return "open"
    elif re.search("your favorite|what.* do you like|what's your|do you have", text):
        return "backstory"
    elif re.search("what do you think|how do you think|what do you like about|what do you love about|what makes you|how do you feel|^how is |^how was|how do you like|why do you like|what kind of| your opinion|your view|your thought|what do you know|who do you think|do you believe|what.* best|who.* best|which.* best|which \w+ are you|what.*it like", text):
        return "ask_opinion"
    elif re.search("how do you (?!like)|find|can you", text):
        return "ask_ability"  # intents already exist from the system/lexical level intents
    elif re.search(r"what.* good|what's on|what is on|\bnow\b|popular|coming out|new books|playing|recent book|in.* theater|what do you know|what book is|what's in|what's the book|called|the name of|what.*genre|\btop\b|rated|\bupcoming\b|\bdate\b|released|\bfact\b|who.* (character|actor|actress|\bcast\b|director)|what.* (character|actor|actress|\bcast\b|director)|what.*plot|tell me.* plot|plot mean|what.*story line|the book.* about|how many|what.* books", text):
        return "ask_info"
    elif re.search("^do you know|have you seen|have you watched", text):
        return "ask_view"
    elif re.search("^would you like|do you like|is it|watch|chill|go to|weekend|oscar", text):
        return "chitchat"
    else:
        return "others"
    return None
'''

def handle_sys_intent(input, intent):
    if intent=="exit_bookRG":
        return {
            "next": "s_exitbookRG",
            "response": "Ok. If you want to talk about something else like books, say let's talk about books.",
            "context": input["currentstate"],
            "syscode": "exit" # cobot can potentially use this to overwrite reponse or whatever
        }
    elif intent=="ask_recommend":
        return {
            # TODO: should make handle_sys_intent a method of BookAutomaton so I can call it's response functions
            "next": "s_startKG",
            "response": "Check out Energy and Civilization: A History, by Vaclav Smil. Smil is one of my favorite authors, and this is his masterpiece. He lays out how our need for energy has shaped human history from the era of donkey-powered mills to today’s quest for renewable energy.",
        }
    return None


''' Don't use for now; not a big issue and increases latency
def detect_profanity(utterances):
    client = get_client(api_key=COBOT_API_KEY)
    r = client.batch_detect_profanity(utterances=utterances)
    classified_offensive = [result["values"][0]["offensivenessClass"] for result in r["offensivenessClasses"]]
    return classified_offensive
'''

def constructDynamicResponse(candidateResponses):
    if type(candidateResponses)==list:
        selectedResponse = random.choice(candidateResponses)
    else:
        selectedResponse = candidateResponses
    finalString = ""
    choicesBuffer = ""
    flag = False
    for char in selectedResponse:
        if char == "(":
            flag = True
        elif char == ")":
            flag = False
            choices = choicesBuffer.split("|")
            finalString += random.choice(choices)
            choicesBuffer = ""
        if flag == True and char != "(" and char != ")":
            choicesBuffer += char
        elif flag == False and char != "(" and char != ")":
            finalString += char
    finalString = " ".join(finalString.split())
    return finalString

def utterance(key: List[str], get_all: bool=False):
    """
    :param key: a list of str that represents the nested dict keys.
                e.g. {key1: {key2: {key3: text}}}, you do ['key1', 'key2', 'key3']
    :param get_all:
    :return: either a list, or an instance of, the utterance, and the hash of the utterance
    terry - not using hash right now because my template format has some qa pairs that don't work with hash
    """
    # return "what what", "hash"
    try:
        from bookchat_templates_data import data
        data = data[key[0]]
        for i in key[1:]:
            data = data[i]
        data = [d['text'] for d in data]

        if get_all:
            return data  # type: List[Tuple[str, str]]
        else:
            return random.choice(data)  # type: Tuple[str, str]

    except Exception as e:
        # Changed to the empty string to let the system catch the error
        return ""



def set_redis_with_userid(prefix, userid, output):
    LOGGER.debug(
        '[REDIS] set redis with expire input: {} output: {}'.format(
            userid, output))
    hinput = hashlib.md5(userid.encode()).hexdigest()
    return r.set(prefix + ':' + hinput, json.dumps(output))

def get_redis_with_userid(prefix, userid):
    LOGGER.debug('[REDIS] get redis input: {}'.format(userid))
    hinput = hashlib.md5(userid.encode()).hexdigest()
    results = r.get(prefix + ':' + hinput)
    LOGGER.debug('[REDIS] get redis output: {}'.format(results))
    if results is None:
        return None
    return json.loads(results)

def tmdb_get_redis(prefix, key):
    hinput = hashlib.md5(key.encode()).hexdigest()
    value = r.get(prefix + ':' + hinput)
    #LOGGER.debug('[REDIS] tmdb_get_redis - key: {}, value: {}'.format(key, value))
    if value is None:
        return None
    return json.loads(value)

# for caching api calls
def tmdb_set_redis(prefix, key, value, expire=90 * 24 * 60 * 60):
    #LOGGER.debug('[REDIS] tmdb_set_redis - key: {}, value: {}, expire {}'.format(key, value, expire))
    hinput = hashlib.md5(key.encode()).hexdigest()
    return r.set(prefix + ':' + hinput, json.dumps(value))
