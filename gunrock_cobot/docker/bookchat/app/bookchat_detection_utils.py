import re
import logging
from nlu.dataclass.returnnlp import ReturnNLP, ReturnNLPSegment
from typing import List, Optional
from bookchat_utils import detect_book_name
from new_bookchat_utils.logging_utils import set_logger_level, get_working_environment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

# Detect book
def detect_entity_1(automaton):
    try:
        entity_names = []
        result, final_book_query, author_only, _ = detect_book_name(automaton)

        if type(result) == list:
            result = result[0]

        if type(result) == dict:
            entity_names.append(result["bookname"])

        LOGGER.debug(" BOOKCHAHT DETECTION entity_names_1 {}".format(entity_names))
        if len(entity_names) == 0:
            return None
        return entity_names
    except:
        LOGGER.debug("[BOOKCHAT_MODULE] Exception in detect_entity_1")
        return None

def detect_author(automaton)->List:
    try:
        entity_names = []
        result, final_book_query, author_only, _ = detect_book_name(automaton)
        LOGGER.debug(" BOOKCHAHT DETECTION Detect Author {}".format(result))
        if result and len(result) > 0:
            return [r.get("bookauthor") for r in result if r.get("bookauthor") is not None]
        else:

            return []
    except:
        return []

# # Detect authors
# def old_detect_author(google_kg, concept: ReturnNLPSegment.Concept=None , ner=None):
#     try:
#         entity_names = []
#         for entry in google_kg:
#             if re.search("author|writer|novelist|poet", entry[1].lower()):
#                 entity_names.append(entry[0])

#         if len(entity_names) == 0:
#             return None
#         return entity_names
#     except:
#         LOGGER.debug("[BOOKCHAT_MODULE] Exception in detect_entity_2")
#         return []

all_genres = {
    "action",
    "adventure",
    "animation",
    "biography",
    "classics",
    "comedy",
    "comics",
    "crime",
    "documentary",
    "drama",
    "family",
    "fantasy",
    "fable",
    "fiction",
    "nonfiction",
    "graphic novels",
    "history",
    "horror",
    "manga",
    "music",
    "mystery",
    "philosophy",
    "poetry",
    "romance",
    "self help",
    "science fiction",
    "thriller",
    "war",
    "western",
    "young adult",
}


# Detect genres
def detect_entity_3(automaton):
    text = automaton.text
    try:
        entity_names = []
        for genre in all_genres:
            if re.search(genre, text):
                entity_names.append(genre)
        genre = detect_genre(automaton)
        LOGGER.info("[BOOKCHAT_MODULE] Entitty Names 4: {0} Genre {1}".format(entity_names, genre))
        if len(entity_names) == 0:
            return None
        return entity_names
    except:
        LOGGER.debug("[BOOKCHAT_MODULE] Exception in detect_entity_3")
        return None

def detect_genre(automaton):
    try:
        text = automaton.text
        detected_genres = []
        entities = automaton.nlu_processor.get_entities()
        for genre in all_genres:
            if re.search(genre, text):
                detected_genres.append(genre)
        return detected_genres
    except:
        return []

