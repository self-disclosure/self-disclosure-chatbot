import logging
import re
import traceback
from transitions import Machine
from bookchat_utils import *
from new_bookchat_utils.logging_utils import set_logger_level, get_working_environment
from nlu.dataclass.returnnlp import ReturnNLP, ReturnNLPSegment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())


SIZE_TRANSITION_HISTORY = 5

def set_book_dialog_init_context(context, book_info):
    author_only = context.current_context.get("author_only", False)
    context.current_context = {
        "current_state": "s_init",
        "questions_asked": 0,
        "trivia_used": 0,
        "book_info": book_info,
        "curr_q_num": None,
        "q_key": "q",
        "transition_history": ["r_t0"] * SIZE_TRANSITION_HISTORY,
        "chitchat_mode": "qa_mode",
        "chitchat_context": [],
        "tags": [],
        "author_only":author_only

    }

def save_book_dialog_current_context(context, book_info):
    context.books[str(book_info["bookid"])] = {
        "current_context": context.current_context,
    }

class BookDialog(Machine):
    # High level specification of states, state transitions and transition conditions
    def __init__(self, automaton, no_prefix=False):
        # Each state has prefix 's_'
        self.states = [
            's_init', # main entry point
            #'s_trivia',
            's_chitchat',
            #'s_ask_interest',
            #'s_ask_share',
            's_exit',
        ]
        # Each state has a corresponding transtion function that chooses the next state depending on certain conditions
        # There are different ways to define transitions. I'm defining arguments using a list.
        # Args ordering: transition_name, source, dest, conditions=None, unless=None, before=None, after=None, prepare=None
        self.transitions = [
            # system
            #['transduce', 's_init', 's_exit', ['c_resuming_dialog', 'c_no_questions_left', 'c_no_trivia_left', 'r_t11']],
            ['transduce', '*', 's_exit', ['c_no_questions_left', 'c_no_trivia_left', 'r_t1']],
            # s_init
            #['transduce', 's_init', 's_trivia', ['c_resuming_dialog', 'r_t2'], ['c_no_trivia_left']],
            #['transduce', 's_init', 's_chitchat', ['c_resuming_dialog', 'r_t3']],
            ['transduce', 's_init', 's_chitchat', ['r_t2']], # first time entry point
            # s_chitchat
            #['transduce', 's_chitchat', 's_trivia', ['c_switch_mode', 'r_t5'], ['c_no_trivia_left']],
            ['transduce', 's_chitchat', '=', ['r_t3']],
            #['transduce', 's_chitchat', 's_ask_share', ['c_no_questions_left', 'r_t3']],
            #['transduce', 's_chitchat', 's_ask_interest', ['c_possibly_bored', 'r_t4']],
            # s_trivia
            #['transduce', 's_trivia', 's_chitchat', ['c_switch_mode', 'r_t7'], ['c_no_chitchat_left']],
            #['transduce', 's_trivia', '=', ['r_t8'], ['c_no_trivia_left']],
            # s_ask_interest
            #['transduce', 's_ask_interest', 's_exit', ['c_not_interested', 'r_t6']],
            #['transduce', 's_ask_interest', 's_ask_share', ['c_no_questions_left' , 'r_t7']],
            #['transduce', 's_ask_interest', 's_chitchat', ['r_t8']],
            # s_ask_share
            #['transduce', 's_ask_share', 's_exit', ['c_nothing_to_share', 'r_t9']],
            #['transduce', 's_ask_share', '=', ['r_t10']],
        ]

        self.automaton = automaton
        self.text = automaton.text
        self.utt = automaton.utt
        self.context = automaton.context
        self.book_info = self.context.current_context["book_info"]
        self.tags = []
        self.no_prefix = no_prefix # First turn no prefix
        # resume
        if str(self.book_info["bookid"]) in self.context.books and self.context.current_context["current_state"] == "s_init" and "current_context" in self.context.books[str(self.book_info["bookid"])]:
            #LOGGER.debug("self.context.books", self.context.books)
            self.tags.append("c_resuming_dialog")
            self.context.current_context = self.context.books[str(self.book_info["bookid"])]["current_context"]
        self.current_context = self.context.current_context

        self.facts = get_reddit_with_book_info(self.book_info, 1)

        # set context info at init
        if self.current_context["current_state"] == "s_init":
            if self.facts is not None:
                self.current_context["num_total_trivia"] = len(self.facts)
            else:
                self.current_context["num_total_trivia"] = 0

        if self.current_context["questions_asked"] >= 4:
            self.tags.append("c_no_questions_left")
        if self.current_context["trivia_used"] >= self.current_context["num_total_trivia"]:
            self.tags.append("c_no_trivia_left")

        LOGGER.debug("No Prefix {}".format(self.no_prefix))

        Machine.__init__(self, states=self.states, transitions=self.transitions, initial=self.current_context["current_state"],
                         prepare_event='prepare', finalize_event='finalize', send_event=True)


        #self.add_transition('init', 'solid', 'liquid')

    def get_response(self, input_data, last_response=""):
        LOGGER.debug("current_context:", self.current_context)
        self.text = input_data["text"][0]
        self.dialog_act = input_data["features"][0]["dialog_act"]
        # self.amz_dialog_act = input_data["features"][0].get("amz_dialog_act")
        self.ner = input_data["features"][0]["ner"]
        # self.asr_correction = input_data["features"][0]["asrcorrection"]
        self.knowledge = input_data["features"][0]["knowledge"]
        # self.topic_keywords = input_data["features"][0]["topic_keywords"]
        # self.sentiment = input_data["features"][0]["sentiment"]
        self.features = input_data["features"][0]
        self.input_data = input_data
        self.old_state = self.state
        self.cache = {} # for caching conditions results if necessary
        self.r_ack = ""
        self.r_body = ""
        self.r_question = ""
        self.transition_history = self.current_context["transition_history"]

        self.transduce()

        self.new_state = self.state
        self.current_context["current_state"] = self.state
        self.current_context["old_state"] = self.old_state
        self.current_context["transition_history"] = self.transition_history
        self.current_context["tags"] = self.tags
        save_book_dialog_current_context(self.context, self.book_info)
        self.final_response = self.r_ack + self.r_body + self.r_question
        # LOGGER.debug("current_context:", self.current_context)
        return self.final_response

    def detect_chitchat_only_response(self):
        if self.current_context.get("curr_q_num") == "q1":
            return True
        return False
    # on_enter states: Most of the logic for each state to generate the response is defined here
    # Every state has a corresponding callback here
    #=======================================================
    def on_enter_s_chitchat(self, event):
        # LOGGER.debug("We've just entered state s_chitchat")

        self.set_ack()
        self.set_body()
        self.set_next_question()

    def on_enter_s_exit(self, event):
        # LOGGER.debug("We've just entered state s_exit")
        # default response; should be overwritten upon returning
        self.r_question = "I was thinking about what book to read and got distracted. Can you remind me what we were talking about? "

    '''
    def on_enter_s_ask_interest(self, event):
        # LOGGER.debug("We've just entered state s_ask_interest")
        self.r_ack = "Gives me something to think about! "
        self.r_question = "Do you still want to talk about " + self.book_info["bookname"] + "?"

    def on_enter_s_ask_share(self, event):
        # LOGGER.debug("We've just entered state s_ask_share")
        self.r_question = "Can you share more of your thoughts on this book? "
    '''
    def set_chitchat_context(self):
        if "chitchat_context" not in self.current_context: # may not exist in old data format
            self.current_context["chitchat_context"] = []

        q_key = self.current_context.get("q_key", "q")
        if re.search(r"(didn't|did not|never|haven't|have not|yet to).* (read).* (\bit\b|\bone\b|book|yet)", self.text) and q_key != "q_not_read":
            self.current_context["chitchat_context"].append("book_not_read")

    def set_ack(self):
        
        author_only = self.current_context.get("author_only", False)
        logging.debug("[BOOKCHAT_DIALOG] {}".format(self.current_context))
        if self.current_context["chitchat_mode"] == "trivia_mode":
            self.r_ack = "" # rely on system ack for now
        elif self.current_context["chitchat_mode"] == "qa_mode" and self.current_context["curr_q_num"] is not None:
            ack_bot = BookAcknowledgeBot(self)
            self.tags.append(self.current_context["curr_q_num"] + "_ack")
            self.r_ack = ack_bot.get_ack(self.current_context["curr_q_num"])
            # overwrite if interrupted by question
        if "book_user_qa_answer_only" in self.automaton.bookchat_user["tags"]:
            self.r_ack = "So. About {}. ".format(self.book_info["bookname"])
        if "c_resuming_dialog" in self.tags:
            self.tags.append("resume_ack")
            self.r_ack = self.utt(["book_dialog", "resume_book_ack"], {"book_title": self.book_info["bookname_full"]})
            #self.r_ack = "I think we were talking about {} before, but we didn't discuss this last time. ".format(self.book_info["bookname"])
        elif self.old_state == "s_init" and not self.no_prefix:
            if author_only:
                self.r_ack = self.utt(["book_dialog", "init_book_ack_author_only"], {"book_title": self.book_info["bookname_full"], "book_author":self.book_info["bookauthor"]})
            else:
                self.r_ack = self.utt(["book_dialog", "init_book_ack"], {"book_title": self.book_info["bookname_full"]})
            #self.r_ack = "I've seen {} before! ".format(self.current_context["book_info"]["bookname"])

    def set_body(self):
        '''
        if self.current_context["questions_asked"] % 2 == 0 and self.current_context["questions_asked"] != 0 and self.current_context["chitchat_mode"] == "qa_mode" and "c_no_trivia_left" not in self.tags:
            self.current_context["chitchat_mode"] = "trivia_mode"
        elif self.current_context["trivia_used"] % 2 == 0 and self.current_context["trivia_used"] != 0 and self.current_context["chitchat_mode"] == "trivia_mode" and "c_no_questions_left" not in self.tags:
            self.current_context["chitchat_mode"] = "qa_mode"
        '''

        if self.current_context["questions_asked"] != 0 and self.current_context["chitchat_mode"] == "qa_mode" and "c_no_trivia_left" not in self.tags:
            self.current_context["chitchat_mode"] = "trivia_mode"
        elif self.current_context["trivia_used"] != 0 and self.current_context["chitchat_mode"] == "trivia_mode" and "c_no_questions_left" not in self.tags:
            self.current_context["chitchat_mode"] = "qa_mode"

        # LOGGER.debug("body chitchat_mode:", self.current_context["chitchat_mode"])

        if self.current_context["chitchat_mode"] == "trivia_mode":
            self.r_body = self.utt(["book_dialog", "trivia_body"], {"trivia": self.facts[self.current_context["trivia_used"]]})
            self.current_context["trivia_used"] += 1

    def set_next_question(self):
        if self.current_context["chitchat_mode"] == "qa_mode":
            if self.current_context["curr_q_num"] is None:
                self.current_context["curr_q_num"] = "q1"
            else:
                self.current_context["curr_q_num"] = "q" + str(int(self.current_context["curr_q_num"][-1]) + 1)

            self.tags.append(self.current_context["curr_q_num"] + "_question")
            # overwrite in BookAcknowledgeBot
            try:
                q_key = self.current_context.get("q_key", "q")
                self.tags.append("q_key_" + q_key)
                if self.current_context.get("qa_template_params") is not None:
                    self.r_question = self.utt(["book_dialog", "book_question_bank", self.current_context["curr_q_num"], q_key], self.current_context["qa_template_params"])
                    self.current_context["qa_template_params"] = None
                else:
                    self.r_question = self.utt(["book_dialog", "book_question_bank", self.current_context["curr_q_num"], q_key])
                self.set_opinion_based_on_question()
            except:
                q_key = "q"
                self.current_context["q_key"] = q_key
                self.tags.append("q_key_" + q_key)
                self.r_question = self.utt(["book_dialog", "book_question_bank", self.current_context["curr_q_num"], q_key])
                self.set_opinion_based_on_question()
                # LOGGER.debug("[BOOKCHAT_MODULE] Exception in set_next_question {}".format(traceback.format_exc()))
            self.current_context["questions_asked"] += 1
        elif self.current_context["chitchat_mode"] == "trivia_mode":
            self.r_question = self.utt(["book_dialog", "trivia_question"])

    def set_opinion_based_on_question(self):
        self.automaton.set_expect_opinion()


    # System level callbacks
    #=======================================================
    def prepare(self, event):
        return True

    def finalize(self, event):
        return True

    # Conditions
    # These callback functions cache their result before they may be checked multiple times
    # Depending on the function, they may have to cache multiple values, e.g. c_entity_detected
    # If conditions callbacks interact with user_attributes, remember to reset fields where necessary
    #==========================================================
    def c_resuming_dialog(self, event):
        if "c_resuming_dialog" in self.tags:
            return True

        return False

    def c_no_questions_left(self, event):
        ## LOGGER.debug("We've just entered c_questions_left")

        # hardcoded num questions total
        if "c_no_questions_left" in self.tags:
            return True

        return False

    def c_no_trivia_left(self, event):
        # temp
        if "c_no_trivia_left" in self.tags:
            return True

        return False

    '''
    def c_possibly_bored(self, event):
        # TODO
        return False

    def c_not_interested(self, event):
        ## LOGGER.debug("We've just entered c_interested")
        if self.nlu_processor.yes:
            return True

        #self.tags["not_interested"] = True
        return False

    def c_nothing_to_share(self, event):
        # TODO
        return True
    '''

    def r_t1(self, event):
        return self.update_transition_history("r_t1")

    def r_t2(self, event):
        return self.update_transition_history("r_t2")

    def r_t3(self, event):
        return self.update_transition_history("r_t3")

    def r_t4(self, event):
        return self.update_transition_history("r_t4")

    def r_t5(self, event):
        return self.update_transition_history("r_t5")

    def r_t6(self, event):
        return self.update_transition_history("r_t6")

    def r_t7(self, event):
        return self.update_transition_history("r_t7")

    def r_t8(self, event):
        return self.update_transition_history("r_t8")

    def r_t9(self, event):
        return self.update_transition_history("r_t9")

    def r_t10(self, event):
        return self.update_transition_history("r_t10")

    def r_t11(self, event):
        return self.update_transition_history("r_t11")

    def r_t12(self, event):
        return self.update_transition_history("r_t12")

    def update_transition_history(self, transition_tag):
        self.transition_history.append(transition_tag)
        self.transition_history.pop(0)
        return True

class BookAcknowledgeBot:
    def __init__(self, fsm):
        self.fsm = fsm
        self.utt = fsm.utt
        self.text = fsm.text
        self.nlu_processor = fsm.automaton.nlu_processor
        self.book_info = self.fsm.book_info
        self.context = self.fsm.context
        self.current_context = self.context.current_context
        self.questions_mapping = {
            "q1": self.get_q1_ack,
            "q2": self.get_q2_ack,
            "q3": self.get_q3_ack,
            "q4": self.get_q4_ack,
        }

    def get_ack(self, q_num):
        return self.questions_mapping[q_num]()

    def get_q1_ack(self):
        ratings_convert = {"zero": 0, "one": 1, "two": 2, "three": 3, "four": 4, "five": 5,
                           "six": 6, "seven": 7, "eight": 8, "nine": 9, "ten": 10}
        other_ratings = {"hundred", "thousand", "point", "half", "million", "billion", "out"}

        rating = -1
        for word in self.fsm.text.split():
            if word in other_ratings:
                rating = -1
                break
            try:
                rating = int(word)
            except BaseException:
                if word in ratings_convert:
                    rating = ratings_convert[word]


        if rating == 10:
            ack = self.utt(["book_dialog", "book_question_bank", "q1", "ack_perfect"])
        elif rating >= 7:
            default_ack = "(Wow|Cool)!"
            blender_ack = self.get_blender_qa()
            if len(blender_ack) > 0:
                default_ack = blender_ack
    
            ack = self.utt(["book_dialog", "book_question_bank", "q1", "ack_high"], {"rating": rating})
        elif rating <= 6 and rating >= 4:
            ack = self.utt(["book_dialog", "book_question_bank", "q1", "ack_med"], {"rating": rating})
        elif rating >= 0:
            ack = self.utt(["book_dialog", "book_question_bank", "q1", "ack_low"], {"rating": rating})
        elif self.nlu_processor.user_ans_unknown:
            ack = self.utt(["book_dialog", "book_question_bank", "q_general", "ack_ans_unknown"])
        else:
            b_ack = self.get_blender_qa()
            if len(b_ack) == 0:
                b_ack = "(Okay|I see)"
            ack = self.utt(["book_dialog", "book_question_bank", "q1", "ack_default"], {"acknowledgement": b_ack})

        # determine the next question and set q_key
        if rating >= 5 or rating == -1:
            self.current_context["q_key"] = "q"
        else:
            self.current_context["q_key"] = "q2"

        return ack

    def get_q2_ack(self):
        q_key = self.current_context["q_key"]
        result = self.get_blender_qa()
        sys_ack = "i see,"
        if result is not None and len(result.split(" ")) < 30 and len(result) > 0:
            sys_ack = result
        if self.nlu_processor.user_ans_unknown:
            ack = self.utt(["book_dialog", "book_question_bank", "q_general", "ack_ans_unknown"])
        elif q_key == "q":

            
            ack = self.utt(["book_dialog", "book_question_bank", "q2", "ack_q1"], {"acknowledgement": sys_ack})
        elif q_key == "q2":
            ack = ack = self.get_blender_qa()
            ack = self.utt(["book_dialog", "book_question_bank", "q2", "ack_q2"], {"acknowledgement": sys_ack})
        

        self.current_context["q_key"] = "q"

        return ack

    def get_q3_ack(self):
        ack = self.get_blender_qa()
        if len(ack) == 0:
            if self.nlu_processor.user_ans_unknown:
                ack = self.utt(["book_dialog", "book_question_bank", "q_general", "ack_ans_unknown"])
            else:
                ack = self.utt(["book_dialog", "book_question_bank", "q3", "ack"])
        self.current_context["q_key"] = "q"
        return ack

    def get_q4_ack(self):
        ack = self.get_blender_qa()
        if self.nlu_processor.user_ans_unknown:
            if len(ack) == 0:
                ack = self.utt(["book_dialog", "book_question_bank", "q_general", "ack_ans_unknown"])
        else:
            if len(ack) == 0:
                ack = self.utt(["book_dialog", "book_question_bank", "q4", "ack"])
        self.current_context["q_key"] = "q"
        return ack

    def get_blender_qa(self):
        try:
            response = self.fsm.automaton.ack_field_raw.get("ack", "")
            LOGGER.info("Blender Response {}".format(response))
            return response
        except Exception as e:
            LOGGER.info("Blender Failed {}".format(e))
            return ""

