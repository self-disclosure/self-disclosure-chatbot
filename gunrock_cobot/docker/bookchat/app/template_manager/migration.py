import logging
import warnings
from enum import Enum
from types import SimpleNamespace
from typing import Dict, List, Optional, Tuple, Union

from template_manager.template import _Template_v3, TEMPLATES_MAP
from template_manager.manager import TemplateManager as TemplateManager_v3

from selecting_strategy.module_selection import ModuleSelector


SelectorAPI = Union[List[str], str]


class _Template_v3_mock(_Template_v3):

    def _str_dict_selector(self, entry: Union[str, dict], embedded_info: dict = None) -> Union[str, dict]:
        logging.warn(f"using _str_dict_selector_mock")
        if type(entry) is not str and type(entry) is not dict:
            raise ValueError(f"Entry type invalid: {type(entry)}; {entry}")
        return entry


# Export Template class with imported enum values
Template_v3_mock = _Template_v3_mock('Template_v3_mock', TEMPLATES_MAP)


class _Template_v2(Enum):
    """
    Mock of the old template class for compatibility
    """

    def utterance(self, selector: SelectorAPI, slots: Dict[str, str], user_attributes_ref) -> Union[str, dict]:
        warnings.warn("TMv.1 is deprecated. Use TemplateManger.", DeprecationWarning, stacklevel=2)

        template_v3_mock = Template_v3_mock[self.name]
        tm = TemplateManager_v3(template_v3_mock, user_attributes_ref)

        if isinstance(selector, list):
            selector = '/'.join(selector)

        return tm.speak(selector, slots)

    def has_utterance(self,
                      unformatted_utterance: str,
                      selector: SelectorAPI) -> bool:
        warnings.warn("TMv.1 is deprecated. Use TemplateManger.", DeprecationWarning, stacklevel=2)

        # mocking user_attributes_ref since it's not required
        ua_ref = SimpleNamespace()
        tm = TemplateManager_v3(Template_v3_mock[self.name], ua_ref)

        if isinstance(selector, list):
            selector = '/'.join(selector)
        return tm.has_utterance(unformatted_utterance, selector)

    def get_child_keys(self, selector: SelectorAPI) -> List[str]:
        # mocking user_attributes_ref since it's not required
        ua_ref = SimpleNamespace()
        tm = TemplateManager_v3(Template_v3_mock[self.name], ua_ref)

        if isinstance(selector, list):
            selector = '/'.join(selector)

        return tm.get_child_keys(selector)

    def has_selector(self, selector: SelectorAPI) -> bool:
        # mocking user_attributes_ref since it's not required
        ua_ref = SimpleNamespace()
        tm = TemplateManager_v3(Template_v3_mock[self.name], ua_ref)

        if isinstance(selector, list):
            selector = '/'.join(selector)

        return tm.has_selector(selector)

    def count_utterances(self, selector: SelectorAPI) -> int:
        # mocking user_attributes_ref since it's not required
        ua_ref = SimpleNamespace()
        tm = TemplateManager_v3(Template_v3_mock[self.name], ua_ref)

        if isinstance(selector, list):
            selector = '/'.join(selector)
        return tm.count_utterances(selector)

    def count_used_utterances(self, selector: SelectorAPI, user_attributes_ref) -> int:
        tm = TemplateManager_v3(Template_v3_mock[self.name], user_attributes_ref)

        if isinstance(selector, list):
            selector = '/'.join(selector)
        return tm.count_used_utterances(selector)

# for TM v2
Templates = _Template_v2('Templates', TEMPLATES_MAP)

