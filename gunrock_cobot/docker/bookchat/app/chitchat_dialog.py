import logging
import re
import traceback
from transitions import Machine
from bookchat_utils import *
from bookchat_detection_utils import detect_author, detect_entity_1, detect_entity_3, detect_genre
from nlu.dataclass.returnnlp import ReturnNLP, ReturnNLPSegment


from new_bookchat_utils.logging_utils import set_logger_level, get_working_environment
from nlu.dataclass.returnnlp import ReturnNLP, ReturnNLPSegment
LOGGER = logging.getLogger(__name__)
set_logger_level(get_working_environment())

SIZE_TRANSITION_HISTORY = 5
DEFAULT_CONTEXT = {
        "current_state": "s_init",
        "old_state": None,
        "q_key": None,
        "transition_history": ["r_t0"] * SIZE_TRANSITION_HISTORY,
        "chitchat_context": [], # persistent context info
        "tags": [], # temporary context info
        "interrupted": False
    }
def set_chitchat_dialog_init_context(context, subdialog_context):
    if context.chitchat is None:
        context.current_context = DEFAULT_CONTEXT
    elif context.chitchat.get("current_context") is None:
        context.current_context = DEFAULT_CONTEXT
    else:
        context.current_context = context.chitchat.get("current_context", {})
    context.current_context["chitchat_dialog_activated"] = True
    context.current_context["new_chitchat"] = True
    context.current_context["current_state"] = "s_init"

def save_chitchat_dialog_current_context(context):
    context.chitchat = {
        "current_context": context.current_context,
    }

class ChitchatDialog(Machine):
    # High level specification of states, state transitions and transition
    # conditions
    def __init__(self, automaton):
        # Each state has prefix 's_'
        self.states = [
            's_init',  # main entry point
            's_chitchat',
            's_exit',
        ]
        # Each state has a corresponding transtion function that chooses the next state depending on certain conditions
        # There are different ways to define transitions. I'm defining arguments using a list.
        # Args ordering: transition_name, source, dest, conditions=None,
        # unless=None, before=None, after=None, prepare=None
        self.transitions = [
            # system
            ['transduce', '*', 's_exit',
                ['c_no_questions_left', 'r_t1']],
            ['transduce', 's_init', 's_chitchat', [
                'r_t2']],  # first time entry point
            ['transduce', 's_chitchat', '=', ['r_t3']],
        ]

        self.automaton = automaton
        self.text = automaton.text
        self.utt = automaton.utt
        self.context = automaton.context
        self.tags = []
        self.nlu_processor = automaton.nlu_processor
        curr_state = "s_init"
        chitchat_context = self.automaton.user_attributes.get("subdialog_context", DEFAULT_CONTEXT)
            
        if len(chitchat_context) == 0:
            chitchat_context = DEFAULT_CONTEXT

        LOGGER.debug("[Chitchat Dialog] Normal Mode Mode {}".format(chitchat_context))
       
        if "current_context" in self.context.chitchat:
            chitchat_context = self.context.chitchat.get("current_context")
            LOGGER.debug("[Chitchat Dialog] Resume Mode {}".format(chitchat_context))
            if chitchat_context and len(chitchat_context) > 0 and chitchat_context.get("interrupted", False):
                self.tags.append("c_resuming_dialog")
            if not chitchat_context:
                chitchat_dialog_activated = DEFAULT_CONTEXT
                
        self.context.current_context = chitchat_context

        self.current_context = chitchat_context

        if "chitchat_context" not in self.current_context:
            LOGGER.debug("[BOOKCHAT_MODULE] Exception with chitchat_context - current_context {}".format(self.current_context))
            self.current_context["chitchat_context"] = []

        if "c_no_questions_left" in self.current_context["chitchat_context"]:
            self.tags.append("c_no_questions_left")

        
        curr_state = chitchat_context.get("current_state", curr_state)
        LOGGER.debug("[Chitchat Dialog] Current State {}".format(curr_state))
        Machine.__init__(
            self,
            states=self.states,
            transitions=self.transitions,
            initial=curr_state,
            prepare_event='prepare',
            finalize_event='finalize',
            send_event=True)

    def get_blender_qa(self):
        try:
            response = self.automaton.ack_field_raw.get("ack", "")
            LOGGER.info("Blender Response {}".format(response))

            # Custom response, I swallow back of all the comments I made about unorganized code
            if "ans_factopinion" in self.nlu_processor.sys_intent:
                return "I see, thank you for your perspective. "
            return response
        except Exception as e:
            LOGGER.info("Blender Failed {}".format(e))
            return ""

    def get_response(self, input_data):
        LOGGER.debug("Get Response from chitchat_dialog: {}".format(self.current_context))
        self.output = {
            "response": "I was thinking about what book to read and got distracted. Can you remind me what we were talking about? ",
            "next_t_transition": "t_chitchat",
            "exit_code": None,
        }


        last_response = input_data.get("last_response", [""])
        self.last_response = last_response[0] if len(last_response) > 0 else ""

        self.text = input_data["text"][0]
        self.dialog_act = input_data["features"][0]["dialog_act"]
        # self.amz_dialog_act = input_data["features"][0].get("amz_dialog_act")
        self.ner = input_data["features"][0]["ner"]
        # self.asr_correction = input_data["features"][0]["asrcorrection"]
        self.knowledge = input_data["features"][0]["knowledge"]
        # self.topic_keywords = input_data["features"][0]["topic_keywords"]
        # self.sentiment = input_data["features"][0]["sentiment"]
        self.cobot_intents = input_data["features"][0]["intent_classify"]["lexical"] + \
            input_data["features"][0]["intent_classify"]["topic"]
        # self.features = input_data["features"][0]
        self.input_data = input_data
        self.old_state = self.state
        self.cache = {}  # for caching conditions results if necessary
        self.r_ack = ""
        self.r_body = ""
        self.r_question = ""
        self.r_filler_1 = ""
        self.r_filler_2 = ""
        self.r_filler_3 = ""
        self.transition_history = self.current_context["transition_history"]

        self.transduce()

        self.new_state = self.state
        self.current_context["current_state"] = self.state
        self.current_context["old_state"] = self.old_state
        self.current_context["transition_history"] = self.transition_history
        self.current_context["tags"] = self.tags
        save_chitchat_dialog_current_context(self.context)
        self.final_response = self.r_filler_1 + self.r_ack + self.r_filler_2 + self.r_body + self.r_filler_3 + self.r_question
        LOGGER.debug("Get Response from after chitchat_dialog: {}".format(self.current_context))
        blender = self.get_blender_qa()
        
        if self.state == "s_init":
            LOGGER.debug("Add Acknowledgement from Blender {}".format(blender))
            self.final_response = blender + self.final_response
        self.output["response"] = self.final_response
        return self.output

    # on_enter states: Most of the logic for each state to generate the response is defined here
    # Every state has a corresponding callback here
    # =======================================================
    def on_enter_s_chitchat(self, event):
        LOGGER.debug("We've just entered state s_chitchat")

        self.set_ack()
        if self.output["exit_code"] != "s_exit":
            self.set_body()
            self.set_next_question()

    def on_enter_s_exit(self, event):
        LOGGER.debug("We've just entered state s_exit")
        # default response; should be overwritten upon returning
        self.r_question = "I was thinking about what book to read and got distracted. Can you remind me what we were talking about? "

    def set_chitchat_context(self):
        pass

    def set_ack(self):
        # not init for first time or resuming ; except for first time, this should always run as it sets the next q_key
        LOGGER.debug("Setting Acknowledgement...")
        if self.current_context.get("q_key") and "c_resuming_dialog" not in self.tags and not self.current_context["interrupted"]:
            q_key = self.current_context["q_key"]
            ack_bot = ChitchatAcknowledgeBot(self)
            LOGGER.debug("Getting Acknowledgement from {}".format(q_key))
            self.r_ack = ack_bot.get_ack(q_key)
        # overwrite ack if interrupted by question
        if "book_user_qa_answer_only" in self.automaton.bookchat_user["tags"] or \
            self.current_context.get("new_chitchat", False) \
            or "c_resuming_dialog" in self.tags or self.old_state == "s_init":
            self.r_ack = "(So|Anyways), I was (thinking|wondering). " # overwrite ack
            self.current_context["new_chitchat"] = False
    def set_body(self):
        pass

    def set_next_question(self):
        if self.current_context.get("q_key") is None:
            self.current_context["q_key"] = "q1"
        if self.current_context.get("interrupted", False):
            self.current_context["interrupted"] = False
            self.current_context["q_key"] = "q{}".format(int(self.current_context["q_key"][1])+1)

        q_key = self.current_context.get("q_key", "q1")
        if q_key == "q6":
            self.current_context["chitchat_context"].append("c_no_questions_left")
            self.output["exit_code"] = "s_exit"
        else:
            self.tags.append(q_key + "_question")

            if "qa_template_params" in self.current_context:
                self.r_question = self.utt(["chitchat_dialog", "chitchat_question_bank", q_key, "q"], self.current_context["qa_template_params"])
                self.current_context["qa_template_params"] = None
            else:
                self.r_question = self.utt(["chitchat_dialog", "chitchat_question_bank", q_key, "q"])


    # System level callbacks
    # =======================================================
    def prepare(self, event):
        return True

    def finalize(self, event):
        return True

    # Conditions
    # These callback functions cache their result before they may be checked multiple times
    # Depending on the function, they may have to cache multiple values, e.g. c_entity_detected
    # If conditions callbacks interact with user_attributes, remember to reset fields where necessary
    # ==========================================================
    def c_resuming_dialog(self, event):
        if "c_resuming_dialog" in self.tags:
            return True

        return False

    def c_no_questions_left(self, event):
        if "c_no_questions_left" in self.tags:
            return True

        return False

    def r_t1(self, event):
        return self.update_transition_history("r_t1")

    def r_t2(self, event):
        return self.update_transition_history("r_t2")

    def r_t3(self, event):
        return self.update_transition_history("r_t3")

    def update_transition_history(self, transition_tag):
        self.transition_history.append(transition_tag)
        self.transition_history.pop(0)
        return True

    def detect_chitchat_only_response(self):
        key = self.current_context.get("q_key")
        if key is None:
            return False
        if key == "q1":
            entity_names = self.nlu_processor.detect_author()
            return entity_names is not None and len(entity_names) > 0 or self.nlu_processor.user_agree(["user_agree"])
        if key == "q2":
            entity_names = detect_entity_1(self.automaton)
            # Special Response detection
            user_said_yes = self.nlu_processor.user_said_yes
            return entity_names is not None and len(entity_names) > 0 or user_said_yes
        if key == "q3":
            is_fiction = self.nlu_processor.detect_fiction()
            is_non_fiction = self.nlu_processor.detect_non_fiction()
            return is_fiction or is_non_fiction

        if key == "q4":
            entity_names = self.nlu_processor.detect_genre
            return entity_names and len(entity_names) > 0
        return False

    def map_to_to_book_dialog(self, entity=None):
        key = self.current_context.get("q_key")
        
        if key is None:
            return None
        if key == "q1":
            # new_key = "q{}".format(int(key[1]) + 1)
            if entity is not None:
                response = self.utt(["chitchat_dialog", "chitchat_question_bank", key, "followup"], {"author_name": entity})
                LOGGER.debug("Mapping to Book Dialog q1: {}".format(entity))
                
                return response, "t_ask_book"

        if key == "q2":
            if entity is not None:
                LOGGER.debug("Mapping to Book Dialog q2: {}".format(entity))
                return " ", "start_book_dialog"

        if key == "q4":
            if entity is not None:
                response = self.utt(["chitchat_dialog", "chitchat_question_bank", key, "followup"], {"genre": entity})
                return response, "t_ask_book"
        return None

class ChitchatAcknowledgeBot:
    def __init__(self, fsm):
        self.fsm = fsm
        self.utt = fsm.utt
        self.text = fsm.text
        self.cobot_intents = fsm.cobot_intents
        self.ner = fsm.ner
        self.knowledge = fsm.knowledge
        # self.sentiment = fsm.sentiment
        self.last_response = fsm.last_response
        self.concept = None # TODO: Set the value after returnnlp is setup
        self.tags = fsm.tags
        self.output = fsm.output
        self.context = self.fsm.context
        self.current_context = self.context.current_context
        self.questions_mapping = {
            "q1": self.get_q1_ack,
            "q2": self.get_q2_ack,
            "q3": self.get_q3_ack,
            "q4": self.get_q4_ack,
            "q5": self.get_q5_ack,
            "q6": self.get_q6_ack,
        }

    def get_ack(self, q_key):
        return self.questions_mapping[q_key]()

    def get_q1_ack(self):
        ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q1", "ack_default"])
        entity_names = self.fsm.nlu_processor.detect_author() # Refactor this
        # entity_names = detect_author_new(self.fsm.nlu_processor)
        user_agree = self.fsm.nlu_processor.user_agree(["user_agree"])
        LOGGER.debug("Chitchat Dialog q1 Detection {}".format(entity_names))
        final_entity = None
        our_entity = "stephen king"
        if entity_names is not None and len(entity_names) > 0:
            
            if len(entity_names) == 1 and not (self.fsm.nlu_processor.user_ans_neg):
                entity = entity_names[0]
                # favorite matches ours
                if almost_simliar(our_entity, entity.lower()):
                    ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q1", "ack_same_author"])
                    final_entity = our_entity
                else:
                    ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q1", "ack_diff_author"], {"author_name": entity})
                    final_entity = entity
            elif len(entity_names) > 1:
                for entity in entity_names:
                    if almost_simliar(our_entity, entity.lower()):
                        continue
                    ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q1", "ack_diff_author"], {"author_name": entity})
                    final_entity = entity
                    break
        elif user_agree:
            ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q1", "ack_same_author"])
            final_entity = our_entity
        if final_entity is not None:
            self.output["exit_code"] = "s_exit"
            self.output["entity"] = final_entity
            self.current_context["interrupted"] = True
        else:
            self.current_context["q_key"] = "q2"
            self.current_context["interrupted"] = False

        return ack

    def get_q2_ack(self):
        ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q2", "ack_default"])

        entity_names = self.fsm.nlu_processor.detect_book()
        # Special Response detection
        user_said_yes = self.fsm.nlu_processor.user_said_yes
        our_book = "to kill a mockingbird"
        final_entity = None
        LOGGER.debug("[Chitchat Dialog] Ack on book that changed your life {0}, {1}".format(entity_names, user_said_yes))
        if user_said_yes: # Note we prompt bible in the question, hence if user said yes, we should use bible's response
            # ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q2", "ack_tell_me_more"])
            if entity_names is not None and len(entity_names) > 0:
                entity = entity_names[0]
                self.output["exit_code"] = "s_exit"
                LOGGER.debug("Our book {0}, Entity {1}".format(our_book, entity.lower()))
                if almost_simliar(our_book, entity.lower()):
                    ack = self.utt(["acknowledge_same_book"], {})
                    final_entity = our_book
                else:
                # if entity == "special_bible_intent":
                #     ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q2", "ack_same_book"])
                # else:
                    ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q2", "ack_yes_with_book_name"], {"book_name": entity})
                    final_entity = entity
            else:
                ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q2", "ack_yes_no_book_name"], {})
                final_entity = our_book

        # TODO Add tell me more to this flow
        # elif "ans_unknown" in self.cobot_intents:
        #     ack = self.utt(["movie_dialog", "movie_question_bank", "q_general", "ack_ans_unknown"])
        elif entity_names is not None and len(entity_names) > 0:
            
            entity = entity_names[0]
            if almost_simliar(our_book, entity.lower()):
                ack = self.utt(["acknowledge_same_book"], {})
            else:
                ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q2", "ack_yes_with_book_name"], {"book_name": entity})
            final_entity = entity
        else:
            ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q2", "ack_default"], {})
        
        if final_entity:
            self.output["exit_code"] = "s_exit"
            self.output["entity"] = final_entity
        else:
            self.current_context["q_key"] = "q3"
        

        return ack

    def get_q3_ack(self):
        ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q3", "ack_default"])
        is_fiction = self.fsm.nlu_processor.detect_fiction()
        is_non_fiction = self.fsm.nlu_processor.detect_non_fiction()

        if is_fiction:
            ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q3", "ack_fiction"])
        elif is_non_fiction:
            ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q3", "ack_non_fiction"])

        self.current_context["q_key"] = "q4"

        return ack

    def get_q4_ack(self):
        ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q4", "ack_default"])

        entity_names = self.fsm.nlu_processor.detect_genre
        entity = None
        # respond with last entity
        if entity_names and len(entity_names) > 0:
            entity = entity_names[-1]
            our_entity = "science fiction"
            # favorite matches ours
            if almost_simliar(entity, our_entity):
                ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q4", "ack_same_genre"])
                entity = our_entity
            else:
                ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q4", "ack_diff_genre"], {"genre": entity})
        elif self.fsm.nlu_processor.user_ans_unknown:
            ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q_4", "ack_ans_unknown"])

        if entity:
            self.output["entity"] = entity
            self.output["exit_code"] = "s_exit"
        else:
            self.current_context["q_key"] = "q5"
        

        return ack

    def get_q5_ack(self):
        ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q5", "ack_default"])

        if re.search(r"ebook|e book|e-book|electronic|kindle",self.text) and not re.search(r"physical|real|hard.*cover|paper.*back",self.text):
            ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q5", "ack_e_book"])
        elif re.search(r"physical|real|hard.*cover|paper.*back",self.text) and not re.search(r"ebook|e book|e-book|electronic|kindle",self.text):
            ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q5", "ack_physical"])
        # elif "ans_unknown" in self.cobot_intents:
        #     ack = self.utt(["movie_dialog", "movie_question_bank", "q_general", "ack_ans_unknown"])

        self.current_context["q_key"] = "q6"

        return ack

    def get_q6_ack(self):
        ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q6", "ack_default"])

        if re.search(r"\bboth\b",self.text) and not re.search(r"\bnot both\b",self.text):
            ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q6", "ack_both"])
        elif re.search(r"\bfun|\benjoy",self.text) and not re.search(r"\blearn|\binfo",self.text):
            ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q6", "ack_enjoyment"])
        elif re.search(r"\blearn|\binfo",self.text) and not re.search(r"\bfun|\benjoy",self.text):
            ack = self.utt(["chitchat_dialog", "chitchat_question_bank", "q6", "ack_information"])
        # elif "ans_unknown" in self.cobot_intents:
        #     ack = self.utt(["movie_dialog", "movie_question_bank", "q_general", "ack_ans_unknown"])

        self.output["exit_code"] = "s_exit"
        self.current_context["chitchat_context"].append("c_no_questions_left")

        return ack

def almost_simliar(entity, our_entity):
    return fuzz.token_set_ratio(entity, our_entity) > 90

