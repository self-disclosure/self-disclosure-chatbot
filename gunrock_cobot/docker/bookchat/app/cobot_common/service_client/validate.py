import decimal
from .exceptions import ParamValidationError

def validate_parameters(params, definition):
    validator = ParamValidator()
    report = validator.validate(params, definition)
    if report.has_errors():
        raise ParamValidationError(report=report.generate_report())

def range_check(name, value, definition, error_type, errors):
    failed = False
    min_allowed = float('-inf')
    max_allowed = float('inf')
    meta_data = {}
    # The timeout parameter doesn't have a definition associated with as it's not an API parameter.
    if definition is not None:
        meta_data = definition.meta_data
    if 'minLength' in meta_data:
        min_allowed = meta_data['minLength']
    if 'maxLength' in meta_data:
        max_allowed = meta_data['maxLength']
    if value < min_allowed or value > max_allowed:
        failed = True
    if failed:
        errors.report(name, error_type, param=value, valid_range=[min_allowed, max_allowed])


class ValidationErrors(object):

    MISSING_REQUIRED_FIELD = 'missing required field'
    UNKNOWN_FIELD = 'unknown field'
    INVALID_TYPE = 'invalid type'
    INVALID_RANGE = 'invalid range'
    INVALID_LENGTH = 'invalid length'
    UNABLE_TO_ENCODE_TO_JSON = 'unable to encode to json'

    def __init__(self):
        self._errors = []

    def has_errors(self):
        if self._errors:
            return True
        return False

    def generate_report(self):
        error_messages = []
        for error in self._errors:
            error_messages.append(self._format_error(error))
        return '\n'.join(error_messages)

    def _format_error(self, error):
        error_type, name, additional = error
        name = self._get_name(name)
        if error_type == self.MISSING_REQUIRED_FIELD:
            return 'Missing required parameter in %s: "%s"' % (
                name, additional['required_name'])
        elif error_type == self.UNKNOWN_FIELD:
            return 'Unknown parameter in %s: "%s", must be one of: %s' % (
                name, additional['unknown_param'],
                ', '.join(additional['valid_names']))
        elif error_type == self.INVALID_TYPE:
            return 'Invalid type for parameter %s, value: %s, type: %s, ' \
                   'valid types: %s' % (name, additional['param'],
                                        type(additional['param']).__name__,
                                        ', '.join(additional['valid_types']))
        elif error_type == self.INVALID_RANGE:
            min_allowed = additional['valid_range'][0]
            max_allowed = additional['valid_range'][1]
            return ('Invalid range for parameter "%s", value: %s, valid range: '
                    '%s-%s' % (name, additional['param'],
                               min_allowed, max_allowed))
        elif error_type == self.INVALID_LENGTH:
            min_allowed = additional['valid_range'][0]
            max_allowed = additional['valid_range'][1]
            return ('Invalid length for parameter "%s", value: %s, valid length: '
                    '%s-%s' % (name, additional['param'],
                               min_allowed, max_allowed))
        elif error_type == self.UNABLE_TO_ENCODE_TO_JSON:
            return 'Invalid parameter %s must be json serializable: %s' \
                % (name, additional['type_error'])

    def report(self, name, reason, **kwargs):
        self._errors.append((reason, name, kwargs))

    def _get_name(self, name):
        if not name:
            return 'input'
        elif name.startswith('.'):
            return name[1:]
        else:
            return name


class ParamValidator(object):

    def validate(self, params, definition):
        errors = ValidationErrors()
        self._validate(params, definition, errors, name='')
        return errors

    def _validate(self, params, definition, errors, name):
        getattr(self, '_validate_%s' % definition.type_name)(
            params, definition, errors, name
        )

    def _validate_json_serializable(self, params, errors, name):
        try:
            json.dumps(params)
        except (ValueError, TypeError) as e:
            errors.report(name, self.UNABLE_TO_ENCODE_TO_JSON, type_error=e)
    
    def _validate_object(self, params, definition, errors, name):
        # Validate type
        if not isinstance(params, dict):
            errors.report(name, errors.INVALID_TYPE, param=params, valid_types='dict')
            return
        # Validate the required fields
        for required_member in definition.required:
            if required_member not in params:
                errors.report(name, errors.MISSING_REQUIRED_FIELD,
                                required_name =  required_member)
        # members = definition.get('properties', {})
        properties = definition.properties
        valid_params = list(properties)
        # Whitelist timeout parameter
        valid_params.append("timeout_in_millis")
        known_params = []
        # Validate known params.
        for param in params:
            if param not in valid_params:
                errors.report(name, errors.UNKNOWN_FIELD, unknown_param=param,
                              valid_names=list(properties))
            else:
                known_params.append(param)
        # Validate structure members.
        for param in known_params:
            if param == "timeout_in_millis":
                self._validate_float(params[param], None, errors, '%s.%s' % (name, param))
            else:
                self._validate(params[param], properties[param],
                               errors, '%s.%s' % (name, param))
    
    def _validate_array(self, param, definition, errors, name):
        if not isinstance(param, list) and not isinstance(param, tuple):
            errors.report(name, errors.INVALID_TYPE, param=param, valid_types=['list', 'tuple'])
            return
        range_check(name, len(param), definition, errors.INVALID_LENGTH, errors)

        item_definition = definition.items
        for i, item in enumerate(param):
            self._validate(item, item_definition, errors, '%s[%s]' % (name, i))

    def _validate_string(self, param, definition, errors, name):
        if not isinstance(param, str):
            errors.report(name, errors.INVALID_TYPE, param=param, valid_types=['str'])
            return
        range_check(name, len(param), definition, errors.INVALID_LENGTH, errors)

    def _validate_float(self, param, definition, errors, name):
        if not isinstance(param, float) and not isinstance(param, int) and not isinstance(param, decimal.Decimal):
            errors.report(name, errors.INVALID_TYPE, param=param, valid_types='float or int')
        range_check(name, param, definition, errors.INVALID_RANGE, errors)
