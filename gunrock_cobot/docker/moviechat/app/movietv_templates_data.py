data = {'ask_movie': [{
                          'text': 'I love to discover new movies. I recently watched Joker because many people told me it was a masterpiece. What movie would you recommend to me? '},
                      {
                          'text': "Understanding humor is really hard for an AI like me, but Parasite made me laugh. What's the last movie that made you really laugh? "},
                      {
                          'text': "I am also a huge fan of the John Wick movies. The action is so exciting, I can't wait for the new one coming next year. What movie do you think has the best action scenes? "},
                      {
                          'text': "One of my favorite science fiction movies is Avengers Endgame. What's the last science fiction movie that you really enjoyed? "},
                      {
                          'text': "Maybe I'm childish but I really liked Frozen Two. What's your favorite animated movie? "},
                      {
                          'text': 'A lot of people tell me that the Shawshank Redemption is the best movie ever made. What movie would you consider the greatest of all time? '},
                      {
                          'text': "I really enjoyed rewatching The Dark Knight recently. What's your favorite superhero movie? "},
                      {
                          'text': "After watching the Harry Potter movies, I became a big fan of fantasy movies. What's your favorite fantasy movie? "},
                      {
                          'text': "After watching The Conjuring, I was so scared I couldn't sleep for weeks. What's your favorite horror movie? "},
                      {
                          'text': "Human love is so interesting. Watching the movie La La Land was really moving. What's your favorite romance movie? "},
                      {
                          'text': "I also love a good crime movies and drama. Pulp Fiction and The Godfather was amazing. What's your favorite crime movie? "},
                      {
                          'text': "I still remember the first time I watched Titanic. If I had eyes, I would have cried. What's a movie that made you emotional? "}]}
