import re

import template_manager
from typing import Optional
template_movies = template_manager.Templates.movies
from question_handling.quetion_handler import QuestionHandler, QuestionResponse, QuestionResponseTag
from movie_preference_predictor import MoviePreferencePredictor
from movie_module_regex import clarify_movie_name

class MovieQuestionHandler:
    def __init__(self, automaton):
        self.automaton = automaton
        self.logger = automaton.logger
        self.text = automaton.coref_text
        self.returnnlp = automaton.returnnlp
        self.subdialog_context_tags = automaton.user_attributes.get("subdialog_context",{}).get("tags", [])
        self.template = template_manager.Templates.movies
        self.question_detector = automaton.question_detector
        self.question_handler = automaton.question_handler

    def handle_question(self, enable_none=False, modified_question="") -> Optional[QuestionResponse]:
        self.logger.debug("[Movie QA]: get_answer")
        if not self.question_detector.has_question():
            return None

        if modified_question:
            self.question_text = modified_question
            self.automaton.moviechat_user["tags"].append("movie_user_qa_answer_only_with_quesiton")
        else:
            self.question_text = self.question_detector.get_question_segment_text()
            self.automaton.moviechat_user["tags"].append("movie_user_qa_answer_only")

        res = self.handles_topic_specific_question()
        if res:
            return QuestionResponse(response=res)

        if self.is_unanswerable_question():
            self.logger.debug("[Movie QA]: is_unanswerable_question")
            # return self.question_handler.generate_i_dont_know_response()
            return QuestionResponse(response=self.question_handler.generate_i_dont_know_response())

        res = self.handle_general_question(enable_none)
        if res:
            return res
        return None

    def is_unanswerable_question(self):
        if self.question_detector.is_ask_back_question() or self.question_detector.is_follow_up_question():
            return True
        if self.automaton.user_attributes["current_subdialog"] == "MovieDialog":
            if "q1_question" in self.subdialog_context_tags and "ask_preference" in self.returnnlp[-1].intent.lexical and \
                    re.search(r"or", self.question_detector.get_question_segment_text()):
                return True
        return False

    def is_clariying_movie_name(self):
        return re.search(clarify_movie_name, self.text)


    def handles_topic_specific_question(self) -> str:
        if re.search(r"(talk|chat|know) about.* movie(s|)$",self.question_text):
            return self.utt(["let_me_know_talk_about_specific_movie"])
        elif re.search(r"you (\bsee|\bwatch|\bsaw).* movie", self.question_text) and \
                self.automaton.user_attributes["current_subdialog"] == "MovieDialog":
            return "Yes, I've seen it. "
        elif re.search(r"^what's .* rated$|rating", self.question_text):  # todo: improve detection
            movie_id = self.automaton.moviechat_user.get("proposed_movie_id", "")
            if movie_id:
                movie_info = MoviePreferencePredictor().get_movie_info(movie_id)
                self.logger.debug("[handles_topic_specific_question]: movie_info: {}".format(movie_info))

                rating = movie_info["movie_vote_average"]
                if rating:
                    return "It's rated {} in TMDB. ".format(rating)
        elif self.is_clariying_movie_name():
            return " "  # todo: add current movie name
        return ""

    def handle_general_question(self, enable_none) -> Optional[QuestionResponse]:
        answer = self.question_handler.handle_question()
        self.logger.debug("[handle_general_question]: {}".format(answer.response))

        if answer.tag == QuestionResponseTag.IDK.value:
            if enable_none:
                return None

        return answer

    def utt(self, selector, slots=None):
        if slots is None:
            slots = {}
        return self.template.utterance(
            selector=selector,
            slots=slots,
            user_attributes_ref=self.automaton.template_manager_hash)