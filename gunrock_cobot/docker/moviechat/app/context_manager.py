from movietv_utils import *
from util_redis import RedisHelper
import logging

# should support tags for state context
# also dialog memory of entities like previsouly talked about movies and tags


class ContextManager:
    def __init__(self, user_id):
        self.user_id = user_id
        self.movies = {}
        self.actors = {}
        self.chitchat = {}
        self.current_subdialog_context= {}
        self.mentioned_movie_ids = []

        # context
        self._load_context()

    def _pack_context(self):
        return {
            "movies": self.movies,
            "actors": self.actors,
            "chitchat": self.chitchat,
            "current_subdialog_context": self.current_subdialog_context,
            "mentioned_movie_ids": self.mentioned_movie_ids
        }

    def _unpack_context(self, context):
        if isinstance(context, dict):
            self.movies = context.get("movies", {})
            self.actors = context.get("actors", {})
            self.chitchat = context.get("chitchat", {})
            self.current_subdialog_context = context.get("current_subdialog_context", {})
            self.mentioned_movie_ids = context.get("mentioned_movie_ids", [])

    def _load_context(self):
        user_context = RedisHelper().get(RedisHelper.MOVIE_USER_PREFIX, self.user_id)
        self._unpack_context(user_context)

    def save_context(self):
        user_context = self._pack_context()
        RedisHelper().set(RedisHelper.MOVIE_USER_PREFIX, self.user_id, user_context)

    def get_mentioned_movie_ids(self):
        results = []
        if self.movies:
            movie_ids = [int(movie_id) for movie_id in list(self.movies) if movie_id]
            results.extend(movie_ids)
            logging.debug("[get_mentioned_movie_ids] ids from user mentioned movie: {}".format(movie_ids))

        results.extend(self.mentioned_movie_ids)
        logging.debug("[get_mentioned_movie_ids] bot mentioned ids: {}".format(self.mentioned_movie_ids))

        return results

    def save_movie_dialog_current_context(self, movie_info):
        self.movies[str(movie_info["movie_id"])] = {
            "current_subdialog_context": self.current_subdialog_context,
        }

    # legacy code
    def set_current_context_movies(self, movie_info, facts):
        if facts is None:
            facts = []
        movie_id = str(movie_info["movie_id"])
        self.current_subdialog_context = {
            "movie_id": movie_id,
            "new_movie": True,
            "more_facts": True,
        }

        # set context info for if movie previously talked about
        # set context info for if new facts exist and indices for these facts
        if movie_id in self.movies:
            # if first time talking about movie
            self.current_subdialog_context["new_movie"] = False
            if self.movies[movie_id]["facts_idx"] >= len(facts):
                self.current_subdialog_context["more_facts"] = False
        else:  # set new entry
            self.movies[movie_id] = {
                "movie_info": movie_info,
                "facts": facts,
                "facts_idx": 0,
            }
            if not facts:
                self.current_subdialog_context["more_facts"] = False

    # legacy code
    def get_current_context_movie_fact(self):
        movie_id = str(self.current_subdialog_context["movie_id"])
        facts_idx = self.movies[movie_id]["facts_idx"]
        if facts_idx < len(self.movies[movie_id]["facts"]):
            self.movies[movie_id]["facts_idx"] += 1
            return self.movies[movie_id]["facts"][facts_idx]
        return None
