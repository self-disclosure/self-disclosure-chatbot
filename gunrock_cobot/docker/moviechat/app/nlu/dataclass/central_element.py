import logging
import warnings
from dataclasses import dataclass, field
from nlu.dataclass.backstory import Backstory
from typing import Any, Dict, List, Optional, Set, Tuple, Union

from ..constants import DialogAct as DialogActEnum
from ..utils.type_checking import is_type, ensure_type, safe_cast


logger = logging.getLogger(__name__)


@dataclass
class CentralElement:

    @dataclass
    class CustomIntents:
        sys: List[str] = field(default_factory=list)
        topic: List[str] = field(default_factory=list)
        lexical: List[str] = field(default_factory=list)

        @classmethod
        def from_dict(cls, d: Dict[str, List[str]]):
            intents = {}
            for intent_str in ('sys', 'topic', 'lexical'):
                intent = ensure_type(d.get(intent_str), list, [])
                intents[intent_str] = list(filter(bool, (ensure_type(i, str, None) for i in intent)))
                return cls(**intents)

    @dataclass
    class DialogAct:
        name: str = "NA"
        label: DialogActEnum = DialogActEnum.NA
        score: float = 0.0

        @classmethod
        def from_params(cls, da: Union[str, Any], score: Union[str, float, Any]):
            name = ensure_type(da, str, 'NA')
            score = ensure_type(score, float, None, warnings=None) or \
                safe_cast(ensure_type(score, str, "0.0", warnings=None), float, 0.0)
            label = safe_cast(name, DialogActEnum, DialogActEnum.NA)
            return cls(name=name, label=label, score=score)

    text: str = ""
    dialog_act: DialogAct = field(default=DialogAct())
    dialog_acts: List[DialogAct] = field(default_factory=list)
    custom_intents: CustomIntents = field(default=CustomIntents())
    sentiment: str = ""
    noun_phrase: List[str] = field(default_factory=list)
    key_phrase: List[str] = field(default_factory=list)
    topic_modules: List[str] = field(default_factory=list)
    backstory: Backstory = field(default=Backstory())

    @classmethod
    def from_dict(cls, central_element: Optional[Dict[str, Any]]) -> 'CentralElement':
        if not central_element or not is_type(central_element, dict, warnings=None):
            logger.warning(f"[CENTRAL_ELEMENT] received invalid input: {central_element}")
            return cls()

        dialog_acts = []
        for da_idx in range(2):
            da_key = "" if da_idx == 0 else str(da_idx + 1)
            dialog_act = CentralElement.DialogAct.from_params(
                da=central_element.get(f"DA{da_key}"), score=central_element.get(f'DA{da_key}_score')
            )
            dialog_acts.append(dialog_act)

        return cls(
            text=ensure_type(central_element.get('text'), str, ""),
            dialog_act=dialog_acts[0], dialog_acts=dialog_acts,
            custom_intents=CentralElement.CustomIntents.from_dict(ensure_type(central_element.get('regex'), dict, {})),
            sentiment=ensure_type(central_element.get('senti'), str, ""),
            noun_phrase=list(filter(bool, (ensure_type(i, str, None)
                                           for i in ensure_type(central_element.get('np'), list, [])))),
            key_phrase=list(filter(bool, (ensure_type(i, str, None)
                                           for i in ensure_type(central_element.get('key_phrase'), list, [])))),
            topic_modules=list(filter(bool, (ensure_type(i, str, None)
                                             for i in ensure_type(central_element.get('module'), list, [])))),
            backstory=Backstory.from_dict(ensure_type(central_element.get('backstory'), dict, {}))
        )

    # MARK: - Util functions

    def has_dialog_act(self,
                       dialog_act: Union[Union[str, DialogActEnum],
                                         List[Union[str, DialogActEnum]],
                                         Set[Union[str, DialogActEnum]]],
                       threshold: Union[float,
                                        Tuple[Optional[float], Optional[float]]] = (0.7, 0.01)) -> bool:
        """
        Check whether any segment matches the dialog act name
        :param dialog_act: the name of the dialog act to be matched
        :param threshold: confidence threshold
        :return: True if matches, otherwise False
        """

        # standardize dialog_act query
        if isinstance(dialog_act, str) or isinstance(dialog_act, DialogActEnum):
            dialog_act = {dialog_act}
        elif isinstance(dialog_act, list):
            dialog_act = set(dialog_act)
        dialog_act: Set[Union[str, DialogActEnum]]

        # standardizing query param
        queries = set()
        for d in dialog_act:
            if isinstance(d, DialogActEnum):
                queries.add(d)
            elif isinstance(d, str):
                try:
                    queries.add(DialogActEnum(d))
                except ValueError as e:
                    logger.warning(e, exc_info=True)
        dialog_act: Set[DialogActEnum]

        # standardizing threshold
        if isinstance(threshold, float) or (isinstance(threshold, tuple) and len(threshold) < 2):
            threshold = (threshold, None)

        values = set(da.label
                     for idx, da in enumerate(self.dialog_acts)
                     if idx < len(threshold) and threshold[idx] and da.score >= threshold[idx])

        return bool(queries & values)


@dataclass
class CentralElementFeatures:

    text: str = ""
    dialog_act: str = ""
    custom_intents: dict = field(default_factory=lambda: {'sys': [],
                                                          'topic': [],
                                                          'lexical': []})
    sentiment: str = ""
    noun_phrase: list = field(default_factory=list)
    key_phrase: list = field(default_factory=list)
    topic_modules: list = field(default_factory=list)
    backstory: dict = field(default_factory=dict)

    def __post_init__(self):
        warnings.warn("Use CentralElement instead.", DeprecationWarning, stacklevel=2)
