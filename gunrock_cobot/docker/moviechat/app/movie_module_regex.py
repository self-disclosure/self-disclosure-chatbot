from nlu.intentmap_scheme import EXCLUDE_NOT, EXCLUDE_NOT_AFTER, DO_NOT

ask_recommend = "|".join([
    r"(recommend|suggest).* me",
    r"you.* (recommend|suggest)",
    r"what is a good movie i should watch"
])

wrong_movie_regex = r"(wrong|incorrect|not a).* (movie|\bshow\b|film|tv|t\. v\.|version|\bone\b)|(wasn't|not).* (what|movie|\bshow\b|film|tv|t\. v\.|version|\bone\b).* (\bi\b|\bi'm\b|name)|got.* wrong|\bnot.* (right|talking about that|(this|that) movie)|you.* (mistake|error|dumb|stupid)"
not_movie_regex = r"(did(n't| not)|was(n't| not)).* (talk|propose|suggest|mention|say|mean).* movie|(isn't|not|wasn't) a movie"

dont_talk_movie = r"i.* do(n't| not).* (talk|chat|discuss) about.* (this|movie|anymore)|not (talk|chat|discuss).* (this|movie|anymore)"
talk_something_else = r".*(talk|chat|speak|discuss).*something.*(else|different|new).*"
likely_mentioning_an_element_of_the_movie = r"(.*)(the)(.*)"
likes_leo_too = r"(.*)leo(.*)(too|also|as well)(.*)"
user_does_not_recommend_movies = r"(.*)(don't|do not|wouldn't|would not)(.*)(recommend)(.*)"
seen_movies_lately = r"have you seen any movies lately|have you seen a good movie lately|Have you seen any films recently"
do_not_discuss_this_movie = r"\bno\b|\bi\b (have|did)(n't| not)|\bnope\b|\bnot (really|\bsee|watch)\b|(do(n't| not)|not).* (talk|chat|discuss).* (it|that|movie)"
never_watch_this_movie = r"(haven't|never|don't|not) (like|watch|\bsee).* movie"
clarify_movie_name = "|".join([
    r"^have i (seen|watched) what$",
    r"what's the name of the movie",
    r"(which|what) movie",
    r".*repeat the movie",
    r"what('s| is| was) (it called|the name)",
])
how_about_you = r"have you|(what|how) about you"
cannot_remember = r"(can't|can( |)not) (remember|think of (any|one|1|it|.*movie|name))"
discuss_another_movie = r"(do(n't| not)|not).* (talk|chat|discuss|like).* (it|that|movie)"
that_is_the_one = r"that('s| is) the (one|1)"
where_do_you_get_that_info = r"where.* you.* (hear|get.* info)|fake|(not|doubt|don't|is that).* true|\blie|(don't|do not).* believe|(that|it).* (stupid|dumb)"
wrong_comment = r"(not|wasn't).* about.* (movie|\bshow\b|film|tv|t\. v\.)|(wrong|incorrect|irrelevant|not.* related).* (info|fact)"
positive_regex = r"(^(?!.*(not|never|don't)).*(\bi am\b|\binterest|\bdo it\b|(talk|chat|discuss) about))"
not_sure_regex = r"(.*)(not sure|don't know|do not know|don't have one|do not have one)(.*)"
like_all =r".*like (all|any).*"
sad = r".*\b(sad(|dest)|cry|crying|cried)\b.*"

# propose movie
follow_up_question = "|".join([
    rf".*is it.*"
])

# watched_movie_regex
rewatch_positive = "|".join([
    rf".*\bi (would|will){EXCLUDE_NOT_AFTER}\b",
    rf".*{EXCLUDE_NOT}again",
    rf".*{EXCLUDE_NOT}rewatch",
    rf".*times"
])


watch_alone = "|".join([
    r".*\b(alone)\b",
    r".*\b(a loan)\b",
    r".*\b(myself)\b",
    r".*\b(plane)\b",
    r".*\b(on my own)\b"
])

watch_with_others = "|".join([
    r".*\b(with)\b",
    r".*\b(others)\b",
    r".*\b(people)\b",
    r".*\b(my)\b",
])

normally_watch = "|".join([
    rf".*\bnormally{EXCLUDE_NOT_AFTER}\b",
    rf".*\busually{EXCLUDE_NOT_AFTER}\b",
    rf".*\btypically{EXCLUDE_NOT_AFTER}\b",
    rf".*\bwould{EXCLUDE_NOT_AFTER} watch\b"
])

new_experience = "|".join([
    rf".*\b{EXCLUDE_NOT}new\b"
])

not_watched_regex = rf".*(didn't|did not|never|haven't|have not|yet to).* (\bwatch(|ed)|\bsee(n|)\b|\bsaw\b|heard\b).*"



