import unittest
import json
from movie_preference_predictor import MoviePreferencePredictor


class TestMoviePreferencePredictor(unittest.TestCase):

    def setUp(self):
        pass

    def test_find_similar_movies(self):
        current_movie_info = {'movie_name': 'the irishman', 'movie_id': 398978, 'movie_genre_ids': [80, 18, 36], 'movie_leading_actor': None, 'movie_release_year': '2019', 'movie_popularity': 296.046}
        movie_preference_predictor = MoviePreferencePredictor(current_movie_info, None, [398978])
        movie_id = movie_preference_predictor.find_a_similar_movie()
        self.assertIsInstance(movie_id, int)
        self.assertNotEqual(movie_id, current_movie_info['movie_id'])

    def test_pick_next_movie(self):
        movie_info = MoviePreferencePredictor().pick_next_movie_with_info()
        print(movie_info)

    def test_get_movie_info(self):
        movie_info = MoviePreferencePredictor().get_movie_info(512200)
        print(movie_info)


if __name__ == '__main__':
    unittest.main()
