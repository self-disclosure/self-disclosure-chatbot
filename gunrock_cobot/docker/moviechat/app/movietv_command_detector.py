from enum import Enum
from typing import List, Optional
import re
from movie_module_regex import ask_recommend
from nlu.dataclass.returnnlp import ReturnNLP
from nlu.command_detector import CommandDetector
import logging

class MovieCommand(Enum):
    EXIT = "exit_moviechat"
    ASK_FAV_MOVIE = "fav_movies"
    ASK_RECOMMEND = "ask_recommend"
    ASK_CURRENT_PLAYING = "currently_playing"
    ASK_SEEN_MOVIE = "know_movies"
    REQ_CHANGE_MOVIE = "change_movie"
    REQ_GUESS_MOVIE = "guess_movie"
    REQ_TV = "req_tv"


class MovieCommandDetector:

    def __init__(self, text, returnnlp):
        logging.info(f"[MovieCommandDetector] init:")

        self.text = text
        self.returnnlp = ReturnNLP.from_list(returnnlp)
        self.common_command_detector = CommandDetector(self.text, self.returnnlp)

    def detect_intents(self) -> Optional[List[str]]:
        logging.info(f"[MovieCommandDetector] detect_intent:")

        subtopic_intent = []

        regex_exit = r"(stop|don't|do not).* (talk|speak|discuss|tell.* me).* about (movie|show|\btv\b|t\. v\.|television)|\bi\b.* (don't|do not|never) (\bwatch|\bsee) (movie|show|tv|t\. v\.)|(chat|talk|discuss|tell.* me).* about.* something (else|beside)|(change|different|next|new) topic|get off movie|\bi\b (hate|don't like|do not like) movie(s|)|(can we|let).* not talk about movie"

        regex_ask_fav_movie = r"(what|tell me).* your favorite.* movie|(what|tell me).* movie you (like|enjoy)"

        if re.search(regex_exit, self.text) or self.common_command_detector.is_request_jumpout():
            subtopic_intent.append(MovieCommand.EXIT.value)
        elif re.search(regex_ask_fav_movie, self.text):
            subtopic_intent.append(MovieCommand.ASK_FAV_MOVIE.value)
        elif "ask_recommend" in self.returnnlp.flattened_lexical_intent or re.search(ask_recommend, self.text) and \
                re.search(r"movie|one|1|film|(anything|something) to watch", self.text):
            subtopic_intent.append(MovieCommand.ASK_RECOMMEND.value)
        elif ((re.search(r"in.* theater|in.* cinema", self.text) and re.search(r"playing|currently|right now|movie|film",
                                                                          self.text)) or
              (re.search(r"(what|tell|which|can|know|give|info|check|see).* movie|film", self.text) and re.search(
                  r"playing|release|out", self.text) and re.search(r"currently|\bnow\b", self.text)) or
              re.search(r"(what(|'s).*(latest|movie).*) (coming|are) (out|release)", self.text)):
            subtopic_intent.append(MovieCommand.ASK_CURRENT_PLAYING.value)
        elif re.search(
                r"movie.* you.* (know|seen|talk|chat)|(what|tell me).* movie.* you.* (watch|seen|saw)|have you (seen|watched) a good movie",
                self.text):
            subtopic_intent.append(MovieCommand.ASK_SEEN_MOVIE.value)
        elif re.search(r"(talk|chat) about.* (another|different) movie|change movie", self.text):
            subtopic_intent.append(MovieCommand.REQ_CHANGE_MOVIE.value)
        elif re.search(r"guess.* my.* movie|(want|can).* you.* guess", self.text):
            subtopic_intent.append(MovieCommand.REQ_GUESS_MOVIE.value)
        elif re.search(r"\btv\b|t\. v\.|television|hulu", self.text) and re.search(
                r"\bwatch|\bsee|\blike|\benjoy|(talk|chat).* about|(\bit|is).* a", self.text):
            subtopic_intent.append(MovieCommand.REQ_TV.value)

        logging.info(f"[MovieCommandDetector] detect_intent: {subtopic_intent}")

        if len(subtopic_intent) == 0:
            return None

        return subtopic_intent