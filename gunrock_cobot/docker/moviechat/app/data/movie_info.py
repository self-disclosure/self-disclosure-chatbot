from dataclasses import dataclass, field
from typing import Any, Dict, List


@dataclass
class MovieInfo:

    @dataclass
    class LeadingActor:
        name: str = ""
        role: str = ""

    movie_name: str = ""
    movie_id: str = ""
    movie_leading_actor: LeadingActor = field(default=LeadingActor)
    movie_release_year: str = ""
    movie_popularity: float = 0.0

    @classmethod
    def from_dict(cls, movie_info: Dict[str, Any]):
        return cls(**movie_info)

if __name__ == '__main__':
    movie_info = {'movie_name': 'joker',
                  'movie_id': 475557,
                  'movie_leading_actor': {'name': 'Joaquin Phoenix', 'role': 'Arthur Fleck / Joker'},
                  'movie_release_year': '2019',
                  'movie_popularity': 219.43
                  }

    movie = MovieInfo.from_dict(movie_info)
    print(movie)