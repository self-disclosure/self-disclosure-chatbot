import logging
from dataclasses import dataclass, asdict, field
from typing import List, Dict, Any, Optional
from movie_user_profile import MovieUserProfile
import random


from movietv_utils import get_actor_trivia_from_reddit

class ActorQuestionType:
    ASK_OPINION_ACTING = "ask_opinion_on_acting"
    ASK_FAV_MOVIE_WITH_ACTOR = "ask_fav_movie_with_actor"


class ActorDiscussionMode:
    ASK_OPINION = "ask_opinion"
    PROVIDE_TRIVIA = "provide_trivia"
    PROVIDE_OPINION = "provide_opinion"

@dataclass
class ActorSubdialogContext:
    actor_name: str = ""
    mode: str = ActorDiscussionMode.ASK_OPINION
    # is_liked_movie_type: bool = False  # if it's the type of movie user usually like
    mentioned_opinion: bool = 0
    trivia_total_count: int = 0
    trivial_used_count: int = 0
    question_used_count: int = 0
    question_asked: List[str] = field(default_factory=list)
    short_answer_count: int = 0
    opinion_comment_answer_count: int = 0


    @classmethod
    def from_dict(cls, ref: Dict[str, Any]):
        result = cls(
            actor_name=ref.get("actor_name", ""),
            mode=ref.get("mode", ActorDiscussionMode.ASK_OPINION),
            trivia_total_count=ref.get("trivia_total_count", 0),
            trivial_used_count=ref.get("trivial_used_count", 0),
            question_used_count=ref.get("question_used_count", 0),
            question_asked=ref.get("question_asked", []),
            short_answer_count=ref.get("short_answer_count", 0),
            opinion_comment_answer_count=ref.get("short_answer_count", 0)
        )
        return result

    def reset(self):
        self.mode = ActorDiscussionMode.ASK_OPINION
        self.trivia_total_count: int = 0
        self.trivial_used_count: int = 0
        self.question_used_count: int = 0
        self.question_asked: List[str] = []
        self.short_answer_count: int = 0
        self.opinion_comment_answer_count: int = 0


class ActorSubdialogManager:
    MAX_TRIVIA_COUNT = 2

    question_bank = [ActorQuestionType.ASK_OPINION_ACTING, ActorQuestionType.ASK_FAV_MOVIE_WITH_ACTOR]

    def __init__(self,
                 actor_subdialog_context: ActorSubdialogContext):
        self.actor_subdialog_context = actor_subdialog_context
        logging.info(f"[ActorSubdialogManager] actor_subdialog_context: {actor_subdialog_context}")
        self.actor_name = self.actor_subdialog_context.actor_name
        self.trivia = []
        # if self.actor_name:
        #     self.retrieve_trivia()

    def set_actor_name_and_retrieve_trivia(self, actor_name: str):
        self.actor_subdialog_context.actor_name = actor_name
        self.actor_name = actor_name
        logging.info(f"[ActorSubdialogManager] actor_name: {self.actor_name}")


    def get_trivia(self):
        if self.trivia:
            return self.trivia

        if self.actor_name:
            retrieved_trivia = get_actor_trivia_from_reddit(self.actor_name)
            if retrieved_trivia:
                self.trivia = retrieved_trivia  # todo: cache
                self.actor_subdialog_context.trivia_total_count = len(self.trivia) if self.trivia else 0

        return self.trivia

    def get_next_state(self):
        result = None
        if self.actor_subdialog_context.mode == ActorDiscussionMode.ASK_OPINION:
            if self.has_available_trivia() and self.used_trivial_fewer_than_max():
                self.actor_subdialog_context.mode = ActorDiscussionMode.PROVIDE_TRIVIA
                result = "provide_actor_trivia"
            elif self.has_available_opinion_question():
                self.actor_subdialog_context.mode = ActorDiscussionMode.ASK_OPINION
                result = "ask_actor_question"
        elif self.actor_subdialog_context.mode == ActorDiscussionMode.PROVIDE_TRIVIA:
            if self.has_available_opinion_question():
                self.actor_subdialog_context.mode = ActorDiscussionMode.ASK_OPINION
                result = "ask_actor_question"
            elif self.has_available_trivia() and self.used_trivial_fewer_than_max():
                self.actor_subdialog_context.mode = ActorDiscussionMode.PROVIDE_TRIVIA
                result = "provide_actor_trivia"

        return result

    def has_available_trivia(self):
        used_trivia = self.actor_subdialog_context.trivial_used_count
        result = used_trivia < len(self.get_trivia())
        return result

    def used_trivial_fewer_than_max(self):
        used_trivia = self.actor_subdialog_context.trivial_used_count
        return used_trivia < self.MAX_TRIVIA_COUNT

    def get_new_trivia_and_update_context(self):
        result = self.get_trivia()[self.actor_subdialog_context.trivial_used_count] + " "
        self.actor_subdialog_context.trivial_used_count += 1
        return result

    def has_available_opinion_question(self):
        next_question = self.get_next_opinion_question()
        return next_question is not None

    def get_next_opinion_question(self):
        used_questions = self.actor_subdialog_context.question_asked

        for candidate in self.question_bank:
            if candidate not in used_questions:
                return candidate

    def get_next_opinion_question_and_update_context(self):
        next_opinion_question = self.get_next_opinion_question()

        if next_opinion_question:
            self.actor_subdialog_context.question_asked.append(next_opinion_question)
            self.actor_subdialog_context.question_used_count += 1
            return "actor_subdialog/{}/question".format(next_opinion_question)

        else:
            return ""

    def get_current_question(self):
        if self.actor_subdialog_context.question_asked:
            return self.actor_subdialog_context.question_asked[-1]