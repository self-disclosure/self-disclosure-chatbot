from movie_preference_extractor import MovieNER, MovieExtractor, GenreExtractor
from movietv_utils import Genre
import json
from fuzzywuzzy import fuzz
import unittest
import pytest


movie_extraction_test_data = [
    ("frozen two", "what was the movie you last seen?", "frozen 2"),
    ("spies in disguise", "By the way, Understanding humor is really hard for an AI like me, but Parasite made me laugh. What's the last movie that made you really laugh?", "spies in disguise"),
    ("doctor seuss's the grinch and how the grinch stole christmas", "Nice. What movie did you see?", "Dr. Seuss' The Grinch"),
    ("that's interesting", "michel is a good guy", None),
    ("Casino Royale", "I'm thrilled to talk about movies. Have you seen any films recently ?", "Casino Royale"),
    ("The avengers", "I'm thrilled to talk about movies. Have you seen any films recently ?", "The avengers"),
    ("The avengers end game", "Nice. What movie did you see?", "Avengers: Endgame"),
    ("aftermath", "I'm thrilled to talk about movies. Have you seen any films recently ?", "aftermath"),
    ("iron man three", "I'm thrilled to talk about movies. Have you seen any films recently ?", "iron man three"),
]

@pytest.mark.parametrize("user_utterance, last_bot_response, expected_result", movie_extraction_test_data)
def test_extract_movie_v2(user_utterance, last_bot_response, expected_result):
    result = MovieExtractor().extract_movies_v2(user_utterance, last_bot_response)
    print(result)
    if result:
        first_result = result[0]
        assert first_result["movie_name"].lower() == expected_result.lower()
    else:
        assert expected_result is None

movie_ner_test_data = [
    ("aladdin", "I'm thrilled to talk about movies. Have you seen any films recently ?"),
    ("the new avengers", "I'm thrilled to talk about movies. Have you seen any films recently ?"),
    ("harry potter", "I'm thrilled to talk about movies. Have you seen any films recently ?"),
    ("hobbit", "I'm thrilled to talk about movies. Have you seen any films recently ?"),
    ("star wars", "I'm thrilled to talk about movies. Have you seen any films recently ?"),
    ("X man", "I'm thrilled to talk about movies. Have you seen any films recently ?"),
    ("the chronicles of narnia", "I'm thrilled to talk about movies. Have you seen any films recently ?"),
    ("the lord of the rings", "I'm thrilled to talk about movies. Have you seen any films recently ?"),
    ("LEGO Marvel Super Heroes", "I'm thrilled to talk about movies. Have you seen any films recently ?"),
    ("spider man", "I'm thrilled to talk about movies. Have you seen any films recently ?"),
    ("iron man", "I'm thrilled to talk about movies. Have you seen any films recently ?"),
    ("i like avengers marvel stuff south", "Yeah? Me too. So do you have a action movie you like? What's it called?"),
    ("when will i do see the new lion king it's epic", "I'm happy to talk about movies. So, have you seen a good movie lately?"),
    ("winner castle",
     "<prosody rate='95%'>Valentine's Day's name is Valentine's Day. <break time='0.15s'/> Anyways, I love to discover new movies. I recently watched Joker because many people told me it was really a masterpiece. What movie would you recommend to me?</prosody>"),
    ("step brothers",
     "For sure. So do you have a comedy movie you like? What's it called?"),
    ("lethal weapon with mel gibson and danny glover",
     "Nice. What movie did you see?"),
    ("i've only seen the first movie", "I see. Yeah, I agree. I read this about the movie. Eddie Murphy says the Shrek series is among his best works. What do you think?"),
    ("yeah i saw king kong today",
     "I'm thrilled to talk about movies. Have you seen any films recently ?"),
    ("Dolittle",
     "I'm thrilled to talk about movies. Have you seen any films recently ?")

]
@pytest.mark.parametrize("user_utterance, last_bot_response", movie_ner_test_data)
def test_fetch_movie_ner(user_utterance, last_bot_response):
    result = MovieNER.fetch(user_utterance, last_bot_response)
    print (result)

@pytest.mark.parametrize("series_name, id", [
    ("Avengers", 86311)
])
def test_get_seres_id_by_name(series_name, id):
    print(MovieExtractor.get_series_id_by_name(series_name))
    assert MovieExtractor.get_series_id_by_name(series_name) == id


def test_extract_movies():
    first_result = MovieExtractor().extract_movies("the joker", strength=2)[0]
    print(first_result)
    assert first_result["movie_name"] == "joker"


@pytest.mark.parametrize("text", ["i agree"])
def test_no_movie_name_included(text):
    assert MovieExtractor().no_movie_name_included(text) == True

@pytest.mark.parametrize("text", ["i saw frozen 2",
                                  "the movie i saw was frozen 2",
                                  "one movie i saw was frozen 2",
                                  # "yes frozen 2",
                                  "yes i watched frozen 2",
                                  "yes we saw frozen 2",
                                  "yes the movie we saw was frozen 2",
                                  "probably frozen 2",
                                  "we saw frozen 2"])

def test_movie_extractor_movie_name_regex_match(text):
    matched_movie_names = MovieExtractor()._find_matched_movie_name_with_regex(text)
    assert len(matched_movie_names) == 1
    assert matched_movie_names[0] == "frozen 2"

@pytest.mark.parametrize("text", ["yeah actually i went and saw one of the movie theater at two",
                                  "frozen 2"])
def test_movie_extractor_movie_name_regex_match_should_not_match(text):
    matched_movie_names = MovieExtractor()._find_matched_movie_name_with_regex(text)
    assert len(matched_movie_names) == 0


def test_are_almost_the_same_movie_name():
    assert MovieExtractor().are_almost_the_same_movie_name("frozen two", "frozen") is True

def test_movie_extractor_search_movie_with_regex_matched_name():
    result, _ = MovieExtractor()._search_movie_with_regex_matched_movie_name("yes frozen 2", None)
    assert len(result) >= 1

def test_movie_extractor_search_with_movie_name_harriet():
    movie_name = "harriet"
    movie_infos, movie_name = MovieExtractor()._search_with_movie_name(movie_name, None)

    assert len(movie_infos) is 1
    assert movie_infos[0]["movie_release_year"] == '2019'


def test_movie_extractor_search_with_movie_name_joker():
    movie_name = "the joker"
    movie_infos, movie_name = MovieExtractor()._search_with_movie_name(movie_name, None)

    print(json.dumps(movie_infos, indent=4))
    assert movie_infos[0]["movie_release_year"] == '2019'

def test_movie_extractor_search_with_movie_name_jurassic_park():
    movie_name = "jurassic park"
    movie_infos, movie_name = MovieExtractor()._search_with_movie_name(movie_name, None)

    print(json.dumps(movie_infos, indent=4))

    assert movie_infos[0]["movie_release_year"] == '1993'

def test_movie_extractor_search_with_movie_name():
    movie_name = "the hobbit"
    movie_infos, movie_name = MovieExtractor()._search_with_movie_name(movie_name, None)

    print(json.dumps(movie_infos, indent=4))
    assert len(movie_infos) == 1
    assert movie_infos[0]["movie_name"] == 'the hobbit the battle of the five armies'



##### Genre extraction ####
genre_extraction_test_data = [
    (
        "i like action movie",
        [Genre.ACTION.value]
    ),
    (
        "i like comedies",
        [GenreExtractor.Genre.COMEDY.value]
    ),
    (
        "that's hilarious",
        []
    ),
    (
        "i like non fiction that's my favorite",
        [Genre.DOCUMENTARY.value]
    ),
    (
        "definitely drama",
        [Genre.DRAMA.value]
    ),
    (
        "like historical",
        [Genre.HISTORY.value]
    ),
    (
        "musical",
        [Genre.MUSIC.value]
    ),
    (
        "mystery movie",
        [Genre.MYSTERY.value]
    ),
    (
        "i like romantic movies",
        [Genre.ROMANCE.value]
    ),
    (
        "i think my favorite well of course my favorite genre of movie is romance on my goodness love mystery movies too but nothing just going to take the place of romance",
        [Genre.ROMANCE.value, Genre.MYSTERY.value]
    ),
    (
        "sci-fi or comedy",
        [Genre.SCIENCE_FICTION.value, Genre.COMEDY.value]
    ),
    (
        "thriller",
        [Genre.THRILLER.value]
    ),
    (
        "maybe the marvel or the star wars series",
        []
    ),
    (
        "romcom and comedy",
        [Genre.COMEDY.value, Genre.ROMANCE.value]
    )
]

@pytest.mark.parametrize("text, expected_result", genre_extraction_test_data)
def test_genre_extractor_romance_and_mystery(text, expected_result):
    assert GenreExtractor().extract_genre(text) == expected_result


def test_genre_enum():
    assert str(Genre.SCIENCE_FICTION) == "science fiction"
    assert Genre.favorite_genre() == ['comedy', 'action', 'science fiction']
    assert Genre(28).name.lower() == "action"

#### General ####
@unittest.skip('test skipped')
def test_fuzz_match():
    assert fuzz.ratio("lego movie the second part", "the lego movie two the second part") == 0