import logging
import os

def get_working_environment():
    if os.environ.get('STAGE') == 'PROD' or os.environ.get('STAGE') == 'BETA':
        return os.environ.get('STAGE').lower()
    else:
        return "local"


def setup_logging_config(logger=None):
    if not logger:
        logger = logging.getLogger()

    working_environment = get_working_environment()

    logging.basicConfig(
        format='[%(levelname)s] %(asctime)s [%(filename)s:%(lineno)d] %(message)s',
    )

    if working_environment == "local":
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)


def unique(sequence):
    seen = set()
    return [x for x in sequence if not (x in seen or seen.add(x))]


def trace_latency(func):
    import time

    def inner_func(*args, **kwargs):
        try:
            start = time.time()
            output = func(*args, **kwargs)  # Call the original function with its arguments.
            end = time.time()

            logging.error(f"[Trace latency] function: {func.__name__}, latency: {(end - start) * 1000}")

            return output

        except Exception as e:
            logging.error(f"[Trace latency] Fail to execute function: {func.__name__}, message: {e}")

    return inner_func