from unittest.mock import ANY
from unittest.mock import patch
from unittest.mock import call
from decimal import Decimal

from movietv_automaton import *
import logging

response_generator = None


def _init_response_generator():
    global response_generator
    response_generator = MovieTVAutomaton(
        logging.getLogger('response generator'))


class TestMovieAutomaton:
    @patch('template_manager.Templates.movies.utterance')
    def test_init_DoYouLikeToWatchMovies_Yes_HaveYouSeenAnyMoviesLately(self, utterance):
        # given
        _init_response_generator()
        input_data = InputData.botAskDoYouLikeToWatchMovie_userAnsPositive
        response = '''Ouu, I\'m excited to talk about movies. So. Umm, have you seen any movies lately? '''
        utterance.return_value = response

        # when
        response_generator.transduce(input_data)

        # then
        assert response_generator.response == response

        utterance.assert_called_once_with(selector=['init', 'one_turn_intro'],
                                          slots={},
                                          user_attributes_ref=ANY)

        assert response_generator.moviechat_user == \
               {'ask_movietv_repeat': True,
                'candidate_module': None,
                'current_loop': {},
                'current_subdialog': None,
                'current_transition': 't_ask_movietv',
                'detected_favourite_genre': '',
                'flow_interruption': False,
                'has_asked_favourite_movie_genre': False,
                'not_first_time': True,
                'propose_continue': 'CONTINUE',
                'propose_topic': None,
                'subdialog_context': {},
                'tags': []}

        assert response_generator.moviechat_context == {'last_module': True}

        # todo: check context data in redis server

    @patch('template_manager.Templates.movies.utterance')
    def test_haveYouSeenAnyMoviesLately_yes_whatMovieDidYouSee(self, utterance):
        # given
        _init_response_generator()
        input_data = InputData.botAskHaveYouSeenAnyMovieLately_userAnsPositive
        response = 'Nice. What movie did you see? '
        utterance.return_value = response

        # when
        response_generator.transduce(input_data)

        # then
        assert response_generator.response == response

        utterance.assert_not_called()  # Ideally should be called, but currently not

        assert response_generator.moviechat_user == \
               {'ask_movietv_repeat': True,
                'candidate_module': None,
                'current_loop': {},
                'current_subdialog': None,
                'current_transition': 't_ask_movietv',
                'detected_favourite_genre': '',
                'flow_interruption': False,
                'has_asked_favourite_movie_genre': False,
                'not_first_time': True,
                'propose_continue': 'CONTINUE',
                'propose_topic': None,
                'subdialog_context': {},
                'tags': []}

        assert response_generator.moviechat_context == {'last_module': True}

        # todo: check context data in redis server

    @patch('context_manager.ContextManager')
    @patch('template_manager.Templates.movies.utterance')
    def test_whatMovieDidYouSee_detectableMovieName_acknowledgeAndAskRating(self, utterance, ContextManager):
        # given
        _init_response_generator()
        input_data = InputData.botAskWhatMovie_userAnsDetectableMovieName

        context_manager = ContextManager.return_value
        context_manager.movies = {}
        context_manager.current_context = {}

        response_ack = '''Ah, I believe you're talking about the avengers. '''
        response_content = '''I'm curious. <break time="150ms" /> What would you rate this movie on a scale from 1 to 10?'''
        utterance.side_effect = [response_ack, response_content]

        # when
        response_generator.transduce(input_data)

        # then
        ContextManager.assert_called_once_with('lesley-090401')

        assert response_generator.response == response_ack + response_content

        calls = [call(selector=['movie_dialog', 'init_movie_ack'], slots={'movie_title': 'the avengers'},
                      user_attributes_ref=ANY),
                 call(selector=['movie_dialog', 'movie_question_bank', 'q1', 'q'], slots={}, user_attributes_ref=ANY)]
        utterance.assert_has_calls(calls)

        assert response_generator.moviechat_user == {'candidate_module': None,
                                                     'current_loop': {},
                                                     'current_subdialog': 'MovieDialog',
                                                     'current_transition': 't_subdialog',
                                                     'detected_favourite_genre': '',
                                                     'flow_interruption': False,
                                                     'has_asked_favourite_movie_genre': False,
                                                     'not_first_time': True,
                                                     'propose_continue': 'CONTINUE',
                                                     'propose_topic': None,
                                                     'subdialog_context': {'current_state': 's_chitchat',
                                                                           'old_state': 's_init',
                                                                           'tags': ['q1_question', 'q_key_q'],
                                                                           'transition_history': ['r_t0',
                                                                                                  'r_t0',
                                                                                                  'r_t0',
                                                                                                  'r_t0',
                                                                                                  'r_t2']},
                                                     'tags': []}

        assert response_generator.moviechat_context == {'last_module': True}

        # todo: check context data in redis server

    @patch('movie_dialog.get_imdb_trivia_new')
    @patch('context_manager.ContextManager')
    @patch('template_manager.Templates.movies.utterance')
    def test_askRating_highRating_acknowledgeAndProvideTrivia(self, utterance, ContextManager, get_imdb_trivia):
        # given
        _init_response_generator()
        input_data = InputData.botAskRating_userAnsNine

        context_manager = ContextManager.return_value
        context_manager.movies = self.get_movies_context_with_agenver()
        context_manager.current_context = {
            'current_state': 's_chitchat', 'questions_asked': 1, 'trivia_used': 0,
            'movie_info': {'movie_name': 'the avengers', 'movie_id': 24428,
                           'movie_overview': 'When an unexpected enemy emerges and threatens global safety and security, Nick Fury, director of the international peacekeeping agency known as S.H.I.E.L.D., finds himself in need of a team to pull the world back from the brink of disaster. Spanning the globe, a daring recruitment effort begins!',
                           'movie_genre_ids': [878, 28, 12],
                           'movie_leading_actor': {'name': 'Robert Downey Jr.', 'role': 'Tony Stark / Iron Man'},
                           'movie_release_year': '2012'}, 'curr_q_num': 'q1', 'q_key': 'q',
            'transition_history': ['r_t0', 'r_t0', 'r_t0', 'r_t0', 'r_t2'], 'chitchat_mode': 'qa_mode',
            'chitchat_context': [], 'tags': ['q1_question', 'q_key_q'], 'num_total_trivia': 4, 'old_state': 's_init'}

        mock_trivia = self.mockTrivia(get_imdb_trivia)

        response_ack = ''''Wow! 9, huh, you liked it! I enjoyed the movie as well. '''
        response_trivia_body = '''I <w '\n "role='amazon:VBD'>read</w> this about the movie. {trivia} '''.format(
            trivia=mock_trivia)
        response_trivia_question = '''What do you think?<'''
        utterance.side_effect = [response_ack, response_trivia_body, response_trivia_question]

        # when
        response_generator.transduce(input_data)

        # then
        assert response_generator.response == response_ack + response_trivia_body + response_trivia_question

        calls = [call(selector=['movie_dialog', 'movie_question_bank', 'q1', 'ack_7-9_rating'], slots={'rating': 9},
                      user_attributes_ref=ANY),
                 call(selector=['movie_dialog', 'trivia_body'], slots={'trivia': mock_trivia[0]},
                      user_attributes_ref=ANY),
                 call(selector=['movie_dialog', 'trivia_question'], slots={}, user_attributes_ref=ANY)]
        utterance.assert_has_calls(calls)

        assert response_generator.moviechat_user == \
               {'ask_movietv_repeat': True,
                'candidate_module': None,
                'current_loop': {},
                'current_transition': 't_subdialog',
                'detected_favourite_genre': '',
                'flow_interruption': False,
                'has_asked_favourite_movie_genre': False,
                'not_first_time': True,
                'propose_continue': 'CONTINUE',
                'propose_topic': None,
                'subdialog_context': {'current_state': 's_chitchat',
                                      'old_state': 's_chitchat',
                                      'tags': ['q1_ack'],
                                      'transition_history': ['r_t0',
                                                             'r_t0',
                                                             'r_t0',
                                                             'r_t2',
                                                             'r_t3']},
                'tags': []}
        assert response_generator.moviechat_context == {'last_module': True}

        # todo: check context data in redis server

    @patch('movie_dialog.get_imdb_trivia_new')
    @patch('context_manager.ContextManager')
    @patch('template_manager.Templates.movies.utterance')
    def test_doYouLikeTheMovie_negAnswer_shouldAckNegAnswer(self, utterance, ContextManager, get_imdb_trivia):
        # given
        _init_response_generator()
        input_data = InputData.botAskIfLikeMovie_userAnsNegative

        context_manager = ContextManager.return_value
        context_manager.movies = self.get_movies_context_with_agenver()
        context_manager.current_context = {
            'current_state': 's_chitchat', 'questions_asked': 1, 'trivia_used': 0,
            'movie_info': {'movie_name': 'the avengers', 'movie_id': 24428,
                           'movie_overview': 'When an unexpected enemy emerges and threatens global safety and security, Nick Fury, director of the international peacekeeping agency known as S.H.I.E.L.D., finds himself in need of a team to pull the world back from the brink of disaster. Spanning the globe, a daring recruitment effort begins!',
                           'movie_genre_ids': [878, 28, 12],
                           'movie_leading_actor': {'name': 'Robert Downey Jr.', 'role': 'Tony Stark / Iron Man'},
                           'movie_release_year': '2012'}, 'curr_q_num': 'q1', 'q_key': 'q',
            'transition_history': ['r_t0', 'r_t0', 'r_t0', 'r_t0', 'r_t2'], 'chitchat_mode': 'qa_mode',
            'chitchat_context': [], 'tags': ['q1_question', 'q_key_q'], 'num_total_trivia': 4, 'old_state': 's_init'}

        mock_trivia = self.mockTrivia(get_imdb_trivia)


        # when
        response_generator.transduce(input_data)

        # then
        calls = [call(selector=['movie_dialog', 'movie_question_bank', 'q1', 'ack_1-3_rating'], slots={},
                      user_attributes_ref=ANY),
                 call(selector=['movie_dialog', 'trivia_body'], slots={'trivia': mock_trivia[0]},
                      user_attributes_ref=ANY),
                 call(selector=['movie_dialog', 'trivia_question'], slots={}, user_attributes_ref=ANY)]
        utterance.assert_has_calls(calls)


    @patch('movie_dialog.get_imdb_trivia_new')
    @patch('context_manager.ContextManager')
    @patch('template_manager.Templates.movies.utterance')
    def test_doYouLikeTheMovie_highlyPositiveAnswer_shouldAckHighlyPositiveAnswer(self, utterance, ContextManager, get_imdb_trivia):
        # given
        _init_response_generator()
        input_data = InputData.botskIfLikeMovie_userAnsHighlyPositive

        context_manager = ContextManager.return_value
        context_manager.movies = self.get_movies_context_with_agenver()
        context_manager.current_context = {
            'current_state': 's_chitchat', 'questions_asked': 1, 'trivia_used': 0,
            'movie_info': {'movie_name': 'the avengers', 'movie_id': 24428,
                           'movie_overview': 'When an unexpected enemy emerges and threatens global safety and security, Nick Fury, director of the international peacekeeping agency known as S.H.I.E.L.D., finds himself in need of a team to pull the world back from the brink of disaster. Spanning the globe, a daring recruitment effort begins!',
                           'movie_genre_ids': [878, 28, 12],
                           'movie_leading_actor': {'name': 'Robert Downey Jr.', 'role': 'Tony Stark / Iron Man'},
                           'movie_release_year': '2012'}, 'curr_q_num': 'q1', 'q_key': 'q',
            'transition_history': ['r_t0', 'r_t0', 'r_t0', 'r_t0', 'r_t2'], 'chitchat_mode': 'qa_mode',
            'chitchat_context': [], 'tags': ['q1_question', 'q_key_q'], 'num_total_trivia': 4, 'old_state': 's_init'}

        mock_trivia = self.mockTrivia(get_imdb_trivia)


        # when
        response_generator.transduce(input_data)

        # then
        calls = [call(selector=['movie_dialog', 'movie_question_bank', 'q1', 'ack_1-3_rating'], slots={},
                      user_attributes_ref=ANY),
                 call(selector=['movie_dialog', 'trivia_body'], slots={'trivia': mock_trivia[0]},
                      user_attributes_ref=ANY),
                 call(selector=['movie_dialog', 'trivia_question'], slots={}, user_attributes_ref=ANY)]
        utterance.assert_has_calls(calls)


    @patch('context_manager.ContextManager')
    @patch('template_manager.Templates.movies.utterance')
    def test_provideMovieTrivia_anyUtterance_askQuestion(self, utterance, ContextManager):
        # given
        _init_response_generator()
        input_data = InputData.botProvideMovieTrivia_userAnsAnyComment

        context_manager = ContextManager.return_value
        context_manager.movies = self.get_movies_context_with_agenver()
        context_manager.current_context = {
            'current_state': 's_chitchat', 'questions_asked': 1, 'trivia_used': 1,
            'movie_info': {'movie_name': 'the avengers', 'movie_id': 24428,
                           'movie_overview': 'When an unexpected enemy emerges and threatens global safety and security, Nick Fury, director of the international peacekeeping agency known as S.H.I.E.L.D., finds himself in need of a team to pull the world back from the brink of disaster. Spanning the globe, a daring recruitment effort begins!',
                           'movie_genre_ids': [878, 28, 12],
                           'movie_leading_actor': {'name': 'Robert Downey Jr.', 'role': 'Tony Stark / Iron Man'},
                           'movie_release_year': '2012'}, 'curr_q_num': 'q1', 'q_key': 'q_9-10_rating',
            'transition_history': ['r_t0', 'r_t0', 'r_t0', 'r_t2', 'r_t3'], 'chitchat_mode': 'trivia_mode',
            'chitchat_context': [], 'tags': ['q1_ack'], 'num_total_trivia': 4, 'old_state': 's_chitchat',
            'qa_template_params': {'rating': 9}}

        response = '''You must have very high praises for this movie to rate it 9. 'What do you '\n 'think was outstanding? Personally, I loved the characters, and I really felt '\n 'for them.'''
        utterance.side_effect = [response]

        # when
        response_generator.transduce(input_data)

        # then
        assert response_generator.response == response

        utterance.assert_called_once_with(selector=['movie_dialog', 'movie_question_bank', 'q2', 'q_9-10_rating'],
                                          slots={'rating': 9}, user_attributes_ref=ANY)

        assert response_generator.moviechat_user == \
               {'ask_movietv_repeat': True,
                'candidate_module': None,
                'current_loop': {},
                'current_transition': 't_subdialog',
                'detected_favourite_genre': '',
                'flow_interruption': False,
                'has_asked_favourite_movie_genre': False,
                'not_first_time': True,
                'propose_continue': 'CONTINUE',
                'propose_topic': None,
                'subdialog_context': {'current_state': 's_chitchat',
                                      'old_state': 's_chitchat',
                                      'tags': ['q2_question', 'q_key_q_9-10_rating'],
                                      'transition_history': ['r_t0',
                                                             'r_t0',
                                                             'r_t2',
                                                             'r_t3',
                                                             'r_t3']},
                'tags': []}

        assert response_generator.moviechat_context == {'last_module': True}

        # todo: check context data in redis server





    def mockTrivia(self, get_imdb_trivia):
        mock_trivia = [
            "Robert Downey Jr. kept food hidden all over the lab set, and apparently nobody could find where it was, so they just let him continue doing it. In the movie, that's his actual food he's offering, and when he was eating, it wasn't scripted, he was just hungry."]
        get_imdb_trivia.return_value = mock_trivia
        return mock_trivia

    def get_movies_context_with_agenver(self):
        return {
            '24428': {
                'current_context': {'current_state': 's_chitchat', 'questions_asked': 1, 'trivia_used': 0,
                                    'movie_info': {'movie_name': 'the avengers', 'movie_id': 24428,
                                                   'movie_overview': 'When an unexpected enemy emerges and threatens global safety and security, Nick Fury, director of the international peacekeeping agency known as S.H.I.E.L.D., finds himself in need of a team to pull the world back from the brink of disaster. Spanning the globe, a daring recruitment effort begins!',
                                                   'movie_genre_ids': [878, 28, 12],
                                                   'movie_leading_actor': {'name': 'Robert Downey Jr.',
                                                                           'role': 'Tony Stark / Iron Man'},
                                                   'movie_release_year': '2012'}, 'curr_q_num': 'q1', 'q_key': 'q',
                                    'transition_history': ['r_t0', 'r_t0', 'r_t0', 'r_t0', 'r_t2'],
                                    'chitchat_mode': 'qa_mode', 'chitchat_context': [],
                                    'tags': ['q1_question', 'q_key_q'],
                                    'num_total_trivia': 4, 'old_state': 's_init'}}}


class InputData:
    botAskDoYouLikeToWatchMovie_userAnsPositive = {
        'intent': ['general', 'general'],
        'slots': [{
            'text': {
                'name': 'text',
                'value': 'open gunrock and yes',
                'confirmationStatus': 'NONE',
                'source': 'USER'
            }
        }, {
            'text': {
                'name': 'text',
                'source': 'USER',
                'value': 'open gunrock and good',
                'confirmationStatus': 'NONE'
            }
        }],
        'text': ['open gunrock and yes', 'open gunrock and good'],
        'features': [{
            'returnnlp': [{
                'text': 'yes',
                'dialog_act': ['pos_answer', '1.0'],
                'intent': {
                    'sys': [],
                    'topic': [],
                    'lexical': ['ans_pos']
                },
                'sentiment': None,
                'googlekg': [],
                'noun_phrase': [],
                'topic': {
                    'topicClass': 'Phatic',
                    'confidence': 999.0,
                    'text': 'yes',
                    'topicKeywords': []
                },
                'concept': []
            }],
            'intent_classify': {
                'sys': [],
                'topic': [],
                'lexical': ['ans_pos']
            },
            'topic2': [{
                'topicClass': 'Phatic',
                'confidence': 999.0,
                'text': 'yes',
                'topicKeywords': []
            }],
            'concept': [],
            'intent': 'general',
            'converttext': 'open gunrock and yes',
            'profanity_check': [0],
            'segmentation': {
                'segmented_text': ['yes'],
                'segmented_text_raw': ['yes']
            },
            'asrcorrection': None,
            'googlekg': [
                []
            ],
            'ner': None,
            'nounphrase2': {
                'noun_phrase': [
                    []
                ],
                'local_noun_phrase': [
                    []
                ]
            },
            'npknowledge': {
                'knowledge': [],
                'noun_phrase': [],
                'local_noun_phrase': []
            },
            'topic': [{
                'topicClass': 'Sports',
                'confidence': 0.289,
                'text': 'open gunrock and yes',
                'topicKeywords': [{
                    'confidence': 0.347,
                    'keyword': '__UNK_WORD__'
                }]
            }],
            'sentiment': None,
            'dialog_act': [{
                'text': 'yes',
                'DA': 'pos_answer',
                'confidence': 1.0
            }, {
                'response_da': [
                    ['statement', '0.9999'],
                    ['statement', '0.6203'],
                    ['statement', '0.9919'],
                    ['opinion', '0.9996']
                ]
            }],
            'spacynp': None,
            'coreference': None,
            'text': 'open gunrock and yes',
            'sentiment2': None,
            'intent_classify2': [{
                'sys': [],
                'topic': [],
                'lexical': ['ans_pos']
            }],
            'topic_module': '',
            'topic_keywords': [],
            'knowledge': [],
            'noun_phrase': [],
            'central_elem': {
                'senti': 'pos',
                'regex': {
                    'sys': [],
                    'topic': [],
                    'lexical': ['ans_pos']
                },
                'DA': 'pos_answer',
                'DA_score': '1.0',
                'module': [],
                'np': [],
                'text': 'yes',
                'backstory': {
                    'confidence': 0.4728534519672394,
                    'text': 'Oh, I was just trying to get to know more about you. '
                }
            }
        }, None],
        'moviechat_context': [None, None],
        'user_id': ['12345666', '12345666'],
        'session_id': ['amzn1.echo-api.session.localinteraction-leozzew3akq2gkjp',
                       'amzn1.echo-api.session.localinteraction-leozzew3akq2gkjp'],
        'resp_type': [None, None],
        'suggest_keywords': None,
        'last_module': 'LAUNCHGREETING',
        'prev_hash': {
            'sys_env': 'local',
            'bf35': ['9c'],
            '87b6': ['49'],
            'used_topic': ['LAUNCHGREETING', 'MOVIECHAT'],
            'selector_history': ['bf35', 'f118', '6f18', 'b0d9', '87b6'],
            '6f18': ['53'],
            'f118': ['6d'],
            'b0d9': ['9e']
        },
        'moviechat_user': {
            'propose_continue': 'STOP',
            'current_transition': 't_init'
        },
        'response': 'Nice! I\'m glad you\'re having a good day!  So, I\'m a huge movie fan! <break time="150ms" /> Do you like to watch movies? '
    }

    botAskHaveYouSeenAnyMovieLately_userAnsPositive = {
        'intent': ['general', 'general'],
        'slots': [{
            'text': {
                'name': 'text',
                'value': 'yes',
                'confirmationStatus': 'NONE',
                'source': 'USER'
            }
        }, {
            'text': {
                'name': 'text',
                'source': 'USER',
                'value': 'open gunrock and yes',
                'confirmationStatus': 'NONE'
            }
        }],
        'text': ['yes', 'open gunrock and yes'],
        'features': [{
            'returnnlp': [{
                'text': 'yes',
                'dialog_act': ['pos_answer', '1.0'],
                'intent': {
                    'sys': [],
                    'topic': [],
                    'lexical': ['ans_pos']
                },
                'sentiment': None,
                'googlekg': [],
                'noun_phrase': [],
                'topic': {
                    'topicClass': 'Phatic',
                    'confidence': 999.0,
                    'text': 'yes',
                    'topicKeywords': []
                },
                'concept': []
            }],
            'intent_classify': {
                'sys': [],
                'topic': [],
                'lexical': ['ans_pos']
            },
            'topic2': [{
                'topicClass': 'Phatic',
                'confidence': 999.0,
                'text': 'yes',
                'topicKeywords': []
            }],
            'concept': [],
            'intent': 'general',
            'converttext': 'yes',
            'profanity_check': [0],
            'segmentation': {
                'segmented_text': ['yes'],
                'segmented_text_raw': ['yes']
            },
            'asrcorrection': None,
            'googlekg': [
                []
            ],
            'ner': None,
            'nounphrase2': {
                'noun_phrase': [
                    []
                ],
                'local_noun_phrase': [
                    []
                ]
            },
            'npknowledge': {
                'knowledge': [],
                'noun_phrase': [],
                'local_noun_phrase': []
            },
            'topic': [{
                'topicClass': 'Phatic',
                'confidence': 999.0,
                'text': 'yes',
                'topicKeywords': []
            }],
            'sentiment': None,
            'dialog_act': [{
                'text': 'yes',
                'DA': 'pos_answer',
                'confidence': 1.0
            }, {
                'response_da': [
                    ['yes_no_question', '0.9385'],
                    ['statement', '0.9999'],
                    ['yes_no_question', '1.0']
                ]
            }],
            'spacynp': None,
            'coreference': None,
            'text': 'yes',
            'sentiment2': None,
            'intent_classify2': [{
                'sys': [],
                'topic': [],
                'lexical': ['ans_pos']
            }],
            'topic_module': '',
            'topic_keywords': [],
            'knowledge': [],
            'noun_phrase': [],
            'central_elem': {
                'senti': 'pos',
                'regex': {
                    'sys': [],
                    'topic': [],
                    'lexical': ['ans_pos']
                },
                'DA': 'pos_answer',
                'DA_score': '1.0',
                'module': [],
                'np': [],
                'text': 'yes',
                'backstory': {
                    'confidence': 0.4728534519672394,
                    'text': 'Oh, I was just trying to get to know more about you. '
                }
            }
        }, None],
        'moviechat_context': [None, {
            'last_module': True
        }],
        'user_id': ['12345666', '12345666'],
        'session_id': ['amzn1.echo-api.session.localinteraction-leozzew3akq2gkjp',
                       'amzn1.echo-api.session.localinteraction-leozzew3akq2gkjp'],
        'resp_type': [None, None],
        'suggest_keywords': None,
        'last_module': 'MOVIECHAT',
        'prev_hash': {
            'sys_env': 'local',
            'bf35': ['9c'],
            '87b6': ['49'],
            'used_topic': ['LAUNCHGREETING', 'MOVIECHAT'],
            'selector_history': ['5738', 'bf35', 'f118', '6f18', 'b0d9'],
            '6f18': ['53'],
            'f118': ['6d'],
            'b0d9': ['9e'],
            '5738': ['c1']
        },
        'moviechat_user': {
            'current_loop': {},
            'subdialog_context': {},
            'ask_movietv_repeat': True,
            'current_transition': 't_ask_movietv',
            'flow_interruption': False,
            'not_first_time': True,
            'propose_continue': 'CONTINUE',
            'tags': []
        },
        'response': 'Ouu, I\'m excited to talk about movies. <break time="150ms" /> So. <break time="150ms" /> Umm, have you seen any movies lately?'
    }

    botAskWhatMovie_userAnsDetectableMovieName = {
        "text": [
            "avenger",
            "yes"
        ],
        "features": [
            {
                "spacynp": [
                    "avenger"
                ],
                "nounphrase2": {
                    "noun_phrase": [
                        [
                            "avenger"
                        ]
                    ],
                    "local_noun_phrase": [
                        [
                            "avenger"
                        ]
                    ]
                },
                "intent_classify2": [
                    {
                        "sys": [],
                        "topic": [],
                        "lexical": []
                    }
                ],
                "segmentation": {
                    "segmented_text": [
                        "avenger"
                    ],
                    "segmented_text_raw": [
                        "avenger"
                    ]
                },
                "asrcorrection": None,
                "concept": [
                    {
                        "noun": "avenger",
                        "data": {
                            "thriller": "0.7821",
                            "amd system": "0.1154",
                            "dodge model": "0.1026"
                        }
                    }
                ],
                "topic2": [
                    {
                        "topicClass": "Movies_TV",
                        "confidence": 999.0,
                        "text": "avenger",
                        "topicKeywords": [
                            {
                                "confidence": 999.0,
                                "keyword": "avenger"
                            }
                        ]
                    }
                ],
                "ner": [
                    {
                        "text": "Avenger",
                        "begin_offset": 0,
                        "end_offset": 7,
                        "label": "GPE"
                    }
                ],
                "npknowledge": {
                    "knowledge": [
                        [
                            "Avenger",
                            "Animated series",
                            "29.963589",
                            "movie"
                        ]
                    ],
                    "noun_phrase": [
                        "avenger"
                    ],
                    "local_noun_phrase": [
                        "avenger"
                    ]
                },
                "sentiment": "neu",
                "converttext": "avenger",
                "sentiment2": [
                    {
                        "neg": "0.0",
                        "neu": "1.0",
                        "pos": "0.0",
                        "compound": "0.0"
                    }
                ],
                "dialog_act": [
                    {
                        "text": "avenger",
                        "DA": "opinion",
                        "confidence": 0.9998
                    },
                    {
                        "response_da": [
                            [
                                "statement",
                                "0.9999"
                            ],
                            [
                                "open_question_opinion",
                                "0.9509"
                            ]
                        ]
                    }
                ],
                "googlekg": [
                    [
                        [
                            "Avenger",
                            "Animated series",
                            "29.963589",
                            "movie"
                        ]
                    ]
                ],
                "returnnlp": [
                    {
                        "text": "avenger",
                        "dialog_act": [
                            "opinion",
                            "0.9998"
                        ],
                        "intent": {
                            "sys": [],
                            "topic": [],
                            "lexical": []
                        },
                        "sentiment": {
                            "neg": "0.0",
                            "neu": "1.0",
                            "pos": "0.0",
                            "compound": "0.0"
                        },
                        "googlekg": [
                            [
                                "Avenger",
                                "Animated series",
                                "29.963589",
                                "movie"
                            ]
                        ],
                        "noun_phrase": [
                            "avenger"
                        ],
                        "topic": {
                            "topicClass": "Movies_TV",
                            "confidence": 999.0,
                            "text": "avenger",
                            "topicKeywords": [
                                {
                                    "confidence": 999.0,
                                    "keyword": "avenger"
                                }
                            ]
                        },
                        "concept": [
                            {
                                "noun": "avenger",
                                "data": {
                                    "thriller": "0.7821",
                                    "amd system": "0.1154",
                                    "dodge model": "0.1026"
                                }
                            }
                        ],
                        "dependency_parsing": None
                    }
                ],
                "text": "avenger",
                "topic": [
                    {
                        "topicClass": "Movies_TV",
                        "confidence": 999.0,
                        "text": "avenger",
                        "topicKeywords": [
                            {
                                "confidence": 999.0,
                                "keyword": "avenger"
                            }
                        ]
                    }
                ],
                "intent": "general",
                "dependency_parsing": None,
                "coreference": None,
                "profanity_check": [
                    0
                ],
                "intent_classify": {
                    "sys": [],
                    "topic": [],
                    "lexical": []
                },
                "topic_module": "",
                "topic_keywords": [
                    {
                        "confidence": 999.0,
                        "keyword": "avenger"
                    }
                ],
                "knowledge": [
                    [
                        "Avenger",
                        "Animated series",
                        "29.963589",
                        "movie"
                    ]
                ],
                "noun_phrase": [
                    "avenger"
                ],
                "central_elem": {
                    "senti": "neu",
                    "regex": {
                        "sys": [],
                        "topic": [],
                        "lexical": []
                    },
                    "DA": "opinion",
                    "DA_score": "0.9998",
                    "module": [],
                    "np": [
                        "avenger"
                    ],
                    "text": "avenger",
                    "backstory": {
                        "text": "<prosody rate = 'x-slow'>um, <break time = '350ms'></break> </prosody> I've never considered that. ",
                        "confidence": 0
                    }
                }
            },
            None
        ],
        "moviechat_context": [
            None,
            {
                "last_module": True
            }
        ],
        "user_id": [
            "lesley-090401",
            "lesley-090401"
        ],
        "session_id": [
            "amzn1.echo-api.session.localinteraction-r5n85y3nqdkbjjk0",
            "amzn1.echo-api.session.localinteraction-r5n85y3nqdkbjjk0"
        ],
        "resp_type": [
            None,
            None
        ],
        "suggest_keywords": None,
        "last_module": "MOVIECHAT",
        "prev_hash": {
            "87b6": [
                "c0"
            ],
            "used_topic": [
                "LAUNCHGREETING",
                "MOVIECHAT"
            ],
            "sys_env": "local",
            "selector_history": [
                "b057",
                "b057",
                "5738",
                "b057",
                "87b6"
            ],
            "b057": [
                "dd",
                "d1",
                "62"
            ],
            "5738": [
                "c1"
            ]
        },
        "moviechat_user": {
            "current_loop": {},
            "subdialog_context": {},
            "ask_movietv_repeat": True,
            "current_transition": "t_ask_movietv",
            "flow_interruption": False,
            "not_first_time": True,
            "propose_continue": "CONTINUE",
            "tags": []
        },
        "response": "Nice. <break time=\"150ms\" /> What movie did you see?"
    }

    botAskRating_userAnsNine = {
        "text": [
            "9",
            "avenger"
        ],
        "features": [
            {
                "npknowledge": {
                    "knowledge": [
                        [
                            "Nine",
                            "2009 film",
                            "59.146027",
                            "movie"
                        ]
                    ],
                    "noun_phrase": [
                        "nine"
                    ],
                    "local_noun_phrase": [
                        "nine"
                    ]
                },
                "sentiment2": [
                    {
                        "neg": "0.0",
                        "neu": "1.0",
                        "pos": "0.0",
                        "compound": "0.0"
                    }
                ],
                "returnnlp": [
                    {
                        "text": "nine",
                        "dialog_act": [
                            "opinion",
                            "0.9998"
                        ],
                        "intent": {
                            "sys": [],
                            "topic": [],
                            "lexical": []
                        },
                        "sentiment": {
                            "neg": "0.0",
                            "neu": "1.0",
                            "pos": "0.0",
                            "compound": "0.0"
                        },
                        "googlekg": [
                            [
                                "Nine",
                                "2009 film",
                                "59.146027",
                                "movie"
                            ]
                        ],
                        "noun_phrase": [
                            "nine"
                        ],
                        "topic": {
                            "topicClass": "Phatic",
                            "confidence": 999.0,
                            "text": "nine",
                            "topicKeywords": []
                        },
                        "concept": [
                            {
                                "noun": "nine",
                                "data": {
                                    "card": "0.5",
                                    "series": "0.25",
                                    "chinese game company": "0.25"
                                }
                            }
                        ],
                        "dependency_parsing": None
                    }
                ],
                "nounphrase2": {
                    "noun_phrase": [
                        [
                            "nine"
                        ]
                    ],
                    "local_noun_phrase": [
                        [
                            "nine"
                        ]
                    ]
                },
                "intent_classify2": [
                    {
                        "sys": [],
                        "topic": [],
                        "lexical": []
                    }
                ],
                "concept": [
                    {
                        "noun": "nine",
                        "data": {
                            "card": "0.5",
                            "series": "0.25",
                            "chinese game company": "0.25"
                        }
                    }
                ],
                "spacynp": None,
                "converttext": "nine",
                "dependency_parsing": None,
                "coreference": None,
                "text": "9",
                "segmentation": {
                    "segmented_text": [
                        "nine"
                    ],
                    "segmented_text_raw": [
                        "nine"
                    ]
                },
                "dialog_act": [
                    {
                        "text": "nine",
                        "DA": "opinion",
                        "confidence": 0.9998
                    },
                    {
                        "response_da": [
                            [
                                "commands",
                                "0.9932"
                            ],
                            [
                                "statement",
                                "0.9999"
                            ],
                            [
                                "open_question_opinion",
                                "0.9998"
                            ]
                        ]
                    }
                ],
                "intent_classify": {
                    "sys": [],
                    "topic": [],
                    "lexical": []
                },
                "topic": [
                    {
                        "topicClass": "Other",
                        "confidence": 999.0,
                        "text": "9",
                        "topicKeywords": []
                    }
                ],
                "profanity_check": [
                    0
                ],
                "ner": [
                    {
                        "text": "9",
                        "begin_offset": 0,
                        "end_offset": 1,
                        "label": "CARDINAL"
                    }
                ],
                "intent": "general",
                "sentiment": None,
                "asrcorrection": None,
                "topic2": [
                    {
                        "topicClass": "Phatic",
                        "confidence": 999.0,
                        "text": "nine",
                        "topicKeywords": []
                    }
                ],
                "googlekg": [
                    [
                        [
                            "Nine",
                            "2009 film",
                            "59.146027",
                            "movie"
                        ]
                    ]
                ],
                "topic_module": "",
                "topic_keywords": [],
                "knowledge": [
                    [
                        "Nine",
                        "2009 film",
                        "59.146027",
                        "movie"
                    ]
                ],
                "noun_phrase": [
                    "nine"
                ],
                "central_elem": {
                    "senti": "neu",
                    "regex": {
                        "sys": [],
                        "topic": [],
                        "lexical": []
                    },
                    "DA": "opinion",
                    "DA_score": "0.9998",
                    "module": [],
                    "np": [
                        "nine"
                    ],
                    "text": "nine",
                    "backstory": {
                        "text": "I haven't thought about that before. ",
                        "confidence": 0
                    }
                }
            },
            None
        ],
        "moviechat_context": [
            None,
            {
                "last_module": True
            }
        ],
        "user_id": [
            "lesley-19090402",
            "lesley-19090402"
        ],
        "session_id": [
            "amzn1.echo-api.session.localinteraction-me138d128u45yoht",
            "amzn1.echo-api.session.localinteraction-me138d128u45yoht"
        ],
        "resp_type": [
            None,
            None
        ],
        "suggest_keywords": None,
        "last_module": "MOVIECHAT",
        "prev_hash": {
            "sys_env": "local",
            "b000": [
                "de"
            ],
            "87b6": [
                "c0"
            ],
            "used_topic": [
                "LAUNCHGREETING",
                "MOVIECHAT"
            ],
            "7cd8": [
                "a9"
            ],
            "selector_history": [
                "b057",
                "7cd8",
                "b000",
                "b057",
                "b057"
            ],
            "b057": [
                "dd",
                "d1"
            ],
            "5738": [
                "c1"
            ]
        },
        "moviechat_user": {
            "current_loop": {},
            "subdialog_context": {
                "current_state": "s_chitchat",
                "old_state": "s_init",
                "transition_history": [
                    "r_t0",
                    "r_t0",
                    "r_t0",
                    "r_t0",
                    "r_t2"
                ],
                "tags": [
                    "q1_question",
                    "q_key_q"
                ]
            },
            "current_subdialog": "MovieDialog",
            "ask_movietv_repeat": True,
            "current_transition": "t_subdialog",
            "flow_interruption": False,
            "not_first_time": True,
            "propose_continue": "CONTINUE",
            "tags": []
        },
        "response": "Great, I know about the avengers! I'm curious. <break time=\"150ms\" /> If you had to rate this movie from 1 to 10, what would it be?"
    }

    botProvideMovieTrivia_userAnsAnyComment = {
        'text': ['sounds good', '9'],
        'features': [{'asrcorrection': None,
                      'central_elem': {'DA': 'appreciation',
                                       'DA_score': '0.9982',
                                       'backstory': {'confidence': 0,
                                                     'text': '<prosody rate = '
                                                             "'x-slow'>um, <break "
                                                             "time = '350ms'></break> "
                                                             "</prosody> I've never "
                                                             'considered that. '},
                                       'module': [],
                                       'np': ['sounds', 'good', 'sounds good'],
                                       'regex': {'lexical': ['ans_pos'],
                                                 'sys': [],
                                                 'topic': []},
                                       'senti': 'pos',
                                       'text': 'sounds good'},
                      'concept': [{'data': {'feature': '0.3148',
                                            'information': '0.313',
                                            'topic': '0.3722'},
                                   'noun': 'sounds'},
                                  {'data': {'concept': '0.2528',
                                            'term': '0.3043',
                                            'word': '0.4428'},
                                   'noun': 'good'},
                                  {'data': {'expression': '1.0'},
                                   'noun': 'sounds good'}],
                      'converttext': 'sounds good',
                      'coreference': None,
                      'dependency_parsing': None,
                      'dialog_act': [{'DA': 'appreciation',
                                      'confidence': 0.9982,
                                      'text': 'sounds good'},
                                     {'response_da': [['statement', '0.9999'],
                                                      ['statement', '0.7075'],
                                                      ['statement', '1.0'],
                                                      ['statement', '0.9408'],
                                                      ['statement', '0.9999'],
                                                      ['statement', '1.0'],
                                                      ['statement', '1.0'],
                                                      ['open_question_opinion',
                                                       '0.9993']]}],
                      'googlekg': [[['sounds',
                                     'Musical instrument',
                                     '39.069687',
                                     '__EMPTY__'],
                                    ['good',
                                     'Novel by Jaroslav Hašek',
                                     '49.204247',
                                     'book'],
                                    ['Sounds Good',
                                     'Television miniseries',
                                     '168.911697',
                                     'movie']]],
                      'intent': 'general',
                      'intent_classify': {'lexical': ['ans_pos'],
                                          'sys': [],
                                          'topic': []},
                      'intent_classify2': [{'lexical': ['ans_pos'],
                                            'sys': [],
                                            'topic': []}],
                      'knowledge': [['sounds',
                                     'Musical instrument',
                                     '39.069687',
                                     '__EMPTY__'],
                                    ['good',
                                     'Novel by Jaroslav Hašek',
                                     '49.204247',
                                     'book'],
                                    ['Sounds Good',
                                     'Television miniseries',
                                     '168.911697',
                                     'movie']],
                      'ner': None,
                      'noun_phrase': ['sounds', 'good', 'sounds good'],
                      'nounphrase2': {'local_noun_phrase': [['sounds', 'good']],
                                      'noun_phrase': [['sounds',
                                                       'good',
                                                       'sounds good']]},
                      'npknowledge': {'knowledge': [['sounds',
                                                     'Musical instrument',
                                                     '39.069687',
                                                     '__EMPTY__'],
                                                    ['good',
                                                     'Novel by Jaroslav Hašek',
                                                     '49.204247',
                                                     'book'],
                                                    ['Sounds Good',
                                                     'Television miniseries',
                                                     '168.911697',
                                                     'movie']],
                                      'local_noun_phrase': ['sounds', 'good'],
                                      'noun_phrase': ['sounds',
                                                      'good',
                                                      'sounds good']},
                      'profanity_check': [0],
                      'returnnlp': [{'concept': [{'data': {'feature': '0.3148',
                                                           'information': '0.313',
                                                           'topic': '0.3722'},
                                                  'noun': 'sounds'},
                                                 {'data': {'concept': '0.2528',
                                                           'term': '0.3043',
                                                           'word': '0.4428'},
                                                  'noun': 'good'},
                                                 {'data': {'expression': '1.0'},
                                                  'noun': 'sounds good'}],
                                     'dependency_parsing': None,
                                     'dialog_act': ['appreciation', '0.9982'],
                                     'googlekg': [['sounds',
                                                   'Musical instrument',
                                                   '39.069687',
                                                   '__EMPTY__'],
                                                  ['good',
                                                   'Novel by Jaroslav Hašek',
                                                   '49.204247',
                                                   'book'],
                                                  ['Sounds Good',
                                                   'Television miniseries',
                                                   '168.911697',
                                                   'movie']],
                                     'intent': {'lexical': ['ans_pos'],
                                                'sys': [],
                                                'topic': []},
                                     'noun_phrase': ['sounds', 'good', 'sounds good'],
                                     'sentiment': {'compound': '0.4404',
                                                   'neg': '0.0',
                                                   'neu': '0.256',
                                                   'pos': '0.744'},
                                     'text': 'sounds good',
                                     'topic': {'confidence': 999.0,
                                               'text': 'sounds good',
                                               'topicClass': 'Phatic',
                                               'topicKeywords': []}}],
                      'segmentation': {'segmented_text': ['sounds good'],
                                       'segmented_text_raw': ['sounds good']},
                      'sentiment': 'pos',
                      'sentiment2': [{'compound': '0.4404',
                                      'neg': '0.0',
                                      'neu': '0.256',
                                      'pos': '0.744'}],
                      'spacynp': None,
                      'text': 'sounds good',
                      'topic': [{'confidence': 999.0,
                                 'text': 'sounds good',
                                 'topicClass': 'Phatic',
                                 'topicKeywords': []}],
                      'topic2': [{'confidence': 999.0,
                                  'text': 'sounds good',
                                  'topicClass': 'Phatic',
                                  'topicKeywords': []}],
                      'topic_keywords': [],
                      'topic_module': ''},
                     None],
        'last_module': 'MOVIECHAT',
        'moviechat_context': [None, {'last_module': True}],
        'moviechat_user': {'ask_movietv_repeat': True,
                           'current_loop': {},
                           'current_subdialog': 'MovieDialog',
                           'current_transition': 't_subdialog',
                           'flow_interruption': False,
                           'not_first_time': True,
                           'propose_continue': 'CONTINUE',
                           'subdialog_context': {'current_state': 's_chitchat',
                                                 'old_state': 's_chitchat',
                                                 'tags': ['q1_ack'],
                                                 'transition_history': ['r_t0',
                                                                        'r_t0',
                                                                        'r_t0',
                                                                        'r_t2',
                                                                        'r_t3']},
                           'tags': []},
        'prev_hash': {'44e4': ['8f'],
                      '5738': ['c1'],
                      '6063': ['59'],
                      '7cd8': ['c6'],
                      '87b6': ['49'],
                      '9d38': ['ab'],
                      'b000': ['10'],
                      'b057': ['dd', 'd1', '62'],
                      'selector_history': ['b057', '44e4', '9d38', '6063', 'b057'],
                      'sys_env': 'local',
                      'used_topic': ['LAUNCHGREETING', 'MOVIECHAT']},
        'resp_type': [None, None],
        'response': '<prosody rate="95%">Nice! 9, huh, you liked it! I enjoyed the '
                    'movie as well. I heard this fact about the movie. <break '
                    'time="150ms" /> Robert Downey Jr. <break time="150ms" /> kept '
                    'food hidden all over the lab set, and apparently nobody could '
                    'find where it was, so they just let him continue doing it. '
                    '<break time="150ms" /> In the movie, that\'s his actual food '
                    "he's offering, and when he was eating, it wasn't scripted, he "
                    'was just hungry. <break time="150ms" /> Any thoughts?</prosody>',
        'session_id': ['amzn1.echo-api.session.localinteraction-tj0orpxk6dnp1f3q',
                       'amzn1.echo-api.session.localinteraction-tj0orpxk6dnp1f3q'],
        'suggest_keywords': None,
        'user_id': ['lesley-19090403', 'lesley-19090403']}

    botAskIfLikeMovie_userAnsNegative = {'a_b_test': ['A', 'A'],
                                         'features': [{'asrcorrection': None,
                                                       'central_elem': {'DA': 'comment',
                                                                        'DA_score': '0.69363933801651',
                                                                        'backstory': {'confidence': 0.6795697212219238,
                                                                                      'followup': '',
                                                                                      'module': '',
                                                                                      'neg': '',
                                                                                      'pos': '',
                                                                                      'postfix': '',
                                                                                      'question': 'are you just '
                                                                                                  'kidding me',
                                                                                      'reason': '',
                                                                                      'tag': '',
                                                                                      'text': '<say-as '
                                                                                              'interpret-as="interjection"> '
                                                                                              'just kidding! '
                                                                                              '</say-as>                                                                                                                                                                                                                                                                                                                                                                                                                            '},
                                                                        'module': [],
                                                                        'np': [],
                                                                        'regex': {'lexical': ['ans_factopinion'],
                                                                                  'sys': [],
                                                                                  'topic': []},
                                                                        'senti': 'neu',
                                                                        'text': "it's silly"},
                                                       'concept': [],
                                                       'converttext': "it's silly",
                                                       'coreference': None,
                                                       'dependency_parsing': {'dependency_label': [['nmod:poss',
                                                                                                    'obj']],
                                                                              'dependency_parent': [[2, 0]],
                                                                              'pos_tagging': [['PRON', 'ADJ']]},
                                                       'dialog_act': [{'DA': 'comment',
                                                                       'confidence': 0.69363933801651,
                                                                       'text': "it's silly"}],
                                                       'googlekg': [[]],
                                                       'intent': 'general',
                                                       'intent_classify': {'lexical': ['ans_factopinion'],
                                                                           'sys': [],
                                                                           'topic': []},
                                                       'intent_classify2': [{'lexical': ['ans_factopinion'],
                                                                             'sys': [],
                                                                             'topic': []}],
                                                       'knowledge': [],
                                                       'ner': None,
                                                       'noun_phrase': [],
                                                       'nounphrase2': {'local_noun_phrase': [[]], 'noun_phrase': [[]]},
                                                       'npknowledge': {'knowledge': [],
                                                                       'local_noun_phrase': [],
                                                                       'noun_phrase': []},
                                                       'profanity_check': [0],
                                                       'returnnlp': [{'concept': [],
                                                                      'dependency_parsing': {'head': [2, 0],
                                                                                             'label': ['nmod:poss',
                                                                                                       'obj']},
                                                                      'dialog_act': ['comment', '0.69363933801651'],
                                                                      'googlekg': [],
                                                                      'intent': {'lexical': ['ans_factopinion'],
                                                                                 'sys': [],
                                                                                 'topic': []},
                                                                      'noun_phrase': [],
                                                                      'pos_tagging': ['PRON', 'ADJ'],
                                                                      'sentiment': {'compound': '0.0258',
                                                                                    'neg': '0.0',
                                                                                    'neu': '0.645',
                                                                                    'pos': '0.355'},
                                                                      'text': "it's silly",
                                                                      'topic': {'confidence': 0.881,
                                                                                'text': "it's silly",
                                                                                'topicClass': 'Phatic',
                                                                                'topicKeywords': []}}],
                                                       'segmentation': {'segmented_text': ["it's silly"],
                                                                        'segmented_text_raw': ["it's silly"]},
                                                       'sentence_completion_text': "it 's silly",
                                                       'sentiment': 'neu',
                                                       'sentiment2': [{'compound': '0.0258',
                                                                       'neg': '0.0',
                                                                       'neu': '0.645',
                                                                       'pos': '0.355'}],
                                                       'spacynp': None,
                                                       'text': "it's silly",
                                                       'topic': [{'confidence': 0.881,
                                                                  'text': "it's silly",
                                                                  'topicClass': 'Phatic',
                                                                  'topicKeywords': []}],
                                                       'topic2': [{'confidence': 0.881,
                                                                   'text': "it's silly",
                                                                   'topicClass': 'Phatic',
                                                                   'topicKeywords': []}],
                                                       'topic_keywords': [],
                                                       'topic_module': ''},
                                                      None],
                                         'intent': ['general', 'general'],
                                         'last_module': 'MOVIECHAT',
                                         'module_selection': {'used_topic': ['LAUNCHGREETING', 'MOVIECHAT']},
                                         'moviechat_context': [None, {'last_module': True}],
                                         'moviechat_user': {'ask_movietv_repeat': True,
                                                            'current_loop': {},
                                                            'current_subdialog': 'MovieDialog',
                                                            'current_transition': 't_subdialog',
                                                            'flow_interruption': False,
                                                            'has_asked_favourite_movie_genre': False,
                                                            'not_first_time': True,
                                                            'propose_continue': 'CONTINUE',
                                                            'subdialog_context': {'current_state': 's_chitchat',
                                                                                  'old_state': 's_init',
                                                                                  'tags': ['q1_question', 'q_key_q'],
                                                                                  'transition_history': ['r_t0',
                                                                                                         'r_t0',
                                                                                                         'r_t0',
                                                                                                         'r_t0',
                                                                                                         'r_t2']},
                                                            'tags': []},
                                         'resp_type': [None, None],
                                         'response': 'Yes I\'ve seen avengers infinity war too! <break time="150ms" /> '
                                                     'Hmm. <break time="150ms" /> How did you feel about the movie? '
                                                     '<break time="150ms" /> Did you enjoy watching it?',
                                         'returnnlp': [[{'concept': [],
                                                         'dependency_parsing': {'head': [2, 0],
                                                                                'label': ['nmod:poss', 'obj']},
                                                         'dialog_act': ['comment', '0.69363933801651'],
                                                         'googlekg': [],
                                                         'intent': {'lexical': ['ans_factopinion'],
                                                                    'sys': [],
                                                                    'topic': []},
                                                         'noun_phrase': [],
                                                         'pos_tagging': ['PRON', 'ADJ'],
                                                         'sentiment': {'compound': '0.0258',
                                                                       'neg': '0.0',
                                                                       'neu': '0.645',
                                                                       'pos': '0.355'},
                                                         'text': "it's silly",
                                                         'topic': {'confidence': 0.881,
                                                                   'text': "it's silly",
                                                                   'topicClass': 'Phatic',
                                                                   'topicKeywords': []}}],
                                                       [{'concept': [{'data': {'film': '0.6364',
                                                                               'movie': '0.1818',
                                                                               'television program': '0.1818'},
                                                                      'noun': 'the avengers'},
                                                                     {'data': {'character': '0.3235',
                                                                               'movie': '0.3176',
                                                                               'thriller': '0.3588'},
                                                                      'noun': 'avengers'}],
                                                         'dialog_act': ['commands', '0.45949310064315796'],
                                                         'googlekg': [['The Avengers',
                                                                       '2012 film',
                                                                       '737.579041',
                                                                       'movie'],
                                                                      ['The Avengers',
                                                                       '2012 film',
                                                                       '124.079308',
                                                                       'movie']],
                                                         'intent': {'lexical': ['ans_factopinion'],
                                                                    'sys': [],
                                                                    'topic': []},
                                                         'noun_phrase': ['the avengers', 'avengers'],
                                                         'pos_tagging': ['DET', 'NOUN'],
                                                         'sentiment': {'compound': '0.0',
                                                                       'neg': '0.0',
                                                                       'neu': '1.0',
                                                                       'pos': '0.0'},
                                                         'text': 'the avengers',
                                                         'topic': {'confidence': '999',
                                                                   'text': 'the avengers',
                                                                   'topicClass': 'Music',
                                                                   'topicKeywords': [{'confidence': '999',
                                                                                      'keyword': 'avengers'}]}}]],
                                         'session_id': ['localtest-session-cf67a456-9150-4449-aac6-a606e5425ca4',
                                                        'localtest-session-cf67a456-9150-4449-aac6-a606e5425ca4'],
                                         'slots': [{'text': {'confirmationStatus': 'NONE',
                                                             'name': 'text',
                                                             'source': 'USER',
                                                             'value': "it's silly"}},
                                                   {'text': {'confirmationStatus': 'NONE',
                                                             'name': 'text',
                                                             'source': 'USER',
                                                             'value': 'the avengers'}}],
                                         'suggest_keywords': None,
                                         'template_manager': {'prev_hash': {'7z4Nog': ['Y9wfQA'],
                                                                            'PPsGsg': ['zforzw'],
                                                                            'VGoH3w': [],
                                                                            'gN4J9Q': ['1rwq/w'],
                                                                            'jSoKmw': ['cI0m2Q'],
                                                                            'jb4KlQ': ['uu0L5g'],
                                                                            'jmYRRA': ['lTIwIg']}},
                                         'text': ["it's silly", 'the avengers'],
                                         'user_id': ['localtest-user-66d06af2-b614-4382-8e97-02f14dd06de0',
                                                     'localtest-user-66d06af2-b614-4382-8e97-02f14dd06de0']}

    botskIfLikeMovie_userAnsHighlyPositive = {'a_b_test': ['A', 'A'],
                                              'features': [{'asrcorrection': None,
                                                            'central_elem': {'DA': 'pos_answer',
                                                                             'DA_score': '0.8599704504013062',
                                                                             'backstory': {
                                                                                 'confidence': 0.6563805341720581,
                                                                                 'followup': '',
                                                                                 'module': '',
                                                                                 'neg': '',
                                                                                 'pos': '',
                                                                                 'postfix': '',
                                                                                 'question': 'I do not like you',
                                                                                 'reason': '',
                                                                                 'tag': '',
                                                                                 'text': '<say-as '
                                                                                         'interpret-as="interjection"> '
                                                                                         'ouch! </say-as><break '
                                                                                         'time="100ms"/> <prosody '
                                                                                         'volume="soft"> I\'m '
                                                                                         'sorry you feel that '
                                                                                         'way.</prosody>                                                                                                                                                                                                                                                                                                                                                                                       '},
                                                                             'module': [],
                                                                             'np': [],
                                                                             'regex': {'lexical': [],
                                                                                       'sys': [],
                                                                                       'topic': []},
                                                                             'senti': 'neu',
                                                                             'text': 'i did'},
                                                            'concept': [],
                                                            'converttext': 'i did it was hilarious',
                                                            'coreference': None,
                                                            'dependency_parsing': {
                                                                'dependency_label': [['nsubj', 'aux'],
                                                                                     ['nsubj',
                                                                                      'cop',
                                                                                      'ccomp']],
                                                                'dependency_parent': [[2, 0], [3, 3, 0]],
                                                                'pos_tagging': [['PRON', 'VERB'],
                                                                                ['PRON', 'AUX', 'ADJ']]},
                                                            'dialog_act': [{'DA': 'pos_answer',
                                                                            'confidence': 0.8599704504013062,
                                                                            'text': 'i did'},
                                                                           {'DA': 'comment',
                                                                            'confidence': 0.9570108652114868,
                                                                            'text': 'it was hilarious'}],
                                                            'googlekg': [[], []],
                                                            'intent': 'general',
                                                            'intent_classify': {'lexical': [], 'sys': [], 'topic': []},
                                                            'intent_classify2': [
                                                                {'lexical': [], 'sys': [], 'topic': []},
                                                                {'lexical': ['ans_pos'],
                                                                 'sys': [],
                                                                 'topic': []}],
                                                            'knowledge': [],
                                                            'ner': None,
                                                            'noun_phrase': [],
                                                            'nounphrase2': {'local_noun_phrase': [[], []],
                                                                            'noun_phrase': [[], []]},
                                                            'npknowledge': {'knowledge': [],
                                                                            'local_noun_phrase': [],
                                                                            'noun_phrase': []},
                                                            'profanity_check': [0],
                                                            'returnnlp': [{'concept': [],
                                                                           'dependency_parsing': {'head': [2, 0],
                                                                                                  'label': ['nsubj',
                                                                                                            'aux']},
                                                                           'dialog_act': ['pos_answer',
                                                                                          '0.8599704504013062'],
                                                                           'googlekg': [],
                                                                           'intent': {'lexical': [], 'sys': [],
                                                                                      'topic': []},
                                                                           'noun_phrase': [],
                                                                           'pos_tagging': ['PRON', 'VERB'],
                                                                           'sentiment': {'compound': '0.0',
                                                                                         'neg': '0.0',
                                                                                         'neu': '1.0',
                                                                                         'pos': '0.0'},
                                                                           'text': 'i did',
                                                                           'topic': {'confidence': 999.0,
                                                                                     'text': 'i did',
                                                                                     'topicClass': 'Phatic',
                                                                                     'topicKeywords': []}},
                                                                          {'concept': [],
                                                                           'dependency_parsing': {'head': [3, 3, 0],
                                                                                                  'label': ['nsubj',
                                                                                                            'cop',
                                                                                                            'ccomp']},
                                                                           'dialog_act': ['comment',
                                                                                          '0.9570108652114868'],
                                                                           'googlekg': [],
                                                                           'intent': {'lexical': ['ans_pos'],
                                                                                      'sys': [],
                                                                                      'topic': []},
                                                                           'noun_phrase': [],
                                                                           'pos_tagging': ['PRON', 'AUX', 'ADJ'],
                                                                           'sentiment': {'compound': '0.4019',
                                                                                         'neg': '0.0',
                                                                                         'neu': '0.426',
                                                                                         'pos': '0.574'},
                                                                           'text': 'it was hilarious',
                                                                           'topic': {'confidence': 999.0,
                                                                                     'text': 'it was hilarious',
                                                                                     'topicClass': 'Phatic',
                                                                                     'topicKeywords': [
                                                                                         {'confidence': 999.0,
                                                                                          'keyword': 'hilarious'}]}}],
                                                            'segmentation': {
                                                                'segmented_text': ['i did', 'it was hilarious'],
                                                                'segmented_text_raw': ['i did',
                                                                                       'it was hilarious']},
                                                            'sentence_completion_text': 'i did it was hilarious',
                                                            'sentiment': 'neu',
                                                            'sentiment2': [{'compound': '0.0',
                                                                            'neg': '0.0',
                                                                            'neu': '1.0',
                                                                            'pos': '0.0'},
                                                                           {'compound': '0.4019',
                                                                            'neg': '0.0',
                                                                            'neu': '0.426',
                                                                            'pos': '0.574'}],
                                                            'spacynp': None,
                                                            'text': 'i did it was hilarious',
                                                            'topic': [{'confidence': 999.0,
                                                                       'text': 'i did it was hilarious',
                                                                       'topicClass': 'Phatic',
                                                                       'topicKeywords': [{'confidence': 999.0,
                                                                                          'keyword': 'hilarious'}]}],
                                                            'topic2': [{'confidence': 999.0,
                                                                        'text': 'i did',
                                                                        'topicClass': 'Phatic',
                                                                        'topicKeywords': []},
                                                                       {'confidence': 999.0,
                                                                        'text': 'it was hilarious',
                                                                        'topicClass': 'Phatic',
                                                                        'topicKeywords': [{'confidence': 999.0,
                                                                                           'keyword': 'hilarious'}]}],
                                                            'topic_keywords': [{'confidence': 999.0,
                                                                                'keyword': 'hilarious'}],
                                                            'topic_module': ''},
                                                           None],
                                              'intent': ['general', 'general'],
                                              'last_module': 'MOVIECHAT',
                                              'module_selection': {'used_topic': ['LAUNCHGREETING', 'MOVIECHAT']},
                                              'moviechat_context': [None, {'last_module': True}],
                                              'moviechat_user': {'ask_movietv_repeat': True,
                                                                 'current_loop': {},
                                                                 'current_subdialog': 'MovieDialog',
                                                                 'current_transition': 't_subdialog',
                                                                 'flow_interruption': False,
                                                                 'has_asked_favourite_movie_genre': False,
                                                                 'not_first_time': True,
                                                                 'propose_continue': 'CONTINUE',
                                                                 'subdialog_context': {'current_state': 's_chitchat',
                                                                                       'old_state': 's_init',
                                                                                       'tags': ['q1_question',
                                                                                                'q_key_q'],
                                                                                       'transition_history': ['r_t0',
                                                                                                              'r_t0',
                                                                                                              'r_t0',
                                                                                                              'r_t0',
                                                                                                              'r_t2']},
                                                                 'tags': []},
                                              'resp_type': [None, None],
                                              'response': "Great, I've seen avengers infinity war too! I'm wondering. "
                                                          '<break time="150ms" /> What did you think of the movie? <break '
                                                          'time="150ms" /> Did you like it?',
                                              'returnnlp': [[{'concept': [],
                                                              'dependency_parsing': {'head': [2, 0],
                                                                                     'label': ['nsubj', 'aux']},
                                                              'dialog_act': ['pos_answer', '0.8599704504013062'],
                                                              'googlekg': [],
                                                              'intent': {'lexical': [], 'sys': [], 'topic': []},
                                                              'noun_phrase': [],
                                                              'pos_tagging': ['PRON', 'VERB'],
                                                              'sentiment': {'compound': '0.0',
                                                                            'neg': '0.0',
                                                                            'neu': '1.0',
                                                                            'pos': '0.0'},
                                                              'text': 'i did',
                                                              'topic': {'confidence': 999.0,
                                                                        'text': 'i did',
                                                                        'topicClass': 'Phatic',
                                                                        'topicKeywords': []}},
                                                             {'concept': [],
                                                              'dependency_parsing': {'head': [3, 3, 0],
                                                                                     'label': ['nsubj', 'cop',
                                                                                               'ccomp']},
                                                              'dialog_act': ['comment', '0.9570108652114868'],
                                                              'googlekg': [],
                                                              'intent': {'lexical': ['ans_pos'], 'sys': [],
                                                                         'topic': []},
                                                              'noun_phrase': [],
                                                              'pos_tagging': ['PRON', 'AUX', 'ADJ'],
                                                              'sentiment': {'compound': '0.4019',
                                                                            'neg': '0.0',
                                                                            'neu': '0.426',
                                                                            'pos': '0.574'},
                                                              'text': 'it was hilarious',
                                                              'topic': {'confidence': 999.0,
                                                                        'text': 'it was hilarious',
                                                                        'topicClass': 'Phatic',
                                                                        'topicKeywords': [{'confidence': 999.0,
                                                                                           'keyword': 'hilarious'}]}}],
                                                            [{'concept': [{'data': {'film': '0.6364',
                                                                                    'movie': '0.1818',
                                                                                    'television program': '0.1818'},
                                                                           'noun': 'the avengers'},
                                                                          {'data': {'character': '0.3235',
                                                                                    'movie': '0.3176',
                                                                                    'thriller': '0.3588'},
                                                                           'noun': 'avengers'}],
                                                              'dependency_parsing': {
                                                                  'head': ['2', '0'],
                                                                  'label': ['det', 'obj']},
                                                              'dialog_act': ['commands', '0.45949310064315796'],
                                                              'googlekg': [['The Avengers',
                                                                            '2012 film',
                                                                            '737.579041',
                                                                            'movie'],
                                                                           ['The Avengers',
                                                                            '2012 film',
                                                                            '124.079308',
                                                                            'movie']],
                                                              'intent': {'lexical': ['ans_factopinion'],
                                                                         'sys': [],
                                                                         'topic': []},
                                                              'noun_phrase': ['the avengers', 'avengers'],
                                                              'pos_tagging': ['DET', 'NOUN'],
                                                              'sentiment': {'compound': '0.0',
                                                                            'neg': '0.0',
                                                                            'neu': '1.0',
                                                                            'pos': '0.0'},
                                                              'text': 'the avengers',
                                                              'topic': {'confidence': '999',
                                                                        'text': 'the avengers',
                                                                        'topicClass': 'Music',
                                                                        'topicKeywords': [{'confidence': '999',
                                                                                           'keyword': 'avengers'}]}}]],
                                              'session_id': ['localtest-session-326c4bed-9a75-4e2c-83b5-5ccbb51a9fcc',
                                                             'localtest-session-326c4bed-9a75-4e2c-83b5-5ccbb51a9fcc'],
                                              'slots': [{'text': {'confirmationStatus': 'NONE',
                                                                  'name': 'text',
                                                                  'source': 'USER',
                                                                  'value': 'i did it was hilarious'}},
                                                        {'text': {'confirmationStatus': 'NONE',
                                                                  'name': 'text',
                                                                  'source': 'USER',
                                                                  'value': 'the avengers'}}],
                                              'suggest_keywords': None,
                                              'template_manager': {'prev_hash': {'7z4Nog': ['Y9wfQA'],
                                                                                 'PPsGsg': ['fLkfQw'],
                                                                                 'VGoH3w': [],
                                                                                 'gN4J9Q': ['CyUXZA'],
                                                                                 'jSoKmw': ['cI0m2Q'],
                                                                                 'jb4KlQ': ['Qz0Pjg'],
                                                                                 'jmYRRA': ['lMMrzw']}},
                                              'text': ['i did it was hilarious', 'the avengers'],
                                              'user_id': ['localtest-user-764b8258-4e6b-4bcc-84de-d4973b213827',
                                                          'localtest-user-764b8258-4e6b-4bcc-84de-d4973b213827']}
