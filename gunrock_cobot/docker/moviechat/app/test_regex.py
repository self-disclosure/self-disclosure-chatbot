import re
import pytest

from movie_module_regex import *
from movie_opinion_detector import MovieOpinionDetector


@pytest.mark.parametrize("text", [
    "i haven't seen it",
    "i haven't watch it yet",
    "i've never seen bill mark in my life i said hallmark",
    "i've never heard of that"
])
def test_not_watched(text):
    assert match_regex(text, not_watched_regex) is True


@pytest.mark.parametrize("text", [
    "i saw it it's great",
])
def test_not_watched_false(text):
    assert match_regex(text, not_watched_regex) is False

@pytest.mark.parametrize("text", [
    "have i seen what",
    "what's the name of the movie",
    "which movie",
    "which movie are you talking about",
    "can you repeat the movie",
])
def test_clarify_movie_name(text):
    assert match_regex(text, clarify_movie_name) is True


@pytest.mark.parametrize("text", [
    "that's awesome but sad",
    "i enjoyed it but i was sad because iron man died",
    "i'm sad",
    "yeah i've seen it it's sad and good",
    "yeah but it was kind of sad",
    "i enjoyed watching it very much but it was sad",
    "cool but it was very sad",
    "this movie is very sad story",
    "when i watch that i cry the little bit",
    "annabelle found train almost made me cry",
    "i cried",
    "the saddest part is when iron man died"
])
def test_sad(text):
    assert match_regex(text, sad) is True


@pytest.mark.parametrize("text", [
    "i like the music",
    "the songs",
    "soundtrack"
])
def test_like_music(text):
    assert match_regex(text, MovieOpinionDetector.like_music) is True


@pytest.mark.parametrize("text", [
    "all the action",
    "fighting",
    "the final fight scene"
])
def test_like_music(text):
    assert match_regex(text, MovieOpinionDetector.like_action) is True


@pytest.mark.parametrize("text", [
    "i like the scene where all the female heroes got together and up the menu",
    "i like the scenes with the winklevoss twins",
    "uh when when the eddie went to the himalayas",
    "i like the end when she when were got her own lightsaber it was it's pretty cool to see that she talked about lightsaber",
    "that at the end marlon finally understands what nemo wanted in his life"
])
def test_like_specific_scene(text):
    assert match_regex(text, MovieOpinionDetector.like_specific_scene) is True


@pytest.mark.parametrize("text", [
    "i actually love the main character in the main actor and actress",
    "dylan o'brien's acting",
    "i like the actors that were in it and the writing of the dialog",
    "the lead actor",
    "i like the new cast"
])
def test_like_actor(text):
    assert match_regex(text, MovieOpinionDetector.like_actor) is True


@pytest.mark.parametrize("text", [
    "the comedy",
    "i think that the fact that it was very like interesting and not so boring",
    "i loved how they were so funny",
    "i liked how funny it was",
    "i like to laugh",
    "it's fun and i like how to do a lot of the"
])
def test_like_funny(text):
    assert match_regex(text, MovieOpinionDetector.like_funny) is True


@pytest.mark.parametrize("text", [
    "i like the character john coffee",
    "i like the main character emmet the he can be funny sometimes",
])
def test_like_character(text):
    assert match_regex(text, MovieOpinionDetector.like_character) is True


@pytest.mark.parametrize("text", [
    "i'd probably watch it again",
    "yes i will watch that movie again",
    "i would definitely definitely watch it again it was amazing",
    "i watch it fifteen times"
])
def test_rewatch_movie_positive(text):
    assert match_regex(text, rewatch_positive) is True


@pytest.mark.parametrize("text", [
    "probably wouldn't",
    "i would not",
])
def test_rewatch_movie_positive_false(text):
    assert match_regex(text, rewatch_positive) is False


@pytest.mark.parametrize("text", [
    "i watched it alone",
    "a loan",
    "on a plane"
])
def test_watch_alone(text):
    assert match_regex(text, watch_alone) is True


@pytest.mark.parametrize("text", [
    "with my dad",
    "with my mom",
    "i watch at my sister avelin",
    "with others",
    "with the others",
    "i watch it at the theater with my family"
])
def test_watch_with_others(text):
    assert match_regex(text, watch_with_others) is True


@pytest.mark.parametrize("text", [
    "i've watched it alone and with the others",
    "well watched it at the theater with my family and then later i've watched it a couple more times by myself"
])
def test_both_watch_alone_and_with_others(text):
    assert match_regex(text, watch_alone) is True
    assert match_regex(text, watch_with_others) is True


@pytest.mark.parametrize("text", [
    "normally watch",
    "would watch"
])
def test_normally_watch(text):
    assert match_regex(text, normally_watch) is True


@pytest.mark.parametrize("text", [
    "normally not watch",
    "would not watch"
])
def test_normally_watch_false(text):
    assert match_regex(text, normally_watch) is False


@pytest.mark.parametrize("text", [
    "is it a scary movie",
    "no is it a comedy",
    "who is it by"
])
def test_follow_up_question(text):
    assert match_regex(text, follow_up_question) is True


@pytest.mark.parametrize("text", [
    "it is",
])
def test_follow_up_question_false(text):
    assert match_regex(text, follow_up_question) is False


@pytest.mark.parametrize("text", [
    "popular character",
    "popular movie character",
    "character",
    "star war character",
    "well loved disney character",
    "loveable character",
    "cartoon character"
])
def test_character_in_concept(text):
    assert match_regex(text, MovieOpinionDetector.regex_concept_character) is True


def match_regex(text, regex_pattern):
    match = re.match(regex_pattern, text)
    return match is not None
