import re
import logging

import template_manager
from typing import Optional, List
template_movies = template_manager.Templates.movies
from question_handling.quetion_handler import QuestionHandler, QuestionResponse, QuestionResponseTag
from movie_preference_predictor import MoviePreferencePredictor
from movie_module_regex import clarify_movie_name
from nlu.question_detector import QuestionDetector
from nlu.dataclass.returnnlp import ReturnNLP, ReturnNLPSegment
from tracker import Tracker, Subdialog
from dispatcher import Dispatcher
from self_disclosure_study.movie_module_context import States


class MovieQuestionHandler:
    def __init__(self, complete_user_utterance: str, returnnlp: Optional[List[dict]], tracker: Tracker, dispatcher: Dispatcher):
        self.text = complete_user_utterance
        self.returnnlp = ReturnNLP.from_list(returnnlp)
        self.tracker = tracker
        self.dispatcher = dispatcher
        self.question_detector = QuestionDetector(self.text, returnnlp)
        self.question_handler = QuestionHandler(
            self.text, returnnlp, 0.83, self.tracker.system_acknowledgement, self.tracker.user_attributes)
        self.question_text = self.question_detector.get_question_segment_text()

    def handle_question(self) -> Optional[QuestionResponse]:
        logging.debug("[handle_question]: ")
        if not self.question_detector.has_question():
            return None

        if self.dispatcher.ack:
            # if acknowledgement exists, that means it's already handled properly
            return None

        self.question_text = self.question_detector.get_question_segment_text()

        res = self.handles_topic_specific_question()
        if res:
            return QuestionResponse(response=res)

        if self.should_skip_question():
            return None
        # if self.is_unanswerable_question():
        #     logging.debug("[Movie QA]: is_unanswerable_question")
        #     # return self.question_handler.generate_i_dont_know_response()
        #     return QuestionResponse(response=self.question_handler.generate_i_dont_know_response())

        res = self.handle_general_question()
        if res:
            return res
        return None

    def is_unanswerable_question(self):
        if self.question_detector.is_ask_back_question() or self.question_detector.is_follow_up_question():
            return True
        return False

    def should_skip_question(self):
        if re.search(r"(talk|chat|know) about.* movie(s|)$",self.question_text):
            return True  # TODO: improve logic here


    def handles_topic_specific_question(self) -> bool:
        """
        :return: True if fsm should not move on, False if should move on
        """
        if not self.question_detector.has_question():
            return False

        if re.search(r"you (\bsee|\bwatch|\bsaw).* (movie|\bit\b)", self.question_text) and \
                self.tracker.current_state in [States.ASK_LIKED_MOVIE, States.MOVIE_NAME_GROUNDING]:
            self.dispatcher.ack = "Yes, I've seen it. "
            return False
        elif re.search(r"^what's .* rated$|rating", self.question_text) and \
             self.tracker.current_state in [States.RECOMMEND_MOVIE, States.ASK_IF_INTERESTED_IN_RECOMMENDED_MOVIE]:  # todo: improve detection

            movie_id = self.tracker.module_context.proposed_movie_ids[-1]

            if movie_id:
                movie_info = MoviePreferencePredictor().get_movie_info(movie_id)
                logging.debug("[handles_topic_specific_question]: movie_info: {}".format(movie_info))

                rating = movie_info["movie_vote_average"]
                if rating:
                    self.dispatcher.ack = "It's rated {} in TMDB. ".format(rating)
                    return False

        return False

    def is_clariying_movie_name(self):
        return re.search(clarify_movie_name, self.text)

    def handle_general_question(self) -> Optional[QuestionResponse]:
        answer = self.question_handler.handle_question()
        logging.debug("[handle_general_question]: {}".format(answer.response))
        return answer
