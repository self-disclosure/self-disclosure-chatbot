import logging

from dataclasses import dataclass, asdict, field
import template_manager


class Dispatcher:

    template_map = {
        'factual': template_manager.Templates.self_disclosure_movie_factual,
        'cognitive': template_manager.Templates.self_disclosure_movie_cognitive,
        'emotional': template_manager.Templates.self_disclosure_movie_emotional
    }

    def __init__(self, user_attributes):
        self.user_attributes = user_attributes
        self.response_text = ResponseText()

        self.get_self_disclosure_level()

        self.template = self.template_map[self.get_self_disclosure_level()]
        logging.info(f" template: {self.template}")

    def get_self_disclosure_level(self):
        level = getattr(self.user_attributes, "self-disclosure-study-current-level", "factual")
        logging.info(f"level {level}")
        return level

    def generate_response(self, selector, slots=None):
        if slots is None:
            slots = {}
        return self.template.utterance(
            selector=selector,
            slots=slots,
            user_attributes_ref=self.user_attributes)

    def propose_topic(self, selector, slots=None):
        if slots is None:
            slots = {}
        topic_proposal_template = template_manager.Templates.transition
        return topic_proposal_template.utterance(
            selector=selector,
            slots=slots,
            user_attributes_ref=self.user_attributes)

    @property
    def ack(self):
        return self.response_text.ack

    @ack.setter
    def ack(self, value: str):
        self.response_text.ack = value

    @property
    def transition(self):
        return self.response_text.transition

    @transition.setter
    def transition(self, value: str):
        self.response_text.transition = value

    @property
    def main_content(self):
        return self.response_text.main_content

    @main_content.setter
    def main_content(self, value: str):
        self.response_text.main_content = value


@dataclass
class ResponseText:
    ack: str = ""
    transition: str = ""
    main_content: str = ""