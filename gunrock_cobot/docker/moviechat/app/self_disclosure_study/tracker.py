import re
from typing import List, Optional
from enum import Enum
import logging

from actor_opinion_detector import ActorOpinionDetector
from actor_subdialog import ActorSubdialogManager
from self_disclosure_study.movie_module_context import MovieModuleContext, CurrentWatchedMovieSubdialogContext, States
from movie_module_regex import not_sure_regex, follow_up_question
from movie_opinion_detector import MovieOpinionDetector
from movietv_command_detector import MovieCommandDetector
from nlu.constants import DialogAct, Positivity, TopicModule
from nlu.dataclass.returnnlp import ReturnNLP, ReturnNLPSegment
from nlu.question_detector import QuestionDetector
from selecting_strategy.module_selection import ModuleSelector, ProposeContinue
from movie_module_regex import not_watched_regex

class Subdialog(Enum):
    ASK_MOVIE_NAME = "ask_movie"  # ask what movie user want to discuss
    WATCHED_MOVIE = "watched_movie"
    PROPOSE_MOVIE = "propose_movie"
    GENERAL_CHITCHAT = "general"
    ACTOR = "actor"

class Tracker:
    def __init__(self, input_data, user_attributes):
        self.a_b_test = input_data['a_b_test'][-1]
        self.user_id = input_data["user_id"][0]
        self.system_acknowledgement = input_data.get('system_acknowledgement', [{}])[0]
        self.text = input_data["text"][0]
        self.last_response = input_data['last_response'][0]
        self.returnnlp = ReturnNLP.from_list(input_data['returnnlp'][0])
        self.user_attributes = user_attributes
        context = input_data.get("moviechat_user", {})
        self.module_context = MovieModuleContext.from_dict(input_data.get("moviechat_user", {}))

        self.module_selector = ModuleSelector(user_attributes)
        self.question_detector = QuestionDetector(self.text, input_data['returnnlp'][0])
        self.command_detector = MovieCommandDetector(self.text, input_data['returnnlp'][0])
        self.movie_opinion_detector = MovieOpinionDetector(self.text, input_data['returnnlp'][0])
        self.actor_opinion_detector = ActorOpinionDetector(self.text, input_data['returnnlp'][0])
        self.user_engagement_detector = EngagementDetector(self.text, input_data['returnnlp'][0])

    def get_abtest_condition(self):
        ab_test_condition = getattr(self.user_attributes, "self-disclosure-study-abtest", "A")
        return ab_test_condition

    @property
    def is_first_conversation(self):
        s = set(self.module_selector.used_topic_modules)
        s -= {TopicModule.MOVIE.value}
        result = not s
        return result

    @property
    def enter_from_other_module(self):
        last_topic = self.module_selector.last_topic_module
        return last_topic != TopicModule.MOVIE.value

    @property
    def is_resume_mode(self) -> bool:
        return self.enter_from_other_module and self.current_state != States.INIT

    @property
    def is_first_entry(self) -> bool:
        return self.enter_from_other_module and self.current_state == States.INIT

    def yes_only(self):
        return self.ans_yes() and len(self.returnnlp) == 1

    def ans_yes(self):
        return self.returnnlp.has_dialog_act(DialogAct.POS_ANSWER) or self.returnnlp.has_intent("ans_positive")

    def no_only(self):
        return self.returnnlp.answer_positivity == Positivity.neg and \
            len(self.returnnlp) == 1 and \
            len(self.text.split()) <= 2

    def ans_not_sure_only(self):
        return self.ans_not_sure() and len(self.returnnlp) == 1

    def back_channeling_only(self):
        return self.returnnlp.has_dialog_act(DialogAct.BACK_CHANNELING) and len(self.returnnlp) == 1

    def ans_not_sure(self):
        return self.returnnlp.has_dialog_act(DialogAct.OTHER_ANSWERS) or \
               self.returnnlp.has_intent("ans_unknown") or \
               re.search(not_sure_regex, self.text)

    def is_follow_up_question(self):
        return self.question_detector.has_question() and \
        re.match(follow_up_question, self.text)

    def not_watched(self):
        if re.search(not_watched_regex, self.text):
            return True
        return False

    @property
    def current_state(self):
        return self.module_context.current_state

    @current_state.setter
    def current_state(self, state):
        self.module_context.current_state = state

    @property
    def current_subdialog(self):
        ask_movie_name = [
            States.ASK_RECENT_SEEN_MOVIE,
            States.ASK_MOVIE_TO_DISCUSS,
            States.ASK_RECENT_SEEN_MOVIE_FOLLOW_UP,
            States.ASK_LIKED_MOVIE_IN_GENRE,
            States.MOVIE_NAME_GROUNDING
        ]

        watched_movie_subdialog = [
            States.PROVIDE_WATCHED_MOVIE_TRIVIA,
            States.ASK_WATCHED_MOVIE_QUESTION,
            States.PROVIDE_WATCHED_MOVIE_TRIVIA
       ]

        propose_movie_subdialog = [
            States.PROPOSE_MOVIE,
            States.ASK_IF_INTERESTED_IN_RECOMMENDED_MOVIE,
            States.PROVIDE_MOVIE_PLOT_SUMMARY
        ]

        general_chitchat_subdialog = [
            States.ASK_FAVORITE_MOVIE_GENRE,
            States.ASK_FAVORITE_ACTOR,
            States.ASK_WATCH_REVIEW_OR_TRAILER,
            States.CONFIRM_LIKED_MOVIE_GENRE
        ]

        actor_subdialog = [
            States.PROVIDE_ACTOR_TRIVIA,
            States.ASK_ACTOR_QUESTION
        ]

        if self.current_state in watched_movie_subdialog:
            return Subdialog.WATCHED_MOVIE
        elif self.current_state in propose_movie_subdialog:
            return Subdialog.PROPOSE_MOVIE
        elif self.current_state in general_chitchat_subdialog:
            return Subdialog.GENERAL_CHITCHAT
        elif self.current_state in actor_subdialog:
            return Subdialog.ACTOR
        elif self.current_state in ask_movie_name:
            return Subdialog.ASK_MOVIE_NAME

    @property
    def propose_continue(self):
        return self.module_context.propose_continue

    @propose_continue.setter
    def propose_continue(self, value: ProposeContinue):
        self.module_context.propose_continue = value.value

    @property
    def system_ack_tag(self):
        if self.system_acknowledgement:
            return self.system_acknowledgement.get("output_tag")
        return ""

    @property
    def system_ack_text(self):
        if self.system_acknowledgement:
            return self.system_acknowledgement.get("ack")
        return ""

    @property
    def liked_genre_ids(self):
        return self.module_context.movie_user_profile.liked_genre_ids

    @liked_genre_ids.setter
    def liked_genre_ids(self, value: List[int]):
        self.module_context.movie_user_profile.liked_genre_ids = value

    @property
    def liked_movie_ids(self):
        return self.module_context.movie_user_profile.liked_movie_ids

    @liked_movie_ids.setter
    def liked_movie_ids(self, value: List[int]):
        self.module_context.movie_user_profile.liked_movie_ids = value

    @property
    def liked_actor_names(self):
        return self.module_context.movie_user_profile.liked_actor_names

    @liked_actor_names.setter
    def liked_actor_names(self, value: List[str]):
        self.module_context.movie_user_profile.liked_actor_names = value

    @property
    def liked_actor_ids(self):
        return self.module_context.movie_user_profile.liked_actor_ids

    @liked_actor_ids.setter
    def liked_actor_ids(self, value: List[str]):
        self.module_context.movie_user_profile.liked_actor_ids = value

    @property
    def current_actor_name(self):
        return self.module_context.current_actor_name

    @current_actor_name.setter
    def current_actor_name(self, actor_name: str):
        self.module_context.current_actor_name = actor_name

    @property
    def current_movie_id(self):
        return self.module_context.current_movie_id

    @current_movie_id.setter
    def current_movie_id(self, movie_id: int):
        self.module_context.current_movie_id = movie_id

    @property
    def detected_movies_info(self) -> List[dict]:
        return self.module_context.detected_movie_infos

    @detected_movies_info.setter
    def detected_movies_info(self, movies_info: List[dict]):
        self.module_context.detected_movie_infos = movies_info

    @property
    def movie_name_grounding_count(self):
        return self.module_context.movie_name_grounding_count

    @movie_name_grounding_count.setter
    def movie_name_grounding_count(self, count: int):
        self.module_context.movie_name_grounding_count = count

    @property
    def current_movie_info(self):
        return self.module_context.current_movie_info

    @current_movie_info.setter
    def current_movie_info(self, movie_info: dict):
        self.module_context.current_movie_info = movie_info

    # @property
    # def current_watched_movie_discussion_mode(self) -> WatchedMovieDiscussionMode:
    #     return WatchedMovieDiscussionMode(self.cobot_module_context.current_watched_movie_subdialog)
    #
    # @current_watched_movie_discussion_mode.setter
    # def current_watched_movie_discussion_mode(self, mode: WatchedMovieDiscussionMode):
    #     self.cobot_module_context.current_watched_movie_discussion_mode = mode

    @property
    def current_watched_movie_subdialog_context(self) -> CurrentWatchedMovieSubdialogContext:
        return self.module_context.current_watched_movie_subdialog


    @current_watched_movie_subdialog_context.setter
    def current_watched_movie_subdialog_context(self, value: dict):
        self.module_context.current_watched_movie_subdialog = value

    @property
    def mentioned_movie_ids(self):
        return self.module_context.mentioned_movie_ids

    @mentioned_movie_ids.setter
    def mentioned_movie_ids(self, movie_ids: List[int]):
        self.module_context.mentioned_movie_ids = movie_ids


class EngagementDetector:
    def __init__(self, user_complete_utterance, returnnlp: Optional[List[dict]]):
        self.user_complete_utterance = user_complete_utterance
        self.returnnlp: ReturnNLP = ReturnNLP.from_list(returnnlp)

    def is_short_or_unengaged_answer(self):
        """
        If is yes_no only or back channeling
        :return:
        """
        if len(self.returnnlp) == 1:
            seg = self.returnnlp[0]
            return self.has_pos_answer(seg) \
                or self.has_neg_answer(seg) \
                or self.has_not_sure_answer(seg) \
                or self.has_dont_care_answer(seg) \
                or self.has_back_channeling(seg)
        else:
            for seg in self.returnnlp:
                if not (self.has_neg_answer(seg) or
                        self.has_not_sure_answer(seg) or
                        self.has_dont_care_answer(seg) or
                        self.has_back_channeling(seg)):
                    return False
            return True

    def is_engaged_answer(self):
        return self.returnnlp.has_dialog_act(DialogAct.OPINION) or \
               self.returnnlp.has_dialog_act(DialogAct.COMMENT)


    @staticmethod
    def has_pos_answer(segment: ReturnNLPSegment):
        return segment.has_dialog_act(DialogAct.POS_ANSWER) or segment.has_intent("ans_positive")

    @staticmethod
    def has_neg_answer(segment: ReturnNLPSegment):
        return segment.has_dialog_act(DialogAct.NEG_ANSWER) or segment.has_intent("ans_negative")

    @staticmethod
    def has_not_sure_answer(segment: ReturnNLPSegment):
        return segment.has_dialog_act(DialogAct.OTHER_ANSWERS) or segment.has_intent("ans_unknown")

    @staticmethod
    def has_dont_care_answer(segment: ReturnNLPSegment):
        return segment.has_intent("ans_dont_care")

    @staticmethod
    def has_back_channeling(segment: ReturnNLPSegment):
        return segment.has_dialog_act(DialogAct.BACK_CHANNELING) and segment.text not in ["wow"]



