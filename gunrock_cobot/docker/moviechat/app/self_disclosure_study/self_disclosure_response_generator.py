import logging
from types import SimpleNamespace
from self_disclosure_study.movie_automaton import MovieTVAutomaton


def handle_message(msg):

    user_attributes = SimpleNamespace(**{
        "session_id": msg["session_id"],
        "user_profile": msg["user_profile"],
        "a_b_test": msg["a_b_test"][0],
        "template_manager": msg["template_manager"],
        "module_selection": msg["module_selection"],
        "previous_modules": msg["previous_modules"],
        "blender_start_time": msg["blender_start_time"],
        "self-disclosure-study-abtest": msg["self-disclosure-study-abtest"],
        "self-disclosure-study-current-level": msg["self-disclosure-study-current-level"]
    })

    response_dict = dict()

    try:
        movie_automaton = MovieTVAutomaton(msg, user_attributes)
        result = movie_automaton.transduce()

        response_dict["user_attributes"] = user_attributes.__dict__
        response_dict["response"] = result["response"]
        response_dict["user_attributes"]["moviechat_user"] = result["moviechat_user"]

    except Exception as e:
        logging.info(f"Response generator error: {e}")

    return response_dict


def get_required_context():
    return ['text']
