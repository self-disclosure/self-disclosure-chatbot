class BaseError(Exception):

    fmt = "An unspecified error occurred"

    def __init__(self, **kwargs):
        msg = self.fmt.format(**kwargs)
        Exception.__init__(self, msg)
        self.kwargs = kwargs


class ParamValidationError(BaseError):
    
    fmt = 'Parameter validation failed:\n{report}'


class UnknownAPIError(BaseError):

    fmt = (
        'Unknown API "{name}". Must be one '
        "of: {choices}"
    )