import requests
import json
import os
from collections import OrderedDict
from .constant import Constant

from .validate import validate_parameters
from .exceptions import UnknownAPIError

class Model(object):
    """
    Parse an API specification file exported from AWS API Gateway as a list of operation description.
    Store operations description in a dictionary. 
    """

    def __init__(self, model_definition, api_key=None, timeout_in_millis=None):
        if api_key is None:
            self._api_key = os.environ.get('COBOT_API_KEY')
        else:
            self._api_key = api_key

        self._headers={
            "Content-Type": "application/json;charset=utf-8",
            "x-api-key": self._api_key,
        }
        self._operation_map = self._get_operation_map(model_definition)

        if timeout_in_millis is None:
            self._timeout_in_millis = Constant.client_timeout_in_millis
        else:
            self._timeout_in_millis = timeout_in_millis
    
    @property
    def operation_map(self):
        return self._operation_map

    def _get_base_url(self, model_definition):
        base_url = 'https://{host}{basePath}'.format(
            host=model_definition['host'],
            basePath =model_definition['basePath']
        )
        return base_url

    def _get_operation_map(self, model_definition):
        operation_map = {}
        for path, path_item in model_definition.get('paths', {}).items():
            for method, operation_object in path_item.items():
                operation = Operation(
                    name=operation_object.get('interface', None),
                    path=self._get_base_url(model_definition) + path,
                    method=method,
                    parameter_definition=self._get_parameter_definition(
                        operation_object.get('parameters'),
                        model_definition.get('definitions')
                    )
                )
                operation_map[operation.name] = operation
        return operation_map
    
    def call_api(self, api_name):
        """
        Generate a https request builder.
        The request builder will load api's input to HTTPS request body.
        """
        if api_name not in self._operation_map:
            raise UnknownAPIError(name=api_name, choices=list(self._operation_map.keys()))
        return self._operation_map[api_name].build_request(self._headers, self._timeout_in_millis)

    @classmethod
    def _get_parameter_definition(cls, parameters, definitions):
        parameter_definition_name = None
        for parameter in parameters:
            if parameter.get('in', None) == 'body':
                definition_name = parameter.get('schema', {}).get('$ref', '')
                parameter_definition_name = definition_name.split('/')[-1]
                break
        
        parameter_definition = Definition(parameter_definition_name, definitions)
        return parameter_definition

    def merge(self, model):
        self._operation_map.update(model.operation_map)
        return self
        

class Operation(object):
    """
    Store one operation description and build a https request.
    """
    def __init__(self, name, path, method, parameter_definition):
        self._name = name
        self._path = path
        self._method = method
        self._parameter_definition = parameter_definition
    
    @property
    def name(self):
        return self._name

    def _build_parameter(self, **params):
        validate_parameters(params, self._parameter_definition)
        return json.dumps(params)

    def build_request(self, headers, timeout_in_millis):
        """
        Generate a HTTPS request builder.
        """
        sender = getattr(requests, self._method)

        def send(**kwarg):
            # Inherit timeout from client if it's not set.
            api_timeout_in_millis = kwarg.get("timeout_in_millis", timeout_in_millis)
            kwarg.pop("timeout_in_millis", None)
            response = sender(
                url=self._path,
                headers=headers,
                data=self._build_parameter(**kwarg),
                timeout=api_timeout_in_millis / 1000.0
            )
            # raise an HTTPError if the HTTP request returned an unsuccessful status code.
            # requests library doc reference: http://docs.python-requests.org/en/master/user/quickstart/#errors-and-exceptions
            response.raise_for_status()
            return response.json()

        return send


class Definition(object):

    META_DATA = {'minLength', 'maxLength'}

    def __init__(self, definition_name, definition_model):
        self._definition_name = definition_name
        self._definition = definition_model.get(definition_name, {})
        self._definition_model = definition_model

        self.type_name = self._definition.get('type')

    @property
    def meta_data(self):
        meta_data_map = {}
        for name, value in self._definition.items():
            if name in self.META_DATA:
                meta_data_map[name] = value
        return meta_data_map

    @property
    def required(self):
        return self._definition.get('required', [])
    
    @property
    def properties(self):
        cls = self.__class__
        properties = OrderedDict()
        for name, definition in self._definition.get('properties', {}).items():
            definition_name = definition.get("$ref", {})
            properties[name] = cls(definition_name, self._definition_model)
        return properties

    @property
    def items(self):
        cls = self.__class__
        definition_name = self._definition.get('items', {}).get('$ref')
        return cls(definition_name, self._definition_model)
