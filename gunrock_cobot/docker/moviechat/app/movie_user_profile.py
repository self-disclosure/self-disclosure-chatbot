from typing import List, Dict, Any
from dataclasses import dataclass, asdict, field


@dataclass
class MovieUserProfile:
    liked_movie_ids: List[int] = field(default_factory=list)
    liked_genre_ids: List[int] = field(default_factory=list)
    liked_actor_names: List[str] = field(default_factory=list)
    liked_actor_ids: List[str] = field(default_factory=list)

    @classmethod
    def from_dict(cls, ref: Dict[str, Any]):
        result = cls(liked_movie_ids=ref.get("liked_movie_ids", []),
                     liked_genre_ids=ref.get("liked_genre_ids", []),
                     liked_actor_names=ref.get("liked_actor_names",[]),
                     liked_actor_ids=ref.get("liked_actor_ids", []))
        return result
