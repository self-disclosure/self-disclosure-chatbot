import os
from timeit import default_timer as timer

from movietv_qa import MovieQuestionHandler
from movietv_command import *
import context_manager
from actor_dialog import *
from movie_dialog import *
from chitchat_dialog import *
from nlu.dataclass.returnnlp import ReturnNLP
from movie_preference_extractor import MovieExtractor, ActorExtractor, GenreExtractor
from movie_preference_predictor import MoviePreferencePredictor
from nlu.question_detector import QuestionDetector
from nlu.constants import TopicModule, Positivity
from nlu.dataclass.returnnlp import ReturnNLP, ReturnNLPSegment
from question_handling.quetion_handler import QuestionHandler
from movietv_command_detector import MovieCommandDetector
from selecting_strategy.module_selection import ModuleSelector
from nlu.constants import Positivity
import template_manager


template_movies = template_manager.Templates.movies

# working environment
WORKING_ENV = "local"
if os.environ.get('STAGE') == 'PROD' or os.environ.get('STAGE') == 'BETA':
    WORKING_ENV = os.environ.get('STAGE').lower()
else:
    WORKING_ENV = "local"

# Import regex
from movie_module_regex import *
from movietv_utils import Genre

"""
All of the previous states written by Yi Mang Yang
"""
from types import SimpleNamespace

GENRE_NEVER_ASKED = -1
GENRE_ASKED_BUT_NOT_DETECTED = -2

BOT_FAVORITE_GENRE = [
    Genre.COMEDY.value,
    Genre.ACTION.value,
    Genre.SCIENCE_FICTION.value,
]

class MovieTVAutomaton:

    def __init__(self, app_logger):
        self.logger = app_logger
        self.__bind_states()
        self.__bind_transitions()
        self.__bind_interruptions()
        self.cfgs = parse_configs()
        self.plot_summary = None
        self.template = template_manager.Templates.movies
        self.prefix = ""
        self.response = ""

    def __bind_states(self):
        self.states_mapping = {
            "s_intro": self.s_intro,
            # special state that just returns the response from the transition
            # callback function
            "s_echo": self.s_echo,  # kind of a dummy. In these cass, t_xx already sets the response and the next t.
            "s_interesting_comment": self.s_interesting_comment,  # Q
            "s_ask_movietv": self.s_ask_movietv,
            "s_ask_exit": self.s_ask_exit,
            "s_chitchat": self.s_chitchat,
            "s_movie_chitchat": self.s_movie_chitchat,
            "s_movie_grounding": self.s_movie_grounding,
            "s_subdialog": self.s_subdialog,
            "s_ask_favourite_movie_genre": self.s_ask_favourite_movie_genre,
            "s_ask_liked_movie_in_genre": self.s_ask_liked_movie_in_genre,
            "s_propose_movie": self.s_propose_movie,
            "s_repeat_movie_name": self.s_repeat_movie_name,
            "s_ask_user_interest_in_bot_proposed_movie": self.s_ask_user_interest_in_bot_proposed_movie,
            "s_provide_plot_summary": self.s_provide_plot_summary,
            "s_try_switch_from_tv_to_movie": self.s_try_switch_from_tv_to_movie
        }

    def __bind_transitions(self):
        self.transitions_mapping = {
            "t_init": self.t_init,
            "t_change_subtopic": self.t_change_subtopic,
            "t_resume": self.t_resume,  # resume after module isn't sequentially selected
            "t_interesting_comment": self.t_interesting_comment,
            "t_ask_movietv": self.t_ask_movietv,
            "t_ask_exit": self.t_ask_exit,
            "t_chitchat": self.t_chitchat,
            "t_movie_chitchat": self.t_movie_chitchat,
            "t_movie_grounding": self.t_movie_grounding,
            "t_subdialog": self.t_subdialog,
            "t_start_actor_dialog": self.t_start_actor_dialog,
            "t_start_chitchat_dialog": self.t_start_chitchat_dialog,
            "t_start_movie_chitchat_dialog": self.t_start_movie_chitchat_dialog,
            "t_ask_favorite_movie_genre": self.t_ask_favorite_movie_genre,
            "t_ask_user_about_a_movie_they_like_in_their_favourite_genre": self.t_ask_user_about_a_movie_they_like_in_their_favourite_genre,
            "t_propose_movie": self.t_propose_movie,
            "t_ask_user_interest_in_bot_proposed_movie": self.t_ask_user_interest_in_bot_proposed_movie,
            "t_provide_plot_summary": self.t_provide_plot_summary,
            "t_try_switch_from_tv_to_movie": self.t_try_switch_from_tv_to_movie

        }

    def __bind_interruptions(self):
        self.interruptions_mapping = {
            "i_ask_continue_movie": self.i_ask_continue_movie,
            # if we want to confirm if the user wants to continue this module
            "i_ask_chat_actor": self.i_ask_chat_actor,  # if we guess the user want to talk about a actor
            "i_ask_movie_name_reply": self.i_ask_movie_name_reply,  # probably not using
        }

    def s_ask_favourite_movie_genre(self):
        self.moviechat_user["current_state"] = "s_ask_favourite_movie_genre"
        self.moviechat_user["favorite_genre_ids"] = [GENRE_ASKED_BUT_NOT_DETECTED]
        self.response = self.utt(["ask_genre", "ask_favourite_movie_genre"], slots={"A": "comedy", "B": "action", "C": "science fiction"})
        return "t_ask_favorite_movie_genre"

    def ask_favorite_genre_or_propose_movie(self):
        # Ask the favourite movie genre if we haven't
        if self.should_ask_favorite_genre():
            return "s_ask_favourite_movie_genre"
        return "s_propose_movie"

    def should_ask_favorite_genre(self):
        return len(self.get_user_favorite_genre_ids()) == 0

    def get_user_favorite_genre_ids(self) -> List:
        return self.moviechat_user.get("favorite_genre_ids", [])

    def t_ask_favorite_movie_genre(self):
        if "ans_same" in self.returnnlp.flattened_lexical_intent:
            self.moviechat_user["favorite_genre_ids"] = BOT_FAVORITE_GENRE
            return "s_ask_liked_movie_in_genre"

        res = self.t_normal_flow("t_ask_favorite_movie_genre")
        if res:
            return res

        self.prefix = self.utt(["ask_genre", "unknown_genre"])


        if re.search(not_sure_regex, self.text):
            self.prefix = self.utt(["ask_genre", "no_favourite"])
        elif re.search(like_all, self.text):
            self.prefix = self.utt(["ask_genre", "like_all"])
        else:
            self.prefix = self.utt(["ask_genre", "unknown_genre"])

        self.prefix += self.utt(["transition_change_subtopic"])

        return "s_propose_movie"


    def s_ask_liked_movie_in_genre(self):
        self.moviechat_user["current_state"] = "s_ask_liked_movie_in_genre"

        first_favorite_genre_id = self.get_user_favorite_genre_ids()[0]

        if first_favorite_genre_id in BOT_FAVORITE_GENRE:
            prefix_template = ["ask_genre", "ack", "same"]
        else:
            prefix_template = ["ask_genre", "ack", "pos_ack"]

        self.prefix = self.utt(prefix_template)

        genre_name = GenreExtractor.Genre(first_favorite_genre_id)
        self.response = self.utt(["ask_genre", "ask_movie_in_genre"],
                                 {"genre": genre_name})
        # return "t_ask_user_about_a_movie_they_like_in_their_favourite_genre"
        return "t_ask_movietv"

    def get_favorite_genre_ids(self):
        if "favorite_genre_ids" in self.moviechat_user:
            return int(self.moviechat_user["favorite_genre_ids"])
        return None

    def t_ask_user_about_a_movie_they_like_in_their_favourite_genre(self):
        self.prefix = self.utt(["ask_genre", "pos_ack"])
        return self.start_chitchat_dialog()

    def transduce(self, input_data, user_attributes):
        # self.user_attributes_hash = UserAttributes(input_data['prev_hash'])
        self.template_manager_hash = user_attributes
        self.module_selector = ModuleSelector(user_attributes)

        # extract info from input_data
        if WORKING_ENV == "local":
            self.text = re.sub("open gunrock and", "", input_data["text"][0]).strip()
            self.logger.info(
                "[movietv_automaton.transduce] Input data: %s", json.dumps(input_data, indent=4))
        else:
            self.text = input_data["text"][0]

        if (input_data["features"][0]).get("coreference"):
            self.coref_text = input_data["features"][0]["coreference"].get(
                "text", input_data["text"][0])
        else:
            self.coref_text = self.text

        self.returnnlp = ReturnNLP.from_list(input_data['returnnlp'][0])
        self.question_detector = QuestionDetector(self.text, input_data['returnnlp'][0])
        self.question_handler = QuestionHandler(self.text,
                                                input_data['returnnlp'][0],
                                                0.83,
                                                input_data['system_acknowledgement'],
                                                self.template_manager_hash)
        self.command_detector = MovieCommandDetector(self.text, input_data['returnnlp'][0])
        self.features = input_data["features"][0]
        self.central_elem = input_data["features"][0]["central_elem"]
        self.dialog_act = input_data["features"][0]["dialog_act"]
        self.amz_dialog_act = input_data["features"][0].get("amz_dialog_act")
        self.ner = input_data["features"][0]["ner"]
        self.asr_correction = input_data["features"][0]["asrcorrection"]
        self.noun_phrase = input_data["features"][0]["noun_phrase"]
        self.knowledge = input_data["features"][0]["knowledge"]
        self.topic_keywords = input_data["features"][0]["topic_keywords"]
        self.sentiment = input_data["features"][0]["sentiment"]
        self.cobot_intents = input_data["features"][0]["intent_classify"]["lexical"] + \
                             input_data["features"][0]["intent_classify"]["topic"]
        # prev moviechat user attributes
        self.user_attributes = input_data["moviechat_user"]  # similar to moviechat_user for previous turn

        # prev cobot state info
        self.context_manager = input_data["moviechat_context"]  # previous turn
        self.sys_resp_type = input_data["resp_type"][1]
        self.user_id = input_data["user_id"][0]
        self.session_id = input_data["session_id"][0]
        self.current_loop = self.user_attributes.get("current_loop", {})  # actually the max number of turns
        self.a_b_test = input_data['a_b_test'][-1]
        self.logger.debug("a_b_test: {}".format(self.a_b_test))
        self.last_response = input_data['last_response'][0]
        self.logger.debug("last_response: {}".format(self.last_response))

        self.input_data = input_data
        self.prefix = ""

        self.context = context_manager.ContextManager(self.user_id)
        self.logger.info("context.movies: %s", self.context.movies)
        self.logger.info("context.current_subdialog_context: %s",
                         self.context.current_subdialog_context)

        # attributes to store in callback functions
        self.moviechat_user = {  # current turn
            "not_first_time": True,  # Q: a flag for having entered movie module
            "propose_continue": "CONTINUE",
            "propose_topic": None,
            "candidate_module": None,
            "tags": [],
        }  # info stored here will be stored in user attr
        self.moviechat_context = {  # just a dummy
            "last_module": True
        }  # info stored here will be stored in cobot current state

        # store response to return here
        self.t_context = {}  # t states can temporarily store info here for s states to use

        self.enter_from_other_module = False  # if we are switched from other modules

        # if a moviechat was not selected consecutively
        sys_continue_flow_types = {"hesitant", "incomplete", "clarify", "asr_error", "req_allow_ask_question", "req_allow_tell_sth"}
        previous_module_is_not_movie = self.context_manager[1] is None
        system_not_ask_for_clarification = self.sys_resp_type not in sys_continue_flow_types
        if previous_module_is_not_movie and self.user_attributes.get(
                "not_first_time") and system_not_ask_for_clarification:
            previous_turn_left_from_chitchat = input_data["moviechat_user"].get("current_transition") == "t_chitchat"
            if previous_turn_left_from_chitchat:
                # Q: what does context.chitchat look like?
                previous_chitchat_wasnt_finished = "current_context" in self.context.chitchat and "interrupted" in \
                                                   self.context.chitchat["current_context"]
                if previous_chitchat_wasnt_finished:
                    self.context.chitchat["current_context"]["interrupted"] = True
            current_transition = "t_resume"
            self.enter_from_other_module = True
        else:
            current_transition = input_data["moviechat_user"]["current_transition"]

        first_time_in_movie = current_transition == "t_init"
        if first_time_in_movie:
            self.enter_from_other_module = True

        # system level intent overrides everything

        self.logger.debug("before self.command_detector.detect_intent()")
        self.subtopic_intent = self.command_detector.detect_intents()

        if self.subtopic_intent:
            if current_transition == "t_chitchat":
                previous_chitchat_wasnt_finished = "current_context" in self.context.chitchat and "interrupted" in \
                                                   self.context.chitchat["current_context"]
                if previous_chitchat_wasnt_finished:
                    self.context.chitchat["current_context"]["interrupted"] = True
            current_transition = "t_change_subtopic"

        # interruption
        flow_interruption_state = self.user_attributes.get("flow_interruption")
        if flow_interruption_state and current_transition != "t_change_subtopic":
            current_transition = self.interruptions_mapping[flow_interruption_state](
                current_transition)
        self.moviechat_user["flow_interruption"] = False


        """
        Restore some attributes
        """
        if "has_asked_favourite_movie_genre" in self.user_attributes.keys():
            self.moviechat_user["has_asked_favourite_movie_genre"] = self.user_attributes[
                "has_asked_favourite_movie_genre"]


        if "favorite_genre_ids" in self.user_attributes.keys():
            self.moviechat_user["favorite_genre_ids"] = self.user_attributes["favorite_genre_ids"]

        if "proposed_movie_id" in self.user_attributes.keys():
            self.moviechat_user["proposed_movie_id"] = self.user_attributes["proposed_movie_id"]

        if "current_state" in self.user_attributes.keys():
            self.moviechat_user["current_state"] = self.user_attributes["current_state"]
            self.logger.debug("current state: ".format(self.moviechat_user["current_state"]))

        # if current_transition == "t_ask_favorite_movie_genre":
        #     self.moviechat_user["detected_favourite_genre_id"] = input_data["moviechat_user"]["detected_favourite_genre_id"]

        self.reset_flags(current_transition)
        self.current_t_transition = current_transition
        next_state = self.execute_current_transition(current_transition)
        self.execute_again = False
        next_transition = self.execute_state(next_state)

        # now, we usually should have a response ready. and the t state is stored for the next turn.
        # but in this case, the response is not ready, and we want to execute the t now.
        if self.execute_again:
            self.t_context = {}
            current_transition = next_transition
            self.reset_flags(current_transition)
            next_state = self.execute_current_transition(current_transition)
            next_transition = self.execute_state(next_state)

        self.context.save_context()
        # save transition for next user utterance
        self.moviechat_user["current_transition"] = next_transition
        self.moviechat_user["current_loop"] = self.current_loop  # integer. how many times
        self.response = self.prefix + self.response
        if len(self.response) > 650:  # hard filter for really long utterances
            self.logger.info("[MOVIECHAT_MODULE] Exception overwrote long response")
            self.response = self.utt(["confused", "lead"])
        return {
            "response": constructDynamicResponse(self.response),
            "moviechat_user": self.moviechat_user,
            "moviechat_context": self.moviechat_context,
            # "prev_hash": self.template_manager_hash.prev_hash,
            "template_manager": dict(self.template_manager_hash.template_manager),
        }

    def execute_state(self, state):
        start = timer()
        self.logger.info(
            "[movietv_automaton.transduce] Entering: %s", state)

        # s state returns t state
        next_transition = self.states_mapping[state]()

        self.logger.info(
            "[movietv_automaton.transduce] Exiting: %s, Time elapsed: %s",
            state,
            timer() - start)
        return next_transition

    def execute_current_transition(self, current_transition):
        start = timer()
        self.logger.info(
            "[movietv_automaton.transduce] Entering: %s", current_transition)
        # t state returns s state
        next_state = self.transitions_mapping[current_transition]()
        self.logger.info(
            "[movietv_automaton.transduce] Exiting: %s, Time elapsed: %s",
            current_transition,
            timer() - start)
        return next_state

    def utt(self, selector, slots=None):
        if slots is None:
            slots = {}
        return self.template.utterance(
            selector=selector,
            slots=slots,
            user_attributes_ref=self.template_manager_hash)

    def utt_propose_topic(self, selector, slots=None):
        if slots is None:
            slots = {}
        topic_proposal_template = template_manager.Templates.transition
        return topic_proposal_template.utterance(
            selector=selector,
            slots=slots,
            user_attributes_ref=self.template_manager_hash)

    # it would better for system to reset this
    def reset_flags(self, current_transition):
        if current_transition != "t_ask_movietv":
            self.user_attributes["ask_movietv_repeat"] = True
            self.moviechat_user["ask_movietv_repeat"] = True
        if current_transition != "t_subdialog" and self.user_attributes.get("current_subdialog") != "ChitchatDialog":
            self.user_attributes["current_subdialog"] = None
            self.user_attributes["subdialog_context"] = {}
            self.moviechat_user["current_subdialog"] = None
            self.moviechat_user["subdialog_context"] = {}
        if current_transition != "t_chitchat" and self.user_attributes.get(
                "current_subdialog") != "MovieDialog" and self.user_attributes.get(
                "current_subdialog") != "ActorDialog":
            self.user_attributes["current_subdialog"] = None
            self.user_attributes["subdialog_context"] = {}
            self.moviechat_user["current_subdialog"] = None
            self.moviechat_user["subdialog_context"] = {}

    def end_of_subdialog(self):
        current_subdialog = self.user_attributes["current_subdialog"]
        if current_subdialog == "ActorDialog":
            return self.end_actor_dialog()
        elif current_subdialog == "MovieDialog":
            return self.end_movie_dialog()

    def try_detect_and_acknowledge_question(self):
        pass

    def try_start_another_movie_dialog(self, strength=0, grounding=False):
        logging.info("try start movie dialog")
        self.text = self.text.lower().replace("zomebie", "zombie").replace("too", "two")

        if not self.text:
            return None

        movie_infos = MovieExtractor().extract_movies_v2(self.text, self.last_response)

        if not movie_infos:
            return None

        if not grounding or len(movie_infos) == 1:
            movie_info = movie_infos[0]
            if self.is_current_movie(movie_info):
                return None

            self.prefix = self.utt(["movie_dialog", "init_movie_ack"], {
                                  "movie_title": movie_info["movie_name"]})
            return self.start_movie_dialog(movie_info)

        # grounding
        self.update_movie_context_for_movie_grounding(movie_infos)
        return "s_movie_grounding"
    
    def update_movie_context_for_movie_grounding(self, movie_infos):
        movie_infos = movie_infos[:3]
        self.context.current_subdialog_context = {
            "movie_infos": movie_infos}
        self.current_loop = {"turns_left": len(movie_infos)}

    def try_actor_dialog(self, resume_state):
        actor_infos = ActorExtractor().extract_actor_with_info(self.ner, self.knowledge)

        if not actor_infos:
            return None

        potential_actor_names_from_strong_intent = \
            ActorExtractor().extract_potential_actor_name_with_strong_regex_intent(self.text)

        for potential_actor_name in potential_actor_names_from_strong_intent:
            for actor_info in actor_infos:
                actor_name_from_ner = actor_info["ner_term"].lower()
                if actor_name_from_ner in potential_actor_name:
                    if str(actor_info["id"]) != str(self.context.current_subdialog_context.get("actor_info", {}).get("id")):
                        if len(actor_name_from_ner.strip().split()) >= 2:
                            return self.start_actor_dialog(actor_info)
                        else:
                            self.t_context["next_transition"] = resume_state
                            actor_name = actor_info["name"]
                            if actor_name != self.user_attributes.get("last_propose_actor"):
                                self.update_context_and_response_when_current_and_previous_actor_unmatched(
                                    actor_info, actor_name)
                                return "s_echo"

        if len(actor_infos) > 0 and (self.sentiment != "neg" or "ans_like" in self.cobot_intents) and \
                len(actor_infos[-1]["ner_term"].strip().split()) > 1 and \
                str(actor_infos[-1]["id"]) != str(self.context.current_subdialog_context.get("actor_info", {}).get("id")):
            self.t_context["next_transition"] = resume_state
            actor_info = actor_infos[-1]
            actor_name = actor_infos[-1]["name"]
            if actor_name != self.user_attributes.get("last_propose_actor"):
                self.update_context_and_response_when_current_and_previous_actor_unmatched(actor_info, actor_name)
                return "s_echo"

        return None


    def update_next_transition_and_return_if_detected_actor_dffers_from_last_proposed_actor(self, resume_state, actor_info):
        actor_name = actor_info["name"]
        if actor_name != self.user_attributes.get("last_propose_actor"):
            self.update_context_and_response_when_current_and_previous_actor_unmatched(
                actor_info, actor_name)
            return "s_echo"
        return None


    def try_genre_dialog(self):
        results = GenreExtractor().extract_genre(self.text)
        if results:
            self.moviechat_user["favorite_genre_ids"] = results
            return "s_ask_liked_movie_in_genre"
        return None

    def try_jump_out_and_change_topic(self):
        for regex in [dont_talk_movie, talk_something_else]:
            if re.search(regex, self.text):
                return "s_ask_exit"
        return None

    def is_current_movie(self, movie_infos):
        return str(movie_infos["movie_id"]) == str(self.get_currently_discussed_movie_info().get("movie_id"))

    def get_currently_discussed_movie_info(self):
        return self.context.current_subdialog_context.get("movie_info", {})

    def get_currently_discussed_movie_id(self):
        current_movie_info = self.get_currently_discussed_movie_info()
        if current_movie_info:
            return int(self.get_currently_discussed_movie_info().get("movie_id"))
        return None

    def start_movie_dialog(self, movie_info):
        print("initiate_movie_dialog:", movie_info["movie_name"])
        self.moviechat_user["current_subdialog"] = "MovieDialog"
        self.user_attributes["current_subdialog"] = "MovieDialog"
        set_movie_dialog_init_context(self.context, movie_info)
        self.subdialog = MovieDialog(self)
        self.subdialog.set_chitchat_context()
        return "s_subdialog"

    def end_movie_dialog(self):
        self.prefix = self.utt(["nothing_else_to_say_movie"],
                               {"movie_name": self.context.current_subdialog_context["movie_info"]["movie_name"]})
        self.prefix += self.utt(["transition_change_subtopic"])
        return self.s_propose_movie()

    def update_context_and_response_when_current_and_previous_actor_unmatched(self, actor_info, actor_name):
        self.context.current_subdialog_context["candidate_actor_info"] = actor_info
        self.response = self.utt(["actor_dialog", "prompt_question"], {
            "actor_name": actor_name})
        self.moviechat_user["flow_interruption"] = "i_ask_chat_actor"
        self.moviechat_user["last_propose_actor"] = actor_name

    # initiate actor dialog
    def start_actor_dialog(self, actor_info):
        print("initiate_actor_dialog:", actor_info["name"])
        self.moviechat_user["current_subdialog"] = "ActorDialog"
        self.user_attributes["current_subdialog"] = "ActorDialog"
        set_actor_dialog_init_context(self.context, actor_info)
        self.subdialog = ActorDialog(self)
        self.subdialog.set_chitchat_context()
        return "s_subdialog"

    def end_actor_dialog(self):
        if "q3_question" in self.user_attributes.get("subdialog_context", {}).get("tags", {}):
            self.execute_again = True
            return "t_ask_movietv"
        elif "c_no_questions_left" in self.context.current_subdialog_context["tags"]:
            self.response = self.utt(["nothing_else_to_say"], {"actor_name":self.context.current_subdialog_context["actor_info"]["name"]})
        else:
            self.response = self.utt(["talk_about_something_else"], {"actor_name":self.context.current_subdialog_context["actor_info"]["name"]})
        self.context.current_subdialog_context = {}
        return "t_ask_movietv"

    def start_chitchat_dialog(self):
        print("initiate_chitchat_dialog")
        self.moviechat_user["current_subdialog"] = "ChitchatDialog"
        set_chitchat_dialog_init_context(self.context)
        self.subdialog = ChitchatDialog(self)
        self.subdialog.set_chitchat_context()
        return "s_chitchat"

    def end_chitchat_dialog(self):
        self.context.current_subdialog_context = {}
        return self.s_ask_movietv()

    def handle_potential_question(self):
        self.movie_question_handler = MovieQuestionHandler(self)
        answer = self.movie_question_handler.handle_question()

        if answer and answer.bot_propose_module and \
                answer.bot_propose_module != TopicModule.MOVIE:
            propose_topic = answer.bot_propose_module.value
            self.prefix = answer.response + "<break time='0.15s'/> "
            self.t_context["propose_topic"] = self.utt_propose_topic([f"propose_topic_short/{propose_topic}"])

            self.t_context["propose_continue"] = "UNCLEAR"
            self.moviechat_user["propose_topic"] = propose_topic
            return "s_ask_exit"

        if answer:
            if self.current_t_transition == "t_ask_movietv":
                self.prefix = answer.response + "<break time='0.15s'/> "
                return "s_ask_movietv"
            else:
                self.prefix = answer.response + "<break time='0.15s'/> " + self.utt(["transition_change_subtopic"])

                return None
        return None  # for now always return None as new logic just prepends question answer

    def handle_potential_command(self, resume_state=None, enable=True):
        if not enable:
            return None
        if resume_state is None:
            resume_state = self.current_t_transition
        self.topic_switch_handler = TopicSwitchHandler(self)
        res = self.topic_switch_handler.handle_potential_topic_switch(resume_state)
        if res:
            return res

        return None

    # =========================================================
    # Callback functions
    # higher priority or corner cases
    def t_change_subtopic(self):
        # TODO: what to do if there is more than one sys intent?
        if len(self.subtopic_intent) > 1:
            pass
        if self.subtopic_intent[0] == "exit_moviechat":
            return "s_ask_exit"
        elif self.subtopic_intent[0] == "fav_movies":
            return self.t_change_subtopic_fav_movies()
        elif self.subtopic_intent[0] == "ask_recommend":
            return self.t_change_subtopic_recommend_movie()
        elif self.subtopic_intent[0] == "currently_playing":
            return self.t_change_subtopic_currently_playing()
        elif self.subtopic_intent[0] == "know_movies":
            return "s_propose_movie"
        elif self.subtopic_intent[0] == "change_movie":
            self.prefix = self.utt(["affirm_talk_about_movie"])
            return "s_ask_movietv"
        elif self.subtopic_intent[0] == "guess_movie":
            self.t_context["next_transition"] = "t_ask_movietv"
            self.response = self.utt(["no_idea_btw_what_movie_talk_about"])
            return "s_echo"
        elif self.subtopic_intent[0] == "req_tv":
            if self.enter_from_other_module:
                return "s_try_switch_from_tv_to_movie"
            else:
                self.t_context["next_transition"] = "t_ask_movietv"
                self.response = self.utt(["no_watch_tv_show"])
                return "s_echo"
        return "s_ask_exit"  # should not reach here

    def s_try_switch_from_tv_to_movie(self):
        self.t_context["next_transition"] = "t_ask_movietv"
        self.response = "Cool, watching TV must be fun! I haven't watched many tv shows yet, but I do watch a lot of movies. Do you like movies? "
        return "t_try_switch_from_tv_to_movie"

    def t_try_switch_from_tv_to_movie(self):
        if self.returnnlp.answer_positivity == Positivity.neg:
            return "s_ask_exit"
        else:
            self.moviechat_user["current_state"] = "s_intro"
            return self.t_ask_movietv()

    def t_change_subtopic_currently_playing(self):
        try:
            now_playing = Movie().now_playing()[0:3]
        except Exception as e:
            self.logger.info("[MOVIECHAT_MODULE] Exception in search_with_movie_name {}".format(repr(e)))
            now_playing = []
        self.response = None
        if len(now_playing) == 3:
            self.response = self.utt(["movie_now_playing"],
                                     {"title1": now_playing[0].title, "title2": now_playing[1].title,
                                      "title3": now_playing[2].title})
        if not self.response:
            ask_phrase = self.utt(["what_movies_are_playing"])
            self.movie_question_handler = MovieQuestionHandler(self)
            res = self.movie_question_handler.handle_question(enable_none=True, modified_question=ask_phrase)

            if res:
                self.response = res + self.utt(["what_movie_talk_more"])
        if not self.response:
            self.response = self.utt(["cannot_look_up_movie"])
        self.t_context["next_transition"] = "t_ask_movietv"
        return "s_echo"

    def t_change_subtopic_recommend_movie(self):
        return "s_propose_movie"

    def t_change_subtopic_fav_movies(self):
        self.movie_question_handler = MovieQuestionHandler(self)
        res = self.movie_question_handler.handle_question(enable_none=True)
        if res:
            self.response = res.response + self.utt(["what_movie_do_you_like"])
        else:
            self.response = self.utt(["get_fav_movie"])
        self.t_context["next_transition"] = "t_ask_movietv"
        return "s_echo"

    def t_ask_exit(self):
        return "s_ask_exit"

    def s_ask_exit(self):
        self.moviechat_user["current_state"] = "s_ask_exit"
        if "propose_topic" in self.t_context:
            self.response = self.t_context["propose_topic"]
        else:
            self.response = "That's okay. "
        if "propose_continue" in self.t_context:
            self.moviechat_user["propose_continue"] = self.t_context["propose_continue"]
        else:
            self.moviechat_user["propose_continue"] = "STOP"
        return "t_resume"

    def s_echo(self):
        self.moviechat_user["current_state"] = "s_echo"

        next_transition = self.t_context.get("next_transition", "t_resume")
        return next_transition

    def t_resume(self):
        return self.s_jump_in("t_resume", "s_ask_movietv")

    def t_init(self):
        return self.s_jump_in("t_init", "s_intro")

    def s_jump_in(self, current_t: str, default_s: str):
        # assumptions about how module is usually triggered initially
        # (1) Topic selection: e.g. Let's talk about movies/tv
        # (2) Question: e.g. What are you watching
        # (3) Statement: e.g. My favorite movie is {movie}

        res = self.try_actor_dialog(current_t)
        if res:
            return res
        if len(self.text.strip().split()) >= 5:
            res = self.try_start_another_movie_dialog(
                strength=2, grounding=self.cfgs.EXPLICIT_MOVIE_GROUNDING)
        else:
            res = self.try_start_another_movie_dialog(
                strength=1, grounding=self.cfgs.EXPLICIT_MOVIE_GROUNDING)
        if res:
            return res
        res = self.handle_potential_question()
        if res:
            return res
        res = self.handle_potential_command()
        if res:
            return res
        return default_s

    def s_intro(self):
        self.moviechat_user["current_state"] = "s_intro"

        if self.prefix == "":
            self.response = self.utt(["init", "one_turn_intro"])
        else:
            self.response = self.utt(["init", "one_turn_intro_short"])
        return "t_ask_movietv"

    def s_ask_movietv(self):
        self.moviechat_user["current_state"] = "s_ask_movietv"

        idx = self.user_attributes.get("movie_q_idx", 0)
        num_questions = len(utterance(["ask_movie"], True))
        if idx >= num_questions or "no_ask_movie_questions_left" in self.user_attributes:  # temp test
            if "no_ask_movie_questions_left" not in self.user_attributes:  # first time reaching the end
                self.moviechat_user["no_ask_movie_questions_left"] = True
                self.prefix = ""
                self.response = self.utt(["lets_talk_about_something_else"])
                self.moviechat_user["propose_continue"] = "STOP"
                return "t_resume"
            self.response = self.utt(["ask_movie_default"])
            return "t_ask_movietv"
        self.moviechat_user["movie_q_idx"] = idx + 1
        ask_movietv = self.utt(["transition_change_subtopic"]) + utterance(["ask_movie"], True)[idx]
        # say_interest_movietv = self.utt(["say_interest_movie"])
        self.response = ask_movietv  # + say_interest_movietv

        return "t_ask_movietv"

    def t_ask_movietv(self):
        # the first time
        if self.moviechat_user["current_state"] == "s_intro" and \
                self.returnnlp.answer_positivity == Positivity.neg:
            if "ask_back" in self.returnnlp.flattened_lexical_intent:
                return "s_propose_movie"
            else:
                self.prefix = self.utt(["maybe_you_been_busy"]) + self.utt(["transition_change_subtopic"])
            return self.ask_favorite_genre_or_propose_movie()
            # self.moviechat_user["ask_movietv_repeat"] = True
            # return "s_ask_movietv"
        if self.has_seen_movie_lately_short_answer():
            return self.ask_what_did_you_see()

        # Check if the user says "i don't recommend movies to anyone"
        if re.search(user_does_not_recommend_movies, self.text):
            self.prefix = self.utt(["thats_okay"]) + self.utt(["transition_change_subtopic"])
            return self.ask_favorite_genre_or_propose_movie()

            # return self.start_chitchat_dialog() # this should skip straight to chitchat

        if self.returnnlp.answer_positivity == Positivity.neg:
            self.moviechat_user["ask_movietv_repeat"] = True
            self.prefix = self.utt(["thats_okay"]) + self.utt(["transition_change_subtopic"])
            return self.ask_favorite_genre_or_propose_movie()


        if "ans_unknown" in self.cobot_intents or re.search(cannot_remember, self.text):
            self.moviechat_user["ask_movietv_repeat"] = True
            self.prefix = self.utt(["its_okay_if_you_are_not_sure"]) + self.utt(["transition_change_subtopic"])
            return self.ask_favorite_genre_or_propose_movie()


        if self.user_attributes.get("ask_movietv_repeat", True) == True:
            if re.search(discuss_another_movie, self.text) or "ans_neg" in self.cobot_intents:
                self.prefix = self.utt(["okay_we_can_discuss_other_movie"])
                self.moviechat_user["ask_movietv_repeat"] = True
                return "s_ask_movietv"

        if self.user_attributes.get("ask_movietv_repeat", True) == False:
            if re.search(not_movie_regex, self.text):
                self.moviechat_user["ask_movietv_repeat"] = True
                self.prefix = self.utt(["were_not_specific_silly_me"]) + self.utt(["transition_change_subtopic"])
                return self.ask_favorite_genre_or_propose_movie()

        # when user answers something like "no", "yes", "okay", "no no", ask
        # them to repeat once
        # todo: change regex match to a more maintainable way
        if unclear_movie_name(self.text) and self.moviechat_user["current_state"] != "s_intro":
            if self.user_attributes.get("ask_movietv_repeat", True):
                self.moviechat_user["ask_movietv_repeat"] = False
                self.t_context["next_transition"] = "t_ask_movietv"
                self.response = self.utt(["may_have_heard_wrong"])
                return "s_echo"
            self.moviechat_user["ask_movietv_repeat"] = True

            self.prefix += self.utt(["transition_change_subtopic"])
            return self.ask_favorite_genre_or_propose_movie()

        res = self.try_actor_dialog("t_resume")
        if res: return res

        res = self.try_start_another_movie_dialog(
            strength=2, grounding=self.cfgs.EXPLICIT_MOVIE_GROUNDING)
        if res:
            return res

        if self.has_seen_movie_lately():
            return self.ask_what_did_you_see()

        if is_potential_tv(self.knowledge):
            print("[is_potential_tv]")
            keywords = get_potential_tv_keywords(self.knowledge)
            self.response = self.utt(["no_watch_tv_show_yet"], {'keywords':keywords})
            self.t_context["next_transition"] = "t_ask_movietv"
            self.moviechat_user["ask_movietv_repeat"] = False
            return "s_echo"

        res = self.try_genre_dialog()
        if res: return res

        res = self.handle_potential_question()
        if res:
            return res

        res = self.handle_potential_command("t_resume")
        if res:
            return res

        if self.user_attributes.get("ask_movietv_repeat", True):
            self.moviechat_user["ask_movietv_repeat"] = False
            self.t_context["next_transition"] = "t_ask_movietv"
            self.response = self.utt(["repeat_movie"])
            return "s_echo"

        self.moviechat_user["ask_movietv_repeat"] = True
        self.prefix = self.utt(["do_not_know_movies_by_that_name"]) + self.utt(["transition_change_subtopic"])
        return self.ask_favorite_genre_or_propose_movie()

    def ask_what_did_you_see(self):
        self.response = self.utt(["what_did_you_see"])
        self.t_context["next_transition"] = "t_ask_movietv"
        self.moviechat_user["ask_movietv_repeat"] = True
        return "s_echo"

    def has_seen_movie_lately_short_answer(self):
        return self.moviechat_user["current_state"] == "s_intro" and \
            self.returnnlp.answer_positivity == Positivity.pos and \
            len(self.returnnlp) == 1

    def has_seen_movie_lately(self):
        return self.moviechat_user["current_state"] == "s_intro" and \
               self.returnnlp.answer_positivity == Positivity.pos

    def s_movie_grounding(self):
        self.moviechat_user["current_state"] = "s_movie_grounding"

        movie_infos = self.context.current_subdialog_context["movie_infos"]
        idx = len(movie_infos) - self.current_loop["turns_left"]
        movie_info = movie_infos[idx]
        try:
            params = {
                "movie_title": movie_info["movie_name"],
                "release_year": movie_info["movie_release_year"],
                "actor_name": movie_info["movie_leading_actor"]["name"],
                "actor_role": movie_info["movie_leading_actor"]["role"],
            }
            if idx == 0 and len(movie_infos) > 1:
                self.response = self.utt(["ground_movie_ack"]) + self.utt(["ground_movie_intro"], {"num_movies": len(movie_infos)}) + self.utt(["ground_movie"], params)
            elif idx == 0:
                self.response = self.utt(["ground_movie_ack"]) + self.utt(["ground_movie"], params)
            else:
                self.response = self.utt(["ground_movie"], params)
        except BaseException:
            params = {"movie_title": movie_info["movie_name"]}
            if idx == 0 and len(movie_infos) > 1:
                self.response = self.utt(["ground_movie_ack"]) + self.utt(
                    ["ground_movie_intro"], {"num_movies": len(movie_infos)}) + self.utt(["ground_movie_alt"],
                                                                                         params)
            elif idx == 0:
                self.response = self.utt(["ground_movie_ack"]) + self.utt(["ground_movie_alt"], params)
            else:
                self.response = self.utt(["ground_movie_alt"], params)
        self.current_loop["turns_left"] -= 1
        return "t_movie_grounding"

    def t_movie_grounding(self):
        if re.search(not_movie_regex, self.text):
            self.prefix = self.utt(["my_mistake"])
            self.prefix += self.utt(["transition_change_subtopic"])
            return self.ask_favorite_genre_or_propose_movie()

        elif "ans_pos" in self.cobot_intents or re.search(that_is_the_one, self.text):
            movie_infos = self.context.current_subdialog_context["movie_infos"]
            idx = len(movie_infos) - self.current_loop["turns_left"] - 1
            movie_info = movie_infos[idx]

            self.prefix = self.utt(["ground_movie_guess_right"])
            return self.start_movie_dialog(movie_info)

        if self.neg_answer_and_try_correcting_movie_name():
            # modify text to improve movie name ner detection
            self.text = re.sub(r"^(no i'm thinking of )|^(no )", "", self.text)
            self.logger.debug("[text] replaced: {}".format(self.text))

        res = self.t_normal_flow("t_movie_grounding")
        if res:
            return res

        # if fails to detect movie name correction in t_normal_flow, it goes here
        if self.neg_answer_and_try_correcting_movie_name():
            self.prefix = self.utt(["do_not_know_movies_by_that_name"]) + self.utt(["transition_change_subtopic"])
            return self.ask_favorite_genre_or_propose_movie()

        if self.current_loop["turns_left"] <= 0:
            self.prefix = self.utt(["grounding_not_know_movie"])
            self.prefix += self.utt(["transition_change_subtopic"])
            return self.ask_favorite_genre_or_propose_movie()

        return "s_movie_grounding"

    def neg_answer_and_try_correcting_movie_name(self):
        return self.returnnlp[0].has_dialog_act(DialogAct.NEG_ANSWER) and len(self.text.split()) > 3

    def s_interesting_comment(self):
        self.moviechat_user["current_state"] = "s_interesting_comment"

        movie_id = self.context.current_subdialog_context["movie_id"]
        recognition = ""
        if self.context.movies[movie_id]["facts_idx"] == 0:
            recognition = self.utt(
                ["know_movie"], {
                    "movie_title": self.context.movies[movie_id]["movie_info"]["movie_name"]})
        elif self.current_loop["turns_left"] == self.cfgs.MOVIE_FACTS_TURNS_PER_TRIGGER:
            recognition = self.utt(["lets_keep_talking"], {"movie_name":self.context.movies[movie_id]["movie_info"]["movie_name"]})
        if "fact" not in self.t_context:
            self.t_context["fact"] = self.context.get_current_context_movie_fact()
        say_fact = self.utt(["say_fact"], {"fact": self.t_context["fact"]})
        ask_interest = self.utt(["ask_reaction"])
        self.current_loop["turns_left"] = self.current_loop["turns_left"] - 1
        self.response = recognition + say_fact + ask_interest
        return "t_interesting_comment"

    def t_interesting_comment(self):
        if re.search(where_do_you_get_that_info, self.text):
            self.response = self.utt(["learned_from_imbg"]),
            self.t_context["next_transition"] = "t_interesting_comment"
            return "s_echo"
        elif re.search(wrong_comment, self.text):
            self.t_context["from_interesting_comment"] = True
            self.t_context["ack_mistake"] = self.utt(["goofed_up"])
            self.current_loop = {"turns_left": 2}
            return "s_movie_chitchat"
        elif re.search(wrong_movie_regex, self.text):
            self.prefix += self.utt(["transition_change_subtopic"])
            return self.ask_favorite_genre_or_propose_movie()

        res = self.t_normal_flow("t_interesting_comment")
        if res:
            return res

        if self.current_loop["turns_left"] <= 0:
            self.prefix += self.utt(["transition_change_subtopic"])
            return self.ask_favorite_genre_or_propose_movie()
        self.t_context["fact"] = self.context.get_current_context_movie_fact()
        if self.t_context["fact"] is None:
            self.prefix += self.utt(["transition_change_subtopic"])
            return self.ask_favorite_genre_or_propose_movie()

        return "s_interesting_comment"

    """
    We try to detect certain cases here.
    """

    def t_normal_flow(self, current_t):
        self.logger.debug("[s_normal_flow] current transition: {}".format(current_t))
        # Try detect actor dialog
        res = self.try_actor_dialog(current_t)
        if res: return res

        # Try to see if we want to talk about a different topic
        res = self.try_jump_out_and_change_topic()
        if res: return res

        if current_t in ['t_ask_movietv', 't_movie_grounding', 't_ask_favorite_movie_genre'] or len(self.text.split()) >= 5:
            res = self.try_start_another_movie_dialog(strength=0, grounding=self.cfgs.EXPLICIT_MOVIE_GROUNDING)
            if res: return res

        if current_t != "t_subdialog":  # don't try genre in the middle of a sub-dialog
            res = self.try_genre_dialog()
            if res: return res

        res = self.handle_potential_question()
        if res: return res

        res = self.handle_potential_command()
        if res: return res

        return None

    # when entering chitchat mode, pass in the number of chitchat turns, and a returning point
    # store chitchat info for future use
    # TODO: use keywords to guide chitchat
    def s_chitchat(self):
        self.moviechat_user["current_state"] = "s_chitchat"

        output = self.subdialog.get_response(self.input_data)
        self.response = output["response"]
        self.moviechat_user["subdialog_context"] = {
            "current_state": self.context.current_subdialog_context["current_state"],
            "old_state": self.context.current_subdialog_context["old_state"],
            "transition_history": self.context.current_subdialog_context["transition_history"],
            "tags": self.context.current_subdialog_context["tags"],
        }

        # after acking the previous question but jump out into different dialog flow
        if output["exit_code"] == "s_exit":
            self.prefix = output["response"]
            return self.end_chitchat_dialog()
        # after entering chitchat after questions are already exhausted
        elif self.context.current_subdialog_context["current_state"] == "s_exit":
            return self.end_chitchat_dialog()

        return output["next_t_transition"]

    def t_chitchat(self):
        self.subdialog = ChitchatDialog(self)
        self.subdialog.set_chitchat_context()
        self.context.current_subdialog_context["interrupted"] = True
        save_chitchat_dialog_current_context(self.context)

        res = self.t_normal_flow("t_chitchat")
        if res:
            return res

        self.context.current_subdialog_context["interrupted"] = False

        return "s_chitchat"

    # legacy?
    def s_movie_chitchat(self):
        self.moviechat_user["current_state"] = "s_movie_chitchat"

        self.current_loop["turns_left"] -= 1
        last_idx = self.user_attributes.get("movie_chitchat_last_idx")
        questions = utterance(["movie_chitchat_question"], True)
        # if wrong movie detected e.g. from say_interesting_comment
        if self.t_context.get("from_interesting_comment"):
            prefix = self.t_context.get(
                "ack_mistake", "") + self.utt(["say_interest_unknown_movie"])
        elif not last_idx:
            if "prefix" in self.current_loop:
                prefix = self.current_loop["prefix"] + \
                         self.utt(["say_interest_unknown_movie"])
            else:
                prefix = self.utt(["not_know_movie"]) + \
                         self.utt(["say_interest_unknown_movie"])
        else:
            prefix = self.utt(["short_response"])
            if last_idx < len(questions):
                questions.pop(last_idx)
        q_inds = range(len(questions))
        idx = random.choice(q_inds)
        self.moviechat_user["movie_chitchat_last_idx"] = idx
        self.response = prefix + questions[idx]
        return "t_movie_chitchat"

    def t_movie_chitchat(self):
        if re.search(wrong_movie_regex, self.text):
            self.prefix += self.utt(["transition_change_subtopic"])
            return self.ask_favorite_genre_or_propose_movie()

        res = self.t_normal_flow("t_movie_chitchat")
        if res:
            return res

        if self.current_loop["turns_left"] <= 0:
            self.moviechat_user["movie_chitchat_last_idx"] = None
            self.prefix += self.utt(["transition_change_subtopic"])
            return self.ask_favorite_genre_or_propose_movie()

        return "s_movie_chitchat"

    def s_subdialog(self):
        self.moviechat_user["current_state"] = "s_subdialog"

        self.response = self.subdialog.get_response(self.input_data)
        self.moviechat_user["subdialog_context"] = {
            "current_state": self.context.current_subdialog_context["current_state"],
            "old_state": self.context.current_subdialog_context["old_state"],
            "transition_history": self.context.current_subdialog_context["transition_history"],
            "tags": self.context.current_subdialog_context["tags"],
        }
        # self.moviechat_user["tags"].append("")

        if self.context.current_subdialog_context["current_state"] == "s_exit":
            return self.end_of_subdialog()

        return "t_subdialog"

    def t_subdialog(self):
        if self.user_attributes["current_subdialog"] == "ActorDialog":
            self.subdialog = ActorDialog(self)
        elif self.user_attributes["current_subdialog"] == "MovieDialog":
            self.subdialog = MovieDialog(self)
        self.subdialog.set_chitchat_context()

        if re.search(wrong_movie_regex, self.text) and self.user_attributes["current_subdialog"] == "MovieDialog":
            self.prefix = self.utt(["my_bad_talk_another_movie"])
            return "s_ask_movietv"
        elif re.search(dont_talk_movie, self.text) and self.user_attributes["current_subdialog"] == "MovieDialog":
            self.prefix = self.utt(["okay_talk_about_another_movie"])
            return "s_ask_movietv"

        res = self.t_normal_flow("t_subdialog")
        if res:
            return res

        return "s_subdialog"

    def t_start_actor_dialog(self):
        res = self.t_normal_flow("t_start_actor_dialog")
        if res:
            return res

        return self.start_actor_dialog(
            self.context.current_subdialog_context["candidate_actor_info"])

    def t_start_chitchat_dialog(self):
        res = self.t_normal_flow("t_start_chitchat_dialog")
        if res:
            return res

        self.prefix += self.utt(["transition_change_subtopic"])
        return self.ask_favorite_genre_or_propose_movie()

    def t_start_movie_chitchat_dialog(self):
        res = self.t_normal_flow("t_start_movie_chitchat_dialog")
        if res:
            return res

        self.current_loop = {"turns_left": 2}
        return "s_movie_chitchat"

    def i_ask_continue_movie(self, current_transition):
        if "ans_neg" in self.cobot_intents:
            return "t_ask_exit"

        return current_transition

    #
    # decide whether to enter actor submodule, or continue current state
    #
    def i_ask_chat_actor(self, current_transition):
        ans_pos_regex = positive_regex
        ans_pos = False
        if re.search(ans_pos_regex, self.text):
            ans_pos = True
        if "ans_pos" in self.cobot_intents or ans_pos:
            return "t_start_actor_dialog"

        return current_transition

    def i_ask_movie_name_reply(self):
        if "ans_pos" in self.cobot_intents:
            return "t_start_movie_chitchat_dialog"

        return "t_start_chitchat_dialog"

    def s_propose_movie(self):
        self.moviechat_user["current_state"] = "s_propose_movie"

        current_movie_info = self.get_currently_discussed_movie_info()
        self.logger.debug("current_movie_info: {}".format(current_movie_info))

        liked_genre_ids = self.get_user_favorite_genre_ids()

        movie_info, connection = MoviePreferencePredictor(
            self.get_currently_discussed_movie_info(),
            liked_genre_ids,
            self.context.get_mentioned_movie_ids(), self.a_b_test).pick_next_movie_with_info()

        self.context.mentioned_movie_ids.append(movie_info["movie_id"])
        self.moviechat_user["proposed_movie_id"] = movie_info["movie_id"]

        if connection:
            self.moviechat_user["proposed_movie_connection"] = connection
            if connection == MoviePreferencePredictor.Connection.SIMILAR_MOVIE.name:
                self.response = self.utt(["propose_movie", "similar"], {"movie_name": movie_info["movie_name"]})
            elif connection == MoviePreferencePredictor.Connection.POPULAR_MOVIE.name:
                self.response = self.utt(["propose_movie", "default"], {"movie_name": movie_info["movie_name"]})
            else:
                self.response = self.utt(["propose_movie", "default"], {"movie_name": movie_info["movie_name"]})
        else:
            self.response = self.utt(["propose_movie", "default"], {"movie_name": movie_info["movie_name"]})
        return "t_propose_movie"

    def t_propose_movie(self):
        movie_id = self.moviechat_user["proposed_movie_id"]

        if re.search(clarify_movie_name, self.text):
            return "s_repeat_movie_name"

        if self.returnnlp.answer_positivity is Positivity.pos:
            movie_info = MoviePreferencePredictor().get_movie_info(movie_id)
            self.prefix = self.utt(["seen_movie_ack"])
            return self.start_movie_dialog(movie_info)

        if self.returnnlp.answer_positivity is Positivity.neg:
            if self.returnnlp[-1].has_intent("ans_like"):
                res = self.try_genre_dialog()
                if res:
                    return res

                res = self.try_start_another_movie_dialog()
                if res:
                    return res

            self.prefix = self.utt(["not_seen_movie_ack"])
            plot_summary = get_movie_plot_summary(movie_id)
            if plot_summary:
                return "s_ask_user_interest_in_bot_proposed_movie"
            else:
                return self.start_chitchat_dialog()

        res = self.t_normal_flow("t_propose_movie")
        if res:
            return res

        return self.start_chitchat_dialog()

    def s_repeat_movie_name(self):
        self.moviechat_user["current_state"] = "s_provide_plot_summary"
        movie_id = self.moviechat_user["proposed_movie_id"]
        movie_info = get_movie_info(movie_id)
        movie_name = movie_info["movie_name"]

        self.response = movie_name
        return "t_propose_movie"

    def s_ask_user_interest_in_bot_proposed_movie(self):
        self.moviechat_user["current_state"] = "s_ask_user_interest_in_bot_proposed_movie"

        self.response = "Do you want to know more about it? "

        return "t_ask_user_interest_in_bot_proposed_movie"

    def t_ask_user_interest_in_bot_proposed_movie(self):
        if self.returnnlp.answer_positivity is Positivity.pos:
            return "s_provide_plot_summary"
        else:
            self.prefix = "That's okay. "
            return self.start_chitchat_dialog()

    def s_provide_plot_summary(self):
        self.moviechat_user["current_state"] = "s_provide_plot_summary"

        movie_id = self.moviechat_user["proposed_movie_id"]
        plot_summary = get_movie_plot_summary(movie_id)
        self.response = self.utt(["provide_plot_summary", "content"], slots={'plot': plot_summary})
        return "t_provide_plot_summary"

    def t_provide_plot_summary(self):
        if "ans_pos" in self.cobot_intents:
            self.prefix = self.utt(["provide_plot_summary", "ack_positive"])
            self.prefix += self.utt(["transition_change_subtopic"])

            return self.start_chitchat_dialog()

        res = self.t_normal_flow("t_provide_plot_summary")
        if res:
            return res

        return self.start_chitchat_dialog()

class UserAttributes:
    def __init__(self, prev_hash):
        self.prev_hash = prev_hash  # type: dict
