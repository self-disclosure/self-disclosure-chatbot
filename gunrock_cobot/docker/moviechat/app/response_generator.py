import json
import re

from movietv_automaton import *
from movietv_utils import *

required_context = ['text']

def get_required_context():
    return required_context

# TODO: if latency is bad, maybe we can speed it up by instantiating the automaton in app.py


def handle_message(msg, app_logger):
    # try:
    #print("msg", json.dumps(msg, indent=4))
    # hack: reset everything moviechat; asr shouldn't return qwe but best just comment this out before pushing
    if re.search(r"reset moviechat qwe", msg["text"][0]):
        msg["moviechat_user"] = None
        msg["moviechat_context"] = [None, None]
    msg["text"][0] = msg["text"][0].lower().strip()

    if msg.get("moviechat_user") is None:  # check if None
        msg["moviechat_user"] = {}
    current_state = msg["moviechat_user"].get("current_transition")
    if current_state == None:
        msg["moviechat_user"]["current_transition"] = "t_init"
    #print("current_state", msg["moviechat_user"]["current_transition"])

    user_attributes = SimpleNamespace(**{
        "session_id": msg["session_id"],
        "user_profile": msg["user_profile"],
        "a_b_test": msg["a_b_test"][0],
        "template_manager": msg["template_manager"],
        "module_selection": msg["module_selection"],
        "previous_modules": msg["previous_modules"],
        "blender_start_time": msg["blender_start_time"]
    })

    RG = MovieTVAutomaton(app_logger)
    results = RG.transduce(msg, user_attributes)
    # some info that we might want to save
    ''' for reference
    response_dict = {
        'response': "hi this is moviechat",
        'user_attributes': {
            'moviechat_user': {
                'last_movie_mentioned': {},
                'last_author_mentioned': {},
                'liked_movies': {},
                'liked_genre': {},
                'liked_authors': {},
                'indices': {}, 
                'current_state': None
            }
        },
        'context_manager': {
            'moviechat_context': {
                'last_state': None,
                'current_state': 't_init',            
            }
        }
    }
    '''


    # parse results and return response plus information to store in user attributes and current cobot state
    response_dict = {}

    response_dict["user_attributes"] = user_attributes.__dict__

    response_dict["response"] = results["response"]

    # persist existing info otherwise current moviechat_user info gets overwritten
    response_dict["user_attributes"]["moviechat_user"] = msg.get("moviechat_user", {})

    # if there are new fields or info for existing fields have changed, update moviechat_user
    for k, v in results["moviechat_user"].items():
        response_dict["user_attributes"]["moviechat_user"][k] = v
    response_dict["context_manager"] = {"moviechat_context": {}}
    for k, v in results["moviechat_context"].items():
        response_dict["context_manager"]["moviechat_context"][k] = v

    response_dict["user_attributes"]["propose_topic"] = results["moviechat_user"]["propose_topic"]
    response_dict["user_attributes"]["template_manager"] = results["template_manager"]


    #print("response_dict", json.dumps(response_dict, indent=4))
    '''
    except Exception as e:
        # To keep cobot running, catch everything!
        return {
            "response": "Oh no, I think my creators misspelled something in my code. Would you mind asking something else?"
        }
    '''
    app_logger.info("[response_generator.handle_message] Output response_dict: %s", json.dumps(
        response_dict, indent=4))
    return response_dict
