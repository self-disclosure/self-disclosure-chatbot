from unittest.mock import ANY
from unittest.mock import patch
from movietv_automaton import *
import logging
import unittest
from unittest.mock import ANY
from unittest.mock import patch
from unittest.mock import call
import pytest
import unittest
from movietv_automaton import *
import logging

response_generator = None

def _init_response_generator():
    global response_generator
    response_generator = MovieTVAutomaton(
        logging.getLogger('response generator'))

class MovieTest(unittest.TestCase):

    @patch('context_manager.ContextManager')
    @patch('template_manager.Templates.movies.utterance')
    def test_enter_actor(self, utterance, ContextManager):
        # given
        _init_response_generator()
        input_data = InputData.botWhoIsFavouriteActor_userAnsBradPitt

        context_manager = ContextManager.return_value
        context_manager.movies = {}
        context_manager.current_context = {'current_state': 's_chitchat', 'old_state': 's_init', 'q_key': 'q1', 'transition_history': ['r_t0', 'r_t0', 'r_t0', 'r_t0', 'r_t2'], 'chitchat_context': [], 'tags': ['q1_question'], 'interrupted': False}
            #{'current_state': 's_chitchat', 'old_state': 's_init', 'q_key': 'q1', 'transition_history': ['r_t0', 'r_t0', 'r_t0', 'r_t0', 'r_t2'], 'chitchat_context': [], 'tags': ['q1_question'], 'interrupted': True, 'candidate_actor_info': {'adult': False, 'id': 287, 'known_for': [{'vote_average': 8.4, 'vote_count': 13736, 'id': 550, 'video': False, 'media_type': 'movie', 'title': 'Fight Club', 'popularity': 28.969, 'poster_path': '/adw6Lq9FiC9zjYEpOqfq03ituwp.jpg', 'original_language': 'en', 'original_title': 'Fight Club', 'genre_ids': [18], 'backdrop_path': '/87hTDiay2N2qWyX4Ds7ybXi9h8I.jpg', 'adult': False, 'overview': 'A ticking-time-bomb insomniac and a slippery soap salesman channel primal male aggression into a shocking new form of therapy. Their concept catches on, with underground "fight clubs" forming in every town, until an eccentric gets in the way and ignites an out-of-control spiral toward oblivion.', 'release_date': '1999-10-15'}, {'vote_average': 8.1, 'vote_count': 9770, 'id': 16869, 'video': False, 'media_type': 'movie', 'title': 'Inglourious Basterds', 'popularity': 23.313, 'poster_path': '/ai0LXkzVM3hMjDhvFdKMUemoBe.jpg', 'original_language': 'en', 'original_title': 'Inglourious Basterds', 'genre_ids': [18, 28, 53, 10752], 'backdrop_path': '/7nF6B9yCEq1ZCT82sGJVtNxOcl5.jpg', 'adult': False, 'overview': 'In Nazi-occupied France during World War II, a group of Jewish-American soldiers known as "The Basterds" are chosen specifically to spread fear throughout the Third Reich by scalping and brutally killing Nazis. The Basterds, lead by Lt. Aldo Raine soon cross paths with a French-Jewish teenage girl who runs a movie theater in Paris which is targeted by the soldiers.', 'release_date': '2009-08-18'}, {'vote_average': 8.2, 'vote_count': 8712, 'id': 807, 'video': False, 'media_type': 'movie', 'title': 'Se7en', 'popularity': 21.906, 'poster_path': '/8zw8IL4zEPjkh8Aysdcd0FwGMb0.jpg', 'original_language': 'en', 'original_title': 'Se7en', 'genre_ids': [80, 9648, 53], 'backdrop_path': '/A0WKbRIojF9DUWG4XLCmg5JK6I5.jpg', 'adult': False, 'overview': 'Two homicide detectives are on a desperate hunt for a serial killer whose crimes are based on the "seven deadly sins" in this dark and haunting film that takes viewers from the tortured remains of one victim to the next. The seasoned Det. Sommerset researches each sin in an effort to get inside the killer\'s mind, while his novice partner, Mills, scoffs at his efforts to unravel the case.', 'release_date': '1995-09-22'}], 'name': 'Brad Pitt', 'popularity': 8.599, 'profile_path': '/kU3B75TyRiCgE270EyZnHjfivoq.jpg', 'ner_term': 'Brad Pitt'}}

        # response_ack = '''So you also like Christopher Nolan. I have a feeling we're going to get along.'''
        # response_content = '''(Anyways|By the way), I really enjoyed watching Black Panther and Wonder Woman recently. What's your favorite superhero movie?'''
        # (Anyways|By the way), I really enjoyed watching Black Panther and Wonder '
        #  "Woman recently. What's your favorite superhero movie?
        response_ack = ''''''
        response_content = ''''''

        utterance.side_effect = [response_ack, response_content]

        # when
        response_generator.transduce(input_data)

        # then

        calls = [call(selector=['chitchat_dialog', 'chitchat_question_bank', 'q2', 'q'], slots={}, user_attributes_ref=ANY)] #, call(selector=[], slots={}, user_attributes_ref=ANY)]
        utterance.assert_has_calls(calls)

        # assert response_generator.response == response_ack+response_content
        assert response_generator.response == ''

        assert response_generator.moviechat_user == {'ask_movietv_repeat': True, 'candidate_module': None, 'current_loop': {}, 'current_subdialog': None, 'current_transition': 't_chitchat', 'flow_interruption': False, 'not_first_time': True, 'propose_continue': 'CONTINUE', 'propose_topic': None, 'subdialog_context': {'current_state': 's_chitchat',                       'old_state': 's_chitchat',                       'tags': ['q2_question'],                       'transition_history': ['r_t0','r_t0','r_t0', 'r_t2','r_t3']},'tags': []}

        assert response_generator.moviechat_context == {'last_module': True}

class InputData:

    botWhoIsFavouriteActor_userAnsBradPitt = {'features': [{'asrcorrection': None,
               'central_elem': {'DA': 'abandoned',
                                'DA_score': '0.9987',
                                'backstory': {'confidence': 0.5804839134216309,
                                              'followup': 'Do you like Star '
                                                          'Trek or Star Wars '
                                                          'better? ',
                                              'module': 'MOVIECHAT',
                                              'tag': 'movie',
                                              'text': "It's kinda "
                                                      "embarrassing, but I've "
                                                      'always had a soft spot '
                                                      'for Leonard Nimoy! Live '
                                                      'long and prosper! '},
                                'module': ['MOVIECHAT', 'MUSICCHAT'],
                                'np': ['brad pitt'],
                                'regex': {'lexical': [],
                                          'sys': [],
                                          'topic': []},
                                'senti': 'neu',
                                'text': 'brad pitt'},
               'concept': [{'data': {'actor': '0.122',
                                     'celebrity': '0.5597',
                                     'star': '0.3183'},
                            'noun': 'brad pitt'}],
               'converttext': 'brad Pitt',
               'coreference': None,
               'dependency_parsing': None,
               'dialog_act': [{'DA': 'abandoned',
                               'confidence': 0.9987,
                               'text': 'brad pitt'},
                              {'response_da': [['opinion', '0.6403'],
                                               ['statement', '0.9687'],
                                               ['open_question_opinion', '1.0'],
                                               ['statement', '1.0'],
                                               ['opinion', '1.0']]}],
               'googlekg': [[['Brad Pitt',
                              'American actor',
                              '841.472473',
                              'movie']]],
               'intent': 'general',
               'intent_classify': {'lexical': [], 'sys': [], 'topic': []},
               'intent_classify2': [{'lexical': [], 'sys': [], 'topic': []}],
               'knowledge': [['Brad Pitt',
                              'American actor',
                              '841.472473',
                              'movie']],
               'ner': [{'begin_offset': 0,
                        'end_offset': 9,
                        'label': 'PERSON',
                        'text': 'Brad Pitt'}],
               'noun_phrase': ['brad pitt'],
               'nounphrase2': {'local_noun_phrase': [['brad pitt']],
                               'noun_phrase': [['brad pitt']]},
               'npknowledge': {'knowledge': [['Brad Pitt',
                                              'American actor',
                                              '841.472473',
                                              'movie']],
                               'local_noun_phrase': ['brad pitt'],
                               'noun_phrase': ['brad pitt']},
               'profanity_check': [0],
               'returnnlp': [{'concept': [{'data': {'actor': '0.122',
                                                    'celebrity': '0.5597',
                                                    'star': '0.3183'},
                                           'noun': 'brad pitt'}],
                              'dependency_parsing': None,
                              'dialog_act': ['abandoned', '0.9987'],
                              'googlekg': [['Brad Pitt',
                                            'American actor',
                                            '841.472473',
                                            'movie']],
                              'intent': {'lexical': [], 'sys': [], 'topic': []},
                              'noun_phrase': ['brad pitt'],
                              'sentiment': {'compound': '0.0',
                                            'neg': '0.0',
                                            'neu': '1.0',
                                            'pos': '0.0'},
                              'text': 'brad pitt',
                              'topic': {'confidence': 999.0,
                                        'text': 'brad pitt',
                                        'topicClass': 'Celebrities',
                                        'topicKeywords': [{'confidence': 999.0,
                                                           'keyword': 'brad '
                                                                      'pitt'}]}}],
               'segmentation': {'segmented_text': ['ner'],
                                'segmented_text_raw': ['brad pitt']},
               'sentiment': 'neu',
               'sentiment2': [{'compound': '0.0',
                               'neg': '0.0',
                               'neu': '1.0',
                               'pos': '0.0'}],
               'spacynp': ['brad pitt'],
               'text': 'brad Pitt',
               'topic': [{'confidence': 999.0,
                          'text': 'brad Pitt',
                          'topicClass': 'Celebrities',
                          'topicKeywords': [{'confidence': 999.0,
                                             'keyword': 'brad pitt'}]}],
               'topic2': [{'confidence': 999.0,
                           'text': 'brad pitt',
                           'topicClass': 'Celebrities',
                           'topicKeywords': [{'confidence': 999.0,
                                              'keyword': 'brad pitt'}]}],
               'topic_keywords': [{'confidence': 999.0,
                                   'keyword': 'brad pitt'}],
               'topic_module': ''},
              None],
 'last_module': 'MOVIECHAT',
 'moviechat_context': [None, {'last_module': True}],
 'moviechat_user': {'ask_movietv_repeat': True,
                    'current_loop': {},
                    'current_subdialog': 'ChitchatDialog',
                    'current_transition': 't_chitchat',
                    'flow_interruption': False,
                    'movie_q_idx': 1,
                    'not_first_time': True,
                    'propose_continue': 'CONTINUE',
                    'subdialog_context': {'current_state': 's_chitchat',
                                          'old_state': 's_init',
                                          'tags': ['q1_question'],
                                          'transition_history': ['r_t0',
                                                                 'r_t0',
                                                                 'r_t0',
                                                                 'r_t0',
                                                                 'r_t2']},
                    'tags': []},
 'prev_hash': {'sys_env': 'local'},
 'resp_type': [None, None],
 'response': '<prosody rate="95%">It\'s okay if you\'re not sure! <break '
             'time="150ms" /> Anyways, I was thinking. <break time="150ms" /> '
             'Who\'s your favorite actor or actress? <break time="150ms" /> '
             'Leonardo DiCaprio is mine! <break time="150ms" /> He '
             'consistently gives a great performance, and he uses his fame to '
             'help the environment too.</prosody>',
 'session_id': ['amzn1.echo-api.session.localinteraction-w2kgjl9avdkp0ye1',
                'amzn1.echo-api.session.localinteraction-w2kgjl9avdkp0ye1'],
 'suggest_keywords': None,
 'text': ['brad Pitt', 'not sure'],
 'user_id': ['jarnold-00813128sndasnd', 'jarnold-00813128sndasnd']}

    botDoYouWantToTalkMoreAboutBradPitt_userAnsYes = {'features': [{'asrcorrection': None,
               'central_elem': {'DA': 'pos_answer',
                                'DA_score': '1.0',
                                'backstory': {'confidence': 0.47285330295562744,
                                              'text': 'Oh, I was just trying '
                                                      'to get to know more '
                                                      'about you. '},
                                'module': [],
                                'np': [],
                                'regex': {'lexical': ['ans_pos'],
                                          'sys': [],
                                          'topic': []},
                                'senti': 'pos',
                                'text': 'yes'},
                                                                    'concept': [],
                                                                    'converttext': 'open gunrock and yes',
                                                                    'coreference': None,
                                                                    'dependency_parsing': {'dependency_parent': [[0]]},
                                                                    'dialog_act': [{'DA': 'pos_answer',
                               'confidence': 1.0,
                               'text': 'yes'},
                              {'response_da': [['comment', '0.6912'],
                                               ['yes_no_question', '0.9098']]}],
                                                                    'googlekg': [[]],
                                                                    'intent': 'general',
                                                                    'intent_classify': {'lexical': ['ans_pos'],
                                   'sys': [],
                                   'topic': []},
                                                                    'intent_classify2': [{'lexical': ['ans_pos'],
                                     'sys': [],
                                     'topic': []}],
                                                                    'knowledge': [],
                                                                    'ner': None,
                                                                    'noun_phrase': [],
                                                                    'nounphrase2': {'local_noun_phrase': [[]], 'noun_phrase': [[]]},
                                                                    'npknowledge': {'knowledge': [],
                               'local_noun_phrase': [],
                               'noun_phrase': []},
                                                                    'profanity_check': [0],
                                                                    'returnnlp': [{'concept': [],
                              'dependency_parsing': [0],
                              'dialog_act': ['pos_answer', '1.0'],
                              'googlekg': [],
                              'intent': {'lexical': ['ans_pos'],
                                         'sys': [],
                                         'topic': []},
                              'noun_phrase': [],
                              'sentiment': {'compound': '0.4019',
                                            'neg': '0.0',
                                            'neu': '0.0',
                                            'pos': '1.0'},
                              'text': 'yes',
                              'topic': {'confidence': 999.0,
                                        'text': 'yes',
                                        'topicClass': 'Phatic',
                                        'topicKeywords': []}}],
                                                                    'segmentation': {'segmented_text': ['yes'],
                                'segmented_text_raw': ['yes']},
                                                                    'sentiment': 'neu',
                                                                    'sentiment2': [{'compound': '0.4019',
                               'neg': '0.0',
                               'neu': '0.0',
                               'pos': '1.0'}],
                                                                    'spacynp': None,
                                                                    'text': 'open gunrock and yes',
                                                                    'topic': [{'confidence': 0.289,
                          'text': 'open gunrock and yes',
                          'topicClass': 'Sports',
                          'topicKeywords': [{'confidence': 0.347,
                                             'keyword': '__UNK_WORD__'}]}],
                                                                    'topic2': [{'confidence': 999.0,
                           'text': 'yes',
                           'topicClass': 'Phatic',
                           'topicKeywords': []}],
                                                                    'topic_keywords': [],
                                                                    'topic_module': ''},
                                                                   None],
                                                      'last_module': 'MOVIECHAT',
                                                      'moviechat_context': [None, {'last_module': True}],
                                                      'moviechat_user': {'ask_movietv_repeat': True,
                    'current_loop': {},
                    'current_subdialog': 'ChitchatDialog',
                    'current_transition': 't_chitchat',
                    'flow_interruption': 'i_ask_chat_actor',
                    'last_propose_actor': 'Brad Pitt',
                    'not_first_time': True,
                    'propose_continue': 'CONTINUE',
                    'subdialog_context': {'current_state': 's_chitchat',
                                          'old_state': 's_init',
                                          'tags': ['q1_question'],
                                          'transition_history': ['r_t0',
                                                                 'r_t0',
                                                                 'r_t0',
                                                                 'r_t0',
                                                                 'r_t2']},
                    'tags': []},
                                                      'prev_hash': {'sys_env': 'local'},
                                                      'resp_type': [None, None],
                                                      'response': 'I think acting is such a cool job. <break time="150ms" /> Are '
             'you interested in talking more about Brad Pitt?',
                                                      'session_id': ['amzn1.echo-api.session.localinteraction-nlodkt00n7aonjy0',
                'amzn1.echo-api.session.localinteraction-nlodkt00n7aonjy0'],
                                                      'suggest_keywords': None,
                                                      'text': ['open gunrock and yes', 'open gunrock and brad Pitt'],
                                                      'user_id': ['jarnold-000009921212', 'jarnold-000009921212']}

    botWhoIsYourFavouriteDirector_userAnsChristopherNolan = {
        'features': [{'asrcorrection': {'christopher nolan': ['christopher nolan'],
                                        'christopher nolan as well': ['christopher '
                                                                      'nolan']},
                      'central_elem': {'DA': 'statement',
                                       'DA_score': '0.9756',
                                       'backstory': {'confidence': 0.6197291016578674,
                                                     'followup': 'Who is your '
                                                                 'favorite director? ',
                                                     'module': 'MOVIECHAT',
                                                     'tag': 'movie',
                                                     'text': "That's a hard question "
                                                             '<break time="300ms"/> '
                                                             "I'm think Ava DuVernay. "
                                                             'I loved her films 13th '
                                                             'and Selma. I think '
                                                             "she's very talented. "},
                                       'module': ['MOVIECHAT'],
                                       'np': ['christopher nolan',
                                              'christopher nolan as well'],
                                       'regex': {'lexical': ['ans_like'],
                                                 'sys': [],
                                                 'topic': []},
                                       'senti': 'pos',
                                       'text': 'i like christopher nolan as well'},
                      'concept': [{'data': {'director': '0.5',
                                            'filmmaker': '0.2759',
                                            'high profile film director': '0.2241'},
                                   'noun': 'christopher nolan'},
                                  {'data': {}, 'noun': 'christopher nolan as well'}],
                      'converttext': 'open gunrock and me too I like Christopher '
                                     "Nolan as well he's great",
                      'coreference': None,
                      'dependency_parsing': None,
                      'dialog_act': [{'DA': 'opinion',
                                      'confidence': 0.9985,
                                      'text': 'me too'},
                                     {'DA': 'statement',
                                      'confidence': 0.9756,
                                      'text': 'i like christopher nolan as well'},
                                     {'DA': 'opinion',
                                      'confidence': 0.4977,
                                      'text': "he's great"},
                                     {'response_da': [['closing', '1.0'],
                                                      ['opinion', '0.9891'],
                                                      ['open_question_opinion',
                                                       '0.9934'],
                                                      ['opinion', '0.7284'],
                                                      ['opinion', '1.0']]}],
                      'googlekg': [[],
                                   [['Christopher Nolan',
                                     'Filmmaker',
                                     '728.310791',
                                     'movie']],
                                   []],
                      'intent': 'general',
                      'intent_classify': {'lexical': ['ans_pos'],
                                          'sys': [],
                                          'topic': []},
                      'intent_classify2': [{'lexical': ['ans_pos'],
                                            'sys': [],
                                            'topic': []},
                                           {'lexical': ['ans_like'],
                                            'sys': [],
                                            'topic': []},
                                           {'lexical': ['ans_pos'],
                                            'sys': [],
                                            'topic': []}],
                      'knowledge': [['Christopher Nolan',
                                     'Filmmaker',
                                     '728.310791',
                                     'movie']],
                      'ner': [{'begin_offset': 31,
                               'end_offset': 48,
                               'label': 'PERSON',
                               'text': 'Christopher Nolan'}],
                      'noun_phrase': ['christopher nolan',
                                      'christopher nolan as well'],
                      'nounphrase2': {'local_noun_phrase': [[],
                                                            ['christopher nolan'],
                                                            []],
                                      'noun_phrase': [[],
                                                      ['christopher nolan',
                                                       'christopher nolan as well'],
                                                      []]},
                      'npknowledge': {'knowledge': [['Christopher Nolan',
                                                     'Filmmaker',
                                                     '728.310791',
                                                     'movie']],
                                      'local_noun_phrase': ['christopher nolan'],
                                      'noun_phrase': ['christopher nolan',
                                                      'christopher nolan as well']},
                      'profanity_check': [0],
                      'returnnlp': [{'concept': [],
                                     'dependency_parsing': None,
                                     'dialog_act': ['opinion', '0.9985'],
                                     'googlekg': [],
                                     'intent': {'lexical': ['ans_pos'],
                                                'sys': [],
                                                'topic': []},
                                     'noun_phrase': [],
                                     'sentiment': {'compound': '0.0',
                                                   'neg': '0.0',
                                                   'neu': '1.0',
                                                   'pos': '0.0'},
                                     'text': 'me too',
                                     'topic': {'confidence': 999.0,
                                               'text': 'me too',
                                               'topicClass': 'Phatic',
                                               'topicKeywords': []}},
                                    {'concept': [{'data': {'director': '0.5',
                                                           'filmmaker': '0.2759',
                                                           'high profile film director': '0.2241'},
                                                  'noun': 'christopher nolan'},
                                                 {'data': {},
                                                  'noun': 'christopher nolan as '
                                                          'well'}],
                                     'dependency_parsing': None,
                                     'dialog_act': ['statement', '0.9756'],
                                     'googlekg': [['Christopher Nolan',
                                                   'Filmmaker',
                                                   '728.310791',
                                                   'movie']],
                                     'intent': {'lexical': ['ans_like'],
                                                'sys': [],
                                                'topic': []},
                                     'noun_phrase': ['christopher nolan',
                                                     'christopher nolan as well'],
                                     'sentiment': {'compound': '0.5574',
                                                   'neg': '0.0',
                                                   'neu': '0.395',
                                                   'pos': '0.605'},
                                     'text': 'i like christopher nolan as well',
                                     'topic': {'confidence': 999.0,
                                               'text': 'i like christopher nolan as '
                                                       'well',
                                               'topicClass': 'Celebrities',
                                               'topicKeywords': [{'confidence': 999.0,
                                                                  'keyword': 'christopher '
                                                                             'nolan'}]}},
                                    {'concept': [],
                                     'dependency_parsing': None,
                                     'dialog_act': ['opinion', '0.4977'],
                                     'googlekg': [],
                                     'intent': {'lexical': ['ans_pos'],
                                                'sys': [],
                                                'topic': []},
                                     'noun_phrase': [],
                                     'sentiment': {'compound': '0.6249',
                                                   'neg': '0.0',
                                                   'neu': '0.328',
                                                   'pos': '0.672'},
                                     'text': "he's great",
                                     'topic': {'confidence': 999.0,
                                               'text': "he's great",
                                               'topicClass': 'Phatic',
                                               'topicKeywords': []}}],
                      'segmentation': {'segmented_text': ['me too',
                                                          'i like ner as well',
                                                          "he's great"],
                                       'segmented_text_raw': ['me too',
                                                              'i like christopher '
                                                              'nolan as well',
                                                              "he's great"]},
                      'sentiment': 'compound',
                      'sentiment2': [{'compound': '0.0',
                                      'neg': '0.0',
                                      'neu': '1.0',
                                      'pos': '0.0'},
                                     {'compound': '0.5574',
                                      'neg': '0.0',
                                      'neu': '0.395',
                                      'pos': '0.605'},
                                     {'compound': '0.6249',
                                      'neg': '0.0',
                                      'neu': '0.328',
                                      'pos': '0.672'}],
                      'spacynp': ['christopher nolan'],
                      'text': 'open gunrock and me too I like Christopher Nolan as '
                              "well he's great",
                      'topic': [{'confidence': 0.528,
                                 'text': 'open gunrock and me too I like Christopher '
                                         "Nolan as well he's great",
                                 'topicClass': 'Other',
                                 'topicKeywords': [{'confidence': 0.049,
                                                    'keyword': 'Christopher Nolan'}]}],
                      'topic2': [{'confidence': 999.0,
                                  'text': 'me too',
                                  'topicClass': 'Phatic',
                                  'topicKeywords': []},
                                 {'confidence': 999.0,
                                  'text': 'i like christopher nolan as well',
                                  'topicClass': 'Celebrities',
                                  'topicKeywords': [{'confidence': 999.0,
                                                     'keyword': 'christopher nolan'}]},
                                 {'confidence': 999.0,
                                  'text': "he's great",
                                  'topicClass': 'Phatic',
                                  'topicKeywords': []}],
                      'topic_keywords': [],
                      'topic_module': ''},
                     None],
        'last_module': 'MOVIECHAT',
        'moviechat_context': [None, {'last_module': True}],
        'moviechat_user': {'ask_movietv_repeat': True,
                           'current_loop': {},
                           'current_subdialog': 'ChitchatDialog',
                           'current_transition': 't_chitchat',
                           'flow_interruption': False,
                           'last_propose_actor': 'Leonardo DiCaprio',
                           'movie_q_idx': 1,
                           'not_first_time': True,
                           'propose_continue': 'CONTINUE',
                           'subdialog_context': {'current_state': 's_chitchat',
                                                 'old_state': 's_chitchat',
                                                 'tags': ['q2_question'],
                                                 'transition_history': ['r_t0',
                                                                        'r_t0',
                                                                        'r_t0',
                                                                        'r_t2',
                                                                        'r_t3']},
                           'tags': []},
        'prev_hash': {'5738': ['c1'],
                      '6812': ['1c'],
                      '6f18': ['53'],
                      '8363': ['b5'],
                      '87b6': ['49'],
                      '96db': ['a9'],
                      'ac48': ['d2'],
                      'b0d9': ['9e'],
                      'selector_history': ['ac48', '6812', '8363', '96db', '5738'],
                      'sys_env': 'local',
                      'used_topic': ['LAUNCHGREETING',
                                     'MOVIECHAT',
                                     'TRAVELCHAT',
                                     'FOODCHAT',
                                     'GAMECHAT']},
        'resp_type': [None, None],
        'response': 'My pleasure! <break time="150ms" /> Hm, okay sure. <break '
                    'time="150ms" /> Do you have a favorite director? I\'ll watch '
                    'anything by Christopher Nolan. <break time="150ms" /> He has so '
                    'many great movies like Inception and Interstellar.',
        'session_id': ['amzn1.echo-api.session.localinteraction-8zojkgfjfjhrrrt7',
                       'amzn1.echo-api.session.localinteraction-8zojkgfjfjhrrrt7'],
        'suggest_keywords': '',
        'text': ["open gunrock and me too I like Christopher Nolan as well he's great",
                 'open gunrock and no thank you'],
        'user_id': ['jarnold', 'jarnold']}

    botWhatIsYourFavouriteActor_userAnsLeonardoDicaprio = {
        'features': [{'asrcorrection': {'leonardo dicaprio': ['leonardo dicaprio']},
                      'central_elem': {'DA': 'statement',
                                       'DA_score': '0.9916',
                                       'backstory': {'confidence': 0.7263971567153931,
                                                     'module': 'MOVIECHAT',
                                                     'text': 'I dunno <break '
                                                             'time="300ms"/> probably '
                                                             "Inception, but that's a "
                                                             'tough question. '},
                                       'module': ['MOVIECHAT', 'MUSICCHAT'],
                                       'np': ['leonardo dicaprio'],
                                       'regex': {'lexical': [],
                                                 'sys': [],
                                                 'topic': []},
                                       'senti': 'neu',
                                       'text': 'leonardo dicaprio is mine too'},
                      'concept': [{'data': {'actor': '0.1388',
                                            'celebrity': '0.6327',
                                            'star': '0.2286'},
                                   'noun': 'leonardo dicaprio'}],
                      'converttext': 'open gunrock and Leonardo Dicaprio is mine too',
                      'coreference': None,
                      'dependency_parsing': None,
                      'dialog_act': [{'DA': 'statement',
                                      'confidence': 0.9916,
                                      'text': 'leonardo dicaprio is mine too'},
                                     {'response_da': [['opinion', '0.6403'],
                                                      ['statement', '0.9687'],
                                                      ['open_question_opinion', '1.0'],
                                                      ['statement', '1.0'],
                                                      ['opinion', '1.0']]}],
                      'googlekg': [[['Leonardo DiCaprio',
                                     'American actor',
                                     '851.830322',
                                     'movie']]],
                      'intent': 'general',
                      'intent_classify': {'lexical': [], 'sys': [], 'topic': []},
                      'intent_classify2': [{'lexical': [], 'sys': [], 'topic': []}],
                      'knowledge': [['Leonardo DiCaprio',
                                     'American actor',
                                     '851.830322',
                                     'movie']],
                      'ner': [{'begin_offset': 17,
                               'end_offset': 34,
                               'label': 'PERSON',
                               'text': 'Leonardo DiCaprio'}],
                      'noun_phrase': ['leonardo dicaprio'],
                      'nounphrase2': {'local_noun_phrase': [['leonardo dicaprio']],
                                      'noun_phrase': [['leonardo dicaprio']]},
                      'npknowledge': {'knowledge': [['Leonardo DiCaprio',
                                                     'American actor',
                                                     '851.830322',
                                                     'movie']],
                                      'local_noun_phrase': ['leonardo dicaprio'],
                                      'noun_phrase': ['leonardo dicaprio']},
                      'profanity_check': [0],
                      'returnnlp': [{'concept': [{'data': {'actor': '0.1388',
                                                           'celebrity': '0.6327',
                                                           'star': '0.2286'},
                                                  'noun': 'leonardo dicaprio'}],
                                     'dependency_parsing': None,
                                     'dialog_act': ['statement', '0.9916'],
                                     'googlekg': [['Leonardo DiCaprio',
                                                   'American actor',
                                                   '851.830322',
                                                   'movie']],
                                     'intent': {'lexical': [], 'sys': [], 'topic': []},
                                     'noun_phrase': ['leonardo dicaprio'],
                                     'sentiment': {'compound': '0.0',
                                                   'neg': '0.0',
                                                   'neu': '1.0',
                                                   'pos': '0.0'},
                                     'text': 'leonardo dicaprio is mine too',
                                     'topic': {'confidence': 999.0,
                                               'text': 'leonardo dicaprio is mine too',
                                               'topicClass': 'Celebrities',
                                               'topicKeywords': [{'confidence': 999.0,
                                                                  'keyword': 'leonardo '
                                                                             'dicaprio'}]}}],
                      'segmentation': {'segmented_text': ['ner is mine too'],
                                       'segmented_text_raw': ['leonardo dicaprio is '
                                                              'mine too']},
                      'sentiment': 'neu',
                      'sentiment2': [{'compound': '0.0',
                                      'neg': '0.0',
                                      'neu': '1.0',
                                      'pos': '0.0'}],
                      'spacynp': ['leonardo dicaprio'],
                      'text': 'open gunrock and Leonardo Dicaprio is mine too',
                      'topic': [{'confidence': 0.609,
                                 'text': 'open gunrock and Leonardo Dicaprio is mine '
                                         'too',
                                 'topicClass': 'Movies_TV',
                                 'topicKeywords': [{'confidence': 0.395,
                                                    'keyword': 'Leonardo DiCaprio'}]}],
                      'topic2': [{'confidence': 999.0,
                                  'text': 'leonardo dicaprio is mine too',
                                  'topicClass': 'Celebrities',
                                  'topicKeywords': [{'confidence': 999.0,
                                                     'keyword': 'leonardo '
                                                                'dicaprio'}]}],
                      'topic_keywords': [],
                      'topic_module': ''},
                     None],
        'last_module': 'MOVIECHAT',
        'moviechat_context': [None, {'last_module': True}],
        'moviechat_user': {'ask_movietv_repeat': True,
                           'current_loop': {},
                           'current_subdialog': 'ChitchatDialog',
                           'current_transition': 't_chitchat',
                           'flow_interruption': False,
                           'movie_q_idx': 1,
                           'not_first_time': True,
                           'propose_continue': 'CONTINUE',
                           'subdialog_context': {'current_state': 's_chitchat',
                                                 'old_state': 's_init',
                                                 'tags': ['q1_question'],
                                                 'transition_history': ['r_t0',
                                                                        'r_t0',
                                                                        'r_t0',
                                                                        'r_t0',
                                                                        'r_t2']},
                           'tags': []},
        'prev_hash': {'5738': ['c1'],
                      '6f18': ['53'],
                      '87b6': ['49'],
                      '96db': ['a9'],
                      'b0d9': ['9e'],
                      'selector_history': ['96db', '5738', '6f18', 'b0d9', '87b6'],
                      'sys_env': 'local',
                      'used_topic': ['LAUNCHGREETING',
                                     'MOVIECHAT',
                                     'TRAVELCHAT',
                                     'FOODCHAT']},
        'resp_type': [None, None],
        'response': '<prosody rate="95%">It\'s okay if you\'re not sure! <break '
                    'time="150ms" /> So, I was thinking. <break time="150ms" /> '
                    'Who\'s your favorite actor or actress? <break time="150ms" /> '
                    'Leonardo DiCaprio is mine! <break time="150ms" /> He '
                    'consistently gives a great performance, and he uses his fame to '
                    'help the environment too.</prosody>',
        'session_id': ['amzn1.echo-api.session.localinteraction-8zojkgfjfjhrrrt7',
                       'amzn1.echo-api.session.localinteraction-8zojkgfjfjhrrrt7'],
        'suggest_keywords': None,
        'text': ['open gunrock and Leonardo Dicaprio is mine too',
                 "open gunrock and I'm not sure"],
        'user_id': ['jarnold', 'jarnold']}

    botWhatWouldYouRecommend_userAnsSpiderman = {'features': [{'asrcorrection': {'spiderman': ['spider man']},
                                                               'central_elem': {'DA': 'opinion',
                                                                                'DA_score': '0.9964',
                                                                                'backstory': {
                                                                                    'confidence': 0.5748897790908813,
                                                                                    'followup': 'Which do you like '
                                                                                                'better: Marvel '
                                                                                                'superhero movies or '
                                                                                                'DC superhero '
                                                                                                'movies? ',
                                                                                    'module': 'MOVIECHAT',
                                                                                    'tag': 'movie',
                                                                                    'text': 'Black Panther was '
                                                                                            'beautifully filmed and '
                                                                                            'equally exciting! '},
                                                                                'module': ['MOVIECHAT',
                                                                                           'MOVIECHAT',
                                                                                           'MOVIECHAT',
                                                                                           'MOVIECHAT'],
                                                                                'np': ['spiderman'],
                                                                                'regex': {'lexical': [],
                                                                                          'sys': [],
                                                                                          'topic': []},
                                                                                'senti': 'neu',
                                                                                'text': 'spiderman'},
                                                               'concept': [{'data': {'character': '0.4785',
                                                                                     'movie': '0.1774',
                                                                                     'superhero': '0.3441'},
                                                                            'noun': 'spiderman'}],
                                                               'converttext': 'open gunrock and spiderman',
                                                               'coreference': None,
                                                               'dependency_parsing': None,
                                                               'dialog_act': [{'DA': 'opinion',
                                                                               'confidence': 0.9964,
                                                                               'text': 'spiderman'},
                                                                              {'response_da': [
                                                                                  ['yes_no_question', '0.7237'],
                                                                                  ['opinion', '0.3515'],
                                                                                  ['opinion', '0.9999'],
                                                                                  ['statement', '1.0'],
                                                                                  ['open_question_opinion',
                                                                                   '0.9999']]}],
                                                               'googlekg': [[['spiderman',
                                                                              'Australian TV series',
                                                                              '23.274673',
                                                                              '__EMPTY__']]],
                                                               'intent': 'general',
                                                               'intent_classify': {'lexical': [], 'sys': [],
                                                                                   'topic': []},
                                                               'intent_classify2': [
                                                                   {'lexical': [], 'sys': [], 'topic': []}],
                                                               'knowledge': [['spiderman',
                                                                              'Australian TV series',
                                                                              '23.274673',
                                                                              '__EMPTY__']],
                                                               'ner': None,
                                                               'noun_phrase': ['spiderman'],
                                                               'nounphrase2': {'local_noun_phrase': [['spiderman']],
                                                                               'noun_phrase': [['spiderman']]},
                                                               'npknowledge': {'knowledge': [['spiderman',
                                                                                              'Australian TV series',
                                                                                              '23.274673',
                                                                                              '__EMPTY__']],
                                                                               'local_noun_phrase': ['spiderman'],
                                                                               'noun_phrase': ['spiderman']},
                                                               'profanity_check': [0],
                                                               'returnnlp': [
                                                                   {'concept': [{'data': {'character': '0.4785',
                                                                                          'movie': '0.1774',
                                                                                          'superhero': '0.3441'},
                                                                                 'noun': 'spiderman'}],
                                                                    'dependency_parsing': None,
                                                                    'dialog_act': ['opinion', '0.9964'],
                                                                    'googlekg': [['spiderman',
                                                                                  'Australian TV series',
                                                                                  '23.274673',
                                                                                  '__EMPTY__']],
                                                                    'intent': {'lexical': [], 'sys': [], 'topic': []},
                                                                    'noun_phrase': ['spiderman'],
                                                                    'sentiment': {'compound': '0.0',
                                                                                  'neg': '0.0',
                                                                                  'neu': '1.0',
                                                                                  'pos': '0.0'},
                                                                    'text': 'spiderman',
                                                                    'topic': {'confidence': 999.0,
                                                                              'text': 'spiderman',
                                                                              'topicClass': 'Music',
                                                                              'topicKeywords': [{'confidence': 999.0,
                                                                                                 'keyword': 'spiderman'}]}}],
                                                               'segmentation': {'segmented_text': ['spiderman'],
                                                                                'segmented_text_raw': ['spiderman']},
                                                               'sentiment': 'neu',
                                                               'sentiment2': [{'compound': '0.0',
                                                                               'neg': '0.0',
                                                                               'neu': '1.0',
                                                                               'pos': '0.0'}],
                                                               'spacynp': ['spiderman'],
                                                               'text': 'open gunrock and spiderman',
                                                               'topic': [{'confidence': 0.674,
                                                                          'text': 'open gunrock and spiderman',
                                                                          'topicClass': 'Movies_TV',
                                                                          'topicKeywords': [{'confidence': 0.439,
                                                                                             'keyword': 'open'}]}],
                                                               'topic2': [{'confidence': 999.0,
                                                                           'text': 'spiderman',
                                                                           'topicClass': 'Music',
                                                                           'topicKeywords': [{'confidence': 999.0,
                                                                                              'keyword': 'spiderman'}]}],
                                                               'topic_keywords': [],
                                                               'topic_module': ''},
                                                              None],
                                                 'last_module': 'MOVIECHAT',
                                                 'moviechat_context': [None, {'last_module': True}],
                                                 'moviechat_user': {'ask_movietv_repeat': True,
                                                                    'current_loop': {},
                                                                    'current_transition': 't_ask_movietv',
                                                                    'flow_interruption': False,
                                                                    'movie_q_idx': 1,
                                                                    'not_first_time': True,
                                                                    'propose_continue': 'CONTINUE',
                                                                    'subdialog_context': {},
                                                                    'tags': []},
                                                 'prev_hash': {'408d': ['23'],
                                                               '5738': ['c1'],
                                                               '6f18': ['53'],
                                                               '87b6': ['49'],
                                                               'bf35': ['9c'],
                                                               'f118': ['1d'],
                                                               'selector_history': ['5738', 'bf35', 'f118', '6f18',
                                                                                    '408d'],
                                                               'sys_env': 'local',
                                                               'used_topic': ['LAUNCHGREETING', 'MOVIECHAT',
                                                                              'ANIMALCHAT']},
                                                 'resp_type': [None, None],
                                                 'response': '<prosody rate="95%">So you haven\'t. <break time="150ms" /> '
                                                             'Maybe you\'ve been busy. <break time="150ms" /> Um, Anyways, I '
                                                             'love to discover new movies. I recently watched Crazy Rich '
                                                             'Asians because someone told me it was hilarious. <break '
                                                             'time="150ms" /> What movie would you recommend to me?</prosody>',
                                                 'session_id': [
                                                     'amzn1.echo-api.session.localinteraction-5ybclaupy10y8s9h',
                                                     'amzn1.echo-api.session.localinteraction-5ybclaupy10y8s9h'],
                                                 'suggest_keywords': '',
                                                 'text': ['open gunrock and spiderman', 'open gunrock and nope'],
                                                 'user_id': ['jarnold', 'jarnold']}

    botAskDoYouLikeToWatchMovie_userAnsPositive = {
        'intent': ['general', 'general'],
        'slots': [{
            'text': {
                'name': 'text',
                'value': 'open gunrock and yes',
                'confirmationStatus': 'NONE',
                'source': 'USER'
            }
        }, {
            'text': {
                'name': 'text',
                'source': 'USER',
                'value': 'open gunrock and good',
                'confirmationStatus': 'NONE'
            }
        }],
        'text': ['open gunrock and yes', 'open gunrock and good'],
        'features': [{
            'returnnlp': [{
                'text': 'yes',
                'dialog_act': ['pos_answer', '1.0'],
                'intent': {
                    'sys': [],
                    'topic': [],
                    'lexical': ['ans_pos']
                },
                'sentiment': None,
                'googlekg': [],
                'noun_phrase': [],
                'topic': {
                    'topicClass': 'Phatic',
                    'confidence': 999.0,
                    'text': 'yes',
                    'topicKeywords': []
                },
                'concept': []
            }],
            'intent_classify': {
                'sys': [],
                'topic': [],
                'lexical': ['ans_pos']
            },
            'topic2': [{
                'topicClass': 'Phatic',
                'confidence': 999.0,
                'text': 'yes',
                'topicKeywords': []
            }],
            'concept': [],
            'intent': 'general',
            'converttext': 'open gunrock and yes',
            'profanity_check': [0],
            'segmentation': {
                'segmented_text': ['yes'],
                'segmented_text_raw': ['yes']
            },
            'asrcorrection': None,
            'googlekg': [
                []
            ],
            'ner': None,
            'nounphrase2': {
                'noun_phrase': [
                    []
                ],
                'local_noun_phrase': [
                    []
                ]
            },
            'npknowledge': {
                'knowledge': [],
                'noun_phrase': [],
                'local_noun_phrase': []
            },
            'topic': [{
                'topicClass': 'Sports',
                'confidence': 0.289,
                'text': 'open gunrock and yes',
                'topicKeywords': [{
                    'confidence': 0.347,
                    'keyword': '__UNK_WORD__'
                }]
            }],
            'sentiment': None,
            'dialog_act': [{
                'text': 'yes',
                'DA': 'pos_answer',
                'confidence': 1.0
            }, {
                'response_da': [
                    ['statement', '0.9999'],
                    ['statement', '0.6203'],
                    ['statement', '0.9919'],
                    ['opinion', '0.9996']
                ]
            }],
            'spacynp': None,
            'coreference': None,
            'text': 'open gunrock and yes',
            'sentiment2': None,
            'intent_classify2': [{
                'sys': [],
                'topic': [],
                'lexical': ['ans_pos']
            }],
            'topic_module': '',
            'topic_keywords': [],
            'knowledge': [],
            'noun_phrase': [],
            'central_elem': {
                'senti': 'pos',
                'regex': {
                    'sys': [],
                    'topic': [],
                    'lexical': ['ans_pos']
                },
                'DA': 'pos_answer',
                'DA_score': '1.0',
                'module': [],
                'np': [],
                'text': 'yes',
                'backstory': {
                    'confidence': 0.4728534519672394,
                    'text': 'Oh, I was just trying to get to know more about you. '
                }
            }
        }, None],
        'moviechat_context': [None, None],
        'user_id': ['12345666', '12345666'],
        'session_id': ['amzn1.echo-api.session.localinteraction-leozzew3akq2gkjp',
                       'amzn1.echo-api.session.localinteraction-leozzew3akq2gkjp'],
        'resp_type': [None, None],
        'suggest_keywords': None,
        'last_module': 'LAUNCHGREETING',
        'prev_hash': {
            'sys_env': 'local',
            'bf35': ['9c'],
            '87b6': ['49'],
            'used_topic': ['LAUNCHGREETING', 'MOVIECHAT'],
            'selector_history': ['bf35', 'f118', '6f18', 'b0d9', '87b6'],
            '6f18': ['53'],
            'f118': ['6d'],
            'b0d9': ['9e']
        },
        'moviechat_user': {
            'propose_continue': 'STOP',
            'current_transition': 't_init'
        },
        'response': 'Nice! I\'m glad you\'re having a good day!  So, I\'m a huge movie fan! <break time="150ms" /> Do you like to watch movies? '
    }

    botAskHaveYouSeenAnyMovieLately_userAnsPositive = {
        'intent': ['general', 'general'],
        'slots': [{
            'text': {
                'name': 'text',
                'value': 'yes',
                'confirmationStatus': 'NONE',
                'source': 'USER'
            }
        }, {
            'text': {
                'name': 'text',
                'source': 'USER',
                'value': 'open gunrock and yes',
                'confirmationStatus': 'NONE'
            }
        }],
        'text': ['yes', 'open gunrock and yes'],
        'features': [{
            'returnnlp': [{
                'text': 'yes',
                'dialog_act': ['pos_answer', '1.0'],
                'intent': {
                    'sys': [],
                    'topic': [],
                    'lexical': ['ans_pos']
                },
                'sentiment': None,
                'googlekg': [],
                'noun_phrase': [],
                'topic': {
                    'topicClass': 'Phatic',
                    'confidence': 999.0,
                    'text': 'yes',
                    'topicKeywords': []
                },
                'concept': []
            }],
            'intent_classify': {
                'sys': [],
                'topic': [],
                'lexical': ['ans_pos']
            },
            'topic2': [{
                'topicClass': 'Phatic',
                'confidence': 999.0,
                'text': 'yes',
                'topicKeywords': []
            }],
            'concept': [],
            'intent': 'general',
            'converttext': 'yes',
            'profanity_check': [0],
            'segmentation': {
                'segmented_text': ['yes'],
                'segmented_text_raw': ['yes']
            },
            'asrcorrection': None,
            'googlekg': [
                []
            ],
            'ner': None,
            'nounphrase2': {
                'noun_phrase': [
                    []
                ],
                'local_noun_phrase': [
                    []
                ]
            },
            'npknowledge': {
                'knowledge': [],
                'noun_phrase': [],
                'local_noun_phrase': []
            },
            'topic': [{
                'topicClass': 'Phatic',
                'confidence': 999.0,
                'text': 'yes',
                'topicKeywords': []
            }],
            'sentiment': None,
            'dialog_act': [{
                'text': 'yes',
                'DA': 'pos_answer',
                'confidence': 1.0
            }, {
                'response_da': [
                    ['yes_no_question', '0.9385'],
                    ['statement', '0.9999'],
                    ['yes_no_question', '1.0']
                ]
            }],
            'spacynp': None,
            'coreference': None,
            'text': 'yes',
            'sentiment2': None,
            'intent_classify2': [{
                'sys': [],
                'topic': [],
                'lexical': ['ans_pos']
            }],
            'topic_module': '',
            'topic_keywords': [],
            'knowledge': [],
            'noun_phrase': [],
            'central_elem': {
                'senti': 'pos',
                'regex': {
                    'sys': [],
                    'topic': [],
                    'lexical': ['ans_pos']
                },
                'DA': 'pos_answer',
                'DA_score': '1.0',
                'module': [],
                'np': [],
                'text': 'yes',
                'backstory': {
                    'confidence': 0.4728534519672394,
                    'text': 'Oh, I was just trying to get to know more about you. '
                }
            }
        }, None],
        'moviechat_context': [None, {
            'last_module': True
        }],
        'user_id': ['12345666', '12345666'],
        'session_id': ['amzn1.echo-api.session.localinteraction-leozzew3akq2gkjp',
                       'amzn1.echo-api.session.localinteraction-leozzew3akq2gkjp'],
        'resp_type': [None, None],
        'suggest_keywords': None,
        'last_module': 'MOVIECHAT',
        'prev_hash': {
            'sys_env': 'local',
            'bf35': ['9c'],
            '87b6': ['49'],
            'used_topic': ['LAUNCHGREETING', 'MOVIECHAT'],
            'selector_history': ['5738', 'bf35', 'f118', '6f18', 'b0d9'],
            '6f18': ['53'],
            'f118': ['6d'],
            'b0d9': ['9e'],
            '5738': ['c1']
        },
        'moviechat_user': {
            'current_loop': {},
            'subdialog_context': {},
            'ask_movietv_repeat': True,
            'current_transition': 't_ask_movietv',
            'flow_interruption': False,
            'not_first_time': True,
            'propose_continue': 'CONTINUE',
            'tags': []
        },
        'response': 'Ouu, I\'m excited to talk about movies. <break time="150ms" /> So. <break time="150ms" /> Umm, have you seen any movies lately?'
    }

    botAskWhatMovie_userAnsDetectableMovieName = {
        "text": [
            "avenger",
            "yes"
        ],
        "features": [
            {
                "spacynp": [
                    "avenger"
                ],
                "nounphrase2": {
                    "noun_phrase": [
                        [
                            "avenger"
                        ]
                    ],
                    "local_noun_phrase": [
                        [
                            "avenger"
                        ]
                    ]
                },
                "intent_classify2": [
                    {
                        "sys": [],
                        "topic": [],
                        "lexical": []
                    }
                ],
                "segmentation": {
                    "segmented_text": [
                        "avenger"
                    ],
                    "segmented_text_raw": [
                        "avenger"
                    ]
                },
                "asrcorrection": None,
                "concept": [
                    {
                        "noun": "avenger",
                        "data": {
                            "thriller": "0.7821",
                            "amd system": "0.1154",
                            "dodge model": "0.1026"
                        }
                    }
                ],
                "topic2": [
                    {
                        "topicClass": "Movies_TV",
                        "confidence": 999.0,
                        "text": "avenger",
                        "topicKeywords": [
                            {
                                "confidence": 999.0,
                                "keyword": "avenger"
                            }
                        ]
                    }
                ],
                "ner": [
                    {
                        "text": "Avenger",
                        "begin_offset": 0,
                        "end_offset": 7,
                        "label": "GPE"
                    }
                ],
                "npknowledge": {
                    "knowledge": [
                        [
                            "Avenger",
                            "Animated series",
                            "29.963589",
                            "movie"
                        ]
                    ],
                    "noun_phrase": [
                        "avenger"
                    ],
                    "local_noun_phrase": [
                        "avenger"
                    ]
                },
                "sentiment": "neu",
                "converttext": "avenger",
                "sentiment2": [
                    {
                        "neg": "0.0",
                        "neu": "1.0",
                        "pos": "0.0",
                        "compound": "0.0"
                    }
                ],
                "dialog_act": [
                    {
                        "text": "avenger",
                        "DA": "opinion",
                        "confidence": 0.9998
                    },
                    {
                        "response_da": [
                            [
                                "statement",
                                "0.9999"
                            ],
                            [
                                "open_question_opinion",
                                "0.9509"
                            ]
                        ]
                    }
                ],
                "googlekg": [
                    [
                        [
                            "Avenger",
                            "Animated series",
                            "29.963589",
                            "movie"
                        ]
                    ]
                ],
                "returnnlp": [
                    {
                        "text": "avenger",
                        "dialog_act": [
                            "opinion",
                            "0.9998"
                        ],
                        "intent": {
                            "sys": [],
                            "topic": [],
                            "lexical": []
                        },
                        "sentiment": {
                            "neg": "0.0",
                            "neu": "1.0",
                            "pos": "0.0",
                            "compound": "0.0"
                        },
                        "googlekg": [
                            [
                                "Avenger",
                                "Animated series",
                                "29.963589",
                                "movie"
                            ]
                        ],
                        "noun_phrase": [
                            "avenger"
                        ],
                        "topic": {
                            "topicClass": "Movies_TV",
                            "confidence": 999.0,
                            "text": "avenger",
                            "topicKeywords": [
                                {
                                    "confidence": 999.0,
                                    "keyword": "avenger"
                                }
                            ]
                        },
                        "concept": [
                            {
                                "noun": "avenger",
                                "data": {
                                    "thriller": "0.7821",
                                    "amd system": "0.1154",
                                    "dodge model": "0.1026"
                                }
                            }
                        ],
                        "dependency_parsing": None
                    }
                ],
                "text": "avenger",
                "topic": [
                    {
                        "topicClass": "Movies_TV",
                        "confidence": 999.0,
                        "text": "avenger",
                        "topicKeywords": [
                            {
                                "confidence": 999.0,
                                "keyword": "avenger"
                            }
                        ]
                    }
                ],
                "intent": "general",
                "dependency_parsing": None,
                "coreference": None,
                "profanity_check": [
                    0
                ],
                "intent_classify": {
                    "sys": [],
                    "topic": [],
                    "lexical": []
                },
                "topic_module": "",
                "topic_keywords": [
                    {
                        "confidence": 999.0,
                        "keyword": "avenger"
                    }
                ],
                "knowledge": [
                    [
                        "Avenger",
                        "Animated series",
                        "29.963589",
                        "movie"
                    ]
                ],
                "noun_phrase": [
                    "avenger"
                ],
                "central_elem": {
                    "senti": "neu",
                    "regex": {
                        "sys": [],
                        "topic": [],
                        "lexical": []
                    },
                    "DA": "opinion",
                    "DA_score": "0.9998",
                    "module": [],
                    "np": [
                        "avenger"
                    ],
                    "text": "avenger",
                    "backstory": {
                        "text": "<prosody rate = 'x-slow'>um, <break time = '350ms'></break> </prosody> I've never considered that. ",
                        "confidence": 0
                    }
                }
            },
            None
        ],
        "moviechat_context": [
            None,
            {
                "last_module": True
            }
        ],
        "user_id": [
            "lesley-090401",
            "lesley-090401"
        ],
        "session_id": [
            "amzn1.echo-api.session.localinteraction-r5n85y3nqdkbjjk0",
            "amzn1.echo-api.session.localinteraction-r5n85y3nqdkbjjk0"
        ],
        "resp_type": [
            None,
            None
        ],
        "suggest_keywords": None,
        "last_module": "MOVIECHAT",
        "prev_hash": {
            "87b6": [
                "c0"
            ],
            "used_topic": [
                "LAUNCHGREETING",
                "MOVIECHAT"
            ],
            "sys_env": "local",
            "selector_history": [
                "b057",
                "b057",
                "5738",
                "b057",
                "87b6"
            ],
            "b057": [
                "dd",
                "d1",
                "62"
            ],
            "5738": [
                "c1"
            ]
        },
        "moviechat_user": {
            "current_loop": {},
            "subdialog_context": {},
            "ask_movietv_repeat": True,
            "current_transition": "t_ask_movietv",
            "flow_interruption": False,
            "not_first_time": True,
            "propose_continue": "CONTINUE",
            "tags": []
        },
        "response": "Nice. <break time=\"150ms\" /> What movie did you see?"
    }

    botAskRating_userAnsNine = {
        "text": [
            "9",
            "avenger"
        ],
        "features": [
            {
                "npknowledge": {
                    "knowledge": [
                        [
                            "Nine",
                            "2009 film",
                            "59.146027",
                            "movie"
                        ]
                    ],
                    "noun_phrase": [
                        "nine"
                    ],
                    "local_noun_phrase": [
                        "nine"
                    ]
                },
                "sentiment2": [
                    {
                        "neg": "0.0",
                        "neu": "1.0",
                        "pos": "0.0",
                        "compound": "0.0"
                    }
                ],
                "returnnlp": [
                    {
                        "text": "nine",
                        "dialog_act": [
                            "opinion",
                            "0.9998"
                        ],
                        "intent": {
                            "sys": [],
                            "topic": [],
                            "lexical": []
                        },
                        "sentiment": {
                            "neg": "0.0",
                            "neu": "1.0",
                            "pos": "0.0",
                            "compound": "0.0"
                        },
                        "googlekg": [
                            [
                                "Nine",
                                "2009 film",
                                "59.146027",
                                "movie"
                            ]
                        ],
                        "noun_phrase": [
                            "nine"
                        ],
                        "topic": {
                            "topicClass": "Phatic",
                            "confidence": 999.0,
                            "text": "nine",
                            "topicKeywords": []
                        },
                        "concept": [
                            {
                                "noun": "nine",
                                "data": {
                                    "card": "0.5",
                                    "series": "0.25",
                                    "chinese game company": "0.25"
                                }
                            }
                        ],
                        "dependency_parsing": None
                    }
                ],
                "nounphrase2": {
                    "noun_phrase": [
                        [
                            "nine"
                        ]
                    ],
                    "local_noun_phrase": [
                        [
                            "nine"
                        ]
                    ]
                },
                "intent_classify2": [
                    {
                        "sys": [],
                        "topic": [],
                        "lexical": []
                    }
                ],
                "concept": [
                    {
                        "noun": "nine",
                        "data": {
                            "card": "0.5",
                            "series": "0.25",
                            "chinese game company": "0.25"
                        }
                    }
                ],
                "spacynp": None,
                "converttext": "nine",
                "dependency_parsing": None,
                "coreference": None,
                "text": "9",
                "segmentation": {
                    "segmented_text": [
                        "nine"
                    ],
                    "segmented_text_raw": [
                        "nine"
                    ]
                },
                "dialog_act": [
                    {
                        "text": "nine",
                        "DA": "opinion",
                        "confidence": 0.9998
                    },
                    {
                        "response_da": [
                            [
                                "commands",
                                "0.9932"
                            ],
                            [
                                "statement",
                                "0.9999"
                            ],
                            [
                                "open_question_opinion",
                                "0.9998"
                            ]
                        ]
                    }
                ],
                "intent_classify": {
                    "sys": [],
                    "topic": [],
                    "lexical": []
                },
                "topic": [
                    {
                        "topicClass": "Other",
                        "confidence": 999.0,
                        "text": "9",
                        "topicKeywords": []
                    }
                ],
                "profanity_check": [
                    0
                ],
                "ner": [
                    {
                        "text": "9",
                        "begin_offset": 0,
                        "end_offset": 1,
                        "label": "CARDINAL"
                    }
                ],
                "intent": "general",
                "sentiment": None,
                "asrcorrection": None,
                "topic2": [
                    {
                        "topicClass": "Phatic",
                        "confidence": 999.0,
                        "text": "nine",
                        "topicKeywords": []
                    }
                ],
                "googlekg": [
                    [
                        [
                            "Nine",
                            "2009 film",
                            "59.146027",
                            "movie"
                        ]
                    ]
                ],
                "topic_module": "",
                "topic_keywords": [],
                "knowledge": [
                    [
                        "Nine",
                        "2009 film",
                        "59.146027",
                        "movie"
                    ]
                ],
                "noun_phrase": [
                    "nine"
                ],
                "central_elem": {
                    "senti": "neu",
                    "regex": {
                        "sys": [],
                        "topic": [],
                        "lexical": []
                    },
                    "DA": "opinion",
                    "DA_score": "0.9998",
                    "module": [],
                    "np": [
                        "nine"
                    ],
                    "text": "nine",
                    "backstory": {
                        "text": "I haven't thought about that before. ",
                        "confidence": 0
                    }
                }
            },
            None
        ],
        "moviechat_context": [
            None,
            {
                "last_module": True
            }
        ],
        "user_id": [
            "lesley-19090402",
            "lesley-19090402"
        ],
        "session_id": [
            "amzn1.echo-api.session.localinteraction-me138d128u45yoht",
            "amzn1.echo-api.session.localinteraction-me138d128u45yoht"
        ],
        "resp_type": [
            None,
            None
        ],
        "suggest_keywords": None,
        "last_module": "MOVIECHAT",
        "prev_hash": {
            "sys_env": "local",
            "b000": [
                "de"
            ],
            "87b6": [
                "c0"
            ],
            "used_topic": [
                "LAUNCHGREETING",
                "MOVIECHAT"
            ],
            "7cd8": [
                "a9"
            ],
            "selector_history": [
                "b057",
                "7cd8",
                "b000",
                "b057",
                "b057"
            ],
            "b057": [
                "dd",
                "d1"
            ],
            "5738": [
                "c1"
            ]
        },
        "moviechat_user": {
            "current_loop": {},
            "subdialog_context": {
                "current_state": "s_chitchat",
                "old_state": "s_init",
                "transition_history": [
                    "r_t0",
                    "r_t0",
                    "r_t0",
                    "r_t0",
                    "r_t2"
                ],
                "tags": [
                    "q1_question",
                    "q_key_q"
                ]
            },
            "current_subdialog": "MovieDialog",
            "ask_movietv_repeat": True,
            "current_transition": "t_subdialog",
            "flow_interruption": False,
            "not_first_time": True,
            "propose_continue": "CONTINUE",
            "tags": []
        },
        "response": "Great, I know about the avengers! I'm curious. <break time=\"150ms\" /> If you had to rate this movie from 1 to 10, what would it be?"
    }

    botProvideMovieTrivia_userAnsAnyComment = {
        'text': ['sounds good', '9'],
        'features': [{'asrcorrection': None,
                      'central_elem': {'DA': 'appreciation',
                                       'DA_score': '0.9982',
                                       'backstory': {'confidence': 0,
                                                     'text': '<prosody rate = '
                                                             "'x-slow'>um, <break "
                                                             "time = '350ms'></break> "
                                                             "</prosody> I've never "
                                                             'considered that. '},
                                       'module': [],
                                       'np': ['sounds', 'good', 'sounds good'],
                                       'regex': {'lexical': ['ans_pos'],
                                                 'sys': [],
                                                 'topic': []},
                                       'senti': 'pos',
                                       'text': 'sounds good'},
                      'concept': [{'data': {'feature': '0.3148',
                                            'information': '0.313',
                                            'topic': '0.3722'},
                                   'noun': 'sounds'},
                                  {'data': {'concept': '0.2528',
                                            'term': '0.3043',
                                            'word': '0.4428'},
                                   'noun': 'good'},
                                  {'data': {'expression': '1.0'},
                                   'noun': 'sounds good'}],
                      'converttext': 'sounds good',
                      'coreference': None,
                      'dependency_parsing': None,
                      'dialog_act': [{'DA': 'appreciation',
                                      'confidence': 0.9982,
                                      'text': 'sounds good'},
                                     {'response_da': [['statement', '0.9999'],
                                                      ['statement', '0.7075'],
                                                      ['statement', '1.0'],
                                                      ['statement', '0.9408'],
                                                      ['statement', '0.9999'],
                                                      ['statement', '1.0'],
                                                      ['statement', '1.0'],
                                                      ['open_question_opinion',
                                                       '0.9993']]}],
                      'googlekg': [[['sounds',
                                     'Musical instrument',
                                     '39.069687',
                                     '__EMPTY__'],
                                    ['good',
                                     'Novel by Jaroslav Hašek',
                                     '49.204247',
                                     'book'],
                                    ['Sounds Good',
                                     'Television miniseries',
                                     '168.911697',
                                     'movie']]],
                      'intent': 'general',
                      'intent_classify': {'lexical': ['ans_pos'],
                                          'sys': [],
                                          'topic': []},
                      'intent_classify2': [{'lexical': ['ans_pos'],
                                            'sys': [],
                                            'topic': []}],
                      'knowledge': [['sounds',
                                     'Musical instrument',
                                     '39.069687',
                                     '__EMPTY__'],
                                    ['good',
                                     'Novel by Jaroslav Hašek',
                                     '49.204247',
                                     'book'],
                                    ['Sounds Good',
                                     'Television miniseries',
                                     '168.911697',
                                     'movie']],
                      'ner': None,
                      'noun_phrase': ['sounds', 'good', 'sounds good'],
                      'nounphrase2': {'local_noun_phrase': [['sounds', 'good']],
                                      'noun_phrase': [['sounds',
                                                       'good',
                                                       'sounds good']]},
                      'npknowledge': {'knowledge': [['sounds',
                                                     'Musical instrument',
                                                     '39.069687',
                                                     '__EMPTY__'],
                                                    ['good',
                                                     'Novel by Jaroslav Hašek',
                                                     '49.204247',
                                                     'book'],
                                                    ['Sounds Good',
                                                     'Television miniseries',
                                                     '168.911697',
                                                     'movie']],
                                      'local_noun_phrase': ['sounds', 'good'],
                                      'noun_phrase': ['sounds',
                                                      'good',
                                                      'sounds good']},
                      'profanity_check': [0],
                      'returnnlp': [{'concept': [{'data': {'feature': '0.3148',
                                                           'information': '0.313',
                                                           'topic': '0.3722'},
                                                  'noun': 'sounds'},
                                                 {'data': {'concept': '0.2528',
                                                           'term': '0.3043',
                                                           'word': '0.4428'},
                                                  'noun': 'good'},
                                                 {'data': {'expression': '1.0'},
                                                  'noun': 'sounds good'}],
                                     'dependency_parsing': None,
                                     'dialog_act': ['appreciation', '0.9982'],
                                     'googlekg': [['sounds',
                                                   'Musical instrument',
                                                   '39.069687',
                                                   '__EMPTY__'],
                                                  ['good',
                                                   'Novel by Jaroslav Hašek',
                                                   '49.204247',
                                                   'book'],
                                                  ['Sounds Good',
                                                   'Television miniseries',
                                                   '168.911697',
                                                   'movie']],
                                     'intent': {'lexical': ['ans_pos'],
                                                'sys': [],
                                                'topic': []},
                                     'noun_phrase': ['sounds', 'good', 'sounds good'],
                                     'sentiment': {'compound': '0.4404',
                                                   'neg': '0.0',
                                                   'neu': '0.256',
                                                   'pos': '0.744'},
                                     'text': 'sounds good',
                                     'topic': {'confidence': 999.0,
                                               'text': 'sounds good',
                                               'topicClass': 'Phatic',
                                               'topicKeywords': []}}],
                      'segmentation': {'segmented_text': ['sounds good'],
                                       'segmented_text_raw': ['sounds good']},
                      'sentiment': 'pos',
                      'sentiment2': [{'compound': '0.4404',
                                      'neg': '0.0',
                                      'neu': '0.256',
                                      'pos': '0.744'}],
                      'spacynp': None,
                      'text': 'sounds good',
                      'topic': [{'confidence': 999.0,
                                 'text': 'sounds good',
                                 'topicClass': 'Phatic',
                                 'topicKeywords': []}],
                      'topic2': [{'confidence': 999.0,
                                  'text': 'sounds good',
                                  'topicClass': 'Phatic',
                                  'topicKeywords': []}],
                      'topic_keywords': [],
                      'topic_module': ''},
                     None],
        'last_module': 'MOVIECHAT',
        'moviechat_context': [None, {'last_module': True}],
        'moviechat_user': {'ask_movietv_repeat': True,
                           'current_loop': {},
                           'current_subdialog': 'MovieDialog',
                           'current_transition': 't_subdialog',
                           'flow_interruption': False,
                           'not_first_time': True,
                           'propose_continue': 'CONTINUE',
                           'subdialog_context': {'current_state': 's_chitchat',
                                                 'old_state': 's_chitchat',
                                                 'tags': ['q1_ack'],
                                                 'transition_history': ['r_t0',
                                                                        'r_t0',
                                                                        'r_t0',
                                                                        'r_t2',
                                                                        'r_t3']},
                           'tags': []},
        'prev_hash': {'44e4': ['8f'],
                      '5738': ['c1'],
                      '6063': ['59'],
                      '7cd8': ['c6'],
                      '87b6': ['49'],
                      '9d38': ['ab'],
                      'b000': ['10'],
                      'b057': ['dd', 'd1', '62'],
                      'selector_history': ['b057', '44e4', '9d38', '6063', 'b057'],
                      'sys_env': 'local',
                      'used_topic': ['LAUNCHGREETING', 'MOVIECHAT']},
        'resp_type': [None, None],
        'response': '<prosody rate="95%">Nice! 9, huh, you liked it! I enjoyed the '
                    'movie as well. I heard this fact about the movie. <break '
                    'time="150ms" /> Robert Downey Jr. <break time="150ms" /> kept '
                    'food hidden all over the lab set, and apparently nobody could '
                    'find where it was, so they just let him continue doing it. '
                    '<break time="150ms" /> In the movie, that\'s his actual food '
                    "he's offering, and when he was eating, it wasn't scripted, he "
                    'was just hungry. <break time="150ms" /> Any thoughts?</prosody>',
        'session_id': ['amzn1.echo-api.session.localinteraction-tj0orpxk6dnp1f3q',
                       'amzn1.echo-api.session.localinteraction-tj0orpxk6dnp1f3q'],
        'suggest_keywords': None,
        'user_id': ['lesley-19090403', 'lesley-19090403']}

    botWhoIsYourFavouriteDirector_userAnsChristopherNolan = {
        'features': [{'asrcorrection': {'christopher nolan': ['christopher nolan'],
                                        'christopher nolan as well': ['christopher '
                                                                      'nolan']},
                      'central_elem': {'DA': 'statement',
                                       'DA_score': '0.9756',
                                       'backstory': {'confidence': 0.6197291016578674,
                                                     'followup': 'Who is your '
                                                                 'favorite director? ',
                                                     'module': 'MOVIECHAT',
                                                     'tag': 'movie',
                                                     'text': "That's a hard question "
                                                             '<break time="300ms"/> '
                                                             "I'm think Ava DuVernay. "
                                                             'I loved her films 13th '
                                                             'and Selma. I think '
                                                             "she's very talented. "},
                                       'module': ['MOVIECHAT'],
                                       'np': ['christopher nolan',
                                              'christopher nolan as well'],
                                       'regex': {'lexical': ['ans_like'],
                                                 'sys': [],
                                                 'topic': []},
                                       'senti': 'pos',
                                       'text': 'i like christopher nolan as well'},
                      'concept': [{'data': {'director': '0.5',
                                            'filmmaker': '0.2759',
                                            'high profile film director': '0.2241'},
                                   'noun': 'christopher nolan'},
                                  {'data': {}, 'noun': 'christopher nolan as well'}],
                      'converttext': 'open gunrock and me too I like Christopher '
                                     "Nolan as well he's great",
                      'coreference': None,
                      'dependency_parsing': None,
                      'dialog_act': [{'DA': 'opinion',
                                      'confidence': 0.9985,
                                      'text': 'me too'},
                                     {'DA': 'statement',
                                      'confidence': 0.9756,
                                      'text': 'i like christopher nolan as well'},
                                     {'DA': 'opinion',
                                      'confidence': 0.4977,
                                      'text': "he's great"},
                                     {'response_da': [['closing', '1.0'],
                                                      ['opinion', '0.9891'],
                                                      ['open_question_opinion',
                                                       '0.9934'],
                                                      ['opinion', '0.7284'],
                                                      ['opinion', '1.0']]}],
                      'googlekg': [[],
                                   [['Christopher Nolan',
                                     'Filmmaker',
                                     '728.310791',
                                     'movie']],
                                   []],
                      'intent': 'general',
                      'intent_classify': {'lexical': ['ans_pos'],
                                          'sys': [],
                                          'topic': []},
                      'intent_classify2': [{'lexical': ['ans_pos'],
                                            'sys': [],
                                            'topic': []},
                                           {'lexical': ['ans_like'],
                                            'sys': [],
                                            'topic': []},
                                           {'lexical': ['ans_pos'],
                                            'sys': [],
                                            'topic': []}],
                      'knowledge': [['Christopher Nolan',
                                     'Filmmaker',
                                     '728.310791',
                                     'movie']],
                      'ner': [{'begin_offset': 31,
                               'end_offset': 48,
                               'label': 'PERSON',
                               'text': 'Christopher Nolan'}],
                      'noun_phrase': ['christopher nolan',
                                      'christopher nolan as well'],
                      'nounphrase2': {'local_noun_phrase': [[],
                                                            ['christopher nolan'],
                                                            []],
                                      'noun_phrase': [[],
                                                      ['christopher nolan',
                                                       'christopher nolan as well'],
                                                      []]},
                      'npknowledge': {'knowledge': [['Christopher Nolan',
                                                     'Filmmaker',
                                                     '728.310791',
                                                     'movie']],
                                      'local_noun_phrase': ['christopher nolan'],
                                      'noun_phrase': ['christopher nolan',
                                                      'christopher nolan as well']},
                      'profanity_check': [0],
                      'returnnlp': [{'concept': [],
                                     'dependency_parsing': None,
                                     'dialog_act': ['opinion', '0.9985'],
                                     'googlekg': [],
                                     'intent': {'lexical': ['ans_pos'],
                                                'sys': [],
                                                'topic': []},
                                     'noun_phrase': [],
                                     'sentiment': {'compound': '0.0',
                                                   'neg': '0.0',
                                                   'neu': '1.0',
                                                   'pos': '0.0'},
                                     'text': 'me too',
                                     'topic': {'confidence': 999.0,
                                               'text': 'me too',
                                               'topicClass': 'Phatic',
                                               'topicKeywords': []}},
                                    {'concept': [{'data': {'director': '0.5',
                                                           'filmmaker': '0.2759',
                                                           'high profile film director': '0.2241'},
                                                  'noun': 'christopher nolan'},
                                                 {'data': {},
                                                  'noun': 'christopher nolan as '
                                                          'well'}],
                                     'dependency_parsing': None,
                                     'dialog_act': ['statement', '0.9756'],
                                     'googlekg': [['Christopher Nolan',
                                                   'Filmmaker',
                                                   '728.310791',
                                                   'movie']],
                                     'intent': {'lexical': ['ans_like'],
                                                'sys': [],
                                                'topic': []},
                                     'noun_phrase': ['christopher nolan',
                                                     'christopher nolan as well'],
                                     'sentiment': {'compound': '0.5574',
                                                   'neg': '0.0',
                                                   'neu': '0.395',
                                                   'pos': '0.605'},
                                     'text': 'i like christopher nolan as well',
                                     'topic': {'confidence': 999.0,
                                               'text': 'i like christopher nolan as '
                                                       'well',
                                               'topicClass': 'Celebrities',
                                               'topicKeywords': [{'confidence': 999.0,
                                                                  'keyword': 'christopher '
                                                                             'nolan'}]}},
                                    {'concept': [],
                                     'dependency_parsing': None,
                                     'dialog_act': ['opinion', '0.4977'],
                                     'googlekg': [],
                                     'intent': {'lexical': ['ans_pos'],
                                                'sys': [],
                                                'topic': []},
                                     'noun_phrase': [],
                                     'sentiment': {'compound': '0.6249',
                                                   'neg': '0.0',
                                                   'neu': '0.328',
                                                   'pos': '0.672'},
                                     'text': "he's great",
                                     'topic': {'confidence': 999.0,
                                               'text': "he's great",
                                               'topicClass': 'Phatic',
                                               'topicKeywords': []}}],
                      'segmentation': {'segmented_text': ['me too',
                                                          'i like ner as well',
                                                          "he's great"],
                                       'segmented_text_raw': ['me too',
                                                              'i like christopher '
                                                              'nolan as well',
                                                              "he's great"]},
                      'sentiment': 'compound',
                      'sentiment2': [{'compound': '0.0',
                                      'neg': '0.0',
                                      'neu': '1.0',
                                      'pos': '0.0'},
                                     {'compound': '0.5574',
                                      'neg': '0.0',
                                      'neu': '0.395',
                                      'pos': '0.605'},
                                     {'compound': '0.6249',
                                      'neg': '0.0',
                                      'neu': '0.328',
                                      'pos': '0.672'}],
                      'spacynp': ['christopher nolan'],
                      'text': 'open gunrock and me too I like Christopher Nolan as '
                              "well he's great",
                      'topic': [{'confidence': 0.528,
                                 'text': 'open gunrock and me too I like Christopher '
                                         "Nolan as well he's great",
                                 'topicClass': 'Other',
                                 'topicKeywords': [{'confidence': 0.049,
                                                    'keyword': 'Christopher Nolan'}]}],
                      'topic2': [{'confidence': 999.0,
                                  'text': 'me too',
                                  'topicClass': 'Phatic',
                                  'topicKeywords': []},
                                 {'confidence': 999.0,
                                  'text': 'i like christopher nolan as well',
                                  'topicClass': 'Celebrities',
                                  'topicKeywords': [{'confidence': 999.0,
                                                     'keyword': 'christopher nolan'}]},
                                 {'confidence': 999.0,
                                  'text': "he's great",
                                  'topicClass': 'Phatic',
                                  'topicKeywords': []}],
                      'topic_keywords': [],
                      'topic_module': ''},
                     None],
        'last_module': 'MOVIECHAT',
        'moviechat_context': [None, {'last_module': True}],
        'moviechat_user': {'ask_movietv_repeat': True,
                           'current_loop': {},
                           'current_subdialog': 'ChitchatDialog',
                           'current_transition': 't_chitchat',
                           'flow_interruption': False,
                           'last_propose_actor': 'Leonardo DiCaprio',
                           'movie_q_idx': 1,
                           'not_first_time': True,
                           'propose_continue': 'CONTINUE',
                           'subdialog_context': {'current_state': 's_chitchat',
                                                 'old_state': 's_chitchat',
                                                 'tags': ['q2_question'],
                                                 'transition_history': ['r_t0',
                                                                        'r_t0',
                                                                        'r_t0',
                                                                        'r_t2',
                                                                        'r_t3']},
                           'tags': []},
        'prev_hash': {'5738': ['c1'],
                      '6812': ['1c'],
                      '6f18': ['53'],
                      '8363': ['b5'],
                      '87b6': ['49'],
                      '96db': ['a9'],
                      'ac48': ['d2'],
                      'b0d9': ['9e'],
                      'selector_history': ['ac48', '6812', '8363', '96db', '5738'],
                      'sys_env': 'local',
                      'used_topic': ['LAUNCHGREETING',
                                     'MOVIECHAT',
                                     'TRAVELCHAT',
                                     'FOODCHAT',
                                     'GAMECHAT']},
        'resp_type': [None, None],
        'response': 'My pleasure! <break time="150ms" /> Hm, okay sure. <break '
                    'time="150ms" /> Do you have a favorite director? I\'ll watch '
                    'anything by Christopher Nolan. <break time="150ms" /> He has so '
                    'many great movies like Inception and Interstellar.',
        'session_id': ['amzn1.echo-api.session.localinteraction-8zojkgfjfjhrrrt7',
                       'amzn1.echo-api.session.localinteraction-8zojkgfjfjhrrrt7'],
        'suggest_keywords': '',
        'text': ["open gunrock and me too I like Christopher Nolan as well he's great",
                 'open gunrock and no thank you'],
        'user_id': ['jarnold', 'jarnold']}

    botWhatIsYourFavouriteActor_userAnsLeonardoDicaprio = {
        'features': [{'asrcorrection': {'leonardo dicaprio': ['leonardo dicaprio']},
                      'central_elem': {'DA': 'statement',
                                       'DA_score': '0.9916',
                                       'backstory': {'confidence': 0.7263971567153931,
                                                     'module': 'MOVIECHAT',
                                                     'text': 'I dunno <break '
                                                             'time="300ms"/> probably '
                                                             "Inception, but that's a "
                                                             'tough question. '},
                                       'module': ['MOVIECHAT', 'MUSICCHAT'],
                                       'np': ['leonardo dicaprio'],
                                       'regex': {'lexical': [],
                                                 'sys': [],
                                                 'topic': []},
                                       'senti': 'neu',
                                       'text': 'leonardo dicaprio is mine too'},
                      'concept': [{'data': {'actor': '0.1388',
                                            'celebrity': '0.6327',
                                            'star': '0.2286'},
                                   'noun': 'leonardo dicaprio'}],
                      'converttext': 'open gunrock and Leonardo Dicaprio is mine too',
                      'coreference': None,
                      'dependency_parsing': None,
                      'dialog_act': [{'DA': 'statement',
                                      'confidence': 0.9916,
                                      'text': 'leonardo dicaprio is mine too'},
                                     {'response_da': [['opinion', '0.6403'],
                                                      ['statement', '0.9687'],
                                                      ['open_question_opinion', '1.0'],
                                                      ['statement', '1.0'],
                                                      ['opinion', '1.0']]}],
                      'googlekg': [[['Leonardo DiCaprio',
                                     'American actor',
                                     '851.830322',
                                     'movie']]],
                      'intent': 'general',
                      'intent_classify': {'lexical': [], 'sys': [], 'topic': []},
                      'intent_classify2': [{'lexical': [], 'sys': [], 'topic': []}],
                      'knowledge': [['Leonardo DiCaprio',
                                     'American actor',
                                     '851.830322',
                                     'movie']],
                      'ner': [{'begin_offset': 17,
                               'end_offset': 34,
                               'label': 'PERSON',
                               'text': 'Leonardo DiCaprio'}],
                      'noun_phrase': ['leonardo dicaprio'],
                      'nounphrase2': {'local_noun_phrase': [['leonardo dicaprio']],
                                      'noun_phrase': [['leonardo dicaprio']]},
                      'npknowledge': {'knowledge': [['Leonardo DiCaprio',
                                                     'American actor',
                                                     '851.830322',
                                                     'movie']],
                                      'local_noun_phrase': ['leonardo dicaprio'],
                                      'noun_phrase': ['leonardo dicaprio']},
                      'profanity_check': [0],
                      'returnnlp': [{'concept': [{'data': {'actor': '0.1388',
                                                           'celebrity': '0.6327',
                                                           'star': '0.2286'},
                                                  'noun': 'leonardo dicaprio'}],
                                     'dependency_parsing': None,
                                     'dialog_act': ['statement', '0.9916'],
                                     'googlekg': [['Leonardo DiCaprio',
                                                   'American actor',
                                                   '851.830322',
                                                   'movie']],
                                     'intent': {'lexical': [], 'sys': [], 'topic': []},
                                     'noun_phrase': ['leonardo dicaprio'],
                                     'sentiment': {'compound': '0.0',
                                                   'neg': '0.0',
                                                   'neu': '1.0',
                                                   'pos': '0.0'},
                                     'text': 'leonardo dicaprio is mine too',
                                     'topic': {'confidence': 999.0,
                                               'text': 'leonardo dicaprio is mine too',
                                               'topicClass': 'Celebrities',
                                               'topicKeywords': [{'confidence': 999.0,
                                                                  'keyword': 'leonardo '
                                                                             'dicaprio'}]}}],
                      'segmentation': {'segmented_text': ['ner is mine too'],
                                       'segmented_text_raw': ['leonardo dicaprio is '
                                                              'mine too']},
                      'sentiment': 'neu',
                      'sentiment2': [{'compound': '0.0',
                                      'neg': '0.0',
                                      'neu': '1.0',
                                      'pos': '0.0'}],
                      'spacynp': ['leonardo dicaprio'],
                      'text': 'open gunrock and Leonardo Dicaprio is mine too',
                      'topic': [{'confidence': 0.609,
                                 'text': 'open gunrock and Leonardo Dicaprio is mine '
                                         'too',
                                 'topicClass': 'Movies_TV',
                                 'topicKeywords': [{'confidence': 0.395,
                                                    'keyword': 'Leonardo DiCaprio'}]}],
                      'topic2': [{'confidence': 999.0,
                                  'text': 'leonardo dicaprio is mine too',
                                  'topicClass': 'Celebrities',
                                  'topicKeywords': [{'confidence': 999.0,
                                                     'keyword': 'leonardo '
                                                                'dicaprio'}]}],
                      'topic_keywords': [],
                      'topic_module': ''},
                     None],
        'last_module': 'MOVIECHAT',
        'moviechat_context': [None, {'last_module': True}],
        'moviechat_user': {'ask_movietv_repeat': True,
                           'current_loop': {},
                           'current_subdialog': 'ChitchatDialog',
                           'current_transition': 't_chitchat',
                           'flow_interruption': False,
                           'movie_q_idx': 1,
                           'not_first_time': True,
                           'propose_continue': 'CONTINUE',
                           'subdialog_context': {'current_state': 's_chitchat',
                                                 'old_state': 's_init',
                                                 'tags': ['q1_question'],
                                                 'transition_history': ['r_t0',
                                                                        'r_t0',
                                                                        'r_t0',
                                                                        'r_t0',
                                                                        'r_t2']},
                           'tags': []},
        'prev_hash': {'5738': ['c1'],
                      '6f18': ['53'],
                      '87b6': ['49'],
                      '96db': ['a9'],
                      'b0d9': ['9e'],
                      'selector_history': ['96db', '5738', '6f18', 'b0d9', '87b6'],
                      'sys_env': 'local',
                      'used_topic': ['LAUNCHGREETING',
                                     'MOVIECHAT',
                                     'TRAVELCHAT',
                                     'FOODCHAT']},
        'resp_type': [None, None],
        'response': '<prosody rate="95%">It\'s okay if you\'re not sure! <break '
                    'time="150ms" /> So, I was thinking. <break time="150ms" /> '
                    'Who\'s your favorite actor or actress? <break time="150ms" /> '
                    'Leonardo DiCaprio is mine! <break time="150ms" /> He '
                    'consistently gives a great performance, and he uses his fame to '
                    'help the environment too.</prosody>',
        'session_id': ['amzn1.echo-api.session.localinteraction-8zojkgfjfjhrrrt7',
                       'amzn1.echo-api.session.localinteraction-8zojkgfjfjhrrrt7'],
        'suggest_keywords': None,
        'text': ['open gunrock and Leonardo Dicaprio is mine too',
                 "open gunrock and I'm not sure"],
        'user_id': ['jarnold', 'jarnold']}

    botWhatWouldYouRecommend_userAnsSpiderman = {'features': [{'asrcorrection': {'spiderman': ['spider man']},
                                                               'central_elem': {'DA': 'opinion',
                                                                                'DA_score': '0.9964',
                                                                                'backstory': {
                                                                                    'confidence': 0.5748897790908813,
                                                                                    'followup': 'Which do you like '
                                                                                                'better: Marvel '
                                                                                                'superhero movies or '
                                                                                                'DC superhero '
                                                                                                'movies? ',
                                                                                    'module': 'MOVIECHAT',
                                                                                    'tag': 'movie',
                                                                                    'text': 'Black Panther was '
                                                                                            'beautifully filmed and '
                                                                                            'equally exciting! '},
                                                                                'module': ['MOVIECHAT',
                                                                                           'MOVIECHAT',
                                                                                           'MOVIECHAT',
                                                                                           'MOVIECHAT'],
                                                                                'np': ['spiderman'],
                                                                                'regex': {'lexical': [],
                                                                                          'sys': [],
                                                                                          'topic': []},
                                                                                'senti': 'neu',
                                                                                'text': 'spiderman'},
                                                               'concept': [{'data': {'character': '0.4785',
                                                                                     'movie': '0.1774',
                                                                                     'superhero': '0.3441'},
                                                                            'noun': 'spiderman'}],
                                                               'converttext': 'open gunrock and spiderman',
                                                               'coreference': None,
                                                               'dependency_parsing': None,
                                                               'dialog_act': [{'DA': 'opinion',
                                                                               'confidence': 0.9964,
                                                                               'text': 'spiderman'},
                                                                              {'response_da': [
                                                                                  ['yes_no_question', '0.7237'],
                                                                                  ['opinion', '0.3515'],
                                                                                  ['opinion', '0.9999'],
                                                                                  ['statement', '1.0'],
                                                                                  ['open_question_opinion',
                                                                                   '0.9999']]}],
                                                               'googlekg': [[['spiderman',
                                                                              'Australian TV series',
                                                                              '23.274673',
                                                                              '__EMPTY__']]],
                                                               'intent': 'general',
                                                               'intent_classify': {'lexical': [], 'sys': [],
                                                                                   'topic': []},
                                                               'intent_classify2': [
                                                                   {'lexical': [], 'sys': [], 'topic': []}],
                                                               'knowledge': [['spiderman',
                                                                              'Australian TV series',
                                                                              '23.274673',
                                                                              '__EMPTY__']],
                                                               'ner': None,
                                                               'noun_phrase': ['spiderman'],
                                                               'nounphrase2': {'local_noun_phrase': [['spiderman']],
                                                                               'noun_phrase': [['spiderman']]},
                                                               'npknowledge': {'knowledge': [['spiderman',
                                                                                              'Australian TV series',
                                                                                              '23.274673',
                                                                                              '__EMPTY__']],
                                                                               'local_noun_phrase': ['spiderman'],
                                                                               'noun_phrase': ['spiderman']},
                                                               'profanity_check': [0],
                                                               'returnnlp': [
                                                                   {'concept': [{'data': {'character': '0.4785',
                                                                                          'movie': '0.1774',
                                                                                          'superhero': '0.3441'},
                                                                                 'noun': 'spiderman'}],
                                                                    'dependency_parsing': None,
                                                                    'dialog_act': ['opinion', '0.9964'],
                                                                    'googlekg': [['spiderman',
                                                                                  'Australian TV series',
                                                                                  '23.274673',
                                                                                  '__EMPTY__']],
                                                                    'intent': {'lexical': [], 'sys': [], 'topic': []},
                                                                    'noun_phrase': ['spiderman'],
                                                                    'sentiment': {'compound': '0.0',
                                                                                  'neg': '0.0',
                                                                                  'neu': '1.0',
                                                                                  'pos': '0.0'},
                                                                    'text': 'spiderman',
                                                                    'topic': {'confidence': 999.0,
                                                                              'text': 'spiderman',
                                                                              'topicClass': 'Music',
                                                                              'topicKeywords': [{'confidence': 999.0,
                                                                                                 'keyword': 'spiderman'}]}}],
                                                               'segmentation': {'segmented_text': ['spiderman'],
                                                                                'segmented_text_raw': ['spiderman']},
                                                               'sentiment': 'neu',
                                                               'sentiment2': [{'compound': '0.0',
                                                                               'neg': '0.0',
                                                                               'neu': '1.0',
                                                                               'pos': '0.0'}],
                                                               'spacynp': ['spiderman'],
                                                               'text': 'open gunrock and spiderman',
                                                               'topic': [{'confidence': 0.674,
                                                                          'text': 'open gunrock and spiderman',
                                                                          'topicClass': 'Movies_TV',
                                                                          'topicKeywords': [{'confidence': 0.439,
                                                                                             'keyword': 'open'}]}],
                                                               'topic2': [{'confidence': 999.0,
                                                                           'text': 'spiderman',
                                                                           'topicClass': 'Music',
                                                                           'topicKeywords': [{'confidence': 999.0,
                                                                                              'keyword': 'spiderman'}]}],
                                                               'topic_keywords': [],
                                                               'topic_module': ''},
                                                              None],
                                                 'last_module': 'MOVIECHAT',
                                                 'moviechat_context': [None, {'last_module': True}],
                                                 'moviechat_user': {'ask_movietv_repeat': True,
                                                                    'current_loop': {},
                                                                    'current_transition': 't_ask_movietv',
                                                                    'flow_interruption': False,
                                                                    'movie_q_idx': 1,
                                                                    'not_first_time': True,
                                                                    'propose_continue': 'CONTINUE',
                                                                    'subdialog_context': {},
                                                                    'tags': []},
                                                 'prev_hash': {'408d': ['23'],
                                                               '5738': ['c1'],
                                                               '6f18': ['53'],
                                                               '87b6': ['49'],
                                                               'bf35': ['9c'],
                                                               'f118': ['1d'],
                                                               'selector_history': ['5738', 'bf35', 'f118', '6f18',
                                                                                    '408d'],
                                                               'sys_env': 'local',
                                                               'used_topic': ['LAUNCHGREETING', 'MOVIECHAT',
                                                                              'ANIMALCHAT']},
                                                 'resp_type': [None, None],
                                                 'response': '<prosody rate="95%">So you haven\'t. <break time="150ms" /> '
                                                             'Maybe you\'ve been busy. <break time="150ms" /> Um, Anyways, I '
                                                             'love to discover new movies. I recently watched Crazy Rich '
                                                             'Asians because someone told me it was hilarious. <break '
                                                             'time="150ms" /> What movie would you recommend to me?</prosody>',
                                                 'session_id': [
                                                     'amzn1.echo-api.session.localinteraction-5ybclaupy10y8s9h',
                                                     'amzn1.echo-api.session.localinteraction-5ybclaupy10y8s9h'],
                                                 'suggest_keywords': '',
                                                 'text': ['open gunrock and spiderman', 'open gunrock and nope'],
                                                 'user_id': ['jarnold', 'jarnold']}

    botAskDoYouLikeToWatchMovie_userAnsPositive = {
        'intent': ['general', 'general'],
        'slots': [{
            'text': {
                'name': 'text',
                'value': 'open gunrock and yes',
                'confirmationStatus': 'NONE',
                'source': 'USER'
            }
        }, {
            'text': {
                'name': 'text',
                'source': 'USER',
                'value': 'open gunrock and good',
                'confirmationStatus': 'NONE'
            }
        }],
        'text': ['open gunrock and yes', 'open gunrock and good'],
        'features': [{
            'returnnlp': [{
                'text': 'yes',
                'dialog_act': ['pos_answer', '1.0'],
                'intent': {
                    'sys': [],
                    'topic': [],
                    'lexical': ['ans_pos']
                },
                'sentiment': None,
                'googlekg': [],
                'noun_phrase': [],
                'topic': {
                    'topicClass': 'Phatic',
                    'confidence': 999.0,
                    'text': 'yes',
                    'topicKeywords': []
                },
                'concept': []
            }],
            'intent_classify': {
                'sys': [],
                'topic': [],
                'lexical': ['ans_pos']
            },
            'topic2': [{
                'topicClass': 'Phatic',
                'confidence': 999.0,
                'text': 'yes',
                'topicKeywords': []
            }],
            'concept': [],
            'intent': 'general',
            'converttext': 'open gunrock and yes',
            'profanity_check': [0],
            'segmentation': {
                'segmented_text': ['yes'],
                'segmented_text_raw': ['yes']
            },
            'asrcorrection': None,
            'googlekg': [
                []
            ],
            'ner': None,
            'nounphrase2': {
                'noun_phrase': [
                    []
                ],
                'local_noun_phrase': [
                    []
                ]
            },
            'npknowledge': {
                'knowledge': [],
                'noun_phrase': [],
                'local_noun_phrase': []
            },
            'topic': [{
                'topicClass': 'Sports',
                'confidence': 0.289,
                'text': 'open gunrock and yes',
                'topicKeywords': [{
                    'confidence': 0.347,
                    'keyword': '__UNK_WORD__'
                }]
            }],
            'sentiment': None,
            'dialog_act': [{
                'text': 'yes',
                'DA': 'pos_answer',
                'confidence': 1.0
            }, {
                'response_da': [
                    ['statement', '0.9999'],
                    ['statement', '0.6203'],
                    ['statement', '0.9919'],
                    ['opinion', '0.9996']
                ]
            }],
            'spacynp': None,
            'coreference': None,
            'text': 'open gunrock and yes',
            'sentiment2': None,
            'intent_classify2': [{
                'sys': [],
                'topic': [],
                'lexical': ['ans_pos']
            }],
            'topic_module': '',
            'topic_keywords': [],
            'knowledge': [],
            'noun_phrase': [],
            'central_elem': {
                'senti': 'pos',
                'regex': {
                    'sys': [],
                    'topic': [],
                    'lexical': ['ans_pos']
                },
                'DA': 'pos_answer',
                'DA_score': '1.0',
                'module': [],
                'np': [],
                'text': 'yes',
                'backstory': {
                    'confidence': 0.4728534519672394,
                    'text': 'Oh, I was just trying to get to know more about you. '
                }
            }
        }, None],
        'moviechat_context': [None, None],
        'user_id': ['12345666', '12345666'],
        'session_id': ['amzn1.echo-api.session.localinteraction-leozzew3akq2gkjp',
                       'amzn1.echo-api.session.localinteraction-leozzew3akq2gkjp'],
        'resp_type': [None, None],
        'suggest_keywords': None,
        'last_module': 'LAUNCHGREETING',
        'prev_hash': {
            'sys_env': 'local',
            'bf35': ['9c'],
            '87b6': ['49'],
            'used_topic': ['LAUNCHGREETING', 'MOVIECHAT'],
            'selector_history': ['bf35', 'f118', '6f18', 'b0d9', '87b6'],
            '6f18': ['53'],
            'f118': ['6d'],
            'b0d9': ['9e']
        },
        'moviechat_user': {
            'propose_continue': 'STOP',
            'current_transition': 't_init'
        },
        'response': 'Nice! I\'m glad you\'re having a good day!  So, I\'m a huge movie fan! <break time="150ms" /> Do you like to watch movies? '
    }

    botAskHaveYouSeenAnyMovieLately_userAnsPositive = {
        'intent': ['general', 'general'],
        'slots': [{
            'text': {
                'name': 'text',
                'value': 'yes',
                'confirmationStatus': 'NONE',
                'source': 'USER'
            }
        }, {
            'text': {
                'name': 'text',
                'source': 'USER',
                'value': 'open gunrock and yes',
                'confirmationStatus': 'NONE'
            }
        }],
        'text': ['yes', 'open gunrock and yes'],
        'features': [{
            'returnnlp': [{
                'text': 'yes',
                'dialog_act': ['pos_answer', '1.0'],
                'intent': {
                    'sys': [],
                    'topic': [],
                    'lexical': ['ans_pos']
                },
                'sentiment': None,
                'googlekg': [],
                'noun_phrase': [],
                'topic': {
                    'topicClass': 'Phatic',
                    'confidence': 999.0,
                    'text': 'yes',
                    'topicKeywords': []
                },
                'concept': []
            }],
            'intent_classify': {
                'sys': [],
                'topic': [],
                'lexical': ['ans_pos']
            },
            'topic2': [{
                'topicClass': 'Phatic',
                'confidence': 999.0,
                'text': 'yes',
                'topicKeywords': []
            }],
            'concept': [],
            'intent': 'general',
            'converttext': 'yes',
            'profanity_check': [0],
            'segmentation': {
                'segmented_text': ['yes'],
                'segmented_text_raw': ['yes']
            },
            'asrcorrection': None,
            'googlekg': [
                []
            ],
            'ner': None,
            'nounphrase2': {
                'noun_phrase': [
                    []
                ],
                'local_noun_phrase': [
                    []
                ]
            },
            'npknowledge': {
                'knowledge': [],
                'noun_phrase': [],
                'local_noun_phrase': []
            },
            'topic': [{
                'topicClass': 'Phatic',
                'confidence': 999.0,
                'text': 'yes',
                'topicKeywords': []
            }],
            'sentiment': None,
            'dialog_act': [{
                'text': 'yes',
                'DA': 'pos_answer',
                'confidence': 1.0
            }, {
                'response_da': [
                    ['yes_no_question', '0.9385'],
                    ['statement', '0.9999'],
                    ['yes_no_question', '1.0']
                ]
            }],
            'spacynp': None,
            'coreference': None,
            'text': 'yes',
            'sentiment2': None,
            'intent_classify2': [{
                'sys': [],
                'topic': [],
                'lexical': ['ans_pos']
            }],
            'topic_module': '',
            'topic_keywords': [],
            'knowledge': [],
            'noun_phrase': [],
            'central_elem': {
                'senti': 'pos',
                'regex': {
                    'sys': [],
                    'topic': [],
                    'lexical': ['ans_pos']
                },
                'DA': 'pos_answer',
                'DA_score': '1.0',
                'module': [],
                'np': [],
                'text': 'yes',
                'backstory': {
                    'confidence': 0.4728534519672394,
                    'text': 'Oh, I was just trying to get to know more about you. '
                }
            }
        }, None],
        'moviechat_context': [None, {
            'last_module': True
        }],
        'user_id': ['12345666', '12345666'],
        'session_id': ['amzn1.echo-api.session.localinteraction-leozzew3akq2gkjp',
                       'amzn1.echo-api.session.localinteraction-leozzew3akq2gkjp'],
        'resp_type': [None, None],
        'suggest_keywords': None,
        'last_module': 'MOVIECHAT',
        'prev_hash': {
            'sys_env': 'local',
            'bf35': ['9c'],
            '87b6': ['49'],
            'used_topic': ['LAUNCHGREETING', 'MOVIECHAT'],
            'selector_history': ['5738', 'bf35', 'f118', '6f18', 'b0d9'],
            '6f18': ['53'],
            'f118': ['6d'],
            'b0d9': ['9e'],
            '5738': ['c1']
        },
        'moviechat_user': {
            'current_loop': {},
            'subdialog_context': {},
            'ask_movietv_repeat': True,
            'current_transition': 't_ask_movietv',
            'flow_interruption': False,
            'not_first_time': True,
            'propose_continue': 'CONTINUE',
            'tags': []
        },
        'response': 'Ouu, I\'m excited to talk about movies. <break time="150ms" /> So. <break time="150ms" /> Umm, have you seen any movies lately?'
    }

    botAskWhatMovie_userAnsDetectableMovieName = {
        "text": [
            "avenger",
            "yes"
        ],
        "features": [
            {
                "spacynp": [
                    "avenger"
                ],
                "nounphrase2": {
                    "noun_phrase": [
                        [
                            "avenger"
                        ]
                    ],
                    "local_noun_phrase": [
                        [
                            "avenger"
                        ]
                    ]
                },
                "intent_classify2": [
                    {
                        "sys": [],
                        "topic": [],
                        "lexical": []
                    }
                ],
                "segmentation": {
                    "segmented_text": [
                        "avenger"
                    ],
                    "segmented_text_raw": [
                        "avenger"
                    ]
                },
                "asrcorrection": None,
                "concept": [
                    {
                        "noun": "avenger",
                        "data": {
                            "thriller": "0.7821",
                            "amd system": "0.1154",
                            "dodge model": "0.1026"
                        }
                    }
                ],
                "topic2": [
                    {
                        "topicClass": "Movies_TV",
                        "confidence": 999.0,
                        "text": "avenger",
                        "topicKeywords": [
                            {
                                "confidence": 999.0,
                                "keyword": "avenger"
                            }
                        ]
                    }
                ],
                "ner": [
                    {
                        "text": "Avenger",
                        "begin_offset": 0,
                        "end_offset": 7,
                        "label": "GPE"
                    }
                ],
                "npknowledge": {
                    "knowledge": [
                        [
                            "Avenger",
                            "Animated series",
                            "29.963589",
                            "movie"
                        ]
                    ],
                    "noun_phrase": [
                        "avenger"
                    ],
                    "local_noun_phrase": [
                        "avenger"
                    ]
                },
                "sentiment": "neu",
                "converttext": "avenger",
                "sentiment2": [
                    {
                        "neg": "0.0",
                        "neu": "1.0",
                        "pos": "0.0",
                        "compound": "0.0"
                    }
                ],
                "dialog_act": [
                    {
                        "text": "avenger",
                        "DA": "opinion",
                        "confidence": 0.9998
                    },
                    {
                        "response_da": [
                            [
                                "statement",
                                "0.9999"
                            ],
                            [
                                "open_question_opinion",
                                "0.9509"
                            ]
                        ]
                    }
                ],
                "googlekg": [
                    [
                        [
                            "Avenger",
                            "Animated series",
                            "29.963589",
                            "movie"
                        ]
                    ]
                ],
                "returnnlp": [
                    {
                        "text": "avenger",
                        "dialog_act": [
                            "opinion",
                            "0.9998"
                        ],
                        "intent": {
                            "sys": [],
                            "topic": [],
                            "lexical": []
                        },
                        "sentiment": {
                            "neg": "0.0",
                            "neu": "1.0",
                            "pos": "0.0",
                            "compound": "0.0"
                        },
                        "googlekg": [
                            [
                                "Avenger",
                                "Animated series",
                                "29.963589",
                                "movie"
                            ]
                        ],
                        "noun_phrase": [
                            "avenger"
                        ],
                        "topic": {
                            "topicClass": "Movies_TV",
                            "confidence": 999.0,
                            "text": "avenger",
                            "topicKeywords": [
                                {
                                    "confidence": 999.0,
                                    "keyword": "avenger"
                                }
                            ]
                        },
                        "concept": [
                            {
                                "noun": "avenger",
                                "data": {
                                    "thriller": "0.7821",
                                    "amd system": "0.1154",
                                    "dodge model": "0.1026"
                                }
                            }
                        ],
                        "dependency_parsing": None
                    }
                ],
                "text": "avenger",
                "topic": [
                    {
                        "topicClass": "Movies_TV",
                        "confidence": 999.0,
                        "text": "avenger",
                        "topicKeywords": [
                            {
                                "confidence": 999.0,
                                "keyword": "avenger"
                            }
                        ]
                    }
                ],
                "intent": "general",
                "dependency_parsing": None,
                "coreference": None,
                "profanity_check": [
                    0
                ],
                "intent_classify": {
                    "sys": [],
                    "topic": [],
                    "lexical": []
                },
                "topic_module": "",
                "topic_keywords": [
                    {
                        "confidence": 999.0,
                        "keyword": "avenger"
                    }
                ],
                "knowledge": [
                    [
                        "Avenger",
                        "Animated series",
                        "29.963589",
                        "movie"
                    ]
                ],
                "noun_phrase": [
                    "avenger"
                ],
                "central_elem": {
                    "senti": "neu",
                    "regex": {
                        "sys": [],
                        "topic": [],
                        "lexical": []
                    },
                    "DA": "opinion",
                    "DA_score": "0.9998",
                    "module": [],
                    "np": [
                        "avenger"
                    ],
                    "text": "avenger",
                    "backstory": {
                        "text": "<prosody rate = 'x-slow'>um, <break time = '350ms'></break> </prosody> I've never considered that. ",
                        "confidence": 0
                    }
                }
            },
            None
        ],
        "moviechat_context": [
            None,
            {
                "last_module": True
            }
        ],
        "user_id": [
            "lesley-090401",
            "lesley-090401"
        ],
        "session_id": [
            "amzn1.echo-api.session.localinteraction-r5n85y3nqdkbjjk0",
            "amzn1.echo-api.session.localinteraction-r5n85y3nqdkbjjk0"
        ],
        "resp_type": [
            None,
            None
        ],
        "suggest_keywords": None,
        "last_module": "MOVIECHAT",
        "prev_hash": {
            "87b6": [
                "c0"
            ],
            "used_topic": [
                "LAUNCHGREETING",
                "MOVIECHAT"
            ],
            "sys_env": "local",
            "selector_history": [
                "b057",
                "b057",
                "5738",
                "b057",
                "87b6"
            ],
            "b057": [
                "dd",
                "d1",
                "62"
            ],
            "5738": [
                "c1"
            ]
        },
        "moviechat_user": {
            "current_loop": {},
            "subdialog_context": {},
            "ask_movietv_repeat": True,
            "current_transition": "t_ask_movietv",
            "flow_interruption": False,
            "not_first_time": True,
            "propose_continue": "CONTINUE",
            "tags": []
        },
        "response": "Nice. <break time=\"150ms\" /> What movie did you see?"
    }

    botAskRating_userAnsNine = {
        "text": [
            "9",
            "avenger"
        ],
        "features": [
            {
                "npknowledge": {
                    "knowledge": [
                        [
                            "Nine",
                            "2009 film",
                            "59.146027",
                            "movie"
                        ]
                    ],
                    "noun_phrase": [
                        "nine"
                    ],
                    "local_noun_phrase": [
                        "nine"
                    ]
                },
                "sentiment2": [
                    {
                        "neg": "0.0",
                        "neu": "1.0",
                        "pos": "0.0",
                        "compound": "0.0"
                    }
                ],
                "returnnlp": [
                    {
                        "text": "nine",
                        "dialog_act": [
                            "opinion",
                            "0.9998"
                        ],
                        "intent": {
                            "sys": [],
                            "topic": [],
                            "lexical": []
                        },
                        "sentiment": {
                            "neg": "0.0",
                            "neu": "1.0",
                            "pos": "0.0",
                            "compound": "0.0"
                        },
                        "googlekg": [
                            [
                                "Nine",
                                "2009 film",
                                "59.146027",
                                "movie"
                            ]
                        ],
                        "noun_phrase": [
                            "nine"
                        ],
                        "topic": {
                            "topicClass": "Phatic",
                            "confidence": 999.0,
                            "text": "nine",
                            "topicKeywords": []
                        },
                        "concept": [
                            {
                                "noun": "nine",
                                "data": {
                                    "card": "0.5",
                                    "series": "0.25",
                                    "chinese game company": "0.25"
                                }
                            }
                        ],
                        "dependency_parsing": None
                    }
                ],
                "nounphrase2": {
                    "noun_phrase": [
                        [
                            "nine"
                        ]
                    ],
                    "local_noun_phrase": [
                        [
                            "nine"
                        ]
                    ]
                },
                "intent_classify2": [
                    {
                        "sys": [],
                        "topic": [],
                        "lexical": []
                    }
                ],
                "concept": [
                    {
                        "noun": "nine",
                        "data": {
                            "card": "0.5",
                            "series": "0.25",
                            "chinese game company": "0.25"
                        }
                    }
                ],
                "spacynp": None,
                "converttext": "nine",
                "dependency_parsing": None,
                "coreference": None,
                "text": "9",
                "segmentation": {
                    "segmented_text": [
                        "nine"
                    ],
                    "segmented_text_raw": [
                        "nine"
                    ]
                },
                "dialog_act": [
                    {
                        "text": "nine",
                        "DA": "opinion",
                        "confidence": 0.9998
                    },
                    {
                        "response_da": [
                            [
                                "commands",
                                "0.9932"
                            ],
                            [
                                "statement",
                                "0.9999"
                            ],
                            [
                                "open_question_opinion",
                                "0.9998"
                            ]
                        ]
                    }
                ],
                "intent_classify": {
                    "sys": [],
                    "topic": [],
                    "lexical": []
                },
                "topic": [
                    {
                        "topicClass": "Other",
                        "confidence": 999.0,
                        "text": "9",
                        "topicKeywords": []
                    }
                ],
                "profanity_check": [
                    0
                ],
                "ner": [
                    {
                        "text": "9",
                        "begin_offset": 0,
                        "end_offset": 1,
                        "label": "CARDINAL"
                    }
                ],
                "intent": "general",
                "sentiment": None,
                "asrcorrection": None,
                "topic2": [
                    {
                        "topicClass": "Phatic",
                        "confidence": 999.0,
                        "text": "nine",
                        "topicKeywords": []
                    }
                ],
                "googlekg": [
                    [
                        [
                            "Nine",
                            "2009 film",
                            "59.146027",
                            "movie"
                        ]
                    ]
                ],
                "topic_module": "",
                "topic_keywords": [],
                "knowledge": [
                    [
                        "Nine",
                        "2009 film",
                        "59.146027",
                        "movie"
                    ]
                ],
                "noun_phrase": [
                    "nine"
                ],
                "central_elem": {
                    "senti": "neu",
                    "regex": {
                        "sys": [],
                        "topic": [],
                        "lexical": []
                    },
                    "DA": "opinion",
                    "DA_score": "0.9998",
                    "module": [],
                    "np": [
                        "nine"
                    ],
                    "text": "nine",
                    "backstory": {
                        "text": "I haven't thought about that before. ",
                        "confidence": 0
                    }
                }
            },
            None
        ],
        "moviechat_context": [
            None,
            {
                "last_module": True
            }
        ],
        "user_id": [
            "lesley-19090402",
            "lesley-19090402"
        ],
        "session_id": [
            "amzn1.echo-api.session.localinteraction-me138d128u45yoht",
            "amzn1.echo-api.session.localinteraction-me138d128u45yoht"
        ],
        "resp_type": [
            None,
            None
        ],
        "suggest_keywords": None,
        "last_module": "MOVIECHAT",
        "prev_hash": {
            "sys_env": "local",
            "b000": [
                "de"
            ],
            "87b6": [
                "c0"
            ],
            "used_topic": [
                "LAUNCHGREETING",
                "MOVIECHAT"
            ],
            "7cd8": [
                "a9"
            ],
            "selector_history": [
                "b057",
                "7cd8",
                "b000",
                "b057",
                "b057"
            ],
            "b057": [
                "dd",
                "d1"
            ],
            "5738": [
                "c1"
            ]
        },
        "moviechat_user": {
            "current_loop": {},
            "subdialog_context": {
                "current_state": "s_chitchat",
                "old_state": "s_init",
                "transition_history": [
                    "r_t0",
                    "r_t0",
                    "r_t0",
                    "r_t0",
                    "r_t2"
                ],
                "tags": [
                    "q1_question",
                    "q_key_q"
                ]
            },
            "current_subdialog": "MovieDialog",
            "ask_movietv_repeat": True,
            "current_transition": "t_subdialog",
            "flow_interruption": False,
            "not_first_time": True,
            "propose_continue": "CONTINUE",
            "tags": []
        },
        "response": "Great, I know about the avengers! I'm curious. <break time=\"150ms\" /> If you had to rate this movie from 1 to 10, what would it be?"
    }

    botProvideMovieTrivia_userAnsAnyComment = {
        'text': ['sounds good', '9'],
        'features': [{'asrcorrection': None,
                      'central_elem': {'DA': 'appreciation',
                                       'DA_score': '0.9982',
                                       'backstory': {'confidence': 0,
                                                     'text': '<prosody rate = '
                                                             "'x-slow'>um, <break "
                                                             "time = '350ms'></break> "
                                                             "</prosody> I've never "
                                                             'considered that. '},
                                       'module': [],
                                       'np': ['sounds', 'good', 'sounds good'],
                                       'regex': {'lexical': ['ans_pos'],
                                                 'sys': [],
                                                 'topic': []},
                                       'senti': 'pos',
                                       'text': 'sounds good'},
                      'concept': [{'data': {'feature': '0.3148',
                                            'information': '0.313',
                                            'topic': '0.3722'},
                                   'noun': 'sounds'},
                                  {'data': {'concept': '0.2528',
                                            'term': '0.3043',
                                            'word': '0.4428'},
                                   'noun': 'good'},
                                  {'data': {'expression': '1.0'},
                                   'noun': 'sounds good'}],
                      'converttext': 'sounds good',
                      'coreference': None,
                      'dependency_parsing': None,
                      'dialog_act': [{'DA': 'appreciation',
                                      'confidence': 0.9982,
                                      'text': 'sounds good'},
                                     {'response_da': [['statement', '0.9999'],
                                                      ['statement', '0.7075'],
                                                      ['statement', '1.0'],
                                                      ['statement', '0.9408'],
                                                      ['statement', '0.9999'],
                                                      ['statement', '1.0'],
                                                      ['statement', '1.0'],
                                                      ['open_question_opinion',
                                                       '0.9993']]}],
                      'googlekg': [[['sounds',
                                     'Musical instrument',
                                     '39.069687',
                                     '__EMPTY__'],
                                    ['good',
                                     'Novel by Jaroslav Hašek',
                                     '49.204247',
                                     'book'],
                                    ['Sounds Good',
                                     'Television miniseries',
                                     '168.911697',
                                     'movie']]],
                      'intent': 'general',
                      'intent_classify': {'lexical': ['ans_pos'],
                                          'sys': [],
                                          'topic': []},
                      'intent_classify2': [{'lexical': ['ans_pos'],
                                            'sys': [],
                                            'topic': []}],
                      'knowledge': [['sounds',
                                     'Musical instrument',
                                     '39.069687',
                                     '__EMPTY__'],
                                    ['good',
                                     'Novel by Jaroslav Hašek',
                                     '49.204247',
                                     'book'],
                                    ['Sounds Good',
                                     'Television miniseries',
                                     '168.911697',
                                     'movie']],
                      'ner': None,
                      'noun_phrase': ['sounds', 'good', 'sounds good'],
                      'nounphrase2': {'local_noun_phrase': [['sounds', 'good']],
                                      'noun_phrase': [['sounds',
                                                       'good',
                                                       'sounds good']]},
                      'npknowledge': {'knowledge': [['sounds',
                                                     'Musical instrument',
                                                     '39.069687',
                                                     '__EMPTY__'],
                                                    ['good',
                                                     'Novel by Jaroslav Hašek',
                                                     '49.204247',
                                                     'book'],
                                                    ['Sounds Good',
                                                     'Television miniseries',
                                                     '168.911697',
                                                     'movie']],
                                      'local_noun_phrase': ['sounds', 'good'],
                                      'noun_phrase': ['sounds',
                                                      'good',
                                                      'sounds good']},
                      'profanity_check': [0],
                      'returnnlp': [{'concept': [{'data': {'feature': '0.3148',
                                                           'information': '0.313',
                                                           'topic': '0.3722'},
                                                  'noun': 'sounds'},
                                                 {'data': {'concept': '0.2528',
                                                           'term': '0.3043',
                                                           'word': '0.4428'},
                                                  'noun': 'good'},
                                                 {'data': {'expression': '1.0'},
                                                  'noun': 'sounds good'}],
                                     'dependency_parsing': None,
                                     'dialog_act': ['appreciation', '0.9982'],
                                     'googlekg': [['sounds',
                                                   'Musical instrument',
                                                   '39.069687',
                                                   '__EMPTY__'],
                                                  ['good',
                                                   'Novel by Jaroslav Hašek',
                                                   '49.204247',
                                                   'book'],
                                                  ['Sounds Good',
                                                   'Television miniseries',
                                                   '168.911697',
                                                   'movie']],
                                     'intent': {'lexical': ['ans_pos'],
                                                'sys': [],
                                                'topic': []},
                                     'noun_phrase': ['sounds', 'good', 'sounds good'],
                                     'sentiment': {'compound': '0.4404',
                                                   'neg': '0.0',
                                                   'neu': '0.256',
                                                   'pos': '0.744'},
                                     'text': 'sounds good',
                                     'topic': {'confidence': 999.0,
                                               'text': 'sounds good',
                                               'topicClass': 'Phatic',
                                               'topicKeywords': []}}],
                      'segmentation': {'segmented_text': ['sounds good'],
                                       'segmented_text_raw': ['sounds good']},
                      'sentiment': 'pos',
                      'sentiment2': [{'compound': '0.4404',
                                      'neg': '0.0',
                                      'neu': '0.256',
                                      'pos': '0.744'}],
                      'spacynp': None,
                      'text': 'sounds good',
                      'topic': [{'confidence': 999.0,
                                 'text': 'sounds good',
                                 'topicClass': 'Phatic',
                                 'topicKeywords': []}],
                      'topic2': [{'confidence': 999.0,
                                  'text': 'sounds good',
                                  'topicClass': 'Phatic',
                                  'topicKeywords': []}],
                      'topic_keywords': [],
                      'topic_module': ''},
                     None],
        'last_module': 'MOVIECHAT',
        'moviechat_context': [None, {'last_module': True}],
        'moviechat_user': {'ask_movietv_repeat': True,
                           'current_loop': {},
                           'current_subdialog': 'MovieDialog',
                           'current_transition': 't_subdialog',
                           'flow_interruption': False,
                           'not_first_time': True,
                           'propose_continue': 'CONTINUE',
                           'subdialog_context': {'current_state': 's_chitchat',
                                                 'old_state': 's_chitchat',
                                                 'tags': ['q1_ack'],
                                                 'transition_history': ['r_t0',
                                                                        'r_t0',
                                                                        'r_t0',
                                                                        'r_t2',
                                                                        'r_t3']},
                           'tags': []},
        'prev_hash': {'44e4': ['8f'],
                      '5738': ['c1'],
                      '6063': ['59'],
                      '7cd8': ['c6'],
                      '87b6': ['49'],
                      '9d38': ['ab'],
                      'b000': ['10'],
                      'b057': ['dd', 'd1', '62'],
                      'selector_history': ['b057', '44e4', '9d38', '6063', 'b057'],
                      'sys_env': 'local',
                      'used_topic': ['LAUNCHGREETING', 'MOVIECHAT']},
        'resp_type': [None, None],
        'response': '<prosody rate="95%">Nice! 9, huh, you liked it! I enjoyed the '
                    'movie as well. I heard this fact about the movie. <break '
                    'time="150ms" /> Robert Downey Jr. <break time="150ms" /> kept '
                    'food hidden all over the lab set, and apparently nobody could '
                    'find where it was, so they just let him continue doing it. '
                    '<break time="150ms" /> In the movie, that\'s his actual food '
                    "he's offering, and when he was eating, it wasn't scripted, he "
                    'was just hungry. <break time="150ms" /> Any thoughts?</prosody>',
        'session_id': ['amzn1.echo-api.session.localinteraction-tj0orpxk6dnp1f3q',
                       'amzn1.echo-api.session.localinteraction-tj0orpxk6dnp1f3q'],
        'suggest_keywords': None,
        'user_id': ['lesley-19090403', 'lesley-19090403']}