import os
import sys
import re
from types import SimpleNamespace
import yweather
import requests
from yahooweather import YahooWeather, UNIT_F
from geotext import GeoText
import datetime
import json
import logging
import random
import template_manager
import time, uuid
import urllib.request
import urllib.error
import hmac, hashlib
from base64 import b64encode
from nlu.dataclass.returnnlp import ReturnNLP

file_dir = os.path.dirname(__file__)

sys.path.append(file_dir)

template_weather = template_manager.Templates.weather
#2019 yahoo weather api
class yahoo_api(object):
    def __init__(self):
        self.consumer_secret = '16e6ea6d5594cb2bff3cbef82403a50975485cc8'
        self.url = 'https://weather-ydn-yql.media.yahoo.com/forecastrss'
        self.method = 'GET'
        self.app_id = 'Ucwrhz6g'
        self.consumer_key = 'dj0yJmk9NkZMQ3A2enZwUGNZJmQ9WVdrOVZXTjNjbWg2Tm1jbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PWUy'
        self.concat = '&'
    def process(self):
        #Prepare signature string (merge all params and SORT them
        merged_params = self.query.copy()
        merged_params.update(self.oauth)
        sorted_params = [k + '=' + urllib.parse.quote(merged_params[k], safe='') for k in sorted(merged_params.keys())]
        signature_base_str =  self.method + self.concat + urllib.parse.quote(self.url, safe='') + self.concat + urllib.parse.quote(self.concat.join(sorted_params), safe='')
        #Generate signature
        composite_key = urllib.parse.quote(self.consumer_secret, safe='') + self.concat
        oauth_signature = b64encode(hmac.new( bytes(composite_key , 'latin-1') , bytes(signature_base_str,'latin-1'), hashlib.sha1).digest())
        oauth_signature = oauth_signature.decode('utf-8')
        #Prepare Authorization header
        self.oauth['oauth_signature'] = oauth_signature
        auth_header = 'OAuth ' + ', '.join(['{}="{}"'.format(k,v) for k,v in self.oauth.items()])
        #Send request
        self.url = self.url + '?' + urllib.parse.urlencode(self.query)
        opener = urllib.request.build_opener()
        opener.addheaders = [('Authorization', auth_header),
                             ('X-Yahoo-App-Id', self.app_id),
                             ('Pragma', 'no-cache'),
                             ('User-Agent', 'Mozilla/5.0')]
        urllib.request.install_opener(opener)
        try:
            response = urllib.request.urlopen(self.url)
        except urllib.error.HTTPError as e:
            del opener
            logging.error('[Weather]HTTPError, Error code: ', e.code)
            return None
        except urllib.error.URLError as e:
            del opener
            logging.error('[Weather]URLError, Reason: ', e.reason)
            return None
        else:
            data = response.read().decode('utf-8')
            del opener
            target = json.loads(data)
            logging.info('[Weather]HTTP request good!')
            return(target)

    def query_(self, location, unit = 'f'):
        # woeid is the woeid of location (e.g. 2122265 of Moscow) , unit is the temp unit (C or F)
        # Returns a dictionary of weather data
        self.query = {'location': location, 'format': 'json' , 'u' : unit}
        self.url = 'https://weather-ydn-yql.media.yahoo.com/forecastrss'
        self.oauth = {
        'oauth_consumer_key': self.consumer_key,
        'oauth_nonce': uuid.uuid4().hex,
        'oauth_signature_method': 'HMAC-SHA1',
        'oauth_timestamp': str(int(time.time())),
        'oauth_version': '1.0'
        }
        return(self.process())


def get_location(utterance):
    all_places = []
    possible_places = list()
    if utterance.count(' ') <= 1:
        place = GeoText(utterance.title())
        all_places.extend(place.cities)
    elif "in" in utterance:
        lutternace = utterance.split()
        inloc = lutternace.index("in")
        possible_places.append(lutternace[inloc + 1])
        if inloc + 2 <= len(lutternace) - 1:
            possible_places.append(str(lutternace[inloc + 1]) + " " + str(lutternace[inloc + 2]))
        for i in range(0, len(possible_places)):
            place = GeoText(str(possible_places[i]).title())
            all_places.extend(place.cities)
    if len(all_places) > 0:
        # UserAttributes.__setattr__()
        return str(all_places[len(all_places) - 1])
    else:
        return ""


def get_current_time(location):
    # TODO: location awareness
    return datetime.datetime.now() - datetime.timedelta(hours=7)


def generate_day(utternace):
    if "monday" in utternace or "Monday" in utternace:
        return "Monday"
    elif "tuesday" in utternace or "Tuesday" in utternace:
        return "Tuesday"
    elif "wednesday" in utternace or "Wednesday" in utternace:
        return "Wednesday"
    elif "thursday" in utternace or "Thursday" in utternace:
        return "Thursday"
    elif "friday" in utternace or "Friday" in utternace:
        return "Friday"
    elif "saturday" in utternace or "Saturday" in utternace:
        return "Saturday"
    elif "sunday" in utternace or "Sunday" in utternace:
        return "Sunday"
    elif "today" in utternace or "Today" in utternace:
        return get_current_time(None).strftime("%A")
        # return datetime.datetime.now().strftime("%A")
    elif "tomorrow" in utternace or "Tomorrow" in utternace:
        tom = get_current_time(None).date() + datetime.timedelta(days=1)
        # tom = datetime.date.today() + datetime.timedelta(days=1)
        return tom.strftime("%A")
    else:
        return get_current_time(None).strftime("%A")
        # return datetime.datetime.now().strftime("%A")

def parse_now(data_now, location, user_attributes_ref):
    temp = data_now["temperature"]
    text = data_now["text"].lower().replace("thunderstorm", "sunny")
    return template_weather.utterance(selector='temperature/today', slots={
        'location': location, 'temp_curr': temp, 'weather_text': text,
    }, user_attributes_ref=user_attributes_ref)


def parse_forecast(data_forecast, day, location, user_attributes_ref):
    week = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
    if day == "Monday" or day == "monday":
        short_day = week[0]
    elif day == "Tuesday" or day == "tuesday":
        short_day = week[1]
    elif day == "Wednesday" or day == "wednesday":
        short_day = week[2]
    elif day == "Thursday" or day == "thursday":
        short_day = week[3]
    elif day == "Friday" or day == "friday":
        short_day = week[4]
    elif day == "Saturday" or day == "saturday":
        short_day = week[5]
    else:
        short_day = week[6]

    for each in data_forecast:
        if each["day"] == short_day:
            return template_weather.utterance(selector='temperature/forecast', slots={
                'day': day, 'location': location, 'weather_text': each["text"].lower().replace("thunderstorm", "sunny"),
                'temp_high': each["high"], 'temp_low': each["low"]
            }, user_attributes_ref=user_attributes_ref)


def parse_wind(data_wind, user_attributes_ref):
    # TODO: Change to mph
    return template_weather.utterance(selector='wind', slots={
        'speed': data_wind["speed"], 'chill': data_wind["chill"]
    }, user_attributes_ref=user_attributes_ref)


def parse_atmosphere(data_atmosphere, user_attributes_ref):
    atmosphere_data = ""
    humidity = data_atmosphere["humidity"]
    return template_weather.utterance(selector='atmosphere', slots={'humidity': data_atmosphere["humidity"]},
                                      user_attributes_ref=user_attributes_ref)


def parse_astronomy(data_astronomy, user_attributes_ref):
    astronomy_data = ""
    return template_weather.utterance(selector='astronomy', slots={'sunrise': data_astronomy['sunrise'],
                                                                   'sunset': data_astronomy['sunset']},
                                      user_attributes_ref=user_attributes_ref)


#using 2019 new API
def get_weather_info(weather, location, day, curr_utt, user_attributes_ref):
    #header struct, call API
    obj = yahoo_api()
    response = obj.query_(location)
    if response:
        if day == "today":
            now = parse_now(response.get("current_observation", {}).get("condition", None), location, user_attributes_ref=user_attributes_ref)
            return now
        else:
            weather_forcast = response.get("forecasts", {})
            if weather_forcast is not None:
                forecast = parse_forecast(weather_forcast, day, location, user_attributes_ref=user_attributes_ref)
                return forecast
            else:
                missingcity_resp = "It's probably gonna be partly cloudy with a maximum of 74 degree farenheit and a minimum of 52 degree farenheit. <break time=\"200ms\"></break> Is there anything else that I can help you with?"
                return missingcity_resp
    else:
        return template_weather.utterance(selector='error/no_connection', slots={},
                                          user_attributes_ref=user_attributes_ref)

def get_weather_data(weather, location, day, curr_utt, user_attributes_ref):
    if location != '':
        return [get_weather_info(weather, location, day, curr_utt, user_attributes_ref=user_attributes_ref), "STOP"]
    else:
        return [template_weather.utterance(selector='error/no_location', slots={},
                                           user_attributes_ref=user_attributes_ref), "CONTINUE"]


def not_forecast_weather(input_text, user_attributes, location):
    if_question = re.search(r"what|will|can|would", input_text)
    question_tags = re.search(r"rain|hail|snow|hot|sun|cloud", input_text)
    if if_question:
        if re.search(r"tomorrow", input_text):
            current_weather = get_weather_data('weather', location, 'tomorrow', input_text, user_attributes)
        else:
            current_weather = get_weather_data('weather', location, 'today', input_text, user_attributes)
        if question_tags:
            required_tag = question_tags.group()
            if (required_tag == "rain" or required_tag == "rainy") and ("thunderstorm" or "rain" in current_weather):
                return {'response': "According to the forecast, it's probably gonna rain! ",
                        'user_attributes': {
                            'prev_hash': getattr(user_attributes, 'prev_hash', None),
                            'location': location,
                            'weatherchat': {
                                'ask_forecast': 'no',
                                'propose_continue': 'STOP'}
                        }
                        }
            elif (required_tag == "sun" or required_tag == "hot") and ("sun" in current_weather):
                return {'response': "According to the weather report, it should be sunny.",
                        'user_attributes': {
                            'prev_hash': getattr(user_attributes, 'prev_hash', None),
                            'location': location,
                            'weatherchat': {
                                'ask_forecast': 'no',
                                'propose_continue': 'STOP'}
                        }
                        }
            elif required_tag == "snow" and "snow" in current_weather:
                return {'response': "It looks like it might snow.",
                        'user_attributes': {
                            'prev_hash': getattr(user_attributes, 'prev_hash', None),
                            'location': location,
                            'weatherchat': {
                                'ask_forecast': 'no',
                                'propose_continue': 'STOP'}
                        }
                        }
            elif required_tag == "hail" and "hail" in current_weather:
                return {'response': "It might hail. Be careful!",
                        'user_attributes': {
                            'prev_hash': getattr(user_attributes, 'prev_hash', None),
                            'location': location,
                            'weatherchat': {
                                'ask_forecast': 'no',
                                'propose_continue': 'STOP'}
                        }
                        }
            else:
                return {'response': "According to the forecast, I doesn't look like it.",
                        'user_attributes': {
                            'prev_hash': getattr(user_attributes, 'prev_hash', None),
                            'location': location,
                            'weatherchat': {
                                'ask_forecast': 'no',
                                'propose_continue': 'STOP'}
                        }
                        }
        else:
            return {'response': "It seems as if something is clouding my forecast powers. How about we talk about "
                                "music, movies or travel for now?",
                    'user_attributes': {
                        'prev_hash': getattr(user_attributes, 'prev_hash', None),
                        'location': location,
                        'weatherchat': {
                            'ask_forecast': 'no',
                            'propose_continue': 'STOP'}
                        }
                    }


    climate_responses = {"hot": "Yeah, I think it's pretty hot. Remember to drink water and stay cool!",
                         "warm": "It looks like it might get warmer. Hopefully, you'll have beautiful weather and sunshine!",
                         "cold": "It could be pretty cold. Try to keep warm!",
                         "cool": "It would be a bit chilly. Bring a jacket!",
                         "sunny": "It looks like it'll be sunny! Get outdoors and enjoy the sunshine!",
                         "rain": "It looks like it might rain. Bring an umbrella!",
                         "wind": "It could be a bit windy in the morning. Please bring a jacket!"
                         }

    possible_climate = re.search(r"hot|warm|cold|cool|rain|wind|sunny", input_text)
    if possible_climate:
        climate = possible_climate.group()
        return {'response': climate_responses[climate],
                'user_attributes': {
                    'prev_hash': getattr(user_attributes, 'prev_hash', None),
                    'location': location,
                    'weatherchat': {
                        'ask_forecast': 'no',
                        'propose_continue': "STOP"}
                }
                }
    else:
        chitchat_templates = ["Speaking of weather, what’s your favorite type of weather?",
                           "Speaking of weather, do you like summer or winter?"]
        return {'response': random.choice(chitchat_templates),
                'user_attributes': {
                    'prev_hash': getattr(user_attributes, 'prev_hash', None),
                    'location': location,
                    'weatherchat': {
                        'ask_forecast': 'no',
                        'propose_continue': "STOP"}
                }
                }


def parse_input_text(utterance):
    # if usr_location is None or usr_location == '':
    #     location = get_location(str(utterance))
    # else:
    #     location = usr_location
    if 'weather' or 'climate' in utterance:
        weather = 'weather'
    elif 'forecast' in utterance:
        weather = 'forecast'
    elif 'temperature' in utterance:
        weather = 'temperature'
    elif 'humidity' in utterance:
        weather = 'humidity'
    else:
        weather = 'weather'
    day = generate_day(utterance)
    return weather, day


required_context = ['intent', 'slots', 'text', 'features']


def get_required_context():
    return required_context


def check_intersect(a, b):
    a_set = set(a)
    b_set = set(b)
    if (a_set & b_set):
        return True
    else:
        return False

def detect_location_in_entities(google_kg, concept_kg):
    logging.info("[WeatherMessage] The google_kg is {}".format(google_kg))
    logging.info("[WeatherMessage] The concept_kg is {}".format(concept_kg))
    location = ""
    for seg_result in google_kg:
        if seg_result:
            for entity in seg_result:
                logging.info("[WeatherMessage] The entity is {}".format(entity))
                if check_intersect(['City', 'Place', 'AdministrativeArea'], entity.entity_type):
                    location = entity.entity_name
                    return location
    for seg_result in concept_kg:
        if seg_result:
            for entity in seg_result:
                for concept_data in entity.data:
                    if concept_data.description in ['city', 'place', 'region']:
                        location = entity.noun
                        return location
    return location.lower()

def handle_message(msg):
    # your response generator model should operate on the text or other context information here
    # logging.info("[WeatherMessage] The message is {}".format(msg))
    input_text = msg['slots']['text']['value'].lower()
    senti = msg['features']['sentiment']
    intent = msg['features']['intent_classify']
    nlpkg = msg['features']['knowledge']
    returnnlp = ReturnNLP.from_list(msg['returnnlp'])
    dialog_act = msg['features']['central_elem']['DA']
    dialog_act_score = float(msg['features']['central_elem']['DA_score'])
    backstory = msg['features']['central_elem']['backstory']
    module_ls = msg['features']['central_elem']['module']
    is_ask_forecast = msg.get('ask_forecast', 'no')

    user_attributes = SimpleNamespace(**{'template_manager': msg['template_manager']})

    # ask for forecast
    if is_ask_forecast == 'yes':
        weekofday = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
        tomorrow = get_current_time(None).date() + datetime.timedelta(hours=12)
        # tomorrow = datetime.datetime.today() - datetime.timedelta(hours=12) + datetime.timedelta(hours=24)
        input_text = input_text + ' weather forecast on ' + weekofday[tomorrow.weekday()]

    # extract the location
    location = ""
    try:
        #use returnnlp detect entities to search city or place
        if returnnlp:
            logging.info("[WeatherMessage] The returnnlp is {}".format(returnnlp))
            location = detect_location_in_entities(returnnlp.googlekg, returnnlp.concept)
        if location == '':
            ner_list = msg['features']['ner']
            for kg_ls in nlpkg:
                if re.search("travel", kg_ls[3]) or re.search("city|county|island", kg_ls[1].lower()):
                    location = kg_ls[0]
                    continue
        if location == '' and ner_list is not None:
            loc_ner = [ner for ner in ner_list if ner['label'] == 'LOC']
            if len(loc_ner) >= 1:
                # try the ner from the pipeline
                location = ' '.join([ner['text'].lower().strip() for ner in loc_ner])
        if location == '' and re.search(r"(\bin\b)", input_text):
            # quick regex
            location = re.sub(r"(.* \bin\b )|(\bon\b.*)|alexa|echo|please|not|tomorrow|what's|the|weather|cool|how|about", "",
                             input_text)
        if location == "":
            location = get_location(input_text)
    except:
        if location == '' and re.search(r"(\bin\b)", input_text):
                # quick regex
                location = re.sub(r"(.* \bin\b )|(\bon\b.*)|alexa|echo|please|not|tomorrow|what's|the|weather|cool|how|about", "",
                                 input_text)
        else:
            location = ""

    if location == "":
        try:
            existing_location = msg['location'].lower().strip()
        except:
            existing_location = ""
        # print(existing_location)
        location = str(existing_location)

    weather, day = parse_input_text(input_text)
    logging.info("[Weather] Get user location: {}".format(location))

    # deal with backstory or miscellenious requests
    if backstory['confidence']>0.9 or 'topic_backstory' in intent['topic'] or dialog_act == "others":
        return {'response': backstory['text'],
                'user_attributes': {
                    'template_manager': getattr(user_attributes, 'template_manager', None),
                    'location': location,
                    'weatherchat': {
                        'ask_forecast': 'no',
                        'propose_continue': "STOP"}
                    }
                }
    elif dialog_act == "thanking" and dialog_act_score>0.8 or 'say_thanks' in intent['sys']:
        return {'response': "You're welcome! By the way, I have travel tips. would you like to know?",
                'user_attributes': {
                    'template_manager': getattr(user_attributes, 'template_manager', None),
                    'location': location,
                    'weatherchat': {
                        'ask_forecast': 'no',
                        'propose_continue': "STOP",
                        'propose_topic': 'TRAVELCHAT'}
                    }
                }
    elif dialog_act == "device_command" and dialog_act_score>0.8 or 'req_play_game' in intent['sys'] or 'req_play_music' in intent['sys'] or "req_task" in intent['sys']:
        return {'response': "Sorry, I can't do that in the social mode. <break time = \"300ms\"></break>So, What else would you like to talk about?",
                'user_attributes': {
                    'template_manager': getattr(user_attributes, 'template_manager', None),
                    'location': location,
                    'weatherchat': {
                        'ask_forecast': 'no',
                        'propose_continue': "STOP",
                        'propose_topic': 'TRAVELCHAT'}
                    }
                }
    elif dialog_act == "device_command" and dialog_act_score>0.8 or 'req_play_game' in intent['sys'] or 'req_play_music' in intent['sys'] or "req_task" in intent['sys']:
        return {'response': "Sorry, I can't do that in the social mode. <break time = \"300ms\"></break>So, What else would you like to talk about?",
                'user_attributes': {
                    'template_manager': getattr(user_attributes, 'template_manager', None),
                    'location': location,
                    'weatherchat': {
                        'ask_forecast': 'no',
                        'propose_continue': "STOP"}
                    }
                }
    elif dialog_act == "complaint" and dialog_act_score>0.8 or 'complaint' in intent['sys']:
        return {'response': "Ouu, I hope to be better at it next time! How about chatting something else?",
                'user_attributes': {
                    'template_manager': getattr(user_attributes, 'template_manager', None),
                    'location': location,
                    'weatherchat': {
                        'ask_forecast': 'no',
                        'propose_continue': "STOP"}
                    }
                }
    elif check_intersect(module_ls, ["MOVIECHAT", "BOOKCHAT", "GAMECHAT", "TECHSCIENCECHAT", "FOODCHAT", "MUSICCHAT", "NEWS"]):
        module = [module for module in module_ls if module in ["MOVIECHAT", "BOOKCHAT", "GAMECHAT", "TECHSCIENCECHAT", "FOODCHAT", "MUSICCHAT", "NEWS"]][0]
        transition_template = "Oh, just to confirm, do you want to talk about " + module.lower().replace("chat", "") + " instead of weather?"
        return {'response': transition_template,
                'user_attributes': {
                    'template_manager': getattr(user_attributes, 'template_manager', None),
                    'location': location,
                    'weatherchat': {
                        'ask_forecast': 'no',
                        'propose_continue': "UNCLEAR",
                        'propose_topic': module}
                    }
                }
    elif re.search(r"\bhot\b|\bcold\b|wind|storm|rain|sunny|\bwarm\b", input_text):
        return not_forecast_weather(input_text, user_attributes, location)


    if 'ans_pos' in intent['lexical'] and 'topic_weather' not in intent['topic'] and is_ask_forecast == "no":
        response = template_weather.utterance(selector='system/end', slots={}, user_attributes_ref=user_attributes)
        return {'response': response,
                'user_attributes': {
                    'template_manager': getattr(user_attributes, 'template_manager', None),
                    'location': location,
                    'weatherchat': {
                        'ask_forecast': 'no',
                        'propose_continue': "STOP"}
                    }
                }
    else:
        if (dialog_act == "back-channeling" or dialog_act == "appreciation") and dialog_act_score > 0.9:
            response = "Hope you enjoyed your day! What else would you like to talk about?"
            response_dict = {
                'response': response,
                'user_attributes': {
                    'template_manager': getattr(user_attributes, 'template_manager', None),
                    'location': location,
                    'weatherchat': {
                        'ask_forecast': 'no',
                        'propose_continue': 'STOP'}
                },
            }
            return response_dict
        else:
            response_list = get_weather_data(weather, location, day, input_text, user_attributes_ref=user_attributes)
            module_feedback = response_list[1]
            response_dict = {
                'response': response_list[0],
                'user_attributes': {
                    'template_manager': getattr(user_attributes, 'template_manager', None),
                    'location': location,
                    'weatherchat': {
                        'ask_forecast': 'no',
                        'propose_continue': module_feedback}
                },
            }
            return response_dict
