import logging
import requests
import json
import template_manager
import warnings

template_social = template_manager.Templates.social
from abtest_additional_config import config, BACKSTORY_URL_PARAPHRASE
from typing import Optional
from nlu.dataclass.backstory import Backstory


def get_backstory_response_text(utterance, a_b_test='A', confidence_threshold=0.7) -> Optional[str]:
    warnings.warn("Use migrated version", DeprecationWarning, stacklevel=2)
    backstory_url = __get_backstory_url(a_b_test)
    result = __load_backstory_response(utterance, backstory_url)

    if result and result['text'] != "":
        if result['confidence'] > confidence_threshold:
            return result['text']
    return None


def get_backstory_response(utterance, a_b_test='A') -> dict:
    backstory_url = __get_backstory_url(a_b_test)
    result = __load_backstory_response(utterance, backstory_url)
    if result:
        return result
    else:
        return {
            "question": "",
            "text": "",
            "reason": "",
            "confidence": 0.0
        }


def get_fallback_backstory_response(user_attributes):
    return template_social.utterance(selector='backstory/no_match', slots={},
                                     user_attributes_ref=user_attributes)


def __get_backstory_url(a_b_test):
    return BACKSTORY_URL_PARAPHRASE


def __load_backstory_response(utterance, backstory_url) -> Optional[dict]:
    if utterance is not None and utterance != '':
        try:
            logging.info('[BACKSTORY] load_backstory_response for utterance: {}'.format(utterance))
            headers = {
                'Content-Type': 'application/json',
            }
            data = {
                'text': utterance
            }

            resp = requests.post(
                backstory_url,
                headers=headers,
                data=json.dumps(data),
                timeout=(0.2, 0.2))
            ret = resp.json()
            logging.info("[BACKSTORY] get backstory response: {}".format(ret))

            if resp.status_code < 300:
                return ret

        except Exception as e:
            logging.error(
                '[BACKSTORY] Fail to get backstory response: {}'.format(e), exc_info=True)

    return None
