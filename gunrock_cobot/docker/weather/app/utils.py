import logging
import os

def get_working_environment():
    if os.environ.get('STAGE') == 'PROD' or os.environ.get('STAGE') == 'BETA':
        return os.environ.get('STAGE').lower()
    else:
        return "local"

def setup_logging_config():
    working_environment = get_working_environment()

    logging.basicConfig(
        format='[%(levelname)s] %(asctime)s [%(filename)s:%(lineno)d] %(message)s',
    )

    if working_environment == "local":
        logging.getLogger().setLevel(logging.DEBUG)
    else:
        logging.getLogger().setLevel(logging.INFO)
