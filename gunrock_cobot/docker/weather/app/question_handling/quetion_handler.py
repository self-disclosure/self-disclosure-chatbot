import logging
import time
from types import SimpleNamespace
from enum import Enum
import re

from nlu.dataclass.returnnlp import ReturnNLP, ReturnNLPSegment
from nlu.command_detector import CommandDetector
from nlu.constants import DialogAct as DialogActEnum
from typing import List, Dict, Any, Optional
from nlu.question_detector import QuestionDetector
from backstory_client import get_backstory_response
from user_profiler.user_profile import UserProfile

from dataclasses import dataclass, field
from nlu.constants import TopicModule
from nlu.dataclass.backstory import Backstory
from question_handling.factual_question_handler import FactualQuestionHandler
from template_manager.template import Template
from template_manager.manager import TemplateManager
from nlu.intentmap_scheme import TELL_ME, WANT_TO
from util_redis import RedisHelper
from nlg_post_process.profanity_classifier import BotResponseProfanityClassifier
from nlg.blender import BlenderResponseGenerator

@dataclass
class QuestionResponse:
    response: str = ""
    user_intent_module: Optional[TopicModule] = None
    bot_propose_module: Optional[TopicModule] = None
    tag: Optional[str] = ""
    backstory_reason: str = ""


class QuestionResponseTag(Enum):
    IDK = "ack_question_idk"
    BLENDER = "blender"

class QuestionHandler:

    def __init__(self, complete_user_utterance: str, returnnlp: Optional[List[dict]], backstory_threshold=0.85, system_ack=None, user_attributes_ref=None):
        self.user_attribute_ref = user_attributes_ref if user_attributes_ref else SimpleNamespace()

        self.session_id = getattr(self.user_attribute_ref, "session_id", "")
        self.user_profile = UserProfile(self.user_attribute_ref)
        self.a_b_test = getattr(self.user_attribute_ref, "a_b_test", "").lower()

        self.complete_user_utterance = complete_user_utterance
        self.returnnlp = ReturnNLP.from_list(returnnlp)

        self.question_detector = QuestionDetector(complete_user_utterance, returnnlp)
        self.question_segment_text = self.question_detector.get_question_segment_text()
        self.backstory_threshold = backstory_threshold
        self.system_ack = system_ack

        self.command_detector = CommandDetector(complete_user_utterance, returnnlp)

        self.blender_generator = BlenderResponseGenerator(user_attributes_ref)

        self.redis_client = RedisHelper()


    def handle_question(self) -> Optional[QuestionResponse]:
        if not self.question_detector.has_question():
            return QuestionResponse()

        preprocessed_input = self.remove_noisy_prefix(self.question_segment_text)

        if self.question_detector.is_potential_backstory_question():
            logging.info("[handle_question] try get response from backstory")
            response_dict = get_backstory_response(preprocessed_input)
            backstory_response = Backstory.from_dict(response_dict)

            if backstory_response.confidence > self.backstory_threshold and backstory_response.text:
                if not backstory_response.module:
                    user_intent_module = TopicModule.SOCIAL
                else:
                    user_intent_module = backstory_response.module

                if not backstory_response.bot_propose_module:
                    bot_propose_module = TopicModule.SOCIAL
                else:
                    bot_propose_module = backstory_response.bot_propose_module

                if backstory_response.reason:
                    backstory_response_reason = backstory_response.reason
                else:
                    backstory_response_reason = ""

                response_text = self.post_process_response_format(backstory_response.text)
                self._update_user_attributes("backstory", response_text)

                return QuestionResponse(response=response_text,
                                        user_intent_module=user_intent_module,
                                        bot_propose_module=bot_propose_module,
                                        backstory_reason=backstory_response_reason)

        if self.question_detector.is_factual_question():
            logging.info("[handle_question] try get response from evi")

            response = FactualQuestionHandler.handle_question(preprocessed_input)
            if response:
                response = self.post_process_response_format(response)
                self._update_user_attributes("evi", response)
                return QuestionResponse(response=response)   # todo: add module

        blender_response = self.blender_generator.generate_response()
        if blender_response:
            self._update_user_attributes("blender", blender_response)
            return QuestionResponse(response=blender_response, tag=QuestionResponseTag.BLENDER)

        if self.command_detector.is_request_tellmore() or self.returnnlp[-1].has_intent("ans_continue"):
            return QuestionResponse()

        return QuestionResponse(response=self.generate_i_dont_know_response(), tag=QuestionResponseTag.IDK.value)

    def get_response_from_blender(self) -> str:
        response = ""
        if not self.session_id:
            return ""

        key = "{}_{}".format(self.session_id, self.user_profile.total_turn_count)
        for i in range(8):
            response = self.redis_client.get(
                RedisHelper.PREFIX_BLENDER_QUESTION_HANDLING, key)

            if response and not self.should_filter_out_inappropriate_blender_response(response) and \
                    not BotResponseProfanityClassifier.is_profane(response):
                response += ". "
                break

            time.sleep(0.05)

        logging.info("[Blender] question handler response = {}, key = {}".format(response, key))

        return response

    @staticmethod
    def should_filter_out_inappropriate_blender_response(response) -> bool:
        response_filter = "|".join([
            r".*\b(my name is)\b.*",
            r".*\b(i'm a)\b\b",
            r".*\b(i'm not)\b",
            r".*\b(i don't know you).*\b",
            r".*\b(i would love to do that)\b",
            r".*\b(snuf film)",
            r".*\b(mad)\b",
            r".*i.*(have|had).*(dog|cat).*",
            r".*\b(i'm going to)\b.*",
            r".*\bi live in\b.*"
        ])

        return re.match(response_filter, response) is not None

    def generate_i_dont_know_response(self) -> str:
        logging.info("[handle_question] generate_i_dont_know_response")

        error_template_manager = TemplateManager(Template.error, self.user_attribute_ref)
        if self.command_detector.has_command():
            if self.command_detector.is_request_change_topic():
                logging.info("[handle_question] is_request_change_topic")
                response = ""  # ignore this one, let system handle this
            elif self.command_detector.has_command_in_question_form_req_topic():
                logging.info("[handle_question] has_command_in_question_form_req_topic")
                response = error_template_manager.speak('question_no_response/command_req_topic', {})
            else:
                logging.info("[handle_question] has_command_in_question_form_no_req_topic")
                response = error_template_manager.speak('question_no_response/command_no_req_topic', {})
            self._update_user_attributes("idk_template", response)
            return response

        if self._has_idk_response():
            response = self.post_process_response_format(self.system_ack.get("ack", ""))
            self._update_user_attributes("idk_server", response)
            return response

        if self.question_detector.is_factual_question():
            response = error_template_manager.speak('question_no_response/factual', {})
        elif self.question_detector.is_opinion_question():
            response = error_template_manager.speak('question_no_response/opinion', {})
        elif self.question_detector.is_yes_no_question():
            response = error_template_manager.speak('question_no_response/yes_no', {})
        else:
            response = error_template_manager.speak('question_no_response/general', {})

        response = self.post_process_response_format(response)
        self._update_user_attributes("idk_template", response)

        return response

    def _has_idk_response(self):
        return self.system_ack and self.system_ack.get("output_tag", "") == "ack_question_idk" and self.system_ack.get("ack", "")

    def post_process_response_format(self, response_text: str):
        if response_text:
            response_text = response_text.strip()
            last_char = response_text[-1]
            if last_char in [".", "?", "!"]:
                response_text += " "
        return response_text

    def _update_user_attributes(self, source, text):
        setattr(self.user_attribute_ref, "qa_response", {
            "source": source,
            "text": text
        })

    @staticmethod
    def remove_noisy_prefix(segment_text: str):
        regex_prefix = rf"({TELL_ME}|{WANT_TO} know)"
        if re.search(regex_prefix, segment_text):
            # split utterance to remove "i wanna know/tell me" and get only the question part
            processed_text = re.split(regex_prefix, segment_text)[-1]
            if processed_text:
                return processed_text.strip()
        return segment_text


