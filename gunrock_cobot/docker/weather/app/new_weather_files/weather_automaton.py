import re
import random
import requests
import json
import os
import sys

from weather_utils import *

class WeatherAutomaton:
    def __init__(self):
        self.__bind_states()
        self.__bind_transitions()

    # bind each state to corresponding callback function that generates response
    def __bind_states(self):
        self.states_mapping = {
            "s_init": self.s_init,
            "s_forecast": self.s_forecast,
            "s_currentweather": self.s_weather
        }

    def __bind_transitions(self):
        self.transitions_mapping = {
            "t_init": self.t_init,
            "t_forecast": self.t_forecast,
            "t_currentweather": self.t_currentweather
        }

    # get a response based on current state
    # by calling the corresponding callback functions
    def transduce(self, input_data):
        # extract info from input_data
        self.text = input_data["text"][0]
        self.ner = input_data["features"][0]["ner"]
        self.sentiment = input_data["features"][0]["sentiment"]
        self.cobot_intents = input_data["features"][0]["intent_classify"]["lexical"]
        self.user_attributes = input_data["weatherchat_user"]
        self.context_manager = input_data["weatherchat_context"]
        self.current_loop = self.user_attributes.get("current_loop", {})
        self.input_data = input_data

        # attributes to store in callback functions
        self.weatherchat_user = {
            "not_first_time": True,
            "propose_continue": "UNCLEAR",
            "propose_topic": ""
        }  # info stored here will be stored in user attr
        self.weatherchat_context = {
            "last_module": True
        }  # info stored here will be stored in cobot current state

        self.response = "Uh oh. I'm getting confused. Can you help me lead the conversation back on track?"  # store response to return here
        self.t_context = {}  # t states can temporarily store info here for s states to use

        # first test for system level intents; these override everything
        sys_intents = detect_sys_intent(self.text)
        if sys_intents:
            current_transition = "t_sys_intent"
        elif self.context_manager[1] == None and self.user_attributes.get(
                "not_first_time"):  # if a weatherchat was not selected consecutively
            current_transition = "t_resume"
        else:
            current_transition = input_data["weatherchat_user"]["current_transition"]

        next_state = self.transitions_mapping[current_transition]()  # t state returns s state
        next_transition = self.states_mapping[next_state]()  # s state returns t state
        self.weatherchat_user["current_transition"] = next_transition  # save transition for next user utterance
        self.weatherchat_user["current_loop"] = self.current_loop
        return {
            "response": construct_dynamic_response(self.response),
            "weatherchat_user": self.weatherchat_user,
            "weatherchat_context": self.weatherchat_context,
        }

    #=========================================================
    # Callback functions
    def t_sys_intent(self):
        # TODO: what to do if there is more than one sys intent?
        if len(self.sys_intents) > 1:
            pass
        if self.sys_intents[0] == "exit_weatherchat":
            return "s_ask_exit"
        return "s_ask_exit" # should not reach here

    def s_ask_exit(self):
        # TODO: template this
        self.response = "Ok. If you want to talk about something else like sports, say let's talk about sports."
        self.weatherchat_user["propose_continue"] = "UNCLEAR"
        self.weatherchat_user["propose_topic"] = ""
        return "t_resume"

    def s_echo(self):
        next_transition = self.t_context.get("next_transition", "t_init")
        return next_transition

    def s_forecast(self):
