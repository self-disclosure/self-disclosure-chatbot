import calendar
import hashlib
import importlib
import logging
import random
from datetime import datetime, date
from enum import Enum
from string import Formatter
from typing import Union, List, Dict, Tuple, Optional

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

use_single_file_template_data = True  # DO NOT CHANGE THIS. Some assumptions are already made.


class Templates(Enum):
    # Global / System templates
    shared, sys_rg, error, topics = 'shared_template', 'intent_general', 'error_template', 'intent_topics'
    dm = 'dm_template'

    # Module templates.
    # Add your template yml file to the 'gunrock_cobot/response_templates/' folder then run this script
    techscience, games = 'techscience_template', 'games_template'
    news, health, weather = 'news_template', 'health_template', 'weather_template'
    animal = 'animal_template'

    @property
    def data(self) -> dict:
        try:
            if use_single_file_template_data:
                return getattr(globals()['template_data'], 'data')[self.name]
            else:
                return getattr(globals()[self.value], 'data')
        except Exception as e:
            logger.critical(e)
            raise e

    def utterance(self, selector: Union[List[str], str], slots: Dict[str, str], user_attributes_ref) -> str:
        """
        Convenience method for calling get utterance.
        :param selector:
        :param slots:
        :param user_attributes_ref:
        :return:
        """
        return get_utterance(self, selector=selector, slots=slots, user_attributes_ref=user_attributes_ref)


def get_utterance(template_origin: Templates,
                  selector: Union[List[str], str],
                  slots: Dict[str, str],
                  user_attributes_ref) -> Union[str, List[str]]:
    """
    The main function of template manager. Use this function to get an utterance from shared_template.
    ### IMPORTANT: ###
    Please also look at response_templates/shared_template.yml to see what which template you want
    and what slots you need to provide.

    :param template_origin:     select which template you want to choose from

    :param selector:            a string or List[str] that specifies which template you want to use.
                                e.g. to select from opinion -> agree: use "opinion/agree"

    :param slots:               a dict used to fill in the slots in the templates.
                                Formatting is done using str Formatter()
                                e.g. for topic_choice: {'topic_a': 'movies', 'topic_b': 'sports'}
                                ### IMPORTANT: ###
                                Include all slots that the template group needs.
                                Match your dict keys with the template name.
    :param user_attributes_ref: prev_hashes from user attribute dictionary from ServiceModule that
                                store the hashes

    :return:                    A completely formatted utterance that you can return to cobot.
    """
    if isinstance(selector, list):
        selector = '/'.join(selector)

    if user_attributes_ref is not None:
        # if isinstance(user_attributes_ref.prev_hash, set):
        #     user_attributes_ref.prev_hash = None

        if user_attributes_ref.prev_hash is None:
            user_attributes_ref.prev_hash = {}  # type: Dict[str, List[str]]

        if _hash_selector(template_origin, selector) not in user_attributes_ref.prev_hash:
            user_attributes_ref.prev_hash[_hash_selector(template_origin, selector)] = []

    try:
        # Temp fix for isinstance(template_origin, str) due to past API spec
        if isinstance(template_origin, str):
            template_origin = Templates[template_origin]

        utterances = _get_utterance_list(template_origin=template_origin, selector=selector.split('/'))

        utterance, hash_id = _select_utterance(
            utterances,
            prev_hashes=user_attributes_ref.prev_hash if user_attributes_ref is not None else None,
            template_origin=template_origin,
            selector=selector
        )
        utterance = _format_fragments(template_origin,
                                      utterance,
                                      user_attributes_ref.prev_hash if user_attributes_ref is not None else None,
                                      selector)

        utterance = utterance.format(**slots)
        if user_attributes_ref is not None and user_attributes_ref.prev_hash is not None:
            user_attributes_ref.prev_hash[_hash_selector(template_origin, selector)].append(hash_id)
        return utterance
    except (KeyError, Exception) as e:
        logger.critical(e)
        # TODO: Graceful error handling here plz
        return "Oh no, I think my creators misspelled something in my code. Would you mind asking me something else?"


def is_in_utterances(template_origin: Templates,
                     selector: Union[List[str], str],
                     unformatted_utterance: str) -> bool:
    if isinstance(selector, list):
        selector = '/'.join(selector)

    try:
        utterances = _get_utterance_list(template_origin=template_origin, selector=selector.split('/'))
        return any(utt == unformatted_utterance for utt, _ in utterances)
    except Exception as e:
        logger.critical(e)
        return False


def compose_sentence(sentence: Union[str, List[str]]) -> str:
    if isinstance(sentence, list):
        sentence = random.choice(sentence)

    final_sentence = ""
    sentence_buffer = ""
    flag = False
    for char in sentence:
        if char == "(":
            flag = True
        elif char == ")":
            flag = False
            choices = sentence_buffer.split("|")
            final_sentence += random.choice(choices)
            sentence_buffer = ""
        if flag == True and char != "(" and char != ")":
            sentence_buffer += char
        elif flag == False and char != "(" and char != ")":
            final_sentence += char
    # final_sentence = " ".join(final_sentence.split())
    return final_sentence


def _hash_selector(template_origin: Templates, selector: str) -> str:
    return template_origin.value + '/' + selector


def _get_utterance_list(template_origin: Templates,
                        selector: List[str]) -> List[Tuple[str, str]]:
    try:
        data = template_origin.data[selector[0]]
        for i in selector[1:]:
            data = data[i]

        data = [(d['text'], d['hash']) for d in data]

        return data
    except KeyError as e:
        raise KeyError("Selector is invalid: {}".format(e))
        # return [("Whoops, my developer typed something wrong in my code. Would you mind saying something else?", '')]


def _select_utterance(utterances: List[Tuple[str, str]],
                      prev_hashes: Dict[str, List[str]],
                      template_origin: Templates,
                      selector: str) -> Tuple[str, str]:
    if prev_hashes is not None:
        if len(utterances) == 1:
            prev_hashes[_hash_selector(template_origin, selector)] = []
            return utterances[0]
        filtered = [(utt, hash_id) for utt, hash_id in utterances
                    if hash_id not in prev_hashes[_hash_selector(template_origin, selector)]]
        if len(filtered) == 0:
            prev_hashes[_hash_selector(template_origin, selector)] = \
                [prev_hashes[_hash_selector(template_origin, selector)][-1]]
            filtered = [(utt, hash_id) for utt, hash_id in utterances
                        if hash_id not in prev_hashes[_hash_selector(template_origin, selector)]]

        return random.choice(filtered)
    else:
        return random.choice(utterances)


def _format_fragments(template_origin: Templates,
                      sentence: str,
                      prev_hashes: Dict[str, List[str]],
                      selector: str) -> str:

    try:
        slots: List[str] = [name for _, name, _, _, in Formatter().parse(sentence) if name is not None]
    except ValueError as e:
        logger.critical(e)
        logger.critical(sentence)
        raise e
    if len(slots) <= 0:
        return sentence
    sub = {}
    for name in slots:
        if not name.startswith(('t_', 's_')):
            sub[name] = '{{{}}}'.format(name)
            continue
        if name.startswith(('t_fragments', 's_fragments')):
            key = name.split('/')

        elif name == 't_time_of_day':
            current_hour = datetime.now().hour
            sub[name] = 'morning' if current_hour < 12 else 'afternoon' if current_hour < 18 else 'evening'
            continue
        elif name == 't_weekday':
            today = datetime.today()
            sub[name] = calendar.day_name[today.weekday()]
            continue
        elif name == 't_talk_about_module_or':
            topics = ['movies', 'the news', 'science', 'technology', 'books', 'sports', 'games']
            random.shuffle(topics)
            sub[name] = ' or '.join(topics[:2])
            continue

        elif name.startswith('t_'):
            key = name.lstrip('t_').split('/')
        elif name.startswith('s_'):
            key = name.lstrip('s_').split('/')
        else:
            sub[name] = ''
            continue

        fragments = _get_utterance_list(Templates.shared if name.startswith('t_') else template_origin, selector=key)
        f_utt, f_hash_id = _select_utterance(fragments, prev_hashes, template_origin, selector)
        # call recursive to replace the fragments in the fragments
        sub[name] = _format_fragments(template_origin, f_utt, prev_hashes, selector)
        # user_attributes.prev_hash.add(f_hash_id)

    try:
        sentence = sentence.format(**sub)
        # return compose_sentence(sentence)
        return sentence
    except KeyError as e:
        raise KeyError("Missing slot: '{}' for selector: '{}'".format(e, selector))


def copy_templates(only: List[str] = None):
    from pathlib import Path
    from glob import glob
    import os
    import shutil
    if Path(__file__).parent.name == 'gunrock_cobot':
        directories = glob('docker/*/app/')
        print(directories)
        copied_path = []
        for directory in directories:
            if only is not None and not any([directory.find(x) != -1 for x in only]):
                continue
            shutil.copy2(__file__, directory)
            templates = glob('response_templates/*.py')
            # print(templates)
            for template in templates:
                template_dir = directory + 'response_templates/'
                if not os.path.exists(template_dir):
                    os.makedirs(template_dir)
                shutil.copy2(template, template_dir)
            copied_path.append(directory)

        print("copied to: {}".format(copied_path))
    elif Path(__file__).parent.name == 'app':
        directory = '../../template_manager.py'
        print(directory)


def __hash_utterance(text: str, template: Templates, path: List[str]) -> str:
    return hashlib.blake2b((template.value + '/' + '/'.join(path) + '/' + text).encode(), digest_size=8).hexdigest()


def import_files():
    """
    Dynamic import for all modules using Template enums
    """
    if use_single_file_template_data:
        try:
            globals()['template_data'] = importlib.import_module('response_templates.template_data')
        except ModuleNotFoundError as e:
            logger.critical(e)
            raise e
    else:
        for template in Templates:
            try:
                globals()[template.value] = importlib.import_module('response_templates.' + template.value)
                break
            except ModuleNotFoundError as e:
                logger.critical(e)
                continue


# MARK: - Related to loading custom csv files

def get_holiday(h_date: date) -> Optional[List[dict]]:
    try:
        holidays = getattr(globals()['template_data'], 'data')['holidays']
        return holidays.get(h_date)
    except Exception as e:
        logger.critical(e)
        raise e


def _import_holiday():
    with open('response_templates/cleaned_holiday.csv', 'r', encoding='utf-8') as f:
        import csv
        import unicodedata
        reader = csv.DictReader(f)

        from collections import defaultdict
        holidays = defaultdict(list)

        for row in reader:
            holidays[datetime.strptime(row['Date'], '%Y-%m-%d').date()].append({
                'date': datetime.strptime(row['Date'], '%Y-%m-%d').date(),
                'category': set(row['Category'].lower().split(', ')),
                'holiday': unicodedata.normalize('NFKC', row['Holiday']).encode('ascii', errors='ignore').decode(),
                'content': unicodedata.normalize('NFKC', row['content']).encode('ascii', errors='ignore').decode(),
                'special_month_holiday': bool(row['special_monthholiday'])
            })

        return dict(holidays)

        # return {datetime.strptime(row['Date'], '%Y-%m-%d').date(): {
        #     'date': datetime.strptime(row['Date'], '%Y-%m-%d').date(),
        #     'category': set(row['Category'].lower().split(', ')),
        #     'holiday': unicodedata.normalize('NFKC', row['Holiday']).encode('ascii', errors='ignore').decode(),
        #     'content': unicodedata.normalize('NFKC', row['content']).encode('ascii', errors='ignore').decode(),
        #     'special_month_holiday': bool(row['special_monthholiday'])
        # } for row in reader}


def __generate_py_file(template_origin: Union[Templates, List[Templates]], additional_dict: dict = None):
    generated_check = []
    if isinstance(template_origin, Templates):
        template_origin = [template_origin]

    write_data = {}
    for t in template_origin:
        try:
            with open('response_templates/{}.yml'.format(t.value), 'r') as f:
                import yaml
                data = yaml.load(f)

                def _generate_utterance_entry(text: str, _template: Templates, path: List[str]):
                    return {
                        'text': text,
                        'hash': __hash_utterance(text, t, path)
                    }

                def nested(d: dict, u: dict, path: List[str]):
                    for k, v in u.items():
                        if isinstance(v, dict):
                            path.append(k)
                            d[k] = nested(d.get(k, {}), v, path)
                            path = []
                        elif isinstance(v, list):
                            d[k] = [_generate_utterance_entry(vi, template_origin, path) for vi in v]
                    return d

                if data is None:
                    return

                data = nested(data, data, [])

                write_data[t.name] = data

                if use_single_file_template_data:
                    generated_check.append(t.name)
                else:
                    with open('response_templates/{}.py'.format(t.value), 'w') as w:
                        w.write("# <--- dict object for '{}' created by template_manager.py ---> #\n"
                                .format(t.value))
                        w.write("# <--- Do not directly edit this file. Edit the yml file and run the "
                                "template_manager.py script instead ---> #\n\n")

                        w.write("data = \\\n")
                        from pprint import pformat
                        w.write(pformat(data, width=120))
                        w.write('\n')

                        generated_check.append(t.name)
        except FileNotFoundError as e:
            import os
            print(os.getcwd())
            raise e

    if additional_dict:
        write_data.update(additional_dict)

    if use_single_file_template_data:
        with open('response_templates/template_data.py', 'w') as w:
            w.write("# <--- dict object for '{}' created by template_manager.py ---> #\n"
                    .format([t.value for t in template_origin]))
            w.write("# <--- Do not directly edit this file. Edit the yml file and run the "
                    "template_manager.py script instead ---> #\n\n")
            w.write('import datetime\n\n\n')

            w.write("data = \\\n")
            from pprint import pformat
            w.write(pformat(write_data, width=120))
            w.write('\n')

    print("Template generated for: {}".format(generated_check))


if __name__ == '__main__':

    __generate_py_file([template for template in Templates],
                       additional_dict={'holidays': _import_holiday()})

    copy_templates(['weather'])

    pass

import_files()
