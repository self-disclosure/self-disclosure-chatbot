import signal
import re
import random
import yweather
from yahooweather import YahooWeather, UNIT_F
from geotext import GeoText
import datetime
import template_manager
from cobot_common.service_client import get_client
from constants_api_keys import COBOT_API_KEY

REDDIT_CLIENT_ID = "lKmLeMmSSLns6A"
REDDIT_CLIENT_SECRET = "Fi6V_pCzKqE-4TRVqcmpMJdET2s"

template_weather = template_manager.Templates.weather

class Timeout:
    """Timeout class using ALARM signal."""
    class Timeout(Exception):
        pass

    def __init__(self, sec):
        self.sec = sec

    def __enter__(self):
        signal.signal(signal.SIGALRM, self.raise_timeout)
        signal.alarm(self.sec)

    def __exit__(self, *args):
        signal.alarm(0)    # disable alarm

    def raise_timeout(self, *args):
        raise Timeout.Timeout()


def generate_woeid(location):
    client = yweather.Client()
    return client.fetch_woeid(location)


def get_location(text, ner=None, strength="strong"):
    '''
    Using ner to find location in utterance. Return none if location is not found
    '''
    loc_ner = list()
    location = ""
    if ner:
        for entity in ner:
            if type(entity) == dict:
                type_of_work = entity.get("label", None) # new string must be integers error. why?
                if type_of_work == 'GPE':
                    loc_ner.append(entity['text'])
        if len(loc_ner) >= 1:
                location = ' '.join([ner['text'].lower().strip() for ner in loc_ner])
        elif re.search(r"(\bin\b)|(^\w+$)|(^\w+ \w+$)", text):
            # quick regex
            location = re.sub(r"(.* \bin\b )|(\bon\b.*)|alexa|echo|please|tomorrow|what's|the|weather|cool", "",
                              text)
    if location:
        return location
    else:
        try:
            existing_location = msg['location'].lower().strip()
        except:
            existing_location = ""
        if existing_location:
            return existing_location
        else:
            return ""


def get_current_time(location):
    # TODO: location awareness
    return datetime.datetime.now() - datetime.timedelta(hours=7)


def generate_day(utternace):
    if "monday" in utternace or "Monday" in utternace:
        return "Monday"
    elif "tuesday" in utternace or "Tuesday" in utternace:
        return "Tuesday"
    elif "wednesday" in utternace or "Wednesday" in utternace:
        return "Wednesday"
    elif "thursday" in utternace or "Thursday" in utternace:
        return "Thursday"
    elif "friday" in utternace or "Friday" in utternace:
        return "Friday"
    elif "saturday" in utternace or "Saturday" in utternace:
        return "Saturday"
    elif "Sunday" in utternace or "Sunday" in utternace:
        return "Sunday"
    elif "today" in utternace or "Today" in utternace:
        return get_current_time(None).strftime("%A")
        # return datetime.datetime.now().strftime("%A")
    elif "tomorrow" in utternace or "Tomorrow" in utternace:
        tom = get_current_time(None).date() + datetime.timedelta(days=1)
        # tom = datetime.date.today() + datetime.timedelta(days=1)
        return tom.strftime("%A")
    else:
        return get_current_time(None).strftime("%A")
        # return datetime.datetime.now().strftime("%A")


def create_full_date(date):
    months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October",
              "November", "December"]
    day, short_month, year = date.split(' ')
    for month in months:
        if short_month == month[0:3]:
            short_month = month
            break
    return day + " " + short_month + " " + str(year)


def parse_now(data_now, location, user_attributes_ref):
    temp = data_now["temp"]
    text = data_now["text"]
    return template_weather.utterance(selector='temperature/today', slots={
        'location': location, 'temp_curr': data_now["temp"], 'weather_text': data_now["text"],
    }, user_attributes_ref=user_attributes_ref)


def parse_forecast(data_forecast, day, location, user_attributes_ref):
    week = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
    if day == "Monday" or day == "monday":
        short_day = week[0]
    elif day == "Tuesday" or day == "tuesday":
        short_day = week[1]
    elif day == "Wednesday" or day == "wednesday":
        short_day = week[2]
    elif day == "Thursday" or day == "thursday":
        short_day = week[3]
    elif day == "Friday" or day == "friday":
        short_day = week[4]
    elif day == "Saturday" or day == "saturday":
        short_day = week[5]
    else:
        short_day = week[6]

    for each in data_forecast:
        if each["day"] == short_day:
            full_date = create_full_date(each["date"])
            return template_weather.utterance(selector='temperature/forecast', slots={
                'day': day, 'location': location, 'weather_text': each["text"].lower(),
                'temp_high': each["high"], 'temp_low': each["low"]
            }, user_attributes_ref=user_attributes_ref)


def parse_wind(data_wind, user_attributes_ref):
    # TODO: Change to mph
    return template_weather.utterance(selector='wind', slots={
        'speed': data_wind["speed"], 'chill': data_wind["chill"]
    }, user_attributes_ref=user_attributes_ref)


def parse_atmosphere(data_atmosphere, user_attributes_ref):
    atmosphere_data = ""
    humidity = data_atmosphere["humidity"]
    return template_weather.utterance(selector='atmosphere', slots={'humidity': data_atmosphere["humidity"]},
                                      user_attributes_ref=user_attributes_ref)


def parse_astronomy(data_astronomy, user_attributes_ref):
    astronomy_data = ""
    return template_weather.utterance(selector='astronomy', slots={'sunrise': data_astronomy['sunrise'],
                                                                   'sunset': data_astronomy['sunset']},
                                      user_attributes_ref=user_attributes_ref)


def get_weather_info(woeid, weather, location, day, user_attributes_ref):
    yweather = YahooWeather(woeid, UNIT_F)
    if yweather.updateWeather():
        rawdata = yweather.RawData
        units = yweather.Units
        now = parse_now(yweather.Now, location, user_attributes_ref=user_attributes_ref)
        forecast = parse_forecast(yweather.Forecast, day, location, user_attributes_ref=user_attributes_ref)
        wind = parse_wind(yweather.Wind, user_attributes_ref=user_attributes_ref)
        atmosphere = parse_atmosphere(yweather.Atmosphere, user_attributes_ref=user_attributes_ref)
        astronomy = parse_astronomy(yweather.Astronomy, user_attributes_ref=user_attributes_ref)
        if day == "today" or day == "Today":
            return now
        else:
            return forecast
    else:
        return template_weather.utterance(selector='error/no_connection', slots={},
                                          user_attributes_ref=user_attributes_ref)


def get_weather_data(weather, location, day, user_attributes_ref):
    if location != '':
        woeid = generate_woeid(location)
        return [get_weather_info(woeid, weather, location, day, user_attributes_ref=user_attributes_ref), "UNCLEAR"]
    else:
        return [template_weather.utterance(selector='error/no_location', slots={},
                                           user_attributes_ref=user_attributes_ref), "CONTINUE"]


def parse_input_text(utterance):
    # if usr_location is None or usr_location == '':
    #     location = get_location(str(utterance))
    # else:
    #     location = usr_location
    if 'weather' or 'climate' in utterance:
        weather = 'weather'
    elif 'forecast' in utterance:
        weather = 'forecast'
    elif 'temperature' in utterance:
        weather = 'temperature'
    elif 'climate' in utterance:
        weather = 'climate'
    else:
        weather = 'weather'
    day = generate_day(utterance)
    return weather, day


def detect_sys_intent(text):
    sys_intents = []
    ''' TODO: figure out how to decide whether user is asking about something else or
              continuing about the current topic
    if re.search(r"(give.* (movie|me).* (recommendation|suggestion)|(suggest|recommend).* me.* movie|you.* (suggest|recommend)|(^what|tell me).* (good|popular).* movie)", text): 
        sys_intents.append("ask_recommend")
    if re.search(r"(talk|chat|do you know).* about|have you read", text): 
        sys_intents.append("req_topic")  
    '''
    if re.search(r"weather|climate|temperature|hot|rain|rainy|wind|windy|cold", text):
        sys_intents.append("ask_weather")
    if re.search(r"(stop|don't|do not).* (talk|discuss|tell.* me).* (movie|show|tv)|\bi\b.* (hate|don't|do not|never).* watch.* (movie|show|tv)|(talk|discuss|tell.* me).* about.* something else", text):
        sys_intents.append("exit_weatherchat")
    if len(sys_intents) == 0:
        return None
    return sys_intents


def construct_dynamic_response(response):
    all_choices = ""
    flag = False
    if response is not None:
        for char in response:
            if char == '(':
                flag = True
            elif char == ')':
                flag = False
            if flag and char != '(':
                all_choices += char
            elif not flag and char == ')':
                selected_response = random.choice(all_choices.split('|'))
                replaceable = '(' + all_choices + char
                response = response.replace(replaceable, selected_response)
                all_choices = ""
    return response
