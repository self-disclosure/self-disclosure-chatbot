import re
import template_manager
from weather_automaton import *
from weather_utils import *

template_weather = template_manager.Templates.weather

required_context = ['text']


def get_required_context():
    return required_context


def handle_message(msg):
    msg["text"][0] = msg["text"][0].lower().strip()
    if msg.get("weatherchat_user") is None:  # check if None
        msg["weatherchat_user"] = {}
    current_state = msg["weatherchat_user"].get("current_transition")
    if current_state == None:
        msg["weatherchat_user"]["current_transition"] = "t_init"

    RG = WeatherAutomaton()
    results = RG.transduce(msg)

    response_dict = {}
    response_dict["response"] = results["response"]
    response_dict["user_attributes"] = {"weatherchat_user": msg.get("weatherchat_user", {})}
    for k, v in results["weatherchat_user"].items():
        response_dict["user_attributes"]["weatherchat_user"][k] = v
    response_dict["context_manager"] = {"weatherchat_context": {}}
    for k, v in results["weatherchat_context"].items():
        response_dict["context_manager"]["weatherchat_context"][k] = v
    '''
    except Exception as e:
        # To keep cobot running, catch everything!
        return {
            "response": "Oh no, I think my creators misspelled something in my code. Would you mind asking something else?"
        }
    '''
    return response_dict








    if location == "":
        try:
            existing_location = msg['location'].lower().strip()
        except:
            existing_location = ""
        # print(existing_location)
        location = str(existing_location)

    weather, day = parse_input_text(input_text)
    logger.info("[Weather] Get user location: {}".format(location))
    if 'ans_pos' in intent['lexical'] and 'topic_weather' not in intent['topic'] and is_ask_forecast == "no":
        response = template_weather.utterance(selector='system/end', slots={}, user_attributes_ref=user_attributes)
        return {'response': response,
                'user_attributes': {
                    'prev_hash': user_attributes.prev_hash,
                    'location': location,
                    'ask_forecast': 'no'
                    },
                'propose_continue': "STOP",
                'propose_topic': ""
                }
    else:
        response_list = get_weather_data(weather, location, day, user_attributes_ref=user_attributes)

    # logger.critical(location)
    response_dict = {
        'response': response_list[0],
        'user_attributes': {
            'prev_hash': user_attributes.prev_hash,
            'location': location,
            'ask_forecast': 'no'
        },
        'propose_continue': response_list[1],
        'propose_topic': ""
    }
    return response_dict
