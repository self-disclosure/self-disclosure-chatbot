"""
Rule-Based Quetion Generation Appoach Based on Fang Hao's Thesis:
https://hao-fang.github.io/res/phd_thesis_socialbot.pdf
Author: Mingyang Zhou
"""
import requests
import requests.auth
import json
from nltk import tokenize
from nltk.tokenize import sent_tokenize, word_tokenize
import csv
import os
import re

from utils import question_selection
from news_kg_api import news_of_type

allenNLP_predictor_url = 'http://ec2-54-152-74-228.compute-1.amazonaws.com:6001/allenlp-parser'
allenNLP_nerpredictor_url = 'http://ec2-54-152-74-228.compute-1.amazonaws.com:6001/allenlp-NERparser'
pattern3_tenses_url = 'http://ec2-54-152-74-228.compute-1.amazonaws.com:6001/pattern3-tenses'
pattern3_lemma_url = 'http://ec2-54-152-74-228.compute-1.amazonaws.com:6001/pattern3-lemma'

headers = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}

TWOSTEP_DEPENDENCY = ['dobj', 'neg', 'ccomp',
                      'tmod', 'xcomp', 'advcl']  # TODO: nmod (nmod in Allen NLP is replaced with a secondary dependency: [prep, pobj])

THREESTEP_DEPENDENCY = {
    'xcomp': ['ccomp', 'dobj'], 'nsubjpass': ['num'], 'prep': ['pobj']}

QUESTION_TYPE_DICT = {'dobj': ["what", "who"],
                      'neg': ["whether"], 'ccomp': ['what'], 'tmod': ['when'], 'xcomp': ['what', 'how'],
                      'xcomp_ccomp': ['what'], 'xcomp_dobj': ['what', 'who'], 'nsubjpass_num': ['how many'],
                      'advcl': ['what', 'why', 'when', 'how'], 'prep_pobj': ['what', 'why', 'when', 'where', 'how']}


def check_span(root):
    """
    Using Depth First Search to Find the Span
    """
    if not root.get("children", None):
        span_start = int(root["spans"][0]['start'])
        span_end = int(root["spans"][0]['end'])
    else:
        span_start = int(root["spans"][0]['start'])
        span_end = int(root["spans"][0]['end'])
        for child in root["children"]:
            new_span_start, new_span_end = check_span(child)
            if new_span_start < span_start:
                span_start = new_span_start
            if new_span_end > span_end:
                span_end = new_span_end
    return span_start, span_end


def nsubjpass_num_qg(question_type, parsing_result, dependency_object, intermediate_key, sample_sentence=None):
    root = parsing_result['root']

    # Step 1 Determine the span of the dependency_text
    # Using the Depth First Search to Find the Whole Span
    dependency_object_span = check_span(
        dependency_object)

    # Step 2 Rank the Sequence for the Question
    origin_text = parsing_result["text"]
    # print(origin_text[int(dependency_object_span[0]):int(dependency_object_span[1])])
    # print(dependency_object_span)
    question_constituents = [question_type]
    span_list = []
    span_root = (root["spans"][0]['start'], root["spans"][0]['end'])
    span_list.append((root, span_root))
    for child in root["children"]:
        if child["nodeType"] != intermediate_key:
            if child["nodeType"] != "dep":
                child_span = check_span(child)
                span_list.append((child, child_span))
        else:
            child_span = (child["spans"][0]['start'], child["spans"][0]['end'])
            span_list.append((child, child_span))
            for grandchild in child.get('children', []):
                grandchild_span = check_span(grandchild)
                if grandchild_span != dependency_object_span:
                    # print(grandchild["word"])
                    span_list.append((grandchild, grandchild_span))

    span_list.sort(key=lambda x: x[1])
    aux_existing = False
    for i, x in enumerate(span_list):
        if x[0]["nodeType"] == "cc":
            if span_list[i - 1][0]["nodeType"] == "punct":
                question_constituents[-1] = "?"
            else:
                question_constituents.append("?")
            break
        elif i == len(span_list) - 1:
            if x[0]["nodeType"] == "punct":
                question_constituents.append("?")
            else:
                question_constituents.append(origin_text[x[1][0]:x[1][1]])
                question_constituents.append("?")
        else:
            question_constituents.append(origin_text[x[1][0]:x[1][1]])

    generated_question = ' '.join(question_constituents)

    # print(generated_question)

    return generated_question


def xcomp_dobj_qg(question_type, parsing_result, dependency_object, intermediate_key, sample_sentence=None):
    root = parsing_result['root']

    # Step 1 Determine the span of the dependency_text
    # Using the Depth First Search to Find the Whole Span
    dependency_object_span = check_span(
        dependency_object)

    # Step 2 Rank the Sequence for the Question
    origin_text = parsing_result["text"]
    # print(origin_text[int(dependency_object_span[0]):int(dependency_object_span[1])])
    # print(dependency_object_span)
    question_constituents = [question_type]
    span_list = []
    span_root = (root["spans"][0]['start'], root["spans"][0]['end'])
    span_list.append((root, span_root))
    aux_existing = False
    for child in root["children"]:
        if child["nodeType"] != intermediate_key:
            child_span = check_span(child)
            span_list.append((child, child_span))
        else:
            child_span = (child["spans"][0]['start'], child["spans"][0]['end'])
            span_list.append((child, child_span))
            for grandchild in child.get('children', []):
                grandchild_span = check_span(grandchild)
                if grandchild_span != dependency_object_span:
                    # print(grandchild["word"])
                    span_list.append((grandchild, grandchild_span))
        if child["nodeType"] == "aux":
            aux_existing = False

    span_list.sort(key=lambda x: x[1])
    for i, x in enumerate(span_list):
        if x[0]["nodeType"] == "aux":
            question_constituents.insert(
                1, origin_text[x[1][0]:x[1][1]])
        elif x[0]["nodeType"] == "auxpass" and not aux_existing:
            question_constituents.insert(
                1, origin_text[x[1][0]:x[1][1]])
            aux_existing = True
        elif x[0]["nodeType"] == "prep" and x[1][1] < root["spans"][0]["start"]:
            continue
        elif x[0]["nodeType"] == "root" and not aux_existing:
                # Check which assistant verb should be added
            root_word = x[0]["word"]
            # root_word_tense = tenses(root_word)[0]
            pattern_tenses_response = requests.post(
                pattern3_tenses_url, json={"word": root_word}, headers=headers)
            root_word_tense = pattern_tenses_response.json()

            if root_word_tense is not None:
                if root_word_tense[0] == 'past':
                    assistant_word = 'did'
                elif root_word_tense[2] == 'singular':
                    assistant_word = 'does'
                else:
                    assistant_word = 'do'
            else:
                assistant_word = 'do'
            # convert word to original form
            # root_word_lemma = lemma(root_word)
            pattern_lemma_response = requests.post(
                pattern3_lemma_url, json={"word": root_word}, headers=headers)
            root_word_lemma = pattern_lemma_response.json()

            question_constituents.insert(1, assistant_word)
            question_constituents.append(root_word_lemma)
        elif x[0]["nodeType"] == "cc" or x[0]["nodeType"] == "conj":
            if span_list[i - 1][0]["nodeType"] == "punct":
                question_constituents[-1] = "?"
            else:
                question_constituents.append("?")
            break
        elif i == len(span_list) - 1:
            if x[0]["nodeType"] == "punct":
                question_constituents.append("?")
            else:
                question_constituents.append(origin_text[x[1][0]:x[1][1]])
                question_constituents.append("?")
        # set up the special rule for tailaux
        # elif x[0]["nodeType"] == "tailaux":
        #     question_constituents.append(x[0]['word'])
        else:
            question_constituents.append(origin_text[x[1][0]:x[1][1]])

    generated_question = ' '.join(question_constituents)

    # print(generated_question)

    return generated_question


def xcomp_ccomp_qg(question_type, parsing_result, dependency_object, intermediate_key, sample_sentence=None):
    root = parsing_result['root']

    # Step 1 Determine the span of the dependency_text
    # Using the Depth First Search to Find the Whole Span
    dependency_object_span = check_span(
        dependency_object)

    # Step 2 Rank the Sequence for the Question
    origin_text = parsing_result["text"]
    # print(origin_text[int(dependency_object_span[0]):int(dependency_object_span[1])])
    # print(dependency_object_span)
    question_constituents = [question_type]
    aux_existing = False
    span_list = []
    span_root = (root["spans"][0]['start'], root["spans"][0]['end'])
    span_list.append((root, span_root))
    aux_existing = True
    for child in root["children"]:
        if child["nodeType"] != intermediate_key:
            child_span = check_span(child)
            span_list.append((child, child_span))
        else:
            child_span = (child["spans"][0]['start'], child["spans"][0]['end'])
            span_list.append((child, child_span))
            for grandchild in child.get('children', []):
                grandchild_span = check_span(grandchild)
                if grandchild_span != dependency_object_span:
                    # print(grandchild["word"])
                    span_list.append((grandchild, grandchild_span))
        if child["nodeType"] == "aux":
            aux_existing = True

    span_list.sort(key=lambda x: x[1])
    for i, x in enumerate(span_list):
        if x[0]["nodeType"] == "aux":
            question_constituents.insert(
                1, origin_text[x[1][0]:x[1][1]])
        elif x[0]["nodeType"] == "auxpass" and not aux_existing:
            question_constituents.insert(
                1, origin_text[x[1][0]:x[1][1]])
            aux_existing = True
        elif x[0]["nodeType"] == "prep" and x[1][1] < root["spans"][0]["start"]:
            continue
        elif x[0]["nodeType"] == "root" and not aux_existing:
                # Check which assistant verb should be added
            root_word = x[0]["word"]
            # root_word_tense = tenses(root_word)[0]
            pattern_tenses_response = requests.post(
                pattern3_tenses_url, json={"word": root_word}, headers=headers)
            root_word_tense = pattern_tenses_response.json()

            if root_word_tense is not None:
                if root_word_tense[0] == 'past':
                    assistant_word = 'did'
                elif root_word_tense[2] == 'singular':
                    assistant_word = 'does'
                else:
                    assistant_word = 'do'
            else:
                assistant_word = 'do'
            # convert word to original form
            # root_word_lemma = lemma(root_word)
            pattern_lemma_response = requests.post(
                pattern3_lemma_url, json={"word": root_word}, headers=headers)
            root_word_lemma = pattern_lemma_response.json()

            question_constituents.insert(1, assistant_word)
            question_constituents.append(root_word_lemma)
        elif x[0]["nodeType"] == "cc" or x[0]["nodeType"] == "conj":
            if span_list[i - 1][0]["nodeType"] == "punct":
                question_constituents[-1] = "?"
            else:
                question_constituents.append("?")
            break
        elif i == len(span_list) - 1:
            if x[0]["nodeType"] == "punct":
                question_constituents.append("?")
            else:
                question_constituents.append(origin_text[x[1][0]:x[1][1]])
                question_constituents.append("?")
        # set up the special rule for tailaux
        # elif x[0]["nodeType"] == "tailaux":
        #     question_constituents.append(x[0]['word'])
        else:
            question_constituents.append(origin_text[x[1][0]:x[1][1]])

    generated_question = ' '.join(question_constituents)

    # print(generated_question)

    return generated_question


def prep_pobj_qg(question_type, parsing_result, dependency_object, sample_sentence=None):
    # Identify the span of each major parts, assuming that the predicate is
    # always verb

    root = parsing_result['root']

    # Step 1 Determine the span of the dependency_text
    # Using the Depth First Search to Find the Whole Span
    dependency_object_span = check_span(
        dependency_object)

    # Step 2 Rank the Sequence for the Question
    origin_text = parsing_result["text"]
    question_constituents = [question_type]

    span_list = []
    span_root = (root["spans"][0]['start'], root["spans"][0]['end'])
    span_list.append((root, span_root))
    punct_link2answer = [dependency_object_span[0], dependency_object_span[1]]
    aux_existing = False
    for child in root["children"]:
        child_span = check_span(child)
        if child_span != dependency_object_span:
            if child["nodeType"] == "punct" and (child_span[0] == punct_link2answer[1] or child_span[1] == punct_link2answer[0]):
                if child_span[1] == punct_link2answer[0]:
                    punct_link2answer[0] = child_span[0]
                else:
                    punct_link2answer[1] = child_span[1]
            else:
                span_list.append((child, child_span))
        if child["nodeType"] == "aux":
            aux_existing = True

    # Sort span_list
    span_list.sort(key=lambda x: x[1])
    for i, x in enumerate(span_list):
        if x[0]["nodeType"] == "aux":
            question_constituents.insert(
                1, origin_text[x[1][0]:x[1][1]])
        elif x[0]["nodeType"] == "auxpass" and not aux_existing:
            question_constituents.insert(
                1, origin_text[x[1][0]:x[1][1]])
            aux_existing = True
        elif x[0]["nodeType"] == "prep" and (x[1][1] - x[1][0] > 50 or x[1][1] < root["spans"][0]["start"]):
            continue
        elif x[0]["nodeType"] == "root" and not aux_existing:
                # Check which assistant verb should be added
            root_word = x[0]["word"]
            # if tenses(root_word):
            #     root_word_tense = tenses(root_word)[0]
            # else:
            #     root_word_tense = None
            pattern_tenses_response = requests.post(
                pattern3_tenses_url, json={"word": root_word}, headers=headers)
            root_word_tense = pattern_tenses_response.json()

            if root_word_tense is not None:
                if root_word_tense[0] == 'past':
                    assistant_word = 'did'
                elif root_word_tense[2] == 'singular':
                    assistant_word = 'does'
                else:
                    assistant_word = 'do'
            else:
                assistant_word = 'do'
            # convert word to original form
            #root_word_lemma = lemma(root_word)
            pattern_lemma_response = requests.post(
                pattern3_lemma_url, json={"word": root_word}, headers=headers)
            root_word_lemma = pattern_lemma_response.json()

            question_constituents.insert(1, assistant_word)
            question_constituents.append(root_word_lemma)
        elif x[0]["nodeType"] == "cc" or x[0]["nodeType"] == "conj":
            if span_list[i - 1][0]["nodeType"] == "punct":
                question_constituents[-1] = "?"
            else:
                question_constituents.append("?")
            break
        elif i == len(span_list) - 1:
            if x[0]["nodeType"] == "punct":
                question_constituents.append("?")
            else:
                question_constituents.append(origin_text[x[1][0]:x[1][1]])
                question_constituents.append("?")
        else:
            question_constituents.append(origin_text[x[1][0]:x[1][1]])

    generated_question = ' '.join(question_constituents)

    # print(generated_question)

    return generated_question


def advcl_qg(question_type, parsing_result, dependency_object, sample_sentence=None):
    # Identify the span of each major parts, assuming that the predicate is
    # always verb

    root = parsing_result['root']

    # Step 1 Determine the span of the dependency_text
    # Using the Depth First Search to Find the Whole Span
    dependency_object_span = check_span(
        dependency_object)

    # Step 2 Rank the Sequence for the Question
    origin_text = parsing_result["text"]
    question_constituents = [question_type]
    # aux_existing = False
    # for child in root["children"]:
    #     child_span = check_span(child)
    #     if child_span != dependency_object_span:
    #         if child["nodeType"] == "aux":
    #             question_constituents.insert(1,
    #                                          origin_text[child_span[0]:child_span[1]])
    #         elif child["nodeType"]
    span_list = []
    span_root = (root["spans"][0]['start'], root["spans"][0]['end'])
    span_list.append((root, span_root))
    punct_link2answer = [dependency_object_span[0], dependency_object_span[1]]
    aux_existing = False
    for child in root["children"]:
        child_span = check_span(child)
        if child_span != dependency_object_span:
            if child["nodeType"] == "punct" and (child_span[0] == punct_link2answer[1] or child_span[1] == punct_link2answer[0]):
                if child_span[1] == punct_link2answer[0]:
                    punct_link2answer[0] = child_span[0]
                else:
                    punct_link2answer[1] = child_span[1]
            else:
                span_list.append((child, child_span))
        if child["nodeType"] == "aux":
            aux_existing = True

    # Sort span_list
    span_list.sort(key=lambda x: x[1])
    for i, x in enumerate(span_list):
        if x[0]["nodeType"] == "aux":
            question_constituents.insert(
                1, origin_text[x[1][0]:x[1][1]])
        elif x[0]["nodeType"] == "auxpass" and not aux_existing:
            question_constituents.insert(
                1, origin_text[x[1][0]:x[1][1]])
            aux_existing = True
        elif x[0]["nodeType"] == "root" and not aux_existing:
                # Check which assistant verb should be added
            root_word = x[0]["word"]
            try:
                pattern_tenses_response = requests.post(
                    pattern3_tenses_url, json={"word": root_word}, headers=headers)
                root_word_tense = pattern_tenses_response.json()
                if root_word_tense is not None:
                    if root_word_tense[0] == 'past':
                        assistant_word = 'did'
                    elif root_word_tense[2] == 'singular':
                        assistant_word = 'does'
                    else:
                        assistant_word = 'do'
                else:
                    assistant_word = 'do'
            except:
                assistant_word = ""
            # convert word to original form
            #root_word_lemma = lemma(root_word)
            pattern_lemma_response = requests.post(
                pattern3_lemma_url, json={"word": root_word}, headers=headers)
            root_word_lemma = pattern_lemma_response.json()

            question_constituents.insert(1, assistant_word)
            question_constituents.append(root_word_lemma)
        elif x[0]["nodeType"] == "cc":
            if span_list[i - 1][0]["nodeType"] == "punct":
                question_constituents[-1] = "?"
            else:
                question_constituents.append("?")
            break
        elif i == len(span_list) - 1:
            if x[0]["nodeType"] == "punct":
                question_constituents.append("?")
            else:
                question_constituents.append(origin_text[x[1][0]:x[1][1]])
                question_constituents.append("?")
        else:
            question_constituents.append(origin_text[x[1][0]:x[1][1]])

    generated_question = ' '.join(question_constituents)

    # print(generated_question)

    return generated_question


def xcomp_qg(question_type, parsing_result, dependency_object, sample_sentence=None):
    root = parsing_result['root']

    # Step 1 Determine the span of the dependency_text
    # Using the Depth First Search to Find the Whole Span
    dependency_object_span = check_span(
        dependency_object)

    # Step 2 Rank the Sequence for the Question
    origin_text = parsing_result["text"]
    question_constituents = [question_type]
    aux_existing = False
    span_list = []
    span_root = (root["spans"][0]['start'], root["spans"][0]['end'])
    span_list.append((root, span_root))
    punct_link2answer = [dependency_object_span[0], dependency_object_span[1]]
    aux_existing = False
    for child in root["children"]:
        child_span = check_span(child)
        if child_span != dependency_object_span:
            if child["nodeType"] == "punct" and (child_span[0] == punct_link2answer[1] or child_span[1] == punct_link2answer[0]):
                if child_span[1] == punct_link2answer[0]:
                    punct_link2answer[0] = child_span[0]
                else:
                    punct_link2answer[1] = child_span[1]
            else:
                span_list.append((child, child_span))
        else:
            if 'VB' in child['attributes']:
                for x in child['children']:
                    if x['nodeType'] == "aux" and x['word'] != "to":
                        # Modify this word with assistant verb
                        x['word'] = ' '.join([x['word'], 'do'])
                        # Modify the nodeType such that it won't be misused by
                        # the other rule
                        x['nodeType'] = "tailaux"
                        # print(x['nodeType'])
                        # print(x['word'])
                        span_list.append(
                            (x, (x["spans"][0]['start'], x["spans"][0]['end'])))
            else:
                for x in child['children']:
                    if x['nodeType'] == "nsubj":
                        node_span = check_span(x)
                        span_list.append(
                            (x, (node_span[0], node_span[1])))

        if child["nodeType"] == "aux":
            aux_existing = True

    span_list.sort(key=lambda x: x[1])
    for i, x in enumerate(span_list):
        if x[0]["nodeType"] == "aux":
            question_constituents.insert(
                1, origin_text[x[1][0]:x[1][1]])
        elif x[0]["nodeType"] == "auxpass" and not aux_existing:
            question_constituents.insert(
                1, origin_text[x[1][0]:x[1][1]])
            aux_existing = True
        elif x[0]["nodeType"] == "prep" and x[1][1] < root["spans"][0]["start"]:
            continue
        elif x[0]["nodeType"] == "root" and not aux_existing:
                # Check which assistant verb should be added
            root_word = x[0]["word"]
            pattern_tenses_response = requests.post(
                pattern3_tenses_url, json={"word": root_word}, headers=headers)
            root_word_tense = pattern_tenses_response.json()

            if root_word_tense is not None:
                if root_word_tense[0] == 'past':
                    assistant_word = 'did'
                elif root_word_tense[2] == 'singular':
                    assistant_word = 'does'
                else:
                    assistant_word = 'do'
            else:
                assistant_word = 'do'
            # convert word to original form
            #root_word_lemma = lemma(root_word)
            pattern_lemma_response = requests.post(
                pattern3_lemma_url, json={"word": root_word}, headers=headers)
            root_word_lemma = pattern_lemma_response.json()

            question_constituents.insert(1, assistant_word)
            question_constituents.append(root_word_lemma)
        elif x[0]["nodeType"] == "cc" or x[0]["nodeType"] == "conj":
            if span_list[i - 1][0]["nodeType"] == "punct":
                question_constituents[-1] = "?"
            else:
                question_constituents.append("?")
            break
        # set up the special rule for tailaux
        elif x[0]["nodeType"] == "tailaux":
            question_constituents.append(x[0]['word'])
        elif i == len(span_list) - 1:
            if x[0]["nodeType"] == "punct":
                question_constituents.append("?")
            else:
                question_constituents.append(origin_text[x[1][0]:x[1][1]])
                question_constituents.append("?")
        else:
            question_constituents.append(origin_text[x[1][0]:x[1][1]])

    generated_question = ' '.join(question_constituents)

    # print(generated_question)

    return generated_question


def tmod_qg(question_type, parsing_result, dependency_object, sample_sentence=None):

    root = parsing_result['root']

    # Step 1 Determine the span of the dependency_text
    # Using the Depth First Search to Find the Whole Span
    dependency_object_span = check_span(
        dependency_object)

    # Step 2 Rank the Sequence for the Question
    origin_text = parsing_result["text"]
    question_constituents = [question_type]
    aux_existing = False
    span_list = []
    span_root = (root["spans"][0]['start'], root["spans"][0]['end'])
    span_list.append((root, span_root))
    previous_child = None
    aux_existing = False
    for child in root["children"]:
        child_span = check_span(child)
        if child_span != dependency_object_span:
            if not (child["nodeType"] == "punct" and previous_child["nodeType"] == "tmod"):
                span_list.append((child, child_span))
        previous_child = child
        if child["nodeType"] == "aux":
            aux_existing = True

    # Sort span_list
    span_list.sort(key=lambda x: x[1])
    for i, x in enumerate(span_list):
        if x[0]["nodeType"] == "aux":
            question_constituents.insert(
                1, origin_text[x[1][0]:x[1][1]])
        elif x[0]["nodeType"] == "auxpass" and not aux_existing:
            question_constituents.insert(
                1, origin_text[x[1][0]:x[1][1]])
            aux_existing = True
        elif x[0]["nodeType"] == "root" and not aux_existing:
                # Check which assistant verb should be added
            root_word = x[0]["word"]
            pattern_tenses_response = requests.post(
                pattern3_tenses_url, json={"word": root_word}, headers=headers)
            root_word_tense = pattern_tenses_response.json()

            if root_word_tense is not None:
                if root_word_tense[0] == 'past':
                    assistant_word = 'did'
                elif root_word_tense[2] == 'singular':
                    assistant_word = 'does'
                else:
                    assistant_word = 'do'
            else:
                assistant_word = 'do'
            # convert word to original form
            #root_word_lemma = lemma(root_word)
            pattern_lemma_response = requests.post(
                pattern3_lemma_url, json={"word": root_word}, headers=headers)
            root_word_lemma = pattern_lemma_response.json()

            question_constituents.insert(1, assistant_word)
            question_constituents.append(root_word_lemma)
        elif x[0]["nodeType"] == "cc":
            if span_list[i - 1][0]["nodeType"] == "punct":
                question_constituents[-1] = "?"
            else:
                question_constituents.append("?")
            break
        elif i == len(span_list) - 1:
            if x[0]["nodeType"] == "punct":
                question_constituents.append("?")
            else:
                question_constituents.append(origin_text[x[1][0]:x[1][1]])
                question_constituents.append("?")
        else:
            question_constituents.append(origin_text[x[1][0]:x[1][1]])

    generated_question = ' '.join(question_constituents)

    # print(generated_question)

    return generated_question


def ccomp_qg(question_type, parsing_result, dependency_object, sample_sentence=None):

    root = parsing_result['root']

    # Step 1 Determine the span of the dependency_text
    # Using the Depth First Search to Find the Whole Span
    dependency_object_span = check_span(
        dependency_object)  # The answer child

    # Step 2 Rank the Sequence for the Question
    origin_text = parsing_result["text"]
    question_constituents = [question_type]
    aux_existing = False
    span_list = []
    span_root = (root["spans"][0]['start'], root["spans"][0]['end'])
    span_list.append((root, span_root))

    punct_link2answer = [dependency_object_span[0], dependency_object_span[1]]
    aux_existing = False
    for child in root["children"]:
        child_span = check_span(child)
        if child_span != dependency_object_span:
            if child["nodeType"] == "punct" and (child_span[0] == punct_link2answer[1] or child_span[1] == punct_link2answer[0]):
                if child_span[1] == punct_link2answer[0]:
                    punct_link2answer[0] = child_span[0]
                else:
                    punct_link2answer[1] = child_span[1]
            else:
                span_list.append((child, child_span))
        if child["nodeType"] == "aux":
            aux_existing = True

    # Sort span_list
    span_list.sort(key=lambda x: x[1])
    for i, x in enumerate(span_list):
        # print(span_list[i-1])
        # print(question_constituents)
        if x[0]["nodeType"] == "aux":
            question_constituents.insert(
                1, origin_text[x[1][0]:x[1][1]])
        elif x[0]["nodeType"] == "auxpass" and not aux_existing:
            question_constituents.insert(
                1, origin_text[x[1][0]:x[1][1]])
            aux_existing = True
        elif x[0]["nodeType"] == "prep" and x[1][1] < root["spans"][0]["start"]:
            continue
        elif x[0]["nodeType"] == "root" and not aux_existing:
                # Check which assistant verb should be added
            root_word = x[0]["word"]
            pattern_tenses_response = requests.post(
                pattern3_tenses_url, json={"word": root_word}, headers=headers)
            root_word_tense = pattern_tenses_response.json()

            if root_word_tense is not None:
                if root_word_tense[0] == 'past':
                    assistant_word = 'did'
                elif root_word_tense[2] == 'singular':
                    assistant_word = 'does'
                else:
                    assistant_word = 'do'
            else:
                assistant_word = 'do'
            # convert word to original form
            #root_word_lemma = lemma(root_word)
            pattern_lemma_response = requests.post(
                pattern3_lemma_url, json={"word": root_word}, headers=headers)
            root_word_lemma = pattern_lemma_response.json()

            question_constituents.insert(1, assistant_word)
            question_constituents.append(root_word_lemma)
        elif x[0]["nodeType"] == "nsubj" and span_list[i - 1][0]["nodeType"] == "root" and i > 0:
            subject_text = origin_text[x[1][0]:x[1][1]]
            question_constituents.insert(
                len(question_constituents) - 1, subject_text)
        elif x[0]["nodeType"] == "cc" or x[0]["nodeType"] == "conj":
            if span_list[i - 1][0]["nodeType"] == "punct":
                question_constituents[-1] = "?"
            else:
                question_constituents.append("?")
            break
        elif i == len(span_list) - 1:
            if x[0]["nodeType"] == "punct":
                question_constituents.append("?")
            else:
                question_constituents.append(origin_text[x[1][0]:x[1][1]])
                question_constituents.append("?")
        else:
            question_constituents.append(origin_text[x[1][0]:x[1][1]])
    # print(question_constituents)
    generated_question = ' '.join(question_constituents)

    # print(generated_question)

    return generated_question


def neg_qg(question_type, parsing_result, dependency_object, sample_sentence=None):

    root = parsing_result['root']
    assert question_type == "whether", "Error: Other question type is detected"

    # Check the span of the dependency_object_span
    dependency_object_span = check_span(dependency_object)

    # Step 2 Rank the Sequence for the Question
    origin_text = parsing_result["text"]
    question_constituents = []
    aux_existing = False

    span_list = []
    span_root = (root["spans"][0]['start'], root["spans"][0]['end'])
    span_list.append((root, span_root))
    aux_existing = False
    punct_link2answer = [dependency_object_span[0], dependency_object_span[1]]
    for child in root["children"]:
        child_span = check_span(child)
        if child_span != dependency_object_span:
            if child["nodeType"] == "punct" and (child_span[0] == punct_link2answer[1] or child_span[1] == punct_link2answer[0]):
                if child_span[1] == punct_link2answer[0]:
                    punct_link2answer[0] = child_span[0]
                else:
                    punct_link2answer[1] = child_span[1]
            else:
                span_list.append((child, child_span))
        if child["nodeType"] == "aux" or child["nodeType"] == "cop":
            aux_existing = True
    # Assert that Aux Must exist among the children of the root
    assert aux_existing, "In a negation dependency, we must have an AUX in the parsing"

    # Sort the span list
    span_list.sort(key=lambda x: x[1])

    for i, x in enumerate(span_list):
        if x[0]["nodeType"] == "aux":
            question_constituents.insert(
                1, origin_text[x[1][0]:x[1][1]])
        elif x[0]["nodeType"] == "auxpass" and not aux_existing:
            question_constituents.insert(
                1, origin_text[x[1][0]:x[1][1]])
            aux_existing = True
        elif x[0]["nodeType"] == "cc":
            if span_list[i - 1][0]["nodeType"] == "punct":
                question_constituents[-1] = "?"
            else:
                question_constituents.append("?")
            break
        elif i == len(span_list) - 1:
            if x[0]["nodeType"] == "punct":
                question_constituents.append("?")
            else:
                question_constituents.append(origin_text[x[1][0]:x[1][1]])
                question_constituents.append("?")
        else:
            question_constituents.append(origin_text[x[1][0]:x[1][1]])

    generated_question = ' '.join(question_constituents)

    # print(generated_question)

    return generated_question


def dobj_qg(question_type, parsing_result, dependency_object, sample_sentence=None):
    # Identify the span of each major parts, assuming that the predicate is
    # always verb

    root = parsing_result['root']

    # Step 1 Determine the span of the dependency_text
    # Using the Depth First Search to Find the Whole Span
    dependency_object_span = check_span(
        dependency_object)

    # Step 2 Rank the Sequence for the Question
    origin_text = parsing_result["text"]
    question_constituents = [question_type]
    aux_existing = False
    # for child in root["children"]:
    #     child_span = check_span(child)
    #     if child_span != dependency_object_span:
    #         if child["nodeType"] == "aux":
    #             question_constituents.insert(1,
    #                                          origin_text[child_span[0]:child_span[1]])
    #         elif child["nodeType"]
    span_list = []
    span_root = (root["spans"][0]['start'], root["spans"][0]['end'])
    span_list.append((root, span_root))
    punct_link2answer = [dependency_object_span[0], dependency_object_span[1]]
    aux_existing = False
    for child in root["children"]:
        child_span = check_span(child)
        if child_span != dependency_object_span:
            # The logic to get rid of the punctuation adjacent to answer span
            if child["nodeType"] == "punct" and (child_span[0] == punct_link2answer[1] or child_span[1] == punct_link2answer[0]):
                if child_span[1] == punct_link2answer[0]:
                    punct_link2answer[0] = child_span[0]
                else:
                    punct_link2answer[1] = child_span[1]
            else:
                span_list.append((child, child_span))
        if child["nodeType"] == "aux":
            aux_existing = True
    # Sort span_list
    span_list.sort(key=lambda x: x[1])
    for i, x in enumerate(span_list):
        if x[0]["nodeType"] == "aux":
            question_constituents.insert(
                1, origin_text[x[1][0]:x[1][1]])
        elif x[0]["nodeType"] == "auxpass" and not aux_existing:
            question_constituents.insert(
                1, origin_text[x[1][0]:x[1][1]])
            aux_existing = True
        elif x[0]["nodeType"] == "prep" and x[1][1] < root["spans"][0]["start"]:
            continue
        elif x[0]["nodeType"] == "root" and not aux_existing:
                # Check which assistant verb should be added
            root_word = x[0]["word"]
            pattern_tenses_response = requests.post(
                pattern3_tenses_url, json={"word": root_word}, headers=headers)
            root_word_tense = pattern_tenses_response.json()

            if root_word_tense is not None:
                if root_word_tense[0] == 'past':
                    assistant_word = 'did'
                elif root_word_tense[2] == 'singular':
                    assistant_word = 'does'
                else:
                    assistant_word = 'do'
            else:
                assistant_word = 'do'
            # convert word to original form
            #root_word_lemma = lemma(root_word)
            pattern_lemma_response = requests.post(
                pattern3_lemma_url, json={"word": root_word}, headers=headers)
            root_word_lemma = pattern_lemma_response.json()

            question_constituents.insert(1, assistant_word)
            question_constituents.append(root_word_lemma)
        elif x[0]["nodeType"] == "cc" or x[0]["nodeType"] == "conj" or (x[0]["nodeType"] == "tmod" and x[1][1] - x[1][0] > 10):

            if span_list[i - 1][0]["nodeType"] == "punct":
                question_constituents[-1] = "?"
            else:
                question_constituents.append("?")
            break
        elif i == len(span_list) - 1:
            if x[0]["nodeType"] == "punct":
                question_constituents.append("?")
            else:
                question_constituents.append(origin_text[x[1][0]:x[1][1]])
                question_constituents.append("?")
        else:
            question_constituents.append(origin_text[x[1][0]:x[1][1]])

    generated_question = ' '.join(question_constituents)

    # print(generated_question)

    return generated_question


def dobj_question_type(dependency_word, sample_sentence):
    # Run NER on the sample_sentence
    NER_response = requests.post(
        allenNLP_nerpredictor_url, json={"sentence": sample_sentence}, headers=headers)
    NER_result = NER_response.json()

    # Find the corresponding tag of dependency_word
    dependency_word_index = NER_result["words"].index(dependency_word)
    dependency_word_tag = NER_result["tags"][dependency_word_index]

    if dependency_word_tag == 'U-PER':
        return "who"
    else:
        return "what"


def xcomp_question_type(dependency, sample_sentence):
    # Check the Attributes of Dependency_word, if it is a verb it should be a what question otherwise it is a
    # how question.

    # Don't ask a question if dependency["attributes"] = "VBG"
    # Handle the bad question generation for "Trump is profoundly compromised,
    # acting just as you would imagine a person with a disordered personality
    # would."
    # print("dependency attributes is: {}".format(dependency["attributes"]))
    if dependency["attributes"][0] == "VBG":
        return None

    if 'VB' in dependency["attributes"][0]:
        return "what"
    else:
        return "how"


def xcomp_dobj_question_type(dependency_word, sample_sentence):
    # Run NER on the sample_sentence
    #NER_result = NER_predictor.predict(sentence=sample_sentence)
    NER_response = requests.post(
        allenNLP_nerpredictor_url, json={"sentence": sample_sentence}, headers=headers)
    NER_result = NER_response.json()

    # Find the corresponding tag of dependency_word
    dependency_word_index = NER_result["words"].index(dependency_word)
    dependency_word_tag = NER_result["tags"][dependency_word_index]

    if dependency_word_tag == 'U-PER':
        return "who"
    else:
        return "what"


def prep_pobj_question_type(dependency, sample_sentence):
    """
    prep_word need to be expanded for the full coverage of or case words
    """
    prep_word = dependency["word"]
    # print(prep_word)
    if prep_word in ['in', 'at', 'from', 'into', 'under', 'near', 'below', 'amid']:
        return "where"
    elif prep_word in ['to', 'for', 'with', 'as', 'on', 'about', 'against']:
        # if prep_word != "of" or re.search("because of", sample_sentence) is not None:
        #     return "what"
        # else:
        #     return "why"
        return "what"
    elif prep_word in ['according', 'by', "out"]:
        if prep_word == "out" and re.search("out of", sample_sentence):
            return "out of what"
        elif prep_word == "according" and re.search("according to", sample_sentence):
            return "according to what"
        else:
            return "{} what".format(prep_word)
    elif prep_word in ['during', 'after', 'before', 'upon']:
        return "when"
    # This is not correct
    elif prep_word == "of" and re.search("because of", sample_sentence):
        return "why"
    else:
        return "how"


def advcl_question_type(dependency, sample_sentence):
    # check if mark exists
    for child in dependency.get("children", []):
        if child["nodeType"] == "mark":
            advcl_mark = child['word']
            # print(advcl_mark)
            if advcl_mark in ['to', 'because', 'for', 'so']:  # in order to, so that
                return 'why'
            elif advcl_mark == "in" and re.search(r"in order to", sample_sentence):
                # lif re.search(r"in order to", "I in order to"):
                return 'why'
            elif advcl_mark in ['after', 'before', 'when']:
                return 'when'
            elif advcl_mark in 'by':
                return 'how'
            elif advcl_mark in ['from', 'with', 'about']:
                return 'what'
            else:
                return None
    return 'how'


def determine_question_type(dependency_key, dependency, dependency_word, sample_sentence, parsing_result):
    try:
        # print(dependency_key)
        # if QUESTION_TYPE_DICT.get(dependency, None):
        # print(dependency_key)
        if len(QUESTION_TYPE_DICT[dependency_key]) > 1:
            if dependency_key == "dobj":
                return dobj_question_type(dependency_word, sample_sentence)
            if dependency_key == "xcomp":
                return xcomp_question_type(dependency, sample_sentence)
            if dependency_key == "advcl":
                return advcl_question_type(dependency, sample_sentence)
            if dependency_key == "xcomp_dobj":
                return xcomp_dobj_question_type(dependency_word, sample_sentence)
            if dependency_key == "prep_pobj":
                # print(prep_pobj_question_type(dependency, sample_sentence))
                return prep_pobj_question_type(dependency, sample_sentence)
        else:
            return QUESTION_TYPE_DICT[dependency_key][0]
    # else:
    #     return None
    except:
        print("No corresponding question type is found for {}".format(
            dependency["nodeType"]))
        return None


def special_filtering(parsing_tree):
    """
    The code to hard code some special cases to process the sentence
    """
    # Rule 1 get rid of ccomp if dboj is in front of it and dobj is faily long
    previous_child = {}
    copy_parsing_tree = parsing_tree["root"]["children"].copy()
    # for x  in parsing_tree["root"]["children"]:
    #     print(x["nodeType"])

    for i, child in enumerate(copy_parsing_tree):
        # print(child["nodeType"])
        if child["nodeType"] == "ccomp" and previous_child.get("nodeType", None) == "dobj":
            # Error Sentence: Facebook scans the contents of messages that
            # people send each other on its Messenger app, blocking any content
            # that contravenes its rules, it has emerged.
            previous_span = check_span(previous_child)
            if previous_span[1] - previous_span[0] > 10:
                # delete this child
                parsing_tree["root"]["children"].remove(child)

        if child["nodeType"] == "advmod" and child["word"].lower() in ["only", "so"]:
            """
            Error Sentence:
            Some fact checkers only found out that they should be vetting paid adverts after Facebook chief executive Mark Zuckerberg described the new policyin Congress on Wednesday.
            """
            parsing_tree["root"]["children"].remove(child)

        if child["nodeType"] == "aux" and previous_child.get("nodeType", None) == "aux":
            """
            Error Sentence:
            A smart intercom company called Teman GateGuard has been pitching its surveillance technology to landlords in New York as a way to sidestep rent-control regulations in the city, according to emails reviewed by CNET.
            """
            child["nodeType"] = "auxpass"

        if child["nodeType"] == "prep" and previous_child.get("word", None) == ",":
            """
            Error Sentence:
            A smart intercom company called Teman GateGuard has been pitching its surveillance technology to landlords in New York as a way to sidestep rent-control regulations in the city, according to emails reviewed by CNET.
            """
            parsing_tree["root"]["children"].remove(child)
            parsing_tree["root"]["children"].remove(previous_child)

        if child["nodeType"] == "ccomp" and previous_child.get("word", None) == ",":
            parsing_tree["root"]["children"].remove(child)
            parsing_tree["root"]["children"].remove(previous_child)

        if child["nodeType"] == "dobj" and previous_child.get("nodeType", None) == "dobj":
            parsing_tree["root"]["children"][i - 1]["children"].append(child)
            parsing_tree["root"]["children"].remove(child)

        if i == 0 and child["word"] == '"' and parsing_tree["text"].split(" ").count('"') == 1:
            parsing_tree["root"]["children"].remove(child)

        if child["word"] == "," and previous_child["nodeType"] == "partmod":
            parsing_tree["root"]["children"].remove(child)
            parsing_tree["root"]["children"].remove(previous_child)
        # if child["word"] == ",":
        #     print(previous_child["nodeType"])
        if child["word"] == "," and previous_child["nodeType"] == "prep":
            """
            Error Sentence:
            So on your new Mate 30 Series, you'll be getting Huawei's App Gallery as your App Store.
            """
            parsing_tree["root"]["children"].remove(child)
            parsing_tree["root"]["children"].remove(previous_child)
            # if parsing_tree["root"]["children"][-1].get("word") == ".":
            #     parsing_tree["root"]["children"].insert(len(parsing_tree["root"]["children"])-1, previous_child)

        # if  child.get('nodeType', None) == previous_child.get('nodeType', None) and child.get('nodeType', None) == "aux":
        #     """
        #     Error Sentence:
        #     So on your new Mate 30 Series, you'll be getting Huawei's App Gallery as your App Store.
        #     """
        #     #Preventing priotize this aux
        previous_child = child

    # for x  in parsing_tree["root"]["children"]:
    #     print(x["nodeType"])
    # return parsing_tree


def question_generation(sample_sentence):
    """
    Return the sequence of results
    """
    # parsing_result = predictor.predict(sentence=sample_sentence)
    # dependency_tree = parsing_result['hierplane_tree']
    allennlp_api_response = requests.post(
        allenNLP_predictor_url, json={"sentence": sample_sentence}, headers=headers)
    dependency_tree = allennlp_api_response.json()

    # print(dependency_tree)
    # Convert the sample_sentence to the one provided in the result
    sample_sentence = dependency_tree["text"]
    # Identify the root
    root = dependency_tree["root"]
    #print("The root in the sentence is: {}".format(root['word']))

    generated_questions = []

    # filter out some bad parts to ask question
    special_filtering(dependency_tree)

    # Check if root is a verb
    if "VB" not in root["attributes"][0]:
        #print("root is not a verb")
        return generated_questions

    # Identify its direct relation and check if there is a Dependency that we
    for child in root["children"]:
        # Generated Questions for Direct Dependency on Predicate
        if child["nodeType"] in TWOSTEP_DEPENDENCY:
            question_data = {}  # Initilaize the Final Structure to be saved
            # Determine the Question Type
            question_type = determine_question_type(
                child["nodeType"], child, child['word'], sample_sentence, dependency_tree)
            if question_type:
                # print("The question can be generated on this dependency: {}".format(
                #     child["nodeType"]))
                # print("The question type is: {}".format(question_type))
                # Store Question Type
                question_data["q_type"] = question_type
                # Specify answer type as well
                # Next Step generate question
                generated_question = QUESTION_GENERATION_FUNC[
                    child["nodeType"]](question_type, dependency_tree, child)
                answer_span = check_span(child)
                # print(answer_span)
                question_data["question"] = generated_question
                question_data["dependency_type"] = child["nodeType"]
                question_data["answer_span"] = sample_sentence[
                    answer_span[0]:answer_span[1]]
                # generated_questions.append(generated_question)
                generated_questions.append(question_data)

        # Generate Questions for Secondary Dependency on Predicate
        if child["nodeType"] in THREESTEP_DEPENDENCY.keys():
            # extract the question for secondary dependency relations
            intermediate_key = child["nodeType"]
            secondary_key = THREESTEP_DEPENDENCY[intermediate_key]
            for grandchild in child.get("children", []):
                if grandchild["nodeType"] in secondary_key:
                    question_data = {}  # Initilaize the Final Structure to be saved
                    # Detect the Secondary Dependency
                    secondary_dependency = '_'.join(
                        [intermediate_key, grandchild["nodeType"]])
                    # print(secondary_dependency)
                    # Determine the Question Type
                    # Adjust a special secondary_dependency type
                    if secondary_dependency == "prep_pcomp":
                        secondary_dependency = "prep_pobj"

                    # print(secondary_dependency)

                    if secondary_dependency != "prep_pobj":
                        question_type = determine_question_type(
                            secondary_dependency, grandchild, grandchild['word'], sample_sentence, dependency_tree)
                    else:
                        question_type = determine_question_type(secondary_dependency, child, child[
                                                                'word'], sample_sentence, dependency_tree)

                    if question_type:
                        question_data["q_type"] = question_type
                        if secondary_dependency != "prep_pobj":
                            generated_question = QUESTION_GENERATION_FUNC[secondary_dependency](
                                question_type, dependency_tree, grandchild, intermediate_key)
                            answer_span = check_span(grandchild)
                        else:
                            generated_question = QUESTION_GENERATION_FUNC[secondary_dependency](
                                question_type, dependency_tree, child, intermediate_key)
                            answer_span = check_span(child)

                        question_data["question"] = generated_question
                        question_data["dependency_type"] = secondary_dependency
                        question_data["answer_span"] = sample_sentence[
                            answer_span[0]:answer_span[1]]
                        # generated_questions.append(generated_question)
                        generated_questions.append(question_data)
    return generated_questions


def question_generation_type(sample_sentence, search_key, secondary_dependency=False):
    """
    Return the sequence of results
    """
    # parsing_result = predictor.predict(sentence=sample_sentence)
    # dependency_tree = parsing_result['hierplane_tree']
    allennlp_api_response = requests.post(
        allenNLP_predictor_url, json={"sentence": sample_sentence}, headers=headers)
    dependency_tree = allennlp_api_response.json()
    # print(dependency_tree)
    # Identify the root
    root = dependency_tree["root"]
    # print("The root in the sentence is: {}".format(root['word']))

    generated_questions = []

    # Store the question_struct
    # Check if root contains a verb
    # print(root["attributes"])
    if "VB" not in root["attributes"][0]:
        print("root is not a verb")
        return generated_questions

    if not secondary_dependency:
        # Identify its direct relation and check if there is a Dependency that
        # we
        for child in root["children"]:
            # Generated Questions for Direct Dependency on Predicate
            if child["nodeType"] == search_key:
                # Determine the Question Type
                question_type = determine_question_type(
                    child["nodeType"], child, child['word'], sample_sentence, dependency_tree)
                if question_type:
                    # print("The question can be generated on this dependency: {}".format(
                    #     child["nodeType"]))
                    # print("The question type is: {}".format(question_type))

                    # Next Step generate question
                    generated_question = QUESTION_GENERATION_FUNC[
                        child["nodeType"]](question_type, dependency_tree, child)
                    generated_questions.append(generated_question)
    else:
        # Generate Questions for Secondary Dependency on Predicate
        for child in root["children"]:
            # Generated Questions for Direct Dependency on Predicate
            if child["nodeType"] == search_key[0]:
                    # extract the question for secondary dependency relations
                intermediate_key = search_key[0]
                secondary_key = [search_key[1]]
                # handle special case
                if search_key[0] == "prep" and search_key[1] == "pobj":
                    secondary_key.append("pcomp")

                for grandchild in child.get("children", []):
                    if grandchild["nodeType"] in secondary_key:
                        # Detect the Secondary Dependency
                        secondary_dependency = '_'.join(
                            [intermediate_key, grandchild["nodeType"]])
                        # print(secondary_dependency)
                        # Determine the Question Type
                        if secondary_dependency == "prep_pcomp":
                            secondary_dependency = "prep_pobj"

                        if secondary_dependency != "prep_pobj":
                            question_type = determine_question_type(
                                secondary_dependency, grandchild, grandchild['word'], sample_sentence, dependency_tree)
                        else:
                            question_type = determine_question_type(secondary_dependency, child, child[
                                'word'], sample_sentence, dependency_tree)

                        if question_type:
                            if secondary_dependency != "prep_pobj":
                                generated_question = QUESTION_GENERATION_FUNC[secondary_dependency](
                                    question_type, dependency_tree, grandchild, intermediate_key)
                            else:
                                generated_question = QUESTION_GENERATION_FUNC[secondary_dependency](
                                    question_type, dependency_tree, child, intermediate_key)

                            generated_questions.append(generated_question)

    return generated_questions

QUESTION_GENERATION_FUNC = {'dobj': dobj_qg,
                            'neg': neg_qg, 'ccomp': ccomp_qg,
                            'tmod': tmod_qg, 'xcomp': xcomp_qg, 'advcl': advcl_qg, 'xcomp_ccomp': xcomp_ccomp_qg,
                            'xcomp_dobj': xcomp_dobj_qg, 'nsubjpass_num': nsubjpass_num_qg, 'prep_pobj': prep_pobj_qg}

if __name__ == "__main__":
    print("Welcome to Question Generator.")

    sample_sentence = "Huawei has announced it is investing $1 Billion to develop its own app ecosystem with Huawei Mobile Services Core which the company says is already integrated to over 45,000 apps."
    from time import time
    start = time()
    generated_questions = question_generation(sample_sentence)
    print(time() - start)
    print(generated_questions)

    #selected_question = question_selection(generated_questions)


# root_word = "wrote"
# pattern_tenses_response = requests.post(
#     pattern3_tenses_url, json={"word": root_word}, headers=headers)
# root_word_tense = pattern_tenses_response.json()
# print(root_word_tense)
