import boto3
from boto3.dynamodb.conditions import Key, Attr
import time
from random import shuffle
from operator import itemgetter
from pprint import pprint
from multiprocessing import Process, Manager
from multiprocessing.pool import ThreadPool
import re
import inflect
from elasticsearch import Elasticsearch, RequestsHttpConnection
from requests_aws4auth import AWS4Auth
import logging
import ast
import time
pool = ThreadPool(processes=2)
p = inflect.engine()
dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
table = dynamodb.Table('News1')


logging.basicConfig(
    format='[%(levelname)s] %(asctime)s [%(filename)s:%(lineno)d] %(message)s',
)
logger = logging.getLogger(__name__)

category2trend = {'business': 'business',
                  'world': 'international',
                  'politics': 'politics',
                  'us': 'news',
                  'u. s.': 'news',
                  'national': 'news',
                  'nation': 'news',
                  'health': 'health',
                  'entertainment': 'entertainment',
                  'science': 'science',
                  'technology': 'technology',
                  'science and technology': 'technology',
                  'sci': 'science',
                  'tech': 'technology',
                  'US': 'news',
                  'finance': 'business',
                  'entertaining': 'entertainment',
                  'gossip': 'entertainment',
                  'international': 'international',
                  'hollywood': 'entertainment',
                  'celebrity': 'entertainment',
                  'celebrities': 'entertainment',
                  'celeb': 'entertainment',
                  'cheerful': 'uplift',
                  # 'news': 'uplift',
                  'happy': 'uplift',
                  'uplifting': 'uplift',
                  'uplift': 'uplift',
                  'fashion': 'fashion',
                  'positive': 'uplift',
                  # 'interesting': 'technology',
                  'music': 'music',
                  'sports': 'sports',
                  'sport': 'sports',
                  'pleasant': 'uplift',
                  'political': 'politics',
                  'movie': 'entertainment'
                  }


def get_categories(category='us', keys=None):
    global category2trend
    return category2trend.get(category, [])


def id_query(id, filter=None):
    if filter:
        response = table.query(
            # ProjectionExpression="id,name,score,sub,title,opinion,related",
            KeyConditionExpression=Key('id').eq(id) & Key('lnp').eq(filter),
            ScanIndexForward=False
        )
        resp = response['Items']
        resp = fixlnp(resp)
        return resp
    else:
        response = table.query(
            # ProjectionExpression= "id,name,score,sub,title,opinion,related",
            KeyConditionExpression=Key('id').eq(id),
            ScanIndexForward=False
        )
        resp = response['Items']
        resp = fixlnp(resp)
        return resp


def np_lookup(np):
    if isinstance(np, list) and len(np) == 1:
        response = table.query(
            # ProjectionExpression= "id,name,score,sub,title,opinion,related",
            IndexName='lnp',
            KeyConditionExpression=Key('lnp').eq(np[0]),
            ScanIndexForward=False
        )
        resp = response['Items']
        if not resp and p.singular_noun(np[0]):
            response = table.query(
                # ProjectionExpression= "id,name,score,sub,title,opinion,related",
                IndexName='lnp',
                KeyConditionExpression=Key('lnp').eq(p.singular_noun(np[0])),
                ScanIndexForward=False
            )
            resp = response['Items']
        resp = fixlnp(resp)
        return resp
    else:
        return None


def np_scan(np):
    SCANLIMIT = 40
    start = time.time()
    if len(np) > 1:
        # fe = Attr('title').contains(np[0].lower()) & Attr('title').contains(np[1].lower())
        fe = Attr('title').contains(np[0].lower())
        for word in np[1:]:
            fe = fe & Attr('title').contains(word.lower())

    elif np:

        fe = Attr('title').contains(np[0].lower())
    else:
        return []

    # fe = Attr('title').contains(np.lower())

    response = table.scan(
        FilterExpression=fe,
    )

    resp = response['Items']
    i = 1
    while 'LastEvaluatedKey' in response:
        response = table.scan(
            ExclusiveStartKey=response['LastEvaluatedKey'], FilterExpression=fe)
        resp.extend(response['Items'])
        stop = time.time()
        elapse = stop - start
        if len(resp) > 5 or elapse > 1:
            break
        i += 1
    stop = time.time()
    resp = fixlnp(resp)
    return resp


def np_search(np):
    np = [x for x in np if x]
    if not np:
        return []
    scan = pool.apply_async(np_scan, (np,))
    query = pool.apply_async(np_lookup, (np,))

    qresult = query.get()
    if not qresult:
        result = scan.get()
    else:
        result = qresult
    result = fixlnp(result)
    return sorted(result[:5], key=itemgetter('score'), reverse=True)


def category(cat="news", handpicked=True) -> list:
    global category2trend
    import time
    t = time.time()
    # if cat == "entertainment" or cat == "fashion" or cat == "uplift" or cat == "sports":
    #     wt = t - (86400*14)
    # else:
    wt = t - (86400 * 10)
    # if cat == "politics" or cat == "science" or cat == "technology":
    #     wt = t - (86400 * 8)
    host = 'search-elasticnews-lasqqls6vejr3s35lzri6sr4x4.us-east-1.es.amazonaws.com'
    awsauth = AWS4Auth('AKIAIBDWK2ZAQZUTRGRQ',
                       'jCCV5uIeuK/D45f9I2+BH0u1sb/4t368yvQ6nAa4', 'us-east-1', 'es')

    try:
        cat = category2trend[cat]
    except KeyError:
        pass

    es = Elasticsearch(
        hosts=[{'host': host, 'port': 443}],
        http_auth=awsauth,
        use_ssl=True,
        verify_certs=True,
        connection_class=RequestsHttpConnection
    )
    res = None
    #handpicked needs to be false when no supervision i.e December-Next competition
    handpicked=False
    if handpicked:
        t1 = time.time()
        wt1 = t - 172800

        res = es.search(index="news1", body={
            "size": 200,
            "query": {
                "bool": {
                    "must": [
                        {"term": {'sub': cat}},
                        {"term": {'hand': 'true'}},


                    ],
                    "should": [
                        {
                            "range": {
                                "created": {
                                    "gte": wt1,
                                    "lte": t1
                                }
                            },
                        },
                        {
                            "match": {
                                "good": 1
                            }
                        }
                    ]
                },
            },
            "sort": [
                {"created": "desc"}
            ],
        }
        )
        # res = es.search(index="news1", body={
        #     "size": 200,
        #     "query": {
        #         "bool": {
        #             "must": [
        #                 {"term": {'sub': cat}},
        #                 {"term": {'hand': 'true'}},
        #                 # {"term": {'good': 1}},
        #                 {"range": {
        #                     "created": {
        #                         "gte": wt,
        #                         "lte": t
        #                     }
        #                 },
        #                 },
        #             ],
        #         },
        #     },
        #     "sort": [
        #         {"created": "desc"}
        #     ],
        # })

    # pprint(res['hits']['hits'][0:2])
    # {"term": {'hand': 'true'}},
    # logger.warning('handpicked result')
    # logger.warning(res['hits']['hits'][0])

    res1 = es.search(index="news1", body={
        "size": 200,
        "query": {
            "bool": {
                "must": {
                    "term": {'sub': cat}
                },
                "should": [
                    {
                        "range": {
                            "created": {
                                "gte": wt,
                                "lte": t
                            }
                        },
                    },
                    {
                        "match": {
                            "good": 1
                        }
                    }
                ]
            },
        },
        # "sort": [
        #     {"_score": "desc"}
        # ],
        "sort": [
            {"created": "desc"}
        ],
    }
    )

    if not res:
        res = res1

    resp = []
    for item in res['hits']['hits']:
        resp.append(item['_source'])

    final = []
    # grounding to nER
    for r in resp:
        bod = ast.literal_eval(str(r['body']))
        if len(bod) > 2:
            final.append(r)

    if not final and resp:
        return resp
    elif not final and not resp:
        resp = []
        for item in res1['hits']['hits']:
            resp.append(item['_source'])

        final = []
        # grounding to nER
        for r in resp:
            bod = ast.literal_eval(str(r['body']))
            if len(bod) > 2:
                final.append(r)

    if len(final) > 10:
        temp = final[0:10]  # reduces chance of duplicate articles
        shuffle(temp)
        final = temp + final[10:]

    return final


def handpicked():
    response = table.query(
        IndexName='hand',
        KeyConditionExpression=Key('hand').eq("true"),
        ScanIndexForward=False
    )
    items = response['Items']
    items = fixlnp(items)
    # insert temporary shuffle from todays HERE
    temp = items[:50]
    shuffle(temp)
    return temp


def fixlnp(data):
    pattern = r" s$"
    final = []
    prog = re.compile(pattern)
    for e in data:
        m = prog.search(e['lnp'])
        if 'lnp' in e and m:
            e['lnp'] = re.sub(pattern, "'s", e['lnp'])
        final.append(e)
    return final


def get_reddit_titles():
    """
        Scans Db for a list of people or words in the title of the moment
        :param np: list of people or words
        :return: list ordered by date of moments
        """

    i = 1
    from time import time
    from datetime import timedelta
    from decimal import Decimal
    now = time()
    filter = int(now - timedelta(7).total_seconds())
    fe = Attr('created').gte(Decimal(filter))
    response = table.scan(FilterExpression=fe)
    resp = response['Items']
    while 'LastEvaluatedKey' in response:
        response = table.scan(
            ExclusiveStartKey=response['LastEvaluatedKey'], FilterExpression=fe)
        resp.extend(response['Items'])
        # break

    import re
    # from datetime import date, timedelta, datetime
    # # weekago = date.today() - timedelta(7)
    end = time()
    lfinal = set()
    for moment in resp:
        lfinal.add(moment['title'])
    #     if (datetime.today() - datetime.strptime(moment['timestamp'], '%Y-%m-%d %H:%M:%S')) <= timedelta(7):
    #         moment['timestamp'] = datetime.strptime(moment['timestamp'], '%Y-%m-%d %H:%M:%S').toordinal()
    #         l.append(moment)
    # sorted(l, key=lambda d: int(''.join(re.split(r"\-*\s*\:*", d['timestamp']))))
    # l = list(set(sorted(l, key=lambda d: d['timestamp'], reverse=True)))
    # result = [dict(tupleized) for tupleized in set(tuple(item.items()) for item in resp)]
    # from pprint import pprint
    # pprint(l)
    print(end - now)
    return list(lfinal)


def news_search(np, ner, env='local', strict=True, limit=100):
    t = time.time()
    wt = t - 604800

    host = 'search-elasticnews-lasqqls6vejr3s35lzri6sr4x4.us-east-1.es.amazonaws.com'
    awsauth = AWS4Auth('AKIAIBDWK2ZAQZUTRGRQ',
                       'jCCV5uIeuK/D45f9I2+BH0u1sb/4t368yvQ6nAa4', 'us-east-1', 'es')
    # logger.warning(np)
    es = Elasticsearch(
        hosts=[{'host': host, 'port': 443}],
        http_auth=awsauth,
        use_ssl=True,
        verify_certs=True,
        connection_class=RequestsHttpConnection
    )
    logger.info(np)
    # if isinstance(, list)
    # if strict:
    res = es.search(index="news1", body={
        "size": limit,
        "query": {
            "bool": {
                "must": {
                    "match": {'summary': np}
                },
                "should": [
                    {
                        "range": {
                            "created": {
                                "gte": wt,
                                "lte": t
                            }
                        },
                    },
                    # {
                    #     "match": {
                    #         "good": 1
                    #     }
                    # }
                ]
            },
        },
        # "sort": [
        #     #{"_score": "desc"},
        #     #{"created": "desc"}
        # ],
    }
    )
    resp = []
    for item in res['hits']['hits']:
        resp.append(item['_source'])

    final = []

    # grounding to nER
    for r in resp:
        # bod = ast.literal_eval(str(r['body']))
        # b = ' '.join(bod[:])
        b = r['summary']
        nplist = np.split()
        partialNP = False

        # if ner it better match
        nermatch = True
        if ner:
            for ne in ner:
                nermatch = True if ne.lower() in b else False
                if nermatch:
                    break
        if ner and not nermatch:
            continue

        for n in nplist:
            if n.lower() in b:
                partialNP = True

                if (ner and nermatch) or partialNP:
                    final.append(r)

    # pprint(final[0])
    if not final:
        logger.info("NO MATCH")

    # print(res['hits']['hits'][0]['_source']['top_comments'])
    # print("Got %d Hits:" % res['hits']['total'])
    # for hit in res['hits']['hits']:
    #     print("%(timestamp)s %(author)s: %(text)s" % hit["_source"])
    newlist = sorted(final[:10], key=itemgetter('score'))
    final = newlist + final[10:]
    return final


def news_search_gram(np, limit=100):
    t = time.time()
    wt = t - 604800

    host = 'search-elasticnews-lasqqls6vejr3s35lzri6sr4x4.us-east-1.es.amazonaws.com'
    awsauth = AWS4Auth('AKIAIBDWK2ZAQZUTRGRQ',
                       'jCCV5uIeuK/D45f9I2+BH0u1sb/4t368yvQ6nAa4', 'us-east-1', 'es')
    # logger.warning(np)
    es = Elasticsearch(
        hosts=[{'host': host, 'port': 443}],
        http_auth=awsauth,
        use_ssl=True,
        verify_certs=True,
        connection_class=RequestsHttpConnection
    )
    res = es.search(index="news1", body={
        "size": limit,
        "query": {
            "bool": {
                "must": {
                    "match": {'body': np}
                },
                # "should": [
                #     {
                #         "range": {
                #             "created": {
                #                 "gte": wt,
                #                 "lte": t
                #             }
                #         },
                #     },
                # {
                #     "match": {
                #         "good": 1
                #     }
                # }
                # ]
            },
        },
        "sort": [
            #{"_score": "desc"},
            {"created": "desc"}
        ],
    }
    )
    resp = []
    for item in res['hits']['hits']:
        resp.append(item['_source'])
    return resp


if __name__ == "__main__":
    pass
    # t = news_search("pittsburgh", None, strict=True, limit=20)
    # pprint(t[0])
    # t = category(cat='news')
    # pprint(t[0:10])
    # print(len(get_artist()))
    # t = get_reddit_titles()
    # import pickle
    #
    # with open('week.txt', 'wt') as out:
    #     pprint(t, stream=out)
    #
    # fixlnp([{'lnp': 'grammy s'}])
    #
    # ["midterm elections", "donald trump", "supreme court", "net neutrality", "amazon's second headquarters",
    #  "solar energy"]
    t = news_search_gram("amazon's second headquarters")
    pprint(t[0:3])
