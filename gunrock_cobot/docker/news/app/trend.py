import redis
import ast
import logging
from kgapi import *

logger = logging.getLogger(__name__)
KEYWORD_PREFIX = "gunrock:kg:headline:"
r = redis.StrictRedis(host='52.87.136.90', port=16517,
                      socket_timeout=3,
                      socket_connect_timeout=1,
                      retry_on_timeout=True,
                      db=0, password="alexaprize", decode_responses=True)


def get_keyword_map(key):
    """
    Trending Items
    :param key: a category of one of the following ['actors', 'animals', 'athletes', 'authors', 'baseball_players', 'baseball_teams',
                  'basketball_players',
                  'basketball_teams', 'books', 'auto_companies', 'colleges_universities', 'automobile_models',
                  'countries',
                  'dog_breeds', 'fashion_labels', 'financial_companies', 'foods', 'games', 'governmental_bodies',
                  'childrens_tv_programs',
                  'films', 'musicians', 'people', 'politicians', 'fast_food_restaurants', 'reality_shows',
                  'retail_companies',
                  'scientists', 'soccer_players', 'soccer_teams', 'programming_languages', 'songs', 'celestial_objects',
                  'sports_cars', 'sports_teams', 'teen_pop_artists', 'tv_shows']
    :return: list of trending items in that category
    """
    return ast.literal_eval(r.get(KEYWORD_PREFIX + key))


# flatten = lambda l: [item.replace('_', ' ') for sublist in l for item in sublist]
# category_list = ["sport", "world", "us", "business", "health", "entertainment", "sci_tech"]
# topics = ["sport", "world", "us", "business", "health", "entertainment", "science and technology"]
def get_categories(category='us', keys=None):
    category2trend = {'business': 'business',
                      'world': 'international',
                      'politics': 'politics',
                      'us': 'news',
                      'national': 'news',
                      'nation': 'news',
                      'health': 'health',
                      'entertainment': 'entertainment',
                      'science': 'science',
                      'technology': 'technology',
                      'science and technology': 'technology',
                      'sci': 'science',
                      'tech': 'technology',
                      'US': 'news',
                      'finance': 'business',
                      'entertaining': 'entertainment',
                      'gossip': 'entertainment',
                      'international': 'international',
                      'hollywood': 'entertainment',
                      'celebrity': 'entertainment',
                      'celebrities': 'entertainment',
                      'celeb': 'entertainment',
                      'cheerful': 'uplift',
                      # 'news': 'uplift',
                      'happy': 'uplift',
                      'uplifting': 'uplift',
                      'fashion': 'fashion',
                      'positive': 'uplift',
                      # 'other news':'uplift',
                      # 'other types': 'uplift',
                      # 'other kinds': 'uplift',
                      # 'interesting': 'technology',
                      'music': 'music',
                      'sports': 'sports',
                      'sport': 'sports',
                      }

    return category2trend.get(category, [])


# OLD
# def get_categories(category='entertainment', keys = None):
#     # category2trend = {'business':['financial_companies',],
#     #                   'entertainment':['actors','fashion_labels','games', 'films', 'musicians', 'songs'],
#     #                   'science':['scientists'],
#     #                   'sports':['athletes', 'sports_teams', 'baseball_players', 'baseball_teams', 'basketball_players', 'basketball_teams', 'soccer_players','soccer_teams'],
#     #                   'technology':['auto_companies', 'automobile_models'],
#     #                   'politics': ['countries','governmental_bodies', 'politicians'],
#     #                   'other': ['authors','books', 'people']}
#     #
#     #
#
#
#     category2trend = {'business': ['retail_companies'],
#                       'entertainment': ['actors', 'musicians'],
#                       'science':['scientists'],
#                       'sports':['sports_teams','athletes'],
#                       'athletes':['athletes'],
#                       'athlete':['athletes'],
#                       'technology':['sports_cars'],
#                       'politics':['politicians'],
#                       'movies':['films'],
#                       'films':['films'],
#                       'books':['books'],
#                       'baseball':['baseball_players'],
#                       'basketball':['basketball_players'],
#                       'soccer':['soccer_players'],
#                       'celebrities':['people'],
#                       'fashion':['fashion_labels'],
#                       'music':['musicians'],
#                       'games':['games'],
#                       'actor':['actors'],
#                       'actors': ['actors'],
#                       'aliens':['celestial_objects'],
#                       'national': ['politicians'],
#                       'world':['countries'],
#                       'celebs':['people'],
#                       'tech': ['scientists'],
#                       'finance': ['financial_companies'],
#                       'celebrity': ['people'],
#                       'people': ['people'],
#                       'musicians':['musicians'],
#                       'sport': ['sports_teams','athletes'],
#                       'social':['people']
#                       }
#
#     if keys:
#         return list(category2trend.keys()) + list(set(flatten(category2trend.values())))
#
#
#     for cat in category2trend.keys():
#         if category in cat:
#             category = cat
#             break
#         elif category in [w.replace('_', ' ') for w in category2trend[cat]]:
#             category = cat
#             break
#     # print(category)
#     try:
#         topics = category2trend[category]
#         random_int = random.randint(0, len(topics)-1)
#         print(topics[random_int])
#         candidates = get_keyword_map(topics[random_int])
#     except KeyError:
#         return []
#     if type(candidates) is not list:
#         candidates = [candidates]
#
#     random.shuffle(candidates)
#     return candidates, category


def get_people(person):
    person = person.lower()
    keys = ['actors', 'animals', 'athletes', 'authors', 'baseball_players', 'baseball_teams',
            'basketball_players',
            'basketball_teams', 'books', 'auto_companies', 'colleges_universities', 'automobile_models',
            'countries',
            'dog_breeds', 'fashion_labels', 'financial_companies', 'foods', 'games', 'governmental_bodies',
            'childrens_tv_programs',
            'films', 'musicians', 'people', 'politicians', 'fast_food_restaurants', 'reality_shows',
            'retail_companies',
            'scientists', 'soccer_players', 'soccer_teams', 'programming_languages', 'songs', 'celestial_objects',
            'sports_cars', 'sports_teams', 'teen_pop_artists', 'tv_shows']

    people = {}
    for k in keys:
        people[k] = get_keyword_map(k)
    # print(people)
    # if person in people.values():
    for k, v in people.items():
        v = [w.lower() for w in v]
        if person in v:
            if k == 'people':
                k = 'celebrities'
            return k.replace('_', ' ')

    return []


if __name__ == "__main__":
    # print(get_keyword_map('people'))
    # print(get_categories('people'))
    # print(get_people('lebron james'))

    print(get_keyword_map('actors'))
