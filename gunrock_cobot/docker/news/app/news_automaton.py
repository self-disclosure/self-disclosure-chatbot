#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""this file contains the DFA for the news response generator"""

import re
import logging
import random
from enum import Enum, IntFlag, auto
from typing import List, Tuple, Optional
from evi import *
import template_manager
import json
from news_kg_api import *
from news_acknowledgement import NewsAcknowledgement
from template_manager import Template, TemplateManager
from user_profiler.user_profile import UserProfile
from utils import *
# from utils import NER_CONCEPT_KEY, NER_GOOGLEKG_KEY, INTENT_PATTERN_MANAGEMENT
# from utils import SYS_OTHER_TOPIC_LIST, TOPIC_KEYWORD_MAP, MODULE_KEYWORD_MAP, KEYWORD_MODULE_MAP, CHIT_CHAT_NER_IGNORE,\
#     NER_IGNORE_LIST, can_map_to_chit_chat, NEWS_MANUAL_NER_LIST, CORONAVIRUS_MAPPING_DICT, IGNORED_NER_LABELS
from nlu.dataclass import CentralElement, ReturnNLP
from nlu.constants import DialogAct as DialogActEnum
from nlu.constants import Positivity as PositivityEnum
from types import SimpleNamespace
from fuzzywuzzy import fuzz
from nlu.command_detector import CommandDetector
from nlu.question_detector import QuestionDetector
from nlu.constants import TopicModule
from question_handling.quetion_handler import QuestionHandler, QuestionResponse, QuestionResponseTag
from selecting_strategy.module_selection import ModuleSelector
from user_profiler.user_profile import UserProfile


BACKSTORY_CONFIDENCE = .80
BACKSTORY_THRESHOLD = 0.80
MAX_THRESHOLD = 1.0
HIGH_THRESHOLD = 0.75
MED_THRESHOLD = 0.5
LOW_THRESHOLD = 0.25
COBOT_API_KEY = 'EwecQ1fkEd4YMVUOHHpeSam7zbef53gipQsxAUki'

# Make True to add QG part to add question in each response for next
# sentence in news
use_generated_question = False

template_social = template_manager.Templates.social
template_shared = template_manager.Templates.shared


logging.basicConfig(
    format='[%(levelname)s] %(asctime)s [%(filename)s:%(lineno)d] %(message)s',
)

logging.getLogger().setLevel(logging.DEBUG)

errorstr = "My news understanding doesn't seem to be working. "

MOMENT_TAG_MAP = {
    'basketball': ['nba', 'ncaa basketball', 'basketball'],
    'football': ['nfl', 'ncaa football'],
    'hockey': ['nhl', 'hockey'],
    'baseball': ['mlb', 'baseball'],
    'tennis': ['tennis'],
    'wrestling': ['wrestling', 'wwe'],
    'soccer': ['premier league', 'major league soccer', 'football', 'fifa 19', 'uefa nations league', 'la liga'],
    'golf': ['pga', 'golf'],
    'gymnastics': ['gymnastics'],
    'Politics': ['politics', 'us news'],
    'Entertainment': ['movies'],
    'Technology': ['technology']
}


class ProposeContinue(IntFlag):
    CONTINUE, STOP, UNCLEAR = auto(), auto(), auto()


class ProposeTopic():
    MOVIE, TECHSCI, TRAVEL = "MOVIECHAT", "TECHSCIENCECHAT", 'TRAVELCHAT'
    GAME, POLITICS, SPORT = 'GAMECHAT', "POLITICS", "SPORT"
    MUSIC, ANIMAL, HOLIDAY = 'MUSICCHAT', 'ANIMALCHAT', 'HOLIDAYCHAT'
    FOOD, FASHION, PSY, WEATHER, NONE = 'FOODCHAT', 'FASHIONCHAT', 'PSYPHICHAT', 'WEATHER', None


class Sentiment(Enum):
    """
    Enum class representing the lexical information from input_data
    """
    NEGATIVE, POSITIVE, UNKNOWN = 'ans_neg', 'ans_pos', 'ans_unknown'

    @classmethod
    def check(cls, lexical):
        try:
            return Sentiment(lexical)
        except ValueError:
            return Sentiment.UNKNOWN

    @classmethod
    def has_sentiment(cls, sentiment, lexicals: List[str]):
        return any(cls.check(lexical) is sentiment for lexical in lexicals)

    def in_lexicals(self, lexicals: List[str]):
        return Sentiment.has_sentiment(self, lexicals)


class States(Enum):
    """
    Defines all state that is possible within GameAutomaton.
    """
    # ["statement", "acknowledgement", "opinion", "appreciation", "abandoned", "yes_no_question", "pos_answer",
    # "opening", "closing", "open_question", "neg_answer", "other_answers", "other", "commands", "hold",
    # "not_understanding", "apology", "thanking", "respond_to_apologize"]

    intro, init, error, done = auto(), auto(), auto(), auto()
    person2 = auto()
    topic1 = auto()
    EVI = auto()
    readmore, headlines, backstory = auto(), auto(), auto()
    giveOpinion, agree, disagree = auto(), auto(), auto()

    # DNE 14
    not_understanding = auto()
    # DA
    appreciation, abandon, yes_no_opinion, = auto(), auto(), auto()
    opinion, statement, comment = auto(), auto(), auto()

    # 21
    complaint, thanking, apology, respond_to_apology, nonsense = auto(
    ), auto(), auto(), auto(), auto()
    other_answers, open_question_factual, open_question_opinion = auto(), auto(), auto()

    debate1, debate2, debateExit, confirmNews, uplift = auto(
    ), auto(), auto(), auto(), auto()
    proposeModule, othernews, source, returning = auto(), auto(), auto(), auto()

    debate, backstoryReason, cinit, ginit, gram, noDomain = auto(
    ), auto(), auto(), auto(), auto(), auto()

    requestTopics, simpleOp, simpleYN, simpleYNReason, advice = auto(
    ), auto(), auto(), auto(), auto()
    readmore_2, readmore_3 = auto(), auto()
    # Mingyang QA
    question_answer = auto()
    sport_type_title, readmore_moments, othernews_moments = auto(), auto(), auto()
    handle_neg_response, intro_chit_chat, chit_chat, command_not_handled = auto(), auto(), auto(), auto()
    # Mingyang exit_news
    exit_news = auto()

    def transduce(self, automaton) -> Tuple[str, ProposeContinue, ProposeTopic]:
        """
        Core function for the state to perform transition.
        :param automaton: a reference to the GameAutomaton
        :return: see ContextManager._Decorator.state()
        """
        # TODO: ASR errors, incoherence
        if automaton.cm.user_attributes.last_module != 'NEWS' \
                and not automaton.cm.user_attributes.newschat.get('curr_state'):
            next_state, params = automaton.__getattribute__(
                't_' + 'intro_chit_chat')()  # type: States
            return automaton.__getattribute__('s_' + next_state.name)(params)
        next_state, params = automaton.__getattribute__(
            't_' + self.name)()  # type: States
        return automaton.__getattribute__('s_' + next_state.name)(params)


class ContextManager:
    """
    GameAutomaton is created at every utterances, this class is the API class for interacting with user_attributes,
    which has a persistent, per-user, store.
    """

    def __init__(self, user_attributes_ref):
        self._user_attributes = user_attributes_ref  # type: UserAttributes
        if not self.newschat.get('curr_state'):
            self.newschat['curr_state'] = States.intro_chit_chat.value
        if not self.newschat.get('curr_news_turns'):
            self.newschat['curr_news_turns'] = 0
        if not self.newschat.get('curr_news_article'):
            self.newschat['curr_news_article'] = None
        if not self.newschat.get('previous_topic'):
            self.newschat['previous_topic'] = []
        if not self.newschat.get('used_urls'):
            self.newschat['used_urls'] = []
        if not self.newschat.get('used_titles'):
            self.newschat['used_titles'] = []
        if not self.newschat.get('last_title_desc'):
            self.newschat['last_title_desc'] = []
        if not self.newschat.get('chit_chat_last_dialog'):
            self.newschat['chit_chat_last_dialog'] = []
        if not self.newschat.get('used_chit_chat'):
            self.newschat['used_chit_chat'] = []
        if not self.newschat.get('supported_keyword_dict'):
            self.newschat['supported_keyword_dict'] = get_frequent_keyword_dict()
        if not self.newschat.get('user_ner'):
            self.newschat['user_ner'] = None
        if not self.newschat.get('next_ner'):
            self.newschat['next_ner'] = None
        if not self.newschat.get('backstory_reason'):
            self.newschat['backstory_reason'] = None
        if not self.newschat.get('uplifting_news_count'):
            self.newschat['uplifting_news_count'] = 0

        _curr_state = self.newschat['curr_state']
        _prev_state = self.newschat.get('prev_state')

        try:
            self._curr_state = States(_curr_state)
            self._prev_state = States(_prev_state) if _prev_state else None
        except ValueError as e:
            logger.warning(
                "Cannot initialize with invalid state id; {}".format(e))
            self._curr_state = States.intro_chit_chat
            self._prev_state = None

        if not self.newschat.get('prev_state_params'):
            self.newschat['prev_state_params'] = {}

    # MARK: - references

    @property
    def user_attributes(self):
        return self._user_attributes

    @property
    def newschat(self):
        return self.user_attributes.newschat

    @property
    def last_module(self):
        return self.user_attributes.last_module

    @property
    def template_manager(self):
        return self.user_attributes.template_manager

    @property
    def used_topic(self):
        ms = getattr(self.user_attributes, "module_selection")
        if ms:
            return getattr(ms, "used_topic")
        else:
            return []

    @property
    def sys_env(self):
        return self.user_attributes.sys_env
        # return 'prod'

    # MARK: - Getter/Setter - System

    @property
    def curr_state(self) -> Optional[States]:
        return States(self._curr_state) if self._curr_state else None

    @curr_state.setter
    def curr_state(self, state: States):
        self._curr_state = state.value
        self._user_attributes.newschat['curr_state'] = state.value

    @property
    def prev_state(self) -> Optional[States]:
        return States(self._prev_state) if self._prev_state else None

    @prev_state.setter
    def prev_state(self, state: States):
        self._prev_state = state.value
        self._user_attributes.newschat['prev_state'] = state.value

    @property
    def prev_state_params(self) -> dict:
        return self.newschat['prev_state_params']

    @property
    def entities(self) -> dict:
        if not self.newschat.get('entities'):
            self.newschat['entities'] = {}
        return self.newschat['entities']

    @property
    def returning(self) -> bool:
        if not self.newschat.get('return'):
            self.newschat['return'] = True
        else:
            return self.newschat['return']

    @returning.setter
    def returning(self, b):
        self.newschat['return'] = b

    @property
    def last_session_id(self) -> str:
        if not self.newschat.get('last_session_id'):
            self.newschat['last_session_id'] = {}
        return self.newschat['last_session_id']

    @last_session_id.setter
    def last_session_id(self, inputstr):
        self.newschat['last_session_id'] = inputstr

    @property
    def debates(self) -> list:
        if not self.newschat.get('debates'):
            self.newschat['debates'] = []
        elif type(self.newschat['debates']) is not list:
            self.newschat['debates'] = []
        return self.newschat['debates']

    # MARK: - Getter/Setter - user_attributes stores
    @property
    def related(self) -> dict:
        if not self.newschat.get('related'):
            self.newschat['related'] = {}
        elif type(self.newschat['related']) is not dict:
            self.newschat['related'] = {}
        return self.newschat['related']

    @property
    def backstory(self) -> dict:
        if not self.newschat.get('backstory'):
            self.newschat['backstory'] = {}
        elif type(self.newschat['backstory']) is not dict:
            self.newschat['backstory'] = {}
        return self.newschat['backstory']

    @backstory.setter
    def backstory(self, b):
        self.newschat['backstory'] = b

    @property
    def articles(self) -> list:

        if not self.newschat.get('articles'):
            self.newschat['articles'] = []
        elif type(self.newschat['articles']) is not list:
            self.newschat['articles'] = []
        return self.newschat['articles']

    @property
    def ners(self) -> list:
        if not self.newschat.get('ners'):
            self.newschat['ners'] = []
        elif type(self.newschat['ners']) is not list:
            self.newschat['ners'] = []
        return self.newschat['ners']

    @ners.setter
    def ners(self, ners):
        self.newschat['ners'] = ners

    @property
    def topics(self) -> list:

        if not self.newschat.get('topics'):
            self.newschat['topics'] = []
        elif type(self.newschat['topics']) is not list:
            self.newschat['topics'] = []
        return self.newschat['topics']

    @property
    def event(self) -> list:
        if not self.newschat.get('event'):
            self.newschat['event'] = []
        elif type(self.newschat['event']) is not list:
            self.newschat['event'] = []
        return self.newschat['event']

    @property
    def grams(self) -> list:
        if not self.newschat.get('grams'):
            self.newschat['grams'] = []
        elif type(self.newschat['grams']) is not list:
            self.newschat['grams'] = []
        return self.newschat['grams']

    # MARK: - Getter/Setter - user_attributes stores

    # MARK: - APIs

    def flush(self):
        self.curr_state = States.init
        self.prev_state = None
        self.prev_state_params.clear()


# Prepare Some Intents for Easy Transition in News-Module
class NewsIntents(object):

    def __init__(self, input_data, user_attributes_ref):
        self.input_data = input_data
        self.user_attributes = user_attributes_ref
        self.ask_question = False
        self.ask_opinion_question = False
        self.nointerest_news = False
        self.quit_chat = False
        self.iscommand = False
        self.central_elem = CentralElement.from_dict(self.input_data['central_elem'])
        self.returnnlp = ReturnNLP(self.input_data['returnnlp'])
        self.command_detector = CommandDetector(self.input_text(), self.input_data['returnnlp'])
        self.question_detector = QuestionDetector(self.input_text(), self.input_data['returnnlp'])
        self.question_handler = QuestionHandler(
            self.input_text(),
            self.input_data['returnnlp'],
            BACKSTORY_THRESHOLD,
            input_data["system_acknowledgement"],
            self.user_attributes
        )
        self.cm = ContextManager(self.user_attributes)
        self.__detect_intents()

    def __detect_intents(self):
        self.command_detect_utility()
        self.detect_question()
        self.detect_nointerest_news()

    def command_detect_utility(self):
        if self.command_detector.has_command():
            self.iscommand = True

    def detect_question(self):
        if self.question_detector.has_question():
            self.ask_question = True
        if self.central_elem.has_dialog_act([DialogActEnum.OPEN_QUESTION_OPINION]):
            no_of_segments = len(self.returnnlp)
            count_of_segments = 0
            for segment in self.returnnlp:
                if segment.has_dialog_act(DialogActEnum.OPEN_QUESTION_OPINION):
                    if not (no_of_segments > 1 > count_of_segments):
                        self.ask_opinion_question = True
                count_of_segments += 1

        if self.has_know_about() and self.detect_frequent_keyword_exists():
            self.ask_question = False
            self.ask_opinion_question = False

    def handle_news_question(self):
        # Step 1: check if it has question
        if not self.question_detector.has_question():
            return None

        # Step 2: Check if it's unanswerable
        if self.is_unanswerable_question():
            return QuestionResponse(response=self.question_handler.generate_i_dont_know_response())

        # # step 3:
        # response = self.handle_topic_specific_question()
        # if response:
        #     return response

        # Step 4
        response = self.handle_general_question()
        if response:
            return response
        return None

    def is_unanswerable_question(self) -> bool:
        if self.question_detector.is_ask_back_question() or self.question_detector.is_follow_up_question():
            return True
        return False

    def handle_general_question(self) -> QuestionResponse:
        answer = self.question_handler.handle_question()
        return answer

    def generate_propose_topic_response(self, template_selector, template_slot=None):
        if template_slot is None:
            template_slot = {}
        topic_proposal_template_manager = TemplateManager(Template.transition, self.user_attributes)
        return topic_proposal_template_manager.speak(f"{template_selector}", template_slot)

    def detect_nointerest_news(self):
        """
        detect the intent of showing not interest in news or switch to other topic
        """
        utterance = self.returnnlp[-1].text
        if any(re.search(x, utterance) for x in INTENT_PATTERN_MANAGEMENT['QUIT_MODULE_PATTERN']):
            self.nointerest_news = True

        if any(re.search(x, utterance) for x in INTENT_PATTERN_MANAGEMENT['QUIT_CHAT_PATTERN']):
            self.nointerest_news = True
            self.quit_chat = True

    def detect_question_regex(self):
        try:
            pat = re.compile(
                r"can i (ask|share|tell)|can you (ask|share|tell)|should i (ask|share|tell)|should you (ask|share|tell)"
                r"|what\s*(do)?\s*you|what (is|are) your|when (is|are) your|which (is|are) your|who (is|are) your")
            if pat.search(self.input_text()):
                return True
            return False
        except Exception as e:
            logging.error(
                "[NEWSMODULE][ERROR] detect_question_regex doesn't give proper return value: {}".format(e), exc_info=True)

    def input_text(self):
        pat = re.compile(r"open gunrock and |alexa|Alexa|echo|Echo")
        try:
            text = re.sub(
                pat, '', self.input_data['text'])
        except (KeyError, TypeError):
            text = re.sub(pat, '', self.input_data['text'])
        # return text
        return text

    def detect_frequent_keyword_exists(self):
        # Check for keyword in frequent word list and add to list of NERs
        user_response = self.input_text()
        frequent_keywords = self.cm.newschat['supported_keyword_dict']
        for dict_key in frequent_keywords:
            if dict_key in user_response:
                return True
        return False

    def has_know_about(self):
        pat = re.compile(
            r"(heard|know|idea|anything) about|"
            r"(what is|what's|what|how) (up|going on|about)")
        if pat.search(self.input_text()):
            return True
        else:
            return False


class NewsAutomaton:

    class _Decorators:

        @classmethod
        def transition(cls, func):
            """
            Wrapper that uses the function as a transition state.
            :param func: function that must be named: 't_<state_name>'
                        function must return either:
                            None (goes to state with same name),
                            State,
                            None, params (params get passed to the state)
                            State, params
            :return: a decorated function
            """

            def wrapper(automaton):
                resp = func(automaton)
                if isinstance(resp, tuple):
                    next_state, params = resp  # type: Tuple[State, dict]
                else:
                    next_state = resp  # type: State
                    params = {}

                params['t_'] = automaton.cm.curr_state

                if next_state:
                    automaton.cm.curr_state = next_state
                    return next_state, params
                else:
                    return automaton.cm.curr_state, params

            return wrapper

        @classmethod
        def state(cls, func):
            """
            Wrapper that uses the function as a state.
            :param func: function that must be named: 's_<state_name>'
                        function must return:
                            utterance: str, next_state: States, propose_continue: ProposeContinue
            :return: wrapper will set the next state into user_attributes,
                    and return utterance and propose_continue
            """

            def wrapper(automaton, params):
                utt, next_state, propose_continue, propose_topic = func(
                    automaton, params)

                automaton.cm.prev_state = automaton.cm.curr_state
                automaton.cm.curr_state = next_state
                return utt, propose_continue, propose_topic

            return wrapper

    # MARK: - Properties

    def __init__(self, input_data, user_attributes_ref, user_attributes_name_space):
        self.user_attribute = user_attributes_ref
        self.cm = ContextManager(user_attributes_ref)  # type: ContextManager
        self.user_profile = UserProfile(user_attributes_ref)
        self.input_data = input_data
        # Detect the Special Intents
        self.intents = NewsIntents(input_data, user_attributes_ref)
        self.template_manager_hash = user_attributes_name_space
        self.template_news = TemplateManager(Template.news, user_attributes_ref=self.template_manager_hash)
        self.module_selection = ModuleSelector(user_attributes_name_space)

        # Detect the NER list and keywords list
        self.ners = self.get_ner_list()
        # Detect other topic list
        self.other_topic = self.get_other_topic()
        # Load the json file that stores chithcat
        with open('political_topics_chitchat.json', 'r') as politic_f:
            self.political_chitchat = json.load(politic_f)
        # Initialize the context available political topic
        self.cm.newschat['available_political_topic'] = [x for x in self.political_chitchat.keys()]
        self.acknowledgement = NewsAcknowledgement(self.input_data, self.cm, self.template_news, States)
        self.returnnlp = ReturnNLP(self.input_data['returnnlp'])
        self.central_elem = CentralElement.from_dict(self.input_data['central_elem'])
        self.sys_acknowledgement = input_data.get('system_acknowledgement', {})
        self.user_profile = UserProfile(self.user_attribute)
        logging.info("[NEWS_MODULE] detected ners in the current utterance: {} returnnlp {}".format(self.ners, self.returnnlp))
    # MARK: - API

    def response(self):
        """
        Main API for getting a response from the automaton.
        :return: a dict with utterance and other flags as specified by the selecting strategy.
        """
        try:
            if self.module_selection.last_topic_module != "NEWS":
                if self.module_selection.get_top_propose_keyword() and self.module_selection.get_top_propose_keyword().text in ["corona", "corona virus", "coronavirus"]:
                    utt, propose_continue, propose_topic = States.intro_chit_chat.transduce(automaton=self)
                elif self.cm.newschat.get("previous_topic", None) and self.cm.newschat.get("previous_topic", None)[0] != "Interesting_News":
                    prev_topic = self.cm.newschat.get("previous_topic", None)[1]
                    self.cm.curr_state = States.headlines
                    if "Sports" in prev_topic:
                        prev_topic = prev_topic.split(':')[1]
                        self.cm.curr_state = States.sport_type_title
                    utt = "I remember you follow {}. Would you like to hear some latest news about {}? ".format(prev_topic, prev_topic)
                    propose_continue = ProposeContinue.CONTINUE
                    propose_topic = ProposeTopic.NONE
                else:
                    utt, propose_continue, propose_topic = States.intro.transduce(automaton=self)
            else:
                utt, propose_continue, propose_topic = self.cm.curr_state.transduce(automaton=self)
        except NotImplementedError as e:
            logger.warning(e)
            utt = "I'm confused. Let's start over. Try again."
            propose_continue = ProposeContinue.STOP, ProposeTopic.NONE
            propose_topic = ProposeTopic.NONE
            self.cm.flush()

        self.cm.newschat['propose_continue'] = propose_continue.name
        self.module_selection.propose_topic = propose_topic
        self.cm.last_session_id = self.session_id
        if self.cm.newschat.get("previous_topic", None) and self.cm.newschat.get("previous_topic", None)[0] != "Interesting_News":
            self.user_profile.topic_module_profiles.news.previous_topic.append(self.cm.newschat['previous_topic'][1])

        temp = vars(self.cm)
        for item in temp:
            print(item, ' : ', temp[item])
        temp = vars(self.cm.user_attributes)
        for item in temp:
            print(item, ' : ', temp[item])
        print(self.cm.newschat)
        return {
            'response': utt,
            'z_cm': (self.cm.prev_state, self.cm.curr_state)
        }

    # MARK: - Internal API
    @property
    def session_id(self):
        return self.input_data['session_id']

    @property
    def coreference(self):
        if self.input_data['features']['coreference']:
            return self.input_data['features']['coreference']['text']
        else:
            return None

    @property
    def input_text(self) -> str:
        # text = removeStopwords(self.input_data['text'])
        pat = re.compile(r"open gunrock and |alexa|Alexa|echo|Echo")
        try:
            text = re.sub(
                pat, '', self.input_data['features']['converttext'])
        except (KeyError, TypeError):
            text = re.sub(pat, '', self.input_data['text'])
        # remove coreference as it conflicts with some regex like sports names.
        # if self.coreference:
        #    return self.coreference
        return text

    @property
    def enter_news(self) -> bool:
        # doing news like this should be fine
        pat = re.compile(r"about (the )?news|some news|news today|news about|news|talk about|chat about")
        if pat.search(self.input_text):
            return True
        else:
            return False

    @property
    def enter_other_news(self) -> bool:
        pat = re.compile(r"other news|any more news")
        if pat.search(self.input_text):
            return True
        else:
            return False

    @property
    def detect_negative_neutral_response(self) -> bool:
        pat = re.compile(r"nothing")
        if pat.search(self.input_text):
            return True
        else:
            return False

    @property
    def detect_positive_neutral_response(self) -> bool:
        pat = re.compile(r"anything")
        if pat.search(self.input_text):
            return True
        else:
            return False

    @property
    def negative_resp_news_type(self):
        negative = re.compile(r"(don't|dont|do not) (really )?(want|like|follow|wanna|care)|i hate")
        return negative.search(self.input_text) or (self.lexical_returnnlp_ans_neg_last_segment and not self.topic_intent_returnnlp)

    @property
    def req_type(self):
        pat = re.compile(r"politics|politician|politic|political|election")
        if pat.search(self.input_text) and not self.negative_resp_news_type:
            return "Politics"

        pat = re.compile(r"tech|technology")
        if pat.search(self.input_text) and not self.negative_resp_news_type:
            return "Technology"

        pat = re.compile(r"entertainment|entertain")
        if pat.search(self.input_text) and not self.negative_resp_news_type:
            return "Entertainment"

        pat = re.compile(r"sport|football|baseball|soccer|tennis|athletic|swimming|basketball")
        if pat.search(self.input_text) and not self.negative_resp_news_type:
            return "Sports"

        pat = re.compile(r"business|finance|stocks|stock market")
        if pat.search(self.input_text) and not self.negative_resp_news_type:
            return "Business"

        else:
            return "Other"

    @property
    def get_sport_name(self):
        pat = re.compile(r"football|nfl")
        if pat.search(self.input_text):
            return "football"

        pat = re.compile(r"baseball|mlb")
        if pat.search(self.input_text):
            return "baseball"

        pat = re.compile(r"basketball|nba|basket|jump ball")
        if pat.search(self.input_text):
            return "basketball"

        pat = re.compile(r"hockey|nhl")
        if pat.search(self.input_text):
            return "hockey"

        pat = re.compile(r"soccer|premier league|la liga|mls|major league soccer")
        if pat.search(self.input_text):
            return "soccer"

        pat = re.compile(r"tennis|wimbledon")
        if pat.search(self.input_text):
            return "tennis"

        else:
            return "Other"

    @property
    def sentiment(self):
        try:
            if self.input_data['features']['sentiment']:
                return self.input_data['features']['sentiment']
        except (KeyError, TypeError):
            return []

    @property
    def negative_ans(self):
        return self.returnnlp.answer_positivity == PositivityEnum.neg

    @property
    def positive_ans(self):
        return self.returnnlp.answer_positivity == PositivityEnum.pos

    @property
    def sentiment_neu(self):
        try:
            for segment in self.returnnlp:
                if segment.sentiment.neu > HIGH_THRESHOLD:
                    return True
            return False
        except:
            return False

    @property
    def command(self):
        return self.central_elem.has_dialog_act(DialogActEnum.COMMANDS)

    @property
    def appreciation(self):
        return self.central_elem.has_dialog_act(DialogActEnum.APPRECIATION)

    @property
    def dialog_act_ans_neg(self):
        return self.returnnlp.has_dialog_act(DialogActEnum.NEG_ANSWER)

    @property
    def dialog_act_ans_pos(self):
        return self.returnnlp.has_dialog_act(DialogActEnum.POS_ANSWER)

    @property
    def get_usr_name(self):
        return self.user_profile.username

    @property
    def get_previous_topic(self):
        res = ""
        if self.check_previous_topic_exists:
            if self.cm.newschat['previous_topic'][0] == "NER":
                res = self.cm.newschat['previous_topic'][1]
            elif self.cm.newschat['previous_topic'][0] == "Interesting_News":
                res = "Interesting_News"
            elif self.cm.newschat['previous_topic'][0] == "Type_News":
                res = self.cm.newschat['previous_topic'][1]
        return res

    @property
    def interesting_news(self) -> bool:
        try:
            pat = re.compile(
                r"(interesting|happy|uplifting)")
            pat1 = re.compile(
                r"(news|things)")
            if pat.search(self.input_text) and pat1.search(self.input_text):
                return True
            return False
        except Exception as e:
            logging.error(
                "[NEWSMODULE][ERROR] interesting_news doesn't give proper return value: {}".format(e), exc_info=True)

    @property
    def some_other_news(self):
        try:
            pat = re.compile(r"something else|some other news|skip|next (one|news)")
            if pat.search(self.input_text):
                return True
            return False
        except Exception as e:
            logging.error(
                "[NEWSMODULE][ERROR] some_other_news doesn't give proper return value: {}".format(e), exc_info=True)

    @property
    def has_exit_keyword(self):
        try:
            pat = re.compile(
                r"exit|stop talking|shut up|go away|change (the )?subject|forget( about)? news")
            if pat.search(self.input_text):
                return True
            return False
        except Exception as e:
            logging.error(
                "[NEWSMODULE][ERROR] has_exit_keyword doesn't give proper return value: {}".format(e), exc_info=True)

    @property
    def any_news(self):
        try:
            pat = re.compile(
                r"anything|anyone|whatever|dont care|don't care|don't know|dont know|yes")
            if pat.search(self.input_text):
                return True
            return False
        except Exception as e:
            logging.error(
                "[NEWSMODULE][ERROR] any_news doesn't give proper return value: {}".format(e), exc_info=True)

    @property
    def dont_care_news(self):
        try:
            pat = re.compile(
                r"(don't|dont)\s*(actually|really|wanna|want to)?\s*(care|know|like|talk|chat|hear)|"
                r"(tired|bored) .*(talking|hearing|chatting|listening) about")
            if pat.search(self.input_text):
                return True
            return False
        except Exception as e:
            logging.error(
                "[NEWSMODULE][ERROR] any_news doesn't give proper return value: {}".format(e), exc_info=True)

    @property
    def req_undetected_ner(self) -> bool:
        try:
            pat = re.compile(r"news about|news regarding|news mention")
            pat1 = re.compile(r"(talk|chat|discuss|hear) (about|some)")
            pat2 = re.compile(r"news|(latest|current|recent) (news|events?)|that$|\bit\b|this$|them$")
            if (pat.search(self.input_text) or (pat1.search(self.input_text) and not pat2.search(self.input_text))) and not self.dont_care_news:
                return True
            return False
        except Exception as e:
            logging.error(
                "[NEWSMODULE][ERROR] req_undetected_ner doesn't give proper return value: {}".format(e), exc_info=True)

    @property
    def opinion_question_on_coronavirus(self):
        try:
            pat = re.compile(
                r"what .*(think|feel) .*(corona|virus|pandemic|covid|ninteen)|what.* you (think|feel)$|(what|how) about (that|it)")
            if pat.search(self.input_text):
                return True
            return False
        except Exception as e:
            logging.error(
                "[NEWSMODULE][ERROR] req_undetected_ner doesn't give proper return value: {}".format(e), exc_info=True)

    @property
    def lexical_ans_pos(self) -> bool:
        try:
            pat = re.compile(
                r"why not|why sure|why yes|go on|keep going|continue|tell me more|keep going|(read|say) it")
            if pat.search(self.input_text):
                return True
            return False
        except Exception as e:
            logging.error(
                "[NEWSMODULE][ERROR] req_undetected_ner doesn't give proper return value: {}".format(e), exc_info=True)

    @property
    def lexical_returnnlp_ans_neg(self):
        for segment in self.returnnlp:
            if 'ans_negative' in segment.intent.lexical:
                return True
        return False

    @property
    def lexical_returnnlp_ans_neg_last_segment(self):
        if 'ans_neg' in self.returnnlp[-1].intent.lexical:
            return True
        return False

    @property
    def lexical_returnnlp_ans_pos(self):
        for segment in self.returnnlp:
            if 'ans_positive' in segment.intent.lexical:
                return True
        return False

    @property
    def topic_intent_returnnlp(self):
        for segment in self.returnnlp:
            if 'topic_newspolitics' in segment.intent.topic:
                return True
        return False

    @property
    def check_previous_topic_exists(self) -> bool:
        return len(self.cm.newschat['previous_topic']) > 0

    def get_ner_list(self, concept_confidence_thred=0.5, google_confidence_thred=300):
        """
        Hybrid NER detection system with multiple mechanism, developed by Mingyang Zhou
        """
        try:
            ner_list = []
            noun_phrase_all = self.input_data['features']['nounphrase2']['noun_phrase'] \
                if self.input_data['features']['nounphrase2']['noun_phrase'] else []
            noun_phrase = []
            if len(noun_phrase_all) > 0:
                index = 0
                while index < len(noun_phrase_all):
                    noun_phrase.extend(noun_phrase_all[index])
                    index += 1

            google_kg = self.input_data['features']['googlekg'][0] if self.input_data['features']['googlekg'] else []
            concept = self.input_data['features']['concept']
            self.cm.newschat['user_ner'] = None

            # check suggest_keywords from other modules
            if self.module_selection.get_top_propose_keyword() and self.module_selection.get_top_propose_keyword().text:
                ner_list.append(self.module_selection.get_top_propose_keyword().text)

            # Check for keyword in frequent word list and add to list of NERs
            user_response = self.input_text
            user_response_split = user_response.split()
            frequent_keywords = self.cm.newschat['supported_keyword_dict']
            frequent_keywords.update(CORONAVIRUS_MAPPING_DICT)
            if self.cm.newschat['next_ner']:
                ner_list.append(self.cm.newschat['next_ner'][0])
                if len(self.cm.newschat['next_ner']) > 1:
                    self.cm.newschat['next_ner'] = self.cm.newschat['next_ner'][1:]
                else:
                    self.cm.newschat['next_ner'] = None
            for dict_key in frequent_keywords:
                if "the" in dict_key:
                    dict_key_for_fuzz = dict_key.split()[-1]
                else:
                    dict_key_for_fuzz = dict_key
                for word in user_response_split:
                    if fuzz.token_set_ratio(word, dict_key_for_fuzz) > 90 and not self.dont_care_news:
                        if frequent_keywords[dict_key] == "coronavirus":
                            ner_list.append(frequent_keywords[dict_key])
                            self.cm.newschat['user_ner'] = frequent_keywords[dict_key]
                        else:
                            ner_list.append(frequent_keywords[dict_key])
                            self.cm.newschat['user_ner'] = dict_key

            for word in user_response_split:
                for ner_word in NEWS_MANUAL_NER_LIST:
                    if fuzz.token_set_ratio(word, ner_word) > 90 and not self.dont_care_news:
                        ner_list.append(ner_word)

            if self.input_data['features']['ner']:
                for x in self.input_data['features']['ner']:
                    if x['text'].lower() in noun_phrase \
                            and x['text'].lower() not in NER_IGNORE_LIST \
                            and not self.dont_care_news\
                            and x["label"] not in IGNORED_NER_LABELS:
                        ner_list.append(x['text'].lower())

            if concept:
                for c in concept:
                    if c['data']:
                        if any([(x in c['data'] and float(c['data'].get(x, '0')) > concept_confidence_thred) for x in NER_CONCEPT_KEY]):
                            if c['noun'].lower() not in NER_IGNORE_LIST:
                                ner_list.append(c['noun'].lower())

            if google_kg:
                for g in google_kg:
                    if g[1].lower() in NER_GOOGLEKG_KEY and float(g[2]) > google_confidence_thred and g[0].lower() not in NER_IGNORE_LIST:
                        ner_list.append(g[0].lower())

            ner_list = list(set(ner_list))
            logging.debug("[NEWS] Inside get_ner_list ner_list = {}".format(ner_list))
            return ner_list

        except (KeyError, TypeError):
            return []

    def get_other_topic(self):
        """
        Return the other topic detected by system while no news intent is detected
        """
        detect_news_topic = False
        all_sys_detected_topic = []
        input_text = self.input_data['text']

        # Add the topic detected by regex
        for x in self.input_data['returnnlp']:
            for y in x['intent'].get('topic', []):
                if y in SYS_OTHER_TOPIC_LIST:
                    all_sys_detected_topic.append(y)
            # Added the topic detected by amazon classifier
            if x.get('topic_class', None):
                all_sys_detected_topic.append(x.get('topic_class'))

        # Add the topic detected by topic_list
        # for z in sys_info['central_elem']['module']:
        #     all_sys_detected_topic.append(z)

        logging.info("[NEWS_MODULE] all system detecetd_topics are: {}".format(
            all_sys_detected_topic))

        # If news topic is detected, it is expected that we still stay with
        # topic_food
        if 'topic_news' in all_sys_detected_topic or "NEWS" in all_sys_detected_topic or \
                re.search(r"\bnews\b|\bread\b", input_text):
            detect_news_topic = True

        if not detect_news_topic:
            # check if the sys_intent list contains other topics
            for topic in all_sys_detected_topic:
                if TOPIC_KEYWORD_MAP.get(topic, None) is not None:
                    return TOPIC_KEYWORD_MAP[topic]

            # check if the sys_topic topic module contains other topics
            for module in all_sys_detected_topic:
                if MODULE_KEYWORD_MAP.get(module, None) is not None:
                    return MODULE_KEYWORD_MAP.get(module, None)

        return None

    def preprocess_ner(self, ner):
        ner = ner.lower()
        pat = re.compile(
            r"'s")
        if pat.search(self.input_text):
            ner = re.sub("'s", '', ner)
        if ner[-1] == ' ':
            ner = ner[:-1]
        return ner

    @property
    def not_chit_chat_ner(self):
        ners = self.ners
        for ner in ners:
            if ner in CHIT_CHAT_NER_IGNORE:
                return False
        return True

    def get_news_from_entity(self, ner_list):
        try:
            ner = []
            utt = ""
            url = ""
            for ner_data in ner_list:
                ner_preprocessed = self.preprocess_ner(ner_data)
                ner.append(ner_preprocessed)
            headline_entity_url = headline_mention(ner)
            if headline_entity_url is not None:
                for key in headline_entity_url.keys():
                    url_list = headline_entity_url[key]
                    if not url_list:
                        continue
                    for j in url_list[::-1]:
                        if 'used_urls' in self.cm.newschat:
                            if j not in self.cm.newschat['used_urls']:
                                url = j
                                break
                        else:
                            url = url_list[-1]
                            break
                    if url:
                        response = news_by_url(url)
                        utt = response["Item"]["Headline"]
                        break

            # if no news found in news_headline_entity, find in news_entity table
            if not url:
                article_entity_url = article_mention(ner)
                if article_entity_url is not None:
                    for key in article_entity_url.keys():
                        url_list = article_entity_url[key]
                        if not url_list:
                            continue
                        for j in url_list[::-1]:
                            if 'used_urls' in self.cm.newschat:
                                if j not in self.cm.newschat['used_urls']:
                                    url = j
                                    break
                            else:
                                url = url_list[-1]
                                break
                        if url:
                            response = news_by_url(url)
                            utt = response["Item"]["Headline"]
                            break
            if not url:
                utt = ""
            return [utt, url]
        except Exception as e:
            logging.error(
                "[NEWSMODULE][ERROR] get_news_from_entity doesn't give proper response: {}".format(e), exc_info=True)
            utt = ""
            url = ""
            return [utt, url]

    @staticmethod
    def add_prosody(utterance) -> str:
        res = ""
        if utterance is None:
            return res
        if utterance[-1] is not ".":
            utterance = utterance + "."
        res = "<prosody rate='95%'> {} </prosody>".format(utterance)
        return res

    def clear_previous_topic(self):
        self.cm.newschat['previous_topic'] = []

    def get_repeat_as_question_utt(self):
        user_utt = self.input_text
        resp = self.process_user_utt(user_utt)
        return resp

    def process_user_utt(self, user_utt) -> str:
        replacement_rules = [(r"\bare you\b", "am i"), (r"\bi was\b", "you were"), (r"\bi am\b", "you are"),
                             (r"\bwere you\b", "was i"),
                             (r"\bi\b", "you"), (r"\bmy\b",
                                                 "your"), (r"\bmyself\b", "yourself"),
                             (r"\byou\b", "i"), (r"\byour\b", "my"),
                             (r"\bme\b", "you")]

        paraphrase_response = user_utt
        # Replace a couple of things
        for rule in replacement_rules:
            paraphrase_response = re.sub(rule[0], rule[1], paraphrase_response)
        return paraphrase_response + "? "

    def get_next_chit_chat_corona(self):
        if len(self.cm.newschat['chit_chat_last_dialog']) > 0:
            prev_chit_chat_state = self.cm.newschat['chit_chat_last_dialog'][-1]
            next_state = States.chit_chat
            utt = ""
            utt_ack = self.acknowledgement.generate_chit_chat_acknowledgement()
            if prev_chit_chat_state == "intro":
                if self.dialog_act_ans_pos or self.lexical_returnnlp_ans_pos:
                    utt = self.template_news.speak("chit_chat/coronavirus/chat_1_1", {})
                    self.cm.newschat['chit_chat_last_dialog'].append("chat_1_1")
                else:
                    utt = self.template_news.speak("chit_chat/coronavirus/chat_1_2", {})
                    self.cm.newschat['chit_chat_last_dialog'].append("chat_1_2")
            elif prev_chit_chat_state == "chat_1_1":
                if self.dialog_act_ans_pos or self.lexical_returnnlp_ans_pos or self.opinion_question_on_coronavirus:
                    utt = self.template_news.speak("chit_chat/coronavirus/chat_1_1_p", {})
                    self.cm.newschat['chit_chat_last_dialog'].append("chat_1_1_p")
                else:
                    utt = self.template_news.speak("chit_chat/coronavirus/chat_1_2", {})
                    self.cm.newschat['chit_chat_last_dialog'].append("chat_1_2")
            elif prev_chit_chat_state == "chat_1_1_p":
                utt = self.template_news.speak("chit_chat/coronavirus/chat_1_2", {})
                self.cm.newschat['chit_chat_last_dialog'].append("chat_1_2")
            elif prev_chit_chat_state == "chat_1_2":
                if self.dialog_act_ans_pos or self.lexical_returnnlp_ans_pos:
                    utt = self.template_news.speak("chit_chat/coronavirus/chat_1_2_p", {})
                    self.cm.newschat['chit_chat_last_dialog'].append("chat_1_2_p")
                else:
                    utt = self.template_news.speak("chit_chat/coronavirus/chat_1_2_n", {})
                    self.cm.newschat['chit_chat_last_dialog'].append("chat_1_2_n")
            elif prev_chit_chat_state == "chat_1_2_p" or prev_chit_chat_state == "chat_1_2_n":
                utt = self.template_news.speak("chit_chat/coronavirus/chat_1_1_n", {})
                self.cm.newschat['chit_chat_last_dialog'].append("chat_1_1_n")
                next_state = States.intro
                self.cm.newschat['next_ner'] = ["coronavirus vaccine test"]
            else:
                utt = self.template_news.speak("general/propose_topic", {})
                next_state = States.intro
                self.cm.newschat['next_ner'] = ["coronavirus vaccine test"]
            utt = " ".join([utt_ack, utt])
            return utt, next_state
        else:
            return "I am sorry I lost my train of thought", States.done

    def get_next_chit_chat(self):
        if len(self.cm.newschat['chit_chat_last_dialog']) > 0:

            prev_chit_chat_state = self.cm.newschat[
                'chit_chat_last_dialog'][-1]
            next_state = States.chit_chat
            utt = ""
            utt_ack = self.acknowledgement.generate_chit_chat_acknowledgement()
            if prev_chit_chat_state == "intro":
                if self.dialog_act_ans_pos or self.lexical_returnnlp_ans_pos:
                    utt = self.template_news.speak("chit_chat/trump_impeachment/chat_1_1", {})
                    self.cm.newschat['chit_chat_last_dialog'].append("chat_1_1")
                else:
                    utt = self.template_news.speak("chit_chat/trump_impeachment/chat_2_1", {})
                    self.cm.newschat['chit_chat_last_dialog'].append("chat_2_1")

            if prev_chit_chat_state == "chat_1_1" or prev_chit_chat_state == "chat_2_2":
                if self.dialog_act_ans_pos or self.lexical_returnnlp_ans_pos:
                    utt = self.template_news.speak("chit_chat/trump_impeachment/chat_1_1_p", {})
                    self.cm.newschat['chit_chat_last_dialog'].append("chat_1_1_p")
                    utt = " ".join([utt_ack, utt])
                    return utt, next_state
                else:
                    utt = self.template_news.speak("chit_chat/trump_impeachment/chat_1_1_n", {})
                    self.cm.newschat['chit_chat_last_dialog'].append("chat_1_1_n")
                    utt = " ".join([utt_ack, utt])
                    return utt, next_state

            if prev_chit_chat_state == "chat_1_1_p" or prev_chit_chat_state == "chat_1_1_n":
                utt = self.template_news.speak("chit_chat/trump_impeachment/chat_1_2", {})
                self.cm.newschat['chit_chat_last_dialog'].append("chat_1_2")

            if prev_chit_chat_state == "chat_1_2":
                if self.dialog_act_ans_pos or self.lexical_returnnlp_ans_pos:
                    utt = self.template_news.speak("chit_chat/trump_impeachment/chat_1_2_p", {})
                    self.cm.newschat['chit_chat_last_dialog'].append("chat_1_2_p")
                else:
                    utt = self.template_news.speak("chit_chat/trump_impeachment/chat_1_2_n", {})
                    self.cm.newschat['chit_chat_last_dialog'].append("chat_1_2_n")
                next_state = States.headlines

            if prev_chit_chat_state == "chat_2_1":
                if self.dialog_act_ans_pos or self.lexical_returnnlp_ans_pos:
                    utt = self.template_news.speak("chit_chat/trump_impeachment/chat_2_1_p", {})
                    self.cm.newschat['chit_chat_last_dialog'].append("chat_2_1_p")
                else:
                    utt = self.template_news.speak("chit_chat/trump_impeachment/chat_2_1_n", {})
                    self.cm.newschat['chit_chat_last_dialog'].append("chat_2_1_n")
                    next_state = States.headlines

            if prev_chit_chat_state == "chat_2_1_p" or prev_chit_chat_state == "chat_2_1_n":
                utt = self.template_news.speak("chit_chat/trump_impeachment/chat_2_2", {})
                utt = " ".join([utt_ack, utt])
                self.cm.newschat['chit_chat_last_dialog'].append("chat_2_2")
                return utt, next_state

            if prev_chit_chat_state == "chat_1_2_p" or prev_chit_chat_state == "chat_1_2_n" or \
                    prev_chit_chat_state == "chat_2_1_p" or prev_chit_chat_state == "chat_2_1_n":
                utt = self.template_news.speak("general/propose_topic", {})
                next_state = States.othernews

            utt = " ".join([utt_ack, utt])
            return utt, next_state

        else:
            return "I am sorry I lost my train of thought", States.done

    def political_chitchat_dialogflow(self):
        # Define the transition of chitchat_state
        if not self.cm.newschat.get('current_political_chitchat_state', None) or self.cm.prev_state != States.chit_chat:
            self.cm.newschat['current_political_chitchat_state'] = 'pc_intro'
        elif self.cm.newschat['current_political_chitchat_state'] == 'pc_intro':
            # answer yes
            if self.dialog_act_ans_pos or self.lexical_returnnlp_ans_pos:
                self.cm.newschat['current_political_chitchat_state'] = 'pc_pos'
            elif self.dialog_act_ans_neg or self.lexical_returnnlp_ans_neg:
                self.cm.newschat['current_political_chitchat_state'] = 'pc_neg'
            else:
                self.cm.newschat['current_political_chitchat_state'] = 'pc_neu'
        elif self.cm.newschat['current_political_chitchat_state'] in ['pc_pos', 'pc_neg']:
            self.cm.newschat['current_political_chitchat_state'] = 'pc_opponent_argue'
        else:
            logging.info("[NEWS_MODULE] Error: Does not fall into any cases of political chitchat.")

        current_topic = self.cm.newschat['current_political_chitchat_topic']
        current_topic_data = self.cm.newschat['current_political_chitchat']

        # ack is the acknowledgement
        ack = self.acknowledgement.generate_political_chitchat_acknowledgement()
        logging.info("[NEWS_MODULE] current political chitchat state: {}".format(
            self.cm.newschat['current_political_chitchat_state']))
        # Define the response of the political chitchat_state
        utt = ""
        next_state = States.chit_chat
        if self.cm.newschat['current_political_chitchat_state'] == 'pc_intro':
            utt = self.template_news.speak("political_chitchat/chitchat_topic_question", {
                                           "topic": current_topic, "topic_question": current_topic_data["topic_question"]})
            self.cm.newschat['prev_political_chitchat_state'] = 'pc_intro'
        elif self.cm.newschat['current_political_chitchat_state'] == 'pc_pos':
            # Introduce the response regarding the opinion by answering yes
            agree_percent = self.cm.newschat[
                'current_political_chitchat']['yes_percent']
            if int(agree_percent) < 40:
                percent_analysis = self.template_news.speak(
                    "political_chitchat/chitchat_less_percent", {"percent_num": agree_percent})
            elif int(agree_percent) > 60:
                percent_analysis = self.template_news.speak(
                    "political_chitchat/chitchat_more_percent", {"percent_num": agree_percent})
            else:
                percent_analysis = self.template_news.speak(
                    "political_chitchat/chitchat_balanced_percent", {"percent_num": agree_percent})
            support_opinion = random.choice(self.cm.newschat['current_political_chitchat']['yes_summarized_opinions'])
            utt = self.template_news.speak("political_chitchat/chitchat_feedback2opinion_np", {
                                           "percentage_analysis": percent_analysis, "support_opinion": support_opinion})

            self.cm.newschat['prev_political_chitchat_state'] = 'pc_pos'
        elif self.cm.newschat['current_political_chitchat_state'] == 'pc_neg':
            # Introduce the response regarding the opinion by answering yes
            agree_percent = self.cm.newschat['current_political_chitchat']['no_percent']
            if int(agree_percent) < 40:
                percent_analysis = self.template_news.speak(
                    "political_chitchat/chitchat_less_percent", {"percent_num": agree_percent})
            elif int(agree_percent) > 60:
                percent_analysis = self.template_news.speak(
                    "political_chitchat/chitchat_more_percent", {"percent_num": agree_percent})
            else:
                percent_analysis = self.template_news.speak(
                    "political_chitchat/chitchat_balanced_percent", {"percent_num": agree_percent})

            support_opinion = random.choice(self.cm.newschat['current_political_chitchat']['no_summarized_opinions'])
            utt = self.template_news.speak("political_chitchat/chitchat_feedback2opinion_np", {
                                           "percentage_analysis": percent_analysis, "support_opinion": support_opinion})

            self.cm.newschat['prev_political_chitchat_state'] = 'pc_neg'

        elif self.cm.newschat['current_political_chitchat_state'] == 'pc_neu':
            yes_percent = self.cm.newschat['current_political_chitchat']['yes_percent']
            no_percent = self.cm.newschat['current_political_chitchat']['no_percent']
            yes_opinion = random.choice(self.cm.newschat['current_political_chitchat']['yes_summarized_opinions'])
            no_opinion = random.choice(self.cm.newschat['current_political_chitchat']['no_summarized_opinions'])

            utt = self.template_news.speak("political_chitchat/chitchat_feedback2opinion_neutral", {
                                           "pos_percentage": yes_percent, "neg_percentage": no_percent, "pos_opinion": yes_opinion, "neg_opinion": no_opinion})
            next_state = States.intro
            self.cm.newschat['prev_political_chitchat_state'] = 'pc_neu'
        elif self.cm.newschat['current_political_chitchat_state'] == 'pc_opponent_argue':
            if self.cm.newschat['prev_political_chitchat_state'] == "pc_pos":
                opponent_opinion = random.choice(
                    self.cm.newschat['current_political_chitchat']['no_summarized_opinions'])
            else:
                opponent_opinion = random.choice(self.cm.newschat['current_political_chitchat'][
                                                 'yes_summarized_opinions'])

            utt = self.template_news.speak(
                "political_chitchat/chitchat_opponent_opinion", {"opponent_opinion": opponent_opinion})
            next_state = States.intro
            self.cm.newschat['prev_political_chitchat_state'] = 'pc_opponent_argue'

        utt = ' '.join([ack, utt])

        return utt, next_state

    def sys_ack_text(self):
        if len(self.sys_acknowledgement.get("ack", "")) > 0:
            if self.sys_acknowledgement.get("output_tag") != "ack_question_idk":
                return self.sys_acknowledgement.get("ack", None)
        return None

    def can_map_to_unused_chit_chat(self):
        mapped_chitchat = can_map_to_chit_chat(self.input_text)
        if mapped_chitchat and mapped_chitchat not in self.cm.newschat['used_chit_chat'] and not self.dont_care_news:
            return mapped_chitchat
        return None

    def ner_can_map_to_chitchat(self):
        for ner in self.ners:
            mapped_chitchat = can_map_to_chit_chat(ner)
            if mapped_chitchat and mapped_chitchat not in self.cm.newschat['used_chit_chat']:
                return mapped_chitchat
            return None

    def backstory_supported_question_on_corona(self):
        if self.is_question_about_corona():
            answer = self.intents.handle_news_question()
            if answer and answer.response:
                return True
        return False

    def is_question_about_corona(self):
        if self.intents.ask_question:
            for dict_key in CORONAVIRUS_MAPPING_DICT:
                if dict_key in self.input_text:
                    return True
        return False

    def match_command_to_state(self):
        if self.req_type is not "Other":
            return States.headlines
        elif self.negative_ans and self.cm.prev_state == States.chit_chat:
            if self.ners:
                self.ners.pop()
            return States.headlines
        elif self.can_map_to_unused_chit_chat() is not None:
            return States.intro_chit_chat
        elif self.ners:
            return States.headlines
        elif self.interesting_news:
            return States.headlines
        elif self.req_undetected_ner:
            return States.headlines
        elif self.cm.prev_state is None:
            return States.intro
        else:
            return States.command_not_handled


    """
    States are used to create the utterance
    Transitions determine what the next state is

    Each state will specify what the next intended state is. e.g. intro -> interesting_comment,
    however, user has the ability to interrupt that by saying e.g. no, or the system can interrupt the flow.
    So the state transition always lands on the transition state for the target state,
    e.g. s_intro -> t_interesting_comment,
    and the transition state is responsible for redirecting state transitions.
    State itself is only responsible for generating the utterance
    """

    """
    State 1
    """

    @_Decorators.transition
    def t_intro_chit_chat(self):
        logging.debug('[NEWS][STATE][t_intro_chit_chat]')
        if self.intents.nointerest_news or self.other_topic is not None:
            return States.exit_news
        elif self.backstory_supported_question_on_corona() and not self.enter_news and not self.opinion_question_on_coronavirus:
            return States.question_answer
        elif self.can_map_to_unused_chit_chat() is not None or self.ner_can_map_to_chitchat():
            return States.intro_chit_chat
        elif self.intents.ask_question and not self.enter_news and self.req_type is "Other":
            # if self.intents.iscommand:
            #     state = self.match_command_to_state()
            #     return state
            return States.question_answer
        elif self.dont_care_news:
            return States.intro
        elif self.ners:
            return States.headlines
        elif self.req_type is not "Other":
            return States.headlines
        elif self.interesting_news:
            return States.headlines
        elif self.req_undetected_ner:
            return States.headlines
        else:
            return States.intro_chit_chat

    @_Decorators.state
    def s_intro_chit_chat(self, params: dict):
        logging.debug('[NEWS][STATE][s_intro_chit_chat]')
        chitchat_topic = self.can_map_to_unused_chit_chat()
        if chitchat_topic is None:
            chitchat_topic = "coronavirus"
        utt_ack = ""
        self.cm.newschat['used_chit_chat'].append(chitchat_topic)
        self.cm.newschat['current_political_chitchat_state'] = None
        self.cm.newschat['prev_political_chitchat_state'] = None
        if chitchat_topic == "coronavirus":
            utt = self.template_news.speak("chit_chat/coronavirus/intro", {})
            self.cm.newschat['chit_chat_last_dialog'].append("intro")
        elif chitchat_topic == "donald trump":
            utt = self.template_news.speak("chit_chat/trump_impeachment/intro", {})
            self.cm.newschat['chit_chat_last_dialog'].append("intro")
        else:
            # Using the political intro as chitchat
            self.cm.newschat['current_political_chitchat_topic'] = chitchat_topic
            self.cm.newschat['current_political_chitchat'] = self.political_chitchat[self.cm.newschat['current_political_chitchat_topic']]
            utt = self.template_news.speak("political_chitchat/chitchat_intro_temp", {"topic": self.cm.newschat['current_political_chitchat_topic']})
        self.cm.newschat['used_chit_chat'].append(chitchat_topic)
        utt = " ".join([utt_ack, utt])
        return utt, States.chit_chat, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @_Decorators.transition
    def t_chit_chat(self):
        logging.debug('[NEWS][STATE][t_chit_chat]')
        if self.intents.nointerest_news or self.other_topic is not None:
            return States.exit_news
        elif self.intents.ask_question and not self.enter_news and not self.opinion_question_on_coronavirus:
            # if self.intents.iscommand:
            #     state = self.match_command_to_state()
            #     return state
            return States.question_answer
        elif self.can_map_to_unused_chit_chat() is not None:
            return States.intro_chit_chat
        elif self.dont_care_news:
            return States.intro
        elif self.ners:
            return States.headlines
        elif self.req_type is not "Other":
            return States.headlines
        elif self.interesting_news:
            return States.headlines
        elif self.req_undetected_ner:
            return States.headlines
        elif (self.dialog_act_ans_neg or self.lexical_returnnlp_ans_neg) and self.cm.newschat['prev_state'] == States.intro_chit_chat.value:
            return States.intro
        else:
            return States.chit_chat

    @_Decorators.state
    def s_chit_chat(self, params: dict):
        logging.debug('[NEWS][STATE][s_chit_chat]')
        chitchat_topic = self.cm.newschat['used_chit_chat'][-1]
        if chitchat_topic == "coronavirus":
            utt, next_state = self.get_next_chit_chat_corona()
        elif chitchat_topic == "donald trump":
            utt, next_state = self.get_next_chit_chat()
        else:
            utt, next_state = self.political_chitchat_dialogflow()
        return utt, next_state, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @_Decorators.transition
    def t_intro(self):
        logging.debug('[NEWS][STATE][t_intro]')
        if self.intents.nointerest_news or self.other_topic is not None:
            return States.exit_news
        elif self.lexical_ans_pos:
            return States.intro
        elif self.intents.ask_question and not self.enter_news:
            # if self.intents.iscommand:
            #     state = self.match_command_to_state()
            #     return state
            return States.question_answer
        elif self.can_map_to_unused_chit_chat() is not None:
            return States.intro_chit_chat
        elif self.dont_care_news:
            return States.headlines
        elif self.negative_ans:
            return States.intro
        elif self.ners:
            return States.headlines
        elif self.req_type is not "Other":
            return States.headlines
        elif self.interesting_news:
            return States.headlines
        elif self.req_undetected_ner:
            return States.headlines
        else:
            return States.intro

    @_Decorators.state
    def s_intro(self, params: dict):
        logging.debug('[NEWS][STATE][s_intro]')
        utt_ack = self.acknowledgement.generate_general_acknowledgement()
        utt_topic = self.template_news.speak("general/propose_topic", {})
        utt = " ".join([utt_ack, utt_topic])
        return utt, States.headlines, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @_Decorators.transition
    def t_headlines(self):
        logging.debug('[NEWS][STATE][t_headlines]')
        if self.intents.nointerest_news or self.other_topic is not None:
            return States.exit_news
        elif self.has_exit_keyword:
            return States.done
        elif self.negative_ans and (self.cm.prev_state == States.othernews_moments or self.cm.prev_state == States.headlines):
            return States.done
        elif self.cm.newschat['uplifting_news_count'] > 2 and not self.ners and self.req_type == "Other":
            return States.done
        elif self.negative_ans and self.cm.prev_state == States.question_answer and self.cm.newschat['uplifting_news_count'] > 0:
            return States.done
        elif (self.negative_ans and (self.cm.prev_state == States.chit_chat or self.cm.prev_state == States.question_answer)) or self.dont_care_news:
            if self.ners:
                self.ners.pop()
            self.clear_previous_topic()
            return States.headlines
        elif self.some_other_news:
            return States.othernews
        elif self.can_map_to_unused_chit_chat() is not None:
            return States.intro_chit_chat
        elif self.positive_ans:
            return States.headlines
        elif self.intents.ask_question and not self.enter_news:
            return States.question_answer
        elif self.ners:
            return States.headlines
        elif self.req_type is not "Other":
            return States.headlines
        elif self.negative_ans or self.detect_negative_neutral_response:
            return States.othernews
        elif self.interesting_news:
            return States.headlines
        elif self.req_undetected_ner:
            return States.headlines
        elif self.get_sport_name is not "Other":
            return States.sport_type_title
        self.clear_previous_topic()
        return States.headlines

    @_Decorators.state
    def s_headlines(self, params: dict):
        logging.debug('[NEWS][STATE][s_headlines]')
        # initialize the count of presentation
        self.cm.newschat['curr_news_turns'] = 0
        update_requested_ner_or_type = False
        utt_ack = ""
        if self.sys_ack_text():
            utt_ack = self.sys_ack_text()
            if 'question' in self.sys_acknowledgement.get('output_tag', {}):
                utt_ack = ""
        if utt_ack == "":
            if self.cm.prev_state is not States.command_not_handled:
                utt_ack = self.template_news.speak("general/acknowledgement_headline", {})
        if self.req_type == "Business":
            utt_ack += self.template_news.speak("general/acknowledgement_business_news", {})

        if self.req_type == "Business":
            utt = self.template_news.speak("general/more_news_interesting", {})
            utt = " ".join([utt_ack, utt])
            self.cm.newschat['previous_topic'] = ["Interesting_News", "Technology"]
            return utt, States.othernews, ProposeContinue.CONTINUE, ProposeTopic.NONE

        if self.ners or self.interesting_news or self.req_type is not "Other" or self.req_undetected_ner:
            update_requested_ner_or_type = True

        try:
            if self.req_type is not "Other" or (self.check_previous_topic_exists and
                                                self.cm.newschat['previous_topic'][0] == "Type_News" and
                                                not update_requested_ner_or_type):
                type_news = self.req_type
                if self.req_type is "Other" and self.check_previous_topic_exists and \
                        self.cm.newschat['previous_topic'][0] == "Type_News":
                    type_news = self.cm.newschat['previous_topic'][1]
                if "Sports" in type_news:
                    utt = self.template_news.speak("general/propose_sport_type", {})
                    utt = " ".join([utt_ack, utt])
                    return utt, States.sport_type_title, ProposeContinue.CONTINUE, ProposeTopic.NONE

                news = get_news_tm_tags(MOMENT_TAG_MAP[type_news], self.cm.newschat['used_titles'])
                if news:
                    utt_transit = self.template_news.speak("general/transit_type_of_news_tm", {})
                    utt = news['title']
                    utt_ques = self.template_news.speak("general/question_read_news", {})
                    utt = "".join([utt_ack, utt_transit, utt, utt_ques])
                    if 'used_titles' not in self.cm.newschat:
                        self.cm.newschat['used_titles'] = news['title']
                    else:
                        self.cm.newschat['used_titles'].append(news['title'])
                    self.cm.newschat['last_title_desc'] = news['desc']
                    self.cm.newschat['previous_topic'] = ["Type_News", type_news]
                    return utt, States.readmore_moments, ProposeContinue.CONTINUE, ProposeTopic.NONE

                else:
                    utt_ack = "I can't recall any news about that sport."
                    transit_utt = self.template_news.speak("general/start_news_interesting", {})
                    self.cm.newschat['previous_topic'] = ["Interesting_News", "Technology"]
                    utt = " ".join([utt_ack, transit_utt])
                    return utt, States.othernews, ProposeContinue.CONTINUE, ProposeTopic.NONE

            elif self.ners or (self.check_previous_topic_exists and self.cm.newschat['previous_topic'][0] == "NER" and not update_requested_ner_or_type):
                logging.info("[NEWSCHAT][NER_NEWS]")
                ner_list = self.ners.copy()
                if not self.ners and self.check_previous_topic_exists and self.cm.newschat['previous_topic'][0] == "NER":
                    ner_list = [self.cm.newschat['previous_topic'][1]]
                utt_url = self.get_news_from_entity(ner_list)
                if not utt_url[0]:
                    utt = self.template_news.speak("general/news_not_found_ner", {"ner": self.preprocess_ner(ner_list[0])})
                    return utt, States.headlines, ProposeContinue.CONTINUE, ProposeTopic.NONE
                self.cm.newschat['last_url'] = utt_url[1]
                if 'used_urls' in self.cm.newschat:
                    self.cm.newschat['used_urls'].append(utt_url[1])
                else:
                    self.cm.newschat['used_urls'] = [utt_url[1]]
                if ner_list is not None:
                    self.cm.newschat['previous_topic'] = ["NER", ner_list[0]]

                if self.cm.newschat['user_ner']:
                    ner = self.cm.newschat['user_ner']
                    self.cm.newschat['previous_topic'] = ["NER", self.cm.newschat['user_ner']]
                else:
                    ner = ner_list[0]

                if self.cm.prev_state == States.question_answer:
                    utt_ack2 = ""
                else:
                    utt_ack2 = self.template_news.speak("general/transit_ner", {"ner": self.preprocess_ner(ner)})
                utt_news = self.add_prosody(utt_url[0])
                transit_utt = self.template_news.speak("general/readmore", {})
                utt = " ".join([utt_ack, utt_ack2, utt_news, transit_utt])
                return utt, States.readmore, ProposeContinue.CONTINUE, ProposeTopic.NONE

            elif self.interesting_news or (self.check_previous_topic_exists and self.cm.newschat['previous_topic'][0] == "Interesting_News" and not update_requested_ner_or_type):
                # For interesting news, give uplifting news
                if 'used_urls' in self.cm.newschat:
                    kg_return = news_of_type("Uplifting", self.cm.newschat['used_urls'], limit=1)
                else:
                    kg_return = news_of_type("Uplifting", [], limit=1)
                self.cm.newschat['previous_topic'] = ["Interesting_News", "Technology"]
                self.cm.newschat['uplifting_news_count'] += 1
                # Create utterance
                utt_ack2 = self.template_news.speak("general/transit_interesting", {})
                utt_news = self.add_prosody(kg_return[0]['Headline'])
                transit_utt = self.template_news.speak("general/readmore", {})
                utt = " ".join([utt_ack, utt_ack2, utt_news, transit_utt])
                self.cm.newschat['last_url'] = kg_return[0]['URL']
                if 'used_urls' in self.cm.newschat:
                    self.cm.newschat['used_urls'].append(kg_return[0]['URL'])
                else:
                    self.cm.newschat['used_urls'] = [kg_return[0]['URL']]
                return utt, States.readmore, ProposeContinue.CONTINUE, ProposeTopic.NONE

            elif self.req_undetected_ner:
                utt = self.template_news.speak("general/news_not_found_unknown_ner", {})
                self.cm.newschat['previous_topic'] = ["Interesting_News", "Technology"]
                return utt, States.headlines, ProposeContinue.CONTINUE, ProposeTopic.NONE

            elif self.req_type is "Other" and self.negative_resp_news_type:
                utt_ack = self.template_news.speak('general/acknowledgement_dont_like', {})
                utt_transit = self.template_news.speak("general/more_news_interesting", {})
                utt = " ".join([utt_ack, utt_transit])
                self.cm.newschat['previous_topic'] = ["Interesting_News", "Technology"]
                return utt, States.othernews, ProposeContinue.CONTINUE, ProposeTopic.NONE

            elif self.any_news:
                # Give uplifting news if no user preference
                if 'used_urls' in self.cm.newschat:
                    kg_return = news_of_type("Uplifting", self.cm.newschat['used_urls'], limit=1)
                else:
                    kg_return = news_of_type("Uplifting", [], limit=1)
                self.cm.newschat['previous_topic'] = ["Interesting_News", "Technology"]
                self.cm.newschat['uplifting_news_count'] += 1
                utt_ack2 = self.template_news.speak("general/transit_no_preference", {})
                utt_news = self.add_prosody(kg_return[0]['Headline'])
                transit_utt = self.template_news.speak("general/readmore", {})
                utt = " ".join([utt_ack, utt_ack2, utt_news, transit_utt])
                self.cm.newschat['last_url'] = kg_return[0]['URL']
                if 'used_urls' in self.cm.newschat:
                    self.cm.newschat['used_urls'].append(kg_return[0]['URL'])
                else:
                    self.cm.newschat['used_urls'] = [kg_return[0]['URL']]
                return utt, States.readmore, ProposeContinue.CONTINUE, ProposeTopic.NONE

            else:
                # can't find any news about that.
                utt_transit = self.template_news.speak("general/start_news_interesting", {})
                utt = " ".join([utt_ack, utt_transit])
                self.cm.newschat['previous_topic'] = ["Interesting_News", "Technology"]
                return utt, States.othernews, ProposeContinue.CONTINUE, ProposeTopic.NONE

        except Exception as e:
            logging.error("[NEWSMODULE][ERROR] s_headlines does properly return a response. err: {}".format(e), exc_info=True)
            utt = self.template_news.speak("general/error_unknown", {})
            return utt, States.headlines, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @_Decorators.transition
    def t_sport_type_title(self):
        logging.debug('[NEWS][STATE][t_sport_type_title]')
        if self.intents.nointerest_news or self.other_topic is not None:
            return States.exit_news
        elif self.has_exit_keyword:
            return States.done
        elif self.intents.ask_question and not self.enter_news:
            # if self.intents.iscommand:
            #     state = self.match_command_to_state()
            #     return state
            return States.question_answer
        elif self.negative_ans and self.cm.prev_state == States.othernews_moments:
            return States.done
        elif self.negative_ans or self.detect_negative_neutral_response:
            return States.othernews_moments
        elif self.dont_care_news:
            return States.handle_neg_response
        return States.sport_type_title

    @_Decorators.state
    def s_sport_type_title(self, params: dict):
        logging.debug('[NEWS][STATE][s_sport_type_title]')
        sport_name = self.get_sport_name
        if self.sys_ack_text():
            utt_ack = self.sys_ack_text()
        else:
            utt_ack = self.template_news.speak("general/acknowledgement_headline", {})

        if sport_name is not "Other" or (len(self.cm.newschat['previous_topic']) > 0 and "Sports" in self.cm.newschat['previous_topic'][1]):

            if self.check_previous_topic_exists and "Sports" in self.cm.newschat['previous_topic'][1]:
                sport_name = self.cm.newschat['previous_topic'][1].split(':')[1]

            sport_tags = MOMENT_TAG_MAP[sport_name]
            news = get_news_tm_tags(sport_tags, self.cm.newschat['used_titles'])

            self.cm.newschat["propose_keywords"] = {
                "SPORT": {
                    'SPORT_TYPE': sport_name,
                    'SPORT_ENTITIES': None
                }
            }

            if news:
                utt_transit = self.template_news.speak("general/transit_type_of_news_tm", {})
                utt = news['title']
                utt_ques = self.template_news.speak("general/question_read_news", {})
                utt = "".join([utt_ack, utt_transit, utt, utt_ques])
                if 'used_titles' not in self.cm.newschat:
                    self.cm.newschat['used_titles'] = news['title']
                else:
                    self.cm.newschat['used_titles'].append(news['title'])
                self.cm.newschat['last_title_desc'] = news['desc']
                self.cm.newschat['previous_topic'] = ["Type_News", "Sports:" + sport_name]
                return utt, States.readmore_moments, ProposeContinue.CONTINUE, ProposeTopic.NONE
            else:
                utt_ack = "I can't recall any news about that sport."
                transit_utt = self.template_news.speak("general/start_news_interesting", {})
                self.cm.newschat['previous_topic'] = ["Interesting_News", "Technology"]
                utt = " ".join([utt_ack, transit_utt])
                return utt, States.othernews, ProposeContinue.CONTINUE, ProposeTopic.NONE

        elif sport_name == "Other":
            utt_ack = "I can't recall any news about that."
            transit_utt = self.template_news.speak("general/start_news_interesting", {})
            self.cm.newschat['previous_topic'] = ["Interesting_News", "Technology"]
            utt = " ".join([utt_ack, transit_utt])
            return utt, States.othernews, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @_Decorators.transition
    def t_readmore_moments(self):
        logging.debug('[NEWS][STATE][t_readmore_moments]')
        if self.intents.nointerest_news or self.other_topic is not None:
            return States.exit_news
        elif self.dialog_act_ans_pos:
            return States.readmore_moments
        elif self.dont_care_news:
            return States.handle_neg_response
        elif self.intents.ask_question and not self.enter_news:
            # if self.intents.iscommand:
            #     state = self.match_command_to_state()
            #     return state
            return States.question_answer
        elif self.req_type is not "Other":
            self.clear_previous_topic()
            return States.headlines
        elif self.can_map_to_unused_chit_chat() is not None:
            return States.intro_chit_chat
        elif self.ners:
            self.clear_previous_topic()
            return States.headlines
        elif self.interesting_news and self.command:
            self.clear_previous_topic()
            return States.headlines
        elif self.some_other_news:
            self.clear_previous_topic()
            return States.headlines
        else:
            return States.readmore_moments

    @_Decorators.state
    def s_readmore_moments(self, params: dict):
        logging.debug('[NEWS][STATE][s_readmore_moments]')
        utt_ack_seen = ""
        if self.dialog_act_ans_pos:
            utt_ack_seen = self.template_news.speak("general/acknowledgement_seen_news", {})
        if self.sys_ack_text():
            utt_ack_seen = self.sys_ack_text()
        utt_ack = self.template_news.speak("general/well", {})
        utt = self.cm.newschat['last_title_desc']
        utt_ques = self.template_news.speak("general/thoughts", {})
        utt = " ".join([utt_ack_seen, utt_ack, utt, utt_ques])
        return utt, States.othernews_moments, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @_Decorators.transition
    def t_readmore(self):
        logging.debug('[NEWS][STATE][t_readmore]')
        if self.intents.nointerest_news or self.other_topic is not None:
            return States.exit_news
        elif self.lexical_ans_pos:
            return States.readmore
        elif self.cm.newschat['uplifting_news_count'] > 2 and not self.ners and self.req_type == "Other" and self.negative_ans:
            return States.done
        elif self.intents.ask_question and not self.enter_news:
            # if self.intents.iscommand:
            #     state = self.match_command_to_state()
            #     return state
            return States.question_answer
        elif self.can_map_to_unused_chit_chat() is not None:
            return States.intro_chit_chat
        elif self.ners:
            self.clear_previous_topic()
            return States.headlines
        elif self.req_type is not "Other":
            self.clear_previous_topic()
            return States.headlines
        elif self.interesting_news and self.command:
            self.clear_previous_topic()
            return States.headlines
        elif self.some_other_news:
            self.clear_previous_topic()
            return States.headlines
        elif not self.negative_ans and not self.dialog_act_ans_neg and not self.lexical_returnnlp_ans_neg_last_segment:
            return States.readmore
        elif self.dialog_act_ans_neg:
            return States.othernews
        elif self.negative_ans and self.cm.prev_state == States.headlines:
            return States.othernews
        elif self.negative_ans:
            return States.othernews
        elif self.req_undetected_ner:
            self.clear_previous_topic()
            return States.headlines
        elif self.has_exit_keyword:
            return States.done
        elif self.dont_care_news:
            return States.handle_neg_response
        else:
            return States.othernews

    @_Decorators.state
    def s_readmore(self, params: dict):
        logging.debug('[NEWS][STATE][s_readmore]')
        if 'last_url' in self.cm.newschat:
            if self.cm.newschat['curr_news_article'] is None or self.cm.newschat['curr_news_turns'] == 0:
                article = news_by_url(self.cm.newschat['last_url'])
                no_of_sent = len(article['Item']['Article'])
                for sent_no in range(no_of_sent):
                    if sent_no == 0:
                        self.cm.newschat['curr_news_article'] = [
                            article['Item']['Article']["Sentence{}".format(sent_no + 1)]]
                    else:
                        self.cm.newschat['curr_news_article'].append(
                            article['Item']['Article']["Sentence{}".format(sent_no + 1)])

            article = self.cm.newschat['curr_news_article']
            no_of_sent = len(article)
            curr_news_turn = self.cm.newschat['curr_news_turns']
            utt = self.template_news.speak("general/acknowledgement", {})
            utt_news = article[curr_news_turn]['Sentence']
            while len(utt_news.split()) < 25 and curr_news_turn < no_of_sent - 1:
                utt_news = " ".join(
                    [utt_news, article[curr_news_turn + 1]['Sentence']])
                curr_news_turn += 1
            utt_news = utt + self.add_prosody(utt_news)

            if curr_news_turn < no_of_sent - 1:
                if "Question_generated" in article[curr_news_turn + 1] and use_generated_question \
                        and len(article[curr_news_turn + 1]['Question_generated']) < 15:
                    transit_utt = article[
                        curr_news_turn + 1]['Question_generated']
                    utt = self.template_news.speak(
                        "qa/news_presentation_q", {"current_sentence": utt_news, "question": transit_utt})
                else:
                    transit_utt = self.template_news.speak(
                        "general/readmore", {})
                    utt = " ".join([utt_news, transit_utt])
                self.cm.newschat['curr_news_turns'] = curr_news_turn + 1
                return utt, States.readmore, ProposeContinue.CONTINUE, ProposeTopic.NONE
            else:
                transit_utt = self.template_news.speak(
                    "qa/last_question", {})
                utt = " ".join([utt_news, transit_utt])
                # Reset the curr_news_turns
                self.cm.newschat['curr_news_turns'] = 0
                # Reset the curr_news_article, when you finish presenting one
                self.cm.newschat['curr_news_article'] = None
                return utt, States.othernews, ProposeContinue.CONTINUE, ProposeTopic.NONE
        utt = self.template_news.speak("general/error_unknown", {})
        return utt, States.headlines, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @_Decorators.transition
    def t_othernews_moments(self):
        logging.debug('[NEWS][STATE][t_othernews_moments]')
        if self.intents.nointerest_news or self.other_topic is not None:
            return States.exit_news
        elif self.has_exit_keyword:
            return States.done
        elif self.intents.ask_question and not self.enter_news:
            # if self.intents.iscommand:
            #     state = self.match_command_to_state()
            #     return state
            return States.question_answer
        elif self.can_map_to_unused_chit_chat() is not None:
            return States.intro_chit_chat
        elif self.ners:
            return States.headlines
        elif self.interesting_news and self.command:
            return States.headlines
        elif self.req_type is not "Other":
            return States.headlines
        elif self.req_undetected_ner:
            return States.headlines
        elif self.dont_care_news:
            return States.handle_neg_response
        else:
            return States.othernews_moments

    @_Decorators.state
    def s_othernews_moments(self, params: dict):
        logging.debug('[NEWS][STATE][s_othernews_moments]')
        topic = self.get_previous_topic
        utt_ack = self.acknowledgement.generate_general_acknowledgement()
        if topic == "Interesting_News" or topic == "":
            utt = self.template_news.speak("general/more_news_interesting", {})
        elif "Sports" in topic:
            sport_name = topic.split(':')
            utt = self.template_news.speak(
                "general/more_news", {'topic': sport_name[1], 'usr_name': self.get_usr_name})
            if 'SPORT' in self.cm.newschat.get("propose_keywords", {}):

                usr_name = self.get_usr_name
                utt = self.template_news.speak("general/propose_sports", {'usr_name': usr_name, 'sport': sport_name[1]})
                if utt_ack:
                    utt = " ".join([utt_ack, utt])

                self.module_selection.add_propose_keyword(self.cm.newschat["propose_keywords"])
                self.cm.newschat['propose_keywords'] = None
                return utt, States.done, ProposeContinue.UNCLEAR, ProposeTopic.SPORT
            if utt_ack:
                utt = " ".join([utt_ack, utt])
            return utt, States.sport_type_title, ProposeContinue.CONTINUE, ProposeTopic.NONE
        else:
            utt = self.template_news.speak("general/more_news", {'topic': topic, 'usr_name': self.get_usr_name})
            if utt_ack:
                utt = " ".join([utt_ack, utt])
        return utt, States.headlines, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @_Decorators.transition
    def t_othernews(self):
        logging.debug('[NEWS][STATE][t_othernews]')
        if self.intents.nointerest_news or self.other_topic is not None:
            return States.exit_news
        elif self.has_exit_keyword:
            return States.done
        elif self.cm.newschat['uplifting_news_count'] > 2 and not self.ners and self.req_type == "Other":
            return States.done
        elif self.cm.prev_state == States.readmore:
            return States.othernews
        elif self.lexical_returnnlp_ans_pos:
            return States.headlines
        elif self.intents.ask_question and not self.enter_news:
            # if self.intents.iscommand:
            #     state = self.match_command_to_state()
            #     return state
            return States.question_answer
        elif self.req_type is not "Other":
            return States.headlines
        elif self.can_map_to_unused_chit_chat() is not None:
            return States.intro_chit_chat
        elif self.ners:
            return States.headlines
        elif self.interesting_news and self.command:
            return States.headlines
        elif self.req_undetected_ner:
            return States.headlines
        elif self.cm.prev_state == States.question_answer and self.positive_ans:
            return States.headlines
        elif self.positive_ans:
            return States.headlines
        elif self.negative_ans:
            self.clear_previous_topic()
            return States.done
        elif self.enter_news:
            return States.headlines
        elif self.dont_care_news:
            return States.handle_neg_response
        else:
            return States.done

    @_Decorators.state
    def s_othernews(self, params: dict):
        logging.debug('[NEWS][STATE][s_othernews]')
        if self.cm.prev_state == States.readmore or self.cm.prev_state == States.headlines:
            topic = self.get_previous_topic
            if topic == "coronavirus vaccine test":
                # self.cm.newschat['previous_topic'][1] = "coronavirus"
                # topic = "coronavirus"
                utt = self.template_news.speak("general/start_news_interesting", {})
                self.cm.newschat['previous_topic'] = ["Interesting_News", "Technology"]
                return utt, States.othernews, ProposeContinue.CONTINUE, ProposeTopic.NONE
            if topic == "Interesting_News" or topic == "":
                utt = self.template_news.speak("general/more_news_interesting", {})
            else:
                utt = self.template_news.speak("general/more_news", {'topic': topic, 'usr_name': self.get_usr_name})
            return utt, States.othernews, ProposeContinue.CONTINUE, ProposeTopic.NONE

        if self.cm.prev_state == States.othernews:
            article = news_latest()
            utt_ack = self.acknowledgement.generate_general_acknowledgement()
            utt = article[0]['Headline'] + "."
            transit_utt = self.template_news.speak("general/readmore", {})
            utt = " ".join([utt_ack, utt, transit_utt])
            return utt, States.readmore, ProposeContinue.CONTINUE, ProposeTopic.NONE

        if self.cm.prev_state == States.intro:
            # if self.some_other_news:
            utt_ack = self.template_news.speak("general/acknowledgement_headline", {})
            # else:
            #     utt_ack = self.acknowledgement.generate_general_acknowledgement()
            utt = self.template_news.speak("general/start_news_interesting", {})
            self.cm.newschat['previous_topic'] = ["Interesting_News", "Technology"]
            utt = " ".join([utt_ack, utt])
            return utt, States.othernews, ProposeContinue.CONTINUE, ProposeTopic.NONE

        utt = self.template_news.speak("general/start_news_interesting", {})
        self.cm.newschat['previous_topic'] = ["Interesting_News", "Technology"]
        return utt, States.othernews, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @_Decorators.transition
    def t_done(self):
        logging.debug('[NEWS][STATE][t_done]')
        if self.dialog_act_ans_pos:
            return States.headlines
        else:
            return States.done

    @_Decorators.state
    def s_done(self, params: dict):
        logging.debug('[NEWS][STATE][s_done]')
        utt = " "
        return utt, States.intro, ProposeContinue.STOP, ProposeTopic.NONE

    @_Decorators.transition
    def t_handle_neg_response(self):
        logging.debug('[NEWS][STATE][t_handle_neg_response]')
        return States.handle_neg_response

    @_Decorators.state
    def s_handle_neg_response(self, params: dict):
        logging.debug('[NEWS][STATE][s_handle_neg_response]')
        utt = self.template_news.speak(
            "general/negative_response", {})
        return utt, States.done, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @_Decorators.transition
    def t_command_not_handled(self):
        logging.debug('[NEWS][STATE][t_command_not_handled]')
        return States.command_not_handled

    @_Decorators.state
    def s_command_not_handled(self, params: dict):
        logging.debug('[NEWS][STATE][s_command_not_handled]')
        utt = "Sorry, I don't know much about that. "
        next_state = self.cm.prev_state
        return utt, next_state, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @_Decorators.transition
    def t_question_answer(self):
        logging.debug('[NEWS][STATE][t_question_answer]')
        if self.intents.nointerest_news or self.other_topic is not None:
            return States.exit_news
        return States.question_answer

    @_Decorators.state
    def s_question_answer(self, params: dict):
        logging.debug('[NEWS][STATE][s_question_answer]')
        # handle question
        answer = None
        utt = ""
        if "ask_reason" in self.returnnlp.lexical_intent[0] and self.cm.newschat['backstory_reason']:
            utt = self.cm.newschat['backstory_reason']
            self.cm.newschat['backstory_reason'] = None
        else:
            answer = self.intents.handle_news_question()

        if answer and answer.bot_propose_module and answer.bot_propose_module != TopicModule.NEWS:
            propose_topic = answer.bot_propose_module.value
            ans_text = answer.response + "<break time='0.15s'/> "
            propose_topic_text = self.intents.generate_propose_topic_response(f"propose_topic_short/{propose_topic}")
            # self.propose_continue = "UNCLEAR"
            # self.propose_topic = propose_topic
            utt = ans_text + propose_topic_text
            next_state = States.intro
            if answer.backstory_reason:
                self.cm.newschat['backstory_reason'] = answer.backstory_reason
            return utt, next_state, ProposeContinue.UNCLEAR, propose_topic

        if answer and answer.backstory_reason:
            self.cm.newschat['backstory_reason'] = answer.backstory_reason

        if answer and not utt:
            utt = answer.response
        logging.debug("[NEWS] response from question handler : {}".format(utt))

        # if self.intents.ask_opinion_question:
        #     if self.central_elem.backstory.confidence > BACKSTORY_CONFIDENCE:
        #         utt = self.central_elem.backstory.text
        # else:
        #     # Implement EVI to answer the factual question
        #     try:
        #         # backstory hotfix
        #         if self.central_elem.backstory.confidence > BACKSTORY_CONFIDENCE:
        #             utt = self.central_elem.backstory.text
        #             if "ask_leave" in self.central_elem.custom_intents.lexical:
        #                 return utt, States.headlines, ProposeContinue.CONTINUE, ProposeTopic.NONE
        #         elif self.central_elem.has_dialog_act(DialogActEnum.YES_NO_QUESTION):
        #             repeat_as_question_utt = self.get_repeat_as_question_utt()
        #             transit_utt = self.template_news.speak(
        #                 "qa/unknown_handeler/yes_no_question_handle", {"usr_name": self.get_usr_name})
        #             utt = " ".join([repeat_as_question_utt, transit_utt])
        #         else:
        #             client = get_client(api_key=COBOT_API_KEY)
        #             res = client.get_answer(
        #                 question=self.central_elem.text, timeout_in_millis=1000)
        #             utt = postprocessing_evi_response(res["response"])
        #     except Exception as e:
        #         logging.error(
        #             "[NEWSMODULE][ERROR] EVI does properly return a response.err: {}".format(e), exc_info=True)
        #
        # if utt is None:
        #     if self.sys_ack_text():
        #         utt = self.sys_ack_text()
        #
        # if utt is None:
        #     # handle the unknown answer case
        #     if self.intents.ask_opinion_question:
        #         utt = self.template_news.speak(
        #             "qa/unknown_handeler/opinion_question", {})
        #     else:
        #         utt = self.template_news.speak(
        #             "general/no_answer", {})

        # Determine the next state from question answer
        transit_utt = ""
        transition = self.template_news.speak(
            "general/transition_acknowledgement", {})
        next_state = States.question_answer
        if self.cm.prev_state == States.readmore:
            if self.cm.newschat['curr_news_turns'] < 2:
                next_state = States.readmore
                transit_utt = self.template_news.speak(
                    'qa/transit-readmore', {})
            else:
                topic = self.get_previous_topic
                if topic in "Interesting_News" or topic == "":
                    transit_utt = self.template_news.speak("general/more_news_interesting", {})
                    self.cm.newschat['previous_topic'] = ["Interesting_News", "Technology"]
                elif topic == "coronavirus vaccine test":
                    self.clear_previous_topic()
                    self.cm.newschat['previous_topic'] = ["Interesting_News", "Technology"]
                    transit_utt = self.template_news.speak("general/more_news_interesting", {})
                else:
                    transit_utt = self.template_news.speak("general/more_news", {'topic': topic, 'usr_name': self.get_usr_name})
                next_state = States.othernews
        elif self.cm.prev_state == States.headlines:
            transit_utt = self.template_news.speak('qa/transit-readmore', {})
            if len(self.cm.newschat['previous_topic']) > 0:
                if self.cm.newschat['previous_topic'][0] == "Type_News":
                    next_state = States.readmore_moments
                else:
                    next_state = States.readmore
            else:  # return to sport_title
                next_state = States.headlines
                transit_utt = self.template_news.speak(
                    "general/more_news_interesting", {})
                self.cm.newschat['previous_topic'] = [
                    "Interesting_News", "Technology"]

        elif self.cm.prev_state == States.intro:
            transit_utt = self.template_news.speak(
                "general/propose_topic", {})
            next_state = States.headlines
        elif self.cm.prev_state == States.othernews:
            transit_utt = self.template_news.speak(
                "general/start_news_interesting", {})
            self.cm.newschat['previous_topic'] = [
                "Interesting_News", "Technology"]
            next_state = States.othernews
        elif self.cm.prev_state == States.readmore_moments or self.cm.prev_state == States.question_answer:
            topic = self.get_previous_topic
            next_state = States.headlines
            if "Sports" in topic:
                next_state = States.sport_type_title
                sport_name = topic.split(':')
                transit_utt = self.template_news.speak(
                    "general/more_news", {'topic': sport_name[1], 'usr_name': self.get_usr_name})
            elif topic == "Interesting_News" or topic == "":
                transit_utt = self.template_news.speak("general/more_news_interesting", {})
                self.cm.newschat['uplifting_news_count'] += 1
            else:
                transit_utt = self.template_news.speak(
                    "general/more_news", {'topic': topic, 'usr_name': self.get_usr_name})

        elif self.cm.prev_state == States.othernews_moments:
            next_state = States.othernews_moments

        elif self.cm.prev_state == States.intro_chit_chat or self.cm.prev_state == States.chit_chat:
            if self.cm.newschat['used_chit_chat'][-1] == "coronavirus":
                transit_utt, next_state = self.get_next_chit_chat_corona()
            elif self.cm.newschat['used_chit_chat'][-1] == "donald trump":
                transit_utt, next_state = self.get_next_chit_chat()
            else:
                transit_utt, next_state = self.political_chitchat_dialogflow()

        # elif self.cm.prev_state == States.chit_chat:
        #     transit_utt,  next_state = self.get_next_chit_chat()

        elif self.cm.prev_state is None:
            transit_utt = self.template_news.speak("general/propose_topic", {})
            # transit_utt = self.template_news.speak(
            #     "chit_chat/trump_impeachment/intro", {})
            # transition = ""
            # self.cm.newschat['chit_chat_last_dialog'].append("intro")
            # self.cm.newschat['use_political_chitchat'] = False
            next_state = States.headlines

        if self.ners:
            transit_utt = self.template_news.speak("general/transit_ner_from_question", {'ner': self.ners[0]})
            self.cm.newschat['next_ner'] = self.ners
            next_state = States.headlines
        # update utt
        utt = " ".join([utt, transition, transit_utt])
        return utt, next_state, ProposeContinue.CONTINUE, ProposeTopic.NONE

    @_Decorators.transition
    def t_exit_news(self):
        logging.debug('[NEWS][STATE][t_exit_news]')
        return States.exit_news

    @_Decorators.state
    def s_exit_news(self, params: dict):
        logging.debug('[NEWS][STATE][s_exit_news]')
        if self.intents.nointerest_news:
            logging.info("[NEWS_MODULE] Not interested in news is detected.")
            if self.intents.quit_chat:
                utt = self.template_news.speak("general/quit_chat_response", {})
                return utt, States.headlines, ProposeContinue.CONTINUE, ProposeTopic.NONE
            utt = self.template_news.speak("general/propose_quit_news", {})
            if self.other_topic is not None:
                proposed_topic = KEYWORD_MODULE_MAP[self.other_topic]
                return utt, States.intro, ProposeContinue.STOP, proposed_topic
            else:
                return utt, States.intro, ProposeContinue.STOP, ProposeTopic.NONE
        elif self.other_topic is not None:
            proposed_topic = KEYWORD_MODULE_MAP[self.other_topic]
            utt = self.template_news.speak("general/propose_module_topic", {"other_topic": self.other_topic})
            return utt, States.intro, ProposeContinue.UNCLEAR, proposed_topic


class UserAttributes:

    def __init__(self):
        self.newschat = {}
        self.template_manager = {}
        self.last_module = ""
        self.session_id = ""


if __name__ == "__main__":

    def __input_data(
            text: str,
            keywords: List[Tuple[float, str]],
            # noun_phrase: List[Tuple[float, str]],
            lexical: List[str],
            knowledge: Tuple[str, str, float, str],
            ner: List[Tuple[float, str]],
            coreference: None,
            local_noun_phrase: List[str],
            noun_phrase: List[str],
    ):
        return {'features': {

            'text': text,
            'ner': ner, 'sentiment': None,
            'noun_phrase': noun_phrase,
            'intent': 'general',
            'topic': [
                {
                    'topicClass': 'Games', 'confidence': 999.0,
                    'text': text,
                    'topicKeywords': [{'confidence': k[0], 'keyword': k[1]} for k in keywords]
                }
            ],
            'intent_classify': {
                # ['ask_yesno']
                'sys': ['req_playgame'], 'topic': ['topic_game'], 'lexical': lexical
            },
            'topic_module': 'NEWS',
            'topic_keywords': [{'confidence': k[0], 'keyword': k[1]} for k in keywords],
            'knowledge': knowledge,
            'coreference': coreference,

        },
            'text': text,
            'coreference': coreference,
            'npknowledge': {'local_noun_phrase': [local_noun_phrase], 'noun_phrase': []}}

    test_cases_db = [
        ("news about trump", [], [], [], ['trump'], [], [], ['trump'])

    ]

    def get_test_case(index: List[int]):
        return [test_cases_db[i] for i in index]

    test_cases = [get_test_case([0])]

    for idx, test_case in enumerate(test_cases):

        _u_a = UserAttributes()

        for i, t in enumerate(test_case):
            print('round {}'.format(i))
            print('usr:\t', t)
            _rg = NewsAutomaton(input_data=__input_data(*t),
                                user_attributes_ref=_u_a)
            _utt = _rg.response()

            print('utt:\t', _utt['response'], _utt['z_cm'])
