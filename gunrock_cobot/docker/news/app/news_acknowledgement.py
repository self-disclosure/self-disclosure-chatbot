import re
import logging

MAX_THRESHOLD = 1.0
HIGH_THRESHOLD = 0.75
MED_THRESHOLD = 0.5
LOW_THRESHOLD = 0.25


class NewsAcknowledgement:
    """
    Class to generate acknowledgement for user's utterance using various attributes like sentiment, lexical,
    length of response and some regex
    """
    def __init__(self, input_data, user_attributes_ref, template_news, states):
        self._input_data = input_data
        self._user_attributes_ref = user_attributes_ref
        self._template_news = template_news
        self._states = states
        self.ack_response = ""
        self._sys_acknowledgement = input_data.get("system_acknowledgement", {})

        #Initialize the pc_state 2 ack mapping
        self.__pc_state2ack_mapping__()
        
    def __pc_state2ack_mapping__(self):
        self.pc2ack = {
                       "pc_intro": self.ack_pc_intro,
                       "pc_pos": self.ack_pc_opinion,
                       "pc_neg": self.ack_pc_opinion,
                       "pc_opponent_argue": self.ack_pc_appreciate_opinion,
                       "pc_neu": self.ack_pc_appreciate_opinion,
        }
    
    @property
    def input_text(self) -> str:
        # text = removeStopwords(self.input_data['text'])
        pat = re.compile(r"open gunrock and |alexa|Alexa|echo|Echo")
        try:
            text = re.sub(
                pat, '', self._input_data['features']['converttext'])
        except (KeyError, TypeError):
            text = re.sub(pat, '', self._input_data['text'])
        return text

    @property
    def sentiment_neg(self):
        try:
            return float(self._input_data['returnnlp'][0]['sentiment']['neg'])
        except (KeyError, TypeError):
            return 0

    @property
    def sentiment_pos(self):
        try:
            return float(self._input_data['returnnlp'][0]['sentiment']['pos'])
        except (KeyError, TypeError):
            return 0

    @property
    def sentiment_neu(self):
        try:
            return float(self._input_data['returnnlp'][0]['sentiment']['neu'])
        except (KeyError, TypeError):
            return 0

    @property
    def command(self):
        try:
            if self._input_data['central_elem']['DA'] == "commands":
                return float(self._input_data['central_elem']['DA_score'])
            return 0
        except (KeyError, TypeError):
            return 0

    @property
    def appreciation(self):
        try:
            if self._input_data['central_elem']['DA'] == "appreciation":
                return float(self._input_data['central_elem']['DA_score'])
            return 0
        except (KeyError, TypeError):
            return 0

    @property
    def dialog_act_ans_neg(self):
        try:
            if self._input_data['central_elem']['DA'] == "neg_answer":
                return float(self._input_data['central_elem']['DA_score'])
            return 0
        except (KeyError, TypeError):
            return 0

    @property
    def dialog_act_ans_pos(self):
        try:
            if self._input_data['central_elem']['DA'] == "pos_answer":
                return float(self._input_data['central_elem']['DA_score'])
            return 0
        except (KeyError, TypeError):
            return 0

    @property
    def lexical_returnnlp_ans_neg(self):
        try:
            if 'ans_neg' in self._input_data['returnnlp'][0]['intent']['lexical']:
                return True
            return False
        except (KeyError, TypeError):
            return 0

    @property
    def lexical_returnnlp_ans_pos(self):
        try:
            if 'ans_pos' in self._input_data['returnnlp'][0]['intent']['lexical']:
                return True
            pat = re.compile(
                r"why not|why sure|why yes|go on|keep going|continue")
            if pat.search(self.input_text):
                return True
            return False
        except (KeyError, TypeError):
            return 0

    @property
    def lexical_returnnlp_ans_factopinion(self):
        try:
            if 'ans_factopinion' in self._input_data['returnnlp'][0]['intent']['lexical']:
                return True
            return False
        except (KeyError, TypeError):
            return 0

    @property
    def prev_state(self):
        return self._user_attributes_ref.newschat[
            'prev_state'] if 'prev_state' in self._user_attributes_ref.newschat else None

    @property
    def curr_state(self):
        return self._user_attributes_ref.newschat[
            'curr_state'] if 'curr_state' in self._user_attributes_ref.newschat else None

    @property
    def sad_news(self):
        try:
            pat = re.compile(
                r"(thats|that is|its|it is) (sad|terrible|bad|horrible)")
            if pat.search(self._input_data['text']):
                return True
            return False
        except Exception as e:
            logging.error(
                "[NEWSMODULE][ERROR] req_undetected_ner doesn't give proper return value: {}".format(e), exc_info=True)

    #  Function to generate general news acknowledgement
    def generate_general_acknowledgement(self):

        if self.curr_state == self._states.question_answer.value:
            return self.ack_response
        elif self._sys_acknowledgement:
            if self._sys_acknowledgement.get('output_tag', None) == "ack_question_idk":
                return self.ack_response
            if 'ack' in self._sys_acknowledgement and self._sys_acknowledgement.get('output_tag', None) != "ack_question_idk":
                if len(self._sys_acknowledgement['ack']) > 0:
                    self.ack_response = self._sys_acknowledgement['ack']
                    return self.ack_response
        if self.prev_state == self._states.readmore.value or \
                self.prev_state == self._states.readmore_moments.value or \
                self.curr_state == self._states.othernews.value:
            # this is probably an opinion
            if self.lexical_returnnlp_ans_factopinion and len(self._input_data['features']['converttext']) > 4:
                self.ack_response = self._template_news.speak('general/opinion_acknowledgement', {})
            if self.sad_news:
                self.ack_response += self._template_news.speak('general/sad_acknowledgement', {})
            elif self.lexical_returnnlp_ans_neg or self.sentiment_neg > HIGH_THRESHOLD or self.dialog_act_ans_neg > HIGH_THRESHOLD:
                self.ack_response += self._template_news.speak('general/negative_acknowledgement', {})
            elif self.lexical_returnnlp_ans_pos or self.sentiment_pos > HIGH_THRESHOLD or self.dialog_act_ans_pos > HIGH_THRESHOLD:
                self.ack_response += self._template_news.speak('general/positive_acknowledgement', {})
            else:
                self.ack_response += self._template_news.speak('general/transition_acknowledgement', {})

        elif self.curr_state == self._states.intro.value:
            if self.lexical_returnnlp_ans_neg or self.sentiment_neg > HIGH_THRESHOLD or self.dialog_act_ans_neg > HIGH_THRESHOLD:
                self.ack_response += self._template_news.speak('general/acknowledgement_dont_like', {})
            else:
                self.ack_response = self._template_news.speak('general/acknowledgement_headline', {})

        return self.ack_response

    # Function to generate chit_chat acknowledgement
    def generate_chit_chat_acknowledgement(self):
        chitchat_topic = self._user_attributes_ref.newschat['used_chit_chat'][-1]
        template_slot = ""
        if chitchat_topic == "donald trump":
            template_slot = "trump_impeachment"
        elif chitchat_topic == "coronavirus":
            template_slot = "coronavirus"
        prev_chit_chat_state = self._user_attributes_ref.newschat['chit_chat_last_dialog'][-1]
        if self.curr_state == self._states.question_answer.value:
            return self.ack_response
        if self._sys_acknowledgement:
            if self._sys_acknowledgement.get('output_tag', None) == "ack_question_idk":
                return self.ack_response
            if 'ack' in self._sys_acknowledgement and self._sys_acknowledgement.get('output_tag', None) != "ack_question_idk":
                if len(self._sys_acknowledgement['ack']) > 0:
                    self.ack_response = self._sys_acknowledgement['ack']
                    return self.ack_response
        # this is probably an opinion
        if self.lexical_returnnlp_ans_factopinion and len(self._input_data['features']['converttext']) > 4:
            self.ack_response = self._template_news.speak('general/opinion_acknowledgement', {})

        if self.sad_news:
            self.ack_response += self._template_news.speak('general/sad_acknowledgement', {})
        elif self.lexical_returnnlp_ans_neg or self.sentiment_neg > HIGH_THRESHOLD or self.dialog_act_ans_neg > HIGH_THRESHOLD:
            self.ack_response += self._template_news.speak('chit_chat/{}/negative_acknowledgement_{}'.format(template_slot, prev_chit_chat_state), {})
        elif self.lexical_returnnlp_ans_pos or self.sentiment_pos > HIGH_THRESHOLD or self.dialog_act_ans_pos > HIGH_THRESHOLD:
            self.ack_response += self._template_news.speak('chit_chat/{}/positive_acknowledgement_{}'.format(template_slot, prev_chit_chat_state), {})
        else:
            self.ack_response += self._template_news.speak('general/transition_acknowledgement', {})

        return self.ack_response

    def generate_political_chitchat_acknowledgement(self):
        prev_political_chitchat_state = self._user_attributes_ref.newschat.get('prev_political_chitchat_state', None)
        if self._sys_acknowledgement:
            if 'ack' in self._sys_acknowledgement:
                if len(self._sys_acknowledgement['ack']) > 0:
                    self.ack_response = self._sys_acknowledgement['ack']
                    return self.ack_response
        if prev_political_chitchat_state is not None:
            self.ack_response = self.pc2ack[prev_political_chitchat_state]()
        else:
            self.ack_response = ""

        return self.ack_response
    
    #Define the acknowledge function for each state
    def ack_pc_intro(self):
        """
        Acknowledge on the intro question whether they support or not support or some other mutual acknowledge. 
        TODO: add an acknowledge regarding i don't know
        """
        ack = ""
        # if self.dialog_act_ans_pos > MED_THRESHOLD or self.lexical_returnnlp_ans_pos:
        #     #TODO: Acknowledge the positive response for the topic question
        #     ack = self._template_news.speak('general/acknowledgement_pc_answer_pos', {})
        # elif self.dialog_act_ans_neg > MED_THRESHOLD or self.lexical_returnnlp_ans_neg:
        #     #TODO: Acknowledge the negative response for the topic question
        #     ack = self._template_news.speak('general/acknowledgement_pc_answer_neg', {})
        if len(self.input_text.split()) > 6:
            ack = self._template_news.speak('general/acknowledgement_appreciate_share_opinion', {})
        else:
            ack = self._template_news.speak('general/acknowledgement_general', {})
 
        return ack

    def ack_pc_opinion(self):
        """
        Acknowledge on the intro question whether they support or not support or some other mutual acknowledge. 
        TODO: Add an acknowledge regarding i don't know
        """
        ack = ""
        if self.dialog_act_ans_pos > MED_THRESHOLD or self.lexical_returnnlp_ans_pos:
            ack = self._template_news.speak('general/acknowledgement_agree_support_op', {})
        elif self.dialog_act_ans_neg > MED_THRESHOLD or self.lexical_returnnlp_ans_neg:
            ack = self._template_news.speak('general/acknowledgement_disagree_support_op', {})
        elif len(self.input_text.split()) > 6:
            ack = self._template_news.speak('general/acknowledgement_appreciate_share_opinion', {})
        else:
            ack = self._template_news.speak('general/acknowledgement_general', {})
 
        return ack
    
    def ack_pc_appreciate_opinion(self):
        """
        Acknowledge on the intro question whether they support or not support or some other mutual acknowledge. 
        TODO: Add an acknowledge regarding i don't know
        """
        ack = ""
        
        if len(self.input_text.split()) > 4:
            ack = self._template_news.speak('general/acknowledgement_appreciate_share_opinion', {})
        else:
            ack = self._template_news.speak('general/acknowledgement_general', {})
 
        return ack





