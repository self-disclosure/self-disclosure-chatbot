import logging
import json
import re
from cobot_common.service_client import get_client
import hashlib
import redis
import requests
from backstory_client import get_backstory_response_text

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
COBOT_API_KEY = 'xgTjk23SRM9Q9VFrUEFOjav0Bn4ot21W8uFLEieT'
EVI_KEY = "gunrock:api:evi"



rds = redis.StrictRedis(host='52.87.136.90', port=16517,
                        socket_timeout=3,
                        socket_connect_timeout=1,
                        retry_on_timeout=True,
                        db=0, password="alexaprize", decode_responses=True)


def get_backstory_or_evi(utterance, a_b_test):
    try:
        # backstory hotfix to wrongly classified factual question (eg. where are you from?)
        backstory = get_backstory_response_text(utterance, a_b_test, 0.82)
        if backstory:
            return backstory

        client = get_client(api_key=COBOT_API_KEY)
        r = client.get_answer(question=utterance, timeout_in_millis=1000)
        logger.debug(r)
        if r["response"] == "" or re.search(r"skill://", r["response"]):
            return None
        elif re.search(r"I didn't get that", r["response"], re.IGNORECASE):
            return None

        if re.search(r"an opinion on that", r["response"]):
            response = "Oh, I haven't thought about that before. "
        return response
    except Exception as e:
        logger.warning("Timeout in EVI_bot with error: {}".format(e))
        return None

def set_redis_with_expire(prefix, input_str, output, expire=24 * 60 * 60):
    logging.info(
        '[REDIS] set redis with expire input: {} output: {}, expire {}'.format(
            input_str, output, expire))
    hinput = hashlib.md5(input_str.encode()).hexdigest()
    return rds.set(prefix + ':' + hinput, json.dumps(output),
                   nx=True, ex=expire)


def get_redis(prefix, input_str):
    logging.info('[REDIS] get redis  input: {}'.format(input_str))
    hinput = hashlib.md5(input_str.encode()).hexdigest()
    results = rds.get(prefix + ':' + hinput)
    if results is None:
        return None
    return json.loads(results)
