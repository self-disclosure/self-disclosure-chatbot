import requests
from urllib.parse import quote

def wp(utterance):
    if not utterance:
        return None
    try:
        # query ="?q=" + utterance
        # query = quote(query, safe='+?q=')
        r = requests.get('https//35.168.206.27:8080/api/v1.0/wp/' + utterance)
        return r.json()
    except Exception as e:
        print(e)
        return None

if __name__ == "__main__":
    print(wp('connor mcgregor'))