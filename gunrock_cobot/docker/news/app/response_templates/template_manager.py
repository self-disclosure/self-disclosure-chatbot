import calendar
import hashlib
import importlib
import logging
import operator
import random
from datetime import datetime, date
from enum import Enum
from functools import reduce
from string import Formatter
from typing import Union, List, Dict, Tuple, Optional

# MARK: - Type Alias
SelectorAPI = Union[List[str], str]

# MARK: - Useful variables
error_response = "Oh no, I think my creators misspelled something in my code. Would you mind asking me something else?"

# MARK: - Logger setup
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# MARK: - Selection Strategy Properties
available_topics = ['MOVIECHAT', 'SPORT', 'ANIMALCHAT', 'MUSICCHAT',
                    'GAMECHAT', 'BOOKCHAT', 'TECHSCIENCECHAT']

topic_map = {
    'SPORT': {'sports'},
    'TECHSCIENCECHAT': {'technology', 'science'},
    'MOVIECHAT': {'movies'},
    'BOOKCHAT': {'books'},
    'TRAVELCHAT': {'traveling'},
    'GAMECHAT': {'games', 'video games'},
    'NEWS': {'the news'},
    'ANIMALCHAT': {'animals'},
    'MUSICCHAT': {'music'},
    'WEATHER': {'the weather'},
    'HOLIDAYCHAT': {'holidays'}
}

# MARK: - Template Class


class Templates(Enum):
    # MARK: - Enum Definition
    # Global / System templates
    shared, sys_rg, error, topics = 'shared_template', 'intent_general', 'error_template', 'intent_topics'
    dm = 'dm_template'

    # Module templates.
    # Add your template yml file to the 'gunrock_cobot/response_templates/' folder then run this script
    social = 'social_template'
    techscience, games = 'techscience_template', 'games_template'
    news, health, weather = 'news_template', 'health_template', 'weather_template'
    animal = 'animal_template'
    movies = 'movies_template'
    #books = 'books_template'

    @property
    def _data(self) -> dict:
        try:
            return getattr(globals()['template_data'], 'data')[self.name]
        except Exception as e:
            logger.critical(e, exc_info=True)
            raise e

    # MARK: - API

    def utterance(self, selector: SelectorAPI, slots: Dict[str, str], user_attributes_ref) -> str:
        """
        The main function of template manager. Use this function to get an utterance from shared_template.
        ### IMPORTANT: ###
        Please also look at response_templates/shared_template.yml to see what which template you want
        and what slots you need to provide.

        :param template_origin:     select which template you want to choose from

        :param selector:            a string or List[str] that specifies which template you want to use.
                                    e.g. to select from opinion -> agree: use "opinion/agree"

        :param slots:               a dict used to fill in the slots in the templates.
                                    Formatting is done using str Formatter()
                                    e.g. for topic_choice: {'topic_a': 'movies', 'topic_b': 'sports'}
                                    ### IMPORTANT: ###
                                    Include all slots that the template group needs.
                                    Match your dict keys with the template name.
        :param user_attributes_ref: prev_hashes from user attribute dictionary from ServiceModule that
                                    store the hashes

        :return:                    A completely formatted utterance that you can return to cobot.
        """
        if isinstance(selector, str):
            selector = selector.split('/')

        # Setting up U_A, prev_hash, and the actual hash key
        if user_attributes_ref is not None:
            if user_attributes_ref.prev_hash is None:
                # type: Dict[str, List[str]]
                user_attributes_ref.prev_hash = {}
            if self._hash_selector(selector) not in user_attributes_ref.prev_hash:
                user_attributes_ref.prev_hash[self._hash_selector(selector)] = [
                ]

        try:
            utterances = self._get_utterance_list(selector)

            utterance, hash_id = self._select_utterance(
                utterances,
                user_attributes_ref.prev_hash if user_attributes_ref else None,
                selector
            )
            utterance = self._format_fragments(
                utterance,
                user_attributes_ref.prev_hash if user_attributes_ref else None,
                selector
            )

            utterance = utterance.format(**slots)

            if user_attributes_ref is not None and user_attributes_ref.prev_hash is not None:
                user_attributes_ref.prev_hash[self._hash_selector(
                    selector)].append(hash_id)
            return utterance
        except (KeyError, Exception) as e:
            logger.warning(e, exc_info=True)
            logger.warning(selector)
            # TODO: Graceful error handling here plz
            return error_response

    def has_utterance(self,
                      unformatted_utterance: str,
                      selector: SelectorAPI) -> bool:
        """
        This function checks if the template has a specified utterance.
        ### IMPORTANT: ###
        The underlying code is using string matching, which would work for strings that have no substitution,
        but will break if there is.
        """
        if isinstance(selector, str):
            selector = selector.split('/')

        try:
            utterances = self._get_utterance_list(selector)
            return any(utt == unformatted_utterance for utt, _ in utterances)
        except Exception as e:
            logger.warning(e, exc_info=True)
            return False

    def get_child_keys(self, selector: SelectorAPI) -> List[str]:
        if isinstance(selector, str):
            selector = selector.split('/')
        try:
            data = traverse(self._data, selector)
            return data.keys()
        except (KeyError, AttributeError) as e:
            logger.warning(e, exc_info=True)
            return []

    def count_utterances(self, selector: SelectorAPI) -> int:
        if isinstance(selector, str):
            selector = selector.split('/')
        try:
            data = traverse(self._data, selector)
            if isinstance(data, list):
                return len(data)
            else:
                return 0
        except KeyError as e:
            logger.warning(e, exc_info=True)
            return 0

    def count_used_utterances(self, selector: SelectorAPI, user_attributes_ref) -> int:
        # Checking U_A
        if user_attributes_ref is None and user_attributes_ref.prev_hash is None:
            return 0
        if isinstance(selector, str):
            selector = selector.split('/')

        hashes = user_attributes_ref.prev_hash.get(
            self._hash_selector(selector))
        return len(hashes) if hashes else 0

    # MARK: - Internal Methods

    def _hash_selector(self, selector: List[str]) -> str:
        return self.value + '/' + '/'.join(selector)

    def _get_utterance_list(self, selector: List[str]) -> List[Tuple[str, str]]:
        data = traverse(self._data, selector)
        data = [(d['text'], d['hash']) for d in data]
        return data

    def _select_utterance(self,
                          utterances: List[Tuple[str, str]],
                          prev_hashes: Dict[str, List[str]],
                          selector: List[str]) -> Tuple[str, str]:
        if prev_hashes is None:
            return random.choice(utterances)
        if len(utterances) == 1:
            prev_hashes[self._hash_selector(selector)] = []
            return utterances[0]

        filtered = [(utt, hash_id) for utt, hash_id in utterances
                    if hash_id not in prev_hashes[self._hash_selector(selector)]]
        if len(filtered) == 0:
            prev_hashes[self._hash_selector(selector)] = [
                prev_hashes[self._hash_selector(selector)][-1]]
            filtered = [(utt, hash_id) for utt, hash_id in utterances
                        if hash_id not in prev_hashes[self._hash_selector(selector)]]

        return random.choice(filtered)

    def _format_fragments(self,
                          sentence: str,
                          prev_hashes: Dict[str, List[str]],
                          selector: List[str]) -> str:
        try:
            slots: List[str] = [name for _, name, _, _,
                                in Formatter().parse(sentence) if name is not None]
        except ValueError as e:
            logger.critical(e, exc_info=True)
            logger.critical(sentence)
            raise e
        if len(slots) <= 0:
            return sentence
        sub = {}
        for name in slots:
            if not name.startswith(('t_', 's_')):
                sub[name] = '{{{}}}'.format(name)
                continue
            if name.startswith(('t_fragments', 's_fragments')):
                key = name.split('/')

            elif name == 't_time_of_day':
                current_hour = datetime.now().hour
                sub[name] = 'morning' if current_hour < 12 else 'afternoon' if current_hour < 18 else 'evening'
                continue
            elif name == 't_weekday':
                today = datetime.today()
                sub[name] = calendar.day_name[today.weekday()]
                continue

            elif name.startswith('t_'):
                key = name.lstrip('t_').split('/')
            elif name.startswith('s_'):
                key = name.lstrip('s_').split('/')
            else:
                sub[name] = ''
                continue

            if name.startswith('t_'):
                fragments = Templates.shared._get_utterance_list(key)
            else:
                fragments = self._get_utterance_list(key)

            f_utt, f_hash_id = self._select_utterance(
                fragments, prev_hashes, selector)
            # call recursive to replace the fragments in the fragments
            sub[name] = self._format_fragments(f_utt, prev_hashes, selector)

        try:
            sentence = sentence.format(**sub)
            return sentence
        except KeyError as e:
            raise KeyError(
                "Missing slot: '{}' for selector: '{}'".format(e, selector))

    # MARK: - template_data.py Generation
    def _hash_utterance(self, text: str, selector: List[str]) -> str:
        try:
            return hashlib.md5((self.value + '/' + '/'.join(selector) + '/' + text).encode()).hexdigest()[:6]
        except TypeError as e:
            raise TypeError(
                "Hash for Path: {} failed. {}".format('/'.join(path), e))

    def _generate_utterance_entry(self, text: str, selector: List[str]):
        return {
            'text': text,
            'hash': self._hash_utterance(text, selector)
        }


# MARK: - Template Internal Functions

def _propose_new_module(amount: int, fragment: bool, user_attributes_ref, return_string=True):
    if not (1 <= amount <= 2):
        raise ValueError(
            "propose new topic amount out of bounds: {}".format(amount))

    if user_attributes_ref:
        if not user_attributes_ref.prev_hash:
            user_attributes_ref.prev_hash = {}

        if not user_attributes_ref.prev_hash.get('used_topic'):
            user_attributes_ref.prev_hash['used_topic'] = []
        used_topic = user_attributes_ref.prev_hash.get('used_topic', [])

        topics = [t for t in available_topics if t not in used_topic]

        if len(topics) < amount:
            # topic = random.sample(available_topics, amount)
            topic = topics[:amount]
            user_attributes_ref.prev_hash['used_topic'].clear()
        else:
            topic = random.sample(topics, amount)
            user_attributes_ref.prev_hash['used_topic'] += topic
    else:
        topic = random.sample(available_topics, amount)

    if return_string:
        topic = [random.sample(topic_map[t], 1)[0] for t in topic]
        if fragment:
            return ' or '.join(topic)
        return Templates.shared.utterance(selector='propose_new_module/{}'.format(amount),
                                          slots={'topic': ' or '.join(topic)},
                                          user_attributes_ref=user_attributes_ref)
    else:
        return [(t, random.sample(topic_map[t], 1)[0]) for t in topic]


# MARK: - Util Functions

def traverse(tree: dict, selector: List[str]):
    return reduce(operator.getitem, selector, tree)


# MARK: - Module Selection

def get_new_module(user_attributes_ref) -> Tuple[str, str]:

    if not user_attributes_ref.prev_hash:
        user_attributes_ref.prev_hash = {}

    if not user_attributes_ref.prev_hash.get('used_topic'):
        user_attributes_ref.prev_hash['used_topic'] = []
    used_topic = user_attributes_ref.prev_hash.get('used_topic', [])

    topics = [t for t in available_topics if t not in used_topic]

    if len(topics) == 0:
        user_attributes_ref.prev_hash['used_topic'] = []
        module = available_topics[0]
        utt = "EMPTY"
    else:
        module = topics[0]
        utt = random.sample(topic_map[module], k=1)[0]
        user_attributes_ref.prev_hash['used_topic'].append(module)

    return module, utt


def add_discussed_module(module: str, user_attributes_ref):
    # if module not in available_topics:
    #     logger.warning("module '{}' is not a valid module".format(module))
    #     return False
    if not user_attributes_ref.prev_hash:
        user_attributes_ref.prev_hash = {}
    if not user_attributes_ref.prev_hash.get('used_topic'):
        user_attributes_ref.prev_hash['used_topic'] = []
    if module not in user_attributes_ref.prev_hash['used_topic']:
        user_attributes_ref.prev_hash['used_topic'].append(module)
    return True


# MARK: - Template Copying and Generation

def copy_templates(only: List[str] = None):
    from pathlib import Path
    from glob import glob
    import os
    import shutil

    directories = glob('docker/*/app/')
    print(directories)
    copied_path = []
    for directory in directories:
        if only is not None and not any([directory.find(x) != -1 for x in only]):
            continue
        shutil.copy2(__file__, directory)
        templates = glob('response_templates/*.py')
        # print(templates)
        for template in templates:
            template_dir = directory + 'response_templates/'
            if not os.path.exists(template_dir):
                os.makedirs(template_dir)
            shutil.copy2(template, template_dir)
        copied_path.append(directory)

    print("copied to: {}".format(copied_path))


def import_files():
    """
    Dynamic import for all modules using Template enums
    """
    try:
        globals()['template_data'] = importlib.import_module(
            'response_templates.template_data')
    except ModuleNotFoundError as e:
        logger.critical(e, exc_info=True)
        raise e


# MARK: - template_data.py generation

def _generate_py_file(template_origin: Union[Templates, List[Templates]]):
    generated_check = []
    if isinstance(template_origin, Templates):
        template_origin = [template_origin]

    write_data = {}
    for t in template_origin:
        try:
            with open('response_templates/{}.yml'.format(t.value), 'r') as f:
                import yaml
                data = yaml.load(f)

                def nested(d: dict, u: dict, path: List[str]):
                    for k, v in u.items():
                        if isinstance(v, dict):
                            path.append(k)
                            d[k] = nested(d.get(k, {}), v, path)
                            path = []
                        elif isinstance(v, list):
                            d[k] = [t._generate_utterance_entry(
                                vi, path) for vi in v]
                    return d

                if data is None:
                    return

                data = nested(data, data, [])

                write_data[t.name] = data

                generated_check.append(t.name)
        except FileNotFoundError as e:
            import os
            print(os.getcwd())
            raise e

    with open('response_templates/template_data.py', 'w') as w:
        w.write("# <--- dict object for '{}' created by template_manager.py ---> #\n"
                .format([t.value for t in template_origin]))
        w.write("# <--- Do not directly edit this file. Edit the yml file and run the "
                "template_manager.py script instead ---> #\n\n")
        # w.write('import datetime\n\n\n')

        w.write("data = \\\n")
        from pprint import pformat
        w.write(pformat(write_data, width=120))
        w.write('\n')

    print("Template generated for: {}".format(generated_check))


if __name__ == '__main__':

    _generate_py_file([template for template in Templates])
    # additional_dict={'holidays': _import_holiday()})

    copy_templates(['weather', 'social', 'news', 'techsciencechat', 'moviechat'])
    pass

import_files()
