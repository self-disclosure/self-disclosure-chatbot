import re
from nlu.profanity_regex import profanity_regex

WH = r"(what|when|where|who|whom|which|whose|why|how)"
MODAL = r"(must|shall|will|should|would|can|could|may|might)"
BE_VERBS = r"( ?\b(am|are|is|was|were)\b ?)"
MAX_1_WORDS = r" *((\w|')+\b *){0,1}"
MAX_2_WORDS = r" *((\w|')+\b *){0,2}"
MAX_3_WORDS = r" *((\w|')+\b *){0,3}"
MAX_4_WORDS = r" *((\w|')+\b *){0,4}"
MAX_6_WORDS = fr"{MAX_3_WORDS}{MAX_3_WORDS}"
MIN_2_WORDS = r" *((\w|')+\b *){2,}"
PPL = r"(actor|actress|author|writer|player|team)(s|es)?"
THINGS = r"(game|movie|book|place|show|song|name)(s|es)?"
QUESTION_START = fr"(((have|did) you|there|(i|i'm)) {MAX_2_WORDS} ((of|about) )?)"
DURATION = fr"(for )?(like )?(just )?(about )?(a )?{MAX_1_WORDS}(sec( |$)|seconds?(?! time)|minutes?|moment)"
YOU_DO = r"(^|(you|we|please|but|so|all right|wait|yeah|yes) +)"
USER_DO = fr"(can i have|give me|think( about( \w+)?)?|back{MAX_2_WORDS}in|need to go|come back)"
START = r"((so|(no\b ?){1,3}|yeah|sure|yes|okay|because|well|but|please|wait|huh|shush|hey|wow|really|oh( my goodness)?) *)?"
SORRY = r"( ?(i'm sorry( to interrupt)?|sorry|my apologies) *)"
MY_THINGS = fr"my{MAX_2_WORDS}({PPL}|{THINGS})( to{MAX_1_WORDS})?"
FRONT_MOD = fr"( ?(a|an|the|this) {MAX_1_WORDS})"
DEVICE = fr"( ?\b(amazon|alexa|echo|alexus|all|you|social bot)\b ?)"
GOING_TO_BED = fr"{MAX_1_WORDS}((gonna have|need|have|'ve|like|'m (about|ready|getting ready|trying)) to )+go to bed{MAX_3_WORDS}"

CONTINUE = r"go for it|continue|go ahead|go for it|keep going|let's hear it|read it all|if you.*(like|want)"

IT_IS = r"(this|that|it)('s| is| was| sounds)"
YOU_ARE = r"you('re| are)"
EXCLUDE_NOT = r"(?!.*not )"
EXCLUDE_NOT_AFTER = r"(?!.*not)"

DO_NOT = r"(do|did)(n't| not)"

POSITIVE_OPINION_ADJ = fr"^(?!.*(n't| not) ){MAX_3_WORDS}(great|adorable|incredible|unbelievable|amazing|excellent|fantastic|cool|exciting|wonderful|awesome|surprising|insightful|impressive)$"
POSITIVE_ADJ_SUITABLE_TO_BE_ECHOED = r"(?!.*not ).*(cute|interesting|hilarious|funny)"
POSITIVE_COMMENT_ADJ = fr"^{MAX_2_WORDS}({IT_IS}|{YOU_ARE}){EXCLUDE_NOT}.*(good|great|amazing|excellent|incredible|fantastic|cool|wonderful|adorable|surprising|insightful|impressive)"
POSITIVE_COMMENT_ADJ_SUITABLE_TO_BE_ECHOED = fr"^({IT_IS} |{YOU_ARE} |(sounds |)(very |pretty )){POSITIVE_ADJ_SUITABLE_TO_BE_ECHOED}"

FORGOT = "|".join([
    r"i ((do|did)(n't| not)|can't|cannot) (recall|remember|think of)",
    r"(hard|difficult) to (recall|think of)",
    r"i forgot",
])

AGREE = "|".join([
    r"\byou('re| are) right\b",
    r"^(?!.*not ).*true$",
    r"^i agree",
    r"i('m| am) with you$",
    r"\byou('re| are) right\b",
    r"^(?!.*(n't|not) ).*true$",
    r"^(?!.*(n't|not) ).*(guess|think) so",
    r"^(?!.*(n't|not) ).*makes sense"
])

DISAGREE = "|".join([
    r"\b(disagree)\b",
    r"don't agree",
    r"(n't|not ) think so",
    r"(n't|not ) make sense",
    r"(n't|not ) true"
])

LIKE = r"(like|love|prefer|recommend|enjoy)"
DISLIKE = r"(dislike|hate|detest|abhor|loathe|despise)"

LETS = rf"(\blet's\b|\blet us\b|\blets\b)"
TELL_ME = fr"(tell me|show me)"
TALK_ABOUT = fr"((talk|chat){MAX_2_WORDS} about)"
WANT_TO = r"(wanna|want to|('d|would) (like|love) to)"
DO_YOU_WANT_TO = fr"do you{MAX_2_WORDS}{WANT_TO}"
CAN_WE = fr"((can|could|shall) (we|i)|how about we|why (dont|don't) (we|you))"
CAN_YOU = fr"((can|could) you)"
COMMAND_IN_QUESTION_FORM_REQ_TOPIC = fr"({DO_YOU_WANT_TO}|{CAN_WE}|{CAN_YOU}){MAX_1_WORDS} ({TALK_ABOUT}|{TELL_ME}(?! more))(?! {WH})"
IS_QUESTION_IN_COMMAND_FORM = fr"({TELL_ME}|{WANT_TO} know) {WH}"


# ================= SYSTEM INTENTS =================
PAUSE_VRB = "|".join([
    fr"wait{MAX_1_WORDS}",
    fr"hold( (on|up))?",
    fr"pause({FRONT_MOD}{MAX_1_WORDS})?",
    fr"stop( talking( to me)?)?",
    fr"shut (up|off)",
    fr"be quiet",
    ])
PAUSE = fr"( ?(just )?(wanna )?( ?{PAUSE_VRB} ?))"
BATHROOM = r"(really )?(((like|have|need) to|gotta) )?(go to|use|go use) (the )?bathroom( (right now|pause|please))?"
SHOPPING_LIST = fr" ?(my|the|our|a|){MAX_1_WORDS}(shopping|grocery) list"
LIGHT = r" ?((the|my|your|our) )?((bedroom|bedside|cabinet|christmas|corner|kitchen|hall|hallway|living room) )?light(s)? ?"
BLUETOOH_VERB = fr"((dis)?connect|(un)?pair|go|turn|search|add|stream|set)"
QUESTION = r"( ?(say )?((a|another|one|the|that|my|your|some|) ?questions?|something|that) ?)"
ASK_QUESTION = fr"( ?\bask( you)?({QUESTION}|$))"
WHAT_YOU_SAID = fr"( ?what you said ?)"
MENTIONED = fr"( ?(what you said|the|that|those|it) ?)"
AGAIN = fr"( ?(again|one more time) ?)"
SAY_AGAIN = fr"( ?(say (it|that|) ?{AGAIN}) ?)"
CHAT = fr"( *\b(this|the)?{MAX_1_WORDS}(chat|conversation)\b *)"
LIKE_TO = fr"( *\b(would like|want|like) to\b *)"

sys_re_patterns = {
    # ----------- System Level Intents --------------------
    "req_allow_ask_question": "|".join([
        fr"let me {ASK_QUESTION}",
        fr"(can|could) i {ASK_QUESTION}",
        fr"i('d| would)? (liked?|loved?|need(ed)?|want(ed)?) to {ASK_QUESTION}",
        fr"i('m| was)? (wanna|gonna|can|will) {ASK_QUESTION}",
        fr"i (have|got){QUESTION}( (for you|to ask you|about you))?{DEVICE}?$",
        fr"^{MAX_2_WORDS}ask you{QUESTION}",
        fr"can you answer{QUESTION}",
        fr"({MODAL}|need) you{MAX_1_WORDS}answer(ing)?{QUESTION}",
        fr"^{MODAL} i start with{QUESTION}$",
        ])
    ,
    "req_allow_tell_sth": "|".join([
        r"(can i|i( need to| wanna|\'d like to)) tell you( \w+){1,}",
        r"would you like to know more$",
        fr"^{MAX_6_WORDS}how come you're not listening$",
        fr"^({START}({MODAL} )?you )?(let me talk|stop interjecting)$",
        fr"^({START}(do )?you )?wanna hear( {WH})?{MAX_3_WORDS}$",
        fr"^{START}(listen )?i wanna talk about {WH}{MAX_2_WORDS}$",
        fr"^{START}(listen )?(how about )?i tell you something{MAX_3_WORDS}$",
    ]),

    "hesitate": "|".join([
        fr"^(hold on|well|wait|hang on {DURATION})$",
        r"let me (think|see) ({DURATION}|about it)$",
        fr"i need a break$",
        fr"^{MAX_6_WORDS}(i try to think|i'm thinking){MAX_2_WORDS}( off the top of my head)?$",
        fr"({USER_DO}|{YOU_DO}{PAUSE}) {DURATION}",
        fr"^{MAX_2_WORDS}just {DURATION}",
        fr"talk (to|with) ((?!you|me).)* {DURATION}",
        fr"can (we|you) {PAUSE} ({DURATION}|please)",
	fr"^{PAUSE}( ({DURATION}|please))?$",
        fr"^{START}{MAX_3_WORDS}let me see$",
	fr"^i {PAUSE}( ({DURATION}|please))?$",
        r"(be right back|be back soon)",
        r"^(i'm )?going to (the )?bathroom( \w+)?$",
        fr"can i {BATHROOM}",
        fr"i need$",
        fr"^(wait |well )?(i )?{BATHROOM}",
        fr"(wait |well )?i {BATHROOM}$",
        ])
    ,

    "not_complete": "|".join([
        fr"^{MAX_6_WORDS}(the|an|would be)$",
        fr"^{MAX_6_WORDS}({BE_VERBS}|put|eat|ate|watched|and|about|not|nothing|a|you) some$",
        fr"^{MAX_6_WORDS}{MAX_3_WORDS}(?<!chick fil )\ba$",
        fr"^{MAX_4_WORDS}(just|because)$",
        fr"^{MAX_3_WORDS}maybe something$",
        fr"^i (like|liked|think|thought|learn|learned|learnt)( that)?$",
        fr"^{MAX_4_WORDS}(it's|that's)$",
        fr"^{MAX_3_WORDS}i'm gonna tell$",
        fr"^{MAX_6_WORDS}(or|and)$",
        fr"^i{MAX_3_WORDS}be like$",
        fr"^i live in$",
        fr"^{MODAL}$",
        fr"^({MODAL}|{WH}('s)?|do|does|did|have|has|had) (i|you|someone|the|a|an|many|few)$",
        fr"^({MODAL}|{WH}('s)?) {WH}$",
        fr"^{MAX_3_WORDS}i would recommend( to)?( you)?( to)?( (watch|watching))?$",
        fr"^{MAX_3_WORDS}i would recommend( you)?( (is|are|would be))?$",
        fr"^{MAX_3_WORDS}i would recommend the{MAX_1_WORDS}(called|named)$",
        r"weather in$",
        fr"^{START}{BE_VERBS}(you|he|she)$",
        fr"^{START}({BE_VERBS} )?called$",
        fr"^{START}(it's|he's|she's|i'm|(it|{MY_THINGS}) {BE_VERBS}) ({FRONT_MOD}?({PPL}|{THINGS})?{MAX_3_WORDS} )?called$",
        fr"^{START}{QUESTION_START}?{FRONT_MOD}?({PPL}|{THINGS}){MAX_3_WORDS}({BE_VERBS} )?called$",
        fr"^the name of {FRONT_MOD}?({PPL}|{THINGS}) {BE_VERBS}$",
        fr"^{MAX_6_WORDS}a (good|nice|bad|terible)$",
        fr"^i{MAX_4_WORDS}going to$",
        fr"^{MAX_4_WORDS}(my|your|his|their)$",
        fr"^{MAX_4_WORDS}(give me|know more) about$",
        fr"^{MAX_1_WORDS}talk about$",
        fr"^my{MAX_6_WORDS}{BE_VERBS}$",
        fr"^{MAX_3_WORDS}my name's$",
        fr"^{MAX_3_WORDS}can you say$",
        fr"^{MAX_1_WORDS}let's say(s)?( else)?$",
        fr"^{MAX_2_WORDS}(i|you){MAX_1_WORDS}(like|have) to$",
        fr"^{MAX_2_WORDS}i (don't|do not){MAX_1_WORDS}(like|have)$",
        fr"^(i am called)$",
        fr"^{START}{MAX_4_WORDS}(i|we){MAX_2_WORDS}((have to|gotta|gonna) )?go with$",
        fr"^{START}{MODAL}(i|we)((have to|gotta|gonna) )?go with$",
        fr"^(oh oh|la la|ho ho|blah blah|ba ba)$",
        fr"^{MAX_4_WORDS}(gonna|wanna)$",
        fr"^{MAX_4_WORDS}wondering$",
        fr"^{MAX_4_WORDS}supposed to$",
        fr"^{MAX_4_WORDS}going to$",
        fr"^{MAX_4_WORDS}something about$",
        fr"^((do you know)|(how do you))$",
        fr"^{MAX_3_WORDS}{WH} (is|are|was|were|can|to)$",
        fr"^{MAX_6_WORDS}{BE_VERBS} there( any)?$",
        fr"^i{MAX_1_WORDS}for$",
        fr"^{MAX_3_WORDS}go for$",
        fr"^{MAX_3_WORDS}i'm$",
        ])
    ,
    "change_topic": r"((can|could) (we|you) (please )?|((\blet's\b|\blet us\b|\blets\b)\s))?((change|get off|stop talking about (the )?)(\b(entertainment|movies|movie|travel|book|books|literature|sport|sports|news|video game.|games|game|music|animals|animal|technology|holiday|obsecure holiday|ted talk)\b( please)?$))|(?<!not to )change (the )?(conversation|(a )?topic|subject)|start over|different (topic|subject)|switch.* topic|^go back (?!to bed)|main menu|next topic|new topic|more topics|new subject|another topic|another subject|other topic|second topic|something else$|anything else$|something different|another thing|move on|^i do next|last topic|some other things",

    "get_off_topic": r"((can|could) (we|you) (please )?|((\blet's\b|\blet us\b|\blets\b)\s))?((get off|stop( talking)?( about)?|i (don't|do not) (want|wanna) (to )?talk( with you)?( about)?|(i'm|i am) tired of talking( about)?|^i (don't|do not) like|^(i'm|i am) not (into|interested in)) (the )?(\b(entertainment|movies|movie|travel|traveling|book|books|reading|literature|sport|sports|news|video game(s)?|games|game|music|musics|animals|animal|technology|holiday|obsecure holiday|fashion|this|that|it|them)\b( please| anymore)?$))",

    "req_topic_jump": "|".join([
        rf"({LETS}\s*{TALK_ABOUT}|(\bi\b)\s*({WANT_TO}|love to|like to)\s*({TALK_ABOUT}|know|ask|learn))(?!( something| anything|$))",
        rf"({CAN_WE}|{CAN_YOU}) ({TALK_ABOUT}|switch to|{TELL_ME}(?! more))",
        rf"^{DO_YOU_WANT_TO} ({TALK_ABOUT}|switch to|{TELL_ME}(?! more))",
        rf"^{TALK_ABOUT}",
        rf"do you (know|have).* (about|on)\b",
        rf"{TELL_ME}(?! more).* about",
        rf"tell me some\b",
        rf"^(i'm|i am) into\b",
        rf"^are there any good\b",
        rf"what is today's news",
    ]),
    "question_topic_jump": rf"^do you like\b|^do you know\b|^have you heard\b|^{DO_YOU_WANT_TO}\b|^{CAN_WE}\b|\b(wondering|wonder) if you have\b",

    "req_not_jump": r"((\blet's\b|\blet us\b|\blets\b) not\s*(talk|chat)|(\bi\b)\s(don't|do not|did not|didn't)\s*(want to|expect to|would like to|like to|love to|wanna)\s*(talk(?!( to\b))|chat|know|ask|learn))",

    "req_topic_jump_single": r"^\b(entertainment|movies|movie|travel|book|books|literature|sport|sports|news|video game.|games|game|music|animals|animal|technology|holiday|obsecure holiday|fashion|food|foods)\b( please)?$",

    "select_wrong": r"not .*i .*(talk|chat|say|said|want|wanna)",

    "clarify": "|".join([
        fr"^(({WH}|{START}|{SORRY}|{DEVICE}) )?{WHAT_YOU_SAID}( before that)?$",
        fr"^(({WH}|{START}|{SORRY}|{DEVICE}) )?{MAX_3_WORDS}what (do|did) you{MAX_1_WORDS}say{MAX_3_WORDS}$",
        fr"^(({WH}|{START}|{SORRY}|{DEVICE}) )?{SAY_AGAIN}{MAX_3_WORDS}{MAX_3_WORDS}$",

        fr"^(({WH}|{START}|{SORRY}|{DEVICE}) )?((can|could) you )?ask({QUESTION}|{MENTIONED})?{AGAIN}$",
        fr"^(({WH}|{START}|{SORRY}|{DEVICE}) )?repeat({QUESTION}|{MENTIONED}| ?\byourself\b ?)?{AGAIN}?( please)?$",
        fr"^(({WH}|{START}|{SORRY}|{DEVICE}) )?repeat({QUESTION}|{MENTIONED}| ?\byourself\b ?){AGAIN}{MAX_3_WORDS}$",
        fr"^(({WH}|{START}|{SORRY}|{DEVICE}) )?what('s| (is|was))({QUESTION}|{MENTIONED}){AGAIN}$",
        fr"^(({WH}|{START}|{SORRY}|{DEVICE}) )?what('s| (is|was))({QUESTION}|that)$",
        fr"^(({WH}|{START}|{SORRY}|{DEVICE}) )?what('s| (is|was))({QUESTION}|{MENTIONED}) repeat$",
        fr"^{MAX_3_WORDS}{START}(?<!i'm gonna )(go ahead and )?repeat( ({QUESTION}|{MENTIONED}){MAX_2_WORDS}$|$)",

        fr"^(({START}|{SORRY}|{DEVICE}) )?{WH} (did|do) you {SAY_AGAIN}{MAX_3_WORDS}{MAX_3_WORDS}$",

        fr"^(({WH}|{START}|{SORRY}|{DEVICE}) )?{MAX_3_WORDS}(i )?(can't|couldn't|did not|didn't) hear( you)?$",
        fr"^(({WH}|{START}|{SORRY}|{DEVICE}) )?{MAX_3_WORDS}(i )?(can't|couldn't|did not|didn't) (hear|catch)({QUESTION}|{MENTIONED})$",
        fr"^(({WH}|{START}|{SORRY}|{DEVICE}) )?{MAX_3_WORDS}what are you asking{MAX_3_WORDS}$",

        fr"^let's try{AGAIN}$",
        fr"^try{AGAIN}$",
        fr"^{AGAIN}( please)?$",

        fr"^{MAX_3_WORDS}pardon{MAX_3_WORDS}$",
        fr"^what{MAX_3_WORDS}and what$",
        fr"^((have|do) )?what$",
        fr"({MODAL}|called) you{MAX_1_WORDS}(repeat|{SAY_AGAIN})(?! my name)",
        ])
    ,
    "clarify_misunderstood": "|".join([
        fr"^({SORRY}|please |{WH} )?i don't understand{MAX_3_WORDS}(you{MAX_1_WORDS}said{MAX_1_WORDS})?$",
        fr"^can't understand that$",
        fr"^{START}?can you{MAX_1_WORDS}explain{MAX_6_WORDS}$",
        fr"^{START}?explain to me{MAX_6_WORDS}$",
        fr"^{START}?explain{MAX_3_WORDS}$",
        fr"^{START}?explain( (again|yourself))?$",
        fr"^{START}?would you like to explain{MAX_3_WORDS}$",
        fr"^{START}?can you explain that{MAX_3_WORDS}$",
        ])
    ,
    "terminate": "|".join([
	r"((don't|dont|not|do not).* (talk|chat) anymore$)",
	r"((let's )?(talk|talking|chat) (about this |it )?later)$",
	r"(^(alexa )?((power off|off|power down|cancel|(i'm|okay) done|leave me alone|dismiss|^can we stop talking)( for a little while)?( please)?$|you can turn off|disconnect|\bi\b.* leave)( please)?$)",
	r"(^(be|alexa) quiet$)",
	r"(cancel.* chat)",
	r"(chat is over)",
	r"(conversation is over)$",
	fr"{MODAL} you (stop|turn off)( now)?( please)?",
	r"(don't|dont|not|do not).* talk to me",
	r"(exit|get out of) social (mode|bot)",

        fr"^thank you good night$",
        fr"^time to go to bed$",
        fr"^(how about )?you need to go to bed$",
        fr"^{START}can we pick up{CHAT}tomorrow$",
        fr"^{START}{MAX_1_WORDS}we{MAX_1_WORDS}talk{MAX_2_WORDS}tomorrow$",
        fr"^{START}go ahead and turn yourself off{MAX_1_WORDS}$",
        fr"^{START}i'll chat with you another time$",
        fr"^{START}i'm calling it a night{MAX_2_WORDS}$",
        fr"(i (think|guess) )?(i|we){GOING_TO_BED}{MAX_2_WORDS}$",
        fr"^{START}(maybe )?it's{MAX_1_WORDS}time (for me )?to go to bed{MAX_1_WORDS}$",
        fr"^{START}(i (guess|think) *)?(it's )?time to go to bed$",
        fr"^{START}it's been really nice talking to you$",
        fr"^{START}(i|we){MAX_2_WORDS}say good night{MAX_1_WORDS}$",
        fr"^{START}(i'm|we're) gonna stop chatting$",

	r"(talk to you tomorrow)$",
	r"(want to stop talking)$",
	r"^((please )?stop talking( please)?)$",
	r"^(conversation over)$",
	r"^(end conversation)$",
	r"^(end this|end (this )?chat|stop conversation)$",
	fr"^{START}(good bye|goodbye)$",
	r"^(let's stop talking)$",
	r"^(let's|let us) call .*a day$",
	r"^(stop chatting)$",
	r"^(stop stop stop)$",
	r"^(turn|shut) off( please)?$",
	r"^(we|you) can stop$",
	r"^can you( please)? stop talking",
	r"^done$",
	r"^have a nice day$",
	r"^i don't want to (talk|listen) to you$",
	fr"^{START}(i )?(need to|have to|gotta) go( have a nice day)?$",
	fr"(?<!do )you need to stop$",
	fr"i (need|have) to leave{MAX_2_WORDS}$",
	fr"i need( you)? to stop( this conversation)?( right)?( now)?( please)?$",
	fr"{START}{MAX_2_WORDS}(?<!do )(i|we|you){MAX_1_WORDS}(?<!don't )(need|have) to stop({MAX_1_WORDS}(chatting|talking)(?! about)| now|$)",
        fr"it's time to end( this conversation)?{MAX_2_WORDS}$",
	r"^let's not talk(?! about)\b",
	r"^no more conversation( please)?$",
	r"^quit$",
	r"^stop (social )?bot",
	r"^stop$",
	r"^turn.*\boff$",
	r"cancel$",
	r"disable social",
	fr"end{CHAT}",
	r"good chatting with you",
	r"got to go$",
	r"i (\b\w*\b )?have to go$",
	r"i gotta go$",
	r"i was finished chatting",
	r"i'm done chatting",
	r"i'm done talking",
	r"i'm done$",
        r"i'm done with you",
	r"it's my bedtime",
	r"quit social",
	r"see you (later|tomorrow|soon|again)$",
	fr"shut up$",
	r"social bot off",
	r"talk( to you)?( again)? later",
	r"we( are|'re) done( here)?",
        fr"(can we|i{LIKE_TO}|how do i) (end|stop){CHAT}",
        fr"^{START}i want you to close$",
        fr"^{START}{MAX_3_WORDS}i have to sleep{MAX_3_WORDS}$",
        fr"^{START}{MAX_3_WORDS}stop talking to me{MAX_3_WORDS}$",
        fr"^{START}{MAX_3_WORDS}i'm going to go{MAX_3_WORDS}$",
        ])
    ,
    "say_thanks": r"good job|great job|you (gonna|will|would) win|love you|thank you|you.* amazing|^you.* intelligent|(that's|thats) funny|make.*me laugh|laugh laugh|\bha\b|i.* into you|blow.* my mind|proud of you",

    "say_bio": r"(.*)(\bwhat\b)(.*)(\byou\b)(.*)(\bremember\b|\bknow\b)(.*)(\babout\b)(.*)(\bme\b)(.*)|(.*)(what)(.*)(your)(.*)(\bname\b)|(can|lets)(.*)\b(talk|chat|discuss)\b(.*)(?<!(\bto\b ))\b(me|myself)\b(.*)|\bwhat.* your name\b|\b(tell|talk|chat|say)\b .*about \b(you|each other|yourself)\b|who are you$|introduce yourself|get to know each|\b(what|your) (abilit|skill)\b|what.* you specialize|what you are$|i don't know you$|\bhi hi$|questions.* you answer|\bdon't you have a name\b",

    "req_more": "|".join([
        r"\btell me more\b",
        fr"(tell|give) me another{MAX_2_WORDS}$",
        fr"what('s| is) it about",
        r"\bone more\b",
        r"^another one\b",
        r"(know|hear|listen to).*more(| please)",
        r"what (else|other) do you (have to tell me|know about)",
        fr"^({CONTINUE})$"
    ]),

    "req_time": r"what time (is it|now)|what.* the time|date today|what day|today's date|what date|tell.* the time",

    "req_task": "|".join([
        r"(^(alexa )?(lights|light) (off|on))(\b|$)",
        r"(^(alexa )?\bcall\b)(?! (me|of|duty|you|it))",
        r"get.* phone",
        r"\b(set|put).* timer\b",
        r"order \bme\b",
        r"^pair (?!of)",
        fr"^bluetooth$",
        fr"^{START}{MAX_3_WORDS}{BLUETOOH_VERB} (on|off|to|from|for|of)?{MAX_1_WORDS}bluetooth",
        fr"^{START}{MAX_3_WORDS}{BLUETOOH_VERB}{FRONT_MOD}bluetooth{MAX_1_WORDS}(on|off|to|from|for|of){MAX_3_WORDS}$",
        fr"^{START}{MAX_3_WORDS}(on|off|to|from|for|of) bluetooth$",
        fr"^{START}{MAX_3_WORDS}bluetooth{MAX_3_WORDS}you",
        fr"^{START}{MAX_3_WORDS}your{MAX_3_WORDS}bluetooth",
        fr"^{START}{MAX_3_WORDS}bluetooth (setting|device)s?",
        fr"{BLUETOOH_VERB}{MAX_3_WORDS}your{MAX_3_WORDS}bluetooth",
        fr"you as{FRONT_MOD}bluetooth",
        r"to do list",
        r"^(call my)",
        r"reminder$",
        r"my calendar$",
        r"^(alexa (order|unlock|connect))",
        r"^(order|unlock|connect)",
        r"open up",
        r"volume (up|down)",
        r"turn (up|down) the volume",
        r"show.* picture",
        r"close the",
        r"phone call",
        r"pairing mode",
        r"talk.* faster",
        r"talk slow",
        r"talk.* soft",
        r"speak.* soft",
        r"^audible$",
        fr"louder$",
        fr"louder please",
        fr"be louder",
        fr"talk{MAX_2_WORDS}louder",
        fr"speak{MAX_2_WORDS}louder",
        r"\bopen (the )?(amazon|spotify)\b",
        r"\bincrease volume\b",
        r"^play.* news\b",
        fr"(are|can) (i|you) (connect|talk|plug|pick)(ed)? {MAX_3_WORDS}fire ?stick",

        fr"^{MAX_2_WORDS}{LIGHT} ((on|off) ?(on|off|stop)?)$",
        fr"^{MAX_2_WORDS}(put|turn) ((on|off) )?{LIGHT}$",
        fr"^{MAX_2_WORDS}(put|turn) {LIGHT}(on|off)?(\b|$)",
        fr"you('re going)?( to)? (put|turn) ((on|off) )?{LIGHT}\b",
        fr"light(s)? (on )?{MAX_2_WORDS} percent",

        fr"^(switch|turn|connect) (on|off|up|to) (my )?{MAX_2_WORDS}$",

        fr"^can you{MAX_3_WORDS}(a book)?{MAX_2_WORDS}my kindle( book)?{MAX_3_WORDS}$",
        fr"^(play|get){MAX_3_WORDS}my kindle{MAX_2_WORDS}$",

        fr"^(play )?{SHOPPING_LIST}{MAX_1_WORDS}$",
        fr"\b(add|ask|put|) (to|on|in) {SHOPPING_LIST}\b",
        fr"\b(start|show|read|make|change) (me )?{SHOPPING_LIST}\b",
        fr"\b(what's|what (is|are))( in )?{SHOPPING_LIST}\b",
        fr"\btalk{MAX_2_WORDS}{SHOPPING_LIST}\b",
        fr"\bshow (me|may|us) (the|some|){MAX_1_WORDS}(movie )?trailer(s)?\b",

        fr"^wake me up$",
        fr"^wake me up ((today|tomorrow) )?(at|before|to|with){MAX_3_WORDS}(a.m.|p.m.|in the morning)?( (thank you|thanks|okay))?$",
        fr"\b(can|could|will|make sure) you wake me up\b",
        fr"\bare you gonna wake me up\b",
        fr"\b(need )?you (need )?to wake me up\b",
        fr"\bset an alarm to wake me\b",
        r"^set alarm",

        r"change (the|my) (tv )?(to )?channel( to)?",
        r"turn (on|off) ((the|my) )?tv",
        fr"^can you read{MAX_6_WORDS}to me$",
        fr"can you increase the volume",
        fr"{DEVICE} put{MAX_2_WORDS}on$",
        fr"{DEVICE} turn{MAX_3_WORDS}on$",
        fr"^{START}{DEVICE}i want to listen to music",
        fr"^could you play me( some)?{MAX_3_WORDS}music",
        fr"^can we listen to( some)?{MAX_3_WORDS}music",
        ])
    ,

    "req_task_special": r"\bopen (the )?amazon\b|\bincrease volume\b",

    "req_play_game": r"(^alexa)?(((lets|let's) play)|((could|can) (you|we) play)|(would you like|do you want) to play) (a game|games)",

    "req_play_music": r"(^(alexa |please )?play)|((lets|let's) play)|((could|can) (you|we) (play))|(^(alexa )?resume)|(^(alexa )?((can|(i)? want) you (to )?)?sing (for )?(me )?(a song)?( please)?)|\bplay something\b",

    "req_location": r"nearest|closest|find .* nearby|where to go|what.*address|how.* get there",

    "complaint": r"\bmisunderstood\b|you.* glitching|you.* annoying|give.* another bot|how terrible.* you|got.* wrong|not.* answer|\bmisspell\b|not listening|not what i was asking|you don't listen|out of social|listen to me|shut.* up|shut.* off|(?<!government).shut.* down|shut your mouth|that.* enough|terrible conversationalist|forget.* it|already said|already asked|mentioned|no .* sense|doesn't .* sense|are you sick|hate you|angry.* you|go to hell|kick your ass|you.* deaf|you.* hoe|you.* forgetful|you.* trash|you.* disappointing|you.* crap|you.* garbage|you're a loser|you.* filthy|what the hell|what the heck|piss|creepy|socially awkward|offended me|offensive|what.*wrong with you|you.* more work|you suck|you.* crazy|you.* scaring|you.* rude|you.* lost|your dirty mouth|^stupid|you.* fired|you.* mistake|you.* limited|why do you keep|you.* idiot|you.* confused|drive me nut|my nerve|you.* (stupid|not smart)|\bme\b \bmad\b|you.* boring|you.* jerk|you.* retarded|you fail|me uncomfortable|already (asked|told)|you.* insane|irritating me|answer my question|you die|i.* confused|stupid question|\bi\b.* \bmad\b|you (don't|do not|didn't|did not) understand|you.* dumb",
    "adjust_speak": r"talk.* too much|too much information|too many question|you.* \bugly\b|you always say|you keep saying|cut me off|be right back|you (spoke|speak).*\bfast\b|talk too.* much|(speak|talk) slower|you.* too loud|you.* noisy|zip.* mouth|bad grammar|grammar sucks",

    "reset": r"^reset$",

    # TODO: ------- move this to topic section ---------------------------
    "need_comfort": r"\b((i am)|i'm|i feel|i have been|i've been)\b((?!\bnot\b|\bmy\b|\byou\b).)*(\bsad\b|\b(feel|feeling) bad\b|\blonely\b|\bdepress\b|\bdepressed\b|\bbullied\b|\bugly\b|\bstressed\b|\bterrible\b|\bhorrible\b|\bhurt\b|\bfrustrated\b|\brejected\b|\bmad\b|\btired\b((?!of).)*$|\bcranky\b|\bexhausted\b|\bawful\b|\bdie\b|\bworried\b|\bsick\b|\bstressing out\b|\bfeeling down\b|\blittle down\b|\blittle bit down\b)|depression|\b(i|i am| i'm)\b((?!(\bdon't\b|\bdo not\b)).)*(gonna|going to|wanna|want to) (cry|kill|suicide(?! squad)|\bdie\b|\bhurt myself\b|\bcut myself\b)|bad day|tough day|rough day|awful day|(broke|break) up with|\bi\b.* bipolar|got fired|suicide(?! squad)|(killing|kill) my|^((?!(\bdon't\b|\bdo not\b)).)*hate (my|me)|talk.*my feelings|i( | want to )talk about.*something personal|need help|\b(my|i)\b.*(got|was) (hit|killed)|have been sick|how to handle anxiety|\bmy life\b.*\bhard\b|^((?!(\bdon't\b|\bdo not\b)).)*kill me|\b(i've|i have)\b((?!not).)*(\bbeen\b).*\bsick\b|^((?!(\bdon't\b|\bdo not\b)).)*(talk|tell|share).*(my|i'm|i am) feeling|^((?!(\bdon't\b|\bdo not\b)).)*(\bcut\b|\bhurt\b).*(\bmy\b) (\bwrist\b)|(\bi'm\b|\bi am\b|\bme\b) (\bsad\b)|^(please )?help me$|^(i'm )?crying|^(i have )anxiety"
}

# ================= TOPIC INTENTS =================


TOPIC_CORONAVIRUS = r"\b(corona virus|pandemic|coronavirus|virus|corona|covid|nineteen)\b"

topic_re_patterns = {
    # ------------- Module level Intents By topics----------------------
    "topic_phatic": r"(what's|whats|what) up|how was yours|talk to me$|good night|good morning|got up|good afternoon|good evening|(doing|going) (well|fine|good|great)|not bad|i'm fine|nice to meet|how are things|my day is",

    "topic_profanity": profanity_regex,

    "topic_controversial": r"\bgun(s)?(?!( on|in))\b|\bdrug\b|get high|\bsmoke\b|weed|alcohol|gay|queer|kkk|nazi|racist|jews|profanity|what race|your race|abortion|religion|netflix and chill|\bhorny\b|pussy|about women|about woman|about girl|sugar tit|sexuality|adult movie|porn|get laid|blowjob|lick my|\bcock\b|\b(rape|rapes|raped|rapist)\b|intercourse|\bslut\b|stripper|booty|boob|\bvirgin\b|hooker|suck your|bad word|curse word|\bass\b|orgasm|blow job|give.* blow|blow me|\bcum\b|\bbutt\b|(want|how) to (kill|maim)|hide.* body|dead body|border wall|policy|masturbat|fuck|stock.* invest|cults|\bgamble\b|\bgambling\b|\btrade.* stock(s)?\b",
    # "topic_todayfunfact": r"you (learn|learned)|flash news|news today|something new|something interesting$|new today|five thirty eight|npr|about alexa|about echo|subject to talk",

    "topic_movietv": r"\b(oscar|venom|deadpool|(the )?joker)\b|\bstar( )?war(s)?\b|\b(watching|watch).*(movies|movie|tv|show(s)?)\b|movie|tv show|cartoon|talk show|film|marvel|wall-e|\b(actor|actors|actress|actresses)\b|\b(watch|watching) tv\b|\btommy wiseau\b|\boscars\b|\bavengers\b|\bkorean drama\b|\bnetflix\b|\bparasite\b|\bgame of thrones\b|\bnineteen seventeen\b|\b(watch|watching) television\b|\btelevision show(s)?\b|\bharry potter\b|\bthe lion king\b|\bhobbit\b|\bstarwar\b|\bstar war(s)?\b|\bchronicles of narnia\b|\bspider( )?man\b|\biron( )?man\b|\bking kong\b|\bstar trek\b|\btoy story\b|\bbatman\b|\bdog's way home\b|\bcomedian(s)?\b|\banime\b|\bsonic the hedgehog\b|\bhogan's heroes\b|\blast friday\b|\btv\b",

    "topic_book": r"\bbook\b|\bbooks\b|\bbible\b|\bpoetry\b|\bliterature\b|\b(reading|to read)\b(?!(.*politic|.*news\b| it\b))|\bwriter\b|\bauthor\b|\bscience fiction\b|\baudiobook(s)?\b|\ballegory of the cave\b|^read$",

    "topic_music": r"rock and roll|(listen|listened) to(?! (me|you|more))|\bsong\b|\bsongs\b|\bsing\b|music|instrument|\bband\b|beatle|singer|album|lyrics|concert|spotify|audible|\bsinging\b|\bviolin\b|\bguitar\b|\brock(?! climb)\b|\bblue face\b|\bplaylist\b|\bmadonna\b|\bblueface\b|\bariana grande\b|\bb\. t\. s\.|\btop country\b",

    "topic_sport": r"patriots|\bsport|(?<!jiminy )cricket|football|tennis|nba|n\. b\. a\.|baseball|softball|nfl|n\. f\. l\.|boxing|gymnastics|score|\bski\b|soccer|golf|volleyball|basketball|bowling|olympic|hockey|astros|world cup|(work|working) out|workout|fitness|\b(like|love) to (run|hike)\b|^(gravitational)\bpool\b|\bskating\b|\bskate\b|\bskateboard\b|\bjogging\b|\bmarathons\b|\bexercise\b|\bsnowboard\b|\bswim\b|\bswimming\b|\bsky diving\b|\bshoot\b|\bshooting\b|\bwrestle\b|\bdive\b|\bweightlifting\b|\bsoftball\b|\bdodge ball\b|\bformula one\b",

    "topic_outdoor": r"\b(water park)\b|\bbikes\b|\bbike\b|\bbiking\b|\bcycle\b|\bcycling\b|\bbicycle\b|\bbicycles\b|\boutdoor\b|\bsurfing\b|\bfishing\b|\bkayaking\b|\bgo to (the )?park\b|\bplay outside\b|\bplay ground\b|\bskiing\b|\b(going|go) for a walk\b|\btake .*walk(s)?\b|\bhike\b|\bhiking\b|\bnature\b|\b(ski|skiing)\b",

    "topic_newspolitics": "|".join([
        r"\b(news|trending|headlines|flash briefing|new in the world|current (event|events)|what.* going on (?!with))\b",
        r"\b((happening|new)? in the election)\b",
        r"\b(politics|democrat|republican|senate|governor|government|white house|president|congress|constitution|liberals|libtard)\b",
        r"\b(trump|obama|dianne feinstein|bill clinton|hillary clinton|paul ryan|mcconnell|kim jong un)\b",
        r"\b(terrorist|september eleventh|conspiracy|employment rate|approval rating|hurricane|korean summit|illegals|caravan)\b",
        r"\b(border)\b",
        r"\b(immigrants|migrants|migration|refugee(s)?)\b",
        r"\b(trade war|iran\b.*\bwar|world war)\b",
        r"\b(stormy daniels|michael moore)\b",
        r"\b(trade|econom(y|ics)|(?<!your) business|tax)\b",
        r"\b(russia|syria|mexico|florence|guatemala|south america|iran)\b",
        fr"{TOPIC_CORONAVIRUS}",
        r"\bclimate change\b",
        r"\bprotest\b|\bgeorge floyd\b|\bcurfew\b",
    ]),

    "topic_coronavirus": fr"{TOPIC_CORONAVIRUS}",

    "topic_entertainment": r"entertainment|gossip|hollywood|celebrity|celebrities|social media|instagram|facebook|tweet|snapchat",

    "topic_fashion": r"fashion|vogue|(echo look)|(fashion fund)|\bmakeup\b|\bkylie jenner\b|\bshopping\b|^shop\b|\bsnowshoe\b|\bthe mall\b|\bhigh heels\b|\bclothes\b|\bkardashian|\bboots\b|\bfragrance|\bperfume",

    "topic_health": r"\bi\b diarrhea|symptom|get sick|\bflu\b|\bi\b.* pain|\bi\b.* sick|i broke my |my .* hurt|\bi\b.*headache|stomach|(have|got|caught).*(cold|cough|cancer)",

    "topic_weather": r"weather|temperature|how cold|how warm|how hot|(is|was|will) it ((be|is|was|going to) )?(cold|\brain\b|snow|hot|cloudy|sunny)",

    "topic_holiday": r"holiday|special day|\bgift\b|halloween|thanksgiving|christmas|santa claus|new year|(trick or treat)|(special today)|(obscure all day)|(obscure all day)",

    "topic_travel": r"travel|nature|road trip|camping|tourism|vacation|go.* beach|national park|flight|\bbeen to\b|\bsan diego\b|\bon a trip\b|\bmove to\b",

    "topic_food": r"restaurant|\bfood|\brecipes|\bnoodle|\bmilk|\bspaghetti|soup|cook|\bbake\b|pizza|sushi|cookie|bacon|\bpie\b|hot dog|burger|cheese|chocolate|lunch|dinner|supper|breakfast|drink|beverage|coffee|cafe|\bbeer\b|wine|\bpie\b|fries|\bice cream\b|\beat\b|\bgingerbread house(s)?\b|\bfruit(s)?\b|\bbbq\b|\bsubway\b|\bbaking\b|\bchipotle\b|\beat(ing)?$|^eight$|\bhungry\b|\brecipe\b",

    "topic_techscience": r"robotics|nano|\bbio\b|anthropology|physics|chemistry|medicine|paleontology|astronomy|\btech\b|\bscience\b|a\. i\.|technology|about robot|artificial intelligence|\ba\b \bi\b|\bai\b|astronomy|biology|engineering|physics|biotech|energy|chemistry|transport|nanotech|about medicine|about space|\bipad\b|\bipads\b|\bplanet\b|\b(?<!on )(the )?earth\b|\bsiri\b|\buniverse\b|\bmineralology\b|\bspaceships\b|\belectronics\b|\bmath\b|\bcomputers\b",

    "topic_psyphi": r"psychology|happiness|meaning of life|philosophy",

    "topic_election": r"won.*(election|midterms|race|senate|house)|results .* (election|midterms)|election|midterm|election day|midterm|trump|where .* vote|vote|register to vote|poll|election day|voting|register to vote|polling|november (six|6)|trade|economy|immigration|economics|election|supreme court nomination|(k|c)avanaugh|healthcare|(universal|public|free) healthcare|single-payer|ted cruz|beto|o('|\s)?rourke|texas senate|texas election|senate|supreme court|senate judicial|(brett )?(k|c)avanaugh|christine (blasey )?ford|homelessness|jobs|stock|economy|nafta|tariff|trade|trade war|abortion|roe .* wade|women vote|daca|DACA|border|wall|mexico|illegals|immigrants|impeach|impeachment|removal from office|fake news|media|bias|left|liberal(s)?|news|propaganda|enemy of the people|new york times|cnn|\bfox\b|migrants|refugee(s)?|guatemala|south america|mexico|caravan|migration|\bmail\b|bomb|soros|usps|stormy|POTUS|mcconnell|results|election|ballot|midterms|jamal sh.*|senate",

    "topic_finance": r"finance|money|invest|bitcoin|cryptocurrency|stock|mortgage|housing|ethereum",

    "topic_saycomfort": r"\b((i am)|i'm|i feel|i have been|i've been)\b((?!\bnot\b|\bmy\b|\byou\b).)*(\bsad\b|\b(feel|feeling) bad\b|\blonely\b|\bdepress\b|\bdepressed\b|\bbullied\b|\bugly\b|\bstressed\b|\bterrible\b|\bhorrible\b|\bhurt\b|\bfrustrated\b|\brejected\b|\bmad\b|\btired\b((?!of).)*$|\bcranky\b|\bexhausted\b|\bawful\b|\bdie\b|\bworried\b|\bsick\b|\bstressing out\b|\bfeeling down\b|\blittle down\b|\blittle bit down\b)|depression|\b(i|i am| i'm)\b((?!(\bdon't\b|\bdo not\b)).)*(gonna|going to|wanna|want to) (cry|kill|suicide(?! squad)|\bdie\b|\bhurt myself\b|\bcut myself\b)|bad day|tough day|rough day|awful day|(broke|break) up with|\bi\b.* bipolar|got fired|suicide(?! squad)|(killing|kill) my|^((?!(\bdon't\b|\bdo not\b)).)*hate (my|me)|talk.*my feelings|i( | want to )talk about.*something personal|need help|\b(my|i)\b.*(got|was) (hit|killed)|have been sick|how to handle anxiety|\bmy life\b.*\bhard\b|^((?!(\bdon't\b|\bdo not\b)).)*kill me|\b(i've|i have)\b((?!not).)*(\bbeen\b).*\bsick\b|^((?!(\bdon't\b|\bdo not\b)).)*(talk|tell|share).*(my|i'm|i am) feeling|^((?!(\bdon't\b|\bdo not\b)).)*(\bcut\b|\bhurt\b).*(\bmy\b) (\bwrist\b)|(\bi'm\b|\bi am\b|\bme\b) (\bsad\b)|^(please )?help me$|^(i'm )?crying|^(i have )anxiety",

    # "topic_relationship": r"relationship|marriage|family|(?<!my) friend|girlfriend|boyfriend|\bmom\b|dad|mother|father|daughter|\bson\b|wife|husband|dating|brother|sister|parenting",

    "topic_animal": r"\bpet(s)?\b|\ba pet\b|animal|\bcat\b|\bcats\b|kangaroo|(?<!hot) \bdog\b|\bdogs\b|\bdoggie\b|\bdoggies\b|puppies|dinosaur|\bmeow\b|\bseahorses\b|\bkitties\b|\bkitty\b",

    "topic_game": r"\bfall out\b|assassin's creed|\bgame(s)?(?! of thrones)\b|xbox|fortnite|ps4|play station|\byoutuber\b|titanfall|wii|nintendo|pokémon|pokemon|fornite|minecraft|super mario|call of duty|legend of zelda|league of legends|angry bird|final fantasy|mario cart|smash brothers|geometry dash|fortnite|undertale|roblox|minecraft|overwatch|hello neighbor|\batari\b|^atari$|\besport|\bplaystation\b|\brubik's cubes\b|\bnaruto\b|\bdestiny\b|\banimal jam\b|\brainbow six( siege)?\b|\bmario brother\b|\bbatman game|\bapex\b|^gaming\b|\banimal crossing\b|\bwitcher three\b|\bthe switch\b|\bskyrim\b",

    "topic_storymeme": r"(?<!(that|more) )(\b(story|stories)\b|fairy tale|riddle|quote|meme|poem|life hack|read.* story|(fun|cool) fact(s)?)(?! about)",

    # "say_funny": r"(^(alexa )?\bspell\b)|name \byou\b|your name is|(^\bsiri\b)|talk to (siri|google)|(alexa|hey|ok|okay) google|\bfbi\b|\bcia\b|(^say you)|(^speak in)|you.* lying|change your voice|\bbark\b|\bmeow\b|\bwoof\b|date.* me|go out.* me|go on a date|mak.* love|make out|^you.* hot|you.* bad girl|talk dirty|you.* liar|you.* love \bme\b|can you speak|kiss me|kiss my|kiss you|miss you|missed you|\bhe\b \bhe\b|i like you|fart|^can i call you|unplug.* you|^am i your|marry me|date you|you.* cheat|say .* in|\bin\b.* accent|with.* accent|\bhumor\b|\bjoke\b|jokes|something funny|tell me.* secret|give me.* money|mess.* you|kidding|you.* naughty|want a.* bot|your software|poop|spying on me|you recording|break you|punch you|smarter than you|knock knock|ignor.* you|in the cloud|how smart|pretend to be|act like|cyber brain|how stupid|kill me|tell alexa|destroy you|thinking about you|tell.*a lie|spooky scream|do i live in|my bab(y|(ies))|be my|say.* times|what sound.* make|you self aware|roast me|guess my|call you baby",

    "say_funny": r"^(jokes|joke)$|(i want|i like|tell me|give|talk).*(joke|jokes)|^knock knock$|(what is|what's) the funniest thing",

    "topic_qa": r"how old is|(what's|whats) (?!.*you|it|that|this|what|\bmy\b|next|going)|(what (is|was|are|were)|"
                r"what's|whats|what're) (?!.*you|it|that|this|what|\bmy\b|next|going)|when (is|was) (?!.*you)|"
                r"when's (?!.*you)|where does|what (does|did) (?!.*it|that|this|he|she)|today in history|(what|who|which|how|tell).* in the world|"
                r"who is (?!.*you|\bgood\b|in|he|she|better|that|it|that|this|there|great)|how (tall|high) is|^how rich|"
                r"what does .* mean|what.* highest|how many|who won |when (are|were) \bthe\b|how about the|who invented|"
                r"how big|how long|how tall|do you know how to|alexa how to|how .* can a|how fast is "
                r"divided|where is the|how heavy|^define|what was the|which.* (is|was)|what do you mean (about|by)|history today|do you know who",

    "topic_backstory": r"how are you|how do you do|you.* (would|will|gonna) win|alexa prize|birthday|easter egg|"
                       r"who is your|where.* you|your favorite|your (boyfriend|girlfriend)|where is your|(what's|whats|what is) \byour\b|who's your|"
                       r"^(who|what) (create|created|make|made|build|built|invent|invented|design|designed|develop|developed) you$|"
                       r"how old are you|how old you are|who are you|tell me about you|do you (have|work|want to)|when is your|when do you|"
                       r"do you have|how \w+ are you|what.* you from|what.* you (like|love|hate|dislike)|what (do|did) you (?!(think|know|mean))|"
                       r"how \w+ are you|tell me your|(what .*do you (like|love))|(^(alexa )?do you (like|think|believe))|what.* you (can|could|are)|"
                       r"\b(can|could|do) i\b|have a question|(^help( me)?$)|(\b^are you (a|an)\b)|your thoughts \S+|tell me why you|your name is",

    "topic_others": r"\bcoins\b|\bcar(s)?\b|airplane|airport|(^ping$)|unicorn|motorcycle|panda express|preschool|bathroom|candy|drones|\bcross stitch\b|\bnumbers\b|\bmagic\b|\borigami\b|\btattoo",

    "req_topic": r"(what.* you (want|like|wanna)(?! to))|what can (you|we) talk about$|(^skip$)|(suggest|give) .*topic|what kind of thing|about something|favorite topic|either topic|what.* topic(s)|what.* choice|what.* option(s)|you \bpick\b|you choose|you decide|whatever$|i'm bored|something interesting|what do you have|\banything.* you (want to|wanna|can) talk about\b|what do you want to talk( about)?$|what can you talk( about)?$",

    "besides_topic": r"\b(besides|in addition to|apart from|but|other than|except|something that (is not|isn't)|not talk about|^not)\s*(entertainment|movies|movie|travel|book|books|reading|literature|sport|sports|news|video game.|games|game|playing game(s)?|music|animals|animal|technology|holiday|obsecure holiday|ted talk)$",

    "topic_dailylife": "|".join([
        r"\b(birth)\b",
        r"\b(social media|social-media|socialmedia)\b",
        r"\b(podcast|pod cast|podcasts|pod casts)\b",
        r"\bschool\b",
        r"\bsleep\b|\bsleeping\b|\bnaps\b",
        r"\b(hang|hanging|hang out|hangout) with (my )?friend(s)?\b",
        r"\bfriend\b|\bfriends\b|^play$|^playing$",
        r"(\bwork\b|\bworking\b)(?! (on|with))",
        r"\bdraw\b|\bdrawing\b|\bpaint\b|\bpainting\b|\bart(s)?\b|\bsketch\b|\bsketching\b|\bphotography\b",
        r"\bdance(s)?\b|\bdancing\b",
        r"\b(clean|cleaning).*(house|yard|room)\b|\bdo cleaning\b|^cleaning",
        r"\bwith my kids\b",
        r"\b(play with|\btalk to) my (father|dad|mother|mom|daddy|mummy)\b",
        r"\bfamily\b",
        r"\byoutube\b",
        r"\bphone\b|\btablet\b",
        r"\btoy(s)?\b",
        r"\bmy birthday\b",
        r"^(talk|talking) (to|with) (you|chatbot)\b",
        r"\bgirlfriend\b|\bboyfriend\b|^i (like|love) a (girl|boy)|\brelationship",
        r"\b(stuck|stucking|stay|staying) (at |in )?(my |the )?(home|house)\b|\bquarantine\b",
        r"\bgardening\b|\bgarden\b",
        r"\bclimate change\b",
    ]),
}


# ================= LEXICAL INTENTS =================

lexical_re_patterns = {
    # command in question form
    "command": "|".join([
        fr"{COMMAND_IN_QUESTION_FORM_REQ_TOPIC}",
        fr"({DO_YOU_WANT_TO}|{CAN_WE}|{CAN_YOU}){MAX_1_WORDS} change the (topic|subject)",
        fr"({DO_YOU_WANT_TO}|{CAN_WE}|{CAN_YOU}){MAX_1_WORDS} (repeat|stop)",
        fr"({DO_YOU_WANT_TO}|{CAN_WE}|{CAN_YOU}){MAX_1_WORDS} talk in",
    ]),
    "command_in_question_form_req_topic": "|".join([
        fr"{COMMAND_IN_QUESTION_FORM_REQ_TOPIC}"
    ]),

    "command_in_question_form_no_req_topic": "|".join([
        rf"({DO_YOU_WANT_TO}|{CAN_WE}|{CAN_YOU}) (?!{TALK_ABOUT}|{TELL_ME})"
    ]),

    # ----------- Question Slot Type ----------------
    "ask_back": "|".join([
        r"^(have you)$",
        r"^(what about you)$",
        r"^(how about you)$",
        r"^((what|who)('s| is) yours)$",
        r"^((what|who)('s| is) your favorite one)$",
        r"^(what do you think)$"
    ]),

    "ask_yesno": r"can you|may you|^are you|^do you|^have you|^would you want|(?<!how) do you$|^did you|^you wanna|^do you wanna|^does|^may i|^would you (?!like)|can you$",

    "ask_preference": r"^do you like|what do you want|^do you love|^do you support|^do you hate|what kind of|what type of|would you like|what.* better|which.* better|who.* better",

    "ask_opinion": r"^what do you think|how do you think|what do you like about|what do you love about|what makes you|how do you feel|^how is |^how was|how do you like|why do you like|what kind of| your opinion|your view|your thought|do you know|who do you think|do you believe|what.* best|who.* best|which.* best|which \w+ are you|what.*it like",

    "ask_advice": r"should i |should we |give .* advice|what.* advice|need.* advice|do you know how to|what.* good way to|tips|what.* suggest|how do i|how can i|what.* can i|what .* do i",

    "ask_ability": r"^can you|^could you|will you|teach me|how to|you.* for me|where do you|how do you|are you able to|^how can you",

    "ask_hobby": r"what do you like (?!about)|what do you love (?!about)|what.* your hobby|what.* your favorite",

    "ask_fact": r"what is (?!you)|what are (?!you)|what is (?!my)|what are (?!my)|what's (?!you)|what's (?!my)",

    "ask_info": r"(do you know( anything| something|) about|what do you know about|tell me about \w+)|(^\bbut\b \bwhich\b)",

    "ask_recommend": r"what.* recommend|which.* recommend|do you.* recommend|can you.* recommend|^recommend|give .* recommend|give.* suggestion|have.* suggestion|make.* suggestion|find me",

    "ask_reason": r"^why(?! not)\b|is that why|how (can|could)|^how is that|how do you know|how did you|tell me why|i wonder|what.* reason$",

    "ask_self": r"what.* \bmy\b|about \bme\b|about my",

    "ask_freq": r"how often|how many times",

    "ask_dist": r"how far|what.*distance",

    "ask_loc": r"where|what.* place|what.* address of",

    "ask_count": r"how many (?!times)",

    "ask_degree": r"how much|how long|how old|how tall|how big|how likely|how fast|to what extent",

    "ask_time": r"^when\b|what.* date|release date|what time|what year|how many days.* till|how many days.* until",

    "ask_person": r"^who\b",

    "ask_name": r"what.* name|what.* called|tell.* your name|(what's|whats) your name",

    "ask_user_name": r"(what.* my name)|(know|remember|tell me|repeat|say) my name",

    "ask_leave": r"something else|i don't want to talk about|done|something else|^change|different|this anymore|(do not|don't) care|not interested|boring|leave|i'm good|i'm ok|i'll pass|this sucks|no more|\bexit\b|\bquit\b|enough (news|sports|animals|games|movies|music)",

    # ----------- Answer Slot Type ---------------------------
    "ans_unknown": "|".join([
        r"i (don't|do not|didn't|did not) (know|remember)(?! that)",
        r"no idea",
        r"(maybe|probably|possibly)$",
        r"no (comment|opinion)",
        r"not( \w+)* sure",
        r"\b(unsure|uncertain)",
        r"(hard to say|depends)$",
        r"\b((hard|tough) (one|question))",
        fr"{FORGOT}"
    ]),

    "ans_dont_care": "|".join([
        rf"{MAX_1_WORDS} whatever$",
        rf"don't{MAX_1_WORDS} care{MAX_4_WORDS}$",
        rf"not interested"
    ]),

    "ans_negative": "|".join([
        r"^(no|not really|nope|nah|na)\b",
        r"^wrong$",
        r"isn't",
        r"\b(none|nothing)",
        r"(i'm|i am) (good|okay)(| thanks)$",
        r"i (did|have)(n't| not)",
        r"\bi don't have (any|a|an|one)\b",
        r"never(?! mind)",
        rf"i {DO_NOT} (watch|have)",
        r"i won't",
        r"my \w+ won't",
        r"not interested",
        r"i hate",
        r"i don't like",
        fr"{FORGOT}",
        fr"{DISAGREE}"
    ]),

    "ans_positive": "|".join([
        r"why not|\byes\b|of course|yup|yep$|\bya\b|\bok\b|yeah|okay",
        rf"^{EXCLUDE_NOT}sure",
        r"^please$|please \bdo\b",
        r"^i am$|^i do$|^i have$",
        r"^absolutely$",
        r"if you.*(like|want)( to)?$",
        r"\bcorrect\b|makes sense|one hundred percent",
        r"^uh-huh$",
        fr"{AGREE}"
    ]),

    "ans_continue": "|".join([
        fr"{CONTINUE}"
    ]),

    "ans_prefer":"|".join([
        r"but i prefer",
        r"but my favorite",
        r"but have you",
    ]),

    "opinion_positive": "|".join([
            fr"{POSITIVE_OPINION_ADJ}",
            fr"{POSITIVE_COMMENT_ADJ}",
            fr"{POSITIVE_COMMENT_ADJ_SUITABLE_TO_BE_ECHOED}$",
            fr"^{POSITIVE_ADJ_SUITABLE_TO_BE_ECHOED}",
            r"^(?!.*not ).*(excited|happy)$",
            rf"i (?!don't|didn't|feel|look).*(like|love) (it|that)",
            r"\bha\b",
            # r"\b(?<!\b(i'm|i am|not) )good$\b",
    ]),

    "opinion_negative": "|".join([
        r"\b(suck(|s)|boring|silly|awful|disappointing|terrible|horrible|disturbing|awkward|stupid|fishy|weird|odd|crap)\b",
        r"^(?!.*not ).*(bad)",
        r"\b(fail)\b",
        r"it('s| is) gross",
        r"messed up",
        r"i don't like",
        fr"{DISAGREE}"
    ]),

    "opinion_crazy": "|".join([
        rf"{IT_IS}{MAX_1_WORDS} (crazy|insane)"
    ]),


    "opinion_surprising": "|".join([
        rf"{IT_IS}{EXCLUDE_NOT}{MAX_2_WORDS} (surprising|unbelievable)",
        rf"^{EXCLUDE_NOT}{MAX_1_WORDS} surprised",
        r"^wow$"
    ]),

    "ans_same": "|".join([
        r"\bsame\b",
        r"\bsam\b",
        r"i like .* (two|too)",
        r"me too",
        r"so do i$",
        r"i .* too$",
        fr"{AGREE}"
    ]),

    "ans_like": "|".join([
        fr"i (?!don't|didn't|feel|look|do not|did not).*{LIKE}",
        fr"i('m| am| was) (?!(not|(very )?little) )(interested in)",
        r"(?<!what)('s| is) (one of |)my favorite",
        r"(?<!who)('s| is) (one of |)my favorite",
        r"my favorite.* is",
        r"^(?!.*not ).*into",
        r"i('m| am) more into",
    ]),

    "ans_dislike": "|".join([
        fr"i {DO_NOT}.*{LIKE}",
        fr"i (?!{DO_NOT}).*{DISLIKE}",
        fr"(i('m| am) )?(not|(very )?little) (interested in|into|big on)",
        r"(i('m| am) )?not a\b.*\bfan\b",
    ]),


    "ans_wish": r"i wish|i want|i hope|i need|i would like|i'd like|i wanna",

    "ans_factopinion": r"^i think|^this is|^that is|^that's|^maybe|^it is|^it's|^we are|^he has|^she has|^they are|^it has|^they have|^there is|^there are|^the|i am.* \ba\b|i'm.* \ba\b|it feels|\bi\b.* feel|that sounds|it sounds|i'm \ba\b|i'm \ban\b",


    # ----------- Information Slot Type ---------------------------
    "info_name": r"my (name is|name wrong|name isn't|name is not|names|name's)|call me|change my name|that('s|s| is) not my name",

    "info_age": r"\bi\b.* years old|\bi\b.* year old|i'm .* years old",

    "info_loc": r"i'm from|i am from|\btime\b in |weather in |happening in |happened in |going on in |i'm in (?!love)|i'm in (?!pain)|i live in",

    "info_time": r"monday|tuesday|wednesday|thursday|friday|saturday|sunday|weekend|morning|\bnoon\b|afternoon|evening|\bnight\b|today|yesterday|tomorrow|last year|right now|years ago",
}


class CompiledPattern:

    def __init__(self):
        self.sys_patterns = {}
        self.topic_patterns = {}
        self.lexical_patterns = {}
        # pre-compile system level patterns
        for key, val in sys_re_patterns.items():
            self.sys_patterns[key] = re.compile(val)
        # pre-compile topic level patterns
        for key, val in topic_re_patterns.items():
            self.topic_patterns[key] = re.compile(val)
        # pre-compile lexical level patterns
        for key, val in lexical_re_patterns.items():
            self.lexical_patterns[key] = re.compile(val)

    def get_sys_regex(self, key):
        return self.sys_patterns[key]

    def get_topic_regex(self, key):
        return self.topic_patterns[key]

    def get_lexical_regex(self, key):
        return self.lexical_patterns[key]

