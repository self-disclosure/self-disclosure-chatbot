import json
import logging
import requests
import boto3
from boto3.dynamodb.conditions import Key, Attr

dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
table = dynamodb.Table('Debates')


def get_opinion(utterance):
    threshold = 0
    try:
        headers = {
            'Content-Type': 'application/json',
        }
        data = {
            'text': utterance
        }

        resp = requests.post(
            'http://ec2-35-171-245-158.compute-1.amazonaws.com:8086/opinion',
            headers=headers,
            data=json.dumps(data),
            timeout=0.5)

        ret = resp.json()
        print(ret)
        # NOTE: http code
        if resp.status_code < 300:
            # TODO: handle low confidence score
            if ret['confidence'] > threshold:
                return ret
            else:
                return None
    except Exception as e:
        logging.warning(
            '[DEBATE] post sentence embedding with data err: {}'.format(e))
        pass

    try:
        headers = {
            'Content-Type': 'application/json',
        }
        data = {
            'text': utterance
        }

        resp = requests.post(
            'http://ec2-35-171-245-158.compute-1.amazonaws.com:8088/opinion',
            headers=headers,
            data=json.dumps(data),
            timeout=0.5)

        ret = resp.json()
        print(ret)
        # NOTE: http code
        if resp.status_code < 300:
            # TODO: handle low confidence score
            if ret['confidence'] > threshold:
                return ret
            else:
                return None
    except Exception as e:
        logging.warning(
            '[DEBATE] post sentence embedding with data err: {}'.format(e))
        pass


def ARI(arg):
    # https://en.wikipedia.org/wiki/Automated_readability_index
    sents = len(arg)
    spaces = 0
    words = 0
    for s in arg:
        s = s.strip()
        words += sum(c.isalpha() or c.isdigit() for c in s)
        spaces += sum(c.isspace() for c in s)
    if not spaces or not sents:
        return 0
    return 4.71 * words / float(spaces) + .5 * (spaces / float(sents)) - 21.43


def debate_lookup(title):
    response = table.query(
        # ProjectionExpression= "id,name,score,sub,title,opinion,related",
        IndexName='title',
        KeyConditionExpression=Key('title').eq(title),
    )
    if response['Items']:
        return response['Items'][0]
    return None


def debate_id(url):
    response = table.query(
        # ProjectionExpression= "id,name,score,sub,title,opinion,related",
        # IndexName='title',
        KeyConditionExpression=Key('url').eq(url),
    )
    if response['Items']:
        return response['Items'][0]
    return None


def debate_tunnel(category):
    debate2tunnel = {'cars': 'TECHSCIENCECHAT',
                     'economics': 'NEWS',
                     'education': 'NEWS',
                     'entertainment': 'NEWS',
                     'fashion': 'NEWS',
                     'games': 'GAMECHAT',
                     'movies': 'MOVIECHAT',
                     'music': 'MUSICCHAT',
                     'news': 'NEWS',
                     'philosophy': 'PSYPHICHAT',
                     'places-travel': 'TRAVEL',
                     'politics': 'NEWS',
                     'religion': 'PSYPHICHAT',
                     'science': 'TECHSCIENCECHAT',
                     'sports': 'SPORT',
                     'technology': 'TECHSCIENCECHAT',
                     }

    try:
        cat = debate2tunnel[category]
    except KeyError:
        cat = None
    return cat


def debate_news(category):
    debate2news = {  # 'cars': 'technology',
        'economics': 'business',
        'entertainment': 'entertainment',
        'fashion': 'fashion',
        # 'games': 'GAMECHAT',
        'movies': 'entertainment',
        'music': 'music',
        'news': 'news',
        'politics': 'politics',
        'science': 'science',
        'sports': 'sports',
        'technology': 'technology',
    }

    try:
        cat = debate2news[category]
    except KeyError:
        cat = None
    return cat


if __name__ == "__main__":
    #     question = []
    #     question = "do you think my kids should watch horror movies?"
    #     question = "did you like avengers infinity war?"
    #     question = "is iron man the greatest?"
    #     question = "did you like star trek discovery?"
    #     question = "what is the best james bond movie?"
    #     question = "I think the jurassic park sequels were not very good"
    #     question = "should we allow mexicans in our country?"
    #     question = "is lebron james the best player in the nba?"
    #     question = "should fortnite be allowed in schools?"
    #     question = "do we have to eat vegetables to be healthy."
    #     question = "should black people be allowed to vote?"
    #     question = "should gay marriage be legal everywhere?"
    #     question = "is cnn fake news?"
    #     question = "I think lebron is a loser"
    #     question = "Should I vaccine my child?"
    #     question = "is trump bad?"
    #     print(question)
    #     print(get_opinion(question))
    db = debate_lookup("Is Trump a bad man?")
    print(db)
