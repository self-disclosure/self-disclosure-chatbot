import requests
from urllib.parse import quote
import boto3
from operator import itemgetter
from boto3.dynamodb.conditions import Key, Attr
from itertools import chain
import time
import re
# import string
# from trend import get_categories
dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
table = dynamodb.Table('KG_Data')


def events(entities: list, keywords: list = None, category: list = None, threadDict: dict = {}) -> list:
    """
    :param entities: a list of entities. maximum number of entities is 2 for now
    :param keywords: optional list of keywords to match events. good for filtering events
    :param category: optional list of categories "sport", "world", "us", "business", "health", "entertainment", "sci_tech"
    :return: a list of events between one or two entities
    """
    if not entities:
        return []
    try:
        ents = '+'.join(entities)
        keys = ''
        if keywords:
            keys += '?keywords=' + '-'.join(keywords) + '&'
        cats = ''
        if category:
            if not keywords:
                cats += '?'
            cats += 'category=' + '-'.join(category)
        query = ents + keys + cats
        query = quote(query, safe='+?=')
        r = requests.get('http://35.168.206.27/api/v1.0/events/' + query)
        temp = r.json()
        final = []
        for each in temp:
            if each['toLabel'] == 'PERSON' and each['fromLabel'] == 'PERSON':
                final.append(each)
        if not final:
            final = temp

        js = QA(final)
        threadDict['events'] = js
        return js
    except Exception as e:
        return []


def related_entity(entities: list, filter: str = None) -> list:
    """
    :param entities: a list of entities. maximum number of entities is 2 for now
    :param filter: optional string label to filter entities. can be person, location, organization
    :return: list of related entities
    """
    if not entities:
        return []
    try:
        ents = '+'.join(entities)
        filt = ''
        if filter:
            filt = '?filter=' + str(filter)
        query = ents + filt
        query = quote(query, safe='+?=')
        # print(query)
        r = requests.get('http://35.168.206.27/api/v1.0/relatedentity/' + query)
        return QA(r.json())
    except Exception as e:
        return []


def entity(entities: list) -> list:
    """
    :param entities: a list of entities. maximum number of entities is 2 for now
    :param filter: optional string label to filter entities. can be person, location, organization
    :return: list of related entities
    """
    if not entities:
        return []
    try:
        ents = '+'.join(entities)
        query = ents
        query = quote(query, safe='+?=')
        r = requests.get('http://35.168.206.27/api/v1.0/entity/' + query)
        return QA(r.json())
    except Exception as e:
        return []


def keywords(word_list: list, threadDict: dict = {}, full=False) -> list:
    # full = True
    SCANLIMIT = 40
    start = time.time()
    # word_list = [word for line in word_list for word in line.split()]
    word_list = list(set(chain(*map(str.split, word_list))))
    fe = Attr('keywords').contains(word_list[0].lower())
    for word in word_list[1:]:
        fe = fe & Attr('keywords').contains(word.lower())

    start = time.time()
    response = table.scan(
        FilterExpression=fe,
    )

    resp = response['Items']
    i = 1
    while 'LastEvaluatedKey' in response:
        response = table.scan(
            ExclusiveStartKey=response['LastEvaluatedKey'], FilterExpression=fe)
        resp.extend(response['Items'])
        stop = time.time()
        elapse = stop - start
        if i == SCANLIMIT or len(resp) > 10 and elapse <= .75:
            break
        i += 1

    final = resp
    # SCORING
    # scoredict = {}
    #
    # for i in range(1,len(word_list)+1):
    #     scoredict[i] = []
    #
    # for r in resp:
    #     score = 0
    #     scoredict[score] = []
    #     for w in word_list:
    #         if w in r['keywords']:
    #             score += 1
    #     scoredict[score].append(r)
    #
    # # print(scoredict[2])
    # final = []
    # for s in sorted(scoredict.keys(), reverse=True):
    #     scorelist = scoredict[s]
    #     # print(scorelist)
    #     # break
    #     scorelist = sorted(scorelist, key=itemgetter('modified'), reverse=True)
    #     final.extend(scorelist)

    # FILTER PERSON
    # final2 = []
    # for each in final:
    #     if each['toLabel'] == 'PERSON' and each['fromLabel'] == 'PERSON':
    #         final2.append(each)
    #
    # fullmatch = False
    # for each in final2:
    #     best = scoredict[len(word_list)]
    #     if each in best:
    #         fullmatch = True
    #         break
    #
    # if not final2 or not fullmatch:
    #     final2 = final

    # print(final2[0:5])
    final2 = QA(final)
    # print()
    # print(final2[0:5])

    stop = time.time()
    print(stop - start)

    threadDict['keywords'] = final2
    return final2


def category(cat, threadDict: dict = {}) -> list:
    category2trend = {'business': 'business',
                      'sport': 'sport',
                      'sports': 'sport',
                      'world': 'world',
                      'politics': 'world',
                      'us': 'us',
                      'national': 'us',
                      'nation': 'us',
                      'health': 'health',
                      'entertainment': 'entertainment',
                      'science': 'sci_tech',
                      'technology': 'sci_tech',
                      'science and technology': 'sci_tech',
                      'sci': 'sci_tech',
                      'tech': 'sci_tech',
                      'US': 'us',
                      'finance': 'business',
                      'entertaining': 'entertainment',
                      'gossip': 'entertainment',
                      'international': 'world',
                      'hollywood': 'entertainment',
                      }

    try:
        cat = category2trend[cat]
    except KeyError:
        pass

    fe = Attr('category').eq(cat)
    response = table.scan(
        Select='ALL_ATTRIBUTES',
        FilterExpression=fe,
    )
    resp = response['Items']
    resp = sorted(resp, key=itemgetter('modified'), reverse=True)

    final2 = []
    for each in resp:
        if each['toLabel'] == 'PERSON' and each['fromLabel'] == 'PERSON':
            final2.append(each)
    if not final2:
        final2 = resp

    final2 = QA(final2)

    threadDict['contents'] = final2
    return final2


def QA(response):
    SUBLIMITDESC = 10
    SUBLIMITBODY = 40
    SENTENCEMAX = 300
    SENTENCEMIN = 30
    lnew = []
    for each in response:
        try:

            d = each['data']
            s, n = re.subn(r'[^\w\s\.\']', '', d)
            if n < SUBLIMITDESC and len(s) < SENTENCEMAX and len(s) > SENTENCEMIN:
                each['data'] = s
            else:
                continue
        except KeyError:
            pass
        try:
            revisedsents = []
            sents = each['body']
            for d in sents:
                s, n = re.subn(r'[^\w\s\.\']', '', d)
                if n < SUBLIMITBODY and len(s) < SENTENCEMAX and len(s) > SENTENCEMIN:
                    revisedsents.append(s)
                else:
                    continue  # optional could stop and not put in the body
            each['body'] = revisedsents
        except KeyError:
            pass
        lnew.append(each)
    return lnew


if __name__ == "__main__":

    # t = keywords(['fashion'] ,full=False)
    # print(t)
    # print()
    # t = keywords(['climate', 'change'], full=False)
    # print(t)
    # print(keywords(['sherlock']))
    # t = keywords(['climate', 'change'], full=True)
    print(entity(['Donald Trump']))
    # print(events)
    # keywords(['washington', 'trump', 'poll', 'vladimir', 'russia', 'united', 'americans', 'majority', 'president', 'using', 'enemy'])
    # print(related_entity(['lebron james'], filter='organization'))
    # print()
    # print()
    print(events(['trump'], category=['business', 'world']))
    # print(events(['lebron james', 'kawhi'], keywords=['lakers','randle']))
    # print(contents(['immigration',]))
    # print(category('sci_tech'))
