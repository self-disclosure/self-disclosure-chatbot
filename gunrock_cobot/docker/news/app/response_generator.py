import logging
import signal
import template_manager
from news_automaton import NewsAutomaton
import time
from types import SimpleNamespace


# Define the Logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

template_news = template_manager.Templates.news

required_context = ['text']

# Define the header
Header = {'Content-Type': 'application/json',
          'Accept': 'application/json'}


def get_required_context():
    return required_context


class Timeout():
    """Timeout class using ALARM signal."""
    class Timeout(Exception):
        pass

    def __init__(self, sec):
        self.sec = sec

    def __entenewr__(self):
        signal.signal(signal.SIGALRM, self.raise_timeout)
        signal.alarm(self.sec)

    def __exit__(self, *args):
        signal.alarm(0)    # disable alarm

    def raise_timeout(self, *args):
        raise Timeout.Timeout()


def generate_response(msg):
    """
    resp = requests.post("http://34.237.243.125:5000/generate_response",\
                         data = json.dumps({'text':input_text}),\
                         headers=Header)
    """
    class UserAttributes:
        """
        Old one, recommend to use user_attribute name space directly
        """
        def __init__(self, news_user, last_module, sys_env, usr_name):
            self.newschat = news_user if news_user else {}
            self.last_module = last_module if last_module else ''
            self.sys_env = sys_env
            self.usr_name = usr_name
            # session_id
            # self.session_id = session_id

    user_attributes = UserAttributes(msg['news_user'], msg['last_module'], msg['sys_env'], msg['usr_name'])

    user_attributes_name_space = SimpleNamespace(**{
        "session_id": msg["session_id"],
        "a_b_test": msg["a_b_test"][0],
        "user_profile": msg["user_profile"],
        "template_manager": msg["template_manager"],
        "module_selection": msg["module_selection"],
        "previous_modules": msg["previous_modules"],
        "blender_start_time": msg["blender_start_time"]
    })

    RG = NewsAutomaton(user_attributes_ref=user_attributes,
                       user_attributes_name_space=user_attributes_name_space,
                       input_data=msg)

    response = RG.response()

    user_attributes.newschat['propose_topic'] = None

    result = {}

    result['response'] = response['response']
    result["user_attributes"] = user_attributes_name_space.__dict__
    result["user_attributes"]["news_user"] = user_attributes.newschat

    return result


def handle_message(msg):
    st = time.time()
    # your response generator model should operate on the text or other
    # context information here
    response = generate_response(msg)
    end = time.time()
    logger.info('timer: {}'.format(end - st))
    print('timer: {}'.format(end - st))
    return response
