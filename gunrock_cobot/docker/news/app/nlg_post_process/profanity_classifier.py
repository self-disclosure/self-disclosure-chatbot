import os
import re
import itertools

from nlu.constants import TopicModule


absolute_path = os.path.dirname(os.path.abspath(__file__))
filepath = absolute_path + "/profanity_list.txt"

profanity_phrases = []
with open(filepath, 'r') as f:
    profanity_phrases = [line.strip() for line in f]


class BotResponseProfanityClassifier:

    @staticmethod
    def is_profane(bot_response, topic_module_name=None):
        whitelist = []
        if topic_module_name:
            whitelist = BotResponseProfanityClassifier.get_profanity_whitelist_for_module(topic_module_name)

        # clean the bot_response
        cleaned_response = bot_response \
                .lower().strip() \
                .replace('.', '') \
                .replace("'s ", " ") \
                .replace('"', '') \
                .replace("'", '')
        for phrase in profanity_phrases:
            is_word = all([i.isalpha() or i == " " for i in phrase])
            if is_word:
                regex = r"\b{}\b".format(phrase)
                result = re.search(regex, cleaned_response)
                if result is not None:
                    if whitelist and phrase in BotResponseProfanityClassifier.get_flattened_white_list_words(whitelist):
                        continue
                    else:
                        return True
            else:
                if phrase in cleaned_response:
                    if whitelist and phrase in whitelist:
                        continue
                    else:
                        return True

        return False

    @staticmethod
    def get_profanity_whitelist_for_module(topic_module_name):
        if topic_module_name == TopicModule.SPORTS.value:
            return ["shooting", "balls"]
        if topic_module_name == TopicModule.BOOK.value:
            return ["moby dick", "damn yankees", "murder mystery"]
        if topic_module_name == TopicModule.MOVIE.value:
            return ["suicide squad",
                    "death", "dead",
                    "To Kill a Mockingbird",
                    "stole",
                    "murder mystery",
                    "kill bill the whole bloody affair",
                    "bad fucking",
                    "died"]
        if topic_module_name == TopicModule.TECHSCI.value:
            return ["dead moth"]
        if topic_module_name == TopicModule.RETRIEVAL.value:
            return ["Tom Hanks stole his mannerisms"]

    @staticmethod
    def get_profane_word(text):
        #TODO
        return

    @staticmethod
    def get_flattened_white_list_words(whitelist):
        return [word for phrase in whitelist for word in phrase.split()]


if __name__ == '__main__':
    import time
    start = time.time()
    response = "i watched suicide squad"
    result = BotResponseProfanityClassifier.is_profane(response, TopicModule.MOVIE.value)
    end = time.time()

    print("is profanity: " + str(result))
    print("latency: " + str((end - start) * 1000))