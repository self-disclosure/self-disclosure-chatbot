# ========================================
# File Name: news_kg_api.py
# Created By: Ishan Jain
# Email: ishjain@ucdavis.edu
# Import as : "from news_kg_api import * "
# ========================================

# ================================
# Import Libraries
# ================================
import re
import boto3
from boto3.dynamodb.conditions import Key, Attr
from botocore.config import Config
import random
import logging

# ================================
# Connect to DynamoDB
# ================================
dynamodb_config = Config(
    region_name='us-east-1',
    connect_timeout=2,
    read_timeout=2.1
)

dynamodb = boto3.resource('dynamodb', config=dynamodb_config)

# ================================
# Variables
# ================================

NewsTable = dynamodb.Table('News')
NEtable = dynamodb.Table('News_Entity')
NEHtable = dynamodb.Table('News_Headline_Entity')
MomentsTable = dynamodb.Table('Moments')

# ================================
# Functions
# ================================

'''
:type: Type of news, if extracted : Technology / Politics 
:limit: number of news articles to be extracted from KG (default=3, max=5)

:return value: list of limit number of news articles with following indexes:
        1. Full_Article : String
        2. Entities : list of String
        3. Date : String (YYYY-MM-DD)
        4. Article : List of summarized sentences with their entities and SVOs
        5. Type : String
        6. Headline : String
        7. URL : String
'''


def news_of_type(type_of_news, used_urls, limit=3):
    # res has result of query
    res = []
    # extract news from news table
    try:
        response = NewsTable.query(
            ProjectionExpression="#url,  Headline, #date",
            ExpressionAttributeNames={"#url": "URL",
                                      "#date": "Date", },
            IndexName='Type-Date-index',
            ScanIndexForward=False,
            KeyConditionExpression=Key('Type').eq(type_of_news)
        )

        for j in response['Items']:
            if j['URL'] not in used_urls:
                res.append(j)
                if len(res) == limit:
                    break
    except Exception as e:
        logging.error("NewsTable: Fail to query dynamodb: {}".format(e))
    return res


'''
:entities: list of entities mentioned in conversation
:return value: dictionary of key-value pairs where:
        Key   : headline_entity : String
        Value : Url_List : list of String
'''


def headline_mention(entities):
    key = "Headline_Entity"
    res = {}
    for ent in entities:
        try:
            item_response = NEHtable.get_item(
                Key={
                    key: ent
                }
            )
            if 'Item' in item_response:
                res[item_response['Item']['Headline_Entity']] = item_response['Item']['URL_List']
        except Exception as e:
            logging.error("NEHtable: Fail to query dynamodb: {}".format(e))
    return res


'''
:entities: list of entities mentioned in conversation
:return value: dictionary of key-value pairs where:
        Key   : headline_entity : String
        Value : Url_List : list of String
'''


def article_mention(entities):
    key = "Entity"
    res = {}
    for ent in entities:
        try:
            item_response = NEtable.get_item(
                Key={
                    key: ent
                }
            )
            if 'Item' in item_response:
                res[item_response['Item']['Entity']] = item_response['Item']['URL_List']
        except Exception as e:
            logging.error("NEtable: Fail to query item {}: {}".format(ent, e))
    return res


def news_of_type_scan(type_of_news, limit=3):
    fe = Attr('Type').eq(type_of_news)
    response = NewsTable.scan(
        Select='ALL_ATTRIBUTES',
        FilterExpression=fe,
    )
    return response['Items'][:limit]


def news_by_url(url):
    item_response = NewsTable.get_item(
        ProjectionExpression="#url,  Headline, #date, Article",
        ExpressionAttributeNames={"#url": "URL",
                                  "#date": "Date", },
        Key={
            'URL': url
        }
    )
    return item_response


def news_latest(limit=2):
    response = NewsTable.query(
        ProjectionExpression="#url,  Headline, #date",
        ExpressionAttributeNames={"#url": "URL",
                                  "#date": "Date", },
        IndexName='Type-Date-index',
        ScanIndexForward=False,
        KeyConditionExpression=Key('Type').eq("Technology")
    )
    return response['Items'][:limit]


def get_news_tm_tags(tags, used_titles):
    # res has result of query
    res = []
    # extract news from news table
    response = MomentsTable.query(
        ProjectionExpression="title, #desc, #source, #type",
        ExpressionAttributeNames={"#desc": "desc",
                                "#source": "source",
                                "#type": "type",},
        IndexName='source',
        ScanIndexForward=False,
        KeyConditionExpression=Key('source').eq(tags[0])
    )
    try:
        # TODO add condition for small desc of TMs
        for j in response['Items']:
            if j['title'] not in used_titles:
                if len(j['desc'].split()) > 20:
                    res.append(j)
                if len(res) == 3:
                    break
    except Exception as e:
        logging.error(
            "[NEWSMODULE][ERROR] While querying the moments table {}".format(e), exc_info=True)
    return res[random.randint(0, 2)]


def get_frequent_keyword_dict():
    try:
        item = NewsTable.get_item(
            Key={
                'URL': 'keyword'
            }
        )
        return item['Item']['Article']

    except Exception as e:
        logging.error(
            "[NEWSMODULE][ERROR] While querying for frequent keyword list table {}".format(e), exc_info=True)
        return {}
