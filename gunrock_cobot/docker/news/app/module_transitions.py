propose_module_ref = {'all': {'international': 'TRAVELCHAT',
                              'entertainment': 'MOVIECHAT',
                              # 'politics': 'ELECTIONCHAT',
                              # 'health': 'PSYPHICHAT',
                              'science': 'TECHSCIENCECHAT',
                              'technology': 'TECHSCIENCECHAT',
                              # 'fashion':'FASHIONCHAT',
                              'music': 'MUSICCHAT',
                              'sports': 'SPORT',
                              },
                      'topics': {'topic_weather': 'WEATHER',
                                 # #### TAGS ####
                                 'topic_movietv': 'MOVIECHAT',
                                 'topic_book': 'BOOKCHAT',
                                 'topic_music': 'MUSICCHAT',
                                 'topic_sport': 'SPORT',
                                 # 'topic_politics': 'POLITICS',
                                 # 'topic_entertainment': 'FASHIONCHAT'
                                 'topic_travel': 'TRAVELCHAT',
                                 'topic_food': 'FOODCHAT',
                                 'topic_techscience': 'TECHSCIENCECHAT',
                                 # 'topic_psyphi': 'PSYPHICHAT',
                                 'topic_animal': 'ANIMALCHAT',
                                 'topic_game': 'GAMECHAT',
                                 'topic_holiday': 'HOLIDAYCHAT',
                                 # 'topic_election': 'ELECTIONCHAT',
                                 },
                      'in': {'entertainment': 'MOVIECHAT',
                             # 'politics': 'ELECTIONCHAT',
                             # 'health': 'PSYPHICHAT',
                             'science': 'TECHSCIENCECHAT',
                             'technology': 'TECHSCIENCECHAT',
                             # 'fashion':'FASHIONCHAT',
                             'sports': 'SPORT',
                             # #### TAGS ####
                             'topic_sport': 'SPORT',
                             # 'topic_politics': 'POLITICS',
                             'topic_entertainment': 'MOVIECHAT',
                             'topic_techscience': 'TECHSCIENCECHAT',
                             },
                      'out': {
                          'music': 'MUSICCHAT',
                          'sports': 'SPORT',
                          'topic_weather': 'WEATHER',
                          # #### TAGS ####
                          'topic_movietv': 'MOVIECHAT',
                          'topic_book': 'BOOKCHAT',
                          'topic_music': 'MUSICCHAT',
                          # 'topic_politics': 'POLITICS',
                          # 'topic_entertainment': 'FASHIONCHAT'
                          'topic_travel': 'TRAVELCHAT',
                          'topic_food': 'FOODCHAT',
                          # 'topic_psyphi': 'PSYPHICHAT',
                          'topic_animal': 'ANIMALCHAT',
                          'topic_game': 'GAMECHAT',
                          'topic_holiday': 'HOLIDAYCHAT',
                          # 'topic_election': 'ELECTIONCHAT',
                      },
                      'rev': {'topic_weather': 'weather',
                              # #### TAGS ####
                              'topic_movietv': 'movies',
                              'topic_book': 'books',
                              'topic_music': 'music',
                              'topic_sport': 'sports',
                              # 'topic_politics': 'POLITICS',
                              'topic_entertainment': 'MOVIECHAT',
                              'topic_travel': 'travel',
                              'topic_food': 'food',
                              'topic_techscience': 'technology and science',
                              # 'topic_psyphi': 'psychology',
                              'topic_animal': 'animals',
                              'topic_game': 'games',
                              'topic_holiday': 'holidays',

                              },
                      'rev_simp': {
                          'international': 'travel',
                          # 'politics': 'the election',
                          'entertainment': 'movies',
                          'science': 'science and technology',
                          'technology': 'science and technology',
                          # 'fashion': 'fashion',
                          'sports': 'sports',
                          # 'health': 'health',
                          'music': 'music',
                      }
                  }

# if row['sub'] not in propose_module_ref['rev_simp'].keys():
#     if row['sub'] == "uplift":
#         row['sub'] = 'happy'
#     elif row['sub'] == "news":
#         row['sub'] = 'trending'
#     utt += template_news.utterance(selector='giveOpinion/othersub',
#                                    slots={
#                                        'sub': str(row['sub'])},
#                                    user_attributes_ref=self.cm.user_attributes)
#     return utt, States.intro, ProposeContinue.CONTINUE, ProposeTopic.NONE
# utt += template_news.utterance(selector='transition/depth',
#                                slots={
#                                    'sub': str(row['sub'])},
#                                user_attributes_ref=self.cm.user_attributes)
# return utt, States.proposeModule, ProposeContinue.CONTINUE, ProposeTopic.NONE
