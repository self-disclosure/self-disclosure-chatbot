import json
import logging
import requests
import hashlib
import redis
import re

RETRIEVAL_LIST_KEY = "gunrock:ic:retrieval:apikeys"


r = redis.StrictRedis(host='52.87.136.90', port=16517,
                      socket_timeout=3,
                      socket_connect_timeout=1,
                      retry_on_timeout=True,
                      db=0, password="alexaprize", decode_responses=True)

def get_retrieval_keys():
    return r.brpoplpush(RETRIEVAL_LIST_KEY, RETRIEVAL_LIST_KEY)


def get_redis(prefix, input_str):
    logging.info('[REDIS] get redis  input: {}'.format(input_str))
    hinput = hashlib.md5(input_str.encode()).hexdigest()
    results = r.get(prefix + ':' + hinput)
    if results is None:
        return None
    return json.loads(results)


def set_redis_with_expire(prefix, input_str, output, expire=24 * 60 * 60):
    logging.info(
        '[REDIS] set redis with expire input: {} output: {}, expire {}'.format(
            input_str, output, expire))
    hinput = hashlib.md5(input_str.encode()).hexdigest()
    return r.set(prefix + ':' + hinput, json.dumps(output), nx=True, ex=expire)


def get_backstory_response(utterance):
    try:
        logging.info('[SOCIAL] backstory with new sentence embedding')
        headers = {
            'Content-Type': 'application/json',
        }
        data = {
            'text': utterance
        }
        logging.info('[SOCIAL] sentence embedding post data: {}'.format(data))
        resp = requests.post(
            'http://ec2-54-175-156-102.compute-1.amazonaws.com:8085/module',
            headers=headers,
            data=json.dumps(data),
            timeout=0.5)
        ret = resp.json()
        # NOTE: http code
        if resp.status_code < 300:
            logging.info("BACKSTORY CONFIDENCE")
            logging.info(str(ret['confidence']))
            # TODO: handle low confidence score
            if ret['confidence'] > 0.8:
                return ret['text']
            else:
                return None
    except Exception as e:
        logging.warning(
            '[SOCIAL] post sentence embedding backsotry  with data err: {}'.format(e))
        pass

    try:
        resp = requests.post(
            'http://ec2-18-204-99-176.compute-1.amazonaws.com:8085/module',
            headers=headers,
            data=json.dumps(data),
            timeout=0.5)
        ret = resp.json()
        # NOTE: http code
        if resp.status_code < 300:
            # TODO: handle low confidence score
            logging.info("BACKSTORY CONFIDENCE")
            logging.info(str(ret['confidence']))
            if ret['confidence'] > 0.8:
                return ret['text']
            else:
                return None
    except Exception as e:
        logging.warning(
            '[SOCIAL] post sentence embedding backsotry  with data err: {}'.format(e))
        pass
    # TODO: change template
    return "I am too shy to tell you the truth."


def postprocessing_evi_response(evi_response):
    r"""
    Some hard reformat of evi_response
    """
    evi_response = evi_response.lower()

    filter_list = ["alexa", "'that' is usually defined as"]
    # 1. get rid of the response that include alexa
    for x in filter_list:
        if re.search(x, evi_response):
            return None

    # get rid of extremely short response from alexa
    if len(evi_response.split()) < 3 or len(evi_response.split()) > 50:
        return None

    return evi_response

##############The function from question-generation branch###############


def question_selection(question_candidates):
    """
    return the a selected question from question_candidates
    Priority by answer path:
        - first tier
            - root/ccomp
            - root/dep (for VPRED)
            - root/neg
            - root/xcomp/ccomp
            - root/xcomp/dobj
        - second tier
            - root/dobj
            - root/nsubj/nummod
            - root/advcl
        - third tier
            - root/nmod
    Output:
    (Selected_Question, Sorted Remaining Candidate Questions)
    """

    sorted_question = []
    for q in question_candidates:
        answer = q["answer_span"]
        ntoks = len(answer.strip().split(' '))

        # answer_path = row.get('QuestionAnswerPath')
        dependency_type = q["dependency_type"]
        if dependency_type in ["neg", "ccomp"]:  # xcomp_ccomp  # xcomp_dobj
            ntoks += 1000
        elif dependency_type in ["nsjub_nummod"]:
            if answer not in ["1", "2", "one", "two"]:
                ntoks += 500
        elif dependency_type in ["dobj", "advcl"]:
            ntoks += 500
        sorted_question.append((ntoks, q))

    if not sorted_question:
        return None

    sorted_question.sort(key=lambda x: x[0], reverse=True)

    if sorted_question[0][0] > 2:
        return (sorted_question[0][1],
                [unit[1] for unit in sorted_question[1:]])
    else:
        return(None, [unit[1] for unit in sorted_question[1:]])


def has_know_about(user_response):
    pat = re.compile(
        r"(heard|know|idea|anything) about|"
        r"(what is|what's|what) (up|going on|about)")
    if pat.search(user_response):
        return True
    else:
        return False


def can_map_to_chit_chat(user_response):
    for k, v in KEYWORD_TO_CHITCHAT_MAPPING.items():
        if k in user_response:
            return v
    # should change this mapping as per whats going on currently in the world
    if has_know_about(user_response) and "the world" in user_response:
        # for now, it is coronavirus
        return "coronavirus"
    return None


#########################################################################
NER_CONCEPT_KEY = ["issue", "environment issue", "team"]
NER_GOOGLEKG_KEY = ["government", "football franchise"]

# define regex patterns for different intents detection
INTENT_PATTERN_MANAGEMENT = {
    "QUIT_CHAT_PATTERN": [
        r"(^finished$|^cancel$|^stop$|\bturn off\b|\bstop talking\b)",
        r"(^stop conversation$|^stop talk)",
        r"((i'm|i am) (done))"
    ],
    "QUIT_MODULE_PATTERN": [
        r"((switch|change) (\w*\s*)* (topic|module))",
        r"((talk|chat|discuss) (about|regarding) (something else|some other))",
        r"i (hate|(dont|don't) like)( the)? news",
    ]
}

# define the out-of-news topic
SYS_OTHER_TOPIC_LIST = ['topic_animal','topic_movietv', 'topic_health', 'topic_weather', 'topic_music', 'topic_book', 'topic_holiday', 'topic_game', 'topic_fashion'] #LIMIT the topic to a small list
SYS_OTHER_MODULE_LIST = ['WEATHER', 'BOOKCHAT', 'GAMECHAT', 'MOVIECHAT', 'FASHIONCHAT', 'HOLIDAYCHAT']
# Define the key to keywords mapping for reference, when we exit the module
TOPIC_KEYWORD_MAP = {'topic_movietv': 'movies',
                     'topic_weather': 'weather',
                     'topic_music': 'music',
                     'topic_book': 'books',
                     'topic_game': 'games',
                     'topic_animal': 'animals',
                     'topic_holiday': 'holiday',
                     'topic_fashion': 'fashion',
                     }

# Define the module word to key word mapping for reference, when we exit
# the module
MODULE_KEYWORD_MAP = {
    'MOVIECHAT': 'movies',
    'WEATHER': 'weather',
    'MUSICCHAT': 'music',
    'BOOKCHAT': 'books',
    'GAMECHAT': 'games',
    'ANIMALCHAT': 'animals',
    'FASHIONCHAT': 'fashion',
    'HOLIDAYCHAT': 'holiday',
}

KEYWORD_MODULE_MAP = {
    'movies': 'MOVIECHAT',
    'weather': 'WEATHER',
    'music': 'MUSICCHAT',
    'books': 'BOOKCHAT',
    'animals': 'ANIMALCHAT',
    'games': 'GAMECHAT',
    'fashion': 'FASHIONCHAT',
    'holiday': 'HOLIDAYCHAT'
}

CHIT_CHAT_NER_IGNORE = ["trump", "president", "donald trump", "climate change", "global warming"]

NER_IGNORE_LIST = ["politics", "tech", "sport", "entertainment", "political", "technology", "sighs", "janet"]

IGNORED_NER_LABELS = ["FAC", "WORK_OF_ART", "LAW", "LANGUAGE", "DATE", "TIME", "PERCENT", "MONEY", "QUANTITY", "ORDINAL", "CARDINAL"]

# KEYWORD_SUPPORTED_LIST = ['coronavirus', 'corona virus']

NEWS_MANUAL_NER_LIST = ["obama", "donald trump", "trump"]

KEYWORD_TO_CHITCHAT_MAPPING = {
    # coronavirus
    "coronavirus": "coronavirus",
    "corona virus": "coronavirus",
    "corona": "coronavirus",
    "virus": "coronavirus",
    "pandemic": "coronavirus",
    "covid": "coronavirus",
    "nineteen": "coronavirus",
    # climate change
    "climate": "climate change",
    "climate change": "climate change",
    "global warming": "climate change",
    "greenhouse": "climate change",
    "renewable": "climate change",
    "alternative energy": "climate change",
    # space exploration
    "space exploration": "space exploration",
    "space technology": "space exploration",
    "outer space": "space exploration",
    "space travel": "space exploration",
    "space": "space exploration",
    # donald trump/impeachment
    "impeachment": "donald trump",
    # "donald trump": "donald trump",
    # "trump": "donald trump",
    # "current president": "donald trump",
    # "president trump": "donald trump",
    # "president": "donald trump",
    # gun control
    "gun control": "gun control",
    "guns": "gun control",
    "bullets": "gun control",
    "weapon": "gun control",
    "firearm": "gun control",
    # drug policy
    "drug policy": "drug policy",
    "drug": "drug policy",
    "drugs": "drug policy",
    "recreational": "drug policy",
    # border wall
    "border wall": "border wall",
    "mexico wall": "border wall",
    "trump wall": "border wall",
    # immigration
    "immigration": "immigration",
    "immigrant": "immigration",
    "visas": "immigration",
    # healthcare: obamacare
    "obamacare": "healthcare",
    "healthcare": "healthcare",
    "pharma": "healthcare",
    # mandatory vaccination
    "vaccination": "mandatory vaccination",
    "vaccinate": "mandatory vaccination",
    "vaccine": "mandatory vaccination",
    # nuclear energy
    "nuclear energy": "nuclear energy",
    "nuclear": "nuclear energy",
}

CORONAVIRUS_MAPPING_DICT = {
    "coronavirus": "coronavirus",
    "corona virus": "coronavirus",
    "corona": "coronavirus",
    "virus": "coronavirus",
    "pandemic": "coronavirus",
    "covid": "coronavirus",
    "nineteen": "coronavirus",
}

