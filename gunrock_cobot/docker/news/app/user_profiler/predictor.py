import logging
import requests

from .user_profiler_data import movie_gender_fb_data, movie_music_corr
from util_redis import RedisHelper


# Mark: - Logger Setup

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setFormatter(logging.Formatter('[UserProfile] %(message)s'))
logger.addHandler(ch)

# TODO: verify value
TIMEOUT = 5.0


class GenderPredictor:
    def get_gender_prediction(self, name=''):
        return RedisHelper().get_predicted_gender(name)

class PreferencePredictor:

    def predict_movie_by_gender(self, gender: str):
        """
        given user gender, return a dict of scores of movie genre preferences
        for that gender.
        retun a tuple of two list. First is the sorted genres, second is their
        corresponding score.
        see user_profiler_data.py for the value.
        """
        gender = gender.lower()
        if gender not in movie_gender_fb_data:
            logger.warn("PreferencePredictor::predict_movie_by_gender received invalid gender: {}".format(gender))
            gender = 'all'

        return tuple(zip(*sorted(movie_gender_fb_data[gender].items(), key=lambda d: d[1], reverse=True)))

    def predict_music_by_movie(self, movie_genre: str):
        """
        given movie genre, return a dict of scores of music genre preferences
        for that movie genre.
        retun a tuple of two list. First is the sorted genres, second is their
        corresponding score.
        see user_profiler_data.py for the value.
        """
        movie_genre = movie_genre.lower()
        if movie_genre not in movie_music_corr:
            logger.warn(f"PreferencePredictor::predict_music_by_movie received invalid movie genre: {movie_genre}")
            return None

        return tuple(zip(*sorted(movie_music_corr[movie_genre].items(), key=lambda d: d[1], reverse=True)))
