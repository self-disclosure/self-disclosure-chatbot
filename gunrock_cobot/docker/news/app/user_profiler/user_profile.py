import logging
from typing import List, Optional, Tuple, Dict, Any
from user_attribute_adaptor import UserAttributeAdaptor
from dataclasses import dataclass, field
from nlu.constants import TopicModule
from nlu.utils.type_checking import ensure_type
from user_profiler.topic_module_profiles import TopicModuleProfiles

logger = logging.getLogger(__name__)


class Gender:
    FEMALE = "female"
    MALE = "male"
    UNKNOWN = "unknown"


class UserProfile(UserAttributeAdaptor):
    def __init__(self, user_attributes_ref):
        super().__init__('user_profile', user_attributes_ref, auto_add_missing_keys=True)

        if 'topic_module_profiles' not in self._storage:
            self._storage['topic_module_profiles'] = {}
        self.topic_module_profiles = TopicModuleProfiles(self._storage['topic_module_profiles'])

    
    @dataclass(frozen=True)
    class PreferrdEntityTopic:
        entity_in_utterance: str = ""
        entity_detected: str = ""
        entity_type: List[str] = field(default_factory=list)
        module: Optional[TopicModule] = None

        @classmethod
        def from_dict(cls, d: Dict[str, Any]):
            module = None
            try:
                module_name = d.get('module', None)
                module = TopicModule(module_name) if module_name else None
            except (KeyError, ValueError) as e:
                logger.warning(f"[RETURNNLP] failed. d={d}, error={e}", exc_info=True)
            finally:
                return cls(
                    entity_in_utterance=ensure_type(d.get('entity_in_utterance'), str, ""),
                    entity_detected=ensure_type(d.get('entity_detected'), str, ""),
                    entity_type=ensure_type(d.get('entity_type'), list, []),
                    module=module
                )

        def __bool__(self):
            return bool(self.module)

        def __eq__(self, other):
            if isinstance(other, self.__class__):
                return self.__dict__ == other.__dict__
            else:
                return False

        def __ne__(self, other):
            return not self.__eq__(other)

    @property
    def username(self) -> str:
        """
        Public API for accessing user's name.

        Case "": Did not have user's name.
        Case <words>: User's name
        """
        if not self._username or self._username == '<no_name>':
            return ""
        else:
            return self._username

    @property
    def _username(self) -> Optional[str]:
        """
        Obtain the raw username stored in user_attributes.
        IMPORTANT!: Private API. Used only by system or launchgreeting. Do not use.

        Case None: User is new user, only possible in first turn
        Case "<no_name>": Did not obtain a valid name from user
        Case <words>: Valid name
        """
        if 'username' not in self._storage:
            self['username'] = None

        old_username = self._get_raw('usr_name')
        if old_username and old_username != self['username']:
            self['username'] = old_username

        return self['username']

    @_username.setter
    def _username(self, value: str):
        if not isinstance(value, str):
            logger.warning(f"username is being set with invalid value: {value}. Ignoring.")
            return
        self['username'] = value
        self._set_raw('usr_name', value)

    @property
    def _username_uncommon(self):
        if 'username_uncommon' in self._storage:
            return self['username_uncommon']

    @_username_uncommon.setter
    def _username_uncommon(self, value: str):
        if not isinstance(value, str):
            logger.warning(f"username_uncommon is being set with invalid value: {value}. Ignoring.")
            return
        self['username_uncommon'] = value

    @property
    def gender(self) -> str:
        """
        Return predicted gender. 'unknown' if no prediction.
        Supported gender: 'male', 'female'
        """

        if 'gender' not in self._storage:
            self['gender'] = Gender.UNKNOWN

        return self['gender']

    @gender.setter
    def gender(self, value: str):
        if not value or type(value) is not str:
            logger.warning(f"gender is being set with invalid value: {value}. Ignoring.")
            return

        self['gender'] = value

    def add_visit(self):
        self.visit += 1

    @property
    def visit(self):
        if 'visit' not in self._storage:
            self['visit'] = 0
        return self['visit']

    @visit.setter
    def visit(self, value: int):
        self['visit'] = value

    def add_total_turn_count(self):
        self.total_turn_count += 1

    @property
    def total_turn_count(self):
        """
        user's total turn count across conversations
        """
        if 'total_turn_count' not in self._storage:
            self['total_turn_count'] = 0
        return self['total_turn_count']

    @total_turn_count.setter
    def total_turn_count(self, value: int):
        self['total_turn_count'] = value

    def add_dominant_turn_count(self):
        self.dominant_turn_count += 1

    @property
    def dominant_turn_count(self):
        """
        user's total turn count across conversations
        """

        if 'dominant_turn_count' not in self._storage:
            self['dominant_turn_count'] = 0
        return self['dominant_turn_count']

    @dominant_turn_count.setter
    def dominant_turn_count(self, value: int):
        self['dominant_turn_count'] = value

    @property
    def dominant_turn_ratio(self) -> float:
        try:
            ratio = self.dominant_turn_count / self.total_turn_count
            formatted_ratio = float("{0:.2f}".format(ratio))
        except Exception as e:
            logger.warning(f"fail to get dominant_turn_ratio: {e}")
            return float(0)

        logger.info(f"dominant_turn_ratio: {formatted_ratio}")
        return formatted_ratio

    @property
    def preferences(self) -> List[Tuple[str, str, str]]:
        if 'preferences' not in self._storage:
            self._storage['preferences'] = []
        return self._storage['preferences']

    @property
    def preferred_entity_topics(self) -> List['UserProfile.PreferrdEntityTopic']:
        if 'preferred_entity_topics' not in self._storage or self._storage['preferred_entity_topics'] is None:
            self._storage['preferred_entity_topics'] = []

        if self._storage['preferred_entity_topics']:
            return [UserProfile.PreferrdEntityTopic.from_dict(elem) for elem in self._storage['preferred_entity_topics']]
        else:
            return []

    @preferred_entity_topics.setter
    def preferred_entity_topics(self, value: Optional[List[PreferrdEntityTopic]]):
        if value is None:
            save_dict_list = []
        else:
            save_dict_list = [
                {"entity_in_utterance": elem.entity_in_utterance,
                 "entity_detected": elem.entity_detected,
                 "entity_type": elem.entity_type,
                 "module": elem.module.value
                 }
                for elem in value
            ]
        self['preferred_entity_topics'] = save_dict_list

    def add_preferred_entity_topics(self, entity_topics: List[PreferrdEntityTopic]):
        for elem in entity_topics:
            if elem not in self.preferred_entity_topics:
                self.preferred_entity_topics = self.preferred_entity_topics + [elem]
