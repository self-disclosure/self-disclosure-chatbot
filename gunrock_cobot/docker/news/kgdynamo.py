from __future__ import print_function # Python 2/3 compatibility
import boto3
from operator import itemgetter
from boto3.dynamodb.conditions import Key, Attr
from itertools import chain

dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
table = dynamodb.Table('KG_Data')

def keywords(word_list: list) -> list:
    # word_list = [word for line in word_list for word in line.split()]
    word_list = list(set(chain(*map(str.split, word_list))))
    fe = Attr('keywords').contains(word_list[0])
    for word in word_list[1:]:
        fe = fe or Attr('keywords').contains(word)

    response = table.scan(
        Select='ALL_ATTRIBUTES',
        FilterExpression=fe,
        )
    return sorted(response['Items'], key=itemgetter('modified'), reverse=True)


if __name__ == "__main__":
    from time import time
    s = time()
    print(keywords(['lebron', 'lonzo ball is my player']))
    e = time()
    print(e-s)
