
import boto3
from boto3.dynamodb.conditions import Key, Attr
import pandas as pd

# Get the service resource.
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('UserTableBeta')
print(table.creation_date_time)

response = table.scan(
    FilterExpression=Attr('user_id').begins_with('self')
)

items = response['Items']

df = pd.DataFrame(items)
df['ab_condition'] = df['map_attributes'].apply(lambda x: x.get('self-disclosure-study-abtest'))
df = df.drop(['map_attributes'], axis=1)

print(df.head())

df.to_csv('user_table_toutput.csv')
