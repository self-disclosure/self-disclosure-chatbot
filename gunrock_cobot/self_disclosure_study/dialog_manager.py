from injector import inject
import logging
from typing import List
import json

from cobot_core.log.logger import LoggerFactory  # noqa
from cobot_core.state_manager import StateManager  # noqa
from cobot_core.selecting_strategy import SelectingStrategy  # noqa
from cobot_core.dialog_manager import DialogManager  # noqa
from cobot_core.ranking_strategy import RankingStrategy  # noqa
from cobot_core.offensive_speech_classifier import OffensiveSpeechClassifier  # noqa
from cobot_core.output_responses_converter import OutputResponsesConverter  # noqa
from cobot_core.nlp.pipeline import ResponseGeneratorsRunner  # noqa

import utils
from selecting_strategy.module_selection import ModuleSelector, GlobalState
from nlu.dataclass.returnnlp import ReturnNLP  # noqa
from nlu.constants import TopicModule
from nlg_post_process.response_content_post_processor import ResponseContentPostProcessor
from nlg_post_process.response_format_post_processor import ResponseFormatProcessor
from nlg_post_process.response_prosody_post_processor import ResponseProsodyProcessor
import template_manager  # noqa
from acknowledgement.acknowledgement_generator import AcknowledgementGenerator


error_template = template_manager.Templates.error
logger = logging.getLogger(__name__)


def set_self_study_abtest_condition_to_current_state(state_manager):
    condition = getattr(state_manager.user_attributes, "self-disclosure-study-abtest", "A")
    setattr(state_manager.current_state, "self-disclosure-study-abtest", condition)
    logger.debug("set_self_study_abtest_condition_to_current_state: {}".format(condition))

    level = getattr(state_manager.user_attributes, "self-disclosure-study-current-level", "factual")
    getattr(state_manager.current_state, "self-disclosure-study-current-level", level)


def save_template_keys_to_current_state_and_reset(state_manager):
    template_keys = get_template_keys(state_manager)
    logger.debug("save_template_keys_to_current_state_and_reset: {}".format(template_keys))
    setattr(state_manager.current_state, "template_keys", template_keys)
    state_manager.user_attributes.map_attributes[
        'template_manager']['selector_history'] = []

def save_qa_response_to_current_state_and_reset(state_manager):
    qa_response = getattr(state_manager.user_attributes, 'qa_response', None)
    logging.info(f"qa_response: {qa_response}")
    if qa_response:
        setattr(state_manager.current_state, "qa_response", qa_response)
        setattr(state_manager.user_attributes, "qa_response", None)


def get_template_keys(state_manager):
    template_manager = getattr(state_manager.user_attributes, 'template_manager', {})
    return template_manager.get('selector_history', []) if template_manager else []

def print_log_for_local_env(log_text):
    if utils.get_working_environment() == "local":
        print("[DEBUG] {}".format(log_text))

class SelfDisclosureDialogManager(DialogManager):

    @inject
    def __init__(self,
                 state_manager: StateManager,
                 selecting_strategy: SelectingStrategy,
                 ranking_strategy: RankingStrategy,
                 offensive_speech_classifier: OffensiveSpeechClassifier,
                 output_responses_converter: OutputResponsesConverter,
                 response_generator_runner: ResponseGeneratorsRunner
                 ) -> None:
        self.state_manager = state_manager
        self.selecting_strategy = selecting_strategy
        self.ranking_strategy = ranking_strategy
        self.offensive_speech_classifier = offensive_speech_classifier
        self.output_responses_converter = output_responses_converter
        self.response_generator_runner = response_generator_runner
        self.logger = LoggerFactory.setup(self)
        self.cur_state = self.state_manager.current_state
        self.module_selector = ModuleSelector(self.state_manager.user_attributes)
        self.returnnlp = None

    def select_response(self, features):
        if features is None:
            features = self.state_manager.current_state.features

        self.returnnlp = ReturnNLP.from_list(features.get("returnnlp", []))

        try:
            result = self._dialog_manager_handler(features)
            return result
        except Exception as e:
            logging.error('[DM] catch error: {}'.format(e), exc_info=True)
            setattr(self.state_manager.current_state, "resp_type", "sys_crash")

            if utils.get_working_environment() == "local":
                response = "CRASH - SYSTEM LEVEL CRASH"
            else:
                response = error_template.utterance(selector='sys_crash', slots={},
                                                user_attributes_ref=self.state_manager.user_attributes)

            save_template_keys_to_current_state_and_reset(self.state_manager)

            return response, False

    def _dialog_manager_handler(self, features):
        should_end_session = False

        self.print_features_for_local_env(features)

        self.update_highest_disclosure_level(features)

        response_generators = self.selecting_strategy.select_response_mode(features)

        self.try_generate_acknowledgement(features)

        self.set_fields_after_selecting_module(response_generators)
        
        self.print_user_attributes_for_local_env("user attributes (before entering topic module)")

        output_responses_dict, responses_list = self.generate_responses(response_generators)
        self.logger.info(f"output_responses_dict: {output_responses_dict}")
        
        if responses_list:
            top_ranked_module, top_ranked_response_text = self.rank(output_responses_dict, responses_list)
        else:
            top_ranked_module, top_ranked_response_text = response_generators[0], []

        content_post_processor = ResponseContentPostProcessor(
            self.state_manager, top_ranked_module, top_ranked_response_text)
        response = content_post_processor.process()

        response = ResponseFormatProcessor(response).process()

        response = ResponseProsodyProcessor(response,
                                            getattr(self.state_manager.current_state, "a_b_test", ""),
                                            response_generators[0]).process()

        logging.info("testest")
        module_selector = ModuleSelector(self.state_manager.user_attributes)
        self.logger.info(f"top ranked module: {top_ranked_module}")
        self.logger.info(f"propose continue: {module_selector.get_propose_continue(top_ranked_module)}")


        self.print_user_attributes_for_local_env("user attributes (in the end)")

        set_self_study_abtest_condition_to_current_state(self.state_manager)
        save_template_keys_to_current_state_and_reset(self.state_manager)
        save_qa_response_to_current_state_and_reset(self.state_manager)

        return response, should_end_session

    def update_highest_disclosure_level(self, features):
        ab_test = getattr(self.state_manager.user_attributes, "self-disclosure-study-abtest", "A")
        if ab_test == "A":
            setattr(self.state_manager.user_attributes, "self-disclosure-study-current-level", "factual")
        elif ab_test == "B":
            setattr(self.state_manager.user_attributes, "self-disclosure-study-current-level", "cognitive")
        elif ab_test == "C":
            setattr(self.state_manager.user_attributes, "self-disclosure-study-current-level", "emotional")
        else:
            current_level = getattr(self.state_manager.user_attributes, "self-disclosure-study-current-level")
            logging.info(f"current_level: {current_level}")
            detected_level = features.get("self_disclosure_level", "factual")

            if detected_level == "emotional":
                setattr(self.state_manager.user_attributes, "self-disclosure-study-current-level", detected_level)
            elif detected_level == "cognitive" and current_level in ["factual"]:
                setattr(self.state_manager.user_attributes, "self-disclosure-study-current-level", detected_level)
            else:
                pass  # no need to update


    def print_features_for_local_env(self, features):
        print_log_for_local_env("feature: ")
        print_log_for_local_env(json.dumps(features, indent=4))

    def print_user_attributes_for_local_env(self, log_text):
        print_log_for_local_env(log_text)
        print_log_for_local_env(self.state_manager.user_attributes.serialize_to_json() + '\n')

    def try_generate_acknowledgement(self, features):
        acknowledgement = AcknowledgementGenerator(self.state_manager, features).generate_acknowledgement()
        setattr(self.state_manager.current_state, "system_acknowledgement", acknowledgement)

    def generate_responses(self, response_generators):
        output_responses_dict = self.response_generator_runner.run(response_generators)
        responses_list, converted_output_responses_dict = self.output_responses_converter.convert(
            output_responses_dict)
        setattr(self.state_manager.current_state,
                'candidate_responses', converted_output_responses_dict)

        return converted_output_responses_dict, responses_list

    def set_fields_after_selecting_module(self, response_generators):
        current_selected_module = response_generators[0]

        if current_selected_module not in ModuleSelector.interruption_modules:
            # Reset expect opinion key
            setattr(self.state_manager.user_attributes, "expect_opinion", False)

        self.module_selector.current_selected_module = TopicModule(current_selected_module)

        self.state_manager.current_state.set_new_field("selected_modules", response_generators)

        print("[DEBUG] selected module: {}".format(self.module_selector.current_selected_module))

    def rank(self, output_responses: dict, response_list: list):
        """
        :param output_responses: key: module name, value: response text
        :param response_list: list of response text
        :return: (top ranked module name, top ranked response text)
        """
        ranking_strategy_input_data = self.generate_ranking_strategy_input(
            output_responses, response_list)
        modules = list(ranking_strategy_input_data.keys())
        top_ranked_module = modules[0]
        top_ranked_response_text = ranking_strategy_input_data[
            top_ranked_module]
        return top_ranked_module, top_ranked_response_text

    def generate_ranking_strategy_input(self,
                                        output_responses_dict: dict,
                                        response_list: List[str]) -> dict:
        self.logger.info("[DM] start generate ranking strategy input...")
        result = {}
        for response_generator_name, response in output_responses_dict.items():
            if isinstance(response, list):
                new_response = [
                    elem for elem in response if elem in response_list]
                result[response_generator_name] = new_response
            else:
                if response in response_list:
                    result[response_generator_name] = response
        self.logger.info(
            "[DM] generate ranking strategy output: {}".format(result))
        return result