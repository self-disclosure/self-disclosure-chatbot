import calendar
import logging
from datetime import datetime, timedelta
from string import Formatter
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from template_manager.template import Template
    from template_manager.tua import TemplateUserAttributes


logger = logging.getLogger(__name__)


class FormatFunctions:

    def get(self, func_name: str):
        return getattr(self, func_name, None)

    def t_time_of_day(self, **kwargs):
        # TODO: pytz timezone awareness
        time = (datetime.utcnow() - timedelta(hours=7)).hour
        return 'morning' if time < 12 else 'afternoon' if time < 18 else 'evening'

    def t_weekday(self, **kwargs):
        # TODO: pytz timezone awareness
        time = (datetime.utcnow() - timedelta(hours=7))
        return calendar.day_name[time.weekday()]

    def t_username(self, *, tua, **kwargs):
        from user_profiler.user_profile import UserProfile
        user_profile = UserProfile(tua.ua_ref)
        return user_profile.username or ""

    def t_fragments(self, *, slot_name, slots, tua, **kwargs):
        try:
            from . import Template
            utterance = Template.shared.speak(slot_name, slots, tua, None)
        except KeyError as e:
            logger.warning("TemplateFormatter::t_fragments have missing arguments: {}".format(e))
            utterance = ""

        return utterance

    def s_fragments(self, *, template, slot_name, selector, slots, tua, **kwargs):
        try:
            if slot_name == selector:  # avoid infinite loop
                return ""
            utterance = template.speak(slot_name, slots, tua, None)
        except KeyError as e:
            logger.warning("TemplateFormatter::s_fragments have missing arguments: {}".format(e))
            utterance = ""

        return utterance

    def t_(self, *, slot_name, slots, tua, **kwargs):
        try:
            slot_name = slot_name.lstrip('t_')
            from . import Template
            utterance = Template.shared.speak(slot_name, slots, tua, None)
        except KeyError as e:
            logger.warning("TemplateFormatter::t_ have missing arguments: {}".format(e))
            utterance = ""

        return utterance

    def s_(self, *, template, slot_name, slots, tua, **kwargs):
        try:
            slot_name = slot_name.lstrip('s_')
            utterance = template.speak(slot_name, slots, tua, None)
        except KeyError as e:
            logger.warning("TemplateFormatter::t_ have missing arguments: {}".format(e))
            utterance = ""

        return utterance

    def t_talk_about_module(self, *, tua, **kwargs):
        from selecting_strategy.module_selection import ModuleSelector
        selector = ModuleSelector(tua.ua_ref)
        topic_module, utt = selector.get_next_modules()[0]
        selector.add_used_topic_module(topic_module)
        return utt


class TemplateFormatter:

    def __init__(self, template: 'Template'):
        self._template = template

    def format_string(self,
                      sentence: str,
                      selector: str,
                      slots: dict,
                      tua: 'TemplateUserAttributes') -> str:

        slot_names = [name for (_, name, _, _) in Formatter().parse(sentence) if name is not None]

        if len(slot_names) <= 0:  # there are no replacement
            return sentence

        replacement = slots
        functions = FormatFunctions()

        for slot_name in slot_names:

            logger.debug(f"slot_name: {slot_name}")
            if slot_name.startswith('t_fragments'):
                formatter_func = functions.get('t_fragments')
            elif slot_name.startswith('s_fragments'):
                formatter_func = functions.get('s_fragments')
            elif functions.get(slot_name):
                formatter_func = functions.get(slot_name)
            elif slot_name.startswith('t_'):
                formatter_func = functions.get('t_')
            elif slot_name.startswith('s_'):
                formatter_func = functions.get('s_')
            else:
                formatter_func = None

            logger.debug(f"formatter_func: {formatter_func}")

            if formatter_func:
                replacement[slot_name] = formatter_func(
                        template=self._template,
                        slot_name=slot_name,
                        sentence=sentence,
                        selector=selector,
                        slots=replacement,
                        tua=tua
                    )

        sentence = sentence.format(**replacement)

        return sentence
