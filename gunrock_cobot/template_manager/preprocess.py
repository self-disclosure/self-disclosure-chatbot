import os
import json
import logging
# import shutil
import yaml
# from pathlib import Path
# from glob import glob
from typing import List, Union

from template_manager import Template
from template_manager.utils import hash_utterance

TEMPLATE_ROOT = os.path.join(os.getenv("COBOT_HOME"), "gunrock_cobot", "response_templates")
logger = logging.getLogger(__name__)


# MARK: - DFS Transform of yml templates

def transform_template_entry(entry: Union[str, dict], **kwargs) -> dict:
    if isinstance(entry, str):
        uid = hash_utterance(kwargs.get('template'), '/'.join(kwargs.get('path')), entry)
    elif isinstance(entry, dict):
        entry_repr = json.dumps(entry, sort_keys=True)
        uid = hash_utterance(kwargs.get('template'), '/'.join(kwargs.get('path')), entry_repr)
    else:
        raise ValueError("Template entry is not a valid type for transform: {}".format(entry))
    return {'entry': entry, 'uid': uid}


def dfs(obj: dict, **kwargs):
    newobj = {}
    for k, v in obj.items():
        path = (kwargs.get('path') or []) + [k]
        if isinstance(v, dict):
            newobj[k] = dfs(v, **{**kwargs, **{
                'path': path,
                'level': kwargs.get('level', 0) + 1
            }})
        elif isinstance(v, list):
            newobj[k] = [
                transform_template_entry(item, **{**kwargs, **{'path': path}})
                for item in v
            ]
        else:
            newobj[k] = v
    return newobj


# MARK: - Distribution of Module and Template Data

def transform_templates(only: List[Template] = None, exclude: List[Template] = None):
    templates = set(only) if only else set([t for t in Template])
    if exclude:
        templates -= set(exclude)
    template_data = {}
    for template in templates:
        data = yaml.full_load(open(os.path.join(TEMPLATE_ROOT, f'{template.value}.yml')))
        template_data[template.name] = dfs(data, template=template)
    return template_data


def write_template_data(template_data):
    with open('response_templates/template_data.py', 'w') as w:
        w.write("# <--- dict object for [{}] created by template_manager ---> #\n"

                .format(', '.join(template_data.keys())))
        w.write("# <--- Do not directly edit this file. Edit the yml file and run the "
                "template_manager.preprocess script instead ---> #\n\n")

        w.write("data = \\\n")
        from pprint import pformat
        w.write(pformat(template_data, width=120))
        w.write('\n')


# MARK: - Main

def main():
    logger.info("[BEGIN] template transformation into template_data")
    template_data = transform_templates()
    logger.info('[SUCCESS] Converted all templates into template_data')

    logger.info("[BEGIN] writing template_data")
    write_template_data(template_data)
    logger.info('[SUCCESS] Templates written to template_data')


if __name__ == '__main__':
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())
    main()
