import sys
import os

from event import LaunchEvent, IntentEvent

import uuid

COBOT_HOME = os.getenv("COBOT_HOME")


class EventGenerator:

    def __init__(self, filename, json_path):
        self.user_id = self.__generate_user_id()
        self.session_id = self.__generate_session_id()
        self.filename = filename
        self.json_path = json_path


    def generate_events_and_write_to_file(self, input_file_name):
        utterances = self.__read_utterances(input_file_name)
        events = self.__generate_all_events(utterances)
        events_json = self.convert_events_to_json(events)
        self.__write_json_to_file(events_json, self.get_generated_file())

    @staticmethod
    def __generate_user_id():
        return "localtest-user-{}".format(uuid.uuid4())

    @staticmethod
    def __generate_session_id():
        return "localtest-session-{}".format(uuid.uuid4())

    def __read_utterances(self, filename):
        # utterances = ["lesley", "I'm good"]  # todo: read from txt file
        with open(filename, "r") as f:
            utterances = f.readlines()
        utterances = [x.strip() for x in utterances]

        return utterances

    def __write_json_to_file(self, events_json, output_filename):
        with open(output_filename, "w+") as f:
            f.write(events_json)

    def convert_events_to_json(self, events: list):
        events_json = [event.to_json() for event in events]
        result = ("[{}]".format(",".join(events_json)))

        return result

    def get_generated_file(self, for_cobot=False):
        output_file = self.filename.replace(".txt", ".json")
        generated_file = os.path.join(self.json_path, output_file)
        if for_cobot:
            return generated_file
        return os.path.join(COBOT_HOME, generated_file)

    def __generate_all_events(self, utterances):
        events = [self.__generate_launch_event()]
        events.extend(self.__generate_intent_events(utterances))
        return events

    def __generate_launch_event(self):
        return LaunchEvent(userId=self.user_id, sessionId=self.session_id)

    def __generate_intent_events(self, utterances):
        intent_events = []
        for utterance in utterances:
            intent_events.append(self.__generate_intent_event(utterance))
        return intent_events

    def __generate_intent_event(self, utterance):
        return IntentEvent(userId=self.user_id, sessionId=self.session_id, text=utterance)

