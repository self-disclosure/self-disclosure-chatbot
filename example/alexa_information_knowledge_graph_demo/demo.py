# -*- coding: utf-8 -*-

# Example script demonstrating how the Alexa Information Knowledge Graph APIs (Entity Resolution and Query) can be
# used to find things to say about a person. Warning: it's only intended as a short end-to-end example to give you a
# glimpse of what those 2 APIs can do. So it cuts corners to implement features beyond the scope of those APIs,
# in particular how natural language is being generated.
#
# See the definition of the 'who_is' function for a more detailed description of the solution implemented in this demo.
#
# The script uses two configuration files to determine what queries to execute based on the classes associated with
# a given person. Those configuration files are provided here as examples, but could be extended with additional classes
# and properties from the Alexa Information Ontology to support a wider range of answers.

import json
import logging
import sys
from random import choice, shuffle

from requests import HTTPError

from cobot_common.service_client import get_client
from exceptions import MissingPreferredLabel, UnknownEntityClass, UnresolvableMention

logger = logging.getLogger(__name__)
logger.setLevel(logging.ERROR)


def configure_logging_handler():
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    return ch


def q(*args):
    return '|'.join(args)


def unpack(results):
    return [unpack_binding_list(result['bindingList']) for result in results]


def unpack_binding_list(binding_list):
    result = {}
    for binding in binding_list:
        result[binding['variable']] = {'dataType': binding['dataType'], 'value': binding['value']}
    return result


def merge_context_entities(context_entities, result):
    for k in result:
        context_entities[k] = result[k]['value']
    return context_entities


def a_or_an_article(word):
    return 'an' if word[0].lower() in ['a', 'e', 'i', 'o', 'u'] else 'a'


def comma_separated_list_words(words):
    if len(words) == 0:
        return ""
    elif len(words) == 1:
        return words[0]
    elif len(words) == 2:
        return words[0] + ' and ' + words[1]
    else:
        return words[0] + ', ' + comma_separated_list_words(words[1:])


class Demo:
    def __init__(self, client, properties_class_mapping, related_entities_queries):
        self.client = client
        self.properties_class_mapping = properties_class_mapping
        self.related_entities_queries = related_entities_queries

    def run_entity_resolution(self, mention, class_constraints):
        try:
            logger.debug("run_entity_resolution request: \"%s\", %s", mention['text'], class_constraints)
            response = self.client.get_knowledge_entity_resolution(mention=mention, classConstraints=class_constraints)
            logger.debug("run_entity_resolution response: %s", response['resolvedEntities'])
            return response
        except HTTPError:
            logger.error(mention)
            logger.error(class_constraints)
            raise

    def run_query(self, query, variable_bindings):
        try:
            logger.debug("run_query request: \"%s\", %s", query['text'], variable_bindings)
            response = self.client.get_knowledge_query_answer(query=query, variableBindings=variable_bindings)
            logger.debug("run_query response: %s", response['results'])
            return response
        except HTTPError:
            logger.error(query)
            logger.error(variable_bindings)
            raise

    def resolve_entity(self, mention, classConstraint):
        mention = {"text": mention}

        class_constraints = [
            {
                "dataType": "aio:Entity",
                "value": classConstraint
            }
        ]

        response = self.run_entity_resolution(mention, class_constraints)

        if len(response['resolvedEntities']) == 0:
            raise UnresolvableMention

        return [r['value'] for r in response['resolvedEntities']]

    def find_entity_pref_label(self, entity):
        query = {
            "text": q(
                'query label',
                'entity <aio:prefLabel> label')
        }

        variable_bindings = [
            {
                "variable": "entity",
                "dataType": "aio:Entity",
                "value": entity
            }
        ]

        response = self.run_query(query, variable_bindings)

        if len(response['results']) == 0:
            raise MissingPreferredLabel

        return unpack(response['results'])[0]['label']['value']

    def find_entity_classes(self, entity, class_constraint):
        query = {
            "text": q(
                'query class, class_label',
                'entity <aio:isAnInstanceOf> class @ <aio:anytime>',
                'class <aio:isASubclassOf> classConstraint',
                'class <aio:prefLabel> class_label')
        }

        variable_bindings = [
            {
                "variable": "entity",
                "dataType": "aio:Entity",
                "value": entity
            },
            {
                "variable": "classConstraint",
                "dataType": "aio:Entity",
                "value": class_constraint
            }
        ]

        response = self.run_query(query, variable_bindings)

        if len(response['results']) == 0:
            raise UnknownEntityClass

        return [(r['class']['value'], r['class_label']['value']) for r in unpack(response['results'])]

    def random_entity_fact(self, entity, cls):
        if cls not in self.properties_class_mapping:
            return None

        property_name = choice(list(self.properties_class_mapping[cls].keys()))
        property_is_reversed = self.properties_class_mapping[cls][property_name]['reversed']

        if property_is_reversed:
            query = {
                "text": q(
                    'query subject_str, property_str, object_str',
                    'object property subject',
                    'subject <aio:prefLabel> subject_str',
                    'property <aio:prefLabel> property_str',
                    'object <aio:prefLabel> object_str')
            }
        else:
            query = {
                "text": q(
                    'query subject_str, property_str, object_str',
                    'subject property object',
                    'subject <aio:prefLabel> subject_str',
                    'property <aio:prefLabel> property_str',
                    'object <aio:prefLabel> object_str')
            }

        variable_bindings = [
            {
                "variable": "subject",
                "dataType": "aio:Entity",
                "value": entity
            },
            {
                "variable": "property",
                "dataType": "aio:Entity",
                "value": property_name
            }
        ]

        response = self.run_query(query, variable_bindings)

        if len(response['results']) == 0:
            return None

        result = choice(unpack(response['results']))
        context = merge_context_entities({}, result)

        if property_is_reversed:
            return "{object_str} {property_str} {subject_str}.".format(**context)
        else:
            return "{subject_str} {property_str} {object_str}.".format(**context)

    def find_related_entity_fact(self, entity, cls):
        if cls not in self.related_entities_queries:
            return None

        random_choice = choice(self.related_entities_queries[cls])
        query = {
            "text": random_choice['query']
        }

        variable_bindings = [
            {
                "variable": "subject",
                "dataType": "aio:Entity",
                "value": entity
            }
        ]

        response = self.run_query(query, variable_bindings)

        if len(response['results']) == 0:
            return None

        result = choice(unpack(response['results']))
        context = merge_context_entities({}, result)
        context['subject'] = entity

        for entity in context:
            context[entity] = self.find_entity_pref_label(context[entity])

        return random_choice['answer'].format(**context)

    def who_is(self, name):
        full_answer = []

        # Find the entity identifier for to the person being mentioned. Since this demo deals with people,
        # 'aio:HumanBeing' is passed as a class constraint to filter out entities that aren't people (e.g.
        # "washington" returns the identifier of George Washington, rather than Washington D.C. or Washington state).
        # The entity identifier corresponds to a node in the knowledge graph. From there, queries can be executed to
        # navigate the knowledge graph from that node and find out more about it.
        resolved_person = self.resolve_entity(name, 'aio:HumanBeing')[0]

        # The answer starts with a list of occupations that person is known for. This demo also uses classes to
        # later determine what queries to run, which is an relatively easy approach to implement.
        resolved_person_classes = self.find_entity_classes(resolved_person, 'aio:HumanBeing')
        person_full_name = self.find_entity_pref_label(resolved_person)
        class_ids, class_labels = zip(*resolved_person_classes)
        classes_answer = "{} is known as {} {}.".format(
            person_full_name,
            a_or_an_article(class_labels[0]),
            comma_separated_list_words(class_labels))
        full_answer.append(classes_answer)

        # Rearrange the list of classes to say something different every time.
        class_ids = list(class_ids)
        class_ids.append('aio:HumanBeing')
        shuffle(class_ids)

        # Try to query for a random fact for each class until one is found.
        for cls in class_ids:
            random_fact = self.random_entity_fact(resolved_person, cls)
            if random_fact:
                full_answer.append(random_fact)
                break

        # Try to query for a related entity for each class until one is found.
        for cls in class_ids:
            related_entity_fact = self.find_related_entity_fact(resolved_person, cls)
            if related_entity_fact:
                full_answer.append(related_entity_fact)
                break

        # Combine all sentences into a single answer
        return " ".join(full_answer)


if __name__ == "__main__":
    logger.addHandler(configure_logging_handler())

    if len(sys.argv) < 3:
        print("Usage: {} api-key \"person name\"".format(sys.argv[0]))
        exit(1)

    api_key = sys.argv[1]
    person_name = sys.argv[2]

    demo = Demo(
        get_client(api_key=api_key, timeout_in_millis=2000),
        json.load(open('properties_class_mapping.json')),
        json.load(open('related_entities_queries.json')))

    answer = demo.who_is(person_name)
    print(answer)
