import sys
from os import path
import re
import json

try:
    from cobot_common.data_processing import Cleaner
except ModuleNotFoundError:
    base_path = path.dirname(path.dirname(path.dirname(path.abspath(__file__))))
    sys.path.append(base_path)

from cobot_common.data_processing.components import ProfanityChecker
from cobot_common.data_processing.annotator import Annotator
from cobot_common.data_processing import Cleaner

source_file_path = path.join(path.dirname(path.abspath(__file__)), 'demo_source_file.txt')

result = Annotator(file_path=source_file_path, api_key='xgTjk23SRM9Q9VFrUEFOjav0Bn4ot21W8uFLEieT') \
    .detect_ner() \
    .detect_topic() \
    .detect_profanity() \
    .detect_sentiment() \
    .output('o.txt')  