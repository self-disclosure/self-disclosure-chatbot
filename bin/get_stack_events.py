import os
import json
import sys
import time
import datetime
try:
	import colorama
	from colorama import Fore, Style
	colorama.init(autoreset=True)
	color = True
except:
	color = False

stack_name = sys.argv[1]
stack_cmd = sys.argv[2]

start_time = datetime.datetime.utcnow()
last_event_time = start_time
first_failure = None
fail_message = ""
success_states = [
	"CREATE_COMPLETE",
	"UPDATE_COMPLETE",
	"DELETE_COMPLETE"
]
progress_states = [
	"CREATE_IN_PROGRESS",
	"DELETE_IN_PROGRESS",
	"REVIEW_IN_PROGRESS",
	"ROLLBACK_IN_PROGRESS",
	"UPDATE_COMPLETE_CLEANUP_IN_PROGRESS",
	"UPDATE_IN_PROGRESS",
	"UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS",
	"UPDATE_ROLLBACK_IN_PROGRESS"
]
failure_states = [
	"CREATE_FAILED",
	"DELETE_FAILED",
	"ROLLBACK_COMPLETE",
	"ROLLBACK_FAILED",
	"UPDATE_ROLLBACK_COMPLETE",
	"UPDATE_ROLLBACK_FAILED"
]

# Initiate the requested stack operation
if os.system(stack_cmd) is not 0:
	print("Unable to initiate the requested stack operation.")
	sys.exit(1)

# Get Stack ID
cmd = "aws cloudformation describe-stacks --stack-name " + stack_name + " > stack_info.json"
if os.system(cmd) is not 0:
	print("Unable to retrieve information about stack " + stack_name + ".")
	sys.exit(1)
js = json.load(open("stack_info.json"))
stack_id = js['Stacks'][0]['StackId']
cmd = "rm -f stack_info.json"
os.system(cmd)

while True:
	time.sleep(1)
	current_time = last_event_time

	# Get stack events
	cmd = "aws cloudformation describe-stack-events --stack-name " + stack_id + " > stack_events.json"
	if os.system(cmd) is not 0:
		print("Unable to get stack events for stack " + stack_name + ".")
		sys.exit(1)
	js = json.load(open("stack_events.json"))
	events = js['StackEvents']
	events.reverse()

	for event in events:
		status = event['ResourceStatus']
		resource = event['LogicalResourceId']
		try:
			reason = " - " + event['ResourceStatusReason']
		except:
			reason = ""
		try:
			event_time = datetime.datetime.strptime(event['Timestamp'],"%Y-%m-%dT%H:%M:%S.%fZ")
		except:
			event_time = datetime.datetime.strptime(event['Timestamp'],"%Y-%m-%dT%H:%M:%SZ")

		if event_time > current_time:
			# Print output for new event
			local_time = event_time.replace(tzinfo=datetime.timezone.utc).astimezone().strftime("%H:%M:%S")
			if status in success_states and color:
				print(local_time + ": " + Fore.GREEN + status + Fore.RESET + " \t" + resource + reason)	
			elif status in progress_states and color:
				print(local_time + ": " + Fore.YELLOW + status + Fore.RESET + " \t" + resource + reason)	
			elif status in failure_states and color:
				print(local_time + ": " + Fore.RED + status + Fore.RESET + " \t" + resource + reason)	
			else:
				print(local_time + ": " + status + " \t" + resource + reason)	
			
			# Check for first failure event
			if status in failure_states and first_failure is None:
				try:
					first_failure = event
					fail_message = first_failure['LogicalResourceId']
					fail_stack_id = first_failure['PhysicalResourceId']
					cmd = "aws cloudformation describe-stack-events --stack-name " + fail_stack_id + " > fail_stack_events.json"
					os.system(cmd)
					js = json.load(open("fail_stack_events.json"))
					fail_events = js['StackEvents']
					fail_events.reverse()
					for fail_event in fail_events:
						fail_event_status = fail_event['ResourceStatus']
						if fail_event_status in failure_states:
							try:
								fail_message += " - " + fail_event['LogicalResourceId']
								fail_message += " - " + fail_event['ResourceStatusReason']
								break
							except:
								fail_message = first_failure['LogicalResourceId']
				except Exception as e:
					first_failure = None

			# Evaluate exit conditions
			if resource == stack_name and status in success_states:
				if color:
					print(local_time + ": " + Fore.GREEN + "SUCCESS!")
				else:
					print(local_time + ": SUCCESS!")
				cmd = "rm -f stack_events.json"
				os.system(cmd)
				sys.exit(0)
			if resource == stack_name and status in failure_states:
				if color:
					if fail_message:
						print(local_time + ": " + Fore.RED + "FAILURE\t\t" + fail_message)
					else:
						print(local_time + ": " + Fore.RED + "FAILURE")
				else:
					if fail_message:
						print(local_time + ": FAILURE\t\t" + fail_message)
					else:
						print(local_time + ": FAILURE")
				cmd = "rm -f stack_events.json"
				os.system(cmd)
				sys.exit(1)

			if event_time > last_event_time:
				last_event_time = event_time