import jinja2
import yaml
import os
import sys

def render(tpl_path, context):
    path, filename = os.path.split(tpl_path)
    return jinja2.Environment(
        loader=jinja2.FileSystemLoader(path or './')
    ).get_template(filename).render(context)

base = "../cobot_deployment/"
path = base+"templates/"
rendered_base = "../cobot_deployment/rendered/"
rendered_path = rendered_base+"templates/"
bot_directory = sys.argv[1]+"/"

def merge(fList):
    ''' 
    Takes a list of yaml files and loads them as a single yaml document.
    Restrictions:
    1) None of the files may have a yaml document marker (---)
    2) All of the files must have the same top-level type (dictionary or list)
    3) If any pointers cross between files, then the file in which they are defined (&) must be 
    earlier in the list than any uses (*).
    '''

    if not fList:
        #if flist is the empty list, return an empty list. This is arbitrary, if it turns out that
        #an empty dictionary is better, we can do something about that.
        return []

    sList = []
    for f in fList:
        with open(f, 'r') as stream:
            sList.append(stream.read())
    fString = ''
    for s in sList:
        fString = fString + '\n'+ s

    try:
        y = yaml.load(fString)
    except Exception as e:
        print("Unable to load yaml files. Please fix any yaml errors in the following files: ")
        for f in fList:
            print(f)
        print(e)
        sys.exit(1)

    return y


def update_module_to_modules_list(module, module_names, modules):
    if module['name'] not in module_names:
        modules.append(module)
        module_names.add(module['name'])

if not os.path.exists(rendered_base):
    os.makedirs(rendered_base)

if not os.path.exists(rendered_path):
    os.makedirs(rendered_path)

config_files = [bot_directory+"modules.yaml"]
try:
    config = merge(config_files)
except Exception as e:
    print("Unable to load module names from modules.yaml. Check modules.yaml in sample_cobot for an example.")
    print(e)
    sys.exit(1)
if not config: 
    # Empty modules.yaml or empty file list passed to merge function. Continue with empty dict to indicate no modules.
    config = {}
if not isinstance(config, dict):
    # List of module names without any headers. Exit to fix formatting.
    print("Unable to load module names from modules.yaml. Check modules.yaml in sample_cobot for an example.")
    sys.exit(1)
if "modules" not in config or not config['modules']:
    # deployment_pipeline expects a list named modules that contains all module names except data modules.
    config['modules'] = []

print('Original config: ', config)

# for backwards compatible,
# when user only specifies module name without writing "name: module_name",
# convert it to {'name': module_name}
converted_config = {}
for k, v in config.items():
    if isinstance(v, list):
        new_v = []
        for elem in v:
            new_v.append({'name': elem} if isinstance(elem, str) else elem)
        converted_config[k] = new_v
    else:
        converted_config[k] = v

print('Converted config: ', converted_config)
module_names = set() # Prevent duplicate module names.
modules = []
data_modules = []

for module in converted_config['modules']:
    update_module_to_modules_list(module_names, modules)

if "cobot_modules" in converted_config and converted_config['cobot_modules']:
    for module in converted_config['cobot_modules']:
        if not os.path.exists(bot_directory+"docker/"+module['name']):
            print("Missing cobot module directory: " + bot_directory+"docker/" + module['name'] + ". Check sample_cobot/docker for an example.")
            sys.exit(1)
        update_module_to_modules_list(module, module_names, modules)
if "infrastructure_modules" in converted_config and converted_config['infrastructure_modules']:
    for module in converted_config['infrastructure_modules']:
        if not os.path.exists("../cobot_common/docker/"+module['name']):
            print("Missing infrastructure module directory: ../cobot_common/docker/" + module['name'] + ". Check cobot_common/docker for an example.")
            sys.exit(1)
        update_module_to_modules_list(module, module_names, modules)
if "data_modules" in converted_config and converted_config['data_modules']:
    try:
        service_config = merge([path+"service-config.yaml"])
    except Exception as e:
        print("Unable to load module configurations from service-config.yaml. Check " + path + "service-config.yaml.")
        print(e)
        sys.exit(1)
    if "data_modules" not in service_config or not service_config['data_modules']:
        print("Invalid service-config.yaml. File must contain data_modules.")
        sys.exit(1)
    service_config_lookup = {}
    for data_module in service_config['data_modules']:
        service_config_lookup[data_module['name']] = data_module
    for module in config['data_modules']:
        if not module in service_config_lookup:
            print("Missing module configuration in " + path + "service-config.yaml for module: " + module + ". Check " + path + "service-config.yaml for an example.")
            sys.exit(1)
        data_modules.append(service_config_lookup[module])
        
if modules:
    converted_config['modules'] = modules
if data_modules:
    converted_config['data_modules'] = data_modules

print('Final config: ', converted_config)

with open(rendered_base+"master.yaml", "w") as fh:
    rendered_template = render(base+"master.yaml",converted_config)
    fh.write(rendered_template)

for filename in os.listdir(path):
    with open(rendered_path+filename, "w") as fh:
        rendered_template = render(path+filename,converted_config)
        fh.write(rendered_template)

        
