from shared import create_dir, ModuleManager, bcolors
import sys
import os
import yaml
import argparse
import subprocess
import json
import logging
import random
import string

from subprocess import run

local_docker_modules = []

# Root directory to find source codes and store log files.
COBOT_HOME = os.environ.get('COBOT_HOME', os.path.dirname(
    os.path.dirname(os.path.abspath(__file__))))
sys.path.append(COBOT_HOME)
# from shared import create_dir, ModuleManager, bcolors

# Directory to store logs from local testing and local docker modules.
LOG_DIRECTORY = os.path.join(COBOT_HOME, 'interactive_mode_logs')

# File to store logs from local testing.
LOCAL_TESTING_LOG_FILE_PATH = os.path.join(
    LOG_DIRECTORY, 'interactive_mode.log')

create_dir(LOG_DIRECTORY)

logging.basicConfig(filename=LOCAL_TESTING_LOG_FILE_PATH, filemode='w', level=logging.INFO,
                    format='[%(levelname)s] %(asctime)s %(filename)s %(message)s')

STOP_WORDS = ['stop', 'quit', 'exit', 'end', 'goodbye', 'bye']


class ASKInvocation:

    def __init__(self, skill_id, invocation_name, user_id):
        self.skill_id = skill_id
        self.invocation_name = invocation_name
        self.cached_responses = {}
        self.user_id = user_id
        self.attributes = {}
        self.session_id = "amzn1.echo-api.session.localinteraction-" + ''.join(
            random.choices(string.ascii_lowercase + string.digits, k=16))

    def build_launch_request(self):
        """Builds a launch request JSON object to pass to the lambda_handler
        """

        request_object = """
        {
            "session": {
                "new": "true",
                "sessionId": "{session_id}",
                "attributes": {},
                "user": {
                    "userId": "amzn1.ask.account.localinteractor"
                },
                "application": {
                    "applicationId": "{skill_id}"
                }
            },
            "version": "1.0",
            "request": {
                "locale": "en-US",
                "timestamp": "2018-01-01T00:00:00Z",
                "type": "LaunchRequest",
                "requestId": "amzn1.echo-api.request.localinteraction",
                "speechRecognition": {}
            }
        }
        """

        request_object = request_object.replace(
            "{session_id}", self.session_id)
        request_object = request_object.replace("{skill_id}", self.skill_id)
        request_object = request_object.replace(
            "amzn1.ask.account.localinteractor", self.user_id)
        return json.loads(request_object)

    def mock_asr_from_utterance(self, utterance):
        """Tokenizes input text and creates an ASR dict with default token confidence of 1.0

        Arguments:
            utterance {string} -- Text to be cleaned

        Returns:
            dict -- ASR dict with tokens and confidence scores
        """

        tokenized = utterance.split(" ")
        confidence = 1.0
        tokens = []
        mock_offset_counter = 0
        for token in tokenized:
            mock_offset_counter += 1000
            start_offset = mock_offset_counter
            mock_offset_counter += 1000
            end_offset = mock_offset_counter
            asr = {"value": token, "confidence": confidence, "startOffsetInMilliseconds": start_offset,
                   "endOffsetInMilliseconds": end_offset}
            tokens.append(asr)

        return tokens

    def build_intent_request(self, session, request, tokens):
        """Creates a request object based off the results of an `ask simulate...` call.

        Arguments:
            session {dict} -- Session JSON object from `ask simulate...`
            request {dict} -- Request JSON object from `ask simulate...`
            tokens {list} -- List of mocked ASR tokens
        """

        request_object = {}
        request_object['version'] = "1.0"
        request_object['session'] = session
        request_object['request'] = request

        # Inject mocked tokens
        request_object['request'].setdefault(
            "speechRecognition", {}).setdefault("hypotheses", [])
        request_object['request']['speechRecognition']['hypotheses'].append({})
        request_object['request']['speechRecognition'][
            'hypotheses'][0]['tokens'] = tokens

        # Mock utterance-level confidence score
        request_object['request']['speechRecognition'][
            'hypotheses'][0]['confidence'] = 1.0

        # Override some session values to simulate the multi-turn interaction
        request_object['session']['new'] = 'false'
        request_object['session']['sessionId'] = self.session_id

        # Override session attributes
        request_object['session']['attributes'] = self.attributes

        # Override userid to simulate user information for multi-turn
        # interaction
        request_object['session']['user']['userId'] = self.user_id

        # Override session attributes
        request_object['session']['attributes'] = self.attributes

        # Override userid to simulate user information for multi-turn interaction
        request_object['session']['user']['userId'] = self.user_id

        return request_object

    def clean_ssml(self, input_text):
        """Cleans SSML speak tags

        Arguments:
            input_text {string} -- Text to be cleaned

        Returns:
            string -- Input text cleaned of <speak> tags
        """
        return input_text.replace('<speak>', '').replace('</speak>', '')

    def clean_utterance(self, utterance):
        """Removes punctuation, strips text, lowercases

        Arguments:
            utterance {string} -- Text to be cleaned

        Returns:
            string -- Lowercased input text stripped and without punctuation
        """

        # Remove punctuation
        punctuation = set(string.punctuation)
        punctuation.remove("'")  # Keep apostrophes in the utterance
        for p in punctuation:
            utterance = utterance.replace(p, " ")

        # Remove double-whitespace left by punctuation replacement
        utterance = " ".join(utterance.split())
        return utterance.strip().lower()

    def cache_utterance_event(self, utterance, event):
        """Caches the utterance's simulation result so we don't have to do an expensive simulate call again
        during this session.

        Arguments:
            utterance {string} -- Utterance to be processed by the lambda_handler
            event {dict} -- Event object to be passed to event handler
        """

        if utterance not in self.cached_responses:
            self.cached_responses[utterance] = event

    def parse_ask_event(self, ask_event):

        # Remove the simulation number and text at the beginning of the result
        # dict string
        result_dict_string = ask_event.stdout.decode('utf-8')
        index_of_simulation_number = str.find(result_dict_string, "{")
        augmented_result_dict_string = result_dict_string[
            index_of_simulation_number:]

        output = json.loads(augmented_result_dict_string, strict=False)

        return output

    def try_forcing_new_ask_session(self, skill_id, utterance):
        """ASK <=1.7.9 - ask simulate behaves like web simulator. Limitations of web simulator also apply here.
        1. We need to create a new session if remote lambda returns error which implies passing open invocation and
           utterance.
        2. ask simulate session timeout is 30 mins
        3. Multiple requests from the same account might create problem also
        4. If the session with ask terminates for what so ever reason, we try to resume the session for local testing.
        """
        retries = 3
        modified_utterance = 'open ' + ask.invocation_name + ' and ' + utterance

        while retries > 0:
            proc = run(('ask simulate --force-new-session -s {} --locale en-US -t "{}"').format(
                skill_id, modified_utterance), shell=True, stdout=subprocess.PIPE)
            output = self.parse_ask_event(proc)

            if output['status'] == "SUCCESSFUL":
                break
            else:
                retries = retries - 1

        return output

    def process(self, utterance):
        """ First checks cache for the utterance, returns result if found. If not found in cache,
        processes the utterance and send it to ASK via `ask simulate...`

        Arguments:
            utterance {string} -- Utterance to be processed by the lambda_handler
        """

        utterance = self.clean_utterance(utterance)

        if utterance not in STOP_WORDS:
            if utterance in self.cached_responses:
                event = self.cached_responses[utterance]
                response_text = self.process_event(event)
            else:
                logging.info('utterance: %s', utterance)
                proc = run(('ask simulate -s {} --locale en-US -t "{}"').format(
                    skill_id, utterance), shell=True, stdout=subprocess.PIPE)

                output = self.parse_ask_event(proc)

                if output['status'] == "FAILED":
                    output = self.try_forcing_new_ask_session(
                        skill_id, utterance)
                    # if status is still failed, there is something else going
                    # on.
                    if output['status'] == "FAILED":
                        logging.info("Unexpected ASK error:")
                        logging.info(output)
                        response_text = ("\n(An unexpected error occurred. Please check if you are able to "
                                         "test the same conversation with web simulator)")

                    else:
                        response_text = self.process_successful_ask_intent_request(
                            output)
                else:
                    response_text = self.process_successful_ask_intent_request(
                        output)

            print(bcolors.OKBLUE + "\n" + response_text + bcolors.ENDC)

    def process_successful_ask_intent_request(self, output):
        # Mock the ASR speechRecognition hypotheses
        tokens = self.mock_asr_from_utterance(utterance)
        try:
            # Take the only invocation request
            event = self.build_intent_request(
                output['result']['skillExecutionInfo']['invocations'][
                    0]['invocationRequest']['body']['session'],
                output['result']['skillExecutionInfo']['invocations'][
                    0]['invocationRequest']['body']['request'],
                tokens
            )

            self.cache_utterance_event(utterance, event)

            response_text = self.process_event(event)

        except KeyError as err:
            logging.info("\nIt appears Event format of ASK has changed.")
            logging.info(output)
            sys.exit("\nFatal: key not found: " + str(err))

        return response_text

    def process_event(self, event):
        """Creates an Event object and sends it to the CoBot's lambda_handler. Returns response text.

        Arguments:
            event {dict} -- Alexa event JSON object
        """
        result = lambda_handler_method(event=event, context={})
        response_text = self.clean_ssml(
            result['response']['outputSpeech']['ssml'])

        if 'sessionAttributes' in result:
            self.attributes = result['sessionAttributes']

        return response_text

    def open_skill(self):
        event = ask.build_launch_request()
        response_text = self.process_event(event)
        print(bcolors.BOLD + "Starting session " +
              self.session_id + bcolors.ENDC)
        print(bcolors.OKBLUE + response_text + bcolors.ENDC)


def parse_config():
    """Parse config file for invocation name and skill ID
    """

    try:
        config = yaml.full_load(open(args.config_file))

    except FileNotFoundError:
        print("config file not found")
        sys.exit()

    try:
        invocation_name = config['invocation_name']
        skill_id = config['skill_id']
        logging.info("Skill ID: " + skill_id)
    except KeyError as err:
        print(str(err) + " not found in config file")
        sys.exit()

    return skill_id, invocation_name


if __name__ == "__main__":

    cobot_home = os.environ.get('COBOT_HOME', os.path.dirname(
        os.path.dirname(os.path.abspath(__file__))))
    logging.info('COBOT_HOME: %s', cobot_home)
    # sys.path.append(cobot_home)

    parser = argparse.ArgumentParser(description='argument parser')

    parser.add_argument('-c', '--config-file', default='',
                        required='True', help='Location of your config.yaml file')
    parser.add_argument('-l', '--lambda_handler_path', required='True',
                        help=('Relative Path (relative to AlexaPrizeCobotToolkit package) to '
                              'your lambda handler class, i.e. sample_cobot.non_trivial_bot'))
    parser.add_argument('-s', '--service_config_file', required='True',
                        help="Path to service module json file."
                             "For local service module, it configures each module's "
                             "unique host_port and Dockerfile's directory. "
                             "For remote service module, it configures each module's url, "
                             "which can be found from Lambda function's environment variables. ")
    parser.add_argument('-u', '--user_id',
                        help='Unique user id which is the primary key in user attribute DynamoDB table',
                        required='True')
    args = parser.parse_args()

    skill_id, invocation_name = parse_config()
    user_id = args.user_id
    ask = ASKInvocation(skill_id, invocation_name, user_id)

    lambda_handler_path = args.lambda_handler_path
    with open(args.service_config_file, 'r') as config_file:
        test_config = json.load(config_file)

    for k, v in test_config.items():
        if 'url' in v.keys():
            os.environ[k] = v['url']
        elif 'port' in v.keys():
            os.environ[k] = "http://localhost:" + v['port']

    module_manager = ModuleManager(test_config)
    module_manager.start(COBOT_HOME)

    # refer to
    # https://stackoverflow.com/questions/547829/how-to-dynamically-load-a-python-class
    # for more detail
    mod = __import__(lambda_handler_path, fromlist=['lambda_handler'])
    lambda_handler_method = getattr(mod, 'lambda_handler')

    ask.open_skill()

    utterance = ""
    while utterance not in STOP_WORDS:
        utterance = input(bcolors.OKGREEN + "> ")
        print(bcolors.ENDC + bcolors.WARNING)
        ask.process(utterance)
        print(bcolors.ENDC)
        module_manager.log(LOG_DIRECTORY)
    module_manager.stop()
    print("\nPlease find all log files at:", bcolors.success(LOG_DIRECTORY))

    sys.exit()
