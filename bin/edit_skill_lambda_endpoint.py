# Takes lambda name or ARN as input and edits .ask/config endpoint uri
# Assumes .ask is in the current directory

import json
import sys

lambda_endpoint = sys.argv[1]

js=json.load(open('.ask/config'))
 
try:
    js['deploy_settings']['default']['merge']['manifest']['apis']['custom']['endpoint']['uri'] = lambda_endpoint
except KeyError:
    print("Invalid skill definition: check .ask/config.")
    raise

json.dump(js, open('.ask/config', 'w'), indent=2)
