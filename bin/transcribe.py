import sys
import argparse
import boto3
import pprint
from boto3.dynamodb.conditions import Key, Attr
import os
import logging

cobot_home = os.environ.get('COBOT_HOME', os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
logging.info('COBOT_HOME: %s', cobot_home)
sys.path.append(cobot_home)

from cobot_python_sdk.dynamodb_manager import DynamoDbManager

dynamodb = boto3.resource('dynamodb')

INTENT_CREATION_DATE_TIME_INDEX_NAME = "intent-creation_date_time-index"
CONVERSATION_ID_CREATION_DATE_TIME_INDEX_NAME = "conversation_id-creation_date_time-index"

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def get_session_id(args):
    if args.session_id == "latest" or args.session_id == "":
        table = dynamodb.Table(args.table_name)

        response = table.query(
            IndexName=INTENT_CREATION_DATE_TIME_INDEX_NAME,
            KeyConditionExpression=Key('intent').eq("LaunchRequestIntent"),
            ScanIndexForward=False,
            Limit=1
        )

        if len(response['Items']) > 0:
            session_id = response['Items'][0]['session_id']
            print(bcolors.HEADER + "Viewing " + session_id + " (latest):" + bcolors.ENDC)
        else:
            sys.exit("Unable to find latest conversation with a LaunchIntent")
    else:
        session_id = args.session_id
        print(bcolors.HEADER + "Viewing " + session_id + ":" + bcolors.ENDC)

    return session_id


def get_conversation_by_session_id(args, session_id):
    query_result = DynamoDbManager.query(table_name=args.table_name,
                                         key_condition=Key('session_id').eq(session_id)
                                         )
    print_out_session_info(args, query_result)


def print_out_turn_info(args, item):
    if 'text' in item:
        print("\n> " + bcolors.OKGREEN + item['text'].strip() + bcolors.ENDC)
    if 'response' in item:
        print(bcolors.OKBLUE + item['response'].strip() + bcolors.ENDC)
        if args.verbosity and args.verbosity == 1:
            print(bcolors.BOLD + "Intent: " + bcolors.ENDC)
            print("  " + item['intent'])
            if 'candidate_responses' in item:
                print(bcolors.BOLD + "\nCandidate Responses: " + bcolors.ENDC)
                for k, v in item['candidate_responses'].items():
                    print("  ", k)
                    if (v == None):
                        print("(Nothing returned from " + k + ")")
                    else:
                        print(v)

        elif args.verbosity and args.verbosity > 1:
            pp = pprint.PrettyPrinter(indent=2)
            pp.pprint(item)
    else:
        print(bcolors.OKBLUE + "(no response)" + bcolors.ENDC)


def print_out_session_info(args, response):
    if response:
        for item in response:
            print_out_turn_info(args, item)
        print("\n")
    else:
        print(bcolors.BOLD + "SessionID or ConversationID not available" + bcolors.ENDC + "\n")
        sys.exit()


def get_conversation_by_conversation_id(args, conversation_id):
    query_result = DynamoDbManager.query(table_name=args.table_name,
                                         key_condition=Key('conversation_id').eq(conversation_id),
                                         index_name=CONVERSATION_ID_CREATION_DATE_TIME_INDEX_NAME
                                         )
    print_out_session_info(args, query_result)


def create_conversation_id_gsi(table_name):
    attribute_definitions = [
        {
            'AttributeName': 'conversation_id',
            'AttributeType': 'S'
        },
        {
            'AttributeName': 'creation_date_time',
            'AttributeType': 'S'
        }
    ]

    key_schema = [
        {
            "AttributeName": "conversation_id",
            "KeyType": "HASH"
        },
        {
            "AttributeName": "creation_date_time",
            "KeyType": "RANGE"
        }
    ]
    projection = {
        'ProjectionType': "ALL"
    }
    DynamoDbManager.create_gsi(table_name=table_name,
                               attribute_definitions=attribute_definitions,
                               key_schema=key_schema,
                               index_name=CONVERSATION_ID_CREATION_DATE_TIME_INDEX_NAME,
                               projection=projection)


def get_conversation_id(args):
    if args.conversation_id == "latest" or args.conversation_id == "":
        table = dynamodb.Table(args.table_name)

        response = table.query(
            IndexName='intent-creation_date_time-index',
            KeyConditionExpression=Key('intent').eq("LaunchRequestIntent"),
            ScanIndexForward=False,
            Limit=1
        )

        if len(response['Items']) > 0 and response['Items'][0].get('conversation_id'):
                print(bcolors.HEADER + "Viewing " + response['Items'][0].get('conversation_id') + " (latest):" + bcolors.ENDC)

        else:
            sys.exit("Unable to find latest conversation with a LaunchIntent")
    else:
        conversation_id = args.conversation_id
        print(bcolors.HEADER + "Viewing " + conversation_id + ":" + bcolors.ENDC)

    return conversation_id


def create_intent_creation_date_time_gsi(table_name):
    attribute_definitions = [
        {
            'AttributeName': 'intent',
            'AttributeType': 'S'
        },
        {
            'AttributeName': 'creation_date_time',
            'AttributeType': 'S'
        }
    ]

    key_schema = [{
        "AttributeName": "intent",
        "KeyType": "HASH"
    },
        {
            "AttributeName": "creation_date_time",
            "KeyType": "RANGE"
        }]

    projection = {
        'ProjectionType': "KEYS_ONLY"
    }

    DynamoDbManager.create_gsi(table_name=table_name,
                               attribute_definitions=attribute_definitions,
                               key_schema=key_schema,
                               index_name=INTENT_CREATION_DATE_TIME_INDEX_NAME,
                               projection=projection)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='argument parser')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-s', '--session-id',
                        help='Session ID of the conversation you wish to view or "latest" session ID')
    group.add_argument('-c', '--conversation-id', default='latest',
                        help='conversation id you wish to view. Default to "latest" conversation ID')
    parser.add_argument('-t', '--table-name', default='StateTable', required='True',
                        help='DynamoDB table where your state is stored')
    parser.add_argument("-v", "--verbosity", action="count", help="increase output verbosity")
    args = parser.parse_args()

    if args.session_id == 'latest' or args.conversation_id == 'latest':
        create_intent_creation_date_time_gsi(args.table_name)

    if args.session_id:
        session_id = get_session_id(args)
        get_conversation_by_session_id(args, session_id)

    elif args.conversation_id:
        create_conversation_id_gsi(args.table_name)
        conversation_id = get_conversation_id(args)
        get_conversation_by_conversation_id(args, conversation_id)
