import subprocess
import os
import shutil


def create_dir(absolute_dir_path):
    """ Create a new directory by absolute path.

    :type absolute_dir_path: str
    :param absolute_dir_path: Absolute path used to create new directory
    """
    if os.path.exists(absolute_dir_path):
        shutil.rmtree(absolute_dir_path)
    os.makedirs(absolute_dir_path)


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    @classmethod
    def warning(cls, msg):
        return cls.WARNING + msg + cls.ENDC

    @classmethod
    def success(cls, msg):
        return cls.OKGREEN + msg + cls.ENDC

    @classmethod
    def error(cls, msg):
        return cls.FAIL + msg + cls.ENDC


class ModuleManager(object):
    """ The class used to manage life cycle of local docker containers configured in test_service_module_config file.

    Life cycle includes starting containers, outputing logs of the containers after testing the lambda function, and stopping all containers.
    """

    def __init__(self, module_configs):
        self._module_configs = module_configs
        self._running_containers = {}

    def start(self, cobot_home):
        """ Start local docker containers of service modules.

        :type: str
        :param cobot_home: The directory contains all cobot souce codes.
        """
        print()
        for module_name, module_config in self._module_configs.items():
            if 'port' in module_config.keys() and 'directory' in module_config.keys():
                port = module_config['port']
                directory = cobot_home + "/" + module_config['directory']
                container_id = self._start_container(directory, module_name, port)
                # If local testing failed to starts docker container, can't use the container id to output logs.
                if container_id is None:
                    continue
                self._running_containers[module_name] = container_id
        print()
        return self

    def log(self, log_directory):
        """ Output logs of local docker containers which are started by local testing scripts.

        :type: str
        :param log_direcotry: The directory to store log files
        """
        for module_name, container_id in self._running_containers.items():
            log_proc = subprocess.run(
                ['docker', 'logs', container_id],
                check=True,
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT  # direct the error ouput from the process to its standard output channel.
            )
            log_file_path = os.path.join(log_directory, '{}_container.log'.format(module_name))
            with open(log_file_path, 'w') as fp:
                # stdout is a bytes sequence and has '\n' at the end
                fp.write(log_proc.stdout.decode('utf-8'))
        return self

    def stop(self):
        """ Stop all docker containers started by local testing script
        """
        container_ids = self._running_containers.values()
        if len(container_ids) > 0:  # If there are running local containers, stop them.
            cmd = ['docker', 'kill']
            cmd.extend(container_ids)
            subprocess.run(
                cmd,
                check=True,
                stdout=subprocess.PIPE
            )

    def _start_container(self, directory, img_name, local_port):
        """ Rebuild docker image and start a new docker container

        :type: str
        :param directory: The directory has remote module's DOCKERFILE and source codes.

        :type: str
        :param img_name: New docker image name which is same as remote module name in test_service_module_config file

        :type: str
        :param local_port: Local host's port binded with docker container.

        :returns: Container id
        """
        subprocess.run(
            ['docker', 'build', '-t', img_name, directory],
            check=True,
            stdout=subprocess.PIPE
        )
        container_proc = subprocess.run(
            ['docker', 'run', '-d', '--rm', '-i', '-p', '{}:80'.format(local_port), img_name],
            # check=True,
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE
        )
        # An exist status of not 0 indicates that the docker container fails to start.
        if container_proc.returncode > 0:
            error_msg = container_proc.stderr.decode('utf-8').strip()
            print(bcolors.error('Error to start docker module: [{module}]'.format(module=img_name)))
            print(bcolors.warning(error_msg))
            print(
                "Try to run this command in the console: " +
                bcolors.success('"docker kill $(docker ps -a -q)"') +
                " to stop all running docker containers and restart local testing. "
                "Otherwise, you won't get logs from this service module.\n"
            )
            # Use None to indicate that the docker container failed to start.
            # Local testing doesn't neeed to output logs of the docker container and stop it in later steps.
            container_id = None
        else:
            print(bcolors.success('Service model: [{module}] is started'.format(module=img_name)))
            container_id = container_proc.stdout.decode('utf-8').strip()
        return container_id
