class Prompt(object):

    welcome_prompt = "<amazon:emotion name='excited' intensity='low'> Hi! this is an Alexa Prize social bot. </amazon:emotion> "
    welcome_prompt_followup = 'What would you like to talk about?'
    goodbye_prompt = ''
    repeat_prompt = "I'm sorry, I couldn't quite hear that. Can you try saying it again?"
    no_answer_prompt = "Sorry, I don't know how to respond to that. Can we talk about other topics, like movies, sports, or news?"
    topic_redirection_prompt = "I don't feel comfortable talking about that. Let's talk about movies, sports, or news."
