import json
from typing import Union


class ServiceModuleResponse(object):
    # allocated attributes in memory, reduce memory usage when created from a dict,
    # http://book.pythontips.com/en/latest/__slots__magic.html
    __slots__ = ['response', 'context_manager', 'user_attributes', 'additional_data']

    def __init__(self,
                 response: Union[str, list, dict],
                 context_manager: dict = {},
                 user_attributes: dict = {},
                 additional_data: dict = {}) -> None:
        """
        :param response: response object from service module
        :param context_manager: updated context manager dictionary from service module
        :param user_attributes: updated user attributes dictionary from service module
        :param additional_data: additional data from service module
        """

        if isinstance(response, dict):
            self.response = response
            self.additional_data = {}
            for key, value in response.items():
                if key not in ['response', 'context_manager', 'user_attributes']:
                    self.additional_data[key] = value
                else:
                    setattr(self, key, value)
            setattr(self, 'context_manager', response.get('context_manager', {}))
            setattr(self, 'user_attributes', response.get('user_attributes', {}))
        else:
            self.response = response
            self.context_manager = context_manager
            self.user_attributes = user_attributes
            self.additional_data = additional_data

    def save_to_state_manager(self, state_manager, module_name) -> None:
        # 1. persist response from response_object to current state
        if module_name and self.response:
            setattr(state_manager.current_state, module_name.lower(), self.response)

        # 2. persist additional context manager key-value pairs from response_object to current state
        for context_key, context_value in self.context_manager.items():
            setattr(state_manager.current_state, context_key.lower(), context_value)
        # 3. persist user attributes from response_object to UserAttributes object
        for user_attributes_key, user_attributes_value in self.user_attributes.items():
            setattr(state_manager.user_attributes, user_attributes_key, user_attributes_value)

    def __str__(self):
        return json.dumps(self.__dict__)
