from injector import inject, singleton
from typing import Dict, Any, List

from cobot_core import DialogManager
from cobot_core.log.logger import LoggerFactory
from cobot_core.nlp.pipeline import ResponseGeneratorsRunner
from cobot_core.offensive_speech_classifier import OffensiveSpeechClassifier
from cobot_core.output_responses_converter import OutputResponsesConverter
from .ranking_strategy import RankingStrategy
from .selecting_strategy import SelectingStrategy
from .state_manager import StateManager

@singleton
class AdvancedDialogManager(DialogManager):
    """
    Advanced dialog manager class will send a dict to ranking strategy.
    """
    def rank_wrapper(self,
                     output_responses: dict,
                     filtered_output_responses: list):
        ranking_strategy_input_data = self.generate_ranking_strategy_input(output_responses, filtered_output_responses)
        response = self.ranking_strategy.rank_advanced(ranking_strategy_input_data)
        return response

    def generate_ranking_strategy_input(self,
                output_responses_dict: dict,
                filtered_output_responses: List[str]) -> dict:
        result = {}
        for response_generator_name, response in output_responses_dict.items():
            if isinstance(response, list):
                new_response = [elem for elem in response if elem in filtered_output_responses]
                result[response_generator_name] = new_response
            else:
                if response in filtered_output_responses:
                    result[response_generator_name] = response
        return result