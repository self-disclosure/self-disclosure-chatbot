from datetime import datetime
from cobot_core.service_module import LocalServiceModule, RemoteServiceModule, ToolkitServiceModule
from cobot_common.service_client.exceptions import ParamValidationError, UnknownAPIError


class Ner(RemoteServiceModule):
    """
    Base class for NER. By default, it uses SpaCy 2.0.
    """


class Sentiment(RemoteServiceModule):
    """
    Base class for Sentiment. By default, it uses the Vader sentiment.
    """


class AlexaPrizeSentiment(ToolkitServiceModule):
    """
    It uses the AP science team's sentiment model hosted by AlexaPrizeToolkitService.
    """

    def execute(self):
        start = datetime.now()
        utterance = self.state_manager.current_state.text
        response = self.toolkit_service_client.batch_detect_sentiment(utterances=[utterance])

        result = None
        if 'sentimentClasses' in response and response['sentimentClasses'] and len(response['sentimentClasses']) > 0:
            result = response['sentimentClasses'][0]['sentimentClass']
            result = self._convert_to_string_label(result)

        end = datetime.now()
        self.logger.info(
            "Finished Sentiment, result: {}, latency:{}ms".format(result, (end - start).total_seconds() * 1000))
        return result


    def _convert_to_string_label(self, result):
        # 0: negative, 1: neutral, 2: positive
        if result == 0:
            return 'neg'
        elif result == 1:
            return 'neu'
        elif result == 2:
            return 'pos'
        else:
            return None

class TopicAnalyzer(ToolkitServiceModule):
    """
    Base class for Topic Analysis. By default, it uses the AP science team's topic model hosted by AlexaPrizeToolkitService.
    """

    def execute(self):
        start = datetime.now()
        utterance = self.state_manager.current_state.text
        result = None
        segmented = False
        if "segmentation" in self.input_data and self.input_data["segmentation"] is not None:
            utterance = self.input_data["segmentation"]["segmented_text_raw"]
            segmented = True
        else:
            utterance = [self.state_manager.current_state.text]

        try:
            response = self.toolkit_service_client.batch_detect_topic(utterances=utterance, timeout_in_millis=300)
        except Exception as e:
            end = datetime.now()
            self.logger.warning(
                "Finished Topic, result: {}, latency:{}ms, errors: {}".format(result, (end - start).total_seconds() * 1000, e))
            return None

        if 'topics' in response and response['topics'] and len(response['topics']) > 0:
            result = response['topics']
            # result = response['topics'][0]['topicClass'] # COBOT 2.1 Update, not applied

        end = datetime.now()
        self.logger.info(
            "Finished Topic, result: {}, latency:{}ms".format(result, (end - start).total_seconds() * 1000))
        return result


class IntentClassifier(LocalServiceModule):
    """
    Base class for Intent Classifier. By default, it uses the ASK intent model.
    """

    def execute(self):
        return None
