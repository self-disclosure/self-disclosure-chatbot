from injector import inject

from cobot_core.log.logger import LoggerFactory
from cobot_core.service_module_manager import ServiceModuleManager
from cobot_core.state_manager import StateManager
from typing import Dict, List, Any

class SelectingStrategy(object):
    """
    A generic mapper from an input string to a list of candidate response mode names, each of which must be
    registered as a response handler with the ResponseGeneratorRegistry..
    
    Base selecting strategy returns a full list of all available candidate modes, each of which maps to a
    ResponseGenerator. The BaseSelectingStrategy is not the most computationally efficient strategy, and 
    transfers all the of the responsibilities of response selection into the RankingStrategy, which then 
    must select the most coherent, reasonable response to the given user inquiry.
    
    A reasonable selecting strategy should take into account context, user intent, and any personalization 
    factors in selecting high probability response modes.
    
    See also: :py:class:`cobot_core.mapping_selecting_strategy.MappingSelectingStrategy`, PolicyStrategy, KerasPolicyStrategy
    
    """

    @inject
    def __init__(self,
                 state_manager: StateManager,
                 service_module_manager: ServiceModuleManager):
        self.state_manager = state_manager
        self.service_module_manager = service_module_manager
        self.logger = LoggerFactory.setup(self)

    def select_response_mode(self, features:Dict[str,Any])->List[str]:

        """
        Returns the list of valid ResponseGenerator modules for the given input
        """
        return self.service_module_manager.response_generator_names
