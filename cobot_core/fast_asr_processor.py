from injector import inject

from cobot_core import StateManager
from cobot_core.asr_processor import ASRProcessor


class FastASRProcessor(ASRProcessor):
    @inject
    def __init__(self,
                 state_manager: StateManager):
        self.state_manager = state_manager

    # To reduce latency when calling alexaprizetoolkitservice, disable checking whether request is offensive, as of April, 2018
    def process(self, input=None):
        setattr(self.state_manager.current_state, 'input_offensive', None)
        return None