import os
from injector import singleton
import logging
from cobot_core.log.logger import LoggerFactory

@singleton
class ServiceURLLoader(object):
    module_to_url = {
    }
    for key, value in os.environ.items():
        module_to_url[key.upper()] = value
    logging.info("module to url mapping: %s", module_to_url)

    @classmethod
    def get_url_for_module(cls, module_name):
        logger = LoggerFactory.setup(cls)

        module_name = module_name.upper()
        if module_name in ServiceURLLoader.module_to_url:
            result = ServiceURLLoader.module_to_url.get(module_name)
            # Special logic for greeter docker modules
            if module_name in ['GREETER']:
                # if result doesn't contain port: i.e.http://localhost, append port 80
                if len(result.split(":")) < 3: result = result + ":80/"
                return result + ("/" if not result.endswith("/") else "") + module_name.lower()
            return result
        else:
            logger.error("{} doesn't exist in loader {}".format(module_name, ServiceURLLoader.module_to_url))
            return ''
