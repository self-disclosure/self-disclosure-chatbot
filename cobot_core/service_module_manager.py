from injector import singleton, inject

from cobot_core.log.logger import LoggerFactory
from cobot_core.nlp.nlp_modules import IntentClassifier, Ner, Sentiment, TopicAnalyzer, AlexaPrizeSentiment
from cobot_core.service_module_config import ServiceModuleConfig
from cobot_core.service_url_loader import ServiceURLLoader
from .state_manager import StateManager
from typing import List, Dict, Any


@singleton
class ServiceModuleManager(object):
    """
        Class to manage service modules by add/update/get each module and
        manage nlp pipeline creation.
    """

    @inject
    def __init__(self,
                 state_manager: StateManager):
        self.state_manager = state_manager
        self.modules = self._load_default_modules()
        self.response_generator_names = set()
        self.nlp = [[]]
        self.nlp_flattened = set()
        self.logger = LoggerFactory.setup(self)

    # initialize module class
    def get_module(self, module_name) -> object:
        if module_name in self.modules.keys():
            service_module_config = self.modules.get(module_name)
            module_class= self.modules.get(module_name).clazz(self.state_manager, module_name, service_module_config)
            return module_class
        else:
            self.logger.error("Module not found in map: %s", module_name)

    def module_exist(self, module_name=str) -> bool:
        return module_name in self.modules.keys()

    def upsert_module(self, module=Dict):
        self.modules[module['name']] = ServiceModuleConfig(module)

    def add_response_generator_module(self, module=Dict[str, Any]):
        self.upsert_module(module)
        self.response_generator_names.add(module['name'])

    def get_response_generator_classes(self) -> List[object]:
        return self.get_module_classes(list(self.response_generator_names))

    def get_module_classes(self, module_names: List[str]) -> List[object]:
        result = []
        for module_name in module_names:
            try:
                new_module = self.get_module(module_name)
                result.append(new_module)
            except:
                self.logger.error('Exception when creating {} module'.format(module_name), exc_info=True)
        return result

    def create_nlp_pipeline(self, nlp_def: List[List[str]]):
        for step_modules in nlp_def:
            for module_name in step_modules:
                if not self.module_exist(module_name):
                    raise Exception("Module not found in map: ", module_name)
                self.nlp_flattened.add(module_name)
        self.nlp = nlp_def
        self.logger.info('NLP pipeline: %s', self.nlp_flattened)

    def get_nlp_pipeline_names(self):
        return self.nlp_flattened

    def _load_default_modules(self):
        default_modules = {}
        INTENT = {
            'name': "intent",
            'class': IntentClassifier,
            'context_manager_keys': ['text']
        }
        NER = {
            'name': "ner",
            'class': Ner,
            'url': ServiceURLLoader.get_url_for_module("nlp") + "/ner",
            # spaCy ner  model is hosted in nlp docker module
            'context_manager_keys': ['text']
            # To set client timeout, add field timeout_in_millis. For example,
            # 'timeout_in_millis': 1000
        }

        # Default returns the sentiment label only, e.g. "pos", "neg", "neu", or "compound"
        # SENTIMENT = {
        #     'name': "sentiment",
        #     'class': Sentiment,
        #     'url': ServiceURLLoader.get_url_for_module("nlp") + "/sentiment",
        #     'context_manager_keys': ['text']
        # }

        # To return the full dict of polarity scores, e.g.
        #      {'pos': 0.746, 'compound': 0.8316, 'neu': 0.254, 'neg': 0.0}
        # uncomment this block and comment other SENTIMENT definitions
        SENTIMENT = {
            'name': "sentiment",
            'class': Sentiment,
            'url': ServiceURLLoader.get_url_for_module("nlp") + "/sentiment",
            'context_manager_keys': ['text']
        }

        # To use AlexaPrizeSentiment class, uncomment this block and comment other SENTIMENT definitions
        # SENTIMENT = {
        #     'name': "sentiment",
        #     'class': AlexaPrizeSentiment,
        #     'url': 'local',
        #     'context_manager_keys': ['text']
        # }

        TOPIC = {
            'name': "topic",
            'class': TopicAnalyzer,
            'url': 'local',
            'context_manager_keys': ['text'],
            'timeout_in_millis': 1300
        }
        default_modules[INTENT['name']] = ServiceModuleConfig(INTENT)
        default_modules[NER['name']] = ServiceModuleConfig(NER)
        default_modules[SENTIMENT['name']] = ServiceModuleConfig(SENTIMENT)
        default_modules[TOPIC['name']] = ServiceModuleConfig(TOPIC)
        return default_modules
