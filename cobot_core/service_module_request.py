from cobot_core.log.logger import LoggerFactory
from .state_manager import StateManager


class ServiceModuleRequest(object):
    DEFAULT_VALUE = None   # Default value when the key doesn't exist in the object

    def __init__(self,
                 service_module_config,
                 state_manager: StateManager):
        self.state_manager = state_manager
        self.logger = LoggerFactory.setup(self)

        self.context_manager = {}
        self.user_attributes = {}

        module_name = service_module_config.name
        context_manager_keys = service_module_config.context_manager_keys
        history_turns = service_module_config.history_turns
        user_attributes_keys = service_module_config.input_user_attributes_keys

        current_state = state_manager.current_state
        session_history = state_manager.session_history
        user_attributes = state_manager.user_attributes

        # 1. Pass state info to input_data
        if len(session_history) < history_turns:
            self.logger.warning(
                'You want {} history turns for module: {}, but only {} history turns are fetched from DynamoDB for this session.'.format(
                    history_turns, module_name, len(session_history)))

        fetch_history_turns = min(len(session_history), history_turns)

        # If 'context_manager_keys' is not specified, Copy all attributes from State object to input_data
        if len(context_manager_keys) == 0:
            for attr, value in current_state.__dict__.items():
                if value:
                    context_manager_keys.append(attr)

        for key in context_manager_keys:
            try:
                values = [getattr(current_state, key, ServiceModuleRequest.DEFAULT_VALUE)]
                for turn in range(fetch_history_turns):
                    value = session_history[turn].get(key, ServiceModuleRequest.DEFAULT_VALUE)
                    values.append(value)
                # history_turns + 1 means the number of requested history turns plus the current turn
                if len(values) < history_turns + 1:
                    values.extend([ServiceModuleRequest.DEFAULT_VALUE] * (history_turns + 1 - len(values)))
                self.context_manager[key] = values
            except:
                self.logger.error("Exception when fetching {} from state manager for module: {} ".format(key, module_name), exc_info=True)

        # 2. Pass user attributes info to input_data
        # If 'input_user_attributes_key' is not specified, Copy all attributes from UserAttributes object to input_data
        if len(user_attributes_keys) == 0:
            user_attributes_keys = ['user_id']
            for attr, value in user_attributes.map_attributes.items():
                user_attributes_keys.append(attr)

        for key in user_attributes_keys:
            self.user_attributes[key] = getattr(state_manager.user_attributes, key)

        # temporary hack to keep all the existing modules (nlp, response generators) unchanged,
        # only send the current state's attributes, rather than a list of current and history turn's attributes,
        # to service modules, when clients only want to send the current state (history_turns = 0).
        if history_turns == 0:
            for key in context_manager_keys:
                self.context_manager[key] = self.context_manager[key][0]

    @property
    def input_data(self) -> dict:
        _input_data = {}
        for k, v in self.context_manager.items():
            _input_data[k] = v
        for k, v in self.user_attributes.items():
            _input_data[k] = v
        return _input_data
