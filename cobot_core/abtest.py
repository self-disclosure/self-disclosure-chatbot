import os
import random
import inspect

from cobot_core.log.logger import LoggerFactory
from .cobot_handler import CobotHandler
from cobot_python_sdk.dynamodb_manager import DynamoDbManager
from cobot_python_sdk.configuration.config_constants import ConfigConstants

"""
Helper class to instantiate alternately configured CobotHandlers (each with their own set of overrides for
SelectingStrategy, RankingStrategy, DialogManager or other key components) within a single Cobot deployment.
See demo files in example/a_b_test for simple examples of using the ABTest class.
"""
env = os.environ.get('STAGE').lower() if os.environ.get('STAGE') is not None else "local"

default_names = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

class ABTest(object):

    def __init__(self,  event,
                        context,
                        app_id,
                        user_table_name,
                        save_before_response,
                        state_table_name,
                        test_case_config,
                        level='conversation',
                        api_key=None
                        ):
        """
        :param event: event from ASK lambda function
        :param context: context from ASK lambda function
        :param app_id: replace with your ASK skill id to validate ASK request. None means skipping ASK request validation.
        :param user_table_name: replace with a DynamoDB table name to store user preference data. We will auto create the DynamoDB table if the table name doesn't exist.
                                 None means user preference data won't be persisted in DynamoDB.
        :param save_before_response: If it is true, skill persists user preference data at the end of each turn.
                                 Otherwise, only at the last turn of whole session.
        :param state_table_name: replace with a DynamoDB table name to store session state data. We will auto create the DynamoDB table if the table name doesn't exist.
                                 None means session state data won't be persisted in DynamoDB.
        :param test_case_config: List of test case configurations for Cobot A/B testing.
                                 Each configuration is a dictionary with keys 'name', 'ratio', 'overrides', 'response_generators', and 'nlp_modules'.
        :param level: level of A/B test, choose between conversation and utterance.
        :param api_key: API key for AlexaPrizeToolkitService.
        """
        self.validate_input(test_case_config, level)
        self.test_cases = []
        self.level = level

        self.logger = LoggerFactory.setup(self, event)

        for config in test_case_config:
            name = config['name']
            ratio = config['ratio']
            overrides = config['overrides']
            response_generators = config['response_generators']
            nlp_modules = config['nlp_modules']

            cobot = CobotHandler(event=event,
                                 context=context,
                                 app_id=app_id,
                                 user_table_name=user_table_name,
                                 save_before_response=save_before_response,
                                 state_table_name=state_table_name,
                                 overrides=overrides,
                                 name=name,
                                 api_key=api_key
                                 )

            if response_generators:
                cobot.add_response_generators(response_generators)

            if nlp_modules:
                self.add_nlp_modules(cobot, nlp_modules)

            test_case = {'cobot': cobot,
                         'ratio': ratio}
            self.test_cases.append(test_case)
            self.save_config(config)


    def validate_input(self, test_case_config, level):
        # Validate that level is either conversation or utterance
        if level not in ['conversation', 'utterance']:
            raise ValueError("Valid level argument should be either conversation or utterance.")

        # Validate that at least two test cases are provided
        if len(test_case_config) < 2:
            raise ValueError("AB test case configuration must contain at least 2 test cases.")

        # Validate that no more than 100 test cases are provided
        if len(test_case_config) > 100:
            raise ValueError("AB test case configuration must not contain more than 100 test cases.")

        # Provide default config values
        default_name_index = 0
        for config in test_case_config:
            if 'name' not in config:
                if default_name_index > len(default_names) - 1:
                    raise KeyError("You have specified more than " + str(len(default_names)) +
                                   " unnamed AB test cases and exceeded the number of default names. "
                                   "Please define the 'name' key for all remaining test cases.")
                config['name'] = default_names[default_name_index]
                default_name_index += 1
            if 'ratio' not in config:
                raise KeyError("Please specify an integer ratio in the range [0,100] for each AB test case.")
            if 'overrides' not in config:
                config['overrides'] = None
            if 'response_generators' not in config:
                config['response_generators'] = None
            if 'nlp_modules' not in config:
                config['nlp_modules'] = [[{'name': 'intent'}, {'name': 'ner'}, {'name': 'sentiment'}, {'name': 'topic'}]]

        # Validate that names are unique
        names = set()
        for config in test_case_config:
            name = config['name']
            if name in names:
                raise ValueError("AB test case names must be unique.")
            names.add(name)

        # Validate that ratios sum to 100
        sum_of_ratios = 0
        for config in test_case_config:
            ratio = config['ratio']
            if not isinstance(ratio, int) or ratio < 0 or ratio > 100:
                raise ValueError("AB test case ratio must be an integer in the range of [0,100].")
            sum_of_ratios += ratio
        if sum_of_ratios != 100:
            raise ValueError("AB test case ratios must sum to 100.")


    def add_nlp_modules(self, cobot, nlp_modules):
        nlp_def = []
        for step in nlp_modules:
            step_modules = []
            for module in step:
                if 'name' not in module:
                    raise KeyError("NLP module definition must contain key: name.")
                if len(module) > 1: # Custom NLP module definition
                    cobot.upsert_module(module)
                step_modules.append(module['name'])
            nlp_def.append(step_modules)
        cobot.create_nlp_pipeline(nlp_def)


    def save_config(self, config):
        try:
            DynamoDbManager.ensure_table_exists(ConfigConstants.AB_CONFIG_TABLE_NAME, 'ab_config')

            # Get source code of a function in filesystem: https://docs.python.org/3/library/inspect.html#inspect.getsourcelines
            if config['overrides']:
                overrides = ("".join(inspect.getsourcelines(config['overrides'])[0][1:]))
            else:
                overrides = None

            item_dict = {
                'name': config['name'],
                'ratio': config['ratio'],
                'overrides': overrides,
                'response_generators': str(config['response_generators']),
                'nlp_modules': str(config['nlp_modules'])
            }

            DynamoDbManager.put_item(table_name=ConfigConstants.AB_CONFIG_TABLE_NAME, item_dict=item_dict)
        except:
            self.logger.error("Exception when storing AB Config to DynamoDB", exc_info=True)


    def run(self):
        selected_socialbot = self.select_a_socialbot()
        self.logger.info("[ABTEST] Seltected AB case: {}".format(selected_socialbot.name))
        if env == "local":
            print("[DEBUG] abtest case: {}".format(selected_socialbot.name))
        selected_socialbot.state_manager.current_state.set_new_field('a_b_test', selected_socialbot.name)
        return selected_socialbot.execute()


    def select_a_socialbot(self):
        new_session_flag = self.test_cases[0]['cobot'].event.get('session.new', None)
        last_state = self.test_cases[0]['cobot'].state_manager.last_state

        if not new_session_flag and self.level == 'conversation' and last_state:
            last_state_test_case = last_state.get('a_b_test', None)
            last_state_session_id = last_state.get('session_id', None)
            current_state_session_id = getattr(self.test_cases[0]['cobot'].state_manager.current_state, 'session_id', None)
            if last_state_test_case and last_state_session_id == current_state_session_id:
                for test_case in self.test_cases:
                    if last_state_test_case == test_case['cobot'].name:
                        self.logger.info('Existing Session AB Test select %s', test_case['cobot'].name)
                        return test_case['cobot']

        random_int = random.randint(0, 99)
        threshold = 0
        for test_case in self.test_cases:
            threshold += test_case['ratio']
            if random_int < threshold:
                self.logger.info('AB Test random select %s', test_case['cobot'].name)
                return test_case['cobot']



