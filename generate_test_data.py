import pandas as pd
import random
import json
import copy
import argparse


def read_csv():
    df = pd.read_csv("annotated_capc.csv", )
    return df


def dump_to_json(list_of_utterances):
    if list_of_utterances == []:
        print("Inavlid utterance kind \n Exit with return code 1")
    else:
        output_file = open("./bin/test_auto_generated_test.json", 'w', encoding='utf-8')
        json.dump(list_of_utterances, output_file)
        output_file.close()


def get_top_100(df):
    empty_json = get_test_template()
    utterances_list = list()
    for each in list(df["text"].head(100)):
        empty_copy = copy.deepcopy(empty_json)
        empty_copy["session"]["new"] = False
        empty_copy["request"]["intent"]["slots"]["text"]["value"] = each
        utterances_list.append(empty_copy)
    return utterances_list
    # return create_test_list(list(df["text"].head(100)), get_test_template(), len(list(df["text"].head(100))))


def get_test_template():
    template = {
        "session": {
            "new": "",
            "sessionId": "SessionId.f37c8cce-ac01-43bf-bdb9-9c45748482cf",
            "application": {
                "applicationId": "amzn1.ask.skill.6ef703a6-7518-486b-83f7-1b44ee139dda"
            },
            "attributes": {
                "lastState": "{\"user_id\": \"amzn1.ask.account.AH3WVAEUEHZCPMQKXZRMLPWRH5LCWY4A332N4BM6MYGNLXKL7YC5SL75BSS6GSJFYNUXNZZYL5WUC7LVJ53VQ5T3FIXAFUOPFGNRDFVE4RDBUAFSBEROEPOYTNFHGJLOULAYZ2BEA24T5FCQA44JTTBSB2PAGGKSBWRAI33DRJFD6SNX5CUIXTFX4EBPXWPKDB3UQV2WLPBITGQ\", \"conversation_id\": null, \"session_id\": \"SessionId.f37c8cce-ac01-43bf-bdb9-9c45748482cf\", \"intent\": \"topic_request\", \"slots\": {\"topic\": {\"name\": \"topic\", \"value\": \"tennis\"}}, \"creation_date_time\": \"2018-02-23T00:05:16.317894\", \"topic\": \"Games\", \"asr\": [], \"text\": \"tennis\", \"response\": \"Sure, I know a few things about tennis. Let's talk about that.\", \"mode\": null, \"input_offensive\": 0, \"sentiment\": \"Positive\", \"ner\": [], \"features\": {\"Step1\": null, \"intent\": \"topic_request\"}, \"candidate_responses\": {\"RULES\": {\"response\": \"Sure, I know a few things about tennis. Let's talk about that.\", \"performance\": [0.08932018280029297], \"error\": false}}}"
            },
            "user": {
                "userId": "amzn1.ask.account.AH3WVAEUEHZCPMQKXZRMLPWRH5LCWY4A332N4BM6MYGNLXKL7YC5SL75BSS6GSJFYNUXNZZYL5WUC7LVJ53VQ5T3FIXAFUOPFGNRDFVE4RDBUAFSBEROEPOYTNFHGJLOULAYZ2BEA24T5FCQA44JTTBSB2PAGGKSBWRAI33DRJFD6SNX5CUIXTFX4EBPXWPKDB3UQV2WLPBITGQ"
            }
        },
        "request": {
            "type": "IntentRequest",
            "requestId": "EdwRequestId.bc4967b2-df0e-4d50-a260-e4a0fdd9c7fc",
            "intent": {
                "name": "general",
                "slots": {
                    "text": {
                        "name": "text",
                        "value": ""
                    }
                }
            },
            "locale": "en-US",
            "timestamp": "2018-02-23T00:05:41Z"
        },
        "context": {
            "AudioPlayer": {
                "playerActivity": "IDLE"
            },
            "System": {
                "application": {
                    "applicationId": "amzn1.ask.skill.6ef703a6-7518-486b-83f7-1b44ee139dda"
                },
                "user": {
                    "userId": "amzn1.ask.account.AH3WVAEUEHZCPMQKXZRMLPWRH5LCWY4A332N4BM6MYGNLXKL7YC5SL75BSS6GSJFYNUXNZZYL5WUC7LVJ53VQ5T3FIXAFUOPFGNRDFVE4RDBUAFSBEROEPOYTNFHGJLOULAYZ2BEA24T5FCQA44JTTBSB2PAGGKSBWRAI33DRJFD6SNX5CUIXTFX4EBPXWPKDB3UQV2WLPBITGQ"
                },
                "device": {
                    "supportedInterfaces": {}
                }
            }
        },
        "version": "1.0"
    }
    return template


def create_test_list(all_intents, empty_json, static_count):
    utterances_list = list()
    for each in all_intents:
        for i in range(0, static_count):
            empty_copy = copy.deepcopy(empty_json)
            empty_copy["session"]["new"] = False
            empty_copy["request"]["intent"]["slots"]["text"]["value"] = random.choice(all_intents[each])
            utterances_list.append(empty_copy)
            # empty_copy["request"]["intent"]["slots"]["text"]["value"] = ""
    return utterances_list


def generate_utterances_for_each_intent(system, topic, lexical, df, utterance, iter):
    # static_iterator = iter
    # kind = utterance
    test_list = []
    template_json = get_test_template()

    all_system_intents = dict()
    for intent in system:
        if intent == utterance:
            all_system_intents[intent] = list(df.text[df[intent] == True])
    test_list.extend(create_test_list(all_system_intents, template_json, iter))

    all_topic_intents = dict()
    for intent in topic:
        if intent == utterance:
            all_topic_intents[intent] = list(df.text[df[intent] == True])
    test_list.extend(create_test_list(all_topic_intents, template_json, iter))

    all_lexical_intents = dict()
    for intent in lexical:
        if intent == utterance:
            all_lexical_intents[intent] = list(df.text[df[intent] == True])
    test_list.extend(create_test_list(all_lexical_intents, template_json, iter))

    return test_list


def get_sample_utterances(data, utterance_type, iter_val):
    system_level_intents = ['say_thanks', 'req_topic', 'req_more', 'req_playmusic', 'say_bio', 'clarify',
                            'change_topic', 'say_thanks', 'complaint', 'say_funny', 'not_complete', 'terminate',
                            'req_playgame', 'say_comfort', 'req_task', 'adjust_speak', 'req_location']

    topic_level_intents = ['topic_phatic', 'topic_movietv', 'topic_music', 'topic_backstory', 'topic_storymeme',
                           'topic_qa', 'topic_sport', 'topic_newspolitics', 'topic_chichat', 'topic_book',
                           'topic_weather', 'topic_techscience', 'topic_food', 'topic_entertainment',
                           'topic_relationship', 'topic_game', 'topic_others', 'topic_sport', 'topic_holiday',
                           'topic_animal', 'topic_controversial', 'topic_psiphy', 'topic_history', 'topic_finance',
                           'topic_health', 'topic_travel']

    lexical_level_intents = ['ans_unknown', 'ans_pos', 'ans_neg', 'ask_name', 'ask_degree', 'ask_time', 'ask_fact',
                             'ask_ability', 'ask_hobby', 'ans_like', 'ask_factopinion', 'ask_reason', 'info_time',
                             'ans_wish', 'ans_factopinion', 'ask_yesno', 'ask_person', 'ask_preference', 'ask_loc',
                             'ask_freq', 'ask_advice', 'ask_opinion', 'ask_self', 'ask_dist', 'ask_recommend',
                             'info_age', 'ask_count', 'info_name', 'info_loc']

    return generate_utterances_for_each_intent(system_level_intents, topic_level_intents, lexical_level_intents, data,
                                               utterance_type, iter_val)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(add_help=False, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-h', '--help', action='help', default=argparse.SUPPRESS,
                        help="generates a test file using annotated_capc.csv based on the given utterance type \n"
                             "creates the test file at location bin/test_auto_certificate_event.json"
                             "usage: python3 generate_test_data.py [utterance_type] [number] \n"
                             "utterance_type: the type of utterance"
                             "count: number of utterances \n")
    parser.add_argument("utterance_type", type=str, help="the kind of utterance to generate test cases for")
    parser.add_argument("count", type=int, help="selects the total number of system, topic and lexical intents "
                                                       "each")
    args = parser.parse_args()
    df = read_csv()

    dump_to_json(get_sample_utterances(df, args.utterance_type, args.count))
    # dump_to_json(get_top_100(df))


