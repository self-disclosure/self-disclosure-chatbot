from elasticsearch import Elasticsearch
import csv
from time import sleep
import sys
import argparse
from tqdm import tqdm
import datetime
import json
import numpy as np


# def print_progress(iteration, total, prefix='', suffix='', decimals=1, bar_length=100):
#     """
#     Call in a loop to create terminal progress bar
#     @params:
#         iteration   - Required  : current iteration (Int)
#         total       - Required  : total iterations (Int)
#         prefix      - Optional  : prefix string (Str)
#         suffix      - Optional  : suffix string (Str)
#         decimals    - Optional  : positive number of decimals in percent complete (Int)
#         bar_length  - Optional  : character length of bar (Int)
#     """
#     str_format = "{0:." + str(decimals) + "f}"
#     percents = str_format.format(100 * (iteration / float(total)))
#     filled_length = int(round(bar_length * iteration / float(total)))
#     bar = '█' * filled_length + '-' * (bar_length - filled_length)

#     sys.stdout.write('\r%s |%s| %s%s %s' % (prefix, bar, percents, '%', suffix)),

#     if iteration == total:
#         sys.stdout.write('\n')
#     sys.stdout.flush()


es = Elasticsearch(
    ['https://search-elasticalexa-7svdsqy32hd3onb7bytqu25gma.us-east-1.es.amazonaws.com'])


def update_query(start_date, end_date):
    q = {
        'size' : 10000,
        "query": {
            "bool": {
                "filter":
                {
                    "range": {
                        "date": {
                            "gte": start_date,
                            "lt": end_date
                        }
                    }
                }
            }
         }
        }
    return q


def get_allcid(arg_startdate, arg_enddate):
    all_cid = []
    all_cidrating = []
    arg_startdate = datetime.datetime.strptime(arg_startdate, "%Y-%m-%d")
    arg_nextdate = arg_startdate + datetime.timedelta(1)
    arg_enddate = datetime.datetime.strptime(arg_enddate, "%Y-%m-%d")
    while arg_nextdate <= arg_enddate:
        new_q = update_query(arg_startdate.strftime("%Y-%m-%d"), arg_nextdate.strftime("%Y-%m-%d"))
        res = es.search(index="conversation_ratings", body=new_q)
        #total = res['hits']['total']
        arg_startdate = arg_nextdate
        arg_nextdate = arg_nextdate + datetime.timedelta(1)
        for hit in res['hits']['hits']:
            cid = hit["_source"]["conversation_id"]
            if cid not in all_cid:
                rating = hit["_source"]["rating"]
                all_cid.append(cid)
                all_cidrating.append({'cid': cid, 'rating': rating})
            else:
                continue
    return all_cidrating

def save_chatlog(arg_startdate, arg_enddate, arg_output):

    with open(args.output, 'w') as csvfile:
        fieldnames = ['cid', 'ts', 'rating','type','text', 'sentiment', 'avg_asr_score', 'ner', 'sys_intent', 'topic_intent', 'lexical_intent', 'selected_modules']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        dt = {}
        print("Retrieving conversations and write to {}".format(arg_output))
        #print_progress(0, total, prefix = 'Progress:', suffix = 'Complete')
        count = 0
        all_cidrating = get_allcid(arg_startdate, arg_enddate)
        total_cid = len(all_cidrating)

        for hit in tqdm(all_cidrating, total = total_cid, unit = 'cid'):
            dt["cid"] = hit['cid']
            dt["rating"] = hit['rating']
            q = {
                'size' : 10000,
                "sort" : [
                    { "timestamp" : "asc" }
                ],
                "query": {
                    "match": {
                        "conversation_id": {
                            "query": dt["cid"]
                        }
                    }
                }
            }
            utt = es.search(index="prod_*", body=q)
            for ut in utt['hits']['hits']:
                dt["ts"] = ut["_source"]["timestamp"]
                dt["type"] = ut["_type"]
                dt["text"] = ut["_source"]["text"]
                if dt["type"] == 'request':
                    current_state_dict = json.loads(ut["_source"]["current_state"])
                    try:
                        dt['sentiment'] = current_state_dict["sentiment"]
                    except:
                        dt['sentiment'] = ''
                    if isinstance(current_state_dict['features']['ner'], list):
                        dt['ner'] = ' '.join([ner['text'] for ner in current_state_dict['features']['ner']])
                    else:
                        dt['ner'] = None
                    dt['avg_asr_score'] =  str(np.mean([token["confidence"] for token in current_state_dict["asr"][0]['tokens']]))
                    dt['sys_intent'] = ' '.join(current_state_dict['features']['intent_classify']['sys'])
                    dt['topic_intent'] = ' '.join(current_state_dict['features']['intent_classify']['topic'])
                    dt['lexical_intent'] = ' '.join(current_state_dict['features']['intent_classify']['lexical'])
                    dt['selected_modules'] = ' '.join(current_state_dict['selected_modules'])
                else:
                    try:
                        dt['sentiment'] = current_state_dict["sentiment"]
                    except:
                        dt['sentiment'] = ''
                    dt['ner'] = ''
                    dt['avg_asr_score'] = ''
                    dt['sys_intent'] =  ' '.join(current_state_dict['features']['intent_classify']['sys'])
                    dt['topic_intent'] = ' '.join(current_state_dict['features']['intent_classify']['topic'])
                    dt['lexical_intent'] = ' '.join(current_state_dict['features']['intent_classify']['lexical'])
                    dt['selected_modules'] = ' '.join([k for k,v in current_state_dict.items() if v==dt["text"]])
                writer.writerow(dt)

                sleep(0.1)
                # Update Progress Bar
                # print_progress(count + 1, len(all_cidrating), prefix = 'Progress:', suffix = 'Complete', bar_length=30)
                # count = count + 1



if __name__ == "__main__":
    now = datetime.datetime.now()
    parser = argparse.ArgumentParser(description='Search range.')
    yesterday = datetime.date.today() - datetime.timedelta(1)
    parser.add_argument('--start', '-s', default=yesterday.strftime("%Y-%m-%d"),
                       help='timerange from (timestamp or date YYYY-mm-dd')
    parser.add_argument('--end', '-e', default=now.strftime("%Y-%m-%d"),
                       help='timerange to (timestamp or date YYYY-mm-dd')
    parser.add_argument('--output', '-o', default= 'output.csv',
                       help='output file')
    print("===================================")
    print("Retrieving ratings...")
    args = parser.parse_args()
    save_chatlog(args.start, args.end, args.output)



