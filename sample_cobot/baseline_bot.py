import os
from injector import inject
from cobot_core.nlp.nlp_modules import Sentiment, AlexaPrizeSentiment
import cobot_core as Cobot
from cobot_core.nlp.pipeline import ResponseGeneratorsRunner, CallModulesWithThreadPool
from cobot_core.offensive_speech_classifier import OffensiveSpeechClassifier
from cobot_core.response_builder import RepromptBuilder
from cobot_core.service_module import RemoteServiceModule
from cobot_core.service_module import ToolkitServiceModule
from cobot_core.service_module import LocalServiceModule
# from sample_cobot.sample_events import new_session_event,existing_session_event, retrieval_event
from cobot_core.service_module_manager import ServiceModuleManager
from cobot_core.service_url_loader import ServiceURLLoader
from miniskills.custom_nlp.corrected_ask_intent_classifier import CorrectedAskIntentClassifier
from miniskills.movies.movies_response_generator import MoviesResponseGenerator
from miniskills.news.news_response_generator import NewsResponseGenerator
from miniskills.share_facts.share_facts_response_generator import ShareFactsResponseGenerator
from miniskills.shower_thoughts.shower_thoughts_response_generator import ShowerThoughtsResponseGenerator
from miniskills.special_user_utterance.special_user_utterance_response_generator import SpecialUserUtteranceResponseGenerator
from miniskills.global_generators.global_goodbye_response_generator import GlobalGoodbyeResponseGenerator
from miniskills.global_generators.global_greeting_response_generator import GlobalGreetingResponseGenerator
from miniskills.global_generators.global_invalid_response_generator import GlobalInvalidResponseGenerator
from miniskills.global_generators.global_launch_response_generator import GlobalLaunchResponseGenerator
from miniskills.custom_nlp.miniskill_reprompt_builder import MiniskillRepromptBuilder
from miniskills.custom_nlp.custom_selecting_strategy import CustomSelectingStrategy
from miniskills.constants.miniskills import Miniskills
from miniskills.constants.miniskill_list import MiniskillList
from miniskills.custom_nlp.user_response_module.user_response_module import UserResponseModule
from miniskills.custom_nlp.knowledge_module.knowledge_module import KnowledgeModule
from miniskills.custom_nlp.evi_module.evi_module import EviModule
from miniskills.custom_nlp.initial_processing.noun_phrase_extraction_module import NounPhraseExtractionModule
from miniskills.custom_nlp.correctional_processing.coreference_extraction_module import CoreferenceExtractionModule
from miniskills.custom_nlp.correctional_processing.asr_correction_module import AsrCorrectionModule
from miniskills.ask_question.ask_question_response_generator import AskQuestionResponseGenerator
class CustomResponseGeneratorRunner(ResponseGeneratorsRunner):
    @inject
    def __init__(self,
                 service_module_manager: ServiceModuleManager,
                 call_modules: CallModulesWithThreadPool,
                 save_mode=False):
        self.service_module_manager = service_module_manager
        self.call_modules = call_modules
        self.save_mode = save_mode


def overrides(binder):
    binder.bind(RepromptBuilder, to=MiniskillRepromptBuilder)
    binder.bind(Cobot.ASRProcessor, to=Cobot.FastASRProcessor)
    binder.bind(Cobot.DialogManager, to=Cobot.AdvancedDialogManager)
    binder.bind(Cobot.SelectingStrategy, to=CustomSelectingStrategy)

def lambda_handler(event, context):
    # app_id: replace with your ASK skill id to validate ask request. None means skipping ASK request validation.
    # user_table_name: replace with a DynamoDB table name to store user preference data. We will auto create the
    #                  DynamoDB table if the table name doesn’t exist.
    #                  None means user preference data won’t be persisted in DynamoDB.
    # save_before_response: If it is true, skill persists user preference data at the end of each turn.
    #                       Otherwise, only at the last turn of whole session.
    # state_table_name: replace with a DynamoDB table name to store session state data. We will auto create the
    #                   DynamoDB table if the table name doesn’t exist.
    #                   None means session state data won’t be persisted in DynamoDB.
    # overrides: provide custom override for dialog manager components.
    # api_key: replace with your api key
    if os.environ.get('STAGE') == 'PROD':
        USER_TABLE_NAME='SampleBotUserTable'
        STATE_TABLE_NAME='SampleBotStateTable'
    else:
        USER_TABLE_NAME='SampleBotUserTableBeta'
        STATE_TABLE_NAME='SampleBotStateTableBeta'
    API_KEY = os.environ.get('API_KEY', 'EwecQ1fkEd4YMVUOHHpeSam7zbef53gipQsxAUki')

    cobot = Cobot.handler(event,
                          context,
                          app_id=None,
                          user_table_name=USER_TABLE_NAME,
                          save_before_response=True,
                          state_table_name=STATE_TABLE_NAME,
                          overrides=overrides,
                          api_key=API_KEY)

    USER_RESPONSE = {
        'name': "user_response_information",
        'class': UserResponseModule,
        'url': 'local',
        'context_manager_keys': ['text']
    }
    cobot.upsert_module(USER_RESPONSE)

    INTENT = {
        'name': "intent",
        'class': CorrectedAskIntentClassifier,
        'url': 'local',
        'context_manager_keys': ['text', 'user_response_information']
    }
    cobot.upsert_module(INTENT)

    EVI = {
        'name': "evi",
        'class': EviModule,
        'url': 'local',
        'context_manager_keys': ['text', 'intent']
    }
    cobot.upsert_module(EVI)

    NOUN_PHRASES = {
         'name': "nounphrases",
         'class': NounPhraseExtractionModule,
         'url': ServiceURLLoader.get_url_for_module("nounphrases"),
         'context_manager_keys': ['text', 'response'],
         'history_turns': 1
     }
    cobot.upsert_module(NOUN_PHRASES)

    COREF = {
         'name': "coref",
         'class': CoreferenceExtractionModule,
         'url': ServiceURLLoader.get_url_for_module("coref"),
         'context_manager_keys': ['text', 'response'],
         'history_turns': 1
     }
    cobot.upsert_module(COREF)

    ASR_CORRECTION = {
         'name': "asrcorr",
         'class': AsrCorrectionModule,
         'url': ServiceURLLoader.get_url_for_module("asrcorr"),
         'context_manager_keys': ['text', 'nounphrases', 'asr']
     }
    cobot.upsert_module(ASR_CORRECTION)

    KNOWLEDGE = {
        'name': "knowledge",
        'class': KnowledgeModule,
        'url': 'local',
        'context_manager_keys': ['text', 'user_response_information']
    }
    cobot.upsert_module(KNOWLEDGE)


    nlp_def = [["nounphrases"], ["coref", "asrcorr"], ["user_response_information"], ["intent"], ["knowledge", "evi"]]
    cobot.create_nlp_pipeline(nlp_def)

    ShareFacts = {
        'name': 'SHAREFACTS',
        'class': ShareFactsResponseGenerator,
        'url': 'local'
    }

    News = {
        'name': 'NEWS',
        'class': NewsResponseGenerator,
        'url': 'local'
    }

    Launch = {
        'name': 'LAUNCH',
        'class': GlobalLaunchResponseGenerator,
        'url': 'local'
    }

    Invalid = {
        'name': 'INVALID',
        'class': GlobalInvalidResponseGenerator,
        'url': 'local'
    }

    Greeting = {
        'name': 'GREETING',
        'class': GlobalGreetingResponseGenerator,
        'url': 'local'
    }

    Goodbye = {
        'name': 'GOODBYE',
        'class': GlobalGoodbyeResponseGenerator,
        'url': 'local'
    }

    SpecialUserUtterance = {
        'name': 'SPECIALUSERUTTERANCE',
        'class': SpecialUserUtteranceResponseGenerator,
        'url': 'local'
    }

    ShowerThoughts = {
        'name': 'SHOWERTHOUGHTS',
        'class': ShowerThoughtsResponseGenerator,
        'url': 'local'
    }

    Movies = {
        'name': 'MOVIES',
        'class': MoviesResponseGenerator,
        'url': 'local'
    }

    AskQuestion = {
        'name': 'ASKQUESTION',
        'class': AskQuestionResponseGenerator,
        'url': 'local'
    }

    cobot.add_response_generators([Launch, Invalid, SpecialUserUtterance, News, Greeting, Goodbye, AskQuestion])
    return cobot.execute()

# if __name__ == '__main__':
# 	lambda_handler(event=new_session_event, context={})
