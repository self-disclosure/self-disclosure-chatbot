## happy_path               <!-- name of the story - just for debugging -->
* _greet              
  - utter_greet

## mood_great
* _mood_great               <!-- user utterance, in format _intent[entities] -->
  - utter_happy

## mood_unhappy_1
* _mood_unhappy
  - utter_cheer_up
  - utter_did_that_help
* _no_intent
  - utter_cheer_up

## mood_unhappy_2
* _mood_unhappy
  - utter_cheer_up
  - utter_did_that_help
* _yes_intent
  - utter_happy

## say_goodbye
* _goodbye
  - utter_goodbye

## topic_request_more_information
* _topic_request
  - utter_new_topic
* _more_info_request
  - utter_more_info

## more_information
* _more_info_request
  - utter_more_info

## topic_request_with_opinion
* _topic_request
  - utter_new_topic
* _opinion_request
  - utter_opinion

## topic_request_opinion_more_info
* _topic_request
  - utter_new_topic
* _opinion_request
  - utter_opinion
* _more_info_request
  - utter_more_info

## opinion_request
* _opinion_request
  - utter_opinion

## frustration
* _frustration
  - utter_frustration

## error
* _error
  - utter_error