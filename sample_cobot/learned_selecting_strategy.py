from cobot_core.service_module_config import ServiceModules
from injector import inject, Key
from cobot_core.state_manager import StateManager
from cobot_core.selecting_strategy import SelectingStrategy
from cobot_common.vectorize import Vectorizer
from typing import Dict, List, Any
from sagemaker.mxnet.model import MXNetPredictor
import numpy

EndpointName = Key("endpoint_name")


class LearnedSelectingStrategy(SelectingStrategy):

    """
    LearnedSelectingStrategy is a SelectingStrategy that learns a mapping from intent sequences (and potentially other state information) to ResponseGenerator modules.

    This implementation uses a SageMaker endpoint, which is trained and deployed offline (see dialog_train.py). The endpoint name is passed in as an argument at initialization time.
    
    """

    @inject
    def __init__(self, endpoint_name: EndpointName, state_manager: StateManager):
        self.endpoint_name = endpoint_name
        self.state_manager = state_manager

    def select_response_mode(self, features:Dict[str,Any])->List[str]:
        """
        Returns the list of valid ResponseGenerator modules for the given input
        """
        intent = features['intent']
        all_rgs = ServiceModules.ResponseGeneratorModulesMap.keys()

        vectorizer = Vectorizer(all_rgs,model_file="cobot-skill/models/en-US.json")
        turns = [[intent,'']]

        (inputs,outputs) = vectorizer.vectorize_turns(turns)
        predictor = MXNetPredictor('sagemaker-dialog-model-test')

        input = inputs[0]
        result = int(predictor.predict([input]))
        #print(result)
        result = vectorizer.invert_output(numpy.array([result]))
        #print(result)

        return result
