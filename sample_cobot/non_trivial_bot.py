import os
from injector import inject
from cobot_core.nlp.nlp_modules import Sentiment, AlexaPrizeSentiment
import cobot_core as Cobot
from cobot_core.nlp.pipeline import ResponseGeneratorsRunner, CallModulesWithThreadPool
from cobot_core.offensive_speech_classifier import OffensiveSpeechClassifier
from cobot_core.response_builder import RepromptBuilder
from cobot_core.service_module import RemoteServiceModule
from cobot_core.service_module import ToolkitServiceModule
# from sample_cobot.sample_events import new_session_event,existing_session_event, retrieval_event
from cobot_core.service_module_manager import ServiceModuleManager
from cobot_core.service_url_loader import ServiceURLLoader
from sample_cobot.custom_ranking_strategy_using_conversation_evaluator import \
    CustomRankingStrategyUsingConversationEvaluator
from sample_cobot.dialog_act_intent_classifier import DialogActIntentClassifier


class EviResponseGeneratorFromService(ToolkitServiceModule):
    def execute(self):
        isWhitelisted = True
        if not isWhitelisted:
            intent = self.input_data['intent']
            slots = self.input_data['slots']
            question_word = slots['question_word']['value']
            question_text = question_word + ' ' + slots['question_text']['value']
        else:
            question_text = self.input_data['text']

        self.logger.info('Evi question_text: %s', question_text)
        r = self.toolkit_service_client.get_answer(question=question_text)
        result = ''
        if 'response' in r:
            result = r['response']
        elif 'message' in r:
            result = r['message']
        self.logger.info('Evi response: %s', result)
        return result


class CustomSentiment(Sentiment):
    def execute(self):
        return 'Positive'


class CustomOffensive(OffensiveSpeechClassifier):
    def classify(self, input_data_list):
        """
        return whether the text is offensive, 0: not-offensive, 1: offensive. This method is used in default ASRProcessor and NLP Pipeline.
        :return: Number
        """
        return [0] * len(input_data_list)


class CustomResponseGeneratorRunner(ResponseGeneratorsRunner):
    @inject
    def __init__(self,
                 service_module_manager: ServiceModuleManager,
                 call_modules: CallModulesWithThreadPool,
                 save_mode=False):
        self.service_module_manager = service_module_manager
        self.call_modules = call_modules
        self.save_mode = save_mode


class CustomRepromptBuilder(RepromptBuilder):

    def build(self, speech_output):
        # access to state manager, i.e. current_state = self.state_manager.current_state
        return 'custom reprompt for {}'.format(speech_output)

def overrides(binder):
    intent_map = {'QAIntent': 'EVI', 'greet': 'RULES', 'topic_request': 'RULES', 'retrieval': 'RETRIEVAL' }
    binder.bind(Cobot.IntentMap, to=intent_map)
    binder.bind(Cobot.SelectingStrategy, to=Cobot.MappingSelectingStrategy)
    # binder.bind(OffensiveSpeechClassifier, to=CustomOffensive)
    # binder.bind(ResponseGeneratorsRunner, CustomResponseGeneratorRunner)
    binder.bind(Cobot.ASRProcessor, Cobot.FastASRProcessor)
    binder.bind(Cobot.DialogManager, Cobot.AdvancedDialogManager)
    # binder.bind(Cobot.RankingStrategy, CustomRankingStrategyUsingConversationEvaluator)
    # binder.bind(Cobot.RepromptBuilder, CustomRepromptBuilder)
    pass



def lambda_handler(event, context):
    # app_id: replace with your ASK skill id to validate ask request. None means skipping ASK request validation.
    # user_table_name: replace with a DynamoDB table name to store user preference data. We will auto create the
    #                  DynamoDB table if the table name doesn’t exist.
    #                  None means user preference data won’t be persisted in DynamoDB.
    # save_before_response: If it is true, skill persists user preference data at the end of each turn.
    #                       Otherwise, only at the last turn of whole session.
    # state_table_name: replace with a DynamoDB table name to store session state data. We will auto create the
    #                   DynamoDB table if the table name doesn’t exist.
    #                   None means session state data won’t be persisted in DynamoDB.
    # overrides: provide custom override for dialog manager components.
    # api_key: replace with your api key

    if os.environ.get('STAGE') == 'PROD':
        USER_TABLE_NAME='UserTable'
        STATE_TABLE_NAME='StateTable'
    else:
        USER_TABLE_NAME='UserTableBeta'
        STATE_TABLE_NAME='StateTableBeta'
    API_KEY = os.environ.get('API_KEY', None)

    cobot = Cobot.handler(event,
                          context,
                          app_id=None,
                          user_table_name=USER_TABLE_NAME,
                          save_before_response=True,
                          state_table_name=STATE_TABLE_NAME,
                          overrides=overrides,
                          api_key=API_KEY)

    # Uncomment it if you want to use Alexa Prize Sentiment model, rather than vaderSentiment model.
    # SENTIMENT = {
    #     'name': "sentiment",
    #     'class': AlexaPrizeSentiment,
    #     'url': 'local',
    #     'context_manager_keys': ['text']
    # }
    # cobot.upsert_module(SENTIMENT)

    # Uncomment it if you want to use Alexa Prize Dialog Act Intent Classifier model, rather than the default ASK Intent Classifier model.
    # INTENT = {
    #     'name': "intent",
    #     'class': DialogActIntentClassifier,
    #     'context_manager_keys': ['text']
    # }
    # cobot.upsert_module(INTENT)
    # nlp_def = [["ner", "sentiment", "topic"], ["intent"]]

    nlp_def = [["ner", "sentiment", "topic", "intent"]]
    cobot.create_nlp_pipeline(nlp_def)

    EviBot = {
        'name': "EVI",
        'class': EviResponseGeneratorFromService,
        'url': 'local',
        'context_manager_keys': ['intent', 'slots', 'text']
    }

    RemoteRuleBot = {
        'name': "RULES",
        'class': RemoteServiceModule,
        'url': ServiceURLLoader.get_url_for_module("GREETER"),
        'context_manager_keys': ['intent', 'slots', 'text']
    }

    RetrievalBot = {
        'name': "RETRIEVAL",
        'class': RemoteServiceModule,
        'url': ServiceURLLoader.get_url_for_module("RETRIEVAL"),
        'context_manager_keys': ['intent', 'slots', 'text']
    }

    cobot.add_response_generators([RemoteRuleBot, EviBot, RetrievalBot])
    return cobot.execute()

# if __name__ == '__main__':
# 	lambda_handler(event=new_session_event, context={})
