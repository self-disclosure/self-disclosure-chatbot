from cobot_core.service_module import ToolkitServiceModule, LocalServiceModule
import time


class DialogActIntentClassifier(ToolkitServiceModule):

    def execute(self):
        start = time.time()
        current_utterance = self.state_manager.current_state.text
        current_utterance_topic = self.state_manager.current_state.topic
        past_utterances = [turn.get('text', 'nan') for turn in self.state_manager.session_history]    # Reason to put 'nan': It's a workaround, since model is converting input to 'nan', and [None] doesn't work with conversation evaluator models.
        past_responses = [turn.get('response', 'nan') for turn in self.state_manager.session_history]

        past_utterances = past_utterances[0: 2] if len(past_utterances) > 2 else past_utterances
        past_responses = past_responses[0: 2] if len(past_responses) > 2 else past_responses

        past_utterances.reverse() #get the past utterances in the ascending order of creation time
        past_responses.reverse()  #get the past responses in the ascending order of creation time

        self.logger.debug(
            "past utterances, result: {}".format(past_utterances))
        self.logger.debug(
            "past responses, result: {}".format(past_responses))

        conversation = {
                "currentUtterance": current_utterance,
                "currentUtteranceTopic": current_utterance_topic,
                "pastUtterances": past_utterances,
                "pastResponses": past_responses
        }
        responses = self.toolkit_service_client.batch_get_dialog_act_intents(conversations=[conversation])

        intents = responses.get('dialogActIntents', [])
        end = time.time()
        self.logger.info(
            "Finished Dialog Act Intent, result: {}, latency:{}ms".format(intents, (end - start) * 1000))

        dialog_act_intent = intents[0] if len(intents) > 0 else None
        return dialog_act_intent