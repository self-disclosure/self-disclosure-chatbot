import warnings

import cobot_core as Cobot
from injector import inject

from cobot_common.service_client import get_client
from cobot_core import StateManager
from cobot_core.log.logger import LoggerFactory
from cobot_core.service_module import ToolkitServiceModule, LocalServiceModule
import time


class ConversationEvaluators(LocalServiceModule):
    """
    Conversation Evaluators uses AP science team's conversation evaluator models hosted by AlexaPrizeToolkitService.
    """
    @inject
    def __init__(self,
                 state_manager: StateManager):
        self.state_manager = state_manager
        if ToolkitServiceModule.api_key is None:
            self.api_key = None
            warnings.warn('Warning: API Key is not set')
        else:
            self.api_key = ToolkitServiceModule.api_key
            self.toolkit_service_client = get_client(api_key=self.api_key, timeout_in_millis=2000)
        self.logger = LoggerFactory.setup(self)

    def execute(self, conversations):
        result = [1.0] * len(conversations)  # isResponseErroneous is 1.0 for all conversations if API key is not specified
        if self.api_key:
            try:
                start = time.time()
                responses = self.toolkit_service_client.batch_get_conversation_scores(conversations=conversations)
                score_name = 'isResponseErroneous'  # this score is used, because preliminary model experiements show this metric closely correlate with conversation quality
                result = self._parse_score_from_conversation_evaluator_response(responses, score_name)
                end = time.time()
                self.logger.info(
                    "Finished Conversation Evaluators, result: {}, latency:{}ms".format(result, (end - start) * 1000))
            except:
                self.logger.error('Exception in calling Conversation Evaluator Model', exc_info=True)

        return result

    def _parse_score_from_conversation_evaluator_response(self, responses: dict, score_name: str):
        result = []
        for score in responses.get('conversationEvaluationScores', []):
            score = score.get(score_name, None)
            result.append(score)
        return result


class CustomRankingStrategyUsingConversationEvaluator(Cobot.RankingStrategy):

    @inject
    def __init__(self, state_manager: StateManager, conversation_evaluators: ConversationEvaluators):
        self.state_manager = state_manager
        self.conversation_evaluators = conversation_evaluators
        self.timeout_in_millis = 2000  # default timeout
        self.logger = LoggerFactory.setup(self)

    def rank(self, responses):
        current_utterance = self.state_manager.current_state.text
        past_utterances = [turn.get('text', 'nan') for turn in self.state_manager.session_history]    # Reason to put 'nan': It's a workaround, since model is converting input to 'nan', and [None] doesn't work with conversation evaluator models.
        past_responses = [turn.get('response', 'nan') for turn in self.state_manager.session_history]

        past_utterances = past_utterances[0: 2] if len(past_utterances) > 2 else past_utterances  # Recommendation: pass the last 2 turns' asr/tts as context
        past_responses = past_responses[0: 2] if len(past_responses) > 2 else past_responses
        past_utterances.reverse()   # get the past utterances in the ascending order of creation time
        past_responses.reverse()    # get the past responses in the ascending order of creation time

        candidate_conversation_list = []
        for candidate_response in responses:
            candidate_conversation = {
                "currentUtterance": current_utterance,
                "currentResponse": candidate_response,
                "pastUtterances": past_utterances,
                "pastResponses": past_responses
            }
            candidate_conversation_list.append(candidate_conversation)

        scores = self.conversation_evaluators.execute(conversations=candidate_conversation_list)

        ranked_best_response = responses[0] if len(responses) > 0 else None
        best_score = 1 # for 'isResponseErroneous' score, the lower the better
        for response, score in zip(responses, scores):
            if score < best_score:
                best_score = score
                ranked_best_response = response

        self.logger.info('Ranked best response {}'.format(ranked_best_response))
        return ranked_best_response
